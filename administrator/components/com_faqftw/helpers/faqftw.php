<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// No direct access to this file
defined('_JEXEC') or die;
 
/**
 * HelloWorld component helper.
 */
abstract class FaqftwHelper
{
        /**
         * Configure the Linkbar.
         */
        public static function addSubmenu($submenu) 
		{
				JSubMenuHelper::addEntry(
					JText::_('COM_FAQFTW_SUBMENU_DASHBOARD'),
					'index.php?option=com_faqftw&view=dashboard',
					$submenu == 'dashboard'
				);
                JSubMenuHelper::addEntry(
					JText::_('COM_FAQFTW_SUBMENU_FAQS'),
                    'index.php?option=com_faqftw&view=faqs',
				    $submenu == 'faqs'
				);
                JSubMenuHelper::addEntry(
					JText::_('COM_FAQFTW_SUBMENU_CATEGORIES'),
	                'index.php?option=com_categories&view=categories&extension=com_faqftw',
	                $submenu == 'categories'
				);
                // set some global property
                $document = JFactory::getDocument();
                if ($submenu == 'categories') 
                {
                     $document->setTitle(JText::_('COM_FAQFTW_ADMINISTRATION_CATEGORIES'));
                }
        }
		
		/**
	 * Gets a list of the actions that can be performed.
	 *
	 */
	public static function getActions($categoryId = 0, $messageId = 0)
	{	
		jimport('joomla.access.access');
		$user	= JFactory::getUser();
		$result	= new JObject;
 
		if (empty($messageId) && empty($categoryId)) {
			$assetName = 'com_faqftw';
		}
		else if (empty($messageId)) {
			$assetName = 'com_faqftw.category.'.(int) $categoryId;
		}
		else {
			$assetName = 'com_faqftw.faq.'.(int) $messageId;
		}
 
    $actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);
		             //   $actions = JAccess::getActions('com_faqftw', 'component');
 
		foreach ($actions as $action) {
			//$result->set($action->name, $user->authorise($action->name, $assetName));
			$result->set($action,	$user->authorise($action, $assetName));
		}
 
		return $result;
	}
}
