<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class FaqftwModelFaqs extends JModelList
{
		
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'a.id',
				'title', 'a.title',
				'alias', 'a.alias',
				'catid', 'a.catid', 'category_name',
				'access', 'a.access', 'access_level',
				'ordering', 'a.ordering',
				'publish_up', 'a.publish_up',
				'publish_down', 'a.publish_down',
				'state', 'a.state',
				'published'
			);
		}

		parent::__construct($config);
	}
	
	public function getListQuery()
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		$query->select(
				'a.id, a.title, a.alias, a.description, a.catid, a.state' .
				', a.checked_out, a.access, a.checked_out_time, a.ordering AS ordering,  a.asked_question, a.publish_up, a.publish_down'
			
		);
		$query->from('#__faqftw_faq AS a');
		
		// Join over the asset groups.
		$query->select('ag.title AS access_level');
		$query->join('LEFT', '#__viewlevels AS ag ON ag.id = a.access');
		
		// Join over categories.
		$query->select('fc.title AS category_name');
		$query->join('LEFT', '#__categories AS fc ON fc.id = a.catid');				

		// Filter by asked
		$asked = $this->getState('filter.asked_question');
		if (is_numeric($asked)) {
			$query->where('a.asked_question = ' . (int) $asked);
		}
	
		// Filter by access level.
		if ($access = $this->getState('filter.access')) {
			$query->where('a.access = '.(int) $access);
		}

		// Filter by state state
		$state = $this->getState('filter.state');
		if (is_numeric($state)) {
			$query->where('a.state = '.(int) $state);
		} elseif ($state == '') {
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by category state
		$category = $this->getState('filter.category');
		if (is_numeric($category)) {
			$query->where('a.catid = ' . (int) $category);
		}
		
		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(a.title LIKE ' . $search . ' OR a.alias LIKE ' . $search . ')');
			}
		}
		
		// Column ordering
		$orderCol = $this->getState('list.ordering','a.id');
		$orderDirn = $this->getState('list.direction','DESC');
		
		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		return $query;
	}
	
	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   type      The table type to instantiate
	 * @param   string    A prefix for the table class name. Optional.
	 * @param   array     Configuration array for model. Optional.
	 * @return  JTable    A database object
	 * @since   1.6
	 */
	public function getTable($type = 'Faq', $prefix = 'FaqftwTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	protected function populateState($ordering = null, $direction = null)
	{
		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
		
		$state = $this->getUserStateFromRequest($this->context.'.filter.state', 'filter_state');
		$this->setState('filter.state', $state);

		$accessId = $this->getUserStateFromRequest($this->context.'.filter.access', 'filter_access', null, 'int');
		$this->setState('filter.access', $accessId);
		
		$category = $this->getUserStateFromRequest($this->context.'.filter.category', 'filter_category', '');
		$this->setState('filter.category', $category);
		
		$asked = $this->getUserStateFromRequest($this->context.'.filter.asked_question', 'filter_asked_question', '');
		$this->setState('filter.asked_question', $asked);
		
		parent::populateState($ordering, $direction);
	}
}