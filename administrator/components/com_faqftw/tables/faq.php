<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class FaqftwTableFaq extends JTable
{
	public function __construct(&$db)
	{
		parent::__construct('#__faqftw_faq', 'id', $db);
	}

	public function store($updateNulls = false)
	{
		// Verify that the alias is unique
		$table = JTable::getInstance('Faq', 'FaqftwTable');
		if ($table->load(array('alias'=>$this->alias)) && ($table->id != $this->id || $this->id==0)) {
			if($this->asked_question == "1" && $this->id==0 ){
				$this->alias = JFactory::getDate()->format("Y-m-d-H-i-s");
			}else{
				$this->setError(JText::_('COM_FAQFTW_ERROR_UNIQUE_ALIAS'));
				return false;			
			}
		}
		/*		
		if($this->category_id == 0){
			$this->setError(JText::_('COM_FAQFTW_ERROR_SELECT_CATEGORY'));
			return false;
		}
*/
		// Attempt to store the data.
		return parent::store($updateNulls);
	}
	public function getCategory($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query = "SELECT * FROM #__categories WHERE id = ". $id . " AND extension = 'com_faqftw'";		
		$db->setQuery( $query );
		$cat = $db->loadObject();
		
		return $cat;
	}
	
	public function getFAQSByCategory($catid){
		$db = JFactory::getDbo();		
		$user = JFactory::getUser();
		$aid = max ($user->getAuthorisedViewLevels());

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__faqftw_faq');	
		$query->where('state = 1 AND access <= ' . $aid);
		$query->where('catid = ' . $catid);
		$query->order('ordering ASC');		
		
		$db->setQuery( $query );
		$rows = $db->loadObjectList();
		
		return $rows;
	}
	
	public function getFaq($id){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query->select('*');
		$query->from('#__faqftw_faq');
		$query->where('id = ' . $id);
		
		$db->setQuery($query);
		$row = $db->loadObject();
		
        return $row;
	}
	
	function check()
	{	
		if (empty($this->alias)) {
			$this->alias = $this->title;
		}
		
		$this->alias = JApplication::stringURLSafe($this->alias);
		if (trim(str_replace('-',' ', $this->alias)) == '') {
			$this->alias = JFactory::getDate()->format("Y-m-d-H-i-s");
		}
		
		if (empty($this->ordering)) {
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__faqftw_faq');
				$max = $db->loadResult();

				$this->ordering = $max+1;
		}
			
		return true;
	}
	
	function getSomeObjectsList(){
		$db = JFactory::getDbo();
		$db->setQuery('SELECT * FROM #__faqftw_faq');
		$rows = $db->loadObjectList();
        return $rows;
	}
	
	
	/**
	 * Method to set the publishing state for a row or list of rows in the database
	 * table.  The method respects checked out rows by other users and will attempt
	 * to checkin rows that it can after adjustments are made.
	 *
	 * @param   mixed	An optional array of primary key values to update.  If not
	 *					set the instance property value is used.
	 * @param   integer The publishing state. eg. [0 = unpublished, 1 = published, 2=archived, -2=trashed]
	 * @param   integer The user id of the user performing the operation.
	 * @return  boolean  True on success.
	 * @since   1.6
	 */
	public function publish($pks = null, $state = 1, $userId = 0)
	{
		$k = $this->_tbl_key;

		// Sanitize input.
		JArrayHelper::toInteger($pks);
		$userId = (int) $userId;
		$state  = (int) $state;

		// If there are no primary keys set check to see if the instance key is set.
		if (empty($pks))
		{
			if ($this->$k)
			{
				$pks = array($this->$k);
			}
			// Nothing to set publishing state on, return false.
			else {
				$this->setError(JText::_('JLIB_DATABASE_ERROR_NO_ROWS_SELECTED'));
				return false;
			}
		}

		// Get an instance of the table
		$table = JTable::getInstance('Faq', 'FaqftwTable');

		// For all keys
		foreach ($pks as $pk)
		{
			// Load the banner
			if (!$table->load($pk))
			{
				$this->setError($table->getError());
			}

			// Verify checkout
			if ($table->checked_out == 0 || $table->checked_out == $userId)
			{
				// Change the state
				$table->state = $state;
				$table->checked_out = 0;
				$table->checked_out_time = $this->_db->getNullDate();

				// Check the row
				$table->check();

				// Store the row
				if (!$table->store())
				{
					$this->setError($table->getError());
				}
			}
		}
		return count($this->getErrors()) == 0;
	}
}
