<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

// require helper file
JLoader::register('FaqftwHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'faqftw.php');


jimport('joomla.application.component.controller');

$controller = JControllerLegacy::getInstance('Faqftw');

// Access check: is this user allowed to access the backend of this component?
if (!JFactory::getUser()->authorise('core.manage', 'com_faqftw')) 
{
        return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

// Perform the Request task
$input = JFactory::getApplication()->input;
$controller->execute($input->getCmd('task'));
 
// Redirect if set by the controller
$controller->redirect();