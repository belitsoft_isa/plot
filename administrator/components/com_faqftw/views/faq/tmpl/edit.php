<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
 ?>
 <script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'faq.cancel' || document.formvalidator.isValid(document.id('faq-form'))) {
			Joomla.submitform(task, document.getElementById('faq-form'));
		}
	}
</script>
<form action="<?php echo JRoute::_('index.php?option=com_faqftw&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" name="adminForm" id="faq-form" class="form-validate">
	<?php echo JLayoutHelper::render('joomla.edit.title_alias', $this); ?>
	<div class="span4">
		<?php echo JLayoutHelper::render('joomla.edit.global', $this); ?>
		<?php 
		//if($this->item->id != 0){
		//	echo $this->form->getControlGroup('ordering'); 
		//} 
		?>
	</div>
	<div class="span8">
		<div class="clr"></div>
		<?php echo $this->form->getLabel('description'); ?>
		<div class="clr"></div>
		<?php echo $this->form->getInput('description'); ?>
	</div>
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
<?php 
return;
?>