<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class FaqftwViewFaq extends JViewLegacy
{
	protected $item;
	protected $form;

	public function display($tpl = null)
	{
		$this->item = $this->get('Item');
		$this->form = $this->get('Form');
		$this->state	= $this->get('State');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		$this->sidebar = JHtmlSidebar::render();
		$this->addToolbar();

		parent::display($tpl);
	}

	public function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);
	
		if ($this->item->id) {
			JToolBarHelper::title(JText::_('COM_FAQFTW_EDIT_FAQS_TITLE'));
		} else {
			JToolBarHelper::title(JText::_('COM_FAQFTW_ADD_FAQS_TITLE'));
		}

		JToolBarHelper::apply('faq.apply', 'JTOOLBAR_APPLY');
		JToolBarHelper::save('faq.save', 'JTOOLBAR_SAVE');
		JToolBarHelper::save2new('faq.save2new', 'JTOOLBAR_SAVE_AND_NEW');

		JToolBarHelper::cancel('faq.cancel');
	}
}