<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');

/**
 * Dashboard View
 */
class FaqftwViewDashboard extends JViewLegacy
{
        /**
         * Dashboard view display method
         * @return void
         */
        function display($tpl = null) {
	
				JHTML::_('behavior.tooltip', '.hasTip');
				jimport('joomla.html.pane');

	
				JToolBarHelper::title( "Dashboard: FAQ FTW", "faq");
				JHtml::stylesheet('com_faqftw/faq.css', array(), true, false, false);		
		    // Settings Icon in Top Bar
				JToolBarHelper::preferences('com_faqftw', '600', '800');
		
				parent::display($tpl);
				}
}