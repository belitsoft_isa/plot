<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$user= &JFactory::getUser();

// website root directory
$root = JURI::root();
?>

<table width="100%" border="0">
	<tr>
		<td width="55%" valign="top">

<div id="cpanel" style="float:left;">
  
	
	<div style="float:left;">
    	<div class="icon">
	    	<a href="index.php?option=com_categories&amp;extension=com_faqftw">
		    	<img alt="<?php echo JText::_('COM_FAQFTW_CATEGORIES'); ?>" src="<?php echo $root; ?>media/com_faqftw/images/dashboard/icon-48-category.png" />
		    	<span><?php echo JText::_('COM_FAQFTW_CATEGORIES'); ?></span>
	    	</a>
    	</div>
  	</div>


	<div style="float:left;">
    	<div class="icon">
	    	<a href="index.php?option=com_faqftw&amp;view=faqs">
		    	<img alt="<?php echo JText::_('COM_FAQFTW_FAQS'); ?>" src="<?php echo $root; ?>media/com_faqftw/images/dashboard/icon-48-article-add.png" />
		    	<span><?php echo JText::_('COM_FAQFTW_FAQS'); ?></span>
	    	</a>
    	</div>
  	</div>
  
	<div class="clr"></div>
  
</div>

</td>
		<td width="45%" valign="top">
			<table class="adminlist">
				<tr>
					<td>
						<center>
		    				<img src="<?php echo $root; ?>media/com_faqftw/images/dashboard/ftw-logo.png" />
	    				</center>
						<div style="font-weight:700;font-size:16px;margin-top:10px;">
							<?php echo JText::_('COM_FAQFTW_BROUGHT_TO_YOU_BY_FTWEXTENSIONS');?>
						</div>
						<br />
						<ul>
						<li style="font-size:14px;">
						Stucked? Can't find yourself? Head to <a href="http://extensions.4u2.co.il/documentation" target="_blank">our documentation</a> for more help.
						</li>
						<li style="font-size:14px;">
						Looking for some more cool extensions? Head to <a href="http://extensions.4u2.co.il/" target="_blank">our site</a> and you might find your wish.
						</li>
						</ul>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<div class="clr"></div>





