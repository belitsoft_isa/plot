<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

class FaqftwViewFaqs extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;
	protected $canDo;

	public function display($tpl = null)
	{
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->state = $this->get('State');
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		$this->canDo = FaqftwHelper::getActions();
		
		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		$this->addToolbar();
		
		parent::display($tpl);
	}
	
	public function addToolbar()
	{
		$canDo = FaqftwHelper::getActions();
		$user		= JFactory::getUser();
		JToolBarHelper::title(JText::_('COM_FAQFTW_FAQS_TITLE'), 'article-add.png');
    if ($canDo->get('core.create') || (count($user->getAuthorisedCategories('com_faqftw', 'core.create'))) > 0 ) {
		  JToolBarHelper::addNew('faq.add','JTOOLBAR_NEW');
		}
		if (($canDo->get('core.edit')) || ($canDo->get('core.edit.own'))) {
		  JToolBarHelper::editList('faq.edit','JTOOLBAR_EDIT');
		}
	
		if ($this->state->get('filter.state') != -2 && $canDo->get('core.delete')) {
		  JToolBarHelper::deleteList('', 'faqs.trash','JTOOLBAR_TRASH');
		}
		
		if ($this->state->get('filter.state') == -2 && $canDo->get('core.delete')) {
			JToolBarHelper::divider();
			JToolBarHelper::deleteList('', 'faqs.delete', 'JTOOLBAR_EMPTY_TRASH');

		}
		if ($canDo->get('core.edit.state')) {
		  JToolBarHelper::divider();
		  JToolBarHelper::custom('faqs.publish', 'publish.png', 'publish_f2.png','JTOOLBAR_PUBLISH', true);
		  JToolBarHelper::custom('faqs.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
		}
		if ($canDo->get('core.admin')) {
		  JToolBarHelper::divider();
		  // Settings Icon in Top Bar
		  JToolBarHelper::preferences('com_faqftw', '600', '800');		
		}
	}
	
	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
			'ordering' => JText::_('JGRID_HEADING_ORDERING'),
			'a.state' => JText::_('JSTATUS'),
			'a.title' => JText::_('COM_FAQFTW_FIELD_FAQ_NAME_LABEL'),
			'a.alias' => JText::_('COM_FAQFTW_FIELD_FAQ_ALIAS_LABEL'),
			'category_name' => JText::_('COM_FAQFTW_FIELD_FAQ_CATEGORY_LABEL'),
			'a.access' => JText::_('JGRID_HEADING_ACCESS'),
			'a.id' => JText::_('JGRID_HEADING_ID')
		);
	}
}