<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.controller');

class FaqftwController extends JControllerLegacy
{
		function display($cachable = false,$urlparams = false) 
		{
			require_once JPATH_COMPONENT.'/helpers/faqftw.php';
			JRequest::setVar('view', 'dashboard', 'method', false);
		     // set default view if not set
			 FaqftwHelper::addSubmenu(JRequest::getCmd('view', 'dashboard'));
			 
		    // call parent behavior
		    parent::display($cachable);
		 }

}