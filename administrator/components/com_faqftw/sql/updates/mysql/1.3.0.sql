ALTER TABLE `#__faqftw_faq`
CHANGE `published` `state` tinyint(4) DEFAULT NULL;
ALTER TABLE `#__faqftw_faq`
  ADD `publish_up` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';
ALTER TABLE `#__faqftw_faq`
  ADD `publish_down` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';
ALTER TABLE `#__faqftw_faq`
CHANGE `category_id` `catid` varchar(255) DEFAULT NULL;
