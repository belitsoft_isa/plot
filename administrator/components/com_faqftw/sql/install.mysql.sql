CREATE TABLE IF NOT EXISTS `#__faqftw_faq` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT,
 `title` varchar(255) DEFAULT NULL,
 `description` longtext,
 `access` int(11) DEFAULT NULL,
 `alias` varchar(255) DEFAULT NULL,
 `catid` varchar(255) DEFAULT NULL,
 `state` tinyint(4) DEFAULT NULL,
 `checked_out` int(11) DEFAULT '0',
 `checked_out_time` DATETIME DEFAULT NULL,
 `ordering` int(10) UNSIGNED DEFAULT NULL,
 `asked_question` int(11) DEFAULT '0',
 `publish_up` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
 `publish_down` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
 PRIMARY KEY (`id`),
 UNIQUE KEY `alias` (`alias`)
);


INSERT INTO `#__categories` (`asset_id` ,`parent_id` ,`lft` ,`rgt` ,`level` ,`path` ,`extension` ,`title` ,`alias`,`note` ,`description` ,`published` ,`checked_out` ,`checked_out_time` ,`access` ,`params` ,`metadesc` ,`metakey` ,`metadata` ,`created_user_id` ,`created_time` ,`modified_user_id` ,`modified_time` ,`hits` ,`language`) VALUES ('50', '1', '0', '1', '1', 'uncategorised', 'com_faqftw', 'Uncategorised', 'uncategorised', '', '<p>This is my first category!</p>', '1', '0', '0000-00-00 00:00:00', '1', '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', '0', '0000-00-00 00:00:00', '903', '2012-09-23 15:34:02', '0', '*'
);
