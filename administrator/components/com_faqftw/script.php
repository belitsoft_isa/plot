<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_faqftw'.DS.'tables');
 
//the name of the class must be the name of your component + InstallerScript
//for example: com_contentInstallerScript for com_content.
class com_faqftwInstallerScript
{
	/*
	 * $parent is the class calling this method.
	 * $type is the type of change (install, update or discover_install, not uninstall).
	 * preflight runs before anything else and while the extracted files are in the uploaded temp folder.
	 * If preflight returns false, Joomla will abort the update and undo everything already done.
	 */
	function preflight( $type, $parent ) {
		$jversion = new JVersion();

		// Installing component manifest file version
		$this->release = $parent->get( "manifest" )->version;
		
		// Manifest file minimum Joomla version
		$this->minimum_joomla_release = $parent->get( "manifest" )->attributes()->version;   

		// abort if the current Joomla release is older
		if( version_compare( $jversion->getShortVersion(), $this->minimum_joomla_release, 'lt' ) ) {
			Jerror::raiseWarning(null, 'Cannot install com_democompupdate in a Joomla release prior to '.$this->minimum_joomla_release);
			return false;
		}
		 if ( $type == 'update' ) {
			$db = JFactory::getDbo();
			$tableFields = $db->getTableColumns("#__faqftw_faq");
			if(!array_key_exists('state',$tableFields)){
				$query = 'ALTER TABLE `#__faqftw_faq`
							CHANGE `published` `state` tinyint(4) DEFAULT NULL;';
				$db->setQuery($query);
		   		$db->query();
			}
			if(!array_key_exists('publish_down',$tableFields)){
				$query = "ALTER TABLE `#__faqftw_faq`
 							ADD `publish_down` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';";
				$db->setQuery($query);
		   		$db->query();
			}
			if(!array_key_exists('publish_up',$tableFields)){
				$query = "ALTER TABLE `#__faqftw_faq`
							ADD `publish_up` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';";
				$db->setQuery($query);
		   		$db->query();
			}
			if(!array_key_exists('catid',$tableFields)){
				$query = 'ALTER TABLE `#__faqftw_faq`
							CHANGE `category_id` `catid` varchar(255) DEFAULT NULL;';
				$db->setQuery($query);
		   		$db->query();
			}
		}
		
	}
 
	/*
	 * $parent is the class calling this method.
	 * install runs after the database scripts are executed.
	 * If the extension is new, the install method is run.
	 * If install returns false, Joomla will abort the install and undo everything already done.
	 */
	function install( $parent ) {
		
		// You can have the backend jump directly to the newly installed component configuration page
		// $parent->getParent()->setRedirectURL('index.php?option=com_democompupdate');
	}
 
	/*
	 * $parent is the class calling this method.
	 * update runs after the database scripts are executed.
	 * If the extension exists, then the update method is run.
	 * If this returns false, Joomla will abort the update and undo everything already done.
	 */
	function update( $parent ) {
	
		// You can have the backend jump directly to the newly updated component configuration page
		// $parent->getParent()->setRedirectURL('index.php?option=com_democompupdate');
		

	}
 
	/*
	 * $parent is the class calling this method.
	 * $type is the type of change (install, update or discover_install, not uninstall).
	 * postflight is run after the extension is registered in the database.
	 */
	function postflight( $type, $parent ) {
	}

	/*
	 * $parent is the class calling this method
	 * uninstall runs before any other action is taken (file removal or database processing).
	 */
	function uninstall( $parent ) {
		echo '<p>' . JText::_('COM_FAQFTW_UNINSTALL') . '</p>';
	}
 
	/*
	 * get a variable from the manifest file (actually, from the manifest cache).
	 */
	function getParam( $name ) {
	
	}
 
	/*
	 * sets parameter values in the component's row of the extension table
	 */
	function setParams($param_array) {
	
	}
}
