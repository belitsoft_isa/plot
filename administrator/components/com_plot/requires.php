<?php
require_once JPATH_ADMINISTRATOR.'/components/com_plot/defines.php';

function pre($q, $die = false)
{
    echo'<pre style="color: black;">';
    print_r($q);
    echo'</pre>';
    if ($die) {
        die(' __END');
    }
}

require_once JPATH_ADMINISTRATOR.'/components/com_easysocial/includes/foundry.php';

$entitiesFiles = glob(JPATH_ADMINISTRATOR.'/components/com_plot/entities'.'/*.php');
foreach ($entitiesFiles as $file) {
    require_once $file;
}

require_once JPATH_ADMINISTRATOR.'/components/com_plot/plot_config.php';
