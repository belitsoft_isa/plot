<?php defined('_JEXEC') or die('Restricted access');


class PlotTableAges extends JTable
{
	//----------------------------------------------------------------------------------------------------
	function __construct(&$db) 
	{
        parent::__construct('#__plot_ages', 'id', $db);
	}
	//----------------------------------------------------------------------------------------------------
	public function delete($pk = null)
	{

		return parent::delete($pk);
	}
}