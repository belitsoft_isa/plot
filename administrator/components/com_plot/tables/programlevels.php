<?php defined('_JEXEC') or die('Restricted access');


class PlotTableProgramlevels extends JTable
{
	//----------------------------------------------------------------------------------------------------
	function __construct(&$db) 
	{
        parent::__construct('#__plot_program_levels', 'id', $db);
	}
	//----------------------------------------------------------------------------------------------------
	public function delete($pk = null)
	{

		return parent::delete($pk);
	}
}