<?php defined('_JEXEC') or die('Restricted access');


class PlotTablePrograms extends JTable
{
    //----------------------------------------------------------------------------------------------------
    function __construct(&$db)
    {
        parent::__construct('#__plot_program', 'id', $db);
    }
    //----------------------------------------------------------------------------------------------------
    public function delete($pk = null)
    {

        return parent::delete($pk);
    }

    public function getTags()
    {
        $tags = plotTags::get('program', $this->id);
        return $tags;
    }

    public function getAges()
    {
        $ages = plotAges::get('program', $this->id);
        return $ages;
    }

}