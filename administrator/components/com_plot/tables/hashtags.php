<?php defined('_JEXEC') or die('Restricted access');


class PlotTableHashtags extends JTable
{
	//----------------------------------------------------------------------------------------------------
	function __construct(&$db) 
	{
        parent::__construct('#__plot_hashtags', 'id', $db);
	}
	//----------------------------------------------------------------------------------------------------
	public function delete($pk = null)
	{

		return parent::delete($pk);
	}
}