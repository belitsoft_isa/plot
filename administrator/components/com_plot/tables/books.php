<?php defined('_JEXEC') or die('Restricted access');


class PlotTableBooks extends JTable
{
    //----------------------------------------------------------------------------------------------------
    function __construct(&$db)
    {
        parent::__construct('#__html5fb_publication', 'c_id', $db);
    }
    //----------------------------------------------------------------------------------------------------
    public function delete($pk = null)
    {

        return parent::delete($pk);
    }

    public function getTags()
    {
        $tags = plotTags::get('book', $this->c_id);
        return $tags;
    }

    public function getAges()
    {
        $ages = plotAges::get('book', $this->c_id);
        return $ages;
    }

}