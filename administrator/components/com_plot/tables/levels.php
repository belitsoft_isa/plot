<?php defined('_JEXEC') or die('Restricted access');


class PlotTableLevels extends JTable
{
	//----------------------------------------------------------------------------------------------------
	function __construct(&$db) 
	{
        parent::__construct('#__plot_levels', 'id', $db);
	}
	//----------------------------------------------------------------------------------------------------
	public function delete($pk = null)
	{

		return parent::delete($pk);
	}
}