<?php defined('_JEXEC') or die('Restricted access');


class PlotTableTexts extends JTable
{
	//----------------------------------------------------------------------------------------------------
	function __construct(&$db) 
	{
        parent::__construct('#__plot_texts', 'id', $db);
	}
	//----------------------------------------------------------------------------------------------------
	public function delete($pk = null)
	{

		return parent::delete($pk);
	}
}