<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class PlotController extends JControllerLegacy
{

    function display($cachable = false, $urlparams = false)
    {
        JRequest::setVar('view', JRequest::getCmd('view', 'Plot'));
        parent::display($cachable);
        $view = strtolower(JRequest::getVar('view'));
        PlotHelperAdmin::addSubmenu($view);
    }

}
