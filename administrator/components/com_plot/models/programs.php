<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');

class PlotModelPrograms extends JModelList
{

    //----------------------------------------------------------------------------------------------------
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'g.title',
                'g.id',
                'p.level'

            );
        }

        parent::__construct($config);
    }

    //----------------------------------------------------------------------------------------------------
    protected function populateState($ordering = null, $direction = null)
    {
        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);
        $id_level = $this->getUserStateFromRequest($this->context . 'filter.level', 'filter_id_level', '');
        $this->setState('filter.level', $id_level);

        parent::populateState('g.id', 'asc');
    }

    //----------------------------------------------------------------------------------------------------
    protected function getListQuery()
    {
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('g.*, l.title AS level_title')
            ->from('`#__social_clusters` AS `g`')
            ->leftJoin('`#__plot_program` AS `p` ON `p`.`id`=`g`.id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON `l`.`id`=`p`.level')
            ->where('`g`.`cluster_type`="group"');



        // Filter by search in name.

        $search = $this->getState('filter.search');

        if (!empty($search)) {
            $search = $db->Quote('%' . $db->escape($search, true) . '%');
            $query->where('`g`.`title` LIKE ' . $search);
        }
        $levelId = (int)$this->getState('filter.level');
        if (is_numeric( $levelId) &&  $levelId != 0) {
            $query->where('`p`.`level` = ' . (int) $levelId);
        }

        $query->order($db->escape($this->state->get('list.ordering').' '.$this->state->get('list.direction')));
     // die(var_dump($query->__toString()));
        return $query;
    }


    public function getSelectOptions()
    {
        $db = $this->_db;

        $query = $db->getQuery(true);
        $query->select('`id` AS level, `title`');
        $query->from('`#__plot_program_levels`');
        $query->order('`level` ASC');
        $db->setQuery($query);
        $rows = $db->loadObjectList();

        $options = array();

        foreach ($rows as $row) {
            $options[] = JHtml::_('select.option', $row->level, $row->title, 'value', 'text');
        }

        return $options;
    }



}
