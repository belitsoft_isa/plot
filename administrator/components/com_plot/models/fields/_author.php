<?php defined('_JEXEC') or die('Restricted Access');

jimport('joomla.form.formfield');

class JFormField_Author extends JFormField
{
    protected $type = '_author';

    //----------------------------------------------------------------------------------------------------
    public function __construct($form = null)
    {
        parent::__construct($form);
    }

    //----------------------------------------------------------------------------------------------------
    public function getLabel()
    {
        return parent::getLabel();
    }

    //----------------------------------------------------------------------------------------------------
    public function getInput()
    {
        $jinput = JFactory::getApplication()->input;
        $id= $jinput->get('c_id', 0);
        $selected = $this->getSelected();
        $html = array();
        $attr = '';
        $this->multiple = false;

        $attr .= $this->element['class'] ? ' class="' . (string)$this->element['class'] . '"' : '';

        if ((string)$this->element['readonly'] == 'true' || (string)$this->element['disabled'] == 'true') {
            $attr .= ' disabled="disabled"';
        }

        $attr .= ($this->element['size'] ? ' size="' . (int)$this->element['size'] . '"' : '');



if($selected){
    $html[] = '<input id="jform_author" type="hidden" name="' . $this->name . '" value="' . (int)$selected->author. '"/>';
    $html[] ='<div id="author_name">'.$selected->name.'</div><span id="remove_author">Remove</span><br>';
    $html[] ='<a class="modal" href="index.php?option=com_plot&task=searchusersbook.showPopup&c_id='.$id.'" rel="{size: {x: 600, y: 450}, handler: \'ajax\'}">Выбрать автора</a>';
}else{
    $html[] = '<input id="jform_author"   type="hidden" name="' . $this->name . '" value=""/>';
    $html[] = '<div id="author_name">Данная книга не имеет автора</div><span id="remove_author"></span><br>';
    $html[] ='<a class="modal" href="index.php?option=com_plot&task=searchusersbook.showPopup&c_id='.$id.'" rel="{size: {x: 600, y: 450}, handler: \'ajax\'}">Выбрать автора</a>';
}


        return implode($html);
    }

    //----------------------------------------------------------------------------------------------------
    public function setProperty($name, $value)
    {
        $this->element[$name] = $value;
    }



    //----------------------------------------------------------------------------------------------------
    protected function getSelected()
    {
        $selected ='';

            $jinput = JFactory::getApplication()->input;

            $id = $jinput->get('c_id', 0);

            if ($id != 0) {
                $db = JFactory::getDbo();

                $query = $db->getQuery(true);
                $query->select('p.`author`, `u`.`name`');
                $query->from('`#__plot_book_cost` AS p');
                $query->innerJoin('`#__users` AS u ON u.id=p.author');
                $query->where('p.`book_id` = ' . (int)$id);
                $db->setQuery($query);
                $selected = $db->loadObject();

            }


        return $selected;
    }

    //----------------------------------------------------------------------------------------------------
    public function setValue($value)
    {
        $this->value = $value;
    }
}