<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('list');

class JFormFieldAges extends JFormFieldList
{

    protected $type = 'ages';

    public function getInput()
    {

        $selectedIds = array();
        foreach ($this->value AS $val) {
            $selectedIds[] = $val->age_id;
        }
        
        $ages = plotAges::getList();
        
        $agesOptions = '';
        if($ages){
        foreach ($ages AS $age) {
            $agesOptions .= '<option '.(in_array($age->id, $selectedIds) ? 'selected="selected"': '').' value="'.$age->id.'" >'.$age->title.'</option>';
        }
        }
        return '<select multiple="multiple" id="'.$this->id.'" name="'.$this->name.'[]">'.
                $agesOptions.
                '</select>';
    }

}
