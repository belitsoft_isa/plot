<?php defined('_JEXEC') or die('Restricted Access');

jimport('joomla.form.formfield');

class JFormField_Textbook extends JFormField
{
    protected $type = '_textbook';

    //----------------------------------------------------------------------------------------------------
    public function __construct($form = null)
    {
        parent::__construct($form);
    }

    //----------------------------------------------------------------------------------------------------
    public function getLabel()
    {
        return parent::getLabel();
    }

    //----------------------------------------------------------------------------------------------------
    public function getInput()
    {
        $jinput = JFactory::getApplication()->input;

        $option = $jinput->get('option');

        $html = array();
        $attr = '';
        $this->multiple = false;

        $attr .= $this->element['class'] ? ' class="' . (string)$this->element['class'] . '"' : '';

        if ((string)$this->element['readonly'] == 'true' || (string)$this->element['disabled'] == 'true') {
            $attr .= ' disabled="disabled"';
        }

        $attr .= ($this->element['size'] ? ' size="' . (int)$this->element['size'] . '"' : '');

        $selected = $this->getSelected();
        jimport( 'joomla.html.editor' );
        $editor = JFactory::getEditor();

        $html[] =$editor->display($this->name, $selected, "400", "100", "150", "10", 1, null, null, null, array('theme' => 'advanced','format'=>'raw'));

        return implode($html);
    }

    //----------------------------------------------------------------------------------------------------
    public function setProperty($name, $value)
    {
        $this->element[$name] = $value;
    }



    //----------------------------------------------------------------------------------------------------
    protected function getSelected()
    {
        $selected ='';

            $jinput = JFactory::getApplication()->input;

            $id = $jinput->get('c_id', 0);

            if ($id != 0) {
                $db = JFactory::getDbo();

                $query = $db->getQuery(true);
                $query->select('p.`text`');
                $query->from('`#__plot_book_cost` AS p');
                $query->where('p.`book_id` = ' . (int)$id);
                $db->setQuery($query);
                $selected =$db->loadResult();

            }


        return $selected;
    }

    //----------------------------------------------------------------------------------------------------
    public function setValue($value)
    {
        $this->value = $value;
    }
}