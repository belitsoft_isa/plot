<?php defined('_JEXEC') or die('Restricted Access');

jimport('joomla.form.formfield');

class JFormField_Url extends JFormField
{
    protected $type = '_url';

    //----------------------------------------------------------------------------------------------------
    public function __construct($form = null)
    {
        parent::__construct($form);
    }

    //----------------------------------------------------------------------------------------------------
    public function getLabel()
    {
        return parent::getLabel();
    }

    //----------------------------------------------------------------------------------------------------
    public function getInput()
    {
        $jinput = JFactory::getApplication()->input;



        $html = array();
        $attr = '';
        $this->multiple = false;

        $attr .= $this->element['class'] ? ' class="' . (string)$this->element['class'] . '"' : '';

        if ((string)$this->element['readonly'] == 'true' || (string)$this->element['disabled'] == 'true') {
            $attr .= ' disabled="disabled"';
        }

        $attr .= ($this->element['size'] ? ' size="' . (int)$this->element['size'] . '"' : '');

        $selected = $this->getSelected();


        $html[] = '<input type="url" id="jform_url" name="' . $this->name . '" value="' . $selected. '"/>';

        return implode($html);
    }

    //----------------------------------------------------------------------------------------------------
    public function setProperty($name, $value)
    {
        $this->element[$name] = $value;
    }



    //----------------------------------------------------------------------------------------------------
    protected function getSelected()
    {
        $selected ='';

            $jinput = JFactory::getApplication()->input;

            $id = $jinput->get('c_id', 0);

            if ($id != 0) {

                $db = JFactory::getDbo();

                $query = $db->getQuery(true);
                $query->select('b.`path`');
                $query->from('`#__plot_books_files` AS b');
                $query->where('b.`type` = "url"');
                $query->where('b.`book_id` = '.$id);
                $db->setQuery($query);
                $selected = $db->loadResult();

            }


        return $selected;
    }

    //----------------------------------------------------------------------------------------------------
    public function setValue($value)
    {
        $this->value = $value;
    }
}