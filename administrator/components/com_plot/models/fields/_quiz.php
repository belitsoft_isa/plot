<?php defined('_JEXEC') or die('Restricted Access');
/*
* HTML5FlippingBook Component
* @package HTML5FlippingBook
* @author JoomPlace Team
* @copyright Copyright (C) JoomPlace, www.joomplace.com
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

JFormHelper::loadFieldClass('list');

class JFormField_Quiz extends JFormFieldList
{
    protected $type = '_quiz';

    //----------------------------------------------------------------------------------------------------
    public function __construct($form = null)
    {
        parent::__construct($form);
    }

    //----------------------------------------------------------------------------------------------------
    public function getLabel()
    {
        return parent::getLabel();
    }

    //----------------------------------------------------------------------------------------------------
    public function getInput()
    {
        $jinput = JFactory::getApplication()->input;

        $option = $jinput->get('option');

        $html = array();
        $attr = '';
        $this->multiple = false;

        $attr .= $this->element['class'] ? ' class="' . (string)$this->element['class'] . '"' : '';

        if ((string)$this->element['readonly'] == 'true' || (string)$this->element['disabled'] == 'true') {
            $attr .= ' disabled="disabled"';
        }

        $attr .= ($this->element['size'] ? ' size="' . (int)$this->element['size'] . '"' : '');
        $attr .= ($this->multiple ? ' multiple="multiple"' : '');

        $options = (array)$this->getOptions();
        array_unshift($options, JText::_('COM_PLOT_SELECT_QUIZ'));
        $selected = (array)$this->getSelected();

        if ((string)$this->element['readonly'] == 'true') {
            $html[] = JHtml::_('select.genericlist', $options, '', trim($attr), 'value', 'text', $this->value, $this->id);
            $html[] = '<input type="hidden" name="' . $this->name . '" value="' . $this->value . '"/>';
        } else {
            $html[] = JHtml::_('select.genericlist', $options, $this->name, trim($attr), 'value', 'text', $selected, $this->id);
        }

        return implode($html);
    }

    //----------------------------------------------------------------------------------------------------
    public function setProperty($name, $value)
    {
        $this->element[$name] = $value;
    }

    //----------------------------------------------------------------------------------------------------
    protected function getOptions()
    {
        $jinput = JFactory::getApplication()->input;
        $options=array();
        $id = $jinput->get('c_id', 0, 'INT');
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('m.`c_id` AS value, m.`c_title` AS text');
        $query->from('`#__quiz_t_quiz` AS m');
        $query->where('m.`c_id` != 0');
        $query->order('m.`c_title` ASC');
        $query->group('m.`c_id`');
        $db->setQuery($query);
        $options = $db->loadObjectList();

        $query = $db->getQuery(true);
        $query->select('`id_quiz`');
        $query->from('`#__plot_book_quiz_map`');
        $query->where('id_pub != ' . $id);
        $db->setQuery($query);
        $used_options = $db->loadObjectList();

        if ($options) {
            foreach ($options AS $key => $val) {
                if ($used_options) {
                    foreach ($used_options AS $u_option) {
                        if ((int)$val->value == (int)$u_option->id_quiz) {
                            unset($options[$key]);
                        }

                    }
                }
            }
        }
        return $options;
    }

    //----------------------------------------------------------------------------------------------------
    protected function getSelected()
    {
        $selected = array();


        $jinput = JFactory::getApplication()->input;

        $id = $jinput->get('c_id', 0);

        if ($id != 0) {
            $db = JFactory::getDbo();

            $query = $db->getQuery(true);
            $query->select('p.`id_quiz`');
            $query->from('`#__plot_book_quiz_map` AS p');
            $query->where('p.`id_pub` = ' . (int)$id);
            $db->setQuery($query);
            $selected = $db->loadColumn(0);

        }


        return $selected;
    }

    //----------------------------------------------------------------------------------------------------
    public function setValue($value)
    {
        $this->value = $value;
    }
}