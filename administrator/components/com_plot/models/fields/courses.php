<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('list');

class JFormFieldCourses extends JFormFieldList
{

    protected $type = 'courses';

    public function getInput()
    {

        $selectedIds = array();
        foreach ($this->value AS $val) {
            $selectedIds[] = $val->course_id;
        }
        
        $courses = plotCourse::getList();
        
        $agesOptions = '';
        if($courses){
        foreach ($courses AS $course) {
            $agesOptions .= '<option '.(in_array($course->id, $selectedIds) ? 'selected="selected"': '').' value="'.$course->id.'" >'.$course->course_name.'</option>';
        }
        }
        return '<select multiple="multiple" id="'.$this->id.'" name="'.$this->name.'[]">'.
                $agesOptions.
                '</select>';
    }

}
