<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('list');

class JFormFieldAfterhashtags extends JFormFieldList
{

    protected $type = 'afterhashtags';

    public function getInput()
    {

        $selectedIds = array();
        foreach ($this->value AS $val) {
            $selectedIds[] = $val->hashtag_id;
        }
        
        $hashtags = plotHashtags::getList();

        $hashtagsOptions = '';
        if($hashtags){
        foreach ($hashtags AS $hashtag) {
            $hashtagsOptions .= '<option '.(in_array($hashtag->id, $selectedIds) ? 'selected="selected"': '').' value="'.$hashtag->id.'" >'.$hashtag->title.'</option>';
        }
        }
        return '<select multiple="multiple" id="'.$this->id.'" name="'.$this->name.'[]">'.
                $hashtagsOptions.
                '</select>';
    }



}
