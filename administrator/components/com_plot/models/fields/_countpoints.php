<?php defined('_JEXEC') or die('Restricted Access');

jimport('joomla.form.formfield');

class JFormField_Countpoints extends JFormField
{
    protected $type = '_countpoints';

    //----------------------------------------------------------------------------------------------------
    public function __construct($form = null)
    {
        parent::__construct($form);
    }

    //----------------------------------------------------------------------------------------------------
    public function getLabel()
    {
        return parent::getLabel();
    }

    //----------------------------------------------------------------------------------------------------
    public function getInput()
    {
        $jinput = JFactory::getApplication()->input;

        $option = $jinput->get('option');

        $html = array();
        $attr = '';
        $this->multiple = false;

        $attr .= $this->element['class'] ? ' class="' . (string)$this->element['class'] . '"' : '';

        if ((string)$this->element['readonly'] == 'true' || (string)$this->element['disabled'] == 'true') {
            $attr .= ' disabled="disabled"';
        }

        $attr .= ($this->element['size'] ? ' size="' . (int)$this->element['size'] . '"' : '');

        $selected = (int)$this->getSelected();


        $html[] = '<input type="number" id="jform_countpoints" name="' . $this->name . '" value="' . $selected. '"/>';

        return implode($html);
    }

    //----------------------------------------------------------------------------------------------------
    public function setProperty($name, $value)
    {
        $this->element[$name] = $value;
    }



    //----------------------------------------------------------------------------------------------------
    protected function getSelected()
    {
        $selected ='';

            $jinput = JFactory::getApplication()->input;

            $id = $jinput->get('c_id', 0);

            if ($id != 0) {
                $db = JFactory::getDbo();

                $query = $db->getQuery(true);
                $query->select('p.`count_points`');
                $query->from('`#__plot_count_points` AS p');
                $query->where('p.`entity` = "book"' );
                $query->where('p.`entity_id` = ' . (int)$id);
                $db->setQuery($query);
                $selected =(int) $db->loadResult();

            }


        return $selected;
    }

    //----------------------------------------------------------------------------------------------------
    public function setValue($value)
    {
        $this->value = $value;
    }
}