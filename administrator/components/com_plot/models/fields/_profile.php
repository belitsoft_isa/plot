<?php defined('_JEXEC') or die('Restricted Access');
/*
* HTML5FlippingBook Component
* @package HTML5FlippingBook
* @author JoomPlace Team
* @copyright Copyright (C) JoomPlace, www.joomplace.com
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/
require_once JPATH_ADMINISTRATOR.'/components/com_plot/plot_config.php';
JFormHelper::loadFieldClass('list');

class JFormField_Profile extends JFormFieldList
{
    protected $type = '_profile';

    //----------------------------------------------------------------------------------------------------
    public function __construct($form = null)
    {
        parent::__construct($form);
    }

    //----------------------------------------------------------------------------------------------------
    public function getLabel()
    {
        return parent::getLabel();
    }

    //----------------------------------------------------------------------------------------------------
    public function getInput()
    {
        $jinput = JFactory::getApplication()->input;

        $option = $jinput->get('option');

        $html = array();
        $attr = '';
        $this->multiple = false;

        $attr .= $this->element['class'] ? ' class="' . (string)$this->element['class'] . '"' : '';

        if ((string)$this->element['readonly'] == 'true' || (string)$this->element['disabled'] == 'true') {
            $attr .= ' disabled="disabled"';
        }

        $attr .= ($this->element['size'] ? ' size="' . (int)$this->element['size'] . '"' : '');
        $attr .= ($this->multiple ? ' multiple="multiple"' : '');

        $options = (array)$this->getOptions();
        $selected = (array)$this->getSelected();

        if ((string)$this->element['readonly'] == 'true') {
            $html[] = JHtml::_('select.genericlist', $options, '', trim($attr), 'value', 'text', $this->value, $this->id);
            $html[] = '<input type="hidden" name="' . $this->name . '" value="' . $this->value . '"/>';
        } else {
            $html[] = JHtml::_('select.genericlist', $options, $this->name, trim($attr), 'value', 'text', $selected, $this->id);
        }

        return implode($html);
    }

    //----------------------------------------------------------------------------------------------------
    public function setProperty($name, $value)
    {
        $this->element[$name] = $value;
    }

    //----------------------------------------------------------------------------------------------------
    protected function getOptions()
    {

        $conf=new plotGlobalConfig();

        $options[0] = new stdClass();
        $options[0]->value = $conf->childProfileId;
        $options[0]->text = JText::_('COM_PLOT_CHILD_PROFILE');
        $options[1] = new stdClass();
        $options[1]->value = $conf->parentProfileId;
        $options[1]->text = JText::_('COM_PLOT_PARENT_PROFILE');

        return $options;
    }

    //----------------------------------------------------------------------------------------------------
    protected function getSelected()
    {
        $selected = array();

        $jinput = JFactory::getApplication()->input;

        $id = $jinput->get('id', 0);

        if ($id != 0) {
            $db = JFactory::getDbo();

            $query = $db->getQuery(true);
            $query->select('p.`profile`');
            $query->from('`#__plot_texts` AS p');
            $query->where('p.`id` = ' . (int)$id);
            $db->setQuery($query);
            $selected = $db->loadColumn(0);

        }
        //}

        return $selected;
    }

    //----------------------------------------------------------------------------------------------------
    public function setValue($value)
    {
        $this->value = $value;
    }
}