<?php defined('_JEXEC') or die('Restricted Access');
/*
* HTML5FlippingBook Component
* @package HTML5FlippingBook
* @author JoomPlace Team
* @copyright Copyright (C) JoomPlace, www.joomplace.com
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

JFormHelper::loadFieldClass('list');

class JFormField_Bookaudio extends JFormFieldList
{
    protected $type = '_bookaudio';

    //----------------------------------------------------------------------------------------------------
    public function __construct($form = null)
    {
        parent::__construct($form);
    }

    //----------------------------------------------------------------------------------------------------
    public function getLabel()
    {
        return parent::getLabel();
    }

    //----------------------------------------------------------------------------------------------------
    public function getInput()
    {
        $jinput = JFactory::getApplication()->input;

        $option = $jinput->get('file');

        $html = array();
        $attr = '';
        $this->multiple = false;

        $attr .= $this->element['class'] ? ' class="' . (string)$this->element['class'] . '"' : '';

        if ((string)$this->element['readonly'] == 'true' || (string)$this->element['disabled'] == 'true') {
            $attr .= ' disabled="disabled"';
        }

        $attr .= ($this->element['size'] ? ' size="' . (int)$this->element['size'] . '"' : '');


       $pdf = $this->getPdf();


      if($pdf){
          $html[]='<span id="plot-audio-file">'.$pdf->path.'</span><input type="button" value="'.JText::_('COM_PLOT_CHANGE').'" onclick="jQuery(\'#plot-audio-button\').show().click(); jQuery(this).hide(); jQuery(\'#plot-audio-file\').hide();" /><input style="display:none" id="plot-audio-button" type="file" name="' . $this->name . '" value="' . $this->value . '" />';
      }else{
          $html[] = '<input type="file" name="' . $this->name . '" value="' . $this->value . '" />';
      }



        return implode($html);
    }

    //----------------------------------------------------------------------------------------------------
    public function setProperty($name, $value)
    {
        $this->element[$name] = $value;
    }

    //----------------------------------------------------------------------------------------------------
    protected function getPdf()
    {
        $jinput = JFactory::getApplication()->input;
        $options=array();
        $id = $jinput->get('c_id', 0, 'INT');
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('b.`path`');
        $query->from('`#__plot_books_files` AS b');
        $query->where('b.`type` = "audio"');
        $query->where('b.`book_id` = '.$id);
        $db->setQuery($query);
        $options = $db->loadObject();

        return $options;
    }

    //----------------------------------------------------------------------------------------------------
    protected function getSelected()
    {
        $selected = array();


        $jinput = JFactory::getApplication()->input;

        $id = $jinput->get('c_id', 0);

        if ($id != 0) {
            $db = JFactory::getDbo();

            $query = $db->getQuery(true);
            $query->select('p.`id_quiz`');
            $query->from('`#__plot_book_quiz_map` AS p');
            $query->where('p.`id_pub` = ' . (int)$id);
            $db->setQuery($query);
            $selected = $db->loadColumn(0);

        }


        return $selected;
    }

    //----------------------------------------------------------------------------------------------------
    public function setValue($value)
    {
        $this->value = $value;
    }
}