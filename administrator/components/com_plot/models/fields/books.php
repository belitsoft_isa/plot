<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('list');

class JFormFieldBooks extends JFormFieldList
{

    protected $type = 'books';

    public function getInput()
    {

        $selectedIds = array();
        foreach ($this->value AS $val) {
            $selectedIds[] = $val->book_id;
        }
        
        $ages = plotBook::getList();
        
        $agesOptions = '';
        if($ages){
        foreach ($ages AS $age) {
            $agesOptions .= '<option '.(in_array($age->c_id, $selectedIds) ? 'selected="selected"': '').' value="'.$age->c_id.'" >'.$age->c_title.'</option>';
        }
        }
        return '<select multiple="multiple" id="'.$this->c_id.'" name="'.$this->name.'[]">'.
                $agesOptions.
                '</select>';
    }

}
