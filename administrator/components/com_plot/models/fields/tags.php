<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('list');

class JFormFieldTags extends JFormFieldList
{

    protected $type = 'tags';

    public function getInput()
    {
        $selectedIds = array();

        foreach ($this->value AS $val) {
            $selectedIds[] = $val->id;
        }
        
        $tags = plotTags::getK2TagsList();

        $tagsOptions = '';
        foreach ($tags AS $tag) {
            $tagsOptions .= '<option '.(in_array($tag->id, $selectedIds) ? 'selected="selected"': '').' value="'.$tag->id.'" >'.$tag->title.'</option>';
        }

        return '<select multiple="multiple" id="'.$this->id.'" name="'.$this->name.'[]">'.
                $tagsOptions.
                '</select>';
    }

}
