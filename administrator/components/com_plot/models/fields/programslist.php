<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('list');

class JFormFieldProgramslist extends JFormFieldList
{

    protected $type = 'programslist';

    public function getInput()
    {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        $selectedIds = array();
        foreach ($this->value AS $val) {

            $selectedIds[] = $val->item_id;
        }
        
        $programs = plotProgram::getList();
        
        $agesOptions = '';
        if($programs){
        foreach ($programs AS $item) {
            if($id!=$item->id){
                $agesOptions .= '<option '.(in_array($item->id, $selectedIds) ? 'selected="selected"': '').' value="'.$item->id.'" >'.$item->title.'</option>';
            }

        }
        }
        return '<select multiple="multiple" id="'.$this->id.'" name="'.$this->name.'[]">'.
                $agesOptions.
                '</select>';
    }

}
