<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');

class PlotModelRiver_texts extends JModelList
{

    public $listOrder;
    public $listDirection;
    public $filter = array();
    public $limitstart = 0;
    # if $limit equals 0 - it is no pagination need and return all items
    public $limit = 0;
    public $total = 0;

    public function __construct($config = array())
    {
        $this->listOrder = 'rt.ordering';
        $this->listDirection = 'ASC';

        parent::__construct($config);
    }

    public function getItems()
    {
        $query = $this->_db->getQuery(true);
        $query->select("SQL_CALC_FOUND_ROWS *")
                ->from("`#__plot_river_texts` AS `rt`")
                ->order($this->listOrder.' '.$this->listDirection);

        foreach ($this->filter AS $filterArray) {
            $query->where($this->_db->quoteName($filterArray['field']).' = '.$this->_db->quote($filterArray['value']));
        }

        if ($this->limit) {
            $this->_db->setQuery($query, $this->limitstart, $this->limit);
        } else {
            $this->_db->setQuery($query);
        }

        $riverTexts = $this->_db->loadObjectList();
        $this->total = $this->_db->setQuery('SELECT FOUND_ROWS()')->loadResult();

        return $riverTexts;
    }

    public function getRiverTextsPagination()
    {
        $store = $this->getStoreId('getPagination');

        // Try to load the data from internal storage.
        if (isset($this->cache[$store])) {
            return $this->cache[$store];
        }

        $page = new JPagination($this->total, $this->limitstart, $this->limit);

        // Add the object to the internal cache.
        $this->cache[$store] = $page;

        return $this->cache[$store];
    }


}
