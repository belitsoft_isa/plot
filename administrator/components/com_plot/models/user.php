<?php

defined('_JEXEC') or die('Restricted access');

class PlotModelUser extends JModelLegacy
{
    
    public function addChild($parentId, $childId)
    {
        $isChildExist = $this->isChildExist($parentId, $childId);
        if (!$isChildExist) {
            $query = "INSERT INTO `#__plot_parents_children_map` SET `parentId` = ".(int) $parentId." , `childId` = ".(int) $childId;
            $this->_db->setQuery($query)->query();
            return true;
        }
        return false;
    }
    
    public function removeChild($parentId, $childId)
    {
        $isChildExist = $this->isChildExist($parentId, $childId);
        if ($isChildExist) {
            $query = "DELETE FROM `#__plot_parents_children_map` WHERE `parentId` = ".(int) $parentId." AND `childId` = ".(int) $childId;
            $this->_db->setQuery($query)->query();
            return true;
        } 
        return false;
    }    
    
    public function isChildExist($parentId, $childId)
    {
        $query = "SELECT * FROM `#__plot_parents_children_map` WHERE `parentId` = ".(int)$parentId." AND `childId` = ".(int)$childId;
        $result = $this->_db->setQuery($query)->loadObject();
        $isChildExist = $result ? true : false;
        return $isChildExist;
    }
    
    public function getChildrenIds($parentId)
    {
        $query = "SELECT `childId` FROM `#__plot_parents_children_map` WHERE `parentId` = ".(int)$parentId;
        $childrenIds = $this->_db->setQuery($query)->loadColumn();
        return $childrenIds;
    }
    
    public function getParentsIds($childId)
    {
        $query = "SELECT `parentId` FROM `#__plot_parents_children_map` WHERE `childId` = ".(int)$childId;
        $parentsIds = $this->_db->setQuery($query)->loadColumn();
        return $parentsIds;
    }

    /**
 * returns count all users that have child profile
 *
 * @access	public
 * @return int
 */
    public function getCountAllChildren(){
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('COUNT(*)')
            ->from('`#__social_profiles_maps`')
            ->where('`state`=1')
            ->where('`profile_id`='.(int)plotGlobalConfig::getVar("childProfileId"));

        return (int)$this->_db->setQuery($query)->loadResult();
    }

    /**
     * returns count all users that have parents
     *
     * @access	public
     * @return int
     */
    public function getCountAllParents(){
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('COUNT(*)')
            ->from('`#__social_profiles_maps`')
            ->where('`state`=1')
            ->where('`profile_id`='.(int)plotGlobalConfig::getVar("parentProfileId"));

        return (int)$this->_db->setQuery($query)->loadResult();
    }

    /**
     * returns count all users that have social register profile
     *
     * @access	public
     * @return int
     */
    public function getCountAllSocialRegisteredUsers(){
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('COUNT(*)')
            ->from('`#__social_profiles_maps`')
            ->where('`state`=1')
            ->where('`profile_id`='.(int)plotGlobalConfig::getVar("registredProfileId"));

        return (int)$this->_db->setQuery($query)->loadResult();
    }
    
}
