<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');

class PlotModelBooks extends JModelList
{

    //----------------------------------------------------------------------------------------------------
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'p.c_id',
                'p.c_title',
                'm.id_quiz',
                'q.c_title',
                'cp.count_points',
                'bc.author_cost',

            );
        }

        parent::__construct($config);
    }

    //----------------------------------------------------------------------------------------------------
    protected function populateState($ordering = null, $direction = null)
    {
        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        $id_quiz = $this->getUserStateFromRequest($this->context . 'filter.id_quiz', 'filter_id_quiz', '');
        $this->setState('filter.id_quiz', $id_quiz);

        parent::populateState('p.c_id', 'asc');
    }

    //----------------------------------------------------------------------------------------------------
    protected function getListQuery()
    {
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('p.*, m.*, q.c_title AS q_title, cp.count_points, bc.author_cost, bc.author_min, bc.author_max, bc.admin_cost, bc.admin_min, bc.admin_max')
            ->from('`#__html5fb_publication` AS `p`')
            ->join('LEFT', '`#__plot_book_quiz_map` AS `m` ON `m`.`id_pub` = `p`.`c_id`')
            ->join('LEFT', '`#__plot_count_points` AS `cp` ON `cp`.`entity_id` = `p`.`c_id` AND `cp`.`entity`="book"')
            ->join('LEFT', '`#__plot_book_cost` AS `bc` ON `bc`.`book_id` = `p`.`c_id`')
            ->join('LEFT', '`#__quiz_t_quiz` AS `q` ON `q`.`c_id` = `m`.`id_quiz`');


        // Filter by search in name.

        $search = $this->getState('filter.search');

        if (!empty($search)) {
            $search = $db->Quote('%' . $db->escape($search, true) . '%');
            $query->where('`p`.`c_title` LIKE ' . $search);
        }

        $quizId = (int)$this->getState('filter.id_quiz');
        if (is_numeric($quizId) && $quizId != 0) {
            $query->where('`m`.`id_quiz` = ' . (int)$quizId);
        }
        $query->order($db->escape($this->state->get('list.ordering').' '.$this->state->get('list.direction')));

        return $query;
    }


    public function getSelectOptions()
    {
        $db = $this->_db;

        $query = $db->getQuery(true);
        $query->select('`c_id` AS id_quiz, `c_title`');
        $query->from('`#__quiz_t_quiz`');
        $query->order('`c_title` ASC');
        $db->setQuery($query);
        $rows = $db->loadObjectList();

        $options = array();

        foreach ($rows as $row) {
            $options[] = JHtml::_('select.option', $row->id_quiz, $row->c_title, 'value', 'text');
        }

        return $options;
    }

    public function getAges($items)
    {

        if ($items) {
            foreach ($items AS $item) {
                $db = $this->_db;

                $query = $db->getQuery(true);
                $query->select('am.*');
                $query->from('`#__plot_age_book_map` AS am');

                $query->where('am.book_id=' . (int)$item->c_id);
                $db->setQuery($query);
                $ages = $db->loadObjectList();
                $item->ages=array();

                if ($ages) {
                    foreach ($ages AS $age) {
                        $query = $db->getQuery(true);
                        $query->select('a.title');
                        $query->from('`#__plot_ages` AS a');
                        $query->where('a.id=' . (int)$age->age_id);
                        $db->setQuery($query);

                        $item->ages[] = $db->loadResult();
                    }
                }
            }
        }

        return $items;

    }

    /**
     * returns count all books authors
     *
     * @access	public
     * @return array ids
     */
    public function getCountAllAuthors(){
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('author')
            ->from('`#__plot_book_cost`')
            ->where('`author`!=0')
            ->group('author');
        $authors=$this->_db->setQuery($query)->loadColumn();;
       return $authors;
    }

}
