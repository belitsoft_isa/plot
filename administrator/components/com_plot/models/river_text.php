<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');
require_once JPATH_COMPONENT.'/models/model.php';

class PlotModelRiver_text extends PlotAdminModel
{

    public $listOrder;
    public $listDirection;

    public function __construct($config = array())
    {
        $this->listOrder = 'tr.ordering';
        $this->listDirection = 'ASC';

        parent::__construct($config);
    }

    public function getItems()
    {
        $query = $this->_db->getQuery(true);
        $query->select("*")
                ->from("`#__plot_river_texts` AS `rt`")
                ->order($this->listOrder.' '.$this->listDirection);

        $riverTexts = $this->_db->setQuery($query)->loadObjectList();

        return $riverTexts;
    }

    public function getForm()
    {
        $form = $this->loadForm('com_plot.river_text', 'river_text', array('control' => 'jform', 'load_data' => true), true);
        return $form;
    }

    protected function loadFormData()
    {
        $data = JFactory::getApplication()->getUserState($this->context, new stdClass);
        return $data;
    }

}
