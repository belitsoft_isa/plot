<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');
require_once JPATH_COMPONENT.'/models/model.php';

class PlotModelCourse extends JModelList
{

    public $listOrder;
    public $listDirection;

    public function __construct($config = array())
    {
        $this->listOrder = 'tr.ordering';
        $this->listDirection = 'ASC';

        parent::__construct($config);
    }

    public function getItems()
    {
        $query = $this->_db->getQuery(true);
        $query->select("*")
                ->from("`#__plot_courses` AS `rt`")
                ->order($this->listOrder.' '.$this->listDirection);

        $course = $this->_db->setQuery($query)->loadObjectList();

        return $course;
    }

    public function getForm()
    {
        $form = $this->loadForm('com_plot.course', 'course', array('control' => 'jform', 'load_data' => true), true);
        return $form;
    }

    protected function loadFormData()
    {
        $data = JFactory::getApplication()->getUserState($this->context, new stdClass);
        return $data;
    }

    public function getSelectedHashtags()
    {
        $db = JFactory::getDbo();
        $input = JFactory::getApplication()->input;
        $id = $input->get('id', 0, 'INT');

        $query = $db->getQuery(true);
        $query->select('a.hashtag_id');
        $query->from('`#__plot_hashtag_course_map` AS a');
        $query->where('a.course_id=' . $id);
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function getSelectedBeforeHashtags()
    {
        $db = JFactory::getDbo();
        $input = JFactory::getApplication()->input;
        $id = $input->get('id', 0, 'INT');

        $query = $db->getQuery(true);
        $query->select('a.hashtag_id');
        $query->from('`#__plot_before_hashtag_course_map` AS a');
        $query->where('a.course_id=' . $id);
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function getSelectedAfterHashtags()
    {
        $db = JFactory::getDbo();
        $input = JFactory::getApplication()->input;
        $id = $input->get('id', 0, 'INT');

        $query = $db->getQuery(true);
        $query->select('a.hashtag_id');
        $query->from('`#__plot_after_hashtag_course_map` AS a');
        $query->where('a.course_id=' . $id);
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function getHashTags()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id, a.title');
        $query->from('`#__plot_hashtags` AS a');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function saveHashtags($hashtags, $tablename='#__plot_hashtag_course_map')
    {

        $jinput = JFactory::getApplication()->input;
        $jform = $jinput->get('jform', array(), 'ARRAY');

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->delete($tablename)
            ->where('course_id=' . (int)$jform["id"]);
        $db->SetQuery($query);

        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

        foreach ($hashtags AS $hashtag) {
            $data = new stdClass();
            $data->hashtag_id = (int)$hashtag;
            $data->course_id = (int)$jform["id"];
            $db = JFactory::getDBO();
            $db->insertObject($tablename, $data);

        }

    }


}
