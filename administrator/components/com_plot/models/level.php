<?php defined('_JEXEC') or die('Restricted access');


class PlotModelLevel extends JModelAdmin
{
    protected $text_prefix = 'com_plot';

    //----------------------------------------------------------------------------------------------------
    public function getTable($type = 'levels', $prefix = 'PlotTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    //----------------------------------------------------------------------------------------------------
    public function getItem($pk = null)
    {
        return parent::getItem($pk);
    }

    //----------------------------------------------------------------------------------------------------
    protected function loadFormData()
    {
        $data = JFactory::getApplication()->getUserState('com_plot.edit.level.data', array());

        if (empty($data)) {
            $data = $this->getItem();
        }

        return $data;
    }

    //----------------------------------------------------------------------------------------------------
    public function getForm($data = array(), $loadData = true)
    {
        $app = JFactory::getApplication();

        $form = $this->loadForm('com_plot.levels', 'level', array('control' => 'jform', 'load_data' => $loadData));

        return (empty($form) ? false : $form);
    }

    //----------------------------------------------------------------------------------------------------
    public function delete(&$pks)
    {
        return parent::delete($pks);
    }

    public function checkLevel($id, $points)
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`m`.`id`')
            ->from('`#__plot_levels` AS `m`')
            ->where('`m`.`points`=' . (int)$points . ' AND `m`.`id`!=' . (int)$id);
        $db->setQuery($query);
        $levels = $db->loadResult();

        if ($levels == NULL) {
            $query = $db->getQuery(true)
                ->select('`m`.`points`,`m`.`id`')
                ->from('`#__plot_levels` AS `m`')
                ->where('`m`.`id` < ' . (int)$id)
                ->order('`m`.`id` DESC');
            $db->setQuery($query);
            $level = $db->loadRow();
            if ((int)$level[0] > (int)$points && $id != (int)$level[1]) {
                JFactory::getApplication()->enqueueMessage(JText::_('COM_PLOT_LEVEL_ERROR'), 'error');
                return false;
            } else {
                return true;
            }
        } else {
            JFactory::getApplication()->enqueueMessage(JText::_('COM_PLOT_DUBLICATE_LEVEL_OR_POINTS'), 'error');
            return false;
        }
    }

    public function saveImages()
    {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');

        if (!$id) {

            $id = $this->getLastId();
        }

        $db = $this->_db;
        $rEFileTypes = "/^\.(jpg|jpeg|gif|png|bmp|xcf|odg){1}$/i";

        if (!JFolder::exists(JPATH_SITE . "/media/com_plot")) {

            JFolder::create(JPATH_SITE . "/media/com_plot");

        }
        if (!JFolder::exists(JPATH_SITE . "/media/com_plot/levels")) {

            JFolder::create(JPATH_SITE . "/media/com_plot/levels");

        }

        //save beaver img
        if (isset($_FILES['beaver']) && $_FILES['beaver']["tmp_name"]) {
            $beaverfiles = JRequest::getVar('beaver', array(), 'files');
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot')) {

                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot');

            }
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'beaver')) {

                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'beaver');

            }


            $ext = JFile::getExt($beaverfiles['name']);

            $new_name = md5(time() . $beaverfiles['name']) . '.' . $ext;

            if (preg_match($rEFileTypes, strrchr($new_name, '.'))) {

                if (JFile::upload($beaverfiles['tmp_name'], JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'beaver' . DIRECTORY_SEPARATOR . $new_name)) {

                    if (JFile::exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'beaver' . DIRECTORY_SEPARATOR . $new_name)) {

                        JFile::move($beaverfiles["tmp_name"], JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'beaver' . DIRECTORY_SEPARATOR . $new_name);

                        chmod(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'beaver' . DIRECTORY_SEPARATOR . $new_name, 0644);
                        $query = $db->getQuery(true)
                            ->update('`#__plot_levels`')
                            ->set('`beaver` = "' . $new_name . '"')
                            ->where('`id` = ' . (int)$id);
                        $db->setQuery($query);
                        $db->execute();

                    }
                }

            }

        } else {
            $query = $db->getQuery(true)
                ->update('`#__plot_levels`')
                ->set('`beaver` = "' . $this->getPreviousBeaver() . '"')
                ->where('`id` = ' . (int)$id);
            $db->setQuery($query);
            $db->execute();
        }

        //save boat img
        if (isset($_FILES['boat']) && $_FILES['boat']["tmp_name"]) {
            $boatfiles = JRequest::getVar('boat', array(), 'files');
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot')) {

                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot');

            }
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'boat')) {

                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'boat');

            }


            $ext = JFile::getExt($boatfiles['name']);

            $new_name = md5(time() . $boatfiles['name']) . '.' . $ext;

            if (preg_match($rEFileTypes, strrchr($new_name, '.'))) {

                if (JFile::upload($boatfiles['tmp_name'], JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'boat' . DIRECTORY_SEPARATOR . $new_name)) {

                    if (JFile::exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'boat' . DIRECTORY_SEPARATOR . $new_name)) {

                        JFile::move($boatfiles["tmp_name"], JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'boat' . DIRECTORY_SEPARATOR . $new_name);

                        chmod(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'boat' . DIRECTORY_SEPARATOR . $new_name, 0644);
                        $query = $db->getQuery(true)
                            ->update('`#__plot_levels`')
                            ->set('`boat` = "' . $new_name . '"')
                            ->where('`id` = ' . (int)$id);
                        $db->setQuery($query);
                        $db->execute();
                    }
                }

            }

        } else {
            $query = $db->getQuery(true)
                ->update('`#__plot_levels`')
                ->set('`boat` = "' . $this->getPreviousBoat() . '"')
                ->where('`id` = ' . (int)$id);
            $db->setQuery($query);
            $db->execute();
        }

        //save house img
        if (isset($_FILES['house']) && $_FILES['house']["tmp_name"]) {
            $housefiles = JRequest::getVar('house', array(), 'files');
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot')) {

                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot');

            }
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'house')) {

                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'house');

            }


            $ext = JFile::getExt($housefiles['name']);

            $new_name = md5(time() . $housefiles['name']) . '.' . $ext;

            if (preg_match($rEFileTypes, strrchr($new_name, '.'))) {

                if (JFile::upload($housefiles['tmp_name'], JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'house' . DIRECTORY_SEPARATOR . $new_name)) {

                    if (JFile::exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'house' . DIRECTORY_SEPARATOR . $new_name)) {

                        JFile::move($housefiles["tmp_name"], JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'house' . DIRECTORY_SEPARATOR . $new_name);

                        chmod(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'house' . DIRECTORY_SEPARATOR . $new_name, 0644);
                        $query = $db->getQuery(true)
                            ->update('`#__plot_levels`')
                            ->set('`house` = "' . $new_name . '"')
                            ->where('`id` = ' . (int)$id);
                        $db->setQuery($query);
                        $db->execute();
                    }
                }

            }

        } else {
            $query = $db->getQuery(true)
                ->update('`#__plot_levels`')
                ->set('`house` = "' . $this->getPreviousHouse() . '"')
                ->where('`id` = ' . (int)$id);
            $db->setQuery($query);
            $db->execute();
        }
    }

    public function getLastId()
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('MAX(id)')
            ->from('#__plot_levels');
        $db->setQuery($query);

        return $db->loadResult();
    }

    public function getPreviousBoat()
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('boat')
            ->from('#__plot_levels')
            ->where('boat!=""')
            ->order('id DESC');
        $db->setQuery($query);
        return $db->loadResult();
    }

    public function getPreviousBeaver()
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('beaver')
            ->from('#__plot_levels')
            ->where('beaver!=""')
            ->order('id DESC');
        $db->setQuery($query);
        return $db->loadResult();
    }

    public function getPreviousHouse()
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('house')
            ->from('#__plot_levels')
            ->where('house!=""')
            ->order('id DESC');
        $db->setQuery($query);
        return $db->loadResult();
    }

    public function deleteImg($path,$type){
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->update('`#__plot_levels`')
            ->set('`'.$type.'` =""' )
            ->where('`'.$type.'` = "' .$path.'"');

        $db->setQuery($query);

        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }
        return true;
    }

}