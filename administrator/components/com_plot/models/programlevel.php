<?php defined('_JEXEC') or die('Restricted access');


class PlotModelProgramlevel extends JModelAdmin
{
    protected $text_prefix = 'com_plot';

    //----------------------------------------------------------------------------------------------------
    public function getTable($type = 'programlevels', $prefix = 'PlotTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    //----------------------------------------------------------------------------------------------------
    public function getItem($pk = null)
    {
        return parent::getItem($pk);
    }

    //----------------------------------------------------------------------------------------------------
    protected function loadFormData()
    {
        $data = JFactory::getApplication()->getUserState('com_plot.edit.programlevel.data', array());

        if (empty($data)) {
            $data = $this->getItem();
        }

        return $data;
    }

    //----------------------------------------------------------------------------------------------------
    public function getForm($data = array(), $loadData = true)
    {
        $app = JFactory::getApplication();

        $form = $this->loadForm('com_plot.programlevels', 'programlevel', array('control' => 'jform', 'load_data' => $loadData));

        return (empty($form) ? false : $form);
    }

    //----------------------------------------------------------------------------------------------------
    public function delete(&$pks)
    {
        return parent::delete($pks);
    }

    public function checkLevel($id, $level)
    {
        $db = $this->_db;
        if($this->isExist($id)){

            $query = $db->getQuery(true)
                ->select('`m`.`id`')
                ->from('`#__plot_program_levels` AS `m`')
                ->where('`m`.`level`=' . (int)$level . ' AND `m`.`id`!=' . (int)$id);
            $db->setQuery($query);
           // die(var_dump($query->__toString()));
            $res=$db->loadResult();
            if($res){
                JFactory::getApplication()->enqueueMessage(JText::_('COM_PLOT_PROGRAMMLEVEL_ERROR_LEVEL_EXIST'), 'error');
            }
            return $res;
        }else{
            $query = $db->getQuery(true)
                ->select('`m`.`id`')
                ->from('`#__plot_program_levels` AS `m`')
                ->where('`m`.`level`=' . (int)$level);
            $db->setQuery($query);
            $res=$db->loadResult();
            if($res){
                JFactory::getApplication()->enqueueMessage(JText::_('COM_PLOT_PROGRAMMLEVEL_ERROR_LEVEL_EXIST'), 'error');
            }
            return $res;
        }


    }

    public function isExist($id){
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`m`.`id`')
            ->from('`#__plot_program_levels` AS `m`')
            ->where('`m`.`id`='.(int)$id);
        $db->setQuery($query);
        return $db->loadResult();
    }

    public function isProgramExistWithLevelExist($idLevel){
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`m`.`id`')
            ->from('`#__plot_program` AS `m`')
            ->where('`m`.`level`='.(int)$idLevel);
        $db->setQuery($query);
        return $db->loadObjectList();
    }

}