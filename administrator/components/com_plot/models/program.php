<?php defined('_JEXEC') or die('Restricted access');


class PlotModelProgram extends JModelAdmin
{
    protected $text_prefix = 'com_plot';

    //----------------------------------------------------------------------------------------------------
    public function getTable($type = 'programs', $prefix = 'PlotTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    public function getItem($pk = null)
    {
        $item = parent::getItem($pk);



        return $item;
    }
    //----------------------------------------------------------------------------------------------------
    protected function loadFormData()
    {
        $data = JFactory::getApplication()->getUserState('com_plot.edit.program.data', array());

        if (empty($data))
        {
            $data = $this->getItem();
        }

        return $data;
    }
    //----------------------------------------------------------------------------------------------------
    public function getForm($data = array(), $loadData = true)
    {
        $app = JFactory::getApplication();

        $form = $this->loadForm('com_plot.program', 'program', array('control' => 'jform', 'load_data' => $loadData));

        return (empty($form) ? false : $form);
    }
    //----------------------------------------------------------------------------------------------------
    public function delete(&$pks)
    {
        return parent::delete($pks);
    }

    public function getSelectedAges()
    {
        $db = JFactory::getDbo();
        $input = JFactory::getApplication()->input;
        $id = $input->get('id', 0, 'INT');

        $query = $db->getQuery(true);
        $query->select('a.age_id');
        $query->from('`#__plot_age_program_map` AS a');
        $query->where('a.program_id=' . $id);
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function getSelectedBooks()
    {
        $db = JFactory::getDbo();
        $input = JFactory::getApplication()->input;
        $id = $input->get('id', 0, 'INT');

        $query = $db->getQuery(true);
        $query->select('a.book_id');
        $query->from('`#__plot_books_program_map` AS a');
        $query->where('a.program_id=' . $id);
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function getSelectedPrograms()
    {
        $db = JFactory::getDbo();
        $input = JFactory::getApplication()->input;
        $id = $input->get('id', 0, 'INT');

        $query = $db->getQuery(true);
        $query->select('a.item_id');
        $query->from('`#__plot_programs_program_map` AS a');
        $query->where('a.program_id=' . $id);
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function getSelectedCourses()
    {
        $db = JFactory::getDbo();
        $input = JFactory::getApplication()->input;
        $id = $input->get('id', 0, 'INT');

        $query = $db->getQuery(true);
        $query->select('a.course_id');
        $query->from('`#__plot_course_program_map` AS a');
        $query->where('a.program_id=' . $id);
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function getAges()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id, a.title');
        $query->from('`#__plot_ages` AS a');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function getBooks()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('m.c_id , m.c_title ');
        $query->from('`#__html5fb_publication` AS m');
        $query->where('m.`published` = 1');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function getPrograms()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('g.id , g.title ');
        $query->from('`#__social_clusters` AS g');
        $query->where('g.`cluster_type` = "group"');
        $query->where('g.`state` = 1');
        $db->setQuery($query);
        $rows = $db->loadObjectList();

        return $rows;
    }

    public function getCourses()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('m.id , m.course_name ');
        $query->from('`#__lms_courses` AS m');
        $query->where('m.`published` = 1');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function save($data){
        $db = JFactory::getDbo();

        if($data['id']){
        $programObj=new plotProgram($data['id']);

            if($programObj->isProgramExist($data['id'])){
                $program = new stdClass();
                $program->id = (int)$data['id'];
                $program->link=$data['link'];
                $program->level=(int)$data['level'];
                $db->updateObject('#__plot_program', $program, 'id');
            }else{
                $program = new stdClass();
                $program->link=$data['link'];
                $program->level=(int)$data['level'];
                $program->id=(int)$data['id'];
                $db->insertObject('#__plot_program', $program, 'id');
            }
            $programObj->updateAges($data['ages']);
            $this->updateBooks($data['books'], $data['id']);
            $this->updateCourses($data['courses'], $data['id']);
            $this->updatePrograms($data['programslist'], $data['id']);
        }

    }


    private function updateBooks($books,$id){
        $db = JFactory::getDbo();

        $query = "DELETE FROM `#__plot_books_program_map` WHERE program_id=".(int)$id;
        $result = $db->setQuery($query)->query();
        foreach ($books as $book) {
            $program = new stdClass();
            $program->book_id=(int)$book;
            $program->program_id=$id;
            $db->insertObject('#__plot_books_program_map', $program, 'program_id');
        }

    }

    private function updatePrograms($programs,$id){
        $db = JFactory::getDbo();

        $query = "DELETE FROM `#__plot_programs_program_map` WHERE program_id=".(int)$id;
        $result = $db->setQuery($query)->query();
        foreach ($programs as $item) {
            $program = new stdClass();
            $program->item_id=(int)$item;
            $program->program_id=$id;
            $db->insertObject('#__plot_programs_program_map', $program, 'program_id');
        }

    }


    private function updateCourses($courses,$id){
        $db = JFactory::getDbo();
        $query = "DELETE FROM `#__plot_course_program_map` WHERE program_id=".(int)$id;
        $result = $db->setQuery($query)->query();
        foreach ($courses as $course) {
            $program = new stdClass();
            $program->course_id=(int)$course;
            $program->program_id=$id;
            $db->insertObject('#__plot_course_program_map', $program, 'program_id');
        }

    }








}