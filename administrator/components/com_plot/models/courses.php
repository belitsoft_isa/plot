<?php

defined('_JEXEC') or die('Restricted access');

class PlotModelCourses extends JModelList
{

    public $listOrder;
    public $listDirection;
    public $filter = array();
    public $limitstart = 0;
    # if $limit equals 0 - it is no pagination need and return all items
    public $limit = 0;
    public $total = 0;

    public function __construct($config = array())
    {
        $this->listOrder = 'rt.ordering';
        $this->listDirection = 'ASC';

        parent::__construct($config);
    }

    //----------------------------------------------------------------------------------------------------
    protected function populateState($ordering = null, $direction = null)
    {
        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);


        parent::populateState(`rt`.`id`, 'asc');
    }


    public function getItems()
    {
        $query = $this->_db->getQuery(true);
        $query->select("SQL_CALC_FOUND_ROWS `rt`.*, `pc`.`admin_min_cost`, `pc`.`admin_cost`, `pc`.`admin_max_cost`, `pc`.`author_min_cost`, `pc`.`author_cost`, `pc`.`author_max_cost`, `pc`.`min_cost`, `cp`.`count_points` AS `count_points`")
                ->from("`#__lms_courses` AS `rt`")
                ->leftJoin("`#__plot_courses` AS `pc` ON (`pc`.`id` = `rt`.`id`)")
                ->leftJoin("`#__plot_count_points` AS `cp` ON (`cp`.`entity` = 'course' AND `cp`.`entity_id` = `rt`.`id`)")
                ->order($this->listOrder.' '.$this->listDirection);

        $search = $this->getState('filter.search');

        if (!empty($search)) {
            $search =  $this->_db->Quote('%' .  $this->_db->escape($search, true) . '%');
            $query->where('`rt`.`course_name` LIKE ' . $search);
        }

        foreach ($this->filter AS $filterArray) {
            $query->where($this->_db->quoteName($filterArray['field']).' = '.$this->_db->quote($filterArray['value']));
        }

        if ($this->limit) {
            $this->_db->setQuery($query, $this->limitstart, $this->limit);
        } else {
            $this->_db->setQuery($query);
        }

        $riverTexts = $this->_db->loadObjectList();
        $this->total = $this->_db->setQuery('SELECT FOUND_ROWS()')->loadResult();

        return $riverTexts;
    }

    public function getCoursesPagination()
    {
        $store = $this->getStoreId('getPagination');

        // Try to load the data from internal storage.
        if (isset($this->cache[$store])) {
            return $this->cache[$store];
        }

        $page = new JPagination($this->total, $this->limitstart, $this->limit);

        // Add the object to the internal cache.
        $this->cache[$store] = $page;

        return $this->cache[$store];
    }

    /**
     * returns count all published courses
     *
     * @access	public
     * @return int
     */
    public function getCountAllCourses(){

        $query = "SELECT SQL_CALC_FOUND_ROWS DISTINCT `c`.*, `cats`.`c_category`, `pc`.*, (`pc`.`admin_min_cost` + `pc`.`author_min_cost`) AS `total_min_cost` FROM `#__lms_courses` AS `c` "
            . "LEFT JOIN `#__lms_course_cats` AS `cats` ON (`c`.`cat_id`=`cats`.`id`) "
            . "LEFT JOIN `#__plot_courses` AS `pc` ON (`c`.`id` = `pc`.`id`) "
            . "INNER JOIN `#__plot_age_course_map` AS `ages` ON (`ages`.`course_id` = `c`.`id` ) "
            . "INNER JOIN `#__plot_tags` AS `tags` ON (`tags`.`entity` = 'course' AND `tags`.`entityId` = `c`.`id` ) "
            . "WHERE `c`.`published` = 1";

        $courses = $this->_db->setQuery($query)->loadObjectList();
        $total = $this->_db->setQuery('SELECT FOUND_ROWS()')->loadResult();
        return (int)$total;
    }

    /**
     * returns count all course authors
     *
     * @access	public
     * @return array ids
     */
    public function getCountAllAuthors(){
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('owner_id')
            ->from('`#__lms_courses`')
            ->where('`owner_id`!=0')
            ->group('owner_id');
        $authors=$this->_db->setQuery($query)->loadColumn();
        return  $authors;
    }

}
