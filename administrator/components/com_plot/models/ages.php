<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');

class PlotModelAges extends JModelList
{

    //----------------------------------------------------------------------------------------------------
    public function __construct($config = array())
    {
        if (empty($config['filter_fields']))
        {
            $config['filter_fields'] = array(
                'a.id',
                'a.title',
                'a.from',
                'a.to',

            );
        }

        parent::__construct($config);
    }
    //----------------------------------------------------------------------------------------------------
    protected function populateState($ordering = null, $direction = null)
    {
        $search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        parent::populateState('a.id', 'asc');
    }
    //----------------------------------------------------------------------------------------------------
    protected function getListQuery()
    {
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('a.*')
            ->from('`#__plot_ages` AS `a`');

        // Filter by search in name.

        $search = $this->getState('filter.search');

        if (!empty($search))
        {
            $search = $db->Quote('%'.$db->escape($search, true).'%');
            $query->where('`a`.`title` LIKE '.$search);
        }
        $query->order($db->escape($this->state->get('list.ordering').' '.$this->state->get('list.direction')));
        return $query;
    }

}
