<?php defined('_JEXEC') or die('Restricted access');
/*
* HTML5FlippingBook Component
* @package HTML5FlippingBook
* @author JoomPlace Team
* @copyright Copyright (C) JoomPlace, www.joomplace.com
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

class PlotModelHashtag extends JModelAdmin
{
	protected $text_prefix = 'com_plot';
	//----------------------------------------------------------------------------------------------------
	public function getTable($type = 'hashtags', $prefix = 'PlotTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	//----------------------------------------------------------------------------------------------------
	public function getItem($pk = null)
	{
		return parent::getItem($pk);
	}
	//----------------------------------------------------------------------------------------------------
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState('com_plot.edit.hashtag.data', array());
		
		if (empty($data))
		{
			$data = $this->getItem();
		}

		return $data;
	}
	//----------------------------------------------------------------------------------------------------
	public function getForm($data = array(), $loadData = true)
	{
		$app = JFactory::getApplication();
		
		$form = $this->loadForm('com_plot.hashtags', 'hashtag', array('control' => 'jform', 'load_data' => $loadData));
		
		return (empty($form) ? false : $form);
	}
	//----------------------------------------------------------------------------------------------------
	public function delete(&$pks)
	{
        $db = JFactory::getDbo();
$cids=implode(',', $pks);

        $query = $db->getQuery(true);
        $query->delete('#__plot_hashtag_book_map')
            ->where('hashtag_id IN (' . $cids.')');
        $db->SetQuery($query);

        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->enqueueMessage($e->getMessage());
            return;
        }
		return parent::delete($pks);
	}
}