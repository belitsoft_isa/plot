<?php defined('_JEXEC') or die('Restricted access');
/*
* HTML5FlippingBook Component
* @package HTML5FlippingBook
* @author JoomPlace Team
* @copyright Copyright (C) JoomPlace, www.joomplace.com
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

class PlotModelBook extends JModelAdmin
{
    protected $text_prefix = 'com_plot';

    //----------------------------------------------------------------------------------------------------
    public function getTable($type = 'books', $prefix = 'PlotTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    public function getItem($pk = null)
    {
        $item = parent::getItem($pk);



        return $item;
    }
    //----------------------------------------------------------------------------------------------------
    protected function loadFormData()
    {
        $data = JFactory::getApplication()->getUserState('com_plot.edit.book.data', array());

        if (empty($data))
        {
            $data = $this->getItem();
        }

        return $data;
    }
    //----------------------------------------------------------------------------------------------------
    public function getForm($data = array(), $loadData = true)
    {
        $app = JFactory::getApplication();
       // $form = $this->loadForm('com_html5flippingbook.publications', 'publication', array('control' => 'jform', 'load_data' => $loadData));
        $form = $this->loadForm('com_plot.book', 'book', array('control' => 'jform', 'load_data' => $loadData));

        return (empty($form) ? false : $form);
    }
    //----------------------------------------------------------------------------------------------------
    public function delete(&$pks)
    {
        return parent::delete($pks);
    }

    public function getSelectedAges()
    {
        $db = JFactory::getDbo();
        $input = JFactory::getApplication()->input;
        $id = $input->get('c_id', 0, 'INT');

        $query = $db->getQuery(true);
        $query->select('a.age_id');
        $query->from('`#__plot_age_book_map` AS a');
        $query->where('a.book_id=' . $id);
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function getSelectedTags()
    {
        $db = JFactory::getDbo();
        $input = JFactory::getApplication()->input;
        $id = $input->get('c_id', 0, 'INT');

        $query = $db->getQuery(true);
        $query->select('a.tagId AS id');
        $query->from('`#__plot_tags` AS a');
        $query->where('a.entityId=' . $id.' AND a.entity="book"');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function getSelectedHashtags()
    {
        $db = JFactory::getDbo();
        $input = JFactory::getApplication()->input;
        $id = $input->get('c_id', 0, 'INT');

        $query = $db->getQuery(true);
        $query->select('a.hashtag_id');
        $query->from('`#__plot_hashtag_book_map` AS a');
        $query->where('a.book_id=' . $id);
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function getSelectedBeforeHashtags()
    {
        $db = JFactory::getDbo();
        $input = JFactory::getApplication()->input;
        $id = $input->get('c_id', 0, 'INT');

        $query = $db->getQuery(true);
        $query->select('a.hashtag_id');
        $query->from('`#__plot_before_hashtag_book_map` AS a');
        $query->where('a.book_id=' . $id);
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function getSelectedAfterHashtags()
    {
        $db = JFactory::getDbo();
        $input = JFactory::getApplication()->input;
        $id = $input->get('c_id', 0, 'INT');

        $query = $db->getQuery(true);
        $query->select('a.hashtag_id');
        $query->from('`#__plot_after_hashtag_book_map` AS a');
        $query->where('a.book_id=' . $id);
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function getBookAges()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id, a.title');
        $query->from('`#__plot_ages` AS a');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function getAdminCost()
    {
        $db = JFactory::getDbo();
        $input = JFactory::getApplication()->input;
        $id = $input->get('c_id', 0, 'INT');

        $query = $db->getQuery(true);
        $query->select('a.admin_cost');
        $query->from('`#__plot_book_cost` AS a');
        $query->where('a.book_id=' . $id);
        $db->setQuery($query);
        $rows = (int)$db->loadResult();

        return $rows;
    }

    public function saveAge($ages)
    {

        $jinput = JFactory::getApplication()->input;
        $jform = $jinput->get('jform', array(), 'ARRAY');

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->delete('#__plot_age_book_map')
            ->where('book_id=' . (int)$jform["c_id"]);
        $db->SetQuery($query);

        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

        foreach ($ages AS $age) {

            $data = new stdClass();
            $data->age_id = (int)$age;
            $data->book_id = (int)$jform["c_id"];

            $db = JFactory::getDBO();
            $db->insertObject('#__plot_age_book_map', $data);

        }

    }


    public function saveTags($tags)
    {

        $jinput = JFactory::getApplication()->input;
        $jform = $jinput->get('jform', array(), 'ARRAY');

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->delete('#__plot_tags')
            ->where('entityId=' . (int)$jform["c_id"].' AND entity="book"');
        $db->SetQuery($query);

        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

        foreach ($tags AS $tag) {

            $data = new stdClass();
            $data->tagId = (int)$tag;
            $data->entity="book";
            $data->entityId = (int)$jform["c_id"];

            $db = JFactory::getDBO();
            $db->insertObject('#__plot_tags', $data);

        }


    }


    public function saveHashtags($hashtags, $tablename='#__plot_hashtag_book_map')
    {

        $jinput = JFactory::getApplication()->input;
        $jform = $jinput->get('jform', array(), 'ARRAY');

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->delete($tablename)
            ->where('book_id=' . (int)$jform["c_id"]);
        $db->SetQuery($query);

        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

        foreach ($hashtags AS $hashtag) {
            $data = new stdClass();
            $data->hashtag_id = (int)$hashtag;
            $data->book_id = (int)$jform["c_id"];
            $db = JFactory::getDBO();
            $db->insertObject($tablename, $data);

        }

    }



    public function saveQuiz()
    {
        $jinput = JFactory::getApplication()->input;
        $jform = $jinput->get('jform', array(), 'ARRAY');
        $db = JFactory::getDbo();

        if ((int)$jform['id_quiz']) {
            $query = $db->getQuery(true);
            $query->select('a.id_quiz');
            $query->from('`#__plot_book_quiz_map` AS a');
            $query->where('a.`id_pub` = ' .(int)$jform['c_id']);
            $db->setQuery($query);
            $id_quiz = (int)$db->loadResult();

            if($id_quiz){
                $query = $db->getQuery(true)
                    ->update('`#__plot_book_quiz_map`')
                    ->set('`id_quiz` = '.(int)$jform['id_quiz'])
                    ->where('`id_pub` = ' .(int)$jform['c_id']);
                $db->setQuery($query);
                try {
                    $db->execute();
                } catch (RuntimeException $e) {
                    $this->setError($e->getMessage());
                    return false;
                }
            }else{
                $data = new stdClass();
                $data->id_quiz = (int)$jform['id_quiz'];
                $data->id_pub = (int)$jform['c_id'];

                $db = JFactory::getDBO();
                $db->insertObject('#__plot_book_quiz_map', $data);
            }

        }else{
            $query = $db->getQuery(true)
                ->update('`#__plot_book_quiz_map`')
                ->set('`id_quiz` = '.(int)$jform['id_quiz'])
                ->where('`id_pub` = ' .(int)$jform['c_id']);
            $db->setQuery($query);
            try {
                $db->execute();
            } catch (RuntimeException $e) {
                $this->setError($e->getMessage());
                return false;
            }
            JError::raiseWarning(500, JText::_('COM_PLOT_NOT_QUIZ_SELECTED'));
        }



    }

    public function saveCost()
    {
        $jinput = JFactory::getApplication()->input;
        $jform = $jinput->get('jform', array(), 'ARRAY');

        $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('a.book_id');
            $query->from('`#__plot_book_cost` AS a');
            $query->where('a.`book_id` = ' .(int)$jform['c_id']);
            $db->setQuery($query);
            $cost = (int)$db->loadResult();

            if($cost){
                $query = $db->getQuery(true)
                    ->update('`#__plot_book_cost`')
                    ->set('`author_cost` = '.(int)$jform['author_cost'])
                    ->set('`author_min` = '.(int)$jform['author_min'])
                    ->set('`author_max` = '.(int)$jform['author_max'])
                    ->set('`admin_cost` = '.(int)$jform['admin_cost'])
                    ->set('`admin_min` = '.(int)$jform['admin_min'])
                    ->set('`admin_max` = '.(int)$jform['admin_max'])
                    ->set('`author` = '.(int)$jform['author'])
                    ->set('`text` = '.$db->quote($db->escape(htmlentities(str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n"),"",$jform['text'])))))
                    ->where('`book_id` = ' .(int)$jform['c_id']);

                $db->setQuery($query);
                try {
                    $db->execute();
                } catch (RuntimeException $e) {
                    $this->setError($e->getMessage());
                    return false;
                }
            }else{
                $data = new stdClass();
                $data->author_cost = (int)$jform['author_cost'];
                $data->author_min = (int)$jform['author_min'];
                $data->author_max = (int)$jform['author_max'];
                $data->admin_cost = (int)$jform['admin_cost'];
                $data->admin_min = (int)$jform['admin_min'];
                $data->admin_max = (int)$jform['admin_max'];
                $data->author = (int)$jform['author'];
                $data->book_id = (int)$jform['c_id'];
                $data->text = $db->escape($jform['text']);

                $db = JFactory::getDBO();
                $db->insertObject('#__plot_book_cost', $data);
            }





    }
    public function savePoints()
    {
        $jinput = JFactory::getApplication()->input;
        $jform = $jinput->get('jform', array(), 'ARRAY');
        $db = JFactory::getDbo();


        $query = $db->getQuery(true);
        $query->select('a.entity_id');
        $query->from('`#__plot_count_points` AS a');
        $query->where('a.`entity` = "book"');
        $query->where('a.`entity_id` = ' .(int)$jform['c_id']);
        $db->setQuery($query);
        $entity_id = (int)$db->loadResult();

        if($entity_id){
            $query = $db->getQuery(true)
                ->update('`#__plot_count_points`')
                ->set('`count_points` = '.(int)$jform['count_points'])
                ->where('`entity` = "book"')
                ->where('`entity_id` = ' .(int)$jform['c_id']);
            $db->setQuery($query);
            try {
                $db->execute();
            } catch (RuntimeException $e) {
                $this->setError($e->getMessage());
                return false;
            }
        }else{
            $data = new stdClass();
            $data->entity = 'book';
            $data->entity_id = (int)$jform['c_id'];
            $data->count_points = (int)$jform['count_points'];

            $db = JFactory::getDBO();
            $db->insertObject('#__plot_count_points', $data);
        }





    }

    public function getTags()
    {
        $tags = plotTags::get('book', $this->id);

        return $tags;
    }

    public function getHashTags()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id, a.title');
        $query->from('`#__plot_hashtags` AS a');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }


    public function saveBookFiles($format)
    {
        $rEFileTypes = "/^\.(".plotGlobalConfig::getVar('bookFilesType')."){1}$/i";
        $file = JRequest::getVar('jform', '', 'FILES');
        if (!empty($file['tmp_name'][$format.'files'])) {
            $file_path = JPATH_SITE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'bookfiles';
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'bookfiles')) {
                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'bookfiles');
            }

            $ext = JFile::getExt($file['name'][$format.'files']);
            $new_name = md5(time() . $file['name'][$format.'files']) . '.' . $ext;
            if (preg_match($rEFileTypes, strrchr($new_name, '.'))) {
                if (JFile::copy($file['tmp_name'][$format.'files'], $file_path . '/' . $new_name)) {
                    return  $this->addBookFiles($new_name, $ext);
                } else {

                    JError::raiseWarning(500, JText::_('COM_PLOT_INVALID_FORMAT'));
                    return false;
                }
            } else {
                JError::raiseWarning(500, JText::_('COM_PLOT_INVALID_FORMAT'));
                //JFactory::getApplication()->enqueueMessage(JText::_('COM_PLOT_INVALID_FORMAT'), 'error');
                return false;
            }

        }
    }


    private function addBookFiles($new_name, $ext){
        $jinput = JFactory::getApplication()->input;
        $jform = $jinput->get('jform', array(), 'ARRAY');
        $id = (int)$jform['c_id'];
        $db = JFactory::getDbo();
        $allowedTypes = explode('|', plotGlobalConfig::getVar('bookAudioFilesType'));
        if(in_array($ext,$allowedTypes)){
            $ext='audio';
        }
        $book=$this->isBookFileExist($id,$ext);
        if($book){
            $query = $db->getQuery(true)
                ->update('`#__plot_books_files`')
                ->set('`path` = "'.$new_name.'"')
                ->where('`type` = "'.$ext.'"')
                ->where('`book_id` = ' .(int)$id);
            $db->setQuery($query);
            try {
                $db->execute();
            } catch (RuntimeException $e) {
                $this->setError($e->getMessage());
                return false;
            }
            $path=JPATH_SITE.DIRECTORY_SEPARATOR.'media'.DIRECTORY_SEPARATOR. 'com_plot' . DIRECTORY_SEPARATOR . 'bookfiles'.DIRECTORY_SEPARATOR.$book->path;
            unlink($path);
        }else{
            $data = new stdClass();
            $data->book_id = $id;
            $data->type = $ext;
            $data->path = $new_name;
            $db = JFactory::getDBO();
            $db->insertObject('#__plot_books_files', $data);
        }

        return true;
    }

    public function addBookLink(){
        $jinput = JFactory::getApplication()->input;
        $db = JFactory::getDbo();
        $jform = $jinput->get('jform', array(), 'ARRAY');
        $id = (int)$jform['c_id'];
        $url=trim($jform['url']);
        $book=$this->isBookFileExist($id,'url');
        if($book){
            $query = $db->getQuery(true)
                ->update('`#__plot_books_files`')
                ->set('`path` = "'.$db->escape($url).'"')
                ->where('`type` = "url"')
                ->where('`book_id` = ' .(int)$id);
            $db->setQuery($query);
            try {
                $db->execute();
            } catch (RuntimeException $e) {
                $this->setError($e->getMessage());
                return false;
            }

        }else{
            $data = new stdClass();
            $data->book_id = $id;
            $data->type = 'url';
            $data->path = $db->escape($url);
            $db = JFactory::getDBO();
            $db->insertObject('#__plot_books_files', $data);
        }
    }

    public function isBookFileExist($book_id,$entity){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.id, a.path');
        $query->from('`#__plot_books_files` AS a');
        $query->where('a.`book_id` = ' .(int)$book_id);
        $query->where('a.type="'.$entity.'"');
        $db->setQuery($query);
        return $db->loadObject();
    }

}