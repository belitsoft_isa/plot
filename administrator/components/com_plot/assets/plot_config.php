<?php

class plotGlobalConfig
{

    public $childProfileId = 2;
    public $parentProfileId = 3;
    public $registredProfileId = 1;
    # tags (interests)
    public $tagsK2CategoryId = 1;
    public $maxTagsForUser = 14;
    public $countPointsForAddTag = 1;
    public $countPointsForAddReasonForTag = 4;
    public $countPointsForChooseSmileyForTag = 5;
    # joomla lms
    public $joomlaLMSRoleStudentId = 2;
    public $joomlaLMSDefaultPointForQuizQuestion = 10;
    #friends
    public $maxNumberOfFriends = 15;
    #video config
    public $videoTypes = 'avi,3gpp,mp4,ogg,webm,ogv,x-flv,x-ms-wmv';
    public $maxVideoSize = 60;
    #book
    public $maxPersentForUnreadBook = 90;
    public $minPersentForOpenBookTest = 30;
    public $bookFilesType = "pdf|epub|fb2|mobi|mp3";
    public $bookAudioFilesType = "mp3|midi";
    #portfolio
    public $countShowLastPortfolios = 2;
    #search
    public $limitSearchItems = 2;
    public $quickDescrBookSearch = 150;
    #search count books
    public $limitSearchBooksItems = 15;
    public $limitSearchBooksChildrenItems = 20;
    public $booksDescriptionMaxSymbolsToShow = 55;
    public $booksChildDescriptionMaxSymbolsToShow = 105;
    #search count children
    public $limitSearchChildrenItems = 18;
    #search count adults
    public $limitSearchAdultsItems = 18;
    #search count courses
    public $limitSearchCoursesItems = 12;
    #search count meetings
    public $limitSearchMeetingsItems = 12;
    public $limitSearchMeetingsChildrenItems = 12;

    #stream
    public $streamDefaultLimit = 3;

    # profile page
    public $searchTitleLimitChars = 29;
    public $coursesNewTopShowInTopMenuCount = 4;
    public $maxStreamItemsCountProfilePage = 10;
    public $courseCostPaymentSystemPercent = 4;
    public $parentProfileCirtificatesLimit = 10;
    public $parentProfilePhotosLimit = 10;
    # profile edit popup
    public $profileEditPopupFriendsMaxNameLength = 65;

    # courses
    public $coursesSmallDefaultAuthorMinCostAbs = 5;
    public $coursesSmallDefaultAuthorPercent = 10;
    public $coursesSmallDefaultAuthorMaxCostAbs = 6;
    public $coursesSmallDefaultQuizPercentToPass = 100;
    public $coursesSmallDefaultFirsCount = 30;
    public $courseDescriptionMaxSymbolsForDisplay = 700;
    # courses page child
    public $coursesResultsShowFirstCount = 6;
    public $coursesDescriptionMaxSymbolsToShow = 120;
    # courses page parent
    public $coursesNewShowForParent = 12;
    public $coursesResultsShowFirstCountParent = 4;
    # course page parent
    public $payForCourseCompleteDefault = 0;
    public $usersCountSearchForBuyCourse = 10;
#picture fo course certificate
    public $coursePicture = 'images/com_plot/course_certificate.jpg';

    #courses id category for book
    public $courseBookCategoryId = 24;

    #count books on shelf  in publication view
    public $countBooksOnShelf = 4;

    #count books on shelf  in read department
    public $countBooksOnShelfReadDepartment = 8;

    #default child birthday
    public $defaultChildBirthday = array('year' => 1990, 'month' => 01, 'day' => '01');

    #publications page parent
    public $booksResultsShowFirstCountParent = 6;
    public $booksParentResultsOnSelf=5;
    public $booksResultsShowFirstCountChild = 4;
    public $booksChildResultsOnSelf=4;

    #book parent page
    public $bookCostPaymentSystemPercent = 4;
    public $payForBookCompleteDefault = 0;
    public $usersCountSearchForBuyBook = 10;

    # footer
    public $footerLinkK2idForChildrens = 11;
    public $footerLinkK2idForParents = 12;
    public $footerLinkK2idThink = 13;
    public $footerLinkK2idToAuthors = 14;
    public $footerLinkK2idCommunities = 15;
    public $footerLinkK2idGames = 16;
    public $footerLinkK2idLearnMore = 17;
    public $footerLinkK2idAboutProject = 18;
    public $footerLinkK2idParticipationRules = 19;
    public $footerLinkK2idUsersAgreement = 20;
    public $footerLinkK2idContacts = 111;
    public $footerLinkK2idCompanies = 150;
    public $footerLinkidJoomBlog=258;

    # system vars
    public $error404K2ItemId = 24;

    #courses size
    public $courseCropWidthMin = 280;
    public $courseCropHeightMin = 168;

    #book size
    public $bookCropWidthMin = 168;
    public $bookCropHeightMin = 235;

    #meetings size
    public $meetingCropWidthMin = 168;
    public $meetingCropHeightMin = 235;

    #avatar size
    public $avatarCropWidthMin = 168;
    public $avatarCropHeightMin = 168;

    #certificate size
    public $certificateCropWidthMin = 212;
    public $certificateCropHeightMin = 297;

    #portfolio size
    public $portfolioCropWidthMin = 168;
    public $portfolioCropHeightMin = 168;

    #video size
    public $videoCropWidthMin = 212;
    public $videoCropHeightMin = 297;

    #photo
    public $photoCropWidthMin = 212;
    public $photoCropHeightMin = 297;

    #activity
    public $childActivityTitleMaxSymbolsToShow = 30;
    public $childActivityAllDescriptionMaxSymbolsToShow = 110;
    public $childPageAllDescriptionMaxSymbolsToShow = 110;
    public $parentActivityAllDescriptionMaxSymbolsToShow = 90;
    # yandex + cashouts
    public $yandexShopId = 21705;
    # public $yandexScid = '56968'; # test
    public $yandexScid = '21655'; # real
    public $yandexShopPassword = 'U1NE40I152YWJDI527VA';
    public $yandexAppAccessToken = '410011413709827.90665E4D8471E79DF0E0E55F7F7BC287B0AE2785B59E9577F4D7EB78F1D73A276870AEB70DD4204988E988FD82D53AFA28F5699BA6BC02DC04FA7582AB11812ACFD596301B805990B0B736FB1C61D818E08E822653DC7A6F8E29F491898C96C2BC6C49B2BDF1BBF49318B271EB69764F3AFF28C428747AF0225AE220BD93DD87';
    public $yandexDaysToMarkOrderAsExpired = '3';

    # wallet
    public $walletChildK2ItemTextId = 120;
    public $walletParentK2ItemTextId = 121;

    #meetings
    public $meetingIsNewMaxDays = 1;
    public $meetingsDaysForSend = 7;
    public $plotMenuHomePageItemId = 104;
    public $meetingsChildPageShowFirstCount = 3;
    public $meetingsParentPageShowFirstCount = 4;
    # social networks
    public $fbAppId = '473991812742077';

    # popup about me
    public $maxLenghtSurname = 20;
    public $maxLenghtFirstName = 20;
    public $maxLenghtPatronymic = 20;
    public $maxLenghtEmail = 30;
    public $maxLenghtPhone = 20;
    public $maxLenghtPassword = 30;
    public $maxLenghtStatus = 20;
    public $maxLenghtRegion = 30;
    public $maxLenghtCity = 30;
    public $maxLenghtSchool = 30;
    public $maxLenghtSchoolAddress = 30;
    public $maxLenghtAboutMe = 450;

    #all my page (photos)
    public $childPhotoCount = 6;
    public $parentPhotoCount = 8;
    public $childCertificateCount = 6;
    public $parentCertificateCount = 8;
    public $childVideoCount = 6;
    public $parentVideoCount = 8;
    public $photosPageChildCoursesCount = 6;
    public $photosPageParentCoursesCount = 8;
    public $photosPageChildPublicationsCount = 6;
    public $photosPageParentPublicationsCount = 6;
    public $photosPageChildEventsCount = 6;
    public $photosPageParentEventsCount = 6;
    public $photosChildMaxCourseDescriptionSymbols = 100;

    #essay
    public $childEssayCount = 1;
    public $parentEssayCount = 1;
    public $essayPicture = 'images/com_plot/essay_background.jpg';
    public $essayTextDelimiter=' ';
    public $essayTextDelimiter2='\n';
    public $essayTextMaxSymbols=130;
    public $essayTextareaMaxSymbols=1300;

    #Children slider
    public $childSliderSlide1Text = "Я найду на плоту новых друзей и буду учиться вместе с ними - зарегистрирую свой аккаунт и добавлю друзей по интересам.";
    public $childSliderSlide1Img = "child1_1940x1228.jpg";
    public $childSliderSlide2Text = "Зарегистрируйся. Выбери детский аккаунт, будь внимателен - у аккаунтов разные функции.<br/><br/>Заполни свой профиль, укажи интересы. Так ты быстрее найдешь друзей! Добавь в друзья своего Взрослого.<br/><br/>Выбери курсы и книги по интересам или категориям, покажи их своему Взрослому для одобрения и мотивационного платежа.<br/><br/>Прочитай книгу, пройди курс, успешно сдай тест и испыташку. Деньги и другие бонусы окажутся в твоем профиле.<br/><br/><br/><br/><br/>";
    public $childSliderSlide2Img = "blank150x95.jpg";
    public $childSliderSlide3Text = "Я буду изучать школьные предметы и все-все на свете вместе с друзьями на Плоту - выберу курсы и книги, родители их одобрят, и я пройду все на отлично!";
    public $childSliderSlide3Img = "child3_1940x1228.jpg";
    public $childSliderSlide4Text = "На Плоту я могу готовиться к экзаменам и зарабатывать! Получу вознаграждение за правильно решенный тест или творческую задачу!";
    public $childSliderSlide4Img = "child4_1940x1228.jpg";
    public $childSliderSlide5Text = "Я здесь автор и делюсь своими знаниями! Сам создам видео-курс - научу друзей тому, что уже знаю и заработаю деньги!";
    public $childSliderSlide5Img = "child5_1940x1228.jpg";
    public $childSliderSlide6Text = "Я попаду в мир интересных книг! Художественная литература, учебные материалы, научные труды - я найду здесь все по темам и интересам, пройду тесты - получу вознаграждение и признание.";
    public $childSliderSlide6Img = "child6_1940x1228.jpg";
    public $childSliderSlide7Text = "Я буду учиться и получать зарплату на Плоту! За каждый курс, за каждую книгу.";
    public $childSliderSlide7Img = "child7_1940x1228.jpg";
    public $childSliderSlide8Text = "Я буду соревноваться с друзьями, честно и весело! Моя личная страница и достижения всё расскажут за меня сами и станут моим резюме в профессии.";
    public $childSliderSlide8Img = "child8_1940x1228.jpg";
    public $childSliderSlide9Text = "Я буду побеждать, зарабатывать рейтинг и получать медальки!";
    public $childSliderSlide9Img = "child9_1940x1228.jpg";

    #Parents slider
    public $parentSliderSlide1Text = "Мотивируй ребенка и сделай его обучение интересным! Одобри курсы и книги, которые хочет ребенок, выдели карманные деньги и внеси их как мотивационный платеж!";
    public $parentSliderSlide1Img = "parent1_1940x1228.jpg";
    public $parentSliderSlide2Text = "Зарегистрируйся, выбери взрослый аккаунт! Будь внимателен, у аккаунтов разные функции.<br/></br/>Заполни свой профиль, укажи интересы. Добавь своего ребенка в друзья<br>Ребенок сам выберает курсы и книги, которые хотел бы изучить или проверить свои знания. Одобри его выбор или предложи свой. Сделай мотивационный платеж - меню находится справа внутри каждого курса или книги.<br/></br/>Ребенок получит деньги на счет сразу, как только успешно пройдет тест.<br/></br/></br/></br/></br/></br/></br/>";
    public $parentSliderSlide2Img = "blank150x95.jpg";
    public $parentSliderSlide3Text = "Помоги ребенку подготовиться к сложной контрольной или экзаменам.  Вместе выберите будущую профессию от призвания. Личная страница достижений ребенка всё расскажет, просто поддержи его!";
    public $parentSliderSlide3Img = "parent3_1940x1228.jpg";
    public $parentSliderSlide4Text = "Самостоятельно выбери, что будет читать и изучать ребенок, учти его интересы и способности!";
    public $parentSliderSlide4Img = "parent4_1940x1228.jpg";
    public $parentSliderSlide5Text = "Стань автором курсов и обучающих материалов для детей и взрослых, начни получать прибыль за все свои знания и умения - найди живую проблему или вопрос, нарисуй объясняшку или сними видео, выложи на Плот, получай авторский процент с каждой покупки.";
    public $parentSliderSlide5Img = "parent5_1940x1228.jpg";
    public $parentSliderSlide6Text = "Верни 80% денег, потраченных на книги и курсы, в личный кошелек ребенку. Вместе научим ребенка грамотно распоряжаться деньгами.";
    public $parentSliderSlide6Img = "parent6_1940x1228.jpg";
    public $parentSliderSlide7Text = "Общайся с другими родителями и авторами курсов (на интересующие темы), вместе создавай курсы на стыке наук - участвовуй в профессиональных группах, выкладывай общие курсы, отвечай на вопросы.";
    public $parentSliderSlide7Img = "parent7_1940x1228.jpg";

    #Authors slider
    public $authorSliderSlide1Text = "Стань Автором! Создавай тесты и курсы для детей и взрослых, находясь в любой точке мира!";
    public $authorSliderSlide1Img = "avtor1_1940x1228.jpg";
    public $authorSliderSlide2Text = "Получай постоянный доход от подготовленных материалов автоматически.";
    public $authorSliderSlide2Img = "avtor2_1940x1228.jpg";
    public $authorSliderSlide3Text = "Знакомься с другими авторами для общения и совместной работы.";
    public $authorSliderSlide3Img = "avtor3_1940x1228.jpg";
    public $authorSliderSlide4Text = "Стань известным автором! Стань лучшим автором! Стань Звездой!";
    public $authorSliderSlide4Img = "avtor4_1940x1228.jpg";

    #Programs
    public $programMasterLevelId = 4;
    public $programMageLevelId = 5;
    public $programPicture = 'images/com_plot/program_certificate.jpg';
    public $programsLimit=5;
    public $programsBookMaxSymbolsToShow = 450;
    public $programsCourseMaxSymbolsToShow = 200;


#id system user
    public $adminUserId=1444;

    static function getVar($name)
    {
        $class = new plotGlobalConfig();
        return $class->$name;
    }

}
