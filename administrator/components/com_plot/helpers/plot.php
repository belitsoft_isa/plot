<?php

defined('_JEXEC') or die('Restricted access');

abstract class PlotHelperAdmin
{

    public static function addSubmenu($submenu)
    {
        JHtmlSidebar::addEntry(JText::_('COM_PLOT_COURSES_SETTINGS'), 'index.php?option=com_plot&view=courses', $submenu == 'courses');
        JHtmlSidebar::addEntry(JText::_('COM_PLOT_SETTINGS_BOOKS'), 'index.php?option=com_plot&view=books', $submenu == 'books');
        JHtmlSidebar::addEntry(JText::_('COM_PLOT_PROGRAMS'), 'index.php?option=com_plot&view=programs', $submenu == 'programs');
        JHtmlSidebar::addEntry(JText::_('COM_PLOT_PROGRAM_LEVELS'), 'index.php?option=com_plot&view=programlevels', $submenu == 'programlevels');
        JHtmlSidebar::addEntry(JText::_('COM_PLOT_HASHTAGS'), 'index.php?option=com_plot&view=hashtags', $submenu == 'hashtags');
        //JHtmlSidebar::addEntry(JText::_('COM_PLOT_RIVER_TEXT'), 'index.php?option=com_plot&view=river_texts', $submenu == 'river_texts');
        JHtmlSidebar::addEntry(JText::_('COM_PLOT_LEAVELS_LIST'), 'index.php?option=com_plot&view=levels', $submenu == 'levels');
        JHtmlSidebar::addEntry(JText::_('COM_PLOT_AGES_LIST'), 'index.php?option=com_plot&view=ages', $submenu == 'ages');
        JHtmlSidebar::addEntry(JText::_('COM_PLOT_TEXTS'), 'index.php?option=com_plot&view=texts', $submenu == 'texts');



    }

    public static function getActions($Id = 0)
    {
        jimport('joomla.access.access');

        $user = JFactory::getUser();
        $result = new JObject;

        if (empty($Id)) {
            $assetName = 'com_plot';
        } else {
            $assetName = 'com_plot.message.'.(int) $Id;
        };

        $actions = JAccess::getActions('com_plot', 'component');

        foreach ($actions as $action) {
            $result->set($action->name, $user->authorise($action->name, $assetName));
        };

        return $result;
    }

    public static function cutString($string, $length)
    {
        if (mb_strlen($string) > (int) $length) {
            $resultText = mb_substr($string, 0, $length).' ...';
        } else {
            $resultText = $string;
        }

        return $resultText;
    }

}
