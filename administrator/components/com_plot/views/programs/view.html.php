<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewPrograms extends JViewLegacy
{



    function display($tpl = null)
    {
        $this->state = $this->get('State');

        $this->items = $this->get('Items');




        $this->pagination = $this->get('Pagination');



        PlotHelperAdmin::addSubmenu('programs');
        $this->levelOptions = $this->get('SelectOptions');
        JHtmlSidebar::addFilter(
            JText::_('COM_PLOT_SELECT_PROGRAM_LEVEL'),
            'filter_id_level',
            JHtml::_('select.options', $this->levelOptions, 'value', 'text', $this->state->get('filter.level'))
        );


        $this->sidebar = JHtmlSidebar::render();

        $this->addToolBar();
        parent::display($tpl);
        $this->setDocument();
    }

    protected function addToolBar()
    {
        JToolBarHelper::title(JText::_('COM_PLOT_MANAGER').' '.JText::_('COM_PLOT_PROGRAMS_LIST'), 'plot');

        JToolbarHelper::editList('program.edit');
    }

    protected function setDocument()
    {
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('Plot Manager - Administrator'));
    }

}
