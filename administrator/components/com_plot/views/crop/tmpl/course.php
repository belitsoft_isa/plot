<?php
defined('_JEXEC') or die;

JHTML::_('behavior.modal');
jimport('joomla.html.html.bootstrap');

$proportion=plotGlobalConfig::getVar('courseCropWidthMin')/plotGlobalConfig::getVar('courseCropHeightMin');
?>

<link type="text/css" href="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate().'/css/style.css'; ?>" rel="stylesheet">
<link type="text/css" href="<?php echo JUri::root(); ?>components/com_plot/libraries/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet">

<script src="<?php echo JUri::root(); ?>media/jui/js/jquery.min.js" type="text/javascript"></script>

<script src="<?php echo JUri::root(); ?>components/com_plot/libraries/jcrop/js/jquery.Jcrop.min.js" type="text/javascript"></script>
<?php if((int)plotGlobalConfig::getVar('courseCropWidthMin')<=(int)$this->img_width && (int)plotGlobalConfig::getVar('courseCropHeightMin')<=$this->img_height){ ?>
<script type="text/javascript">

    function showCoords(coords)
    {
        jQuery('input[name=x]').val(coords.x);
        jQuery('input[name=y]').val(coords.y);
        jQuery('input[name=x2]').val(coords.x2);
        jQuery('input[name=y2]').val(coords.y2);
        jQuery('input[name=w]').val(coords.w);
        jQuery('input[name=h]').val(coords.h);
    };


    jQuery(document).ready(function(){
        var this_img_w=jQuery('.jcrop').width(),
            this_img_h=jQuery('.jcrop').height(),
            img_w=<?php echo $this->img_width; ?>,
            img_h=<?php echo $this->img_height; ?>,
            coefficient_w=img_w/this_img_w,
            coefficient_h=img_h/this_img_h;
        if(coefficient_w<0 || coefficient_w==0){
            coefficient_w=1
        }
        if(coefficient_h<0 || coefficient_h==0){
            coefficient_h=1
        }

        jQuery('#img-coefficient-w').val(coefficient_w);
        jQuery('#img-coefficient-h').val(coefficient_h);

        jQuery('.jcrop').Jcrop({
            minSize: [ (<?php echo plotGlobalConfig::getVar('courseCropWidthMin'); ?>)/coefficient_w, (<?php echo plotGlobalConfig::getVar('courseCropHeightMin'); ?>)/coefficient_h ],
            setSelect:   [ 0, 0, <?php echo plotGlobalConfig::getVar('courseCropWidthMin'); ?>, <?php echo plotGlobalConfig::getVar('courseCropHeightMin'); ?> ],
            allowSelect: true,
            onChange: showCoords,
            onSelect: showCoords,
            aspectRatio: <?php echo $proportion; ?>
        });

        jQuery('input[name=img-width]').val(jQuery('.jcrop-holder img').width());
        jQuery('input[name=img-height]').val(jQuery('.jcrop-holder img').height());

        jQuery('body').addClass('popup-style');
    });
</script>

<style type="text/css">
    #avatar-crop {
        background-color: #fff;
        border-radius: 13px;
        margin: 1px;
        padding: 10px 20px;
    }
    #avatar-crop img{
        max-width: 400px;
        max-height: 400px;
    }
    #avatar-crop form {
        margin-top: 10px;
    }
    #avatar-crop form input {
        margin-right: 10px;
    }
</style>

<div id="about-me-container" class="wrap">
    <p class="image-preview">Создай превью фотографии с помощью рамки:</p>
    <div id="tabs">
        <div id="avatar-crop">
            <img class="jcrop" src="<?php echo $this->originalPhotoUrl;?>" />

            <form method="POST" id="crop-image" action="<?php echo JRoute::_('index.php?option=com_plot&task=resolution.ajaxSaveCroppedCourse');?>" >
                <input type="submit" class="ui-selectmenu-button" value="Сохранить">
                <input class="ui-selectmenu-button" type="button" value="Отмена" onclick="window.parent.SqueezeBox.close();" />

                <input type="hidden" name="x" value="" id="imgx" />
                <input type="hidden" name="y" value="" id="imgy"/>
                <input type="hidden" name="x2" value="" id="imgx2"/>
                <input type="hidden" name="y2" value="" id="imgy2"/>
                <input type="hidden" name="w" value="" id="imgw"/>
                <input type="hidden" name="h" value="" id="imgh"/>
                <input type="hidden" name="img_src" value="<?php echo $this->originalPhotoUrl;?>">
                <input type="hidden" name="img-width" value="" />
                <input type="hidden" name="img-height" value="" />
                <input type="hidden" name="img-coefficient-w" id="img-coefficient-w" value="" />
                <input type="hidden" name="img-coefficient-h" id="img-coefficient-h" value="" />

            </form>
        </div>
    </div>
</div>
<?php }else{ ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            var sel=window.parent.document.getElementById('jform_image').value='';
        });
    </script>
    <div>Минимальный размер картинки должен быть <?php echo plotGlobalConfig::getVar('courseCropWidthMin');?>x<?php echo plotGlobalConfig::getVar('courseCropHeightMin'); ?></div>
<?php } ?>