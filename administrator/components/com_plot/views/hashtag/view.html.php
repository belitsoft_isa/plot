<?php defined('_JEXEC') or die('Restricted Access');
/*
* HTML5FlippingBook Component
* @package HTML5FlippingBook
* @author JoomPlace Team
* @copyright Copyright (C) JoomPlace, www.joomplace.com
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

//require_once(JPATH_COMPONENT_ADMINISTRATOR.'/libs/HtmlHelper.php');

class PlotViewHashtag extends JViewLegacy
{
	protected $state;
	protected $item;
	protected $form;
	//----------------------------------------------------------------------------------------------------
	public function display($tpl = null) 
	{

		$this->state = $this->get('State');
		$this->item = $this->get('Item');
		$this->form = $this->get('Form');

        $document = JFactory::getDocument();
        $document->addScript(JURI::root().'administrator/components/com_plot/assets/js/BootstrapFormHelper.js');
        $document->addScript(JURI::root().'administrator/components/com_plot/assets/js/BootstrapFormValidator.js');
		$this->addToolbar();
		parent::display($tpl);
	}

	protected function addToolbar() {
        JToolBarHelper::title(JText::_('COM_PLOT_MANAGER').' '.JText::_('COM_PLOT_HASHTAG_EDIT'), 'plot');
		JToolBarHelper::apply('hashtag.apply', 'JTOOLBAR_APPLY');
		JToolBarHelper::save('hashtag.save', 'JTOOLBAR_SAVE');
		JToolBarHelper::custom('hashtag.save2new', 'save-new', 'save-new_f2', 'JTOOLBAR_SAVE_AND_NEW', false);
		JToolBarHelper::cancel('hashtag.cancel', 'JTOOLBAR_CANCEL');
	}
}