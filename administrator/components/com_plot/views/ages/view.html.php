<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewAges extends JViewLegacy
{



    function display($tpl = null)
    {
        $this->state = $this->get('State');
        $this->items = $this->get('Items');

        $this->pagination = $this->get('Pagination');

        PlotHelperAdmin::addSubmenu('ages');
        $this->sidebar = JHtmlSidebar::render();

        $this->addToolBar();
        parent::display($tpl);
        $this->setDocument();
    }

    protected function addToolBar()
    {
        JToolBarHelper::title(JText::_('COM_PLOT_MANAGER').' '.JText::_('COM_PLOT_AGES_LIST'), 'plot');

        JToolbarHelper::addNew('age.add');
        JToolbarHelper::editList('age.edit');
        JToolbarHelper::trash('age.delete');
    }

    protected function setDocument()
    {
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('Plot Manager - Administrator'));
    }

}
