<?php
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');


$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$sortFields = array(
    'a.id' => JText::_('JGLOBAL_FIELD_ID_LABEL'),
    'a.key' => JText::_('COM_PLOT_TEXT_KEY'),
    'a.profile' => JText::_('COM_PLOT_TEXT_PROFILE'),
);
$sortedByOrder = ($listOrder == 'a.ordering');


?>

<script type="text/javascript">

    Joomla.orderTable = function () {
        table = document.getElementById('sortTable');
        direction = document.getElementById('directionTable');
        order = table.options[table.selectedIndex].value;

        if (order != '<?php echo $listOrder; ?>') {
            dirn = 'asc';
        }
        else {
            dirn = direction.options[direction.selectedIndex].value;
        }

        Joomla.tableOrdering(order, dirn, '');
    }

</script>


<form method="post" name="adminForm" id="adminForm" action="<?php echo 'index.php?option=com_plot&view=texts'; ?>"
      method="post" autocomplete="off">
    <input type="hidden" name="task" value=""/>
    <input type="hidden" name="boxchecked" value="0"/>
    <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
    <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
    <?php echo JHtml::_('form.token'); ?>
    <?php if (!empty($this->sidebar)) { ?>
        <div id="j-sidebar-container" class="span2">
            <?php echo $this->sidebar; ?>
        </div>
    <?php } ?>

    <div id="j-main-container" class="<?php echo(empty($this->sidebar) ? '' : 'span10'); ?> ">

        <div id="filter-bar" class="btn-toolbar">
            <div class="filter-search btn-group pull-left">
                <label for="filter_search"
                       class="element-invisible"><?php echo JText::_('COM_PLOT_SEARCH_BY_TITLE'); ?></label>
                <input type="text" name="filter_search" id="filter_search"
                       placeholder="<?php echo JText::_('COM_PLOT_SEARCH_BY_KEY'); ?>" value="<?php
                echo $this->escape($this->state->get('filter.search')); ?>"
                       title="<?php echo JText::_('COM_PLOT_SEARCH_BY_TITLE'); ?>"/>
            </div>
            <div class="btn-group pull-left hidden-phone">
                <button class="btn tip hasTooltip" type="submit"
                        title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
                <button class="btn tip hasTooltip" type="button"
                        onclick="document.id('filter_search').value='';this.form.submit();" title="<?php
                echo JText::_('JSEARCH_FILTER_CLEAR'); ?>"><i class="icon-remove"></i></button>
            </div>
            <div class="btn-group pull-right">
            </div>
            <div class="btn-group pull-right">
                <label for="limit"
                       class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
                <?php echo $this->pagination->getLimitBox(); ?>
            </div>
            <div class="btn-group pull-right">
                <label for="directionTable"
                       class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></label>
                <select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
                    <option value=""><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></option>
                    <option
                        value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING'); ?></option>
                    <option
                        value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING'); ?></option>
                </select>
            </div>
            <div class="btn-group pull-right">
                <label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY'); ?></label>
                <select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
                    <option value=""><?php echo JText::_('JGLOBAL_SORT_BY'); ?></option>
                    <?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder); ?>
                </select>
            </div>
        </div>

        <div class="clearfix"></div>

        <table class="table table-striped ">
            <thead>
            <tr>
                <th>
                    <input type="checkbox" name="checkall-toggle" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>"
                           onclick="Joomla.checkAll(this);"/>
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort', 'COM_PLOT_ID', 'a.id', $listDirn, $listOrder); ?>
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort', 'COM_PLOT_TEXT_KEY', 'a.key', $listDirn, $listOrder); ?>
                </th>
                <th>
                    <?php echo JHtml::_('grid.sort', 'COM_PLOT_TEXT_PROFILE', 'a.profile', $listDirn, $listOrder); ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($this->items) {
                foreach ($this->items as $i => $item) {
                    ?>
                    <tr class="row<?php echo $i % 2; ?>">
                        <td>
                            <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                        </td>
                        <td>
                            <?php
                            echo $this->escape($item->id); ?>
                        </td>
                        <td>
                            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=text&layout=edit&id=' . $item->id); ?>">
                                <?php echo $this->escape($item->key); ?></a>

                        </td>
                        <td>
                            <?php
                            if ($item->profile == plotGlobalConfig::getVar('parentProfileId')) {
                                echo JText::_('COM_PLOT_PARENT_PROFILE');
                            } else {
                                echo JText::_('COM_PLOT_CHILD_PROFILE');
                            }

                            ?>
                        </td>

                    </tr>
                <?php
                }
            }
            ?>
            </tbody>
            <tfoot>
            <tr>
                <?php
                if (!$this->items) {
                    $html = array();

                    $html[] = '<td colspan="100%" class="_html5fb_noitems">';


                    $html[] = JText::_('COM_PLOT_TEXTS_NOITEMS') . ' – ' .
                        '<a onclick="javascript:Joomla.submitbutton(\'text.add\')" href="javascript:void(0);">' .
                        JText::sprintf('COM_PLOT_CREATE_NEW_ONE', Jtext::_('COM_PLOT_START_TEXT')) . '</a>';


                    $html[] = '</td>';

                    echo implode('', $html);
                } else {
                    echo '<td colspan="100%" class="_html5fb_pagination">' . $this->pagination->getListFooter() . '</td>';
                }
                ?>
            </tr>
            </tfoot>
        </table>

    </div>
</form>

