<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewTexts extends JViewLegacy
{



    function display($tpl = null)
    {
        $this->state = $this->get('State');
        $this->items = $this->get('Items');

        $this->pagination = $this->get('Pagination');

        PlotHelperAdmin::addSubmenu('texts');
        $this->sidebar = JHtmlSidebar::render();

        $this->addToolBar();
        parent::display($tpl);
        $this->setDocument();
    }

    protected function addToolBar()
    {
        JToolBarHelper::title(JText::_('COM_PLOT_MANAGER').' '.JText::_('COM_PLOT_TEXTS_LIST'), 'plot');

        JToolbarHelper::addNew('text.add');
        JToolbarHelper::editList('text.edit');
        JToolbarHelper::trash('text.delete');
    }

    protected function setDocument()
    {
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('Plot Manager - Administrator'));
    }

}
