<?php
defined('_JEXEC') or die('Restricted access');
?>
<style>
    #sbox-content.sbox-content-ajax{
        overflow: hidden;
    }
    #search-author {
        background-color: #FFF;
        height: 100%;
        font-size: 12px;
        line-height: 16px;
        margin: -10px;
    }
    #search-author .search-users {
        padding: 10px 20px;
        box-sizing: border-box;
        font: inherit;
        font-weight: 400;
        background: -moz-linear-gradient(top, rgba(0, 0, 0, 0.35) 0%, transparent 100%);
        /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(0, 0, 0, 0.35)), color-stop(100%, transparent));
        /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0.35) 0%, transparent 100%);
        /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, rgba(0, 0, 0, 0.35) 0%, transparent 100%);
        /* Opera 11.10+ */
        background: -ms-linear-gradient(top, rgba(0, 0, 0, 0.35) 0%, transparent 100%);
        /* IE10+ */
        background: linear-gradient(to bottom, rgba(0, 0, 0, 0.35) 0%, rgba(0, 0, 0, 0) 100%);
        /* W3C */
        *{
            vertical-align: middle;
            color: #007bb2;
            display: inline-block;
        }
    }
    #search-author .search-value {
        border: 1px solid #ccc;
        border-radius: 5px;
        box-sizing: border-box;
        color: #666;
        font: inherit;
        margin: 10px 15px 10px 0;
        padding: 14px 10px;
        width: 20em;
        display: inline-block;
        background-color: #fff;
    }
    #search-author .search-users-loading {
        display: inline-block;
    }
    #search-author .users-list {
        height: 400px;
        position: relative;
    }
    #search-author .users-list:before, #search-author .users-list:after {
        content: "";
        display: table;
        line-height: 0;
    }
    #search-author .users-list:after {
        clear: both;
    }
    #search-author .users-list {
        zoom: 1;
    }
    #search-author .user-row {
        padding: 10px;
        box-sizing: border-box;
        font: inherit;
        width: 50%;
        display: inline-block;
        float: left;
        overflow: hidden;
        position: relative;
    }
    #search-author .user-row:not(.inactive):hover {
        background-color: rgba(100,188,213,0.5);
        cursor: pointer;
    }
    #search-author .user-row div {
        vertical-align: middle;
        display: inline-block;
    }
    #search-author .user-avatar {
        width: 40px;
        height: 40px;
        margin: 0 1em 0 0;
        border: 1px solid #999;
        border-radius: 10px;
        margin: 3px 10px;
    }
    #search-author .user-avatar img{
        display: block;
        width: 100%;
        border-radius: 10px;
    }
    #search-author .user-name {
        width:17em;
    }
    #search-author .message {
        color: #007bb2;
        font-size: 0.9em;
    }
    #search-author .pagination {
        padding: 5px 20px;
        margin: 10px 0;
        box-sizing: border-box;
        clear: both;
        position: absolute;
        width: 100%;
        bottom: 0;
    }
    #search-author .pagination > div {
        display: inline-block;
        vertical-align: middle;
        padding: 5px 8px;
        color: #666;
        background-color: #fff;
    }
    #search-author .pagination > div:not(:last-child) {
        margin-right: 1em;
    }
    #search-author .pagination > div:hover, #search-author .pagination > div.active {
        -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(102, 102, 102, 0.8);
        -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(102, 102, 102, 0.8);
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(102, 102, 102, 0.8);
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        cursor: pointer;
    }
</style>
<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">
    function addNewUserCourseBuyFor(userId, userName)
    {
        jQuery('#author_name').html(userName);
      jQuery('#jform_author').val(userId);
       jQuery('#remove_author').html('Remove');
        SqueezeBox.close();
    }

    jQuery(document).ready(function(){

        jQuery('#search-author .search-value').keyup(function(){
            jQuery('#search-author .search-users-loading').html('Загрузка...');
            jQuery.ajax({
                type: "POST",
                url: "index.php?option=com_plot&task=searchusersbook.ajaxGetUsersList&c_id=<?php echo  $this->c_id;?>",
                data: {
                    searchValue: jQuery('#search-author .search-value').val(),
                    page: 1

                },
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    jQuery('#search-author .users-list').html(data.html);
                    jQuery('#search-author .search-users-loading').html('');

                }
            });
        });

        jQuery('#search-author .users-list').on('click', '.user-row:not(.inactive)', function(){
            var userId = jQuery(this).attr('userid');
            var userName = jQuery.trim( jQuery(this).find('.user-name').html() );
            addNewUserCourseBuyFor(userId, userName);
        });

        jQuery('#search-author .users-list').on('click', '.pagination .page-item', function(){
            var page = jQuery(this).attr('page');
            var searchValue = jQuery.trim( jQuery('#search-author .search-value').val() );
            jQuery('#search-author .search-users-loading').html('Загрузка...');
            jQuery.ajax({
                type: "POST",
                url: "index.php?option=com_plot&task=searchusersbook.ajaxGetUsersList&c_id=<?php echo  $this->c_id;?>",
                data: {
                    searchValue: searchValue,
                    page: page

                },
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                   jQuery('#search-author .users-list').html(data.html);
                   jQuery('#search-author .search-users-loading').html('');

                }
            });

        });

    });
</script>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="CSS"> ?>

<?php # </editor-fold> ?>

<div id="search-author">

    <div class="search-users">
        <input class="search-value" type="text" placeholder="Введите имя..."/>
        <div class="search-users-loading"></div>
    </div>

    <div class="users-list">
        <?php require 'users.list.php'; ?>
    </div>

</div>
