<?php
defined('_JEXEC') or die('Restricted access');

?>

<?php foreach ($this->users AS $user) { ?>
<div class="user-row <?php echo in_array($this->c_id, plotUser::factory($user->id)->getBooksIdsBoughtForMe()) ? 'inactive' : ''; ?>" userid="<?php echo $user->id; ?>">
    <div class="user-avatar">
        <img src="<?php echo plotUser::factory($user->id)->getSquareAvatarUrl(); ?>" />
    </div>
    <div class="user-name">
        <?php echo $user->name; ?>
    </div>

</div>
<?php } ?>

<div class="pagination">
    <?php for ( $i = $this->pagination->pagesStart; $i <= $this->pagination->pagesStop; $i++ ) { ?>
    <div page="<?php echo $i; ?>" class="page-item <?php echo $this->pagination->pagesCurrent == $i ? 'active' : ''; ?>">
        <?php echo $i; ?>
    </div>
    <?php } ?>
</div>