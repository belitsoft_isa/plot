<?php
defined('_JEXEC') or die('Restricted access');

class PlotViewSearchUsersBook extends JViewLegacy
{
    
    function display($tpl = null)
    {
        $model = JModelLegacy::getInstance('searchUsers', 'plotModel');
        $this->c_id = JRequest::getInt('c_id', '0');
        $this->users = $model->getUsers(array(), '`u`.`name` ASC', $limit = array('limitstart' => '0', 'limit' => '10'));
        $this->totalUser = $model->totalUsers;
        $this->pagination = new JPagination( $this->totalUser, 0, plotGlobalConfig::getVar('usersCountSearchForBuyBook') );

        parent::display();
        die;
    }

}
