<?php defined('_JEXEC') or die('Restricted Access');


class PlotViewProgram extends JViewLegacy
{
	protected $state;
	protected $item;
	protected $form;
	//----------------------------------------------------------------------------------------------------
	public function display($tpl = null) 
	{

		$this->state = $this->get('State');
		$this->item = $this->get('Item');
        $jinput = JFactory::getApplication()->input;
        $this->id = $jinput->get('id', 0, 'INT');

		$this->form = $this->get('Form');

        $this->selected_ages=$this->get('SelectedAges');
        $this->ages=$this->get('Ages', $this->selected_ages);
        $this->selected_books=$this->get('SelectedBooks');
        $this->books=$this->get('Books', $this->selected_books);
        $this->selected_courses=$this->get('SelectedCourses');
        $this->courses=$this->get('Courses', $this->selected_courses);
        $this->selected_programs=$this->get('SelectedPrograms');

        $this->programs=$this->get('Programs', $this->selected_programs);

        $document = JFactory::getDocument();
        $document->addScript(JURI::root().'administrator/components/com_plot/assets/js/BootstrapFormHelper.js');
        $document->addScript(JURI::root().'administrator/components/com_plot/assets/js/BootstrapFormValidator.js');
		$this->addToolbar();
		parent::display($tpl);
	}

	protected function addToolbar() {
        JToolBarHelper::title(JText::_('COM_PLOT_MANAGER').' '.JText::_('COM_PLOT_PROGRAM_EDIT'), 'plot');
		JToolBarHelper::apply('program.apply', 'JTOOLBAR_APPLY');
		JToolBarHelper::save('program.save', 'JTOOLBAR_SAVE');
		JToolBarHelper::cancel('program.cancel', 'JTOOLBAR_CANCEL');
	}
}