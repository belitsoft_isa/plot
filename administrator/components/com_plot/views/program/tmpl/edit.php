<?php defined('_JEXEC') or die('Restricted Access');

JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.modal');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.tooltip');

?>

<script type="text/javascript">

    jQuery(document).ready(function () {
        window.App = window.App || {};

        App.Valid = {};


        jQuery("#remove_author").on('click', function(){
            jQuery("#jform_author").val('');
            jQuery('#author_name').html('');
            jQuery('#remove_author').html('');
        })

    });
    Joomla.submitbutton = function (task) {

        if (task == 'program.cancel' || document.formvalidator.isValid(document.id('adminForm'))) {


            Joomla.submitform(task, document.getElementById('adminForm'));


        }
        else {
            return false;
        }
    }


</script>


<form name="adminForm" id="adminForm" action="index.php" method="post" autocomplete="off" enctype='multipart/form-data' class="form-validate">
    <input type="hidden" name="option" value="com_plot"/>
    <input type="hidden" name="view" value="program"/>
    <input type="hidden" name="layout" value="edit"/>
    <input type="hidden" name="task" value=""/>
    <input type="hidden" value="<?php echo $this->id; ?>" name="jform[id]" >
    <?php echo JHtml::_('form.token'); ?>


    <table class="table table-striped">
        <tbody>
       <tr>
           <td> <?php echo $this->form->getLabel('title'); ?></td>
           <td colspan="2"><?php echo $this->form->getInput('title'); ?></td>
       </tr>
       <tr>
           <td> <?php echo $this->form->getLabel('level'); ?></td>
           <td colspan="2"><?php echo $this->form->getInput('level'); ?></td>
       </tr>
       <tr>
           <td> <?php echo $this->form->getLabel('link'); ?></td>
           <td colspan="2"><?php echo $this->form->getInput('link'); ?></td>
       </tr>
       <tr>
           <td colspan="2"><?php echo $this->form->renderField('programslist', null, $this->programs); ?></td>
       </tr>
       <tr>
           <td colspan="2"><?php echo $this->form->renderField('ages', null, $this->ages); ?></td>
       </tr>
       <tr>
           <td colspan="2"><?php echo $this->form->renderField('books', null, $this->books); ?></td>
       </tr>
       <tr>
           <td colspan="2"><?php echo $this->form->renderField('courses', null, $this->courses); ?></td>
       </tr>
        </tbody>
    </table>
</form>



