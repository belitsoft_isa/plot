<?php defined('_JEXEC') or die('Restricted Access');

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.formvalidation');
?>

<script type="text/javascript">

    jQuery(document).ready(function() {

        window.App = window.App || {};

        App.Valid = {};


    });

    Joomla.submitbutton = function (task) {

        if (task == 'text.cancel' || (document.formvalidator.isValid(document.id('adminForm')))) {
            Joomla.submitform(task, document.getElementById('adminForm'));
        }
        else {
            return false;
        }
    }

</script>


<form name="adminForm" id="adminForm" action="index.php" method="post" autocomplete="off"  class="form-validate">
    <input type="hidden" name="option" value="com_plot"/>
    <input type="hidden" name="view" value="text"/>
    <input type="hidden" name="layout" value="edit"/>
    <input type="hidden" name="task" value=""/>
    <input type="hidden" name="id" value="<?php echo $this->item->id; ?>"/>
    <?php echo JHtml::_('form.token'); ?>

    <?php echo $this->form->getInput('id'); ?>


    <table class="table table-striped">
        <tbody>
        <tr>
            <td><?php echo $this->form->getLabel('key'); ?></td>
            <td><?php echo $this->form->getInput('key'); ?></td>
        </tr>
        <tr>
            <td><?php echo $this->form->getLabel('profile'); ?></td>
            <td><?php echo $this->form->getInput('profile'); ?></td>
        </tr>
        <tr>
            <td><?php echo $this->form->getLabel('description'); ?></td>
            <td><?php echo $this->form->getInput('description'); ?></td>
        </tr>
        </tbody>
    </table>



</form>