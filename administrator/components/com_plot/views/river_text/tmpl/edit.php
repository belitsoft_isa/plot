<?php
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.modal');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.tooltip');
?>

<script type="text/javascript">
    Joomla.submitbutton = function(task, type) {
        if (task === 'river_text.cancel' || document.formvalidator.isValid(document.id('river-text-form'))) {
            Joomla.submitform(task, document.id('river-text-form'));
        }
    };
</script>

<div id="system-message-container"></div>

<form class="form-validate" id="river-text-form" name="adminForm" method="post" action="#">

    <div class="form-horizontal">

        <div id="myTabContent" class="tab-content">
            <div class="row-fluid">

                <div class="span9">
                    <?php echo $this->form->getControlGroup('title', null, $this->river_text->title); ?>
                    <?php echo $this->form->getControlGroup('text', null, $this->river_text->text); ?>
                </div>

                <div class="span3">
                    <fieldset class="form-vertical">
                        <?php echo $this->form->getControlGroup('published', null, $this->river_text->published); ?>
                    </fieldset>
                </div>

            </div>
        </div>

    </div>

    <input type="hidden" value="" name="task">
    <input type="hidden" value="<?php echo $this->river_text->id; ?>" name="jform[id]" >

    <?php echo JHtml::_('form.token'); ?>

</form>

