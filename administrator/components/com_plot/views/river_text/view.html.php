<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewRiver_text extends JViewLegacy
{

    function display($tpl = null)
    {
        $this->form = $this->get('Form');

        $id = JRequest::getInt('id', 0);
        $this->river_text = new riverText($id);

        $this->addToolBar();
        parent::display($tpl);
        $this->setDocument();
    }

    protected function addToolBar()
    {
        $input = JFactory::getApplication()->input;
        $input->set('hidemainmenu', true);

        if ($this->river_text->id) {
            JToolBarHelper::title(JText::_('COM_PLOT_MANAGER_EDIT_RIVER_TEXT'), 'plot');
        } else {
            JToolBarHelper::title(JText::_('COM_PLOT_MANAGER_NEW_RIVER_TEXT'), 'plot');
        }

        JToolbarHelper::apply('river_text.save');
        JToolbarHelper::save('river_text.saveClose');
        JToolbarHelper::save2new('river_text.saveNew');
        JToolbarHelper::cancel('river_text.cancel');
    }

    protected function setDocument()
    {
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('Plot Manager - Administrator'));
    }

}
