<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewBooks extends JViewLegacy
{



    function display($tpl = null)
    {
        $this->state = $this->get('State');

        $this->items = $this->get('Items');

        $model=$this->getModel();
        $this->items=$model->getAges($this->items);

        $this->pagination = $this->get('Pagination');

        $this->quizOptions = $this->get('SelectOptions');
        JHtmlSidebar::addFilter(
            JText::_('COM_PLOT_SELECT_QUIZ'),
            'filter_id_quiz',
            JHtml::_('select.options', $this->quizOptions, 'value', 'text', $this->state->get('filter.id_quiz'))
        );
        PlotHelperAdmin::addSubmenu('books');
        $this->sidebar = JHtmlSidebar::render();

        $this->addToolBar();
        parent::display($tpl);
        $this->setDocument();
    }

    protected function addToolBar()
    {
        JToolBarHelper::title(JText::_('COM_PLOT_MANAGER').' '.JText::_('COM_PLOT_BOOKS_LIST'), 'plot');

        JToolbarHelper::editList('book.edit');
    }

    protected function setDocument()
    {
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('Plot Manager - Administrator'));
    }

}
