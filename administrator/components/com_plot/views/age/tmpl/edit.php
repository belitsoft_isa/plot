<?php defined('_JEXEC') or die('Restricted Access');

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.formvalidation');
?>

<script type="text/javascript">

    jQuery(document).ready(function() {

        window.App = window.App || {};

        App.Valid = {};

        App.Valid.FormValid = function() {
            var error=true,
                from = jQuery('#jform_from').val(),
                to = jQuery('#jform_to').val(),
                id=<?php echo (int)$this->item->id; ?>;
            if(from<0 || to<0){
                error=false;
            }
            if(from>to){
                error=false;
            }
            jQuery.post('index.php?option=com_plot&task=age.checkAge', {from: from, to:to, id:id}, function (data) {
                var res=jQuery.parseJSON(data);
                if(res.res==false){
                    error=false;
                    alert('dublicate');
                }


            });
            return error;
        }

    });

    Joomla.submitbutton = function (task) {


        if (task == 'age.cancel' || (document.formvalidator.isValid(document.id('adminForm')) && App.Valid.FormValid())) {

           Joomla.submitform(task, document.getElementById('adminForm'));
        }
        else {
            return false;
        }
    }

</script>


<form name="adminForm" id="adminForm" action="index.php" method="post" autocomplete="off"  class="form-validate">
    <input type="hidden" name="option" value="com_plot"/>
    <input type="hidden" name="view" value="age"/>
    <input type="hidden" name="layout" value="edit"/>
    <input type="hidden" name="task" value=""/>
    <input type="hidden" name="id" value="<?php echo $this->item->id; ?>"/>
    <?php echo JHtml::_('form.token'); ?>

    <?php echo $this->form->getInput('id'); ?>


    <table class="table table-striped">
        <tbody>
        <tr>
            <td><?php echo $this->form->getLabel('title'); ?></td>
            <td><?php echo $this->form->getInput('title'); ?></td>
        </tr>
        <tr>
            <td><?php echo $this->form->getLabel('from'); ?></td>
            <td><?php echo $this->form->getInput('from'); ?></td>
        </tr>
        <tr>
            <td><?php echo $this->form->getLabel('to'); ?></td>
            <td><?php echo $this->form->getInput('to'); ?></td>
        </tr>
        </tbody>
    </table>



</form>