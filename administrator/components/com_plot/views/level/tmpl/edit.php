<?php defined('_JEXEC') or die('Restricted Access');

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.formvalidation');
?>

<script type="text/javascript">

    jQuery(document).ready(function () {
        jQuery('#viewTabs a:first').tab('show');
    });

    function deleteImg(path, typeimg){

        jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_plot&task=level.deleteImg",
            data: {
                'path': path,
                'typeimg':typeimg

            },
            success: function (response) {
                if(response){
                    jQuery('#tab_'+typeimg).html(' <input type="file" name="'+typeimg+'">');
                }else{
                    alert('error');
                }

            }
        });
    }

    Joomla.submitbutton = function (task) {


        if (task == 'level.cancel' || document.formvalidator.isValid(document.id('adminForm'))) {

            Joomla.submitform(task, document.getElementById('adminForm'));
        }
        else {
            return false;
        }
    }

</script>
<ul class="nav nav-tabs" id="viewTabs">
    <li><a href="#tab_general" data-toggle="tab"><?php echo  JText::_('COM_PLOT_LEVEL_GENERAL'); ?></a></li>
    <li><a href="#tab_beaver" data-toggle="tab"><?php echo  JText::_('COM_PLOT_BEAVER_TAB'); ?></a></li>
    <li><a href="#tab_boat" data-toggle="tab"><?php echo  JText::_('COM_PLOT_BOAT_TAB'); ?></a></li>
    <li><a href="#tab_house" data-toggle="tab"><?php echo  JText::_('COM_PLOT_HOUSE_TAB'); ?></a></li>
</ul>
<form name="adminForm" id="adminForm" action="index.php" method="post" enctype="multipart/form-data" class="form-validate">
    <input type="hidden" name="option" value="com_plot"/>
    <input type="hidden" name="view" value="level"/>
    <input type="hidden" name="layout" value="edit"/>
    <input type="hidden" name="task" value=""/>
    <input type="hidden" name="id" value="<?php echo $this->item->id; ?>"/>
    <?php echo JHtml::_('form.token'); ?>
<div class="tab-content">

    <div class="tab-pane" id="tab_general">

            <?php

            echo $this->form->getLabel('id'); ?>
            <table class="table table-striped">
                <tbody>
                <tr>
                    <td><?php echo $this->form->getLabel('title'); ?></td>
                    <td><?php echo $this->form->getInput('title'); ?></td>
                </tr>
                <tr>
                    <td><?php echo $this->form->getLabel('points'); ?></td>
                    <td><?php echo $this->form->getInput('points'); ?></td>
                </tr>
                </tbody>
            </table>



    </div>
    <div class="tab-pane" id="tab_beaver">
        <?php if($this->form->getValue('beaver')==''){ ?>
            <input type="file" name="beaver">
        <?php }else{ ?>
            <img src="<?php echo JURI::root() . 'images/com_plot/beaver/'.$this->form->getValue('beaver'); ?>" > <a href="javascript:void(0)" onclick="deleteImg('<?php echo $this->form->getValue('beaver'); ?>','beaver');"><?php echo  JText::_('COM_PLOT_DELETE_IMG'); ?></a>
      <?php  }
        ?>
    </div>
    <div class="tab-pane" id="tab_boat">
        <?php if($this->form->getValue('boat')==''){ ?>
            <input type="file" name="boat">
        <?php }else{ ?>
            <img src="<?php echo JURI::root() . 'images/com_plot/boat/'.$this->form->getValue('boat'); ?>" > <a href="javascript:void(0)" onclick="deleteImg('<?php echo $this->form->getValue('boat'); ?>','boat');"><?php echo  JText::_('COM_PLOT_DELETE_IMG'); ?></a>
       <?php }
        ?>

    </div>
    <div class="tab-pane" id="tab_house">
        <?php if($this->form->getValue('house')==''){ ?>
            <input type="file" name="house">
          <?php }else{ ?>
            <img src="<?php echo JURI::root() . 'images/com_plot/house/'.$this->form->getValue('house'); ?>" > <a href="javascript:void(0)" onclick="deleteImg('<?php echo $this->form->getValue('house'); ?>', 'house');"><?php echo  JText::_('COM_PLOT_DELETE_IMG'); ?></a>
        <?php
        }
        ?>


    </div>

</div>
</form>