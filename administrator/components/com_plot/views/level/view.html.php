<?php defined('_JEXEC') or die('Restricted Access');

//require_once(JPATH_COMPONENT_ADMINISTRATOR.'/libs/HtmlHelper.php');

class PlotViewLevel extends JViewLegacy
{
	protected $state;
	protected $item;
	protected $form;
	//----------------------------------------------------------------------------------------------------
	public function display($tpl = null) 
	{

		$this->state = $this->get('State');
		$this->item = $this->get('Item');
		$this->form = $this->get('Form');

        $document = JFactory::getDocument();
        $document->addScript(JURI::root().'administrator/components/com_plot/assets/js/BootstrapFormHelper.js');
        $document->addScript(JURI::root().'administrator/components/com_plot/assets/js/BootstrapFormValidator.js');
		$this->addToolbar();
		parent::display($tpl);
	}

	protected function addToolbar() {
        JToolBarHelper::title(JText::_('COM_PLOT_MANAGER').' '.JText::_('COM_PLOT_AGE_EDIT'), 'plot');
		JToolBarHelper::apply('level.apply', 'JTOOLBAR_APPLY');
		JToolBarHelper::save('level.save', 'JTOOLBAR_SAVE');
		JToolBarHelper::custom('level.save2new', 'save-new', 'save-new_f2', 'JTOOLBAR_SAVE_AND_NEW', false);
		JToolBarHelper::cancel('level.cancel', 'JTOOLBAR_CANCEL');
	}
}