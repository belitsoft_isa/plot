<?php
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

?>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.js-stools-column-order').click(function() {
            var direction = jQuery(this).attr('data-direction') === "ASC" ? "asc" : "desc";
            Joomla.tableOrdering('rt.course_name', direction, '');
            return false;
        });
    });
</script>

<form method="post" name="adminForm" id="adminForm">

    <?php if (!empty($this->sidebar)) { ?>
        <div id="j-sidebar-container" class="span2">
            <?php echo $this->sidebar; ?>
        </div>
    <?php } ?>

    <div id="j-main-container" class="<?php if (!empty($this->sidebar)) echo 'span10'; ?>">
        <div id="filter-bar" class="btn-toolbar">
            <div class="filter-search btn-group pull-left">
                <label for="filter_search"
                       class="element-invisible"><?php echo JText::_('COM_PLOT_SEARCH_BY_TITLE'); ?></label>
                <input type="text" name="filter_search" id="filter_search"
                       placeholder="<?php echo JText::_('COM_PLOT_SEARCH_BY_TITLE'); ?>" value="<?php
                echo $this->escape($this->state->get('filter.search')); ?>"
                       title="<?php echo JText::_('COM_PLOT_SEARCH_BY_TITLE'); ?>"/>
            </div>
            <div class="btn-group pull-left hidden-phone">
                <button class="btn tip hasTooltip" type="submit"
                        title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
                <button class="btn tip hasTooltip" type="button"
                        onclick="document.id('filter_search').value='';this.form.submit();" title="<?php
                echo JText::_('JSEARCH_FILTER_CLEAR'); ?>"><i class="icon-remove"></i></button>
            </div>
            </div>
        <div class="js-stools clearfix">
            <div class="clearfix">
                <div style="float: right;">
                    <?php echo $this->pagination->getLimitBox(); ?>
                </div>
            </div>
        </div>        

        <table class="table table-striped" id="riverTextsList">
            <thead>
                <tr>
                    <th width="1%" class="hidden-phone center">
                        <?php echo JHtml::_('grid.checkall'); ?>
                    </th>
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'COM_PLOT_COURSE_NAME', 'rt.course_name', $this->listDirection, $this->listOrder); ?>
                    </th>
                    <th width="20%">
                        <?php echo JHtml::_('grid.sort', 'COM_PLOT_DESCRIPTION', 'rt.course_description', $this->listDirection, $this->listOrder); ?>
                    </th>         
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'COM_PLOT_ADMIN_MIN_COST', 'pc.admin_min_cost', $this->listDirection, $this->listOrder); ?>
                    </th>         
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'JGLOBAL_FIELD_ADMIN_COST_LABEL', 'pc.admin_cost', $this->listDirection, $this->listOrder); ?>
                    </th>         
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'COM_PLOT_ADMIN_MAX_COST', 'pc.admin_max_cost', $this->listDirection, $this->listOrder); ?>
                    </th>         
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'COM_PLOT_AUTHOR_MIN_COST', 'pc.author_min_cost', $this->listDirection, $this->listOrder); ?>
                    </th>         
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'JGLOBAL_FIELD_AUTHOR_COST_LABEL', 'pc.author_cost', $this->listDirection, $this->listOrder); ?>
                    </th>         
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'COM_PLOT_AUTHOR_MAX_COST', 'pc.author_max_cost', $this->listDirection, $this->listOrder); ?>
                    </th>         
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'JGLOBAL_FIELD_FIR_PRICE_LABEL', 'pc.count_points', $this->listDirection, $this->listOrder); ?>
                    </th>         
                    <th width="1%">
                        <?php echo JHtml::_('grid.sort', 'COM_PLOT_ID', 'rt.id', $this->listDirection, $this->listOrder); ?>
                    </th>         
                </tr>
            </thead>
            <tbody class="ui-sortable">
                <?php foreach ($this->items as $i => $item) { ?>
                    <tr class="row<?php echo $i % 2; ?>" sortable-group-id="<?php echo 2;#.$item->id;    ?>">
                        <td class="center hidden-phone">
                            <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                        </td>
                        <td class="small hidden-phone">
                            <?php echo '<a href="'.JRoute::_('index.php?option=com_plot&view=course&layout=edit&id='.$item->id).'">'.$item->course_name.'</a>'; ?>
                        </td>
                        <td class="small hidden-phone">
                            <?php echo PlotHelperAdmin::cutString($item->course_description, 200); ?>
                        </td>
                        <td class="small hidden-phone">
                            <?php echo $item->admin_min_cost; ?>
                        </td>
                        <td class="small hidden-phone">
                            <?php echo $item->admin_cost; ?>
                        </td>
                        <td class="small hidden-phone">
                            <?php echo $item->admin_max_cost; ?>
                        </td>
                        <td class="small hidden-phone">
                            <?php echo $item->author_cost; ?>
                        </td>
                        <td class="small hidden-phone">
                            <?php echo $item->author_max_cost; ?>
                        </td>
                        <td class="small hidden-phone">
                            <?php echo $item->min_cost; ?>
                        </td>
                        <td class="small hidden-phone">
                            <?php echo $item->count_points; ?>
                        </td>
                        <td class="small hidden-phone">
                            <?php echo $item->id; ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>

        </table>

        <?php echo $this->pagination->getListFooter(); ?>

    </div>

    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="filter_order" value="<?php echo $this->listOrder; ?>" />
    <input type="hidden" name="filter_order_Dir" value="<?php echo $this->listDirection; ?>" />    
    <?php echo JHtml::_('form.token'); ?>

</form>    
