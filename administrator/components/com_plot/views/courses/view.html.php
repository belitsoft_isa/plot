<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewCourses extends JViewLegacy
{

    const DEFAUL_ITEMS_PER_PAGE = 20;
    const DEFAUL_LIMITSTART = 0;
    const DEFAUL_ORDERING = 'rt.ordering';

    function display($tpl = null)
    {
        $app = JFactory::getApplication();
        $coursesModel = $this->getModel();
        $this->state = $this->get('State');

        $this->listOrder = JRequest::getString('filter_order', $app->getUserState('courses_filter_order', self::DEFAUL_ORDERING));
        $this->listDirection = JRequest::getString('filter_order_Dir', $app->getUserState('courses_filter_order_Dir', 'ASC'));
        $app->setUserState('courses_filter_order', $this->listOrder);
        $app->setUserState('courses_filter_order_Dir', $this->listDirection);

        $this->isOrderByOrdering = $this->listOrder == self::DEFAUL_ORDERING;

        $coursesModel->listOrder = $this->listOrder;
        $coursesModel->listDirection = $this->listDirection;

        if (JRequest::getInt('limit', '') && JRequest::getInt('limit') != $app->getUserState('courses_limit', '')) {
            $coursesModel->limitstart = self::DEFAUL_LIMITSTART;
            $app->setUserState('courses_limitstart', $coursesModel->limitstart);
        } else {
            $coursesModel->limitstart = JRequest::getInt('limitstart', $app->getUserState('courses_limitstart', self::DEFAUL_LIMITSTART));
            $app->setUserState('courses_limitstart', $coursesModel->limitstart);
        }

        $coursesModel->limit = JRequest::getInt('limit', $app->getUserState('courses_limit', self::DEFAUL_ITEMS_PER_PAGE));
        $app->setUserState('courses_limit', $coursesModel->limit);

        $this->items = $coursesModel->getItems();

        $this->pagination = $coursesModel->getCoursesPagination();

        PlotHelperAdmin::addSubmenu('courses');
        $this->sidebar = JHtmlSidebar::render();

        $this->addToolBar();
        parent::display($tpl);
        $this->setDocument();
    }

    protected function addToolBar()
    {
        JToolBarHelper::title('Plot Manager: '.JText::_('COM_PLOT_COURSES_SETTINGS'), 'plot');

        JToolbarHelper::editList('courses.edit');
    }

    protected function setDocument()
    {
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('Plot Manager - Administrator'));
    }

}
