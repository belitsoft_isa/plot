<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewplot extends JViewLegacy
{

    function display($tpl = null)
    {
        PlotHelperAdmin::addSubmenu('plot');
        $this->sidebar = JHtmlSidebar::render();
        
        $this->addToolBar();
        parent::display($tpl);
        $this->setDocument();
    }

    protected function addToolBar()
    {
        JToolBarHelper::title(JText::_('Plot Manager'), 'plot');
    }

    protected function setDocument()
    {
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('Plot Manager - Administrator'));
    }

}
