<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewCourse extends JViewLegacy
{

    function display($tpl = null)
    {
        $this->form = $this->get('Form');
        
        $id = JRequest::getInt('id', 0);
        $this->course = new plotCourse($id);
        $this->selected_hashtags=$this->get('SelectedHashtags');
        $this->hashtags= $this->get('HashTags',$this->selected_hashtags);
        $this->selected_before_hashtags=$this->get('SelectedBeforeHashtags');
        $this->before_hashtags= $this->get('HashTags',$this->selected_before_hashtags);
        $this->selected_after_hashtags=$this->get('SelectedAfterHashtags');
        $this->after_hashtags= $this->get('HashTags',$this->selected_after_hashtags);

        $this->addToolBar();
        parent::display($tpl);
        $this->setDocument();
    }

    protected function addToolBar()
    {
        $input = JFactory::getApplication()->input;
        $input->set('hidemainmenu', true);

        if ($this->course->lmsId) {
            JToolBarHelper::title(JText::_('COM_PLOT_MANAGER_EDIT_COURSE'), 'plot');
        } 

        JToolbarHelper::apply('course.save');
        JToolbarHelper::save('course.saveClose');
        JToolbarHelper::cancel('course.cancel');
    }

    protected function setDocument()
    {
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('Plot Manager - Administrator'));
    }

}
