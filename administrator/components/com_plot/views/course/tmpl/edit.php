<?php
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.modal');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.tooltip');
?>

<script type="text/javascript">

    jQuery(document).ready(function () {
        window.App = window.App || {};

        App.Valid = {};
        App.Valid.Ages = function () {
            var arr = [],
                msg_cont = jQuery('#system-message-container').html(),
                err_text = '';

            jQuery.map(jQuery("#jform_ages_chzn").find(".search-choice span"), function (option) {
                return arr.push(jQuery(option).text());
            });
            if (arr.length) {
                return true;
            } else {
                err_text += '<button type="button" class="close" data-dismiss="alert">×</button>';
                err_text += '<div class="alert alert-error"><h4 class="alert-heading"><?php echo JText::_("COM_PLOT_MESSAGE_ERROR");?></h4>';
                err_text += '<p><?php echo JText::_("COM_PLOT_SELECTED_AGE");?></p></div>';
                jQuery('#system-message-container').html(msg_cont + err_text);
                jQuery('#jform_ages_chzn').addClass('plot_error');
                return false;
            }

        }

        App.Valid.Tags = function () {
            var arr = [],
                msg_cont = jQuery('#system-message-container').html(),
                err_text = '';

            jQuery.map(jQuery("#jform_tags_chzn").find(".search-choice span"), function (option) {
                return arr.push(jQuery(option).text());
            });
            if (arr.length) {
                return true;
            } else {
                err_text += '<button type="button" class="close" data-dismiss="alert">×</button>';
                err_text += '<div class="alert alert-error"><h4 class="alert-heading"><?php echo JText::_("COM_PLOT_MESSAGE_ERROR");?></h4>';
                err_text += '<p><?php echo JText::_("COM_PLOT_SELECTED_AGE");?></p></div>';
                jQuery('#system-message-container').html(msg_cont + err_text);
                jQuery('#jform_tags_chzn').addClass('plot_error');
                return false;
            }

        }

    });
    Joomla.submitbutton = function(task, type) {
        jQuery('#system-message-container').html('');
        jQuery('.plot_error').each(function () {
            jQuery(this).removeClass('plot_error');
        });
        if (task === 'course.cancel' || (document.formvalidator.isValid(document.id('course-form')) && App.Valid.Ages() && App.Valid.Tags())) {
            Joomla.submitform(task, document.id('course-form'));
        }
    };
</script>

<div id="system-message-container"></div>

<form class="form-validate" id="course-form" name="adminForm" method="post" action="#">

    <div class="form-horizontal">

        <div id="myTabContent" class="tab-content">
            <div class="row-fluid">

                <div class="span9">
                    <?php echo $this->form->renderField('title', null, $this->course->course_name); ?>
                    <?php echo $this->form->renderField('admin_min_cost', null, $this->course->admin_min_cost); ?>
                    <?php echo $this->form->renderField('admin_cost', null, $this->course->admin_cost); ?>
                    <?php echo $this->form->renderField('admin_max_cost', null, $this->course->admin_max_cost); ?>
                    <?php echo $this->form->renderField('author_min_cost', null, $this->course->author_min_cost); ?>
                    <?php echo $this->form->renderField('author_cost', null, $this->course->author_cost); ?>
                    <?php echo $this->form->renderField('author_max_cost', null, $this->course->author_max_cost); ?>
                    <?php echo $this->form->renderField('count_points', null, $this->course->count_points); ?>
                    <?php echo $this->form->renderField('ages', null, $this->course->getAges()); ?>
                    <?php echo $this->form->renderField('tags', null, $this->course->getTags()); ?>
                    <?php echo $this->form->renderField('image', null, $this->course->image); ?>
                    <?php echo $this->form->renderField('id_pub', null, $this->course->id_pub); ?>
                    <?php echo $this->form->renderField('hashtags', null, $this->hashtags); ?>
                    <?php echo $this->form->renderField('beforehashtags', null, $this->before_hashtags); ?>
                    <?php echo $this->form->renderField('afterhashtags', null, $this->after_hashtags); ?>
                    <?php echo $this->form->renderField('small', null, $this->course->small); ?>
                </div>

            </div>
        </div>

    </div>

    <input type="hidden" value="" name="task">
    <input type="hidden" value="<?php echo $this->course->lmsId; ?>" name="jform[id]" >

    <?php echo JHtml::_('form.token'); ?>

</form>
