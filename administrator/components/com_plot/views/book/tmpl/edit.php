<?php defined('_JEXEC') or die('Restricted Access');

JHtml::_('behavior.framework');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.modal');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.tooltip');
?>

<script type="text/javascript">

    jQuery(document).ready(function () {
        window.App = window.App || {};

        App.Valid = {};

        App.Valid.FormValid = function () {
            var authormin = parseInt(jQuery('#jform_authormin').val(), 10),
                authorcost = parseInt(jQuery('#jform_authorcost').val(), 10),
                authormax = parseInt(jQuery('#jform_authormax').val(), 10),
                adminmin = parseInt(jQuery('#jform_adminmin').val(), 10),
                admincost = parseInt(jQuery('#jform_admincost').val(), 10),
                adminmax = parseInt(jQuery('#jform_adminmax').val(), 10),
                persent = parseInt(jQuery('#jform_percent').val(), 10),
                sum = jQuery('#jform_sum').val(),
                min_sum = jQuery('#jform_min_sum').val(),
                error = true;
            if (authormax < authormin || adminmax < adminmin) {
                alert('Максимальное значение должно быть больше минимального');
                error = false;
            }
            if (persent > 100) {
                alert(' <?php echo  JText::_("COM_PLOT_CHECK_PERCENT");?>');
                error = false;
            }


            return error;
        }
        App.Valid.Ages = function () {
            var arr = [],
                msg_cont = jQuery('#system-message-container').html(),
                err_text = '';

            jQuery.map(jQuery("#jform_ages_chzn").find(".search-choice span"), function (option) {
                return arr.push(jQuery(option).text());
            });
            if (arr.length) {
                return true;
            } else {
                err_text += '<button type="button" class="close" data-dismiss="alert">×</button>';
                err_text += '<div class="alert alert-error"><h4 class="alert-heading"><?php echo JText::_("COM_PLOT_MESSAGE_ERROR");?></h4>';
                err_text += '<p><?php echo JText::_("COM_PLOT_SELECTED_AGE");?></p></div>';
                jQuery('#system-message-container').html(msg_cont + err_text);
                jQuery('#jform_ages_chzn').addClass('plot_error');
                return false;
            }

        }

        App.Valid.Tags = function () {
            var arr = [],
                msg_cont = jQuery('#system-message-container').html(),
                err_text = '';

            jQuery.map(jQuery("#jform_tags_chzn").find(".search-choice span"), function (option) {
                return arr.push(jQuery(option).text());
            });
            if (arr.length) {
                return true;
            } else {
                err_text += '<button type="button" class="close" data-dismiss="alert">×</button>';
                err_text += '<div class="alert alert-error"><h4 class="alert-heading"><?php echo JText::_("COM_PLOT_MESSAGE_ERROR");?></h4>';
                err_text += '<p><?php echo JText::_("COM_PLOT_SELECTED_AGE");?></p></div>';
                jQuery('#system-message-container').html(msg_cont + err_text);
                jQuery('#jform_tags_chzn').addClass('plot_error');
                return false;
            }

        }

        App.Valid.Quiz = function () {
            var msg_cont = jQuery('#system-message-container').html(),
                err_text = '';

            if (jQuery("#jform_id_quiz_chzn").find(".chzn-single span").text() == '<?php echo  JText::_("COM_PLOT_SELECT_QUIZ"); ?>') {
                err_text += '<button type="button" class="close" data-dismiss="alert">×</button>';
                err_text += '<div class="alert alert-error"><h4 class="alert-heading"><?php echo JText::_("COM_PLOT_MESSAGE_ERROR");?></h4>';
                err_text += '<p><?php echo JText::_("COM_PLOT_NOT_QUIZ_SELECTED");?></p></div>';
                jQuery('#system-message-container').html(msg_cont + err_text);
                jQuery('#jform_id_quiz_chzn').addClass('plot_error');
                return false;
            } else {
                return true;
            }
        }


        jQuery("#remove_author").on('click', function () {
            jQuery("#jform_author").val('');
            jQuery('#author_name').html('');
            jQuery('#remove_author').html('');
        });

    });
    Joomla.submitbutton = function (task) {
        jQuery('#system-message-container').html('');
        jQuery('.plot_error').each(function () {
            jQuery(this).removeClass('plot_error');
        });

        if (task == 'book.cancel' || (document.formvalidator.isValid(document.id('adminForm')) && App.Valid.FormValid() && App.Valid.Ages() && App.Valid.Tags() && App.Valid.Quiz())) {


            Joomla.submitform(task, document.getElementById('adminForm'));


        }
        else {
            return false;
        }
    }


</script>

<script type="text/javascript">
    tinyMCE.init({
        mode: "textareas",
        theme: "simple",
        editor_selector: "mceSimple"
    });

    tinyMCE.init({
        mode: "textareas",
        theme: "advanced",
        editor_selector: "mceAdvanced"
    });
</script>

<form name="adminForm" id="adminForm" action="index.php" method="post" autocomplete="off" enctype='multipart/form-data'
      class="form-validate">
    <input type="hidden" name="option" value="com_plot"/>
    <input type="hidden" name="view" value="age"/>
    <input type="hidden" name="layout" value="edit"/>
    <input type="hidden" name="task" value=""/>

    <?php echo JHtml::_('form.token'); ?>


    <?php echo $this->form->getInput('c_id'); ?>
    <table class="table table-striped">
        <tbody>
        <tr>
            <td> <?php echo $this->form->getLabel('c_title'); ?></td>
            <td colspan="2"><?php echo $this->form->getInput('c_title'); ?></td>
        </tr>
        <tr>
            <td> <?php echo $this->form->getLabel('author_min'); ?></td>
            <td colspan="2"> <?php echo $this->form->getInput('author_min'); ?></td>
        </tr>
        <tr>
            <td> <?php echo $this->form->getLabel('author_cost'); ?></td>
            <td colspan="2"><?php echo $this->form->getInput('author_cost'); ?></td>
        </tr>
        <tr>
            <td><?php echo $this->form->getLabel('author_max'); ?></td>
            <td colspan="2"><?php echo $this->form->getInput('author_max'); ?></td>
        </tr>
        <tr>
            <td><?php echo $this->form->getLabel('admin_min'); ?></td>
            <td colspan="2"><?php echo $this->form->getInput('admin_min'); ?></td>
        </tr>
        <tr>
            <td> <?php echo $this->form->getLabel('admin_cost'); ?></td>
            <td colspan="2"> <?php echo $this->form->getInput('admin_cost'); ?></td>
        </tr>
        <tr>
            <td><?php echo $this->form->getLabel('admin_max'); ?></td>
            <td colspan="2"><?php echo $this->form->getInput('admin_max'); ?></td>
        </tr>
        <tr>
            <td> <?php echo $this->form->getLabel('id_quiz'); ?></td>
            <td colspan="2"> <?php echo $this->form->getInput('id_quiz'); ?></td>
        </tr>
        <tr>
            <td><?php echo $this->form->getLabel('count_points'); ?></td>
            <td colspan="2"><?php echo $this->form->getInput('count_points'); ?></td>
        </tr>
        <tr>
            <td><?php echo $this->form->getLabel('text'); ?></td>
            <td colspan="2"><?php echo $this->form->getInput('text'); ?></td>
        </tr>
        <tr>
            <td> <?php echo $this->form->getLabel('author'); ?></td>
            <td colspan="2"> <?php echo $this->form->getInput('author'); ?></td>
        </tr>
        <tr>
            <td colspan="3"><?php echo $this->form->renderField('tags', null, $this->tags); ?></td>

        </tr>
        <tr>
            <td colspan="3"><?php echo $this->form->renderField('ages', null, $this->ages); ?></td>
        </tr>
        <tr>
            <td> <?php echo $this->form->getLabel('pdffiles'); ?></td>
            <?php if ($this->form->getInput('pdffiles')) {
                ?>
                <td> <?php echo $this->form->getInput('pdffiles'); ?></td>
                <td></td>
            <?php
            } else {
                ?>
                <td colspan="2"> <?php echo $this->form->getInput('pdffiles'); ?></td>
            <?php
            }?>

        </tr>

        <tr>
            <td> <?php echo $this->form->getLabel('mobifiles'); ?></td>
            <?php if ($this->form->getInput('mobifiles')) {
                ?>
                <td> <?php echo $this->form->getInput('mobifiles'); ?></td>
                <td></td>
            <?php
            } else {
                ?>
                <td colspan="2"> <?php echo $this->form->getInput('mobifiles'); ?></td>
            <?php
            }?>

        </tr>
        <tr>
            <td> <?php echo $this->form->getLabel('fb2files'); ?></td>
            <?php if ($this->form->getInput('fb2files')) {
                ?>
                <td> <?php echo $this->form->getInput('fb2files'); ?></td>
                <td></td>
            <?php
            } else {
                ?>
                <td colspan="2"> <?php echo $this->form->getInput('fb2files'); ?></td>
            <?php
            }?>

        </tr>
        <tr>
            <td> <?php echo $this->form->getLabel('epubfiles'); ?></td>
            <?php if ($this->form->getInput('epubfiles')) {
                ?>
                <td> <?php echo $this->form->getInput('epubfiles'); ?></td>
                <td></td>
            <?php
            } else {
                ?>
                <td colspan="2"> <?php echo $this->form->getInput('epubfiles'); ?></td>
            <?php
            }?>

        </tr>
        <tr>
            <td> <?php echo $this->form->getLabel('audiofiles'); ?></td>
            <?php if ($this->form->getInput('audiofiles')) {
                ?>
                <td> <?php echo $this->form->getInput('audiofiles'); ?></td>
                <td></td>
            <?php
            } else {
                ?>
                <td colspan="2"> <?php echo $this->form->getInput('audiofiles'); ?></td>
            <?php
            }?>

        </tr>
        <tr>
            <td> <?php echo $this->form->getLabel('url'); ?></td>
            <td colspan="2"> <?php echo $this->form->getInput('url'); ?></td>
        </tr>
        <!--<tr>
            <td colspan="3"><?php echo $this->form->renderField('hashtags', null, $this->hashtags); ?></td>
        </tr>
        <tr>
            <td colspan="3"><?php echo $this->form->renderField('beforehashtags', null, $this->before_hashtags); ?></td>
        </tr>
        <tr>
            <td colspan="3"><?php echo $this->form->renderField('afterhashtags', null, $this->after_hashtags); ?></td>
        </tr>-->
        </tbody>
    </table>
</form>



