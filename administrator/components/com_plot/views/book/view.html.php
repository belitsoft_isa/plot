<?php defined('_JEXEC') or die('Restricted Access');


class PlotViewBook extends JViewLegacy
{
	protected $state;
	protected $item;
	protected $form;
	//----------------------------------------------------------------------------------------------------
	public function display($tpl = null) 
	{

		$this->state = $this->get('State');
		$this->item = $this->get('Item');


		$this->form = $this->get('Form');

        $this->selected_ages=$this->get('SelectedAges');
        $this->ages=$this->get('Ages', $this->selected_ages);
        $this->selected_tags=$this->get('SelectedTags');
        $this->tags= $this->get('Tags',$this->selected_tags);
        $this->selected_hashtags=$this->get('SelectedHashtags');
        $this->hashtags= $this->get('HashTags',$this->selected_hashtags);
        $this->selected_before_hashtags=$this->get('SelectedBeforeHashtags');
        $this->before_hashtags= $this->get('HashTags',$this->selected_before_hashtags);
        $this->selected_after_hashtags=$this->get('SelectedAfterHashtags');
        $this->after_hashtags= $this->get('HashTags',$this->selected_after_hashtags);
        $this->adminCost=$this->get('AdminCost');
        $document = JFactory::getDocument();
        $document->addScript(JURI::root().'administrator/components/com_plot/assets/js/BootstrapFormHelper.js');
        $document->addScript(JURI::root().'administrator/components/com_plot/assets/js/BootstrapFormValidator.js');
		$this->addToolbar();
		parent::display($tpl);
	}

	protected function addToolbar() {
        JToolBarHelper::title(JText::_('COM_PLOT_MANAGER').' '.JText::_('COM_PLOT_BOOK_EDIT'), 'plot');
		JToolBarHelper::apply('book.apply', 'JTOOLBAR_APPLY');
		JToolBarHelper::save('book.save', 'JTOOLBAR_SAVE');
		JToolBarHelper::cancel('book.cancel', 'JTOOLBAR_CANCEL');
	}
}