<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewRiver_texts extends JViewLegacy
{

    const DEFAUL_ITEMS_PER_PAGE = 20;
    const DEFAUL_LIMITSTART = 0;
    const DEFAUL_ORDERING = 'rt.ordering';

    function display($tpl = null)
    {
        $app = JFactory::getApplication();
        $riverTextsModel = $this->getModel();

        $this->listOrder = JRequest::getString('filter_order', $app->getUserState('river_texts_filter_order', self::DEFAUL_ORDERING));
        $this->listDirection = JRequest::getString('filter_order_Dir', $app->getUserState('river_texts_filter_order_Dir', 'ASC'));
        $app->setUserState('river_texts_filter_order', $this->listOrder);
        $app->setUserState('river_texts_filter_order_Dir', $this->listDirection);

        $this->isOrderByOrdering = $this->listOrder == self::DEFAUL_ORDERING;

        $riverTextsModel->listOrder = $this->listOrder;
        $riverTextsModel->listDirection = $this->listDirection;

        if (JRequest::getInt('limit', '') && JRequest::getInt('limit') != $app->getUserState('river_texts_limit', '')) {
            $riverTextsModel->limitstart = self::DEFAUL_LIMITSTART;
            $app->setUserState('river_texts_limitstart', $riverTextsModel->limitstart);
        } else {
            $riverTextsModel->limitstart = JRequest::getInt('limitstart', $app->getUserState('river_texts_limitstart', self::DEFAUL_LIMITSTART));
            $app->setUserState('river_texts_limitstart', $riverTextsModel->limitstart);
        }

        $riverTextsModel->limit = JRequest::getInt('limit', $app->getUserState('river_texts_limit', self::DEFAUL_ITEMS_PER_PAGE));
        $app->setUserState('river_texts_limit', $riverTextsModel->limit);

        $this->items = $riverTextsModel->getItems();

        $this->pagination = $riverTextsModel->getRiverTextsPagination();

        PlotHelperAdmin::addSubmenu('river_texts');
        $this->sidebar = JHtmlSidebar::render();

        $this->addToolBar();
        parent::display($tpl);
        $this->setDocument();
    }

    protected function addToolBar()
    {
        JToolBarHelper::title(JText::_('Plot Manager: River Texts'), 'plot');

        JToolbarHelper::addNew('river_text.add');
        JToolbarHelper::editList('river_texts.edit');
        JToolbarHelper::publish('river_texts.publish', 'JTOOLBAR_PUBLISH', true);
        JToolbarHelper::unpublish('river_texts.unpublish', 'JTOOLBAR_UNPUBLISH', true);
        JToolbarHelper::trash('river_texts.delete');
    }

    protected function setDocument()
    {
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('Plot Manager - Administrator'));
    }

}
