<?php
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

if ($this->isOrderByOrdering) {
    $saveOrderingUrl = 'index.php?option=com_plot&task=river_texts.ajaxSaveOrder&tmpl=component';
    JHtml::_('sortablelist.sortable', 'riverTextsList', 'adminForm', strtolower($this->listDirection), $saveOrderingUrl);
}
?>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.js-stools-column-order').click(function() {
            var direction = jQuery(this).attr('data-direction') === "ASC" ? "asc" : "desc";
            Joomla.tableOrdering('rt.ordering', direction, '');
            return false;
        });
    });
</script>

<form method="post" name="adminForm" id="adminForm">

    <?php if (!empty($this->sidebar)) { ?>
        <div id="j-sidebar-container" class="span2">
            <?php echo $this->sidebar; ?>
        </div>
    <?php } ?>

    <div id="j-main-container" class="<?php if (!empty($this->sidebar)) echo 'span10'; ?>">

        <div class="js-stools clearfix">
            <div class="clearfix">
                <div style="float: right;">
                    <?php echo $this->pagination->getLimitBox(); ?>
                </div>
            </div>
        </div>        

        <table class="table table-striped" id="riverTextsList">
            <thead>
                <tr>
                    <th width="1%" class="nowrap center hidden-phone">
                        <?php echo JHtml::_('searchtools.sort', '', 'rt.ordering', $this->listDirection, $this->listOrder, null, 'asc', 'JGRID_HEADING_ORDERING', 'icon-menu-2'); ?>
                    </th>
                    <th width="1%" class="hidden-phone center">
                        <?php echo JHtml::_('grid.checkall'); ?>
                    </th>
                    <th width="1%" class="nowrap center">
                        <?php echo JHtml::_('grid.sort', 'JSTATUS', 'rt.published', $this->listDirection, $this->listOrder); ?>
                    </th>
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'JGLOBAL_TITLE', 'rt.title', $this->listDirection, $this->listOrder); ?>
                    </th>
                    <th width="20%">
                        <?php echo JHtml::_('grid.sort', 'COM_PLOT_TEXT', 'rt.text', $this->listDirection, $this->listOrder); ?>
                    </th>         
                    <th width="1%">
                        <?php echo JHtml::_('grid.sort', 'COM_PLOT_ID', 'rt.id', $this->listDirection, $this->listOrder); ?>
                    </th>         
                </tr>
            </thead>
            <tbody class="ui-sortable">
                <?php foreach ($this->items as $i => $item) { ?>
                    <tr class="row<?php echo $i % 2; ?>" sortable-group-id="<?php echo 2;#.$item->id;    ?>">
                        <td class="order nowrap center hidden-phone">
                            <span class="sortable-handler <?php if (!$this->isOrderByOrdering) echo 'inactive hasTooltip'; ?>"
                                  <?php if (!$this->isOrderByOrdering) echo 'title="'.JText::_('COM_PLOT_PLEASE_SORT_BY_ORDER_TO_ENABLE_REORDERING').'"'; ?>>
                                <i class="icon-menu"></i>
                            </span>
                            <input type="text" style="display:none" name="order[]" size="5" value="<?php echo $item->ordering; ?>" class="width-20 text-area-order " />
                        </td>
                        <td class="center hidden-phone">
                            <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                        </td>
                        <td class="center">
                            <?php echo JHtml::_('jgrid.published', $item->published, $i, 'river_texts.', 1, 'cb'); ?>
                        </td>
                        <td class="small hidden-phone">
                            <?php echo '<a href="'.JRoute::_('index.php?option=com_plot&view=river_text&layout=edit&id='.$item->id).'">'.$item->title.'</a>'; ?>
                        </td>
                        <td class="small hidden-phone">
                            <?php echo PlotHelperAdmin::cutString($item->text, 200); ?>
                        </td>
                        <td class="small hidden-phone">
                            <?php echo $item->id; ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>

        </table>

        <?php echo $this->pagination->getListFooter(); ?>

    </div>

    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="filter_order" value="<?php echo $this->listOrder; ?>" />
    <input type="hidden" name="filter_order_Dir" value="<?php echo $this->listDirection; ?>" />    
    <?php echo JHtml::_('form.token'); ?>

</form>    
