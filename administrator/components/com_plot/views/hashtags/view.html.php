<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewHashtags extends JViewLegacy
{



    function display($tpl = null)
    {
        $this->state = $this->get('State');
        $this->items = $this->get('Items');

        $this->pagination = $this->get('Pagination');

        PlotHelperAdmin::addSubmenu('hashtags');
        $this->sidebar = JHtmlSidebar::render();

        $this->addToolBar();
        parent::display($tpl);
        $this->setDocument();
    }

    protected function addToolBar()
    {
        JToolBarHelper::title(JText::_('COM_PLOT_MANAGER').' '.JText::_('COM_PLOT_HASHTAGS_LIST'), 'plot');

        JToolbarHelper::addNew('hashtag.add');
        JToolbarHelper::editList('hashtag.edit');
        JToolbarHelper::trash('hashtag.delete');
    }

    protected function setDocument()
    {
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('Plot Manager - Administrator'));
    }

}
