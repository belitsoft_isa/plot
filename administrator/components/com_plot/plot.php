<?php

defined('_JEXEC') or die('Restricted access');

// Added for Joomla 3.0
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
};

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_plot')) {
    return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
};

// Load cms libraries
JLoader::registerPrefix('J', JPATH_PLATFORM.'/cms');
// Load joomla libraries without overwrite
JLoader::registerPrefix('J', JPATH_PLATFORM.'/joomla', false);

// require helper files
JLoader::register('PlotHelperAdmin', dirname(__FILE__).DS.'helpers'.DS.'plot.php');

// import joomla controller library
jimport('joomla.application.component.controller');

# component requires
require_once JPATH_COMPONENT.'/requires.php';

// Add CSS file for all pages
$document = JFactory::getDocument();
$document->addScript('components/com_plot/assets/js/plot.js');

// Get an instance of the controller prefixed by Plot
$controller = JControllerLegacy::getInstance('Plot');

// Perform the Request task
$controller->execute(JFactory::getApplication()->input->get('task'));

// Redirect if set by the controller
$controller->redirect();
