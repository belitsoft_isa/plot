<?php

/**
 * @package     Joomla.Libraries
 * @subpackage  HTML
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */
defined('JPATH_PLATFORM') or die;

/**
 * Utility class for JavaScript behaviors
 *
 * @package     Joomla.Libraries
 * @subpackage  HTML
 * @since       1.5
 */
class PHtmlBehavior extends JHtmlBehavior
{

    public static function modal($selector = 'a.modal', $params = array())
    {
        $document = JFactory::getDocument();

        // Load the necessary files if they haven't yet been loaded
        if (!isset(static::$loaded[__METHOD__])) {
            // Include MooTools framework
            static::framework(true);
            // Load the JavaScript and css
            PHtml::_('script', JUri::root().'media/com_plot/system/modal.js', true, false);
            PHtml::_('stylesheet', 'system/modal.css', array(), true);
        }

        $sig = md5(serialize(array($selector, $params)));

        if (isset(static::$loaded[__METHOD__][$sig])) {
            return;
        }

        // Setup options object
        $opt['ajaxOptions'] = (isset($params['ajaxOptions']) && (is_array($params['ajaxOptions']))) ? $params['ajaxOptions'] : null;
        $opt['handler'] = (isset($params['handler'])) ? $params['handler'] : null;
        $opt['parseSecure'] = (isset($params['parseSecure'])) ? (bool) $params['parseSecure'] : null;
        $opt['closable'] = (isset($params['closable'])) ? (bool) $params['closable'] : null;
        $opt['closeBtn'] = (isset($params['closeBtn'])) ? (bool) $params['closeBtn'] : null;
        $opt['iframePreload'] = (isset($params['iframePreload'])) ? (bool) $params['iframePreload'] : null;
        $opt['iframeOptions'] = (isset($params['iframeOptions']) && (is_array($params['iframeOptions']))) ? $params['iframeOptions'] : null;
        $opt['size'] = (isset($params['size']) && (is_array($params['size']))) ? $params['size'] : null;
        $opt['shadow'] = (isset($params['shadow'])) ? $params['shadow'] : null;
        $opt['overlay'] = (isset($params['overlay'])) ? $params['overlay'] : null;
        $opt['onOpen'] = (isset($params['onOpen'])) ? $params['onOpen'] : null;
        $opt['onClose'] = (isset($params['onClose'])) ? $params['onClose'] : null;
        $opt['onUpdate'] = (isset($params['onUpdate'])) ? $params['onUpdate'] : null;
        $opt['onResize'] = (isset($params['onResize'])) ? $params['onResize'] : null;
        $opt['onMove'] = (isset($params['onMove'])) ? $params['onMove'] : null;
        $opt['onShow'] = (isset($params['onShow'])) ? $params['onShow'] : null;
        $opt['onHide'] = (isset($params['onHide'])) ? $params['onHide'] : null;

        // Include jQuery
        JHtml::_('jquery.framework');

        if (isset($params['fullScreen']) && (bool) $params['fullScreen']) {
            $opt['size'] = array('x' => '\\jQuery(window).width() - 80', 'y' => '\\jQuery(window).height() - 80');
        }

        $options = JHtml::getJSObject($opt);

        // Attach modal behavior to document
        $document
                ->addScriptDeclaration(
                        "
		jQuery(function($) {
			SqueezeBox.initialize(".$options.");
			SqueezeBox.assign($('".$selector."').get(), {
				parse: 'rel'
			});
		});"
        );

        // Set static array
        static::$loaded[__METHOD__][$sig] = true;

        return;
    }

}
