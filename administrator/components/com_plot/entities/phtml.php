<?php

defined('JPATH_PLATFORM') or die;

jimport('joomla.environment.browser');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.path');
jimport('joomla.utilities.arrayhelper');

require_once 'pbehavior.php';

class PHtml extends JHtml
{

    public static function _($key)
    {
        list($key, $prefix, $file, $func) = static::extract($key);
        
        if (array_key_exists($key, static::$registry)) {
            $function = static::$registry[$key];
            $args = func_get_args();

            // Remove function name from arguments
            array_shift($args);

            return static::call($function, $args);
        }
        
        $className = $prefix.ucfirst($file);
        
        if (!class_exists($className)) {
            $path = JPath::find(static::$includePaths, strtolower($file).'.php');

            if ($path) {
                require_once $path;

                if (!class_exists($className)) {
                    throw new InvalidArgumentException(sprintf('%s not found.', $className), 500);
                }
            } else {
                throw new InvalidArgumentException(sprintf('%s %s not found.', $prefix, $file), 500);
            }
        }

        $toCall = array($className, $func);

        if (is_callable($toCall)) {
            static::register($key, $toCall);
            $args = func_get_args();

            // Remove function name from arguments
            array_shift($args);
            return static::call($toCall, $args);
        } else {
            
            throw new InvalidArgumentException(sprintf('%s::%s not found.', $className, $func), 500);
        }
    }
    
    protected static function call($function, $args)
    {
            if (!is_callable($function))
            {
                    throw new InvalidArgumentException('Function not supported', 500);
            }

            // PHP 5.3 workaround
            $temp = array();

            foreach ($args as &$arg)
            {
                    $temp[] = &$arg;
            }
            
            return call_user_func_array($function, $temp);
    }

    protected static function extract($key)
    {
        $key = preg_replace('#[^A-Z0-9_\.]#i', '', $key);

        // Check to see whether we need to load a helper file
        $parts = explode('.', $key);

        $prefix = (count($parts) == 3 ? array_shift($parts) : 'PHtml');
        $file = (count($parts) == 2 ? array_shift($parts) : '');
        $func = array_shift($parts);

        return array(strtolower($prefix.'.'.$file.'.'.$func), $prefix, $file, $func);
    }
    
    public static function script($file, $framework = false, $relative = false, $path_only = false, $detect_browser = true, $detect_debug = true)
    {
        // Include MooTools framework
        if ($framework) {
            static::_('behavior.framework');
        }
        
        $includes = static::includeRelativeFiles('js', $file, $relative, $detect_browser, $detect_debug);

        // If only path is required
        if ($path_only) {
            if (count($includes) == 0) {
                return null;
            } elseif (count($includes) == 1) {
                return $includes[0];
            } else {
                return $includes;
            }
        }
        // If inclusion is required
        else {
            $document = JFactory::getDocument();

            foreach ($includes as $include) {
                $document->addScript($include);
            }
        }
    }
    

}

