<?php

class plotTags
{

    static function add($tagId, $entity, $entityId)
    {
        $db = JFactory::getDbo();
        $isTagExist = self::checkIsTagExist($tagId, $entity, $entityId);
        if (!$isTagExist) {
            $query = "INSERT INTO `#__plot_tags` SET `tagId` = ".(int) $tagId.", `entity` = ".$db->quote($entity).", `entityId` = ".(int) $entityId;
            return $db->setQuery($query)->query();
        }
        return false;
    }

    static function remove($tagId = '', $entity = '', $entityId = '')
    {
        $db = JFactory::getDbo();
        $whereArr = array();
        if ($tagId) {
            $whereArr[] = "`tagId` = ".(int) $tagId;
        }
        if ($entity) {
            $whereArr[] = "`entity` = ".$db->quote($entity);
        }
        if ($entityId) {
            $whereArr[] = "`entityId` = ".(int) $entityId;
        }
        $where = $whereArr ? 'WHERE '.implode(' AND ', $whereArr) : '';

        $query = "DELETE FROM `#__plot_tags` $where";
        $result = $db->setQuery($query)->query();
        return $result;
    }

    static function get($entity = '', $entityId = '')
    {
        $db = JFactory::getDbo();
        $whereParts = array();
        if ($entity) {
            $whereParts[] = "`t`.`entity` = ".$db->quote($entity);
        }
        if ($entityId) {
            $whereParts[] = "`t`.`entityId` = ".(int) $entityId;
        }
        $whereString = '';
        if ($whereParts) {
            $whereString = 'WHERE '.implode(' AND ', $whereParts);
        }

        $query = "SELECT `i`.`id`, `i`.`title`, `i`.`published`, `t`.`entity`, `t`.`entityId`, `t`.`title` AS `tagTitle`, `t`.`smiley` FROM `#__plot_tags` AS `t` LEFT JOIN `#__k2_items` AS `i` ON (`i`.`id` = `t`.`tagId`) $whereString";
        $tags = $db->setQuery($query)->loadObjectList();

        return $tags;
    }

    static function checkIsTagExist($tagId, $entity, $entityId)
    {
        $db = JFactory::getDbo();
        $query = "SELECT `id` FROM `#__plot_tags` WHERE `tagId` = ".(int) $tagId." AND `entity` = ".$db->quote($entity)." AND `entityId` = ".(int) $entityId;
        $result = $db->setQuery($query)->loadResult();
        return $result ? true : false;
    }

    static function getK2TagsList()
    {
        $db = JFactory::getDbo();
        $query = "SELECT `id`, `title` FROM `#__k2_items` WHERE `published` = 1 AND `catid` = ".plotGlobalConfig::getVar('tagsK2CategoryId'). " ORDER BY `title` ASC";
        $tags = $db->setQuery($query)->loadObjectList();
        return $tags;
    }

}
