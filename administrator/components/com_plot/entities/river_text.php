<?php

class riverText
{

    public $id = 0;
    public $published = 1;

    public function __construct($id = 0)
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select("*")
                ->from("`#__plot_river_texts` AS `rt`")
                ->where("`rt`.`id` = ".$db->quote($id));

        $riverText = $db->setQuery($query)->loadObject();

        if ($riverText) {
            foreach ($riverText AS $fName => $fValue) {
                $this->$fName = $fValue;
            }
        }

        return $riverText;
    }

    public function __get($name)
    {
        return false;
    }

    public function publish()
    {
        $db = JFactory::getDbo();
        $this->published = 1;
        $db->updateObject('#__plot_river_texts', $this, 'id');
    }

    public function unpublish()
    {
        $db = JFactory::getDbo();
        $this->published = 0;
        $db->updateObject('#__plot_river_texts', $this, 'id');
    }

    public function delete()
    {
        $db = JFactory::getDbo();

        $conditions = array($db->quoteName('id').' = '.$this->id);

        $query = $db->getQuery(true);
        $query->delete('#__plot_river_texts');
        $query->where($conditions);

        $db->setQuery($query)->query();
    }

    public function setOrdering($ordering = 0)
    {
        $db = JFactory::getDbo();
        $this->ordering = $ordering;
        $db->updateObject('#__plot_river_texts', $this, 'id');
    }

    public function save()
    {
        if ($this->isExist()) {
            $this->update();
        } else {
            $this->insert();
        }
        return $this->id;
    }

    public function isExist()
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('id')
                ->from('#__plot_river_texts')
                ->where('id='.$db->quote($this->id));

        $result = $db->setQuery($query)->loadResult();
        return $result;
    }

    private function update()
    {
        $db = JFactory::getDbo();
        $db->updateObject('#__plot_river_texts', $this, 'id');
    }

    private function insert()
    {
        $db = JFactory::getDbo();
        $this->ordering = $this->getMaxExistingOrdering() + 1;
        $db->insertObject('#__plot_river_texts', $this, 'id');
        $this->id = $db->insertid();
    }

    private function getMaxExistingOrdering()
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('MAX(ordering)')
                ->from('#__plot_river_texts');

        $maxOrdering = $db->setQuery($query)->loadResult();
        return $maxOrdering;
    }

}
