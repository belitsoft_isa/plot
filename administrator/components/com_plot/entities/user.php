<?php

require_once JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/user/user.php';
require_once JPATH_SITE . '/components/com_plot/helpers/plot.php';

class plotUser extends SocialUser
{

    public function __construct($params = array(), $debug = false)
    {
        parent::__construct($params, $debug);
        $this->userModel = JModelLegacy::getInstance('User', 'plotModel');
    }

    public function addChild($childId)
    {
        $childUser = plotUser::factory($childId);

        if ($childUser->profile_id == plotGlobalConfig::getVar('childProfileId') && $this->profile_id == plotGlobalConfig::getVar('parentProfileId')) {
            return $this->userModel->addChild($this->id, $childId);
        }
        return false;
    }


    public function getUserPoints()
    {


        $db = JFactory::getDbo();

        $query = $db->getQuery(true)
            ->clear()
            ->select('SUM(points) AS res')
            ->from('`#__social_points_history`')
            ->where('user_id='.$this->id);

        $db->setQuery($query);

        return (int)$db->loadResult();

    }

    public function isParent()
    {
        return ($this->profile_id == plotGlobalConfig::getVar('parentProfileId'));
    }

    public function removeChild($childId)
    {
        $childUser = plotUser::factory($childId);
        if ($childUser->profile_id == plotGlobalConfig::getVar('childProfileId') && $this->profile_id == plotGlobalConfig::getVar('parentProfileId')) {
            return $this->userModel->removeChild($this->id, $childId);
        }
        return false;
    }

    public function getChildrenIds()
    {
        $childrenIds = $this->userModel->getChildrenIds($this->id);
        return $childrenIds;
    }

    # <editor-fold defaultstate="collapsed" desc="FRIENDS"> 
    public function getTotalFriendsList()
    {
        require_once JPATH_ADMINISTRATOR . '/components/com_easysocial/models/friends.php';
        $friends = JModelLegacy::getInstance('friends', 'EasySocialModel');
        return $friends->getFriends($this->id, array("state" => SOCIAL_FRIENDS_STATE_FRIENDS));
    }

    public function approveFriendRequest($friendId)
    {
        $db = JFactory::getDbo();

        $approveQuery = "UPDATE `#__social_friends` SET `state` = 1 WHERE `actor_id` = " . (int)$friendId . " AND target_id = " . (int)$this->id . " AND state = '-1'";
        $db->setQuery($approveQuery)->query();

        return true;
    }

    public function rejectFriendRequest($friendId)
    {
        $db = JFactory::getDbo();

        $friendTableQuery = "SELECT * FROM `#__social_friends` WHERE ((`actor_id`=" . (int)$friendId . " AND `target_id`=" . (int)$this->id . ") OR (`actor_id`=" . (int)$this->id . " AND `target_id`=" . (int)$friendId . ")) AND `state`='-1'";

        $socialFriendTable = $db->setQuery($friendTableQuery)->loadObject();

        if (!$socialFriendTable) {
            return false;
        }

        $deleteSocialFriendQuery = "DELETE FROM `#__social_friends` WHERE `id` = $socialFriendTable->id";
        $db->setQuery($deleteSocialFriendQuery)->query();

        $deletePlotFriendQuery = "DELETE FROM `#__plot_friends` WHERE `social_id` = $socialFriendTable->id";
        $db->setQuery($deletePlotFriendQuery)->query();

        return true;
    }

    public function removeFriend($friendId)
    {
        $db = JFactory::getDbo();
        $socialFriendshipQuery = "SELECT `id` FROM `#__social_friends` WHERE `actor_id`=" . (int)$friendId . " AND `target_id`=" . (int)$this->id . " OR `actor_id`=" . (int)$this->id . " AND `target_id`=" . (int)$friendId;
        $socialFriendshipId = $db->setQuery($socialFriendshipQuery)->loadResult();

        if (!$socialFriendshipId) {
            return false;
        }

        $deleteSocialFriendQuery = "DELETE FROM `#__social_friends` WHERE `id` = $socialFriendshipId";
        $db->setQuery($deleteSocialFriendQuery)->query();

        $deletePlotFriendQuery = "DELETE FROM `#__plot_friends` WHERE `social_id` = $socialFriendshipId";
        $db->setQuery($deletePlotFriendQuery)->query();

        return true;
    }

    public function getFriendshipTag($friendId)
    {

        $db = JFactory::getDbo();
        $query = "SELECT `pf`.`tag_id` FROM `#__social_friends` AS `sf` "
            . "LEFT JOIN `#__plot_friends` AS `pf` ON (`pf`.`social_id` = `sf`.`id`) "
            . "WHERE (`sf`.`actor_id` = " . $db->quote($friendId) . " AND `sf`.`target_id` = " . $db->quote($this->id) . ") "
            . "OR    (`sf`.`actor_id` = " . $db->quote($this->id) . " AND `sf`.`target_id` = " . $db->quote($friendId) . ") ";

        $tagId = $db->setQuery($query)->loadResult();
        if (!$tagId) {
            $tagId = 0;
        }
        return $tagId;
    }

    # </editor-fold>

    public function getParentsIds()
    {
        $parentsIds = $this->userModel->getParentsIds($this->id);
        return $parentsIds;
    }

    public function getTags()
    {
        $userTags = plotTags::get('user', $this->id);
        return $userTags;
    }

    public static function loadUsers($ids = null, $debug = false)
    {
        // Determine if the argument is an array.
        $argumentIsArray = is_array($ids);

        // If it is null or 0, the caller wants to retrieve the current logged in user.
        if (is_null($ids) || (is_string($ids) && $ids == '') || !$ids) {
            $ids = array(JFactory::getUser()->id);
        }

        // Ensure that id's are always an array
        $ids = Foundry::makeArray($ids);

        // Reset the index of ids so we don't load multiple times from the same user.
        $ids = array_values($ids);

        // Always create the guest objects first.
        self::createGuestObject();

        // Total needs to be computed here before entering iteration as it might be affected by unset.
        $total = count($ids);

        // Placeholder for items that are already loaded.
        $loaded = array();

        // @task: We need to only load user's that aren't loaded yet.
        for ($i = 0; $i < $total; $i++) {
            if (empty($ids)) {
                break;
            }

            if (!isset($ids[$i]) && empty($ids[$i])) {
                continue;
            }

            $id = $ids[$i];

            // If id is null, we know we want the current user.
            if (is_null($id)) {
                $ids[$i] = JFactory::getUser()->id;
            }

            // The parsed id's could be an object from the database query.
            if (is_object($id) && isset($id->id)) {
                $id = $id->id;

                // Replace the current value with the proper value.
                $ids[$i] = $id;
            }

            if (isset(self::$userInstances[$id])) {
                $loaded[] = $id;
                unset($ids[$i]);
            }
        }

        // @task: Reset the ids after it was previously unset.
        $ids = array_values($ids);

        // Place holder for result items.
        $result = array();

        foreach ($loaded as $id) {
            $result[] = self::$userInstances[$id];
        }

        if (!empty($ids)) {
            // @task: Now, get the user data.

            $model = Foundry::model('Users');

            $users = $model->getUsersMeta($ids);

            if ($users) {
                // @task: Iterate through the users list and add them into the static property.
                foreach ($users as $user) {
                    // Get the user's cover photo
                    $user->cover = self::getCoverObject($user);

                    // Detect if the user has an avatar.
                    $user->defaultAvatar = false;

                    if ($user->avatar_id) {
                        $defaultAvatar = Foundry::table('DefaultAvatar');
                        $defaultAvatar->load($user->avatar_id);
                        $user->defaultAvatar = $defaultAvatar;

                    }

                    // Try to load the user from `#__social_users`
                    // If the user record doesn't exists in #__social_users we need to initialize it first.
                    if (!$model->metaExists($user->id)) {
                        $model->createMeta($user->id);
                    }

                    // Attach fields for this user.
                    $user->fields = $model->initUserData($user->id);

                    // // Get user's badges
                    $user->badges = Foundry::model('Badges')->getBadges($user->id);

                    // Create an object of itself and store in the static object.
                    $obj = new plotUser($user);

                    self::$userInstances[$user->id] = $obj;

                    $result[] = self::$userInstances[$user->id];
                }
            } else {
                foreach ($ids as $id) {
                    // Since there are no such users, we just use the guest object.
                    if(isset(self::$userInstances[0])){
                        self::$userInstances[$id] = self::$userInstances[0];
                    }else{
                        self::$userInstances[$id] = 0;
                    }


                    $result[] = self::$userInstances[$id];
                }
            }
        }

        // If the argument passed in is not an array, just return the proper value.
        if (!$argumentIsArray && count($result) == 1) {
            #return $result[0];
            return new plotUser($result[0]);
        }

        return $result;
    }

    public static function factory($ids = null, $debug = false)
    {
        $items = self::loadUsers($ids, $debug);
        return $items;
    }

    # <editor-fold defaultstate="collapsed" desc="COURSES">
    public function getCoursesIdsBoughtForMe($onlyNotFinished = false)
    {
        $db = JFactory::getDbo();
        $query = "SELECT course_id FROM `#__plot_courses_paid` WHERE `child_id` = " . (int)$this->id . ($onlyNotFinished ? ' AND `finished`=0' : '');
        $courses = $db->setQuery($query)->loadColumn();
        return array_unique($courses);
    }

    public function getFinishedPriceForMyCourse($courseId)
    {
        $db = JFactory::getDbo();
        $query = "SELECT `finished_price` FROM `#__plot_courses_paid` WHERE `child_id` = " . (int)$this->id . " AND `course_id`=" . (int)$courseId;
        $finishedPrice = $db->setQuery($query)->loadResult();
        return $finishedPrice ? $finishedPrice : 0;
    }

    public function isCourseFinished($courseId)
    {
        $db = JFactory::getDbo();
        $query = "SELECT `finished` FROM `#__plot_courses_paid` WHERE `child_id` = " . (int)$this->id . " AND `course_id`=" . (int)$courseId;
        $result = $db->setQuery($query)->loadResult();
        return $result ? true : false;
    }

    public function isCourseNew($courseId)
    {
        $db = JFactory::getDbo();
        $query = "SELECT `is_new` FROM `#__plot_courses_paid` WHERE `child_id` = " . (int)$this->id . " AND `course_id`=" . (int)$courseId;
        $result = $db->setQuery($query)->loadResult();
        return $result ? true : false;
    }

    public function getCoursesCompletePercent($courseId)
    {
        $db = JFactory::getDbo();
        $percent = 0;
        $query = "SELECT `lps`.* FROM `#__lms_learn_path_steps` as `lps` "
            . "WHERE `lps`.`course_id` = '" . $courseId . "'";
        $db->setQuery($query);
        $all_steps = $db->loadObjectList();

        $query = "SELECT lpsr.* FROM `#__lms_learn_path_results` as lpr, `#__lms_learn_path_step_results` as lpsr "
            . "WHERE lpr.id = lpsr.result_id "
            . "AND lpr.course_id = '" . $courseId . "' "
            . "AND lpr.user_id = '" . $this->id . "'";
        $db->setQuery($query);
        $all_result_steps = $db->loadObjectList();

        $tmp_all_steps = array();
        foreach ($all_steps as $n => $step) {
            $tmp_all_steps[$n] = $step;
            $tmp_all_steps[$n]->step_status = 0;
            foreach ($all_result_steps as $result_step) {
                if ($step->id == $result_step->step_id) {
                    $tmp_all_steps[$n]->step_status = $result_step->step_status;
                }
            }
        }

        $all_steps_new = $tmp_all_steps;

        if (isset($all_steps_new) && count($all_steps_new)) {
            $completed_step = 0;
            foreach ($all_steps_new as $step) {
                if (isset($step->step_status) && $step->step_status == 1) {
                    $completed_step++;
                }
            }

            if ($completed_step) {
                $percent = round(($completed_step / count($all_steps_new)) * 100);
            }
        }

        return $percent;
    }

    public function finishCourse($courseId)
    {
        $db = JFactory::getDbo();
        $query = "UPDATE `#__plot_courses_paid` SET `finished` = 1, `finished_date` = '" . JFactory::getDate()->toSql() . "'  WHERE `child_id` = " . (int)$this->id . " AND `course_id`=" . (int)$courseId;
        $db->setQuery($query)->query();

        if (PlotHelper::isCourseCertificateExist($courseId)) {
            PlotHelper::saveLmsCertificate($courseId);
        }
    }

    public function coursePayMoney($courseId)
    {

    }

    public function courseMarkAsNotNew($courseId)
    {
        $db = JFactory::getDbo();
        $query = "UPDATE `#__plot_courses_paid` SET `is_new` = 0 "
            . "WHERE `child_id` = " . $db->quote($this->id) . " "
            . "AND `course_id` = " . $db->quote($courseId);
        $db->setQuery($query)->query();
    }

    public function getFinishedCourses()
    {
        $db = JFactory::getDbo();
        $query = "SELECT `cp`.`course_id`, `cp`.`finished_price`, `lmsc`.`course_name` AS `title`, `cp`.`finished_date`,`pc`.`image`, `lmsc`.`course_description`  FROM `#__plot_courses_paid` AS `cp` "
            . "LEFT JOIN `#__lms_courses` AS `lmsc` ON (`lmsc`.`id` = `cp`.`course_id`) "
            . "LEFT JOIN `#__plot_courses` AS `pc` ON (`pc`.`id` = `cp`.`course_id`) "
            . "WHERE `child_id` = " . (int)$this->id . " AND `finished`=1 AND `lmsc`.`published` =1 "
            . "ORDER BY `cp`.`finished_date` DESC";
        $courses = $db->setQuery($query)->loadObjectList();
        return $courses;
    }
    # </editor-fold>

    # <editor-fold defaultstate="collapsed" desc="BOOKS">
    public function getFinishedPriceForMyBook($bookId)
    {
        $db = JFactory::getDbo();
        $query = "SELECT `finished_price` FROM `#__plot_books_paid` WHERE `child_id` = " . (int)$this->id . " AND `book_id`=" . (int)$bookId;
        $finishedPrice = $db->setQuery($query)->loadResult();
        return $finishedPrice ? $finishedPrice : 0;
    }

    public function getBooksIdsBoughtForMe($onlyNotFinished = false)
    {
        $db = JFactory::getDbo();
        $query = "SELECT book_id FROM `#__plot_books_paid` WHERE `child_id` = " . (int)$this->id . ($onlyNotFinished ? ' AND `finished`=0' : '');
        $books = $db->setQuery($query)->loadColumn();
        return array_unique($books);
    }

    public function getFinishedBooks()
    {
        $db = JFactory::getDbo();
        $query = "SELECT `bp`.`book_id`, `bp`.`finished_price`, `pubs`.`c_title` AS `title`, `finished_date` AS `finished_date` FROM `#__plot_books_paid` AS `bp` "
            . "LEFT JOIN `#__html5fb_publication` AS `pubs` ON (`pubs`.`c_id` = `bp`.`book_id`) "
            . "WHERE `child_id` = " . (int)$this->id . " AND `finished`=1";
        $books = $db->setQuery($query)->loadObjectList();
        return $books;
    }


    public function getAuthorship()
    {
        $db = JFactory::getDbo();
        $query = "SELECT `id`,`uid`,`entity`, `entityId`, `sum` AS `finished_price`, `date` AS `finished_date` FROM `#__plot_author_money`  "
            . "WHERE `uid` = " . (int)$this->id;
        $authorship = $db->setQuery($query)->loadObjectList();
        return $authorship;
    }

    public function getFinishedEssay($id)
    {
        $db = JFactory::getDbo();
        $query = "SELECT `e`.* FROM `#__plot_essay` AS `e` "
            . "WHERE `id` = " . (int)$id . " AND `status`=1";
        $essaies = $db->setQuery($query)->loadObjectList();
        return $essaies;
    }

    # </editor-fold>

    # <editor-fold defaultstate="collapsed" desc="PROFILE FIELDS">
    public function getSocialFieldData($key, $default = '')
    {

        if (!$key) {
            $result = $default;
        } else {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            if ($key == 'ADDRESS') {

                $query = $db->getQuery(true);
                $query->clear()
                    ->select('`data`')
                    ->from('#__social_fields_data')
                    ->where('uid=' . (int)$this->id)
                    ->where('datakey=' . $db->quote('country'));
                $country = $db->setQuery($query)->loadResult();

                $result = new stdClass();
                $result->country = $country ? $country : 'ZZ';

                $query->clear()
                    ->select('`data`')
                    ->from('#__social_fields_data')
                    ->where('uid=' . (int)$this->id)
                    ->where('datakey=' . $db->quote('state'));

                $state = $db->setQuery($query)->loadResult();
                $result->state = $state ? $state : '';

                $query->clear()
                    ->select('`data`')
                    ->from('#__social_fields_data')
                    ->where('uid=' . (int)$this->id)
                    ->where('datakey=' . $db->quote('city'));
                $city = $db->setQuery($query)->loadResult();
                $result->city = $city ? $city : '';

            } else if ($key == 'BIRTHDAY') {


                $query = $db->getQuery(true);
                $query->clear()
                    ->select('`data`')
                    ->from('#__social_fields_data')
                    ->where('uid=' . (int)$this->id)
                    ->where('datakey=' . $db->quote('day'));

                $day = $db->setQuery($query)->loadResult();

                $query->clear()
                    ->select('`data`')
                    ->from('#__social_fields_data')
                    ->where('uid=' . (int)$this->id)
                    ->where('datakey=' . $db->quote('month'));
                $month = $db->setQuery($query)->loadResult();

                $query->clear()
                    ->select('`data`')
                    ->from('#__social_fields_data')
                    ->where('uid=' . (int)$this->id)
                    ->where('datakey=' . $db->quote('year'));
                $year = $db->setQuery($query)->loadResult();
                $result = new stdClass();
                $result->day = $day;
                $result->month = $month;
                $result->year = $year;

            } else {
                $query->select('data')
                    ->from('#__social_fields_data')
                    ->where('uid=' . (int)$this->id)
                    ->where('datakey=' . $db->quote($key));

                $result = $db->setQuery($query)->loadResult();
                if($this->isJson($result)){
                    $result='';
                }
            }


        }


        return $result;

        /*if (!isset($this->fields[$key])) {
            $result = $default;
        } else {
            $jsonDecoded = json_decode($this->fields[$key]->data);
            if (json_last_error() == JSON_ERROR_NONE) {
                if(is_numeric($this->fields[$key]->data)){
                    $result = $this->fields[$key]->data;
                }else{
                    $result = $jsonDecoded;
                }

            } else {
                $result = $this->fields[$key]->data;
            }
            if ($key == 'ADDRESS' && !$result) {
                $result = new stdClass();
                $result->country = 'ZZ';
                $result->state = '';
                $result->city = '';
            }

        }*/

        return $result;
    }


    public function isJson($string) {
        return is_array(json_decode($string,true));
    }

    public function getSocialFieldId($key)
    {
        $db = JFactory::getDbo();

        $stepsIdsForProfileQuery = "SELECT `id` FROM `#__social_fields_steps` WHERE `uid`=" . (int)$this->profile_id . " AND `type`='profiles'";
        $stepsIdsForProfile = $db->setQuery($stepsIdsForProfileQuery)->loadColumn();
        $stepsIdsForProfile[] = 0;

        $query = "SELECT `f`.`id` FROM `#__social_fields` AS `f` WHERE `f`.`unique_key` = " . $db->quote($key) . " AND `step_id` IN (" . implode(',', $stepsIdsForProfile) . ")";
        $id = $db->setQuery($query)->loadResult();
        return $id;
    }

    public function getCountryCode()
    {
        $socialFieldAddress = $this->getSocialFieldData('ADDRESS');

        $countryCode = (isset($socialFieldAddress->country) && $socialFieldAddress->country) ? $socialFieldAddress->country : 'ZZ';
        return $countryCode;
    }

    public function saveName(stdClass $obj)
    {
        $db = JFactory::getDbo();

        if (isset($obj->last)) {
            $query = "UPDATE `#__social_fields_data` SET `data`=" . $db->quote($obj->last) . ", `raw`=" . $db->quote($obj->last) . " WHERE  `uid`=" . (int)$this->id . " AND `type`='user' AND `datakey`='last'";
            $db->setQuery($query)->query();
            $first = $this->getSocialFieldData('first');
            $middle = $this->getSocialFieldData('middle');
            $obj->name = $first . ' ' . $middle . ' ' . $obj->last;

            $usersQuery = "UPDATE `#__users` SET `name`=" . $db->quote($obj->name) . " WHERE `id`=" . (int)$this->id;
            $db->setQuery($usersQuery)->query();
            $query = "UPDATE `#__social_fields_data` SET `data`=" . $db->quote($obj->name) . ", `raw`=" . $db->quote($obj->name) . " WHERE  `uid`=" . (int)$this->id . " AND `type`='user' AND `datakey`='name'";
            $db->setQuery($query)->query();
            /*$nameFieldId = $this->getSocialFieldId('JOOMLA_FULLNAME');
            $query = "UPDATE `#__social_fields_data` SET `data`=" . $db->quote(json_encode($obj)) . ", `raw`=" . $db->quote($obj->name) . " WHERE `field_id`=" . (int)$nameFieldId . " AND `uid`=" . (int)$this->id . " AND `type`='user'";
            $db->setQuery($query)->query();*/

        }
        if (isset($obj->first)) {
            $query = "UPDATE `#__social_fields_data` SET `data`=" . $db->quote($obj->first) . ", `raw`=" . $db->quote($obj->first) . " WHERE  `uid`=" . (int)$this->id . " AND `type`='user' AND `datakey`='first'";
            $db->setQuery($query)->query();
            $last = $this->getSocialFieldData('last');
            $middle = $this->getSocialFieldData('middle');
            $obj->name = $obj->first . ' ' . $middle . ' ' . $last;
            $usersQuery = "UPDATE `#__users` SET `name`=" . $db->quote($obj->name) . " WHERE `id`=" . (int)$this->id;
            $db->setQuery($usersQuery)->query();
            $query = "UPDATE `#__social_fields_data` SET `data`=" . $db->quote($obj->name) . ",  `raw`=" . $db->quote($obj->name) . " WHERE  `uid`=" . (int)$this->id . " AND `type`='user' AND `datakey`='name'";
            $db->setQuery($query)->query();
           /* $nameFieldId = $this->getSocialFieldId('JOOMLA_FULLNAME');
            $query = "UPDATE `#__social_fields_data` SET `data`=" . $db->quote(json_encode($obj)) . ", `raw`=" . $db->quote($obj->name) . " WHERE  `datakey`='name' AND `uid`=" . (int)$this->id . " AND `type`='user'";
            $db->setQuery($query)->query();*/
        }
        if (isset($obj->middle)) {
            $query = "UPDATE `#__social_fields_data` SET `data`=" . $db->quote($obj->middle) . ", `raw`=" . $db->quote($obj->middle) . " WHERE  `uid`=" . (int)$this->id . " AND `type`='user' AND `datakey`='middle'";
            $db->setQuery($query)->query();
            $last = $this->getSocialFieldData('last');
            $first = $this->getSocialFieldData('first');
            $obj->name = $first . ' ' . $obj->middle . ' ' . $last;
            $usersQuery = "UPDATE `#__users` SET `name`=" . $db->quote($obj->name) . " WHERE `id`=" . (int)$this->id;
            $db->setQuery($usersQuery)->query();
            $query = "UPDATE `#__social_fields_data` SET `data`=" . $db->quote($obj->name) . ", `raw`=" . $db->quote($obj->name) . " WHERE  `uid`=" . (int)$this->id . " AND `type`='user' AND `datakey`='name'";
            $db->setQuery($query)->query();
            /*$nameFieldId = $this->getSocialFieldId('JOOMLA_FULLNAME');
            $query = "UPDATE `#__social_fields_data` SET `data`=" . $db->quote(json_encode($obj)) . ", `raw`=" . $db->quote($obj->name) . " WHERE `field_id`=" . (int)$nameFieldId . " AND `uid`=" . (int)$this->id . " AND `type`='user'";
            $db->setQuery($query)->query();*/
        }

        /* if ($obj->first && $obj->last && isset($obj->middle)) {
             $obj->name = $obj->first.' '.$obj->middle.' '.$obj->last;
             $nameFieldId = $this->getSocialFieldId('JOOMLA_FULLNAME');
             $query = "UPDATE `#__social_fields_data` SET `data`=".$db->quote(json_encode($obj)).", `raw`=".$db->quote($obj->name)." WHERE `field_id`=".(int) $nameFieldId." AND `uid`=".(int) $this->id." AND `type`='user'";
             $db->setQuery($query)->query();

             $usersQuery = "UPDATE `#__users` SET `name`=".$db->quote($obj->name)." WHERE `id`=".(int) $this->id;
             $db->setQuery($usersQuery)->query();
             return true;
         }*/

        return true;
    }

    public function saveAddress(stdClass $obj)
    {
        $result = false;
        if ($obj->country) {

            $fieldId = $this->getSocialFieldId('country');
            $result = $this->saveSocialField($fieldId, 'country', $obj->country);
        }
        if ($obj->state) {
            $fieldId = $this->getSocialFieldId('state');
            $result = $this->saveSocialField($fieldId, 'state', $obj->state);
        }
        if ($obj->city) {
            $fieldId = $this->getSocialFieldId('city');
            $result = $this->saveSocialField($fieldId, 'city', $obj->city);;
        }
        /*$nameFieldId = $this->getSocialFieldId('ADDRESS');
        $query = "UPDATE `#__social_fields_data` SET `data`=".$db->quote(json_encode($obj)).", `raw`=".$db->quote($raw)." WHERE `field_id`=".(int) $nameFieldId." AND `uid`=".(int) $this->id." AND `type`='user'";
        $result = $db->setQuery($query)->query();*/

        return $result;
    }

    public function saveSocialField($fieldId, $key, $data)
    {
        $db = JFactory::getDbo();
        if ($this->isFieldForUserExist($key)) {
            $query = "UPDATE `#__social_fields_data` SET `data`=" . $db->quote($data) . ", `raw`=" . $db->quote($data) . " WHERE `datakey`=" . $db->quote($key) . " AND `uid`=" . (int)$this->id . " AND `type`='user'";
            $result = $db->setQuery($query)->query();
        } else {
            $fieldData = new stdClass();
            $fieldData->field_id = $fieldId;
            $fieldData->uid = $this->id;
            $fieldData->type = 'user';
            $fieldData->data = $data;
            $fieldData->datakey = $key;
            $fieldData->params = '';
            $fieldData->raw = $data;
            $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
        }
        return $result;
    }

    public function saveEmail($email)
    {
        $db = JFactory::getDbo();
        if (!$this->isEmailOrUsernameExist($email)) {
            $set = '`email`=' . $db->quote($email) . ($this->isLoggedViaSocials() ? '' : ', `username`=' . $db->quote($email));
            $query = "UPDATE `#__users` SET $set WHERE `id`=" . (int)$this->id;
            return $db->setQuery($query)->query();
        } else {
            return false;
        }
    }

    private function isEmailOrUsernameExist($email)
    {
        $db = JFactory::getDbo();
        $checkEmailQuery = "SELECT `id` FROM `#__users` WHERE `email`=" . $db->quote($email);
        $isEmailExist = $db->setQuery($checkEmailQuery)->loadResult();

        $checkUsernameQuery = "SELECT `id` FROM `#__users` WHERE `username`=" . $db->quote($email);
        $isUsernameExist = $db->setQuery($checkUsernameQuery)->loadResult();

        if ($isEmailExist || $isUsernameExist) {
            return true;
        }
        return false;
    }

    public function saveNewPassword($password)
    {
        $db = JFactory::getDbo();
        $passHash = JUserHelper::hashPassword($password);
        $query = "UPDATE `#__users` SET `password`=" . $db->quote($passHash) . " WHERE `id`=" . (int)$this->id;
        $result = $db->setQuery($query)->query();
        return $result;
    }

    private function isFieldForUserExist($key)
    {
        $db = JFactory::getDbo();
        //$query = "SELECT `id` FROM `#__social_fields_data` WHERE `field_id`=".(int) $fieldId." AND `uid`=".(int) $this->id." AND `type`='user'";
        $query = "SELECT `id` FROM `#__social_fields_data` WHERE `datakey`=" . $db->quote($key) . " AND `uid`=" . (int)$this->id . " AND `type`='user'";
        $result = $db->setQuery($query)->loadResult();
        return $result;
    }

    # </editor-fold>

    public function isLoggedViaSocials()
    {
        return ($this->username != $this->email);
    }

    # <editor-fold defaultstate="collapsed" desc="MONEY">
    public function addMoney($amount, $user_id = 0)
    {

        if (!$user_id) {
            $user_id = $this->id;
        }
        $db = JFactory::getDbo();
        if ($this->isMoneyRowExist()) {
            $query = "UPDATE `#__plot_money` SET `money` = `money` + " . (int)$amount . " WHERE `user_id` = " . (int)$user_id;
        } else {
            $query = "INSERT INTO `#__plot_money` SET `user_id` = " . (int)$user_id . ", `money` = " . (int)$amount;
        }
        $result = $db->setQuery($query)->query();

        $this->updateEncodedMoneyValue();
        return $result;
    }

    public function addAuthorMoneyInformation($entity, $entityId, $sum){
        $db = JFactory::getDbo();

        $money = new stdClass();
        $money->uid = (int)$this->id;
        $money->entity = $entity;
        $money->entityId = (int)$entityId;
        $money->sum = (int)$sum;
        $money->date=Foundry::date()->toMySQL();
       return  $db->insertObject('#__plot_author_money', $money);

    }


    public function getMoney()
    {
        $query = "SELECT `money` FROM `#__plot_money` WHERE `user_id` = " . (int)$this->id;
        $money = (int)JFactory::getDbo()->setQuery($query)->loadResult();
        return $money;
    }

    public function getCashoutAmountTotal()
    {
        $db = JFactory::getDbo();

        $query = "SELECT SUM(`amount`) FROM `#__plot_cashouts` WHERE `userId` = " . (int)$this->id;
        $totalAmount = $db->setQuery($query)->loadResult();
        return $totalAmount ? $totalAmount : 0;
    }

    public function getCashouts()
    {
        $db = JFactory::getDbo();

        $query = "SELECT * FROM `#__plot_cashouts` WHERE `userId` = " . (int)$this->id . " ORDER BY `date` DESC";
        $cashouts = $db->setQuery($query)->loadObjectList();
        return $cashouts;
    }

    public function cashout($amount, $method)
    {
        $db = JFactory::getDbo();
        $cashout = new stdClass();
        $cashout->userId = (int)$this->id;
        $cashout->amount = (int)$amount;
        $cashout->cashoutMethod = $method;
        $cashout->date = JFactory::getDate()->toSql();
        $db->insertObject('#__plot_cashouts', $cashout);

        $this->addMoney(-$amount);
        return true;
    }

    public function checkMoneyValue()
    {
        $money = $this->getMoney();
        if ($money == 0) {
            return true;
        }
        $query = "SELECT `value` FROM `#__plot_level_m` WHERE `id` = " . (int)$this->id;
        $moneyEncrypted = (string)JFactory::getDbo()->setQuery($query)->loadResult();
        if (md5(MONEY_ENCRYPTING_SALT . $money) == $moneyEncrypted) {
            return true;
        }
        return false;
    }

    private function isMoneyRowExist()
    {
        $db = JFactory::getDbo();
        $query = "SELECT `user_id` FROM `#__plot_money` WHERE `user_id` = " . (int)$this->id;
        $result = $db->setQuery($query)->loadResult();
        return $result;
    }

    private function updateEncodedMoneyValue()
    {
        $db = JFactory::getDbo();
        $moneyAmount = $this->getMoney();
        $db->setQuery("DELETE FROM `#__plot_level_m` WHERE `id` = " . (int)$this->id)->query();
        $db->setQuery("INSERT INTO `#__plot_level_m` SET "
            . "`id` = " . (int)$this->id . ", "
            . "`value` = " . $db->quote(md5(MONEY_ENCRYPTING_SALT . $moneyAmount))
        )->query();
        return true;
    }

    # </editor-fold>

    public function getCurrentAge()
    {

        $birthday = $this->getSocialFieldData('BIRTHDAY');

        if (!$birthday) {
            $birthday = new stdClass();
            $birthday->day = $this->getSocialFieldData('day');
            $birthday->month = $this->getSocialFieldData('month');
            $birthday->year = $this->getSocialFieldData('year');
        }
        if (!isset($birthday->day)) {
            $birthdayString = '1900-01-01';
        } else {
            $birthdayString = $birthday->year . '-' . $birthday->month . '-' . $birthday->day;
        }

        $birthdayDate = JFactory::getDate($birthdayString);
        $currentAge = JFactory::getDate()->diff($birthdayDate)->y;
        return $currentAge;
    }

    public function getSquareAvatarUrl()
    {
        if ($this->avatars['square'] && file_exists(JPATH_BASE . '/media/com_easysocial/avatars/users/' . $this->id . '/' . $this->avatars['square'])) {
            $avatarUrl = JUri::root() . 'media/com_easysocial/avatars/users/' . $this->id . '/' . $this->avatars['square'];
        } else {
            if ($this->isParent()) {
                $avatarUrl = JUri::root() . 'media/com_easysocial/defaults/avatars/user/square.png';
            } else {
                $avatarUrl = JUri::root() . 'images/com_plot/def_tag_avatar.jpg';
            }

        }

        return $avatarUrl;
    }

    public function plotGetAvatar(){


        if(!$this->isParent() && (strpos($this->getAvatar(),'/media/com_easysocial/defaults/avatars/user/medium.png')!==false)){
            $avatar=$this->getAvatar();
            $avatar=str_replace("medium.png","child_medium.jpg",$this->getAvatar());
            return $avatar;
        }else{
            return $this->getAvatar();
        }

    }

    /**
     * $date - show user level, that was at selected date.
     */
    public function getLevel($date = '')
    {
        $db = Foundry::db();
        if($this->id){
            $points=(int)$this->getUserPoints();
        }else{
           $points=(int)$this->getPoints();
        }

        $currentUserPoints = $date ? $this->getUserPointsAtDate($date) : $points;

        $queryCurrentLevel = "SELECT * FROM `#__plot_levels` WHERE `points` <= " . (int)$currentUserPoints . " ORDER BY `points` DESC";
        $db->setQuery($queryCurrentLevel, 0, 1);
        $currentLevel = $db->loadObject();

        $queryNextLevel = "SELECT * FROM `#__plot_levels` WHERE `points` > " . (int)$currentUserPoints . " ORDER BY `points` ASC";
        $db->setQuery($queryNextLevel, 0, 1);
        $nextLevel = $db->loadObject();

        $currentLevel->pointsRemainToNextLevel = isset($nextLevel->points) ? ($nextLevel->points - $currentUserPoints) : 0;

        return $currentLevel;
    }

    private function getUserPointsAtDate($date)
    {
        $db = JFactory::getDbo();
        $query = "SELECT SUM(`points`) FROM `#__social_points_history` WHERE `user_id` = " . (int)$this->id . " AND `created` <= " . $db->quote($date);
        $points = $db->setQuery($query)->loadResult();

        return (int)$points;
    }

    public function getLevelNewHouse()
    {
        # $db = JFactory::getDbo();

        $currentLevel = $this->getLevel();

        # now hardcode - 1, 5 and 10 levels for new houses
        $levelNewHouse = new stdClass();
        if ($currentLevel->title < 5) {
            $levelNewHouse->title = 5;
        } elseif ($currentLevel->title >= 5 && $currentLevel->title < 10) {
            $levelNewHouse->title = 10;
        } else {
            $levelNewHouse = false;
        }

        /*
        
        DONT remove this!!! code for time when will be good functionality for images.
        
        $levelsQuery = "SELECT * FROM `#__plot_levels` WHERE `points` > ".(int) $this->getPoints()." ORDER BY `points` ASC";
        $levels = $db->setQuery($levelsQuery)->loadObjectList();
        
        $levelNewHouse = false;
        foreach ($levels AS $level) {
            if ($level->house && $level->house != $currentLevel->house) {
                $levelNewHouse = $level;
                break;
            }
        }
        */
        return $levelNewHouse;
    }

    public function getStream($options)
    {
        $db = JFactory::getDbo();

        $options['limitstart'] = isset($options['limitstart']) ? (int)$options['limitstart'] : 0;
        $options['limit'] = isset($options['limit']) ? (int)$options['limit'] : 0;

        $query = "SELECT `sph`.*, `sp`.`command`, `sp`.`title`, `pph`.`entity_id` FROM `#__social_points_history` AS `sph` "
            . "INNER JOIN `#__social_points` AS `sp` ON (`sp`.`id` = `sph`.`points_id`) "
            . "INNER JOIN `#__plot_points_history` AS `pph` ON (`pph`.`sph_id` = `sph`.`id`) "
            . "WHERE `sph`.`user_id` = " . (int)$this->id . " "
            . "AND `sp`.`command` != 'photos.upload' "
            . "ORDER BY `sph`.`created` DESC "
            . "LIMIT " . $options['limitstart'] . ', ' . $options['limit'];
        $stream = $db->setQuery($query)->loadObjectList();

        foreach ($stream AS $i => $streamObj) {
            switch ($streamObj->command) {
                case 'buy.book':
                case 'read.book':
                    $book = new plotBook($streamObj->entity_id);
                    $stream[$i]->bottom_title = $book->c_title;
                    $stream[$i]->bottom_description = $book->c_pub_descr;

                    if (file_exists(JPATH_SITE . '/media/com_html5flippingbook/thumbs/thimb_' . $book->c_thumb)) {
                        $stream[$i]->image_url = JUri::root() . 'media/com_html5flippingbook/thumbs/thimb_' . $book->c_thumb;
                    } else {
                        $stream[$i]->image_url = JUri::root() . 'images/com_plot/def_book.jpg';
                    }

                    $stream[$i]->original = JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $streamObj->entity_id);
                    $stream[$i]->author = '';
                    if ($book->book_author) {
                        $stream[$i]->author = $book->book_author;
                    } else {
                        $stream[$i]->author = $book->c_author;
                    }
                    break;
                case 'course.finish':
                    $course = new plotCourse($streamObj->entity_id);
                    $stream[$i]->bottom_title = $course->course_name;
                    $stream[$i]->bottom_description = $course->course_description;
                    $stream[$i]->image_url = isset($course->image) ? JUri::root() . $course->image : 'default';
                    $stream[$i]->original = JRoute::_('index.php?option=com_plot&view=course&id=' . $streamObj->entity_id);
                    break;
                case 'avatar.change':

                    $stream[$i]->bottom_title = '';
                    $stream[$i]->bottom_description = '';
                    $stream[$i]->image_url = Foundry::photo(plotUser::factory()->id, SOCIAL_TYPE_USER, $this->getAvatarPhotoId())->data->getSource('square');//Foundry::photo($stream[$i]->entity_id)->data->getSource('square');
                    $stream[$i]->original = Foundry::photo(plotUser::factory()->id, SOCIAL_TYPE_USER, $stream[$i]->entity_id)->data->getSource('original');

                    break;
                case 'add.tag':
                    $tag = PlotHelper::getTagById($stream[$i]->entity_id);
                    $stream[$i]->title .= ' "' . $tag->title . '"';
                    $stream[$i]->bottom_title = '';
                    $stream[$i]->bottom_description = '';
                    $stream[$i]->image_url = JUri::root() . 'images/com_plot/def_tag.jpg';
                    $stream[$i]->created=Foundry::date()->toMySQL();
                    break;
                case 'assess.tag':
                    $tag = PlotHelper::getTagById($stream[$i]->entity_id);
                    $stream[$i]->title .= ' "' . $tag->title . '"';
                    $stream[$i]->bottom_title = '';
                    $stream[$i]->bottom_description = '';
                    $stream[$i]->image_url = JUri::root() . 'images/com_plot/def_tag.jpg';
                    $stream[$i]->created=Foundry::date()->toMySQL();
                    break;
                case 'describe.tag':
                    $tag = PlotHelper::getTagById($stream[$i]->entity_id);
                    $stream[$i]->title .= ' "' . $tag->title . '"';
                    $stream[$i]->bottom_title = '';
                    $stream[$i]->bottom_description = '';
                    $stream[$i]->image_url = JUri::root() . 'images/com_plot/def_tag.jpg';
                    $stream[$i]->created=Foundry::date()->toMySQL();
                    break;
                case 'add.essay':
                    $essay = PlotHelper::getEssayById($stream[$i]->entity_id);
                    if ($essay) {
                        $stream[$i]->bottom_title = JText::_('COM_PLOT_ESSAY_WAS_CREATED');
                        $stream[$i]->bottom_description = '';
                        $stream[$i]->image_url = JUri::root() . 'templates/plot/img/default-essay.jpg';
                        $stream[$i]->original = JUri::root() . 'media/com_plot/essay/' . $stream[$i]->entity_id . '/' . $essay->img;

                    }
                    break;
                case 'finished.program':
                    $program = new plotProgram($stream[$i]->entity_id);
                    if ($program) {
                        $stream[$i]->bottom_title = $program->title;
                        $stream[$i]->bottom_description = $program->description;
                        if (JFile::exists(JPATH_BASE . '/' . $program->img)) {
                            $stream[$i]->image_url = JUri::root() . $program->img;
                            $stream[$i]->original = JUri::root() . $program->img;
                        } else {
                            $stream[$i]->image_url = JUri::root() . 'templates/plot/img/blank150x150.jpg';
                            $stream[$i]->original = JUri::root() . 'templates/plot/img/blank150x150.jpg';
                        }

                    }
                    break;
                case 'add.coursevideo':
                    $video = PlotHelper::getCourseVideoById($stream[$i]->entity_id);
                    if ($video) {
                        $stream[$i]->bottom_title = JText::_('COM_PLOT_COURSE_VIDEO_WAS_CREATED');
                        $stream[$i]->bottom_description = '';
                        $stream[$i]->image_url = '';
                        $stream[$i]->original = '';
                        if ((strripos($video->link, 'youtube.com/') !== FALSE)) {
                            $youtubeNumber = substr($video->link, strripos($video->link, '?v=') + 3);
                            if (($pos = strpos($youtubeNumber, '&')) !== FALSE)
                                $youtubeNumber = substr($youtubeNumber, 0, $pos);
                            if (file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'coursevideo' . DIRECTORY_SEPARATOR . $video->user_id . DIRECTORY_SEPARATOR . 'thumb/' . $youtubeNumber . '.jpg')) {
                                $stream[$i]->image_url = JUri::root() . 'media/com_plot/coursevideo/' . $video->user_id . '/thumb/' . $youtubeNumber . '.jpg';

                            } else {
                                $stream[$i]->image_url = JUri::root() . 'images/com_plot/def_video.jpg';
                            }
                            $stream[$i]->original = JRoute::_('index.php?option=com_plot&task=profile.ajaxOpenVideo&youtubeNumber=' . $youtubeNumber);
                        }
                        $stream[$i]->youtubeNumber=$youtubeNumber;
                    }
                    break;
                case 'add.photo':
                    $photo = PlotHelper::getImageThumbById($stream[$i]->entity_id);
                    $original = PlotHelper::getImageOriginalById($stream[$i]->entity_id);
                    $stream[$i]->bottom_title = $photo->title;
                    $stream[$i]->bottom_description = $photo->caption;
                    $stream[$i]->image_url = JUri::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $stream[$i]->entity_id . '/' . $photo->value;
                    $stream[$i]->original = JUri::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $stream[$i]->entity_id . '/' . $original->value;
                    break;
                case 'add.certificate':
                    $photo = PlotHelper::getImageThumbById($stream[$i]->entity_id);
                    $original = PlotHelper::getImageOriginalById($stream[$i]->entity_id);
                    $stream[$i]->bottom_title = $photo->title;
                    $stream[$i]->bottom_description = $photo->caption;
                    $stream[$i]->image_url = JUri::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $stream[$i]->entity_id . '/' . $photo->value;
                    $stream[$i]->original = JUri::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $stream[$i]->entity_id . '/' . $original->value;
                    $stream[$i]->created=$original->created;
                    break;
                case 'add.old.certificate':
                    $photo = PlotHelper::getImageThumbById($stream[$i]->entity_id);
                    $original = PlotHelper::getImageOriginalById($stream[$i]->entity_id);
                    $stream[$i]->bottom_title = $photo->title;
                    $stream[$i]->bottom_description = $photo->caption;
                    $stream[$i]->image_url = JUri::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $stream[$i]->entity_id . '/' . $photo->value;
                    $stream[$i]->original = JUri::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $stream[$i]->entity_id . '/' . $original->value;
                    $stream[$i]->created=$original->created;
                    break;
                case 'add.meeting':
                case 'subscribe.meeting':
                case 'unsubscribe.meeting':
                    $event = PlotHelper::getEventById($stream[$i]->entity_id);
                    if ($event) {
                        $stream[$i]->bottom_title = $event->title;
                        $stream[$i]->bottom_description = $event->description;
                        if (file_exists(JPATH_SITE . '/images/com_plot/events/' . (int)$event->user_id . '/thumb/' . $event->img)) {
                            $stream[$i]->image_url = JUri::root() . 'images/com_plot/events/' . (int)$event->user_id . '/thumb/' . $event->img;
                        } else {
                            $stream[$i]->image_url = JUri::root() . 'images/com_plot/def_meeting.jpg';
                        }
                        $stream[$i]->original = JRoute::_('index.php?option=com_plot&view=event&id=' . $stream[$i]->entity_id);
                    } else {
                        $stream[$i]->bottom_title = '';
                        $stream[$i]->bottom_description = '';
                        $stream[$i]->image_url = JUri::root() . 'images/com_plot/def_meeting.jpg';
                        $stream[$i]->original = '#';
                    }
                    break;
                case 'add.video':
                    $video = PlotHelper::getVideoById($stream[$i]->entity_id);
                    $stream[$i]->bottom_title = $video->title;
                    $stream[$i]->bottom_description = $video->description;
                    $stream[$i]->image_url = JUri::root() . 'media/com_plot/videos/' . $video->uid . '/' . $video->path;
                    $stream[$i]->video_type = $video->type;
                    if ($video->type == 'link') {
                        if ((strripos($video->path, 'youtube.com/') !== FALSE)) {
                            $youtubeNumber = substr($video->path, strripos($video->path, '?v=') + 3);
                            if (($pos = strpos($youtubeNumber, '&')) !== FALSE)
                                $youtubeNumber = substr($youtubeNumber, 0, $pos);
                            if (file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR . $video->uid . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $youtubeNumber . '.jpg')) {
                                $stream[$i]->image_url = JUri::root() . 'images/com_plot/video/' . $video->uid . '/thumb/' . $youtubeNumber . '.jpg';
                                $stream[$i]->image_url = str_replace("\\images", "/images", $stream[$i]->image_url);
                            } else {
                                $stream[$i]->image_url = JUri::root() . 'images/com_plot/def_video.jpg';
                            }
                            $stream[$i]->youtubeNumber = $youtubeNumber;
                            $stream[$i]->original = '//www.youtube.com/embed/' . $youtubeNumber;
                        } else {
                            $stream[$i]->image_url = JUri::root() . 'images/com_plot/def_video.jpg';
                            $stream[$i]->original = JUri::root() . 'images/com_plot/def_video.jpg';
                        }
                    } else {
                        $stream[$i]->youtubeNumber = $stream[$i]->entity_id;
                        $stream[$i]->image_url = JUri::root() . 'images/com_plot/def_video.jpg';
                        if (file_exists(JPATH_SITE . '/media/com_plot/videos/' . $video->uid . '/thumb/' . substr($video->path, 0, -3) . 'jpg')) {
                            $stream[$i]->image_url = JUri::root() . 'media/com_plot/videos/' . $video->uid . '/thumb/' . substr($video->path, 0, -3) . 'jpg';
                        }
                    }
                    break;
                case 'add.portfolio':
                    $portfolio = PlotHelper::getPortfolioById($stream[$i]->entity_id);
                    if ($portfolio) {
                        $stream[$i]->bottom_title = $portfolio->title;
                        $stream[$i]->bottom_description = $portfolio->description;
                        if (file_exists(JPATH_SITE . '/images/com_plot/portfolio/' . (int)$portfolio->user_id . '/thumb/' . $portfolio->img)) {
                            $stream[$i]->image_url = JUri::root() . 'images/com_plot/portfolio/' . (int)$portfolio->user_id . '/thumb/' . $portfolio->img;
                        } else {
                            $stream[$i]->image_url = JUri::root() . 'images/com_plot/def_portfolio.jpg';
                        }
                        $stream[$i]->original = "#";
                    } else {
                        $stream[$i]->bottom_title = '';
                        $stream[$i]->bottom_description = '';
                        $stream[$i]->image_url = JUri::root() . 'images/com_plot/def_portfolio.jpg';
                        $stream[$i]->original = '#';
                    }
                    break;
                default:
                    unset($stream[$i]);
                    break;
            }
        }

        return $stream;
    }

    public function getAvatarPhotoId()
    {
        $db = JFactory::getDbo();
        $query = "SELECT `photo_id` FROM `#__social_avatars` WHERE `uid` = " . (int)$this->id;
        $avatarId = $db->setQuery($query)->loadResult();
        return $avatarId ? $avatarId : 0;
    }

    public function isBookAuthor($bookId)
    {
        if (plotUser::factory()->id) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true)
                ->select('`p`.author')
                ->from('`#__plot_book_cost` AS `p`')
                ->where('`p`.`book_id` = ' . (int)$bookId);

            $id = $db->setQuery($query)->loadResult();

            return ((int)$id == (int)plotUser::factory()->id) ? true : false;
        } else {
            return false;
        }

    }

    public function isBookFinished($bookId)
    {
        if (plotUser::factory()->id) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true)
                ->select('`p`.read')
                ->from('`#__plot_books` AS `p`')
                ->where('`p`.`book_id` = ' . (int)$bookId)
                ->where('`p`.`child_id`=' . (int)$this->id);

            $finished = $db->setQuery($query)->loadResult();

            return $finished;
        } else {
            return false;
        }
    }

    public function setCourseFinished($courseId)
    {
        if (plotUser::factory()->id) {
            $db = JFactory::getDbo();
            $query = "UPDATE `#__plot_courses_paid` SET `finished_course`=1 WHERE `course_id`=".(int)$courseId." AND `child_id`=" . (int)$this->id;
            $result = $db->setQuery($query)->query();
            return $result;
        }
    }



    public function isBookTestFinished($bookId)
    {
        if (plotUser::factory()->id) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true)
                ->select('`p`.read')
                ->from('`#__plot_books` AS `p`')
                ->where('`p`.`book_id` = ' . (int)$bookId)
                ->where('`p`.`child_id`=' . (int)$this->id);

            $finished = $db->setQuery($query)->loadResult();
            if ($finished == 1) {
                return $finished;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }


    public function getFinishedCourse($id)
    {
        $db = JFactory::getDbo();
        $query = "SELECT `e`.* FROM `#__plot_course_video` AS `e` "
            . "WHERE e.`id` = " . (int)$id . " AND e.`status`=1";
        $courses = $db->setQuery($query)->loadObjectList();
        return $courses;
    }

    public function getFinishedBookByBookId($id)
    {
        $db = JFactory::getDbo();
        $query = "SELECT `e`.* FROM `#__plot_essay` AS `e` "
            . "WHERE e.`book_id` = " . (int)$id . " AND e.`status`=1 AND e.user_id=" . (int)$this->id;

        $book = $db->setQuery($query)->loadObject();
        return $book;
    }

    public function getFinishedCourseByCourseId($id)
    {
        $db = JFactory::getDbo();
        $query = "SELECT `e`.* FROM `#__plot_course_video` AS `e` "
            . "WHERE `course_id` = " . (int)$id . " AND `status`=1 AND user_id=" . (int)$this->id;
        $course = $db->setQuery($query)->loadObject();
        return $course;
    }

    public function isAvaliableProgram()
    {
        if (plotUser::factory()->id) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true)
                ->select('`c`.id')
                ->from('`#__plot_program` AS `p`')
                ->leftJoin('`#__social_clusters_nodes` AS c ON c.id=p.id')
                ->where('(`p`.`level`=' . (int)plotGlobalConfig::getVar("programMasterLevelId") . ' OR `p`.`level`=' . (int)plotGlobalConfig::getVar("programMageLevelId") . ')')
                ->where('c.type="user"')
                ->where('c.uid=' . $this->id);

            $finished = $db->setQuery($query)->loadColumn();

            if (!$finished) {
                return false;
            }
            if ($finished) {
                $ids = implode(',', $finished);

                $query = $db->getQuery(true)
                    ->select('`c`.cluster_node_id')
                    ->from('`#__plot_clusrers_node_map` AS `c`')
                    ->where('c.status=1')
                    ->where('c.cluster_node_id IN("' . $ids . '")');

                $programs = $db->setQuery($query)->loadColumn();
                if ($programs) {
                    return true;
                }
            }

            return false;
        } else {
            return false;
        }
    }


    public function programProgress($program_id)
    {
        $progObj = new plotProgram($program_id);
        $courses = $progObj->getAllCoursesByProgramId();
        $books = $progObj->getAllBooksByProgramId();
        $count_all_books = count($books);
        $count_all_courses = count($courses);
        $count_finifed_books = 0;
        $count_finifed_courses = 0;

        foreach ($courses AS $item) {
            if ($this->getFinishedCourseByCourseId($item)) {
                $count_finifed_courses++;
            }
        }
        foreach ($books AS $item) {
            if ($this->getFinishedBookByBookId($item)) {
                $count_finifed_books++;
            }
        }
        $total_program = $count_all_books + $count_all_courses;
        $total_finished = $count_finifed_books + $count_finifed_courses;
        if ($total_program <= $total_finished) {
            return 100;
        } else {
            return $percent = (int)(100 * $total_finished) / $total_program;

        }

    }

    public function isHaveBooks()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`b`.id')
            ->from('`#__plot_books_paid` AS `b`')
            ->innerJoin('`#__html5fb_publication` AS `p` ON p.c_id=b.book_id')
            ->where('b.child_id=' . (int)$this->id)
            ->where('p.published=1');


        return $db->setQuery($query)->loadResult();
    }

    public function isHaveCourses()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`b`.id')
            ->from('`#__plot_courses_paid` AS `b`')
            ->innerJoin('`#__lms_courses` AS `l` ON l.id=b.course_id')
            ->where('b.child_id=' . (int)$this->id)
            ->where('l.published=1');


        return $db->setQuery($query)->loadResult();
    }

    public function bookIsReaded($bookId)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`b`.read')
            ->from('`#__plot_books` AS `b`')
            ->where('b.`book_id` = ' . (int)$bookId)
            ->where('b.`child_id` = ' . (int)plotUser::factory()->id);
        return $db->setQuery($query)->loadResult();
    }

    public function essayIsWritten($bookId)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`b`.status')
            ->from('`#__plot_essay` AS `b`')
            ->where('b.`book_id` = ' . (int)$bookId)
            ->where('b.`user_id` = ' . (int)plotUser::factory()->id);

        return $db->setQuery($query)->loadResult();
    }

    public function courseVideoApproved($course_id)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`b`.status')
            ->from('`#__plot_course_video` AS `b`')
            ->where('b.`course_id` = ' . (int)$course_id)
            ->where('b.`user_id` = ' . (int)plotUser::factory()->id);
        return $db->setQuery($query)->loadResult();
    }

    public function isCourseVideoAdd($course_id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`b`.*')
            ->from('`#__plot_course_video` AS `b`')
            ->where('b.`course_id` = ' . (int)$course_id)
            ->where('b.`user_id` = ' . (int)plotUser::factory()->id);
        return $db->setQuery($query)->loadObject();
    }

    public function isEssayIsset($bookId)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`b`.*')
            ->from('`#__plot_essay` AS `b`')
            ->where('b.`book_id` = ' . (int)$bookId)
            ->where('b.`user_id` = ' . (int)plotUser::factory()->id);

        return $db->setQuery($query)->loadObject();
    }

    public function getCountMyPrograms()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('p.id')
            ->from('#__plot_program AS c')
            ->leftJoin('`#__social_clusters` AS `p` ON c.id=p.id')
            ->leftJoin('`#__social_clusters_categories` AS `cats` ON cats.id=p.category_id')
            ->leftJoin('`#__social_clusters_nodes` AS cn ON cn.cluster_id=c.id')
            ->leftJoin('`#__plot_age_program_map` AS age ON age.program_id=p.id')
            ->leftJoin('`#__plot_ages` AS ages ON ages.id=age.age_id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON l.id=c.level')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`p`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`p`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")');
        if (!$this->isAvaliableProgram()) {
            $query->where('c.level!=' . plotGlobalConfig::getVar("programMageLevelId"));
        }

        $query->where('`cn`.uid=' . (int)$this->id)
            ->group('c.id');
        $res = $db->setQuery($query)->loadColumn();
        if ($res) {
            return count($res);
        } else {
            return 0;
        }

    }

    public function isHaveEvents()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`e`.id')
            ->from('`#__plot_events` AS `e`')
            ->innerJoin('`#__plot_user_event_map` AS `em` ON em.event_id=e.id')
            ->where('`e`.user_id=' . (int)$this->id . ' OR `em`.uid=' . (int)$this->id)
            ->where('e.end_date>=NOW()')
            ->group('`e`.`id`')
            ->order('`e`.`create_date` DESC');


        return $db->setQuery($query)->loadResult();
    }

    public function isCourseTestFinished($courseId)
    {
        $db = JFactory::getDbo();
        $query = "SELECT `finished_course` FROM `#__plot_courses_paid` WHERE `child_id` = " . (int)$this->id . " AND `course_id`=" . (int)$courseId;
        $result = $db->setQuery($query)->loadResult();
        return $result ? true : false;
    }

    public  function isNewBook($bookId){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`b`.read')
            ->from('`#__plot_books` AS `b`')
            ->where('`b`.`book_id`='.(int)$bookId)
            ->where('`b`.`child_id` ='.$this->id);
        return (int)$db->setQuery($query)->loadResult();
    }

    public function isHasTag($tag_id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`b`.id')
            ->from('`#__plot_tags` AS `b`')
            ->where('`b`.`entity`="user"')
            ->where('`b`.`entityId` ='.$this->id)
            ->where('`b`.`tagId` ='.$tag_id);
        return (int)$db->setQuery($query)->loadResult();
    }

}
