<?php

class plotBook
{

    public $id = 0;

    public function __construct($id = 0)
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select("`pub`.*, `res`.`width`, `res`.`height`, c.author_cost, c.author_min AS author_min_cost, c.author_max AS author_max_cost, c.admin_cost, c.admin_min AS admin_min_cost, c.admin_max AS admin_max_cost, c.author AS book_author, c.text")
                ->from("#__html5fb_publication AS pub")
                ->leftJoin('#__plot_book_cost AS c ON `c`.book_id=`pub`.`c_id`')
                ->leftJoin('`#__html5fb_resolutions` AS `res` ON `pub`.`c_resolution_id` = `res`.`id`')
                ->where("`pub`.`c_id` = ".(int) $id);
        $book = $db->setQuery($query)->loadObject();
        if( $book){
            $this->c_id = $book->c_id;
            foreach ($book AS $i => $value) {
                $this->$i = $value;


            }
        }else{
            $this->c_id=$this->id;
        }




    }

    public function __get($name)
    {
        return false;
    }

    public function getBookMinCost()
    {
        $buyerCosts = $this->getBuyerCosts(0);
        return $buyerCosts['total'];
    }

    public function getBuyerCosts($amountForComplete)
    {
        # 1. calculate first values based on percentage
        $preTotalSumWithoutPaymentSystemFee1 = $amountForComplete / ((ONE_HUNDRED_PERCENT - $this->admin_cost - $this->author_cost) / 100);
        $preTotalSum1 = $preTotalSumWithoutPaymentSystemFee1 / ((ONE_HUNDRED_PERCENT - plotGlobalConfig::getVar('bookCostPaymentSystemPercent')) / 100);
        $preAdminSum1 = $preTotalSumWithoutPaymentSystemFee1 * ($this->admin_cost / ONE_HUNDRED_PERCENT);
        $preAuthorSum1 = $preTotalSumWithoutPaymentSystemFee1 * ($this->author_cost / ONE_HUNDRED_PERCENT);

        # 2. check if author cost and admin cost out of range
        $isAdminSumOutOfRange = false;
        $isAuthorSumOutOfRange = false;
        if ($preAdminSum1 < $this->admin_min_cost) {
            $preAdminSum1 = $this->admin_min_cost;
            $isAdminSumOutOfRange = true;
        }
        if ($preAdminSum1 > $this->admin_max_cost) {
            $preAdminSum1 = $this->admin_max_cost;
            $isAdminSumOutOfRange = true;
        }
        if ($preAuthorSum1 < $this->author_min_cost) {
            $preAuthorSum1 = $this->author_min_cost;
            $isAuthorSumOutOfRange = true;
        }
        if ($preAuthorSum1 > $this->author_max_cost) {
            $preAuthorSum1 = $this->author_max_cost;
            $isAuthorSumOutOfRange = true;
        }

        # 3. if author cost or admin cost out of range - recalculate values
        if ($isAdminSumOutOfRange && !$isAuthorSumOutOfRange) {
            $preTotalSumWithoutPaymentSystemFee2 = ($amountForComplete + $preAdminSum1) / ((ONE_HUNDRED_PERCENT - $this->author_cost) / 100);
            $preTotalSum2 = $preTotalSumWithoutPaymentSystemFee2 / ((ONE_HUNDRED_PERCENT - plotGlobalConfig::getVar('bookCostPaymentSystemPercent')) / 100);
            $preAdminSum2 = $preAdminSum1;
            $preAuthorSum2 = $preTotalSumWithoutPaymentSystemFee2 * ($this->author_cost / ONE_HUNDRED_PERCENT);
        }
        if ($isAuthorSumOutOfRange && !$isAdminSumOutOfRange) {
            $preTotalSumWithoutPaymentSystemFee2 = ($amountForComplete + $preAuthorSum1) / ((ONE_HUNDRED_PERCENT - $this->admin_cost) / 100);
            $preTotalSum2 = $preTotalSumWithoutPaymentSystemFee2 / ((ONE_HUNDRED_PERCENT - plotGlobalConfig::getVar('bookCostPaymentSystemPercent')) / 100);
            $preAdminSum2 = $preTotalSumWithoutPaymentSystemFee2 * ($this->admin_cost / ONE_HUNDRED_PERCENT);
            $preAuthorSum2 = $preAuthorSum1;
        }
        if ($isAuthorSumOutOfRange && $isAdminSumOutOfRange) {
            $preTotalSumWithoutPaymentSystemFee2 = $amountForComplete + $preAuthorSum1 + $preAdminSum1;
            $preTotalSum2 = $preTotalSumWithoutPaymentSystemFee2 / ((ONE_HUNDRED_PERCENT - plotGlobalConfig::getVar('bookCostPaymentSystemPercent')) / 100);
            $preAdminSum2 = $preAdminSum1;
            $preAuthorSum2 = $preAuthorSum1;
        }

        # 4. second check if admin / author costs out of range
        $isAdminSumOutOfRange2 = false;
        $isAuthorSumOutOfRange2 = false;
        if (isset($preAdminSum2) && ($preAdminSum2 < $this->admin_min_cost)) {
            $preAdminSum2 = $this->admin_min_cost;
            $isAdminSumOutOfRange2 = true;
        }
        if (isset($preAdminSum2) && ($preAdminSum2 > $this->admin_max_cost)) {
            $preAdminSum2 = $this->admin_max_cost;
            $isAdminSumOutOfRange2 = true;
        }
        if (isset($preAuthorSum2) && ($preAuthorSum2 < $this->author_min_cost)) {
            $preAuthorSum2 = $this->author_min_cost;
            $isAuthorSumOutOfRange2 = true;
        }
        if (isset($preAuthorSum2) && ($preAuthorSum2 > $this->author_max_cost)) {
            $preAuthorSum2 = $this->author_max_cost;
            $isAuthorSumOutOfRange2 = true;
        }

        # 5. second recalculating values if admin / author costs out of range
        if ($isAdminSumOutOfRange2 && !$isAuthorSumOutOfRange2) {
            $preTotalSumWithoutPaymentSystemFee3 = ($amountForComplete + $preAdminSum2) / ((ONE_HUNDRED_PERCENT - $this->author_cost) / 100);
            $preTotalSum3 = $preTotalSumWithoutPaymentSystemFee3 / ((ONE_HUNDRED_PERCENT - plotGlobalConfig::getVar('bookCostPaymentSystemPercent')) / 100);
            $preAdminSum3 = $preAdminSum2;
            $preAuthorSum3 = $preTotalSumWithoutPaymentSystemFee3 * ($this->author_cost / ONE_HUNDRED_PERCENT);
        }
        if ($isAuthorSumOutOfRange2 && !$isAdminSumOutOfRange2) {
            $preTotalSumWithoutPaymentSystemFee3 = ($amountForComplete + $preAuthorSum2) / ((ONE_HUNDRED_PERCENT - $this->admin_cost) / 100);
            $preTotalSum3 = $preTotalSumWithoutPaymentSystemFee3 / ((ONE_HUNDRED_PERCENT - plotGlobalConfig::getVar('bookCostPaymentSystemPercent')) / 100);
            $preAdminSum3 = $preTotalSumWithoutPaymentSystemFee3 * ($this->admin_cost / ONE_HUNDRED_PERCENT);
            $preAuthorSum3 = $preAuthorSum2;
        }
        if ($isAuthorSumOutOfRange2 && $isAdminSumOutOfRange2) {
            $preTotalSumWithoutPaymentSystemFee3 = $amountForComplete + $preAuthorSum2 + $preAdminSum2;
            $preTotalSum3 = $preTotalSumWithoutPaymentSystemFee3 / ((ONE_HUNDRED_PERCENT - plotGlobalConfig::getVar('bookCostPaymentSystemPercent')) / 100);
            $preAdminSum3 = $preAdminSum2;
            $preAuthorSum3 = $preAuthorSum2;
        }

        # 6. get last calculated values
        $totalSumWithoutPaymentSystemFee = $preTotalSumWithoutPaymentSystemFee1;
        if (isset($preTotalSumWithoutPaymentSystemFee2)) {
            $totalSumWithoutPaymentSystemFee = $preTotalSumWithoutPaymentSystemFee2;
        }
        if (isset($preTotalSumWithoutPaymentSystemFee3)) {
            $totalSumWithoutPaymentSystemFee = $preTotalSumWithoutPaymentSystemFee3;
        }
        $totalSum = $preTotalSum1;
        if (isset($preTotalSum2)) {
            $totalSum = $preTotalSum2;
        }
        if (isset($preTotalSum3)) {
            $totalSum = $preTotalSum3;
        }
        $adminSum = $preAdminSum1;
        if (isset($preAdminSum2)) {
            $adminSum = $preAdminSum2;
        }
        if (isset($preAdminSum3)) {
            $adminSum = $preAdminSum3;
        }
        $authorSum = $preAuthorSum1;
        if (isset($preAuthorSum2)) {
            $authorSum = $preAuthorSum2;
        }
        if (isset($preAuthorSum3)) {
            $authorSum = $preAuthorSum3;
        }

        # 7. round data and calculate using minus for no sum lost
        $data = array(
            'admin' => round($adminSum),
            'author' => round($authorSum),
            'paymentSystem' => round($totalSum) - round($authorSum) - round($adminSum) - round($amountForComplete),
            'total' => round($totalSum),
        );

        return $data;
    }

    public function whoBoughtBook($bookId, $userId){

            $db = JFactory::getDbo();
            $query = $db->getQuery(true)
                ->select('`p`.parent_id')
                ->from('`#__plot_books_paid` AS `p`')
                ->where('`p`.`book_id` = ' . (int)$bookId)
                ->where('`p`.`child_id`='.(int)$userId);

            $buyer = $db->setQuery($query)->loadResult();
            return $buyer;
    }
    static function getList()
    {
        $db = JFactory::getDbo();

        $query = "SELECT * FROM `#__html5fb_publication` WHERE published=1";
        $ages = $db->setQuery($query)->loadObjectList();
        return $ages;
    }

    public function bookPrograms(){

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('g.*, l.title AS level_title, p.link, m.`value` AS img')
            ->from('`#__plot_books_program_map` AS `b`')
            ->innerJoin('`#__social_clusters` AS `g` ON g.id=b.program_id')
            ->leftJoin('`#__plot_program` AS `p` ON `p`.`id`=`g`.id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON `l`.`id`=`p`.level')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`g`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`g`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")')
            ->where('`g`.`cluster_type`="group"')
            ->where("`b`.`book_id` = ".(int)$this->c_id)
            ->group('g.id');
        $programs = $db->setQuery($query)->loadObjectList();
        foreach ($programs AS $row) {
            $group 	= FD::group( $row->id );
            $row->link = FRoute::groups( array( 'layout' => 'item' , 'id' => $group->getAlias() ), false ). '?plot=1';
            $thumbnailPath = JPATH_SITE . '/' . $row->img;
            if ($row->img == "" || !is_file($thumbnailPath) || !$row->img) {
                $row->img = JURI::root() . "/media/com_easysocial/defaults/avatars/group/square.png";
            }else{
                $row->img = JURI::root() .$row->img;
            }
        }
        return $programs;
    }


    public function getBookProgramsIds(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('b.program_id')
            ->from('`#__plot_books_program_map` AS `b`')
            ->where("`b`.`book_id` = ".(int)$this->c_id);
        $programs = $db->setQuery($query)->loadColumn();
        return $programs;
    }

    public function isGetPages(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('b.*')
            ->from('`#__html5fb_pages` AS `b`')
            ->where("`b`.`publication_id` = ".(int)$this->c_id);
        $pages = $db->setQuery($query)->loadResult();

        return $pages;
    }

    public function setIsNotNewBook($uid){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->update('`#__plot_books`')
            ->set("`read` = 0")
            ->where('`child_id` ='.(int)$uid)
            ->where('`book_id`=' . $this->c_id);
        $db->setQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

    }



}
