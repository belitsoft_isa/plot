<?php

class plotHashtags
{

    static function getList()
    {
        $db = JFactory::getDbo();

        $query = "SELECT * FROM `#__plot_hashtags`";
        $ages = $db->setQuery($query)->loadObjectList();

        return $ages;
    }

    static function get($entity, $entityId)
    {
        $db = JFactory::getDbo();

        $query = "SELECT `age_id` FROM `#__plot_age_".$entity."_map` AS `ent` "
                ."LEFT JOIN `#__plot_ages` AS `a` ON (`a`.`id` = `ent`.`age_id`) "
                ."WHERE `ent`.`".$entity."_id` = ".(int) $entityId."";

        $ages = $db->setQuery($query)->loadObjectList();
        return $ages;
    }

    static function remove($entity, $entityId = '', $ageId = '')
    {
        $db = JFactory::getDbo();
        $whereArr = array();
        if ($entityId) {
            $whereArr[] = "`".$entity."_id` = ".(int) $entityId;
        }
        if ($ageId) {
            $whereArr[] = "`age_id` = ".(int) $ageId;
        }
        $where = $whereArr ? 'WHERE '.implode(' AND ', $whereArr) : '';

        $query = "DELETE FROM `#__plot_age_".$entity."_map` $where";
        $result = $db->setQuery($query)->query();
        return $result;
    }

    static function add($entity, $entityId, $ageId)
    {
        $db = JFactory::getDbo();
        $isAgeExist = self::checkIsAgeExist($entity, $entityId, $ageId);
        if (!$isAgeExist) {
            $query = "INSERT INTO `#__plot_age_".$entity."_map` SET `age_id` = ".(int) $ageId.", `".$entity."_id` = ".(int) $entityId;

            return $db->setQuery($query)->query();
        }
        return false;
    }



}
