<?php

class plotCourse
{

    public $id = 0;

    public function __construct($id = 0)
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select("`lc`.`id` AS `lmsId`, `lc`.`course_name`, `lc`.`course_description`,`pc`.*, `cp`.`count_points` AS `count_points`, `lc`.`cat_id`, `lms_cats`.`c_category`, "
            . "`lc`.`owner_id`, `lc`.`published`, `lc`.`publish_start`, `lc`.`start_date`, `lc`.`publish_end`, `lc`.`end_date`, b.id_pub")
            ->from("`#__lms_courses` AS `lc`")
            ->leftJoin("#__plot_courses AS `pc` ON (`pc`.`id` = `lc`.`id`)")
            ->leftJoin("#__plot_count_points AS `cp` ON (`cp`.`entity` = 'course' AND `cp`.`entity_id` = `lc`.`id`)")
            ->leftJoin("`#__lms_course_cats` AS `lms_cats` ON (`lms_cats`.`id` = `lc`.`cat_id`)")
            ->leftJoin('`#__plot_course_book_map` AS `b` ON `b`.id_course=`pc`.id')
            ->where("`lc`.`id` = " . $db->quote($id));

        $course = $db->setQuery($query)->loadObject();

        if ($course) {
            foreach ($course AS $fName => $fValue) {
                $this->$fName = $fValue;
            }
            if (!$course->image) {
                $this->image = 'templates/plot/img/default_course.jpg';
            }
        }

        if (!$this->id && $this->lmsId) {
            $this->id = $this->lmsId;
        }

        $this->setEmptyCostsToZero();

        return $course;
    }

    private function setEmptyCostsToZero()
    {
        if (!$this->admin_min_cost) {
            $this->admin_min_cost = 0;
        }
        if (!$this->admin_cost) {
            $this->admin_cost = 0;
        }
        if (!$this->admin_max_cost) {
            $this->admin_max_cost = 0;
        }
        if (!$this->author_min_cost) {
            $this->author_min_cost = 0;
        }
        if (!$this->author_cost) {
            $this->author_cost = 0;
        }
        if (!$this->author_max_cost) {
            $this->author_max_cost = 0;
        }
        if (!$this->count_points) {
            $this->count_points = 0;
        }
    }

    public function __get($name)
    {
        return false;
    }

    public function save()
    {
        if ($this->isExist()) {
            $this->update();
        } else {
            $this->insert();
        }
        return $this->id;
    }

    public function isExist()
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('id')
            ->from('#__plot_courses')
            ->where('id=' . $db->quote($this->id));

        $result = $db->setQuery($query)->loadResult();
        return $result;
    }


    public function savePublication($courseId, $pubId)
    {
        if ((int)$pubId) {
            if ($this->isCoursePublicationExist()) {
                $this->updateCoursePublication($pubId);
            } else {
                $this->insertCoursePublication($pubId);
            }
            return $this->id;
        }

    }

    public function isCoursePublicationExist()
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('id_course')
            ->from('#__plot_course_book_map')
            ->where('id_course=' . $db->quote($this->id));

        $result = $db->setQuery($query)->loadResult();
        return $result;
    }

    private function insertCoursePublication($pubId)
    {
        $db = JFactory::getDbo();
        $course = new stdClass();
        $course->id_course = $this->id;
        $course->id_pub = $pubId;
        $db->insertObject('#__plot_course_book_map', $course, 'id_course');
    }

    private function updateCoursePublication($pubId)
    {
        $db = JFactory::getDbo();
        $course = new stdClass();
        $course->id_course = $this->id;
        $course->id_pub = $pubId;
        $db->updateObject('#__plot_course_book_map', $course, 'id_course');
    }

    private function update()
    {
        $db = JFactory::getDbo();
        plotPoints::updateInsert($this->count_points, 'course', $this->id);

        $countPoints = $this->count_points;
        unset($this->count_points);

        $db->updateObject('#__plot_courses', $this, 'id');

        $this->count_points = $countPoints;
        $this->updateTags();
        $this->updateAges();


    }

    private function insert()
    {
        $db = JFactory::getDbo();
        plotPoints::updateInsert($this->count_points, 'course', $this->id);

        $countPoints = $this->count_points;
        unset($this->count_points);

        $db->insertObject('#__plot_courses', $this, 'id');

        $this->count_points = $countPoints;
        $this->updateTags();
        $this->updateAges();
    }

    private function updateTags()
    {
        plotTags::remove('', 'course', $this->id);
        foreach ($this->tags AS $tagId) {
            plotTags::add($tagId, 'course', $this->id);
        }
    }


    private function updateAges()
    {
        plotAges::remove('course', $this->id);
        foreach ($this->ages AS $ageId) {
            plotAges::add('course', $this->id, $ageId);
        }
    }

    public function getTags()
    {
        $tags = plotTags::get('course', $this->id);
        return $tags;
    }

    public function getAges()
    {
        $ages = plotAges::get('course', $this->id);
        return $ages;
    }

    public function getAuthor()
    {
        $author = plotUser::factory($this->owner_id);
        return $author;
    }

    public function getMinCost()
    {
        $buyerCosts = $this->getBuyerCosts(0);
        return $buyerCosts['total'];
    }

    public function getBuyerCosts($amountForComplete)
    {
        # 1. calculate first values based on percentage
        $preTotalSumWithoutPaymentSystemFee1 = $amountForComplete / ((ONE_HUNDRED_PERCENT - $this->admin_cost - $this->author_cost) / 100);
        $preTotalSum1 = $preTotalSumWithoutPaymentSystemFee1 / ((ONE_HUNDRED_PERCENT - plotGlobalConfig::getVar('courseCostPaymentSystemPercent')) / 100);
        $preAdminSum1 = $preTotalSumWithoutPaymentSystemFee1 * ($this->admin_cost / ONE_HUNDRED_PERCENT);
        $preAuthorSum1 = $preTotalSumWithoutPaymentSystemFee1 * ($this->author_cost / ONE_HUNDRED_PERCENT);

        # 2. check if author cost and admin cost out of range
        $isAdminSumOutOfRange = false;
        $isAuthorSumOutOfRange = false;
        if ($preAdminSum1 < $this->admin_min_cost) {
            $preAdminSum1 = $this->admin_min_cost;
            $isAdminSumOutOfRange = true;
        }
        if ($preAdminSum1 > $this->admin_max_cost) {
            $preAdminSum1 = $this->admin_max_cost;
            $isAdminSumOutOfRange = true;
        }
        if ($preAuthorSum1 < $this->author_min_cost) {
            $preAuthorSum1 = $this->author_min_cost;
            $isAuthorSumOutOfRange = true;
        }
        if ($preAuthorSum1 > $this->author_max_cost) {
            $preAuthorSum1 = $this->author_max_cost;
            $isAuthorSumOutOfRange = true;
        }

        # 3. if author cost or admin cost out of range - recalculate values
        if ($isAdminSumOutOfRange && !$isAuthorSumOutOfRange) {
            $preTotalSumWithoutPaymentSystemFee2 = ($amountForComplete + $preAdminSum1) / ((ONE_HUNDRED_PERCENT - $this->author_cost) / 100);
            $preTotalSum2 = $preTotalSumWithoutPaymentSystemFee2 / ((ONE_HUNDRED_PERCENT - plotGlobalConfig::getVar('courseCostPaymentSystemPercent')) / 100);
            $preAdminSum2 = $preAdminSum1;
            $preAuthorSum2 = $preTotalSumWithoutPaymentSystemFee2 * ($this->author_cost / ONE_HUNDRED_PERCENT);
        }
        if ($isAuthorSumOutOfRange && !$isAdminSumOutOfRange) {
            $preTotalSumWithoutPaymentSystemFee2 = ($amountForComplete + $preAuthorSum1) / ((ONE_HUNDRED_PERCENT - $this->admin_cost) / 100);
            $preTotalSum2 = $preTotalSumWithoutPaymentSystemFee2 / ((ONE_HUNDRED_PERCENT - plotGlobalConfig::getVar('courseCostPaymentSystemPercent')) / 100);
            $preAdminSum2 = $preTotalSumWithoutPaymentSystemFee2 * ($this->admin_cost / ONE_HUNDRED_PERCENT);
            $preAuthorSum2 = $preAuthorSum1;
        }
        if ($isAuthorSumOutOfRange && $isAdminSumOutOfRange) {
            $preTotalSumWithoutPaymentSystemFee2 = $amountForComplete + $preAuthorSum1 + $preAdminSum1;
            $preTotalSum2 = $preTotalSumWithoutPaymentSystemFee2 / ((ONE_HUNDRED_PERCENT - plotGlobalConfig::getVar('courseCostPaymentSystemPercent')) / 100);
            $preAdminSum2 = $preAdminSum1;
            $preAuthorSum2 = $preAuthorSum1;
        }

        # 4. second check if admin / author costs out of range
        $isAdminSumOutOfRange2 = false;
        $isAuthorSumOutOfRange2 = false;
        if (isset($preAdminSum2) && ($preAdminSum2 < $this->admin_min_cost)) {
            $preAdminSum2 = $this->admin_min_cost;
            $isAdminSumOutOfRange2 = true;
        }
        if (isset($preAdminSum2) && ($preAdminSum2 > $this->admin_max_cost)) {
            $preAdminSum2 = $this->admin_max_cost;
            $isAdminSumOutOfRange2 = true;
        }
        if (isset($preAuthorSum2) && ($preAuthorSum2 < $this->author_min_cost)) {
            $preAuthorSum2 = $this->author_min_cost;
            $isAuthorSumOutOfRange2 = true;
        }
        if (isset($preAuthorSum2) && ($preAuthorSum2 > $this->author_max_cost)) {
            $preAuthorSum2 = $this->author_max_cost;
            $isAuthorSumOutOfRange2 = true;
        }

        # 5. second recalculating values if admin / author costs out of range
        if ($isAdminSumOutOfRange2 && !$isAuthorSumOutOfRange2) {
            $preTotalSumWithoutPaymentSystemFee3 = ($amountForComplete + $preAdminSum2) / ((ONE_HUNDRED_PERCENT - $this->author_cost) / 100);
            $preTotalSum3 = $preTotalSumWithoutPaymentSystemFee3 / ((ONE_HUNDRED_PERCENT - plotGlobalConfig::getVar('courseCostPaymentSystemPercent')) / 100);
            $preAdminSum3 = $preAdminSum2;
            $preAuthorSum3 = $preTotalSumWithoutPaymentSystemFee3 * ($this->author_cost / ONE_HUNDRED_PERCENT);
        }
        if ($isAuthorSumOutOfRange2 && !$isAdminSumOutOfRange2) {
            $preTotalSumWithoutPaymentSystemFee3 = ($amountForComplete + $preAuthorSum2) / ((ONE_HUNDRED_PERCENT - $this->admin_cost) / 100);
            $preTotalSum3 = $preTotalSumWithoutPaymentSystemFee3 / ((ONE_HUNDRED_PERCENT - plotGlobalConfig::getVar('courseCostPaymentSystemPercent')) / 100);
            $preAdminSum3 = $preTotalSumWithoutPaymentSystemFee3 * ($this->admin_cost / ONE_HUNDRED_PERCENT);
            $preAuthorSum3 = $preAuthorSum2;
        }
        if ($isAuthorSumOutOfRange2 && $isAdminSumOutOfRange2) {
            $preTotalSumWithoutPaymentSystemFee3 = $amountForComplete + $preAuthorSum2 + $preAdminSum2;
            $preTotalSum3 = $preTotalSumWithoutPaymentSystemFee3 / ((ONE_HUNDRED_PERCENT - plotGlobalConfig::getVar('courseCostPaymentSystemPercent')) / 100);
            $preAdminSum3 = $preAdminSum2;
            $preAuthorSum3 = $preAuthorSum2;
        }

        # 6. get last calculated values
        $totalSumWithoutPaymentSystemFee = $preTotalSumWithoutPaymentSystemFee1;
        if (isset($preTotalSumWithoutPaymentSystemFee2)) {
            $totalSumWithoutPaymentSystemFee = $preTotalSumWithoutPaymentSystemFee2;
        }
        if (isset($preTotalSumWithoutPaymentSystemFee3)) {
            $totalSumWithoutPaymentSystemFee = $preTotalSumWithoutPaymentSystemFee3;
        }
        $totalSum = $preTotalSum1;
        if (isset($preTotalSum2)) {
            $totalSum = $preTotalSum2;
        }
        if (isset($preTotalSum3)) {
            $totalSum = $preTotalSum3;
        }
        $adminSum = $preAdminSum1;
        if (isset($preAdminSum2)) {
            $adminSum = $preAdminSum2;
        }
        if (isset($preAdminSum3)) {
            $adminSum = $preAdminSum3;
        }
        $authorSum = $preAuthorSum1;
        if (isset($preAuthorSum2)) {
            $authorSum = $preAuthorSum2;
        }
        if (isset($preAuthorSum3)) {
            $authorSum = $preAuthorSum3;
        }

        # 7. round data and calculate using minus for no sum lost
        $data = array(
            'admin' => round($adminSum),
            'author' => round($authorSum),
            'paymentSystem' => round($totalSum) - round($authorSum) - round($adminSum) - round($amountForComplete),
            'total' => round($totalSum),
        );

        return $data;
    }

    public function isPublished()
    {
        $now = JFactory::getDate();
        $courseStartDate = JFactory::getDate($this->start_date);
        $courseEndDate = JFactory::getDate($this->end_date);

        $published = true;
        if (!$this->published || ($this->publish_start && $now < $courseStartDate) || ($this->publish_end && $now > $courseEndDate->modify('+1 day'))) {
            $published = false;
        }

        return $published;
    }

    public function getCountPushcases()
    {
        $db = JFactory::getDbo();
        $query = "SELECT COUNT(*) FROM `#__plot_courses_paid` WHERE `course_id`=" . $db->quote($this->id) . " AND `child_id` <> " . $db->quote($this->owner_id);
        $countPushcases = $db->setQuery($query)->loadResult();
        return $countPushcases;
    }

    public function getCountSuccesfullyStudied()
    {
        $db = JFactory::getDbo();
        $query = "SELECT COUNT(*) FROM `#__plot_courses_paid` WHERE `course_id`=" . $db->quote($this->id) . " AND `finished` = 1";
        $countPushcases = $db->setQuery($query)->loadResult();
        return $countPushcases;
    }

    static function getList()
    {
        $db = JFactory::getDbo();

        $query = "SELECT * FROM `#__lms_courses` WHERE published=1";
        $ages = $db->setQuery($query)->loadObjectList();
        return $ages;
    }

    public function coursePrograms()
    {

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('g.*, l.title AS level_title, p.link, m.`value` AS img')
            ->from('`#__plot_course_program_map` AS `b`')
            ->innerJoin('`#__social_clusters` AS `g` ON g.id=b.program_id')
            ->leftJoin('`#__plot_program` AS `p` ON `p`.`id`=`g`.id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON `l`.`id`=`p`.level')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`g`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`g`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")')
            ->where('`g`.`cluster_type`="group"')
            ->where("`b`.`course_id` = " . (int)$this->id)
            ->group('g.id');
        $programs = $db->setQuery($query)->loadObjectList();
        foreach ($programs AS $row) {
            $group = FD::group($row->id);
            $row->link = FRoute::groups(array('layout' => 'item', 'id' => $group->getAlias()), false) . '?plot=1';
            $thumbnailPath = JPATH_SITE . '/' . $row->img;
            if ($row->img == "" || !is_file($thumbnailPath) || !$row->img) {
                $row->img = JURI::root() . "/media/com_easysocial/defaults/avatars/group/square.png";
            } else {
                $row->img = JURI::root() . $row->img;
            }
        }
        return $programs;
    }

    public function getCourseProgramsIds()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('b.program_id')
            ->from('`#__plot_course_program_map` AS `b`')
            ->where("`b`.`course_id` = " . (int)$this->c_id);
        $programs = $db->setQuery($query)->loadColumn();
        return $programs;
    }

    public function isGetPages(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('b.*')
            ->from('`#__html5fb_pages` AS `b`')
            ->where("`b`.`publication_id` = ".(int)$this->id_pub);
        $pages = $db->setQuery($query)->loadResult();

        return $pages;
    }



    public function isSmall(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('b.small')
            ->from('`#__plot_courses` AS `b`')
            ->where("`b`.`id` = ".(int)$this->id);
        $small = (int)$db->setQuery($query)->loadResult();
        return $small;
    }

    public function whoBoughtCourse($userId){

        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`p`.parent_id')
            ->from('`#__plot_courses_paid` AS `p`')
            ->where('`p`.`course_id` = ' . (int)$this->id)
            ->where('`p`.`child_id`='.(int)$userId);

        $buyer = $db->setQuery($query)->loadResult();
        return $buyer;
    }

}
