<?php

class plotCoursePaid
{

    public $id = 0;

    public function __construct($id = 0)
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select("*")
                ->from("`#__plot_courses_paid` AS `course_paid`")
                ->where("`id` = ".$db->quote($id));

        $coursePaid = $db->setQuery($query)->loadObject();

        if ($coursePaid) {
            foreach ($coursePaid AS $fName => $fValue) {
                $this->$fName = $fValue;
            }
        }
        
        return $coursePaid;
    }

    public function __get($name)
    {
        return false;
    }

}
