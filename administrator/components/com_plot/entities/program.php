<?php

class plotProgram
{

    public $id = 0;

    public function __construct($id = 0)
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('g.*, l.title AS level_title, p.link,  m.`value` AS img')
            ->from('`#__social_clusters` AS `g`')
            ->leftJoin('`#__plot_program` AS `p` ON `p`.`id`=`g`.id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON `l`.`id`=`p`.level')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`g`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`g`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")')
            ->where('`g`.`cluster_type`="group"')
            ->where("`g`.`id` = " . (int)$id)
            ->group('g.id');

        $book = $db->setQuery($query)->loadObject();
        if ($book) {
            $this->id = $book->id;
            foreach ($book AS $i => $value) {
                $this->$i = $value;
            }
        } else {
            $this->id = $this->id;
        }


    }

    public function getProgramAgesTitle()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('at.title');
        $query->from('`#__plot_age_program_map` AS a');
        $query->leftJoin('`#__plot_ages` AS at ON at.id=a.age_id');
        $query->where('a.program_id=' . $this->id);
        $db->setQuery($query);
        $rows = $db->loadColumn();

        return $rows;
    }

    public function __get($name)
    {
        return false;
    }

    public function isProgramExist($id)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('m.id ');
        $query->from('`#__plot_program` AS m');
        $query->where('m.`id` = ' . (int)$id);
        $db->setQuery($query);
        return $db->loadResult();
    }

    public function updateAges($ages)
    {
        plotAges::remove('program', $this->id);
        foreach ($ages AS $ageId) {
            plotAges::add('program', $this->id, $ageId);
        }
    }

    public function getPrograms()
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('g.*, l.title AS level_title, p.link, m.`value` AS img')
            ->from('`#__plot_programs_program_map` AS `b`')
            ->innerJoin('`#__social_clusters` AS `g` ON g.id=b.program_id')
            ->leftJoin('`#__plot_program` AS `p` ON `p`.`id`=`g`.id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON `l`.`id`=`p`.level')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`g`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`g`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")')
            ->where('`g`.`cluster_type`="group"')
            ->where("`b`.`program_id` = " . (int)$this->id)
            ->group('g.id');

        $programs = $db->setQuery($query)->loadObjectList();
        foreach ($programs AS $row) {
            $group = FD::group($row->id);
            $row->link = FRoute::groups(array('layout' => 'item', 'id' => $group->getAlias()), false) . '?plot=1';
            $thumbnailPath = JPATH_SITE . '/' . $row->img;
            if ($row->img == "" || !is_file($thumbnailPath) || !$row->img) {
                $row->img = JURI::root() . "/media/com_easysocial/defaults/avatars/group/square.png";
            } else {
                $row->img = JURI::root() . $row->img;
            }
        }
        return $programs;

    }

    public function getAllLevels(){
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('b.*')
            ->from('`#__plot_program_levels` AS `b`');
        return $db->setQuery($query)->loadObjectList();
    }

    static function getList()
    {
        $db = JFactory::getDbo();

        $query = "SELECT * FROM `#__social_clusters` WHERE `cluster_type`='group' AND state=1 ";
        $ages = $db->setQuery($query)->loadObjectList();
        return $ages;
    }

    public function getAllCoursesByProgramId()
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('p.course_id')
            ->from('`#__plot_course_program_map` AS `p`')
            ->where("`p`.`program_id` = " . (int)$this->id);
        return $db->setQuery($query)->loadColumn();
    }

    public function getAllBooksByProgramId()
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('p.book_id')
            ->from('`#__plot_books_program_map` AS `p`')
            ->where("`p`.`program_id` = " . (int)$this->id);
        return $db->setQuery($query)->loadColumn();
    }

    public function getAllCatigories()
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('p.*')
            ->from('`#__social_clusters_categories` AS `p`')
            ->where("`p`.`state` = 1")
            ->where('`p`.`type`="group"');
        $result=$db->setQuery($query)->loadObjectList();
        foreach($result AS $item){
            $item->count_cat=$this->getCountGroupsInCatsPrograms($item->id);
        }
        return $result;
    }

    public function setFinished($id, $uid)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`c`.id')
            ->from('`#__plot_program` AS `p`')
            ->leftJoin('`#__social_clusters_nodes` AS c ON c.id=p.id')
            ->where('c.type="user"')
            ->where('c.uid=' . $uid)
            ->where('p.id=' . (int)$id);
        $node_id = (int)$db->setQuery($query)->loadResult();
        $this->isExistProgramUserNode($uid);
        $obj = new stdClass();
        $obj->status = 1;
        $obj->cluster_node_id = $node_id;
        $db->updateObject('#__plot_clusrers_node_map', $obj, 'cluster_node_id');
        $this->generateCertificate($id, $uid);
        plotPoints::assign('finished.program', 'com_plot', $uid, $id);
    }


    public function generateCertificate($id, $uid){


            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_plot/programs')) {

                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_plot/programs');

            }

            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_plot/programs/'.$uid)) {

                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_plot/programs/' . $uid);
            }

            $program=new plotProgram($id);
            $user=plotUser::factory($uid);
            $new_img_name = md5(time() . $uid);
            $text='"'.$program->title.'"';
            $im = imagecreatefromjpeg(plotGlobalConfig::getVar('programPicture'));
            // Allocate A Color For The Text
        $green=imagecolorallocate($im, 7, 178, 76);
        $light_green=imagecolorallocate($im, 113, 172, 152);
        $brown=imagecolorallocate($im, 114, 80, 45);
        $cert_date=date("d.m.Y");
            // Set Path to Font File
            $font_path = 'media/tahoma.ttf';
            // Print Text On Image
        $program_lenth=strlen($text);

        imagettftext($im, 55, 0, 450, 1530, $green, $font_path, $user->name);
        if($program_lenth<=50){
            imagettftext($im, 45, 0, 850, 2200, $light_green, $font_path, $text);
        }else{
            imagettftext($im, 45, 0, 650, 2200, $light_green, $font_path, $text);
        }
        imagettftext($im, 45, 0, 300, 2700, $brown, $font_path, $cert_date);

            // imagettftext($im, 25, 0, 180, 720, $white, $font_path, $user_score);
            imagepng($im, 'media/com_plot/programs/'  .$uid.'/'.  $new_img_name . '.png');
            imagedestroy($im);

        $this->insertEssayInSocial($new_img_name . '.png', $id, $uid);
            //return $new_img_name . '.png';

    }


    private function insertEssayInSocial($img, $exist, $uid)
    {

        $db = Foundry::db();
        $my = plotUser::factory( $uid);
        $query = $db->getQuery(true)
            ->select('`a`.*')
            ->from('`#__social_albums` AS `a`')
            ->where('`a`.`uid` = ' . (int)$my->id)
            ->where('`a`.`type` = "plot-programs"');
        $db->setQuery($query);
        $album = $db->loadObject();

        if (!$album) {
            $album = Foundry::table('Album');
            $album->uid = $my->id;
            $album->type = 'plot-programs';
            $album->created = Foundry::date()->toMySQL();
            $album->ordering = 0;
            $album->assigned_date = Foundry::date()->toMySQL();
            $db->insertObject('#__social_albums', $album);
            $albumId = $db->insertid();
        } else {
            $albumId = $album->id;
        }

        $photo = new stdClass();
        $photo->uid = $my->id;
        $photo->type = 'user';
        $photo->album_id = $albumId;
        $photo->title = JText::_('COM_PLOT_PROGRAM_FINISHED');
        $photo->caption = JText::_('COM_PLOT_PROGRAM_FINISHED');
        $photo->created = Foundry::date()->toMySQL();
        $photo->assigned_date = Foundry::date()->toMySQL();
        $photo->featured = 0;
        $photo->state = 0;
        $photo->storage = 'joomla';
        $photo->ordering = 0;
        $db->insertObject('#__social_photos', $photo);
        $photoId = $db->insertid();

        $meta = new stdClass();
        $meta->photo_id = $photoId;
        $meta->group = 'path';
        $meta->property = 'thumbnail';
        $meta->value = JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_plot/programs/' .$uid.'/'.  $img;

        $db->insertObject('#__social_photos_meta', $meta);
        $metaid = $db->insertid();

        $this->thumbnailCreate(JPATH_BASE . '/media/com_plot/programs/' .$uid.'/'.  $img, JPATH_BASE . '/media/com_plot/programs/' .$uid.'/thumbnail_'.  $img);

    }

    public function thumbnailCreate($url, $filename) {
        require_once JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/image/image.php';
        $image = new SocialImage();
        $image = $image->load($url);
        $tmpImagePath = $filename;
        $image->resize(plotGlobalConfig::getVar('photoCropWidthMin'), plotGlobalConfig::getVar('photoCropHeightMin'));
        $image->save($tmpImagePath);
    }


    public function isPssedAllCourses($uid)
    {
        $courses = $this->getAllCoursesByProgramId();
        $finished = true;
        $my = plotUser::factory($uid);
        if ($courses) {
            foreach ($courses AS $course) {
                if (!$my->getFinishedCourseByCourseId($course)) {
                    $finished = false;
                }

            }
            return $finished;
        }
        return false;

    }

    public function isReadedAllBooks($uid)
    {
        $books = $this->getAllBooksByProgramId();

        $finished = true;
        $my = plotUser::factory($uid);
        if ($books) {
            foreach ($books AS $book) {
                if (!$my->getFinishedBookByBookId($book)) {
                    $finished = false;
                }
            }
            return $finished;

        }
        return false;
    }

    public function isProgramFinished($uid){

        $this->isExistProgramUserNode($uid);
        if($this->isPssedAllCourses($uid) && $this->isReadedAllBooks($uid)){
            $this->setFinished($this->id, $uid);
            return true;
        }
        return false;
    }

    public function isExistProgramUserNode($uid=0){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`c`.id')
            ->from('`#__social_clusters_nodes` AS c ')
            ->where('c.type="user"')
            ->where('c.uid=' . $uid)
            ->where('c.cluster_id=' . (int)$this->id);
        $node_id = (int)$db->setQuery($query)->loadResult();

        if($node_id){
            $query = $db->getQuery(true)
                ->select('`c`.*')
                ->from('`#__plot_clusrers_node_map` AS c ')
                ->where('c.cluster_node_id='.(int)$node_id);
            $node_obj = $db->setQuery($query)->loadObject();

            if(!$node_obj){
                $obj = new stdClass();
                $obj->status = 0;
                $obj->cluster_node_id = $node_id;
                $db->insertObject('#__plot_clusrers_node_map', $obj, 'cluster_node_id');
            }
        }

    }

    public function getCountCourses(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('`lc`.`id`')
            ->from('#__plot_course_program_map AS c')
            ->leftJoin('`#__lms_courses` AS `lc` ON lc.id=c.course_id')
            ->leftJoin("#__plot_courses AS `pc` ON (`pc`.`id` = `lc`.`id`)")
            ->where('c.program_id=' . $db->quote((int)$this->id));
       $res=$db->setQuery($query)->loadColumn();
        if($res){
            return count($res);
        }else{
            return 0;
        }

    }

    public function getCountBooks(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('p.c_id')
            ->from('#__plot_books_program_map AS c')
            ->leftJoin('`#__html5fb_publication` AS `p` ON p.c_id=c.book_id')
            ->leftJoin('`#__plot_book_cost` AS `b` ON b.id=p.c_id')
            ->where('c.program_id=' . $db->quote((int)$this->id));
        $res=$db->setQuery($query)->loadColumn();
        if($res){
            return count($res);
        }else{
            return 0;
        }

    }

    public function getCountSubPrograms(){
        $db = JFactory::getDbo();
        $my = plotUser::factory();
        $query = $db->getQuery(true);
        $query->select('p.id')
            ->from('#__plot_programs_program_map AS c')
            ->leftJoin('`#__social_clusters` AS `p` ON p.id=c.item_id')
            ->leftJoin('`#__plot_program` AS `pr` ON pr.id=c.item_id')
            ->leftJoin('`#__plot_age_program_map` AS age ON age.program_id=p.id')
            ->leftJoin('`#__plot_ages` AS ages ON ages.id=age.age_id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON l.id=pr.level')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`p`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`p`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")');
        if (!$my->isAvaliableProgram()) {
            $query->where('pr.level!=' . plotGlobalConfig::getVar("programMageLevelId"));
        }
        $query->where('c.program_id=' . $db->quote((int)$this->id))
            ->group('c.item_id');
        $res=$db->setQuery($query)->loadColumn();
        if($res){
            return count($res);
        }else{
            return 0;
        }

    }

    public function getCountAllPrograms(){
        $db = JFactory::getDbo();
        $my = plotUser::factory();
        $query = $db->getQuery(true);
        $query->select('p.id')
            ->from('#__plot_program AS c')
            ->leftJoin('`#__social_clusters` AS `p` ON c.id=p.id')
            ->leftJoin('`#__social_clusters_categories` AS `cats` ON cats.id=p.category_id')
            ->leftJoin('`#__plot_age_program_map` AS age ON age.program_id=p.id')
            ->leftJoin('`#__plot_ages` AS ages ON ages.id=age.age_id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON l.id=c.level')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`p`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`p`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")');
        if (!$my->isAvaliableProgram()) {
            $query->where('c.level!=' . plotGlobalConfig::getVar("programMageLevelId"));
        }
        $query->group('c.id');
        $res=$db->setQuery($query)->loadColumn();
        if($res){
            return count($res);
        }else{
            return 0;
        }

    }

    public function getCountFeaturedPrograms(){
        $db = JFactory::getDbo();
        $my = plotUser::factory();
        $query = $db->getQuery(true);
        $query->select('p.id')
            ->from('#__plot_program AS c')
            ->leftJoin('`#__social_clusters` AS `p` ON c.id=p.id')
            ->leftJoin('`#__social_clusters_categories` AS `cats` ON cats.id=p.category_id')
            ->leftJoin('`#__plot_age_program_map` AS age ON age.program_id=p.id')
            ->leftJoin('`#__plot_ages` AS ages ON ages.id=age.age_id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON l.id=c.level')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`p`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`p`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")')
            ->where('p.featured=1');
        if (!$my->isAvaliableProgram()) {
            $query->where('c.level!=' . plotGlobalConfig::getVar("programMageLevelId"));
        }
        $query->group('c.id');
        $res=$db->setQuery($query)->loadColumn();
        if($res){
            return count($res);
        }else{
            return 0;
        }

    }

    public function getCountGroupsInCatsPrograms($id){
        $db = JFactory::getDbo();
        $my = plotUser::factory();
        $query = $db->getQuery(true);
        $query->select('p.id')
            ->from('#__plot_program AS c')
            ->leftJoin('`#__social_clusters` AS `p` ON c.id=p.id')
            ->leftJoin('`#__social_clusters_categories` AS `cats` ON cats.id=p.category_id')
            ->leftJoin('`#__plot_age_program_map` AS age ON age.program_id=p.id')
            ->leftJoin('`#__plot_ages` AS ages ON ages.id=age.age_id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON l.id=c.level')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`p`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`p`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")');
        if (!$my->isAvaliableProgram()) {
            $query->where('c.level!=' . plotGlobalConfig::getVar("programMageLevelId"));
        }
        $query->where('p.category_id=' . (int)$id)
            ->group('c.id');
        $res=$db->setQuery($query)->loadColumn();
        if($res){
            return count($res);
        }else{
            return 0;
        }

    }


}
