<?php

class plotPoints
{

    static function updateInsert($countPoints, $entity, $entityId)
    {
        $db = JFactory::getDbo();
        $isPointExist = self::checkIsPointExist($entity, $entityId);
        if (!$isPointExist) {
            $query = "INSERT INTO `#__plot_count_points` SET `count_points` = ".(int) $countPoints.", `entity` = ".$db->quote($entity).", `entity_id` = ".(int) $entityId;
            return $db->setQuery($query)->query();
        } else {
            $query = "UPDATE `#__plot_count_points` SET `count_points` = ".(int) $countPoints." WHERE `entity` = ".$db->quote($entity)." AND `entity_id` = ".(int) $entityId;
            return $db->setQuery($query)->query();
        }
        return false;
    }

    static function get($entity = '', $entityId = '')
    {
        $db = JFactory::getDbo();
        $whereParts = array();
        if ($entity) {
            $whereParts[] = "`t`.`entity` = ".$db->quote($entity);
        }
        if ($entityId) {
            $whereParts[] = "`t`.`entity_id` = ".(int) $entityId;
        }
        $whereString = '';
        if ($whereParts) {
            $whereString = 'WHERE '.implode(' AND ', $whereParts);
        }

        $query = "SELECT `count_points` FROM `#__plot_count_points` $whereString";
        $points = ($entity && $entityId) ? $db->setQuery($query)->loadObject() : $db->setQuery($query)->loadObjectList();
        return $points;
    }

    static function checkIsPointExist($entity, $entityId)
    {
        $db = JFactory::getDbo();
        $query = "SELECT `id` FROM `#__plot_count_points` WHERE `entity` = ".$db->quote($entity)." AND `entity_id` = ".(int) $entityId;
        $result = $db->setQuery($query)->loadResult();
        return $result ? true : false;
    }

    static function assign( $command , $extension , $userId, $entityId = 0 )
    {
        $config = Foundry::config();
        // Check if points system is enabled.
        if (!$config->get('points.enabled')) {
            return false;
        }

        // Retrieve the points table.
        $points = Foundry::table('Points');
        $state = $points->load(array('command' => $command, 'extension' => $extension));

        // Check the command and extension and see if it is valid.
        if (!$state) {
            // If it doesn't exist, just throw an error.
            Foundry::logError(__FILE__, __LINE__, 'POINTS: Command and extension '.$command.','.$extension.' not found.');
            return false;
        }

        // Check the rule and see if it is published.
        if ($points->state != SOCIAL_STATE_PUBLISHED) {
            // If points is unpublished, throw an error.
            Foundry::logError(__FILE__, __LINE__, 'POINTS: Command and extension '.$command.','.$extension.' unpublished.');
            return false;
        }

        // Add history.
        #$socialHistory = Foundry::table('PointsHistory');
        $socialHistory = new stdClass();
        $socialHistory->points_id = $points->id;
        $socialHistory->user_id = $userId;
        $socialHistory->points = $points->points;
        $socialHistory->created = JFactory::getDate()->toSql();
        $socialHistory->state = SOCIAL_STATE_PUBLISHED;
        $socialHistory->message = '';

        $db = JFactory::getDbo();
        $db->insertObject('#__social_points_history', $socialHistory);
        $socialHistoryId = $db->insertid();

        $plotHistory = new stdClass();
        $plotHistory->sph_id = $socialHistoryId;
        $plotHistory->entity_id = $entityId;
        $db->insertObject('#__plot_points_history', $plotHistory);

        $user = Foundry::user($userId);
        $user->addPoints($points->points);

        return true;
    }


    static function getPointsByCommand($command){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`p`.points')
            ->from('`#__social_points` AS `p`')
            ->where('`p`.`command` = ' .$db->quote($command));

        $points = $db->setQuery($query)->loadResult();

        return (int)$points;
    }


}
