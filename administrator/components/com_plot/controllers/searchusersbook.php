<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class PlotControllerSearchUsersBook extends JControllerForm
{

    public function showPopup()
    {

        $view = $this->getView('SearchUsersBook', 'raw');
        $view->display();
        die;
    }

    
    public function ajaxGetUsersList()
    {

        $model = JModelLegacy::getInstance('searchUsers', 'plotModel');

        $view = $this->getView('SearchUsersBook', 'raw');
        $view->setLayout('users.list');
        
        $view->c_id = JRequest::getInt('c_id', '0');
        
        $filter = array();
        $searchValue = JRequest::getVar('searchValue', '');
        $page = JRequest::getVar('page', '0');
        
        if ($searchValue) {
            $filter[] = array('field' => 'u.name', 'type' => 'like', 'value' => $searchValue);
        }
        $view->users = $model->getUsers( $filter, 'u.name ASC', $limit = array('limitstart' => ($page - 1) * plotGlobalConfig::getVar('usersCountSearchForBuyBook'), 'limit' => plotGlobalConfig::getVar('usersCountSearchForBuyBook')) );
        $view->totalUser = $model->totalUsers;
        
        $view->pagination = new JPagination( $view->totalUser, ($page - 1) * plotGlobalConfig::getVar('usersCountSearchForBuyBook'), plotGlobalConfig::getVar('usersCountSearchForBuyBook') );

        $result = $view->loadTemplate();
        if ($result instanceof Exception) {
            return $result;
        }
        
        $data['html'] = $result;

        echo json_encode($data);
        die;
    }


}
