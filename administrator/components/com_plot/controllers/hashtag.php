<?php defined('_JEXEC') or die('Restricted access');


class PlotControllerHashtag extends JControllerForm
{
    //----------------------------------------------------------------------------------------------------
    public function add()
    {
        parent::add();
    }

    //----------------------------------------------------------------------------------------------------
    public function save($key = null, $urlVar = null)
    {
        $jinput = JFactory::getApplication()->input;
        $task = $jinput->get('task', '');
        if ($task == 'cancel') {
            $this->setRedirect(JRoute::_('index.php?option=com_plot&view=hashtags', false));
            return;
        }
        parent::save($key, $urlVar);
    }

    public function delete()
    {

        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Get items to remove from the request.
        $cid = $this->input->get('cid', array(), 'array');

        if (!is_array($cid) || count($cid) < 1) {
            JError::raiseWarning(500, JText::_($this->text_prefix . '_NO_ITEM_SELECTED'));
        } else {
            // Get the model.
            $model = $this->getModel();

            // Remove the items.
            if ($model->delete($cid)) {
                $this->setMessage(JText::plural($this->text_prefix . '_N_ITEMS_DELETED', count($cid)));
            } else {
                $this->setMessage($model->getError());
            }
        }

        $this->setRedirect(JRoute::_('index.php?option=com_plot&view=hashtags', false));
    }


}