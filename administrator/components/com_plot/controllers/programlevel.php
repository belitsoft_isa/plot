<?php defined('_JEXEC') or die('Restricted access');


class PlotControllerProgramlevel extends JControllerForm
{
	//----------------------------------------------------------------------------------------------------
	public function add()
	{
		parent::add();
	}
	//----------------------------------------------------------------------------------------------------
	public function save($key = null, $urlVar = null)
	{

        $jform=$this->input->get('jform', array(), 'array');
        $id=$this->input->get('id',0, 'INT');
        $task=$this->input->get('task');

        if($task=='cancel'){
            $this->setRedirect(JRoute::_('index.php?option=com_plot&view=programlevels', false));
            return;
        }
        if($jform['level']<1){
            JError::raiseWarning(500, JText::_('COM_PLOT_FIELD_VALUE_LESS_THEN_ZERO'));
            $this->setRedirect(JRoute::_('index.php?option=com_plot&view=programlevel&layout=edit&id='.(int)$id, false));
            return;
        }

        $model=$this->getModel();


       if($model->checkLevel($id,$jform['level'] )){
           $this->setRedirect(JRoute::_('index.php?option=com_plot&view=programlevel&layout=edit&id='.(int)$id, false));
           return;
       }else{

           parent::save($key, $urlVar);

       }






	}
    public function delete() {

        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Get items to remove from the request.
        $cid = $this->input->get('cid', array(), 'array');

        if (!is_array($cid) || count($cid) < 1)
        {
            JError::raiseWarning(500, JText::_($this->text_prefix . '_NO_ITEM_SELECTED'));
        }
        else
        {

            // Get the model.
            $model = $this->getModel();
            foreach($cid AS $id){
                $exist=$model->isProgramExistWithLevelExist($id);
                if(!empty($exist)){
                    JError::raiseWarning(500, JText::_('Уровень используется в программах'));
                    $this->setRedirect(JRoute::_('index.php?option=com_plot&view=programlevels', false));
                    return;
                }
            }

            if(in_array((int)plotGlobalConfig::getVar('programMasterLevelId'), $cid) || in_array((int)plotGlobalConfig::getVar('programMageLevelId'), $cid)){
                JError::raiseWarning(500, JText::_('Измените в конфигурационном файле id для уровня мастер или для уровня мудрец'));
                $this->setRedirect(JRoute::_('index.php?option=com_plot&view=programlevels', false));
                return;
            }
            // Remove the items.
            if ($model->delete($cid))
            {
                $this->setMessage(JText::plural($this->text_prefix . '_N_ITEMS_DELETED', count($cid)));
            }
            else
            {
                $this->setMessage($model->getError());
            }
        }

        $this->setRedirect(JRoute::_('index.php?option=com_plot&view=programlevels', false));
    }





}