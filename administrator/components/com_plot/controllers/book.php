<?php defined('_JEXEC') or die('Restricted access');


class PlotControllerBook extends JControllerForm
{
    //----------------------------------------------------------------------------------------------------
    public function add()
    {
        parent::add();
    }

    //----------------------------------------------------------------------------------------------------
    public function save($key = null, $urlVar = null)
    {
        $model = $this->getModel();
        $jinput = JFactory::getApplication()->input;

        $task = $jinput->get('task', '');
        $id = $jinput->get('c_id', 0, 'INT');
        $jform = $jinput->get('jform', array(), 'ARRAY');
        $ages = $jform['ages'];
        $tags = $jform['tags'];
        $url = trim($jform['url']);
        $hashtags = $jform['hashtags'];
        $beforehashtags = $jform['beforehashtags'];
        $afterhashtags = $jform['afterhashtags'];

        $file = JRequest::getVar('jform', '', 'FILES');
        if ($task == 'cancel') {
            $this->setRedirect(JRoute::_('index.php?option=com_plot&view=books', false));
            return;
        }
        if (empty($ages)) {

            JError::raiseWarning(500, JText::_('COM_PLOT_SELECTED_AGE'));
            $this->setRedirect(JRoute::_('index.php?option=com_plot&view=book&layout=edit&c_id=' . (int)$jform['c_id'], false));
            return;
        } elseif (empty($tags)) { $model->saveTags($tags);
            JError::raiseWarning(500, JText::_('COM_PLOT_SELECTED_AGE'));
            $this->setRedirect(JRoute::_('index.php?option=com_plot&view=book&layout=edit&c_id=' . (int)$jform['c_id'], false));
            return;
        } else {

            $model->saveAge($ages);
            $model->saveTags($tags);
            $model->saveQuiz();
            $model->saveCost();
            $model->savePoints();
            //$model->saveHashtags($hashtags, '#__plot_hashtag_book_map');
            //$model->saveHashtags($beforehashtags, '#__plot_before_hashtag_book_map');
            //$model->saveHashtags($afterhashtags, '#__plot_after_hashtag_book_map');
            if ($url) {
                if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
                    JError::raiseWarning(500, JText::_('COM_PLOT_INFALID_URL'));
                    $this->setRedirect(JRoute::_('index.php?option=com_plot&view=book&layout=edit&c_id=' . (int)$jform['c_id'], false));
                } else {
                    $model->addBookLink();
                }
            }


            if ($file['tmp_name']['pdffiles']) {
                $ext = JFile::getExt($file['name']['pdffiles']);

                if ($ext != 'pdf') {
                    JError::raiseWarning(500, JText::_('COM_PLOT_INVALID_FORMAT'));
                    $this->setRedirect(JRoute::_('index.php?option=com_plot&view=book&layout=edit&c_id=' . (int)$jform['c_id'], false));
                } else {
                    $model->saveBookFiles('pdf');
                }
            }
            if ($file['tmp_name']['mobifiles']) {
                $ext = JFile::getExt($file['name']['mobifiles']);
                if ($ext != 'mobi') {
                    JError::raiseWarning(500, JText::_('COM_PLOT_INVALID_FORMAT'));
                    $this->setRedirect(JRoute::_('index.php?option=com_plot&view=book&layout=edit&c_id=' . (int)$jform['c_id'], false));
                } else {
                    $model->saveBookFiles('mobi');
                }
            }
            if ($file['tmp_name']['fb2files']) {
                $ext = JFile::getExt($file['name']['fb2files']);
                if ($ext != 'fb2') {
                    JError::raiseWarning(500, JText::_('COM_PLOT_INVALID_FORMAT'));
                    $this->setRedirect(JRoute::_('index.php?option=com_plot&view=book&layout=edit&c_id=' . (int)$jform['c_id'], false));
                } else {
                    $model->saveBookFiles('fb2');
                }
            }
            if ($file['tmp_name']['epubfiles']) {
                $ext = JFile::getExt($file['name']['epubfiles']);
                if ($ext != 'epub') {
                    JError::raiseWarning(500, JText::_('COM_PLOT_INVALID_FORMAT'));
                    $this->setRedirect(JRoute::_('index.php?option=com_plot&view=book&layout=edit&c_id=' . (int)$jform['c_id'], false));
                } else {
                    $model->saveBookFiles('epub');
                }
            }
            if ($file['tmp_name']['audiofiles']) {
                $allowedVideoTypes = explode('|', plotGlobalConfig::getVar('bookAudioFilesType'));
                if ($file['type']['audiofiles']) {
                    list($type, $format) = explode('/', $file['type']['audiofiles']);
                } else {
                    list($type, $format) = array('', '');
                }
                if (!in_array($format, $allowedVideoTypes)) {
                    JError::raiseWarning(500, JText::_('COM_PLOT_INVALID_FORMAT'));
                    $this->setRedirect(JRoute::_('index.php?option=com_plot&view=book&layout=edit&c_id=' . (int)$jform['c_id'], false));
                } else {
                    $model->saveBookFiles('audio');
                }

            }

            switch ($task) {
                case "apply":
                    $this->setRedirect(JRoute::_('index.php?option=com_plot&view=book&layout=edit&c_id=' . (int)$jform['c_id'], false));
                    break;
                case "save":
                    $this->setRedirect(JRoute::_('index.php?option=com_plot&view=books', false));
                    break;
            }

        }


    }


}