<?php defined('_JEXEC') or die('Restricted access');


class PlotControllerLevel extends JControllerForm
{
	//----------------------------------------------------------------------------------------------------
	public function add()
	{
		parent::add();
	}
	//----------------------------------------------------------------------------------------------------
	public function save($key = null, $urlVar = null)
	{

        $jform=$this->input->get('jform', array(), 'array');
        $id=$this->input->get('id',0, 'INT');
        $task=$this->input->get('task');

        if($task=='cancel'){
            $this->setRedirect(JRoute::_('index.php?option=com_plot&view=levels', false));
            return;
        }
        if($jform['points']<1){
            JError::raiseWarning(500, JText::_('COM_PLOT'));
            $this->setRedirect(JRoute::_('index.php?option=com_plot&view=level&layout=edit&id='.(int)$id, false));
            return;
        }
        $model=$this->getModel();

       if(!$model->checkLevel($id,$jform['points'] )){
           $this->setRedirect(JRoute::_('index.php?option=com_plot&view=level&layout=edit&id='.(int)$id, false));
           return;
       }else{

           parent::save($key, $urlVar);
           $model=$this->getModel();
           $model->saveImages();

       }






	}
    public function delete() {

        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Get items to remove from the request.
        $cid = $this->input->get('cid', array(), 'array');

        if (!is_array($cid) || count($cid) < 1)
        {
            JError::raiseWarning(500, JText::_($this->text_prefix . '_NO_ITEM_SELECTED'));
        }
        else
        {
            // Get the model.
            $model = $this->getModel();

            // Remove the items.
            if ($model->delete($cid))
            {
                $this->setMessage(JText::plural($this->text_prefix . '_N_ITEMS_DELETED', count($cid)));
            }
            else
            {
                $this->setMessage($model->getError());
            }
        }

        $this->setRedirect(JRoute::_('index.php?option=com_plot&view=levels', false));
    }

    public function deleteImg()
    {
        $path = $this->input->get('path', '', 'STRING');
        $type=$this->input->get('typeimg', '');

        $model = $this->getModel();
        if( $model->deleteImg($path,$type)){
            echo true;
        }else{
            echo false;
        }
        die;
    }

}