<?php defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class PlotControllerDev extends JControllerAdmin
{


    public function dev()
    {

     /* $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`a`.*')
            ->from('`#__social_fields_data2` AS `a`')
        ->where('`a`.data!=""');
        $db->setQuery($query);

        $ages = $db->loadObjectList();
        $data_prepare=array();

        foreach($ages AS $a){
            if($a->data){
                $params=json_decode($a->data, true);

                if(is_array($params)){
                    foreach($params AS $key=>$value){
                        $fieldData = new stdClass();
                        $fieldData->field_id=$a->field_id;
                        $fieldData->uid=$a->uid;
                        $fieldData->type=$a->type;
                        $fieldData->datakey=$key;
                        $fieldData->data=$value;
                        $fieldData->params=$params;
                        $fieldData->raw=$a->raw? $a->raw :'';
                        $data_prepare[]=$fieldData;
                        $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
                    }
                }else{
                    if($a->field_id && $a->uid){
                        switch ($a->field_id) {
                            case 7:
                                $fieldData = new stdClass();
                                $fieldData->field_id=$a->field_id;
                                $fieldData->uid=$a->uid;
                                $fieldData->type=$a->type;
                                $fieldData->datakey='JOOMLA_TIMEZONE';
                                $fieldData->data=$a->data;
                                $fieldData->params=$params;
                                $fieldData->raw=$a->raw;
                                $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
                                break;
                            case 8:
                                $fieldData = new stdClass();
                                $fieldData->field_id=$a->field_id;
                                $fieldData->uid=$a->uid;
                                $fieldData->type=$a->type;
                                $fieldData->datakey='PERMALINK';
                                $fieldData->data=$a->data;
                                $fieldData->params=$params;
                                $fieldData->raw=$a->raw;
                                $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
                                break;
                            case 10:
                                $fieldData = new stdClass();
                                $fieldData->field_id=$a->field_id;
                                $fieldData->uid=$a->uid;
                                $fieldData->type=$a->type;
                                $fieldData->datakey='GENDER';
                                $fieldData->data=$a->data;
                                $fieldData->params=$params;
                                $fieldData->raw=$a->raw;
                                $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
                                break;
                            case 11:
                            $fieldData = new stdClass();
                            $fieldData->field_id=$a->field_id;
                            $fieldData->uid=$a->uid;
                            $fieldData->type=$a->type;
                            $fieldData->datakey='BIRTHDAY';
                            $fieldData->data=$a->data;
                            $fieldData->params=$params;
                            $fieldData->raw=$a->raw;
                                $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
                            break;
                            case 30:
                                $fieldData = new stdClass();
                                $fieldData->field_id=$a->field_id;
                                $fieldData->uid=$a->uid;
                                $fieldData->type=$a->type;
                                $fieldData->datakey='HEADER-1';
                                $fieldData->data=$a->data;
                                $fieldData->params=$params;
                                $fieldData->raw=$a->raw;
                                $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
                                break;
                            case 33:
                                $fieldData = new stdClass();
                                $fieldData->field_id=$a->field_id;
                                $fieldData->uid=$a->uid;
                                $fieldData->type=$a->type;
                                $fieldData->datakey='HEADER';
                                $fieldData->data=$a->data;
                                $fieldData->params=$params;
                                $fieldData->raw=$a->raw;
                                $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
                                break;
                            case 50:
                                $fieldData = new stdClass();
                                $fieldData->field_id=$a->field_id;
                                $fieldData->uid=$a->uid;
                                $fieldData->type=$a->type;
                                $fieldData->datakey='URL';
                                $fieldData->data=$a->data;
                                $fieldData->params=$params;
                                $fieldData->raw=$a->raw;
                                $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
                                break;
                            case 53:
                                $fieldData = new stdClass();
                                $fieldData->field_id=129;
                                $fieldData->uid=$a->uid;
                                $fieldData->type=$a->type;
                                $fieldData->datakey='phone';
                                $fieldData->data=$a->data;
                                $fieldData->params=$params;
                                $fieldData->raw=$a->raw;
                                $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
                                break;
                            case 61:
                                $fieldData = new stdClass();
                                $fieldData->field_id=128;
                                $fieldData->uid=$a->uid;
                                $fieldData->type=$a->type;
                                $fieldData->datakey='STATUS';
                                $fieldData->data=$a->data;
                                $fieldData->params=$params;
                                $fieldData->raw=$a->raw;
                                $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
                                break;
                            case 82:
                                $fieldData = new stdClass();
                                $fieldData->field_id=125;
                                $fieldData->uid=$a->uid;
                                $fieldData->type=$a->type;
                                $fieldData->datakey='SCHOOL';
                                $fieldData->data=$a->data;
                                $fieldData->params=$params;
                                $fieldData->raw=$a->raw;
                                $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
                                break;
                            case 83:
                                $fieldData = new stdClass();
                                $fieldData->field_id=126;
                                $fieldData->uid=$a->uid;
                                $fieldData->type=$a->type;
                                $fieldData->datakey='SCHOOL_ADDRESS';
                                $fieldData->data=$a->data;
                                $fieldData->params=$params;
                                $fieldData->raw=$a->raw;
                                $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
                                break;
                            case 84:
                                $fieldData = new stdClass();
                                $fieldData->field_id=127;
                                $fieldData->uid=$a->uid;
                                $fieldData->type=$a->type;
                                $fieldData->datakey='ABOUT_ME';
                                $fieldData->data=$a->data;
                                $fieldData->params=$params;
                                $fieldData->raw=$a->raw;
                                $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
                                break;
                        }
                    }
                }

            }
        }
        pre($data_prepare);
        die();*/

    }

}