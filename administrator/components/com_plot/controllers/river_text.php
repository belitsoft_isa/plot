<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class PlotControllerRiver_text extends JControllerAdmin
{

    public function add()
    {
        $this->setRedirect('index.php?option=com_plot&view=river_text&layout=edit');
    }

    public function save()
    {
        $riverTextId = $this->saveRiverText();
        $this->setRedirect("index.php?option=com_plot&view=river_text&layout=edit&id=$riverTextId");
    }

    public function saveClose()
    {
        $this->saveRiverText();
        $this->setRedirect("index.php?option=com_plot&view=river_texts");
    }

    public function saveNew()
    {
        $this->saveRiverText();
        $this->setRedirect("index.php?option=com_plot&view=river_text&layout=edit");
    }

    public function cancel()
    {
        $this->setRedirect("index.php?option=com_plot&view=river_texts");
    }    
    
    private function saveRiverText()
    {
        $formData = JRequest::get()['jform'];

        $river_text = new riverText();
        $river_text->id = $formData['id'];
        $river_text->title = $formData['title'];
        $river_text->text = $formData['text'];
        $river_text->published = $formData['published'];
        $riverTextId = $river_text->save();
        return $riverTextId;
    }

}
