<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class PlotControllerCourses extends JControllerAdmin
{

    public function edit()
    {
        $courseId = JRequest::getVar('cid', array(0))[0];
        $this->setRedirect("index.php?option=com_plot&view=course&layout=edit&id=$courseId");
    }

}
