<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class PlotControllerCourse extends JControllerAdmin
{

    public function save()
    {
        $courseId = $this->saveCourse();
        $this->setRedirect("index.php?option=com_plot&view=course&layout=edit&id=$courseId");
    }

    public function saveClose()
    {
        $this->saveCourse();
        $this->setRedirect("index.php?option=com_plot&view=courses");
    }

    public function cancel()
    {
        $this->setRedirect("index.php?option=com_plot&view=courses");
    }

    private function saveCourse()
    {
        $model = $this->getModel('course');
        $form = JRequest::get();
        $formData = $form['jform'];

        $course = new plotCourse();
        $course->id = $formData['id'];
        $course->admin_min_cost = $formData['admin_min_cost'];
        $course->admin_cost = $formData['admin_cost'];
        $course->admin_max_cost = $formData['admin_max_cost'];
        $course->author_min_cost = $formData['author_min_cost'];
        $course->author_cost = $formData['author_cost'];
        $course->author_max_cost = $formData['author_max_cost'];
        $course->min_cost = isset($formData['min_cost'])?(int)$formData['min_cost']:0;
        $course->count_points = $formData['count_points'];
        $course->tags = $formData['tags'];
        $course->small = isset($formData['small'])?(int)$formData['small']:0;
        $course->ages = $formData['ages'];
        $course->image = $formData['image'];

        $hashtags = $formData['hashtags'];
        $beforehashtags = $formData['beforehashtags'];
        $afterhashtags = $formData['afterhashtags'];
        $courseId = $course->save();
        if(isset($formData['id_pub'])){
            $course->savePublication($courseId, $formData['id_pub']);
        }

        $model->saveHashtags($hashtags, '#__plot_hashtag_course_map');
        $model->saveHashtags($beforehashtags, '#__plot_before_hashtag_course_map');
        $model->saveHashtags($afterhashtags, '#__plot_after_hashtag_course_map');
        return $courseId;
    }

}
