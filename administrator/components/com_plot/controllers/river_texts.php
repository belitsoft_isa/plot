<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class PlotControllerRiver_texts extends JControllerAdmin
{

    public function publish()
    {
        $task = explode('.', $this->input->post->get('task', 'unpublish'))[1];

        foreach ($this->input->post->get('cid', array(), 'array') as $riverTextId) {
            $riverText = new riverText($riverTextId);
            $riverText->$task();
        }

        $this->setRedirect('index.php?option=com_plot&view=river_texts');
    }

    public function delete()
    {
        foreach ($this->input->post->get('cid', array(), 'array') as $riverTextId) {
            $riverText = new riverText($riverTextId);
            $riverText->delete();
        }
        
        $this->setRedirect('index.php?option=com_plot&view=river_texts&limitstart=0');
    }

    public function ajaxSaveOrder()
    {
        $riverTextsIds = $this->input->post->get('cid', array(), 'array');
        $ordering = $this->input->post->get('order', array(), 'array');

        foreach ($riverTextsIds as $i => $riverTextId) {
            $riverText = new riverText($riverTextId);
            $riverText->setOrdering($ordering[$i]);
        }

        die;
    }

    public function edit()
    {
        $riverTextId = JRequest::getVar('cid', array(0))[0];
        $this->setRedirect("index.php?option=com_plot&view=river_text&layout=edit&id=$riverTextId");
    }

}
