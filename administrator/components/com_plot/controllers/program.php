<?php defined('_JEXEC') or die('Restricted access');


class PlotControllerProgram extends JControllerForm
{
    //----------------------------------------------------------------------------------------------------
    public function add()
    {
        parent::add();
    }

    //----------------------------------------------------------------------------------------------------
    public function save($key = null, $urlVar = null)
    {
        $model = $this->getModel();
        $jinput = JFactory::getApplication()->input;

        $task = $jinput->get('task', '');

        $jform = $jinput->get('jform', array(), 'ARRAY');
        $id = $jform['id'];
        $ages = $jform['ages'];
        $books = $jform['books'];
        $courses = $jform['courses'];
        $programslist=$jform['programslist'];

        if (empty($ages)) {
            JError::raiseWarning(500, JText::_('COM_PLOT_SELECTED_AGE'));
            $this->setRedirect(JRoute::_('index.php?option=com_plot&view=program&layout=edit&id=' . $id, false));
            return;
        } elseif (empty($books)) {
            JError::raiseWarning(500, JText::_('COM_PLOT_SELECTED_BOOK'));
            $this->setRedirect(JRoute::_('index.php?option=com_plot&view=program&layout=edit&id=' . $id, false));
            return;
        } elseif (empty($courses)) {
            JError::raiseWarning(500, JText::_('COM_PLOT_SELECTED_COURSES'));
            $this->setRedirect(JRoute::_('index.php?option=com_plot&view=program&layout=edit&id=' . $id, false));
        } elseif(empty($programslist)){
            JError::raiseWarning(500, JText::_('COM_PLOT_SELECTED_PROGRAMS'));
            $this->setRedirect(JRoute::_('index.php?option=com_plot&view=program&layout=edit&id=' . $id, false));
        }else{
            if (strpos($jform['link'], 'youtube.com') !== false || strpos($jform['link'], 'youtu.be') !== false) {
               $model=$this->getModel('program');

                $model->save($jform);
            } else {

                JError::raiseWarning(500, JText::_('COM_PLOT_INVALID_LINK'));
                $this->setRedirect(JRoute::_('index.php?option=com_plot&view=program&layout=edit&id=' . $id, false));

            }

            switch ($task) {
                case "apply":
                    $this->setRedirect(JRoute::_('index.php?option=com_plot&view=program&layout=edit&id=' . (int)$id, false));
                    break;
                case "save":
                    $this->setRedirect(JRoute::_('index.php?option=com_plot&view=programs', false));
                    break;
            }

       }


    }




}