<?php
defined('_JEXEC') or die('Restricted access');

class PlotControllerResolution extends JControllerForm
{

    public function ajaxForm()
    {

        $profileEditView = $this->getView('crop', 'html');
        $profileEditView->setLayout('default');
        $img_path = JRequest::getString('img');
        list($width, $height) = getimagesize($img_path);
        $profileEditView->set('originalPhotoUrl', $img_path);
        $profileEditView->set('img_width', $width);
        $profileEditView->set('img_height', $height);
        $profileEditView->display();
        die;
    }

    public function ajaxFormCourse()
    {
        $profileEditView = $this->getView('crop', 'html');
        $profileEditView->setLayout('course');
        $img_path = JRequest::getString('img');
        list($width, $height) = getimagesize(JUri::root().$img_path);
        $profileEditView->set('originalPhotoUrl', JUri::root().$img_path);
        $profileEditView->set('img_width', $width);
        $profileEditView->set('img_height', $height);
        $profileEditView->display();
        die;
    }

    public function ajaxSaveCroppedImg()
    {

        require_once JPATH_ADMINISTRATOR.'/components/com_easysocial/includes/image/image.php';
        $formData = array();
        parse_str(JRequest::getVar('data'), $formData);

        $imgWidth = JRequest::getInt('img-width');
        $imgHeight = JRequest::getInt('img-height');
        $width = JRequest::getInt('w') / $imgWidth;
        $height = JRequest::getInt('h') / $imgHeight;
        $left = JRequest::getInt('x') / $imgWidth;
        $top = JRequest::getInt('y') / $imgHeight;

        $img_src = JRequest::getString('img_src');
        $temp = explode('/', $img_src);
        $img = end($temp);
        $folder = $temp[count($temp) - 2];

        $image = new SocialImage();
        $image = $image->load(JPATH_SITE.'/media/com_html5flippingbook/thumbs/'.$img);

        $imageWidth = $image->getWidth();
        $imageHeight = $image->getHeight();
        if (!is_null($top) && !is_null($left) && !is_null($width) && !is_null($height)) {
            $actualX = $imageWidth * $left;
            $actualY = $imageHeight * $top;
            $actualWidth = $imageWidth * $width;
            $actualHeight = $imageHeight * $height;

            // Now we'll need to crop the image
            $image->crop($actualX, $actualY, $actualWidth, $actualHeight);
        } else {
            if ($imageWidth > $imageHeight) {
                $x = ($imageWidth - $imageHeight) / 2;
                $y = 0;
                $image->crop($x, $y, $imageHeight, $imageHeight);
            } else {
                $x = 0;
                $y = ($imageHeight - $imageWidth) / 2;
                $image->crop($x, $y, $imageWidth, $imageWidth);
            }
        }
        if (file_exists(JPATH_SITE.'/media/com_html5flippingbook/thumbs/'.'thimb_'.$img)) {
            unlink(JPATH_SITE.'/media/com_html5flippingbook/thumbs/'.'thimb_'.$img);
        }
        $tmpImagePath = JPATH_SITE.'/media/com_html5flippingbook/thumbs/'.'thimb_'.$img;
        $image->resize(plotGlobalConfig::getVar('bookCropWidthMin'), plotGlobalConfig::getVar('bookCropHeightMin'));
        $image->save($tmpImagePath);


        // Unset the image to free up some memory
        unset($image);

        // Reload the image again to get the correct resource pointing to the cropped image.
        $image = Foundry::image();
        $image->load($tmpImagePath);
        $doc = JFactory::getDocument();
        $doc->addScriptDeclaration(
                "window.parent.document.getElementById('jform_c_thumb_preview').src='".JUri::root()."/media/com_html5flippingbook/thumbs/thimb_".$img."';".
                "parent.count_load=0;".
                // "jQuery('#jform_page_image_chzn a.chzn-single span', window.parent.document).html('thimb_".$img."');".
                // "jQuery('#jform_page_image option[value=\'thimb_".$img."\']', window.parent.document).removeAttr('selected');".
                // "jQuery('#jform_page_image option[value=\'thimb_".$img."\']', window.parent.document).attr('selected','selected');".
                "window.parent.SqueezeBox.close();"
        );
        return;
    }

    public function ajaxSaveCroppedCourse()
    {
        require_once JPATH_ADMINISTRATOR.'/components/com_easysocial/includes/image/image.php';
        $formData = array();
        parse_str(JRequest::getVar('data'), $formData);

        $imgWidth = JRequest::getInt('img-width');
        $imgHeight = JRequest::getInt('img-height');
        $width = JRequest::getInt('w') / $imgWidth;
        $height = JRequest::getInt('h') / $imgHeight;
        $left = JRequest::getInt('x') / $imgWidth;
        $top = JRequest::getInt('y') / $imgHeight;

        $img_src = JRequest::getString('img_src');
        
        $temp = explode('/', $img_src);
        $img = end($temp);

        $image = new SocialImage();
        $image = $image->load( JPATH_SITE . '/' . substr($img_src, strlen(JUri::root())) );

        $imageWidth = $image->getWidth();
        $imageHeight = $image->getHeight();
        if (!is_null($top) && !is_null($left) && !is_null($width) && !is_null($height)) {
            $actualX = $imageWidth * $left;
            $actualY = $imageHeight * $top;
            $actualWidth = $imageWidth * $width;
            $actualHeight = $imageHeight * $height;

            // Now we'll need to crop the image
            $image->crop($actualX, $actualY, $actualWidth, $actualHeight);
        } else {
            if ($imageWidth > $imageHeight) {
                $x = ($imageWidth - $imageHeight) / 2;
                $y = 0;
                $image->crop($x, $y, $imageHeight, $imageHeight);
            } else {
                $x = 0;
                $y = ($imageHeight - $imageWidth) / 2;
                $image->crop($x, $y, $imageWidth, $imageWidth);
            }
        }
        if (file_exists(JPATH_SITE.'/images/com_plot/courses/'.'thimb_'.$img)) {
            unlink(JPATH_SITE.'/images/com_plot/courses/'.'thimb_'.$img);
        }
        $tmpImagePath = JPATH_SITE.'/images/com_plot/courses/'.'thimb_'.$img;
        $image->resize(plotGlobalConfig::getVar('courseCropWidthMin'), plotGlobalConfig::getVar('courseCropHeightMin'));
        $image->save($tmpImagePath);


        // Unset the image to free up some memory
        unset($image);

        // Reload the image again to get the correct resource pointing to the cropped image.
        $image = Foundry::image();
        $image->load($tmpImagePath);
        $doc = JFactory::getDocument();
        $doc->addScriptDeclaration(
                "jQuery('#jform_image', window.parent.document).val('images/com_plot/courses/thimb_".$img."');".
                "window.parent.SqueezeBox.close();"
        );
        return;
    }

}
