<?php defined('_JEXEC') or die('Restricted access');


class PlotControllerAge extends JControllerForm
{
    //----------------------------------------------------------------------------------------------------
    public function add()
    {
        parent::add();
    }

    //----------------------------------------------------------------------------------------------------
    public function save($key = null, $urlVar = null)
    {
        $jinput = JFactory::getApplication()->input;
        $task = $jinput->get('task', '');
        if ($task == 'cancel') {
            $this->setRedirect(JRoute::_('index.php?option=com_plot&view=ages', false));
            return;
        }
        parent::save($key, $urlVar);
    }

    public function delete()
    {

        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Get items to remove from the request.
        $cid = $this->input->get('cid', array(), 'array');

        if (!is_array($cid) || count($cid) < 1) {
            JError::raiseWarning(500, JText::_($this->text_prefix . '_NO_ITEM_SELECTED'));
        } else {
            // Get the model.
            $model = $this->getModel();

            // Remove the items.
            if ($model->delete($cid)) {
                $this->setMessage(JText::plural($this->text_prefix . '_N_ITEMS_DELETED', count($cid)));
            } else {
                $this->setMessage($model->getError());
            }
        }

        $this->setRedirect(JRoute::_('index.php?option=com_plot&view=ages', false));
    }

    public function checkAge()
    {
        $data=array();
        $jinput = JFactory::getApplication()->input;
        $from = $jinput->get('from', 0, 'INT');
        $to = $jinput->get('to', 0, 'INT');
        $id = $jinput->get('id', 0, 'INT');
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->select('`a`.from, `a`.to')
            ->from('`#__plot_ages` AS `a`')

            ->where('`a`.`id` != ' . (int)$id);
        $db->setQuery($query);

        $ages = $db->loadObjectList();
        foreach ($ages AS $age) {
            for ($i = (int)$age->from; $i <= (int)$age->to; $i++) {
                $temp = array();
                $temp[]=$i;
            }
            if (in_array((int)$age->from, $temp) || in_array((int)$age->to, $temp)) {
                $data['res']=false;
                echo   json_encode($data);
                exit();
                return;
            }

        }
        $data['res']=true;
        echo   json_encode($data);
        exit();
        return;

    }

}