<?php

class plotSocialConfig
{
    # river page
    public $riverTitle = 'Плот';
    public $riverDesc = 'Образовательная сеть на Плоту!';
    public $riverImagePath = 'templates/plot/img/logo-big.jpg';
    public $riverImageWidth = '720';
    public $riverImageHeight = '720';
    
    # courses page
    public $coursesTitle = 'Плот - курсы';
    public $coursesDesc = 'Список курсов для изучения';
    public $coursesImagePath = 'templates/plot/img/social_share/courses.jpg';
    public $coursesImageWidth = '696';
    public $coursesImageHeight = '434';
    
    # publications page
    public $publicationsTitle = 'Плот - книги';
    public $publicationsDesc = 'Список книг для изучения';
    public $publicationsImagePath = 'templates/plot/img/social_share/publications.jpg';
    public $publicationsImageWidth = '696';
    public $publicationsImageHeight = '434';
    
    # events page
    public $eventsTitle = 'Плот - думалки';
    public $eventsDesc = 'Список думалок образовательной сети На Плоту';
    public $eventsImagePath = 'templates/plot/img/social_share/events.jpg';
    public $eventsImageWidth = '696';
    public $eventsImageHeight = '434';
    
    # k2 articles image
    public $k2ArticlesImage = 'templates/plot/img/social_share/events.jpg';
    
    static function get($name)
    {
        $class = new plotSocialConfig();
        return $class->$name;
    }

}
