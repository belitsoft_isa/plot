<?php
/**
* Joomlaquiz Deluxe Component for Joomla 3
* @package Joomlaquiz Deluxe
* @author JoomPlace Team
* @Copyright Copyright (C) JoomPlace, www.joomplace.com
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Quiz model.
 *
 */
class JoomlaquizModelQuiz extends JModelAdmin
{
	protected $text_prefix = 'COM_JOOMLAQUIZ';
		
	public function getTable($type = 'quiz', $prefix = 'JoomlaquizTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	public function getItem($pk = null)
	{
		$result = parent::getItem($pk);
		return $result;
	}
		
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_joomlaquiz.edit.quiz.data', array());

		if (empty($data)) {
			$data = $this->getItem();

			// Prime some default values.
			if ($this->getState('quiz.c_id') == 0) {
				$app = JFactory::getApplication();
				$id = $app->getUserState('com_joomlaquiz.edit.quiz.c_id');
				if ($id) $data->set('c_id', JFactory::getApplication()->input->getInt('c_id', $id));
			}
		}
		
		return $data;
	}
	
	public function getForm($data = array(), $loadData = true)
	{
		$app	= JFactory::getApplication();
		$form = $this->loadForm('com_joomlaquiz.quiz', 'quiz', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}
	
	public function getCategories(){
		$db = JFactory::getDBO();
		
		$query = "SELECT * FROM #__quiz_t_category order by c_category";
		$db->setQuery( $query );
		$jq_cats = $db->loadObjectList();
		
		return $jq_cats;
	}
	
	public function getCertificates(){
		$db = JFactory::getDBO();
		
		$jq_cert = array();
		$query = "SELECT id as value, cert_name as text FROM #__quiz_certificates order by cert_file";
		$db->setQuery( $query );
		$jq_cert = $db->loadObjectList();
		
		return $jq_cert;
	}
	
	public function getTemplates(){
		$db = JFactory::getDBO();
		
		$jq_temps = array();
		$query = "SELECT * FROM #__quiz_templates order by id";
		$db->setQuery( $query );
		$jq_temps = $db->loadObjectList();
		
		return $jq_temps;
	}
	
	public function getQuizData($quiz_id)
	{
		$db = JFactory::getDBO();
		$return = array();
		
		$feed_opres = array();
		if($quiz_id){
			$query = "SELECT * FROM #__quiz_feed_option WHERE quiz_id=".$quiz_id;
			$db->setQuery( $query );
			$feed_opres = $db->loadObjectList();
		}
		$return['feed_opres'] = $feed_opres;
		
		$return['if_pool'] = array();
		if($quiz_id){
			$query = "SELECT * FROM #__quiz_pool WHERE q_id=".$quiz_id;
			$db->setQuery($query);
			$return['if_pool'] =  $db->loadObjectList();
		}
		
		$query = 'SELECT DISTINCT(qc_tag) AS value, qc_tag AS text'
		. ' FROM #__quiz_q_cat'
		. ' WHERE TRIM(qc_tag) <> \'\''
		. ' ORDER BY qc_tag'
		;
		$db->setQuery( $query );
		$qc_tag[] = JHTML::_('select.option', '', JText::_('COM_JOOMLAQUIZ_CHOOSE_HEAD_CATEGORY'));
		$qc_tag = array_merge( $qc_tag, $db->loadObjectList() );
		foreach($qc_tag as $i=>$tag) {
			if($tag->value) {
				$qc_tag[$i]->value = base64_encode($tag->text);
			}
		}
		$return['head_cat_arr'] = $qc_tag;
				
		$jq_qcat = array();
		$query = "SELECT qc_id as value, qc_category as text, qc_tag AS head_category FROM #__quiz_q_cat order by qc_tag, qc_category, qc_id";
		$db->setQuery( $query );
		$jq_qcat =  $db->loadObjectList();
		$return['jq_pool_cat'] = $jq_qcat;
		
		$return['q_count'] = '';
		if($quiz_id){
			$db->setQuery("SELECT `q_count` FROM #__quiz_pool WHERE `q_id` = '".$quiz_id."'");
			$return['q_count'] = $db->loadResult();
		}
		
		return $return;
	}

    //isHack start
    public function getSumPointsPoolByCat($cat_id){
        $db = $this->_db;
        $query = $db->getQuery(true);
        $query->select('SUM(c.a_point) AS pool_score')
            ->from('#__quiz_t_question AS q')
            ->innerJoin('#__quiz_t_choice AS c ON q.c_id=c.c_question_id')
            ->where('q.c_ques_cat=' . (int)$cat_id)
            ->where('c.c_right=1');
        $result = $db->setQuery($query)->loadResult();
        return $result;
    }
    //isHack start
}