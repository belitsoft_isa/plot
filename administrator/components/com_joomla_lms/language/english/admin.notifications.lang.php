<?php
/**
* admin.notifications.lang.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$JLMS_LANGUAGE['_JLMS_NOTS_SELFENROLMENT_INTO_FREE_COURSE'] = 'SelfEnrolment into free course';
$JLMS_LANGUAGE['_JLMS_NOTS_SELFENROLMENT_INTO_PAID_COURSE'] = 'SelfEnrolment into paid course';
$JLMS_LANGUAGE['_JLMS_NOTS_ENROLMENT_INTO_COURSE_BY_COURSE_TEACHER'] = 'Enrolment into the course by course teacher';
$JLMS_LANGUAGE['_JLMS_NOTS_ENROLMENT_INTO_COURSE_BY_JADMIN'] = 'Enrolment into the course by Joomla Administrator';
$JLMS_LANGUAGE['_JLMS_NOTS_USER_COMPLETES_COURSE'] = 'User completes the course';
$JLMS_LANGUAGE['_JLMS_NOTS_TEACHER_MARK_COURSE_COMPLETION'] = 'Teacher marks course completion';
$JLMS_LANGUAGE['_JLMS_NOTS_IMPORT_USER_INTO_LMS_FROM_CSV'] = 'Import user into LMS from CSV';
$JLMS_LANGUAGE['_JLMS_NOTS_USER_COMPLETES_QUIZ'] = 'User completes the quiz';
$JLMS_LANGUAGE['_JLMS_NOTS_TEACHER_REVIEWS_HOMEWORK_SUBMISSION'] = 'Teacher reviews homework submission';
$JLMS_LANGUAGE['_JLMS_NOTS_NEW_DROPBOX_FILE_RECEIVED'] = 'New Dropbox file received';
$JLMS_LANGUAGE['_JLMS_NOTS_USER_COMPLETES_LEARNING_PATH'] = 'User completes learning path';
$JLMS_LANGUAGE['_JLMS_NOTS_USER_SUBMITS_HOMEWORK'] = 'User submits homework';


$JLMS_LANGUAGE['_JLMS_NOTS_EMAIL_NOTS'] = 'Email Notifications';
$JLMS_LANGUAGE['_JLMS_NOTS_SELECT_USR_TPL'] = 'Please select user email template';
$JLMS_LANGUAGE['_JLMS_NOTS_SELECT_MNGR_TPL'] = 'Please select manager email template';
$JLMS_LANGUAGE['_JLMS_NOTS_EDIT_NOT'] = 'Edit Notification';
$JLMS_LANGUAGE['_JLMS_NOTS_SELECT_ROLES'] = 'Select roles';
$JLMS_LANGUAGE['_JLMS_NOTS_SELECT_USR_E_TPL'] = 'Select user email template';
$JLMS_LANGUAGE['_JLMS_NOTS_SELECT_MNGR_E_TPL'] = 'Select manager email template';
$JLMS_LANGUAGE['_JLMS_NOTS_NOT_SEND_TO_LENRS'] = 'do not send letters to learners';
$JLMS_LANGUAGE['_JLMS_NOTS_NOT_SEND_TO_MNGRS'] = 'do not send letters to managers';
$JLMS_LANGUAGE['_JLMS_NOTS_EMAIL_TPLS'] = 'Email Templates';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_ENTR_TPL_NAME'] = 'Please enter email template name';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_ENTR_TPL_SBJ'] = 'Please enter email template subject';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_SELCT_NOT_TYPE'] = 'Please select notification type';
$JLMS_LANGUAGE['_JLMS_NOTS_EDIT_EML_TPL'] = 'Edit Email Template';
$JLMS_LANGUAGE['_JLMS_NOTS_NEW_EML_TPL'] = 'New Email Template';
$JLMS_LANGUAGE['_JLMS_NOTS_ENTER_VLS'] = 'Enter values';
$JLMS_LANGUAGE['_JLMS_NOTS_SELCT_NOT_TYPE'] = 'Select notification type';
$JLMS_LANGUAGE['_JLMS_NOTS_TEXT_SNIPPETS'] = 'Text snippets';
$JLMS_LANGUAGE['_JLMS_NOTS_BODY_HTML'] = 'Body HTML';
$JLMS_LANGUAGE['_JLMS_NOTS_BODY_TEXT'] = 'Body Text';
$JLMS_LANGUAGE['_JLMS_NOTS_SLCT_TMP_'] = ' - Select template - ';
$JLMS_LANGUAGE['_JLMS_NOTS_SLCT_N_TYPE_'] = ' - Select notification type - ';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_SAVED'] = 'Notification was successfully saved.';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_TPL_SAVED'] =  'Template was successfully saved.';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_TPL_DELETED'] =  'Template was successfully deleted.';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_TPLS_DELETED'] =  'Templates were successfully deleted.';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_PLUGIN_IS_NOT_ENABLED'] =  'JoomlaLMS \'Email notifications\' plugin is not enabled.  In order to activate email notifications please enable the plugin on <a href="index.php?option=com_joomla_lms&task=pluginslist">JoomlaLMS plugins</a> page.';

?>