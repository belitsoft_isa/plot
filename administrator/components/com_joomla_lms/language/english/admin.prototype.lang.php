<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ADVANCED'] = 'Advanced';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ADVANCED_ASSIGNED_GROUPS_ONLY_1'] = 'Work with assigned groups only';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ADVANCED_VIEW_ALL_COURSE_CATEGORIES'] = 'View all course categories';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ADVANCED_ASSIGNED_GROUPS_ONLY_2'] = 'Work within assigned group only<br /><small>(dropbox, chat, mailbox and forum)</small>';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LMS'] = 'LMS';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LMS_CREATE_COURSE'] = 'Create courses';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LMS_ORDER_COURSE'] = 'Order courses';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS'] = 'Docs';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_VIEW'] = "View list of documents (this setting grants access to the 'documents' tool)";
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_VIEW_ALL'] = 'View unpublished items in the list';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_ORDER'] = 'Order items in the list';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_PUBLISH'] = 'Publish/Unpublish items';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_MANAGE'] = 'Manage items (create/edit/delete)';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_ONLY_OWN_ITEMS'] = 'User can manage only their own items';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_ONLY_OWN_ROLES'] = 'User can manage only items created by users of the same user role';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_SET_PERMISSIONS'] = 'Set custom permissions for folders';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_IGNORE_PERMISSIONS'] = 'Ignore custom permissions configured for folders';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_QUIZZES'] = 'Quizzes';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_QUIZZES_VIEW'] = "View list of quizzes (this setting grants access to the 'quizzes' tool)";
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_QUIZZES_VIEW_ALL'] = 'View all (published and unpublished) items in the list';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_QUIZZES_VIEW_STATS'] = 'View quizzes statistics';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_QUIZZES_PUBLISH'] = 'Publish/Unpublish items';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_QUIZZES_MANAGE'] = 'Manage quizzes/questions (create/edit/delete)';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_QUIZZES_MANAGE_POOL'] = 'Manage course questions pool';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LINKS'] = 'Links';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LINKS_VIEW'] = "View list of links (this setting grants access to the 'links' tool)";
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LINKS_VIEW_ALL'] = 'View all (published and unpublished) items in the list';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LINKS_ORDER'] = 'Order items in the list';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LINKS_PUBLISH'] = 'Publish/Unpublish items';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LINKS_MANAGE'] = 'Manage links (create/edit/delete)';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LINKS_ONLY_OWN_ITEMS'] = 'User can manage only their own items';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LINKS_ONLY_OWN_ROLE'] = 'User can manage only items created by users of the same user role';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LPATHS'] = 'LPaths';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LPATHS_VIEW'] = "View list of Learning Paths (this setting grants acess to the 'learning paths' tool)";
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LPATHS_VIEW_ALL'] = 'View all (published and unpublished) items in the list';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LPATHS_ORDER'] = 'Order items in the list';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LPATHS_PUBLISH'] = 'Publish/Unpublish items';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LPATHS_MANAGE'] = 'Manage LPaths (create/edit/delete)';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LPATHS_ONLY_OWN_ITEMS'] = 'User can manage only their own items';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LPATHS_ONLY_OWN_ROLE'] = 'User can manage only items created by users of the same user role';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ANNOUNCE'] = 'Announce';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ANNOUNCE_VIEW'] = "View list of announcements (this setting grants acess to the 'announcements' tool)";
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ANNOUNCE_MANAGE'] = 'View all (published and unpublished) items in the list';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ANNOUNCE_ONLY_OWN_ITEMS'] = 'User can manage only their own items';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ANNOUNCE_ONLY_OWN_ROLE'] = 'User can manage only items created by users of the same user role';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DROPBOX'] = 'Dropbox';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DROPBOX_VIEW'] = 'View list of DropBox items';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DROPBOX_SEND_TO_TEACHERS'] = 'Send items to teachers';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DROPBOX_SEND_TO_LEARNERS'] = 'Send items to learners';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DROPBOX_MARK_AS_CORRECTED'] = 'Mark items as corrected';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_HOMEWORK'] = 'Homework';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_HOMEWORK_VIEW'] = 'View list of homework items';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_HOMEWORK_VIEW_STATS'] = 'View statistics';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_HOMEWORK_VIEW_ALL'] = 'View all (published and unpublished) items in the list';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_HOMEWORK_PUBLISH'] = 'Publish/Unpublish items';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_HOMEWORK_MANAGE'] = 'Manage items (create/edit/delete)';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ATTENDANCE'] = 'Attendance';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ATTENDANCE_VIEW'] = 'Access to attendance tool';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ATTENDANCE_MANAGE'] = 'Manage attendance records';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_CHAT'] = 'Chat';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_CHAT_VIEW'] = 'Access course/group chat';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_CHAT_MANAGE'] = "Access all groups chats ('manager' rights)";

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_CONFERENCE'] = 'Conference';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_CONFERENCE_VIEW'] = 'Access course conference tool';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_CONFERENCE_MANAGE'] = "Participate in the conference as 'manager'";

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_GRADEBOOK'] = 'Gradebook';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_GRADEBOOK_VIEW'] = 'View GradeBook';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_GRADEBOOK_MANAGE'] = "Manage course 'completion' status";
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_GRADEBOOK_CONFIGURE'] = 'Configure (create/edit grades, scales, etc.)';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_TRACKING'] = 'Tracking';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_TRACKING_MANAGE'] = 'View course tracking information';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_TRACKING_CLEAR_STATS'] = 'Clear course tracking statistics';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_MAILBOX'] = 'Mailbox';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_MAILBOX_VIEW'] = 'Use course mailbox';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_MAILBOX_MANAGE'] = 'Manage course mailbox';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_USERS'] = 'Users';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_USERS_MANAGE'] = 'Manage course participants';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_USERS_MANAGE_TEACHERS'] = 'Manage course teachers/assistants';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_USERS_VIEW'] = 'View list of course participants';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_USERS_IMPORTS_USERS'] = 'Import new users';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_COURSE'] = 'Course';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_COURSE_EDIT'] = 'Edit course properties';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_COURSE_MANAGE_SETTINGS'] = 'Manage course settings';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_COURSE_MANAGE_TOPICS'] = 'Manage course topics';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_COURSE_MANAGE_CERTIFICATES'] = 'Manage course certificates';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LIBRARY'] = 'Library';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LIBRARY_ONLY_OWN_ITEMS'] = 'Teacher can manage only their own items';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_CEO'] = 'Ceo';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_CEO_VIEW_ALL_USERS'] = 'View All users';
