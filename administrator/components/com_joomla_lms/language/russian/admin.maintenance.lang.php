<?php
/**
* admin.maintenance.lang.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$JLMS_LANGUAGE['_JLMS_MAINT_BTN_LOG'] = '������';
$JLMS_LANGUAGE['_JLMS_MAINT_BTN_CHECK_DB'] = '��������� ��';

$JLMS_LANGUAGE['_JLMS_MAINT'] = '������������';
$JLMS_LANGUAGE['_JLMS_MAINT_DESC'] = "'��������� ��' - ��� �������� ��������� ���������, �� �������� �� ���� ���� ������ ���, � ���������� ������, ���� ��� ����������.<br />
				'���' - ������� ���� � ������ ���������� �������� ������������ (�� ������ ������������ ���� ���� � ������ ��������� JoomlaLMS �� �������).<br />
				<b>�����:</b> ���������� '��������� ���� ������' ������ ��� ����� ������������� ���������� JoomlaLMS ��� ���������� �� ��������� ������.";
$JLMS_LANGUAGE['_JLMS_MAINT_CHECK_DB'] = '�������� ���� ������ JoomlaLMS';
$JLMS_LANGUAGE['_JLMS_MAINT_TABLE'] = '�������';
$JLMS_LANGUAGE['_JLMS_MAINT_STATUS'] = '������';
$JLMS_LANGUAGE['_JLMS_MAINT_IF_YOU_SEE'] = '���� �� ������ ��� �������� ����� 1 ������, ����������, ������� 
					<a href="{link}">�����</a>
					����� ������� �� ��������� ������� ������������.';
$JLMS_LANGUAGE['_JLMS_MAINT_CFG_IS_EMPTY'] = '������� �������� ��� �����, ����������, �������� �������� �������� ��� � ��������� ���������� �� ����������';
$JLMS_LANGUAGE['_JLMS_MAINT_CAT_IS_EMPTY'] = '������� ��������� ������ �����, ����������, ��������� ��������� ������ �� �������� ���������� �����������';
$JLMS_LANGUAGE['_JLMS_MAINT_TYPES_IS_EMPTY'] = '������ ����� ������, ������� ��������� ��� ��������, ����';
$JLMS_LANGUAGE['_JLMS_MAINT_TYPES_LIST_GENER'] = '������ ����������� ����� ������ ������� ������������';
$JLMS_LANGUAGE['_JLMS_MAINT_GRADE_LIST_GENER'] = '������ ����������� ��������� ������� ������ ������� ������������';
$JLMS_LANGUAGE['_JLMS_MAINT_GRADE_LIST_IS_EMPTY'] = '������ ��������� ������� ������ ����';
$JLMS_LANGUAGE['_JLMS_MAINT_LANG_LIST_IS_EMPTY'] = '������ ������ ����';
$JLMS_LANGUAGE['_JLMS_MAINT_LANG_LIST_GENER'] = '������ ������ ������� ������������';
$JLMS_LANGUAGE['_JLMS_MAINT_MENU_LIST_IS_EMPTY'] = '������ ��������� ���� JoomlaLMS ����';
$JLMS_LANGUAGE['_JLMS_MAINT_MENU_LIST_GENER'] = '������ ��������� ���� JoomlaLMS ������� ������������';
$JLMS_LANGUAGE['_JLMS_MAINT_PLUGINS_LIST_IS_EMPTY'] = '������ �������� JoomlaLMS ����, ����������, �������� �������� ���������� ��������� JoomlaLMS';
$JLMS_LANGUAGE['_JLMS_MAINT_QUEST_TS_LIST_IS_EMPTY'] = '������ ����� �������� ����';
$JLMS_LANGUAGE['_JLMS_MAINT_QUEST_TS_LIST_GENER'] = '������ ����� �������� ������� ������������';
$JLMS_LANGUAGE['_JLMS_MAINT_ROLES_LIST_IS_EMPTY'] = '������ ����� ������������� JoomlaLMS ����';
$JLMS_LANGUAGE['_JLMS_MAINT_ROLES_LIST_GENER'] = '������ ����� ������������� JoomlaLMS ������� ������������';
$JLMS_LANGUAGE['_JLMS_MAINT_T_ROLE_NOT_CFG'] = '���� ������������� �� ��������� �� �������������';
$JLMS_LANGUAGE['_JLMS_MAINT_T_ROLE_CFG'] = '���� ������������� �� ��������� ������� �������������';
$JLMS_LANGUAGE['_JLMS_MAINT_L_ROLE_NOT_CFG'] = '���� ������� �� ��������� �� ���������';
$JLMS_LANGUAGE['_JLMS_MAINT_L_ROLE_CFG'] = '���� ������� �� ��������� ������� ���������';
$JLMS_LANGUAGE['_JLMS_MAINT_RCS_FIXED'] = '������ ����������';
$JLMS_LANGUAGE['_JLMS_MAINT_DB_V_UPDATED'] = '���� ������ JoomlaLMS ������� ��������� �� ������';
$JLMS_LANGUAGE['_JLMS_MAINT_DB_V'] = '������ ���� ������ JoomlaLMS';
$JLMS_LANGUAGE['_JLMS_MAINT_DB_CHECK'] = '�������� ���� ������ JoomlaLMS';
?>