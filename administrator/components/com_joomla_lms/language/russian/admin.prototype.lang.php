<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ADVANCED'] = '�����������';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ADVANCED_ASSIGNED_GROUPS_ONLY_1'] = '������ ������ � ������������ ��������';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ADVANCED_VIEW_ALL_COURSE_CATEGORIES'] = '�������� ��� ��������� ������';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_ADVANCED_ASSIGNED_GROUPS_ONLY_2'] = '������ ������ � ������������ �������� (dropbox, chat, forum, mail)';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LMS'] = '���';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LMS_CREATE_COURSE'] = '�������� ������';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_LMS_ORDER_COURSE'] = '������� ������';

$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS'] = '���������';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_VIEW'] = "����������� ������ ���������� (���� �������� ������������� ������ � ����������� '���������')";
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_VIEW_ALL'] = '�������� ���������������� ��������� � ������';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_ORDER'] = '������� ��������� � ������';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_PUBLISH'] = '���������� / �������� ���������� �������';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_MANAGE'] = '���������� ���������� (�������� / ��������� / ��������)';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_ONLY_OWN_ITEMS'] = '������������ ����� ��������� ������ ������ ����������� ����������';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_ONLY_OWN_ROLES'] = '������������ ����� ��������� ������ ����������, ���������� �������������� ��� �� ����, ��� � ������������';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_SET_PERMISSIONS'] = '���������� ������ ���������� ��� �����';
$JLMS_LANGUAGE['_JLMS_PROTOTYPE_DOCS_IGNORE_PERMISSIONS'] = '������������ ���������������� ���������� ��������� ��� �����';