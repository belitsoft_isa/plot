<?php
/**
* admin.notifications.lang.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$JLMS_LANGUAGE['_JLMS_NOTS_SELFENROLMENT_INTO_FREE_COURSE'] = '��������������� ���������� �� ���������� ����';
$JLMS_LANGUAGE['_JLMS_NOTS_SELFENROLMENT_INTO_PAID_COURSE'] = '��������������� ���������� �� ������� ����';
$JLMS_LANGUAGE['_JLMS_NOTS_ENROLMENT_INTO_COURSE_BY_COURSE_TEACHER'] = '���������� �� ����, ���� ��������';
$JLMS_LANGUAGE['_JLMS_NOTS_ENROLMENT_INTO_COURSE_BY_JADMIN'] = '���������� �� ���� ��� Joomla �������������';
$JLMS_LANGUAGE['_JLMS_NOTS_USER_COMPLETES_COURSE'] = 'User completes the course';
$JLMS_LANGUAGE['_JLMS_NOTS_TEACHER_MARK_COURSE_COMPLETION'] = 'Teacher marks course completion';
$JLMS_LANGUAGE['_JLMS_NOTS_IMPORT_USER_INTO_LMS_FROM_CSV'] = 'Import user into LMS from CSV';
$JLMS_LANGUAGE['_JLMS_NOTS_USER_COMPLETES_QUIZ'] = 'User completes the quiz';
$JLMS_LANGUAGE['_JLMS_NOTS_TEACHER_REVIEWS_HOMEWORK_SUBMISSION'] = 'Teacher reviews homework submission';
$JLMS_LANGUAGE['_JLMS_NOTS_NEW_DROPBOX_FILE_RECEIVED'] = 'New Dropbox file received';
$JLMS_LANGUAGE['_JLMS_NOTS_USER_COMPLETES_LEARNING_PATH'] = 'User completes learning path';
$JLMS_LANGUAGE['_JLMS_NOTS_USER_SUBMITS_HOMEWORK'] = 'User submits homework';


$JLMS_LANGUAGE['_JLMS_NOTS_EMAIL_NOTS'] = 'E-mail-�����������';
$JLMS_LANGUAGE['_JLMS_NOTS_SELECT_USR_TPL'] = '����������, �������� e-mail-������ ��� ������������';
$JLMS_LANGUAGE['_JLMS_NOTS_SELECT_MNGR_TPL'] = '����������, �������� e-mail-������ ��� ������������';
$JLMS_LANGUAGE['_JLMS_NOTS_EDIT_NOT'] = '������������� �����������';
$JLMS_LANGUAGE['_JLMS_NOTS_SELECT_ROLES'] = '�������� ����';
$JLMS_LANGUAGE['_JLMS_NOTS_SELECT_USR_E_TPL'] = '�������� e-mail-������ ��� ������������';
$JLMS_LANGUAGE['_JLMS_NOTS_SELECT_MNGR_E_TPL'] = '�������� e-mail-������ ��� ������������';
$JLMS_LANGUAGE['_JLMS_NOTS_NOT_SEND_TO_LENRS'] = '�� �������� ��������� ��������';
$JLMS_LANGUAGE['_JLMS_NOTS_NOT_SEND_TO_MNGRS'] = '�� �������� ��������� �������������';
$JLMS_LANGUAGE['_JLMS_NOTS_EMAIL_TPLS'] = 'E-mail-�������';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_ENTR_TPL_NAME'] = '����������, ������� �������� e-mail-�������';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_ENTR_TPL_SBJ'] = '����������, ������� ���� e-mail-�������';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_SELCT_NOT_TYPE'] = '����������, �������� ��� �����������';
$JLMS_LANGUAGE['_JLMS_NOTS_EDIT_EML_TPL'] = '������������� E-mail-������';
$JLMS_LANGUAGE['_JLMS_NOTS_NEW_EML_TPL'] = '������� E-mail-������';
$JLMS_LANGUAGE['_JLMS_NOTS_ENTER_VLS'] = '������ ���������';
$JLMS_LANGUAGE['_JLMS_NOTS_SELCT_NOT_TYPE'] = '�������� ��� �����������';
$JLMS_LANGUAGE['_JLMS_NOTS_TEXT_SNIPPETS'] = '��������� ��������';
$JLMS_LANGUAGE['_JLMS_NOTS_BODY_HTML'] = '�������� ����� � ������� HTML';
$JLMS_LANGUAGE['_JLMS_NOTS_BODY_TEXT'] = '�������� �����';
$JLMS_LANGUAGE['_JLMS_NOTS_SLCT_TMP_'] = ' - �������� ������ - ';
$JLMS_LANGUAGE['_JLMS_NOTS_SLCT_N_TYPE_'] = ' - �������� ��� ����������� - ';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_SAVED'] = '����������� ���� ������� ���������.';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_TPL_SAVED'] =  '������ ��� ������� ��������.';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_TPL_DELETED'] =  '������ ��� ������� ������.';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_TPLS_DELETED'] =  '������� ���� ������� �������.';
$JLMS_LANGUAGE['_JLMS_NOTS_MSG_PLUGIN_IS_NOT_ENABLED'] =  'JoomlaLMS \'Email notifications\' ������ �� �����������. ��� ��������� E-mail �����������, ����������, ����������� ������ �� �������� <a href="index.php?option=com_joomla_lms&task=pluginslist">JoomlaLMS plugins</a>.';

?>