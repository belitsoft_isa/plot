<?php
/**
* admin.joomla_lms.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
if (!defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) { die( 'Restricted access' ); }

if ( !defined( '_JLMS_EXEC' ) ) { define( '_JLMS_EXEC', 1 ); }

if (!defined('_JOOMLMS_COMP_NAME')) {
	define( '_JOOMLMS_COMP_NAME', 'JoomlaLMS' );	
}

$doc = JFactory::getDocument();//for 'addStyleSheet'

global $option;
$option = 'com_joomla_lms';


if (!defined('DS')) { define( 'DS', DIRECTORY_SEPARATOR ); }
if (!defined('_JOOMLMS_FRONT_HOME')) {
	define('_JOOMLMS_FRONT_HOME', JPATH_SITE . DS . 'components' . DS . 'com_joomla_lms');
}
require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "component.legacy.php");
require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.factory.php");
//require_once(_JOOMLMS_FRONT_HOME. "/includes/libraries/lms.lib.language.php"); //it connects to the main.php

$doc->addStyleSheet(JURI::root().'administrator/components/com_joomla_lms/assets/css/joomla_lms_style.css?rev=' . JLMSFactory::getConfig()->getVersionToken());

$db = JFactory::getDbo();
$juser = JLMSFactory::getUser();

	if (!defined('_JOOMLMS_ADMIN_HOME')) {
		define('_JOOMLMS_ADMIN_HOME', dirname(__FILE__));
	}
	if (!defined('_JOOMLMS_INCLUDES_PATH')) {
		define('_JOOMLMS_INCLUDES_PATH', JPATH_SITE.'/components/com_joomla_lms/includes/' );
	}
	if (!defined('_JOOMLMS_FRONT_HOME')) {
		define('_JOOMLMS_FRONT_HOME', JPATH_SITE.'/components/com_joomla_lms');
	}
	
	if (!defined('DS')) { define( 'DS', DIRECTORY_SEPARATOR ); }
	
	require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "lms_legacy.php");
	
	require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.factory.php");
	
	require_once(_JOOMLMS_FRONT_HOME . DS . "joomla_lms.main.php");
	
	$GLOBALS['JLMS_DB'] = & JLMSFactory::getDB();
	
	//require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.class.php");	
	/* CONFIG class */
	//require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.config.php");
	
	if( JLMS_J16version() ) 
	{
		JHtml::_('behavior.framework', true);
		
		$appl = JFactory::getApplication();
		$tplName = $appl->getTemplate();
		
		define('ADMIN_IMAGES_PATH', JURI::base().'templates/'.$tplName.'/images/admin/');
		define('DEFAULT_ADMIN_TEMPLATE', 'bluestork');		
	} else {
		define('ADMIN_IMAGES_PATH', 'images/');
		define('DEFAULT_ADMIN_TEMPLATE', 'khepri');
	}
	
	require_once(_JOOMLMS_ADMIN_HOME.'/../../../components/com_joomla_lms/joomla_lms.class.php');
	
	$GLOBALS['JLMS_CONFIG'] = JLMSFactory::getConfig();
	global $JLMS_CONFIG, $JLMS_LANGUAGE;
	
	//require_once(_JOOMLMS_FRONT_HOME. "/includes/config.inc.php");
	$lang =JFactory::getLanguage();
		
	JLMS_require_lang( $JLMS_LANGUAGE, 'admin.main.lang', $JLMS_CONFIG->get('default_language'), 'backend' );	 
	JLMS_require_lang( $JLMS_LANGUAGE, 'admin.prototype.lang', $JLMS_CONFIG->get('default_language'), 'backend' );	 
	JLMS_processLanguage( $JLMS_LANGUAGE, false, 'backend' );
	

$GLOBALS['license_lms_users'] = 100;
$GLOBALS['license_lms_roles'] = true;
$GLOBALS['license_lms_recurrent'] = true;
$GLOBALS['license_lms_branding_free'] = false;
$GLOBALS['lms_license_edition_str'] = 'JoomlaLMS TRIAL';
$is_trial = false;

if (function_exists("ioncube_file_is_encoded") && ioncube_file_is_encoded()) {
	$license_properties = array();
	if (function_exists('ioncube_license_properties')) {
		$license_properties = ioncube_license_properties();
		if (isset($license_properties['users']['value'])) {
			$GLOBALS['license_lms_users'] = $license_properties['users']['value'];
		}
		if (isset($license_properties['branding']['value']) && $license_properties['branding']['value'] == 1) {
			$GLOBALS['license_lms_branding_free'] = true;
		}
		if (isset($license_properties['type']['value']) && $license_properties['type']['value'] == 1) {
			$GLOBALS['lms_license_edition_str'] = _JLMS_ABOUT_LICENSE_EDITION_STD;
			$GLOBALS['license_lms_recurrent'] = false;
		} elseif (isset($license_properties['type']['value']) && $license_properties['type']['value'] == 2) {
			$GLOBALS['lms_license_edition_str'] = _JLMS_ABOUT_LICENSE_EDITION_PRO;
		} else {
			$is_trial = true;
		}
		if (isset($license_properties['roles']['value']) && $license_properties['roles']['value'] == 1) {
			$GLOBALS['license_lms_roles'] = true;
			$GLOBALS['license_lms_recurrent'] = true;//in case if STD with roles
		} else {
			$GLOBALS['license_lms_roles'] = false;
		}
	}
}

//$is_trial = true;//temporary
$GLOBALS['is_jlms_trial'] = $is_trial;
if ($is_trial) {
	$GLOBALS['license_lms_roles'] = false;
	$GLOBALS['is_jlms_trial_roles_heading_text'] = " <font color='red'>("._JLMS_ABOUT_PRO_ONLY.")</font>";
	$GLOBALS['is_jlms_trial_roles_page_text'] = 'Note: full user roles management functionality is available in PRO edition only. STD edition does not allow creating custom roles. In JoomlaLMS Trial it is possible to overview the custom roles creation process, but not possible to save the created roles (this limitation is implemented to avoid problems when moving from Trial to STD edition where custom roles are not available).<br /><a target="_blank" title="JoomlaLMS editions comparison" href="http://www.joomlalms.com/lms/joomlalms-professional-edition-features.html">[view full editions comparison]</a>';
}
$GLOBALS['jlms_license_expires_str'] = '<span style = "font-weight:bold; color:green">'._JLMS_ABOUT_LICENSE_EXPIRES.'</span>';
if (function_exists('ioncube_file_info')) {
	$a = ioncube_file_info();
	if (isset($a['FILE_EXPIRY']) && $a['FILE_EXPIRY']) {
		$expired_on_time = $a['FILE_EXPIRY'];
		$ex_color = 'green';
		$now = time();
		if (($expired_on_time - $now) < (30*24*60*60)) {
			$ex_color = 'red';
		}
		$GLOBALS['jlms_license_expires_str'] = '<span style = "font-weight:bold; color:'.$ex_color.'">'.date('d F Y', $a['FILE_EXPIRY'])."</span>";
	}
}

if( JLMS_J25version() ) 
{
	require_once(_JOOMLMS_ADMIN_HOME.'/jlms25.classes.php');
}

if( JLMS_J30version() ) 
{
	JHtml::_('formbehavior.chosen', 'select');
}

if ( $juser->get('id') ) {
	$backend_gids = $JLMS_CONFIG->get('backend_access_gid' );	
	
	$backend_gid = explode(',',$backend_gids);	
	$backend_gid = array_merge( $backend_gid, JLMS_getAdminGroups() );	
		
	if( JLMS_J16version() ) 
	{
		$query = "SELECT group_id FROM #__user_usergroup_map WHERE user_id = ".$juser->get('id');	
		$db->SetQuery($query);
		$user_gid = JLMSDatabaseHelper::loadResultArray();
	} else {
		$query = "SELECT gid FROM #__users WHERE id = ".$juser->get('id');	
		$db->SetQuery($query);
		$user_gid = JLMSDatabaseHelper::loadResultArray();	
	}	
		
	if ( $user_gid ) 
	{
		$resArray = array_intersect( $user_gid, $backend_gid );						
		if ( empty( $resArray ) ) {			
			mosRedirect("index.php", _JLMS_MSG_DONT_HV_ACCESS );
		}
	}
	
	require_once(_JOOMLMS_FRONT_HOME . "/includes/notifications/notifications.manager.php");

	//require_once(_JOOMLMS_ADMIN_HOME.'/../../../components/com_joomla_lms/joomla_lms.class.php');
	require_once(_JOOMLMS_ADMIN_HOME.'/admin.joomla_lms.func.php' );
	require_once( dirname(__FILE__).'/admin.joomla_lms.html.php' );

	require_once(_JOOMLMS_FRONT_HOME.DS.'includes'.DS.'libraries'.DS.'lms.lib.recurrent.pay.php');

	$mem_lim_global = $JLMS_CONFIG->get('memory_limit_global', 0);
	if ($mem_lim_global) {
		JLMS_adjust_memory_limit($mem_lim_global);
	}

	$GLOBALS['lms_version_full'] = $JLMS_CONFIG->get('jlms_version', '1.2.0');
	
	global $lms_version_full;
	if (strlen($lms_version_full) == 14) {
		$GLOBALS['lms_version'] = substr($lms_version_full,0,5);
		$GLOBALS['lms_version_build'] = substr($lms_version_full,6);
	} elseif (substr($lms_version_full, -2) == 'RC') {
		$GLOBALS['lms_version'] = substr($lms_version_full,0,5);
		$GLOBALS['lms_version_build'] = substr($lms_version_full,6);
	} else {
		$GLOBALS['lms_version'] = $lms_version_full;
		$GLOBALS['lms_version_build'] = '';
	}
	global $lms_version;
	global $lms_version_build;

	$GLOBALS['lms_version_check'] = $JLMS_CONFIG->get('lms_check_version', 0);
	global $lms_version_check;
	$task 	= mosGetParam( $_REQUEST, 'task', '' );
	
	$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) );
	$cid 	= mosGetParam( $_POST, 'cid', mosGetParam( $_GET, 'cid', array(0) ) );
	if (!is_array( $cid )) {
		$cid = array(0);
	}
		
	switch ($task)
	{
		case 'lms_forums':
				JLMS_require_lang( $JLMS_LANGUAGE, 'admin.forums.lang', $JLMS_CONFIG->get('default_language'), 'backend' );			
		break;
		case 'lms_roles':case 'roles': case 'edit_role':case 'editA_role':case 'new_role':
		case 'cancel_role':case 'apply_role':case 'save_role':case 'del_role':
				JLMS_require_lang( $JLMS_LANGUAGE, 'admin.roles.lang', $JLMS_CONFIG->get('default_language'), 'backend' );				
		break;		
		case 'lms_users': case 'group_managers':
				JLMS_require_lang( $JLMS_LANGUAGE, 'admin.users.lang', $JLMS_CONFIG->get('default_language'), 'backend' );				
		break;		
		case 'lms_maintenance':
				JLMS_require_lang( $JLMS_LANGUAGE, 'admin.maintenance.lang', $JLMS_CONFIG->get('default_language'), 'backend' );		
		break;		
		case 'dev_config':case 'config':case 'config_save':
		case 'config_apply':
				JLMS_require_lang( $JLMS_LANGUAGE, 'admin.config.lang', $JLMS_CONFIG->get('default_language'), 'backend' );	
		break;		
		# ---	PAGE TIP MANAGEMENT	--- #
		case 'page_tips':case 'edit_ptip':case 'editA_ptip':case 'new_ptip':
		case 'cancel_ptip':case 'apply_ptip':case 'save_ptip':	case 'del_ptip':
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.tips.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break;		
		# ---	PAYMENTS	--- #
		case 'new_payment': case 'save_newpayment': case 'cancel_newpayment':
		case 'payments':case 'edit_payment':case 'editA_payment':case 'del_payments':
		case 'save_payment':case 'cancel_payment':case 'skip_change':case 'apply_change':
		case 'get_payment_invoice':	case 'gen_payment_invoice': case 'pays_list_pdf': case 'sales_report': case 'sales_report_pdf': case 'pays_list_xls': 
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.payments.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break;
		# ---	SUBSCRIPTIONS	--- #		
		case 'assign':case 'save_assign':case 'cancel_assign':case 'subscriptions':
		case 'subscription_save':case 'subscription_apply':case 'publish_subscription':
		case 'delete_subscription':case 'unpublish_subscription':case 'edit_subscription':	
		case 'editA_subscription':case 'new_subscription':case 'renew':case 'renew_apply':
		case 'cancel_sub':	case 'config_subscriptions':
		case 'save_subconf':case 'publish_c':case 'unpublish_c':case 'new_c':case 'edit_c':
		case 'editA_c':case 'save_c':case 'remove_c':case 'cancel_c':case 'countrieslist':
		case 'save_default_tax':
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.subscriptions.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break;

		# ---	USERS	--- #
		case 'users':case 'add_user':case 'edit_user':case 'editA_user':case 'apply_user':
		case 'save_user':case 'cancel_user':case 'del_user':		
		# ---	GROUPS	--- #
		case 'classes':case 'add_class':case 'edit_class':case 'editA_class':case 'del_class':
		case 'apply_class':case 'save_class':case 'cancel_class':case 'view_class':case 'viewA_class':
		case 'remove_from_class':case 'remove_user':case 'add_to_class':case 'add_new_to_class':
		case 'view_class_users':case 'view_class_users_groups':case 'view_class_users_courses':
		case 'add_stu_to_group':case 'add_stu_to_course':case 'edit_stu_in_course':case 'save_stu_in_group':
		case 'remove_stu_from_group':case 'cancel_user_in_group':case 'cancel_user_in_course':
		case 'view_assistants':case 'add_assistant':case 'add_stu':case 'add_user_save':case 'remove_assistant':
		case 'remove_stu':case 'remove_stu_from_course':case 'edit_stu':case 'edit_user_save':case 'cancel_stu':		
		case 'cancel_assistant':
		case 'list_courses_student':	
		# ---	CEO/PARENTS	--- #
		case 'view_childrens': case'add_child': case 'edit_child':
			
		case 'view_parents':case 'add_parent':case 'edit_parent':case 'editA_parent':
		case 'remove_parent':case 'cancel_parent':case 'save_parent':
		# ---	WAITING LISTS --- #
		case 'show_waiting_lists':case 'add_from_waiting_list':case 'remove_from_waiting_list':
		case 'orderup_waiting_list':case 'orderdown_waiting_list':
		# ---	CERTIFICATES	--- #
		case 'certificates':
		case 'edit_certificate':
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.users.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.parents.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break;
		# ---   PROCESSORS  --- #
		case 'processorslist':case 'publish_proc':case 'default_p':case 'defaulta_p':		
		case 'unpublish_proc':case 'edit_p':case 'editA_p':case 'save_p':case 'apply_p':case 'remove_p':		
		case 'cancel_p':
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.processors.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break;
		# ---   PLANS  --- #
		case 'plans':case 'duplicate_plan':
		case 'edit_plan':case 'editA_plan':
		case 'new_plan':case 'save_plan':case 'delete_plan':case 'cancel_plan':case 'publish_plan':
		case 'unpublish_plan':
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.plans.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break;	
		case 'new_discount':case 'edit_discount':case 'editA_discount':
		case 'delete_discount':case 'enable_discount':case 'disable_discount':case 'save_discount':
		case 'cancel_discount':case 'discounts':case 'new_discount_coupon':case 'edit_discount_coupon':	
		case 'editA_discount_coupon':case 'delete_discount_coupon':case 'enable_discount_coupon':	
		case 'disable_discount_coupon':case 'save_discount_coupon':case 'cancel_discount_coupon':
		case 'discount_coupons':case 'discount_coupons_statistics':
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.discounts.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break;				
		# ---   CSV  --- #
		case 'csv_operations':case 'csv_import':case 'csv_export':case 'csv_delete':
		case 'csv_do_import':case 'csv_do_export':case 'csv_do_delete':case 'csv_do_delete_yes':
		case 'csv_back_to':		
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.csv.lang', $JLMS_CONFIG->get('default_language'), 'backend' );			
		break;		
				
		# ---   COURSE BACKUPS    --- #
		case 'courses_list':case 'back':case 'view_course_backup':case 'course_export':case 'backup_download':
		case 'course_backup_gen':case 'import':case 'download':case 'course_backups_del':case 'course_import':
		JLMS_require_lang( $JLMS_LANGUAGE, 'admin.courses.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		case 'cancel_backups':		
		# ---   TOTAL BACKUPS    --- #
		case 'backup':case 'backup_generate':case 'backup_restore':case 'backups_delete':
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.backups.lang', $JLMS_CONFIG->get('default_language'), 'backend' );			
		break;		
		# ---	CONFIG	--- #		
		case 'cb_integration':case 'cb_integration_edit':case 'cb_integration_add':
		case 'cb_integration_delete':case 'cb_integration_save':
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.userprofile.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break;

		case 'menu_manage':case 'saveorder':case 'orderup':case 'orderdown':
		case 'publish_menu':case 'unpublish_menu':		
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.menumanager.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break;
		
		case 'look_feel':case 'look_feel_save':
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.appearance.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break;		

		# ---	COURSES		--- #
		case 'courses':case 'publish_course':case 'unpublish_course':case 'new_course':case 'edit_course':
		case 'editA_course':case 'save_course':case 'apply_course':case 'cancel_course':
		case 'del_course':case 'course_delete_yes':case 'courses_template':case 'courses_templ_add':
		case 'courses_templ_edit':case 'courses_templ_del':case 'courses_templ_save':case 'course_order_up':	
		case 'course_order_down':case 'course_save_order':
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.courses.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break;
		
		# ---	MULTICATEGORIES	--- #
		case 'multicat':case 'multicat_config':case 'multicat_config_save':case 'multicat_new':
		case 'multicat_edit':case 'multicat_editA':case 'multicat_apply':case 'multicat_save':
		case 'multicat_delete':
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.categories.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break;
		# ---	LANGUAGES		--- #
		case 'languages':case 'export_lang':case 'import_lang':case 'upload_lang':
		case 'del_lang':case 'cancel_lang':case 'publish_lang':case 'unpublish_lang':case 'default_lang':
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.languages.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break;		
		# ---    MESSAGE Configuration    --- #
		case 'mailsup_list':case 'mailsup_new':case 'mailsup_edit':case 'mailsup_apply':
		case 'mailsup_save':case 'mailsup_delete':case 'mailsup_conf':case 'mailsup_conf_save':
		case 'mail_main': case 'mail_iframe':
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.mail.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break;
		# ---   PLUGINS   --- #
		case 'pluginslist':case 'publish_plugin':case 'unpublish_plugin':case 'edit_plugin':
		case 'editA_plugin':case 'save_plugin':case 'apply_plugin':case 'remove_plugin': case 'install_plugin':
		case 'cancel_plugin': case 'save_plugins_order':
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.plugins.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break;
				

		# ---    NOTIFICATION SENDING  --- #				
		case 'notifications': case 'edit_notification': case 'apply_notification': 		
		case 'save_notification': case 'enable_notification': case 'disable_notification': 		
		case 'email_templates': case 'new_email_template':	case 'edit_email_template': 
		case 'apply_email_template': case 'save_email_template': case 'enable_email_template': 
		case 'disable_email_template': case 'delete_email_templates':
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.notifications.lang', $JLMS_CONFIG->get('default_language'), 'backend' );
		break; 
						
		case 'about':case 'support': default:
			JLMS_require_lang( $JLMS_LANGUAGE, 'admin.about.lang', $JLMS_CONFIG->get('default_language'), 'backend' );		
		break;        
	}	
	
	JLMS_processLanguage( $JLMS_LANGUAGE, false, 'backend' );

	$option = 'com_joomla_lms';
				
	switch ($task)
	{	    
		case 'lms_forums':
				require_once(_JOOMLMS_ADMIN_HOME.'/files/admin.forums.php');
		break;

		case 'lms_roles':
				require_once(_JOOMLMS_ADMIN_HOME.'/files/admin.roles.php');
		break;
		
		case 'lms_users':
				require_once(_JOOMLMS_ADMIN_HOME.'/files/admin.users.php');
		break;
		
		case 'group_managers':
				require_once(_JOOMLMS_ADMIN_HOME.'/files/admin.group_managers.php');
		break;
		
		case 'lms_maintenance':
				require_once(_JOOMLMS_ADMIN_HOME.'/files/admin.maintenance.php');
		break;
		
		case 'dev_config':
				require_once(_JOOMLMS_ADMIN_HOME.'/files/admin.dev_config.php');
		break;
		
		# ---	PAGE TIP MANAGEMENT	--- #
		case 'page_tips':			JLMS_PageTipsList( $option );						break;
		case 'edit_ptip':			JLMS_editPageTip( intval($cid[0]), $option );		break;
		case 'editA_ptip':			JLMS_editPageTip( $id, $option );					break;
		case 'new_ptip':			JLMS_editPageTip( 0, $option );						break;
		case 'cancel_ptip':			mosRedirect("index.php?option=$option&task=page_tips"); break;
		case 'apply_ptip':
		case 'save_ptip':			JLMS_savePageTip($option, $task);					break;
		case 'del_ptip':			JLMS_delPageTip( $cid[0], $option );					break;

		# ---	ROLES MANAGEMENT	--- #
		case 'roles':				JLMS_RolesList( $option );							break;
		case 'edit_role':			JLMS_editRole( intval($cid[0]), $option );			break;
		case 'editA_role':			JLMS_editRole( $id, $option );						break;
		case 'new_role':			JLMS_editRole( 0, $option );						break;
		case 'cancel_role':			mosRedirect("index.php?option=$option&task=roles"); break;
		case 'apply_role':
		case 'save_role':			JLMS_saveRole($option, $task);						break;
		case 'del_role':			JLMS_delRole( intval($cid[0]), $option );			break;


		# ---	PAYMENTS	--- #
		case 'new_payment':			JLMS_CreateNewPayment( $option );					break;
		case 'save_newpayment':		JLMS_SaveNewPayment( $option );						break;
		
		case 'sales_report_pdf':
		case 'sales_report':		JLMS_salesReport( $option );						break;		

		case 'cancel_newpayment':
				mosRedirect("index.php?option=$option&task=payments");
		break;
		
		case 'pays_list_pdf':
		case 'pays_list_xls':
		case 'payments':			JLMS_PaymentsList( $option );						break;
		
		case 'edit_payment':		JLMS_EditPaymentInfo( $cid[0], $option );			break;
		case 'editA_payment':		JLMS_EditPaymentInfo( $id, $option );				break;
		case 'del_payments':		JLMS_removeFromPayments( $option );					break;
		case 'save_payment':		JLMS_savePaymentInfo( $option );					break;
		case 'cancel_payment':
		case 'skip_change':
		mosRedirect("index.php?option=$option&task=payments");
		break;
		case 'apply_change':		JLMS_changePaymentInfo_FOR_user($option,$cid,$id);	break;
		case 'get_payment_invoice':	JLMS_getPaymentInvoice($id, $option);				break;
		case 'gen_payment_invoice':	JLMS_GenerateNewInvoice($id, $option);				break;

		# ---	SUBSCRIPTIONS	--- #
		
		case 'assign':				JLMS_assign( 0, $option );							break;
		case 'save_assign':			JLMS_saveAssign($option);							break;
		case 'cancel_assign':		JLMS_cancelAssign($option);							break;
		case 'subscriptions':		JLMS_ListSubscriptions( $option );					break;
		case 'subscription_save':	JLMS_saveSubscription( $option );					break;
		case 'subscription_apply':	JLMS_saveSubscription( $option, true );					break;
		case 'publish_subscription':JLMS_changeSubscriptions( $cid, 1, $option );		break;
		case 'delete_subscription':	JLMS_removeFromSubscriptions( $option );			break;
		case 'unpublish_subscription':JLMS_changeSubscriptions( $cid, 0, $option );		break;
		case 'edit_subscription':	JLMS_NewEditSubscription( $cid[0], $option );		break;
		case 'editA_subscription':	JLMS_NewEditSubscription( $id, $option );			break;
		case 'new_subscription':	JLMS_NewEditSubscription( $cid[0], $option );		break;
		case 'renew':				JLMS_ReNewSubscription( $cid[0], $option );			break;
		case 'renew_apply':			JLMS_ReNewSubscriptionApply( $cid, $id, $option );	break;
		case 'cancel_sub':			cancelSubscription(  $option );						break;
		case 'processorslist':		JLMS_showProcessorsList( $option );					break;
		case 'default_p':			changeProcessor( $option, $id );					break;
		case 'defaulta_p':			changeProcessor($option, $cid[0]);					break;
		case 'publish_proc':		changeProc( $cid, 1, $option );						break;
		case 'unpublish_proc':		changeProc( $cid, 0, $option );						break;
		case 'edit_p':				editProcessor( intval( $cid[0] ), $option );		break;
		case 'editA_p':				editProcessor( $id, $option );						break;
		case 'save_p':
		case 'apply_p':				saveProcessor( $option ,$task);						break;		
		case 'cancel_p':			cancelProcessor( $id );								break;
		case 'plans':				JLMS_showPlans($option);							break;
		case 'duplicate_plan':		JLMS_duplicatePlan( $cid, $option );				break;		
		case 'edit_plan':			JLMS_editPlan( intval( $cid[0] ), $option);			break;		
		case 'editA_plan':			JLMS_editPlan( $id, $option );						break;
		case 'new_plan':			JLMS_editPlan( '0', $option);						break;		
		case 'save_plan':			JLMS_savePlan( $option );							break;		
		case 'delete_plan':			JLMS_removePlan( $cid, $option );					break;		
		case 'cancel_plan':			JLMS_cancelPlan();									break;
		case 'publish_plan':		JLMS_changePlan( $cid, 1, $option );				break;		
		case 'unpublish_plan':		JLMS_changePlan( $cid, 0, $option );				break;
		case 'new_discount':		JLMS_editDiscount(0, $option);						break;		
		case 'edit_discount':		JLMS_editDiscount(intval( $cid[0] ), $option);		break;
		case 'editA_discount':		JLMS_editDiscount($id, $option);					break;
		case 'delete_discount':		JLMS_deleteDiscount($cid, $option);					break;
		case 'enable_discount':		JLMS_changeDiscount($cid, 1, $option);				break;
		case 'disable_discount':	JLMS_changeDiscount($cid, 0, $option);				break;		
		case 'save_discount':		JLMS_saveDiscount($option);							break;
		case 'cancel_discount':		JLMS_cancelDiscount($option);						break;		
		case 'discounts':			JLMS_showDiscounts($option);						break;		
		case 'new_discount_coupon':			JLMS_editDiscountCoupon(0, $option);						break;		
		case 'edit_discount_coupon':		JLMS_editDiscountCoupon(intval( $cid[0] ), $option);		break;
		case 'editA_discount_coupon':		JLMS_editDiscountCoupon($id, $option);						break;
		case 'delete_discount_coupon':		JLMS_deleteDiscountCoupon($cid, $option);					break;
		case 'enable_discount_coupon':		JLMS_changeDiscountCoupon($cid, 1, $option);				break;
		case 'disable_discount_coupon':		JLMS_changeDiscountCoupon($cid, 0, $option);				break;		
		case 'save_discount_coupon':		JLMS_saveDiscountCoupon($option);							break;
		case 'cancel_discount_coupon':		JLMS_cancelDiscountCoupon($option);							break;		
		case 'discount_coupons':			JLMS_showDiscountCoupons($option);							break;
		case 'discount_coupons_statistics':	JLMS_showDiscountCouponsStatistics($option);				break;				
		
		case 'config_subscriptions':configSubscription($option);						break;
		case 'save_subconf':		saveSubsConfig($option);							break;
		// countries
		case 'publish_c':			jlms_changeCountry( $cid, 1, $option );				break;
		case 'unpublish_c':			jlms_changeCountry( $cid, 0, $option );				break;
		case 'new_c':				jlms_editCountry( '0', $option);					break;
		case 'edit_c':				jlms_editCountry( intval( $cid[0] ), $option );		break;
		case 'editA_c':				jlms_editCountry( $id, $option );					break;
		case 'save_c':				jlms_saveCountry( $option );						break;
		case 'remove_c':			jlms_removeCountry( $cid, $option );				break;
		case 'cancel_c':			jlms_cancelCountry();								break;
		case 'countrieslist':		jlms_showCountriesList( $option );					break;
		case 'save_default_tax':	jlms_saveDefaultTax( $option );						break;

		# ---	USERS	--- #
		case 'users':				mosRedirect("index.php?option=$option&task=lms_users");	break;
		case 'add_user':			JLMS_editUser( 0, $option );						break;
		case 'edit_user':			JLMS_editUser( intval( $cid[0] ), $option );		break;
		case 'editA_user':			JLMS_editUser( $id, $option );						break;
		case 'apply_user':
		case 'save_user':			JLMS_saveUser( $option );							break;
		case 'cancel_user':			JLMS_cancelUser( $option );							break;
		case 'del_user':			JLMS_removeUser( $cid, $option );					break;

		# ---   CSV  --- #
		case 'csv_operations':		jlms_listOperation( -1, $option );					break;
		case 'csv_import':			jlms_listOperation( 0, $option );					break;
		case 'csv_export':			jlms_listOperation( 1, $option );					break;
		case 'csv_delete':			jlms_listOperation( 2, $option );					break;
		case 'csv_do_import':		jlms_csvImport( $option );							break;
		case 'csv_do_export':		jlms_csvExport( $option );							break;
		case 'csv_do_delete':		jlms_csvDelete( $option );							break;
		case 'csv_do_delete_yes':	jlms_csvDelete_yes( $option );						break;
		case 'csv_back_to':			jlms_csvBackTo( $option );							break;

		# ---	GROUPS	--- #
		case 'classes':				JLMS_ListClasses( $option );						break;
		case 'add_class':			JLMS_editClass( 0, $option );						break;
		case 'edit_class':			JLMS_editClass( intval( $cid[0] ), $option );		break;
		case 'editA_class':			JLMS_editClass( $id, $option );						break;
		case 'del_class':			JLMS_delClass( $cid, $option );						break;
		case 'apply_class':
		case 'save_class':			JLMS_saveClass( $option );							break;
		case 'cancel_class':		JLMS_cancelClass( $option );						break;
		case 'view_class':			JLMS_viewClass(0, $option );						break;
		case 'viewA_class':			JLMS_viewClass( $cid[0], $option );					break;
		case 'remove_from_class':	JLMS_removeFromClass( $option );					break;
		case 'remove_user':			JLMS_removeFromCourse( $option );					break;
		case 'add_to_class':		JLMS_addToClass( $option );							break;
		case 'add_new_to_class':	JLMS_addnewToClass( $option );						break;

		case 'view_class_users':	JLMS_viewClassUsers( $option );						break;
		case 'view_class_users_groups': JLMS_viewUsersInGroups( $option );				break;
		case 'view_class_users_courses': JLMS_viewUsersInCourses( $option );			break;
		case 'add_stu_to_group':	JLMS_addUserToGlobalGroup( $option );				break;
		case 'add_stu_to_course':	JLMS_addUserToCourse( $option );					break;
		
		case 'list_courses_student':JLMS_List_Courses_Student($option);					break;
		
		case 'edit_stu_in_course':	JLMS_showEditUser( $option, 1);						break;
		case 'save_stu_in_group':	JLMS_saveUserInGlobalGroup( $option );				break;
		case 'remove_stu_from_group':	JLMS_removeUsersFromGlobalGroup( $cid, $option );	break;
		case 'cancel_user_in_group':	mosRedirect("index.php?option=$option&task=view_class_users_groups"); break;
		case 'cancel_user_in_course':	mosRedirect("index.php?option=$option&task=view_class_users_courses"); break;

		case 'view_assistants':		JLMS_viewAssistants( $option );						break;
		case 'add_assistant':		JLMS_showAddUser( $option, 2 );						break;
		case 'add_stu':				JLMS_showAddUser( $option, 1 );						break;
		case 'add_user_save':		JLMS_addUserToGroup( $option );						break;
		case 'remove_assistant':	JLMS_removeAssistant( intval($cid[0]), $option );	break;
		case 'remove_stu':			JLMS_removeStudent( $option );						break;
		case 'remove_stu_from_course':	JLMS_removeStudent( $option );				break;
		case 'edit_stu':			JLMS_showEditUser( $option, 1);						break;
		case 'edit_user_save':		JLMS_EditUserSave( $option );						break;
		case 'cancel_stu':
		mosRedirect("index.php?option=$option&task=view_class_users");
		break;
		case 'cancel_assistant':
		mosRedirect("index.php?option=$option&task=view_assistants");
		break;
		# --- CEO/PARENTS --- #
		case 'view_childrens':		JLMS_viewChildrens( $option );						break;
		case 'add_child':
		case 'edit_child':			JLMS_editChildren( $option );						break;
		case 'save_child':			JLMS_saveChildren( $option );						break;	
		case 'delte_child':			JLMS_deleteChildren( $option );						break;	
		
		case 'view_parents':		JLMS_viewParents( $option );						break;
		case 'add_parent':			JLMS_editParent( 0, $option );						break;
		case 'edit_parent':			JLMS_editParent( intval($cid[0]), $option );		break;
		case 'editA_parent':		JLMS_editParent( $id, $option );					break;
		
		case 'delete_parent':		JLMS_deleteParent($option);							break;
		
		case 'remove_parent':		JLMS_removeParent( $cid, $option );					break;
		case 'cancel_parent':
		mosRedirect("index.php?option=$option&task=view_parents");
		break;
		case 'save_parent':			JLMS_saveParent( $option );							break;

		# ---   COURSE BACKUPS    --- #
		case 'courses_list':		JLMS_coursesList( $option ); 						break;
		case 'back': mosRedirect("index.php?option=$option&task=courses_list");		break;
		case 'view_course_backup':
		JLMS_courseBackups( $option, $id );											break;
		case 'course_export':
		require_once (_JOOMLMS_INCLUDES_PATH."jlms_course_export.php");
		JLMS_courseExport( $option, $id, 'exp');			break;

		case 'backup_download':
		require_once (_JOOMLMS_INCLUDES_PATH."jlms_download.php");
		JLMS_download(  $backup_name);						break;

		case 'course_backup_gen':
		require_once (_JOOMLMS_INCLUDES_PATH."jlms_course_export.php");
		JLMS_courseExport( $option, $id, 'gen');
		mosRedirect ("index.php?option=$option&task=view_course_backup&id=$id", _JLMS_BCK_MSG_HAS_BEEN_CREATED );
		break;
		case 'import':				joomla_lms_adm_html::JLMS_courseImport( $option );	break;
		case 'download':			$backup_id = mosGetParam($_REQUEST, 'backup_id', 0);
		JLMS_courseDownload(  $option, $backup_id );		break;
		case 'course_backups_del':	JLMS_coursebackupsDelete( $cid, $option, $id );		break;
		case 'course_import':
				
		if (class_exists('JParameter')) {
			require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.params.new.php");
		} else {
			require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.params.php");
		}
		require_once (_JOOMLMS_INCLUDES_PATH."jlms_course_import.php"); // !!! in this file present some LANGUAGE CONSTANTS
		JLMS_courseImport( $option );
		mosRedirect ("index.php?option=com_joomla_lms&task=import", _JLMS_MSG_COURSE_HAS_BEEN_IMPORTED );
		break;
		case 'cancel_backups':  	JLMS_cancelBackups( $option );						break;
		# ---   TOTAL BACKUPS    --- #
		case 'backup':				JLMS_backupsList( $option );   						break;
		case 'backup_generate':		JLMS_backupGenerate( $option );					    break;
		case 'backup_restore':		JLMS_backupRestore( $cid[0], $option );				break;
		case 'backups_delete':		JLMS_backupDelete( $cid, $option );					break;
		
		# ---	CONFIG	--- #
		//case 'frontpage':			JLMS_frontpageSetup( $option );						break;
		//case 'fp_save':				JLMS_frontpageSetup_save( $option );			break;
		case 'cb_integration':		JLMS_cb_integration( $option );						break;
		case 'cb_integration_edit':	JLMS_cb_integration_edit( $cid[0], $option );		break;
		case 'cb_integration_add':	JLMS_cb_integration_edit( 0, $option );				break;
		case 'cb_integration_delete':	JLMS_cb_integration_delete( $cid, $option );	break;
		case 'cb_integration_save':	JLMS_cb_integration_save( $option );				break;

		case 'menu_manage':			JLMS_menuManage( $option );							break;
		case 'saveorder':			JLMS_saveOrder( $cid );								break;
		case 'orderup':				JLMS_orderMenu( intval( $cid[0] ), -1, $option );	break;
		case 'orderdown':			JLMS_orderMenu( intval( $cid[0] ), 1, $option );	break;
		case 'publish_menu':		JLMS_changeMenu( $cid, 1, $option );				break;
		case 'unpublish_menu':		JLMS_changeMenu( $cid, 0, $option );				break;
		case 'config':				JLMS_config( $option );		   						break;
		case 'config_save':
		case 'config_apply':		JLMS_config_save( $option );			   			break;
		case 'look_feel':			JLMS_Look_Feel($option);							break;	
		case 'look_feel_save':		JLMS_Look_Feel_Save($option);						break;	
        case 'uploadunicode':       JLMS_uploadUnicode();                               break;	

		# ---	COURSES		--- #
		case 'courses':				JLMS_ListCourses( $option );						break;
		case 'publish_course':		JLMS_changeCourse( $cid, 1, $option );				break;
		case 'unpublish_course':	JLMS_changeCourse( $cid, 0, $option );				break;
		case 'new_course':			JLMS_newCourse( $option );							break;
		case 'edit_course':			JLMS_editCourse( $cid[0],  $option );				break;
		case 'editA_course':		JLMS_editCourse( $id,  $option );					break;
		case 'save_course':			JLMS_saveCourse( $id,  $option ,$task );			break;
		case 'apply_course':		JLMS_saveCourse( $id,  $option, $task );			break;
		case 'cancel_course':		JLMS_cancelCourse( );								break;
		case 'del_course':			JLMS_pre_DeleteCourse( $cid[0], $option );			break;
		case 'course_delete_yes': 	JLMS_deleteCourse( $cid[0], $option );				break;
		case 'courses_template':	JLMS_ListCoursesTemplate( $option );				break;
		case 'courses_templ_add':	JLMS_ListCoursesTemplAdd( 0	, $option );			break;
		case 'courses_templ_edit':	JLMS_ListCoursesTemplAdd( $cid[0], $option );		break;
		case 'courses_templ_del':	JLMS_ListCoursesTemplDel( $cid, $option );			break;
		case 'courses_templ_save':	JLMS_ListCoursesTemplSave( $option );				break;
		//---course ordering---//
		case 'course_order_up':		JLMS_orderCourse( intval( $cid[0] ), -1, $option );	break;
		case 'course_order_down':	JLMS_orderCourse( intval( $cid[0] ), 1, $option );	break;
		case 'course_save_order':	JLMS_course_saveOrder( $cid, $option );				break;
		
		# ---	MULTICATEGORIES	--- #
		case 'multicat':			FLMS_ListCategories( $option );						break;
		case 'multicat_config':		FLMS_CategoriesConfig( $option );					break;
		
		case 'multicat_config_save':FLMS_CategoriesConfigSave( $option );				break;
		
		case 'multicat_new':		FLMS_EditCategories( 0, $option );					break;
		case 'multicat_edit':		FLMS_EditCategories( $cid[0], $option );			break;
		case 'multicat_editA':		FLMS_EditCategories( $id, $option );				break;
		case 'multicat_apply':
		case 'multicat_save':		FLMS_SaveCategories( $option, $task );				break;
		case 'multicat_delete':		FLMS_DeleteCategories( $cid, $option );				break;
		

		# ---	LANGUAGES		--- #
		case 'languages':			JLMS_ListLangs( $option );							break;
		case 'default_lang':		JLMS_makeLangDefault( $cid[0], $option );			break;		
		case 'export_lang':			JLMS_ExportLang( $cid[0], $option );				break;
		case 'import_lang':
		joomla_lms_adm_html::JLMS_showImportLang( $option );
		break;
		case 'upload_lang':			JLMS_UploadLang( $option );							break;
		case 'del_lang':			JLMS_DeleteLang( $cid[0], $option );				break;
		case 'cancel_lang':
		mosRedirect( "index.php?option=com_joomla_lms&task=languages" );
		break;
		case 'publish_lang':		JLMS_changeLang( $cid, 1, $option );				break;
		case 'unpublish_lang':		JLMS_changeLang( $cid, 0, $option );				break;
		# ---	CERTIFICATES	--- #
		case 'certificates':		JLMS_ShowCertificatesList( $option );				break;
		case 'edit_certificate':	JLMS_editCertificate( $cid[0], $option );			break;
		case 'save_certificate':	JLMS_saveCertificate( $id, $option );				break;

		# ---    MESSAGE Configuration    --- #
		case 'mailsup_list':		JLMS_MailSupList($option);							break;
		case 'mailsup_new':			JLMS_MailSupEdit(0, $option);						break;
		case 'mailsup_edit':		JLMS_MailSupEdit($cid[0], $option);					break;
		case 'mailsup_apply':
		case 'mailsup_save':		JLMS_MailSupSave($option,$task);					break;
		case 'mailsup_delete':		JLMS_MailSupDelete($cid, $option);					break;
		case 'mailsup_conf':		JLMS_MailSupConf($option);							break;
		case 'mailsup_conf_save':	JLMS_MailSupConfSave($option);						break;

		# ---   PLUGINS   --- #
		case 'pluginslist':			JLMS_showPluginsList( $option );					break;
		case 'publish_plugin':		changePlugin( $cid, 1, $option );					break;
		case 'unpublish_plugin':	changePlugin( $cid, 0, $option );					break;
		case 'edit_plugin':			editPlugin( intval( $cid[0] ), $option );			break;
		case 'editA_plugin':		editPlugin( $id, $option );							break;
		case 'save_plugin':
		case 'apply_plugin':		savePlugin( $option ,$task);						break;
		case 'remove_plugin':		removePlugin( $cid, $option );						break;
		case 'cancel_plugin':		cancelPlugin( $id, $option );						break;
		case 'install_plugin':		installPlugin( $id, $option );						break;
		case 'save_plugins_order':	savePluginsOrder( $option );						break;
		case 'pluginsorderup':		pluginsReorder( $option, true );					break;
		case 'pluginsorderdown':	pluginsReorder( $option );							break;		
		

		# ---    WAITING LISTS  --- #
		case 'show_waiting_lists':	showWaitingLists( $option );						break;
		case 'add_from_waiting_list': addUserFromWaitingList($cid, $option);			break;
		case 'remove_from_waiting_list': removeUsersFromWaitingList($cid, $option);		break;
		case 'orderup_waiting_list': orderWaitingListElement($id, -1, $option);			break;
		case 'orderdown_waiting_list': orderWaitingListElement($id, 1, $option);		break;

		# ---    NOTIFICATION SENDING  --- #
		case 'mail_main':
		$redirect_url = urldecode( mosGetParam($_REQUEST, 'redirect') );
		$user_id = mosGetParam($_REQUEST, 'assigned', '-1');
		$mail_object = new JLMS_Mail($db, $redirect_url, 'back');
		$mail_object->setAssigned($user_id);
		$mail_object->showPage();
		break;
		case 'mail_iframe':
		$redirect_url = urldecode( mosGetParam($_REQUEST, 'redirect') );
		$user_id = mosGetParam($_REQUEST, 'assigned', '-1');
		$mail_object = new JLMS_Mail($db, $redirect_url, 'back');
		$mail_object->setAssigned($user_id);
		$mail_object->getMails();
		$mail_object->showIFrame();
		die;
		break;
		case 'notifications': JLMS_ListNotifications(); break;
		case 'edit_notification': JLMS_EditNotification( intval($cid[0])?intval($cid[0]):$id ); break;
		case 'apply_notification': 		
		case 'save_notification': JLMS_SaveNotification( $id); break;
		case 'enable_notification': JLMS_EnableNotification( $id ); break;
		case 'disable_notification': JLMS_EnableNotification( $id, false ); break;
		
		case 'email_templates': JLMS_ListEmailTemplates(); break;
		case 'new_email_template': 
		case 'edit_email_template': JLMS_EditEmailTemplate( intval($cid[0])?intval($cid[0]):$id ); break;
		case 'apply_email_template': 		
		case 'save_email_template': JLMS_SaveEmailTemplate( $id ); break;
		case 'enable_email_template': JLMS_EnableEmailTemplate( $id ); break;
		case 'disable_email_template': JLMS_EnableEmailTemplate( $id, false ); break;
		case 'delete_email_templates': JLMS_DeleteEmailTemplates( $cid ); break;
		
		# ---    Helper Users List    --- #
		case 'users_list':			Helper_UsersList::getUsers();						break;

		# ---    XREHb    --- #
		case 'about':				jlms_ViewAboutPage($option);						break;
		case 'support':				joomla_lms_adm_html::View_SupportPage();			break;
		#case 'help':				joomla_lms_adm_html::View_HelpPage();				break;
		#default:					joomla_lms_adm_html::HomePage();					break;
		case 'latestVersion':		jlms_latestVersion();								break;
		default:					jlms_ViewAboutPage($option);						break;
	}
}
?>