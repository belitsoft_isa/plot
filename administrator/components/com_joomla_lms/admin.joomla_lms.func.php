<?php
/**
* admin.joomla_lms.func.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/
				
// no direct access
if (!defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) { die( 'Restricted access' ); }

function JLMS_assign($id, $option) 
{
	$db = JFactory::getDBO();

	$cid 	= mosGetParam( $_POST, 'cid', mosGetParam( $_GET, 'cid', array(0) ) );
	if (!is_array( $cid )) {
		$cid = array(0);
	}
	$sub_id = $cid[0];

	if(!$sub_id) {
		$sub_id = intval(mosGetParam($_REQUEST,'sub_id',0));
	}
 
	$usergroup = intval(mosGetParam($_REQUEST,'usergroup',0));

	$query = "SELECT sub_name FROM #__lms_subscriptions WHERE id='".$sub_id."'";
	$db->SetQuery($query);
	$sub_name = $db->LoadResult();

	$query = "SELECT a.id as value, a.ug_name as text FROM #__lms_users_in_global_groups as b, #__lms_usergroups as a where a.course_id = 0 AND a.id = b.group_id group by a.ug_name order by a.ug_name";
	$db->setQuery( $query );
	$u_users = $db->loadObjectList();
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_SLCT_USRGR);
	$list_users = array_merge( $list_users, $u_users );
	$lists['usergroups'] = mosHTML::selectList( $list_users, 'usergroup', 'class="text_area" size="1" style="width:266px" onchange="jlms_changeUsergroup(this);"', 'value', 'text', $usergroup );

	if($usergroup) {
		$query = "SELECT a.id as value, a.username as text FROM #__users as a, #__lms_users_in_global_groups as b where b.group_id = '".$usergroup."' AND b.user_id = a.id order by a.username";
	}
	else {
		$query = "SELECT id as value, username as text FROM #__users order by username";
	}
	$db->setQuery( $query );
	$u_users = $db->loadObjectList();
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_SLCT_USER_);
	$list_users = array_merge( $list_users, $u_users );
	$lists['users'] = mosHTML::selectList( $list_users, 'user_id', 'class="text_area" size="1" style="width:266px" onchange="jlms_changeUserSelect(this);"', 'value', 'text', '' );

	if($usergroup) {
		$query = "SELECT a.id as value, a.name as text FROM #__users as a, #__lms_users_in_global_groups as b where b.group_id = '".$usergroup."' AND b.user_id = a.id order by a.name";
	}
	else {
		$query = "SELECT id as value, name as text FROM #__users ORDER BY name";	
	}

	$db->SetQuery($query);
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_NAME_);
	$pr = $db->loadObjectList();
	$list_users = array_merge( $list_users, $pr );
	$lists['users_names'] = mosHTML::selectList( $list_users, 'user_name', 'class="text_area" style="width:266px" size="1" onchange="jlms_changeUserSelect(this);"', 'value', 'text', '' );

	if($usergroup) {
		$query = "SELECT a.id as value, a.email as text FROM #__users as a, #__lms_users_in_global_groups as b where b.group_id = '".$usergroup."' AND b.user_id = a.id order by a.email";
	}
	else {
		$query = "SELECT id as value, email as text FROM #__users ORDER BY email";	
	}

	$db->SetQuery($query);
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_OR_EMAIL_);
	$pr = $db->loadObjectList();
	$list_users = array_merge( $list_users, $pr );
	$lists['users_emails'] = mosHTML::selectList( $list_users, 'user_email', 'class="text_area" style="width:266px" size="1" onchange="jlms_changeUserSelect(this);"', 'value', 'text', '' );

	joomla_lms_adm_html::JLMS_assign($row, $lists, $option, $sub_name, $sub_id);
}


function JLMS_saveAssign($option) {
	$db = JFactory::getDBO();

	if (class_exists('JParameter')) {
		require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.params.new.php");
	} else {
		require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.params.php");
	}
	require_once (_JOOMLMS_FRONT_HOME . DS . "includes" . DS ."joomla_lms.subscription.lib.php");

	$sub_id = intval(mosGetParam($_REQUEST,'sub_id',0));
	$user_id = intval(mosGetParam($_REQUEST,'user_id',0));
	$user_name = intval(mosGetParam($_REQUEST,'user_name',0));
	$user_email = intval(mosGetParam($_REQUEST,'user_email',0));
	$group_id = intval(mosGetParam($_REQUEST,'usergroup',0));

	if ($sub_id) {
		$users = array();
		if($user_id) {
			$users[] = $user_id;
		} elseif($user_name) {
			$user_id = $user_name;
			$users[] = $user_id;
		} elseif ($user_email) {
			$user_id = $user_email;
			$users[] = $user_id;
		} elseif ($group_id) {
			$query = "SELECT distinct user_id FROM #__lms_users_in_global_groups WHERE group_id = $group_id";
			$db->SetQuery($query);
			$users = JLMSDatabaseHelper::loadResultArray();
		}

		if (count($users)) {
			foreach ($users as $new_user_id) {
				$payment_info = new stdClass();
				$payment_info->id = 0;
				$payment_info->payment_type = 0;
				$payment_info->sub_id = $sub_id;
				$payment_info->user_id = $new_user_id;
				$payment_info->date = date("Y-m-d H:i:s");
					
				jlms_enroll_user_by_payment($payment_info, $db, 0, 0);
			}
		}
	}

	mosRedirect( "index.php?option=com_joomla_lms&task=subscriptions" );
}

function jlms_ViewAboutPage($option) {
	global $jlms_license_expires_str;
	$expired_str = $jlms_license_expires_str;
	$users_str = '<span style = "font-weight:bold; color:green">'._JLMS_UNLIMITED.'</span>';
	global $license_lms_users;
	if ($license_lms_users) {
		$db = JFactory::getDBO();
		$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
		$db->SetQuery( $query );
		$total_students = $db->LoadResult();
		$ex_color = 'green';
		if ( ($license_lms_users*80/100) < $total_students ) {
			$ex_color = 'red';
		}
		$users_str = '<span style = "font-weight:bold; color:'.$ex_color.'">'.$total_students."</span> "._JLMS_LICENSE_USERS_OF." <span style = 'font-weight:bold; color:".$ex_color."'>".$license_lms_users."</span>";
	}
	joomla_lms_adm_html::View_AboutPage($expired_str, $users_str);
}

/**
 * * * * * * * * * * User ROLES MANAGEMENT * * * * * * * * * * *
 * 
 * 24.01.2008 - (DEN)
 * 
 */

function JLMS_PageTipsList( $option ) {
	$db = JFactory::getDBO();
	$query = "SELECT * FROM #__lms_page_tips";// ORDER BY roletype_id, lms_usertype";
	$db->SetQuery($query);
	$ptips = $db->loadObjectList();
	$lists = array();
	joomla_lms_adm_html::JLMS_showPageTipsList( $ptips, $option, $lists );
}

function JLMS_editPageTip($id, $option) {
	$db = JFactory::getDBO();
	$GLOBALS['jlms_toolbar_id'] = $id;
	$row = new mos_Joomla_LMS_pagetip( $db );
	// load the row from the db table
	$row->load( $id );
	$lists = array();
	joomla_lms_adm_html::jlms_editPageTip( $row, $lists, $option );
}

function JLMS_delPageTip($id, $option) {
	$db = JFactory::getDBO();
	$query = "DELETE FROM #__lms_page_tips WHERE id = $id";
	$db->SetQuery($query);
	$db->query();
	mosRedirect( "index.php?option=$option&task=page_tips", _JLMS_TIPS_MSG_TIP_REMOVED );
}

function JLMS_savePageTip( $option, $task) {
	$db = JFactory::getDBO();

	$row = new mos_Joomla_LMS_pagetip( $db );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	$row->tip_message = get_magic_quotes_gpc() ? stripslashes( $row->tip_message ) : $row->tip_message;
	$row->course_id = 0;
	$row->user_id = 0;

	if (!$row->check()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	if ($task == 'apply_ptip') {
		mosRedirect( "index.php?option=$option&task=editA_ptip&id=".$row->id );
	} else {
		mosRedirect( "index.php?option=$option&task=page_tips" );
	}
}

/**
 * * * * * * * * * * User ROLES MANAGEMENT * * * * * * * * * * *
 * 
 * 08.10.2007 - (DEN)
 * 
 */
function JLMS_RolesList( $option ) {
	$db = JFactory::getDBO();
	$query = "SELECT * FROM #__lms_usertypes WHERE roletype_id <> 0 ORDER BY roletype_id, lms_usertype";
	$db->SetQuery($query);
	$roles = $db->loadObjectList();
	$lists = array();
	joomla_lms_adm_html::JLMS_showRolesList( $roles, $option, $lists );
}

function JLMS_editRole($id, $option) {
	$db = JFactory::getDBO();
	$GLOBALS['jlms_toolbar_id'] = $id;
	$row = new mos_Joomla_LMS_userrole( $db );
	// load the row from the db table
	$row->load( $id );

	$lists = array();
	if (!$id) {
		$row->roletype_id = 0;
	}
	
	$role_types = array();
	$role_types[] = mosHTML::makeOption( 0, _JLMS_ROLES_SLCT_ROLE_TYPE_ );
	$role_types[] = mosHTML::makeOption( 4, _JLMS_ROLES_ADMIN_ROLE );
	$role_types[] = mosHTML::makeOption( 2, _JLMS_ROLES_TEACHER_ROLE );
	$role_types[] = mosHTML::makeOption( 5, _JLMS_ROLES_ASSISTANT_ROLE );
	$lists['role_type'] = mosHTML::selectList($role_types, 'roletype_id', 'class="text_area" size="1" style="width:266px;" ', 'value', 'text', $row->roletype_id );

	joomla_lms_adm_html::jlms_editRole( $row, $lists, $option );
}

function JLMS_delRole($id, $option) {
	$db = JFactory::getDBO();

	$row = new mos_Joomla_LMS_userrole( $db );
	// load the row from the db table
	$row->load( $id );
	if ($row->roletype_id == 1) {
		$query = "SELECT count(*) FROM #__lms_usertypes WHERE roletype_id = 1";
		$db->SetQuery($query);
		$role_nums = $db->LoadResult();
		if ($role_nums == 1) {
			mosRedirect( "index.php?option=$option&task=roles", _JLMS_ROLES_MSG_CANT_REM_ALL );
			die;
		}
		$can_remove = true;
		$query = "SELECT count(*) FROM #__lms_users_in_groups WHERE role_id = $id";
		$db->SetQuery($query);
		$role_nums = $db->LoadResult();
		if ($role_nums) {
			mosRedirect( "index.php?option=$option&task=roles", _JLMS_ROLES_MSG_ROLE_IN_USE );
			die;
		}
		$query = "SELECT count(*) FROM #__lms_spec_reg_questions WHERE role_id = $id";
		$db->SetQuery($query);
		$role_nums = $db->LoadResult();
		if ($role_nums) {
			mosRedirect( "index.php?option=$option&task=roles", _JLMS_ROLES_MSG_ROLE_IN_USE );
			die;
		}
		$query = "SELECT count(*) FROM #__lms_certificates WHERE parent_id <> 0 AND crtf_type = $id";
		$db->SetQuery($query);
		$role_nums = $db->LoadResult();
		if ($role_nums) {
			mosRedirect( "index.php?option=$option&task=roles", _JLMS_ROLES_MSG_ROLE_IN_USE );
			die;
		}
	} else {
		mosRedirect( "index.php?option=$option&task=roles", _JLMS_ROLES_MSG_CANT_REM_ONLY_L );
		die;
	}
	$query = "DELETE FROM #__lms_usertypes WHERE id = $id AND roletype_id = 1";
	$db->SetQuery($query);
	$db->query();
	mosRedirect( "index.php?option=$option&task=roles", _JLMS_ROLES_MSG_ROLE_REMOVED );
}

function JLMS_saveRole( $option, $task) {
	$db = JFactory::getDBO();

	$row = new mos_Joomla_LMS_userrole( $db );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if (!$row->check()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if ($task == 'apply_role') {
		mosRedirect( "index.php?option=$option&task=editA_role&id=".$row->id );
	} else {
		mosRedirect( "index.php?option=$option&task=roles" );
	}
}






/**
 * * * * * * * * * * * CERTIFICATES SECTION * * * * * * * * * * * 
 * */

/**
 * Shows list of all site certificates.
 * @param unknown_type $option
 */

function ShellSortAsc($elements, $length) {
    $k=0;
    $gap[0] = (int) ($length / 2);
 
    while($gap[$k] > 1) {
        $k++;
        $gap[$k] = (int)($gap[$k-1] / 2);
    }//end while
 
    for($i = 0; $i <= $k; $i++){
        $step=$gap[$i];
 
        for($j = $step; $j < $length; $j++) {
            $temp = $elements[$j];
            $p = $j - $step;
            while($p >= 0 && strtotime($elements[$j]->crtf_date) < strtotime($elements[$p]->crtf_date)) {
                $elements[$p + $step] = $elements[$p];
                $p = $p - $step;
            }//end while
            $elements[$p + $step] = $temp;
        }//endfor j
    }//endfor i
 
    return $elements;
}

function ShellSortDesc($elements, $length) {
    $k=0;
    $gap[0] = (int) ($length / 2);
 
    while($gap[$k] > 1) {
        $k++;
        $gap[$k] = (int)($gap[$k-1] / 2);
    }//end while
 
    for($i = 0; $i <= $k; $i++){
        $step=$gap[$i];
 
        for($j = $step; $j < $length; $j++) {
            $temp = $elements[$j];
            $p = $j - $step;
            while($p >= 0 && strtotime($elements[$j]->crtf_date) > strtotime($elements[$p]->crtf_date)) {
                $elements[$p + $step] = $elements[$p];
                $p = $p - $step;
            }//end while
            $elements[$p + $step] = $temp;
        }//endfor j
    }//endfor i
 
    return $elements;
}

function JLMS_ShowCertificatesList( $option ) {
	
	JToolBarHelper::title( _JOOMLMS_COMP_NAME.': '._JLMS_CERTS_LIST );
	JToolBarHelper::divider();
	JToolBarHelper::editList('edit_certificate');
	JToolBarHelper::spacer();
	
	$app = JFactory::getApplication('administrator');
	$db = JLMSFactory::getDB();
	
	$JLMS_CONFIG = & JLMSFactory::getConfig();

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	$filt_course = intval( $app->getUserStateFromRequest( "filt_course{$option}", 'filt_course', 0 ) );
	$filt_user = intval( $app->getUserStateFromRequest( "filt_user{$option}", 'filt_user', 0 ) );
	$filt_crtf = strval( $app->getUserStateFromRequest( "filt_crtf{$option}", 'filt_crtf', '' ) );
	
	$start_date	= $app->getUserStateFromRequest( "filter_start_date_{$option}", 'f_start_date', '' );
	$end_date 	= $app->getUserStateFromRequest( "filter_end_date_{$option}", 'f_end_date', '' );
	$f_start_date = $start_date != '-' ? JLMS_dateToDB($start_date) . ' 00:00:01' : '';
	$f_end_date = $end_date  != '-' ? JLMS_dateToDB($end_date) . ' 23:59:59' : '';
	$start_date = $start_date == '-' ? '' : $start_date;
	$end_date = $end_date == '-' ? '' : $end_date;
	
	$rows = array();
	
	$query = "SELECT a.*"
	. "\n, a.crt_date as crtf_date"
	. "\n, b.uniq_id"
	. "\n, b.last_printed"
	
	. "\n, b.username"
	. "\n, u.username as cur_username"
	. "\n, u.id as cur_user"
	. "\n, b.name"
	. "\n, u.name as cur_name"
	. "\n, b.course_name"
	. "\n, c.course_name as cur_course_name"
	. "\n, q.c_id as quiz_id"
	. "\n, b.quiz_name as quiz_name"
	. "\n, q.c_title as cur_quiz_name"
	
	. "\n, a.id as cu_id"
	. "\n, b.id as cp_id"
	
	. "\n FROM"
	. "\n #__lms_certificate_users as a"
	. "\n LEFT JOIN #__lms_certificate_prints as b"
	. "\n ON 1"
		. "\n AND a.course_id = b.course_id"
		. "\n AND a.user_id = b.user_id"
		. "\n AND b.quiz_id = ".$db->quote(0)
		
	//. "\n LEFT JOIN #__users as u"
	//. "\n ON 1"
	//	. "\n AND a.user_id = u.id"
	//. "\n LEFT JOIN #__lms_courses as c"
	//. "\n ON 1"
	//	. "\n AND a.course_id = c.id"
		
	. "\n LEFT JOIN #__lms_quiz_t_quiz as q"
	. "\n ON 1"
		. "\n AND b.quiz_id = q.c_id"
	
	. "\n, #__users as u"
	. "\n, #__lms_courses as c"
		
	. "\n WHERE 1"
	
	. "\n AND a.user_id = u.id"
	. "\n AND a.course_id = c.id"
	
	//."\n AND a.course_id = b.course_id"
	//."\n AND a.user_id = b.user_id"
	. ($filt_course ? "\n AND a.course_id = ".$db->quote($filt_course) : '')
	. ($filt_user ? "\n AND a.user_id = ".$db->quote($filt_user) : '')
	. ($filt_crtf ? "\n AND b.uniq_id LIKE ".$db->quote($filt_crtf.'%') : '')
	. ($f_start_date ? "\n AND a.crt_date > ".$db->quote($f_start_date) : '')
	. ($f_end_date ? "\n AND a.crt_date < ".$db->quote($f_end_date) : '')
	. "\n ORDER BY crtf_date DESC"
	;
	$db->setQuery($query);
	$certificates_courses = $db->loadObjectList();
	
	$query = "SELECT a.*"
	. "\n, a.quiz_date as crt_date"
	. "\n, a.quiz_date as crtf_date"
	//. "\n, IF(b.id > 0, b.crtf_date, a.quiz_date) as crtf_date"
	. "\n, b.uniq_id"
	//. "\n, b.crtf_date"
	. "\n, b.last_printed"
	
	. "\n, b.username"
	. "\n, u.username as cur_username"
	. "\n, u.id as cur_user"
	. "\n, b.name"
	. "\n, u.name as cur_name"
	. "\n, b.course_name"
	. "\n, c.course_name as cur_course_name"
	. "\n, q.c_id as quiz_id"
	. "\n, b.quiz_name as quiz_name"
	. "\n, q.c_title as cur_quiz_name"
	
	. "\n, a.id as qr_id"
	. "\n, b.id as cp_id"
	
	. "\n FROM"
	."\n #__lms_quiz_results as a"
	. "\n LEFT JOIN #__lms_certificate_prints as b"
	. "\n ON 1"
		. "\n AND a.course_id = b.course_id"
		. "\n AND a.user_id = b.user_id"
		. "\n AND b.quiz_id > ".$db->quote(0)
	
	//. "\n LEFT JOIN #__users as u"
	//. "\n ON 1"
	//	. "\n AND a.user_id = u.id"
	//. "\n LEFT JOIN #__lms_courses as c"
	//. "\n ON 1"
	//	. "\n AND a.course_id = c.id"
		
	. "\n LEFT JOIN #__lms_quiz_t_quiz as q"
	. "\n ON 1"
		. "\n AND a.quiz_id = q.c_id"
	
	."\n, #__users as u"
	."\n, #__lms_courses as c"	
		
	. "\n WHERE 1"
	. "\n AND a.user_id = u.id"
	. "\n AND a.course_id = c.id"
	. ($filt_course ? "\n AND a.course_id = ".$db->quote($filt_course) : '')
	. ($filt_user ? "\n AND a.user_id = ".$db->quote($filt_user) : '')
	. ($filt_crtf ? "\n AND b.uniq_id LIKE ".$db->quote($filt_crtf.'%') : '')
	. ($f_start_date ? "\n AND a.quiz_date > ".$db->quote($f_start_date) : '')
	. ($f_end_date ? "\n AND a.quiz_date < ".$db->quote($f_end_date) : '')
	. "\n ORDER BY crtf_date DESC"
	;
	$db->setQuery($query);
	$certificates_quizes = $db->loadObjectList();
	
	$rows = array_merge($certificates_courses, $certificates_quizes);
	
	//proeducate certificate
	if($JLMS_CONFIG->get('enabled_clear_course_user_data', false)){
		$CCUD = new JLMS_ClearCourseUserData();
		if($filt_course){
			$CCUD->setCourseID($filt_course);
		}
		if($filt_user){
			$CCUD->setUserID($filt_user);
		}
		if(is_array($CCUD->getArhiveCertificatePrints())){
			$rows = array_merge($rows, $CCUD->getArhiveCertificatePrints());
		}
	}
	//proeducate certificate
	
	//echo '<pre>';
	//print_r($rows);
	//echo '</pre>';
	//echo 'xxx';
	//die;
	
	//sort{
	$rows = ShellSortDesc($rows, count($rows));
	//}sort
	
	//pagination{
	$total = count($rows);
	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );
	$tmp = array();
	for($i=$pageNav->limitstart;$i<($pageNav->limitstart + $pageNav->limit);$i++){
		if(isset($rows[$i])){
			$tmp[] = $rows[$i];
		}
	}
	if(isset($tmp) && count($tmp)){
		$rows = array();
		$rows = $tmp;
	}
	//}pagination
	
	//get u_ids{
	$u_ids = array();
	foreach($rows as $row){
		if(isset($row->user_id) && $row->user_id){ //&& !in_array($row->user_id, $u_ids)){
			$u_ids[] = $row->user_id;
		}
		
		if(!$row->quiz_id){
			$row->quiz_id = 0;
		}
		
		if(!$row->cp_id){
			if(isset($row->cu_id) && $row->cu_id){
				$row->cp_id = $row->cu_id;
			} else
			if(isset($row->qr_id) && $row->qr_id){
				$row->cp_id = $row->qr_id;
			}
		}
		
		$row->check_certificate_file_on_server = false;
		if(isset($row->course_id) && isset($row->user_id) && isset($row->quiz_id)){
			require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_certificates.php");
			$outputCertificate = new JLMS_outputCertificate($row->course_id, $row->user_id, $row->quiz_id);
			if(isset($outputCertificate->check_certificate_file_on_server)){
				$row->check_certificate_file_on_server = $outputCertificate->check_certificate_file_on_server;
				$row->certificate_file_on_server = $outputCertificate->getCertificateFileOnServer($outputCertificate->certificate_which_printed);
				$row->certificate_file_relative_path = JURI::root() . $outputCertificate->getCertificateFileRelativePath($outputCertificate->certificate_which_printed);
			}
		}
	}
	$u_ids = array_unique($u_ids);
	//}get u_ids

	$lists = array();
	$query = "SELECT a.id as value, a.course_name as text FROM #__lms_courses as a ORDER BY a.course_name";
	$db->setQuery( $query );
	$sf_courses = array();
	$sf_courses[] = mosHTML::makeOption( '0', _JLMS_CERTS_SLCT_COURSE_ );
	$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
	$lists['jlms_courses'] = mosHTML::selectList( $sf_courses, 'filt_course', 'class="text_area" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $filt_course );
	
	$lists['jlms_crtfs'] = $filt_crtf;
	
	$lists['start_date'] = $start_date;
	$lists['end_date'] = $end_date;

	joomla_lms_adm_html::JLMS_showCertificatesList( $rows, $pageNav, $option, $lists );
}

function JLMS_editCertificate($complex_id, $option){
	
	JToolBarHelper::title( _JOOMLMS_COMP_NAME.': '._JLMS_CERTS_EDIT );
	JToolBarHelper::divider();
	JToolBarHelper::save('save_certificate');
	JToolBarHelper::cancel('certificates');
	JToolBarHelper::spacer();
	
	$db = & JLMSFactory::getDB();
	
	preg_match('#(\d+)_(\d+)_(\d+)_(\d+)#', $complex_id, $out);
	if(isset($out) && isset($out[0])){
		if(isset($out[1])){
			$course_id = $out[1];
		}
		if(isset($out[2])){
			$quiz_id = $out[2];
		}
		if(isset($out[3])){
			$user_id = $out[3];
		}
		if(isset($out[4])){
			$exist_uniq_id = $out[4];
		}
	}
	
	$certificate_course = new stdClass();
	$certificate_quiz = new stdClass();
	
	if(!$quiz_id){
		$query = "SELECT a.*"
		. "\n, a.crt_date as crtf_date"
		. "\n, b.uniq_id"
		. "\n, b.last_printed"
		
		. "\n, b.username"
		. "\n, u.username as cur_username"
		. "\n, u.id as cur_user"
		. "\n, b.name"
		. "\n, u.name as cur_name"
		. "\n, b.course_name"
		. "\n, c.course_name as cur_course_name"
		. "\n, q.c_id as quiz_id"
		. "\n, b.quiz_name as quiz_name"
		. "\n, q.c_title as cur_quiz_name"
		
		. "\n, a.id as cu_id"
		. "\n, b.id as cp_id"
		
		. "\n FROM"
		. "\n #__lms_certificate_users as a"
		. "\n LEFT JOIN #__lms_certificate_prints as b"
		. "\n ON 1"
			. "\n AND a.course_id = b.course_id"
			. "\n AND a.user_id = b.user_id"
			. "\n AND b.quiz_id = ".$db->quote($quiz_id)
		. "\n LEFT JOIN #__users as u"
		. "\n ON 1"
			. "\n AND a.user_id = u.id"
		. "\n LEFT JOIN #__lms_courses as c"
		. "\n ON 1"
			. "\n AND a.course_id = c.id"
		. "\n LEFT JOIN #__lms_quiz_t_quiz as q"
		. "\n ON 1"
			. "\n AND b.quiz_id = q.c_id"
		. "\n WHERE 1"
		."\n AND a.course_id = ".$db->quote($course_id)
		."\n AND a.user_id = ".$db->quote($user_id)
		//."\n AND b.quiz_id = ".$db->quote($quiz_id)
		;
		$db->setQuery($query);
		$certificate_course = $db->loadObject();
		
		//echo '<pre>$certificate_course';
		//print_r($certificate_course);
		//echo '</pre>';
		//echo $db->getErrorMsg();
		
	} else {
		
		$query = "SELECT a.*"
		. "\n, a.quiz_date as crt_date"
		. "\n, a.quiz_date as crtf_date"
		. "\n, b.uniq_id"
		. "\n, b.last_printed"
		
		. "\n, b.username"
		. "\n, u.username as cur_username"
		. "\n, u.id as cur_user"
		. "\n, b.name"
		. "\n, u.name as cur_name"
		. "\n, b.course_name"
		. "\n, c.course_name as cur_course_name"
		. "\n, q.c_id as quiz_id"
		. "\n, b.quiz_name as quiz_name"
		. "\n, q.c_title as cur_quiz_name"
		
		. "\n, a.id as qr_id"
		. "\n, b.id as cp_id"
		
		. "\n FROM"
		."\n #__lms_quiz_results as a"
		. "\n LEFT JOIN #__lms_certificate_prints as b"
		. "\n ON 1"
			. "\n AND a.course_id = b.course_id"
			. "\n AND a.user_id = b.user_id"
			. "\n AND b.quiz_id > ".$db->quote(0)
		. "\n LEFT JOIN #__users as u"
		. "\n ON 1"
			. "\n AND a.user_id = u.id"
		. "\n LEFT JOIN #__lms_courses as c"
		. "\n ON 1"
			. "\n AND a.course_id = c.id"
		. "\n LEFT JOIN #__lms_quiz_t_quiz as q"
		. "\n ON 1"
			. "\n AND a.quiz_id = q.c_id"	
		. "\n WHERE 1"
		."\n AND a.course_id = ".$db->quote($course_id)
		."\n AND a.user_id = ".$db->quote($user_id)
		."\n AND a.quiz_id = ".$db->quote($quiz_id)
		;
		$db->setQuery($query);
		$certificate_quiz = $db->loadObject();
		
		//echo '<pre>$certificate_quiz';
		//print_r($certificate_quiz);
		//echo '</pre>';
		//echo $db->getErrorMsg();
		
	}
	
	$row = isset($certificate_course->id) ? $certificate_course : $certificate_quiz;
	
	if(!$row->quiz_id){
		$row->quiz_id = 0;
	}
	
	$row->check_certificate_file_on_server = false;
	if(isset($row->course_id) && isset($row->user_id) && isset($row->quiz_id)){
		require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_certificates.php");
		$outputCertificate = new JLMS_outputCertificate($row->course_id, $row->user_id, $row->quiz_id);
		if(isset($outputCertificate->check_certificate_file_on_server)){
			$row->check_certificate_file_on_server = $outputCertificate->check_certificate_file_on_server;
			$row->certificate_file_on_server = $outputCertificate->getCertificateFileOnServer($outputCertificate->certificate_which_printed);
			$row->certificate_file_relative_path = JURI::root() . $outputCertificate->getCertificateFileRelativePath($outputCertificate->certificate_which_printed);
		}
	}
	
	if(isset($row->crtf_date) && $row->crtf_date){
		$row->crtf_date_object = new stdClass();
		$row->crtf_date_object->crtf_date_with_tz = JLMS_offsetDateToDisplay($row->crtf_date);
		$row->crtf_date_object->crtf_date_with_tz = JLMS_dateToDB($row->crtf_date_object->crtf_date_with_tz);
		
		if($row->crtf_date_object->crtf_date_with_tz && preg_match( "/([0-9]{4})-([0-9]{2})-([0-9]{2})\s+([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})/", $row->crtf_date_object->crtf_date_with_tz, $regs )){
			if(isset($regs) && isset($regs[0])){
				$row->crtf_date_object->year = $regs[1];
				$row->crtf_date_object->month = $regs[2];
				$row->crtf_date_object->day = $regs[3];
				$row->crtf_date_object->hour = $regs[4];
				$row->crtf_date_object->minut = $regs[5];
				$row->crtf_date_object->second = $regs[6];
				
				$row->crtf_date_object->dates = JHTML::_(
															'calendar',
															$row->crtf_date_object->year."-".$row->crtf_date_object->month."-".$row->crtf_date_object->day,
															'date',
															'date',
															'%Y-%m-%d',
															null
														);
				$list_hours = array();
				for($i=0;$i<24;$i++){
					$list_hours[] = str_pad($i, 2, '0', STR_PAD_LEFT);
				}
				$row->crtf_date_object->hours = JHTML::_('select.genericlist', $list_hours, 'hour', 'class="inputbox"', 'value', 'text', $row->crtf_date_object->hour );
				$list_minuts = array();
				for($i=0;$i<60;$i++){
					$list_minuts[] = str_pad($i, 2, '0', STR_PAD_LEFT);
				}
				$row->crtf_date_object->minuts = JHTML::_('select.genericlist', $list_minuts, 'minut', 'class="inputbox"', 'value', 'text', $row->crtf_date_object->minut );
				$list_seconds = array();
				for($i=0;$i<60;$i++){
					$list_seconds[] = str_pad($i, 2, '0', STR_PAD_LEFT);
				}
				$row->crtf_date_object->seconds = JHTML::_('select.genericlist', $list_seconds, 'second', 'class="inputbox"', 'value', 'text', $row->crtf_date_object->second );
			}
		}
	}
	
	$row->complex_id = $complex_id;
	
	joomla_lms_adm_html::JLMS_editCertificate($row, $option);
}

function JLMS_saveCertificate($id, $option){
	
	$app = & JFactory::getApplication('administrator');
	$db = & JLMSFactory::getDB();
	
	$complex_id = JRequest::getVar('id', false);
	$course_id = 0;
	$quiz_id = 0;
	$user_id = 0;
	$exist_uniq_id = 0;
	if($complex_id){
		preg_match('#(\d+)_(\d+)_(\d+)_(\d+)#', $complex_id, $out);
		if(isset($out) && isset($out[0])){
			if(isset($out[1])){
				$course_id = $out[1];
			}
			if(isset($out[2])){
				$quiz_id = $out[2];
			}
			if(isset($out[3])){
				$user_id = $out[3];
			}
			if(isset($out[4])){
				$exist_uniq_id = $out[4];
			}
		}	
	}
	
	$part_date = JRequest::getVar('date', '0000-00-00');
	$part_hour = JRequest::getVar('hour', 0);
	$part_minut = JRequest::getVar('minut', 0);
	$part_second = JRequest::getVar('second', 0);
	
	$date_to_store = $part_date . " " . str_pad($part_hour, 2, "0", STR_PAD_LEFT).":".str_pad($part_minut, 2, "0", STR_PAD_LEFT).":".str_pad($part_second, 2, "0", STR_PAD_LEFT);
	$date_to_store = JLMS_dateWithTimeZoneCMSConvertToUTC($date_to_store);
	
	$certificate_course = new stdClass();
	$certificate_quiz = new stdClass();
	
	if(!$quiz_id){
		$query = "SELECT a.*"
		. "\n, a.crt_date as crtf_date"
		. "\n, b.uniq_id"
		. "\n, b.last_printed"
		. "\n, b.username"
		. "\n, u.username as cur_username"
		. "\n, u.id as cur_user"
		. "\n, b.name"
		. "\n, u.name as cur_name"
		. "\n, b.course_name"
		. "\n, c.course_name as cur_course_name"
		. "\n, q.c_id as quiz_id"
		. "\n, b.quiz_name as quiz_name"
		. "\n, q.c_title as cur_quiz_name"
		. "\n, a.id as cu_id"
		. "\n, b.id as cp_id"
		. "\n FROM"
		. "\n #__lms_certificate_users as a"
		. "\n LEFT JOIN #__lms_certificate_prints as b"
		. "\n ON 1"
			. "\n AND a.course_id = b.course_id"
			. "\n AND a.user_id = b.user_id"
			. "\n AND b.quiz_id = ".$db->quote($quiz_id)
		. "\n LEFT JOIN #__users as u"
		. "\n ON 1"
			. "\n AND a.user_id = u.id"
		. "\n LEFT JOIN #__lms_courses as c"
		. "\n ON 1"
			. "\n AND a.course_id = c.id"
		. "\n LEFT JOIN #__lms_quiz_t_quiz as q"
		. "\n ON 1"
			. "\n AND b.quiz_id = q.c_id"
		. "\n WHERE 1"
		."\n AND a.course_id = ".$db->quote($course_id)
		."\n AND a.user_id = ".$db->quote($user_id)
		;
		$db->setQuery($query);
		$certificate_course = $db->loadObject();
		
		//echo '<pre>$certificate_course';
		//print_r($certificate_course);
		//echo '</pre>';
		//echo $db->getErrorMsg();
		
	} else {
		
		$query = "SELECT a.*"
		. "\n, a.quiz_date as crt_date"
		. "\n, a.quiz_date as crtf_date"
		. "\n, b.uniq_id"
		. "\n, b.last_printed"
		. "\n, b.username"
		. "\n, u.username as cur_username"
		. "\n, u.id as cur_user"
		. "\n, b.name"
		. "\n, u.name as cur_name"
		. "\n, b.course_name"
		. "\n, c.course_name as cur_course_name"
		. "\n, q.c_id as quiz_id"
		. "\n, b.quiz_name as quiz_name"
		. "\n, q.c_title as cur_quiz_name"
		. "\n, a.id as qr_id"
		. "\n, b.id as cp_id"
		. "\n FROM"
		."\n #__lms_quiz_results as a"
		. "\n LEFT JOIN #__lms_certificate_prints as b"
		. "\n ON 1"
			. "\n AND a.course_id = b.course_id"
			. "\n AND a.user_id = b.user_id"
			. "\n AND b.quiz_id > ".$db->quote(0)
		. "\n LEFT JOIN #__users as u"
		. "\n ON 1"
			. "\n AND a.user_id = u.id"
		. "\n LEFT JOIN #__lms_courses as c"
		. "\n ON 1"
			. "\n AND a.course_id = c.id"
		. "\n LEFT JOIN #__lms_quiz_t_quiz as q"
		. "\n ON 1"
			. "\n AND a.quiz_id = q.c_id"	
		. "\n WHERE 1"
		."\n AND a.course_id = ".$db->quote($course_id)
		."\n AND a.user_id = ".$db->quote($user_id)
		."\n AND a.quiz_id = ".$db->quote($quiz_id)
		;
		$db->setQuery($query);
		$certificate_quiz = $db->loadObject();
		
		//echo '<pre>$certificate_quiz';
		//print_r($certificate_quiz);
		//echo '</pre>';
		//echo $db->getErrorMsg();
		
	}
	
	$row = isset($certificate_course->id) ? $certificate_course : $certificate_quiz;
	
	if(isset($certificate_course->id)){
		$row->type_certificate = 1;
	} else {
		$row->type_certificate = 2;
	}
	
	$query = "SELECT *"
	. "\n FROM #__lms_users_in_groups"
	. "\n WHERE 1"
	. "\n AND course_id = ".$db->quote($course_id)
	. "\n AND user_id = ".$db->quote($user_id)
	;
	$db->setQuery($query);
	$user_in_course = $db->loadObject();
	
	//echo '<pre>';
	//print_r($user_in_course);
	//print_r(strtotime($user_in_course->enrol_time));
	//echo '</pre>';
	//die;
	
	$is_update_date = true;
	if(strtotime($row->crtf_date) == strtotime($date_to_store)){
		$is_update_date = false;
	}
		
	if($is_update_date)	{
		$delete_certificate_on_server = false;
		if(isset($row->type_certificate) && $row->type_certificate == 1){
			$query = "UPDATE #__lms_certificate_users"
			. "\n SET"
			. "\n crt_date = ".$db->quote($date_to_store)
			. "\n WHERE 1"
			. "\n AND course_id = ".$db->quote($course_id)
			. "\n AND user_id = ".$db->quote($user_id)
			;
			$db->setQuery($query);
		} else
		if(isset($row->type_certificate) && $row->type_certificate == 2){
			$query = "UPDATE #__lms_quiz_results"
			. "\n SET"
			. "\n quiz_date = ".$db->quote($date_to_store)
			. "\n WHERE 1"
			. "\n AND course_id = ".$db->quote($course_id)
			. "\n AND user_id = ".$db->quote($user_id)
			. ($quiz_id ? "\n AND quiz_id = ".$db->quote($quiz_id) : '')
			;
			$db->setQuery($query);
		}
		if(isset($row->type_certificate) && in_array($row->type_certificate, array(1,2)) && $db->query()){
			$delete_certificate_on_server = true;
		}
		
		if(isset($delete_certificate_on_server) && $delete_certificate_on_server){
			require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_certificates.php");
			$outputCertificate = new JLMS_outputCertificate($course_id, $user_id, $quiz_id);
			if(isset($outputCertificate->check_certificate_file_on_server) && $outputCertificate->check_certificate_file_on_server){
				$outputCertificate->status_delete_certificate_file_on_server = $outputCertificate->deleteCertificateFileOnServer($outputCertificate->certificate_which_printed);
			}
		}
	}
		
	$app->redirect('index.php?option=com_joomla_lms&task=certificates');
}


function jlms_listOperation( $type, $option ) {
	global $JLMS_CONFIG;
	$db = JFactory::getDBO();

	//$type 	= mosGetParam( $_REQUEST, 'view_type', $type );
	$course_id 	= mosGetParam( $_REQUEST, 'course_id', -1 );
	$course_id_i 	= mosGetParam( $_REQUEST, 'course_id_i', -3 );
	$class_id_a 	= mosGetParam( $_REQUEST, 'class_id', 0 );
	if (is_array($class_id_a)) {
		$class_id = $class_id_a[0];
	} else {
		$class_id = 0;
	}

	$lists = array();
	$lists['group_id'] = $class_id;
	$query = "SELECT a.id as value, a.course_name as text FROM #__lms_courses as a order by a.course_name";
	$db->setQuery( $query );
	$sf_courses_db = $db->loadObjectList();
	$sf_courses = array();
	$sf_courses[] = mosHTML::makeOption( '-1', _JLMS_CSV_FRM_JCMS_ );
	$sf_courses[] = mosHTML::makeOption( '0', _JLMS_CSV_FRM_ALL_CRSS );
	$sf_courses = array_merge( $sf_courses, $sf_courses_db );
	$course_id = ($course_id == -1 ? (isset($sf_courses[0]->value)?$sf_courses[0]->value:$course_id) : $course_id);
	$course_id_i = ($course_id_i == -1 ? (isset($sf_courses[0]->value)?$sf_courses[0]->value:$course_id_i) : $course_id_i);
	$lists['jlms_courses_exp'] = mosHTML::selectList( $sf_courses, 'course_id', ' class="text_area" size="1" onchange="submitbutton(\'csv_export\');" style="width:322px;"', 'value', 'text', $course_id );
	//$lists['jlms_courses_imp'] = mosHTML::selectList( $sf_courses, 'course_id_i', ' class="text_area" size="1" onchange="submitbutton(\'csv_import\');"', 'value', 'text', $course_id_i );
	$sf_courses2 = array();

	if ($JLMS_CONFIG->get('use_global_groups', 1)) {
		$sf_courses2[] = mosHTML::makeOption( '-3', _JLMS_CSV_RMV_SLCTD_USRS );
	}

	$sf_courses2[] = mosHTML::makeOption( '-1', _JLMS_CSV_FRM_JCMS_ENT_REM_ );
	$sf_courses2[] = mosHTML::makeOption( '0', _JLMS_CSV_FRM_ALL_CRSS_E_ );

	$sf_courses2 = array_merge( $sf_courses2, $sf_courses_db );
	$lists['jlms_courses_del'] = mosHTML::selectList( $sf_courses2, 'course_id_del[]', ' multiple="multiple" class="text_area chzn-done" size="5" style="width:322px;"', 'value', 'text', ( ( ($course_id == -1) && $JLMS_CONFIG->get('use_global_groups', 1) ) ? -3 : $course_id) );


	//kosmos added 13.02.08
	$query = "SELECT a.id as value, a.course_name as text FROM #__lms_courses as a order by a.course_name";
	$db->setQuery( $query );
	$sf_courses = array();
	$sf_courses[] = mosHTML::makeOption( '-3', _JLMS_CSV_INTO_JCMS_ONLY_ );
	// !!! $sf_courses[] = mosHTML::makeOption( '0', '- into all courses -' );
	//TODO: review the comments below
	/* eto tak bysto ne sdelat' + nado dobavit' proverki na 'category permissions,
	 t.e. esli importim vo vse kursy i v opredelennuyu gruppu - to v etom sluchae
	 nado importit' ne vo vse kursy  tol'ko v dostupnye dlya etoi kategorii. */
	$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
	$course_id = ($course_id == -1 ? (isset($sf_courses[0]->value)?$sf_courses[0]->value:$course_id) : $course_id);
	$course_id_i = ($course_id_i == -1 ? (isset($sf_courses[0]->value)?$sf_courses[0]->value:$course_id_i) : $course_id_i);
	$lists['jlms_courses_imp'] = mosHTML::selectList( $sf_courses, 'course_id_i', 'id="course_id_i" class="text_area" size="1" onchange="submitbutton(\'csv_import\');" style="width:322px;"', 'value', 'text', $course_id_i );
	//kosmos end

	if ($JLMS_CONFIG->get('use_global_groups', 1)) {
		if ($course_id > 0) {
			$query = "SELECT user_id FROM #__lms_users_in_groups WHERE course_id = $course_id";
			$db->setQuery($query);
			$cid = JLMSDatabaseHelper::loadResultArray();
			if (!$cid) $cid = array(-1);
			$query = "SELECT group_id FROM #__lms_users_in_global_groups WHERE user_id IN (".implode(',', $cid).")";
			$db->setQuery($query);
			$gid = JLMSDatabaseHelper::loadResultArray();
			if (!$gid) $gid = array(-1);
			$query = "SELECT id AS value, ug_name AS text FROM #__lms_usergroups WHERE id IN (".implode(',', $gid).") AND course_id = 0";//course id check just in case))
			$db->setQuery($query);
			$groups = $db->loadObjectList();
		} else {
			$query = "SELECT id AS value, ug_name AS text FROM #__lms_usergroups WHERE course_id = 0 ORDER BY ug_name";//course id check just in case))
			$db->setQuery($query);
			$groups = $db->loadObjectList();
		}
		$sf_courses = array();
		/* list of groups for 'export' feature */
		$sf_courses[] = mosHTML::makeOption( '-1', _JLMS_CSV_ANY_USER_ );
		$sf_courses[] = mosHTML::makeOption( '0', _JLMS_CSV_ONL_USERS_IN_GR_ );
		//TODO:: is this a bug ??? ... loadobjectlist is present on the line below, but there is no setquery!
		$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
		$lists['jlms_groups_export'] = mosHTML::selectList( $sf_courses, 'class_id[]', ' multiple="multiple" class="text_area chzn-done" size="5" style="width:322px;"', 'value', 'text', ($class_id ? $class_id : -1) );

		$query = "SELECT a.id as value, a.ug_name as text FROM #__lms_usergroups as a"
		. "\n WHERE a.course_id = 0"
		. "\n order by a.ug_name";
		$db->setQuery( $query );
		$sf_courses = array();
		//$sf_courses[] = mosHTML::makeOption( '-1', '- Select Group -' );
		$sf_courses[] = mosHTML::makeOption( '0', _JLMS_CSV_SLCT_GR_ );
		$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
		$lists['jlms_groups_import'] = mosHTML::selectList( $sf_courses, 'class_id_import', ' class="text_area" size="1" style="width:322px;"', 'value', 'text', $class_id );
		$lists['jlms_groups_delete'] = mosHTML::selectList( $sf_courses, 'class_id_delete', ' class="text_area" size="1" style="width:322px;"', 'value', 'text', $class_id );

	} else {

		if ($course_id > 0) {
			$query = "SELECT a.id as value, a.ug_name as text FROM #__lms_usergroups as a"
			. ( $course_id ? "\n WHERE a.course_id = $course_id" : '')
			. "\n order by a.ug_name";
			$db->setQuery( $query );
			$db_course_groups = $db->loadObjectList();
		} else {
			$db_course_groups = array();
		}
		$sf_courses = array();
		//$sf_courses[] = mosHTML::makeOption( '-1', '- Select Group -' );
		/* list of groups for 'export' feature */
		$sf_courses[] = mosHTML::makeOption( '-1', _JLMS_CSV_ALL_USRS_ );
		if ($course_id > 0) {
			$sf_courses[] = mosHTML::makeOption( '0', _JLMS_CSV_USRS_WO_GR );
		}
		$sf_courses = array_merge( $sf_courses,  $db_course_groups);
		$lists['jlms_groups_export'] = mosHTML::selectList( $sf_courses, 'class_id[]', ' multiple="multiple" class="text_area" size="3" style="width:322px;"', 'value', 'text', ($class_id ? $class_id : -1) );


		$query = "SELECT a.id as value, a.ug_name as text FROM #__lms_usergroups as a"
		. ( $course_id ? "\n WHERE a.course_id = $course_id_i" : '')
		. "\n order by a.ug_name";
		$db->setQuery( $query );
		$sf_courses = array();
		//$sf_courses[] = mosHTML::makeOption( '-1', '- Select Group -' );
		$sf_courses[] = mosHTML::makeOption( '0', _JLMS_CSV_USRS_WO_GR );
		$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
		$lists['jlms_groups_import'] = mosHTML::selectList( $sf_courses, 'class_id_import', ' class="text_area" size="1" style="width:322px;"', 'value', 'text', $class_id );
		$lists['jlms_groups_delete'] = '';
	}

	joomla_lms_adm_html::jlms_showOperation( $type, $option, $lists );
}

function jlms_csvBackTo( $option ) {
	$back_to 	= mosGetParam( $_REQUEST, 'back_to', 'csv_operations' );
	mosRedirect( "index.php?option=$option&task=$back_to" );
}
function JLMS_processCSVField_admin($field_text) {
	$field_text = trim(strip_tags($field_text));
	$field_text = str_replace( '&#039;', "'", $field_text );
	$field_text = str_replace( '&#39;', "'", $field_text );
	$field_text = str_replace('&quot;',  '"', $field_text );
	$field_text = str_replace( '"', '""', $field_text );
	if (function_exists('html_entity_decode')) {
		$field_text = @html_entity_decode($field_text, ENT_QUOTES, 'UTF-8');
	} elseif (function_exists('get_html_translation_table')) {
		$trans_tbl = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
		$trans_tbl = array_flip($trans_tbl);
		$field_text = strtr($field_text, $trans_tbl);
	}
	$field_text = preg_replace('~&#x([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $field_text);
	$field_text = preg_replace('~&#([0-9]+);~e', 'chr("\\1")', $field_text);
	$field_text = '"'.$field_text.'"';
	return $field_text;
}
function jlms_csvExport( $option ) {
	global $JLMS_CONFIG;
	$db = JFactory::getDBO();

	require_once (_JOOMLMS_FRONT_HOME.'/joomla_lms.main.php');
	$type 	= mosGetParam( $_REQUEST, 'sel_exp_type', 0 );
	$course_id 	= mosGetParam( $_REQUEST, 'course_id', 0 );
	$cid 	= mosGetParam( $_REQUEST, 'class_id', array(0) );
	//$cid = mosGetParam( $_POST, 'cid', array(0) );

	if (!is_array( $cid ) ) { $cid = array(); }
	$group_specified = false;
	$group_ids = array();
	if (count($cid)) {
		foreach ($cid as $cid1) {
			$group_ids[] = intval($cid1);
		}
		$group_ids = array_unique($group_ids);
	}
	
	
	$error = false;
	if (count($group_ids)) {
		$group_specified = true;
		if ($JLMS_CONFIG->get('use_global_groups', 1)) {
		} elseif ($course_id > 0) {
			foreach($group_ids as $group_id){
				if ($group_id > 0) {
					if ( !(JLMS_GetGroupCourse($group_id) == $course_id) ) {
						$error = true;
						break;
					}
				}
			}
		}
	}
	
	if ($error) {		
		mosRedirect( "index.php?option=$option&task=csv_operations" );
		die;
	}
	

	$query_export = '';
	
	// export "from Joomla CMS!" and "from all courses"
	if (isset($group_ids[0]) && $group_ids[0] == intval('-1')) {		
		$group_ids = array();
	}
	
	if ($course_id > 0) {		
		if (count($group_ids) && $JLMS_CONFIG->get('use_global_groups', 1)) {
			if (in_array(0, $group_ids)) {
				// find all users who participate at least in one group
				$query = "SELECT distinct user_id FROM #__lms_users_in_global_groups";
				$db->setQuery($query);
				$course_users_in_global = JLMSDatabaseHelper::loadResultArray();
			} else {
				//find all users who participate in specified groups
				$query = "SELECT distinct user_id FROM #__lms_users_in_global_groups WHERE group_id IN ( ".implode(',', $group_ids)." ) ";
				$db->setQuery($query);
				$course_users_in_global = JLMSDatabaseHelper::loadResultArray();
			}
			$query_export = "SELECT a.username, a.name, a.email, '' as teacher_comment "
			. "\n FROM #__users as a, #__lms_users_in_groups as b "
			. "\n WHERE a.id = b.user_id AND b.course_id = $course_id AND b.group_id IN ( ".implode(',', $group_ids)." ) ORDER BY a.username, a.name";
		} elseif (count($group_ids) && !$JLMS_CONFIG->get('use_global_groups', 1)) {
			// export users from course, but only whos participates in specified local groups.			
			$query_export = "SELECT a.username, a.name, a.email, '' as teacher_comment "
			. "\n FROM #__users as a, #__lms_users_in_groups as b "
			. "\n WHERE a.id = b.user_id AND b.course_id = $course_id AND b.group_id IN ( ".implode(',', $group_ids)." ) ORDER BY a.username, a.name";			
		} else {			
			$query_export = "SELECT a.username, a.name, a.email, '' as teacher_comment "
			. "\n FROM #__users as a, #__lms_users_in_groups as b "
			. "\n WHERE a.id = b.user_id AND b.course_id = $course_id ORDER BY a.username, a.name";
		}
	} else {		
		if (count($group_ids) && $JLMS_CONFIG->get('use_global_groups', 1)) {
			if (in_array(0, $group_ids)) {
				// find all users who participate at least in one group
				$query = "SELECT distinct user_id FROM #__lms_users_in_global_groups";
				$db->setQuery($query);
				$course_users_in_global = JLMSDatabaseHelper::loadResultArray();
			} else {
				//find all users who participate in specified groups
				$query = "SELECT distinct user_id FROM #__lms_users_in_global_groups WHERE group_id IN ( ".implode(',', $group_ids)." ) ";
				$db->setQuery($query);
				$course_users_in_global = JLMSDatabaseHelper::loadResultArray();				
			}
			if (!$course_users_in_global) $course_users_in_global = array(0);
			if ($course_id) { // this means $course_id == -1
				// from Joomla! CMS
				if( JLMS_J16version() ) 
				{
					$query_export = "SELECT a.username, a.name, a.email, '' AS teacher_comment "
					."\n FROM #__users AS a, #__user_usergroup_map AS m WHERE a.id IN ( ".implode(',', $course_users_in_global)." ) AND m.user_id = a.id AND m.group_id NOT IN (7,8) ORDER BY a.username, a.name";
				} else {
					$query_export = "SELECT a.username, a.name, a.email, '' AS teacher_comment "
					."\n FROM #__users AS a WHERE a.id IN ( ".implode(',', $course_users_in_global)." ) AND a.gid < 25 ORDER BY a.username, a.name";
				}
			} else {
				// from all courses == export users who participate at lease in one course
				$query_export = "SELECT a.username, a.name, a.email, '' as teacher_comment "
				. "\n FROM #__users as a, #__lms_users_in_groups as b "
				. "\n WHERE a.id = b.user_id AND a.id IN ( ".implode(',', $course_users_in_global)." ) GROUP BY a.id ORDER BY a.username, a.name";
			}
		} else {
			if ($course_id) {
				if( JLMS_J16version() ) 
				{
					// $course_id == -1 - from Joomla! CMS
					$query_export = "SELECT a.username, a.name, a.email, '' AS teacher_comment "
					."\n FROM #__users AS a, #__user_usergroup_map AS m WHERE m.user_id = a.id AND m.group_id NOT IN (7,8)  ORDER BY a.username, a.name";
				} else {
					// $course_id == -1 - from Joomla! CMS
					$query_export = "SELECT a.username, a.name, a.email, '' AS teacher_comment "
					."\n FROM #__users AS a WHERE a.gid < 25  ORDER BY a.username, a.name";
				}
				
			} else {
				// $course_id == 0 - from all courses == export users who participate at lease in one course
				$query_export = "SELECT a.username, a.name, a.email, '' as teacher_comment "
				. "\n FROM #__users as a, #__lms_users_in_groups as b "
				. "\n WHERE a.id = b.user_id GROUP BY a.id  ORDER BY a.username, a.name";
			}
		}
	}
	
	
	if ($query_export) {
		$db->SetQuery($query_export);
		$exp_users = $db->LoadObjectList();				
		
		if (count($exp_users)) {
			$ug_name = 'course_users_export';//$exp_users[0]->ug_name;
			$text_to_csv = 'username,name,email,teacher comments'."\n";
			
			foreach ($exp_users as $exp_user) {
				$text_to_csv .= $exp_user->username.",".$exp_user->name.",".$exp_user->email.",".JLMS_processCSVField_admin(strip_tags($exp_user->teacher_comment))."\n";
			}
			if (preg_match('/Opera(\/| )([0-9].[0-9]{1,2})/', $_SERVER['HTTP_USER_AGENT'])) {
				$UserBrowser = "Opera";
			}
			elseif (preg_match('/MSIE ([0-9].[0-9]{1,2})/', $_SERVER['HTTP_USER_AGENT'])) {
				$UserBrowser = "IE";
			} else {
				$UserBrowser = '';
			}
			ob_clean();
			header("Content-type: application/csv");
			header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
			header("Content-Length: ".strlen(trim($text_to_csv)));
			header('Content-Disposition: attachment; filename="'.$ug_name.'.csv"');
			if ($UserBrowser == 'IE') {
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Pragma: public');
			} else {
				header('Pragma: no-cache');
			}		
            
			echo "\xEF\xBB\xBF"; //UTF-8 BOM
			echo $text_to_csv;			
			exit();			
		}
		mosRedirect( "index.php?option=$option&task=csv_operations" );
	} else {		
		mosRedirect( "index.php?option=$option&task=csv_operations" );
	}
}

function jlms_csvImport( $option ) {
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	global $license_lms_users, $JLMS_CONFIG;
	
	$db = JFactory::getDBO();
	$acl  = & JLMSFactory::getJoomlaACL();
	
	require_once (_JOOMLMS_FRONT_HOME.'/joomla_lms.main.php');
		
	$course_id_i = intval(mosGetParam($_REQUEST,'course_id_i', 0));

	$db->setQuery("SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'is_cb_installed' ");
	$is_cb_installed = $db->loadResult();
	$db->setQuery("SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'new_user_password' ");
	$new_user_password = $db->loadResult();

	$import_log = array();
	$course_id = intval(mosGetParam($_REQUEST, 'course_id_i', 0));
	$group_id = intval(mosGetParam($_REQUEST, 'class_id_import', -1));

	$joomla_user_export = intval(mosGetParam($_REQUEST,'joomla_user_export'));

	$teacher_comment = '';
	$group_id_checked = false;
	$local_group_id = 0;
	$global_group_id = 0;   
   
	if ($group_id) 
    {
		if ($group_id > 0) {
			if ($JLMS_CONFIG->get('use_global_groups', 1)) {
				//TODO: !!! check if this group can access course into which users are imported
				// e.g. if category of the course is restricted and this group didnt' located in the permissioned list
				$group_id_checked = true;
				$global_group_id = $group_id; 
			} else {
				if (JLMS_GetGroupCourse($group_id) == $course_id) {
					$group_id_checked = true;
					$local_group_id = $group_id;
				} else {
					$group_id_checked = true;
					$group_id = 0;
				}
			}
		} else {
			$group_id_checked = false;
		}
	} else {
		$group_id_checked = true;
		$group_id = 0;
	}

	// TODO: !!! if CEO user is imported, set him as manager for specified group (group is selected on teh 'csv import' page) !!!

	if ( ( ($group_id_checked && $JLMS_CONFIG->get('use_global_groups', 1)) || ( !$JLMS_CONFIG->get('use_global_groups', 1)) ) && ( $course_id || $joomla_user_export) ) {

		if ( isset($_FILES['csv_file_import']) ) {
			require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_csv_import.php");
			// make arrays of valid fields for error checking
			$fieldDescriptors = new DeImportFieldDescriptors();
			$fieldDescriptors->addRequired('username');
			$fieldDescriptors->addRequired('name');
			$fieldDescriptors->addRequired('email');
			$fieldDescriptors->addOptional('password', $new_user_password);
			$fieldDescriptors->addOptional('usergroup', '');

			$userfile			= mosGetParam($_FILES, 'csv_file_import', null);
			$userfileTempName	= $userfile['tmp_name'];
			$userfileName 		= $userfile['name'];
			
			$loader		= new DeCsvLoader();
			$loader->setFileName($userfileTempName);
			if (!$loader->load()) {
				echo "<script> alert('import failed: ".$loader->getErrorMessage()."'); window.history.go(-1); </script>\n"; exit();
			}

			if (!JLMS_prepareImport($loader, $fieldDescriptors, true)) {
				echo "<script> alert('".$loader->errorMessage."'); window.history.go(-1); </script>\n"; exit();
			}

			$requiredFieldNames	= $fieldDescriptors->getRequiredFieldNames();
			$allFieldNames		= $fieldDescriptors->getFieldNames();

			// Prepare the data to be imported but first validate all entries before eventually importing
			// (a bit like a software transaction)
			$rows	= array();
			$ii = 0;
			$usergroup_field_present = false;
			while(!$loader->isEof()) {
				$values		= $loader->getNextValues();				

				if (!JLMS_prepareImportRow($loader, $fieldDescriptors, $values, $requiredFieldNames, $allFieldNames, true)) {
					/*echo "<script> alert('".$ii." row import failed'); window.history.go(-1); </script>\n"; exit();*/
				} else {
					if (isset($row)) {
						unset($row);
					}
										
					$row = new mosUser( $db );					

					if (!$row->bind( $values )) { mosErrorAlert( $row->getError() ); }

					mosMakeHtmlSafe($row);

					$row->id 		= 0;
					if( !JLMS_J30version() ) 
					{
						$row->usertype 	= 'Registered';
					}
					
					if( JLMS_J16version() ) {
						$row->groups = $acl->get_group_id( 'Registered', 'ARO' );
					} else {
						$row->gid = $acl->get_group_id( 'Registered', 'ARO' );
					}				

					if (!$row->check()) {
						$row->is_checked = 0;
						$row->checked_msg = $row->getError();					
					} else {
						$row->is_checked = 1;
						$row->checked_msg = '';
					}	
					
					$pwd 				= $row->password;
					$row->pwd			= $pwd;
					$row->password 		= JLMS_HashPassword( $row->password );
					$row->registerDate 	= date( 'Y-m-d H:i:s' );
					$row->course_start_date = (isset($values['start date']))?$values['start date']:$db->getNullDate();
                    $row->course_end_date = (isset($values['end date']))?$values['end date']:$db->getNullDate();
					
					if (isset($values['usergroup']) && $values['usergroup']) {
						$usergroup_field_present = true;
						$row->usergroup = $values['usergroup'];
					}
					$rows[]		=& $row;
				}
				$ii ++;
			}			
			
			// Finally import the data
			$do_add = false;
			if ($license_lms_users) {
				$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
				$db->SetQuery( $query );
				$total_students = $db->LoadResult();
				if (intval($total_students) < intval($license_lms_users)) {
					$do_add = true;
				}
			} else {
				$do_add = true;
			}

			$usergroups_arr = array();
			if ($JLMS_CONFIG->get('use_global_groups', 1)) 
			{
				$query = "SELECT * FROM #__lms_usergroups WHERE course_id = 0";
				$db->SetQuery($query);
				$usergroups_arr = $db->LoadObjectList();
			}
						
			foreach(array_keys($rows) as $k) {				
				$row_user	=& $rows[$k];
				$i_log = new StdClass();
				$i_log->userinfo = '<b>'.$row_user->username.'</b>, '.$row_user->name.' ('.$row_user->email.') password: <b>'.$row_user->pwd.'</b>';
				
				$email_params['user_was_added'] = false;	
				$email_params['group_id'] = 0;
				$email_params['course_id'] = 0;
				
				$pwd = $row_user->pwd;							
				$course_start_date  = $row_user->course_start_date;
                $course_end_date    = $row_user->course_end_date;
                
				if( $row_user->course_start_date != $db->getNullDate() ) 
                {
                    $course_publish_start = 1; 
                } else {
                    $course_publish_start = 0;
                }
                
                if( $row_user->course_end_date != $db->getNullDate() ) 
                {
                    $course_publish_end = 1; 
                } else {
                    $course_publish_end = 0;
                }            
				unset($row_user->pwd);				
                unset($row_user->course_start_date);
                unset($row_user->course_end_date);  
				
				if ($row_user->is_checked == 1) {
					if ($do_add) {
						$user_usergroup_str = isset($row_user->usergroup) ? trim($row_user->usergroup) : '';
						unset($row_user->usergroup);
						unset($row_user->is_checked);
						unset($row_user->checked_msg);
						
						/*
						if( JLMS_J16version() ) 
						{
							$saveResult = $row_user->save();
						} else {
						*/
						$saveResult = $row_user->store();
						
						if ( !$saveResult ) 
						{
							$i_log->result = '<b>'.$row_user->getError().'</b>';
						} else {
							$email_params['user_was_added'] = true;
														
							if ($JLMS_CONFIG->get('use_global_groups', 1)) {
								if($course_id_i != -3) {
									$i_log->result = '';
								} else {
									$i_log->result = 'OK';
								}
							} else {
								if($course_id_i != -3) {
									$i_log->result = '';
								} else {
								$i_log->result = 'OK';
							}
							}
							if( !JLMS_J16version() ) 
							{
								$row_user->checkin();
							}
							
							
							$user_id = $row_user->id;							
								
								
							if ($is_cb_installed) {
								$query = "INSERT INTO `#__comprofiler` (`id`,`user_id`) VALUES ('".$user_id."', '".$user_id."')";
								$db->SetQuery($query);
								$db->query();
							}
							
							if($course_id_i != -3) {								
								$query = "INSERT INTO #__lms_users_in_groups ( course_id, group_id, user_id, teacher_comment, enrol_time, publish_start, publish_end, start_date, end_date ) VALUES('".$course_id."', '".$local_group_id."', '".$user_id."', '".$teacher_comment."', '".JLMS_gmdate()."', '".$course_publish_start."', '".$course_publish_end."', '".$course_start_date."', '".$course_end_date."')";
								$db->SetQuery( $query );
								
								if ($db->query()) {																		
									$email_params['group_id'] = $local_group_id;
									$email_params['course_id'] = $course_id;																		
									
									$i_log->result .= 'OK';									
								} else {									
									$i_log->result .= 'failed';									
								}
							}
											
							if( $JLMS_CONFIG->get('use_global_groups', 1) ) 
                            {
    							if ($usergroup_field_present && $user_usergroup_str ) 
                                {
    								$global_group_id_2nd = 0;
    								foreach ($usergroups_arr as $usergroups_item) {
    									if ($usergroups_item->ug_name && $user_usergroup_str == $usergroups_item->ug_name ) {
    										$global_group_id_2nd = $usergroups_item->id;
    										break;
    									}
    								}
    								
    								if ($global_group_id_2nd) {
    									$query = "INSERT INTO #__lms_users_in_global_groups (group_id, user_id) VALUES('".$global_group_id_2nd."', '".$user_id."')";
    									$db->SetQuery( $query );
    									
    									if ($db->query()) {											
    										$email_params['group_id'] = $global_group_id_2nd;										
    										
    										$i_log->result .= str_replace('{gl_group_name}', $global_group_str_name, _JLMS_CSV_ADD_TO_GR_OK );
    									} else {
    										$i_log->result .= str_replace( '{gl_group_name}', $global_group_str_name, _JLMS_CSV_ADD_TO_GR_FAILED );
    									}
    								}
    							}
                                
                                if ($global_group_id) 
                                {     
                                    $email_params['group_id'] = $group_id;
                                    
    							    $i_log = addUserToGlobalGroupByGroupId( $group_id, $user_id, $i_log );            
                                                           						
    							}
                            }
						}
					} else {
						$i_log->result = _JLMS_CSV_USR_LMT_EXCEEDED;
					}
				} else {
					// 21.09.2007 - temporary fix for Joomla 1.5 (DEN), '_REGWARN_INUSE' is not available in 1.5
					if (( defined('_REGWARN_INUSE') && ($row_user->checked_msg == _REGWARN_INUSE || $row_user->checked_msg == _REGWARN_EMAIL_INUSE))
					|| (( class_exists('JText') && ($row_user->checked_msg == JText::_('WARNREG_INUSE') || $row_user->checked_msg == 'WARNREG_INUSE' || $row_user->checked_msg == JText::_( 'WARNREG_EMAIL_INUSE' )) || $row_user->checked_msg == JText::_('JLIB_DATABASE_ERROR_USERNAME_INUSE')) || $row_user->checked_msg == JText::_('JLIB_DATABASE_ERROR_EMAIL_INUSE') )) {
						if ($do_add) {
							$user_id = 0;
							// change userinfo (we don't know anything about his password)
							$i_log->userinfo = '<b>'.$row_user->username.'</b>, '.$row_user->name.' ('.$row_user->email.')';
							if ( (defined('_REGWARN_INUSE') && $row_user->checked_msg == _REGWARN_EMAIL_INUSE) 
							|| (( class_exists('JText') && ($row_user->checked_msg == JText::_('WARNREG_INUSE') || $row_user->checked_msg == 'WARNREG_INUSE' || $row_user->checked_msg == JText::_( 'WARNREG_EMAIL_INUSE' )) || $row_user->checked_msg == JText::_('JLIB_DATABASE_ERROR_EMAIL_INUSE')) )) {
								$query = "SELECT id FROM #__users WHERE email = '".$row_user->email."'";
								$db->SetQuery( $query );
								$user_id = $db->LoadResult();
								$i_log->result = _JLMS_CSV_USR_W_EMAIL_REGISTRED;
							}
							if ( (defined('_REGWARN_INUSE') && $row_user->checked_msg == _REGWARN_INUSE) 
							|| (( class_exists('JText') && ($row_user->checked_msg == JText::_('WARNREG_INUSE') || $row_user->checked_msg == 'WARNREG_INUSE' || $row_user->checked_msg == JText::_( 'WARNREG_EMAIL_INUSE' )) || $row_user->checked_msg == JText::_('JLIB_DATABASE_ERROR_USERNAME_INUSE')) )) {
								$query = "SELECT id FROM #__users WHERE username = '".$row_user->username."'";
								$db->SetQuery( $query );
								$user_id = $db->LoadResult();
								$i_log->result = _JLMS_CSV_USR_W_UNAME_REGISTRED;
							}
							if ($row_user->checked_msg == 'WARNREG_INUSE') {
								$row_user->checked_msg = _JLMS_CSV_USR_UNAME_IN_USE;
							} elseif ($row_user->checked_msg == 'WARNREG_EMAIL_INUSE') {
								$row_user->checked_msg = _JLMS_CSV_EMAIL_REGISTRED;
							}
							$xid = 0;
							if ($user_id && ($course_id_i != -3) ) {
								$query = "SELECT user_id FROM #__lms_users_in_groups WHERE course_id = $course_id AND user_id = $user_id";
								$db->SetQuery( $query );
								$xid = intval( $db->loadResult() );
							}
							if (!$xid && ($user_id > 0) && ($course_id_i != -3) ) {
								$query = "INSERT INTO #__lms_users_in_groups (course_id, group_id, user_id, teacher_comment, enrol_time, publish_start, publish_end, start_date, end_date ) VALUES('".$course_id."', '".$local_group_id."', '".$user_id."', ".$db->quote($teacher_comment).", '".JLMS_gmdate()."', '".$course_publish_start."', '".$course_publish_end."', '".$course_start_date."', '".$course_end_date."')";
								$db->SetQuery( $query );
								if ($db->query()) {											
										
									$email_params['group_id'] = $local_group_id;
									$email_params['course_id'] = $course_id;
																		
									$i_log->result .= _JLMS_CSV_ADD_TO_CRS_OK;
								} else {
									$i_log->result .= _JLMS_CSV_ADD_TO_CRS_FAILED;
								}
							} else {
								if ($course_id_i != -3) {
									$i_log->result = _JLMS_CSV_USR_ALR_ENROLL_IN_CRS;
								} else {
									$i_log->result = $row_user->checked_msg;
								}
							}
									if ($JLMS_CONFIG->get('use_global_groups', 1)) {
										if ($global_group_id) {
										      $email_params['group_id'] = $group_id;
                                              
										      $i_log = addUserToGlobalGroupByGroupId( $group_id, $user_id, $i_log );
										}
									}

						} else {
							$i_log->result = _JLMS_CSV_USR_LMT_EXCEEDED;
						}
					} else {
						$i_log->result = $row_user->checked_msg;
					}
				}
				$import_log[] = $i_log;
								
				if( $email_params['user_was_added'] || $email_params['course_id'] || $email_params['group_id'] ) 
				{	
					//send email to import user					 
					$course = new stdClass();
					$course->course_alias = '';
					$course->course_name = '';
													
					$query = "SELECT course_name, name_alias FROM #__lms_courses WHERE id = '".$email_params['course_id']."'";
					$db->setQuery( $query );
					$course = $db->loadObject();								
					
					$user_group = new stdClass(); 
					$user_group->ug_name = '';	
														
					$query = "SELECT ug_name  FROM #__lms_usergroups WHERE id = '".$email_params['group_id']."'";
					$db->setQuery( $query );
					$user_group = $db->loadObject();
					
					$params['user_id'] = $user_id;
					$params['course_id'] = $email_params['course_id'];									
					
					$Item = $JLMS_CONFIG->get('Itemid');		
																			
					$params['markers']['{password}'] = $pwd;
					$params['markers']['{email}'] = $row_user->email;
					$params['markers']['{name}'] = $row_user->name;
					$params['markers']['{username}'] = $row_user->username;
					
					$Itemid = $JLMS_CONFIG->get('Itemid');					
					
					$lmslink = $JLMS_CONFIG->get('live_site').'/index.php?option=com_joomla_lms&Itemid='.$Itemid;
					$params['markers']['{lmslink}'] = '<a href="'.$lmslink.'">'.$lmslink.'</a>';
					$params['markers_nohtml']['{lmslink}'] = $lmslink;			
					
					$params['wrappers']['{ifcreated}'] = 'hide';
					$params['wrappers']['{ifcourse}'] = 'hide';
					$params['wrappers']['{ifgroup}'] = 'hide';				
					
					if( $email_params['user_was_added'] ) 
						$params['wrappers']['{ifcreated}'] = 'show';
					if( $email_params['course_id'] )
						$params['wrappers']['{ifcourse}'] = 'show';
					if( $email_params['group_id'] )
						$params['wrappers']['{ifgroup}'] = 'show';					
					
					if( isset($course) )
						$params['markers']['{coursename}'] = $course->course_name;//( $course->course_alias )?$course->course_alias:$course->course_name;
					
					if( isset($user_group) )	
						$params['markers']['{groupname}'] = $user_group->ug_name;
						
					$params['action_name'] = 'OnCSVImportUser';				
													
					$_JLMS_PLUGINS->loadBotGroup('emails');
					$plugin_result_array = $_JLMS_PLUGINS->trigger('OnCSVImportUser', array (& $params));
                    
                    if( $params['course_id'] ) 
                    {
						$_JLMS_PLUGINS->loadBotGroup('system');
						$_JLMS_PLUGINS->trigger('OnCSVImportUserToCourse', array (& $params));						
                    }                  
				}
			}
		} // end if ( isset($_FILES['csv_file_import']) )
		//	echo '<pre>';print_r($import_log);echo '</pre>';
		joomla_lms_adm_html::showLog( $import_log, $option, $course_id, $group_id, 'CSV Import Log', 'csv_import' );
	} else { // end if (checks)
		mosRedirect( "index.php?option=$option&task=csv_operations" );
	}
}

function addUserToGlobalGroupByGroupId( $group_id, $user_id, $i_log ) 
{
    static $impGroup;
    
    if( !$group_id ) return false; 
    
    $db = JFactory::getDbo();
    
    if( !isset($impGroup) ) 
    {
        $query = "SELECT * FROM #__lms_usergroups WHERE id = ".$group_id;
        $db->setQuery($query);      
        $impGroup = $db->loadObject();
    }   
       
    if( $impGroup->parent_id ) 
    {
         $query = "SELECT count(*) FROM #__lms_users_in_global_groups WHERE user_id = $user_id AND group_id = ".$impGroup->parent_id." AND subgroup1_id = ".$impGroup->id;
         $db->SetQuery( $query );
         $isSubExist = $db->LoadResult();         
         
        $query = "SELECT count(*) FROM #__lms_users_in_global_groups WHERE user_id = $user_id AND group_id = ".$impGroup->parent_id;        
    } else {
        $query = "SELECT count(*) FROM #__lms_users_in_global_groups WHERE user_id = $user_id AND group_id = ".$impGroup->id;
    } 
    
    $db->SetQuery( $query );
    $isExist = $db->LoadResult();    
    
    $query = '';                           
    
    if ( $impGroup->parent_id && !$isSubExist ) 
    {            
        if( $isExist )
        {
            $query = "UPDATE #__lms_users_in_global_groups SET subgroup1_id = ".$impGroup->id." WHERE group_id = ".$impGroup->parent_id." AND user_id = ".$user_id;                                                                                                             
        } else {
            $query = "INSERT INTO #__lms_users_in_global_groups (group_id, user_id, role_id, subgroup1_id ) VALUES('".(($impGroup->parent_id)?$impGroup->parent_id:$impGroup->id)."', '".$user_id."', 0, '".(($impGroup->parent_id)?$impGroup->id:0)."')";
        }    
                                                    
    } else if ( !$isExist ) 
    {                                                
        $query = "INSERT INTO #__lms_users_in_global_groups (group_id, user_id ) VALUES('".$impGroup->id."', '".$user_id."')";
    }  
       
    if( $query ) 
    {        
       $db->SetQuery( $query );        
        
        if ($db->query()) 
        {										
    				
    		$email_params['group_id'] = $impGroup->id;									
    		
    		$i_log->result .= str_replace('{gl_group_name}', $impGroup->ug_name, _JLMS_CSV_ADD_TO_GR_OK );
    	} else {
    		$i_log->result .= str_replace('{gl_group_name}', $impGroup->ug_name, _JLMS_CSV_ADD_TO_GR_FAILED );
    	}
    } else {
        $i_log->result .= str_replace('{gl_group_name}', $impGroup->ug_name, _JLMS_CSV_USR_ALR_PART_IN_GR);
    } 
    
    return $i_log;
}               

function jlms_csvDelete( $option ) {
	$db = JFactory::getDBO();
	$sel_courses = intval(mosGetParam($_REQUEST, 'sel_courses', 3));
	$del_type = intval(mosGetParam($_REQUEST, 'del_type', 0));
	$courses_ids1 = mosGetParam($_REQUEST, 'course_id_del', array(0));
	$courses_ids = implode(',', $courses_ids1);
	$group_id = intval(mosGetParam($_REQUEST, 'class_id_delete', 0));
	$courses_names = '';
	$group_name = '';
	if ((isset($courses_ids1[0]) && intval($courses_ids1[0]) === 0) || (isset($courses_ids1[0]) && intval($courses_ids1[0]) === -1)) {
		$query = "SELECT id FROM #__lms_courses";
		$db->SetQuery( $query );
		$courses_ids2 = JLMSDatabaseHelper::loadResultArray();
		$courses_ids = implode(',',$courses_ids2);
		if (!$courses_ids) {
			$courses_ids = '0';
		}
		$courses_names = 'all';
		if ($group_id) {
			$query = "SELECT ug_name FROM #__lms_usersgroups WHERE course_id = 0 AND id = $group_id";
			$db->SetQuery( $query );
			$group_name = $db->LoadResult();
		}
	}
	if ((isset($courses_ids1[0]) && intval($courses_ids1[0]) === -1)) {
		$del_type = 1;
		$courses_names = 'all';
	} elseif ((isset($courses_ids1[0]) && intval($courses_ids1[0]) === -3)) {
		$del_type = 2;
		if ($group_id) {
			$query = "SELECT ug_name FROM #__lms_usergroups WHERE course_id = 0 AND id = $group_id";
			$db->SetQuery( $query );
			$group_name = $db->LoadResult();
		}
	} else {
		$del_type = 0;
		$query = "SELECT course_name FROM #__lms_courses WHERE id IN (".$courses_ids.")";
		$db->SetQuery( $query );
		$courses_names = JLMSDatabaseHelper::loadResultArray();
		$courses_names = implode(',',$courses_names);
		if (isset($courses_ids1[0]) && intval($courses_ids1[0]) === 0) {
			$courses_names = 'all';
		}
	}

	if ( isset($_FILES['csv_file_delete']) ) {
		require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_csv_import.php");
		// make arrays of valid fields for error checking
		$fieldDescriptors = new DeImportFieldDescriptors();
		$fieldDescriptors->addRequired('username');
		$fieldDescriptors->addRequired('name');
		$fieldDescriptors->addRequired('email');

		$userfile			= mosGetParam($_FILES, 'csv_file_delete', null);
		$userfileTempName	= $userfile['tmp_name'];
		$userfileName 		= $userfile['name'];

		$loader		= new DeCsvLoader();
		$loader->setFileName($userfileTempName);
		if (!$loader->load()) {
			echo "<script> alert('import failed: ".$loader->getErrorMessage()."'); window.history.go(-1); </script>\n"; exit();
		}
		if (!JLMS_prepareImport($loader, $fieldDescriptors, true)) {
			echo "<script> alert('import failed'); window.history.go(-1); </script>\n"; exit();
		}
		$requiredFieldNames	= $fieldDescriptors->getRequiredFieldNames();
		$allFieldNames		= $fieldDescriptors->getFieldNames();
		// Prepare the data to be imported but first validate all entries before eventually importing
		// (a bit like a software transaction)
		$usernames	= array();
		$emails	= array();
		$ii = 0;
		while(!$loader->isEof()) {
			$values		= $loader->getNextValues();
			if (!JLMS_prepareImportRow($loader, $fieldDescriptors, $values, $requiredFieldNames, $allFieldNames, true)) {
				/*echo "<script> alert('".$ii." row import failed'); window.history.go(-1); </script>\n"; exit();*/
			} else {
				$usernames[] = $db->Quote($values['username']);
				$emails[] = $db->Quote($values['email']);
			}
			$ii++;
		}
		$del_users = array();
		if (!empty($usernames) && !empty($emails)) {
			$query = "SELECT a.* FROM #__users AS a "
			."WHERE (a.username IN ( ".implode(",",$usernames)." ) OR a.email IN ( ".implode(",",$emails)." )) ";
			$db->SetQuery( $query );
			$del_users = $db->LoadObjectList();
		}
		if (count($del_users)) {
			$lists = array();
			joomla_lms_adm_html::confirm_delUsers( $del_users, $lists, $option, $sel_courses, $del_type, $courses_ids, $group_id, $courses_names, $group_name );
		} else {
			mosRedirect( "index.php?option=$option&task=csv_operations" );
		}
	} else {
		mosRedirect( "index.php?option=$option&task=csv_operations" );
	}
}

function jlms_csvDelete_yes( $option ) {
	$db = JFactory::getDBO();
	$user = JLMSFactory::getUser();

	$sel_courses = intval(mosGetParam($_REQUEST, 'sel_courses', 3));
	$del_type = intval(mosGetParam($_REQUEST, 'del_type', 0));
	$group_id_global = intval(mosGetParam($_REQUEST, 'group_id', 0));
	$courses_ids_str = mosGetParam($_REQUEST, 'courses_ids', '');
	$courses_ids = explode(',', $courses_ids_str);
	$del_users_count = 0;
	
	$db->setQuery("SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'is_cb_installed' ");
	$is_cb_installed = $db->loadResult();


	$cid = mosGetParam( $_POST, 'cid', array(0) );
	if (!is_array( $cid )) { $cid = array(0); }
	$i = 0;
	while ($i < count($cid)) {
		$cid[$i] = intval($cid[$i]);
		$i ++;
	}
	$del_ids = $cid;
	$del_ids_str = implode(',',$cid);

	@set_time_limit('3000');

	if (count($del_ids)) {
		
		if ($del_type != 2)
		{
			require_once(_JOOMLMS_FRONT_HOME.'/joomla_lms.main.php');
			require_once(_JOOMLMS_FRONT_HOME."/includes/lms_del_operations.php");
			
			if ($del_type == 0) $del_users_count = count($del_ids);
			
			foreach($courses_ids as $course_id) 
			{
				$group_ids = array();
				$query = "SELECT a.id FROM #__lms_usergroups AS a WHERE a.course_id = $course_id";
				$db->SetQuery( $query );
				$group_ids = JLMSDatabaseHelper::loadResultArray();
				$group_ids[] = 0;
				
				foreach($group_ids as $group_id) {
					JLMS_DelOp_deleteCourseStudents( $course_id, $group_id, $del_ids );
				}
			
				// update owner id only for point "from Joomla CMS (entire remove)"
				if ($del_type == 1) 
				{	
					$query = "UPDATE #__lms_courses SET owner_id = ".$user->get('id')." WHERE id = $course_id AND owner_id IN ($del_ids_str) ";
					$db->SetQuery( $query );
					$db->Query();
				}
			
			}
		}
		
		if ($del_type == 1) // delete Joomla user only for point "from Joomla CMS (entire remove)"
		{
			if (JLMS_J16version())
			{
				jimport( 'joomla.access.access' );
			}else
			{
				$acl = JFactory::getACL();
			}
			
			foreach($del_ids as $del_uid) {
				
				if (isset($del_user)) {unset($del_user);}
				$del_uid = (int)$del_uid;
				$can_user_del = false;
				
				
				$del_user = new mosUser( $db );				
				$del_user->load($del_uid);
				
				if (JLMS_J16version())
				{
					if ((!$user->authorise('core.admin')) && JAccess::check($del_uid, 'core.admin')) 
					{
						$can_user_del = false;
					}
					else
					{
						$can_user_del = true;
					}
				}else //ACL check for J!1.5
				{										
					$uobjectID 	= $acl->get_object_id( 'users', $del_uid, 'ARO' );
					$ugroups 	= $acl->get_object_groups( $uobjectID, 'ARO' );
					$this_ugroup = strtolower( $acl->get_group_name( $ugroups[0], 'ARO' ) );
					
					if ( $this_ugroup == 'super administrator' || ( $del_uid == $user->get('id')) || (( $this_ugroup == 'administrator' ) && ( $user->get( 'gid' ) == 24 )) )
					{
						$can_user_del = false;
					}
					else
					{
						$count = 2;
						if ( $del_user->get( 'gid' ) == 25 )
						{
							$query = 'SELECT COUNT( id ) FROM #__users WHERE gid = 25 AND block = 0';
							$db->setQuery( $query );
							$count = $db->loadResult();
						}
						if ( $count <= 1 && $del_user->get( 'gid' ) == 25 )
						{
							$can_user_del = false;
						}
						else
						{
							$can_user_del = true;
						}
					}
				}

				if ($can_user_del)
				{					
					$del_user->delete($del_uid);
					
					$query = "DELETE FROM #__lms_users_in_global_groups WHERE user_id =".$del_uid;
					$db->SetQuery( $query );
					$db->Query();
					
					$del_users_count++;
				}				
			}
		}
		if ($del_type == 2) {
			$query = "DELETE FROM #__lms_users_in_global_groups WHERE user_id IN ($del_ids_str) AND group_id = $group_id_global";
			$db->SetQuery( $query );
			$db->Query();
			
			$del_users_count = count($del_ids);
		}
	}
	
	JFactory::getApplication()->enqueueMessage('Successfully deleted '.$del_users_count.' user(s)');
				
	mosRedirect( "index.php?option=$option&task=csv_delete" );
}
##########################################################################
###	--- ---   SUBSCRIPTIONS 	--- --- ###
##########################################################################
function jlms_latestVersion(){
	global $lms_version;

	require_once( JPATH_SITE . DS . 'administrator' . DS . 'components' . DS . 'com_joomla_lms' . DS . 'Snoopy.class.php' );

	$s = new Snoopy();
	$s->read_timeout = 90;
	$s->referer = JURI::root();
	@$s->fetch('http://www.joomlalms.com/lms_version_check/joomlaLmsVersion.php?current_version='.urlencode($lms_version));
	$version_info = $s->results;
	$version_info_pos = strpos($version_info, ":");
	if ($version_info_pos === false) {
		$version = $version_info;
		$info = null;
	} else {
		$version = substr( $version_info, 0, $version_info_pos );
		$info = substr( $version_info, $version_info_pos + 1 );
	}
	if($s->error || $s->status != 200){
		echo '<font color="red">Connection to update server failed: ERROR: ' . $s->error . ($s->status == -100 ? 'Timeout' : $s->status).'</font>';
	} else if($version == $lms_version){
		echo '<font color="green" style="font-weight:bold;">' . $version . '</font>' . $info;
	} else {
		echo '<font color="red" style="font-weight:bold;">' . $version . '</font>' . $info;
	}

	die;
}
function configSubscription($option) {
	$db = JFactory::getDBO();
	$row = array();
	$query = "SELECT * FROM #__lms_subscriptions_config";
	$db->setQuery($query);
	$row = $db->loadObjectList();
	//var_dump($row);
	if(!count($row)) $row[0] = new stdClass();
	joomla_lms_adm_html::configSubscription($row[0], $option);
}

function saveSubsConfig($option) {
	$db = JFactory::getDBO();

	$site_name = strval(mosGetParam($_REQUEST, 'site_name', ''));
	$site_descr = strval(mosGetParam($_REQUEST, 'site_descr', ''));
	$comp_descr = strval(mosGetParam($_REQUEST, 'comp_descr', ''));
	$comments = strval(mosGetParam($_REQUEST, 'comments', ''));
	$invoice_descr = strval(mosGetParam($_REQUEST, 'invoice_descr', ''));
	$thanks_text = strval(mosGetParam($_REQUEST, 'thanks_text', ''));
	$mail_subj = strval(mosGetParam($_REQUEST, 'mail_subj', ''));
	$mail_body = strval(mosGetParam($_REQUEST, 'mail_body', ''));
	
	$site_name = (get_magic_quotes_gpc()) ? stripslashes( $site_name ) : $site_name;
	$site_descr = (get_magic_quotes_gpc()) ? stripslashes( $site_descr ) : $site_descr;
	$comp_descr = (get_magic_quotes_gpc()) ? stripslashes( $comp_descr ) : $comp_descr;
	$comments = (get_magic_quotes_gpc()) ? stripslashes( $comments ) : $comments;
	$invoice_descr = (get_magic_quotes_gpc()) ? stripslashes( $invoice_descr ) : $invoice_descr;
	$thanks_text = (get_magic_quotes_gpc()) ? stripslashes( $thanks_text ) : $thanks_text;
	$mail_subj = (get_magic_quotes_gpc()) ? stripslashes( $mail_subj ) : $mail_subj;
	$mail_body = (get_magic_quotes_gpc()) ? stripslashes( $mail_body ) : $mail_body;

	$query = "SELECT COUNT(*) FROM #__lms_subscriptions_config";
	$db->setQuery($query);
	if($db->loadResult())
	{
		$query = "UPDATE #__lms_subscriptions_config SET site_name = ".$db->quote($site_name).", site_descr = ".$db->quote($site_descr).", comp_descr = ".$db->quote($comp_descr).", comments = ".$db->quote($comments).", invoice_descr = ".$db->quote($invoice_descr).", thanks_text = ".$db->quote($thanks_text).", mail_subj = ".$db->quote($mail_subj).", mail_body = ".$db->quote($mail_body)."";
	}
	else
	{
		$query = "INSERT INTO #__lms_subscriptions_config(site_name,site_descr,comp_descr,comments,invoice_descr,thanks_text,mail_subj,mail_body)";
		$query .= "\n VALUES (".$db->quote($site_name).", ".$db->quote($site_descr).", ".$db->quote($comp_descr).", ".$db->quote($comments).", ".$db->quote($invoice_descr).", ".$db->quote($thanks_text).", ".$db->quote($mail_subj).", ".$db->quote($mail_body).")";
	}
	$db->setQuery($query);
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
	}
	$msg = _JLMS_SUBS_MSG_CONFIG_SAVED;
	mosRedirect( "index.php?option=$option&task=config_subscriptions", $msg );
}
// countries
function jlms_showCountriesList( $option ) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	
	$row = new jlms_adm_config();
	$row->bind($_POST);

	$row->loadFromDb( $db );
	
	$row->default_tax_type = isset($row->default_tax_type) ? $row->default_tax_type : 1;
	$row->default_tax = (isset($row->default_tax) && $row->default_tax) ? $row->default_tax : 0;
	
	$lists = array();
	$lists['enabletax'] 		= mosHTML::yesnoRadioList( 'enabletax', 'class="inputbox"',	$row->enabletax );
	
	$row->get_country_info = isset($row->get_country_info) ? $row->get_country_info : 0;
	
	$arr = array();
	$arr[] = mosHTML::makeOption(0, 'IP');
	$arr[] = mosHTML::makeOption(1, 'CB Profile');
	$lists['get_country_info']	= mosHTML::selectList($arr, 'get_country_info', '', 'value', 'text',  $row->get_country_info);
	
	// get the total number of records
	$query = "SELECT COUNT(*)"
	. "\n FROM #__lms_subscriptions_countries AS cd"
	;
	$db->setQuery( $query );
	$total = $db->loadResult();

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	// get the subset (based on limits) of required records
	$query = "SELECT * "
	. "\n FROM #__lms_subscriptions_countries AS cd"
	. "\n ORDER BY cd.name ASC"
	. "\n LIMIT $pageNav->limitstart, $pageNav->limit"
	;
	$db->setQuery( $query );
	$rows = $db->loadObjectList();

	joomla_lms_adm_html::jlms_showCountriesList( $rows, $lists, $row, $pageNav, $option);
}

function jlms_saveDefaultTax($option){
	$db = JFactory::getDBO();
	$row = new jlms_adm_config();
	$row->loadFromDb( $db );
	$row->attendance_days = unserialize($row->attendance_days);
	$row->bind($_POST);
	$row->saveToDb( $db );
	mosRedirect ("index.php?option=com_joomla_lms&task=countrieslist", _JLMS_SUBS_MSG_SETTS_SAVED );
}

function jlms_editCountry( $id, $option ) {
	$db = JFactory::getDBO();
	$user = JLMSFactory::getUser();
	
	$GLOBALS['jlms_toolbar_id'] = $id;
	$row = new mos_Joomla_LMS_country( $db );
	// load the row from the db table
	$row->load( $id );

	if ($id) {
		// do stuff for existing records
		//$row->checkout($user->get('id'));
	} else {
		// do stuff for new records
		$row->published = 1;
	}
	$lists = array();

	// build the html radio buttons for published
	$lists['published'] = mosHTML::yesnoradioList( 'published', '', $row->published );

	joomla_lms_adm_html::jlms_editCountry( $row, $lists, $option, $params );
}

function jlms_saveCountry( $option ) {
	$db = JFactory::getDBO();

	$row = new mos_Joomla_LMS_country( $db );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	// save params
	$params = mosGetParam( $_POST, 'params', '' );
	if (is_array( $params )) {
		$txt = array();
		foreach ( $params as $k=>$v) {
			$txt[] = "$k=$v";
		}
		$row->params = implode( "\n", $txt );
	}

	// pre-save checks
	if (!$row->check()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	// save the changes
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	//$row->checkin();
	 if (!JLMS_J30version()) $row->updateOrder();

	mosRedirect( "index.php?option=$option&task=countrieslist" );
}

function jlms_removeCountry( &$cid, $option ) {
	$db = JFactory::getDBO();

	if (count( $cid )) {
		$cids = implode( ',', $cid );
		$query = "DELETE FROM #__lms_subscriptions_countries"
		. "\n WHERE id IN ( $cids )"
		;
		$db->setQuery( $query );
		if (!$db->query()) {
			echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}

	mosRedirect( "index.php?option=$option&task=countrieslist" );
}

function jlms_changeCountry( $cid=null, $state=0, $option ) {
	$db = JFactory::getDBO();
	$user = JLMSFactory::getUser();

	if (!is_array( $cid ) || count( $cid ) < 1) {
		$msg = $publish ? _JLMS_SUBS_MSG_SLCT_TO_PUBL : _JLMS_SUBS_MSG_SLCT_TO_UNPUBL;
		echo "<script> alert('".$msg."'); window.history.go(-1);</script>\n";
		exit();
	}

	$cids = implode( ',', $cid );

	$query = "UPDATE #__lms_subscriptions_countries"
	. "\n SET published = " . intval( $state )
	. "\n WHERE id IN ( $cids )"
	//. "\n AND ( checked_out = 0 OR ( checked_out = ".$user->get('id')." ) )"
	;
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}
	mosRedirect( "index.php?option=$option&task=countrieslist" );
}

function jlms_cancelCountry() {
	$db = JFactory::getDBO();
	$row = new mos_Joomla_LMS_country( $db );
	$row->bind( $_POST );
	//$row->checkin();
	mosRedirect( "index.php?option=com_joomla_lms&task=countrieslist" );
}

//end countries
function JLMS_saveSubscription($option, $is_apply = false) {
	$db = JFactory::getDBO();
	
	$restricted = intval(mosGetParam($_REQUEST, 'restricted', 0));
	if($restricted){
		$restricted_groups = mosGetParam($_REQUEST, 'restricted_groups', array());		
		if(count($restricted_groups)){
			$str_restricted_groups = '';
			$divider = '|';
			foreach($restricted_groups as $r_group){
				$str_restricted_groups .= $divider.$r_group;	
			}
			$str_restricted_groups .= $divider;
			$_POST['restricted_groups'] = $str_restricted_groups;
		}
	} else {
		$_POST['restricted_groups'] = '';
	}
		
	$row = new mos_Joomla_LMS_Subscription( $db );

	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	$row->sub_name = get_magic_quotes_gpc() ? stripslashes( $row->sub_name ) : $row->sub_name;
	$row->sub_descr = get_magic_quotes_gpc() ? stripslashes( $row->sub_descr ) : $row->sub_descr;
	$row->date = date('Y-m-d H:i:s');
	
	//izvrascheniya prodolzhayutsya...teper' nado normal'no zanesti eti id vo vse tablicy
	$matches = array();
	if(!is_numeric($row->account_type))
	{		
		preg_match('/6:(\d+)/', $row->account_type, $matches);
		$row->account_type = 6;
		if ($row->id)
		{
			$query = "DELETE FROM `#__lms_plans_subscriptions` WHERE subscr_id = '".$row->id."' ";
			$db->setQuery($query);
			$db->query();
		}
	}	

	if (!$row->check()) {		
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if (!$row->store()) {						
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

if (isset($matches[1]))
	{
		$query = "INSERT INTO `#__lms_plans_subscriptions` (`subscr_id`, `plan_id`) VALUES (".$row->id.", ".$matches[1].")";		
		$db->setQuery($query);
		$db->query();
	}

	$course_in_sub = mosGetParam($_REQUEST, 'courses_in_sub', '');
	$val = '';
	for ($i= 0; $i<count($course_in_sub);$i++){
		$val .= "('".$row->id."' , '".$course_in_sub[$i]."') ";
		if (($i+1) != count($course_in_sub)){
			$val .= ',';
		}
	}	
	
	$query = "DELETE FROM `#__lms_subscriptions_courses` WHERE sub_id = '".$row->id."' ";
	$db->setQuery($query);
	$db->query();

	$query = "INSERT INTO `#__lms_subscriptions_courses` (sub_id , course_id) VALUES $val ";
	$db->setQuery($query);
	$db->query();

	if ($is_apply) {
		mosRedirect( "index.php?option=$option&task=editA_subscription&id=$row->id" );
	} else {
		mosRedirect( "index.php?option=$option&task=subscriptions" );
	}
}


function JLMS_removeFromSubscriptions( $option ) {
	$db = JFactory::getDBO();
	$cid 	= mosGetParam( $_REQUEST, 'cid', array(0) );
	$group_id = intval( mosGetParam( $_REQUEST, 'group_id', 0 ) );
	$cid = implode(',',$cid);
	$query = "DELETE FROM #__lms_subscriptions WHERE id IN ($cid) ";
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
	}
	$query = "DELETE FROM #__lms_subscriptions_courses WHERE sub_id IN ($cid) ";
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
	}
	
	$query = "DELETE FROM `#__lms_plans_subscriptions` WHERE subscr_id IN ($cid) ";
	$db->setQuery($query);
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
	}
			
	mosRedirect("index.php?option=$option&task=subscriptions", _JLMS_SUBS_MSG_DELETED );
}


function JLMS_ListSubscriptions( $option ) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$filter	= mosGetParam($_REQUEST,'status_filter',0);
	$limit		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );

	$where = '';
	if($filter == 1){
		$where = " WHERE s.published = '1'";
	}elseif($filter == 2){
		$where = " WHERE s.published = '0'";
	}

	$query = "SELECT count(s.id) FROM `#__lms_subscriptions` AS s $where ";
	$db->setQuery($query);
	$total = $db->loadResult();

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	$query = "SELECT s.*, p.name AS plan_name FROM `#__lms_subscriptions` AS s 
	LEFT JOIN #__lms_plans_subscriptions AS ps ON s.id = ps.subscr_id
	LEFT JOIN #__lms_plans AS p ON p.id = ps.plan_id   
	$where "
	. "\n ORDER BY s.sub_name, s.start_date, s.date DESC "
	. "\n LIMIT $pageNav->limitstart, $pageNav->limit "
	;
	$db->setQuery($query);
	$rows = $db->loadObjectList();
		
	$status_filter = array();
	$status_filter[] = mosHTML::makeOption(0, _JLMS_SUBS_ALL );
	$status_filter[] = mosHTML::makeOption('1', _JLMS_PUBLISHED );
	$status_filter[] = mosHTML::makeOption('2', _JLMS_UNPUBLISHED );
	$lists['status_filter'] = mosHTML::selectList( $status_filter, 'status_filter', 'class="inputbox" size="1" onchange="document.adminForm.submit( );"', 'value', 'text', "$filter" );
	joomla_lms_adm_html::JLMS_subscriptionsList( $rows, $pageNav, $option, $lists);
}

function JLMS_changeSubscriptions( $cid=null, $state=0, $option ) {
	$db = JFactory::getDBO();

	if (!is_array( $cid ) || count( $cid ) < 1) {
		$msg = $publish ? _JLMS_SUBS_MSG_SLCT_TO_PUBL_C : _JLMS_SUBS_MSG_SLCT_TO_UNPUBL_C;
		echo "<script> alert('".$msg."'); window.history.go(-1);</script>\n";
		exit();
	}

	$cids = implode( ',', $cid );

	$query = "UPDATE #__lms_subscriptions"
	. "\n SET published = " . intval( $state )
	. "\n WHERE id IN ( $cids )"
	;
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}
	mosRedirect( "index.php?option=$option&task=subscriptions" );
}

function JLMS_NewEditSubscription( $id = 0 , $option ){	
	global $JLMS_CONFIG;
		
	$db = JFactory::getDBO();
	$GLOBALS['jlms_toolbar_id'] = $id;
	$query = "SELECT a.*, b.username, b.email, d.c_category FROM `#__lms_courses` as a "
	. "\n LEFT JOIN `#__lms_course_cats` as d ON d.id = a.cat_id "
	. "\n ,`#__users` as b "
	. "\n WHERE a.owner_id = b.id AND a.paid = 1 "
	. "\n ORDER BY a.course_name"
	;
	$db->SetQuery( $query );
	$rows = $db->LoadObjectList();
	
	$i = 0;
	while ($i < count($rows)) {
		$rows[$i]->course_name = $rows[$i]->course_name . '('.$rows[$i]->c_category.') ('.$rows[$i]->username.')';
		$i ++;
	}
	
	$query = "SELECT *" 
			."\n FROM #__lms_plans"
			."\n WHERE published=1";
	$db->setQuery($query);
	$plans = $db->loadObjectList();

	$subs = new mos_Joomla_LMS_Subscription( $db );
	$subs->load( $id );	
		
	if (!$id) {
		$subs->published = 1;
		$subs->account_type = 1;
		$subs->access_days = 10;
		$subs->price = 0;
		$subs->discount = 0;
		$subs->start_date = date('Y-m-d');
		$subs->end_date = date('Y-m-d');
		$subs->restricted = 0;
	} elseif ($subs->account_type == '6')
	{	
		$query = "SELECT plan_id" 
				."\n FROM #__lms_plans_subscriptions"
				."\n WHERE subscr_id=".$id;
		$db->setQuery($query);
		$subs->account_type = '6:'.$db->loadResult();
	}	

	$query = "SELECT a.*, b.username, b.email, d.c_category FROM `#__lms_courses` as a "
	. "\n LEFT JOIN `#__lms_course_cats` as d ON d.id = a.cat_id,`#__lms_subscriptions_courses` as c "
	. "\n ,`#__users` as b "
	. "\n WHERE a.owner_id = b.id AND c.sub_id = $id AND c.course_id = a.id "
	. "\n ORDER BY a.course_name"
	;
	$db->SetQuery( $query );
	$courses_in = $db->LoadObjectList();

	$i = 0;
	while ($i < count($courses_in)) {
		$courses_in[$i]->course_name = $courses_in[$i]->course_name . ' &nbsp;('.$courses_in[$i]->c_category.') ('.$courses_in[$i]->username.')';
		$i ++;
	}

	$lists = array();

	$lists['published']	= mosHTML::yesnoRadioList( 'published', 'class="inputbox"',	$subs->published);

	//$lists['courses_list']  = mosHTML::selectList( $rows, 'courses_list', 'style=\'width:340px\' size="10" multiple="multiple" ', 'id', 'course_name', '');
	$lists['courses_in_sub'] = mosHTML::selectList( $courses_in, 'courses_in_sub[]', 'class="chzn-done" style=\'width:340px\' size="10" multiple="multiple" ', 'id', 'course_name', '');
	
	if ($JLMS_CONFIG->get('use_global_groups', 1)) {
		$javascript = 'onclick="javascrript:view_fields(this);"';
		$lists['restricted']	= mosHTML::yesnoRadioList('restricted', 'class="inputbox" '.$javascript, $subs->restricted);
		
		$query = "SELECT id as value, ug_name as text FROM #__lms_usergroups WHERE course_id = 0";
		$db->setQuery($query);
		$db->query();
		$groups = $db->loadObjectList();
		
		$restricted_groups = array();
		$disabled = '';
		if(!$subs->restricted){
			$disabled = 'disabled="disabled"';
		} else {
			$restricted_groups = explode("|", $subs->restricted_groups);
		}
		$tmp = array();
		if(isset($restricted_groups) && count($restricted_groups)){
			foreach($restricted_groups as $r_group){
				if(intval($r_group) && $r_group){
					$tmp[] = $r_group;	
				}
			}
			$restricted_groups = $tmp;
		}
		
		$lists['restricted_groups'] = mosHTML::selectList( $groups, 'restricted_groups[]', 'class="chzn-done" size="10" multiple="multiple" style="width: 40%;" '.$disabled, 'value', 'text', $restricted_groups);
	}
		
	joomla_lms_adm_html::JLMS_createSubscription( $rows, $option, $lists, $subs, $courses_in, $plans);

}

function JLMS_ReNewSubscription( $id = 0 , $option ){
	$db = JFactory::getDBO();

	$rows = array();

	if ($id) {
		$query = "SELECT a.user_id, b.username, b.name FROM `#__lms_payments` AS a "
		. "\n  LEFT JOIN `#__users` AS b ON a.user_id = b.id "
		. "\n WHERE a.sub_id = $id AND LOWER(a.status) = 'completed' ";
		$db->SetQuery( $query );
		$rows1 = $db->LoadObjectList();
	
		$query = "SELECT a.user_id, b.username, b.name FROM `#__lms_payments` AS a "
		. "\n  LEFT JOIN `#__users` AS b ON a.user_id = b.id , `#__lms_payment_items` AS i"
		. "\n WHERE a.sub_id = 0 AND LOWER(a.status) = 'completed' AND a.payment_type = 2 AND a.id = i.payment_id AND i.item_id = $id AND i.item_type = 0";
		$db->SetQuery( $query );
		$rows2 = $db->LoadObjectList();
	
		$rows = array();
		foreach ($rows1 as $row1) {
			$rows[] = $row1;
		}
		foreach ($rows2 as $row2) {
			$is_exists = false;
			foreach ($rows1 as $row1) {
				if ($row1->user_id == $row2->user_id) {
					$is_exists = true;
					break;
				}
			}
			if (!$is_exists) {
				$rows[] = $row2;
			}
		}
		//TODO: sort the $rows alphabetically by username/name
	}

	if (count($rows)){
		joomla_lms_adm_html::JLMS_ReNewSubscription_HTML( $rows, $option, $id);
	}else{
		mosRedirect( "index.php?option=$option&task=subscriptions", _JLMS_SUBS_MSG_NO_COMPL_PAYMS );
	}
}

function JLMS_ReNewSubscriptionApply( $cid, $id, $option ){
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$db = JFactory::getDBO();
	$query = "SELECT * FROM `#__lms_subscriptions` WHERE id = $id";
	$db->setQuery($query);
	$subscription = $db->loadObject();

	$public_sub = new stdClass();
	$public_sub->publish_start = 1;
	$public_sub->start_date = '';
	$public_sub->publish_end = 1;
	$public_sub->end_date = '';
	if ($subscription->account_type == 2){
		$public_sub->start_date = $subscription->start_date;
		$public_sub->end_date = $subscription->end_date;
	}elseif($subscription->account_type == 3){
		$public_sub->start_date = $subscription->start_date;
		$public_sub->publish_end = 0;
	}elseif($subscription->account_type == 4){
		/*$public_sub->start_date = $subscription->start_date;
		$public_sub->end_date = $subscription->end_date;*/

		// 03 July 2007 (changed by DEN)
		$public_sub->start_date = date('Y-m-d');
		$public_sub->end_date = date('Y-m-d', time() + intval($subscription->access_days)*24*60*60);

	}else{
		$public_sub->publish_start = 0;
		$public_sub->publish_end = 0;
	}

	$query = "SELECT * FROM `#__lms_subscriptions_courses` WHERE sub_id = '".$id."' ";
	$db->setQuery($query);
	$courses = $db->loadObjectList();

	$values = '';$i = 1;

	foreach ($courses as $course){
		foreach ($cid as $user_id){
			$query = "SELECT * FROM `#__lms_users_in_groups` WHERE course_id = ".$course->course_id." AND user_id = ".$user_id." ";
			$db->setQuery($query);
			$test = $db->loadObject();
			if (is_object($test)){
				if ($test->publish_start){
					$test->publish_start = $public_sub->publish_start;
					if ($test->publish_start && (strtotime($test->start_date)> strtotime($public_sub->start_date) )){
						$test->start_date = $public_sub->start_date;
					}
				}
				if ($test->publish_end){
					$test->publish_end = $public_sub->publish_end;
					if ($test->publish_end && (strtotime($test->end_date) < strtotime($public_sub->end_date) )){
						$test->end_date = $public_sub->end_date;
					}
				}

				$query = "UPDATE `#__lms_users_in_groups` SET  publish_start = '".$test->publish_start."', start_date = '".$test->start_date."', publish_end = '".$test->publish_end."', end_date = '".$test->end_date."' WHERE course_id = ".$course->course_id." AND user_id = ".$user_id." ";
				$db->setQuery($query);
				$db->query();
			}
			else{
				$do_add = false;
				$query = "SELECT lms_config_value FROM #__lms_config WHERE lms_config_var = 'license_lms_users'";
				$db->SetQuery( $query );
				$license_lms_users = $db->LoadResult();
				if ($license_lms_users) {
					$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
					$db->SetQuery( $query );
					$total_students = $db->LoadResult();
					if (intval($total_students) < intval($license_lms_users)) {
						$do_add = true;
					}
					if (!$do_add) {
						$query = "SELECT count(*) FROM #__lms_users_in_groups WHERE user_id = '".$user_id."'";
						$db->SetQuery( $query );
						if ($db->LoadResult()) {
							$do_add = true;
						}
					}
				} else {
					$do_add = true;
				}
				if ($do_add) {
					$query = "INSERT INTO `#__lms_users_in_groups` (course_id, group_id, user_id, publish_start, start_date, publish_end, end_date ) VALUES ('".$course->course_id."', '0', '".$user_id."' ,'".$public_sub->publish_start."' ,'".$public_sub->start_date."','".$public_sub->publish_end."','".$public_sub->end_date."') ";
					$db->setQuery($query);
					$db->query();

					$user_info = new stdClass();
					$user_info->user_id = $user_id;
					$user_info->group_id = 0;
					$user_info->course_id = $course->course_id;
					$_JLMS_PLUGINS->loadBotGroup('user');
					$_JLMS_PLUGINS->trigger('onCourseJoin', array($user_info));
				}
			}
			$i++;
		}
	}
	mosRedirect("index.php?option=$option&task=subscriptions", _JLMS_SUBS_MSG_CHANGES_APPLIED);
}

function JLMS_PaymentsList( $option ) 
{
	global $JLMS_CONFIG;
	
	$app = JFactory::getApplication('administrator');
	$db = JFactory::getDBO();

	$filter		= intval( mosGetParam($_REQUEST, 'status_filter', 0));
	$proc_filter = intval( mosGetParam($_REQUEST, 'proc_filter', 0));
	$task = mosGetParam( $_REQUEST, 'task', 0 );
	
	if ($task == 'pays_list_pdf' || $task == 'pays_list_xls')
		{
		//ini_set("display_errors","1");
		//ini_set("error_reporting", E_ALL);
		ini_set("memory_limit", "450M"); 
		ini_set('max_execution_time', 160); 
		}
		
	$period_filter = JRequest::getVar('period_filter');
		
	$limit		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	$search_term = strval( $app->getUserStateFromRequest( "payments{$option}search_term", 'search_term', '' ) );
	$search_term = get_magic_quotes_gpc() ? stripslashes( $search_term ) : $search_term;
	$lists['search_term'] = $search_term;
	$where = '';
	if($filter){
		if($filter == 1){
			$filter_str = 'pending';	
		} elseif($filter == 2){
			$filter_str = 'Completed';	
		}
		$where .= " AND a.status = '".$filter_str."'";
		
	}
	
	if( $proc_filter ) 
	{		
		$where .= " AND a.proc_id = '".$proc_filter."'";
	}
	
	if( $period_filter ) 
	{			
		$arr = explode( '-', $period_filter );	
		$where .= " AND YEAR(a.date) = '".$arr[0]."' AND MONTH(a.date) = '".$arr[1]."'";
	}
	
	if ($search_term) {
		$search_term = '%'.$search_term.'%';
		$search_term = $db->quote($search_term);
		$where .= " AND (b.name like $search_term OR b.email like $search_term OR b.username like $search_term)";
	}

	$query = "SELECT COUNT(a.id)"
	. "\n FROM #__lms_payments as a WHERE a.proc_id = -10";
	$db->setQuery( $query );
	$special_orders = $db->loadResult();

	$query = "SELECT COUNT(a.id)"
	. "\n FROM #__lms_payments as a LEFT JOIN #__users as b ON a.user_id = b.id WHERE 1 = 1 $where";
	$db->setQuery( $query );
	$total = $db->loadResult();

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	$query = "SELECT a.*, b.username, c.sub_name, c.sub_name as course_name FROM `#__lms_payments` as a LEFT JOIN #__lms_subscriptions as c ON (a.sub_id = c.id AND a.payment_type = 0) LEFT JOIN `#__users` as b ON a.user_id = b.id"//, `#__lms_courses` as c "
	. "\n WHERE 1 = 1 "
	. "\n $where "
	. "\n ORDER BY date DESC, id DESC ";
	if( $task != 'pays_list_pdf' && $task != 'pays_list_xls' ) { $query .= " LIMIT $pageNav->limitstart, $pageNav->limit";}	
	$db->setQuery( $query );
	$rows = $db->loadObjectList();
	
	$query = "SELECT count(*) AS `count`, sum(a.amount) AS amount, a.cur_code
		FROM `#__lms_payments` AS a"
	. "\n WHERE a.status = 'completed' "
	.$where	
	. "\n GROUP BY a.cur_code";
	$db->setQuery( $query );
	$rowsAmounts = $db->loadObjectList();	
	
	$count_completed = 0;
	$total_compl_amount = array();	
	
	foreach( $rowsAmounts as $row ) 
	{		
		if( empty($row->cur_code) )
			$row->cur_code = $JLMS_CONFIG->get('jlms_cur_code');
			
		if( !isset($total_compl_amount[$row->cur_code]) )
			$total_compl_amount[$row->cur_code] = 0;
											
		$count_completed += $row->count;				
		$total_compl_amount[$row->cur_code] += $row->amount;		
	}		
	
	$params['count_completed'] = $count_completed;
	$params['total_compl_amount'] = $total_compl_amount; 
	
	//if( $task != 'pays_list_pdf' )
	//	$rows = array_slice( $rows, $pageNav->limitstart, $pageNav->limit);	
		
	$query = "SELECT lms_config_value FROM #__lms_config  WHERE lms_config_var = 'jlms_subscr_invoice_path'";
	$db->setQuery( $query );
	$file_path = $db->loadResult();

	if( $task != 'pays_list_pdf' && $task != 'pays_list_xls' )
	{
	for($j=0;$j<count($rows);$j++)
	{
		//TODO: move all db queries outside from 'for' loop
		$query = "SELECT filename FROM #__lms_subs_invoice WHERE subid = ". (int) $rows[$j]->id;
		$db->setQuery( $query );
		$dbfname = $db->loadResult();
		if($dbfname) {
			$rows[$j]->filename = $file_path.'/'.$dbfname;
		} else {
			$rows[$j]->filename = '';
		}
	}
	}
	$status_filter = array();
	$status_filter[] = mosHTML::makeOption(0, _JLMS_PAYS_ALL_ORDERS_ );
	$status_filter[] = mosHTML::makeOption(1, _JLMS_PENDING );
	$status_filter[] = mosHTML::makeOption(2, _JLMS_COMPLETED );
	$lists['status_filter'] = mosHTML::selectList( $status_filter, 'status_filter', 'class="text_area" size="1" style="width:264px" onchange="document.adminForm.submit();"', 'value', 'text', $filter);
	
	$query = "SELECT DATE_FORMAT( date, '%M') AS month, DATE_FORMAT( date, '%Y') AS year, DATE_FORMAT( date, '%Y-%m') AS value  
				FROM #__lms_payments 
				WHERE date > '2005-01-01' 
				GROUP BY DATE_FORMAT( date, '%Y-%m') 
				ORDER BY DATE_FORMAT( date, '%Y-%m') DESC";
	$db->setQuery( $query );
	$periods = $db->loadObjectList();
	
	for( $i=0; $i<count( $periods ); $i++) 
	{
		$periods[$i]->text = JText::_( $periods[$i]->month ).' '.$periods[$i]->year;
	}	
	
	$period_list = array();
	$period_list[] = mosHTML::makeOption(0, _JLMS_PAYS_ALL_PERIODS_ );	
	if( isset( $periods[0] )) $period_list = array_merge( $period_list, $periods );
	$lists['period_filter'] = mosHTML::selectList( $period_list, 'period_filter', 'class="text_area" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $period_filter);
		
	$query = "SELECT id AS value, name AS text FROM #__lms_subscriptions_procs";
	$db->setQuery( $query );
	$procs = $db->loadObjectList();
		
	$proc_list = array();
	$proc_list[] = mosHTML::makeOption(0, _JLMS_PAYS_ALL_PROCS_ );
	if ($special_orders) {
		$proc_list[] = mosHTML::makeOption(-10, _JLMS_PAYS_SPEC_ORDERS_PROCS );
	}
	
	if( isset( $procs[0] )) $proc_list = array_merge( $proc_list, $procs );
	$lists['proc_filter'] = mosHTML::selectList( $proc_list, 'proc_filter', 'class="text_area" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $proc_filter);
	
	if(isset($rows) && count($rows)){
		for($i=0;$i<count($rows);$i++){
			$rows[$i]->date = JLMS_offsetDateToDisplay($rows[$i]->date);
		}
	}
	
	if( $task == 'pays_list_pdf')
		joomla_lms_adm_html::JLMS_paymentsListPDF( $rows, $params );		
	else if( $task == 'pays_list_xls'){

		$search_term = strval( $app->getUserStateFromRequest( "payments{$option}search_term", 'search_term', '' ) );
		$search_term = get_magic_quotes_gpc() ? stripslashes( $search_term ) : $search_term;
		$xml_params = new stdclass();
		$xml_params->status = (isset($filter_str)?$filter_str:'');
		$xml_params->period_filter = (isset($period_filter)?$period_filter:'');
		$xml_params->search_term = (isset($search_term)?(htmlspecialchars($search_term)):'');
		$xml_params->proc_filter= (isset($proc_filter)?(htmlspecialchars($proc_filter)):'');
		$xml_params->procs = (isset($procs)?$procs:'');
		$xml_params->period_list = (isset($period_list)?$period_list:'');

		joomla_lms_adm_html::JLMS_paymentsListXLS( $rows, $xml_params );
	}else joomla_lms_adm_html::JLMS_paymentsList( $rows, $pageNav, $option, $lists, $params);
		
}

function JLMS_salesReport( $option ) 
{
	$app = JFactory::getApplication('administrator');
	$filter		= mosGetParam($_REQUEST, 'status_filter', 'all');
	$period_filter = JRequest::getVar('period_filter');
	$limit 		= intval( $app->getUserStateFromRequest( "saleslist_view_listlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "{$option}_saleslist_view_limitstart", 'limitstart', 0 ) );
	
	$search_term = strval( $app->getUserStateFromRequest( "payments{$option}search_term", 'search_term', '' ) );
	$search_term = get_magic_quotes_gpc() ? stripslashes( $search_term ) : $search_term;
	$lists['search_term'] = $search_term;
				
	$task = mosGetParam( $_REQUEST, 'task', 0 );
	
	$db = JFactory::getDBO();
	
	$conds = array();
	
	if( $filter != 'all' ) 
	{
		$conds[] = 's.published = '.$filter; 	
	}
	
	if( $period_filter ) 
	{			
		$arr = explode( '-', $period_filter );	
		$conds[] = "YEAR(p.date) = '".$arr[0]."' AND MONTH(p.date) = '".$arr[1]."'";
	}
	
	if ($search_term) {
		$search_term = '%'.$search_term.'%';
		$search_term = $db->quote($search_term);
		$conds[] = "s.sub_name like $search_term";
	}

	$where = '';
	if( isset($conds[0]))	
		$where = 'AND '.implode( ' AND ', $conds ); 
	
	$sql = "SELECT COUNT(*) FROM (
				SELECT s.id 
				FROM #__lms_subscriptions AS s, #__lms_payments AS p, #__lms_payment_items AS pi
				WHERE s.id = pi.item_id AND pi.payment_id = p.id AND p.status = 'completed' AND p.payment_type = '2'
				$where
				GROUP BY s.id
			) AS t
			"			
			;
	$db->setQuery( $sql );
	$total = $db->loadResult();
		
	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );
	
	$sql = "SELECT s.id, s.sub_name AS name, COUNT( s.id ) AS count
			FROM #__lms_subscriptions AS s, #__lms_payments AS p, #__lms_payment_items AS pi
			WHERE s.id = pi.item_id AND pi.payment_id = p.id AND p.status = 'completed' AND p.payment_type = '2'
			$where 
			GROUP BY s.id" 
			." ORDER BY s.sub_name"
			.(( $task == 'sales_report_pdf' )?'':" LIMIT $pageNav->limitstart, $pageNav->limit" )			
			;
	$db->setQuery( $sql );
	$rows = $db->loadObjectList();
				
	$status_filter = array();
	$status_filter[] = mosHTML::makeOption('all', _JLMS_PAYS_ALL_SUBS_ );
	$status_filter[] = mosHTML::makeOption('1', _JLMS_PAYS_PUBLISH_SUBS );
	$status_filter[] = mosHTML::makeOption('0', _JLMS_PAYS_UNPUBLISH_SUBS );
	
	$lists['status_filter'] = mosHTML::selectList( $status_filter, 'status_filter', 'class="text_area" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $filter);
	
	$query = "SELECT DATE_FORMAT( p.date, '%M') AS month, DATE_FORMAT( p.date, '%Y') AS year, DATE_FORMAT( p.date, '%Y-%m') AS value  
				FROM #__lms_subscriptions AS s, #__lms_payments AS p, #__lms_payment_items AS pi				 
				WHERE p.date > '2005-01-01' AND s.id = pi.item_id AND pi.payment_id = p.id AND p.status = 'completed' AND p.payment_type = '2'
				GROUP BY DATE_FORMAT( p.date, '%Y-%m') 
				ORDER BY DATE_FORMAT( p.date, '%Y-%m') DESC";
	$db->setQuery( $query );
	$periods = $db->loadObjectList();
	
	for( $i=0; $i<count( $periods ); $i++) 
	{
		$periods[$i]->text = JText::_( $periods[$i]->month ).' '.$periods[$i]->year;
	}	
	
	$period_list = array();
	$period_list[] = mosHTML::makeOption(0, _JLMS_PAYS_ALL_PERIODS_ );	
	if( isset( $periods[0] )) $period_list = array_merge( $period_list, $periods );
	$lists['period_filter'] = mosHTML::selectList( $period_list, 'period_filter', 'class="text_area" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $period_filter);
			
	
	if( $task == 'sales_report_pdf')
		joomla_lms_adm_html::JLMS_salesReportPDF( $rows );		
	else
		joomla_lms_adm_html::JLMS_salesReport( $rows, $pageNav, $option, $lists );
	
}
		

function JLMS_CreateNewPayment($option) {
	$db = JFactory::getDBO();

	$lists = array();
	$query = "SELECT id as value, username as text FROM #__users order by username";
	$db->setQuery( $query );
	$u_users = $db->loadObjectList();
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_SLCT_USER_);
	$list_users = array_merge( $list_users, $u_users );
	$lists['users'] = mosHTML::selectList( $list_users, 'user_id', 'class="text_area" size="1" style="width:266px" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', 0 );

	$query = "SELECT id as value, name as text FROM #__users ORDER BY name";
	$db->SetQuery($query);
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_NAME_);
	$pr = $db->loadObjectList();
	$list_users = array_merge( $list_users, $pr );
	$lists['users_names'] = mosHTML::selectList( $list_users, 'user_name', 'class="text_area" style="width:266px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', 0 );

	$query = "SELECT id as value, email as text FROM #__users ORDER BY email";
	$db->SetQuery($query);
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_OR_EMAIL_);
	$pr = $db->loadObjectList();
	$list_users = array_merge( $list_users, $pr );
	$lists['users_emails'] = mosHTML::selectList( $list_users, 'user_email', 'class="text_area" style="width:266px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', 0 );

	$query = "SELECT id as value, sub_name as text FROM #__lms_subscriptions ORDER BY sub_name";
	$db->SetQuery($query);
	$list_subs = array();
	$list_subs[] = mosHTML::makeOption( '0', _JLMS_SELECT_SUBSCRIPTION);
	$pr = $db->loadObjectList();
	$list_subs = array_merge( $list_subs, $pr );
	$lists['subscriptions'] = mosHTML::selectList( $list_subs, 'item_id', 'class="text_area" '.(JLMS_J30version()?'':'style="width:266px"').' size="1"', 'value', 'text', 0 );

	$query = "SELECT id as value, name as text FROM #__lms_subscriptions_procs ORDER BY name";
	$db->SetQuery($query);
	$list_pm = array();
	$list_pm[] = mosHTML::makeOption( '0', _JLMS_SELECT_PAYMENT_METHOD);
	$pr = $db->loadObjectList();
	$list_pm = array_merge( $list_pm, $pr );
	$lists['payment_methods'] = mosHTML::selectList( $list_pm, 'payment_method', 'class="text_area" '.(JLMS_J30version()?'':'style="width:266px"').' size="1"', 'value', 'text', 0 );

	$status[] = mosHTML::makeOption('Pending', _JLMS_PENDING );
	$status[] = mosHTML::makeOption('Completed', _JLMS_COMPLETED );
	$lists['status'] = mosHTML::selectList( $status, 'status', 'class="inputbox" size="1" ', 'value', 'text', 'Pending' );

	joomla_lms_adm_html::JLMS_createPayment( $option, $lists );
}

function JLMS_SaveNewPayment( $option ) {
	$db = JFactory::getDBO();
	$user_id = intval(mosGetParam($_POST, 'user_id', 0));
	$sub_id = intval(mosGetParam($_POST, 'item_id', 0));
	$paym_method = intval(mosGetParam($_POST, 'payment_method', 0));
	$status = strval(mosGetParam($_POST, 'status', 'Pending'));
	if ($user_id && $sub_id && $paym_method && ($status == "Completed" || $status == "Pending")) {
		$query = "SELECT name FROM #__lms_subscriptions_procs WHERE id = $paym_method";
		$db->SetQuery($query);
		$pm_name = $db->LoadResult();
		$pdate = mosGetParam($_POST, 'p_date', date('Y-m-d'));
		$amount = mosGetParam($_POST, 'p_amount', 0);
		$amount = number_format($amount, 2, '.', '');
		$query = "INSERT INTO #__lms_payments (payment_type, sub_id, txn_id, processor, status, type, amount, date, user_id ) VALUES (2, 0, '', ".$db->Quote($pm_name).", ".$db->Quote($status).", 0, ".$db->Quote($amount).", ".$db->Quote($pdate).", $user_id )";
		$db->SetQuery($query);
		$db->query();
		$pid = $db->insertid();
		$query = "INSERT INTO #__lms_payment_items (payment_id, item_id, item_type) VALUES ($pid, $sub_id, 0)";
		$db->SetQuery($query);
		$db->query();
		if ($status == "Completed") {
			$old_status = 'Pending';
			$query = "SELECT username FROM #__users WHERE id = $user_id";
			$db->SetQuery($query);
			$username = $db->LoadResult();
			$courses_id = array();
			$query = "SELECT distinct a.*, b.username, b.email, d.c_category FROM `#__lms_courses` as a "
			. "\n LEFT JOIN `#__lms_course_cats` as d ON d.id = a.cat_id,`#__lms_subscriptions_courses` as c "
			. "\n ,`#__users` as b "
			. "\n WHERE a.owner_id = b.id AND c.sub_id = $sub_id AND c.course_id = a.id ";
			$db->SetQuery( $query );
			$courses_in = $db->LoadObjectList();
			if (count($courses_in) && $username) {
				joomla_lms_adm_html::JLMS_editPaymentInfo_STEP2_HTML($status, $old_status, $courses_in, $pid, $username, $option);
			} else {
				mosRedirect("index.php?option=$option&task=payments", _JLMS_PAYS_PAYM_CREATED_FAILED_TO_ENROL);
			}
		} else {
			mosRedirect("index.php?option=$option&task=payments", _JLMS_PAYS_PAYM_CREATED);
		}
	} else {
		mosRedirect("index.php?option=$option&task=payments", _JLMS_PAYS_WRONG_PAYM_DETAILS);
	}
}

function JLMS_EditPaymentInfo( $id = 0 , $option ){
	$db = JFactory::getDBO();

	$proceed_to_edit = true;
	$query = "SELECT * FROM #__lms_payments as a WHERE a.id = $id";
	$db->SetQuery( $query );
	$payment = $db->LoadObject();
	
	$courses_in = array();
	$lists = array();
	if ( !$payment->payment_type ) {
		$query = "SELECT a.*, b.id as sub_id, b.sub_name,b.discount, b.account_type, b.start_date, b.end_date, b.access_days, c.username, c.email FROM `#__lms_payments` as a "
		. "\n LEFT JOIN `#__lms_subscriptions` as b ON b.id = a.sub_id, "
		. "\n `#__users` as c "
		. "\n WHERE a.id = $id AND a.user_id = c.id ";
		$db->SetQuery( $query );
		$order = $db->LoadObject();
		$query = "SELECT a.*, b.username, b.email, d.c_category FROM `#__lms_courses` as a "
		. "\n LEFT JOIN `#__lms_course_cats` as d ON d.id = a.cat_id,`#__lms_subscriptions_courses` as c "
		. "\n ,`#__users` as b "
		. "\n WHERE a.owner_id = b.id AND c.sub_id = ".$order->sub_id." AND c.course_id = a.id ";
		$db->SetQuery( $query );
		$courses_in = $db->LoadObjectList();
	} elseif ($payment->payment_type == 1) {
		$query = "SELECT a.*, b.id as sub_id, 'Custom subscription' as sub_name, '0' as discount, '1' as account_type, '' as start_date, '' as end_date, '' as access_days, c.username, c.email FROM `#__lms_payments` as a "
		. "\n LEFT JOIN `#__lms_subscriptions_custom` as b ON b.id = a.sub_id, "
		. "\n `#__users` as c "
		. "\n WHERE a.id = $id AND a.user_id = c.id ";
		$db->SetQuery( $query );
		$order = $db->LoadObject();
		$query = "SELECT a.*, b.username, b.email, d.c_category FROM `#__lms_courses` as a "
		. "\n LEFT JOIN `#__lms_course_cats` as d ON d.id = a.cat_id,`#__lms_subscriptions_custom_courses` as c "
		. "\n ,`#__users` as b "
		. "\n WHERE a.owner_id = b.id AND c.sub_id = ".$order->sub_id." AND c.course_id = a.id ";
		$db->SetQuery( $query );
		$courses_in = $db->LoadObjectList();
	} elseif ($payment->payment_type == 2) {
		$query = "SELECT a.*, b.filename as invoice_file, '0' as sub_id, 'Payment #$id' as sub_name, '0' as discount, '1' as account_type, '' as start_date, '' as end_date, '' as access_days, c.username, c.email FROM `#__lms_payments` as a LEFT JOIN #__lms_subs_invoice as b ON a.id = b.subid, "
		. "\n `#__users` as c "
		. "\n WHERE a.id = $id AND a.user_id = c.id ";
		$db->SetQuery( $query );
		$order = $db->LoadObject();
		$query = "SELECT * FROM `#__lms_payment_items` WHERE payment_id = ".intval($id);
		$db->setQuery($query);
		$payment_items = $db->LoadObjectList();
		$sub_courses = array();
		$sub_confs = array();
		foreach ($payment_items as $pitem) {
			if ($pitem->item_type == 0) {
				$sub_courses[] = $pitem->item_id;
			} elseif ($pitem->item_type == 1) {
				$sub_confs[] = $pitem->item_id;
			}
		}
		$subscriptions = array();
		if (count($sub_confs)) {
			$sub_confs_str = implode(',',$sub_confs);
			$query = "SELECT sub_name FROM #__lmsce_conf_subs WHERE id IN ($sub_confs_str)";
			$db->setQuery($query);
			$cciss = $db->LoadObjectList();
			foreach ($cciss as $ccis) {
				$subscriptions[]  = $ccis->sub_name;
			}

		}
		if (count($sub_courses)) {
			$sub_courses_str = implode(',',$sub_courses);
			$query = "SELECT sub_name FROM #__lms_subscriptions WHERE id IN ($sub_courses_str)";
			$db->setQuery($query);
			$ccis = $db->LoadObjectList();
			foreach ($ccis as $cci) {
				$subscriptions[]  = $cci->sub_name;
			}

		}
		$lists['subscriptions'] = $subscriptions;
	} else {
		$proceed_to_edit = false;
	}	
	
	if( is_object($payment) ) 
	{
		if( $payment->parent_id ) {
			$query = 'SELECT * FROM #__lms_payments WHERE id = '.$db->quote($payment->parent_id).' OR parent_id = '.$db->quote($payment->parent_id).' ORDER BY date DESC';
			$db->SetQuery( $query );
			$payments = $db->loadObjectList();	 
		} else {
			$query = 'SELECT * FROM #__lms_payments WHERE id = '.$db->quote($payment->id).' OR parent_id = '.$db->quote($payment->id).' ORDER BY date DESC';
			$db->SetQuery( $query );
			$payments = $db->loadObjectList();
		}
	}
		
	if ($proceed_to_edit){
		
		//fix timezone
		//$order->date = JLMS_fixDateStoredOnServerTimeZone($order->date, '2014-01-20 00:00:00');
		$order->date = JLMS_offsetDateToDisplay($order->date);
		
		$lists['payment_type'] = $payment->payment_type;
		$status[] = mosHTML::makeOption('cancelled', _JLMS_CANCELLED );
		$status[] = mosHTML::makeOption('pending', _JLMS_PENDING );
		$status[] = mosHTML::makeOption('completed', _JLMS_COMPLETED );
		$lists['status'] = mosHTML::selectList( $status, 'status', 'class="inputbox" size="1" ', 'value', 'text', strtolower($order->status) );		
		joomla_lms_adm_html::JLMS_editPaymentInfo_HTML( $payments, $order, $option, $lists, $courses_in);
	} else {
		mosRedirect("index.php?option=$option&task=payments", _JLMS_PAYS_MSG_WRONG_PAY_INFO);
	}
}

function JLMS_savePaymentInfo($option){
	$db = JFactory::getDBO();

	$id = mosGetParam($_POST, 'id', 0);
	$status = mosGetParam($_POST, 'status', '');
	$username = mosGetParam($_POST, 'username', '');
	$pdate = mosGetParam($_POST, 'p_date', gmdate('Y-m-d'));
	$pdate = JLMS_dateWithTimeZoneCMSConvertToUTC($pdate);
	$pdate = JLMS_dateToDB($pdate);

	$old_status = mosGetParam($_POST, 'old_status', '');
	$query = "UPDATE `#__lms_payments` SET status = ".$db->Quote($status).", date = ".$db->Quote($pdate)." WHERE id = $id ";
	$db->setQuery($query);
	$db->query();

	if (strtolower($old_status) == strtolower($status)) {
		mosRedirect("index.php?option=$option&task=payments");
	} else {

		$query = "SELECT payment_type, sub_id FROM `#__lms_payments` WHERE id = $id";
		$db->setQuery($query);
		$sub = $db->loadObject();
		if (is_object($sub)) {
			$sub_id = $sub->sub_id;
			$courses_in = array();
			if (!$sub->payment_type) {
				$query = "SELECT a.*, b.username, b.email, d.c_category FROM `#__lms_courses` as a "
				. "\n LEFT JOIN `#__lms_course_cats` as d ON d.id = a.cat_id,`#__lms_subscriptions_courses` as c "
				. "\n ,`#__users` as b "
				. "\n WHERE a.owner_id = b.id AND c.sub_id = $sub_id AND c.course_id = a.id ";
				$db->SetQuery( $query );
				$courses_in = $db->LoadObjectList();
			} elseif ($sub->payment_type == 1) {
				$query = "SELECT a.*, b.username, b.email, d.c_category FROM `#__lms_courses` as a "
				. "\n LEFT JOIN `#__lms_course_cats` as d ON d.id = a.cat_id,`#__lms_subscriptions_custom_courses` as c "
				. "\n ,`#__users` as b "
				. "\n WHERE a.owner_id = b.id AND c.sub_id = $sub_id AND c.course_id = a.id ";
				$db->SetQuery( $query );
				$courses_in = $db->LoadObjectList();
			} elseif ($sub->payment_type == 2) {
				$query = "SELECT distinct item_id FROM #__lms_payment_items WHERE payment_id = $id AND item_type = 0";
				$db->SetQuery( $query );
				$sub_c = JLMSDatabaseHelper::loadResultArray();
				if (count($sub_c)) {
					$sub_c_str = implode(',',$sub_c);
					$query = "SELECT distinct a.*, b.username, b.email, d.c_category FROM `#__lms_courses` as a "
					. "\n LEFT JOIN `#__lms_course_cats` as d ON d.id = a.cat_id,`#__lms_subscriptions_courses` as c "
					. "\n ,`#__users` as b "
					. "\n WHERE a.owner_id = b.id AND c.sub_id IN ($sub_c_str) AND c.course_id = a.id ";
					$db->SetQuery( $query );
					$courses_in = $db->LoadObjectList();
				}
			} else {
				mosRedirect("index.php?option=$option&task=payments");
			}
			
			if ( strtolower($old_status) == 'completed' || strtolower($status) == 'completed' ) {
				joomla_lms_adm_html::JLMS_editPaymentInfo_STEP2_HTML($status, $old_status, $courses_in, $id, $username, $option);			
			} else {			
				mosRedirect("index.php?option=$option&task=payments");
			}			
		} else {
			mosRedirect("index.php?option=$option&task=payments");
		}
	}
}

function JLMS_changePaymentInfo_FOR_user( $option, $cid, $id ){
    require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.scorm.clearcourseuserdata.php");
	$JLMS_CONFIG = & JLMSFactory::getConfig();
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$db = JFactory::getDBO();

	$query = "SELECT payment_type, sub_id, user_id, `date` FROM `#__lms_payments` WHERE id = $id ";
	$db->setQuery($query);
	$payment = $db->loadObject();
	$p_date  = $payment->date;
	$p_date = JLMS_offsetDateToDisplay($p_date);
	$p_date = JLMS_dateToDB($p_date);

	$status = mosGetParam($_REQUEST, 'old_status', '');
	
	//b Events JLMS Plugins		
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$_JLMS_PLUGINS->loadBotGroup('system');
	$args = array();
	$args[0] = $id;
	$args[1] = $status;
	$args[2] = 1;
	$_JLMS_PLUGINS->trigger('onChangeStatusPayment', $args);
	//e Events JLMS Plugins
	
	if ($payment->payment_type == 2) {
		//mosRedirect("index.php?option=$option&task=payments", 'Payment not found');
	}
	if (strtolower($status) == 'completed'){
		$cid = implode(',',$cid);
		$query = "DELETE FROM `#__lms_users_in_groups` WHERE user_id = ".$payment->user_id." AND course_id IN ($cid)";
		$db->setQuery($query);
		$db->query();

		mosRedirect("index.php?option=$option&task=payments", _JLMS_PAYS_MSG_PAY_INFO_CHANGED);
	} else {
		if (!$payment->payment_type) {
			$query = "SELECT * FROM `#__lms_subscriptions` WHERE id = '".$payment->sub_id."' ";
			$db->setQuery($query);
			$subscription = $db->loadObject();
		} elseif ($payment->payment_type == 1) {
			$query = "SELECT * FROM `#__lms_subscriptions_custom` WHERE id = '".$payment->sub_id."' ";
			$db->setQuery($query);
			$subscription = $db->loadObject();
			$subscription->account_type = 1;
		} elseif ($payment->payment_type == 2) {
			//mosRedirect("index.php?option=$option&task=payments", 'Payment not found');
		} else {
			mosRedirect("index.php?option=$option&task=payments", _JLMS_PAYS_MSG_WRONG_PAY_INFO);
		}
		if ($payment->payment_type == 2) {
			/* ----------- 21 July 2008 (DEN) - new payments processing ---------------- */

			$courses_to_enroll = array(); // here would be placed items, which we should proceed
			$conferences_to_enroll = array();
			$sub_courses = array();
			$sub_conferences = array();

			// get list of subscriptions to proceed
			$query = "SELECT * FROM #__lms_payment_items WHERE payment_id = $id";
			$db->setQuery($query);
			$payment_subs = $db->loadObjectList();
			$sub_courses = array();
			$sub_conferences = array();
			foreach ($payment_subs as $ps) {
				if ($ps->item_type == 0) {
					$new_sub = new stdClass();
					$new_sub->item_type = 0;
					$new_sub->id = $ps->item_id;
					$sub_courses[] = $new_sub;
				} elseif ($ps->item_type == 1) {
					$sub_conferences[] = $ps->item_id;
				}
			}

			// get list of courses/conferences to proceed
			foreach ($sub_courses as $sub_c) {
				if ($sub_c->item_type == 0) {
					$query = "SELECT * FROM `#__lms_subscriptions` WHERE id = '$sub_c->id'";
					$db->setQuery($query);
					$subscription = null;
					$subscription = $db->loadObject();
					if (is_object($subscription)) {
						$query = "SELECT course_id FROM `#__lms_subscriptions_courses` WHERE sub_id = '$sub_c->id'";
						$db->setQuery($query);
						$courses = $db->loadObjectList();
						$i = 0;
						while ($i < count($courses)) {
							if (in_array($courses[$i]->course_id, $cid)) { // if admin have chosen the course in the list on the previous page.
								$j = 0;$create_new = true;
								foreach ($courses_to_enroll as $ce) {
									if ($ce->course_id == $courses[$i]->course_id) {
										$create_new = false; break;
									}
									$j ++;
								}

								$new_course_item = new stdClass();
								$new_course_item->course_id = $courses[$i]->course_id;
								$new_course_item->publish_start = 1;
								$new_course_item->start_date = '';
								$new_course_item->publish_end = 1;
								$new_course_item->end_date = '';
								$new_course_item->access_days = 0;
								if ($subscription->account_type == 2) {
									$new_course_item->start_date = $subscription->start_date;
									$new_course_item->end_date = $subscription->end_date;
								} elseif ($subscription->account_type == 3) {
									$new_course_item->start_date = $subscription->start_date;
									$new_course_item->publish_end = 0;
								} elseif ($subscription->account_type == 4) {
									$new_course_item->access_days = $subscription->access_days;
								} else {
									$new_course_item->publish_start = 0;
									$new_course_item->publish_end = 0;
								}
			
								if ($create_new) {
									$courses_to_enroll[] = $new_course_item;
								} else {
									// else we should work with $courses_to_enroll[$j] (if the course is exists in the list of courses to subscribe to)
									if ($subscription->account_type == 4) {
										$courses_to_enroll[$j]->access_days = $courses_to_enroll[$j]->access_days + $subscription->access_days;
									} else {
										if ($courses_to_enroll[$j]->publish_start){
											$courses_to_enroll[$j]->publish_start = $new_course_item->publish_start;
											if ($courses_to_enroll[$j]->publish_start && ( ($courses_to_enroll[$j]->start_date && (strtotime($courses_to_enroll[$j]->start_date) > strtotime($new_course_item->start_date))) || !$courses_to_enroll[$j]->start_date ) ){
												$courses_to_enroll[$j]->start_date = $new_course_item->start_date;
											}
										}
										if ($courses_to_enroll[$j]->publish_end){
											$courses_to_enroll[$j]->publish_end = $new_course_item->publish_end;
											if ($courses_to_enroll[$j]->publish_end && ( ($courses_to_enroll[$j]->end_date && (strtotime($courses_to_enroll[$j]->end_date) < strtotime($new_course_item->end_date))) || !$courses_to_enroll[$j]->end_date ) ){
												$courses_to_enroll[$j]->end_date = $new_course_item->end_date;
											}	
										}
									}
								}
							}
							$i ++;
						}
					}
				}
			}
			
			// * * * * * enrollment is here
			foreach ($courses_to_enroll as $public_sub){
				
				//proeducate certificate
				if($JLMS_CONFIG->get('enabled_clear_course_user_data', false) && isset($public_sub->course_id) && $public_sub->course_id && isset($payment->user_id) && $payment->user_id){
					$CCUD = new JLMS_ClearCourseUserData();
					$CCUD->initializeClearCourseUserData($public_sub->course_id, $payment->user_id);
				}
				//proeducate certificate

				$query = "SELECT * FROM `#__lms_users_in_groups` WHERE course_id = ".$public_sub->course_id." AND user_id = ".$payment->user_id;
				$db->setQuery($query);
				$test = $db->loadObject();
				
				if (is_object($test)) {

					if ($test->publish_start){
						$test->publish_start = $public_sub->publish_start;
						if ($test->publish_start && (strtotime($test->start_date) > strtotime($public_sub->start_date) ) && $public_sub->start_date){
							$test->start_date = $public_sub->start_date;
						}
					}
					if ($test->publish_end){
						$test->publish_end = $public_sub->publish_end;
						if ($test->publish_end && (strtotime($test->end_date) < strtotime($public_sub->end_date) ) && $public_sub->end_date){
							$test->end_date = $public_sub->end_date;
						}	
					}

					if ($public_sub->access_days) {
						if ($test->publish_end) {
							if (strtotime($test->end_date) < strtotime(date('Y-m-d'))) {
								$test->end_date = date('Y-m-d',strtotime(date('Y-m-d')) + intval($public_sub->access_days)*24*60*60);
							} else {
								$test->end_date = date('Y-m-d',strtotime($test->end_date) + intval($public_sub->access_days)*24*60*60);
							}
						} elseif ($test->publish_start) {
							$test->start_date = date('Y-m-d',strtotime($test->start_date) - intval($public_sub->access_days)*24*60*60);
						}
					}

					$query = "UPDATE `#__lms_users_in_groups` SET publish_start = '".$test->publish_start."', start_date = '".$test->start_date."', publish_end = '".$test->publish_end."', end_date = '".$test->end_date."' WHERE course_id = ".$public_sub->course_id." AND user_id = ".$payment->user_id;
					$db->setQuery($query);
					$db->query();
				} else {
					$do_add = false;
					global $license_lms_users;
					if ($license_lms_users) {
						$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
						$db->SetQuery( $query );
						$total_students = $db->LoadResult();
						if (intval($total_students) < intval($license_lms_users)) {
							$do_add = true;
						}
						if (!$do_add) {
							$query = "SELECT count(*) FROM #__lms_users_in_groups WHERE user_id = '".$payment->user_id."'";
							$db->SetQuery( $query );
							if ($db->LoadResult()) {
								$do_add = true;
							}
						}
					} else {
						$do_add = true;
					}
					if ($do_add) {
						$course_info = new stdClass();
						$course_info->course_id = $public_sub->course_id;
						$course_info->future_course = 0;
						$query = "SELECT params, IF(publish_start = 1, IF(start_date <= '".date('Y-m-d')."', 0, 1), 0) as is_future_course, start_date FROM #__lms_courses WHERE id = ".$course_info->course_id;
						$db->setQuery($query);
						$course_params = '';
						$course_getinfo = $db->LoadObject();
						if (is_object($course_getinfo)) {
							$course_params = $course_getinfo->params;
							$course_info->future_course = $course_getinfo->is_future_course;
							$course_info->start_date = $course_getinfo->start_date;
						}
						$params = new MosParameters($course_params);
						$course_info->max_attendees = $params->get('max_attendees', 0);
						
						if ($course_info->future_course && $public_sub->access_days && (!$public_sub->publish_end || ($public_sub->publish_end && !$public_sub->end_date)) && (!$public_sub->publish_start || ($public_sub->publish_start && !$public_sub->start_date) )) {
							// just an X days access to the future course
							$public_sub->publish_start = 1;
							$public_sub->start_date = $course_info->start_date;
							$public_sub->publish_end = 1;
							$public_sub->end_date = date('Y-m-d', strtotime($public_sub->start_date) + intval($public_sub->access_days)*24*60*60);
							$public_sub->access_days = 0;
						} elseif ($public_sub->access_days) {
							if ($public_sub->publish_end) {
								if ($public_sub->end_date) {
									$public_sub->end_date = date('Y-m-d', strtotime($public_sub->end_date) + intval($public_sub->access_days)*24*60*60);
								} else {
									$public_sub->end_date = date('Y-m-d', strtotime($p_date) + intval($public_sub->access_days)*24*60*60);
									if ($public_sub->publish_start && !$public_sub->start_date) {
										$public_sub->start_date = date('Y-m-d', strtotime($p_date));
									}
								}
							} elseif ($public_sub->publish_start) {
								if ($public_sub->start_date) {
									$public_sub->start_date = date('Y-m-d',strtotime($public_sub->start_date) - intval($public_sub->access_days)*24*60*60);
								} else {
									$public_sub->start_date = date('Y-m-d', strtotime($p_date) - intval($public_sub->start_date)*24*60*60); // hmmmm...... script will never comes here :)
								}
							}
							if ($public_sub->publish_start && !$public_sub->start_date) {
								$public_sub->publish_start = 0;
							}
						}

						$user_info = new stdClass();
						$user_info->user_id = $payment->user_id;
						$user_info->group_id = 0;
						$user_info->course_id = $public_sub->course_id;

						$_JLMS_PLUGINS->loadBotGroup('course');
						$plugin_result_array = $_JLMS_PLUGINS->trigger('onCourseJoinAttempt', array($user_info, $course_info));

						$allow_join = true;
						if (is_array($plugin_result_array)) {
							foreach ($plugin_result_array as $result) {
								if (!$result) $allow_join = false;
							}
						}

						if ($allow_join) {
							$query = "INSERT INTO `#__lms_users_in_groups` (course_id, group_id, user_id, publish_start, start_date, publish_end, end_date, enrol_time ) VALUES ('".$public_sub->course_id."', '0', '".$payment->user_id."' ,'".$public_sub->publish_start."' ,'".$public_sub->start_date."','".$public_sub->publish_end."','".$public_sub->end_date."', '".gmdate('Y-m-d H:i:s')."') ";
							$db->setQuery($query);
							if($db->query()) 
							{
								$Itemid = $JLMS_CONFIG->get('Itemid');
								$e_course = new stdClass();
								$e_course->course_alias = '';
								$e_course->course_name = '';
	
								$query = "SELECT course_name, name_alias FROM #__lms_courses WHERE id = '".$public_sub->course_id."'";
								$db->setQuery( $query );
								$e_course = $db->loadObject();
	
								$query = "SELECT email, name, username FROM #__users WHERE id = '".$user_id."'";
								$db->setQuery( $query );
								$e_user = $db->loadObject();
	
								$e_params['user_id'] = $user_id;
								$e_params['course_id'] = $public_sub->course_id;
								$e_params['markers']['{email}'] = $e_user->email;
								$e_params['markers']['{name}'] = $e_user->name;
								$e_params['markers']['{username}'] = $e_user->username;
								$e_params['markers']['{coursename}'] = $e_course->course_name;
								$e_params['markers']['{courselink}'] = JLMSEmailRoute("index.php?option=com_joomla_lms&Itemid=$Itemid&task=details_course&id=".$public_sub->course_id);
								$e_params['markers']['{courselink}'] = '<a href="'.$e_params['markers']['{courselink}'].'">'.$e_params['markers']['{courselink}'].'</a>';
								$e_params['action_name'] = 'OnEnrolmentInCourseByJoomlaAdmin';
	
								$_JLMS_PLUGINS->loadBotGroup('emails');
								$plugin_result_array = $_JLMS_PLUGINS->trigger('OnEnrolmentInCourseByJoomlaAdmin', array (& $e_params));
							}

							$_JLMS_PLUGINS->loadBotGroup('user');
							$_JLMS_PLUGINS->trigger('onCourseJoin', array($user_info));
							//TODO: check the line above, rmove & befor db???
						} else {
							//user was placed to the course WL.
						}
					}
				}	
				$i++;
			}

			if (!empty($sub_conferences)) {
				$conf_items_str = implode(',',$sub_conferences);
				$query = "SELECT credits FROM #__lmsce_conf_user_credits WHERE user_id = ".$payment->user_id;
				$db->setQuery($query);
				$user_credits = intval($db->LoadResult());
				$query = "SELECT id, num_credits FROM #__lmsce_conf_subs WHERE id IN ($conf_items_str)";
				$db->setQuery($query);
				$credits_data = $db->LoadObjectList();
				$credits = 0;
				foreach ($sub_conferences as $subc) {
					foreach ($credits_data as $cd) {
						if ($cd->id == $subc) {
							$credits = $credits + $cd->num_credits; break;
						}
					}
				}
				$c_c = $user_credits + $credits;
				$query = "SELECT count(*) FROM #__lmsce_conf_user_credits WHERE user_id = ".$payment->user_id;
				$db->setQuery($query);
				if ($db->LoadResult()) {
					$query = "UPDATE #__lmsce_conf_user_credits SET credits = $c_c WHERE user_id = ".$payment->user_id;
					$db->setQuery($query);
					$db->query();
				} else {
					$query = "INSERT INTO #__lmsce_conf_user_credits (credits, user_id) VALUES ($c_c, ".$payment->user_id.")";
					$db->setQuery($query);
					$db->query();
				}
			}

			/* ----------- {END} 21 July 2008 (DEN) - new payments processing {END} ---------------- */
		} else {
			$public_sub = new stdClass();
			$public_sub->publish_start = 1;
			$public_sub->start_date = '';
			$public_sub->publish_end = 1;
			$public_sub->end_date = '';
			if ($subscription->account_type == 2) {
				$public_sub->start_date = $subscription->start_date;
				$public_sub->end_date = $subscription->end_date;
			} elseif ($subscription->account_type == 3) {
				$public_sub->start_date = $subscription->start_date;
				$public_sub->publish_end = 0;
			} elseif ($subscription->account_type == 4) {
				/*$public_sub->start_date = $subscription->start_date;
				$public_sub->end_date = $subscription->end_date;*/
	
				// 03 July 2007 (changed by DEN)
				$public_sub->start_date = date('Y-m-d');
				$public_sub->end_date = date('Y-m-d', time() + intval($subscription->access_days)*24*60*60);
	
			} else {
				$public_sub->publish_start = 0;
				$public_sub->publish_end = 0;
			}
	
			$values = '';$i = 1;
	
			foreach ($cid as $course){
				$query = "SELECT * FROM `#__lms_users_in_groups` WHERE course_id = ".$course." AND user_id = ".$payment->user_id." ";
				$db->setQuery($query);
				$test = $db->loadObject();
				if (is_object($test)){

					if ($test->publish_start){
						$test->publish_start = $public_sub->publish_start;
						if ($test->publish_start && (strtotime($test->start_date)> strtotime($public_sub->start_date) )){
							$test->start_date = $public_sub->start_date;
						}
					}
					if ($test->publish_end){
						$test->publish_end = $public_sub->publish_end;
						if ($test->publish_end && (strtotime($test->end_date) < strtotime($public_sub->end_date) )){
							$test->end_date = $public_sub->end_date;
						}
					}
	
					$query = "UPDATE `#__lms_users_in_groups` SET  publish_start = '".$test->publish_start."', start_date = '".$test->start_date."', publish_end = '".$test->publish_end."', end_date = '".$test->end_date."' WHERE course_id = ".$course->course_id." AND user_id = ".$user_id." ";
					$db->setQuery($query);
					$db->query();
				}
				else{
					$do_add = false;
					$query = "SELECT lms_config_value FROM #__lms_config WHERE lms_config_var = 'license_lms_users'";
					$db->SetQuery( $query );
					$license_lms_users = $db->LoadResult();
					if ($license_lms_users) {
						$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
						$db->SetQuery( $query );
						$total_students = $db->LoadResult();
						if (intval($total_students) < intval($license_lms_users)) {
							$do_add = true;
						}
						if (!$do_add) {
							$query = "SELECT count(*) FROM #__lms_users_in_groups WHERE user_id = '".$user_id."'";
							$db->SetQuery( $query );
							if ($db->LoadResult()) {
								$do_add = true;
							}
						}
					} else {
						$do_add = true;
					}
					if ($do_add) {
						$query = "INSERT INTO `#__lms_users_in_groups` (course_id, group_id, user_id, publish_start, start_date, publish_end, end_date ) VALUES ('".$course."', '0', '".$payment->user_id."' ,'".$public_sub->publish_start."' ,'".$public_sub->start_date."','".$public_sub->publish_end."','".$public_sub->end_date."') ";
						$db->setQuery($query);
						$db->query();
	
						$user_info = new stdClass();
						$user_info->user_id = $payment->user_id;
						$user_info->group_id = 0;
						$user_info->course_id = $course;
						$_JLMS_PLUGINS->loadBotGroup('user');
						$_JLMS_PLUGINS->trigger('onCourseJoin', array($user_info));
					}
				}
				$i++;
			}
		}
		mosRedirect("index.php?option=$option&task=payments", _JLMS_PAYS_MSG_PAY_DET_CHANGED);
	}
}

function JLMS_getPaymentInvoice($paym_id, $option) {
	$db = JFactory::getDBO();
	$paym_id = intval($paym_id);
	$invoice_file = '';
	if ($paym_id) {
		$query = "SELECT b.filename FROM #__lms_payments as a, #__lms_subs_invoice as b WHERE a.id = $paym_id AND a.id = b.subid AND a.payment_type = 2";
		$db->setQuery( $query );
		$invoice_file = $db->LoadResult();
	}
	if ($invoice_file) {
		$query = "SELECT lms_config_value FROM #__lms_config  WHERE lms_config_var = 'jlms_subscr_invoice_path'";
		$db->setQuery( $query );
		$invoices_path = $db->loadResult();
		if ($invoices_path) {
			$invoice_file = $invoices_path . '/' . $invoice_file;
		}
	}
	if ($invoice_file && file_exists($invoice_file) && is_readable( $invoice_file ) ) {
		$file_name = 'invoice_'.(str_pad($paym_id, 5, '0', STR_PAD_LEFT)).'.pdf';

		@set_time_limit(0);

		$v_date = date("Y-m-d H:i:s");
		if (preg_match('Opera(/| )([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT'])) {
			$UserBrowser = "Opera";
		}
		elseif (preg_match('MSIE ([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT'])) {
			$UserBrowser = "IE";
		} else {
			$UserBrowser = '';
		}
		$mime_type = ($UserBrowser == 'IE' || $UserBrowser == 'Opera') ? 'application/octetstream' : 'application/octet-stream';
		header('Content-Type: ' . $mime_type );
		header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		if ($UserBrowser == 'IE') {
			header('Content-Disposition: '.(isset($headers['Content-Disposition']) ? $headers['Content-Disposition'] : 'attachment').'; filename="' . $file_name . '";');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Content-Length: '. filesize($invoice_file)); 
			header('Pragma: public');
		} else {
			header('Content-Disposition: '.(isset($headers['Content-Disposition']) ? $headers['Content-Disposition'] : 'attachment').'; filename="' . $file_name . '";');
			header('Content-Length: '. filesize($invoice_file)); 
			header('Pragma: no-cache');
		}
		@ob_end_clean();
		readfile( $invoice_file );
		exit();
	} else {
		//TODO: error message styles
		echo '<center>';
		echo _JLMS_PAYS_INVOICE_NOT_FOUND;
		echo '</center>';
	}
}

function JLMS_GenerateNewInvoice($paym_id, $option) {
	$db = JFactory::getDBO();
	$paym_id = intval($paym_id);
	$old_invoice_file = '';
	$invoices_path = '';
	if ($paym_id) {
		$query = "SELECT b.filename FROM #__lms_payments as a, #__lms_subs_invoice as b WHERE a.id = $paym_id AND a.id = b.subid AND a.payment_type = 2";
		$db->setQuery( $query );
		$old_invoice_file = $db->LoadResult();
	}
	$query = "SELECT lms_config_value FROM #__lms_config  WHERE lms_config_var = 'jlms_subscr_invoice_path'";
	$db->setQuery( $query );
	$invoices_path = $db->loadResult();
	if ($invoices_path) {
		$old_invoice_file = $invoices_path . '/' . $old_invoice_file;
	}
	if ($old_invoice_file && file_exists($old_invoice_file) && is_readable( $old_invoice_file ) ) {
		@unlink($old_invoice_file);
	}

	if (class_exists('JParameter')) {
		require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.params.new.php");
	} else {
		require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.params.php");
	}
	require_once (_JOOMLMS_FRONT_HOME . DS . "includes" . DS ."joomla_lms.subscription.lib.php");

	$fake_params = null;
	JLMS_CART_generateinvoice($paym_id, $fake_params, true, false);

	mosRedirect("index.php?option=$option&task=editA_payment&id=$paym_id");
}

function JLMS_removeFromPayments( $option ) {
	$db = JFactory::getDBO();
	$cid 	= mosGetParam( $_REQUEST, 'cid', array(0) );
	$group_id = intval( mosGetParam( $_REQUEST, 'group_id', 0 ) );
	$cid = implode(',',$cid);
	$query = "DELETE FROM #__lms_payments WHERE id IN ($cid) ";
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
	}

	mosRedirect("index.php?option=$option&task=payments", _JLMS_PAYS_MSG_PAY_DEL_SUCCES);
}

function JLMS_showProcessorsList( $option ) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	// install new processors found and delete with the files missed
	$msg = '';
	$handle = opendir(JPATH_SITE.'/components/com_joomla_lms/includes/processors');
	while (false !== ($file = readdir($handle))) {
		if ($file != "." && $file != ".." && preg_match('/\.xml$/',$file)) {
			$files[]=$file;
		};
	};
	closedir($handle);

	for ($i=0; $i<count($files); $i++) {
		// check if new
		$query = "SELECT COUNT(*) FROM #__lms_subscriptions_procs WHERE filename='".str_replace('.xml','',$files[$i])."'";
		$db->setQuery( $query );
		$total = $db->loadResult();
		if ($total == 0 && file_exists(JPATH_SITE.'/components/com_joomla_lms/includes/processors/'.str_replace('.xml','.php',$files[$i]))) {
			$xml = implode('', file(JPATH_SITE.'/components/com_joomla_lms/includes/processors/'.$files[$i]));
			preg_match_all("/name>.*name>/", $xml, $dop);
			$proc_name = str_replace('</','',str_replace("name>",'',$dop[0][0]));
			$query = "INSERT INTO #__lms_subscriptions_procs ( id , name , filename , params ) VALUES ('' , '".$proc_name."', '".str_replace('.xml','',$files[$i])."', '')";
			$db->setQuery( $query );
			$db->query();
			$msg.=$proc_name._JLMS_PROCS_MSG_INSTALLED.'<br/>';
		}
	}
	$query = "SELECT * FROM #__lms_subscriptions_procs ";
	$db->setQuery( $query );
	$row2 = $db->loadObjectList();
	for ($i=0; $i<count($row2); $i++) {
		if (!file_exists(JPATH_SITE.'/components/com_joomla_lms/includes/processors/'.$row2[$i]->filename.'.xml') || !file_exists(JPATH_SITE.'/components/com_joomla_lms/includes/processors/'.$row2[$i]->filename.'.php')) {
			$query = "DELETE FROM #__lms_subscriptions_procs WHERE id='".$row2[$i]->id."'";
			$db->setQuery( $query );
			$db->query();
			@unlink(JPATH_SITE.'/components/com_joomla_lms/includes/processors/'.$row2[$i]->filename.'.xml');
			@unlink(JPATH_SITE.'/components/com_joomla_lms/includes/processors/'.$row2[$i]->filename.'.php');
			$msg.=_JLMS_PROCS_MSG_FILES_FOR.$row2[$i]->name._JLMS_PROCS_MSG_MISS.'<br />';
		}
	}

	// show available processors

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );

	// get the total number of records
	$query = "SELECT COUNT(*)"
	. "\n FROM #__lms_subscriptions_procs AS cd"
	;
	$db->setQuery( $query );
	$total = $db->loadResult();

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	// get the subset (based on limits) of required records
	$query = "SELECT * "
	. "\n FROM #__lms_subscriptions_procs AS cd"
	. "\n ORDER BY cd.ordering "
	. "\n LIMIT $pageNav->limitstart, $pageNav->limit"
	;
	$db->setQuery( $query );
	$rows = $db->loadObjectList();
	
	for ($i=0; $i<count($rows); $i++) 
	{	
		
		$rows[$i]->is_recurrent =false;
		$rows[$i]->is_single =false;
								
		require_once( JPATH_SITE.'/components/com_joomla_lms/includes/processors/'.$rows[$i]->filename.'.php');
		if( method_exists( 'JLMS_'.$rows[$i]->filename, 'validate_recurrent_subscription' ) ) 
		{
			$rows[$i]->is_recurrent =true;
		}
		
		if( method_exists( 'JLMS_'.$rows[$i]->filename, 'validate_callback' ) || class_exists('authorizenet_class') ) 
		{
			$rows[$i]->is_single =true;
		} 
	}	

	joomla_lms_adm_html::showProcessorsList( $rows, $pageNav, $option, $msg);
}

function cancelProcessor( $id ) {
	mosRedirect( "index.php?option=com_joomla_lms&task=processorslist" );
}

function cancelSubscription( $id ) {
	mosRedirect( "index.php?option=com_joomla_lms&task=subscriptions" );
}

function JLMS_cancelAssign ($id ) {
	mosRedirect( "index.php?option=com_joomla_lms&task=subscriptions" );
}


function changeProcessor( $option, $id ) {
	$db = JFactory::getDBO();

	$query = "UPDATE #__lms_subscriptions_procs SET default_p=0";
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}
	$query = "UPDATE #__lms_subscriptions_procs SET default_p=1  WHERE id=".$id;
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}
	mosRedirect( "index.php?option=$option&task=processorslist" );
}

function changeProc( $cid=null, $state=0, $option ) {
	$db = JFactory::getDBO();

	if (!is_array( $cid ) || count( $cid ) < 1) {
		$action = $publish ? 'publish' : 'unpublish';
		echo "<script> alert('"._JLMS_PROCS_MSG_SELECT_ITEM."'); window.history.go(-1);</script>\n";
		exit();
	}

	$cids = implode( ',', $cid );

	$query = "UPDATE #__lms_subscriptions_procs SET published = " . intval( $state ). " WHERE id IN ( $cids )";
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}
	mosRedirect( "index.php?option=$option&task=processorslist" );
}

function editProcessor( $id, $option ) {
	$db = JFactory::getDBO();
	
	$row = new mosLMSsubscriptionprocessor( $db );	
	$row->load( $id );
	
	$lists = array();
	$params = array();
		
	JlmsViewProcessor::editPage( $row, $lists, $params, $option );
}

function saveProcessor( $option, $task ) {
	$db = JFactory::getDBO();
	$params = mosGetParam( $_POST, 'params', '' );

	if (is_array( $params )) {
		$txt = array();
		foreach ($params as $k=>$v) {
			$txt[] = "$k=$v";
		}
		$_POST['params'] = mosParameters::textareaHandling( $txt );
	}
	$row = new mosLMSsubscriptionprocessor( $db );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}	
	
	// pre-save checks
	if (!$row->check()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}	

	// save the changes
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if ($task == 'save_p'){
		mosRedirect( "index.php?option=$option&task=processorslist", _JLMS_PROCS_MSG_DETS_CHANGED );
	}else{
		mosRedirect( "index.php?option=$option&hidemainmenu=1&task=editA_p&id=".$row->id , _JLMS_PROCS_MSG_DETS_CHANGED);
	}
}

##########################################################################
###	--- ---   JLMS CONFIG	 	--- --- ###
##########################################################################
function JLMS_menuManage( $option ){
	$db = JFactory::getDBO();
	$menutype = intval(mosGetParam($_REQUEST, 'menutype', 0));

	$query = "SELECT * FROM `#__lms_menu` WHERE user_access = ".$menutype." ORDER  by ordering";
	$db->setQuery($query);
	$menu = $db->loadObjectList();
	$limitstart = 0;
	$limit = 30;
	$options = $lists = array();

	//$options[] = mosHTML::makeOption( 0, '-- Select menu type --' );
	$options[] = mosHTML::makeOption( -1, _JLMS_MENUM_GUEST_MENU );
	$options[] = mosHTML::makeOption( 0, _JLMS_MENUM_HOMEP_MENU );
	$options[] = mosHTML::makeOption( 1, _JLMS_MENUM_TEACHER_MENU );
	$options[] = mosHTML::makeOption( 2, _JLMS_MENUM_STUDENT_MENU );
	$options[] = mosHTML::makeOption( 6, _JLMS_MENUM_PARENT_MENU );
	//$lists['menutype'] = mosHTML::selectList($options, 'menutype', '' , 'value', 'text', $menutype);
	$lists['menutype'] = mosHTML::selectList( $options, 'menutype', 'class="text_area" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $menutype );

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( count($menu), $limitstart, $limit  );

	$JLMS_config = new stdClass();
	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'tracking_enable'";
	$db->setQuery($query);
	$JLMS_config->tracking_enable = $db->loadResult();

	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'plugin_quiz'";
	$db->setQuery($query);
	$JLMS_config->plugin_quiz = $db->loadResult();

	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'plugin_forum'";
	$db->setQuery($query);
	$JLMS_config->plugin_forum = $db->loadResult();

	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'conference_enable'";
	$db->setQuery($query);
	$JLMS_config->conference_enable = $db->loadResult();

	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'chat_enable'";
	$db->setQuery($query);
	$JLMS_config->chat_enable = $db->loadResult();
	
	$GLOBALS['jlms_toolbar_menutype'] = $menutype;
	joomla_lms_adm_html::JLMS_showMenuManage( $menu, $option, $pageNav, $menutype, $lists, $JLMS_config);
}

function JLMS_orderMenu( $uid, $inc, $option ) {
	$db = JFactory::getDBO();
	$menutype 	= mosGetParam($_REQUEST, 'menutype', 2);

	$row = new mos_Joomla_LMS_menuManage( $db );
	$row->load( $uid );
	$row->move( $inc, ' user_access = '.$menutype );

	mosRedirect( 'index.php?option='. $option .'&task=menu_manage&menutype='. $menutype );
}


function JLMS_saveOrder( &$cid ) {
	$db = JFactory::getDBO();
	
	$menutype 	= mosGetParam($_REQUEST, 'menutype', 1);
	$row		= new mos_Joomla_LMS_menuManage( $db );
	
	$total		= count( $cid );
	$order 		= josGetArrayInts( 'order' );
	$conditions = array();

	// update ordering values
	for( $i=0; $i < $total; $i++ ) {
		$row->load( (int) $cid[$i] );
		if ($row->ordering != $order[$i]) {
			$row->ordering = $order[$i];
			if (!$row->store()) {
				echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
				exit();
			}
			// remember to updateOrder this group
			$condition = "user_access = " . $db->Quote( $row->user_access );
			$found = false;
			foreach ( $conditions as $cond )
				if ($cond[1]==$condition) {
					$found = true;
					break;
				} // if
			if (!$found) $conditions[] = array($row->id, $condition);
		}
	}
	foreach ( $conditions as $cond ) {
		$row->load( $cond[0] );
		$row->updateOrder( $cond[1] );
	}
	$msg 	= _JLMS_MENUM_MSG_NEW_ORDER_SAVED;
	mosRedirect( 'index.php?option=com_joomla_lms&task=menu_manage&menutype='. $menutype, $msg );
}


function JLMS_cb_integration( $option ) {
	global $JLMS_CONFIG;
	$db = JFactory::getDBO();

	$row = new jlms_adm_config();
	$row->bind($_POST);
	$row->loadFromDb( $db );
	$lists = array();
	$lists['is_cb_installed'] 	= mosHTML::yesnoRadioList( 'is_cb_installed', 'class="inputbox"',	$row->is_cb_installed );

	// juser
	if ($JLMS_CONFIG->get('juser_integration', 0)) 
	{
		$targets_juser = BuildFieldsList_JUser();
		
		$lists['jlms_juser_address'] = mosHTML::selectList( $targets_juser, 'jlms_juser_address', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_juser_address);
		$lists['jlms_juser_city'] = mosHTML::selectList( $targets_juser, 'jlms_juser_city', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_juser_city );
		$lists['jlms_juser_state'] = mosHTML::selectList( $targets_juser, 'jlms_juser_state', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_juser_state );
		$lists['jlms_juser_postal_code'] = mosHTML::selectList( $targets_juser, 'jlms_juser_postal_code', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_juser_postal_code);
		$lists['jlms_juser_country'] = mosHTML::selectList( $targets_juser, 'jlms_juser_country', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_juser_country );
		$lists['jlms_juser_phone'] = mosHTML::selectList( $targets_juser, 'jlms_juser_phone', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_juser_phone );
		
		$lists['jlms_juser_location'] = mosHTML::selectList( $targets_juser, 'jlms_juser_location', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_juser_location );
		$lists['jlms_juser_website'] = mosHTML::selectList( $targets_juser, 'jlms_juser_website', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_juser_website );
		$lists['jlms_juser_icq'] = mosHTML::selectList( $targets_juser, 'jlms_juser_icq', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_juser_icq );
		$lists['jlms_juser_aim'] = mosHTML::selectList( $targets_juser, 'jlms_juser_aim', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_juser_aim );
		$lists['jlms_juser_yim'] = mosHTML::selectList( $targets_juser, 'jlms_juser_yim', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_juser_yim );
		$lists['jlms_juser_msn'] = mosHTML::selectList( $targets_juser, 'jlms_juser_msn', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_juser_msn );
		$lists['jlms_juser_company'] = mosHTML::selectList( $targets_juser, 'jlms_juser_company', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_juser_company );
	}
	
	// cb fields

	$targets = array();
	if (file_exists(JPATH_BASE.DS.'components'.DS.'com_comprofiler'.DS.'comprofiler.php')) $targets = BuildFieldsList();
	$lists['jlms_cb_address'] = mosHTML::selectList( $targets, 'jlms_cb_address', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_cb_address);
	$lists['jlms_cb_city'] = mosHTML::selectList( $targets, 'jlms_cb_city', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_cb_city );
	$lists['jlms_cb_state'] = mosHTML::selectList( $targets, 'jlms_cb_state', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_cb_state );
	$lists['jlms_cb_postal_code'] = mosHTML::selectList( $targets, 'jlms_cb_postal_code', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_cb_postal_code);
	$lists['jlms_cb_country'] = mosHTML::selectList( $targets, 'jlms_cb_country', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_cb_country );
	$lists['jlms_cb_phone'] = mosHTML::selectList( $targets, 'jlms_cb_phone', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_cb_phone );

	$lists['jlms_cb_location'] = mosHTML::selectList( $targets, 'jlms_cb_location', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_cb_location );
	$lists['jlms_cb_website'] = mosHTML::selectList( $targets, 'jlms_cb_website', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_cb_website );
	$lists['jlms_cb_icq'] = mosHTML::selectList( $targets, 'jlms_cb_icq', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_cb_icq );
	$lists['jlms_cb_aim'] = mosHTML::selectList( $targets, 'jlms_cb_aim', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_cb_aim );
	$lists['jlms_cb_yim'] = mosHTML::selectList( $targets, 'jlms_cb_yim', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_cb_yim );
	$lists['jlms_cb_msn'] = mosHTML::selectList( $targets, 'jlms_cb_msn', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_cb_msn );
	$lists['jlms_cb_company'] = mosHTML::selectList( $targets, 'jlms_cb_company', 'class="inputbox" size="1"', 'value', 'text', $row->jlms_cb_company );
	
	//Mod EOGN-1203
	$sgb_cb_columns = $JLMS_CONFIG->get('show_gradebook_cb_columns', false);
	if($sgb_cb_columns){
		//$query = "SELECT * FROM #__lms_cb_gb WHERE 1";
		//$db->setQuery($query);
		//$data = $db->loadObjectList();
		//$gb = array();
		//foreach($data as $d){
		//	$gb[$d->var] = $d->value;
		//}
		
		$lists['gradebook'] = array();
		$lists['gradebook']['cb_address'] = mosHTML::yesnoRadioList('gb_cb_address', 'class="text_area"', (isset($row->gb_cb_address) ? $row->gb_cb_address : 0));
		$lists['gradebook']['cb_country'] = mosHTML::yesnoRadioList('gb_cb_country', 'class="text_area"', (isset($row->gb_cb_country) ? $row->gb_cb_country : 0));
		$lists['gradebook']['cb_city'] = mosHTML::yesnoRadioList('gb_cb_city', 'class="text_area"', (isset($row->gb_cb_city) ? $row->gb_cb_city : 0));
		$lists['gradebook']['cb_state'] = mosHTML::yesnoRadioList('gb_cb_state', 'class="text_area"', (isset($row->gb_cb_state) ? $row->gb_cb_state : 0));
		$lists['gradebook']['cb_postal_code'] = mosHTML::yesnoRadioList('gb_cb_postal_code', 'class="text_area"', (isset($row->gb_cb_postal_code) ? $row->gb_cb_postal_code : 0));
		$lists['gradebook']['cb_country'] = mosHTML::yesnoRadioList('gb_cb_country', 'class="text_area"', (isset($row->gb_cb_country) ? $row->gb_cb_country : 0));
		$lists['gradebook']['cb_phone'] = mosHTML::yesnoRadioList('gb_cb_phone', 'class="text_area"', (isset($row->gb_cb_phone) ? $row->gb_cb_phone : 0));
		$lists['gradebook']['cb_location'] = mosHTML::yesnoRadioList('gb_cb_location', 'class="text_area"', (isset($row->gb_cb_location) ? $row->gb_cb_location : 0));
		$lists['gradebook']['cb_website'] = mosHTML::yesnoRadioList('gb_cb_website', 'class="text_area"', (isset($row->gb_cb_website) ? $row->gb_cb_website : 0));
		$lists['gradebook']['cb_icq'] = mosHTML::yesnoRadioList('gb_cb_icq', 'class="text_area"', (isset($row->gb_cb_icq) ? $row->gb_cb_icq : 0));
		$lists['gradebook']['cb_aim'] = mosHTML::yesnoRadioList('gb_cb_aim', 'class="text_area"', (isset($row->gb_cb_aim) ? $row->gb_cb_aim : 0));
		$lists['gradebook']['cb_yim'] = mosHTML::yesnoRadioList('gb_cb_yim', 'class="text_area"', (isset($row->gb_cb_yim) ? $row->gb_cb_yim : 0));
		$lists['gradebook']['cb_msn'] = mosHTML::yesnoRadioList('gb_cb_msn', 'class="text_area"', (isset($row->gb_cb_msn) ? $row->gb_cb_msn : 0));
		$lists['gradebook']['cb_company'] = mosHTML::yesnoRadioList('gb_cb_company', 'class="text_area"', (isset($row->gb_cb_company) ? $row->gb_cb_company : 0));
	}
	//Mod EOGN-1203
	
	//radio
	$lists['assoc'] = array('{lms_cb_address}','{lms_cb_city}','{lms_cb_state}','{lms_cb_pcode}','{lms_cb_country}','{lms_cb_phone}','{lms_cb_location}','{lms_cb_website}','{lms_cb_icq}','{lms_cb_aim}','{lms_cb_yim}','{lms_cb_msn}', '{lms_cb_company}');
	$lists['new_fields'] = array();
	$query = "SELECT * FROM #__lms_cb_assoc ORDER BY id ";
	$db->setQuery($query);
	$new_rows = $db->LoadObjectList();
	for($i=0;$i<count($new_rows);$i++)
	{
		$lists['new_fields'][$i]['cb_id'] = $new_rows[$i]->id;
		$lists['new_fields'][$i]['cb_name'] = $new_rows[$i]->field_name;
		$lists['new_fields'][$i]['cb_field'] = mosHTML::selectList( $targets, 'jlms_cb_new_'.$new_rows[$i]->id, 'class="inputbox" size="1"', 'value', 'text', $new_rows[$i]->cb_field_id );
		$lists['new_fields'][$i]['cb_assoc'] = stripslashes( $new_rows[$i]->cb_assoc);
		if($sgb_cb_columns){
			$lists['new_fields'][$i]['gradebook'] = mosHTML::yesnoRadioList('gb_cb_new_'.$new_rows[$i]->id, 'class="text_area"', $new_rows[$i]->gb);
		}
	}
	
	joomla_lms_adm_html::JLMS_CB_integration( $row, $lists, $option);
}
function JLMS_cb_integration_edit( $id, $option ) {
	$db = JFactory::getDBO();
	$GLOBALS['jlms_toolbar_id'] = $id;
	$row = new stdClass();
	$row->cb_field_id = 0;
	$row->id = 0;
	$row->field_name = '';
	$row->cb_assoc = '';
	if($id)
	{
		$isset_table = true;
		//check exists table
		if (JLMS_J30version())
		{
			$app = JFactory::getApplication();
			$prefix = $app->getCfg('dbprefix');
			
			$query = "SHOW TABLES LIKE '".$prefix."lms_cb_assoc'";
			$db->setQuery($query);
			$isset_table = $db->loadResult();
		}
		if ($isset_table)
		{
			$query = "SELECT * FROM #__lms_cb_assoc WHERE id='".$id."'";
			$db->setQuery($query);
			$rows = $db->loadObjectList();
			$row = $rows[0];
		}
	}

	$targets = BuildFieldsList();
	$lists = array();
	$lists['cb_field'] = mosHTML::selectList( $targets, 'cb_field_id', 'class="inputbox" size="1"', 'value', 'text', $row->cb_field_id );
	joomla_lms_adm_html::JLMS_CB_integration_edit( $row, $lists, $option);
}

function BuildFieldsList() {
	$db = JFactory::getDBO();
	$targets = array();
	$targets[] = mosHTML::makeOption( 0, 'disabled' );
	
	$dops = null;
	$isset_table = true;
		//check exists table
		if (JLMS_J30version())
		{
			$app = JFactory::getApplication();
			$prefix = $app->getCfg('dbprefix');
			
			$query = "SHOW TABLES LIKE '".$prefix."comprofiler_fields'";
			$db->setQuery($query);
			$isset_table = $db->loadResult();
		}
		if ($isset_table)
		{
			$query = "SELECT * FROM #__comprofiler_fields WHERE sys != '1' ORDER BY name";
			$db->setQuery($query);
			$dops = $db->loadObjectList();	
		}
		
	if (count($dops)) {
		foreach ($dops as $dop) {
			$targets[] = mosHTML::makeOption( $dop->fieldid, $dop->name );
		}
	}
	return $targets;
}
function BuildFieldsList_JUser() {
	$db = JFactory::getDBO();
	$targets = array();
	$targets[] = mosHTML::makeOption( 0, 'disabled' );
	if (file_exists(dirname(__FILE__)."/../../../components/com_juser/juser.php")) {
		$query = "SELECT * FROM #__extending_field_list WHERE published = 1 ORDER BY title";
		$db->setQuery($query);
		$dops = $db->loadObjectList();
		if (count($dops)) {
			foreach ($dops as $dop) {
				$targets[] = mosHTML::makeOption( $dop->id, $dop->title );
			}
		}
	}
	return $targets;
}
function JLMS_cb_integration_save( $option ) {
	global $JLMS_CONFIG;
	
	$db = JFactory::getDBO();
	
	$sgb_cb_columns = $JLMS_CONFIG->get('show_gradebook_cb_columns', false);
	
	
	
	if(!isset($_POST['cb_id']))
	{
		$row = new jlms_adm_config();
		$row->loadFromDb( $db );
		$row->attendance_days = unserialize($row->attendance_days);
		$row->bind($_POST);
		$row->saveToDb( $db );
		
		//Mod EOGN-1203
		/*
		if($sgb_cb_columns){
			
			//$query = "CREATE TABLE IF NOT EXISTS `#__lms_cb_gb` (`var` varchar(250) NOT NULL, `value` int(11) NOT NULL) DEFAULT CHARSET=utf8;";
			//$db->setQuery($query);
			//$db->query();
			//$query = "ALTER TABLE `#__lms_cb_assoc` ADD `gb` INT NOT NULL;";
			//$db->setQuery($query);
			//$db->query();
			
			$query = "DELETE FROM #__lms_cb_gb WHERE 1";
			$db->setQuery($query);
			$db->query();
			
			if(isset($_POST['gb']) && count($_POST['gb'])){
				$gb = $_POST['gb'];
				foreach($gb as $var=>$value){
					$query = "INSERT INTO #__lms_cb_gb (var, value) VALUES ('".$var."', '".$value."')";
					$db->setQuery($query);
					$db->query();
				}
			}
		}
		*/
		//Mod EOGN-1203
		
		$query = "SELECT * FROM #__lms_cb_assoc ";
		$db->setQuery($query);
		$new_rows = $db->LoadObjectList();
		for($i=0;$i<count($new_rows);$i++)
		{
			if (isset($_POST['jlms_cb_new_'.$new_rows[$i]->id]))
			{
				$query = "UPDATE #__lms_cb_assoc SET cb_field_id = '".$_POST['jlms_cb_new_'.$new_rows[$i]->id]."'"
				. ($sgb_cb_columns ? "\n, gb = '".$_POST['gb_cb_new_'.$new_rows[$i]->id]."'" : '')
				. "\n WHERE id='".$new_rows[$i]->id."'";
				$db->setQuery($query);
				if (!$db->query()) {
					echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
				}
			}
		}
	}
	else
	{		
		$cb_id = JRequest::getInt('cb_id', 0);		
		$cb_assoc = JRequest::getString('cb_assoc');
		$cb_assoc = str_replace('{','',$cb_assoc);
		$cb_assoc = str_replace('}','',$cb_assoc);
		/*$cb_assoc = '{'.$cb_assoc.'}';*/
		if($cb_id)
		{
			$query = "UPDATE #__lms_cb_assoc SET field_name = '".$db->quote(JRequest::getString('field_name'))."', cb_field_id = '".$db->quote(JRequest::getInt('cb_field_id', 0))."', cb_assoc = '".$db->quote($cb_assoc)."'"
			. ($sgb_cb_columns ? "\n, gb = '".$db->quote(JRequest::getString('gb_cb_new'))."'" : '')
			. "\n WHERE id='".$cb_id."'";
			$db->setQuery($query);
			if (!$db->query()) {
				echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
			}
		}
		else
		{			
			$query = "SELECT  count(id) FROM #__lms_cb_assoc WHERE cb_assoc = ".$db->quote($cb_assoc);
			$db->setQuery($query);
			$index =  $db->loadResult();
			
			$cb_assoc = ($index)?$cb_assoc.$index:$cb_assoc;
			
			$query = "INSERT INTO #__lms_cb_assoc"
			. "\n (id, field_name, cb_field_id, cb_assoc"
			. ($sgb_cb_columns ? "\n, gb" : '')
			. "\n )"
			. "\n VALUES"
			. "\n ('', ".$db->quote(JRequest::getString('field_name')).", ".$db->quote(JRequest::getInt('cb_field_id', 0)).", ".$db->quote($cb_assoc)
			. ($sgb_cb_columns ? "\n, ".$db->quote(JRequest::getString('gb_cb_new')) : '')
			. "\n)"
			;
			$db->setQuery($query);
			if (!$db->query()) {
				echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
			}
		}
	}
	mosRedirect ("index.php?option=com_joomla_lms&task=cb_integration");
}
function JLMS_cb_integration_delete($cids, $option) {
	$db = JFactory::getDBO();
	if(count($cids))
	{
		$ids = implode(',',$cids);
		$query = "DELETE FROM #__lms_cb_assoc WHERE id IN (".$ids.")";
		$db->setQuery($query);
		if (!$db->query()) {
			echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}
	mosRedirect ("index.php?option=com_joomla_lms&task=cb_integration");
}
function offMsg_yesnoRadioList( $tag_name, $tag_attribs, $selected, $yes="Yes", $no="No" ) {
		
	$arr = array(
		mosHTML::makeOption( '1', _JLMS_NO ),
		mosHTML::makeOption( '0', _JLMS_YES )
	);
	$radiolist = mosHTML::radioList( $arr, $tag_name, $tag_attribs, $selected );
	if (JLMS_J30version())
		{ 
			$radiolist = '<fieldset class="radio btn-group">'.$radiolist.'</fieldset>';
		}
	return $radiolist;
}

function JLMS_config( $option ) {
	$db = JFactory::getDBO();
	$acl  = & JLMSFactory::getJoomlaACL();
        
    JLMSLang::isExtFontExistsChecking(_JLMS_NOTS_EXT_FONT_DOESNT_EXIST_CFG, false);
    JLMSLang::isBaseFontExistsChecking();

	$row = new jlms_adm_config();
	$row->bind($_POST);
	$row->loadFromDb( $db );

	$lists['conference_enable']	= mosHTML::yesnoRadioList( 'conference_enable', 'class="inputbox"',	$row->conference_enable );

	// 24.08.2007 tab warnings
	$lists['conference_warning'] = (!$row->conference_enable && $row->flascommRoot)?true:false;
	$lists['confpath_warning'] = ($row->conference_enable && !$row->flascommRoot)?true:false;
	$lists['confclients_warning'] = ($row->conference_enable && !$row->maxConfClients)?true:false;

	if ($row->conference_enable && !$lists['confpath_warning']) {
		if ($row->flascommRoot) {
			if (strlen($row->flascommRoot) > 10) {
				$order_num = substr($row->flascommRoot, -7, 5);
				$order_num2 = substr($row->flascommRoot, -6, 5);
				if (substr($row->flascommRoot,-1) == '/' && intval($order_num) == $order_num && substr($row->flascommRoot,-2,1)=='_') {
					$lists['confpath_warning'] = true;
				} elseif (substr($row->flascommRoot,-1) != '/' && substr($row->flascommRoot,-1)=='_' && intval($order_num2) == $order_num2 ) {
				} elseif (substr($row->flascommRoot,-1) == '/') {
				} else {
					$lists['confpath_warning'] = true;
				}
			}
		}
	}

	$lists['forum_warning'] = false;
	if ($row->plugin_forum) {
		if (!$row->forum_path) {
			$lists['forum_warning'] = true;
		} elseif (!file_exists($row->forum_path.'/Settings.php')){
			$lists['forum_warning'] = true;
		} elseif (substr($row->forum_path,-1) == '/') {
			$lists['forum_warning'] = true;
		}
	}

	$lists['tracking_enable'] 	= mosHTML::yesnoRadioList( 'tracking_enable', 'class="inputbox"', $row->tracking_enable );
	$lists['chat_enable'] 		= mosHTML::yesnoRadioList( 'chat_enable', 'class="inputbox"', $row->chat_enable );
	

	/* 18.10.2007 (DEN) - certificate option */
	$lists['save_certificates'] 	= mosHTML::yesnoRadioList( 'save_certificates', 'class="inputbox"', ($row->save_certificates === null ? 1 : $row->save_certificates) );
	$lists['crtf_show_sn'] 	= mosHTML::yesnoRadioList( 'crtf_show_sn', 'class="inputbox"', ($row->crtf_show_sn === null ? 1 : $row->crtf_show_sn) );
	$lists['crtf_show_barcode'] 	= mosHTML::yesnoRadioList( 'crtf_show_barcode', 'class="inputbox"', ($row->crtf_show_barcode === null ? 1 : $row->crtf_show_barcode) );
	$lists['crtf_duplicate_wm'] 	= mosHTML::yesnoRadioList( 'crtf_duplicate_wm', 'class="inputbox"', ($row->crtf_duplicate_wm === null ? 1 : $row->crtf_duplicate_wm) );
	/* end of certificate options */

	// !!!!!!!!!!!!! 24.08.2007 (text message is changed from 'online to 'offline'
	// $row->lms_isonline should be inverted for selectlist()
	$lists['lms_isonline'] 		= offMsg_yesnoRadioList( 'lms_isonline', 'class="inputbox"', $row->lms_isonline );
	$lists['lms_isoffline_warning'] = !$row->lms_isonline;

	// 24.08.2007 (check CB availability)
	$lists['is_cb_installed'] 	= mosHTML::yesnoRadioList( 'is_cb_installed', 'class="inputbox"', $row->is_cb_installed );
	$lists['lms_cb_warning'] = false;
	$cb_ex = file_exists(JPATH_SITE."/components/com_comprofiler/comprofiler.php");
	if ($row->is_cb_installed && !$cb_ex) {
		$lists['lms_cb_warning'] = true;
	} elseif(!$row->is_cb_installed && $cb_ex) {
		$lists['lms_cb_warning'] = true;
	}

	// 30.08.2007 (DEN) - configurable dimiurg task (guests checkout)
	$lists['guest_access_subscriptions'] = mosHTML::yesnoRadioList( 'guest_access_subscriptions', 'class="inputbox"', $row->guest_access_subscriptions );

	$lists['allow_import_users']= mosHTML::yesnoRadioList( 'allow_import_users', 'class="inputbox"',	$row->allow_import_users );
	$lists['enableterms'] 		= mosHTML::yesnoRadioList( 'enableterms', 'class="inputbox"',	$row->enableterms );
	$lists['enabletax'] 		= mosHTML::yesnoRadioList( 'enabletax', 'class="inputbox"',	$row->enabletax );

	// 27.11.2007 (DEN) - SSL mod
	$lists['use_secure_checkout'] = mosHTML::yesnoRadioList( 'use_secure_checkout', 'class="inputbox"',	$row->use_secure_checkout );
	
	// 29.03.2011 (DEN) - Multiple subscriptions chackout
	$lists['multiple_subs_checkout'] = mosHTML::yesnoRadioList( 'multiple_subs_checkout', 'class="inputbox"',	$row->multiple_subs_checkout );

	// 05.06.2008 (DEN) - 2nd SSL mod
	$lists['use_secure_enrollment'] = mosHTML::yesnoRadioList( 'use_secure_enrollment', 'class="inputbox"',	$row->use_secure_enrollment );

	//10.01.2008 (DEN) Custom subscriptions mod
	$lists['use_custom_subscr'] = mosHTML::yesnoRadioList( 'use_custom_subscr', 'class="inputbox"',	$row->use_custom_subscr );
	
	$options_date_format = array();
	$options_date_format[] = mosHTML::makeOption( 'Y-m-d', _JLMS_CFG_DATE_FORMAT_YMD );
	//$options_date_format[] = mosHTML::makeOption( 'Y, M d', 'Year Month(short) day(dd)' ); //test development
	//$options_date_format[] = mosHTML::makeOption( 'Y, F d', 'Year Month(full) day(dd)' ); //test development
	$options_date_format[] = mosHTML::makeOption( 'd-m-Y', _JLMS_CFG_DATE_FORMAT_DMY );
	$options_date_format[] = mosHTML::makeOption( 'm-d-Y', _JLMS_CFG_DATE_FORMAT_MDY );
	//$options_date_format[] = mosHTML::makeOption( 'M d, Y', 'Month(short) day(dd), Year' ); //test development
	//$options_date_format[] = mosHTML::makeOption( 'F d, Y', 'Month(full) day(dd), Year' ); //test development
	$lists['date_format']		= mosHTML::RadioList( $options_date_format, 'date_format', 'class="inputbox"', $row->date_format, 'value', 'text' );
	
	//test development
	//$options_time_format = array();
	//$options_time_format[] = mosHTML::makeOption( 'H:i:s', 'Hour:Minutes:Seconds' );
	//$options_time_format[] = mosHTML::makeOption( 'H:i', 'Hour:Minutes' );
	//$options_time_format[] = mosHTML::makeOption( 'h:i A', 'Hour:Minutes AM/PM' );
	//$lists['time_format']		= mosHTML::RadioList( $options_time_format, 'time_format', 'class="inputbox"', $row->time_format, 'value', 'text' );
	//test development
	
	//lms check version type
	$options_check_vers[] = mosHTML::makeOption( 0, _JLMS_CFG_AUTOMATIC );
	$options_check_vers[] = mosHTML::makeOption( 1, _JLMS_CFG_MANUALLY );
	$lists['lms_check_version']	= mosHTML::selectList( $options_check_vers, 'lms_check_version', ' class="text_area" ', 'value', 'text', $row->lms_check_version );
	//courses (1.0.3)
	/*Custom 911CHef (Max)*/
	$lists['show_short_description'] 	= mosHTML::yesnoRadioList( 'show_short_description', 'class="inputbox"', (!isset($row->show_short_description) ? 0 : $row->show_short_description) );
	$lists['show_course_publish_dates'] 	= mosHTML::yesnoRadioList( 'show_course_publish_dates', 'class="inputbox"',	(!isset($row->show_course_publish_dates) ? 0 : $row->show_course_publish_dates) );
	$price_fee_type = array();
	$price_fee_type[] = mosHTML::makeOption( 0, _JLMS_CFG_HIDE );
	$price_fee_type[] = mosHTML::makeOption( 1, _JLMS_CFG_SHOW_FEE_COL );
	$price_fee_type[] = mosHTML::makeOption( 2, _JLMS_CFG_SHOW_PRICE_COL );
	$lists['price_fee_type']	= mosHTML::selectList( $price_fee_type, 'price_fee_type', ' class="text_area" ', 'value', 'text', (!isset($row->price_fee_type) ? 1 : $row->price_fee_type) );
	
//	$lists['show_fee_column'] 	= mosHTML::yesnoRadioList( 'show_fee_column', 'class="inputbox"',	$row->show_fee_column );
	$lists['show_course_authors'] 	= mosHTML::yesnoRadioList( 'show_course_authors', 'class="inputbox"',	$row->show_course_authors );
	$lists['show_paid_courses'] = mosHTML::yesnoRadioList( 'show_paid_courses', 'class="inputbox"',	$row->show_paid_courses );
	$lists['show_future_courses'] = mosHTML::yesnoRadioList( 'show_future_courses', 'class="inputbox"',	$row->show_future_courses );
	$lists['show_course_fee_property'] 	= mosHTML::yesnoRadioList( 'show_course_fee_property', 'class="inputbox"',	$row->show_course_fee_property );
	$lists['show_course_spec_property'] = mosHTML::yesnoRadioList( 'show_course_spec_property', 'class="inputbox"',	$row->show_course_spec_property );
	$lists['show_course_meta_property'] = mosHTML::yesnoRadioList( 'show_course_meta_property', 'class="inputbox"',	$row->show_course_meta_property );
	$lists['show_course_access_property'] = mosHTML::yesnoRadioList( 'show_course_access_property', 'class="inputbox"',	$row->show_course_access_property );
	$lists['sec_cat_use'] = mosHTML::yesnoRadioList( 'sec_cat_use', 'class="inputbox"',	$row->sec_cat_use );
	$lists['sec_cat_show'] = mosHTML::yesnoRadioList( 'sec_cat_show', 'class="inputbox"',	$row->sec_cat_show );
	$lists['max_attendees_change'] = mosHTML::yesnoRadioList( 'max_attendees_change', 'class="inputbox"',	$row->max_attendees_change );
	$lists['use_global_groups'] = mosHTML::yesnoRadioList( 'use_global_groups', 'class="inputbox"', isset($row->use_global_groups)?$row->use_global_groups:1, _JLMS_CFG_MODE_GROUP_GLOBAL, _JLMS_CFG_MODE_GROUP_LOCAL );
	//---sort courses
	$lms_courses_sortby[] = mosHTML::makeOption( 0, _JLMS_CFG_COURSE_NAME );
	$lms_courses_sortby[] = mosHTML::makeOption( 1, _JLMS_CFG_ORDINAL_NUM );
	$lists['lms_courses_sortby']	= mosHTML::selectList( $lms_courses_sortby, 'lms_courses_sortby', ' class="text_area" ', 'value', 'text', $row->lms_courses_sortby );

	$colors = array();
	//default style
	$colors[1] = array('#E0DFE4','#F1F6CE','#E7F1B2','#999999', '#798730','#E0DFE4','#666666');
	//citrus cocktail
	$colors[2] = array('#FFF799','#FFE96B','#FFBE52','#25004A', '#25004A','#DE73FF','#DE73FF');
	//Blues'n lime
	$colors[3] = array('#E3E3E3','#7E8AA2','#263248','#OA111F', '#B8CC00','#E3E3E3','#B8CC00');
	//Blue smoke
	$colors[4] = array('#EFEFEF','#E4E4E4','#BFC5CE','#9D9D9D', '#4679C5','#B1CEF9','#7E8795');
	//Timber breeze
	$colors[5] = array('#F8F3A1','#EBCD54','#BFAB52','#2E3C1B', '#F8F3A1','#9EBA49','#877318');
	//Purple
	$colors[6] = array('#E4EDF9','#F2D3F6','#E7BDE5','#D57EBC', '#AE20A2','#E4EDF9','#6E6FBD');
	//Banana flambe
	$colors[7] = array('#FCD876','#E2AD3B','#BF5C00','#5C110F', '#FCD771','#901811','#901811');
	//Sunny
	$colors[8] = array('#FCF7BE','#FEF28F','#FFDA39','#EFDA39', '#DD4F08','#FCF7BE','#EF9846');
	//ice
	$colors[9] = array('#E3E6E9','#D3E1EC','#BDD0E0','#648DAF', '#097CDA','#E3E6E9','#1A4E78');
	$style = 0;

	for($i=1; $i<=count($colors); $i++){
		if ($row->conf_background == $colors[$i][0] && $row->conf_main_color == $colors[$i][1] && $row->conf_title_color == $colors[$i][2] && $row->conf_border_color == $colors[$i][3] &&  $row->conf_title_font_color == $colors[$i][4] && $row->conf_toolbar_color == $colors[$i][5] && $row->conf_files_font_color == $colors[$i][6]){
			$style = $i;
		}
	}

	$colors_scheme[] = mosHTML::makeOption( 0, _JLMS_CFG_CUSTOM_STYLE );
	$colors_scheme[] = mosHTML::makeOption( 1, _JLMS_CFG_DEF_STYLE );
	$colors_scheme[] = mosHTML::makeOption( 2, _JLMS_CFG_CITR_COCKTAIL );
	$colors_scheme[] = mosHTML::makeOption( 3, _JLMS_CFG_BLUES_N_LIME );
	$colors_scheme[] = mosHTML::makeOption( 4, _JLMS_CFG_BLU_COLOR_SMOKE );
	$colors_scheme[] = mosHTML::makeOption( 5, _JLMS_CFG_TIMBER_BREEZE );
	$colors_scheme[] = mosHTML::makeOption( 6, _JLMS_CFG_PURPLE );
	$colors_scheme[] = mosHTML::makeOption( 7, _JLMS_CFG_BANANA_FLAMBE );
	$colors_scheme[] = mosHTML::makeOption( 8, _JLMS_CFG_SUNNY );
	$colors_scheme[] = mosHTML::makeOption( 9, _JLMS_CFG_ICE );
	$lists['colors_scheme']	= mosHTML::selectList( $colors_scheme, 'colors_scheme', ' class="text_area" onchange = "change_colors(this.selectedIndex)" ', 'value', 'text', $style );

	//_fdow - first day of week
	$options_fdow[] = mosHTML::makeOption( 1, _JLMS_WD_MONDAY );
	$options_fdow[] = mosHTML::makeOption( 2, _JLMS_WD_SUNDAY );
	$lists['date_format_fdow']	= mosHTML::RadioList( $options_fdow, 'date_format_fdow', 'class="inputbox"', $row->date_format_fdow, 'value', 'text' );

	//plugin
	$lists['plugin_forum'] 						= mosHTML::yesnoRadioList( 'plugin_forum', 'class="inputbox"', $row->plugin_forum );
	//$lists['plugin_lpath_forum'] 				= mosHTML::yesnoRadioList( 'plugin_lpath_forum', 'class="inputbox"', $row->plugin_lpath_forum );

	//$lists['plugin_private_forum'] 				= mosHTML::yesnoRadioList( 'plugin_private_forum', 'class="inputbox"', $row->plugin_private_forum ); // 15.03.2008 - by DEN - private teachers discussions
	//$lists['plugin_private_lpath_forum']		= mosHTML::yesnoRadioList( 'plugin_private_lpath_forum', 'class="inputbox"', $row->plugin_private_lpath_forum ); // 15.03.2008 - by DEN - private lpath discussions

	$lists['plugin_quiz'] 						= mosHTML::yesnoRadioList( 'plugin_quiz', 'class="inputbox"', $row->plugin_quiz );
	$lists['quiz_progressbar'] 					= mosHTML::yesnoRadioList( 'quiz_progressbar', 'class="inputbox"', isset($row->quiz_progressbar)?$row->quiz_progressbar:0 );
	$lists['quiz_progressbar_highlight'] 		= mosHTML::yesnoRadioList( 'quiz_progressbar_highlight', 'class="inputbox"', isset($row->quiz_progressbar_highlight)?$row->quiz_progressbar_highlight:0 );
	$lists['quiz_progressbar_smooth'] 			= mosHTML::yesnoRadioList( 'quiz_progressbar_smooth', 'class="inputbox"', isset($row->quiz_progressbar_smooth)?$row->quiz_progressbar_smooth:1 );
	$lists['quiz_hs_offset_manual_correction'] 	= mosHTML::yesnoRadioList( 'quiz_hs_offset_manual_correction', 'class="inputbox" onChange="jlms_Change();"',	$row->quiz_hs_offset_manual_correction );

	$lists['pathway_show_lmshome'] 				= mosHTML::yesnoRadioList( 'pathway_show_lmshome', 'class="inputbox"', isset($row->pathway_show_lmshome)?$row->pathway_show_lmshome:0 );
	$lists['pathway_show_coursehome'] 			= mosHTML::yesnoRadioList( 'pathway_show_coursehome', 'class="inputbox"', isset($row->pathway_show_coursehome)?$row->pathway_show_coursehome:1 );
	
	//$lists['enablejointerms'] 	= mosHTML::yesnoRadioList( 'enablejointerms', 'class="inputbox"',	$row->enablejointerms );
	$query = "SELECT lang_name as value, lang_name as text FROM #__lms_languages ORDER BY lang_name";
	$db->SetQuery( $query );
	$lang = $db->LoadObjectList();
	// $lang[] = mosHTML::makeOption('english' , 'english');
	$lists['default_language'] = mosHTML::selectList($lang, 'default_language', ' class="text_area" ' , 'value', 'text', $row->default_language);

	// list of email payment
	$lists['subscr_status_email']	= mosHTML::yesnoRadioList( 'jlms_subscr_status_email', 'class="inputbox"',	$row->jlms_subscr_status_email );
	//mosHTML::selectList( $subscr_pay_email, 'jlms_subscr_status_email', '', 'value', 'text', $row->jlms_subscr_status_email );
	$lists['jlms_notecez'] 		= mosHTML::yesnoRadioList( 'jlms_notecez', 'class="inputbox"', $row->jlms_notecez );

	$query = "SELECT * FROM `#__lms_attendance_periods` ";
	$db -> setQuery($query);
	$periods = $db->loadObjectList();
	
	//FLMS 17.07.08 parent = 0
	$query = "SELECT * FROM `#__lms_course_cats` WHERE parent = '0' ORDER BY c_category ";
	$db -> setQuery($query);
	$course_cats = $db->loadObjectList();

	$query = "SELECT * FROM `#__lms_file_types` ";
	$db -> setQuery($query);
        
	$version = new JVersion();
	if (strnatcasecmp( $version->RELEASE, '1.6' ) >= 0) 
		{
			 $types = $db->loadColumn();
		}
		else $types = JLMSDatabaseHelper::loadResultArray();
		
	$file_types = '';

	if (sizeof($types))	$file_types = implode(', ',$types);

	$query = "SELECT * FROM `#__lms_gradebook_cats` ";
	$db -> setQuery($query);
	$gradebook_cats = $db->loadObjectList();

	global $jlms_license_expires_str;
	$lists['expired_str'] = $jlms_license_expires_str;

	$users_str = '<span style = "font-weight:bold; color:green">'._JLMS_CFG_UNLIMITED.'</span>';
	global $license_lms_users;
	if ($license_lms_users) {
		$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
		$db->SetQuery( $query );
		$total_students = $db->LoadResult();
		$ex_color = 'green';
		if ( ($license_lms_users*80/100) < $total_students ) {
			$ex_color = 'red';
		}
		$users_str = '<span style = "font-weight:bold; color:'.$ex_color.'">'.$total_students."</span> "._JLMS_LICENSE_USERS_OF." <span style = 'font-weight:bold; color:".$ex_color."'>".$license_lms_users."</span>";
	}
	$lists['users_str'] = $users_str;	
	
	$gtree = $acl->get_group_children_tree( null, 'Public Backend', true ) ;
	
	$backend_gids = $row->backend_access_gid;
	
	$backend_gid = explode(',',$backend_gids);	
	$backend_gid = array_merge( $backend_gid, JLMS_getAdminGroups() );
	
	$selected = array();
	foreach ($backend_gid as $o) {
		$s = new stdClass();
		$s->value = $o;
		$selected[] = $s;
	}
	
	$lists['backend_access_gid'] = mosHTML::selectList( $gtree, 'backend_access_gid[]', 'size="4" multiple="multiple"', 'value', 'text', $selected );
	/* 20May2009 - end of BackEnd access customization */

	global $license_lms_branding_free;
	if ($license_lms_branding_free) {
			$doc = JFactory::getDocument();
			$style = '				
					fieldset.branding 
					{
						float: left;	
						border: 0 none;	
						padding: 0 !important;
					}
					
					fieldset.branding input
					{
						float: left;
					}
					
					fieldset.branding label
					{				
						float: left;
						min-width: 5px !important;
						clear: none;
						padding: 0 5px 0 0;
						margin-top: 5px;
					}		
			';
			$doc->addStyleDeclaration( $style );
			if (JLMS_J30version()) {
				$lists['branding_free_configured'] = mosHTML::yesnoRadioList( 'branding_free_configured', 'class="inputbox"', $row->branding_free_configured );
			} else {
				$lists['branding_free_configured'] = '<fieldset class="branding">'.mosHTML::yesnoRadioList( 'branding_free_configured', 'class="inputbox"', $row->branding_free_configured ).'</fieldset>';
			}
	} else {
		$lists['branding_free_configured'] = '<span style = "font-weight:bold; color:red">'._JLMS_NO.'</span><input type="hidden" name="branding_free_configured" value="0" />';
	}

	joomla_lms_adm_html::JLMS_showconfig( $row, $lists, $periods, $course_cats, $gradebook_cats, $file_types, $option, $colors);
}

function JLMS_uploadUnicode()
{
        @set_time_limit( 3600 );
        
        $fileName = 'arial-unicode.ttf'; 
		$src = 'http://joomlalms.com/user_downloads/'.$fileName;
        $tmp = JPATH_SITE.DS.'tmp'.DS.$fileName;
		$dest = JPATH_SITE.DS.'media'.DS.$fileName;
        
        $app = JFactory::getApplication();
                
        @$fp = fopen($src,"r");
        @$fp1 = fopen($tmp,"w");
        
        $msg = '';
        
        if( $fp && $fp1 ) 
        {   
            $res = false;
            
            while(!feof($fp))
            {                
                $res = fwrite($fp1,fgets($fp,8192), 8192);
            }
            
            fclose($fp);
            fclose($fp1);        
             
            if( $res !== false && filesize($tmp) ) 
            {      	
        		if ( JFile::copy( $tmp, $dest ) ) 
        		{
        		  $msg = _JLMS_NOTS_FONT_COPYING_COMPLETED;            		
        		} else {
                    $errors = JError::getErrors();
                    if( $errors ) {
                        if( JLMS_J16version() ) {
                            $msg = implode('<br />', $errors );
                        } else {
                            $msg = $errors[0]->message;
                        }
                    }
        		}
          } else {
            $msg = _JLMS_NOTS_FONT_COPYING_ERROR;
          }
       } else {
            $msg = _JLMS_NOTS_FONT_COPYING_ERROR;
       }
       
       ob_clean(); 
       echo '{"msg": "'.$msg.'"}';        		
       exit();	
} 

function JLMS_config_save( $option ) {
	$db = JFactory::getDBO();
	$JLMS_CONFIG = JLMSFactory::getConfig();
	
    $error = '';

	$row = new jlms_adm_config();
	$row->loadFromDb( $db );			
	$row->bind($_POST);	
	$row->attendance_days = $_POST['attendance_days'];
	
	$rulesOptions = array();
		
	if( !empty($row->backend_access_gid) ) 
	{
		$rulesOptions = array_fill_keys($row->backend_access_gid, true);		
		$row->backend_access_gid = implode( ',', $row->backend_access_gid );		
	}    

	if( JLMS_J25version() ) 
	{
		$rules = array();	
		$rules['rules'] = array('core.mange' => $rulesOptions );	
		$result = JComponentHelper::getComponent($option);		
		$optModel = JLMSModelOptions::getInstance('options', 'JLMSModel');
		
		$data	= array(
					'params'	=> $rules,
					'id'		=> $result->id,
					'option'	=> $option
					);	
		if( !$optModel->save($data) ) 
		{
			$error = '<br>'.implode("\n", $optModel->getErrors());
		}
	}
	
    	
	$row->saveToDb( $db );        

	$begin 	= mosGetParam($_REQUEST, 'sf_hid_scale','');
	$end 	= mosGetParam($_REQUEST, 'sf_hid_scale2','');

	$i = 1;
	$test = array();
	$test2 = array();
	foreach ($begin as $value){
		$test[$i] = $value;
		$i++;
	}
	$k = 1;
	foreach ($end as $value){
		$test2[$k] = $value;
		$k++;
	}

	$query = "DELETE FROM `#__lms_attendance_periods` ";
	$db -> setQuery($query);
	$db->query();    

	for ($i=1; $i<=count($test);$i++){
		$query = "INSERT INTO `#__lms_attendance_periods` (id, period_begin, period_end) "
		."VALUES ('".$i."', '".$test[$i]."', '".$test2[$i]."')";
		$db -> setQuery($query);
		$db->query();
	}
	//////////////////////////////////////////////////////////
	//				File types saving						//
	//////////////////////////////////////////////////////////
	$query = "DELETE FROM `#__lms_file_types` ";
	$db -> setQuery($query);
	$db->query();

	$types 	= mosGetParam($_REQUEST, 'jlms_file_types','');

	$file_types = explode(',',$types);

	$values = '';
	for($i=0;$i<count($file_types);$i++){
		$values .= " ( '".trim($file_types[$i])."' ) ";
		if (($i+1) != count($file_types)){ $values .= ' , ';}
	}

	if ($values)
    {
		$query = "INSERT INTO `#__lms_file_types` ( `filetype` ) VALUES $values ";
		$db -> setQuery($query);
		$db->query();
	}

	//////////////////////////////////////////////////////////
	//				gradeBooks cats saving					//
	//////////////////////////////////////////////////////////
	$grade_cats = mosGetParam($_REQUEST, 'jlms_grade_cat_name','');
	$grade_ids	= mosGetParam($_REQUEST, 'jlms_grade_cat_id','');

	$i = 1;
	$test = array();
	$test2 = array();
	foreach ($grade_cats as $value){
		$test[$i] = $value;
		$i++;
	}
	$k = 1;

	foreach ($grade_ids as $value){
		$test2[$k] = $value;
		$k++;
	}
	$query = "DELETE FROM `#__lms_gradebook_cats` ";
	$db -> setQuery($query);
	$db->query();

	for ($i=1; $i<=count($test);$i++){
		$id = '';
		if ($test2[$i]){
			$id = $test2[$i];
		}
		$query = "INSERT INTO `#__lms_gradebook_cats` (id, gb_category) "
		."VALUES ('".$id."', '".$test[$i]."')";
		$db -> setQuery($query);
		$db->query();
	}
	
	$forum_path = mosGetParam($_REQUEST, 'forum_path', '');
	$forum_enable = intval(mosGetParam($_REQUEST, 'plugin_forum', 0));
	if ($forum_enable) 
	{         
        if (!file_exists($forum_path.'/Settings.php'))
        {
        	$error .= '<br>'._JLMS_CFG_MSG_F_DOESNT_EXISTS;
        } else {
        	if(class_exists('JLMS_SMF'))
        	{
				$JLMS_CONFIG->set('forum_path', $forum_path);
				$JLMS_CONFIG->set('forum_enable', $forum_enable);
				
        		$forum = & JLMS_SMF::getInstance();
        		if( $forum )
        		{
        			$forum->resetLocalCookiesOption();
        		}
        	}			
        }
	}
	mosRedirect ("index.php?option=com_joomla_lms&task=config", _JLMS_CFG_MSG_SETT_SAVED.$error);
}




##########################################################################
###	--- ---   COURSE BACKUPS	 	--- --- ###
//TODO: this feature is out of date ?
##########################################################################
function JLMS_coursesList( $option ) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );

	$query = "SELECT COUNT(*)"
	. "\n FROM #__lms_courses";
	$db->setQuery( $query );
	$total = $db->loadResult();

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	$query = "SELECT * FROM `#__lms_courses` "
	. "\n ORDER BY id "
	. "\n LIMIT $pageNav->limitstart, $pageNav->limit";
	$db->setQuery( $query );
	$rows = $db->loadObjectList();

	joomla_lms_adm_html::JLMS_coursesList( $rows, $pageNav, $option);
}

function JLMS_courseBackups( $option, $course_id ) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );

	$query = "SELECT COUNT(*)"
	. "\n FROM #__lms_courses_backups";
	$db->setQuery( $query );
	$total = $db->loadResult();

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	$query = "SELECT * FROM `#__lms_courses_backups`  "
	. "WHERE course_id = $course_id "
	. "\n ORDER BY backupdate DESC"
	. "\n LIMIT $pageNav->limitstart, $pageNav->limit";
	$db->setQuery( $query );
	$rows = $db->loadObjectList();

	$name = "SELECT course_name FROM `#__lms_courses`  "
	. "WHERE id = '".$course_id."' ";
	$db->setQuery( $name );
	$c_name = $db->loadResult();
	$GLOBALS['jlms_toolbar_cname'] = $c_name;
	joomla_lms_adm_html::JLMS_coursebackupsList( $rows, $pageNav, $option, $course_id, $c_name);
}

//TODO: this feature is out of date ?
function JLMS_courseDownload(  $option, $backup_id ){
	$db = JFactory::getDBO();
	require_once (_JOOMLMS_INCLUDES_PATH."jlms_download.php");

	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'jlms_backup_folder'";
	$db->setQuery($query);
	$backup_path = $db->loadResult();

	$query = "SELECT name FROM `#__lms_courses_backups` WHERE id = '$backup_id'";
	$db -> setQuery($query);
	$backup_name = $db-> loadResult();

	JLMS_download ( $backup_name,$backup_path."/".$backup_name );
}


##########################################################################
###	--- ---   TOTAL BACKUPS	 	--- --- ###
##########################################################################
//TODO: this feature is out of date ?
function JLMS_backupsList( $option ) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );

	$query = "SELECT COUNT(*)"
	. "\n FROM #__lms_backups";
	$db->setQuery( $query );
	$total = $db->loadResult();

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	$query = "SELECT * FROM `#__lms_backups` "
	. "\n ORDER BY backupdate DESC"
	. "\n LIMIT $pageNav->limitstart, $pageNav->limit";
	$db->setQuery( $query );
	$rows = $db->loadObjectList();

	joomla_lms_adm_html::JLMS_backupsListhtml( $rows, $pageNav, $option);
}


function JLMS_backupGenerate( $option ){
	$db = JFactory::getDBO();
	$app = JFactory::getApplication();
	$dbprefix = $app->getCfg('dbprefix');
	@set_time_limit('3000');

	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'jlms_backup_folder'";
	$db->setQuery($query);
	$backup_path = $db->loadResult();
	//dump table of lms_agenda
	require_once(_JOOMLMS_FRONT_HOME . "/includes/libraries/lms.lib.zip.php");

	$tables = array(
	'#__lms_agenda',
	'#__lms_attendance',
	'#__lms_attendance_periods',
	'#__lms_cb_assoc',
	'#__lms_certificate_prints',
	'#__lms_certificate_users',
	'#__lms_certificates',
	'#__lms_chat_history',
	'#__lms_chat_users',
	'#__lms_conference_config',
	'#__lms_conference_doc',
	'#__lms_conference_period',
	'#__lms_conference_records',
	'#__lms_conference_usr',
	'#__lms_config',
	'#__lms_course_cats',
	'#__lms_course_cats_config',
	'#__lms_course_level',
	//'#__lms_course_price',
	'#__lms_courses',
	'#__lms_courses_backups',
	'#__lms_courses_template',
	'#__lms_disc_c_usage_stats',
	'#__lms_discount_coupons',
	'#__lms_discounts',
	'#__lms_documents',
	'#__lms_documents_perms',
	'#__lms_documents_view',
	'#__lms_documents_zip',
	'#__lms_dropbox',
	'#__lms_email_notifications',
	'#__lms_email_templates',
	'#__lms_file_types',
	'#__lms_files',
	'#__lms_forum_details',
	'#__lms_forums',
	'#__lms_gqp_cats',
	'#__lms_gqp_levels',
	'#__lms_gradebook',
	'#__lms_gradebook_cats',
	'#__lms_gradebook_items',
	'#__lms_gradebook_lpaths',
	'#__lms_gradebook_scale',
	'#__lms_homework',
	'#__lms_homework_results',
	'#__lms_languages',
	'#__lms_learn_path_conds',
	'#__lms_learn_path_grades',
	'#__lms_learn_path_prerequisites',
	'#__lms_learn_path_results',
	'#__lms_learn_path_step_results',
	'#__lms_learn_path_step_quiz_results',
	'#__lms_learn_path_step_types',
	'#__lms_learn_path_steps',
	'#__lms_learn_paths',
	'#__lms_links',
	'#__lms_local_menu',
	'#__lms_menu',
	'#__lms_message_configuration',
	'#__lms_messagelist',
	'#__lms_messages',
	'#__lms_messages_to',
	'#__lms_n_scorm',
	'#__lms_n_scorm_lib',
	'#__lms_n_scorm_scoes',
	'#__lms_n_scorm_scoes_data',
	'#__lms_n_scorm_scoes_track',
	'#__lms_n_scorm_seq_mapinfo',
	'#__lms_n_scorm_seq_objective',
	'#__lms_n_scorm_seq_rolluprulecond',
	'#__lms_n_scorm_seq_rulecond',
	'#__lms_n_scorm_seq_ruleconds',
	'#__lms_notifications',
	'#__lms_outer_documents',
	'#__lms_page_notices',
	'#__lms_page_tips',
	'#__lms_payment_info',
	'#__lms_payment_items',
	'#__lms_payments',
	'#__lms_payments_checksum',
	'#__lms_plans',
	'#__lms_plans_subscriptions',
	'#__lms_plugins',
	'#__lms_quiz_images',
	'#__lms_quiz_languages',
	'#__lms_quiz_r_student_blank',
	'#__lms_quiz_r_student_choice',
	'#__lms_quiz_r_student_hotspot',
	'#__lms_quiz_r_student_matching',
	'#__lms_quiz_r_student_question',
	'#__lms_quiz_r_student_quiz',
	'#__lms_quiz_r_student_quiz_pool',
	'#__lms_quiz_r_student_scale',
	'#__lms_quiz_r_student_survey',
	'#__lms_quiz_results',
	'#__lms_quiz_t_blank',
	'#__lms_quiz_t_category',
	'#__lms_quiz_t_choice',
	'#__lms_quiz_t_hotspot',
	'#__lms_quiz_t_matching',
	'#__lms_quiz_t_qtypes',
	'#__lms_quiz_t_question',
	'#__lms_quiz_t_question_fb',
	'#__lms_quiz_t_quiz',
	'#__lms_quiz_t_quiz_gqp',
	'#__lms_quiz_t_quiz_gqp_questions',
	'#__lms_quiz_t_quiz_pool',
	'#__lms_quiz_t_scale',
	'#__lms_quiz_t_text',
	'#__lms_quiz_templates',
	'#__lms_scorm_packages',
	'#__lms_scorm_sco',
	'#__lms_spec_reg_answers',
	'#__lms_spec_reg_questions',
	'#__lms_subs_invoice',
	'#__lms_subscriptions',
	'#__lms_subscriptions_config',
	'#__lms_subscriptions_countries',
	'#__lms_subscriptions_courses',
	'#__lms_subscriptions_custom',
	'#__lms_subscriptions_custom_courses',
	'#__lms_subscriptions_procs',
	'#__lms_topic_items',
	'#__lms_topics',
	'#__lms_track_chat',
	'#__lms_track_downloads',
	'#__lms_track_hits',
	'#__lms_track_learnpath_stats',
	'#__lms_track_learnpath_steps',
	'#__lms_user_assign_groups',
	'#__lms_user_assigned_groups',
	'#__lms_user_courses',
	'#__lms_user_parents',
	'#__lms_user_permissions',
	'#__lms_usergroups',
	'#__lms_users',
	'#__lms_users_in_global_groups',
	'#__lms_users_in_groups',
	'#__lms_usertypes',
	'#__lms_waiting_lists',
	'#__lms_user_roles_assignments'
	);
	$backup = '';

	$query = "SET SQL_MODE='ANSI'";
	$db->setQuery($query);
	$db -> query();

	for ( $i=0; $i<count($tables);$i++){
		$query = "SHOW TABLES like '".str_replace('#__', $dbprefix, $tables[$i])."'";
		$db->setQuery( $query );
		$isset = $db->loadResult();
		if ($isset) {
			$query = "SHOW CREATE TABLE ".$tables[$i]." ";
			$db -> setQuery($query);
			//TODO: check if loadassoclist works in Joomla 1.5 and Joomla 1.6 and if it works as before in Joomla 1.0.x
			$rows = $db -> loadAssocList();
			if (!$db->geterrormsg()) {
				$backup .= "DROP TABLE IF EXISTS `".$tables[$i]."`;\r\n";
				//formiruem CREATE TABLE
				foreach ($rows as $row){
					next($row);
					$k = key($row);
					$backup .= str_replace('"', '`', $row[$k]).";\r\n";
				}
				$query = "SELECT * FROM ".$tables[$i]." ";
				$db -> setQuery ($query);
				$ins_array = $db -> loadAssocList();
		
				$query = "SHOW COLUMNS FROM ".$tables[$i];
				$db -> setQuery($query);
				$columns = $db -> loadObjectList();
		
				//formiruem shapku dlia INSERT
				$insert = "INSERT INTO `".$tables[$i]."` (";
				$k = 0;
				foreach ($columns as $fields){
					$insert .= "`".$fields->Field."`";
					if($k != (count($columns)-1)){
						$insert .= ",";
					}
					$k++;
				}
				$insert .= ") VALUES (";
				//end
				//formiruem spisok INSERTOv
				foreach($ins_array as $ins){
					$backup .= $insert;
					$k=0;
					foreach ($columns as $fields){
						$backup .= "'".$ins[$fields->Field]."'";
						if($k != (count($columns)-1)){
							$backup .= ",";
						}
						$k++;
					}
					$backup .= ");\r\n";
				}
			}
		}
	}

	$filename = JPATH_SITE.'/tmp/backup.sql';
	$handle = fopen($filename, 'w');

	// Write $somecontent in our open file.
	if (fwrite($handle, $backup) === FALSE) {
		echo "File /tmp/backup.sql not writable. Please make this file/folder writable and try again.";
		exit;
	}
	fclose($handle);

	//@chmod($backup_path, '777');
	$uniq = mktime();
	$backup_zip = $backup_path.'/backup_'.$uniq.'.zip';
	$pz = new PclZip($backup_zip);

	//get backups folder path
	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'jlms_backup_folder'";
	$db->setQuery($query);
	$backups_folder_from_config = $db->loadResult();

	//add _lms_files catalog
	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'jlms_doc_folder'";
	$db->setQuery($query);
	$filename = $db->loadResult();
	if ($filename != $backups_folder_from_config) {
		$pz->create($filename,'files',$filename);
	}

	//add lms_scorm archive catalog
	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'scorm_folder'";
	$db->setQuery($query);
	$scorm_folder = $db->loadResult();

	$filename = JPATH_SITE .'/' .$scorm_folder;
	if ($filename != $backups_folder_from_config) {
		$pz->add($filename,'scorm',$filename);
	}

	//add certificates archive catalog
	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'jlms_crtf_folder'";
	$db->setQuery($query);
	$crtf_folder = $db->loadResult();
	if ($crtf_folder != $backups_folder_from_config) {
		$pz->add($crtf_folder,'certificates',$crtf_folder);
	}

	//add invoices archive catalog
	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'jlms_subscr_invoice_path'";
	$db->setQuery($query);
	$invoices_folder = $db->loadResult();
	if ($invoices_folder != $backups_folder_from_config) {
		$pz->add($invoices_folder,'invoices',$invoices_folder);
	}

	//add sql file
	$filename = JPATH_SITE . '/tmp/backup.sql';
	$pz->add($filename,'', JPATH_SITE . '/tmp/');

	$query = "INSERT INTO `#__lms_backups` (name, backupdate) VALUES ('backup_".$uniq.".zip','".date('Y-m-d H:i:s',$uniq)."')";
	$db -> setQuery($query);
	$db -> query();

	mosRedirect ("index.php?option=com_joomla_lms&task=backup", _JLMS_BCK_MSG_HAS_BEEN_CREATED );

}

function JLMS_backupRestore( $id, $option ){
	$db = JFactory::getDBO();
	@set_time_limit('3000');
	require_once(_JOOMLMS_INCLUDES_PATH . "libraries/lms.lib.zip.php");
	require_once(_JOOMLMS_INCLUDES_PATH . "jlms_dir_operation.php");
	require_once(_JOOMLMS_INCLUDES_PATH . "jlms_zip_operation.php");
	require_once ( _JOOMLMS_INCLUDES_PATH . "jlms_splitSqlFile.php");

	$query = "SELECT name FROM #__lms_backups WHERE id = '$id'";
	$db -> setQuery ($query);
	$name = $db -> loadResult();

	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'jlms_backup_folder'";
	$db->setQuery($query);
	$backup_path = $db->loadResult();

	$extract_dir = JPATH_SITE."/media/backup_".uniqid()."/";
	//exstarct archive in uniqfolder media
	$backup_name = $backup_path."/".$name;
	extractBackupArchive($backup_name, $extract_dir);

	//read backup.sql file
	ob_start();
	@readfile ($extract_dir.'/backup.sql' );
	$sql = ob_get_contents();
	ob_end_clean();
	//formiruem massiv zaprosov k baze dannix
	$instr_array = array();
	$release = '';

	JLMS_splitSqlFile($instr_array, $sql, $release);
	//restore jlms dbase
	for ($i=0;$i<count($instr_array);$i++){
		$db->setQuery($instr_array[$i]['query']);
		$db->query();
	}
	//restore saved /files

	$fromDir = $extract_dir."files";
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'jlms_doc_folder'";
	$db->setQuery($query);
	$doc_folder = $db->loadResult();

	$toDir = $doc_folder;
	if (!is_dir($toDir)) {
		mkdir ($toDir);
	}
	if (is_dir($fromDir)) {
		copydirr($fromDir,$toDir);
	}

	//certificates
	$fromDir = $extract_dir."certificates";
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'jlms_crtf_folder'";
	$db->setQuery($query);
	$cr_folder = $db->loadResult();

	$toDir = $cr_folder;
	if (!is_dir($toDir)) {
		mkdir ($toDir);
	}
	if (is_dir($fromDir)) {
		copydirr($fromDir,$toDir);
	}

	//ionoices
	$fromDir = $extract_dir."invoices";
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'jlms_subscr_invoice_path'";
	$db->setQuery($query);
	$in_folder = $db->loadResult();

	$toDir = $in_folder;
	if (!is_dir($toDir)) {
		mkdir ($toDir);
	}
	if (is_dir($fromDir)) {
		copydirr($fromDir,$toDir);
	}

	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'scorm_folder'";
	$db->setQuery($query);
	$scorm_folder = $db->loadResult();
	//restore saved scorm
	$fromDir = $extract_dir."scorm/";
	$toDir = JPATH_SITE."/".$scorm_folder;
	if (!is_dir($toDir)) {
		mkdir ($toDir);
	}
	copydirr($fromDir,$toDir);

	//delete tmp files
	deldir($extract_dir);

	mosRedirect ( "index.php?option=com_joomla_lms&task=backup", _JLMS_BCK_MSG_RESTORE_CREATED );

}

function JLMS_backupDelete( $cid, $option ){
	$db = JFactory::getDBO();

	$cid = implode(',',$cid);

	$query = "SELECT name FROM #__lms_backups WHERE id IN ('$cid')";
	$db -> setQuery ($query);
	$b_list = $db -> loadObjectList();
	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'jlms_backup_folder'";
	$db->setQuery($query);
	$backup_path = $db->loadResult();

	for($i=0;$i<count($b_list);$i++){
		$path = $backup_path."/".$b_list[$i]->name;
		if(file_exists($path)){
			unlink ($path);
		}
	}
	$query = "DELETE FROM `#__lms_backups` WHERE id IN ($cid)";
	$db -> setQuery ($query);
	$db -> query();

	mosRedirect ( "index.php?option=com_joomla_lms&task=backup", _JLMS_BCK_MSG_DELETED );
}

function JLMS_coursebackupsDelete( $cid, $option, $course_id ){
	$db = JFactory::getDBO();

	$cid = implode(',',$cid);

	$query = "SELECT name FROM #__lms_courses_backups WHERE id IN ('$cid')";
	$db -> setQuery ($query);
	$b_list = $db -> loadObjectList();

	$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'jlms_backup_folder'";
	$db->setQuery($query);
	$backup_path = $db->loadResult();

	for($i=0;$i<count($b_list);$i++){
		$path = $backup_path."/".$b_list[$i]->name;
		if(file_exists($path)){
			unlink ($path);
		}
	}
	$query = "DELETE FROM `#__lms_courses_backups` WHERE id IN ($cid)";
	$db -> setQuery ($query);
	$db -> query();

	mosRedirect ( "index.php?option=com_joomla_lms&task=view_course_backup&id=".$course_id, _JLMS_BCK_MSG_DELETED );
}

##########################################################################
###	--- ---   	USERS	 	--- --- ###
##########################################################################
function JLMS_ListUsers( $option ) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );

	// get the total number of records
	$query = "SELECT COUNT(*)"
	. "\n FROM #__lms_users WHERE lms_usertype_id IN (1,5)";
	$db->setQuery( $query );
	$total = $db->loadResult();

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	// get the subset (based on limits) of required records
	$query = "SELECT b.lms_usertype, a.lms_block, a.id, c.username, c.name, c.email "
	. "\n FROM  #__lms_usertypes as b, #__lms_users as a LEFT JOIN #__users as c ON a.user_id = c.id WHERE a.lms_usertype_id = b.id AND a.lms_usertype_id IN (1,5)"
	. "\n ORDER BY c.username ASC, b.roletype_id DESC"
	. "\n LIMIT $pageNav->limitstart, $pageNav->limit"
	;
	$db->setQuery( $query );
	$rows = $db->loadObjectList();

	joomla_lms_adm_html::JLMS_showUsersList( $rows, $pageNav, $option );
}

function JLMS_editUser( $id, $option ) {
	$db = JFactory::getDBO();

	$row = new mos_Joomla_LMS_User( $db );
	// load the row from the db table
	$row->load( $id );
	if (!$id) {
		$row->lms_block = 0;
	}
	$GLOBALS['jlms_toolbar_id'] = $id;
	$lists = array();
	$query = "SELECT id, username FROM #__users order by username";
	$db->setQuery( $query );
	$sf_j_users = $db->loadObjectList();
	$lists['jlms_users'] = mosHTML::selectList( $sf_j_users, 'user_id', 'class="text_area" size="1" style="width:200px" ', 'id', 'username', $row->user_id );

	$query2 = "SELECT id as value, lms_usertype as text FROM #__lms_usertypes WHERE id IN (1, 5) order by id";
	$db->setQuery( $query2 );
	$lms_types = $db->loadObjectList();

	$lists['jlms_usertypes'] = mosHTML::selectList( $lms_types, 'lms_usertype_id', 'class="text_area" size="1" style="width:200px" ', 'value', 'text', $row->lms_usertype_id );
	$lists['jlms_blockuser'] = mosHTML::yesnoRadioList( 'lms_block', 'class="inputbox" size="1"', $row->lms_block );


	joomla_lms_adm_html::JLMS_editUser( $row, $lists, $option );
}

function JLMS_saveUser( $option ) {
	$db = JFactory::getDBO();

	$row = new mos_Joomla_LMS_User( $db );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if ($row->lms_usertype_id != 1 && $row->lms_usertype_id != 5) {
		$row->lms_usertype_id = 1;
	}
	$row->lms_block = 0;
	if (!$row->check()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	mosRedirect( "index.php?option=$option&task=users" );
}

function JLMS_removeUser( &$cid, $option ) {
	$db = JFactory::getDBO();
	if (count( $cid )) {
		$cids = implode( ',', $cid );
		$query = "DELETE FROM #__lms_users"
		. "\n WHERE id IN ( $cids ) AND lms_usertype_id IN (1,5)"
		;
		$db->setQuery( $query );
		if (!$db->query()) {
			echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}
	mosRedirect( "index.php?option=$option&task=users" );
}

function JLMS_cancelUser($option) {
	mosRedirect("index.php?option=$option&task=users");
}
function JLMS_cancelBackups($option) {		
	mosRedirect("index.php?option=$option");
}
##########################################################################
###	--- ---   	CLASSES	 	--- --- ###
##########################################################################
function JLMS_ListClasses( $option ) {
	global $JLMS_CONFIG;
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');
	
	$members_without_group = '';
	$teacher_assistanse = '';
		
	if ($JLMS_CONFIG->get('use_global_groups', 1)) 
	{
		$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit{$option}_list_classes_global_groups", 'limit', $app->getCfg('list_limit') ) );
		$limitstart = intval( $app->getUserStateFromRequest( "view{$option}_list_classes_global_groups_limitstart", 'limitstart', 0 ) );

		$query = "SELECT * FROM #__lms_usergroups WHERE course_id = 0 ORDER BY ug_name,parent_id";
		$db->setQuery($query);
		$db->query();
		$total = $db->getNumRows();

		require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
		$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

		//$db->setQuery($query, $limitstart, $limit);
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		if (sizeof($rows))
		{
			$parents = $childrens = $rows_tree = $rez_rows = array();
			//Get parent and child groups
			foreach ( $rows as $row ) 
			{
				if ($row->parent_id==0)
				{
					$parents[] = $row;
				}else $childrens[] = $row;
			}
			//Create tree array
			if (sizeof($parents))
			{
				foreach ( $parents as $parent ) 
				{
					$rows_tree[] = $parent;
					foreach ( $childrens as $child ) 
					{
						if ($child->parent_id==$parent->id)
						{
							$rows_tree[] = $child;
						}
					} 
				}
			}
			//Split tree
			if ($limit==0) $rez_rows = $rows_tree;
			else
			{
				foreach ( $rows_tree as $k=>$trow ) 
					{
						if ($k>=$limitstart && ($k<$limit+$limitstart)) 
						{
							$rez_rows[] = $trow;
						}
					}
			}
		$rows = $rez_rows;	
		}

		for($i=0;$i<count($rows);$i++) {
			
			if($rows[$i]->parent_id) {
				$query = "SELECT count(ug.id) FROM #__lms_users_in_global_groups AS ug, #__lms_usergroups AS g, #__users AS u WHERE g.id=ug.group_id AND u.id=ug.user_id AND ug.subgroup1_id = ".$rows[$i]->id."";
			}
			else {
				$query = "SELECT count(ug.id) FROM #__lms_users_in_global_groups AS ug, #__lms_usergroups AS g, #__users AS u WHERE g.id=ug.group_id AND u.id=ug.user_id AND ug.group_id=".$rows[$i]->id."";
			}

			$db->setQuery($query);
			$db->query();
			$total = $db->LoadResult();

			if($total == 1) {
				if($rows[$i]->parent_id) {
					$query = "SELECT a.name FROM #__users as a, #__lms_users_in_global_groups as b WHERE b.subgroup1_id = ".$rows[$i]->id." AND b.user_id = a.id";
				}
				else {
					$query = "SELECT a.name FROM #__users as a, #__lms_users_in_global_groups as b WHERE b.group_id = ".$rows[$i]->id." AND b.user_id = a.id";
				}

				$db->setQuery($query);
				$db->query();
				$name = $db->LoadResult();
				$rows[$i]->members = $name;
			}
			else {
				$rows[$i]->members = $total." "._JLMS_USERS_MEMBER_S;
			}
			
			$query = "SELECT count(user_id) FROM #__lms_user_assign_groups WHERE group_id = ".$rows[$i]->id."";
			$db->setQuery($query);
			$db->query();
			$total = $db->LoadResult();
			if($total == 1) {
				$query = "SELECT a.name FROM #__users as a, #__lms_user_assign_groups as b WHERE b.group_id = ".$rows[$i]->id." AND b.user_id = a.id";
				$db->setQuery($query);
				$db->query();
				$name = $db->LoadResult();
				$rows[$i]->managers = $name;
			}
			else {
				$rows[$i]->managers = $total." "._JLMS_USERS_MANAGER_S;
			}
		}
		
		$lists = array();
	} else {
		$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit{$option}_list_classes", 'limit', $app->getCfg('list_limit') ) );
		$limitstart = intval( $app->getUserStateFromRequest( "view{$option}_list_classes_limitstart", 'limitstart', 0 ) );

		$filt_course = intval( $app->getUserStateFromRequest( "filt_course{$option}", 'filt_course', 0 ) );
		//$course_filter = intval( $app->getUserStateFromRequest( "course_filter{$option}_lms_users", 'course_id', 0 ) );
		
		$query = "SELECT COUNT(b.id)"
		. "\n FROM #__lms_usergroups as b, #__lms_courses as c LEFT JOIN #__users as d ON c.owner_id = d.id "
		. "\n WHERE b.course_id = c.id"
		. ($filt_course ? "\n AND c.id = '$filt_course'" : '' )
		. "\n AND b.course_id != 0"
		;
		$db->setQuery( $query );
		$total = $db->loadResult();

		require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
		$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

		// get the subset (based on limits) of required records
		$query = "SELECT b.*, c.course_name, d.username, d.name, d.email"
		. "\n FROM  #__lms_usergroups as b, #__lms_courses as c LEFT JOIN #__users as d ON c.owner_id = d.id "
		. "\n WHERE b.course_id = c.id"
		. ($filt_course ? "\n AND c.id = '$filt_course'" : '' )
		. "\n AND b.course_id != 0"
		. "\n ORDER BY b.ug_name, c.course_name"
		. "\n LIMIT $pageNav->limitstart, $pageNav->limit"
		;
		$db->setQuery( $query );
		$rows = $db->loadObjectList();
		
		$query = "SELECT b.id,  IF(COUNT(d.id) = 1, d.name, CONCAT(count(d.id), ' "._JLMS_USERS_MEMBER_S."')) AS members"
		. "\n FROM  #__lms_usergroups as b, #__lms_courses as c, #__users as d, #__lms_users_in_groups as e"
		. "\n WHERE b.course_id = c.id AND e.group_id = b.id AND e.user_id = d.id "
		. ($filt_course ? "\n AND c.id = '$filt_course'" : '' )
		. "\n AND b.course_id != 0"
		. "\n GROUP BY b.ug_name"
		;
		$db->setQuery( $query );
		$members = $db->loadObjectList('id');		
						
		for($i=0;$i<count($rows);$i++) 		
		{	
			if( isset($members[$rows[$i]->id]) ) {
				$rows[$i]->members = $members[$rows[$i]->id]->members;			
			} else {
				$rows[$i]->members = '0 '._JLMS_USERS_MEMBER_S;			
			}
		}		
		
		$query = "SELECT IF(COUNT(d.id) = 1, d.name, CONCAT(count(d.id), ' "._JLMS_USERS_MEMBER_S."')) AS members"
		. "\n FROM  #__lms_courses as c, #__users as d, #__lms_users_in_groups as e"
		. "\n WHERE c.id = e.course_id AND e.user_id = d.id "
		. ($filt_course ? "\n AND c.id = '$filt_course'" : '' )		
		. "\n AND e.group_id = 0"					
		;
		$db->setQuery( $query );		
		$members_without_group = $db->loadResult();			
		
		$query = "SELECT COUNT(*)"
		. "\n FROM  #__lms_usertypes as b, #__lms_user_courses as a LEFT JOIN #__users as c ON a.user_id = c.id LEFT JOIN #__lms_courses as d ON a.course_id = d.id "
		. "\n WHERE a.role_id = b.id"
		. ($filt_course ? " AND a.course_id = $filt_course" : '')
		;
		$db->setQuery($query);
		$db->query();
		$total = $db->LoadResult();				
		
		if($total == 1) {
			$query = "SELECT c.name"
			. "\n FROM  #__lms_usertypes as b, #__lms_user_courses as a LEFT JOIN #__users as c ON a.user_id = c.id LEFT JOIN #__lms_courses as d ON a.course_id = d.id "
			. "\n WHERE a.role_id = b.id"
			. ($filt_course ? " AND a.course_id = $filt_course" : '');
			$db->setQuery($query);
			$db->query();
			$name = $db->LoadResult();
			$teacher_assistanse = $name;
		}
		else {
			$teacher_assistanse = $total." "._JLMS_USERS_MEMBER_S;
		}		
		
		$lists = array();
		$query = "SELECT a.id as value, a.course_name as text FROM #__lms_courses as a order by a.course_name";
		$db->setQuery( $query );
		$sf_courses = array();
		$sf_courses[] = mosHTML::makeOption( '0', _JLMS_USERS_SLCT_COURSE_ );
		$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
		$lists['jlms_courses'] = mosHTML::selectList( $sf_courses, 'filt_course', 'class="text_area" size="1" style="width: 266px;" onchange="document.adminForm.submit();"', 'value', 'text', $filt_course );
		$lists['filt_course'] = $filt_course;
	}
	
	joomla_lms_adm_html::JLMS_showClassesList( $rows, $pageNav, $lists, $option, $members_without_group, $teacher_assistanse);
}

function JLMS_editClass( $id, $option ) {
	global $JLMS_CONFIG;
	$db = JFactory::getDBO();
	
	if ($JLMS_CONFIG->get('use_global_groups', 1)) {
		$GLOBALS['jlms_toolbar_id'] = $id;
		$row = new mos_Joomla_LMS_Class( $db );
		// load the row from the db table
		if ($id) $row->load( $id );
		$lists = array();
		$lists['group_forum'] = mosHTML::yesnoRadioList('group_forum', 'class="inputbox"', $row->group_forum);
		$lists['group_chat'] = mosHTML::yesnoRadioList('group_chat', 'class="inputbox"', $row->group_chat);
		$lists['published_start'] = mosHTML::yesnoRadioList('publish_start_date', 'class="inputbox"', isset($row->id)?$row->publish_start_date:0);
		$lists['published_end'] = mosHTML::yesnoRadioList('publish_end_date', 'class="inputbox"', isset($row->id)?$row->publish_end_date:0);

		if ($row->id) {
			$query = "SELECT count(*) FROM #__lms_usergroups WHERE parent_id = ".intval( $row->id );
			$db->SetQuery( $query );
			$group_has_childs = $db->loadResult();
		} else {
			$group_has_childs = false;
		}
		if (!$group_has_childs) {
			//populate list of available parents with gropus which didn't associated with any parent
			
			$query = "SELECT id as value, ug_name as text FROM #__lms_usergroups WHERE course_id = 0"
			. (intval($row->id) ? "\n AND id NOT IN (".intval($row->id).")" : '' )
			//. ($row->id ? "" : "\n AND id NOT IN (SELECT parent_id FROM #__lms_usergroups WHERE parent_id > 0)" )
			. "\n AND parent_id = 0"
			. "\n ORDER by ug_name";
			
			$db->setQuery( $query );
			$ug_names = $db->loadObjectList();			
			
			$javascript = 'onchange="javascript:hide_fields();"';
			$list_ug_names = array();
			$list_ug_names[] = mosHTML::makeOption( '0', _JLMS_USERS_PARENT_GR_ );
			$list_ug_names = array_merge( $list_ug_names, $ug_names );
			$lists['ug_names'] = mosHTML::selectList( $list_ug_names, 'parent_id', 'class="text_area" size="1" style="width:266px" id="parent_id"'.$javascript, 'value', 'text', $row->parent_id );
		} else {
			$lists['ug_names'] = ' - <input type="hidden" name="parent_id" value="0" />';
		}
		
	} else {
		$GLOBALS['jlms_toolbar_id'] = $id;
		$row = new mos_Joomla_LMS_Class( $db );
		// load the row from the db table
		if ($id) $row->load( $id );
		if (!$id) {

		}

		$lists = array();
		$query = "SELECT a.id, a.course_name FROM #__lms_courses as a order by a.course_name";
		$db->setQuery( $query );
		$sf_j_users = $db->loadObjectList();
		$lists['jlms_courses'] = mosHTML::selectList( $sf_j_users, 'course_id', 'class="text_area" size="1"', 'id', 'course_name', $row->course_id );
		$lists['group_forum'] = mosHTML::yesnoRadioList('group_forum', 'class="inputbox"', $row->group_forum);
		$lists['group_chat'] = mosHTML::yesnoRadioList('group_chat', 'class="inputbox"', $row->group_chat);
		$lists['published_start'] = mosHTML::yesnoRadioList('publish_start_date', 'class="inputbox"', isset($row->id)?$row->publish_start_date:0);
		$lists['published_end'] = mosHTML::yesnoRadioList('publish_end_date', 'class="inputbox"', isset($row->id)?$row->publish_end_date:0);
	}
	
	$date_now = date("Y-m-d");
	if($row->start_date == '0000-00-00'){
		$row->start_date = $date_now;	
	}
	if($row->end_date == '0000-00-00'){
		$row->end_date = $date_now;	
	}
	joomla_lms_adm_html::JLMS_editClassHtml( $row, $lists, $option );
}

function JLMS_delClass( $cid, $option ) {
	global $JLMS_CONFIG;
	if (count( $cid )) {
		$db = JFactory::getDBO();
		$cids = implode( ',', $cid );
		$query = "DELETE FROM #__lms_usergroups"
		. "\n WHERE id IN ( $cids )"
		;
		$db->setQuery( $query );
		if (!$db->query()) {
			echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
		if ($JLMS_CONFIG->get('use_global_groups', 1)) {
			$query = "DELETE FROM #__lms_users_in_global_groups WHERE group_id IN ( $cids )";
		} else {
			$query = "UPDATE #__lms_users_in_groups SET group_id = 0 WHERE group_id IN ( $cids )";
		}
		$db->setQuery( $query );
		if (!$db->query()) {
			echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}
	mosRedirect( "index.php?option=$option&task=classes" );
}
function JLMS_saveClass( $option ) {
	$user = JLMSFactory::getUser();
	$db = JFactory::getDBO();

	$is_subgroup = mosGetParam($_REQUEST,'is_subgroup');
	
	$row = new mos_Joomla_LMS_Class( $db );
	
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if ($row->id) {
		unset($row->owner_id);
		unset($row->course_id);
	} else {
		$row->owner_id = $user->get('id');
	}
	$row->id = intval($row->id);
	$row->parent_id = intval($row->parent_id);
	$is_new_group = $row->id ? false : true;
	$row->ug_name = (get_magic_quotes_gpc()) ? stripslashes( $row->ug_name ) : $row->ug_name;
	$row->ug_description = (get_magic_quotes_gpc()) ? stripslashes( $row->ug_description ) : $row->ug_description;
	if (!$row->check()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if (intval($row->id) && $row->parent_id) {
		//check if group doesn't have childs, because parent is configured
		$query = "SELECT count(*) FROM #__lms_usergroups WHERE parent_id = ". intval($row->id);
		$db->SetQuery( $query );
		$our_group_has_childs = $db->loadResult();
		if ($our_group_has_childs) {
			//null the parent, because we didn't have more than 1 sub-level for groups... and this group already has childs
			$row->parent_id = 0;
		}
	}
	
	if (intval($row->id)) {
		if ($row->id == $row->parent_id) {
			//impossible to put group into itself - null the parent
			$row->parent_id = 0;
		}
	}
	
	if (intval($row->parent_id)) {
		//check if parent item is not a child in another group... we don't have more than 1 sub-group level.
		$query = "SELECT parent_id FROM #__lms_usergroups WHERE id = ". intval($row->parent_id);
		$db->SetQuery( $query );
		$our_parent_is_child = $db->loadResult();
		if ($our_parent_is_child) {
			//impossible to put group to another child - null the parent
			$row->parent_id = 0;
		}
		if (intval($row->parent_id)) {
			$query = "SELECT id FROM #__lms_usergroups WHERE id = ". intval($row->parent_id);
			$db->SetQuery( $query );
			$is_our_parent_exists = $db->loadResult();
			if (!$is_our_parent_exists) {
				$row->parent_id = 0;
			}
		}
	}
	
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	//TODO: check if new parent group could access the courses where existent users are participated ... and inform admin about the problem ... if any
	//TODO: check if 'list of allowed groups' for 'restricted category' is correct after editing group and making it child (because only 'parent' groups can be configured for 'restricted categories')
	if (!$is_new_group) {
		if($row->parent_id) {
			//if group is child - update users records and set group_id to parent
			$query = "UPDATE #__lms_users_in_global_groups SET group_id = ".$row->parent_id.", subgroup1_id = ".$row->id." WHERE group_id = ".$row->id;
			$db->SetQuery( $query );
			$db->query();
			//also if parent was changed - update it to the new one
			$query = "UPDATE #__lms_users_in_global_groups SET group_id = ".$row->parent_id.", subgroup1_id = ".$row->id." WHERE subgroup1_id = ".$row->id;
			$db->SetQuery( $query );
			$db->query();
		} else {
			//if subgroup moved to the top - update group_id for it's users and null subgroup1_id
			$query = "UPDATE #__lms_users_in_global_groups SET group_id = ".$row->id.", subgroup1_id = 0 WHERE subgroup1_id = ".$row->id;
			$db->SetQuery( $query );
			$db->query();
		}
	}
	
	$query = "UPDATE `#__lms_forum_details` AS fd SET fd.need_update = 1 
					WHERE fd.board_type IN (SELECT f.id FROM `#__lms_forums` AS f WHERE f.forum_level != 1 AND f.user_level = 2) AND fd.group_id = '".$row->id."'";
	$db->setQuery($query);	
	if( $db->query() ) 
	{					
		$query = "SELECT id FROM #__lms_courses WHERE published = 1 AND add_forum = 1";
		$db->SetQuery($query);
		$courseIds = JLMSDatabaseHelper::loadResultArray();
		
		if(class_exists('JLMS_SMF')){				
			$forum = & JLMS_SMF::getInstance();
		} else {
			$forum = false;
		}
		
		if( !empty($courseIds) ) 
		{					
			foreach( $courseIds AS $courseId ) 
			{					
				if( $forum ){
					$forum->applyChanges( $courseId );
				}				
			}
		}
	}

	global $task;
	if($task == 'apply_class'){
		mosRedirect( "index.php?option=$option&task=editA_class&id=".$row->id );
	} else {
		mosRedirect( "index.php?option=$option&task=classes" );
	}
}

function JLMS_removeClass( &$cid, $option ) {
	global $JLMS_CONFIG;
	$db = JFactory::getDBO();
	if (count( $cid )) {
		$cids = implode( ',', $cid );
		$query = "DELETE FROM #__lms_usergroups"
		. "\n WHERE id IN ( $cids )"
		;
		$db->setQuery( $query );
		if (!$db->query()) {
			echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
		if ($JLMS_CONFIG->get('use_global_groups', 1)) {
			$query = "DELETE FROM #__lms_users_in_global_groups WHERE WHERE group_id IN ( $cids )";
		} else {
			$query = "UPDATE #__lms_users_in_groups SET group_id = 0 WHERE group_id IN ( $cids )";
		}
		$db->setQuery( $query );
		if (!$db->query()) {
			echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}
	mosRedirect( "index.php?option=$option&task=classes" );
}

function JLMS_cancelClass($option) {
	mosRedirect("index.php?option=$option&task=classes");
}
function JLMS_viewClassUsers( $option ) {
	global $JLMS_CONFIG;

	if ($JLMS_CONFIG->get('use_global_groups', 1)) {
		mosRedirect("index.php?option=$option&task=view_class_users_courses");
	}
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	$class_id = intval( $app->getUserStateFromRequest( "class_id{$option}", 'class_id', -1 ) );
	$course_id = intval( $app->getUserStateFromRequest( "filt_course{$option}", 'course_id', 0 ) );
	
	$u_search = strval( $app->getUserStateFromRequest( "u_search{$option}", 'u_search', '' ) );
	
	$class_id2 = true;
	if ($class_id == -1) $class_id2 = false;
	if ($class_id2 && $class_id && $course_id) {
		$query = "SELECT count(*) FROM #__lms_usergroups WHERE id = $class_id AND course_id = $course_id";
		$db->SetQuery( $query );
		$fr = $db->loadResult();
		if (!$fr) {
			$class_id = -1;
			$class_id2 = false;
		}
	}
	// get the total number of records
	$query = "SELECT COUNT(a.user_id)"
	. "\n FROM  #__lms_users_in_groups as a LEFT JOIN #__lms_usergroups as b ON a.group_id = b.id, #__users as c, #__lms_courses as d "
	. "\n WHERE a.user_id = c.id AND a.course_id = d.id"
	. ( $class_id2 ? "\n AND a.group_id = $class_id" : '')
	. ( $course_id ? "\n AND a.course_id = $course_id" : '')
	.(($u_search)?"\n AND (c.username LIKE '%".$u_search."%' OR c.name LIKE '%".$u_search."%' OR c.email LIKE '%".$u_search."%')":"")
	;
	$db->setQuery( $query );
	$total = $db->loadResult();

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	$query = "SELECT a.*, a.user_id as id, b.ug_name, c.username, c.name, c.email, d.course_name"
	. "\n FROM  #__lms_users_in_groups as a LEFT JOIN #__lms_usergroups as b ON a.group_id = b.id, #__users as c, #__lms_courses as d "
	. "\n WHERE a.user_id = c.id AND a.course_id = d.id"
	. ( $class_id2 ? "\n AND a.group_id = $class_id" : '')
	. ( $course_id ? "\n AND a.course_id = $course_id" : '')
	.(($u_search)?"\n AND (c.username LIKE '%".$u_search."%' OR c.name LIKE '%".$u_search."%' OR c.email LIKE '%".$u_search."%')":"")
	. "\n ORDER BY c.username, c.name"
	. "\n LIMIT $pageNav->limitstart, $pageNav->limit"
	;
	$db->setQuery( $query );
	$rows = $db->loadObjectList();
	$lists = array();
	$lists['group_id'] = $class_id;
	$query = "SELECT a.id as value, a.course_name as text FROM #__lms_courses as a order by a.course_name";
	$db->setQuery( $query );
	$sf_courses = array();
	$sf_courses[] = mosHTML::makeOption( '0', _JLMS_USERS_SLCT_COURSE_ );
	$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
	$lists['jlms_courses'] = mosHTML::selectList( $sf_courses, 'course_id', 'class="text_area" size="1" style="width: 266px;" onchange="document.adminForm.submit();"', 'value', 'text', $course_id );
	$lists['jlms_course_id'] = $course_id;
	
	$query = "SELECT a.id as value, a.ug_name as text FROM #__lms_usergroups as a"
	. ( $course_id ? "\n WHERE a.course_id = $course_id" : "\n WHERE a.course_id != 0")
	. "\n order by a.ug_name";
	$db->setQuery( $query );
	$sf_courses = array();
	$sf_courses[] = mosHTML::makeOption( '-1', _JLMS_USERS_SLCT_GR_ );
	$sf_courses[] = mosHTML::makeOption( '0', _JLMS_USERS_USRS_WITHOUT_GR );
	$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
	$lists['jlms_groups'] = mosHTML::selectList( $sf_courses, 'class_id', 'class="text_area" size="1" style="width: 266px;" onchange="document.adminForm.submit();"', 'value', 'text', $class_id );
	$lists['u_search'] = $u_search;
	
	joomla_lms_adm_html::JLMS_ViewClassUsersList( $rows, $lists, $pageNav, $option );
}
function JLMS_viewUsersInGroups ($option) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$group_id = intval(mosGetParam($_REQUEST, 'group_id', 0));
	$subgroup1_id = intval(mosGetParam($_REQUEST, 'subgroup1_id', 0));
	
	if($group_id == 0) {
		$subgroup1_id = 0;
	}

    // save choosen group filter
    $session = JFactory::getSession();
    if(!is_null(mosGetParam($_REQUEST, 'group_id'))){
        $session->set('group_id', $group_id);
    }

	$u_search = strval( $app->getUserStateFromRequest( "u_search{$option}", 'u_search', '' ) );
	
	$query = "SELECT ug.id AS id, u.name AS user_name, u.id AS user_id, g.ug_name AS group_name, g.id AS group_id, u.name, u.email, u.username, ug.subgroup1_id"
	. "\n FROM #__lms_users_in_global_groups AS ug, #__lms_usergroups AS g, #__users AS u"
	. "\n WHERE g.id=ug.group_id AND u.id=ug.user_id"
	. "\n AND ug.group_id NOT IN (SELECT x.group_id FROM #__lms_users_in_global_groups as x, #__lms_usergroups as y WHERE x.subgroup1_id = 0 AND x.group_id = y.id AND y.parent_id > 0 )"
	. "\n ".($group_id ? "AND ug.group_id=$group_id" : '')
	. "\n ".($subgroup1_id ? "AND ug.subgroup1_id=$subgroup1_id" : "")
	.(($u_search)?"\n AND (u.username LIKE '%".$u_search."%' OR u.name LIKE '%".$u_search."%' OR u.email LIKE '%".$u_search."%')":"")
	. "\n ORDER BY u.name";
	$db->setQuery($query);
	//	echo str_replace('#__', 'jos_', $query);
	$db->query();
	
	$total = $db->getNumRows();
	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	$db->setQuery($query, $limitstart, $limit);
	$rows = $db->loadObjectList();
	
	for($i=0;$i<count($rows);$i++) {
		if($rows[$i]->subgroup1_id) {
			$query = "SELECT ug_name FROM #__lms_usergroups as b WHERE id = '".$rows[$i]->subgroup1_id."'";
			$db->setQuery( $query );
			$rows[$i]->subgroup = $db->LoadResult();
		}
	}	
	
	$lists = array();
	$query = "SELECT id AS value, ug_name AS text FROM #__lms_usergroups WHERE course_id=0 AND parent_id = 0 ORDER BY ug_name";
	$db->setQuery($query);
	$tmp = array();
	$tmp[] = mosHTML::makeOption( '0', _JLMS_USERS_ALL_GROUPS_ );
	$tmp = array_merge($tmp, $db->loadObjectList());
	$lists['jlms_groups'] = mosHTML::selectList( $tmp, 'group_id', 'class="text_area" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $session->get('group_id', $group_id));
	$lists['u_search'] = $u_search;
	
	if($group_id) {
		$query = "SELECT id AS value, ug_name AS text FROM #__lms_usergroups WHERE course_id=0 AND parent_id = ".$group_id." ORDER BY ug_name";
		$db->setQuery($query);
		$tmp = array();
		$tmp[] = mosHTML::makeOption( '0', _JLMS_USERS_SUBGROUPS_ );
		$tmp = array_merge($tmp, $db->loadObjectList());
		$lists['jlms_subgroups'] = mosHTML::selectList( $tmp, 'subgroup1_id', 'class="text_area" size="1" style="width: 266px;" onchange="document.adminForm.submit();"', 'value', 'text', $subgroup1_id);
	}
	
	joomla_lms_adm_html::JLMS_ViewUsersInGroups( $rows, $lists, $pageNav, $option, $group_id );
}
function JLMS_addUserToGlobalGroup ( $option ) {
	$db = JFactory::getDBO();
	
	$cid 	= mosGetParam( $_POST, 'cid', mosGetParam( $_GET, 'cid', array(0) ) );
	if (!is_array( $cid )) {
		$cid = array(0);
	}
	
	$rows_groups = array();
	
	if($cid[0]) {
		$query = "SELECT a.*,b.username, b.name, b.email FROM #__lms_users_in_global_groups as a, #__users as b WHERE a.id = '".$cid[0]."' AND a.user_id = b.id";
		$db->SetQuery($query);
		$row = $db->loadObject();
		
		$query = "SELECT a.*, b.ug_name FROM #__lms_users_in_global_groups as a, #__lms_usergroups as b WHERE a.user_id = '".$row->user_id."' AND a.group_id = b.id ORDER BY b.ug_name";
		$db->setQuery( $query );
		$rows_groups = $db->LoadObjectList();
		
		for($i=0;$i<count($rows_groups);$i++) {
			if($rows_groups[$i]->subgroup1_id) {
				$query = "SELECT ug_name FROM #__lms_usergroups as b WHERE id = '".$rows_groups[$i]->subgroup1_id."'";
				$db->setQuery( $query );
				$rows_groups[$i]->subgroup = $db->LoadResult();
			}
		}	
		if(mosGetParam($_REQUEST,'group_id')) {
			$row->group_id = mosGetParam($_REQUEST,'group_id');
		}
	}
	else {
        $row = new stdClass();
		$row->id = 0;
		$row->user_id = 0;
		$row->group_id = 0;
		$row->subgroup1_id = 0;
	}
	
	if(!$row->group_id) {
		$row->group_id = intval(mosGetParam($_REQUEST, 'group_id', 0));
	}
	
	$lists = array();
	/*
	$query = "SELECT id as value, username as text, name, email FROM #__users ORDER BY username";
	$db->SetQuery($query);
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_SLCT_USER_ );
	$pr = $db->loadObjectList();
	$i = 0;
	while ($i < count($pr)) {
		$pr[$i]->text = $pr[$i]->text . " (".$pr[$i]->name.", ".$pr[$i]->email.")";
		$i ++;
	}
	$list_users = array_merge( $list_users, $pr );
	$lists['users'] = mosHTML::selectList( $list_users, 'user_id', 'class="text_area" style="width:300px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', $row->user_id );

	$query = "SELECT id as value, name as text FROM #__users ORDER BY name";
	$db->SetQuery($query);
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_NAME_ );
	$pr = $db->loadObjectList();
	$list_users = array_merge( $list_users, $pr );
	$lists['users_names'] = mosHTML::selectList( $list_users, 'user_name', 'class="text_area" style="width:300px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', $row->user_id );

	$query = "SELECT id as value, email as text FROM #__users ORDER BY email";
	$db->SetQuery($query);
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_OR_EMAIL_ );
	$pr = $db->loadObjectList();
	$list_users = array_merge( $list_users, $pr );
	$lists['users_emails'] = mosHTML::selectList( $list_users, 'user_email', 'class="text_area" style="width:300px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', $row->user_id );
	*/
	
	$lists['jlms_groups'] = '';
	$query = "SELECT a.id as value, a.ug_name as text FROM #__lms_usergroups as a"
	. "\n WHERE a.course_id = 0"
	. "\n AND parent_id = 0"
	. "\n order by a.ug_name";
	$db->setQuery( $query );
	$sf_courses = array();
	$sf_courses[] = mosHTML::makeOption( '0', _JLMS_USERS_SLCT_GR_ );
	$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
	$javasript = 'onchange="document.adminForm.task.value=\'add_stu_to_group\';document.adminForm.submit();"';
	$lists['jlms_groups'] = mosHTML::selectList( $sf_courses, 'group_id', 'class="text_area" style="width:300px;" size="1" '.$javasript, 'value', 'text', $row->group_id );
	
	if($row->group_id) {
		$query = "SELECT id AS value, ug_name AS text FROM #__lms_usergroups WHERE course_id=0 AND parent_id = ".$row->group_id." ORDER BY ug_name";
		$db->setQuery($query);
		$tmp = array();
		$tmp[] = mosHTML::makeOption( '0', _JLMS_USERS_SUBGROUPS_ );
		$tmp = array_merge($tmp, $db->loadObjectList());
		$lists['jlms_subgroups'] = mosHTML::selectList( $tmp, 'subgroup1_id', 'class="text_area" size="1" style="width:300px;"', 'value', 'text', $row->subgroup1_id);
	}
	
	joomla_lms_adm_html::addUserToGlobalGroup( $lists, $option, $row, $rows_groups );
}
function JLMS_saveUserInGlobalGroup ($option) {
	$db = JFactory::getDBO();

	$user_id = mosGetParam($_REQUEST, 'user_id', 0);
	$group_id = mosGetParam($_REQUEST, 'group_id', 0);
	$subgroup1_id = mosGetParam($_REQUEST, 'subgroup1_id', 0);
	$id = mosGetParam($_REQUEST,'id');
	$edit_user = mosGetParam($_REQUEST,'edit_user');
	
	$user_ids = mosGetParam( $_REQUEST, 'user_ids');
	if ($user_ids && $group_id)
	{
		$uids = explode(',',$user_ids);
		if (sizeof($uids))
		{
		$is_j3 = JLMS_J30version();
			foreach ($uids as $user_id)
			{
				$query = "SELECT `name` FROM #__users WHERE `id` =".(int)$user_id;
				$db->setQuery( $query );
				$usr_name = $db->LoadResult();
				
				$query = "SELECT COUNT(*) FROM #__lms_users_in_global_groups WHERE user_id=$user_id AND group_id=$group_id";
				$db->setQuery($query);
				if (!$db->loadResult()) 
				{			
					$query = "INSERT INTO #__lms_users_in_global_groups SET user_id=$user_id, group_id=$group_id, subgroup1_id=$subgroup1_id";
					$db->setQuery($query);
					$db->query();
					$mess = str_replace('{USER}',$usr_name,_JLMS_USERS_MSG_USR_ADDED);						
					JFactory::getApplication()->enqueueMessage($mess);
				} else 
				{
					$mess = str_replace('{USER}',$usr_name,_JLMS_USERS_MSG_USR_ASSOCIATED);						
					if ($is_j3)	JFactory::getApplication()->enqueueMessage($mess, 'warning'); else JError::raiseNotice( 100, $mess );
				}
			}
		}
	} else if ($group_id && $user_id) 
	{
		$query = "SELECT `name` FROM #__users WHERE `id` =".(int)$user_id;
		$db->setQuery( $query );
		$usr_name = $db->LoadResult();
		
		$query = "SELECT COUNT(*) FROM #__lms_users_in_global_groups WHERE user_id=$user_id AND group_id=$group_id";
		$db->setQuery($query);		
		if (!$db->loadResult()) {
			
			if($edit_user) {
				$query = "UPDATE #__lms_users_in_global_groups SET group_id = $group_id, subgroup1_id=$subgroup1_id WHERE id = $id";
			}
			else {
				$query = "INSERT INTO #__lms_users_in_global_groups SET user_id=$user_id, group_id=$group_id, subgroup1_id=$subgroup1_id";
			}
			
			$db->setQuery($query);
			$db->query();

			$msg = str_replace('{USER}',$usr_name,_JLMS_USERS_MSG_USR_ADDED);
		} else {
			$msg = str_replace('{USER}',$usr_name,_JLMS_USERS_MSG_USR_ASSOCIATED);	
		}
	} else {
		$msg = _JLMS_USERS_MSG_UNABLE_TO_ADD_USR;
	}
	mosRedirect("index.php?option=$option&task=view_class_users_groups", $msg);
}
function JLMS_removeUsersFromGlobalGroup ($cid, $option) {
	$db = JFactory::getDBO();
	$query = "DELETE FROM #__lms_users_in_global_groups WHERE id IN (".implode(',', $cid).")";
	$db->setQuery($query);
	$db->query();
	$msg = _JLMS_USERS_MSG_USRS_REMOVED;
	mosRedirect("index.php?option=$option&task=view_class_users_groups", $msg);
}

function JLMS_getFromCIDs( $cids, $type = 'default' ) 
{
	static $instance;
	
	if( !is_array($cids)) return array(); 
	
	if( !isset($instance) ) 
	{
		$res = array();		
		
		$instance['users'] = array();
		$instance['courses'] = array();
		$instance['coursesUsers'] = array();
		
		foreach( $cids AS $cid ) 
		{
			if( $cid ) 
			{
				$arr = call_user_func_array( 'explode', array( '_', $cid ) );
								 
				if( isset( $arr[0] ) && isset( $arr[1] ) ) 
				{					
					$instance['coursesUsers'][$arr[1]][] = $arr[0];
					$instance['usersCourses'][$arr[0]][] = $arr[1];			
					$instance['users'][] = $arr[0];
					$instance['courses'][] = $arr[1];
				}								
				
				$instance['users'] = array_unique($instance['users']);
				$instance['courses'] = array_unique($instance['courses']);
			}						
		}	
	}	
	
	switch( $type ) 
	{
		case 'users':
			return $instance['users']; 
		case 'courses':
			return $instance['courses'];			
		break;
		case 'coursesUsers':
			return $instance['coursesUsers'];			
		break;
		case 'usersCourses':
			return $instance['usersCourses'];			
		break;
		default: 
			return $instance; 
	}	
	
	return $instance; 
}

function JLMS_List_Courses_Student($option){
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');
	
	$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
	
	$user_ids = JLMS_getFromCIDs( $cid, 'users' );
	
	$user_info = '';
			
	if( count($user_ids) == 1 ) 
	{		
		$query = "SELECT *"
		. "\n FROM #__users"
		. "\n WHERE id = '".$user_ids[0]."'"
		;
		$db->setQuery($query);
		$user_info = $db->loadObject();	 
	}
		
	$query = "SELECT a.*, b.id, b.course_name, c.username, c.name, c.email, d.lms_usertype"
	. "\n FROM #__lms_users_in_groups as a"
	. "\n, #__lms_courses as b"
	. "\n, #__users as c"
	. "\n, #__lms_usertypes as d"
	. "\n WHERE a.user_id IN (".implode(',', $user_ids).")"
	. "\n AND a.course_id = b.id"
	. "\n AND a.user_id = c.id"
	. "\n AND a.role_id = d.id"
	. "\n ORDER BY c.username"
	;
	$db->setQuery($query);
	$list_courses_student = $db->loadObjectList();
	
	joomla_lms_adm_html::JLMS_ListCoursesStudent($user_info, $list_courses_student, $option, JLMS_getFromCIDs( $cid, 'usersCourses' ));
}

function JLMS_viewUsersInCourses ($option) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	$course_id  = intval( $app->getUserStateFromRequest( "filt_course{$option}", 'course_id', 0 ) );
	$group_id  = intval( $app->getUserStateFromRequest( "filt_group{$option}", 'group_id', 0 ) );
	
	$u_search = strval( $app->getUserStateFromRequest( "u_search{$option}", 'u_search', '' ) );
	
	if($group_id){
		//TODO: what the fuck?
		$query = "SELECT user_id FROM #__lms_";
		
	}
	
	$query = "SELECT ug.*, ug.user_id as id, u.username, u.name, u.email, c.course_name, d.lms_usertype"
	. "\n FROM  #__lms_users_in_groups as ug, "
	. "\n #__users as u, "
	. "\n #__lms_courses as c, "
	. "\n #__lms_usertypes as d "
	. ( $group_id ? "\n, #__lms_users_in_global_groups as uig " : '')
	. "\n WHERE ug.user_id = u.id AND ug.course_id = c.id AND ug.role_id = d.id"
	. ( $course_id ? "\n AND ug.course_id = $course_id" : '')
	. ( $group_id ? "\n AND uig.group_id = $group_id AND uig.user_id = ug.user_id" : '')
	.(($u_search)?"\n AND (u.username LIKE '%".$u_search."%' OR u.name LIKE '%".$u_search."%' OR u.email LIKE '%".$u_search."%')":"")
	. "\n ORDER BY u.username, u.name"
	;
	$db->setQuery( $query );
	$db->query();
	$total = $db->getNumRows();
	
	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	$db->setQuery( $query, $limitstart, $limit );
	$rows = $db->loadObjectList();	
	
	$lists = array();
	$query = "SELECT a.id as value, a.course_name as text FROM #__lms_courses as a order by a.course_name";
	$db->setQuery( $query );
	$sf_courses = array();
	$sf_courses[] = mosHTML::makeOption( '0', _JLMS_USERS_ALL_COURSES_ );
	$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
	$lists['jlms_courses'] = mosHTML::selectList( $sf_courses, 'course_id', 'class="text_area" size="1" style="width: 266px;" onchange="document.adminForm.submit();"', 'value', 'text', $course_id );
	
	$query = "SELECT a.id as value, a.ug_name as text FROM #__lms_usergroups as a WHERE a.course_id = 0 AND a.parent_id = 0 ORDER BY a.ug_name";
	$db->setQuery( $query );
	$sf_groups = array();
	$sf_groups[] = mosHTML::makeOption( '0', _JLMS_USERS_ALL_GROUPS_ );
	$sf_groups = array_merge( $sf_groups, $db->loadObjectList() );
	$lists['jlms_groups'] = mosHTML::selectList( $sf_groups, 'group_id', 'class="text_area" size="1" style="width: 266px;" onchange="document.adminForm.submit();"', 'value', 'text', $group_id );
	
	$lists['u_search'] = $u_search;
	
	joomla_lms_adm_html::JLMS_ViewUsersInCourses( $rows, $lists, $pageNav, $option );
}
function JLMS_addUserToCourse ( $option ) {
	$db = JFactory::getDBO();
	
	$cid 	= mosGetParam( $_POST, 'cid', mosGetParam( $_GET, 'cid', array(0) ) );
	if (!is_array( $cid )) {
		$cid = array(0);
	}

	$lists = array();
	
	/*
	$query = "SELECT id as value, username as text, name, email FROM #__users ORDER BY username";
	$db->SetQuery($query);
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_SLCT_USER_ );
	$pr = $db->loadObjectList();
	$i = 0;
	while ($i < count($pr)) {
		$pr[$i]->text = $pr[$i]->text . " (".$pr[$i]->name.", ".$pr[$i]->email.")";
		$i ++;
	}
	$list_users = array_merge( $list_users, $pr );
	$lists['users'] = mosHTML::selectList( $list_users, 'user_id', 'class="text_area" style="width:300px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', null );

	$query = "SELECT id as value, name as text FROM #__users ORDER BY name";
	$db->SetQuery($query);
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_NAME_ );
	$pr = $db->loadObjectList();
	$list_users = array_merge( $list_users, $pr );
	$lists['users_names'] = mosHTML::selectList( $list_users, 'user_name', 'class="text_area" style="width:300px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', null );

	$query = "SELECT id as value, email as text FROM #__users ORDER BY email";
	$db->SetQuery($query);
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_OR_EMAIL_ );
	$pr = $db->loadObjectList();
	$list_users = array_merge( $list_users, $pr );
	$lists['users_emails'] = mosHTML::selectList( $list_users, 'user_email', 'class="text_area" style="width:300px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', null );
	*/
	
	$query = "SELECT a.id as value, a.course_name as text FROM #__lms_courses as a order by a.course_name";
	$db->setQuery( $query );
	$sf_courses = array();
	$sf_courses[] = mosHTML::makeOption( '0', _JLMS_USERS_SLCT_COURSE_ );
	$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
	$course_id = mosGetParam($_REQUEST, 'course_id', 0);
	$lists['jlms_courses'] = mosHTML::selectList( $sf_courses, 'course_id', 'class="text_area" size="1" ', 'value', 'text', $course_id );

	$lists['publish_start'] = mosHTML::yesnoRadioList( 'publish_start', 'class="text_area" ', 0);
	$lists['publish_end'] = mosHTML::yesnoRadioList( 'publish_end', 'class="text_area" ', 0);
	
	//Roles
	$JLMS_ACL = JLMSFactory::getACL();
	$default_role = $JLMS_ACL->defaultRole(1);
	$role_id = mosGetParam($_REQUEST, 'role_id', $default_role);
	$query = "SELECT id as value, lms_usertype as text FROM #__lms_usertypes WHERE roletype_id = '1' ORDER BY roletype_id";
	$db->setQuery($query);
	$roles = array();
	$roles[] = mosHTML::makeOption(0, _JLMS_USERS_SLCT_ROLE_);
	$roles = array_merge($roles, $db->loadObjectList());
	$lists['role'] = mosHTML::selectList( $roles, 'role_id', 'class="text_area" size="1"', 'value', 'text', $role_id );
	
	joomla_lms_adm_html::addCourseUser( $lists, $option, 1 );
}
function JLMS_saveUserInCourse ($option) {
	$db = JFactory::getDBO();
	$user_id = mosGetParam($_REQUEST, 'user_id', 0);
	$group_id = mosGetParam($_REQUEST, 'group_id', 0);
	$query = "SELECT COUNT(*) FROM #__lms_users_in_global_groups WHERE user_id=$user_id AND group_id=$group_id";
	$db->setQuery($query);
	if (!$db->loadResult()) {
		$query = "INSERT INTO #__lms_users_in_global_groups SET user_id=$user_id, group_id=$group_id";
		$db->setQuery($query);
		$db->query();
		$msg = _JLMS_USERS_MSG_USR_ADDED;
	} else {
		$msg = _JLMS_USERS_MSG_USR_IN_GROUP;
	}
	mosRedirect("index.php?option=$option&task=view_class_users_groups", $msg);
}
function JLMS_removeUsersFromCourse ($cid, $option) {
	$db = JFactory::getDBO();
	$query = "DELETE FROM #__lms_users_in_global_groups WHERE id IN (".implode(',', $cid).")";
	$db->setQuery($query);
	$db->query();
	$msg = _JLMS_USERS_MSG_USR_REMOVED;
	mosRedirect("index.php?option=$option&task=view_class_users_groups", $msg);
}
function JLMS_viewAssistants( $option ) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	$course_id = intval( $app->getUserStateFromRequest( "filt_course{$option}", 'course_id', 0 ) );
	$query = "SELECT COUNT(a.user_id)"
	. "\n FROM #__lms_user_courses as a"
	. "\n WHERE a.role_id = 4"
	. ( $course_id ? "\n AND a.course_id = $course_id" : '')
	;
	$db->setQuery( $query );
	$total = $db->loadResult();

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	$query = "SELECT a.course_id, a.user_id, a.user_id as id, c.username, c.name, c.email, d.course_name"
	. "\n FROM  #__lms_user_courses as a, #__users as c, #__lms_courses as d "
	. "\n WHERE a.role_id = 4"
	. ( $course_id ? "\n AND a.course_id = $course_id" : '')
	. "\n AND a.user_id = c.id AND a.course_id = d.id"
	. "\n ORDER BY c.username, c.name"
	. "\n LIMIT $pageNav->limitstart, $pageNav->limit"
	;
	$db->setQuery( $query );
	$rows = $db->loadObjectList();
	$lists = array();
	$lists['course_id'] = $course_id;
	$query = "SELECT a.id as value, a.course_name as text FROM #__lms_courses as a order by a.course_name";
	$db->setQuery( $query );
	$sf_courses = array();
	$sf_courses[] = mosHTML::makeOption( '0', _JLMS_USERS_SLCT_COURSE_ );
	$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
	$lists['jlms_courses'] = mosHTML::selectList( $sf_courses, 'course_id', 'class="text_area" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $course_id );
	joomla_lms_adm_html::JLMS_ViewClassAssistants( $rows, $lists, $pageNav, $option );
}
function JLMS_showAddUser( $option, $utype = 1 ) {
	global $JLMS_CONFIG;
	$db = JFactory::getDBO();

	if ($utype != 1 && $utype != 2) { $utype = 1; }

	if ($JLMS_CONFIG->get('use_global_groups', 1) && $utype != 2) {
		$lists = array();
		$query = "SELECT id as value, username as text, name, email FROM #__users ORDER BY username";
		$db->SetQuery($query);
		$list_users = array();
		$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_SLCT_USER_);
		$pr = $db->loadObjectList();
		$i = 0;
		while ($i < count($pr)) {
			$pr[$i]->text = $pr[$i]->text . " (".$pr[$i]->name.", ".$pr[$i]->email.")";
			$i ++;
		}
		$list_users = array_merge( $list_users, $pr );
		$lists['users'] = mosHTML::selectList( $list_users, 'user_id', 'class="text_area" style="width:300px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', null );

		$query = "SELECT id as value, name as text FROM #__users ORDER BY name";
		$db->SetQuery($query);
		$list_users = array();
		$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_NAME_);
		$pr = $db->loadObjectList();
		$list_users = array_merge( $list_users, $pr );
		$lists['users_names'] = mosHTML::selectList( $list_users, 'user_name', 'class="text_area" style="width:300px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', null );

		$query = "SELECT id as value, email as text FROM #__users ORDER BY email";
		$db->SetQuery($query);
		$list_users = array();
		$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_OR_EMAIL_);
		$pr = $db->loadObjectList();
		$list_users = array_merge( $list_users, $pr );
		$lists['users_emails'] = mosHTML::selectList( $list_users, 'user_email', 'class="text_area" style="width:300px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', null );

		$lists['jlms_groups'] = '';
		if ($utype == 1) {
			$query = "SELECT a.id as value, a.ug_name as text FROM #__lms_usergroups as a"
			. "\n WHERE a.course_id = 0"
			. "\n order by a.ug_name";
			$db->setQuery( $query );
			$sf_courses = array();
			$sf_courses[] = mosHTML::makeOption( '-1', _JLMS_USERS_SLCT_GR_ );
			$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
			$lists['jlms_groups'] = mosHTML::selectList( $sf_courses, 'class_id', 'class="text_area" size="1"', 'value', 'text', NULL );
			$lists['publish_start'] = mosHTML::yesnoRadioList( 'publish_start', 'class="text_area" ', 0);
			$lists['publish_end'] = mosHTML::yesnoRadioList( 'publish_end', 'class="text_area" ', 0);
		}
	} else {
		$lists = array();
		$app = JFactory::getApplication('administrator');
		$course_id = intval( $app->getUserStateFromRequest( "filt_course{$option}", 'course_id', 0 ) );
		$query = "SELECT id as value, username as text, name, email FROM #__users ORDER BY username";
		$db->SetQuery($query);
		$list_users = array();
		$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_SLCT_USER_ );
		$pr = $db->loadObjectList();
		$i = 0;
		while ($i < count($pr)) {
			$pr[$i]->text = $pr[$i]->text . " (".$pr[$i]->name.", ".$pr[$i]->email.")";
			$i ++;
		}
		$list_users = array_merge( $list_users, $pr );
		$lists['users'] = mosHTML::selectList( $list_users, 'user_id', 'class="text_area" style="width:300px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', null );

		$query = "SELECT id as value, name as text FROM #__users ORDER BY name";
		$db->SetQuery($query);
		$list_users = array();
		$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_NAME_ );
		$pr = $db->loadObjectList();
		$list_users = array_merge( $list_users, $pr );
		$lists['users_names'] = mosHTML::selectList( $list_users, 'user_name', 'class="text_area" style="width:300px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', null );

		$query = "SELECT id as value, email as text FROM #__users ORDER BY email";
		$db->SetQuery($query);
		$list_users = array();
		$list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_OR_EMAIL_ );
		$pr = $db->loadObjectList();
		$list_users = array_merge( $list_users, $pr );
		$lists['users_emails'] = mosHTML::selectList( $list_users, 'user_email', 'class="text_area" style="width:300px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', null );

		$query = "SELECT a.id as value, a.course_name as text FROM #__lms_courses as a order by a.course_name";
		$db->setQuery( $query );
		$sf_courses = array();
		$sf_courses[] = mosHTML::makeOption( '0', _JLMS_USERS_SLCT_COURSE_ );
		$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
		$lists['jlms_courses'] = mosHTML::selectList( $sf_courses, 'course_id', 'class="text_area" size="1" ', 'value', 'text', $course_id );
		$lists['jlms_groups'] = '';
		if ($utype == 1) {
			$lists['jlms_courses'] = mosHTML::selectList( $sf_courses, 'course_id', 'class="text_area" size="1" onchange="window.location.href=\'index.php?option=com_joomla_lms&task=add_stu&course_id=\'+this.value;"', 'value', 'text', $course_id );

			$query = "SELECT a.id as value, a.ug_name as text FROM #__lms_usergroups as a"
			. ( $course_id ? "\n WHERE a.course_id = $course_id" : ' WHERE a.course_id <> 0')
			. "\n order by a.ug_name";
			$db->setQuery( $query );
			$add_ccc = $db->loadObjectList();
			$sf_courses = array();
			$sf_courses[] = mosHTML::makeOption( '-1', _JLMS_USERS_SLCT_GR_ );
			$sf_courses[] = mosHTML::makeOption( '0', _JLMS_USERS_USRS_WOUT_GR );
			if (!empty($add_ccc)) {
				$sf_courses = array_merge( $sf_courses, $add_ccc);
			}
			$lists['jlms_groups'] = mosHTML::selectList( $sf_courses, 'class_id', 'class="text_area" size="1"', 'value', 'text', NULL );
			$lists['publish_start'] = mosHTML::yesnoRadioList( 'publish_start', 'class="text_area" ', 0);
			$lists['publish_end'] = mosHTML::yesnoRadioList( 'publish_end', 'class="text_area" ', 0);
		}
	}
	$GLOBALS['jlms_toolbar_utype'] = $utype;	
	joomla_lms_adm_html::addCourseUser( $lists, $option, $utype );
}
function JLMS_showEditUser( $option ) {
	$db = JFactory::getDBO();
	$lists = array();
	
	$cid = JRequest::getVar('cid', array() );
	
	$arr = JLMS_getFromCIDs( $cid );
	
	$user_id = 0;
	$course_id = 0;
	
	if( count($arr) ) 
	{
		if( isset($arr['users'][0]) )
			$user_id = $arr['users'][0];
		if( isset($arr['courses'][0]) )
			$course_id = $arr['courses'][0];		
	} 	
	
	if ( $user_id && $course_id ) {
		$query = "SELECT a.*, b.username, b.name, b.email, c.course_name, c.spec_reg, a.enrol_time"//, q.course_question, w.user_answer"
		. "\n FROM #__lms_users_in_groups as a, #__users as b, #__lms_courses as c"
//		. "\n LEFT JOIN #__lms_spec_reg_questions as q ON c.id = q.course_id"
//		. "\n LEFT JOIN #__lms_spec_reg_answers as w ON c.id = w.course_id AND w.user_id = $user_id"
		. "\n WHERE a.user_id = $user_id AND a.user_id = b.id AND a.course_id = $course_id AND a.course_id = c.id";
		$db->SetQuery($query);
		$row = $db->LoadObject();
		if (is_object($row) && isset($row->username)) {
			$lists['user_info'] = $row->username.", ".$row->name." (".$row->email.")";
			$lists['course_info'] = $row->course_name;
			$lists['jlms_groups'] = '';
	
			$query = "SELECT a.id as value, a.ug_name as text FROM #__lms_usergroups as a"
			. ( $course_id ? "\n WHERE a.course_id = $course_id" : '')
			. "\n order by a.ug_name";
			$db->setQuery( $query );
			$sf_courses = array();
			$sf_courses[] = mosHTML::makeOption( '0', _JLMS_USERS_USRS_WOUT_GR );
			$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
			$lists['jlms_groups'] = mosHTML::selectList( $sf_courses, 'class_id', 'class="text_area" size="1"', 'value', 'text', $row->group_id );
	
			$lists['publish_start'] = mosHTML::yesnoRadioList( 'publish_start', 'class="text_area" ', $row->publish_start);
			$lists['publish_end'] = mosHTML::yesnoRadioList( 'publish_end', 'class="text_area" ', $row->publish_end);
	
			if ( isset($row->spec_reg) && $row->spec_reg ) {
				$row->spec_reg = 1;
				$query = "SELECT role_id, id, is_optional, course_question FROM #__lms_spec_reg_questions WHERE course_id = $course_id AND (role_id = 0) ORDER BY role_id DESC, ordering";
				$db->SetQuery( $query );
				$sr_quests = $db->LoadObjectList();
				$sr_answs = array();
				if (!empty($sr_quests)) {
					$sr_ids = array();
					$sr_qq = array();
					foreach ($sr_quests as $srq) {
						$sr_ids[] = $srq->id;
						$sr_qq[] = $srq;
					}
					if (!empty($sr_ids)) {
						$sr_idss = implode(',',$sr_ids);
						$query = "SELECT a.*, b.course_question FROM #__lms_spec_reg_answers as a, #__lms_spec_reg_questions as b WHERE a.course_id = $course_id AND a.user_id = $user_id AND a.role_id = 0 AND a.quest_id IN ($sr_idss) AND a.quest_id = b.id ORDER BY b.ordering";
						$db->SetQuery( $query );
						$sr_answs = $db->LoadObjectList();
						for ($i1 = 0; $i1 < count($sr_quests); $i1 ++) {
							$sr_quests[$i1]->user_answer = '';
							if (count($sr_answs)) {
								for ($j1 = 0; $j1 < count($sr_answs); $j1 ++) {
									if ($sr_answs[$j1]->quest_id == $sr_quests[$i1]->id) {
										$sr_quests[$i1]->user_answer = $sr_answs[$j1]->user_answer;
										break;
									}
								}
							}
						}
					}
				}
				$row->spec_answers = $sr_quests;
			}
	
			//Roles
			$JLMS_ACL = JLMSFactory::getACL();
			$default_role = $JLMS_ACL->defaultRole(1);
			$role_id = isset($row->role_id) ? $row->role_id : $default_role;
			$query = "SELECT id as value, lms_usertype as text FROM #__lms_usertypes WHERE roletype_id = '1' ORDER BY roletype_id";
			$db->setQuery($query);
			$roles = array();
			$roles[] = mosHTML::makeOption(0, _JLMS_USERS_SLCT_ROLE_);
			$roles = array_merge($roles, $db->loadObjectList());
			$lists['role'] = mosHTML::selectList( $roles, 'role_id', 'class="text_area" size="1"', 'value', 'text', $role_id );
							
			$row->enrol_time = JLMS_addJoomlaLocale( 'Y-m-d H:i' , $row->enrol_time );
			
			joomla_lms_adm_html::editCourseUser( $row, $lists, $option );
		} else {
			mosRedirect( "index.php?option=$option&task=view_class_users" );
		}
	} else {
		$link = "index.php?option=$option&task=view_class_users";
		mosRedirect( $link );
	}
}
function JLMS_EditUserSave( $option ) {
	
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$db = JFactory::getDBO();

	$user_id = intval(mosGetParam($_REQUEST, 'user_id', 0));
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$group_id = intval(mosGetParam($_REQUEST, 'class_id', 0));
	$JLMS_ACL = JLMSFactory::getACL();
	$default_role = $JLMS_ACL->defaultRole(1);
	$role_id = intval(mosGetParam($_REQUEST, 'role_id', $default_role));
		
	if ($user_id && $course_id) {
		$p_s = intval(mosGetParam($_REQUEST, 'publish_start', 0));
		$d_s = strval(mosGetParam($_REQUEST, 'start_date', ''));
		$p_e = intval(mosGetParam($_REQUEST, 'publish_end', 0));
		$d_e = strval(mosGetParam($_REQUEST, 'end_date', ''));
		$e_t = strval(mosGetParam($_REQUEST, 'enrol_time', ''));
		
		if ($p_s) { $p_s = 1; } else { $p_s = 0;$d_s = '0000-00-00'; }
		if ($p_e) { $p_e = 1; } else { $p_e = 0;$d_e = '0000-00-00'; }
				
		$enrol_time = JLMS_addJoomlaLocale( 'Y-m-d H:i' , mosGetParam($_REQUEST, 'enrol_time', ''), 'todb' );
							
		$query = "UPDATE #__lms_users_in_groups SET group_id = $group_id, publish_start = $p_s, start_date = '".$d_s."', publish_end = $p_e, end_date = '".$d_e."', role_id = '".$role_id."', enrol_time = '".$enrol_time."'"
		. "\n WHERE user_id = $user_id AND course_id = $course_id";
		$db->SetQuery( $query );
		$db->query();
		
		$is_spec_reg = intval(mosGetParam($_REQUEST, 'spec_reg', 0));
		if ($is_spec_reg) {
			$user_answers = mosGetParam($_REQUEST, 'user_answer', array());
			$spec_reg_quest_ids = mosGetParam($_REQUEST, 'spec_reg_quest_id', array());
			if (count($user_answers) && count($user_answers) == count($spec_reg_quest_ids)) {
				$i = 0;
				while ($i < count($user_answers)) {
					$cur_quest_id = intval($spec_reg_quest_ids[$i]);
					$query = "SELECT c.course_question FROM #__lms_spec_reg_questions as c "
					. "\n WHERE c.course_id = $course_id AND c.id = $cur_quest_id";
					$db->SetQuery( $query );
					if ($db->LoadResult()) {
						$cur_answer = strval($user_answers[$i]);
						if ($cur_answer) {
							$query = "SELECT id, user_answer FROM #__lms_spec_reg_answers WHERE course_id = $course_id AND user_id = $user_id AND quest_id = $cur_quest_id AND role_id = 0";
							$db->SetQuery( $query );
							$old_answer = $db->LoadObject();
							if (is_object($old_answer)) {
								$query = "UPDATE #__lms_spec_reg_answers SET user_answer = ".$db->Quote($cur_answer)." WHERE course_id = $course_id AND user_id = $user_id AND quest_id = $cur_quest_id AND role_id = 0";
								$db->SetQuery( $query );
								$db->query();
							} else {
								$query = "INSERT INTO #__lms_spec_reg_answers (course_id, user_id, role_id, quest_id, user_answer)"
								. "\n VALUES ( $course_id, ".$user_id.", 0, $cur_quest_id, ".$db->Quote($cur_answer).")";
								$db->SetQuery( $query );
								$db->query();
							}
						} else {
							$query = "DELETE FROM #__lms_spec_reg_answers WHERE course_id = $course_id AND user_id = $user_id AND quest_id = $cur_quest_id AND role_id = 0";
							$db->SetQuery( $query );
							$db->query();
						}
					}
					$i ++;
				}
			}
		}
	}
	
	if ( $JLMS_CONFIG->get('use_global_groups', 1) ) {
		$link = "index.php?option=$option&task=view_class_users_courses";
	} else {
		$link = "index.php?option=$option&task=view_class_users";
	}
	
	mosRedirect( $link );
}

function JLMS_addUserToGroup( $option ) {
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$db = JFactory::getDBO();

	$msg = '';
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$group_id = intval(mosGetParam($_REQUEST, 'class_id', 0));
	
	$JLMS_ACL = JLMSFactory::getACL();
	$default_role = $JLMS_ACL->defaultRole(1);
	$role_id = intval(mosGetParam($_REQUEST, 'role_id', $default_role));
	
	$user_id = intval(mosGetParam($_REQUEST, 'user_id', 0));
	$utype = intval(mosGetParam($_REQUEST, 'utype', 1));
	if ($utype != 1 && $utype != 2) { $utype = 1; }
	$teacher_comment = strval(mosGetParam($_REQUEST, 'teacher_comment', ''));
	
	$p_s = intval(mosGetParam($_REQUEST, 'publish_start', 0));
	$d_s = strval(mosGetParam($_REQUEST, 'start_date', ''));
	$p_e = intval(mosGetParam($_REQUEST, 'publish_end', 0));
	$d_e = strval(mosGetParam($_REQUEST, 'end_date', ''));
	if ($p_s) { $p_s = 1; } else { $p_s = 0;$d_s = '0000-00-00'; }
	if ($p_e) { $p_e = 1; } else { $p_e = 0;$d_e = '0000-00-00'; }
	
	$user_ids = mosGetParam( $_REQUEST, 'user_ids');
	if ($user_ids && $course_id)
	{	global $license_lms_users;
		$uids = explode(',',$user_ids);
		if (sizeof($uids))
		{
			foreach ($uids as $user_id)
			{
				$do_add = false;

				$query = "SELECT count(*) FROM #__lms_users_in_groups WHERE course_id = '".$course_id."' AND user_id = '".$user_id."'";
				$db->SetQuery( $query );
				$c = $db->LoadResult();
				
				$query = "SELECT count(*) FROM #__lms_user_courses WHERE course_id = '".$course_id."' AND user_id = '".$user_id."'";
				$db->SetQuery( $query );
				$c2 = $db->LoadResult();
				if ($utype == 1) 
				{				
					if ((int)$c == 0) 
					{
						if ($license_lms_users) 
						{
							$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
							$db->SetQuery( $query );
							$total_students = $db->LoadResult();
							if (intval($total_students) < intval($license_lms_users)) 
							{
								$do_add = true;
							}
							if (!$do_add) 
							{
								$query = "SELECT count(*) FROM #__lms_users_in_groups WHERE user_id = '".$user_id."'";
								$db->SetQuery( $query );
								if ($db->LoadResult()) {
									$do_add = true;
								}
							}
						} else 
							{
							$do_add = true;
							}
						if ($do_add) 
						{	
							$query = "INSERT INTO #__lms_users_in_groups"
								. "\n (course_id, group_id, user_id, role_id, teacher_comment, publish_start, start_date, publish_end, end_date, enrol_time)"
								. "\n VALUES"
								. "\n ('".$course_id."', '".$group_id."', '".$user_id."', '".$role_id."', '".$teacher_comment."', $p_s, '".$d_s."', $p_e, '".$d_e."', '".JLMS_gmdate()."')"
								;
								$db->SetQuery( $query );
								if( $db->query() ) 
								{
									//send email to import user						
									$course = new stdClass();
									$course->course_alias = '';
									$course->course_name = '';			
																		
									$query = "SELECT course_name, name_alias FROM #__lms_courses WHERE id = '".$course_id."'";
									$db->setQuery( $query );
									$course = $db->loadObject();								
									
									$user_group = new stdClass(); 
									$user_group->ug_name = '';	
																		
									$query = "SELECT ug_name  FROM #__lms_usergroups WHERE id = '".$group_id."'";
									$db->setQuery( $query );
									$user_group = $db->loadObject();
															
									$params['user_id'] = $user_id;	
									$params['course_id'] = $course_id;	
									
									$user = new stdClass();
									$user->email = '';
									$user->name = '';
									$user->username = '';						
									
									$query = "SELECT email, name, username FROM #__users WHERE id = '".$user_id."'";
									$db->setQuery( $query );
									$user = $db->loadObject();								
									
									$params['markers']['{email}'] = $user->email;											
									$params['markers']['{name}'] = $user->name;
									$params['markers']['{username}'] = $user->username;
									$params['markers']['{coursename}'] = $course->course_name;//( $course->course_alias )?$course->course_alias:$course->course_name;
									
									if( $user_group )
										$params['markers']['{groupname}'] = $user_group->ug_name;
									
									$Itemid = $JLMS_CONFIG->getItemid();						
									$params['markers']['{courselink}'] = JURI::root()."index.php?option=com_joomla_lms&Itemid=$Itemid&task=details_course&id=$course_id";					
									
									$params['action_name'] = 'OnEnrolmentInCourseByJoomlaAdmin';				
																	
									$_JLMS_PLUGINS->loadBotGroup('emails');
									$_JLMS_PLUGINS->loadBotGroup('system');
									$params['course_ids'] = array($course_id);
									$plugin_result_array = $_JLMS_PLUGINS->trigger('OnEnrolmentInCourseByJoomlaAdmin', array (& $params));
								}

								$user_info = new stdClass();
								$user_info->user_id = $user_id;
								$user_info->group_id = $group_id;
								$user_info->course_id = $course_id;
								$_JLMS_PLUGINS->loadBotGroup('user');
								$_JLMS_PLUGINS->trigger('onCourseJoin', array($user_info));
							} else {
								$msg = _JLMS_USERS_MSG_USR_L_EXCEEDED;
							}
					}
				}
				elseif ($utype == 2) {
					if ((int)$c2 == 0) {
						$query = "INSERT INTO #__lms_user_courses (user_id, course_id, role_id) VALUES('".$user_id."', '".$course_id."', '4')";
						$db->SetQuery( $query );
						$db->query();
					}
				}
				
			} //end foreach
		}
	}
	/*OLD CODE AT 11.06.2013 not need in future */
	if ( $course_id && $user_id && !$user_ids )
	{	
		$query = "SELECT count(*) FROM #__lms_users_in_groups WHERE course_id = '".$course_id."' AND user_id = '".$user_id."'";
		$db->SetQuery( $query );
		$c = $db->LoadResult();
		
		$query = "SELECT count(*) FROM #__lms_user_courses WHERE course_id = '".$course_id."' AND user_id = '".$user_id."'";
		$db->SetQuery( $query );
		$c2 = $db->LoadResult();
	
		if ($utype == 1) {
			if (intval($c) == 0) {				
				$do_add = false;
				global $license_lms_users;
				if ($license_lms_users) {
					$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
					$db->SetQuery( $query );
					$total_students = $db->LoadResult();
					if (intval($total_students) < intval($license_lms_users)) {
						$do_add = true;
					}
					if (!$do_add) {
						$query = "SELECT count(*) FROM #__lms_users_in_groups WHERE user_id = '".$user_id."'";
						$db->SetQuery( $query );
						if ($db->LoadResult()) {
							$do_add = true;
						}
					}
				} else {
					$do_add = true;
				}
				if ($do_add) {
					$query = "INSERT INTO #__lms_users_in_groups"
					. "\n (course_id, group_id, user_id, role_id, teacher_comment, publish_start, start_date, publish_end, end_date, enrol_time)"
					. "\n VALUES"
					. "\n ('".$course_id."', '".$group_id."', '".$user_id."', '".$role_id."', '".$teacher_comment."', $p_s, '".$d_s."', $p_e, '".$d_e."', '".JLMS_gmdate()."')"
					;
					$db->SetQuery( $query );
					if( $db->query() ) 
					{
						//send email to import user						
						$course = new stdClass();
						$course->course_alias = '';
						$course->course_name = '';			
															
						$query = "SELECT course_name, name_alias FROM #__lms_courses WHERE id = '".$course_id."'";
						$db->setQuery( $query );
						$course = $db->loadObject();								
						
						$user_group = new stdClass(); 
						$user_group->ug_name = '';	
															
						$query = "SELECT ug_name  FROM #__lms_usergroups WHERE id = '".$group_id."'";
						$db->setQuery( $query );
						$user_group = $db->loadObject();
												
						$params['user_id'] = $user_id;	
						$params['course_id'] = $course_id;	
						
						$user = new stdClass();
						$user->email = '';
						$user->name = '';
						$user->username = '';						
						
						$query = "SELECT email, name, username FROM #__users WHERE id = '".$user_id."'";
						$db->setQuery( $query );
						$user = $db->loadObject();								
						
						$params['markers']['{email}'] = $user->email;											
						$params['markers']['{name}'] = $user->name;
						$params['markers']['{username}'] = $user->username;
						$params['markers']['{coursename}'] = $course->course_name;//( $course->course_alias )?$course->course_alias:$course->course_name;
						
						if( $user_group )
							$params['markers']['{groupname}'] = $user_group->ug_name;
						
						$Itemid = $JLMS_CONFIG->getItemid();						
						$params['markers']['{courselink}'] = JURI::root()."index.php?option=com_joomla_lms&Itemid=$Itemid&task=details_course&id=$course_id";					
						
						$params['action_name'] = 'OnEnrolmentInCourseByJoomlaAdmin';				
														
						$_JLMS_PLUGINS->loadBotGroup('emails');
						$_JLMS_PLUGINS->loadBotGroup('system');
						$params['course_ids'] = array($course_id);
						$plugin_result_array = $_JLMS_PLUGINS->trigger('OnEnrolmentInCourseByJoomlaAdmin', array (& $params));
					}

					$user_info = new stdClass();
					$user_info->user_id = $user_id;
					$user_info->group_id = $group_id;
					$user_info->course_id = $course_id;
					$_JLMS_PLUGINS->loadBotGroup('user');
					$_JLMS_PLUGINS->trigger('onCourseJoin', array($user_info));
				} else {
					$msg = _JLMS_USERS_MSG_USR_L_EXCEEDED;
				}
			}
		} elseif ($utype == 2) {
			if (intval($c2) == 0) {
				$query = "INSERT INTO #__lms_user_courses (user_id, course_id, role_id) VALUES('".$user_id."', '".$course_id."', '4')";
				$db->SetQuery( $query );
				$db->query();
			}
		}
	}
	/*END OLD CODE*/
	
	$link = "index.php?option=$option&task=view_class_users";
		
	if ($utype == 2) {
		$link = "index.php?option=$option&task=view_assistants";
	} else {
		if ( $JLMS_CONFIG->get('use_global_groups', 1) ) 
		{			
			$link = "index.php?option=$option&task=view_class_users_courses";
		}
	}
	
	mosRedirect( $link, $msg );
}
function JLMS_removeAssistant( $user_id, $option ) {
	$db = JFactory::getDBO();
	$course_id = intval(mosGetParam($_REQUEST, 'assistant_course', 0));
	#$user_id = intval(mosGetParam($_REQUEST, 'user_id', 0));
	$query = "DELETE FROM #__lms_user_courses WHERE user_id = '".$user_id."' AND course_id = '".$course_id."' AND role_id = 4";
	$db->SetQuery( $query );
	$db->query();
	mosRedirect( "index.php?option=$option&task=view_assistants" );
}
function JLMS_removeStudent( $option ) {
	$db = JFactory::getDBO();
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$_JLMS_PLUGINS = JLMSFactory::getPlugins();
	
	$cids = JRequest::getVar( 'cid', array() );
	$group_id = JRequest::getInt( 'group_id', 0 );
	
	$arrCids = JLMS_getFromCIDs( $cids, 'coursesUsers' );		
	
	require_once(_JOOMLMS_FRONT_HOME."/includes/lms_del_operations.php");
		
	foreach( $arrCids AS $course_id => $del_ids ) 
	{
		JLMS_DelOp_deleteCourseStudents( $course_id, $group_id, $del_ids );

		$query = "DELETE FROM #__lms_users_in_groups"
		. "\n WHERE course_id = '".$course_id."' AND user_id IN (".implode( ',', $del_ids ).")"
		;
		$db->setQuery( $query );
		$db->query();

		//24.01.2014 Add waiting list update after remove student
		//section of plugin actions
				$_JLMS_PLUGINS->loadBotGroup('course');
				$users_joined = $_JLMS_PLUGINS->trigger('onCourseExpulsion', array($course_id));

				if (isset($users_joined[0]) && count($users_joined[0])) {
					$users_joined = $users_joined[0];
					$users_info = array();
					foreach ($users_joined as $user) {
						$tmp = new stdClass();
						$tmp->user_id = $user->user_id;
						$tmp->course_id = $user->course_id;
						$tmp->teacher_id = $my->id;
						$users_info[] = $tmp;
					}

					if (count($users_info)) {
						$_JLMS_PLUGINS->loadBotGroup('user');
						$_JLMS_PLUGINS->loadBotGroup('notifications');
						$_JLMS_PLUGINS->trigger('onCourseJoin', array($users_info));
					}
				}
		//end plugin actions
	}
	
	if( count($arrCids) ) 
	{
		$_JLMS_PLUGINS->loadBotGroup('system');
		
		$plg_params['removedStuData'] = JLMS_getFromCIDs( $cids );
		$lists['plugin_return'] = $_JLMS_PLUGINS->trigger('onRemoveUserCourse', array(&$plg_params));
	}

	if ( $JLMS_CONFIG->get('use_global_groups', 1) ) {
		mosRedirect( "index.php?option=$option&task=view_class_users_courses" );
	} else {
		mosRedirect( "index.php?option=$option&task=view_class_users" );
	}
}

function JLMS_viewClass($class_id, $option) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	if (!$class_id) {
		$class_id = intval( $app->getUserStateFromRequest( "class_id{$option}", 'class_id', 0 ) );
	}


	$query = "SELECT a.course_id, a.user_id, b.ug_name, c.username, c.name, c.email"
	. "\n FROM  #__lms_users_in_groups as a, #__lms_usergroups as b, #__users as c"
	. "\n WHERE a.group_id = b.id AND b.id = '".$class_id."' AND a.user_id = c.id"
	. "\n ORDER BY c.username, c.name"
	;
	$db->setQuery( $query );
	$rows = $db->loadObjectList();
	$i = 0;
	while ($i < count($rows)) {
		$rows[$i]->user_name = $rows[$i]->name.' ('.$rows[$i]->username.')';
		$i ++;
	}
	$lists = array();
	$lists['class_users'] = mosHTML::selectList( $rows, 'class_users[]', 'class="text_area" size="20" style="width:80%" multiple', 'user_id', 'user_name', '0' );
	$course_users = array();
	$lists['group_name'] = isset($rows[0]->ug_name)?$rows[0]->ug_name:'';
	$lists['course_id'] = isset($rows[0]->course_id)?$rows[0]->course_id:'';
	$lists['group_id'] = $class_id;
	$user_ids = array(0);
	if (isset($rows[0]->course_id)) {
		$course_id = $rows[0]->course_id;
		$i = 0;
		while ($i < count($rows)) {
			$user_ids[] = $rows[$i]->user_id;
			$i ++;
		}
		$rr = implode(',',$user_ids);
		$query = "SELECT a.course_id, a.user_id, b.ug_name, c.username, c.name, c.email"
		. "\n FROM  #__lms_users_in_groups as a LEFT JOIN #__lms_usergroups as b ON a.group_id = b.id, #__users as c"
		. "\n WHERE a.course_id = '".$course_id."' AND a.user_id = c.id AND a.user_id NOT IN ($rr)"
		. "\n ORDER BY c.username, c.name"
		;
		$db->setQuery( $query );
		$course_users = $db->loadObjectList();
		$i = 0;
		while ($i < count($course_users)) {
			$course_users[$i]->user_name = $course_users[$i]->name.' ('.$course_users[$i]->username.') - '.$course_users[$i]->ug_name;
			$user_ids[] = $course_users[$i]->user_id;
			$i ++;
		}
	}
	$rrr = implode(',',$user_ids);
	$query = "SELECT id as user_id, username, name, email FROM #__users WHERE id NOT IN ($rrr)";
	$db->setQuery( $query );
	$joomla_users = $db->loadObjectList();
	$i = 0;
	while ($i < count($joomla_users)) {
		$joomla_users[$i]->user_name = $joomla_users[$i]->name.' ('.$joomla_users[$i]->username.')';
		$i ++;
	}
	$lists['joomla_users'] = mosHTML::selectList( $joomla_users, 'joomla_users[]', 'class="text_area" size="20" style="width:80%" multiple', 'user_id', 'user_name', '0' );
	$lists['course_users'] = mosHTML::selectList( $course_users, 'course_users[]', 'class="text_area" size="20" style="width:80%" multiple', 'user_id', 'user_name', '0' );
	joomla_lms_adm_html::JLMS_ViewClassUsers( $rows, $lists, $option );
}
function JLMS_removeFromClass( $option ) {
	$db = JFactory::getDBO();
	$cid 	= mosGetParam( $_REQUEST, 'class_users', array(0) );
	$group_id = intval( mosGetParam( $_REQUEST, 'group_id', 0 ) );
	$cid = implode(',',$cid);
	$query = "UPDATE #__lms_users_in_groups SET group_id = 0 WHERE user_id IN ($cid) AND group_id = '".$group_id."'";
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
	}
	mosRedirect("index.php?option=$option&task=view_class&class_id=$group_id");
}
function JLMS_removeFromCourse( $option ) {
	$db = JFactory::getDBO();
	$cid 	= mosGetParam( $_REQUEST, 'cid', array(0) );
	$group_id = intval( mosGetParam( $_REQUEST, 'group_id', 0 ) );
	$cid = implode(',',$cid);
	$query = "DELETE FROM #__lms_users_in_groups WHERE user_id IN ($cid) AND group_id = '".$group_id."'";
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
	}
	mosRedirect("index.php?option=$option&task=view_class_users&class_id=$group_id");
}
function JLMS_addToClass( $option ) {
	$db = JFactory::getDBO();
	$cid 	= mosGetParam( $_REQUEST, 'course_users', array(0) );
	$course_id = intval( mosGetParam( $_REQUEST, 'course_id', 0 ) );
	$group_id = intval( mosGetParam( $_REQUEST, 'group_id', 0 ) );
	$cid = implode(',',$cid);
	$query = "UPDATE #__lms_users_in_groups SET group_id = '".$group_id."' WHERE user_id IN ($cid) AND course_id = '".$course_id."'";
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
	}
	mosRedirect("index.php?option=$option&task=view_class&class_id=$group_id");
}
function JLMS_addnewToClass( $option ) {
	$db = JFactory::getDBO();

	$cid 	= mosGetParam( $_REQUEST, 'joomla_users', array(0) );
	$course_id = intval( mosGetParam( $_REQUEST, 'course_id', 0 ) );
	$group_id = intval( mosGetParam( $_REQUEST, 'group_id', 0 ) );
	$cid_str = implode(',',$cid);
	$query = "SELECT user_id FROM #__lms_users_in_groups WHERE user_id IN ($cid_str) AND course_id = '".$course_id."'";
	$db->SetQuery( $query );
	$uids = JLMSDatabaseHelper::loadResultArray();
	if (count($uids)) {
		$cid = array_diff($cid,$uids);
	}
	if (count($cid)) {
		$cid_str = implode(',',$cid);
		$query = "SELECT user_id FROM #__user_courses WHERE course_id = '".$course_id."'";
		$db->setQuery( $query );
		$uids = JLMSDatabaseHelper::loadResultArray();
		if (count($uids)) {
			$cid = array_diff($cid,$uids);
		}
		if (count($cid)) {
			global $license_lms_users;
			foreach($cid as $ins_id) {
				$do_add = false;
				if ($license_lms_users) {
					$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
					$db->SetQuery( $query );
					$total_students = $db->LoadResult();
					if (intval($total_students) < intval($license_lms_users)) {
						$do_add = true;
					}
					if (!$do_add) {
						$query = "SELECT count(*) FROM #__lms_users_in_groups WHERE user_id = '".$ins_id."'";
						$db->SetQuery( $query );
						if ($db->LoadResult()) {
							$do_add = true;
						}
					}
				} else {
					$do_add = true;
				}
				if ($do_add) {
					$query = "INSERT INTO #__lms_users_in_groups (course_id, group_id, user_id, enrol_time) VALUES "
					. "\n ($course_id, ".$group_id.", ".$ins_id.", '".JLMS_gmdate()."')";
					$db->setQuery( $query );
					if (!$db->query()) {
						echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
					}
				} else {
					echo "<script> alert('"._JLMS_USERS_MSG_USR_L_EXCEEDED."'); window.history.go(-1); </script>\n";
					die;
				}
			}
		}
	}
	mosRedirect("index.php?option=$option&task=view_class&class_id=$group_id");
}
##########################################################################
###	--- ---   	COURSES	 	--- --- ###
##########################################################################
function JLMS_ListCourses( $option ) {
	global $JLMS_CONFIG;
	$app = JFactory::getApplication('administrator');
	$db = JFactory::getDBO();

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	$filt_cat = intval( $app->getUserStateFromRequest( "filt_cat{$option}", 'filt_cat', 0 ) );
	
	//FLMS multicat
	if(!defined('_JLMS_COURSES_COURSES_GROUPS')){
		define('_JLMS_COURSES_COURSES_GROUPS', _JLMS_CRSS_COURSE_CATS.':');
	}
	$levels = array();
	if ($JLMS_CONFIG->get('multicat_use', 0)){
		$lists = array();
		$query = "SELECT * FROM #__lms_course_cats_config ORDER BY id";
		$db->setQuery($query);
		$levels = $db->loadObjectList();
		if(count($levels) == 0){
			for($i=0;$i<5;$i++){
				$levels[$i] = new stdClass();
				if($i>0){
					$levels[$i]->cat_name = _JLMS_COURSES_COURSES_GROUPS;	
				} else {
					$levels[$i]->cat_name = _JLMS_COURSES_COURSES_GROUPS;
				}
			}
		} else {
			for($i=0;$i<count($levels);$i++){
				if($levels[$i]->cat_name == ''){
					$levels[$i]->cat_name = _JLMS_COURSES_COURSES_GROUPS;	
				}
			}
		}
		$lists['levels'] = $levels;

		$level_id = array();
		$old_level_id = array();
		for($i=0;$i<count($levels);$i++){
			if($i == 0){
				$level_id[$i] = intval( $app->getUserStateFromRequest( "filter_id_".$i."{$option}", "filter_id_".$i."", 0 ) );
				$_REQUEST['filter_id_'.$i] = $level_id[$i];
				$parent_id[$i] = 0;
			} else {
				$level_id[$i] = intval( $app->getUserStateFromRequest( "filter_id_".$i."{$option}", "filter_id_".$i."", 0 ) );
				$_REQUEST['filter_id_'.$i] = $level_id[$i];
				$parent_id[$i] = $level_id[$i-1];
			}
			$query = "SELECT count(id) FROM `#__lms_course_cats` WHERE parent = '".$parent_id[$i]."' ORDER BY c_category";
			$db->setQuery($query);
			$groups = $db->loadResult();
			if($groups==0){
				$level_id[$i] = $_REQUEST['filter_id_'.$i.''] = 0;	
			}
		}
		
		for($i=0;$i<count($levels);$i++){
			if($i > 0 && $level_id[$i - 1] == 0){
				$level_id[$i] = $_REQUEST['filter_id_'.$i.''] = 0;
				$_REQUEST['filter_id_'.$i] = $level_id[$i];
				$parent_id[$i] = 0;
			} elseif($i == 0 && $level_id[$i] == 0) {
				$level_id[$i] = $_REQUEST['filter_id_'.$i.''] = 0;
				$_REQUEST['filter_id_'.$i] = $level_id[$i];
				$parent_id[$i] = 0;
			}
		}
		
		$javascript = 'onclick="javascript:read_filter();" onchange="javascript:write_filter();document.adminForm.task.value=\'courses\';document.adminForm.submit();"';
		for($i=0;$i<count($levels);$i++) {
			if( $parent_id[$i] == 0) {
				$query = "SELECT * FROM `#__lms_course_cats` WHERE `parent` = '0'";
				$query .= "\n ORDER BY `c_category`";
			}
			else {
				$query = "SELECT * FROM `#__lms_course_cats` WHERE parent = '".$parent_id[$i]."' ORDER BY c_category";
			}
			$db->setQuery($query);
			$groups = $db->loadObjectList();
			
			if($parent_id[$i] && $i > 0 && count($groups)) {
				$type_level[$i][] = mosHTML::makeOption( 0, ' &nbsp; ' );
				foreach ($groups as $group){
					$type_level[$i][] = mosHTML::makeOption( $group->id, $group->c_category );
				}
				$lists['filter_'.$i.''] = mosHTML::selectList($type_level[$i], 'filter_id_'.$i.'', 'class="text_area" size="1" style="width: 266px;" '.$javascript, 'value', 'text', $level_id[$i] ); //onchange="document.location.href=\''. $link_multi .'\';"
			} elseif($i == 0) {
				$type_level[$i][] = mosHTML::makeOption( 0, ' &nbsp; ' );
				foreach ($groups as $group){
					$type_level[$i][] = mosHTML::makeOption( $group->id, $group->c_category );
				}
				$lists['filter_'.$i.''] = mosHTML::selectList($type_level[$i], 'filter_id_'.$i.'', 'class="text_area" size="1" style="width: 266px;" '.$javascript, 'value', 'text', $level_id[$i] ); //onchange="document.location.href=\''. $link_multi .'\';"
			}
		}	
	} else {
		$lists = array();
		$query = "SELECT a.id as value, a.c_category as text FROM #__lms_course_cats as a order by a.c_category";
		$db->setQuery( $query );
		$sf_cats = array();
		$sf_cats[] = mosHTML::makeOption( '0', '- Select Category -' );
		$sf_cats = array_merge( $sf_cats, $db->loadObjectList() );
		$lists['jlms_course_cats'] = mosHTML::selectList( $sf_cats, 'filt_cat', 'class="text_area" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $filt_cat );
	}
	//FLMS multicat
	$where = '';
	if ($JLMS_CONFIG->get('multicat_use', 0)){
		//NEW MUSLTICATS
		$tmp_level = array();
		$last_catid = 0;
		$i=0;
		foreach($_REQUEST as $key=>$item){
			if(preg_match('#filter_id_(\d+)#', $key, $result)){
				if($item){
					$tmp_level[$i] = $result;
					$last_catid = $item;
					$i++;
				}	
			}	
		}
		$query = "SELECT * FROM #__lms_course_cats ORDER BY id";
		$db->setQuery($query);
		$all_cats = $db->loadObjectList();
		
		$tmp_cats_filter = array();
		$children = array();
		foreach($all_cats as $cat){
			$pt = $cat->parent;
			$list = @$children[$pt] ? $children[$pt] : array();
			array_push($list, $cat->id);
			$children[$pt] = $list;
		}
		$tmp_cats_filter[0] = $last_catid;
		$i=1;
		foreach($children as $key=>$childs){
			if($last_catid == $key){
				foreach($children[$key] as $v){
					if(!in_array($v, $tmp_cats_filter)){
						$tmp_cats_filter[$i] = $v;
						$i++;
					}
				}
			}
		}
		foreach($children as $key=>$childs){
			if(in_array($key, $tmp_cats_filter)){
				foreach($children[$key] as $v){
					if(!in_array($v, $tmp_cats_filter)){
						$tmp_cats_filter[$i] = $v;
						$i++;
					}
				}
			}
		}
		$tmp_cats_filter = array_unique($tmp_cats_filter);
		$catids = implode(",", $tmp_cats_filter);
		if($last_catid && count($tmp_cats_filter)){
			$where .= "\n AND ( a.cat_id IN (".$catids.")";
			if($JLMS_CONFIG->get('sec_cat_use', 0)){
				foreach ($tmp_cats_filter as $tmp_cats_filter_one) {
					$where .= "\n OR a.sec_cat LIKE '%|".$tmp_cats_filter_one."|%'";
				}
			}
			$where .= "\n )";
		}
	} else {
		$where .= $filt_cat ? "\n AND a.cat_id = '$filt_cat'" : "";
	}
	
	// get the total number of records
	$query = "SELECT COUNT(a.id)"
	. "\n FROM #__lms_courses as a"
	. "\n WHERE 1 = 1"
	. "\n $where"
	;
	$db->setQuery( $query );
	$total = $db->loadResult();

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	// get the subset (based on limits) of required records
	$query = "SELECT a.*, b.c_category, u.name, u.username, u.email"
	. "\n FROM  #__lms_courses as a"
	. "\n LEFT JOIN #__lms_course_cats as b ON a.cat_id = b.id"
	. "\n LEFT JOIN #__users as u ON a.owner_id = u.id"
	. "\n WHERE 1"
	. "\n $where"
//	. "\n ORDER BY ".($JLMS_CONFIG->get('lms_courses_sortby',0)?"a.ordering,":"")."a.course_name, b.c_category"
	. "\n ORDER BY ".($JLMS_CONFIG->get('lms_courses_sortby',0)?"a.ordering, a.course_name, a.id":"a.course_name, a.ordering, a.id")
	. "\n LIMIT $pageNav->limitstart, $pageNav->limit"
	;
	$db->setQuery( $query );
	$rows = $db->loadObjectList();
	joomla_lms_adm_html::JLMS_showCoursesList( $rows, $pageNav, $lists, $option);
}

function JLMS_changeMenu( $cid=null, $state=0, $option ) {
	$db = JFactory::getDBO();
	$menutype = mosGetParam($_REQUEST, 'menutype', 0);
	if (!is_array( $cid ) || count( $cid ) < 1) {
		$msg = $publish ? _JLMS_MSG_SLCT_I_TO_PUBL : _JLMS_MSG_SLCT_I_TO_UNPUBL;
		echo "<script> alert('".$msg."'); window.history.go(-1);</script>\n";
		exit();
	}

	$cids = implode( ',', $cid );

	$query = "UPDATE #__lms_menu"
	. "\n SET published = " . intval( $state )
	. "\n WHERE id IN ( $cids )"
	;
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}
	mosRedirect( "index.php?option=$option&task=menu_manage&menutype=".$menutype );
}



function JLMS_changeCourse( $cid=null, $state=0, $option ) {
	$db = JFactory::getDBO();

	if (!is_array( $cid ) || count( $cid ) < 1) {
		$msg = $publish ? _JLMS_MSG_SLCT_I_TO_PUBL : _JLMS_MSG_SLCT_I_TO_UNPUBL;
		echo "<script> alert('".$msg."'); window.history.go(-1);</script>\n";
		exit();
	}

	$cids = implode( ',', $cid );

	$query = "UPDATE #__lms_courses"
	. "\n SET published = " . intval( $state )
	. "\n WHERE id IN ( $cids )"
	;
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}
	mosRedirect( "index.php?option=$option&task=courses" );
}
function JLMS_cancelCourse( ) {
	mosRedirect( "index.php?option=com_joomla_lms&task=courses" );
}

function JLMS_newCourse($option) {
	joomla_lms_adm_html::newCourseInstructions( $option );
}

function JLMS_multicats($last_catid, $tmp, $i=0){
	
	$db = JFactory::getDBO();
	$acl  = & JLMSFactory::getJoomlaACL();
	
	$query = "SELECT parent FROM #__lms_course_cats WHERE id = '".$last_catid."'";
	$db->setQuery($query);
	$parent = $db->loadResult();
	$tmp[$i] = new stdClass();
	$tmp[$i]->catid = $last_catid;
	$tmp[$i]->parent = isset($parent)?$parent:0;
	if($parent){
		$last_catid = $parent;
		$i++;
		$tmp = JLMS_multicats($last_catid, $tmp, $i);
	}
	return $tmp;
}

function JLMS_editCourse( $id, $option ) {
	global $JLMS_CONFIG;
	$db = JFactory::getDBO();
	
	$acl  = & JLMSFactory::getJoomlaACL();

	if(!defined('_JLMS_COURSES_COURSES_GROUPS')){
		define('_JLMS_COURSES_COURSES_GROUPS', _JLMS_CRSS_COURSE_CATS.':');
	}
	$row = new mos_Joomla_LMS_Course( $db );
	$row->load( $id );
	if (isset($row->id) && $row->id) {
		$params = new mosParameters($row->params);
		$lists = array();
		$query = "SELECT * FROM `#__lms_course_cats`";
		$db->setQuery($query);
		$groups = $db->loadObjectList();
		$type_g[] = mosHTML::makeOption( 0, _JLMS_CRSS_SLCT_CAT_ );
		$i = 1;
		foreach ($groups as $group){
			$type_g[] = mosHTML::makeOption( $group->id, $group->c_category );
			$i++;
		}
		$lists['cat_id'] = mosHTML::selectList($type_g, 'cat_id', 'class="text_area" size="1" style="width:266px;" ', 'value', 'text', $row->cat_id );
		$lists['sec_cat_id'] = '';
		if ($JLMS_CONFIG->get('sec_cat_use', 0)) {
			$course_sec_cats = explode('|', $row->sec_cat);
			array_shift($course_sec_cats); array_pop($course_sec_cats);
			$selected_cats = array();
			foreach ($course_sec_cats as $course_sec_cat_id) {
				$selected_cat = new stdClass();
				$selected_cat->value = $course_sec_cat_id;
				$selected_cats[] = $selected_cat;
			}
			$lists['sec_cat_id'] = FLMS_parent( 0, $selected_cats, 'sec_cat_ids[]', false );
		}

		if ($JLMS_CONFIG->get('multicat_show_admin_levels', 0)) {
			if($JLMS_CONFIG->get('flms_integration', 0)){
				$javascript = 'onclick="read_filter();" onchange="javascript:write_filter();document.adminForm.submit();"';
			} else {
				$query = "SELECT count(*) FROM `#__lms_course_cats` WHERE parent > 0";
				$db->setQuery($query);
				$count_subs = $db->loadResult();
				if($count_subs){
					$javascript = 'onclick="read_filter();" onchange="javascript:write_filter();document.adminForm.submit();"';
				} else {
					$javascript = '';
				}
			}
		} else {
			$javascript = '';
		}

		//FLMS multicategories
		$levels = array();
		if ($JLMS_CONFIG->get('multicat_use', 0)){
			//NEW MULTICAT
			if($id){
				$tmp_level = array();
				$last_catid = 0;
				$i=0;
				foreach($_REQUEST as $key=>$item){
					if(preg_match('#level_id_(\d+)#', $key, $result)){
						if($item){
							$tmp_level[$i] = $result;
							$last_catid = $item;
							$i++;
						}	
					}	
				}
				if(!$i){
					$query = "SELECT cat_id FROM #__lms_courses WHERE id = '".$id."'";
					$db->setQuery($query);
					$last_catid = $db->loadResult();	
				}
				
				$tmp = array();
				$tmp = JLMS_multicats($last_catid, $tmp);
				$tmp = array_reverse($tmp);
				
				$tmp_pop = $tmp;
				$tmp_p = array_pop($tmp_pop);
				if(count($tmp) && $tmp_p->catid){
					$next = count($tmp);
					$tmp[$next] = new stdClass();
					$tmp[$next]->catid = 0;
					$tmp[$next]->parent = $tmp_p->catid;
				}
			} else {
				$tmp_level = array();
				$last_catid = 0;
				$i=0;
				foreach($_REQUEST as $key=>$item){
					if(preg_match('#level_id_(\d+)#', $key, $result)){
						$tmp_level[$i] = $result;
						$last_catid = $item;
						$i++;	
					}	
				}
				$tmp = array();
				$tmp = JLMS_multicats($last_catid, $tmp);
				$tmp = array_reverse($tmp);
				
				$tmp_pop = $tmp;
				$tmp_p = array_pop($tmp_pop);
				if(count($tmp) && $tmp_p->catid){
					$next = count($tmp);
					$tmp[$next] = new stdClass();
					$tmp[$next]->catid = 0;
					$tmp[$next]->parent = isset($tmp_p->catid)?$tmp_p->catid:0;
				}
			}
			$query = "SELECT * FROM #__lms_course_cats_config ORDER BY id";
			$db->setQuery($query);
			$levels = $db->loadObjectList();
			if(count($levels) == 0){
				for($i=0;$i<5;$i++){
					$levels[$i] = new stdClass();
					if($i > 0){
						$levels[$i]->cat_name = _JLMS_COURSES_COURSES_GROUPS;	
					} else {
						$levels[$i]->cat_name = _JLMS_COURSES_COURSES_GROUPS;
					}
				}
			}
			for($i=0;$i<count($levels);$i++){
				if(isset($tmp[$i])){
					$query = "SELECT * FROM `#__lms_course_cats` WHERE parent = '".$tmp[$i]->parent."' ORDER BY c_category";
					$db->setQuery($query);
					$groups = $db->loadObjectList();
					
					if($tmp[$i]->parent && $i > 0 && count($groups)){
						$type_level[$i][] = mosHTML::makeOption( 0, '&nbsp;' );
						
						foreach ($groups as $group){
							$type_level[$i][] = mosHTML::makeOption( $group->id, $group->c_category );
						}
						$lists['level_'.$i.''] = mosHTML::selectList($type_level[$i], 'level_id_'.$i.'', 'class="text_area" size="1" style="width:266px;" '.$javascript, 'value', 'text', $tmp[$i]->catid );
					} elseif($i == 0) {
						$type_level[$i][] = mosHTML::makeOption( 0, ' - '._JLMS_CRSS_SELECT.' '.$levels[$i]->cat_name.' - ' );
					
						foreach ($groups as $group){
							$type_level[$i][] = mosHTML::makeOption( $group->id, $group->c_category );
						}
						$lists['level_'.$i.''] = mosHTML::selectList($type_level[$i], 'level_id_'.$i.'', 'class="text_area" size="1" style="width:266px;" '.$javascript, 'value', 'text', $tmp[$i]->catid );
					}
				}
			}
			//NEW MULTICAT
		}

		$query = "SELECT * FROM `#__lms_languages`";
		$db->setQuery($query);
		$lang_all = $db->loadObjectList();
		$i = 1;
		foreach ($lang_all as $lang_one){
			$lang[] = mosHTML::makeOption( $lang_one->id, $lang_one->lang_name );
			$i++;
		}
		$lists['language'] = mosHTML::selectList($lang, 'language', 'class="text_area" size="1" ', 'value', 'text', $row->language );
		$lists['add_forum'] = mosHTML::yesnoRadioList( 'add_forum', 'class="text_area disabled" disabled="disabled" ', $row->add_forum);
		$lists['add_hw'] = mosHTML::yesnoRadioList( 'add_hw', 'class="text_area" ', $row->add_hw);
		$lists['add_attend'] = mosHTML::yesnoRadioList( 'add_attend', 'class="text_area" ', $row->add_attend);
		$lists['add_chat'] = mosHTML::yesnoRadioList( 'add_chat', 'class="text_area" ', $row->add_chat);
		$lists['self_reg'] = mosHTML::yesnoRadioList( 'self_reg', 'class="text_area" ', $row->self_reg);
		$lists['published'] = mosHTML::yesnoRadioList( 'published', 'class="text_area" ', $row->published);
		$lists['publish_start'] = mosHTML::yesnoRadioList( 'publish_start', 'class="text_area" ', $row->publish_start);
		$lists['publish_end'] = mosHTML::yesnoRadioList( 'publish_end', 'class="text_area" ', $row->publish_end);

		/* 27.02.2007 + usergroup access (paid request) */
		// ensure user can't add group higher than themselves		

		$gtree = array();
		$gtree[] = mosHTML::makeOption( 0, _JLMS_CRSS_DEF_ACCESS );
		$gtree = array_merge($gtree, $acl->get_group_children_tree( null, 'USERS', false ) );

		$s_gid = array(0);
		if ($row->gid) {
			$s_gid = explode( ",", $row->gid );
		}
		$selected = array();
		foreach ($s_gid as $o) {
			$s = new stdClass();
			$s->value = $o;
			$selected[] = $s;
		}

		$lists['gid'] = mosHTML::selectList( $gtree, 'gid[]', 'size="10" multiple="multiple"', 'value', 'text', $selected );
		/* End of usergroups list*/
		
		//Course Properties Event//
		$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
		$_JLMS_PLUGINS->loadBotGroup('system');
		$plugin_args = array();
		$plugin_args[] = $row;
		$lists['plugin_return'] = $_JLMS_PLUGINS->trigger('onShowCoursePropertiesAdmin', $plugin_args);
		$all_fields_tmp = $lists['plugin_return'];
		$lists['plugin_return'] = array();
		foreach($all_fields_tmp as $fields_tmp){
			foreach($fields_tmp as $field_tmp){
				$lists['plugin_return'][] = $field_tmp;
			}
		}
		//Course Properties Event//

        $query = "SELECT published FROM #__lms_plugins WHERE element = 'waitinglist'";
        $db->setQuery($query);
        $rowWaitingList = $db->loadObject();
        $waitingListPublished = $rowWaitingList->published == 1 ? true : false;

        $options = array();
        $options['waitingListPublished'] = $waitingListPublished;

		joomla_lms_adm_html::editCourse( $row, $lists, $option, $params, $levels, $options );
	}
}
function JLMS_saveCourse( $id,  $option, $task ) {
	global $JLMS_CONFIG;
	$db = JFactory::getDBO();

	if ($JLMS_CONFIG->get('multicat_use', 0)) {
		//NEW version multicat
		$tmp_level = array();
		$last_catid = 0;
		$i=0;
		foreach($_REQUEST as $key=>$item){
			if(preg_match('#level_id_(\d+)#', $key, $result)){
				if($item){
					$tmp_level[$i] = $result;
					$last_catid = $item;
					$i++;
				}	
			}	
		}
		$_POST['cat_id'] = $last_catid;
		//NEW version multicat
	}

	$row = new mos_Joomla_LMS_Course( $db );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	unset($row->add_forum);
	//unset($row->owner_id);

	$course_name_post = isset($_REQUEST['course_name'])?strval($_REQUEST['course_name']):'';
	$course_name_post = (get_magic_quotes_gpc()) ? stripslashes( $course_name_post ) : $course_name_post;
	$row->course_name = $course_name_post;
	$metakeys_post = isset($_REQUEST['metakeys'])?strval($_REQUEST['metakeys']):'';
	$metakeys_post = (get_magic_quotes_gpc()) ? stripslashes( $metakeys_post ) : $metakeys_post;
	$row->metakeys = $metakeys_post;
	$metadesc_post = isset($_REQUEST['metadesc'])?strval($_REQUEST['metadesc']):'';
	$metadesc_post = (get_magic_quotes_gpc()) ? stripslashes( $metadesc_post ) : $metadesc_post;
	$row->metadesc = $metadesc_post;
	$course_description_post = isset($_REQUEST['course_description'])?strval($_REQUEST['course_description']):'';
	$course_description_post = (get_magic_quotes_gpc()) ? stripslashes( $course_description_post ) : $course_description_post;
	$row->course_description = $course_description_post;

	$course_sh_description_post = isset($_REQUEST['course_sh_description'])?strval($_REQUEST['course_sh_description']):'';
	$course_sh_description_post = (get_magic_quotes_gpc()) ? stripslashes( $course_sh_description_post ) : $course_sh_description_post;
	$row->course_sh_description = $course_sh_description_post;

	$row->gid = array(0);
	$gid = mosGetParam($_POST, 'gid', array(0));
	$gid_real = array();
	foreach ($gid as $g) {
		$gid_real[] = $g;
	}
	if (in_array(0,$gid_real)) { $gid_real = array(0); }
	$row->gid = implode(",", $gid_real);

	$query = "SELECT lms_config_value FROM #__lms_config WHERE lms_config_var='sec_cat_use'";
	$db->setQuery($query);
	if ($db->loadResult()) {
		$row->sec_cat = '|'.implode('|', josGetArrayInts( 'sec_cat_ids' )).'|';
	}
	$params = mosGetParam( $_POST, 'params', '' );
	if (is_array( $params )) {
		$txt = array();
		foreach ( $params as $k=>$v) {
			if (get_magic_quotes_gpc()) {
				$v = stripslashes( $v );
			}
			$txt[] = "$k=$v";
		}
		$row->params = implode( "\n", $txt );
	}
	
	//Course Properties Event//
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$_JLMS_PLUGINS->loadBotGroup('system');
	$plugin_args = array();
	$lists['plugin_return'] = $_JLMS_PLUGINS->trigger('onSaveCoursePropertiesAdmin', $plugin_args);
	if(isset($lists['plugin_return']) && count($lists['plugin_return'])){
		$fields = $lists['plugin_return'];
		
		if(isset($row->id) && $row->id){
			$row_p = new mos_Joomla_LMS_Course( $db );
			$row_p->load($row->id);
		}
		
		$params = strlen($row_p->params) ? explode("\n", $row_p->params) : array();
		
		$params_check = array();
		$i=0;
		foreach($params as $param){
			preg_match('#(.*)=(.*)#', $param, $out);
			
			if(isset($out[0]) && strlen($out[0]) && $out[1] && isset($out[2])){
				$params_check[$i]->name = $out[1];
				$params_check[$i]->value = $out[2];
				$i++;
			}
		}
		
		$all_fields_tmp = $fields;
		$fields = array();
		foreach($all_fields_tmp as $fields_tmp){
			foreach($fields_tmp as $field_tmp){
				$fields[] = $field_tmp;
			}
		}
		
		for($i=0;$i<count($fields);$i++){
			$field = $fields[$i];
			
			$triger = true;
			foreach($params_check as $param_ch){
				if(strtolower($field->name) == $param_ch->name){
					if($field->value == $param_ch->value){
						$triger = false;
						break;
					} else {
						$triger = false;
						$param_ch->value = $field->value;
						break;
					}
				}
			}
			
			$params = array();
			foreach($params_check as $param_ch){
				$params[] = strtolower($param_ch->name).'='.$param_ch->value;
			}
			
			if($triger && strlen($field->name) && $field->value){
				$params[] = strtolower($field->name).'='.$field->value;
			}
			
		}
		
		$row->params = count($params) ? implode("\n", $params) : $row->params;
	}
	//Course Properties Event//
	
	if (!$row->check()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}else 
	{ 
		if (isset($_REQUEST['update_all_resources']) && isset($_REQUEST['update_old_owner']))
		{
			$course_id = (int) $row->id;
			$old_uid = (int)$_REQUEST['update_old_owner'];
			$new_uid = (int)$row->owner_id;
		if ($course_id && $old_uid && $new_uid)
		{
			$inclfile_path = JPATH_SITE . DS . 'administrator' . DS . 'components' . DS . 'com_joomla_lms' . DS . 'files'.DS.'admin.courses.php';
			if (file_exists($inclfile_path))
			{
				require_once($inclfile_path);
				$upd_course = new JLMS_Admin_Courses_Tools();
				$upd_course->updateCourseOvner($course_id,$old_uid,$new_uid);
			}
		}
		}
	}
	if ($task == 'apply_course') {
		mosRedirect( "index.php?option=com_joomla_lms&task=editA_course&id=$id" );
	} else {
		mosRedirect( "index.php?option=com_joomla_lms&task=courses" );
	}
}
function JLMS_pre_DeleteCourse( $id, $option ) {
	$db = JFactory::getDBO();
	$query = "SELECT * FROM #__lms_courses WHERE id = $id";
	$db->SetQuery( $query );
	$rows = $db->LoadObjectList();
	if (count($rows)) {
		joomla_lms_adm_html::view_preDeletePage( $rows, $id, $option );
	} else {
		mosRedirect( "index.php?option=com_joomla_lms&task=courses" );
	}
}
function JLMS_deleteCourse( $id, $option ) {
	global $JLMS_CONFIG;
	$db = JFactory::getDBO();

	$query = "SELECT lms_config_value FROM #__lms_config WHERE lms_config_var = 'jlms_doc_folder'";
	$db->SetQuery( $query );
	$jlms_doc_folder = $db->LoadResult();
	if (!defined('_JOOMLMS_DOC_FOLDER')) {
		define('_JOOMLMS_DOC_FOLDER', $jlms_doc_folder."/");
	}
	$query = "SELECT lms_config_value FROM #__lms_config WHERE lms_config_var = 'scorm_folder'";
	$db->SetQuery( $query );
	$scorm_folder = $db->LoadResult();
	if (!defined('_JOOMLMS_SCORM_FOLDER')) {
		define('_JOOMLMS_SCORM_FOLDER',  $scorm_folder);
	}
	if (!defined('_JOOMLMS_SCORM_FOLDER_PATH')) {
		define('_JOOMLMS_SCORM_FOLDER_PATH',  JPATH_SITE . DS . $scorm_folder);
	}
	require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_del_operations.php");
	JLMS_DelOp_deleteCourse( $id );
	require_once(_JOOMLMS_FRONT_HOME . "/includes/flms/flms.courses.php");
	if($JLMS_CONFIG->get('flms_integration', 0)){
		FLMS_delete_params_lesson($id);
	}
	if ($JLMS_CONFIG->get('multicat_use', 0)){
		FLMS_delete_multicat($id);	
	}
	mosRedirect( "index.php?option=com_joomla_lms&task=courses" );
}

##########################################################################
###	--- ---   	LANGUAGES	 	--- --- ###
##########################################################################

function JLMS_ListLangs( $option ) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
    
    JLMSLang::isExtFontExistsChecking();
    JLMSLang::isBaseFontExistsChecking();

	// get the total number of records
	$query = "SELECT COUNT(a.id)"
	. "\n FROM #__lms_languages as a";
	$db->setQuery( $query );
	$total = $db->loadResult();

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	// get the subset (based on limits) of required records
	$query = "SELECT a.*"
	. "\n FROM  #__lms_languages as a"
	. "\n ORDER BY a.lang_name"
	. "\n LIMIT $pageNav->limitstart, $pageNav->limit"
	;
	$db->setQuery( $query );
	$rows = $db->loadObjectList();
	
	$front_langs = scandir(_JOOMLMS_FRONT_HOME.DS.'languages');
	$back_langs = scandir(_JOOMLMS_ADMIN_HOME.DS.'language');
	
	$query = "SELECT lms_config_value FROM #__lms_config WHERE lms_config_var='default_language'";
	$db->setQuery( $query );
	$def_lang = $db->loadResult();
	
	for( $i=0; $i < count($rows); $i++ ) 
    {        
		$rows[$i]->default = ( $rows[$i]->lang_name == $def_lang );
		$rows[$i]->is_front_trans = in_array( $rows[$i]->lang_name, $front_langs );
		$rows[$i]->is_back_trans = in_array( $rows[$i]->lang_name, $back_langs );
	}	
	
	joomla_lms_adm_html::JLMS_showLangsList( $rows, $pageNav, $lists, $option);
}
function JLMS_changeLang( $cid=null, $state=0, $option ) {
	$db = JFactory::getDBO();

	if (!is_array( $cid ) || count( $cid ) < 1) {
		$msg = $publish ? _JLMS_MSG_SLCT_I_TO_PUBL : _JLMS_MSG_SLCT_I_TO_UNPUBL;
		echo "<script> alert('".$msg."'); window.history.go(-1);</script>\n";
		exit();
	}

	$cids = implode( ',', $cid );

	$query = "UPDATE #__lms_languages"
	. "\n SET published = " . intval( $state )
	. "\n WHERE id IN ( $cids )"
	;
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}
	mosRedirect( "index.php?option=$option&task=languages" );
}

function JLMS_makeLangDefault( $id, $option ) {
	$db = JFactory::getDBO();
	
	$query = "SELECT lang_name FROM #__lms_languages WHERE id = $id";
	$db->setQuery( $query );
	$lang_name = $db->LoadResult();
	
	$query = "UPDATE #__lms_config SET lms_config_value=".$db->quote( $lang_name )." WHERE lms_config_var='default_language'";
	$db->setQuery( $query );
	$db->Query();	
	
	mosRedirect( "index.php?option=$option&task=languages" );
}

function JLMS_ExportLang( $id, $option ) {
	$db = JFactory::getDBO();
	if ($id) {
		$query = "SELECT lang_name FROM #__lms_languages WHERE id = $id";
		$db->setQuery( $query );
		$lang_name = $db->LoadResult();
		if (file_exists(JPATH_SITE.'/components/com_joomla_lms/languages/'.$lang_name.'/main.lang.php') || 
			file_exists(JPATH_SITE.'/administrator/components/com_joomla_lms/language/'.$lang_name.'/admin.main.lang.php') ) {

			require_once(_JOOMLMS_FRONT_HOME . "/includes/libraries/lms.lib.zip.php");
			require_once(_JOOMLMS_FRONT_HOME . "/includes/libraries/lms.lib.files.php");
			$uniq = $id . "_" . mktime();
			$config =JFactory::getConfig();

			 if (!JLMS_J30version())
			 {
			 $tmppath = $config->getValue('config.tmp_path');
			 
			 } else {
			 
			 $tmppath = $config->get('tmp_path');
			 
			 }
                        $lang_zip = $tmppath.DS.'lang_'.$uniq.'.zip';
			mkdir($tmppath.DS.'lang_'.$uniq);
			mkdir($tmppath.DS.'lang_'.$uniq.DS.$lang_name);
			if (file_exists(JPATH_SITE.'/components/com_joomla_lms/languages/'.$lang_name.'/main.lang.php')) {
				mkdir($tmppath.DS.'lang_'.$uniq.DS.$lang_name.DS.'site');
				JLMS_Files::copyFolder(JPATH_SITE.DS.'components'.DS.'com_joomla_lms'.DS.'languages'.DS.$lang_name, $tmppath.DS.'lang_'.$uniq.DS.$lang_name.DS.'site');
			}
			if (file_exists(JPATH_SITE.'/administrator/components/com_joomla_lms/language/'.$lang_name.'/admin.main.lang.php')) {
				mkdir($tmppath.DS.'lang_'.$uniq.DS.$lang_name.DS.'admin');
				JLMS_Files::copyFolder(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_joomla_lms'.DS.'language'.DS.$lang_name, $tmppath.DS.'lang_'.$uniq.DS.$lang_name.DS.'admin');
			}
			$pz = new PclZip($lang_zip);
			$pz->create($tmppath.DS.'lang_'.$uniq.DS.$lang_name, PCLZIP_OPT_REMOVE_PATH, $tmppath.DS.'lang_'.$uniq.DS.$lang_name);
			require_once (_JOOMLMS_INCLUDES_PATH."jlms_download.php");
			JLMS_download ($lang_name.'.zip', $lang_zip, false);
			@unlink($lang_zip);
			exit;
		}
	}
	mosRedirect( "index.php?option=com_joomla_lms&task=languages" );
}
function JLMS_UploadLang( $option ) {
	$db = JFactory::getDBO();
	$temp_file = mosGetParam( $_FILES, 'import_lang_file', null );
	if (!$temp_file) {
		mosErrorAlert ( _JLMS_LANG_MSG_UPLOAD_FAILED );
	}
	$temp_file_name = $temp_file['name'];
	if (empty($temp_file_name)) {
		mosErrorAlert( _JLMS_LANG_MSG_SELECT_FILE );
	}
	$filename = explode(".", $temp_file_name);
	if (preg_match("/[^0-9a-zA-Z_]/i", $filename[0])) {
		mosErrorAlert( _JLMS_LANG_MSG_ALPHANUMERIC );
	}
	if (strcmp(substr($temp_file_name,-4,1),".")) {
		mosErrorAlert( _JLMS_LANG_MSG_BAD_EXT );
	}
	if (strcmp(substr($temp_file_name,-4),".zip")) {
		mosErrorAlert( _JLMS_LANG_MSG_BAD_EXT );
	}
	$lang_name = $filename[0];			
	$tmp_name = $temp_file['tmp_name'];
	require_once(_JOOMLMS_INCLUDES_PATH . DS . "libraries" . DS. "lms.lib.files.php");
	require_once(_JOOMLMS_INCLUDES_PATH . DS . "libraries" . DS. "lms.lib.zip.php");
	$msg = '';
	$resultdir = JLMS_Files::uploadFile( $temp_file['tmp_name'], $temp_file_name, $msg );
	if (!$resultdir) {
		mosErrorAlert($msg);
	}
	$extract_dir = JPATH_SITE.DS.'tmp'.DS.'jlms_languages'.DS.$lang_name.DS;
	$archive = JPATH_SITE.DS.'tmp'.DS.$temp_file_name;
	
	//exstract archive in uniqfolder media
	JLMS_Zip::extractFile( $archive, $extract_dir );
	
	$admin_temp = $extract_dir.'admin';
	$site_temp = $extract_dir.'site';
	$admin_dest = _JOOMLMS_ADMIN_HOME.DS.'language'.DS.$lang_name;
	$site_dest = _JOOMLMS_FRONT_HOME.DS.'languages'.DS.$lang_name;

	$is_adm_temp_exst = JFolder::exists( $admin_temp );
	$is_site_temp_exst = JFolder::exists( $site_temp );

	if( $is_adm_temp_exst ) 
	{	
		if( JFolder::exists( $admin_dest ) )
			JFolder::delete( $admin_dest );
		JFolder::copy( $admin_temp, $admin_dest );
	}

	if( $is_site_temp_exst ) 
	{	
		if( JFolder::exists( $site_dest ) )
			JFolder::delete( $site_dest );
		JFolder::copy( $site_temp, $site_dest );
	}

	if( !$is_site_temp_exst && !$is_adm_temp_exst )
		JFolder::copy( $extract_dir, $site_dest );	

	$query = "SELECT count(*) FROM #__lms_languages WHERE lang_name = ".$db->Quote( $lang_name );
	$db->SetQuery( $query );
	if ( !$db->LoadResult() ) 
	{
		$query = "INSERT INTO #__lms_languages (lang_name) VALUES (".$db->Quote( $lang_name ).")";
		$db->SetQuery( $query );
		$db->query();		
	}

	if( JFile::exists( $archive ) ) JFile::delete( $archive );
	JFolder::delete( JPATH_SITE.DS.'tmp'.DS.'jlms_languages' );

	mosRedirect( "index.php?option=com_joomla_lms&task=languages" );
}
function JLMS_DeleteLang( $id, $option ) {
	$db = JFactory::getDBO();
	$query = "SELECT lang_name FROM #__lms_languages WHERE id = $id AND lang_name <> 'english'";
	$db->SetQuery( $query );
	$lang_name = $db->loadResult();
	if ($lang_name) {
		$query = "DELETE FROM #__lms_languages WHERE id = $id";
		$db->SetQuery( $query );
		$db->query();
		$query = "SELECT id FROM #__lms_languages WHERE lang_name = 'engilsh'";
		$db->SetQuery( $query );
		$eng_id = $db->loadResult();
		if ($eng_id) {
			$query = "UPDATE #__lms_courses SET language = $eng_id WHERE language = $id";
			$db->SetQuery( $query );
			$db->query();
			$query = "UPDATE #__lms_config SET lms_config_value = 'engilsh' WHERE lms_config_var = 'default_language' AND lms_config_value = '$lang_name'";
			$db->SetQuery( $query );
			$db->query();
		}
		require_once(_JOOMLMS_FRONT_HOME . "/includes/jlms_dir_operation.php");
		
		deldir(_JOOMLMS_ADMIN_HOME. "/language/".$lang_name."/");
		deldir(_JOOMLMS_FRONT_HOME. "/languages/".$lang_name."/");	
	}
	mosRedirect( "index.php?option=com_joomla_lms&task=languages" );
}

function JLMS_viewChildrens($option){
	$db = JFactory::getDbo();
	$app = JFactory::getApplication('frontend');
	
	$option	= JRequest::getCmd( 'option' );
	
	$limit 		= intval( $app->getUserStateFromRequest( "view{$option}limit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	
	$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
	JArrayHelper::toInteger($cid, array(0));
	
	$parent_id = JRequest::getVar( 'parent_id', $cid[0], '', 'int' );
	$group_id = JRequest::getVar( 'group_id', 0, '', 'int' );
	
	$query = "SELECT a.id, a.parent_id, a.user_id, b.username, b.name, b.email"
	. "\n FROM #__lms_user_parents as a"
	. "\n LEFT JOIN #__users as b ON a.user_id = b.id"
	. "\n, #__lms_usertypes as c"
	. "\n, #__lms_users as d"
	. "\n WHERE 1"
//	.($parent_id ? "\n AND a.parent_id = '".$parent_id."'" : '')
	. "\n AND a.parent_id = '".$parent_id."'"
	. "\n AND a.parent_id = d.user_id"
	. "\n AND d.lms_usertype_id = c.id"
	. "\n ORDER BY b.username"
	;
	$db->setQuery($query);
	$db->query();
	
	$total = $db->getNumRows();

	jimport('joomla.html.pagination');
	$pagination = new JPagination($total, $limitstart, $limit);
	
	$db->setQuery($query, $pagination->limitstart, $pagination->limit);
	$rows = $db->loadObjectList();
	
	$lists = array();
	
	$query = "SELECT DISTINCT(a.user_id), a.*, c.name, c.username, c.email, a.user_id as value, c.username as text"
	. "\n FROM #__lms_users as a"
	. "\n, #__lms_usertypes as b"
	. "\n, #__users as c"
	. "\n WHERE 1"
	. "\n AND a.lms_usertype_id = b.id"
	. "\n AND a.user_id = c.id"
	. "\n AND b.roletype_id = 3"
	. "\n ORDER BY c.username"
	;
	$db->setQuery($query);
	$parents = $db->loadObjectList();
	$list_parents = array();
//	$list_parents[] = JHTML::_('select.option', 0, _JLMS_USERS_NAME_ );
	$list_parents = array_merge( $list_parents, $parents );
	$lists['f_parents'] = JHTML::_('select.genericlist', $list_parents, 'parent_id', 'class="inputbox" style="width: 300px;" size="1" onchange="submitbutton(\'switch_parent\');" ', 'value', 'text', $parent_id );
	$lists['parent_id'] = $parent_id;
	
	joomla_lms_adm_html::JLMS_ViewChildrens($rows, $pagination, $lists, $option);
}

function JLMS_editChildren(){
	$db = JFactory::getDbo();
	$app = JFactory::getApplication('frontend');
	
	$option	= JRequest::getCmd( 'option' );
	$task	= JRequest::getCmd( 'task' );
	$Itemid	= JRequest::getCmd( 'Itemid' );
	
	$limit 		= intval( $app->getUserStateFromRequest( "view{$option}limit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	
	$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
	JArrayHelper::toInteger($cid, array(0));
	
	if($task == 'edit_child'){
		$parent_id = intval( $app->getUserStateFromRequest( "view{$option}parent_id", 'parent_id', $cid[0] ) );	
	} else {
		$parent_id = JRequest::getVar( 'parent_id', $cid[0], '', 'int' );
	}
	
	$h_parent_id = JRequest::getVar( 'h_parent_id', 0, '', 'int' );
	$group_ids = JRequest::getVar( 'group_ids', array(0), '', 'array' );
	$f_group_id = JRequest::getVar( 'f_group_id', 0, '', 'int' );
	
	$query = "SELECT a.user_id"
	. "\n FROM #__lms_user_parents as a"
	. "\n WHERE 1"
	. "\n AND a.parent_id = '".$parent_id."'"
	;
	$db->setQuery($query);
	$exist_uids = JLMSDatabaseHelper::loadResultArray();
	if($parent_id){
		$exist_uids[] = $parent_id;
	}
	
	$query = "SELECT a.group_id"
	. "\n FROM #__lms_user_assign_groups as a"
	. "\n WHERE 1"
	. "\n AND a.user_id = '".$parent_id."'"
	;
	$db->setQuery($query);
	$exist_gids = JLMSDatabaseHelper::loadResultArray();
	
	if($f_group_id){
		$query = "SELECT a.*"
		. "\n FROM #__users as a"
		. "\n, #__lms_users_in_global_groups as c"
		. "\n WHERE 1"
		. "\n AND c.user_id = a.id"
		. "\n AND c.group_id = '".$f_group_id."'"
		.(count($exist_uids) ? "\n AND a.id NOT IN (".implode(',', $exist_uids).")" : '')
		. "\n ORDER BY a.username"
		;
	} else {
		$query = "SELECT a.*"
		. "\n FROM #__users as a"
		. "\n WHERE 1"
		.(count($exist_uids) ? "\n AND a.id NOT IN (".implode(',', $exist_uids).")" : '')
		. "\n ORDER BY a.username"
		;
	}
	$db->setQuery($query);
	$db->query();
	
	$total = $db->getNumRows();

	jimport('joomla.html.pagination');
	$pagination = new JPagination($total, $limitstart, $limit);
	
	$db->setQuery($query, $pagination->limitstart, $pagination->limit);
	$rows = $db->loadObjectList();
		
	$lists = array();
	
	$query = "SELECT DISTINCT(a.user_id), a.*, c.name, c.username, c.email, a.user_id as value, c.username as text"
	. "\n FROM #__lms_users as a"
	. "\n, #__lms_usertypes as b"
	. "\n, #__users as c"
	. "\n WHERE 1"
	. "\n AND a.lms_usertype_id = b.id"
	. "\n AND a.user_id = c.id"
	. "\n AND b.roletype_id = 3"
	. "\n ORDER BY c.username"
	;
	$db->setQuery($query);
	$parents = $db->loadObjectList();
	$list_parents = array();
	$list_parents[] = JHTML::_('select.option', 0, _JLMS_CEO_SLCT_PAR_USERNAME_ );
	$list_parents = array_merge( $list_parents, $parents );
	$lists['list_parents'] = JHTML::_('select.genericlist', $list_parents, 'parent_id', 'class="inputbox" style="width: 300px;" size="1" onchange="submitbutton(\'switch_parent\');" ', 'value', 'text', $parent_id );
	
	if($parent_id){
		$query = "SELECT group_id"
		. "\n FROM #__lms_user_assign_groups"
		. "\n WHERE 1"
		. "\n AND user_id = '".$parent_id."'"
		;
		$db->setQuery($query);
		if(is_array(JLMSDatabaseHelper::loadResultArray())){
			$db_group_ids = JLMSDatabaseHelper::loadResultArray();
			
			if($parent_id == $h_parent_id){
				$group_ids = $group_ids + $db_group_ids;
			} else {
				$group_ids = count($db_group_ids) ? $db_group_ids : array(0);
			}
		}
	} else {
		$group_ids = array(0);
	}
	
	$query = "SELECT id as value, ug_name as text"
	. "\n FROM #__lms_usergroups"
	. "\n WHERE 1"
	. "\n AND course_id = 0"
	. "\n AND parent_id = 0"
	;
	$db->setQuery($query);
	$groups = $db->loadObjectList();
	$list_groups = array();
	$list_groups[] = JHTML::_('select.option', 0, _JLMS_USERS_SLCT_GR_ );
	$list_groups = array_merge( $list_groups, $groups );
	$lists['list_groups'] = JHTML::_('select.genericlist', $list_groups, 'group_ids[]', 'class="inputbox" style="width: 300px;" size="10" multiple="multiple"', 'value', 'text', $group_ids );
	
	$query = "SELECT id as value, ug_name as text"
	. "\n FROM #__lms_usergroups"
	. "\n WHERE 1"
	. "\n AND course_id = 0"
	. "\n AND parent_id = 0"
	;
	$db->setQuery($query);
	$groups = $db->loadObjectList();
	$f_groups = array();
	$f_groups[] = JHTML::_('select.option', 0, _JLMS_USERS_SLCT_GR_ );
	$f_groups = array_merge( $f_groups, $groups );
	$lists['f_groups'] = JHTML::_('select.genericlist', $f_groups, 'f_group_id', 'class="inputbox" style="width: 300px;" size="1" onchange="submitbutton(\'switch_parent\');"', 'value', 'text', $f_group_id );
	$lists['parent_id'] = $parent_id;
		
	$lists['task'] = $task;
	
	joomla_lms_adm_html::JLMS_EditChildren($rows, $pagination, $lists, $option);
}

function JLMS_saveChildren($option){
	$db = JFactory::getDbo();
	$app = JFactory::getApplication('administrator');
	
	$option	= JRequest::getCmd( 'option' );
	
	$parent_id = JRequest::getVar( 'parent_id', 0, '', 'int' );
	$group_ids = JRequest::getVar( 'group_ids', array(0), '', 'array' );
	JArrayHelper::toInteger($group_ids, array(0));
	
	$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
	JArrayHelper::toInteger($cid, array(0));
	
	$grp_add = false;
	$usr_add = false;
	
	if(count($group_ids)){
		
		$query = "DELETE FROM #__lms_user_assign_groups WHERE user_id = '".$parent_id."'";
		$db->setQuery($query);
		$db->query();
		
		if($group_ids[0] && count($group_ids)){
			foreach($group_ids as $group_id){
				$query = "INSERT INTO #__lms_user_assign_groups"
				. "\n (user_id, group_id)"
				. "\n VALUES"
				. "\n (".$parent_id.", ".$group_id.")"
				;
				$db->setQuery($query);
				if($db->query()){
					$grp_add = true;
				}
			}
		}
	}
	if($cid[0] && count($cid)){
		
		$query = "SELECT user_id"
		. "\n FROM #__lms_user_parents"
		. "\n WHERE 1"
		. "\n AND parent_id = '".$parent_id."'"
		;
		$db->setQuery($query);
        $dbHelper = new JLMSDatabaseHelper();
        $loadedResult = $dbHelper->loadResultArray();
		$exist_uids =  $loadedResult ?  $loadedResult : array();
		
//		$query = "DELETE FROM #__lms_user_parents WHERE parent_id = '".$parent_id."'";
//		$db->setQuery($query);
//		$db->query();
		
		foreach($cid as $user_id){
			if(!in_array($user_id, $exist_uids)){
				$query = "INSERT INTO #__lms_user_parents"
				. "\n (parent_id, user_id)"
				. "\n VALUES"
				. "\n (".$parent_id.", ".$user_id.")"
				;
				$db->setQuery($query);
				if($db->query()){
					$usr_add = true;
				}
			}
		}
	}

	if($grp_add && !$usr_add){
		$app->redirect("index.php?option=$option&task=group_managers&filt_users=$parent_id");
	} else {
		$app->redirect("index.php?option=$option&task=view_childrens&parent_id=$parent_id");
	}
}

function JLMS_deleteChildren($option){
	$db = JFactory::getDbo();
	$app = JFactory::getApplication('administrator');
	
	$option	= JRequest::getCmd( 'option' );
	
	$parent_id = JRequest::getVar( 'parent_id', 0, '', 'int' );
	$group_ids = JRequest::getVar( 'group_ids', array(0), '', 'array' );
	JArrayHelper::toInteger($group_ids, array(0));
	
	$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
	JArrayHelper::toInteger($cid, array(0));
	
	if($cid[0] && count($cid)){
		$query = "DELETE FROM #__lms_user_parents WHERE parent_id = '".$parent_id."' AND id IN (".implode(',', $cid).")";
		$db->setQuery($query);
		$db->query();
	}
	$app->redirect("index.php?option=$option&task=view_childrens&parent_id=$parent_id");
}

function JLMS_deleteParent($option){
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');
	
	$option	= JRequest::getCmd( 'option' );
	
	$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
	JArrayHelper::toInteger($cid, array(0));
	
	if($cid[0] && count($cid)){
		$query = "DELETE FROM #__lms_user_parents WHERE 1 AND parent_id IN (".implode(',', $cid).")";
		$db->setQuery($query);
		$db->query();
		
		$query = "DELETE FROM #__lms_user_assign_groups WHERE 1 AND user_id IN (".implode(',', $cid).")";
		$db->setQuery($query);
		$db->query();
	}
	$app->redirect("index.php?option=$option&task=view_parents");
}

function JLMS_viewParents( $option ) {
	$db = JFactory::getDbo();
	$app = JFactory::getApplication('administrator');
	
	$option	= JRequest::getCmd( 'option' );
	
	$limit 		= intval( $app->getUserStateFromRequest( "view{$option}limit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	
	$query = "SELECT a.*, c.name, c.username, c.email"
	. "\n FROM #__lms_users as a"
	. "\n, #__lms_usertypes as b"
	. "\n, #__users as c"
	. "\n WHERE 1"
	. "\n AND a.lms_usertype_id = b.id"
	. "\n AND a.user_id = c.id"
	. "\n AND b.roletype_id = 3"
	. "\n GROUP BY c.id"
	;
	$db->setQuery($query);
	$db->query();
	
	$total = $db->getNumRows();
	
	jimport('joomla.html.pagination');
	$pagination = new JPagination($total, $limitstart, $limit);
	
	$db->setQuery($query, $pagination->limitstart, $pagination->limit);
	$parents = $db->loadObjectList();
	
	for($i=0;$i<count($parents);$i++){
		
		$query = "SELECT a.*, b.ug_name as group_name"
		. "\n FROM #__lms_user_assign_groups as a"
		. "\n, #__lms_usergroups as b"
		. "\n WHERE 1"
		. "\n AND a.user_id = '".$parents[$i]->user_id."'"
		. "\n AND a.group_id = b.id"
		. "\n AND b.course_id = 0"
		;
		$db->setQuery($query);
		$grp_info = $db->loadObjectList();
		
		$parents[$i]->count_groups = count($grp_info);
		
		foreach($grp_info as $n=>$grp){
			$parents[$i]->groups[$n] = $grp;
		}
		
		$query = "SELECT a.parent_id, a.user_id, b.username, b.name, b.email"
		. "\n FROM #__lms_user_parents as a"
		. "\n, #__users as b"
		. "\n WHERE 1"
		. "\n AND a.parent_id = '".$parents[$i]->user_id."'"
		. "\n AND a.user_id = b.id"
		;
		$db->setQuery($query);
		$usr_info = $db->loadObjectList();
		
		$parents[$i]->count_users = count($usr_info);
		
		foreach($usr_info as $n=>$usr){
			$parents[$i]->users[$n] = $usr;
		}
		
	}
	
	$lists = array();

	joomla_lms_adm_html::JLMS_ViewParents( $parents, $lists, $pagination, $option );
}
function JLMS_editParent( $id, $option ) {
	global $JLMS_CONFIG;
	$db = JFactory::getDBO();

	$view_switch = intval(mosGetParam($_REQUEST, 'view_switch', 0));

	$parent_id = 0;
	$group_id = 0;
	$learners = array();
	$stores = array();
	$GLOBALS['jlms_toolbar_id'] = $id;

	if ($id) {
		$query = "SELECT parent_id FROM #__lms_user_parents WHERE id = $id";
		$db->SetQuery( $query );
		$parent_id = $db->LoadResult();
		
		if($view_switch){
			$parent_id = $id;
		}	
		
		if (!$parent_id) {
			$parent_id = 0;
		} else {
			if($JLMS_CONFIG->get('use_global_groups', 1)){
				$query = "SELECT a.user_id, u.username, u.name, u.email, ug.id as group_id, ug.ug_name"
				. "\n FROM #__lms_user_parents as a"
				. "\n LEFT JOIN #__lms_users_in_global_groups as ugg ON a.user_id = ugg.user_id"
				. "\n LEFT JOIN #__lms_usergroups as ug ON ug.id = ugg.group_id"
				. "\n, #__users as u"
				. "\n WHERE a.parent_id = $parent_id AND a.user_id = u.id"
				. "\n ORDER BY u.username, u.name, u.email";
				$db->SetQuery($query);
				$learners = $db->LoadObjectList();
				
				$tmp_ugname = array();
				$learner_id = 0;
				foreach($learners as $lrn){
					if($learner_id == 0 || $learner_id != $lrn->user_id){
						$learner_id = $lrn->user_id;
					}
					$tmp_ugname[$learner_id][] = isset($lrn->ug_name)?$lrn->ug_name:'';	
				}
				$tmp_learners = array();
				$last_u_id = 0;
				$i=0;
				foreach($tmp_ugname as $user_id=>$ug_names){
					foreach($learners as $lrn){
						if($user_id == $lrn->user_id && ($last_u_id == 0 || $lrn->user_id != $last_u_id)){
							$tmp_learners[$i] = $lrn;
							$tmp_learners[$i]->ug_name = implode("<br />", $tmp_ugname[$lrn->user_id]);
							$last_u_id = $lrn->user_id;
							$i++;	
						}
					}
				}
				$learners = $tmp_learners;
				
				$query = "SELECT upsi.*, ug.ug_name "
				. "\n FROM #__lms_user_assigned_groups as upsi, #__lms_usergroups as ug"
				. "\n WHERE 1"
				. "\n AND upsi.grp_id = ug.id"
				. "\n AND ug.course_id = 0"
				. "\n AND upsi.parent_id = $parent_id"
				. "\n ORDER BY ug.ug_name"
				;
				$db->setQuery($query);
				$stores = $db->loadObjectList();
			} else {
				$db->SetQuery("SELECT a.user_id, u.username, u.name, u.email FROM #__lms_user_parents as a, #__users as u WHERE a.parent_id = $parent_id AND a.user_id = u.id ORDER BY u.username, u.name, u.email");
				$learners = $db->LoadObjectList();
			}
		}
	}
	
	$lists = array();
	$query = "SELECT id as value, username as text, name, email FROM #__users ORDER BY username";
	$db->SetQuery($query);
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_CEO_SLCT_PAR_USERNAME_);
	$pr = $db->loadObjectList();
	$i = 0;
	while ($i < count($pr)) {
		$pr[$i]->text = $pr[$i]->text . " (".$pr[$i]->name.", ".$pr[$i]->email.")";
		$i ++;
	}
	$list_users = array_merge( $list_users, $pr );
	$lists['parents'] = mosHTML::selectList( $list_users, 'parent_id', 'class="text_area" style="width:300px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', $parent_id );
	$list_users[0]->text = _JLMS_CEO_SLCT_LEN_USERNAME_;
	$lists['users'] = mosHTML::selectList( $list_users, 'user_id', 'class="text_area" id="jlms_user_ids" style="width:300px" size="1" onChange="jlms_changeUserSelect_stu(this);" ', 'value', 'text', null );
	if (class_exists('JToolBarHelper')) { // Joomla 1.5
		$lists['users'] = str_replace('id="user_id"', 'id="jlms_user_ids"', $lists['users']);
	}

	$query = "SELECT id as value, name as text FROM #__users ORDER BY name";
	$db->SetQuery($query);
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_CEO_NAME_ );
	$pr = $db->loadObjectList();
	$list_users = array_merge( $list_users, $pr );
	$lists['parents_names'] = mosHTML::selectList( $list_users, 'parent_name', 'class="text_area" style="width:300px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', $parent_id );
	$lists['users_names'] = mosHTML::selectList( $list_users, 'user_name', 'class="text_area" id="jlms_user_names" style="width:300px" size="1" onChange="jlms_changeUserSelect_stu(this);" ', 'value', 'text', null );
	if (class_exists('JToolBarHelper')) { // Joomla 1.5
		$lists['users_names'] = str_replace('id="user_name"', 'id="jlms_user_names"', $lists['users_names']);
	}

	$query = "SELECT id as value, email as text FROM #__users ORDER BY email";
	$db->SetQuery($query);
	$list_users = array();
	$list_users[] = mosHTML::makeOption( '0', _JLMS_CEO_OR_EMAIL_ );
	$pr = $db->loadObjectList();
	$list_users = array_merge( $list_users, $pr );
	$lists['parents_emails'] = mosHTML::selectList( $list_users, 'parent_email', 'class="text_area" style="width:300px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', $parent_id );
	$lists['users_emails'] = mosHTML::selectList( $list_users, 'user_email', 'class="text_area" id="jlms_user_emails" style="width:300px" size="1" onChange="jlms_changeUserSelect_stu(this);" ', 'value', 'text', null );
	if (class_exists('JToolBarHelper')) { // Joomla 1.5
		$lists['users_emails'] = str_replace('id="user_email"', 'id="jlms_user_emails"', $lists['users_emails']);
	}
	
	if($JLMS_CONFIG->get('use_global_groups', 1)){
		$query = "SELECT id as value, ug_name as text FROM #__lms_usergroups WHERE course_id = 0 ORDER BY ug_name";
		$db->SetQuery($query);
		$list_groups = array();
		$list_groups[] = mosHTML::makeOption( '0', _JLMS_CEO_SLCT_GR_NAME_ );
		$grps = $db->loadObjectList();
		$list_groups = array_merge( $list_groups, $grps );
		$lists['groups'] = mosHTML::selectList( $list_groups, 'group_id', 'class="text_area" id="jlms_group_ids" style="width:300px" size="1" ', 'value', 'text', $group_id );
		if (class_exists('JToolBarHelper')) { // Joomla 1.5
			$lists['groups'] = str_replace('id="group_id"', 'id="jlms_group_ids"', $lists['groups']);
		}
		
		$lists['groups_store'] = mosHTML::selectList( $list_groups, 'group_id', 'class="text_area" id="jlms_group_store_ids" style="width:300px" size="1" ', 'value', 'text', $group_id );
		if (class_exists('JToolBarHelper')) { // Joomla 1.5
			$lists['groups_store'] = str_replace('id="group_id"', 'id="jlms_group_store_ids"', $lists['groups_store']);
		}
	}
	$lists['view_switch'] = $view_switch;
	
	$row = new stdClass();
	$row->id = $id;
	joomla_lms_adm_html::JLMS_editParentHtml( $row, $lists, $option, $learners, $stores );
}
function JLMS_saveParent( $option ) {
	$db = JFactory::getDBO();
	
	$view_switch = intval(mosGetParam($_REQUEST, 'view_switch', 0));
	
	$parent_id = intval(mosGetParam($_REQUEST, 'parent_id', 0));
	
	if ($parent_id){
		//extra BrustersIceCream
		$store_grp_ids = mosGetParam($_REQUEST, 'jq_hid_store_grp_ids', array());
		
		$query = "DELETE FROM #__lms_user_assigned_groups WHERE parent_id = $parent_id";
		$db->setQuery( $query );
		$db->query();
		
		if(count($store_grp_ids)){
			$query = "INSERT INTO #__lms_user_assigned_groups (parent_id, grp_id) VALUES";
			$i = 0;
			foreach($store_grp_ids as $grp_id){
				$query .= "('".$parent_id."', '".$grp_id."')".(($i == (count($store_grp_ids) - 1))?'':', ');	
				$i ++;
			}
			$db->SetQuery( $query );
			$db->query();	
		}
		
		
		$query = "DELETE FROM #__lms_user_parents WHERE parent_id = $parent_id";
		$db->SetQuery( $query );
		$db->query();
		$cid = mosGetParam( $_POST, 'jq_hid_fields_ids', array(0) );
		if (!empty($cid)) {
			$cid = array_unique($cid);
			$cids = implode(',', $cid);
			$query = "SELECT id FROM #__users WHERE id IN ($cids) AND id <> ".$parent_id;
			$db->SetQuery( $query );
			$uids = JLMSDatabaseHelper::loadResultArray();
			if (!empty($uids)) {
				$query = "INSERT INTO #__lms_user_parents (parent_id, user_id) VALUES ";
				$i = 0;
				foreach ($uids as $u_id) {
					$query .= "($parent_id, $u_id)".(($i == (count($uids) - 1))?'':', ');
					$i ++;
				}
				$db->SetQuery( $query );
				$db->query();
			}
		}
		
		$cid_grp = mosGetParam( $_POST, 'jq_hid_fields_grp_ids', array() );
		if(!empty($cid_grp)){
			$cid_grp = array_unique($cid_grp);
			$cids_grp = implode(',', $cid_grp);	
			$query = "SELECT a.user_id"
			. "\n FROM #__lms_users_in_global_groups as a, #__users as b"
			.(count($uids) ? "\n , #__lms_user_parents as c" : '')
			. "\n WHERE a.user_id = b.id AND a.group_id IN (".$cids_grp.")"
			.(count($uids) ? "\n AND a.user_id NOT IN (".implode(",", $uids).")" : '')
			. "\n AND a.user_id <> ".$parent_id."";
			$db->SetQuery( $query );
			$uids_grp = JLMSDatabaseHelper::loadResultArray();
			
			if (!empty($uids_grp)) {
				$query = "INSERT INTO #__lms_user_parents (parent_id, user_id) VALUES ";
				$i = 0;
				foreach ($uids_grp as $u_id) {
					$query .= "($parent_id, $u_id)".(($i == (count($uids_grp) - 1))?'':', ');
					$i ++;
				}
				$db->SetQuery( $query );
				$db->query();
			}
		}
	}
	$link_r = "index.php?option=$option&task=view_parents";
	$link_r .= $view_switch?"&view_switch=".$view_switch:"";
	mosRedirect( $link_r );
}

function JLMS_removeParent( $cid, $option ) {
	$db = JFactory::getDBO();

	$view_switch = intval(mosGetParam($_REQUEST, 'view_switch', 0));

	if(isset($cid) && count($cid)){
		if($view_switch){
			$query = "SELECT parent_id FROM #__lms_user_parent_store WHERE id IN (".implode(',', $cid).")";
			$db->setQuery($query);
			$del_parent_ids = JLMSDatabaseHelper::loadResultArray();
			
			$query = "DELETE FROM #__lms_user_parent_store WHERE id IN (".implode(',', $cid).")";
			$db->SetQuery( $query );
			$db->query();
			
			if(count($del_parent_ids)){
				$query = "DELETE FROM #__lms_user_assigned_groups WHERE parent_id IN (".implode(',', $del_parent_ids).")";
				$db->SetQuery( $query );
				$db->query();	
			}
		} else {
			$query = "DELETE FROM #__lms_user_parents WHERE id IN (".implode(',', $cid).")";
			$db->SetQuery( $query );
			$db->query();
		}
	}
	$link_r = "index.php?option=$option&task=view_parents";
	$link_r .= $view_switch?"&view_switch=".$view_switch:"";
	mosRedirect( $link_r );
}

//---------------MESSAGES CONFIG----------------////
function JLMS_MailSupList($option) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );

	$db->setQuery("SELECT COUNT(*) FROM #__lms_messagelist");
	$total = $db->loadResult();

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	$db->setQuery( "SELECT * FROM #__lms_messagelist", $pageNav->limitstart, $pageNav->limit );
	$rows = $db->loadObjectlist();
	joomla_lms_adm_html::JLMS_MailSupList($rows, $pageNav, $option);

}
function JLMS_MailSupEdit($cid, $option) {
	$db = JFactory::getDBO();
	$row = new stdClass();
	$GLOBALS['jlms_mailsup_cid'] = $cid;
	if($cid)
	{
		$query = "SELECT * FROM #__lms_messagelist WHERE id=".$cid;
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		if(count($rows))
		$row = $rows[0];
	}

	joomla_lms_adm_html::JLMS_MailSupEdit($row, $option);
}
function JLMS_MailSupSave($option, $task) {
	$db = JFactory::getDBO();
	$zid = intval(mosGetParam($_POST, 'sm_id', 0));
	$pm_name = strval(mosGetParam($_POST, 'pm_name', ''));
	$pm_email = strval(mosGetParam($_POST, 'pm_email', ''));
	$pm_name = (get_magic_quotes_gpc()) ? stripslashes( $pm_name ) : $pm_name;
	$pm_email = (get_magic_quotes_gpc()) ? stripslashes( $pm_email ) : $pm_email;
	if($zid)
	{
		$query = "UPDATE #__lms_messagelist SET pm_name=".$db->Quote($pm_name).", pm_email=".$db->Quote($pm_email)." WHERE id=".$zid;
		$db->setQuery($query);
		$db->query();
	}
	else {
		$query = "INSERT INTO #__lms_messagelist(id,pm_name,pm_email) VALUES('', ".$db->Quote($pm_name).", ".$db->Quote($pm_email).")";
		$db->setQuery($query);
		$db->query();
	}
	if ($task == 'mailsup_apply') {
		mosRedirect( "index.php?option=$option&task=mailsup_edit&id=".$zid );
	} else {
		mosRedirect( "index.php?option=$option&task=mailsup_list" );
	}
}

function JLMS_MailSupDelete($cid, $option) {
	$db = JFactory::getDBO();
	$cids = implode(",", $cid);
	$query = "DELETE FROM #__lms_messagelist WHERE id IN (".$cids.")";
	$db->setQuery($query);
	if($db->query()){
		mosRedirect( "index.php?option=$option&task=mailsup_list" );
	}
}

function JLMS_MailSupConf($option) {
	$db = JFactory::getDBO();
	$db->setQuery("SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'mess_enotify'");
	$mess_enotify = $db->loadResult();
	$db->setQuery("SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'mess_alearn'");
	$mess_alearn = $db->loadResult();
	$db->setQuery("SELECT message_value FROM `#__lms_message_configuration` WHERE message_conf = 'mail_title'");
	$mail_title = $db->loadResult();
	$db->setQuery("SELECT message_value FROM `#__lms_message_configuration` WHERE message_conf = 'mail_body'");
	$mail_body = $db->loadResult();
	joomla_lms_adm_html::JLMS_MailSupConf($mess_enotify, $mess_alearn, $mail_title, $mail_body, $option);
}
function JLMS_MailSupConfSave($option) {
	$db = JFactory::getDBO();
	$mess_enotify = intval(mosGetParam($_POST, 'mess_enotify', 0));
	$mess_alearn = intval(mosGetParam($_POST, 'mess_alearn', 0));
	$mail_title = strval(mosGetParam($_POST, 'mail_title', ''));
	$mail_body = strval(mosGetParam($_POST, 'mail_body', ''));
	$mail_title = (get_magic_quotes_gpc()) ? stripslashes( $mail_title ) : $mail_title;
	$mail_body = (get_magic_quotes_gpc()) ? stripslashes( $mail_body ) : $mail_body;
	$db->SetQuery("SELECT count(*) FROM `#__lms_config` WHERE lms_config_var = 'mess_enotify'");
	if (!$db->loadResult()) {
		$db->setQuery("INSERT INTO `#__lms_config` (lms_config_value, lms_config_var) VALUES (".$mess_enotify.", 'mess_enotify')");
		$db->query();
	} else {
		$db->setQuery("UPDATE `#__lms_config` SET lms_config_value=".$mess_enotify." WHERE lms_config_var = 'mess_enotify' ");
		$db->query();
	}
	/*$db->SetQuery("SELECT count(*) FROM `#__lms_config` WHERE lms_config_var = 'mess_alearn'");
	if (!$db->loadResult()) {
		$db->setQuery("INSERT INTO `#__lms_config` (lms_config_value, lms_config_var) VALUES (".$mess_alearn.", 'mess_alearn')");
		$db->query();
	} else {
		$db->setQuery("UPDATE `#__lms_config` SET lms_config_value=".$mess_alearn." WHERE lms_config_var = 'mess_alearn' ");
		$db->query();
	}*/
	$db->SetQuery("SELECT count(*) FROM `#__lms_message_configuration` WHERE message_conf = 'mail_title'");
	if (!$db->loadResult()) {
		$db->setQuery("INSERT INTO `#__lms_message_configuration` (message_value, message_conf) VALUES (".$db->Quote($mail_title).", 'mail_title')");
		$db->query();
	} else {
		$db->setQuery("UPDATE `#__lms_message_configuration` SET message_value=".$db->Quote($mail_title)." WHERE message_conf = 'mail_title' ");
		$db->query();
	}
	$db->SetQuery("SELECT count(*) FROM `#__lms_message_configuration` WHERE message_conf = 'mail_body'");
	if (!$db->loadResult()) {
		$db->setQuery("INSERT INTO `#__lms_message_configuration` (message_value, message_conf) VALUES (".$db->Quote($mail_body).", 'mail_body')");
		$db->query();
	} else {
		$db->setQuery("UPDATE `#__lms_message_configuration` SET message_value=".$db->Quote($mail_body)." WHERE message_conf = 'mail_body' ");
		$db->query();
	}
	mosRedirect( "index.php?option=$option&task=mailsup_conf" );
}
function JLMS_ListCoursesTemplate( $option ) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "courses_template_viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "courses_template_view{$option}limitstart", 'limitstart', 0 ) );
	$query = "SELECT COUNT(*) FROM #__lms_courses_template";
	$db->setQuery( $query );
	$total = $db->loadResult();

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	$query = "SELECT * FROM #__lms_courses_template ORDER BY templ";
	$db->setQuery($query, $limitstart, $limit);
	$rows = $db->loadObjectList();
	joomla_lms_adm_html::JLMS_ListCoursesTemplate( $rows, $pageNav, $option);
}
function JLMS_ListCoursesTemplAdd( $cid, $option ) {
	$db = JFactory::getDBO();
	$row = array();
	$GLOBALS['jlms_toolbar_id'] = $cid;
	if($cid)
	{
		$db->setQuery("SELECT * FROM #__lms_courses_template WHERE id=".$cid);
		$rows = $db->loadObjectList();
		$row = $rows[0];
	}
	$db->setQuery("SELECT id,course_name FROM #__lms_courses ORDER BY course_name");
	$cours = $db->loadObjectList();
	$status_filter[] = mosHTML::makeOption(0, _JLMS_CRSS_SLCT_CRS_,'id','course_name' );
	$cours = array_merge($status_filter,$cours);
	$lists['courses_list']  = mosHTML::selectList( $cours, 'courses_list','style="width: 266px;" class="text_area"', 'id', 'course_name', '');
	joomla_lms_adm_html::JLMS_ListCoursesTemplAdd( $row, $lists, $option);
	//jlms_backup_folder
}
function JLMS_ListCoursesTemplSave( $option ) {
	$db = JFactory::getDBO();
	$tbl_id = intval(mosGetParam($_POST, 'tpl_id', 0));
	$course_id = intval(mosGetParam($_POST, 'courses_list', 0));
	$templ = mosGetParam($_POST, 'templ', '');
	$templ = (get_magic_quotes_gpc()) ? stripslashes( $templ ) : $templ;
	$query = '';
	
	if($tbl_id)
	{
		$query = "UPDATE #__lms_courses_template SET templ=".$db->quote($templ)." WHERE id=".$tbl_id;

	}
	else {
		if ($course_id) {
			$db->setQuery("SELECT course_name FROM #__lms_courses WHERE id=".$course_id);
			$cours_name = $db->loadResult();
			require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "jlms_course_export.php");
			$filename = JLMS_courseExport ($option, $course_id, "tpl");
			$query = "INSERT INTO #__lms_courses_template(id,course_id,templ,course_name,filename,data) VALUES ('', ".$db->quote($course_id).", ".$db->quote($templ).", ".$db->quote($cours_name).", ".$db->quote($filename).",'".date("Y-m-d")."') ";
		}
	}
	
	$msg = '';
	if ($query) {
		$db->setQuery($query);
		$db->query();
	} else {
		$msg = _JLMS_MAIL_CRS_NOT_SLCT;
	}
	
	mosRedirect( "index.php?option=$option&task=courses_template" );

}
function JLMS_ListCoursesTemplDel( $cid, $option ) {
	$db = JFactory::getDBO();
	if(count($cid))
	{
		$query = "SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'jlms_backup_folder'";
		$db->setQuery($query);
		$filepath = $db->loadResult();
		for($i=0;$i<count($cid);$i++)
		{
			$query = "SELECT filename FROM #__lms_courses_template WHERE id=".$cid[$i];
			$db->setQuery($query);
			$filename = $db->loadResult();
			unlink($filepath.'/'.$filename);
		}
		$cids = implode(',',$cid);
		$query = "DELETE FROM #__lms_courses_template WHERE id IN (".$cids.")";
		$db->setQuery($query);
		$db->query();
	}
	mosRedirect( "index.php?option=$option&task=courses_template" );
}



//plugin system implementation by TPETb
function JLMS_showPluginsList( $option ) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$files = array();
	// install new processors found and delete with the files missed	
	$plugins_dir = JPATH_SITE.'/components/com_joomla_lms/includes/plugins';
	$handle = opendir($plugins_dir);
	while (false !== ($dir_name = readdir($handle))) {
		if (is_dir($plugins_dir.'/'.$dir_name) && $dir_name != "." && $dir_name != "..") {
			$dir = opendir($plugins_dir.'/'.$dir_name);
			while (false !== ($file = readdir($dir))) {
				if ($file != "." && $file != ".." && preg_match('/\.xml$/',$file)) {
					$tmp = array('dir' => $dir_name, 'file' => $file);
					$files[]=$tmp;
				}
			}
		}

	}
	closedir($handle);
	
	if (sizeof($files)) {
		for ($i=0; $i<count($files); $i++) {
			// check if new
			$query = "SELECT COUNT(*) FROM #__lms_plugins WHERE element = '".str_replace('.xml','',$files[$i]['file'])."' AND folder = '".$files[$i]['dir']."'";
			$db->setQuery( $query );
			$total = $db->loadResult();
			$phpfile = JPATH_SITE.'/components/com_joomla_lms/includes/plugins/'.$files[$i]['dir'].'/'.str_replace('.xml','.php',$files[$i]['file']);
			$xmlfile = JPATH_SITE.'/components/com_joomla_lms/includes/plugins/'.$files[$i]['dir'].'/'.$files[$i]['file'];		
			
			
			if ($total == 0 && file_exists($xmlfile)) {			
				// xml file for module
				$xmlDoc = JLMSFactory::getXMLParser();
				if (!method_exists($xmlDoc,'loadFile'))
				{					
					$pluginName = str_replace('.xml','',$files[$i]['file']);										
					
					$xmlDoc = JFactory::getXML($xmlfile);
					$root = &$xmlDoc;
					$attributes = $root->attributes();				
					
					if( $root->name() == 'jlmsplugin' && $attributes->type == 'plugin' && $attributes->group != $files[$i]['dir'] ) {						
						$app->enqueueMessage(str_replace( '{plg_name}', $pluginName, _JLMS_PLGS_MSG_WRONG_DIR ), 'error');
					} else if ($root->name() == 'jlmsplugin' && $attributes->type == 'plugin' ) {									
								$row = new mosLMSplugin( $db );								
								$element = &$root->name;
								$row->name = $element ? trim( $element) : '';
								$element = &$root->short_description;								
								$row->short_description = $element ? trim( $element) : '';
									
						$row->element = $pluginName;
						$row->folder = $files[$i]['dir'];
						
								$order = $root->order;								
								$row->ordering = $order;
						
						if( $row->ordering == '') 
						{
							$query = 'SELECT max(ordering) FROM #__lms_plugins WHERE folder ='.$db->quote( $row->folder ).' GROUP BY folder';
							$db->setQuery( $query );
							$max_ordering = (int) $db->loadResult();				
							
							$row->ordering = ($max_ordering +1);						
						}

						require_once($phpfile);
						if(function_exists('install')){
							install();
						}
				
						if ($row->store()) {							
							$app->enqueueMessage($row->name.' '._JLMS_PLGS_MSG_INSTALLED);
						} else {							
							$app->enqueueMessage(str_replace( '{plg_name}', $pluginName, _JLMS_PLGS_MSG_ERROR_INSTALL ), 'error');
						}
					} else {						
						$app->enqueueMessage(str_replace( '{plg_name}', $pluginName, _JLMS_PLGS_MSG_NOT_FIND_XML ), 'error');
					}					
				}else{				
					if ($xmlDoc->loadFile( $xmlfile )) 
					{
						$root = &$xmlDoc->document;
						$pluginName = str_replace('.xml','',$files[$i]['file']);
						if( $root->name() == 'jlmsplugin' && $root->attributes( 'type' ) == 'plugin' && $root->attributes( 'group' ) != $files[$i]['dir'] ) 
						{							
							$app->enqueueMessage(str_replace( '{plg_name}', $pluginName, _JLMS_PLGS_MSG_WRONG_DIR ), 'error');
						} else if ($root->name() == 'jlmsplugin' && $root->attributes( 'type' ) == 'plugin' ) 
						{													
							$row = new mosLMSplugin( $db );
							
							$element = &$root->getElementByPath( 'name' );
							$row->name = $element ? trim( $element->data() ) : '';
							$element = &$root->getElementByPath( 'short_description' );
							$row->short_description = $element ? trim( $element->data() ) : '';
																	
							$row->element = $pluginName;
							$row->folder = $files[$i]['dir'];
							
							$order = $root->getElementByPath( 'order' );				
							$row->ordering = $order->data();
							
							if( $row->ordering == '') 
							{
								$query = 'SELECT max(ordering) FROM #__lms_plugins WHERE folder ='.$db->quote( $row->folder ).' GROUP BY folder';
								$db->setQuery( $query );
								$max_ordering = (int) $db->loadResult();				
								
								$row->ordering = ($max_ordering +1);						
							}

							require_once($phpfile);
							if(function_exists('install')){
								install();
							}
					
							if ($row->store()) {								
								$app->enqueueMessage($row->name.' '._JLMS_PLGS_MSG_INSTALLED);
							} else {								
								$app->enqueueMessage(str_replace( '{plg_name}', $pluginName, _JLMS_PLGS_MSG_ERROR_INSTALL ), 'error');
							}
						} else {							
							$app->enqueueMessage(str_replace( '{plg_name}', $pluginName, _JLMS_PLGS_MSG_NOT_FIND_XML ), 'error');
						}
					}
				}				
			}
		}
	}
	
	
	$query = "SELECT * FROM #__lms_plugins";
	$db->setQuery( $query );
	$row2 = $db->loadObjectList();
	for ($i=0; $i<count($row2); $i++) {
		if (!file_exists(JPATH_SITE.'/components/com_joomla_lms/includes/plugins/'.$row2[$i]->folder.'/'.$row2[$i]->element.'.xml') || !file_exists(JPATH_SITE.'/components/com_joomla_lms/includes/plugins/'.$row2[$i]->folder.'/'.$row2[$i]->element.'.php')) {
			$query = "DELETE FROM #__lms_plugins WHERE id='".$row2[$i]->id."'";
			$db->setQuery( $query );
			$db->query();
			@unlink(JPATH_SITE.'/components/com_joomla_lms/includes/plugins/'.$row2[$i]->folder.'/'.$row2[$i]->element.'.xml');
			@unlink(JPATH_SITE.'/components/com_joomla_lms/includes/plugins/'.$row2[$i]->folder.'/'.$row2[$i]->element.'.php');			
			$app->enqueueMessage(_JLMS_PLGS_MSG_F_FOR.' '.$row2[$i]->name.' '._JLMS_PLGS_MSG_F_MISSED, 'error');
		}
	}

	// show available plugins

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );

	// get the total number of records
	$query = "SELECT COUNT(*) FROM #__lms_plugins";
	$db->setQuery( $query );
	$total = $db->loadResult();
	
	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	// get the subset (based on limits) of required records
	$query = "SELECT * "
	. "\n FROM #__lms_plugins"
	. "\n ORDER BY folder, ordering "
	. "\n LIMIT $pageNav->limitstart, $pageNav->limit"
	;
	$db->setQuery( $query );
	$rows = $db->loadObjectList();	
	
	joomla_lms_adm_html::showPluginsList( $rows, $pageNav, $option );
}

function changePlugin( $cid=null, $state=0, $option ) {
	$db = JFactory::getDBO();

	if (!is_array( $cid ) || count( $cid ) < 1) {
		$msg = $publish ? _JLMS_MSG_SLCT_I_TO_PUBL : _JLMS_MSG_SLCT_I_TO_UNPUBL;
		echo "<script> alert('".$msg."'); window.history.go(-1);</script>\n";
		exit();
	}

	$cids = implode( ',', $cid );

	$query = "UPDATE #__lms_plugins SET published = " . intval( $state ). " WHERE id IN ( $cids )";
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}
	mosRedirect( "index.php?option=$option&task=pluginslist" );
}

function editPlugin( $uid, $option ) {
	$db = JFactory::getDBO();

	$lists = array();
	$row = new mosLMSplugin( $db );
	// load the row from the db table
	$row->load( (int)$uid );

	if ($uid) {
		if ( $row->ordering > -10000 && $row->ordering < 10000 ) {
			// build the html select list for ordering
			$query = "SELECT ordering AS value, name AS text"
			. "\n FROM #__lms_plugins"
			. "\n WHERE folder = " . $db->Quote ( $row->folder )			
			. "\n AND ordering > -10000"
			. "\n AND ordering < 10000"
			. "\n ORDER BY ordering"
			;
			$order = mosGetOrderingList( $query );
			$lists['ordering'] = mosHTML::selectList( $order, 'ordering', 'class="inputbox" size="1"', 'value', 'text', intval( $row->ordering ) );
		} else {
			$lists['ordering'] = '<input type="hidden" name="ordering" value="'. $row->ordering .'" />'._JLMS_PLGS_MSG_CANT_REORDERED;
		}
		$lists['folder'] = '<input type="hidden" name="folder" value="'. $row->folder .'" />'. $row->folder;
		
		// xml file for module
		$xmlfile = JPATH_SITE.'/components/com_joomla_lms/includes/plugins/' .$row->folder . '/' . $row->element .'.xml';
		$xmlDoc = JLMSFactory::getXMLParser();			
		if (!JLMS_J30version())
		{
		if ( $xmlDoc->loadFile( $xmlfile ) ) 
		{		
			$root = &$xmlDoc->document;
			if ($root->name() == 'jlmsplugin' ) {
				$element = &$root->getElementByPath( 'description' );				
				$row->description = $element ? trim( $element->data() ) : '';
				$element = &$root->getElementByPath( 'short_description' );
				$row->short_description = $element ? trim( $element->data() ) : '';				
			}
		}
		}else
		{			
			$xmlDoc = JFactory::getXML($xmlfile);			
			$root = &$xmlDoc;		
			if ($root->name() == 'jlmsplugin' ) {
				$element = &$root->description;				
				$row->description = $element ? trim( $element) : '';
				$element = &$root->short_description;
				$row->short_description = $element ? trim( $element) : '';				
			}
		}
	} else {
		mosRedirect( "index.php?option=$option&task=pluginslist", _JLMS_PLGS_MSG_NOT_FOUND );
	}

	JlmsViewPlugin::editPage( $row, $lists, $params, $option );
}

function savePlugin( $option, $task ) {
	$db = JFactory::getDBO();
	$params = mosGetParam( $_POST, 'params', '' );
	
	if (is_array( $params )) {
		$txt = array();
		foreach ($params as $k=>$v) {
			$v1 = get_magic_quotes_gpc() ? stripslashes( $v ) : $v;
			$txt[] = "$k=$v1";
		}
		$_POST['params'] = mosParameters::textareaHandling( $txt );
	}	
	
	$row = new mosLMSplugin( $db );
	
	
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}	
	
	$row->name = get_magic_quotes_gpc() ? stripslashes( $row->name ) : $row->name;

	// save the changes
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	
	$row->reorder( 'folder = '.$db->Quote($row->folder).' AND ordering > -10000 AND ordering < 10000' );
	
	if ($task == 'save_plugin'){
		mosRedirect( "index.php?option=$option&task=pluginslist", _JLMS_PLGS_MSG_DET_CHANGED );
	}else{
		mosRedirect( "index.php?option=$option&hidemainmenu=1&task=editA_plugin&id=".$row->id , _JLMS_PLGS_MSG_DET_CHANGED );
	}
}

function savePluginsOrder( $option )
{
	// Check for request forgeries
	JRequest::checkToken() or jexit( 'Invalid Token' );

	// Initialize some variables
	$db		=JFactory::getDBO();

	$cid 	= JRequest::getVar( 'cid', array(), 'post', 'array' );
	JArrayHelper::toInteger($cid);

	if (empty( $cid )) {
		JError::raiseWarning( 500, _JLMS_MSG_NO_ITEMS_SELECTED );
		mosRedirect( "index.php?option=$option&task=pluginslist");
	}

	$total		= count( $cid );
	$row = new mosLMSplugin( $db );
	$groupings = array();

	$order 		= JRequest::getVar( 'order', array(0), 'post', 'array' );
	JArrayHelper::toInteger($order);

	// update ordering values
	for ($i = 0; $i < $total; $i++)
	{
		$row->load( (int) $cid[$i] );
		// track postions
		$groupings[] = $row->folder;

		if ($row->ordering != $order[$i])
		{
			$row->ordering = $order[$i];
			if (!$row->store()) {
				return JError::raiseWarning( 500, $db->getErrorMsg() );
			}
		}
	}

	// execute updateOrder for each parent group
	$groupings = array_unique( $groupings );
	foreach ($groupings as $group){
		$row->reorder('folder = '.$db->Quote($group));
	}
	
	mosRedirect( "index.php?option=$option&task=pluginslist", _JLMS_MSG_NEW_ORDERING_SAVED );
}

function pluginsReorder( $option, $isup = false ) 
{	

	// Check for request forgeries
	JRequest::checkToken() or jexit( 'Invalid Token' );

	// Initialize some variables
	$db		=JFactory::getDBO();

	$cid 	= JRequest::getVar( 'cid', array(), 'post', 'array' );
	JArrayHelper::toInteger($cid);
	
	$inc	= ( $isup ? -1 : 1 );

	if (empty( $cid )) {
		return JError::raiseWarning( 500, _JLMS_MSG_NO_ITEMS_SELECTED );
	}

	$row = new mosLMSplugin( $db );
	$row->load( (int) $cid[0] );

	$row->move( $inc, 'folder = '.$db->Quote( $row->folder ) );
	
	mosRedirect( "index.php?option=$option&task=pluginslist");
}

function removePlugin( &$cid, $option ) {
	$db = JFactory::getDBO();

	if (count( $cid )) {
		for ($i=0; $i<count($cid); $i++) {
			$query = "SELECT element,folder FROM #__lms_plugins WHERE id='".$cid[$i]."' ";
			$db->setQuery( $query );
			$row = $db->loadObject();
			
			require_once(JPATH_SITE."/components/com_joomla_lms/includes/plugins/$row->folder/$row->element.php");
			if(function_exists('uninstall')){
				uninstall();
			}
			
			@unlink(JPATH_SITE."/components/com_joomla_lms/includes/plugins/$row->folder/$row->element.xml");
			@unlink(JPATH_SITE."/components/com_joomla_lms/includes/plugins/$row->folder/$row->element.php");
		}
		$cids = implode( ',', $cid );
		$query = "DELETE FROM #__lms_plugins"
		. "\n WHERE id IN ( $cids )"
		;
		$db->setQuery( $query );
		if (!$db->query()) {
			echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}
	mosRedirect( "index.php?option=$option&task=pluginslist", _JLMS_PLGS_MSG_DELETED );
}

function cancelPlugin( $id, $option ) {
	mosRedirect( "index.php?option=$option&task=pluginslist" );
}

function installPlugin( $id, $option ) 
{
		// Get the uploaded file information
		$userfile = JRequest::getVar('install_package', null, 'files', 'array' );
		
		$error = false;

		// Make sure that file uploads are enabled in php
		if (!(bool) ini_get('file_uploads')) 
		{
			JError::raiseWarning('installPlugin', JText::_('WARNINSTALLFILE'));
			$error = true;		
		}

		// Make sure that zlib is loaded so that the package can be unpacked
		if (!extension_loaded('zlib')) 
		{
			JError::raiseWarning('installPlugin', JText::_('WARNINSTALLZLIB'));
			$error = true;			
		}

		// If there is no uploaded file, we have a problem...
		if (!is_array($userfile) ) 
		{
			JError::raiseWarning('installPlugin', JText::_('No file selected'));
			$error = true;			
		}

		// Check if there was a problem uploading the file.
		if ( $userfile['error'] || $userfile['size'] < 1 )
		{
			JError::raiseWarning('installPlugin', JText::_('WARNINSTALLUPLOADERROR'));
			$error = true;			
		}
		
		// Build the appropriate paths
		$config =JFactory::getConfig();
		 
			 if (!JLMS_J30version())
			 {
		$tmp_path = $config->getValue('config.tmp_path');
			 
			 } else {
			 
			 $tmp_path = $config->get('tmp_path');
			 
			 }
		$tmp_dest 	= $tmp_path.DS.$userfile['name'];
		$tmp_src	= $userfile['tmp_name'];
		
		if( !is_writable( $tmp_path ) ) 
		{
			JError::raiseWarning('installPlugin', str_replace( '{dir}', $tmp_dest, JText::_( '_JLMS_MSG_DIR_IS_UNWRITABLE' ) ));
			$error = true;
		} else if( !$error ) {
			
			// Move uploaded file
			jimport('joomla.filesystem.file');
			jimport('joomla.installer.helper');		
			
			$uploaded = JFile::upload($tmp_src, $tmp_dest);
			
			// Unpack the downloaded package file		
			$tmpdir = uniqid('install_');
	
			// Clean the paths to use for archive extraction
			$extractdir = JPath::clean(dirname($tmp_dest).DS.$tmpdir);
			$archivename = JPath::clean($tmp_dest);
	
			// do the unpacking of the archive
			$result = JArchive::extract( $archivename, $extractdir);
			
			$files = JFolder::files($extractdir, '\.xml$', 1, true);
			
			$xmlDoc = JLMSFactory::getXMLParser();
			//$xmlDoc->resolveErrors( true );
					

				if (!method_exists($xmlDoc,'loadFile'))
				{
				$xmlDoc = JFactory::getXML($files[0]);
				$root = &$xmlDoc;
				
					if ($root->name() == 'jlmsplugin' && $root->attributes()->type == 'plugin' ) 
					{
						$group = $root->attributes()->group;				
					$dest = JPATH_SITE.DS.'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'plugins'.DS.$group;
					
					if( !is_writable( $dest ) ) 
					{
						JError::raiseWarning('installPlugin', str_replace( '{dir}', $dest, JText::_( '_JLMS_MSG_DIR_IS_UNWRITABLE' ) ));
						$error = true;
					} else {												 
						JFolder::copy( $extractdir, $dest, '', true );
					}				
				} else {
					echo "<script> alert('".str_replace( '{plg_name}', '', _JLMS_PLGS_MSG_NOT_FIND_XML )."'); window.history.go(-1); </script>\n"; exit();
				}
				
			
				}else
				{						
				$xmlDoc->loadFile( $files[0] );
				$root = &$xmlDoc->document;
				{
					if ($root->name() == 'jlmsplugin' && $root->attributes( 'type' ) == 'plugin' ) 
					{
						$group = $root->attributes( 'group' );				
						$dest = JPATH_SITE.DS.'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'plugins'.DS.$group;
						
						if( !is_writable( $dest ) ) 
						{
							JError::raiseWarning('installPlugin', str_replace( '{dir}', $dest, JText::_( '_JLMS_MSG_DIR_IS_UNWRITABLE' ) ));
							$error = true;
						} else {												 
							JFolder::copy( $extractdir, $dest, '', true );
						}				
					} else {
						echo "<script> alert('".str_replace( '{plg_name}', '', _JLMS_PLGS_MSG_NOT_FIND_XML )."'); window.history.go(-1); </script>\n"; exit();
					}
				}
			}
			
			JInstallerHelper::cleanupInstall($archivename, $extractdir);
		}
		
		if( !$error )
			mosRedirect( "index.php?option=$option&task=pluginslist", _JLMS_PLGS_MSG_INSTALLED );
		else
			mosRedirect( "index.php?option=$option&task=pluginslist" );
			
}

function showWaitingLists($option) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	$filt_course= intval( $app->getUserStateFromRequest( "filt_course{$option}", 'filt_course', 0 ) );

	$query = "SELECT c.id AS course_id, u.id AS user_id, c.course_name, u.username AS user_name, u.name AS user_fullname, wl.id AS id, wl.ordering AS ordering"
	. "\n FROM #__lms_waiting_lists AS wl"
	. "\n LEFT JOIN #__lms_courses AS c ON wl.course_id=c.id"
	. "\n LEFT JOIN #__users AS u ON wl.user_id=u.id"
	. "\n WHERE 1"
	. ($filt_course ? "\n AND c.id = '$filt_course'" : '' )
	. "\n ORDER BY ordering ASC";
	$db->setQuery($query);
	$rows = $db->loadObjectList();

	$total= count($rows);

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	//work out reorder button perms (quite stupid)
	prepareOrderingPerms($rows, 'course_id', 'ordering');
	for ($i=0; $i<$pageNav->limitstart; $i++) array_shift($rows);
	while (count($rows)>$pageNav->limit) array_pop($rows);

	//create lists
	$lists = array();
	$query = "SELECT a.id as value, a.course_name as text FROM #__lms_courses as a order by a.course_name";
	$db->setQuery( $query );
	$sf_courses = array();
	$sf_courses[] = mosHTML::makeOption( '0', _JLMS_WAIT_SLCT_CRS );
	$sf_courses = array_merge( $sf_courses, $db->loadObjectList() );
	$lists['jlms_courses'] = mosHTML::selectList( $sf_courses, 'filt_course', 'class="text_area" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $filt_course );

	joomla_lms_adm_html::showWaitingLists( $rows, $pageNav, $lists, $option );
}

function fixWaitingListOrdering ($course_id) {
	$db = JFactory::getDBO();
	$query = "SELECT * FROM #__lms_waiting_lists WHERE course_id=$course_id ORDER BY ordering ASC";
	$db->setQuery($query);
	$rows = $db->loadObjectList();

	$query = '';
	for ($i=0; $i<count($rows); $i++) {
		$query .= "\n UPDATE #__lms_waiting_lists SET ordering=$i WHERE id=".$rows[$i]->id;
	}
	$db->setQuery($query);
}

function orderWaitingListElement ($id, $inc, $option) {
	$db = JFactory::getDBO();
	$query = "SELECT * FROM #__lms_waiting_lists WHERE id=$id";
	$db->setQuery($query);
	$row = $db->loadObject();

	fixWaitingListOrdering($row->course_id);

	$query = "UPDATE #__lms_waiting_lists SET ordering=$row->ordering WHERE ordering=".($row->ordering + $inc)." AND course_id=$row->course_id";
	$db->setQuery($query);
	$db->query();

	$query = "UPDATE #__lms_waiting_lists SET ordering=".($row->ordering + $inc)." WHERE id=$id";
	$db->setQuery($query);
	$db->query();

	mosRedirect( "index.php?option=$option&task=show_waiting_lists", _JLMS_WAIT_USRS_REORDERED );
}

function addUserFromWaitingList ($cid, $option) {
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$db = JFactory::getDBO();

	$query = "SELECT * FROM #__lms_waiting_lists WHERE id IN (".implode('', $cid).")";
	$db->setQuery($query);
	$rows = $db->loadObjectList();

	$users_info = array();
	foreach ($rows as $row) {
		$query = "INSERT INTO `#__lms_users_in_groups` ( course_id , user_id ) VALUES ($row->course_id,$row->user_id)";
		$db->setQuery($query);
		$db->query();

		$user_info = new stdClass();
		$user_info->user_id = $row->user_id;
		$user_info->group_id = 0; //??? or what?
		$user_info->course_id = $row->course_id;
		$user_info->teacher_id = 0;
		$users_info[] = $user_info;

		$course_info = new stdClass();
		$course_info->course_id = $row->course_id;
	}

	$_JLMS_PLUGINS->loadBotGroup('course');
	$_JLMS_PLUGINS->trigger('onCourseAdd', array($users_info, $course_info));
	$_JLMS_PLUGINS->loadBotGroup('user');
	$_JLMS_PLUGINS->trigger('onCourseJoin', array($users_info));
	$_JLMS_PLUGINS->loadBotGroup('notifications');
	$_JLMS_PLUGINS->trigger('onCourseJoin', array($users_info));

	if (count($users_info)) {
		$redirect = urlencode("index.php?option=$option&task=show_waiting_lists");
		//TODO: do something with index3 !
		mosRedirect("index.php?tmpl=component&option=com_joomla_lms&task=mail_main&assigned=0&redirect=$redirect");
	}
	mosRedirect( "index.php?option=$option&task=show_waiting_lists", _JLMS_WAIT_MSG_USRS_ADDED_TO_CRS );
}

function removeUsersFromWaitingList ($cid, $option) {
	$db = JFactory::getDBO();
	$query = "DELETE FROM #__lms_waiting_lists WHERE id IN (".implode(',', $cid).")";
	$db->setQuery($query);
	$db->query();
	mosRedirect( "index.php?option=$option&task=show_waiting_lists", _JLMS_WAIT_MSG_USRS_REM );
}

function prepareOrderingPerms (&$rows, $group_field, $ordering_field='ordering') {
	$max_orderings = array();
	foreach ($rows as $row) {
		if (!isset($max_orderings[$row->$group_field]) || $max_orderings[$row->$group_field] < $row->$ordering_field) $max_orderings[$row->$group_field] = $row->$ordering_field;
		if (!isset($min_orderings[$row->$group_field]) || $min_orderings[$row->$group_field] > $row->$ordering_field) $min_orderings[$row->$group_field] = $row->$ordering_field;
	}
	foreach ($rows as $key => $row) {
		$rows[$key]->allow_up = ($row->$ordering_field > $min_orderings[$row->$group_field]) ? 1 : 0;
		$rows[$key]->allow_down = ($row->$ordering_field < $max_orderings[$row->$group_field]) ? 1 : 0;
	}
}


function JLMS_Look_Feel($option){
	$db = JFactory::getDBO();
	$row = new jlms_adm_config();
	$row->loadFromDb( $db );
	$lists['lofe_show_top']	= mosHTML::yesnoRadioList( 'lofe_show_top', 'class="inputbox"',	$row->lofe_show_top );
	$lists['lofe_menu_style']	= $row->lofe_menu_style;
	$sf_courses = array();
	$sf_courses[] = mosHTML::makeOption( '0', '16x16' );
	$sf_courses[] = mosHTML::makeOption( '1', '24x24' );
	$sf_courses[] = mosHTML::makeOption( '2', '32x32' );
	$lists['lofe_menu_style'] = mosHTML::selectList( $sf_courses, 'lofe_menu_style', 'class="text_area" size="1" ', 'value', 'text', $row->lofe_menu_style );
	
	//Show Status Lapths/Scorms //by Max - 25.02.2011
	$scorm_status_as = array();
	$scorm_status_as[] = mosHTML::makeOption( '0', 'progressbar' );
	$scorm_status_as[] = mosHTML::makeOption( '1', 'text' );
	$scorm_status_as[] = mosHTML::makeOption( '2', 'icon' );
	$lists['scorm_status_as'] = mosHTML::selectList( $scorm_status_as, 'scorm_status_as', 'class="text_area" size="1" ', 'value', 'text', isset($row->scorm_status_as) ? $row->scorm_status_as : 0 );
	
	//Show Status Lapths/Scorms //by Max - 25.02.2011
	$lpath_status_as = array();
	$lpath_status_as[] = mosHTML::makeOption( '0', 'progressbar' );
	$lpath_status_as[] = mosHTML::makeOption( '1', 'text' );
	$lpath_status_as[] = mosHTML::makeOption( '2', 'icon' );
	$lists['lpath_status_as'] = mosHTML::selectList( $lpath_status_as, 'lpath_status_as', 'class="text_area" size="1" ', 'value', 'text', isset($row->lpath_status_as) ? $row->lpath_status_as : 0 );
	
	//Show Status Quizzes //by Max - 13.05.2011
	$quiz_status_as = array();
	$quiz_status_as[] = mosHTML::makeOption( '0', 'points received  + "status icon"' );
	$quiz_status_as[] = mosHTML::makeOption( '3', 'points receive' );
	$quiz_status_as[] = mosHTML::makeOption( '4', 'percentage' );
	$quiz_status_as[] = mosHTML::makeOption( '5', 'percentage + "status icon"' );
	$quiz_status_as[] = mosHTML::makeOption( '1', 'complete / incomplete' );
	$quiz_status_as[] = mosHTML::makeOption( '2', 'passed / failed' );
	$lists['quiz_status_as'] = mosHTML::selectList( $quiz_status_as, 'quiz_status_as', 'class="text_area" size="1" ', 'value', 'text', isset($row->quiz_status_as) ? $row->quiz_status_as : 0 );

	$lists['lofe_show_head']	= mosHTML::yesnoRadioList( 'lofe_show_head', 'class="inputbox"',	$row->lofe_show_head );
	$lists['lofe_show_course_box']	= mosHTML::yesnoRadioList( 'lofe_show_course_box', 'class="inputbox"',	$row->lofe_show_course_box );
	$sf_box = array();
	$sf_box[] = mosHTML::makeOption( '0', 'selectbox' );
	$sf_box[] = mosHTML::makeOption( '1', 'js-based list' );
	$lists['lofe_box_type']	= mosHTML::selectList( $sf_box, 'lofe_box_type', 'class="text_area" size="1" ', 'value', 'text', $row->lofe_box_type );
	
	$lists['frontpage_courses']	= mosHTML::yesnoRadioList( 'frontpage_courses', 'class="inputbox"',	$row->frontpage_courses );
	$lists['frontpage_courses_expand_all']	= mosHTML::yesnoRadioList( 'frontpage_courses_expand_all', 'class="inputbox"',	$row->frontpage_courses_expand_all );
	$lists['frontpage_allcourses']	= mosHTML::yesnoRadioList( 'frontpage_allcourses', 'class="inputbox"',	$row->frontpage_allcourses );
	$lists['frontpage_announcements']	= mosHTML::yesnoRadioList( 'frontpage_announcements', 'class="inputbox"',	$row->frontpage_announcements );
	$lists['frontpage_homework']	= mosHTML::yesnoRadioList( 'frontpage_homework', 'class="inputbox"',	$row->frontpage_homework );
	$lists['frontpage_dropbox']	= mosHTML::yesnoRadioList( 'frontpage_dropbox', 'class="inputbox"',	$row->frontpage_dropbox );
	$lists['frontpage_mailbox']	= mosHTML::yesnoRadioList( 'frontpage_mailbox', 'class="inputbox"',	$row->frontpage_mailbox );
	$lists['frontpage_certificates']	= mosHTML::yesnoRadioList( 'frontpage_certificates', 'class="inputbox"',	$row->frontpage_certificates );
	$lists['frontpage_latest_forum_posts']	= mosHTML::yesnoRadioList( 'frontpage_latest_forum_posts', 'class="inputbox"',	$row->frontpage_latest_forum_posts );

	joomla_lms_adm_html::JLMS_cLook_Feel( $row, $lists, $option);
}
function JLMS_Look_Feel_save( $option ) {
	$db = JFactory::getDBO();
	$row = new jlms_adm_config();
	$row->loadFromDb( $db );
	$row->attendance_days = unserialize($row->attendance_days);
	$row->bind($_POST);
	$row->saveToDb( $db );
	mosRedirect ("index.php?option=com_joomla_lms&task=look_feel", _JLMS_LF_SETT_SAVED );
}

function FLMS_ListCategories($option){
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "jlms_cats_viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "jlms_cats_viewlimitstart", 'limitstart', 0 ) );
	$levellimit = intval( $app->getUserStateFromRequest( "jlms_cats_viewlimit", 'levellimit', 10 ) );

	$query = "SELECT id, c_category as name, c_category as title, parent, parent as parent_id"
	. "\n FROM #__lms_course_cats"
	. "\n ORDER BY c_category"
	;
	$db->setQuery( $query );
	$rows = $db->loadObjectList();
	
	// establish the hierarchy of the menu
	$children = array();
	// first pass - collect children
	foreach ($rows as $v ) {
		$pt = $v->parent;
		$list = (isset($children[$pt]) && $children[$pt] ) ? $children[$pt] : array();
		array_push( $list, $v );
		$children[$pt] = $list;
	}
	// second pass - get an indent list of the items
	$list = mosTreeRecurse( 0, '', array(), $children, max( 0, $levellimit-1 ) );
	// eventually only pick out the searched items.

	$total = count( $list );

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	$levellist = mosHTML::integerSelectList( 1, 20, 1, 'levellimit', 'size="1" class="jlms_level_limit" onchange="document.adminForm.submit();"', $levellimit );

	
	// slice out elements based on limits
	$list = array_slice( $list, $pageNav->limitstart, $pageNav->limit );

	joomla_lms_adm_html::FLMS_vListCategories($option, $list, $pageNav, $levellist);
}

function FLMS_EditCategories($uid, $option){
	$db = JFactory::getDBO();

	//tab1
	$menu = new mos_Joomla_LMS_Categories( $db );
	$menu->load( (int)$uid );

	$menu->name = $menu->c_category;
	if(!$menu->lesson_type){
		$menu->lesson_type = 1;	
	}
	
	// build the html select list for parent item
	$lists['parent'] = FLMS_parent( $menu->id, $menu->parent );

	$list = array();

	$javascript = ' onclick="javascrript:view_fields(this,0);"';
//	if(false){
	if($menu->parent){
		$disabled = ' disabled="disabled"';
	} else {
		$disabled = '';	
	}

	$lists['restricted_category'] = mosHTML::yesnoRadioList( 'restricted', 'id="restricted_radio" class="inputbox"'. $disabled . $javascript, $menu->restricted );

	$query = "SELECT groups FROM #__lms_course_cats WHERE id = '".$uid."'";
	$db->setQuery($query);
	$db->query();
	$groups = $db->loadResult();

	$groups = substr($groups,  1, (strlen($groups)-2));

	$groups_arr = explode('|',$groups);
	
	$query = "SELECT * FROM #__lms_usergroups WHERE course_id = 0 AND parent_id = 0";
	$db->setQuery($query);
	$db->query();
	$rows = $db->loadObjectList();
	
	if($menu->restricted && !$menu->parent){
		$disabled = '';	
	} else {
		$disabled = ' disabled="disabled"';
	}
	
	// assemble menu items to the array
	$select_list = '<select id="restricted_groups" class="text_area chzn-done" name="groups[]" size="12" multiple="multiple"'.$disabled.' style="width: 272px;">';
	for($i=0;$i<count($rows);$i++) {
		$selected = '';
		for($j=0;$j<count($groups_arr);$j++) {
			if($groups_arr[$j] == $rows[$i]->id)  {
				$selected = 'selected="selected"';	
				break;
			}			
		}
		$select_list .= '<option value="'.$rows[$i]->id.'" '.$selected.'>'.$rows[$i]->ug_name.'</option>';
	}
	$select_list .= '</select>';
	
	$lists['restricted_groups'] = $select_list;
	
	joomla_lms_adm_html::FLMS_vEditCategories( $menu, $lists, $list, $option );
}

function FLMS_SaveCategories($option, $task='multicat_save'){
	global $JLMS_CONFIG;
	$db = JFactory::getDBO();
	
	$save_mass = array();
	$save_mass['id'] = intval(mosGetParam( $_REQUEST, 'id', 0));
	$save_mass['c_category'] = mosGetParam( $_REQUEST, 'name', '');
	$save_mass['parent'] = intval(mosGetParam( $_REQUEST, 'parent', 0));
	$save_mass['lesson_type'] = intval(mosGetParam( $_REQUEST, 'lesson_type', 0));
	$save_mass['restricted'] = intval(mosGetParam( $_REQUEST, 'restricted', 0));
	
	if($save_mass['id']){
		$query = "SELECT * FROM #__lms_course_cats ORDER BY parent";
		$db->setQuery($query);
		$all_cats = $db->loadObjectList();
		
		$last_catid = $save_mass['id'];
		$tmp_cats_filter = array();
		$children = array();
		foreach($all_cats as $cat){
			$pt = $cat->parent;
			$list = @$children[$pt] ? $children[$pt] : array();
			array_push($list, $cat->id);
			$children[$pt] = $list;
		}
		$tmp_cats_filter[0] = $last_catid;
		$i=1;
		foreach($children as $key=>$childs){
			if($last_catid == $key){
				foreach($children[$key] as $v){
					if(!in_array($v, $tmp_cats_filter)){
						$tmp_cats_filter[$i] = $v;
						$i++;
					}
				}
			}
		}
		foreach($children as $key=>$childs){
			if(in_array($key, $tmp_cats_filter)){
				foreach($children[$key] as $v){
					if(!in_array($v, $tmp_cats_filter)){
						$tmp_cats_filter[$i] = $v;
						$i++;
					}
				}
			}
		}
		$tmp_cats_filter = array_unique($tmp_cats_filter);
		$cids = implode(",", $tmp_cats_filter);
		
		$query = "UPDATE #__lms_course_cats SET lesson_type = '".$save_mass['lesson_type']."' WHERE id IN (".$cids.")";
		$db->setQuery($query);
		$db->query();
	}
	
	if($save_mass['parent'] && !$save_mass['lesson_type']){
		$query = "SELECT lesson_type FROM #__lms_course_cats WHERE id = '".$save_mass['parent']."'";
		$db->setQuery($query);
		$lesson_type_up_level = $db->loadResult();
		$save_mass['lesson_type'] = $lesson_type_up_level;
	}
	
	$groups 	= mosGetParam( $_REQUEST, 'groups', array(0) );
	$restricted_category = intval(mosGetParam( $_REQUEST, 'restricted', 0));
	
	$row = new mos_Joomla_LMS_Categories( $db );

	if (!$row->bind( $save_mass )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	$row->c_category = ampReplace( $row->c_category );
	
	if (!$row->check()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	$groups_str = '';
	if ($restricted_category) {
		if(count($groups) && $groups[0] != 0) {
			$razd = '|';
			for($i=0;$i<count($groups);$i++) {
				$groups_str .= $razd.$groups[$i];
			}	
			$groups_str .= '|';
		}
	}

	$row->groups = $groups_str;
	
	if (!$JLMS_CONFIG->get('use_global_groups', 1) && $row->id){
		if($row->parent) {
			$row->restricted = '';
			$row->groups = '';
		}
		else {		
			unset($row->restricted);
			unset($row->groups);
		}
	}
	
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	$msg = _JLMS_LF_MSG_CAT_I_SAVED;
	switch ( $task ) {
		case 'multicat_apply':
			mosRedirect( 'index.php?option='.$option.'&task=multicat_editA&id='.$row->id.'&hidemainmenu=1' , $msg );
			break;

		case 'multicat_save':
		default:
			mosRedirect( 'index.php?option='.$option.'&task=multicat', $msg );
			break;
	}	
}

function FLMS_DeleteCategories($cid=NULL, $option){
	$db = JFactory::getDBO();
	
	$query = "SELECT * FROM #__lms_course_cats";
	$db->setQuery($query);
	$items = $db->loadObjectList();
	
	$deletes = array();
	
	$children = array();
	foreach ( $items as $item ) {
		if ( $item->parent == $cid[0] ) {
			$children[] = $item->id;
		}		
	}
	$deletes = FLMS_ChildrenRecurse( $items, $children, $children );
	$deletes = array_merge( array($cid[0]), $deletes );
	
	$parent = 0;
	foreach($items as $item){
		if($item->id == $cid[0]){
			$parent = $item->parent;	
		}
	}
	
	$deletes = implode(",", $deletes);
	$query = "UPDATE #__lms_courses SET cat_id = '".$parent."' WHERE cat_id IN (".$deletes.")";
	$db->setQuery($query);
	if($db->query()){
		$query = "DELETE FROM #__lms_course_cats WHERE id IN (".$deletes.")";
		$db->setQuery($query);
		$db->query();
	}

	$msg = _JLMS_LF_MSG_CAT_I_DELETED;
	mosRedirect( 'index.php?option='.$option.'&task=multicat', $msg );
}

function FLMS_ChildrenRecurse( $mitems, $parents, $list, $maxlevel=20, $level=0 ) {
	// check to reduce recursive processing
	if ( $level <= $maxlevel && count( $parents ) ) {
		$children = array();
		foreach ( $parents as $id ) {			
			foreach ( $mitems as $item ) {
				if ( $item->parent == $id ) {
					$children[] = $item->id;
				}		
			}
		}	
		// check to reduce recursive processing
		if ( count( $children ) ) {
			$list = FLMS_ChildrenRecurse( $mitems, $children, $list, $maxlevel, $level+1 );
			$list = array_merge( $list, $children );
		}
	}
	return $list;
}

function FLMS_parent($ex_id = 0, $selected = 0, $sb_name='parent', $show_root = true){
	$db = JFactory::getDBO();

	$id = '';
	if ( $ex_id ) {
		$id = "\n AND id != " . (int) $ex_id;
	}

	// get a list of the menu items
	// excluding the current menu item and its child elements
	$query = "SELECT id, c_category as name, parent, c_category as title, parent as parent_id"
	. "\n FROM #__lms_course_cats"
	. "\n WHERE 1"
	. $id
	. "\n ORDER BY parent, c_category"
	;
	$db->setQuery( $query );
	$mitems = $db->loadObjectList();

	// establish the hierarchy of the menu
	$children = array();

	if ( $mitems ) {
		// first pass - collect children
		foreach ( $mitems as $v ) {
			$pt 	= $v->parent;
			$list 	= @$children[$pt] ? $children[$pt] : array();
			array_push( $list, $v );
			$children[$pt] = $list;
		}
	}
	// second pass - get an indent list of the items
	$list = mosTreeRecurse( 0, '', array(), $children, 20, 0, 0 );
	
	$javascript = ' onchange="view_fields(this,1);"';

	// assemble menu items to the array
	$mitems 	= array();
	$all_options = '';
	$param_selected = '';
	if($selected == 0){
		$param_selected = 'selected="selected"';
	}
	if ($show_root) {
		$mitems[] 	= mosHTML::makeOption( '0', _JLMS_FLMS_TOP );
		$all_options .= '<option value="0" '.$param_selected.'>'._JLMS_FLMS_TOP.'</option>'. "\n";
	}
	
	$param_selected = '';
	foreach ( $list as $item ) {
		$mitems[] = mosHTML::makeOption( $item->id, ($show_root ? '&nbsp;&nbsp;&nbsp;' : ''). $item->treename );
		$param_selected = '';
		if($selected == $item->id){
			$param_selected = 'selected="selected"';
		}
		$all_options .= '<option value="'.$item->id.'" '.$param_selected.'>'.$item->treename.'</option>'. "\n";
	}

	//$output = mosHTML::selectList( $mitems, $sb_name, 'class="text_area" size="12"'.(is_array($selected) ? ' multiple="multiple"' : '').' style="width: 272px;"'. $javascript, 'value', 'text', $selected );
	$output = '<select id="'.$sb_name.'" name="'.$sb_name.'" class="text_area" size="12"'.(is_array($selected) ? ' multiple="multiple"' : '').' style="width: 272px;"'.$javascript.'>' . "\n";
	$output .= $all_options;
	$output .= '</select>';
	return $output; 	
}

function FLMS_CategoriesConfig($option){
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$levellimit = intval( $app->getUserStateFromRequest( "view{$option}limit", 'levellimit', 10 ) );
	$levellist = mosHTML::integerSelectList( 1, 20, 1, 'levellimit', 'size="1" onchange="document.adminForm.submit();"', $levellimit );
	
	$query = "SELECT * FROM #__lms_course_cats_config WHERE id > 0 ORDER BY id";
	$db->setQuery($query);
	$row = $db->loadObjectList();
	
	$rows = array();
	for($i=0;$i<$levellimit;$i++){
		$num = $i + 1;
		$rows[$i] = new stdClass();
		$rows[$i]->id = $i;
		$rows[$i]->cat_name = (isset($row[$i]->cat_name) && $row[$i]->cat_name!='')?$row[$i]->cat_name:'';
	}
	
	joomla_lms_adm_html::FLMS_vCategoriesConfig($rows, $levellist, $option);
}

function FLMS_CategoriesConfigSave($option){
	$db = JFactory::getDBO();
	
	$query = "SELECT * FROM #__lms_course_cats_config";
	$db->setQuery($query);
	$all = $db->loadObjectList();
	
	$i=1;
	foreach($_POST as $key=>$item){
		if(substr($key, 0, 9) == 'titlecat_'){
			if(isset($all[$i-1])){
				$query = "UPDATE #__lms_course_cats_config SET id = '".$i."', cat_name = '".$item."' WHERE id = '".$i."'";
				$db->setQuery($query);
				$db->query();
			} else {
				$query = "INSERT INTO #__lms_course_cats_config (id, cat_name) VALUES (".$i.", '".$item."')";
				$db->setQuery($query);
				$db->query();	
			}
			$i++;	
		}
	}
	
	$all_count = count($all);
	$delete = array();
	for($j=$i;$j<$all_count+1;$j++){
		$delete[] = $j;	
	}
	$delete_str = implode(",", $delete);
	
	if(count($delete)){
		$query = "DELETE FROM #__lms_course_cats_config WHERE id IN (".$delete_str.")";
		$db->setQuery($query);
		$db->query();
	}
	
	$msg = _JLMS_LF_MSG_CONFIG_SAVED;
	mosRedirect( 'index.php?option='.$option.'&task=multicat_config' , $msg );
}
function JLMS_orderCourse( $uid, $inc, $option ) {
	global $JLMS_CONFIG;
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$filt_cat = intval( $app->getUserStateFromRequest( "filt_cat{$option}", 'filt_cat', 0 ) );
	$m_filt = '';
	if(!defined('_JLMS_COURSES_COURSES_GROUPS')){
		define('_JLMS_COURSES_COURSES_GROUPS', _JLMS_LF_CRS_CATS );
	}
	//FLMS multicat
	$levels = array();
	if ($JLMS_CONFIG->get('multicat_use', 0)){
		$query = "SELECT * FROM #__lms_course_cats_config ORDER BY id";
		$db->setQuery($query);
		$levels = $db->loadObjectList();
		if(count($levels) == 0){
			for($i=0;$i<5;$i++){
				if($i>0){
					$levels[$i]->cat_name = _JLMS_COURSES_COURSES_GROUPS;	
				} else {
					$levels[$i]->cat_name = _JLMS_COURSES_COURSES_GROUPS;
				}
			}
		}
		$lists['levels'] = $levels;

		$level_id = array();
		$old_level_id = array();
		for($i=0;$i<count($levels);$i++){
			if($i == 0){
				$level_id[$i] = intval( $app->getUserStateFromRequest( "filter_id_".$i."{$option}", "filter_id_".$i."", 0 ) );
				$parent_id[$i] = 0;
				if(intval( mosGetParam( $_REQUEST, 'filter_id_'.$i.'', 0 ) )) 
					$m_filt .= '&filter_id_'.$i.'='.intval( mosGetParam( $_REQUEST, 'filter_id_'.$i.'', 0 ) );
			} else {
				$level_id[$i] = intval( $app->getUserStateFromRequest( "filter_id_".$i."{$option}", "filter_id_".$i."", 0 ) );
				$parent_id[$i] = $level_id[$i-1];
				if(intval( mosGetParam( $_REQUEST, 'filter_id_'.$i.'', 0 ) )) 
					$m_filt .= '&filter_id_'.$i.'='.intval( mosGetParam( $_REQUEST, 'filter_id_'.$i.'', 0 ) );
			}
			$query = "SELECT count(id) FROM `#__lms_course_cats` WHERE parent = '".$parent_id[$i]."' ORDER BY c_category";
			$db->setQuery($query);
			$groups = $db->loadResult();
			if($groups==0){
				$level_id[$i] = $_REQUEST['filter_id_'.$i.''] = 0;	
			}
		}
		
		for($i=0;$i<count($levels);$i++){
			if($i > 0 && $level_id[$i - 1] == 0){
				$level_id[$i] = $_REQUEST['filter_id_'.$i.''] = 0;
				$parent_id[$i] = 0;
			} elseif($i == 0 && $level_id[$i] == 0) {
				$level_id[$i] = $_REQUEST['filter_id_'.$i.''] = 0;
				$parent_id[$i] = 0;
			}
		}
		
		$javascript = 'onclick="javascript:read_filter();" onchange="javascript:write_filter();document.adminForm.task.value=\'courses\';document.adminForm.submit();"';
		for($i=0;$i<count($levels);$i++) {
			if( $parent_id[$i] == 0) {
				$query = "SELECT * FROM `#__lms_course_cats` WHERE `parent` = '0'";
				$query .= "\n ORDER BY `c_category`";
			}
			else {
				$query = "SELECT * FROM `#__lms_course_cats` WHERE parent = '".$parent_id[$i]."' ORDER BY c_category";
			}
			$db->setQuery($query);
			$groups = $db->loadObjectList();
			
			if($parent_id[$i] && $i > 0 && count($groups)) {
				$type_level[$i][] = mosHTML::makeOption( 0, ' &nbsp; ' );
				foreach ($groups as $group){
					$type_level[$i][] = mosHTML::makeOption( $group->id, $group->c_category );
				}
				$lists['filter_'.$i.''] = mosHTML::selectList($type_level[$i], 'filter_id_'.$i.'', 'class="inputbox" size="1" '.$javascript, 'value', 'text', $level_id[$i] ); //onchange="document.location.href=\''. $link_multi .'\';"
			} elseif($i == 0) {
				$type_level[$i][] = mosHTML::makeOption( 0, ' &nbsp; ' );
				foreach ($groups as $group){
					$type_level[$i][] = mosHTML::makeOption( $group->id, $group->c_category );
				}
				$lists['filter_'.$i.''] = mosHTML::selectList($type_level[$i], 'filter_id_'.$i.'', 'class="inputbox" size="1" '.$javascript, 'value', 'text', $level_id[$i] ); //onchange="document.location.href=\''. $link_multi .'\';"
			}
		}	
	} else {
		$lists = array();
		$query = "SELECT a.id as value, a.c_category as text FROM #__lms_course_cats as a order by a.c_category";
		$db->setQuery( $query );
		$sf_cats = array();
		$sf_cats[] = mosHTML::makeOption( '0', _JLMS_LF_SLCT_CAT );
		$sf_cats = array_merge( $sf_cats, $db->loadObjectList() );
		$lists['jlms_course_cats'] = mosHTML::selectList( $sf_cats, 'filt_cat', 'class="text_area" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $filt_cat );
	}
	//FLMS multicat
	$where = '';
	if ($JLMS_CONFIG->get('multicat_use', 0)){
		//NEW MUSLTICATS
		$tmp_level = array();
		$last_catid = 0;
		if(isset($_REQUEST['category_filter']) && $_REQUEST['category_filter']){
			$last_catid = $_REQUEST['category_filter'];
		} else {
			$i=0;
			foreach($_REQUEST as $key=>$item){
				if(preg_match('#filter_id_(\d+)#', $key, $result)){
					if($item){
						$tmp_level[$i] = $result;
						$last_catid = $item;
						$i++;
					}	
				}	
			}
		}
		
		$query = "SELECT * FROM #__lms_course_cats ORDER BY id";
		$db->setQuery($query);
		$all_cats = $db->loadObjectList();
		
		$tmp_cats_filter = array();
		$children = array();
		foreach($all_cats as $cat){
			$pt = $cat->parent;
			$list = @$children[$pt] ? $children[$pt] : array();
			array_push($list, $cat->id);
			$children[$pt] = $list;
		}
		$tmp_cats_filter[0] = $last_catid;
		$i=1;
		foreach($children as $key=>$childs){
			if($last_catid == $key){
				foreach($children[$key] as $v){
					if(!in_array($v, $tmp_cats_filter)){
						$tmp_cats_filter[$i] = $v;
						$i++;
					}
				}
			}
		}
		foreach($children as $key=>$childs){
			if(in_array($key, $tmp_cats_filter)){
				foreach($children[$key] as $v){
					if(!in_array($v, $tmp_cats_filter)){
						$tmp_cats_filter[$i] = $v;
						$i++;
					}
				}
			}
		}
		$tmp_cats_filter = array_unique($tmp_cats_filter);
		$catids = implode(",", $tmp_cats_filter);
		
		if($last_catid && count($tmp_cats_filter)){
			$where .= "\n ( cat_id IN (".$catids.")";
			if($JLMS_CONFIG->get('sec_cat_use', 0)){
				foreach ($tmp_cats_filter as $tmp_cats_filter_one) {
					$where .= "\n OR sec_cat LIKE '%|".$tmp_cats_filter_one."|%'";
				}
			}
			$where .= "\n )";
		}
	} else {
		$where .= $filt_cat ? "cat_id = '$filt_cat'" : "";
	}
	$row = new mos_Joomla_LMS_Course( $db );
	$row->load( $uid );
	$row->move( $inc, $where);

	mosRedirect( 'index.php?option='. $option ."&task=courses".$m_filt );
}
function JLMS_course_saveOrder( &$cid, $option) {
	$db = JFactory::getDBO();

	$total		= count( $cid );
	$order 		= josGetArrayInts( 'order' );

	$row		= new mos_Joomla_LMS_Course( $db );
	$conditions = array();

	// update ordering values
	for( $i=0; $i < $total; $i++ ) {
		$row->load( (int) $cid[$i] );
		if ($row->ordering != $order[$i]) {
			$row->ordering = $order[$i];
			if (!$row->store()) {
				echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
				exit();
			}
			// remember to updateOrder this group
			$condition = "";
			$found = false;
			
			if (!$found) $conditions[] = array($row->id, $condition);
		}
	}

	// execute updateOrder for each group
	foreach ( $conditions as $cond ) {
		$row->load( $cond[0] );
		$row->updateOrder( $cond[1] );
	}

	$msg 	= _JLMS_LF_MSG_ORDER_SAVED;
	mosRedirect( 'index.php?option='. $option .'&task=courses' , $msg );
}

function JLMS_ListNotifications() {
	global $option;
	//TODO: remove global option declaration
	
	$app = JFactory::getApplication('administrator');
	$db = JFactory::getDBO();
	
	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );
	
	$notification_events = NotificationsManager::getNotificationEvents();
	
	$rows = array();
	
	$oneIsEnabled = false;
				
	if( $notification_events ) 
	{
		$sql = "SELECT * FROM #__lms_email_notifications";
		$db->setQuery( $sql );
		$rows = $db->loadObjectList('id');		
					
		for( $i=0; $i<count($notification_events); $i++ ) 
		{
			$notification_event = $notification_events[$i];			
			$notific_type =  NotificationsManager::getNotificationType( $notification_event->notification_type );		
			
			if( isset($rows[$notification_event->id]) ) 
			{	
				$row = $rows[$notification_event->id];														
				$notification_event->disabled = $row->disabled;
				$notification_event->learner_template = $row->learner_template;
				$notification_event->manager_template = $row->manager_template;			
			}
			
			if( !$notification_event->disabled ) 
			{
				$oneIsEnabled = true;
			}	
											
			$notifications[$notific_type->name][] = $notification_event;		
		}								 
	}
	
	$query = "SELECT count(1) FROM #__lms_plugins WHERE element = 'emails' AND published = 1";
	$db->setQuery( $query );
	if( !$db->loadResult() && $oneIsEnabled ) 
	{
		$app->enqueueMessage( _JLMS_NOTS_MSG_PLUGIN_IS_NOT_ENABLED, 'error');			
	}
			
	joomla_lms_adm_html::JLMS_ListNotifications( $notifications, $option );
}

function JLMS_EditNotification( $id ) {
	global $option;
	//TODO remove global option;
	$db = JFactory::getDBO();
	
	$notification_event = NotificationsManager::getNotificationEvent( $id );
	$email_templates = NotificationsManager::getEmailTemplatesByNotificType( $notification_event->notification_type );
	$notification_type = NotificationsManager::getNotificationType( $notification_event->notification_type );
			
	$sql = "SELECT * FROM #__lms_email_templates WHERE notification_type = '".$notification_event->notification_type."' AND disabled = '0'";
	$db->setQuery( $sql );
	$e_tpls = $db->loadObjectList();
	
	if( $e_tpls ) 
	{
		if( $email_templates ) 
		{
			$email_templates = array_merge($email_templates, $e_tpls);		
		} else 
		{
			$email_templates = $e_tpls;
		}		
	}	
				
	$sql = "SELECT * FROM #__lms_email_notifications WHERE id = '".$id."'";
	$db->setQuery( $sql );
	$row = $db->loadObject();
			
	if( $row ) 
	{		
		$notification_event->learner_template = $row->learner_template;
		$notification_event->manager_template = $row->manager_template;
		
		if( $notification_event->selected_manager_roles )
			$notification_event->selected_manager_roles = explode( ',', $row->selected_manager_roles );
		else
			$notification_event->selected_manager_roles = array(0);			
		
		$notification_event->learner_template_disabled = $row->learner_template_disabled;
		$notification_event->manager_template_disabled = $row->manager_template_disabled;
		$notification_event->disabled = $row->disabled;				 
	}
	
	$where = '';	
	if( is_array($notification_type->manager_role_types) ) 
	{		
		$where = " WHERE roletype_id IN (".implode(',', $notification_type->manager_role_types).")";
	}
	
	$query = "SELECT id as value, lms_usertype as text, roletype_id, IF(roletype_id = 4, 1, IF(roletype_id = 2, 2, 3)) as ordering FROM #__lms_usertypes ".$where." ORDER BY ordering, id, lms_usertype";
	$db->SetQuery($query);
	$roles = $db->LoadObjectList();	
				
	$cur_role = $notification_event->selected_manager_roles;
	
	$sel_html = '<select id="roles_selections" name="selected_manager_roles[]" multiple="multiple" class="text_area" style="width:322px" size=15>';
	//$sel_html .= '<option value="0"></option>';
	$prev_roletype = 0;
	foreach ($roles as $role) {
		if ($role->roletype_id != $prev_roletype) {
			if ($prev_roletype) { $sel_html .= '</optgroup>'; }
			$prev_roletype = $role->roletype_id;
			if ($role->roletype_id == 4) {
				$sel_html .= '<optgroup label="'._JLMS_ADMIN_ROLES.'">';
			}
			if ($role->roletype_id == 2) {
				$sel_html .= '<optgroup label="'._JLMS_TEACHER_ROLES.'">';
			}
			if ($role->roletype_id == 5) {
				$sel_html .= '<optgroup label="'._JLMS_ASSISTANT_ROLES.'">';
			}
			if ($role->roletype_id == 3) {
				$sel_html .= '<optgroup label="'._JLMS_STAFF_ROLES.'">';
			}
		}
		$selected = '';
		if ( in_array( $role->value, $cur_role ) ) {
			$selected = ' selected="selected"';
		}
		$sel_html .= '<option value="'.$role->value.'"'.$selected.'>'.$role->text.'</option>';
	}
	$sel_html .= '</optgroup>';
	$sel_html .= '</select>';
	$lists['jlms_roles'] = $sel_html;
	
	if( $notification_event->use_learner_template ) {		
		$templates_list = array();
		$templates_list[] = mosHTML::makeOption( '0', _JLMS_NOTS_SLCT_TMP_);
						
		if( $email_templates[0] ) {			
			foreach( $email_templates AS $email_template ) 
			{
				$temlate = new stdClass();
				$temlate->text = $email_template->name;
				$temlate->value = $email_template->id;
				$templates_list[] = $temlate; 
			}	
		}	
				
		$lists['jlms_learner_email_template'] = mosHTML::selectList( $templates_list, 'learner_email_template', ' class="text_area" size="1" style="width:322px;"', 'value', 'text', $notification_event->learner_template );
	}
		
	if( $notification_event->use_manager_template ) {
		$templates_list = array();
		$templates_list[] = mosHTML::makeOption( '0', _JLMS_NOTS_SLCT_TMP_);
					
		if( $email_templates[0] ) {			
			foreach( $email_templates AS $email_template ) 
			{
				$temlate = new stdClass();
				$temlate->text = $email_template->name;
				$temlate->value = $email_template->id;
				$templates_list[] = $temlate; 
			}	
		}	
			
		$lists['jlms_manager_email_template'] = mosHTML::selectList( $templates_list, 'manager_email_template', ' class="text_area" size="1" style="width:322px;"', 'value', 'text', $notification_event->manager_template );
	}		
					
	joomla_lms_adm_html::JLMS_EditNotification( $notification_event, $option, $lists );	
}

function JLMS_SaveNotification( $id ) {
	global $option, $task;
	//TODO: remove globals

	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$subject = JRequest::getVar( 'subject' );
	$learner_template = JRequest::getInt( 'learner_email_template' );
	$manager_template = JRequest::getInt( 'manager_email_template' );
	$learner_template_disabled = JRequest::getInt( 'learner_template_disabled', 0 );
	$manager_template_disabled = JRequest::getInt( 'manager_template_disabled', 0 );
	$selected_manager_roles = JRequest::getVar( 'selected_manager_roles' );
	$disabled = JRequest::getBool( 'enabled' )?0:1;
				
	if( is_array( $selected_manager_roles ) ) 
	{
		$selected_manager_roles = implode(',', $selected_manager_roles);
	}
						
	$sql = "SELECT count(*) FROM #__lms_email_notifications				   
				WHERE id = '".$id."'";
	$db->setQuery( $sql );
			
	if( !$db->loadResult() ) 
	{				
		$sql = "INSERT INTO #__lms_email_notifications( `id`, `learner_template`, `manager_template`, `selected_manager_roles`, `learner_template_disabled`, `manager_template_disabled`, `disabled` ) VALUES('".$id."','".$learner_template."','".$manager_template."','".$selected_manager_roles."','".$learner_template_disabled."','".$manager_template_disabled."','".$disabled."')";		
	} else {		
		$sql = "UPDATE #__lms_email_notifications 
					SET  `learner_template` = '".$learner_template."'
		 				,`manager_template` = '".$manager_template."'
						,`selected_manager_roles` = '".$selected_manager_roles."'
						,`learner_template_disabled` = '".$learner_template_disabled."'
						,`manager_template_disabled` = '".$manager_template_disabled."'						
						,`disabled` = '".$disabled."'     
					WHERE id = '".$id."'";		
	}			
	
	$db->setQuery( $sql );
	if ( $db->Query() ) 
	{
		if( !$id )
			$id = $db->insertid();
			
		$msg = _JLMS_NOTS_MSG_SAVED;	
	} else {
		//TODO: check if function 'enqueueMessage' is still exists in application		
		$app->enqueueMessage( $db->getErrorMsg(), 'error' );
		$msg= '';			
	}
						
	if( $task == 'apply_notification' ) {		
		$app->redirect( 'index.php?option='. $option .'&task=edit_notification&id='.$id , $msg, 'message' );
	} else {
		$app->redirect( 'index.php?option='. $option .'&task=notifications' , $msg, 'message' );
	}		
}

function JLMS_EnableNotification( $id, $enable = true ) {
	global $option, $task;
	//TODO: remove globals

	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$sql = "SELECT count(*) FROM #__lms_email_notifications				   
				WHERE id = '".$id."'";
	$db->setQuery( $sql );
	if( !$db->loadResult() ) 
	{
		$notification_event = NotificationsManager::getNotificationEvent( $id );
		
		$selected_manager_roles = '';	
		if(isset($notification_event->selected_manager_roles[0])) {	
			$selected_manager_roles = implode($notification_event->selected_manager_roles);
		} 
		
		$sql = "INSERT INTO #__lms_email_notifications( `id`, `learner_template`, `manager_template`, `selected_manager_roles`, `learner_template_disabled`, `manager_template_disabled`, `disabled` ) VALUES( '".$id."','".$notification_event->learner_template."','".$notification_event->manager_template."','".$selected_manager_roles."','".$learner_template_disabled."','".$manager_template_disabled."','".($enable?0:1)."')";
		$db->setQuery( $sql );
		$db->Query();
	} else {
		$sql = "UPDATE #__lms_email_notifications 
				SET disabled = '".($enable?0:1)."'   
				WHERE id = '".$id."'";
		$db->setQuery( $sql );
		$db->Query();			
	}
				
	$app->redirect( 'index.php?option='. $option .'&task=notifications' );	
}

function JLMS_ListEmailTemplates() {
	global $option; //TODO: remove global option

	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );

	$sql = "SELECT count(*) FROM #__lms_email_templates";
	$db->setQuery( $sql );
	$total = $db->loadResult();

	$sql = "SELECT id, name, subject, disabled, '0' AS native FROM #__lms_email_templates";
	$db->setQuery( $sql );
	$rows = $db->loadObjectList();

	$nativeEmailTemplates = NotificationsManager::getEmailTemplates();

	$nativeRows = array();

	foreach( $nativeEmailTemplates AS  $natTpl ) 
	{
		$nativeTpl = new stdClass();
		$nativeTpl->id = $natTpl->id;
		$nativeTpl->name = isset($natTpl->name)?$natTpl->name:'no name';
		$nativeTpl->subject = isset($natTpl->subject)?$natTpl->subject:'no subject';
		$nativeTpl->disabled = false;
		$nativeTpl->native = true;
		$nativeRows[] = $nativeTpl; 
	}

	if( isset( $nativeRows[0] ) && isset( $rows[0] ) ) {
		$rows = array_merge($nativeRows, $rows);
	} elseif( isset( $nativeRows[0] ) ) {
		$rows = $nativeRows;
	}
	
	jimport('joomla.html.pagination');
	$pageNav = new JPagination( count($rows), $limitstart, $limit  );

	$rows = array_slice( $rows, $pageNav->limitstart, $pageNav->limit);

	joomla_lms_adm_html::JLMS_ListEmailTemplates( $rows, $pageNav, $option );
}

function JLMS_EditEmailTemplate( $id = 0 ) {
	global $option, $templateid;
	//TODO: global tempalteid ? what is it?
	
	$db = JFactory::getDBO();

	$row = new stdClass();
	$row->id = 0;
	$row->name = '';
	$row->subject = '';
	$row->template_html = '';
	$row->template_alt_text = '';
	$row->disabled = 1;	
	$row->native = false;	
	$row->notification_type = 0;

	if( $id ) {					
		$row = NotificationsManager::getEmailTemplate( $id );	
	}	

	$row->template_html = $row->template_html;
	$row->template_alt_text = $row->template_alt_text;

	//variable used in toolbar.joomla_lms.html
	$templateid = $row->id;	

	$notification_types = NotificationsManager::getNotificationTypes();

	$notification_types_list = array();
	$notification_types_list[] = mosHTML::makeOption( '0', _JLMS_NOTS_SLCT_N_TYPE_);	

	$markers_tips = '';

	if( $notification_types[0] ) {			
		foreach( $notification_types AS $notification_type ) 
		{
			$notific_type = new stdClass();
			$notific_type->text = $notification_type->name;
			$notific_type->value = $notification_type->id;
			$notification_types_list[] = $notific_type;

			$display = ($row->notification_type == $notific_type->value)?"":"style=\"display: none;\"";  
			$markers_tips .= "<span $display id=\"markers_tip".$notific_type->value."\">".$notification_type->markers."</span>";  
		}	
	}

	$disabled = $row->native?'disabled="disabled"':'';		

	$lists['jlms_notification_types'] = mosHTML::selectList( $notification_types_list, 'notification_type', ' class="text_area" '.$disabled.' size="1" style="width:322px;" onchange="change_markers_tip( this );"', 'value', 'text', $row->notification_type );	

	$lists['jlms_markers_tips']	= $markers_tips;

	joomla_lms_adm_html::JLMS_EditEmailTemplate( $row, $option, $lists );	
}

function JLMS_SaveEmailTemplate( $id ) {
	global $option, $task;
	//TODO: remove globals

	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$name = JRequest::getVar( 'name' );
	$subject = JRequest::getVar( 'subject' );
		
	$post_body_html = JRequest::getVar('body_html', '', '', '', JREQUEST_ALLOWHTML ); 
	$post_body_text = JRequest::getVar('body_text', '', '', '', JREQUEST_ALLOWHTML);
		
	$notification_type = JRequest::getInt( 'notification_type' );
	$disabled = JRequest::getBool( 'enabled' )?0:1;
		
	if( $id ) {			
		$sql = "UPDATE #__lms_email_templates 
					SET name = '".$name."'
						,subject = '".$subject."'
						,notification_type = ".$db->quote($notification_type)."
						,body_html = ".$db->quote($post_body_html)."
					    ,body_text = ".$db->quote($post_body_text)."					    
						,disabled = '".$disabled."'   
					WHERE id = '".$id."'";			
	} else {
		$query = "SELECT MAX(id) FROM #__lms_email_templates";
		$db->setQuery( $query );
		$maxId = $db->loadResult();
			
		$setCustomId = "";
		if( $maxId < 100 ) 
		{			
			$setCustomId = "id = 101, ";	
		}
	
		$sql = "INSERT INTO #__lms_email_templates 
					SET ".$setCustomId."
						name = '".$name."' 
						,subject = '".$subject."'
						,notification_type = ".$db->quote($notification_type)."
						,body_html = ".$db->quote($post_body_html)."
					    ,body_text = ".$db->quote($post_body_text)."					    
						,disabled = '".$disabled."'";						
	}	

	$db->setQuery( $sql );

	if ( $db->Query() ) 
	{
		if( !$id )
			$id = $db->insertid();
			
		$msg = _JLMS_NOTS_MSG_TPL_SAVED;	
	} else {
		//TODO: check if function enqueueMessage is still exists in application
		$app->enqueueMessage( $db->getErrorMsg(), 'error' );
		$msg= '';			
	}

	if( $task == 'apply_email_template' ) {		
		$app->redirect( 'index.php?option='. $option .'&task=edit_email_template&id='.$id , $msg, 'message' );
	} else {
		$app->redirect( 'index.php?option='. $option .'&task=email_templates' , $msg, 'message' );
	}		
}

function JLMS_EnableEmailTemplate( $id, $enable = true ) {
	global $option;

	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$sql = "UPDATE #__lms_email_templates 
				SET disabled = '".($enable?0:1)."'   
				WHERE id = '".$id."'";
	$db->setQuery( $sql );
	$db->Query();

	$app->redirect( 'index.php?option='. $option .'&task=email_templates' );	
}

function JLMS_DeleteEmailTemplates( $cid ) {
	global $option;
	
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');
		
	$sql = "DELETE FROM #__lms_email_templates				   
				WHERE id IN (".implode( ',', $cid ).")";
	$db->setQuery( $sql );
	if ( $db->Query() ) 
	{
		$msg = (count($cid)>1)?_JLMS_NOTS_MSG_TPLS_DELETED:_JLMS_NOTS_MSG_TPL_DELETED;	
	} else {
		$msg = '';//$db->getErrorMsg();
		$app->enqueueMessage( $db->getErrorMsg(), 'error' );
	}
				
	$app->redirect( 'index.php?option='. $option .'&task=email_templates', $msg, 'message' );	
}

function JLMS_showPlans($option) {
	$app = JFactory::getApplication('administrator');
	$db = JFactory::getDBO();

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );

	$query = "SELECT COUNT(*) FROM #__lms_plans AS cd";
	$db->setQuery( $query );
	$total = $db->loadResult();

	require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
	$pageNav = new JLMSPagination( $total, $limitstart, $limit  );

	$query = "SELECT *  FROM #__lms_plans AS cd LIMIT $pageNav->limitstart, $pageNav->limit";
	$db->setQuery( $query );
	$rows = $db->loadObjectList();

	joomla_lms_adm_html::JLMS_showPlans( $rows, $pageNav, $option);
}

function JLMS_editPlan( $id, $option ) {
	$db = JFactory::getDBO();
	$user = JLMSFactory::getUser();

	$row = new mos_Joomla_LMS_plan( $db );
	$row->load( $id );

	if ($id) {
		$row->checkout($user->get('id'));
	} 
	
	$lists = array();

	$targets = array();
	$targets[] = mosHTML::makeOption( 'D', _JLMS_DAYS );
	$targets[] = mosHTML::makeOption( 'W', _JLMS_WEEKS );
	$targets[] = mosHTML::makeOption( 'M', _JLMS_MONTHS );
	$targets[] = mosHTML::makeOption( 'Y', _JLMS_YEARS );

	$lists['t1'] = mosHTML::selectList( $targets, 't1', 'class="inputbox" size="4"', 'value', 'text', $row->t1 );
	$lists['t2'] = mosHTML::selectList( $targets, 't2', 'class="inputbox" size="4"', 'value', 'text', $row->t2 );
	$lists['t3'] = mosHTML::selectList( $targets, 't3', 'class="inputbox" size="4"', 'value', 'text', $row->t3 );
	$lists['src'] = mosHTML::yesnoradioList( 'src', '', $row->src );
	$lists['sra'] = mosHTML::yesnoradioList( 'sra', '', $row->sra );

	$lists['published'] = mosHTML::yesnoradioList( 'published', '', $row->published );
	$lists['params'] = mosHTML::yesnoradioList( 'params', '', $row->params );

	joomla_lms_adm_html::JLMS_editPlan( $row, $lists, $option);
}

function JLMS_savePlan( $option ) {
	$db = JFactory::getDBO();

	$row = new mos_Joomla_LMS_plan( $db );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	// pre-save checks
	if (!$row->check()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	// save the changes
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	$row->checkin();//TODO: checkin / ceckout .... don't think that necessary fields for checkin/checkout are exist in our DB

	mosRedirect( "index.php?option=$option&task=plans", _JLMS_PLANS_MSG_SAVED );
}

function JLMS_removePlan( &$cid, $option ) {
	$db = JFactory::getDBO();
	if (count( $cid )) {
		$cids = implode( ',', $cid );
		$query = "DELETE FROM #__lms_plans WHERE id IN ( $cids )";
		$db->setQuery( $query );
		if (!$db->query()) {
			echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}

	mosRedirect( "index.php?option=$option&task=plans", 'The plans were successfully deleted' );
	//TODO: translations
}

function JLMS_duplicatePlan( $cid=null, $option ) {
	$db = JFactory::getDBO();

	if (!is_array( $cid ) || count( $cid ) < 1) {
		echo "<script> alert('Select an item to Duplicate!'); window.history.go(-1);</script>\n";
		//TODO: translations
		exit();
	}

	foreach($cid as $id) {
		$site = new mos_Joomla_LMS_plan( $db );
		$site->load($id);
		$site->id = '';
		$site->name = 'Copy of '.$site->name;
		//TODO: translations, instead of adding 'copy of' into Joomlalms language files - use joomla word for this.
		//check how it work in Joomla menu/articles management
		if (!$site->check()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if (!$site->store()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
	}

	mosRedirect( "index.php?option=$option&task=plans" );
}

function JLMS_changePlan( $cid=null, $state=0, $option ) {
	$db = JFactory::getDBO();
	$user = JLMSFactory::getUser();

	if (!is_array( $cid ) || count( $cid ) < 1) {
		$msg = $publish ? _JLMS_MSG_SLCT_I_TO_PUBL : _JLMS_MSG_SLCT_I_TO_UNPUBL;
		echo "<script> alert('".$msg."'); window.history.go(-1);</script>\n";
		exit();
	}

	$cids = implode( ',', $cid );

	$query = "UPDATE #__lms_plans SET published = " . intval( $state ). " WHERE id IN ( $cids ) AND ( checked_out = 0 OR ( checked_out = ".$user->get('id')." ) )";
	$db->setQuery( $query );
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}
	mosRedirect( "index.php?option=$option&task=plans" );
}

function JLMS_cancelPlan() {
	$db = JFactory::getDBO();
	$row = new mos_Joomla_LMS_plan( $db );
	$row->bind( $_POST );
	$row->checkin();
	//TODO: what is it? is checkin/checkout feature fully designed for plans? remove it if not.
	mosRedirect('index.php?option=com_joomla_lms&task=plans');
}

function JLMS_showDiscounts($option) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );

	$query = "SELECT COUNT(*) FROM #__lms_discounts";
	$db->setQuery( $query );
	$total = $db->loadResult();

	jimport('joomla.html.pagination');
	$pageNav = new JPagination( $total, $limitstart, $limit  );

	$query = "SELECT *  FROM #__lms_discounts LIMIT $pageNav->limitstart, $pageNav->limit";
	$db->setQuery( $query );
	$rows = $db->loadObjectList();

	for( $i = 0; $i<count($rows); $i++ ) 
	{
		if( $rows[$i]->subscriptions )
			$rows[$i]->count_subscriptions = count(explode(',',$rows[$i]->subscriptions));
		else
			$rows[$i]->count_subscriptions = 0;
			
		if( $rows[$i]->usergroups )
			$rows[$i]->count_usergroups = count(explode(',',$rows[$i]->usergroups));
		else
			$rows[$i]->count_usergroups = 0;
			
		if( $rows[$i]->users )
			$rows[$i]->count_users = count(explode(',',$rows[$i]->users));
		else
			$rows[$i]->count_users = 0;
	}	

	joomla_lms_adm_html::JLMS_showDiscounts( $rows, $pageNav, $option);
}

function JLMS_saveDiscount($option) {
	$db = JFactory::getDBO();

	$users = mosGetParam($_REQUEST, 'users', '');		
	$row = new mos_Joomla_LMS_discount( $db );

	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	if( is_array( $row->usergroups ) ) {
		if( $row->usergroups[0] == '0' ) 
				unset($row->usergroups[0]);
				 
		$row->usergroups	= implode( ',', $row->usergroups );
	}

	if( is_array( $row->subscriptions ) ) {
		if( $row->subscriptions[0] == '0' ) 
				unset($row->subscriptions[0]);
		$row->subscriptions = implode( ',', $row->subscriptions ) ;
	}	

	if (!$row->check()) {		
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	if (!$row->store()) {		
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	mosRedirect( "index.php?option=$option&task=discounts", _JLMS_DISC_MSG_SAVED );
}

function JLMS_editDiscount($id, $option) {
	$db = JFactory::getDBO();

	$row = new mos_Joomla_LMS_discount( $db );
	$row->load( $id );	
	
	if( $row->subscriptions )
		$row->subscriptions_arr = explode(',',$row->subscriptions);
	else
		$row->subscriptions_arr = array();
			
	if( $row->usergroups )
		$row->usergroups_arr = explode(',',$row->usergroups);
	else
		$row->usergroups_arr = array();		
		
	$query = "SELECT id AS value, ug_name AS text FROM #__lms_usergroups WHERE course_id = 0";
	$db->setQuery( $query );
	$groups = $db->loadObjectList();
					
	$lists = array();
		
	$all_groups[] = mosHTML::makeOption( '0', '&nbsp;');
	$all_groups = array_merge( $all_groups, $groups );
	
	$lists['usergroups'] = mosHTML::selectList( $all_groups, 'usergroups[]', 'class="inputbox  chzn-done" size="20" multiple', 'value', 'text',$row->usergroups_arr );
	
	$query = "SELECT id AS value, sub_name AS text FROM #__lms_subscriptions WHERE published = 1";
	$db->setQuery( $query );
	$subscriptions = $db->loadObjectList();
	
	$all_subscriptions[] = mosHTML::makeOption( '0', '&nbsp;');
	$all_subscriptions = array_merge( $all_subscriptions, $subscriptions );
	
	$lists['subscriptions'] = mosHTML::selectList( $all_subscriptions, 'subscriptions[]', 'class="inputbox chzn-done" size="20" multiple', 'value', 'text',$row->subscriptions_arr );	
	
	$all_subscriptions[] = mosHTML::makeOption( '0', '&nbsp;');
	$all_subscriptions = array_merge( $all_subscriptions, $subscriptions );
	
	$disc_types = array();
	$disc_types[] = mosHTML::makeOption( 0, _JLMS_PERCENT);
	$disc_types[] = mosHTML::makeOption( 1, _JLMS_TOTAL);	
	$lists['discount_type'] = mosHTML::selectList( $disc_types, 'discount_type', 'class="inputbox"', 'value', 'text', (int)$row->discount_type );	
	
	joomla_lms_adm_html::JLMS_editDiscount( $row, $lists, $option);
}

function JLMS_deleteDiscount($cid, $option) {
	$db = JFactory::getDBO();

	if( isset($cid[0]) ) {
		$query = "DELETE FROM #__lms_discounts WHERE id IN (".implode(',',$cid).") ";
		$db->setQuery( $query );
		if (!$db->query()) {
			echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}

	mosRedirect( "index.php?option=$option&task=discounts", _JLMS_DISC_MSG_DELETED );
}

function JLMS_changeDiscount($cid=null, $action=0, $option) {
	$db = JFactory::getDBO();

	$query = "UPDATE #__lms_discounts SET enabled = " . intval( $action ). " WHERE id IN (".implode(",", $cid).")";
	$db->setQuery( $query );	
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}	

	mosRedirect( "index.php?option=$option&task=discounts" );
}

function JLMS_showDiscountCoupons($option) {
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "view{$option}limitstart", 'limitstart', 0 ) );

	$query = "SELECT COUNT(*) FROM #__lms_discount_coupons WHERE removed = 0";
	$db->setQuery( $query );
	$total = $db->loadResult();

	jimport('joomla.html.pagination');
	$pageNav = new JPagination( $total, $limitstart, $limit  );

	$query = "SELECT *  FROM #__lms_discount_coupons WHERE removed = 0 LIMIT $pageNav->limitstart, $pageNav->limit";
	$db->setQuery( $query );
	$rows = $db->loadObjectList();

	for( $i = 0; $i<count($rows); $i++ ) 
	{
		if( $rows[$i]->subscriptions )
			$rows[$i]->count_subscriptions = count(explode(',',$rows[$i]->subscriptions));
		else
			$rows[$i]->count_subscriptions = 0;
			
		if( $rows[$i]->usergroups )
			$rows[$i]->count_usergroups = count(explode(',',$rows[$i]->usergroups));
		else
			$rows[$i]->count_usergroups = 0;
			
		if( $rows[$i]->users )
			$rows[$i]->count_users = count(explode(',',$rows[$i]->users));
		else
			$rows[$i]->count_users = 0;
	}	

	joomla_lms_adm_html::JLMS_showDiscountCoupons( $rows, $pageNav, $option);
}

function JLMS_saveDiscountCoupon($option) {
	$db = JFactory::getDBO();

	$users = mosGetParam($_REQUEST, 'users', '');	
	$row = new mos_Joomla_LMS_discount_coupon( $db );

	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	
	if( $row->discount_type == 0 && $row->value > 100 )//percent 
	{
		$row->value = 100;
	}	

	if( is_array( $row->usergroups ) ) {
		if( $row->usergroups[0] == '0' ) {
			unset($row->usergroups[0]);
		}
		$row->usergroups	= implode( ',', $row->usergroups );
	}

	if( is_array( $row->subscriptions ) ) {
		if( $row->subscriptions[0] == '0' ) 
				unset($row->subscriptions[0]);
		$row->subscriptions = implode( ',', $row->subscriptions ) ;
	}	

	// pre-save checks
	if (!$row->check()) {		
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	// save the changes
	if (!$row->store()) {		
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}	

	mosRedirect( "index.php?option=$option&task=discount_coupons", _JLMS_DISC_MSG_C_SAVED );
}

function JLMS_editDiscountCoupon($id, $option) {
	$db = JFactory::getDBO();

	$row = new mos_Joomla_LMS_discount_coupon( $db );
	$row->load( $id );	
	
	if( $row->subscriptions ) {
		$row->subscriptions_arr = explode(',',$row->subscriptions);
	} else {
		$row->subscriptions_arr = array();
	}

	if( $row->usergroups ) {
		$row->usergroups_arr = explode(',',$row->usergroups);
	} else {
		$row->usergroups_arr = array();		
	}

	$query = "SELECT id AS value, ug_name AS text FROM #__lms_usergroups WHERE course_id = 0";
	$db->setQuery( $query );
	$groups = $db->loadObjectList();
					
	$lists = array();
		
	$all_groups[] = mosHTML::makeOption( '0', '&nbsp;');
	$all_groups = array_merge( $all_groups, $groups );
	
	$lists['usergroups'] = mosHTML::selectList( $all_groups, 'usergroups[]', 'class="inputbox chzn-done" size="20" multiple', 'value', 'text',$row->usergroups_arr );
	
	$query = "SELECT id AS value, sub_name AS text FROM #__lms_subscriptions WHERE published = 1";
	$db->setQuery( $query );
	$subscriptions = $db->loadObjectList();
	
	$all_subscriptions[] = mosHTML::makeOption( '0', '&nbsp;');
	$all_subscriptions = array_merge( $all_subscriptions, $subscriptions );
	
	$lists['subscriptions'] = mosHTML::selectList( $all_subscriptions, 'subscriptions[]', 'class="inputbox chzn-done" size="20" multiple', 'value', 'text',$row->subscriptions_arr );	
	
	$coupon_types = array();
	$coupon_types[] = mosHTML::makeOption( 0, _JLMS_DISC_PERMANENT);
	$coupon_types[] = mosHTML::makeOption( 1, _JLMS_DISC_ONE_TIME);	
	$lists['coupon_type'] = mosHTML::selectList( $coupon_types, 'coupon_type', 'class="inputbox"', 'value', 'text', (int)$row->coupon_type );	
	
	$disc_types = array();
	$disc_types[] = mosHTML::makeOption( 0, _JLMS_PERCENT);
	$disc_types[] = mosHTML::makeOption( 1, _JLMS_TOTAL);	
	$lists['discount_type'] = mosHTML::selectList( $disc_types, 'discount_type', 'class="inputbox"', 'value', 'text', (int)$row->discount_type );	
	
	joomla_lms_adm_html::JLMS_editDiscountCoupon( $row, $lists, $option);
}

function JLMS_deleteDiscountCoupon($cid, $option) {
	$db = JFactory::getDBO();

	if( isset($cid[0]) ) {
		$query = "UPDATE #__lms_discount_coupons SET removed = 1 WHERE id IN (".implode(',',$cid).") ";
		$db->setQuery( $query );
		if (!$db->query()) {
			echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}
	mosRedirect( "index.php?option=$option&task=discount_coupons", _JLMS_DISC_MSG_C_DELETED );
}

function JLMS_changeDiscountCoupon($cid=null, $action=0, $option) {
	$db = JFactory::getDBO();

	$query = "UPDATE #__lms_discount_coupons SET enabled = " . intval( $action ). " WHERE id IN (".implode(",", $cid).")";
	$db->setQuery( $query );	
	if (!$db->query()) {
		echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
		exit();
	}	
	mosRedirect( "index.php?option=$option&task=discount_coupons" );
}

function JLMS_showDiscountCouponsStatistics() {
	global $option;//TODO remove global option declaration
	$db = JFactory::getDBO();
	$app = JFactory::getApplication('administrator');

	$limit 		= intval( $app->getUserStateFromRequest( "viewlistlimit", 'limit', $app->getCfg('list_limit') ) );
	$limitstart = intval( $app->getUserStateFromRequest( "viewcouponstats{$option}limitstart", 'limitstart', 0 ) );

	$query = "SELECT COUNT(*) FROM #__users AS u, #__lms_disc_c_usage_stats AS dcs 
		LEFT JOIN #__lms_discount_coupons AS dc ON dc.id = dcs.coupon_id
		LEFT JOIN #__lms_payments AS p ON p.id = dcs.payment_id 
		WHERE u.id = dcs.user_id";
	$db->setQuery( $query );
	$total = $db->loadResult();

	jimport('joomla.html.pagination');
	$pageNav = new JPagination( $total, $limitstart, $limit  );

	$query = "SELECT dcs.id, dcs.date, dcs.coupon_code, u.name, u.username, u.email, p.id AS payment_id, dc.id AS coupon_id FROM #__users AS u, #__lms_disc_c_usage_stats AS dcs
		LEFT JOIN #__lms_discount_coupons AS dc ON dc.id = dcs.coupon_id
		LEFT JOIN #__lms_payments AS p ON p.id = dcs.payment_id 
		WHERE u.id = dcs.user_id
		ORDER BY dcs.date DESC
		LIMIT $pageNav->limitstart, $pageNav->limit		 
		";
	$db->setQuery( $query );
	$rows = $db->loadObjectList();

	joomla_lms_adm_html::JLMS_showDiscountCouponsStatistics( $rows, $pageNav, $option);
}

class JlmsViewPlugin 
{
	public static function editPage15( & $row, & $lists, & $params, $option )
	{		
		$lists['published'] = mosHTML::yesnoRadioList( 'published', 'class="inputbox"', $row->published );	

		$path = JPATH_SITE."/components/com_joomla_lms/includes/plugins/$row->folder/$row->element.xml";
		if (!file_exists( $path )) {
			$path = '';
		}
		// get params definitions	
		$params = new jlmsPluginParameters( $row->params, $path, 'plugin' );		
		
		JlmsTmplPlugin::editPage15( $row, $lists, $params, $option );
	}
	
	public static function editPage25( & $row, & $lists, & $params, $option )
	{	
		$lists['published'] = mosHTML::yesnoRadioList( 'published', 'class="inputbox"', $row->published );	

		$path = JPATH_SITE."/components/com_joomla_lms/includes/plugins/$row->folder/$row->element.xml";
		if (!file_exists( $path )) {
			$path = '';
		}
		// get params definitions	
		$params = new jlmsPluginParameters( $row->params, $path, 'plugin' );
		
		JlmsTmplPlugin::editPage25( $row, $lists, $params, $option );
	}
	
	public static function editPage30( & $row, & $lists, & $params, $option )
	{
		$lists['published'] = JFormHelper::loadFieldType('radio', true);//JHtml::_('select.booleanlist', 'published', 'class="inputbox"', $row->published);	
		$lists['published'] = mosHTML::yesnoRadioList( 'published', 'class="inputbox"', $row->published );	
		
		$file = JPATH_SITE."/components/com_joomla_lms/includes/plugins/$row->folder/$row->element.xml";		
		$xmlDoc = JFactory::getXML($file);
		$attributes = $xmlDoc->attributes();
		
		if ($xmlDoc->name() == 'jlmsplugin' && $attributes->type == 'plugin' ) {			
			$row->description = $xmlDoc->description;				
		}
		
		$data = '
		<?xml version="1.0" encoding="utf-8"?>
		<form>
			<fieldset >				
				<field
					name="name"
					type="text"					
					size="20"
					label="'._JLMS_NAME.'"
					description=""
					/>
				<field
					name="folder"
					type="text"
					class="readonly"
					size="20"
					label="'._JLMS_FOLDER.'"
					description=""
					readonly="true" />
				<field
					name="element"
					type="text"
					class="readonly"
					size="20"
					label="'._JLMS_PLGS_PLG_FILE.'"
					description=""
					readonly="true" />				
				<field
					name="ordering"
					type="pluginsOrdering"
					class="inputbox"
					label="'._JLMS_PLGS_PLG_ORDER.'"
					description="" />

				<field
					name="published"
					type="radio"
					class="btn-group"
					label="'._JLMS_PUBLISHED.'"
					description=""
					size="1"
					default="1">
					<option value="1">'.JText::_('yes').'</option>
					<option value="0">'.JText::_('no').'</option>
				</field>		
			</fieldset>
		</form>
		';
		
		$form = JForm::getInstance('editplugin', $data );		
		JForm::addFieldPath(JPATH_COMPONENT.'/models/fields/');
		$form->loadFile($file, false, '//config');
		
		$registry = new JRegistry;
		$registry->loadString($row->params);
		$row->params = $registry->toArray();		
		
		$form->bind( $row );					
		
		$lists['form'] = $form;
		
		JlmsTmplPlugin::editPage30( $row, $lists, $params, $option );
	}
	
	public static function editPage35( & $row, & $lists, & $params, $option )
	{
		$lists['published'] = JFormHelper::loadFieldType('radio', true);//JHtml::_('select.booleanlist', 'published', 'class="inputbox"', $row->published);	
		$lists['published'] = mosHTML::yesnoRadioList( 'published', 'class="inputbox"', $row->published );	
		
		$file = JPATH_SITE."/components/com_joomla_lms/includes/plugins/$row->folder/$row->element.xml";		
		$xmlDoc = JFactory::getXML($file);
		$attributes = $xmlDoc->attributes();
		
		if ($xmlDoc->name() == 'jlmsplugin' && $attributes->type == 'plugin' ) {			
			$row->description = $xmlDoc->description;				
		}				
		
		$data = '
		<?xml version="1.0" encoding="utf-8"?>
		<form>
			<fieldset >				
				<field
					name="name"
					type="text"					
					size="20"
					label="'._JLMS_NAME.'"
					description=""
					/>
				<field
					name="folder"
					type="text"
					class="readonly"
					size="20"
					label="'._JLMS_FOLDER.'"
					description=""
					readonly="true" />
				<field
					name="element"
					type="text"
					class="readonly"
					size="20"
					label="'._JLMS_PLGS_PLG_FILE.'"
					description=""
					readonly="true" />				
				<field
					name="ordering"
					type="pluginsOrdering"
					class="inputbox"
					label="'._JLMS_PLGS_PLG_ORDER.'"
					description="" />

				<field
					name="published"
					type="radio"
					class="btn-group"
					label="'._JLMS_PUBLISHED.'"
					description=""
					size="1"
					default="1">
					<option value="1">'.JText::_('yes').'</option>
					<option value="0">'.JText::_('no').'</option>
				</field>		
			</fieldset>
		</form>
		';
		
		$form = JForm::getInstance('editplugin', $data );		
		JForm::addFieldPath(JPATH_COMPONENT.'/models/fields/');
		$form->loadFile($file, false, '//config');
		
		$registry = new JRegistry;
		$registry->loadString($row->params);
		$row->params = $registry->toArray();				
		
		$form->bind( $row );					
		
		$lists['form'] = $form;
		
		JlmsTmplPlugin::editPage35( $row, $lists, $params, $option );
	}
	
	public static function editPage( & $row, & $lists, & $params, $option )
	{
		if( JLMS_J31version() ) 
		{
			JlmsViewPlugin::editPage35( $row, $lists, $params, $option );
		} else if( JLMS_J30version() ) {
			JlmsViewPlugin::editPage30( $row, $lists, $params, $option );
		} else if ( JLMS_J16version() ) {
			JlmsViewPlugin::editPage25( $row, $lists, $params, $option );
		} else {
			JlmsViewPlugin::editPage15( $row, $lists, $params, $option );
		}		
	}
}

class JlmsViewProcessor 
{
	public static function editPage15( & $row, & $lists, & $params, $option )
	{		
	
		$file = JPATH_SITE .'/components/com_joomla_lms/includes/processors/'.$row->filename.'.xml';
		$xmlDoc = JLMSFactory::getXMLParser();
		$xmlDoc->loadFile( $file );
		
		$root = &$xmlDoc->document;
		if ($root->name() == 'jlmsplugin' && $root->attributes( 'type' ) == 'paymentprocessor' ) {
			$element = &$root->getElementByPath( 'description' );
			$row->description = $element ? trim( $element->data() ) : '';
		}		
	
		$lists['published'] = mosHTML::yesnoRadioList( 'published', 'class="inputbox"', $row->published );			
		
		$params = new jlmsPluginParameters( $row->params, $file, 'plugin' );		
		
		JlmsTmplProcessor::editPage15( $row, $lists, $params, $option );
	}
	
	public static function editPage25( & $row, & $lists, & $params, $option )
	{	
		$file = JPATH_SITE .'/components/com_joomla_lms/includes/processors/'.$row->filename.'.xml';
		$xmlDoc = JLMSFactory::getXMLParser();
		$xmlDoc->loadFile( $file );
		
		$root = &$xmlDoc->document;
		if ($root->name() == 'jlmsplugin' && $root->attributes( 'type' ) == 'paymentprocessor' ) {
			$element = &$root->getElementByPath( 'description' );
			$row->description = $element ? trim( $element->data() ) : '';
		}		
	
		$lists['published'] = mosHTML::yesnoRadioList( 'published', 'class="inputbox"', $row->published );			
		
		$params = new jlmsPluginParameters( $row->params, $file, 'plugin' );		
		
		JlmsTmplProcessor::editPage25( $row, $lists, $params, $option );		
	}	
	
	public static function editPage30( & $row, & $lists, & $params, $option )
	{		
		$file = JPATH_SITE .'/components/com_joomla_lms/includes/processors/'.$row->filename.'.xml';		
		
		$xmlDoc = JFactory::getXML($file);							
		$attributes = $xmlDoc->attributes();				
		
		if ($xmlDoc->name() == 'jlmsplugin' && $attributes->type == 'paymentprocessor' ) {			
			$row->description = $xmlDoc->description;				
		}				
			
		$data = '
		<?xml version="1.0" encoding="utf-8"?>
		<form>
			<fieldset >				
				<field
					name="name"
					type="text"					
					size="20"
					label="'._JLMS_NAME.'"
					description=""
					/>				
				<field
					name="filename"
					type="text"
					class="readonly"
					size="20"
					label="'._JLMS_PROCS_FILENAMES.'"
					description=""
					readonly="true" />				
				<field
					name="published"
					type="radio"
					class="btn-group"
					label="'._JLMS_PUBLISHED.'"
					description=""
					size="1"
					default="1">
					<option value="1">'.JText::_('yes').'</option>
					<option value="0">'.JText::_('no').'</option>
				</field>		
			</fieldset>
		</form>
		';
		
		$form = JForm::getInstance('editplugin', $data );		
		$form->loadFile($file, false, '//config');
		
		$registry = new JRegistry;
		$registry->loadString($row->params);
		$row->params = $registry->toArray();		
		
		$form->bind( $row );			
		
		$lists['form'] = $form;		
		
		JlmsTmplProcessor::editPage30( $row, $lists, $params, $option );
	}
	
	public static function editPage35( & $row, & $lists, & $params, $option )
	{		
		$file = JPATH_SITE .'/components/com_joomla_lms/includes/processors/'.$row->filename.'.xml';		
		
		$xmlDoc = JFactory::getXML($file);							
		$attributes = $xmlDoc->attributes();				
		
		if ($xmlDoc->name() == 'jlmsplugin' && $attributes->type == 'paymentprocessor' ) {			
			$row->description = $xmlDoc->description;				
		}				
			
		$data = '
		<?xml version="1.0" encoding="utf-8"?>
		<form>
			<fieldset >				
				<field
					name="name"
					type="text"					
					size="20"
					label="'._JLMS_NAME.'"
					description=""
					/>				
				<field
					name="filename"
					type="text"
					class="readonly"
					size="20"
					label="'._JLMS_PROCS_FILENAMES.'"
					description=""
					readonly="true" />				
				<field
					name="published"
					type="radio"
					class="btn-group"
					label="'._JLMS_PUBLISHED.'"
					description=""
					size="1"
					default="1">
					<option value="1">'.JText::_('yes').'</option>
					<option value="0">'.JText::_('no').'</option>
				</field>		
			</fieldset>
		</form>
		';		
				
		$form = JForm::getInstance('editplugin', $data );			
		$form->loadFile($file, false, '//config');		
		
		$registry = new JRegistry;
		$registry->loadString($row->params);
		$row->params = $registry->toArray();		
		
		$form->bind( $row );					
		$lists['form'] = $form;		
		
		JlmsTmplProcessor::editPage35( $row, $lists, $params, $option );
	}
	
	public static function editPage( & $row, & $lists, & $params, $option )
	{
		
		if( JLMS_J31version() ) 
		{			
			JlmsViewProcessor::editPage35( $row, $lists, $params, $option );
		} else if( JLMS_J30version() ) {
			JlmsViewProcessor::editPage30( $row, $lists, $params, $option );
		} else if ( JLMS_J16version() ) {			
			JlmsViewProcessor::editPage25( $row, $lists, $params, $option );
		} else {
			JlmsViewProcessor::editPage15( $row, $lists, $params, $option );
		}		
	}	
}

?>