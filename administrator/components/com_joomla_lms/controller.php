<?php
/**
 * controller.php
 * @package     Joomla.Administrator
 * @subpackage  com_joomla_lms
 * @author 		Joomla LMS Team
 * @copyright   Copyright (c) JoomaLMS eLearning Software http://www.joomlalms.com/
 */

defined('_JEXEC') or die;

/**
 * Joomla_lms Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_joomla_lms
 * @since       3.0
 */


 
jimport('joomla.application.component.controller');
jimport('joomla.application.component.model');
jimport('joomla.application.component.view' );

if( !class_exists('JControllerLegacy' ))
{
	class JControllerLegacy extends JController {}
}

if( !class_exists('JViewLegacy' ))
{
	class JViewLegacy extends JView{}
}

if( !class_exists('JModelLegacy' ))
{
	class JModelLegacy extends JModel {
		static function addIncludePath( $path='' )
		{
			JModel::addIncludePath($path);
		}
	}
}
 
 
class Joomla_lmsController extends JControllerLegacy
{
	public function display($cachable = false, $urlparams = false)
	{
		//JHtml::_('behavior.mootools');
		JHtml::_('behavior.framework', true);
		JHtml::_('behavior.switcher');
		
		$doc = JFactory::getDocument();
		$doc->addStyleSheet(JURI::root().'administrator/components/com_joomla_lms/assets/css/joomlalms_style_j30.css');
		
		require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'admin.joomla_lms.php');
		require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'toolbar.joomla_lms.php');
	}
}
