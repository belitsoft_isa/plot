<?php
/**
 * jlms_script.php
 * @package     Joomla.Administrator
 * @subpackage  com_joomla_lms
 * @author 		Joomla LMS Team
 * @copyright   Copyright (c) JoomaLMS eLearning Software http://www.joomlalms.com/
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
if (!defined('DS')) define('DS',DIRECTORY_SEPARATOR);
/**
 * Script file of HelloWorld component
 */
if (!class_exists('com_joomla_lmsInstallerScript')) 
{
class com_joomla_lmsInstallerScript
{
        /**
         * method to install the component
         *
         * @return void
         */
        function install($parent) 
        {
			$this->installLegacy();
        }
		/**
         * method to install the component
         *
         * @return void
         */
		function installLegacy() {
			$app = JFactory::getApplication();
			if($app->isAdmin()){
				$start_path = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_joomla_lms'.DS;			
				require_once($start_path. 'install.joomla_lms.php');
				com_install();
			}
		} 
        /**
         * method to uninstall the component
         *
         * @return void
         */
        function uninstall($parent) 
        {          	
			
        }
 
        /**
         * method to update the component
         *
         * @return void
         */
        function update($parent) 
        {
            // $parent is the class calling this method
			$this->installLegacy();              
        }
 
        /**
         * method to run before an install/update/uninstall method
         *
         * @return void
         */
        function preflight($type, $parent) 
        {
                // $parent is the class calling this method
                // $type is the type of change (install, update or discover_install)
               
        }
 
        /**
         * method to run after an install/update/uninstall method
         *
         * @return void
         */
        function postflight($type, $parent) 
        {
                // $parent is the class calling this method
                // $type is the type of change (install, update or discover_install)
				//update copy controller and main file.
		
		
		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.folder');
			$start_path = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_joomla_lms'.DS;						
				if (JFolder::exists($start_path.'admin'))
					{
						JFile::move( $start_path.'admin'.DS.'controller.php', $start_path.'controller.php' );
						JFile::move( $start_path.'admin'.DS.'joomla_lms.php', $start_path.'joomla_lms.php' );
						JFolder::move( $start_path.'admin'.DS.'assets', $start_path.DS.'assets' );
						JFolder::delete($start_path.'admin');
					}	
		
		//
                
        }
}
}