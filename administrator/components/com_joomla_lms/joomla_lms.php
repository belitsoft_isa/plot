<?php
/**
 * joomla_lms.php
 * @package     Joomla.Administrator
 * @subpackage  com_joomla_lms
 * @author 		Joomla LMS Team
 * @copyright   Copyright (c) JoomaLMS eLearning Software http://www.joomlalms.com/
 */

defined('_JEXEC') or die;

if (!JFactory::getUser()->authorise('core.manage', 'com_joomla_lms'))
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

if (!defined('JPATH_BASE')) { define('JPATH_BASE', dirname(__FILE__) ); }
if (!defined('DS')) { define( 'DS', DIRECTORY_SEPARATOR ); }

$controller	= JControllerLegacy::getInstance('Joomla_LMS');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();