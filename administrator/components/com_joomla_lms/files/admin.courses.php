<?php
/**
* admin.courses.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
if (!defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) { die( 'Restricted access' ); }

class JLMS_Admin_Courses_Tools
{
	private $_db = null;
	
	function __construct() 
	{
		$this->_db = JFactory::getDBO();
	}
	public function updateCourseOvner($course_id=0,$old_uid=0,$new_uid=0)
	{
		if (!$course_id || !$old_uid || !$new_uid) return false;
		if ($old_uid == $new_uid) return false;
		$updateTables 	= array();
		$default_params = array('course_id', 'owner_id');
		$updateTables['lms_agenda'] 			= $default_params;
		$updateTables['lms_conference_doc'] 	= $default_params;
		$updateTables['lms_conference_records'] = array('course_id', 'user_id');
		$updateTables['lms_documents_zip'] 		= $default_params;
		$updateTables['lms_homework'] 			= $default_params;
		$updateTables['lms_learn_paths'] 		= $default_params;
		$updateTables['lms_links'] 				= $default_params;
		$updateTables['lms_quiz_t_quiz'] 		= array('course_id', 'c_user_id');
		$updateTables['lms_scorm_packages'] 	= $default_params;
		$updateTables['lms_usergroups'] 		= $default_params;
		
		foreach ($updateTables as $tblname=>$tparams)
		{
			$this->updateDatabaseTableFields($tblname,$tparams,$old_uid, $course_id, $new_uid);
		}
		
		//Documents
		$files = $this->getFilesIds($course_id,$old_uid);
		if (sizeof($files))
		{
			$this->updateFilesTable($files,$new_uid);			
		}
		$this->updateDatabaseTableFields('lms_documents',$default_params,$old_uid, $course_id, $new_uid);
		
		//Role in course		
		$isset_in_table = $this->checkUserInCourses($course_id,$new_uid);
		if (!$isset_in_table)
		{
			$roleid = $this->getActiveRoleId();
			$this->insertToCourses($course_id,$new_uid,$roleid);
		}
	}
	
	private function updateDatabaseTableFields($tablename='',$fields=null, $old_uid, $course_id, $new_uid)
	{
		$db = $this->_db;
		$result = false;
		$query = $db->getQuery(true);
			$query->update('`#__'.$tablename.'`');
			if (isset($fields[0])) 
			{				
				$query->where('`'.$fields[0].'`='.(int)$course_id);
				if (isset($fields[1])) 
				{
					$query->where('`'.$fields[1].'`='.(int)$old_uid);
					$query->set('`'.$fields[1].'`='.(int)$new_uid);
				}
			}
		$db->setQuery($query);
		$result = $db->query();
		return $result;
	}
	
	private function getFilesIds($course_id=0,$old_uid=0)
	{
		$db = $this->_db;
		$query = $db->getQuery(true);
			$query->select('`file_id`');
			$query->from('`#__lms_documents`');
			$query->where('`course_id`='.(int)$course_id);
			$query->where('`owner_id`='.(int)$old_uid);
			$query->where('`folder_flag`=0');
		$db->setQuery($query);
		$files = $db->loadColumn();
		return $files;
	}
	
	private function updateFilesTable($ids=0,$new_uid=0)
	{
		$db = $this->_db;
		$result = false;
		$in = implode(',',$ids);
		$query = $db->getQuery(true);
			$query->update('`#__lms_files`');
			$query->set('`owner_id`='.(int)$new_uid);
			$query->where('`id` IN ('.$in.')');
		$db->setQuery($query);
		$result = $db->query();
		return $result;
	}
	
	private function getActiveRoleId()
	{
		$db = $this->_db;
		$roleid = 0;
		$query = $db->getQuery(true);
			$query->select('*');
			$query->from('`#__lms_usertypes`');
			$query->where('`roletype_id`=2');
			$query->order('`id` ASC');
		$db->setQuery($query);
		$results = $db->loadObjectList();
		if (sizeof($results))
		{
			$roleid = $results[0]->id;
			foreach ( $results as $result ) 
			{
				if ($result->default_role==1) 
				{
					$roleid = $result->id;
					break;
				}
			}
		}
		return $roleid;
	}
	
	private function checkUserInCourses($course_id=0,$new_uid=0)
	{
		$db = $this->_db;
		$query = $db->getQuery(true);
			$query->select('`user_id`');
			$query->from('`#__lms_user_courses`');
			$query->where('`user_id`='.(int)$course_id);
			$query->where('`course_id`='.(int)$new_uid);
		$db->setQuery($query);
		$result = $db->loadResult();
		
		return $result;
	}
	
	private function insertToCourses($course_id=0,$new_uid=0,$roleid=0)
	{
		$db = $this->_db;
		$query = $db->getQuery(true);
			$query->insert('`#__lms_user_courses`');
			$query->set('`user_id`='.(int)$new_uid);
			$query->set('`course_id`='.(int)$course_id);
			$query->set('`role_id`='.(int)$roleid);
		$db->setQuery($query);
		$result = $db->query();
		return $result;
	}
}
?>