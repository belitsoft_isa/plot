<?php
/**
* admin.roles.toolbar.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

if (!defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) { die( 'Restricted access' ); }

//processors
class ALR_toolbar {

	static function _EDIT() {
		global $is_jlms_trial_roles_heading_text;
		if (class_exists('JToolBarHelper')) {
			global $jlms_toolbar_id;
			JToolBarHelper::title( _JOOMLMS_COMP_NAME.': '.($jlms_toolbar_id?_JLMS_ROLES_EDIT_USER_ROLE : _JLMS_ROLES_NEW_USER_ROLE).($is_jlms_trial_roles_heading_text ? $is_jlms_trial_roles_heading_text : '') );
		}		
		JToolBarHelper::apply('apply');
		JToolBarHelper::spacer();
		JToolBarHelper::save('save');		
		JToolBarHelper::spacer();
		JToolBarHelper::cancel( 'cancel', _JLMS_CANCEL);
		JToolBarHelper::spacer();
	}

    static function _ASSIGNMENT() {
        global $is_jlms_trial_roles_heading_text;
        if (class_exists('JToolBarHelper')) {
            JToolBarHelper::title( _JLMS_ROLES_ASSIGNMENTS.($is_jlms_trial_roles_heading_text ? $is_jlms_trial_roles_heading_text : ''), 'user.png' );
        }

        if( JLMS_J16version() )
        {
            $cfgClass = 'options';
        } else {
            $cfgClass = 'config';
        }
        $bar = JToolBar::getInstance('toolbar');

        JToolBarHelper::apply('apply_assignment');
        JToolBarHelper::spacer();
        JToolBarHelper::save('save_assignment');
        JToolBarHelper::spacer();
        $bar->appendButton( 'Confirm', _JLMS_CONFIRM_RESET_DEFAULT, 'refresh', _JLMS_ROLES_ASSIGNMENTS_DEFAULT, 'default_assignment', false );
        JToolBarHelper::spacer();
        JToolBarHelper::cancel( 'cancel_course', _JLMS_CLOSE );
        JToolBarHelper::spacer();
    }

	static function _DEFAULT() {
		global $is_jlms_trial_roles_heading_text;
		if (class_exists('JToolBarHelper')) {
			JToolBarHelper::title( _JLMS_ROLES_MANAGEMENT.($is_jlms_trial_roles_heading_text ? $is_jlms_trial_roles_heading_text : ''), 'user.png' );
		}

		if( JLMS_J16version() ) 
		{
			$cfgClass = 'options';			
		} else {
			$cfgClass = 'config';
		}
		$bar = JToolBar::getInstance('toolbar');		
//		$bar->appendButton( 'Popup', 'config', 'Allow role assignments', 'index.php?option=com_joomla_lms&task=lms_roles&page=new' );
		
		if (method_exists('JToolBarHelper','addNewX')) {JToolBarHelper::addNewX('new');} else {JToolBarHelper::addNew('new');}
		JToolBarHelper::spacer();
		JToolBarHelper::editList('edit');
		JToolBarHelper::spacer();
		JToolBarHelper::custom( 'default_role', 'publish.png', 'publish_f2.png', _JLMS_DEFAULT, true );
		JToolBarHelper::spacer();		
		if( JLMS_J16version() ) 
		{
			JToolBarHelper::custom( 'assignment', 'options.png', 'options.png', _JLMS_ROLES_ASSIGNMENTS, false);
		} else {
			JToolBarHelper::custom( 'assignment', 'apply.png', 'apply_f2.png', _JLMS_ROLES_ASSIGNMENTS, false);
		}		
		JToolBarHelper::spacer();
		JToolBarHelper::deleteList('', 'delete', _JLMS_DELETE);
		JToolBarHelper::spacer();
	}
}

function ALR_process_toolbar() {
	$page 	= mosGetParam( $_REQUEST, 'page', 'list' );
	switch ($page) {
		case 'edit':
		case 'editA':
		case 'new':
			ALR_toolbar::_EDIT();
		break;
        case 'assignment':
            ALR_toolbar::_ASSIGNMENT();
            break;
		case 'list':
		default:
			ALR_toolbar::_DEFAULT();
		break;

	}
}
?>