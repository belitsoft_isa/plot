<?php
/**
* admin.roles.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
if (!defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) { die( 'Restricted access' ); }

require_once(_JOOMLMS_ADMIN_HOME.'/files/admin.roles.html.php');
require_once(_JOOMLMS_ADMIN_HOME.'/files/admin.roles.class.php');

$task 	= mosGetParam( $_REQUEST, 'task', 'lms_roles' );
$page 	= mosGetParam( $_REQUEST, 'page', 'list' );
if ($task != 'lms_roles') {
	mosRedirect( "index.php?option=$option&task=lms_roles");
}
$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) );
$cid 	= mosGetParam( $_POST, 'cid', mosGetParam( $_GET, 'cid', array(0) ) );

if ($page == 'edit' || $page == 'editA' || $page == 'new' || $page == 'save' || $page == 'apply' || $page == 'delete' || $page == 'default_role') {
	global $JLMS_CONFIG, $license_lms_roles, $is_jlms_trial;
	if (!$license_lms_roles) {
		if ($page == 'edit' || $page == 'editA' || $page == 'new') {
			if (!$is_jlms_trial) {
				mosRedirect( "index.php?option=$option&task=lms_roles", _JLMS_ROLES_VERS_LIMIT );
			}
		} else {
			mosRedirect( "index.php?option=$option&task=lms_roles", _JLMS_ROLES_VERS_LIMIT );
		}
	}
}
if ($page == 'default_assignment' || $page == 'save_assignment') {
	global $license_lms_roles, $is_jlms_trial;
	if (!$license_lms_roles) {
		echo _JLMS_ROLES_VERS_LIMIT; die;
	}
}
switch ($page) {
	
	case 'default_assignment':
		ALR_defaultAssignments($option, 1);
	break;
    case 'apply_assignment':
	case 'save_assignment':
		ALR_save_assignment($option, $page);
	break;
		
	case 'assignment':
		ALR_assignment($option);
	break;	
	
	case 'default_role':
		ALR_default_role( intval( $cid[0] ), $option);
	break;	
	case 'edit':
		ALR_editItem( intval( $cid[0] ), $option);
	break;
	case 'editA':
		ALR_editItem( $id, $option);
	break;
	case 'new':
		ALR_editItem( 0, $option);
	break;
	case 'save':
	case 'apply':
		ALR_saveItem( $id, $option, $page );
	break;
	case 'delete':
		ALR_deleteItem( intval( $cid[0] ), $option, $page );
	break;
	case 'cancel':
		mosRedirect( "index.php?option=$option&task=lms_roles" );
	break;
	case 'list':
	default:
		ALR_showList( $option );
	break;
}

function ALR_save_assignment($option, $page){
	
	$db = JFactory::getDBO();
	
	require_once(_JOOMLMS_FRONT_HOME . "/includes/classes/acl/assignments_roles.php");
	$assignments = & JLMS_get_allow_assignments_roles();
	
	$post_assignments = array();
	
	$i=0;
	foreach($_REQUEST as $key=>$item){
		if(preg_match_all('#role_(\d+)#', $key, $out)){
			if(isset($out[1][0]) && $out[1][0]){
				$role_id = $out[1][0];
				
				$post_assignments[$i] = new stdClass();
				$post_assignments[$i]->role_id = $role_id; 
				$post_assignments[$i]->assignment = $item; 
				
				$i++;
			}
		}
	}
	
	$result_save = array();
	
	$roles = ALR_roles($db);
	
//	$xroles = $roles;
	$xroles = array();
	$order = array(4,2,5);
	foreach($order as $x){
		foreach($roles as $role){
			if($role->roletype_id == $x){
				$xroles[] = $role;
			}
		}
	}
	
	$yroles = $roles;
	
	$i=0;
	foreach($xroles as $xrole){
		foreach($yroles as $yrole){
			
			$add_el_in_save	= false;
			$value = 0;
			
			foreach($post_assignments as $post_n=>$post_assign){
				if($xrole->id == $post_assign->role_id && in_array($yrole->id, $post_assign->assignment)){
					$value = 1;
					$add_el_in_save = true;
				}
			}
			
			foreach($assignments as $n=>$assign){
				if($xrole->roletype_id == $assign->roletype_id && in_array($yrole->roletype_id, $assign->assignment)){
					if(!$add_el_in_save){
						$value = 0;
						$add_el_in_save = true;
					} else {
						$add_el_in_save = false;
					}
				}
			}
			
			if($add_el_in_save){
				$result_save[$i] = new stdClass();
				$result_save[$i]->role_id = $xrole->id;
				$result_save[$i]->role_assign = $yrole->id;
				$result_save[$i]->value = $value;
				
				$i++;
			}
		}
	}
	
	ALR_defaultAssignments($option);
	
	if(isset($result_save) && count($result_save)){
		foreach($result_save as $r_save){
			$save_data = get_object_vars($r_save);
			
			$query = "INSERT INTO #__lms_user_roles_assignments"
			. "\n (role_id, role_assign, value)"
			. "\n VALUES"
			. "\n (".$save_data['role_id'].", ".$save_data['role_assign'].", ".$save_data['value'].")"
			;
			$db->setQuery($query);
			$db->query();
		}
	}

    if ($page == 'apply_assignment') {
        mosRedirect( "index.php?option=$option&task=lms_roles&page=assignment" );
    } else {
        mosRedirect( "index.php?option=com_joomla_lms&task=lms_roles" );
    }
}

function ALR_defaultAssignments($option, $redirect=0){
	
	$db = JFactory::getDBO();
	
	$query = "DELETE FROM #__lms_user_roles_assignments"
	. "\n WHERE 1"
	;
	$db->setQuery($query);
	$db->query();
	
	if($redirect){
		mosRedirect( "index.php?option=$option&task=lms_roles&page=assignment" );
	}
}

function ALR_roles(&$db){
	
	$roles = array();
	$query = "SELECT *"
	. "\n FROM #__lms_usertypes"
	. "\n WHERE 1"
	. "\n AND roletype_id > 0"
	;
	$db->setQuery($query);
	$roles = $db->loadObjectList();
	
	$order = array(4,2,5,1,3);
	$tmp_roles = array();
	foreach($order as $x){
		foreach($roles as $role){
			if($role->roletype_id == $x){
				$tmp_roles[] = $role;
			}
		}
	}
	if(isset($tmp_roles) && count($tmp_roles)){
		$roles = array();
		$roles = $tmp_roles;
	}
	return $roles;
}

function ALR_getRoletype($role_id, &$db){ //no use
	
	$roletype_id = false;
	
	$roles = ALR_roles($db);
	
	foreach($roles as $role){
		if($role->id == $role_id){
			$roletype_id = $role->roletype_id;
		}
	}
	return $roletype_id;
}

function ALR_prepare_Assignments(&$in_assignments, &$db){
	
	$out_assignments = false;
	
	$roles = ALR_roles($db);
	
	$xroles = array();
	$order = array(4,2,5);
	foreach($order as $x){
		foreach($roles as $role){
			if($role->roletype_id == $x){
				$xroles[] = $role;
			}
		}
	}
	
	$yroles = $roles;
	
	$i=0;
	foreach($xroles as $xrole){
		foreach($yroles as $yrole){
			
			foreach($in_assignments as $in_a){
				if($xrole->roletype_id == $in_a->roletype_id && in_array($yrole->roletype_id, $in_a->assignment)){
					
					$out_assignments[$i] = new stdClass();
					$out_assignments[$i]->role_id = $xrole->id;
					$out_assignments[$i]->role_assign = $yrole->id;
					$out_assignments[$i]->value = 1;
					
					$i++;
				}
			}
			
		}
	}
	
	$in_assignments = array();
	
	return $out_assignments;
}

function ALR_getAssignments(&$db){
	
	require_once(_JOOMLMS_FRONT_HOME . "/includes/classes/acl/assignments_roles.php");
	$assignments = & JLMS_get_allow_assignments_roles();
	$assignments = ALR_prepare_Assignments($assignments, $db);
	
	$query = "SELECT *"
	. "\n FROM #__lms_user_roles_assignments"
	. "\n WHERE 1"
	;
	$db->setQuery($query);
	$db_tmp_assignments = $db->loadObjectList();
	
	$tmp_asssignments = array();
	$i=0;
	foreach($assignments as $assign){
		$tmp_asssignments[$i] = $assign;
		
		
		foreach($db_tmp_assignments as $db_tmp_assign){
			if($assign->role_id == $db_tmp_assign->role_id && $assign->role_assign == $db_tmp_assign->role_assign){
				if($db_tmp_assign->value){
					$tmp_asssignments[$i]->value = $db_tmp_assign->value;
				} else 
				if(!$db_tmp_assign->value){
					$tmp_asssignments[$i]->value = $db_tmp_assign->value;
				}
			} 
			else {
				$tmp_asssignments[] = $db_tmp_assign;
			}
		}
		
		$i++;
	}
	if(isset($tmp_asssignments) && count($tmp_asssignments)){
		$assignments = array();
		$assignments = $tmp_asssignments;
	}
	
	return $assignments;
}

function ALR_assignment($option){
	$db = JFactory::getDBO();

	$roles = ALR_roles($db);
	
	$assignments = ALR_getAssignments($db);
	
	ALR_html::showAssignment($option, $roles, $assignments);
}

function ALR_showList( $option ) {
	$db = JFactory::getDbo();
	$query = "SELECT * FROM #__lms_usertypes WHERE roletype_id <> 0 ORDER BY id, roletype_id, lms_usertype";
	$db->SetQuery($query);
	$roles = $db->loadObjectList();

	$lists = array();
	ALR_html::showList( $roles, $option, $lists );
}

function ALR_editItem($id, $option) {
	$db = JFactory::getDbo();

	$GLOBALS['jlms_toolbar_id'] = $id;
	$row = new JLMS_role_item( $db );
	// load the row from the db table
	$row->load( $id );

	$lists = array();
	if (!$id) {
		$row->roletype_id = intval( mosGetParam( $_REQUEST, 'roletype_id', 0 ) );
	}
	
	$javascript = 'onchange="adminForm.page.value=\'new\';adminForm.submit();"';
	$role_types = array();
	$role_types[] = mosHTML::makeOption( 0, _JLMS_ROLES_SLCT_ROLE_TYPE_ );
	$role_types[] = mosHTML::makeOption( 4, _JLMS_ROLES_ADMIN_ROLE );
	$role_types[] = mosHTML::makeOption( 2, _JLMS_ROLES_TEACHER_ROLE );
	$role_types[] = mosHTML::makeOption( 5, _JLMS_ROLES_ASSISTANT_ROLE );
	$role_types[] = mosHTML::makeOption( 1, _JLMS_ROLES_LEARNER_ROLE );//	if($id){
	$role_types[] = mosHTML::makeOption( 3, _JLMS_ROLES_CEO_ROLE );
//	}
	$lists['role_type'] = mosHTML::selectList($role_types, 'roletype_id', 'class="text_area" size="1" style="width:266px;"'.$javascript, 'value', 'text', $row->roletype_id );
	
	//load permisions
	$text_permisions = array();
	$tmp_permisions = array();

	require_once(_JOOMLMS_FRONT_HOME . "/includes/classes/acl/prototype.php");
	$text_permisions = & JLMS_role_prototype($row->roletype_id);
	
	if ($row->roletype_id == 4) {
		require_once(_JOOMLMS_FRONT_HOME . "/includes/classes/acl/administrator.php");
		$tmp_permisions = & JLMS_get_administrator_role();
	} elseif ($row->roletype_id == 2) {
		require_once(_JOOMLMS_FRONT_HOME . "/includes/classes/acl/teacher.php");
		$tmp_permisions = & JLMS_get_teacher_role();
	} elseif ($row->roletype_id == 5) {
		require_once(_JOOMLMS_FRONT_HOME . "/includes/classes/acl/assistant.php");
		$tmp_permisions = & JLMS_get_assistant_role();
	} elseif ($row->roletype_id == 1) {
		require_once(_JOOMLMS_FRONT_HOME . "/includes/classes/acl/learner.php");
		$tmp_permisions = & JLMS_get_learner_role();
	} elseif ($row->roletype_id == 3) {
		require_once(_JOOMLMS_FRONT_HOME . "/includes/classes/acl/ceo.php");
		$tmp_permisions = & JLMS_get_ceo_role();
	}
	
	/*
	Extra permissions not permissions file role. (Max)
	*/
	foreach($text_permisions as $key=>$text_permision) {
		$available_roletypes = isset($text_permision->role_types) ? $text_permision->role_types : array(0);
		foreach($text_permision as $key2=>$text_permision2){
			$available = 0;
			if($available_roletypes[0] == 0){
				$available = 1;
			} else {
				if(in_array($row->roletype_id, $available_roletypes)) {
					$available = 1;	
				}
			}
			if($available && $key2 != 'role_types' && !isset($tmp_permisions[$key]->$key2)) {
				$tmp_permisions[$key]->$key2 = 0;
			}	
		}
	}
	
	$rows_permissions = array();
	if($id){
		$query = "SELECT * FROM #__lms_user_permissions WHERE role_id = '".$id."'";
		$db->setQuery($query);
		$rows_permissions = $db->loadObjectList();
	}
	
	$tmp_db_permissions = array();
	$p_category = '';
	foreach($rows_permissions as $row_permission){
		if($p_category == '' || $p_category != $row_permission->p_category){
			$p_category = $row_permission->p_category;
		}
		$text_prms = $row_permission->p_permission;
		$value_prms = $row_permission->p_value;
		if($text_prms != 'id' && $text_prms != 'role_id' && $text_prms != 'p_category'){
			if( !isset($tmp_db_permissions[$p_category]) ) {
				$tmp_db_permissions[$p_category] = new stdClass();
			}
			$tmp_db_permissions[$p_category]->$text_prms = $value_prms;
		}
	}
	
	$permissions = array();
	foreach($tmp_permisions as $p_category=>$tmp_prms){
		$available_roles = isset($text_permisions[$p_category]->role_types)?$text_permisions[$p_category]->role_types:array(0);
		if(isset($text_permisions[$p_category]->role_types) && in_array($row->roletype_id, $available_roles)){
			foreach($tmp_prms as $permision=>$value){
				if(isset($text_permisions[$p_category]->$permision)){
					if (!isset($permissions[$p_category])) {
						$permissions[$p_category] = new stdclass();
					}
					$permissions[$p_category]->$permision = $tmp_permisions[$p_category]->$permision;
					if(isset($tmp_permisions[$p_category]->$permision) && isset($tmp_db_permissions[$p_category]->$permision)){
						$permissions[$p_category]->$permision = $tmp_db_permissions[$p_category]->$permision;
					}
				}	
			}
		}	
	}
	
	ALR_html::editItem( $row, $lists, $text_permisions, $permissions, $option );
}

function ALR_deleteItem($id, $option) {
	$db = JFactory::getDbo();

	$row = new JLMS_role_item( $db );
	// load the row from the db table
	$row->load( $id );
	
//	echo '<pre>';
//	print_r($row);
//	echo '</pre>';
//	die;

	if ($row->roletype_id == 4 || $row->roletype_id == 2 || $row->roletype_id == 5 || $row->roletype_id == 3) {
		$query = "SELECT count(*) FROM #__lms_usertypes WHERE roletype_id = ".$row->roletype_id;
		$db->SetQuery($query);
		$role_nums = $db->LoadResult();
		if ($role_nums == 1) {
			mosRedirect( "index.php?option=$option&task=lms_roles", _JLMS_ROLES_MSG_CANT_REM_ALL_T );
			die;
		}
		$query = "SELECT count(*) FROM #__lms_users WHERE lms_usertype_id = $id";
		$db->SetQuery($query);
		$role_nums = $db->LoadResult();
		if ($role_nums) {
			mosRedirect( "index.php?option=$option&task=lms_roles", _JLMS_ROLES_MSG_ROLE_IN_USE_SYS );
			die;
		}
		$query = "SELECT count(*) FROM #__lms_user_courses WHERE role_id = $id";
		$db->SetQuery($query);
		$role_nums = $db->LoadResult();
		if ($role_nums) {
			mosRedirect( "index.php?option=$option&task=lms_roles", _JLMS_ROLES_MSG_ROLE_IN_USE_CRS );
			die;
		}
		
//		$query = "SELECT count(*)"
//		. "\n FROM #__lms_user_parents as a"
//		. "\n, #__lms_users as b"
//		. "\n WHERE 1"
//		. "\n AND b.lms_usertype_id = $id"
//		. "\n AND b.user_id = a.parent_id"
//		;
//		$db->SetQuery($query);
//		echo $query;
//		die;
		
		$role_nums = $db->LoadResult();
		if ($role_nums) {
			mosRedirect( "index.php?option=$option&task=lms_roles", _JLMS_ROLES_MSG_ROLE_IN_USE_CRS );
			die;
		}
		$query = "DELETE FROM #__lms_usertypes WHERE id = $id AND roletype_id IN (4,2,5,3)";
		$db->SetQuery($query);
		$db->query();
		$query = "DELETE FROM #__lms_user_permissions WHERE role_id = '".$id."'";
		$db->SetQuery($query);
		$db->query();
	} else {
		mosRedirect( "index.php?option=$option&task=lms_roles", str_replace('{roletype_id}',$row->roletype_id, _JLMS_ROLES_MSG_CANT_REM_THIS_ROLE_T) );
		die;
	}
	mosRedirect( "index.php?option=$option&task=lms_roles", _JLMS_ROLES_MSG_ROLE_REMOVED );
}

function ALR_saveItem( $id, $option, $page) {
	$db = JFactory::getDbo();

	$msg = '';
	if (!$id) {
		$msg = _JLMS_ROLES_MSG_ROLE_CREATED;
	}

	$roletype_id = intval( mosGetParam( $_REQUEST, 'roletype_id', 0 ) );
	$default_role = intval( mosGetParam( $_REQUEST, 'default_role', 0 ) );
	if($roletype_id && $default_role){
		$query = "UPDATE #__lms_usertypes SET default_role = '0' WHERE roletype_id = '".$roletype_id."'";
		$db->setQuery($query);
		$db->query();
	}

	$row = new JLMS_role_item( $db );
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if (!$row->check()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	
	$role_id = (!$id ? $row->id : $_REQUEST['id']);
	
	//Permissions save
	$tmp_permissions = array();
	$tmp_permissions = $_REQUEST['permissions'];
	$permissions = array();
	$i=0;
	foreach($tmp_permissions as $p_category=>$tmp_prms){
		foreach($tmp_prms as $p_permission=>$p_value){
			$permissions[$i]['role_id'] = $role_id;
			$permissions[$i]['p_category'] = $p_category;
			$permissions[$i]['p_permission'] = $p_permission;
			$permissions[$i]['p_value'] = $p_value;
			$i++;
		}	
	}
	
	if(count($permissions)){
		$query = "DELETE FROM #__lms_user_permissions WHERE role_id = '".$role_id."'";	
		$db->setQuery($query);
		$db->query();
	}
	
	$i=0;
	foreach($permissions as $prms){
		$row_prms = new JLMS_role_permissions( $db );
		if (!$row_prms->bind( $prms )) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if (!$row_prms->check()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if (!$row_prms->store()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		$i++;
	}
	
	if ($page == 'apply') {
		mosRedirect( "index.php?option=$option&task=lms_roles&page=editA&id=".$row->id, $msg );
	} else {
		mosRedirect( "index.php?option=$option&task=lms_roles", $msg );
	}
}

function ALR_default_role($id, $option){
	$db = JFactory::getDbo();

	$query = "SELECT * FROM #__lms_usertypes WHERE id = '".$id."'";
	$db->setQuery($query);
	$obj_role = $db->loadObject();

	$access = array(2, 1);
	if( is_object($obj_role) && isset($obj_role->roletype_id) && in_array($obj_role->roletype_id, $access) ){
		$query = "UPDATE #__lms_usertypes SET default_role = '0' WHERE roletype_id = '".$obj_role->roletype_id."'";	
		$db->setQuery($query);
		$db->query();

		$query = "UPDATE #__lms_usertypes SET default_role = '1' WHERE id = '".$id."'";	
		$db->setQuery($query);
		$db->query();
		$msg = _JLMS_SUCCESSFUL;
	} else {
		$msg = _JLMS_ROLES_MSG_ONLY_TEACH_STUD;
	}
	mosRedirect( "index.php?option=$option&task=lms_roles", $msg );	
}
?>