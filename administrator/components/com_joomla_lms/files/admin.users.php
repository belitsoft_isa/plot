<?php

/**
 * admin.users.php
 * (c) JoomaLMS eLearning Software http://www.joomlalms.com/
 * */
defined('_JEXEC') or die;

//TODO: remove additional lang files from 'files' folder
require_once(_JOOMLMS_ADMIN_HOME . '/files/admin.users.html.php');
require_once(_JOOMLMS_ADMIN_HOME . '/files/admin.users.class.php');
$option = 'com_joomla_lms';
$task = mosGetParam($_REQUEST, 'task', 'lms_users');
$page = mosGetParam($_REQUEST, 'page', 'list');

$id = intval(mosGetParam($_REQUEST, 'id', 0));
$cid = mosGetParam($_POST, 'cid', mosGetParam($_GET, 'cid', array(0)));
if (!is_array($cid)) {
    $cid = array(0);
}

switch ($page) {
    case 'edit':
        ALU_editItem(intval($cid[0]), $option);
        break;
    case 'editA':
        ALU_editItem($id, $option);
        break;
    case 'new':
        ALU_editItem(0, $option);
        break;
    case 'save':
    case 'apply':
        ALU_saveItem($id, $option, $page);
        break;

    case 'delete':
        ALU_deleteItem($cid, $option, $page);
        break;
    case 'cancel':
        mosRedirect("index.php?option=$option&task=lms_users");
        break;
    case 'list':
    default:
        ALU_showList($option);
        break;
}

function ALU_showList($option) {
    $db = JFactory::getDbo();
    $app = JFactory::getApplication('administrator');

    $view_by = intval($app->getUserStateFromRequest("view_by{$option}_lms_users", 'view_by', 0));
    $course_filter = intval($app->getUserStateFromRequest("course_filter{$option}_lms_users", 'course_id', 0));
    if (!$view_by) {
        $course_filter = 0;
    }
    $role_filter = intval($app->getUserStateFromRequest("role_filter{$option}_lms_users", 'role_id', 0));

    $u_search = strval($app->getUserStateFromRequest("u_search{$option}", 'u_search', ''));

    $limit = intval($app->getUserStateFromRequest("viewlistlimit{$option}_lms_users", 'limit', $app->getCfg('list_limit')));
    $limitstart = intval($app->getUserStateFromRequest("view{$option}_lms_users_limitstart", 'limitstart', 0));

    // get the total number of records
    if ($view_by) {
        $query = "SELECT COUNT(*)"
                . "\n FROM  #__lms_usertypes as b, #__lms_user_courses as a LEFT JOIN #__users as c ON a.user_id = c.id LEFT JOIN #__lms_courses as d ON a.course_id = d.id "
                . "\n WHERE a.role_id = b.id"
                . ($course_filter ? " AND a.course_id = $course_filter" : '')
                . ($role_filter ? " AND a.role_id = $role_filter" : '')
                . (($u_search) ? "\n AND (c.username LIKE '%" . $u_search . "%' OR c.name LIKE '%" . $u_search . "%' OR c.email LIKE '%" . $u_search . "%')" : "")
                . "\n ORDER BY c.username, d.course_name ASC, b.roletype_id DESC"
        ;
    } else {
        $query = "SELECT COUNT(*)"
                . "\n FROM  #__lms_usertypes as b, #__lms_users as a LEFT JOIN #__users as c ON a.user_id = c.id WHERE a.lms_usertype_id = b.id"
                . ($role_filter ? " AND b.id = $role_filter" : '')
                . (($u_search) ? "\n AND (c.username LIKE '%" . $u_search . "%' OR c.name LIKE '%" . $u_search . "%' OR c.email LIKE '%" . $u_search . "%')" : "")
                . "\n ORDER BY c.username ASC, b.roletype_id DESC"
        ;
    }
    $db->setQuery($query);
    $total = $db->loadResult();

    require_once( JPATH_SITE . DS . 'components' . DS . 'com_joomla_lms' . DS . 'includes' . DS . 'classes' . DS . 'lms.pagination.new.php');
    $pageNav = new JLMSPagination($total, $limitstart, $limit);

    // get the subset (based on limits) of required records
    if ($view_by) {
        $query = "SELECT b.lms_usertype, a.course_id, a.user_id, a.user_id as id, c.username, c.name, c.email, d.course_name, c.id as main_id "
                . "\n FROM  #__lms_usertypes as b, #__lms_user_courses as a LEFT JOIN #__users as c ON a.user_id = c.id LEFT JOIN #__lms_courses as d ON a.course_id = d.id "
                . "\n WHERE a.role_id = b.id"
                . ($course_filter ? " AND a.course_id = $course_filter" : '')
                . ($role_filter ? " AND a.role_id = $role_filter" : '')
                . (($u_search) ? "\n AND (c.username LIKE '%" . $u_search . "%' OR c.name LIKE '%" . $u_search . "%' OR c.email LIKE '%" . $u_search . "%')" : "")
                . "\n ORDER BY c.username, d.course_name ASC, b.roletype_id DESC"
                . "\n LIMIT $pageNav->limitstart, $pageNav->limit"
        ;
    } else {
        $query = "SELECT b.lms_usertype, a.lms_block, a.id, c.username, c.name, c.email, c.id as main_id "
                . "\n FROM  #__lms_usertypes as b, #__lms_users as a LEFT JOIN #__users as c ON a.user_id = c.id WHERE a.lms_usertype_id = b.id"
                . ($role_filter ? " AND b.id = $role_filter" : '')
                . (($u_search) ? "\n AND (c.username LIKE '%" . $u_search . "%' OR c.name LIKE '%" . $u_search . "%' OR c.email LIKE '%" . $u_search . "%')" : "")
                . "\n ORDER BY c.username ASC, b.roletype_id DESC"
                . "\n LIMIT $pageNav->limitstart, $pageNav->limit"
        ;
    }
    $db->setQuery($query);
    $rows = $db->loadObjectList();

    for ($i = 0; $i < count($rows); $i++) {
        $query = "SELECT count(a.group_id) FROM #__lms_user_assign_groups as a, #__lms_users as b, #__lms_usergroups as c 
					WHERE b.id = " . $rows[$i]->id . " AND b.user_id = a.user_id AND a.group_id = c.id";

        $db->setQuery($query);
        $db->query();
        $total = $db->LoadResult();

        if ($total == 1) {
            $query = "SELECT a.ug_name FROM #__lms_usergroups as a, #__lms_user_assign_groups as b, #__lms_users as c WHERE c.id = " . $rows[$i]->id . " AND b.group_id = a.id AND c.user_id = b.user_id";
            $db->setQuery($query);
            $db->query();
            $name = $db->LoadResult();
            $rows[$i]->groups = $name;
        } else {
			$rows[$i]->groups = $total . " ".JLMS_pluralForm($total, _JLMS_GROUP_A, _JLMS_GROUP_Y, _JLMS_GROUP_);
        }
    }

    $lists = array();
    $view_by_types = array();
    $view_by_types[] = mosHTML::makeOption(0, _JLMS_USERS_SYSTEM_LVL);
    $view_by_types[] = mosHTML::makeOption(1, _JLMS_USERS_COURSE_LVL);
    $lists['view_by'] = mosHTML::selectList($view_by_types, 'view_by', 'class="text_area" size="1" onchange="document.adminForm.submit();" ', 'value', 'text', $view_by);
    if ($view_by) {
        $query = "SELECT id as value, course_name as text FROM #__lms_courses ORDER BY course_name";
        $db->setQuery($query);
        $courses_db = $db->loadObjectList();
        $courses = array();
        $courses[] = mosHTML::makeOption(0, '&nbsp;');
        $courses = array_merge($courses, $courses_db);
        $lists['course_filter'] = mosHTML::selectList($courses, 'course_id', 'class="text_area" size="1" style="width:266px;" onchange="document.adminForm.submit();" ', 'value', 'text', $course_filter);
    }
//	echo '<pre>';
//	print_r($courses_db);
//	echo '</pre>';

    $lists['u_search'] = $u_search;
    $lists['course_id'] = $course_filter;

    $role_types = array(2, 3, 4);
    if ($view_by) {
        $role_types = array(2, 5);
    }
    $str_role_types = implode(",", $role_types);
    $query = "SELECT id as value, lms_usertype as text, roletype_id, IF(roletype_id = 4, 1, IF(roletype_id = 2, 2, 3)) as ordering FROM #__lms_usertypes WHERE roletype_id IN (" . $str_role_types . ") ORDER BY ordering, id, lms_usertype";
    $db->SetQuery($query);
    $roles = $db->LoadObjectList('value');
//	echo '<pre>';
//	print_r($roles);
//	echo '</pre>';

    $cur_role = $role_filter;
    $sel_name = 'role_id';
    $sel_html = '<select id="roles_selections" name="' . $sel_name . '" class="text_area" onchange="document.adminForm.submit();">';
    $sel_html .= '<option value="0"> ' . _JLMS_USERS_SLCT_ROLE_ . ' </option>';
    $prev_roletype = 0;
    foreach ($roles as $role) {
        if ($role->roletype_id != $prev_roletype) {
            if ($prev_roletype) {
                $sel_html .= '</optgroup>';
            }
            $prev_roletype = $role->roletype_id;
            if ($role->roletype_id == 4) {
                $sel_html .= '<optgroup label="' . _JLMS_ADMIN_ROLES . '">';
            }
            if ($role->roletype_id == 2) {
                $sel_html .= '<optgroup label="' . _JLMS_TEACHER_ROLES . '">';
            }
            if ($role->roletype_id == 5) {
                $sel_html .= '<optgroup label="' . _JLMS_ASSISTANT_ROLES . '">';
            }
            if ($role->roletype_id == 3) {
                $sel_html .= '<optgroup label="' . _JLMS_STAFF_ROLES . '">';
            }
        }
        $selected = '';
        if ($role->value == $cur_role) {
            $selected = ' selected="selected"';
        }
        $sel_html .= '<option value="' . $role->value . '"' . $selected . '>' . $role->text . '</option>';
    }
    $sel_html .= '</optgroup>';
    $sel_html .= '</select>';
    $lists['role_filter'] = $sel_html;

    if ($view_by) {
        ALU_html::showListCourse($rows, $pageNav, $option, $lists);
    } else {

        ALU_html::showListSystem($rows, $pageNav, $option, $lists);
    }
}

function ALU_editItem($id, $option) {
    $db = JFactory::getDbo();
    $GLOBALS['jlms_toolbar_id'] = $id;
    $msg = '';
    $view_by = intval(mosGetParam($_REQUEST, 'view_by', 0));
    $course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));

    if ($view_by) {
        if (!$course_id) {
            $msg = _JLMS_USERS_FLTR_BY_CRS;
            mosRedirect("index.php?option=$option&task=lms_users", $msg);
            die;
        } else {
            $query = "SELECT user_id, course_id, user_id as id, role_id FROM #__lms_user_courses WHERE user_id = $id AND course_id = $course_id";
            $db->setQuery($query);
            $row = $db->LoadObject();
            if (is_object($row) && isset($row->user_id)) {
                
            } elseif (!$id) {
                $row = new StdClass();
                $row->id = 0;
                $row->user_id = 0;
                $row->course_id = 0;
                $row->role_id = 0;
            } else {
                mosRedirect("index.php?option=$option&task=lms_users");
            }
            $query = "SELECT * FROM #__lms_courses WHERE id = $course_id";
            $db->setQuery($query);
            $course_info = $db->LoadObject();
            if (is_object($course_info) && isset($course_info->id)) {
                
            } else {
                mosRedirect("index.php?option=$option&task=lms_users", _JLMS_USERS_CRS_NOT_FOUND);
            }
        }
    } else {
        //TODO: check  how this class declarated...is this declaration native for Joomla 1.5 / 1.6 ?
        $row = new JLMS_user_system_level($db);
        $row->load($id);
        if (!$id) {
            $row->lms_usertype_id = 0;
        } else {
            
        }
    }

    $user_id = $row->user_id;

    $lists = array();

    $role_types = array(2, 3, 4);
    if ($view_by) {
        $role_types = array(2, 5);
    }
    $str_role_types = implode(",", $role_types);
    $query = "SELECT id as value, lms_usertype as text, roletype_id, IF(roletype_id = 4, 1, IF(roletype_id = 2, 2, 3)) as ordering FROM #__lms_usertypes WHERE roletype_id IN (" . $str_role_types . ") ORDER BY ordering, id, lms_usertype";
    $db->SetQuery($query);
    $roles = $db->LoadObjectList('value');


    $cur_role = $view_by ? $row->role_id : $row->lms_usertype_id;
    $sel_name = $view_by ? 'role_id' : 'lms_usertype_id';
    $sel_html = '<select id="roles_selections" class="text_area" style="width:266px" name="' . $sel_name . '">';
    $sel_html .= '<option value="0">' . _JLMS_USERS_SLCT_ROLE_ . '</option>';
    $prev_roletype = 0;
    foreach ($roles as $role) {
        if ($role->roletype_id != $prev_roletype) {
            if ($prev_roletype) {
                $sel_html .= '</optgroup>';
            }
            $prev_roletype = $role->roletype_id;
            if ($role->roletype_id == 4) {
                $sel_html .= '<optgroup label="' . _JLMS_ADMIN_ROLES . '">';
            }
            if ($role->roletype_id == 2) {
                $sel_html .= '<optgroup label="' . _JLMS_TEACHER_ROLES . '">';
            }
            if ($role->roletype_id == 5) {
                $sel_html .= '<optgroup label="' . _JLMS_ASSISTANT_ROLES . '">';
            }
            if ($role->roletype_id == 3) {
                $sel_html .= '<optgroup label="' . _JLMS_STAFF_ROLES . '">';
            }
        }
        $selected = '';
        if ($role->value == $cur_role) {
            $selected = ' selected="selected"';
        }
        $sel_html .= '<option value="' . $role->value . '"' . $selected . '>' . $role->text . '</option>';
    }
    $sel_html .= '</optgroup>';
    $sel_html .= '</select>';

    $lists['roles'] = $sel_html;
    $lists['user_id'] = $user_id;

    if ($user_id) {
        $query = "SELECT username, name, email FROM #__users WHERE id = $user_id";
        $db->SetQuery($query);
        $user_info = $db->loadObject();
        if (is_object($user_info) && isset($user_info->username)) {
            $lists['user_info'] = $user_info->name . ' (' . $user_info->username . '), ' . $user_info->email;
        } else {
            mosRedirect("index.php?option=$option&task=lms_users", _JLMS_USERS_USR_NOT_FOUND);
        }
    } else {
        /*
          $query = "SELECT id as value, username as text FROM #__users order by username";
          $db->setQuery( $query );
          $u_users = $db->loadObjectList();
          $list_users = array();
          $list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_SLCT_USER_ );
          $list_users = array_merge( $list_users, $u_users );
          $lists['users'] = JHTML::_('select.genericlist', $list_users, 'user_id', 'class="text_area" size="1" style="width:266px" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', $row->user_id );

          $query = "SELECT id as value, name as text FROM #__users ORDER BY name";
          $db->SetQuery($query);
          $list_users = array();
          $list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_NAME_);
          $pr = $db->loadObjectList();
          $list_users = array_merge( $list_users, $pr );
          $lists['users_names'] = JHTML::_('select.genericlist', $list_users, 'user_name', 'class="text_area" style="width:266px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', $row->user_id );

          $query = "SELECT id as value, email as text FROM #__users ORDER BY email";
          $db->SetQuery($query);
          $list_users = array();
          $list_users[] = mosHTML::makeOption( '0', _JLMS_USERS_OR_EMAIL_);
          $pr = $db->loadObjectList();
          $list_users = array_merge( $list_users, $pr );
          $lists['users_emails'] = JHTML::_('select.genericlist', $list_users, 'user_email', 'class="text_area" style="width:266px" size="1" onChange="jlms_changeUserSelect(this);" ', 'value', 'text', $row->user_id );
         */
    }

    $lists['view_by'] = $view_by;
    $lists['course_id'] = $course_id;
    if ($view_by) {
        $lists['course_name'] = $course_info->course_name . ' (ID: ' . $course_id . ')';
        ALU_html::editItem($row, $lists, $option);
    } else {
        ALU_html::editItem($row, $lists, $option);
    }
}

function ALU_deleteItem($cid, $option) {
    $db = JFactory::getDbo();
    $msg = '';
    $view_by = intval(mosGetParam($_REQUEST, 'view_by', 0));
    $course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
    $cids = implode(',', $cid);
    if ($view_by) {
        if ($course_id) {
            $query = "DELETE FROM #__lms_user_courses"
//			. "\n WHERE user_id = $id AND course_id = $course_id"
                    . "\n WHERE user_id IN (" . $cids . ") AND course_id = $course_id"
            ;
            $db->setQuery($query);
            if (!$db->query()) {
                echo "<script> alert('" . $db->getErrorMsg() . "'); window.history.go(-1); </script>\n";
            }
            if (count($cid) == 1) {
                $msg = _JLMS_USERS_MSG_USR_REMOVED_F_MBRS;
            } else {
                $msg = _JLMS_USERS_MSG_USRS_REMOVED_F_MBRS;
            }
        } else {
            $msg = _JLMS_USERS_FLTR_BY_CRS_REM;
        }
    } else {
        $query = "SELECT user_id FROM #__lms_users"
                . "\n WHERE id IN (" . $cids . ")"
        ;
        $db->setQuery($query);
        $real_user_ids = JLMSDatabaseHelper::loadResultArray();
        $query = "DELETE FROM #__lms_users"
                . "\n WHERE id IN (" . $cids . ")"
        ;
        $db->setQuery($query);
        if (!$db->query()) {
            echo "<script> alert('" . $db->getErrorMsg() . "'); window.history.go(-1); </script>\n";
        }
        if (count($real_user_ids)) {
            $real_user_ids_str = implode(',', $real_user_ids);
            $query = "DELETE FROM #__lms_user_assign_groups"
                    . "\n WHERE user_id IN (" . $real_user_ids_str . ")"
            ;
            $db->setQuery($query);
            if (!$db->query()) {
                echo "<script> alert('" . $db->getErrorMsg() . "'); window.history.go(-1); </script>\n";
            }
        }
        if (count($cid) == 1) {
            $msg = _JLMS_USERS_WAS_REM_F_TEACH_ADMIN;
        } else {
            $msg = _JLMS_USERS_WERE_REM_F_TEACH_ADMIN;
        }
    }
    mosRedirect("index.php?option=$option&task=lms_users", $msg);
}

function ALU_saveItem($id, $option, $page) {
    $db = JFactory::getDbo();

    $view_by = intval(mosGetParam($_REQUEST, 'view_by', 0));
    $course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
    $user_ids = mosGetParam($_REQUEST, 'user_ids');
    $user_id = intval( mosGetParam( $_REQUEST, 'user_id', 0 ));
    
    if ($user_id && !sizeof($user_ids)) $user_ids = $user_id;

    $JLMS_DB = JLMSFactory::getDB();

    //Note: if exists $view_by then it's course level else it's system level
    if ($view_by) {
        $role_id = intval(mosGetParam($_REQUEST, 'role_id', 0));
        if ($user_ids) {
            $uids = explode(',', $user_ids);
            if (sizeof($uids)) {
                $is_j3 = JLMS_J30version();
                foreach ($uids as $user_id) {
                    
                    if ($course_id && $user_id && $role_id) {
                        $query = "SELECT `name` FROM #__users WHERE `id` = $user_id";
                        $db->setQuery($query);
                        $usr_name = $db->LoadResult();
                        if (!$id) {
                            $query = "SELECT count(*) FROM #__lms_user_courses WHERE user_id = $user_id AND course_id = $course_id";
                            $db->setQuery($query);
                            $count_yet = $db->LoadResult();
                            if ($count_yet) {
                                $mess = str_replace('{USER}', $usr_name, _JLMS_USERS_EX_TEACH_ASSIST);
                                if ($is_j3)
                                    JFactory::getApplication()->enqueueMessage($mess, 'warning');
                                else
                                    JError::raiseNotice(100, $mess);
                                $proceed_with_store = false;
                            } else {
                                $query = "INSERT INTO #__lms_user_courses (user_id, course_id, role_id) VALUES ($user_id, $course_id, $role_id)";
                                $db->setQuery($query);
                                if (!$db->query()) {
                                    echo "<script> alert('" . $db->getErrorMsg() . "'); window.history.go(-1); </script>\n";
                                }
                                $mess = str_replace('{USER}', $usr_name, _JLMS_USERS_ADD_TEACH_ASSIST);
                                JFactory::getApplication()->enqueueMessage($mess, 'message');
                            }
                        } else {
                            $query = "UPDATE #__lms_user_courses SET role_id = $role_id WHERE user_id = $user_id AND course_id = $course_id";
                            $db->setQuery($query);
                            if (!$db->query()) {
                                echo "<script> alert('" . $db->getErrorMsg() . "'); window.history.go(-1); </script>\n";
                            }
                            $mess = str_replace('{USER}', $usr_name, _JLMS_USERS_USR_ROLE_UPD);
                            JFactory::getApplication()->enqueueMessage($mess, 'message');
                        }
                    } else {
                        $mess = _JLMS_USERS_SLC_ROLE_F_LIST;
                        if ($is_j3)
                            JFactory::getApplication()->enqueueMessage($mess, 'warning');
                        else
                            JError::raiseNotice(100, $mess);
                    }
                }
            }
        }
    } else {
        if ($user_ids) {
            
            $uids = explode(',', $user_ids);
            
            if (sizeof($uids)) {
                $is_j3 = JLMS_J30version();
                foreach ($uids as $uid) {
                    $query = "SELECT `name` FROM #__users WHERE `id` = $uid";
                    $db->setQuery($query);
                    $usr_name = $db->LoadResult();

                    $proceed_with_store = true;

                    $user_id = (int) $uid;
                    $query = "SELECT count(*) FROM #__lms_users WHERE user_id = $user_id";
                    $db->setQuery($query);
                    $count_yet = $db->LoadResult();
                    if ($count_yet && !$id) {
                        $mess = str_replace('{USER}', $usr_name, _JLMS_USERS_EX_ADM_TEACH_L);
                        if ($is_j3)
                            JFactory::getApplication()->enqueueMessage($mess, 'warning');
                        else
                            JError::raiseNotice(100, $mess);
                        $proceed_with_store = false;
                    }

                    if ($proceed_with_store) {
                        $row = new JLMS_user_system_level($db);
                        $_POST['user_id'] = (int) $uid;

                        if (!$row->bind($_POST)) {
                            echo "<script> alert('" . $row->getError() . "'); window.history.go(-1); </script>\n";
                            exit();
                        }

                        if (!$row->check()) {
                            echo "<script> alert('" . $row->getError() . "'); window.history.go(-1); </script>\n";
                            exit();
                        }

                        if (!$row->store()) {
                            echo "<script> alert('" . $row->getError() . "'); window.history.go(-1); </script>\n";
                            exit();
                        }
                        
                          
                        $mess = str_replace('{USER}', $usr_name, ($id?_JLMS_USERS_USR_ROLE_UPD:_JLMS_USERS_ADD_TEACH_ASSIST));
                        JFactory::getApplication()->enqueueMessage($mess, 'message');
                    }
                }

                $_POST['user_id'] = null;
            }
        }
    }

    if ($course_id) {
        $query = "UPDATE #__lms_forum_details SET need_update = 1 WHERE course_id = $course_id";
        $JLMS_DB->setQuery($query);
        if ($JLMS_DB->query()) {
            $forum = & JLMS_SMF::getInstance();
            if ($forum)
                $forum->applyChanges($course_id);
        }
    }

    if ($page == 'apply') {
        if ($view_by) {
            mosRedirect("index.php?option=$option&task=lms_users&page=editA&view_by=1&course_id=$course_id&id=$user_id", $msg);
        } else {
            mosRedirect("index.php?option=$option&task=lms_users&page=editA&id=" . $row->id, $msg);
        }
    } else {
        mosRedirect("index.php?option=$option&task=lms_users", $msg);
    }
}

?>