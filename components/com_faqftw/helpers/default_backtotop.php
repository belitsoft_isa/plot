<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
?>
<?php
	if($this->params->get('back_to_top_type') == 1){ ?>
		<span class="backtotop_<?php echo $this->params->get('list_style'); ?>_<?php echo $this->params->get('align'); ?>">
		<?php   
		$params = 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no';
        $href = "onclick=\"window.open('index.php?option=com_faqftw&view=faq&tmpl=component&item=" . $this->_currentFaq->id . "&print=1','win2','".$params."'); return false;\"";
		?>
        <a <?php echo $href; ?> href="javascript:;" style="text-decoration: none; background: none;" >
		<?php if($this->params->get('print_type') == 1){ ?>
					<img src="<?php echo JURI::base() . 'media/com_faqftw/images/print.png' ?>" alt="Print"/>
		<?php }elseif($this->params->get('print_type') == 2){ 
			  		echo JText::_('COM_FAQFTW_PRINT'); 
			  } ?>
			
		</a>
		<?php
			if($this->params->get('back_to_top_func') == 2){ ?>
				<a href="#faqtitle" style="text-decoration: none; background: none;">
	  <?php }elseif($this->params->get('back_to_top_func') == 1){ ?>
	 			<a href="#first" style="text-decoration: none; background: none;">
	  <?php }else{ ?>
				 <a href="#" style="text-decoration: none; background: none;">
	  <?php } ?>
			<img src="<?php echo JURI::base() . 'media/com_faqftw/images/back-to-top.png' ?>" alt="Back to top"/>
			</a>
		</span>
<?php }elseif($this->params->get('back_to_top_type') == 2){ ?>
		<span class="backtotop_<?php echo $this->params->get('list_style') ?>_<?php echo $this->params->get('align'); ?>">
		<?php   
		$params = 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no';
        $href = "onclick=\"window.open('index.php?option=com_faqftw&view=faq&tmpl=component&item=" . $this->_currentFaq->id . "&print=1','win2','".$params."'); return false;\"";
		?>
        <a <?php echo $href; ?> href="javascript:;" style="text-decoration: none; background: none; margin: 0 10px 0 0;" >
		<?php if($this->params->get('print_type') == 1){ ?>
					<img src="<?php echo JURI::base() . 'media/com_faqftw/images/print.png' ?>" alt="Print"/>
		<?php }elseif($this->params->get('print_type') == 2){ 
			  		echo JText::_('COM_FAQFTW_PRINT'); 
			  } ?>
			
		</a>
			<span class="text-back">
			<?php
			if($this->params->get('back_to_top_func') == 2){ ?>
				<a href="#faqtitle">
	  <?php }elseif($this->params->get('back_to_top_func') == 1){ ?>
	 			<a href="#first">
	  <?php }else{ ?>
				 <a href="#">
	  <?php } ?>
				 <?php  echo JText::_('COM_FAQFTW_BACK_TO_TOP'); ?>
				</a>
			</span>
		</span>
<?php
	  }
?>
