<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

//get user access level
$user = JFactory::getUser();

$user_access_level = max ($user->getAuthorisedViewLevels());

if( $this->params->get('align') == "ltr"){
	$text_align = "left";
}else{
	$text_align = "right";
}

?>
	<div class="ask_new_question" style="text-align:<?php echo $text_align?>;"> 
		<div class="ask_new_question_header" style="text-align:<?php echo $text_align?>;"> 
		<?php 
			if($user_access_level >= $this->params->get('show_ask_question_access')){ ?>
			<div class="ask_question_back_image_<?php echo $this->params->get('align'); ?>">
			<span id="ask_question" class="ask_new_link"><?php echo JText::_('COM_FAQFTW_ASK_A_NEW_QUESTION'); ?></span>
			</div>
		</div>
			<?php echo $this->loadTemplate('askcore'); ?>
			<?php }else{ ?>
						<div class="no_access"><?php echo JText::_('COM_FAQFTW_ASK_QUESTION_NO_ACCESS'); ?></div>
				</div>
			<?php
				} ?>
	</div>	