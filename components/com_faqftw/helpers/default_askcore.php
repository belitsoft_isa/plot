<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$error = '';
$publickey = $this->params->get('publickey');
$privatekey= $this->params->get('privatekey');

//get user access level
$user = JFactory::getUser();

$user_access_level = max ($user->getAuthorisedViewLevels());

if( $this->params->get('align') == "ltr"){
	$text_align = "left";
}else{
	$text_align = "right";
}

if($this->params->get('show_captcha',1) && $publickey != '' && $privatekey != ''){
	require_once(JPATH_ROOT . DS . 'media/com_faqftw/captcha/recaptchalib.php');
}	
?>
			<script type="text/javascript">
				function validateForm() {
					var newFaqForm = document.faqForm; 					
					if((newFaqForm.title.value) == '' || (newFaqForm.title.value) == '<?php echo JText::_('COM_FAQFTW_TYPE_YOUR_QUESTION'); ?>') {
						alert("<?php echo (JText::_( 'ALERT_MISSING_QUESTION' ));?>");
						newFaqForm.title.focus();
						return false;
					}else if( newFaqForm.title.value.length < 5 ){
						alert("<?php echo (JText::_( 'ALERT_QUESTION_TOO_SHORT' ));?>");
						newFaqForm.title.focus();
						return false;						
					}
				}				 
			</script>		
			<div id="ask" class="ask">					
				<div id="new_question_form" class="new_faq_form">
					<form action="<?php echo JRoute::_('index.php?option=com_faqftw&task=faq.save'); ?>" method="post" name="faqForm" id="faqForm" onsubmit="return validateForm()" >									
						<fieldset>									
							<textarea name="title" id="title" rows="3" onfocus="if (this.value == '<?php echo JText::_('COM_FAQFTW_TYPE_YOUR_QUESTION'); ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php echo JText::_('COM_FAQFTW_TYPE_YOUR_QUESTION'); ?>';}" style="text-align:<?php echo $text_align?>;"><?php echo JText::_('COM_FAQFTW_TYPE_YOUR_QUESTION'); ?></textarea>	
						<?php	
							if ($this->params->get('show_captcha') == "1" && $publickey != '' && $privatekey != '' ){
								echo recaptcha_get_html($publickey, $error, $this->params->get('captcha_theme'),false, false);
							}elseif($this->params->get('show_captcha') == "1" && JPluginHelper::isEnabled('captcha','recaptcha')){
								JPluginHelper::importPlugin('captcha');
								$dispatcher = JDispatcher::getInstance();
								$dispatcher->trigger('onInit','dynamic_recaptcha_1');
							?>
								<div id="dynamic_recaptcha_1"></div>
							<?php
							}

							if($text_align == "left"){ ?>
								<input type="submit" class="btn btn-primary" value="<?php echo (JText::_( 'COM_FAQFTW_SUBMIT_QUESTION' )); ?>" onclick="javascript:submitbutton();" />				
								<a class="btn" title="<?php echo (JText::_( 'COM_FAQFTW_CANCEL' )); ?>" onclick="javascript:void(0);" id="ask-cancel">Cancel</a>						   <?php
							}else{ ?>
								<input type="submit" class="btn btn-primary" value="<?php echo (JText::_( 'COM_FAQFTW_SUBMIT_QUESTION' )); ?>" onclick="javascript:submitbutton();" />
								<a class="btn" title="<?php echo (JText::_( 'COM_FAQFTW_CANCEL' )); ?>" onclick="javascript:void(0);" id="ask-cancel">Cancel</a>						
							<?php
							} 
							?>
							<input type="hidden" name="published" id="published" value="0"/>
							<input type="hidden" name="access" id="access" value="1"/>
							<input type="hidden" name="asked_question" id="asked_question" value="1"/>
							<input type="hidden" name="description" value="" />	
							<input type="hidden" name="ordering" value="" />
							<input type="hidden" name="option" value="com_faqftw" />	
							<input type="hidden" name="task" value="faq.save" />	
							<input type="hidden" name="view" value="<?php echo $this->getName(); ?>" />
							<?php echo JHtml::_('form.token');?>	
						</fieldset>	
		  			</form>							
				</div>
			</div>	