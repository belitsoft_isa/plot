<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Registration controller class for Users.
 *
 * @package		Joomla.Site
 * @subpackage	com_users
 * @since		1.6
 */
class FaqftwControllerFaq extends JControllerLegacy
{
	
	/**
	 * Method to add an asked FAQ.
	 *
	 * @return	boolean		True on success, false on failure.
	 */
	public function save()
	{
		// Initialise variables.
		$app = JFactory::getApplication();
		$params = $app->getParams();
		$model	= $this->getModel('Faqs', 'FaqftwModel');
		$view = JRequest::getVar('view', 'faqs', 'post', 'string');

		// Check for request forgeries.
		if(!JSession::checkToken()){
			$this->setRedirect(JRoute::_('index.php?option=com_faqftw&view=' . $view, false));
			return false;
		} 

		if ($params->get('show_captcha',1) && $params->get('publickey') != "" && $params->get('privatekey') != ""){
			require_once(JPATH_ROOT . DS . '/media/com_faqftw/captcha/recaptchalib.php');
			$private_key = $params->get('privatekey');
			$captcha_challenge = JRequest::getVar('recaptcha_challenge_field', '', 'post', 'string');
			$captcha_response = JRequest::getVar('recaptcha_response_field', '', 'post', 'string');
			if($captcha_response == '' || $captcha_challenge == ''){
				$this->setMessage(JText::sprintf('COM_FAQFTW_CAPTCHA_EMPTY', $model->getError()), 'warning');
				$this->setRedirect(JRoute::_('index.php?option=com_faqftw&view='. $view, false));
				return false;
			}
			$resp = recaptcha_check_answer($private_key, $_SERVER["REMOTE_ADDR"], $captcha_challenge, $captcha_response);
	  		if (!$resp->is_valid) {
	    	// What happens when the CAPTCHA was entered incorrectly
				$this->setMessage(JText::sprintf('COM_FAQFTW_CAPTCHA_INCORRECT', $model->getError()), 'warning');
				$this->setRedirect(JRoute::_('index.php?option=com_faqftw&view='. $view, false));
				return false;
			}
			
		}elseif ($params->get('show_captcha',1) && JPluginHelper::isEnabled('captcha','recaptcha')){
			$post = JRequest::get('post');      
			JPluginHelper::importPlugin('captcha');
			$dispatcher = JDispatcher::getInstance();
			$res = $dispatcher->trigger('onCheckAnswer',$post['recaptcha_response_field']);
			fwrite($fh, $res[0] . "\n");
			if(!$res[0]){
			$this->setMessage(JText::sprintf('COM_FAQFTW_CAPTCHA_INCORRECT', $model->getError()), 'warning');
			$this->setRedirect(JRoute::_('index.php?option=com_faqftw&view='. $view, false));
			    return false;
			}
		}

		$data = array(
			'title' => JRequest::getVar('title', '', 'post', 'string'),
			'description' => JRequest::getVar('description', '', 'post', 'string'),
			'state' => JRequest::getVar('published', '0', 'post', 'string'),
			'access' => JRequest::getVar('access', '1', 'post', 'string'),
			'catid' => JRequest::getVar('category_id', '0', 'post', 'string'),
			'asked_question' => JRequest::getVar('asked_question', '0', 'post', 'string'),
			'ordering' => JRequest::getVar('ordering', '', 'post', 'string')
		);
		// Attempt to save the data.
		$return	= $model->save($data);

		// Check for errors.
		if ($return === false) {
		
			// Redirect back to the edit screen.
			$this->setMessage(JText::sprintf('COM_FAQFTW_SAVE_FAILED', $model->getError()), 'warning');
			$this->setRedirect(JRoute::_('index.php?option=com_faqftw&view='. $view, false));
			return false;
		}

		// Send email if requested.
		if($params->get('send_mail') == 1)
		{
			$mailer = JFactory::getMailer();
			$mailer->setSender('FAQ FTW');
			if($params->get('email_address') != "" ){
				$mailer->addRecipient(explode(",",$params->get('email_address')));
			}else{
				$config =& JFactory::getConfig();			
				$recipent =  $config->get('config.mailfrom');
				$mailer->addRecipient($recipent);
			}
			$mailer->setSubject(JText::_('COM_FAQFTW_NEW_ASKED_QUESTION'));
			$body = '<h2>'. JText::sprintf('COM_FAQFTW_NEW_ASKED_QUESTION_TO', JURI::root()) . '</h2>'
					    . '<p>' . JText::_('COM_FAQFTW_NEW_ASKED_QUESTION_BODY') . '</p>'
						. '<div>' . $data['title'] . '</div>';
			$mailer->setBody($body);
			$mailer->isHTML(true);
			$mailer->send();
		}
		
		// Redirect to view.
		$this->setMessage(JText::_('COM_FAQFTW_SAVE_SUCCESS'));
		$this->setRedirect(JRoute::_('index.php?option=com_faqftw&view='. $view, false));
		return true;
	}
}
