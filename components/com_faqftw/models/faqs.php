<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.modellist');

class FaqftwModelFaqs extends JModelList
{
  	var $_total = null;
	var $_pagination = null;
    var $_isEmptyCatsShown = false;
	
	function __construct()
 	 {
        parent::__construct();
 
        $mainframe = JFactory::getApplication();
		$params = $mainframe->getParams();
		$limit_param = $params->get('pagination_limit');
		$this->_isEmptyCatsShown = $params->get('show_empty_category');

		if($params->get('show_pagination',1)){	
			if(!is_numeric($limit_param) || empty($limit_param)){
				$limit_param = '20';
			}
		}else{
			$limit_param = null;
		}
        // Get pagination request variables
        $limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit',$limit_param, 'int');
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
 
        // In case limit has been changed, adjust it
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
 
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);
	  }
	  
	public function getCategory(){
		//get user access level
		$user = JFactory::getUser();
		$aid = max ($user->getAuthorisedViewLevels());

		$query = parent::getListQuery();
		$query->select('*');
		$query->from('#__faqftw_faq');	
		$query->where('state = 1 AND access <= ' . $aid);
		$faqs = $db->loadObjectList();
		$rows = array();

		//Avoid warnings.
		if( !is_object( $rows[0] ) ) {
  		  $rows[0] = new StdClass;
		}

		$rows[0]->faqs = $faqs;

		return $rows;
	}
	
	public function getFaqsWithoutCats($returnRows = false)
	{
		$app = JFactory::getApplication();
		$params = $app->getParams();
		$db = $this->getDBO();		
		$cats = $params->get('categories_selected');
		
		//get user access level
		$user = JFactory::getUser();
		$aid = max ($user->getAuthorisedViewLevels());

		$query = parent::getListQuery();
		$query->select('*');
		$query->from('#__faqftw_faq');	
		$query->where('state = 1 AND access <= ' . $aid);
		if(!empty($cats)){
			$cats_where = '(';
			for($i=0;$i < count($cats);$i++){
				$cats_where .= 'catid = ' . $cats[$i];
				if($i != count($cats) - 1){
					$cats_where .= ' OR ';
				}
			}
			$cats_where .= ')';
			$query->where($cats_where);
		}
		$query->order('ordering ASC');	
		if($returnRows){
			return $query;
		}
		$db->setQuery( $query, $this->getState('limitstart'), $this->getState('limit'));
		$faqs = $db->loadObjectList();
		$rows = array();
		
		//Avoid warnings.
		if( !is_object( $rows[0] ) ) {
  		  $rows[0] = new StdClass;
		}
		
		$rows[0]->faqs = $faqs;

		return $rows;
	}
	
	/**
	 * Method to get an FAQ from a category.
	 * @access public
	 * @property the category ID.
	 * @return array
	 */
	public function getFAQS($catid)
	{
		$db = $this->getDBO();		
		$user = JFactory::getUser();
		$aid = max ($user->getAuthorisedViewLevels());

		$query = parent::getListQuery();
		$query->select('*');
		$query->from('#__faqftw_faq');	
		$query->where('state = 1 AND access <= ' . $aid);
		$query->where('catid = ' . $catid);
		$query->order('ordering ASC');		
		
		$db->setQuery( $query );
		$rows = $db->loadObjectList();
		
		return $rows;
	}
	
	/**
	 * Method to get the data, With or without the categories.
	 * @access public
	 * @return array
	 */
	function getData($returnRows = false) {
   		
		$app = JFactory::getApplication();
		$params = $app->getParams();
	
		// if data hasn't already been obtained, load it
        if (empty($this->_data)) {
			if( $params->get('show_cats') == 1 || $params->get('show_cats') == ""){
					 return $this->getFaqsWithCats();
			}else{
					 return $this->getFaqsWithoutCats();
			}	
        }
	}
	
	/**
	 * Method to get Categories and Faqs
	 * @access public
	 * @return array
	 */
	function getFaqsWithCats($returnRows = false) {

		$db = $this->getDBO();			

		$where = $this->_buildWhere();	

		$query = "SELECT * FROM #__categories ". $where ." ORDER BY rgt ";		
		
		$db->setQuery( $query, $this->getState('limitstart'), $this->getState('limit'));

		if($returnRows){
			return $query;
		}
		
		$rows = $db->loadObjectList();
		
		foreach($rows as $key=>$row){
			$rows[$key]->faqs = $this->getFAQS($row->id);
			if(empty($rows[$key]->faqs) && !$this->_isEmptyCatsShown){
				unset($rows[$key]);
			}
		}
		
		return $rows;
	}
	
	/**
	 * Method to get Query Where
	 * @access public
	 * @return array
	 */
	function _buildWhere() {
		
		$where = array();
		$app = JFactory::getApplication();
		$params = $app->getParams();
		$cats = $params->get('categories_selected');
		
		// Filter by access level.
		//if ($access = $this->getState('filter.access')) {
		$user	= JFactory::getUser();
		$groups	= implode(',', $user->getAuthorisedViewLevels());
		$where[] = 'access IN ('.$groups.')';
		//}
		if(isset($cats)){	
			$cats	= implode(',', $cats);
			if(!empty($cats)){
				$where[] = 'id IN ('.$cats.')';
			}
		}
		$where[] = 'parent_id = 1';
		$where[] = 'extension = "com_faqftw" ';
		$where[] = 'published = 1';		
		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );

		return $where;	
	}
	
	function getPagination()
  	{
        // Load the content if it doesn't already exist
        if (empty($this->_pagination)) {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
        }
        return $this->_pagination;
  	}
	
	function getTotal()
  	{
        // Load the content if it doesn't already exist
		$app = JFactory::getApplication();
		$params = $app->getParams();
		
        if (empty($this->_total)) {
			if( $params->get('show_cats') == 1 || $params->get('show_cats') == ""){
				$query = $this->getFaqsWithCats(true);
			}elseif( $params->get('show_cats') == 0 ){
				$query = $this->getFaqsWithoutCats(true);
			}	

            $this->_total = $this->_getListCount($query);       
        }
		
        return $this->_total;
 	}

	/**
	 * Method to get Query SubWhere
	 * @access public
	 * @return array
	 */
	function _buildSubWhere($id) {
		
		$subwhere = array();
		
		// Filter by access level.
		//if ($access = $this->getState('filter.access')) {
		$user	= JFactory::getUser();
		$groups	= implode(',', $user->getAuthorisedViewLevels());
		$subwhere[] = 'access IN ('.$groups.')';
		//}
		
		$subwhere[] = 'extension = "com_faqftw" ';
		$subwhere[] = 'published = 1';
		$subwhere[] = "parent_id = ".$id;
					
		$subwhere 		= ( count( $subwhere ) ? ' WHERE ' . implode( ' AND ', $subwhere ) : '' );

		return $subwhere;	
	}
	
	function save($data) {
		JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_faqftw'.DS.'tables');
		$row = JTable::getInstance('faq', 'FaqftwTable');

		if (!$row->bind( $data )) {
		        return JError::raiseWarning( 500, $row->getError() );
		}
		if (!$row->check()) {
		        return JError::raiseWarning( 500, $row->getError() );
		}
		if (!$row->store()) {
		        JError::raiseError(500, $row->getError() );
		}
	}
}