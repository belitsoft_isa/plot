<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.modeladmin');

class FaqftwModelFaq extends JModelAdmin
{
	public function getTable($type = 'faq', $prefix = 'FaqftwTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState('com_faqftw.edit.faq.data', array());

		if (empty($data)) {
			$data = $this->getItem();

			// Prime some default values.
			if ($this->getState('faq.id') == 0) {
				$app = JFactory::getApplication();
				$data->set('category_id', JRequest::getInt('category_id', $app->getUserState('com_faqftw.faqs.filter.category_id')));
			}
		}


		return $data;
	}
	
	public function getForm($data = array(), $loadData = true)
	{
		$form = $this->loadForm('com_faqftw.faq', 'faq', array('control' => 'jform', 'load_data' => $loadData));


		return $form;
	}
	
	public function getData(){
		$faqId = JRequest::getVar( 'item' );
		JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_faqftw'.DS.'tables');
		$table =& JTable::getInstance('Faq', 'FaqftwTable');
		return $table->getFaq($faqId);
	}
	
	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since	1.6
	 */
	protected function prepareTable(&$table)
	{
		$date = JFactory::getDate();
		$user = JFactory::getUser();

		$table->title		= htmlspecialchars_decode($table->title, ENT_QUOTES);
		$table->alias		= JApplication::stringURLSafe($table->alias);
		//$table->description = htmlentities($table->description);

		if (empty($table->alias)) {
			$table->alias = JApplication::stringURLSafe($table->title);
		}

		if (empty($table->id)) {
			// Set the values

			// Set ordering to the last item if not set
			if (empty($table->ordering)) {
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__faqftw_faq');
				$max = $db->loadResult();

				$table->ordering = $max+1;
			}
		}
		else {
			// Set the values
		}
	}
	
	
	
}
