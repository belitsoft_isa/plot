<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$document = JFactory::getDocument();

$document->addStyleSheet(JURI::base() . 'media/com_faqftw/css/faq.css');

jimport( 'joomla.application.categories' );

if($this->params->get('align') == ""){
	$this->params->set('align',"ltr");
}
if ($this->params->get('list_style') == ""){
	$this->params->set('list_style',"default");
}
if( $this->params->get('align') == "ltr"){
	$text_align = "left";
}else{
	$text_align = "right";
}

$q_color = $this->getColorFromString($this->params->get('q_color')); 
$a_color = $this->getColorFromString($this->params->get('a_color')); 
$background_color = $this->getColorFromString($this->params->get('background_color'));
?>
<dl class="<?php echo $this->params->get('align'); ?>" <?php echo ($background_color != "" ? 'style="background:' . $background_color . ';"' : ''); ?>>
		<dt id="<?php echo $this->faq->alias; ?>">
			<span class="faq_default_title" <?php echo ($q_color != "" ? 'style="color:' . $q_color . ';"' : ''); ?>>
				<?php echo $this->faq->title; ?>
			</span>
		</dt>
		<dd class="faq_answer_default" <?php echo ($a_color != "" ? 'style="color:' . $a_color . ';"' : ''); ?>><?php echo $this->faq->description ?></dd>
 </dl>
	
<?php
if($this->params->get('show_footer',1)){
	echo $this->loadTemplate('footer');
}
?>
