<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$document = JFactory::getDocument();


$document->addStyleSheet(JURI::base() . 'media/com_faqftw/css/faq.css');

//get user access level
$user = JFactory::getUser();
$user_access_level = max ($user->getAuthorisedViewLevels());

$publickey = $this->params->get('publickey');
$privatekey= $this->params->get('privatekey');
if($this->params->get('show_captcha',1) && $publickey != '' && $privatekey != ''){
	require_once(JPATH_ROOT . DS .  'media/com_faqftw/captcha/recaptchalib.php');
}
	$error = '';
if($this->params->get('align') == ""){
	$this->params->set('align',"ltr");
}
if( $this->params->get('align') == "ltr"){
	$text_align = "left";
}else{
	$text_align = "right";
}
if($this->params->get('show_ask_question_access') == ""){
	$this->params->set('show_ask_question_access',1);
}
$q_color =str_replace('#','',$this->params->get('q_color')); 
$a_color =str_replace('#','',$this->params->get('a_color')); 
  
if($this->params->get('show_title',1)){
	if($this->params->get('title') != null){ ?>
		<h1 style="text-align:<?php echo $text_align?>;"><?php echo $this->params->get('title'); ?></h1>
	<?php }else{ ?>
		<h1 style="text-align:<?php echo $text_align?>;"><?php echo JText::_('COM_FAQFTW_FAQS'); ?></h1>
	<?php }
}
if($this->params->get('message')){ ?>
	<div class="faqmessage">
		<p style="text-align:<?php echo $text_align?>;"><?php echo $this->params->get('message'); ?></p>
	</div>
<?php } ?>
	<div class="ask_new_question_plain" style="text-align:<?php echo $text_align?>;"> 
		<?php 
			if($user_access_level >= $this->params->get('show_ask_question_access')){ ?>
				<?php echo $this->loadTemplate('askcore'); ?>
	<?php }else{ ?>
				<div class="no_access"><?php echo JText::_('COM_FAQFTW_ASK_QUESTION_NO_ACCESS'); ?></div>
			</div>
	<?php
		  } ?>
</div>	
<?php
if($this->params->get('show_footer',1)){
	echo $this->loadTemplate('footer');
}
?>