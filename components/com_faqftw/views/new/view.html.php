<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class FaqftwViewNew extends JViewLegacy
{
	protected $items;
	protected $params;
	protected $model;

	public function display($tpl = null)
	{
		$this->addTemplatePath(JPATH_COMPONENT.'/helpers/');
		//$this->items = $this->get('Items');	
		//$this->params = JFactory::getApplication()->getParams();
		$app			= JFactory::getApplication();
								$this->params 		= $app->getParams(); 
			//s					$this->model		= $this->getModel();
					

		parent::display($tpl);
	}
}