<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.view');

class FaqftwViewFaqs extends JViewLegacy
{
	protected $items;
	protected $params;
	protected $model;
	protected $rows;
	protected $_currentFaq;
	
	public function display($tpl = null)
	{
		$this->addTemplatePath(JPATH_COMPONENT.'/helpers/');
		
		$pagination = $this->get('Pagination');
		// Check for errors.
        if (count($errors = $this->get('Errors'))) 
        {
           JError::raiseError(500, implode('<br />', $errors));
           return false;
        }
		
		$app	= JFactory::getApplication();
		$this->params = $app->getParams(); 
		$this->model  = $this->getModel();
		$this->pagination = $pagination;
		$this->rows  = $this->get('Data');

		$this->assignRef('pagination', $pagination);
		parent::display($tpl);
	}
		
	protected function getColorFromString($str){
		//Check for a hex color string '#c1c2b4'
		if(preg_match('/^#[a-f0-9]{6}$/i', $str)) //hex color is valid
		{
		      return $str;
		} 
		//Check for a hex color string without hash 'c1c2b4'
		else if(preg_match('/^[a-f0-9]{6}$/i', $str)) //hex color is valid
		{
		      return ('#' . $str);
		} 
		
		return $str;
	}
}