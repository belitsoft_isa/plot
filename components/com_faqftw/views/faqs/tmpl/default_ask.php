<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

//get user access level
$user = JFactory::getUser();

$user_access_level = max ($user->getAuthorisedViewLevels());
$publickey = $this->params->get('publickey');
$privatekey= $this->params->get('privatekey');
if($this->params->get('show_captcha',1) && $publickey != '' && $privatekey != ''){
	require_once(JPATH_ROOT . DS . 'media/com_faqftw/captcha/recaptchalib.php');
}
$error = '';
	
?>
	<div class="ask_new_question" style="text-align:<?php echo $text_align?>;"> 
		<div class="ask_new_question_header" style="text-align:<?php echo $text_align?>;"> 
		<?php 
			if($user_access_level >= $this->params->get('show_ask_question_access')){ ?>
			<div class="ask_question_back_image_<?php echo $this->params->get('align'); ?>">
			<span id="ask_question" class="ask_new_link"><?php echo JText::_('COM_FAQFTW_ASK_A_NEW_QUESTION'); ?></span>
			</div>
		</div>
			<script type="text/javascript">
				function submitbutton() {
					var newFaqForm = document.faqForm; 					
					if((newFaqForm.title.value) == '' || (newFaqForm.title.value) == '<?php echo JText::_('COM_FAQFTW_TYPE_YOUR_QUESTION'); ?>') {
						alert("<?php echo (JText::_( 'ALERT_MISSING_QUESTION' ));?>");
						newFaqForm.title.focus();
					}else if( newFaqForm.title.value.length < 5 ){
						alert("<?php echo (JText::_( 'ALERT_QUESTION_TOO_SHORT' ));?>");
						newFaqForm.title.focus();						
					}else{
						newFaqForm.submit();
					}
				}				 
			</script>		
			<div id="ask" class="ask">					
				<div id="new_question_form" class="new_faq_form" style="display:none;">												<form action="<?php echo JRoute::_('index.php?option=com_faqftw&task=faq.save'); ?>" method="post" name="faqForm" id="faqForm" >									
						<fieldset>									
							<textarea name="title" id="title" rows="3" onfocus="if (this.value == '<?php echo JText::_('COM_FAQFTW_TYPE_YOUR_QUESTION'); ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php echo JText::_('COM_FAQFTW_TYPE_YOUR_QUESTION'); ?>';}" style="text-align:<?php echo $text_align?>;"><?php echo JText::_('COM_FAQFTW_TYPE_YOUR_QUESTION'); ?></textarea>	
						<?php	
							if ($this->params->get('show_captcha') == "1" && $publickey != '' && $privatekey != '' ){
								echo recaptcha_get_html($publickey, $error, $this->params->get('captcha_theme'),false, false);
							}elseif($this->params->get('show_captcha') == "1" && JPluginHelper::isEnabled('captcha','recaptcha')){
								JPluginHelper::importPlugin('captcha');
								$dispatcher = JDispatcher::getInstance();
								$dispatcher->trigger('onInit','dynamic_recaptcha_1');
							?>
								<div id="dynamic_recaptcha_1"></div>
							<?php
							}
							if($text_align == "left"){ ?>
								<input type="button" class="button" value="<?php echo (JText::_( 'COM_FAQFTW_SUBMIT_QUESTION' )); ?>" onclick="javascript:submitbutton();" />
								<input type="button" class="button" value="<?php echo (JText::_( 'COM_FAQFTW_CANCEL' )); ?>" onclick="javascript:void(0);" id="ask-cancel" />																						<?php
							}else{ ?>
								<input type="button" class="button" value="<?php echo (JText::_( 'COM_FAQFTW_CANCEL' )); ?>" onclick="javascript:void(0);" id="ask-cancel" />	
								<input type="button" class="button" value="<?php echo (JText::_( 'COM_FAQFTW_SUBMIT_QUESTION' )); ?>" onclick="javascript:submitbutton();" />
							<?php
							} 
							?>
							<input type="hidden" name="published" id="published" value="0"/>
							<input type="hidden" name="access" id="access" value="1"/>
							<input type="hidden" name="asked_question" id="asked_question" value="1"/>
							<input type="hidden" name="description" value="" />	
							<input type="hidden" name="ordering" value="" />
							<input type="hidden" name="option" value="com_faqftw" />	
							<input type="hidden" name="task" value="faq.save" />	
							<input type="hidden" name="view" value="faqs" />
							<?php echo JHtml::_('form.token');?>	
						</fieldset>	
		  			</form>							
				</div>
			</div>	
			<?php }else{ ?>
						<div class="no_access"><?php echo JText::_('COM_FAQFTW_ASK_QUESTION_NO_ACCESS'); ?></div>
			<?php
				} ?>
	</div>	