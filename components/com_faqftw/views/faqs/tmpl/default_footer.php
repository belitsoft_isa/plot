<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

?>
<div class="footer">
	<a target="_blank" title="FTW Extensions - FAQ FTW Component" href="http://extensions.4u2.co.il">FTW Extensions - FAQ FTW</a>
</div>
