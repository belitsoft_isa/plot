<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

$document = JFactory::getDocument();

$document->addScript(JURI::base() . 'media/com_faqftw/js/jquery-1.6.1.min.js');
$document->addScript(JURI::base() . 'media/com_faqftw/js/jquery-noconflict.js');
$document->addScript(JURI::base() . 'media/com_faqftw/js/faq.js');
$document->addStyleSheet(JURI::base() . 'media/com_faqftw/css/faq.css');

jimport( 'joomla.application.categories' );

if($this->params->get('align') == ""){
	$this->params->set('align',"ltr");
}
if ($this->params->get('list_style') == ""){
	$this->params->set('list_style',"default");
}
if( $this->params->get('align') == "ltr"){
	$text_align = "left";
}else{
	$text_align = "right";
}
if($this->params->get('show_cats',1)){
	$display_cat = true;
}else{
	$display_cat = false;
}
if($this->params->get('back_to_top_func') == ""){
	$this->params->set('back_to_top_func',0);
}
if($this->params->get('show_ask_question') == ""){
	$this->params->set('show_ask_question',1);
}
if($this->params->get('show_ask_question_order') == ""){
	$this->params->set('show_ask_question',0);
}
if($this->params->get('show_ask_question_access') == ""){
	$this->params->set('show_ask_question_access',1);
}

$q_color = $this->getColorFromString($this->params->get('q_color')); 
$a_color = $this->getColorFromString($this->params->get('a_color')); 
$background_color = $this->getColorFromString($this->params->get('background_color'));

if($this->params->get('show_title',1)){ ?>
	<a name="faqtitle"></a>
	<?php
	if($this->params->get('title') != null){ ?>
		<h1 style="text-align:<?php echo $text_align?>;"><?php echo $this->params->get('title'); ?></h1>
	<?php }else{ ?>
		<h1 style="text-align:<?php echo $text_align?>;"><?php echo JText::_('COM_FAQFTW_FAQS'); ?></h1>
	<?php }
}
if($this->params->get('message')){ ?>
	<div class="faqmessage">
		<p style="text-align:<?php echo $text_align?>;"><?php echo $this->params->get('message'); ?></p>
	</div>
<?php } 

if($this->params->get('show_ask_question') == 1 && $this->params->get('show_ask_question_order') == 0 ){ 
		echo $this->loadTemplate('ask');
}
?>
<a name="first"></a>
<?php

foreach ($this->rows as $cat){ 
	if($display_cat){ ?>
			<a name="<?php echo $cat->alias; ?>"></a>
			<dl class="category <?php echo $this->params->get('align'); ?>">
				<dt id="<?php echo $cat->alias; ?>" class="category_head">
					<div class="category_title_<?php echo $this->params->get('align'); ?>">
						<?php echo $cat->title; ?>
					</div>

				</dt>
				
				<dd class="categories_faqs">
					<?php if($this->params->get('show_cats_desc') == 1){ ?>
						<span class="<?php echo $this->params->get('align'); ?>">
							<?php echo $cat->description; ?>
						</span>
				 	<?php } ?>
					<div class="faq_container">
<?php
	}
	if ($this->params->get('list_style') == 'default'): ?>
					
					<?php foreach ($cat->faqs as $faq): ?>
						<?php $this->_currentFaq = $faq; ?>
						<a name="<?php echo $faq->alias; ?>"></a>
						<dl class="faq_default <?php echo $this->params->get('align'); ?>" <?php echo ($background_color != "" ? 'style="background:' . $background_color . ';"' : ''); ?>>
							<dt id="<?php echo $faq->alias; ?>">
								<div class="letter_a_<?php echo $this->params->get('align'); ?>"></div>
								<div class="letter_q_<?php echo $this->params->get('align'); ?>"></div>
								<span class="faq_default_title" <?php echo ($q_color != "" ? 'style="color:' . $q_color . ';"' : ''); ?>>
									<?php echo $faq->title; ?>
								</span>
							</dt>
							
							<dd class="faq_answer_default" <?php echo ($a_color != "" ? 'style="color:' . $a_color . ';"' : ''); ?>><?php echo $faq->description ?></dd>
					   <?php if($this->params->get('back_to_top_type') != 0){
								echo $this->loadTemplate('backtotop');
							} ?>
						 </dl>
					<?php endforeach ?>
				
		<?php elseif($this->params->get('list_style') == 'simple'): ?>
				
					<?php foreach ($cat->faqs as $faq): ?>
						<?php $this->_currentFaq = $faq; ?>
						<dl class="faq_simple <?php echo $this->params->get('align'); ?>" <?php echo ($background_color != "" ? 'style="background:' . $background_color . ';"' : ''); ?>>
							<dt id="<?php echo $faq->alias; ?>">
								<span class="faq_question_simple_<?php echo $this->params->get('align'); ?>" <?php echo ($q_color != "" ? 'style="color:' . $q_color . ';"' : ''); ?>>
									<?php echo $faq->title ?>
								</span>
							</dt>
							<dd class="faq_answer_simple" <?php echo ($a_color != "" ? 'style="color:' . $a_color . ';"' : ''); ?>>
								<?php echo $faq->description ?>
								
							</dd>	
					  <?php if($this->params->get('back_to_top_type') != 0){
								echo $this->loadTemplate('backtotop');
							} ?>
						</dl>
					<?php endforeach ?>	
					
		<?php elseif($this->params->get('list_style') == 'standart'): ?>
				
					<?php foreach ($cat->faqs as $faq): ?>
					<?php $this->_currentFaq = $faq; ?>
					<dl class="faq_standart <?php echo $this->params->get('align'); ?>">
							<dt class="faq_question_standart" id="<?php echo $faq->alias; ?>" <?php echo ($background_color != "" ? 'style="background:' . $background_color . ';"' : ''); ?>>
								<span class="standart_title_<?php echo $this->params->get('align'); ?>" <?php echo ($q_color != "" ? 'style="color:' . $q_color . ';"' : ''); ?>>
									<?php echo $faq->title ?>
								</span>
							</dt>
							<dd class="faq_answer_standart" <?php echo ($a_color != "" ? 'style="color:' . $a_color . ';"' : ''); ?>>
								<?php echo $faq->description ?>
							</dd>	
					  <?php if($this->params->get('back_to_top_type') != 0){
								echo $this->loadTemplate('backtotop');
							} ?>
						</dl>
					<?php endforeach ?>	
				
		<?php endif ?>
	<?php	if($display_cat){ ?>
				</div>
			</dd>
		</dl>	
<?php 
			}
}

if($this->params->get('show_ask_question') == 1 && $this->params->get('show_ask_question_order') == 1 ){ 
	echo $this->loadTemplate('ask');
} 

if ($this->params->def('show_pagination', 0) == 1  || ($this->params->get('show_pagination') == 2 && $this->pagination->get('pages.total') > 1)) : ?>
	<div class="pagination" style="text-align:center;">

		<?php if ($this->params->def('show_pagination_results', 1)) : ?>
			<p class="counter">
					<?php echo $this->pagination->getPagesCounter(); ?>
			</p>
		<?php  endif; ?>
				<?php echo $this->pagination->getPagesLinks(); ?>
	</div>
<?php endif; ?>

<?php
if($this->params->get('show_footer',1)){
	echo $this->loadTemplate('footer');
}
if($this->params->get('faqs_default_status') == 0){ ?>
	<script type="text/javascript">
		jQuery(openAll);
	</script>
<?php
}elseif($this->params->get('faqs_default_status') == 1){ ?>
	<script type="text/javascript">
		jQuery(openCats);
	</script>
<?php
}
?>
