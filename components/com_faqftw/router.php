<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
defined('_JEXEC') or die;

jimport('joomla.application.categories');

/**
 * Build the route for the com_faqftw component
 *
 * @param	array	An array of URL arguments
 * @return	array	The URL arguments to use to assemble the subsequent URL.
 * @since	1.5
 */
function FaqftwBuildRoute(&$query)
{
	$segments	= array();

	// get a menu item based on Itemid or currently active
	$app		= JFactory::getApplication();
	$menu		= $app->getMenu();
	$params		= JComponentHelper::getParams('com_faqftw');

	// we need a menu item.  Either the one specified in the query, or the current active one if none specified
	if (empty($query['Itemid'])) {
		$menuItem = $menu->getActive();
		$menuItemGiven = false;
	}
	else {
		$menuItem = $menu->getItem($query['Itemid']);
		$menuItemGiven = true;
	}
	
	// are we dealing with an article or category that is attached to a menu item?
	if (($menuItem instanceof stdClass) && isset($query['view']) && $menuItem->query['view'] == $query['view']) {
		unset($query['view']);
		return $segments;
	}


	if(isset($query['view']))
    {
       $segments[] = $query['view'];
       unset( $query['view'] );
    }

	return $segments;
}

/**
 * Parse the segments of a URL.
 *
 * @param	array	The segments of the URL to parse.
 *
 * @return	array	The URL attributes to be used by the application.
 * @since	1.5
 */
function FaqftwParseRoute($segments)
{
	$vars = array();

	//Get the active menu item.
	$app	= JFactory::getApplication();
	$menu	= $app->getMenu();
	$item	= $menu->getActive();
	$params = JComponentHelper::getParams('com_faqftw');

	// Count route segments
	$count = count($segments);

	 $vars = array();
       switch($segments[0])
       {
	   	 case 'faqs':
                 $vars['view'] = 'faqs';
                 break;
	   }
	

	return $vars;
}
