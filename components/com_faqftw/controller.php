<?php
/*------------------------------------------------------------------------
# com_faqftw - FAQ FTW
# ------------------------------------------------------------------------
# author    FTW Extensions
# copyright Copyright (C) 2012 extensions.4u2.co.il. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.extensions.4u2.co.il
# Technical Support:  Email - support@extensions.4u2.co.il
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport('joomla.application.component.controller');
if(!defined('DS')){
define('DS',DIRECTORY_SEPARATOR);
}

class FaqftwController extends JControllerLegacy
{
	public function display($cachable = false, $urlparams = false)
	{
		// Get the document object.
		$document	= JFactory::getDocument();

		// Set the default view name and format from the Request.
		$vName	 = JRequest::getCmd('view', 'faqs');
		$vFormat = $document->getType();
		$lName	 = JRequest::getCmd('layout', 'default');
		
		$view = $this->getView($vName, $vFormat);
		$model = $this->getModel($vName);
		
		// Push the model into the view (as default).
		$view->setModel($model, true);
		$view->setLayout($lName);

		// Push document object into the view.
		$view->assignRef('document', $document);

		$view->display();
			
	}
	
}