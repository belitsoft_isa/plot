<?php
/**
* JoomBlog component for Joomla 3.x
* @package JoomBlog
* @author JoomPlace Team
* @Copyright Copyright (C) JoomPlace, www.joomplace.com
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

defined('_JEXEC') or die('Restricted access');

class JbblogEditBlogTask extends JbBlogBaseController{
	
	function JbblogEditBlogTask(){
		$this->toolbar = JB_TOOLBAR_BLOGGER;
	}

	
	function display()
	{
		global $_JB_CONFIGURATION, $Itemid;

		$mainframe	= JFactory::getApplication();
		$my	= JFactory::getUser();		
		$doc = JFactory::getDocument();
		
		if(!jbCanBlogCreate()){
			$mainframe->redirect($_SERVER['HTTP_REFERER'],JText::_('COM_JOOMBLOG_BLOG_ADMIN_NO_PERMISSIONS_TO_EDIT'));
			return;
		}
		$data = array();
		if (JFactory::getApplication()->input->get('blogid'))
		{
			$db			= JFactory::getDBO();
			$blog_id = JFactory::getApplication()->input->get('blogid');
			$query		= "SELECT * from #__joomblog_list_blogs as lb WHERE lb.id=".$blog_id;
			$db->setQuery( $query );
			$blog_data = $db->loadObject();
			$data['id'] = $blog_data->id;
			$data['user_id'] = $blog_data->user_id;
			$data['published'] = $blog_data->published;
			$data['create_date'] = $blog_data->create_date;
			$data['title'] = $blog_data->title;
			$data['alias'] = $blog_data->alias;
			$data['description'] = $blog_data->description;
			$data['metadesc'] = $blog_data->metadesc;
			$data['metakey'] = $blog_data->metakey;
			$data['asset_id'] = $blog_data->asset_id;
			$data['approved'] = $blog_data->approved;
			$data['access'] = $blog_data->access;
			$data['waccess'] = $blog_data->waccess;
			if(empty($data['alias'])) $data['alias'] = trim(jbTitleToLink($data['title']));
			if (trim(str_replace('-','',$data['alias'])) == '') {
			$data['alias'] = JFactory::getDate()->format('Y-m-d-H-i-s');
			}			
		}

		$pathway = $mainframe->getPathway();
		$tpl = new JoomblogTemplate();
 		$blogModel = $this->getModel('Blog', 'JoomblogModel');
		$blogForm = $blogModel->getForm($data);
		$tpl->set('blogForm', $blogForm, true);
		$html = $tpl->fetch(JB_TEMPLATE_PATH."/admin/editblog.html");
		$html = str_replace("src=\"icons", "src=\"" . rtrim( JURI::base() , '/' ) . "/components/com_joomblog/templates/admin/icons", $html);
		return $html;
	}
}
