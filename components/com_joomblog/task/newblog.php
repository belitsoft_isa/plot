<?php
/**
* JoomBlog component for Joomla 3.x
* @package JoomBlog
* @author JoomPlace Team
* @Copyright Copyright (C) JoomPlace, www.joomplace.com
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

defined('_JEXEC') or die('Restricted access');

class JbblogNewBlogTask extends JbBlogBaseController{
	
	function JbblogNewBlogTask(){
		$this->toolbar = JB_TOOLBAR_BLOGGER;
	}

	
	function display()
	{
		global $_JB_CONFIGURATION, $Itemid;

		$mainframe	= JFactory::getApplication();
		$my	= JFactory::getUser();		
		$doc = JFactory::getDocument();
		
		if(!jbCanBlogCreate()){
			$mainframe->redirect($_SERVER['HTTP_REFERER'],JText::_('COM_JOOMBLOG_BLOG_ADMIN_NO_PERMISSIONS_TO_CREATE'));
			return;
		}
		
		$pathway = $mainframe->getPathway();
		$tpl = new JoomblogTemplate();
 		$blogModel = $this->getModel('Blog', 'JoomblogModel');
		$blogForm = $blogModel->getForm();
		$tpl->set('blogForm', $blogForm, true);
		$html = $tpl->fetch(JB_TEMPLATE_PATH."/admin/newblog.html");
		$html = str_replace("src=\"icons", "src=\"" . rtrim( JURI::base() , '/' ) . "/components/com_joomblog/templates/admin/icons", $html);
		return $html;
	}
}
