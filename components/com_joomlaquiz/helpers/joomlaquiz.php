<?php
/**
* Joomlaquiz Deluxe Component for Joomla 3
* @package Joomlaquiz Deluxe
* @author JoomPlace Team
* @Copyright Copyright (C) JoomPlace, www.joomplace.com
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die;

require_once(JPATH_SITE."/components/com_joomlaquiz/libraries/apps.php");

/**
 * Joomlaquiz Deluxe component helper.
 */
class JoomlaquizHelper
{
		public static function getSettings(){
			$data = new stdClass;
			$db = JFactory::getDBO();
		
			$db->setQuery("SELECT `config_var`, `config_value` FROM #__quiz_configuration");
			$settings = $db->loadObjectList();
		
			if(count($settings)){
				foreach($settings as $setting){
					$array = (array) $setting;
					$data->$array['config_var'] = $array['config_value'];
				}
			}
		
			return $data;
		}
		
		public static function isJoomfish()
		{
			$db = JFactory::getDBO();
			$query = "SELECT COUNT(*) FROM #__extensions WHERE `name`  = 'com_joomfish'";
			$db->SetQuery($query);
			$is_joomfish = (int)$db->LoadResult();

			if (!defined('_JQ_IS_JOOMFISH')) {
				define('_JQ_IS_JOOMFISH', $is_joomfish);
			}

			$jfcookie = (isset($_COOKIE['jfcookie'])) ? $_COOKIE['jfcookie'] : null;

			if (isset($jfcookie["lang"]) && $jfcookie["lang"] != "") {
				$jf_lang = $jfcookie["lang"];
			} else {
				$jf_lang = JFactory::getApplication()->input->get('lang', 'en');
			}

			if (!defined('_JQ_JF_LANG')) {
				define('_JQ_JF_LANG', $jf_lang);
			}
			
			return true;
		}
		
		public static function JQ_GetJoomFish(&$original, $table='', $field='', $id = 0) {

			if (_JQ_IS_JOOMFISH && $original && $table && $field && $id) {
				$database = JFactory::getDBO();

				$query = "SELECT `a`.`value` FROM `#__jf_content` AS `a`, `#__languages` AS `b` 
							WHERE `b`.`id` = `a`.`language_id` AND `b`.`shortcode` = '"._JQ_JF_LANG."' AND `a`.`reference_table` = '".$table."' AND `a`.`reference_field` = '".$field."' AND `a`.`reference_id` = '".$id."' AND `a`.`published` = 1";
				$database->SetQuery( $query );
				$translate = $database->LoadResult();

				if ($translate) {
					$original = $translate;
				}		
			}
		}
		
		public static function jq_substr($str, $start, $length=null) {
			if (function_exists('mb_substr')) {
				if ($length!==null)
					return mb_substr($str, $start, $length);
				else
					return mb_substr($str, $start);
			} else {
				if ($length!==null)
					return substr($str, $start, $length);
				else
					return substr($str, $start);
			}
		}
		
		public static function jq_strpos($haystack, $needle, $offset=null) {
			if (function_exists('mb_strpos')) {
				if ($offset!==null)
					return mb_strpos($haystack, $needle, $offset);
				else
					return mb_strpos($haystack, $needle);
			} else {
				if ($offset!==null)
					return strpos($haystack, $needle, $offset);
				else
					return strpos($haystack, $needle);
			}
		}
		
		public static function getQuestionType($new_qtype_id){
			
			$db = JFactory::getDBO();
			$reg_types = array(); $type = '';
			
			$db->setQuery("SELECT `c_id`, `c_type` FROM #__quiz_t_qtypes");
			$reg_types = $db->loadObjectList();
			
			if(count($reg_types)){
				foreach($reg_types as $reg_type){
					$reg_type = (array) $reg_type;
					if($reg_type['c_id'] == $new_qtype_id){
						$type = $reg_type['c_type'];
						break;
					}
				}
			}
			
			return $type;
		}
		
        public static function getVersion() 
        {
        	$db = JFactory::getDbo();
			$db->setQuery('SELECT `c_par_value` FROM #__quiz_setup WHERE `c_par_name` = "quiz_version"');
			return $db->loadResult();
        }
		
		public static function loadAddonsFunctions($type, $class_prefix, $subpath, $is_suffix = true)
		{
			$class_suffix = '';
			if(file_exists(JPATH_SITE.'/plugins/joomlaquiz/'.$subpath.'/'.$type.'.php')){
				$class_suffix = ($is_suffix) ? ucfirst($type) : '';
				if(!class_exists($class_prefix.$class_suffix)){
					require_once(JPATH_SITE.'/plugins/joomlaquiz/'.$subpath.'/'.$type.'.php');
				}
			} else {
				return false;
			}
			
			return $class_suffix;
		}
		
		public static function poweredByHTML(){
			$content = '';
			$jq_config = JoomlaquizHelper::getSettings();
			if(isset($jq_config->jq_show_dev_info) && $jq_config->jq_show_dev_info) {		
				$word = 'component';
				if (intval(md5(JPATH_SITE.'quiz')) % 2 == 0) $word = 'extension';
				$content = '<br/><div style="text-align:center;">Powered by <span title="JoomPlace"><a target="_blank" title="JoomPlace" href="http://www.joomplace.com/">Joomla '.$word.'</a></span> JoomlaQuiz Deluxe Software.</div>';
			}
			return $content;
		}
		
		public static function jq_UTF8string_check($string) {
			
			return preg_match('%(?:
				[\xC2-\xDF][\x80-\xBF]
				|\xE0[\xA0-\xBF][\x80-\xBF]
				|[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}
				|\xED[\x80-\x9F][\x80-\xBF]
				|\xF0[\x90-\xBF][\x80-\xBF]{2}
				|[\xF1-\xF3][\x80-\xBF]{3}
				|\xF4[\x80-\x8F][\x80-\xBF]{2}
				)+%xs', $string);
		}

		public static function jq_string_jq_substr($str, $offset, $length = NULL) {
			if (JoomlaquizHelper::jq_UTF8string_check($str)) {
				return JoomlaquizHelper::jq_UTF8string_jq_substr($str, $offset, $length);
			} else {
				return JoomlaquizHelper::jq_substr($str, $offset, $length);
			}
		}


		public static function decode_unicode_url($str)
		{
		  $res = '';

		  $i = 0;
		  $max = strlen($str) - 6;
		  while ($i <= $max)
		  {
			$character = $str[$i];
			if ($character == '%' && $str[$i + 1] == 'u')
			{
			  $value = hexdec(JoomlaquizHelper::jq_substr($str, $i + 2, 4));
			  $i += 6;

			  if ($value < 0x0080) // 1 byte: 0xxxxxxx
				$character = chr($value);
			  else if ($value < 0x0800) // 2 bytes: 110xxxxx 10xxxxxx
				$character =
					chr((($value & 0x07c0) >> 6) | 0xc0)
				  . chr(($value & 0x3f) | 0x80);
			  else // 3 bytes: 1110xxxx 10xxxxxx 10xxxxxx
				$character =
					chr((($value & 0xf000) >> 12) | 0xe0)
				  . chr((($value & 0x0fc0) >> 6) | 0x80)
				  . chr(($value & 0x3f) | 0x80);
			}
			else
			  $i++;

			$res .= $character;
		  }

		  return $res . JoomlaquizHelper::jq_substr($str, $i);
		}

		public static function jq_UTF8string_jq_substr($str, $offset, $length = NULL) {
			
			if ( $offset >= 0 && $length >= 0 ) {

				if ( $length === NULL ) {
					$length = '*';
				} else {
					if ( !preg_match('/^[0-9]+$/', $length) ) {
						trigger_error(JText::_('COM_QUIZ_UTF8_EXPECTS_3_PARAMS'), E_USER_WARNING);
						return '';//FALSE;
					}

					$strlen = strlen(utf8_decode($str));
					if ( $offset > $strlen ) {
						return '';
					}

					if ( ( $offset + $length ) > $strlen ) {
					   $length = '*';
					} else {
						$length = '{'.$length.'}';
					}
				}

				if ( !preg_match('/^[0-9]+$/', $offset) ) {
					trigger_error(JText::_('COM_QUIZ_UTF8_EXPECTS_2_PARAMS'), E_USER_WARNING);
					return '';//FALSE;
				}

				$pattern = '/^.{'.$offset.'}(.'.$length.')/us';

				preg_match($pattern, $str, $matches);

				if ( isset($matches[1]) ) {
					return $matches[1];
				}

				return '';//FALSE;

			} else {

				// Handle negatives using different, slower technique
				// From: http://www.php.net/manual/en/function.substr.php#44838
				preg_match_all('/./u', $str, $ar);
				if( $length !== NULL ) {
					return join('',array_slice($ar[0],$offset,$length));
				} else {
					return join('',array_slice($ar[0],$offset));
				}
			}
		}
		
		public static function JQ_load_template($template_name){
			if ( file_exists(JPATH_SITE . '/components/com_joomlaquiz/views/templates/view.html.php') ) {
				if(!class_exists('JoomlaquizViewTemplates')){
					require_once(JPATH_SITE . '/components/com_joomlaquiz/views/templates/view.html.php');
					$view = new JoomlaquizViewTemplates($template_name);
				}
			}
		}
		
		public static function Blnk_replace_answers($qdata) {
		
			$database = JFactory::getDBO();
			$n=1;
			$query = "SELECT `c_id` FROM `#__quiz_t_blank` WHERE `c_question_id` = ".$qdata->c_id;
			$database->setQuery($query);
			$blanks = (array)$database->loadColumn();
			
			$query = "( SELECT c_text FROM #__quiz_t_text WHERE c_blank_id  IN ('".implode("','", $blanks)."') ) UNION (SELECT c_text FROM #__quiz_t_faketext WHERE c_quest_id = ".$qdata->c_id.") ORDER BY rand()";
			$database->setQuery($query);
			$answers = (array)$database->loadColumn();
			
			srand ((float)microtime()*1000000);
			shuffle ($answers);
			$html = '';
			if (count($answers)) {
				$html = '<div style="clear:both;"></div>';
				foreach($answers as $answer){
					if ($answer != '[empty]')
					$html .= '<div class="jq_draggable_answer '.$qdata->c_image.'" xid="dd_blk_id_'.$n++.'">'.$answer.'</div>';
				}
				$html .= '<div style="clear:both;"></div>';
			}
			
			return str_replace('{answers}', $html, $qdata->c_question);
		}
		
		public static function Blnk_replace_quest($q_id, $q_text, $stu_quiz_id=0, $c_qform=0){
			$database = JFactory::getDBO();
			$query = "SELECT c_id FROM #__quiz_r_student_question AS sq WHERE c_stu_quiz_id = '".$stu_quiz_id."' AND c_question_id = '".$q_id."'";
			$database->SetQuery( $query );
			$sid = $database->loadResult( );
			
			$query = "SELECT c_answer FROM #__quiz_r_student_blank WHERE c_sq_id = '".$sid."' ORDER BY c_id";
			$database->SetQuery( $query );
			$answers = $database->loadColumn();			

			$query = "SELECT * FROM #__quiz_t_blank WHERE c_question_id=".$q_id;
			$database->setQuery($query);
			$blnk = $database->loadObjectList();
			for($i=0;$i<count($blnk);$i++){
				$replacement = JoomlaQuiz_template_class::JQ_createBlank(($blnk[$i]->ordering + 1), (isset($answers[$i])?$answers[$i]:''), $blnk[$i]->css_class, $blnk[$i]->c_id, $c_qform);
				if(function_exists('str_ireplace')){
					$q_text = str_ireplace("{Blank".($blnk[$i]->ordering + 1)."}", $replacement, $q_text);
				}else{
					$q_text = str_replace("{Blank".($blnk[$i]->ordering + 1)."}", $replacement, $q_text);
					$q_text = str_replace("{blank".($blnk[$i]->ordering + 1)."}", $replacement, $q_text);
				}
			}
			
			return $q_text;
		}
		
		public static function Blnk_replace_quest_fdb($q_id, $q_text, $stu_quiz_id){
			$database = JFactory::getDBO();
			
			$query = "SELECT * FROM #__quiz_t_blank WHERE c_question_id=".$q_id;
			$database->setQuery($query);
			$blnk = $database->loadObjectList();
			
			$query = "SELECT c_id FROM #__quiz_r_student_question AS sq WHERE c_stu_quiz_id = '".$stu_quiz_id."' AND c_question_id = '".$q_id."'";
			$database->SetQuery( $query );
			$sid = $database->loadResult( );
						
			$query = "SELECT * FROM #__quiz_r_student_blank WHERE c_sq_id = '".$sid."' ORDER BY c_id ";
			$database->SetQuery( $query );
			$tmp = $database->LoadAssocList();
			$q_text = str_replace("{blank", "{Blank", $q_text);			
			for($i=0;$i<count($blnk);$i++){
				
				$query = "SELECT c_id, c_text FROM #__quiz_t_text WHERE c_blank_id = ".$blnk[$i]->c_id." ORDER BY ordering";
				$database->setQuery($query);
				$tmp2 = $database->loadObjectList();
				$c_texts = array();
				foreach($tmp2 as $t=>$cd) {		
					JoomlaquizHelper::JQ_GetJoomFish($tmp2[$t]->c_text, 'quiz_t_text', 'c_text', $tmp2[$t]->c_id);
					$c_texts[] = $tmp2[$t]->c_text;
				}
			
				$replacement = JoomlaQuiz_template_class::JQ_createBlank_fdb($c_texts, (isset($tmp[$i]['c_answer'])? $tmp[$i]['c_answer']: ''), 'red');
				
				if(function_exists('str_ireplace')){
					$q_text = str_ireplace("{Blank".($blnk[$i]->ordering + 1)."}", $replacement, $q_text);
				}else{
					$q_text = str_replace("{Blank".($blnk[$i]->ordering + 1)."}", $replacement, $q_text);
					$q_text = str_replace("{blank".($blnk[$i]->ordering + 1)."}", $replacement, $q_text);
				}
			}
			return $q_text;
		}
		
		public static function Blnk_replace_quest_review($q_id, $q_text){
			
			$database = JFactory::getDBO();

			$query = "SELECT * FROM #__quiz_t_blank WHERE c_question_id=".$q_id;
			$database->setQuery($query);
			$blnk = $database->loadObjectList();
			for($i=0;$i<count($blnk);$i++){
				
				$query = "SELECT c_id, c_text FROM #__quiz_t_text WHERE c_blank_id = ".$blnk[$i]->c_id." ORDER BY ordering ";
				$database->setQuery($query);
				$tmp2 = $database->loadObjectList();
				$c_texts = array();
				foreach($tmp2 as $t=>$cd) {		
					JoomlaquizHelper::JQ_GetJoomFish($tmp2[$t]->c_text, 'quiz_t_text', 'c_text', $tmp2[$t]->c_id);
					$c_texts[] = $tmp2[$t]->c_text;
				}
				
				$replacement = JoomlaQuiz_template_class::JQ_createBlank_review(implode(', ',$c_texts));		
				
				if(function_exists('str_ireplace')){
					$q_text = str_ireplace("{Blank".($blnk[$i]->ordering + 1)."}", $replacement, $q_text);
				}else{
					$q_text = str_replace("{Blank".($blnk[$i]->ordering + 1)."}", $replacement, $q_text);
					$q_text = str_replace("{blank".($blnk[$i]->ordering + 1)."}", $replacement, $q_text);
				}
			}
			return $q_text;
		}
		
		public static function JQ_GetItemId(){
			static $jqItemid = -1;
			
			$ret = '';
			if($jqItemid == -1)
			{	
				global $Itemid;
				if(JFactory::getApplication()->input->get('Itemid') != 0) {
				  $Itemid = $jqItemid = JFactory::getApplication()->input->get('Itemid');
				  $ret = '&Itemid='.$jqItemid;
				}else{
				  $Itemid = $jqItemid = 0;
				}
			} elseif ($jqItemid > 0) {
				$ret = '&Itemid='.$jqItemid;
			}

			return $ret;
		}
		
		public static function JQ_Email($sid, $email_to) {
			
			$database = JFactory::getDBO();
			
			if (!preg_match("/^[_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3}$/i", $email_to)) {
				return false;
			}
						
			if(!class_exists('JoomlaquizModelPrintresult')){
				require_once(JPATH_SITE.'/components/com_joomlaquiz/models/printresult.php');
			}
			$str = JoomlaquizModelPrintresult::JQ_PrintResultForMail($sid);
			
			$email = $email_to;
			$subject = JText::_('COM_QUIZ_RESULTS');
			$message = html_entity_decode($str, ENT_QUOTES);
			
			$config = new JConfig();
			$mailfrom = $config->mailfrom;
			$fromname = $config->fromname;
			
			if ($mailfrom != "" && $fromname != "") {
				$adminName2 = $fromname;
				$adminEmail2 = $mailfrom;
			} else {
				
				$query = "SELECT name, email"
				. "\n FROM #__users"
				. "\n WHERE LOWER( usertype ) = 'superadministrator'"
				. "\n OR LOWER( usertype ) = 'deprecated'"
				;
				$database->setQuery( $query );
				$rows = $database->loadObjectList();
				$row2 			= $rows[0];
				$adminName2 	= $row2->name;
				$adminEmail2 	= $row2->email;
			}
			
			$jmail = new JMail();
			return $jmail->sendMail($adminEmail2, $adminName2, $email, $subject, $message, 1, null, null, null, null, null);
		}
		
		public static function JQ_ShowText_WithFeatures($text, $force_compatibility = false) {
			
			jimport( 'joomla.html.parameter' );
			
			// Black list of mambots:
			$banned_bots = array();
			$row = new stdclass();
			
			$row->id = null;
			$row->text = $text;
			$row->introtext = '';
			$params = new JInput();
			$new_text = $text;

			$dispatcher	= JDispatcher::getInstance();

			JPluginHelper::importPlugin('content');
			$results = $dispatcher->trigger('onContentPrepare', array ('com_joomlaquiz', &$row, &$params, 0));
			$results = $dispatcher->trigger('onPrepareContent', array (& $row, & $params, 0));
			$new_text = $row->text;
		 
			return $new_text;
		}
		
		public static function isQuizAttepmts($quiz_id, $lid=0, $rel_id=0, $order_id=0, &$msg) {
		
			$my = JFactory::getUser();
			$database = JFactory::getDBO();
			
			if (!$quiz_id)
				return false;
			
			$query = "SELECT * FROM #__quiz_t_quiz WHERE c_id = '{$quiz_id}'";
			$database->setQuery($query);
			$quiz = $database->loadObjectList();
			
			if (!isset($quiz[0])) 
				return false;
			$quiz = $quiz[0];
			
			$unique_pass_str = '';
			if (!$my->id && isset($_COOKIE['quizupi'][$quiz_id])) {	
				$unique_pass_id = $_COOKIE['quizupi'][$quiz_id];
				
				$unique_pass_str = " `unique_pass_id` = '".$unique_pass_id."' AND `c_lid` = 0 AND `c_rel_id` = 0 ";	
			} elseif ($lid && $my->id) {
				$unique_pass_str = " `c_student_id` = '".$my->id."' AND `c_lid` = '".$lid."' ";
			} elseif ($my->id) {
				$unique_pass_str = " `c_student_id` = '".$my->id."' ";
			}
			
			if (!$unique_pass_str) {	
				return true;
			}
			
			//stand alone quiz	or  free learn path
			if (!$order_id && !$rel_id) {
				if ($quiz->c_number_times) {			
					//no period, just check number of tries	
					$query = "SELECT COUNT(*) FROM #__quiz_r_student_quiz WHERE `c_quiz_id` = '".$quiz_id."' ". ($quiz->c_allow_continue ? ' AND c_finished = 1 ': ''). " AND {$unique_pass_str} ";
					$database->SetQuery( $query );
					$number_times_passed = (int)$database->loadResult();
					
					if ($number_times_passed < $quiz->c_number_times) {
						return true;
					} elseif ($quiz->c_min_after) {
						$query = "SELECT `c_date_time` FROM #__quiz_r_student_quiz WHERE `c_quiz_id` = '".$quiz_id."' ". ($quiz->c_allow_continue ? ' AND c_finished = 1 ': ''). " AND {$unique_pass_str} ORDER BY `c_id` DESC LIMIT ".$quiz->c_number_times;
						$database->SetQuery( $query );
						$user_tries = $database->loadColumn();
						$last_try_date = strtotime($user_tries[count($user_tries)-1]);
					
						if ($last_try_date + ($quiz->c_min_after*60) > time() ){
							if ($quiz->c_once_per_day && date('d', $last_try_date) != date('d')) {
								return true;
							}
							$msg = $last_try_date + ($quiz->c_min_after*60) - time();
							return false;
						} else {
							return true;
						}
					} else {
						return false;
					}			
				} else {
					return true;
				}		
			} else {
				if ($order_id < 1000000000) {
					$query = "SELECT qp.*"
					. "\n FROM #__virtuemart_orders AS vm_o"
					. "\n INNER JOIN #__virtuemart_order_items AS vm_oi ON vm_oi.virtuemart_order_id = vm_o.virtuemart_order_id"
					. "\n INNER JOIN #__quiz_products AS qp ON qp.pid = vm_oi.virtuemart_product_id"
					. "\n WHERE vm_o.virtuemart_user_id = {$my->id} AND vm_o.virtuemart_order_id = $order_id AND qp.id = $rel_id AND vm_o.order_status IN ('C')"
					;
				} else {
					$query = "SELECT qp.*"
					. "\n FROM `#__quiz_payments` AS p"
					. "\n INNER JOIN `#__quiz_products` AS qp ON qp.pid = p.pid"
					. "\n WHERE p.user_id = {$my->id} AND p.id = '".($order_id-1000000000)."' AND qp.id = '{$rel_id}' AND p.status IN ('Confirmed') "
					;
				}
				$database->SetQuery( $query );
				$rel_check = $database->loadObjectList();
				if(empty($rel_check)) {
					return false;
				}
							
							
				$query = "SELECT attempts FROM `#__quiz_products` WHERE `id` = '{$rel_id}' ";		
				$database->SetQuery( $query );
				$product_params_attempts = (int)$database->loadResult();
				
				$product_quantity = 1;
				if ($order_id < 1000000000) {
					$query = "SELECT vm_oi.product_quantity"
					. "\n FROM #__virtuemart_orders AS vm_o"
					. "\n INNER JOIN #__virtuemart_order_items AS vm_oi ON vm_oi.virtuemart_order_id = vm_o.virtuemart_order_id"
					. "\n INNER JOIN #__quiz_products AS qp ON qp.pid = vm_oi.virtuemart_product_id"
					. "\n WHERE vm_o.virtuemart_user_id = {$my->id} AND vm_o.virtuemart_order_id = $order_id AND qp.id = $rel_id AND vm_o.order_status IN ('C')"
					;
				
					$database->SetQuery( $query );
					$product_quantity = ($database->loadResult()) ? (int)$database->loadResult() : 1;
				}
				
				if (!$product_params_attempts)
					return true;
				
				if($rel_check[0]->type == 'l') {
					$query = "SELECT attempts FROM #__quiz_lpath_stage WHERE uid = '{$my->id}' AND oid = '{$order_id}' AND rel_id = '{$rel_id}' AND lpid = '{$rel_check[0]->rel_id}' AND qid = '{$quiz_id}'";
				} else {
					$query = "SELECT attempts FROM #__quiz_products_stat WHERE uid = '{$my->id}' AND oid = '{$order_id}' AND qp_id = '{$rel_id}' ";
				}
				$database->SetQuery( $query );
				$products_stats_attempts = (int)$database->loadResult();
				
				if ($products_stats_attempts < $product_params_attempts * $product_quantity)
					return true;
			}
			
			return false;
		}
		
		public static function JQ_checkPackage($package_id, $rel_id, $vm=1) {
		
			$database = JFactory::getDBO();
			$my = JFactory::getUser();
			$mainframe = JFactory::getApplication();
			
			$_SESSION['quiz_check_rel_item'] = 0;
			$rel_id = intval($rel_id);
			$package_id = intval($package_id);
			$quiz_params = array();
			$quiz_params[0] = new stdClass;
				
			if (!$rel_id || !$package_id) {
				$quiz_params[0]->error = 1;
				$quiz_params[0]->message = '';
				return $quiz_params[0];
			}		
		
			if ($vm) {
				$query = "SELECT qp.*"
				. "\n FROM #__virtuemart_orders AS vm_o"
				. "\n INNER JOIN #__virtuemart_order_items AS vm_oi ON vm_oi.virtuemart_order_id = vm_o.virtuemart_order_id"
				. "\n INNER JOIN #__quiz_products AS qp ON qp.pid = vm_oi.virtuemart_product_id"
				. "\n WHERE vm_o.virtuemart_user_id = {$my->id} AND vm_o.virtuemart_order_id = $package_id AND qp.id = $rel_id AND vm_o.order_status IN ('C')"
				;
			} else {
				$query = "SELECT qp.*"
				. "\n FROM `#__quiz_payments` AS p"
				. "\n INNER JOIN `#__quiz_products` AS qp ON qp.pid = p.pid"
				. "\n WHERE p.user_id = {$my->id} AND p.id = '".($package_id-1000000000)."' AND qp.id = '{$rel_id}' AND p.status IN ('Confirmed') "
				;
			}
			$database->SetQuery( $query );
			$rel_check = $database->loadObjectList();
			if(empty($rel_check)) {
				$quiz_params[0]->error = 1;
				$quiz_params[0]->message = '<p align="left">'.JText::_('COM_QUIZ_LPATH_NOT_AVAILABLE').'</p>';
				return $quiz_params[0];
			}
		
			$product_data = $rel_check[0];
					
			$products_stat = array();
			$query = "SELECT *"
			. "\n FROM #__quiz_products_stat"
			. "\n WHERE uid = '{$my->id}' AND qp_id = '{$rel_id}' "
			. "\n AND oid = '$package_id' "
			;
			$database->SetQuery( $query );
			$products_stat = $database->loadObjectList('qp_id');
			
			//Check for xdays, period
			if($product_data->xdays) {
				if(!empty($products_stat) && array_key_exists($rel_id, $products_stat)) {
					$confirm_date = strtotime($products_stat[$rel_id]->xdays_start);
				} else {
					if ($vm) {
						$query = "SELECT UNIX_TIMESTAMP(order_history.created_on) "
							. "\n FROM #__virtuemart_order_histories AS order_history"
							. "\n INNER JOIN #__virtuemart_order_items AS order_item ON order_item.virtuemart_order_id = order_history.virtuemart_order_id"
							. "\n WHERE order_history.order_status_code = 'C' AND order_item.virtuemart_order_id = $package_id AND order_item.virtuemart_product_id = '{$product_data->pid}'"
							. "\n ORDER BY order_history.created_on DESC"
							. "\n LIMIT 1"
							;
					} else {
						$query = "SELECT UNIX_TIMESTAMP(p.confirmed_time) "
								. "\n FROM #__quiz_payments AS p"
								. "\n WHERE p.id = '".($package_id-1000000000)."' AND p.status = 'Confirmed' AND  p.pid = '{$product_data->pid}'"
								. "\n ORDER BY p.confirmed_time DESC"
								. "\n LIMIT 1"
								;
					}
					$database->setQuery($query);
					$confirm_date = $database->loadResult();
				}
				
				if($confirm_date) {
					$ts_day_end = $confirm_date + $product_data->xdays*24*60*60;
					if(time() > $ts_day_end) {
						$quiz_params[0]->error = 1;
						$quiz_params[0]->message = '<p align="left">'.JText::_('COM_ACCESS_EXPIRED').'</p>';
						return $quiz_params[0];
					}
				} else {
					$quiz_params[0]->error = 1;
					$quiz_params[0]->message = '<p align="left">'.($product_data->type == 'l' ? JText::_('COM_LPATH_NOT_AVAILABLE') : JText::_('COM_QUIZ_NOT_AVAILABLE')).'</p>';
					return $quiz_params[0];
				}
		
			} else if (($product_data->period_start && $product_data->period_start != '0000-00-00')
					|| ($product_data->period_end && $product_data->period_end != '0000-00-00')) {
				
				if(!empty($products_stat) && array_key_exists($rel_id, $products_stat)) {
					$product_data->period_start = $products_stat[$rel_id]->period_start;
					$product_data->period_end = $products_stat[$rel_id]->period_end;
				}	
				
				$ts_start = null;
				if($product_data->period_start && $product_data->period_start != '0000-00-00') {
					$ts_start = strtotime($product_data->period_start . ' 00:00:00');
				}
		
				$ts_end = null;
				if($product_data->period_end && $product_data->period_end != '0000-00-00') {
					$ts_end = strtotime($product_data->period_end . ' 23:59:59');
				}
				$ts = time();
				if(($ts_start && $ts_start > $ts) || ($ts_end && $ts_end < $ts)) {
					$quiz_params[0]->error = 1;
					$quiz_params[0]->message = '<p align="left">'.JText::_('COM_ACCESS_EXPIRED').'</p>';
					return $quiz_params[0];
				}
			}
			
			//Check attempts
			$product_quantity = 1;
			if($vm){
				$query = "SELECT vm_oi.product_quantity"
				. "\n FROM #__virtuemart_orders AS vm_o"
				. "\n INNER JOIN #__virtuemart_order_items AS vm_oi ON vm_oi.virtuemart_order_id = vm_o.virtuemart_order_id"
				. "\n INNER JOIN #__quiz_products AS qp ON qp.pid = vm_oi.virtuemart_product_id"
				. "\n WHERE vm_o.virtuemart_user_id = {$my->id} AND vm_o.virtuemart_order_id = ".$package_id." AND qp.id = $rel_id AND vm_o.order_status IN ('C')"
				;
				$database->SetQuery( $query );
				$product_quantity = ($database->loadResult()) ? (int)$database->loadResult() : 1;
			}
			
			$attempts = (!empty($products_stat) && array_key_exists($rel_id, $products_stat) && $products_stat[$rel_id]->attempts ? $products_stat[$rel_id]->attempts : 0);
			if($product_data->attempts && ($product_data->attempts * $product_quantity) <= $attempts) {
				$quiz_params[0]->error = 1;
				$quiz_params[0]->message = '<p align="left">'.JText::_('COM_ACCESS_EXPIRED').'</p>';
				return $quiz_params[0];
			}
			
			if($rel_check[0]->type == 'q') {
				$_SESSION['quiz_check_rel_item'] = 1;
				$_SESSION['quiz_check_rel_order_id'] = $package_id;
				$_SESSION['quiz_check_rel_item_id'] = $rel_check[0]->rel_id;
		
				$mainframe->redirect(JURI::root()."index.php?option=com_joomlaquiz&view=quiz&package_id={$package_id}&rel_id={$rel_id}&quiz_id={$rel_check[0]->rel_id}&force=1");
			} else if($rel_check[0]->type == 'l') {
				$lpath_id = $rel_check[0]->rel_id;
			}

			return $lpath_id;
		}
		
		public static function getTotalScore($qch_ids, $quiz_id){
			
			jimport('joomla.filesystem.folder');
			$database = JFactory::getDBO();
			$query = "SELECT SUM(c_point) FROM #__quiz_t_question WHERE c_id IN (".$qch_ids.") AND published = 1 AND c_type <> 11";
			$database->SetQuery( $query );
			$max_score = $database->LoadResult();
			
			$appsLib = JqAppPlugins::getInstance();
			$plugins = $appsLib->loadApplications();
						
			$folders = JFolder::folders(JPATH_SITE.'/plugins/joomlaquiz/', '.', false, false);
			if(count($folders)){
				foreach($folders as $folder){
					
					$data = array();
					$data['quest_type'] = $folder;
					$data['qch_ids'] = $qch_ids;
					$data['quiz_id'] = $quiz_id;
					$data['max_score'] = 0;
					
					$database->setQuery("SELECT `enabled` FROM `#__extensions` WHERE folder = 'joomlaquiz' AND type = 'plugin' AND element = '".$folder."'");
					$enabled = $database->loadResult();
					
					if($enabled){
						$appsLib->triggerEvent( 'onTotalScore' , $data );
						$max_score += $data['max_score'];
					}
				}
			}
			
			return $max_score;			
		}
		
		public static function getJavascriptFunctions(){
			jimport('joomla.filesystem.folder');
			$paths = array();
			$folders = JFolder::folders(JPATH_SITE.'/plugins/joomlaquiz/', '.', false, false);
			if(count($folders)){
				foreach($folders as $folder){
					if(JFolder::exists(JPATH_SITE.'/plugins/joomlaquiz/'.$folder.'/js/functions/')){
						$func_files = JFolder::files(JPATH_SITE.'/plugins/joomlaquiz/'.$folder.'/js/functions/', '.', false, false, array('index.html'));
						if(count($func_files)){
							foreach($func_files as $func_file){
								$paths[] = JURI::root().'plugins/joomlaquiz/'.$folder.'/js/functions/'.$func_file;
							}
						}
					}
				}
			}
			return $paths;
		}
		
		public static function getJavascriptIncludes($dir='includes')
		{
			jimport('joomla.filesystem.folder');
			
			$folders = JFolder::folders(JPATH_SITE.'/plugins/joomlaquiz/', '.', false, false);
			if(count($folders)){
				foreach($folders as $folder){
					if(JFolder::exists(JPATH_SITE.'/plugins/joomlaquiz/'.$folder.'/js/'.$dir.'/')){
						$include_files = JFolder::files(JPATH_SITE.'/plugins/joomlaquiz/'.$folder.'/js/'.$dir.'/', '.', false, false, array('index.html'));
						
						if(count($include_files)){
							foreach($include_files as $include_file){
								echo "\n";
								include_once(JPATH_SITE.'/plugins/joomlaquiz/'.$folder.'/js/'.$dir.'/'.$include_file);
							}
						} else {
							echo "";
						}
					}
				}
			}
		}
}