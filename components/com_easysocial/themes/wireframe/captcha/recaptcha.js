

EasySocial.require()
    .done(function($){

        window.recaptchaCallback = function() {
            grecaptcha.render('recaptcha_<?php echo $uid;?>', {
                'sitekey': '<?php echo $key;?>',
                'theme': '<?php echo $theme;?>'
            });
        }
    });
