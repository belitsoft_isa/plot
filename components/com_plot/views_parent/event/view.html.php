<?php
defined('_JEXEC') or die;

class PlotViewEvent extends PlotViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'event';

    public function display($tpl = null)
    {
        $this->templateUrl = JURI::base().'templates/'.JFactory::getApplication()->getTemplate();
        $this->componentUrl = JURI::base() . 'components/com_plot';
        $this->referrerUrl = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : JRoute::_('index.php?option=com_plot&view=river');
        $app = JFactory::getApplication();
        $eventsModel = JModelLegacy::getInstance('events', 'plotModel');

        $event_id =  JRequest::getInt('id', 0);
        $this->event=$eventsModel->getEvent($event_id);
        if (!$this->event) {
            $app->enqueueMessage(JText::_('COM_PLOT_REQUESTED_PAGE_NOT_EXISTS_ANYMORE'), 'message');
            $app->redirect(JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('error404K2ItemId'), false));
        }
        $this->tags=$eventsModel->getEventTags($event_id);
        $this->ages=$eventsModel->getEventAges($event_id);
        
        $this->setOgMeta();
        return parent::display($tpl);
    }

    private function setOgMeta()
    {
        $db = JFactory::getDbo();
        require_once JPATH_ADMINISTRATOR.'/components/com_plot/plot_social_config.php';
        $metaOgUrl = '<meta property="og:url" content="'.PlotHelper::getAbsRoutedUri('index.php?option=com_plot&view=event&id='.$this->event->id).'" />';
        $metaOgTitle = '<meta property="og:title" content="'.$db->escape($this->event->title).'" />';
        $metaOgDescription = '<meta property="og:description" content="'.$db->escape(strip_tags($this->event->description)).'" />';
        $metaOgImage = '<meta property="og:image" content="'.PlotHelper::getAbsRoutedUri('index.php?option=com_plot&view=event&task=event.pickoutimage&id='.$this->event->id).'&canv_width=200&canv_height=200" />';
        $metaOgSiteName = '<meta property="og:site_name" content="'.JFactory::getConfig()->get('sitename').'" />';
        $metaOgFbAppId = '<meta property="fb:app_id" content="473991812742077" />';
        $metaOgType = '<meta property="og:type" content="website" />';
        $metaOgLocale = '<meta property="og:locale" content="ru_RU" />';
        JFactory::getDocument()->addCustomTag($metaOgTitle.$metaOgSiteName.$metaOgUrl.$metaOgDescription.$metaOgFbAppId.$metaOgType.$metaOgLocale.$metaOgImage);
        return true;
    }       
    
}
