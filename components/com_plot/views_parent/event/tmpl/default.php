<?php
defined('_JEXEC') or die;
?>


<script type="text/javascript" src="<?php echo JUri::root() . 'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.min.js"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.basic.js"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/assets/js/plot.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo JUri::root() . 'components/com_plot/libraries/jquery_input_mask_plugin/jquery.mask.min.js'; ?>"></script>


<script type="text/javascript">
    function deleteEvent(that) {
        var event_id = jQuery(that).attr('data-id');
        jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_plot&task=events.deleteEvent",
            data: {
                'event_id': event_id
            },
            success: function (response) {
                if (response) {
                    window.location.href="<?php echo JRoute::_('index.php?option=com_plot&view=events'); ?>"
                } else {
                    alert('error');
                }
            }
        });
    }

    function eventRemoveConfirm(eventId){
        SqueezeBox.open('<?php echo  JRoute::_("index.php?option=com_plot&task=photos.deleteConfirm")?>&entity=event&id='+eventId, {size:{x:300, y:150}, handler:'ajax'});
    }

    function backToEvent(){
        window.location.href="<?php echo JRoute::_('index.php?option=com_plot&view=events'); ?>"
    }

</script>

<main class="parent-profile parent-one-elem">
    <div class="top parent-profile">
        <?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_header')); ?>
        <?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_top_menu')); ?>
    </div>
    <div class="wrap main-wrap parent-profile">
        <div class="aside-left parent-profile"></div>
        <div class="aside-right parent-profile"></div>
        <section class="parent-one-elem">
            <div class="parent-courses-think-tabs" onclick="backToEvent();">
                <button class="link-back">Назад</button>
            </div>
            <?php if ($this->event) {

                ?>

                <div class="elem-info parent-meeting">
                    <div class="elem-picture">
                        <div class="circle-img">
                            <svg class="clip-svg">
                                <?php  if (file_exists(JPATH_SITE . '/images/com_plot/events/' .  $this->event->user_id .  '/thumb/'.$this->event->img)){
                                    ?>
                                    <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                           xmlns:xlink="http://www.w3.org/1999/xlink"
                                           xlink:href="<?php echo JUri::root() .  'images/com_plot/events/' .  $this->event->user_id .  '/thumb/'.$this->event->img; ?>"></image>
                                <?php }elseif(file_exists(JPATH_SITE . '/images/com_plot/events/' .  $this->event->user_id .  '/'.$this->event->img)){
                                    ?>
                                    <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                           xmlns:xlink="http://www.w3.org/1999/xlink"
                                           xlink:href="<?php echo JUri::root() .  'images/com_plot/events/' .  $this->event->user_id .  '/'.$this->event->img; ?>"></image>
                                    <?php
                                }else{
                                    ?>
                                    <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                           xmlns:xlink="http://www.w3.org/1999/xlink"
                                           xlink:href="<?php echo JUri::root().'images/com_plot/def_meeting.jpg' ?>"></image>
                                <?php
                                }?>



                            </svg>

                        </div>
                        <div id="wrap-btn-event-<?php echo $this->event->id; ?>">
                            <?php
                            if(PlotHelper::isNotOldDate($this->event->end_date)) {
                                if ((int)PlotHelper::chechEventSubscription($this->event->id)) {
                                    if ((int)PlotHelper::isOwnerEvent($this->event->id)) {
                                        ?>
                                        <button class="add hover-shadow" onclick="eventRemoveConfirm(<?php echo $this->event->id; ?>); return false;"
                                                data-id="<?php echo $this->event->id; ?>"><?php echo JText::_('COM_PLOT_REMOVE_EVENT'); ?></button>
                                    <?php
                                    } else {

                                        ?>
                                        <button class="add hover-shadow"
                                                onclick="App.Event.RemoveSubscribe(this); return false;"
                                                data-id="<?php echo $this->event->id; ?>"><?php echo JText::_('COM_PLOT_REMOVE_SUBSCRIBE_EVENT'); ?></button>
                                    <?php
                                    }

                                } else {
                                    ?>
                                    <button class="add hover-shadow" onclick="App.Event.Subscribe(this);return false;"
                                            data-id="<?php echo $this->event->id; ?>"><?php echo JText::_('COM_PLOT_SUBSCRIBE_ON_EVENT'); ?></button>
                                <?php
                                }
                            }?>
                        </div>
                    </div>
                    <div class="meeting-info">
                        <h6><?php echo $this->event->title; ?></h6>
                        <i class="author">автор: <?php echo plotUser::factory($this->event->user_id)->name ?></i>
                        <table>
                            <tr>
                                <td>Количество участников:</td>
                                <td class="quantity">
                                    <?php echo PlotHelper::countEventSubscription($this->event->id); ?>
                                    <svg viewBox="0 0 23 23" preserveAspectRatio="xMinYMin meet">
                                        <use xlink:href="#people"></use>
                                    </svg>
                                </td>
                            </tr>
                            <tr>
                                <td>Дата проведения:</td>
                                <td>
                                    <?php if (PlotHelper::is_date($this->event->start_date)) { ?>
                                        <span> <?php echo PlotHelper::russian_date(JHtml::date($this->event->start_date, 'd.m.Y')); ?> </span>
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Время проведения:</td>
                                <td class="counter">
                                    <p class="start-time">
                                        <b>Время начала</b>
                                        <?php if (PlotHelper::is_date($this->event->start_date)) { ?>
                                            <time><?php echo JHtml::date($this->event->start_date, 'H:m'); ?></time>
                                        <?php } ?>
                                    </p>

                                    <?php
                                    if (PlotHelper::is_date($this->event->start_date) && PlotHelper::is_date($this->event->end_date)) {
                                        echo  PlotHelper::renderEventDuration($this->event);
                                    }
                                    ?>

                                </td>
                            </tr>
                            <tr>
                                <td>Место проведения:</td>
                                <td class="address">
                                    <address><?php  echo $this->event->place; ?></address>
                                </td>
                            </tr>
                            <tr>
                                <td>Список интересов:</td>
                                <td>
                                    <?php
                                    $count_tags=count($this->tags);
                                    foreach($this->tags  AS $i=>$tag){ ?>
                                        <?php
                                        if($i!=$count_tags-1){
                                          echo   $tag->title.', ';
                                        }else{
                                            echo   $tag->title;
                                        }
                                        ?>
                                    <?php  } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Возраст:</td>
                                <td>
                                    <?php
                                    $count_ages=count($this->ages);
                                    foreach($this->ages  AS $i=>$age){ ?>
                                        <?php
                                        if($i!=$count_ages-1){
                                            echo   $age->title.', ';
                                        }else{
                                            echo   $age->title;
                                        }
                                        ?>
                                    <?php  } ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clr"></div>
                    <div class="elem-about">
                        <h5>Подробное описание:</h5>
                        <?php echo $this->event->description; ?>
                    </div>
                </div>
            <?php } ?>
        </section>
    </div>
</main>

