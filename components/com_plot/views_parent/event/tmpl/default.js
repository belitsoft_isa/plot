function changeSelectChildMenu(event, ui)
{
    var selectedOption = jQuery(event.target).find('option:selected');
    var allOptionsExcludeSelectedChildrensIds = []; 
    var currentSelectId = jQuery(event.target).prop('id');
    
    jQuery(event.target).find('option:not(:selected)').each(function(){
        allOptionsExcludeSelectedChildrensIds.push(jQuery(this).val());
    });
    
    jQuery('select[id^=recipient-]').each(function(){
        if (jQuery(this).prop('id') != currentSelectId) {
            var elementIterator = jQuery(this);
            allOptionsExcludeSelectedChildrensIds.each(function(v){
                if (!elementIterator.find('option[value='+v+']').length) {
                    elementIterator.append('<option value="' + v + '">' + jQuery(event.target).find('option[value='+v+']').text() + '</option>');
                }
            });
            jQuery(this).find('option[value='+selectedOption.val()+']').remove();
        }
    });
    
    jQuery('select[id^=recipient-]').selectmenu('refresh');
}

jQuery(document).ready(function() {
    jQuery("#recipient-1").selectmenu({ change: function( event, ui ) {changeSelectChildMenu(event, ui);} });
    
    // add / remove new children course buy for
    jQuery('#course-children-for').submit(function(){
        if ( jQuery('#course-children-for [name=add]').val() == '1' ) {
            var lastSelectIdNum = parseInt(jQuery('select[id^=recipient-]:last').attr('id').replace(/[^0-9]/g, ''));
            var selectIdNew = 'recipient-' + (lastSelectIdNum + 1);
            var selectedChildrens = [];
            var newOptions = '';

            jQuery('select[id^=recipient-] > option:selected').each(function(){
                selectedChildrens.push(jQuery(this).val());
            });

            jQuery('#recipient-1 option').each(function () {
                if (jQuery.inArray(jQuery(this).val(), selectedChildrens) === -1) {
                    newOptions += jQuery(this)[0].outerHTML;
                }
            });

            if (newOptions) {
                jQuery('<select id="' + selectIdNew + '">' + newOptions + '</div>').insertBefore('#course-children-for > fieldset .add-recipient:first');
                //jQuery('#course-children-for > fieldset').append('<select id="' + selectIdNew + '">' + newOptions + '</div>');
                jQuery("#" + selectIdNew).selectmenu({ change: function( event, ui ) {changeSelectChildMenu(event, ui);} });
            }
            // remove selected in new from each other selects
            var selectedValInNewText = jQuery('select[id^=recipient-]:last > option:selected').val();
            
            jQuery('select[id^=recipient-]:not(:last) option:not(:selected)').each(function(){
                if (jQuery(this).val() == selectedValInNewText) {
                    jQuery(this).remove();
                }
            });
            calculateCourseCost();
        } else if (jQuery('select[id^=recipient-]').length > 1) {
            var selectedOptionHtml = jQuery('<div>').append(jQuery('select[id^=recipient-]:last option:selected').clone()).html();
            jQuery('select[id^=recipient-]:not(:last)').each(function(){
                jQuery(this).append(selectedOptionHtml);
            });
            jQuery('select[id^=recipient-]:last').remove();
            calculateCourseCost();
        }
        
        jQuery('#course-children-for [name=add]').val('0');
        jQuery('select[id^=recipient-]').selectmenu('refresh');
        
        return false;
    });
    
    jQuery('#reward').mask('0000000');

    calculateCourseCost();
    jQuery('#reward').keyup(function(){ calculateCourseCost(); });
    jQuery('#reward').change(function(){ calculateCourseCost(); });
    
    jQuery('#course-children-for .users-course-for-list').on('click', '.remove', function(){
        jQuery(this).parent().fadeOut(300, function(){
            jQuery(this).remove();
            calculateCourseCost();
        })
    });
    
});