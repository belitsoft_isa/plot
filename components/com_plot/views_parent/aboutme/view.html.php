<?php
defined('_JEXEC') or die;

class PlotViewAboutme extends PlotViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'aboutme';
    
    public function display($tpl = null)
    {
        $app = JFactory::getApplication();
        $this->templateUrl = JURI::base() . 'templates/' . $app->getTemplate();
        $this->componentUrl = JURI::base() . 'components/com_plot';
        
        $modelAboutme = $this->getModel();
        
        $this->my = plotUser::factory();
        $this->childrenIds = $modelAboutme->getAllChildrenIds();
        
        parent::display($tpl);
    }
    
}
