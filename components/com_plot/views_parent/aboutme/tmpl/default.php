<?php
defined('_JEXEC') or die;
?>
<link href="<?php echo JUri::root().'components/com_plot/views_parent/profile/tmpl/default.css'; ?>" type="text/css" rel="stylesheet">

<script type="text/javascript" src="<?php echo JURI::base(); ?>media/jui/js/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.min.js"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.basic.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'components/com_plot/libraries/preview_master_plugin/jquery.preimage.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root().'components/com_plot/libraries/jquery_input_mask_plugin/jquery.mask.min.js'; ?>"></script>

<script type="text/javascript" src="<?php echo JUri::root().'components/com_plot/views_parent/profile/tmpl/default.js'; ?>"></script>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">

    function modalLoad() {
        var modal = jQuery('.modal-overlay');
        jQuery('#parent-video-id1, a[href="#parent-video-id1"]').click(function() {
           modal.css('display', 'block');
        });
        jQuery('.modal-close').click(function() {
            modal.css('display', 'none');
        });
        modal.click(function(event) {
            e = event || window.event;
            if (e.target == this) {
                jQuery(modal).css('display', 'none');
            }
        });
    }

    jQuery(document).ready(function( jQuery ) {
        modalLoad();
        jQuery( "#activity-feed, #parent-add-activity, #parent-add-video" ).tabs();
        jQuery('#parent-photo-upl-1').preimage();
        jQuery('#parent-certificate-upl-1').preimage();
    });

</script>
<?php # </editor-fold> ?>

<div class="top parent-profile">
    <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_header') ); ?>
    <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_top_menu') ); ?>
</div>

<main class="parent-profile">
    <div class="wrap main-wrap parent-profile">
        <div class="add-left aside-left parent-profile"></div>
        <div class="add-right aside-right parent-profile"></div>
        <section class="activity-feed">
            <div id="activity-feed">
                <ul class="activity-feed-tabs">
                    <li><a href="#about-me">Обо мне</a></li>
                    <li><a href="#about-me"></a></li>
                </ul>
                <?php # <editor-fold defaultstate="collapsed" desc="ABOUT ME"> ?>
                <div id="about-me" class="parent-profile black" style="margin: 10px 20px;">
                    <form method="POST" action="<?php echo JRoute::_('index.php?option=com_plot&task=aboutme.addChild'); ?>">
                        <select name="child-id" style="margin-top: 20px;">
                            <option>Выберите ребенка</option>
                            <?php 
                            foreach ($this->childrenIds AS $childId) { 
                                $child = plotUser::factory($childId);
                            ?>
                            <option value="<?php echo $child->id; ?>"><?php echo $child->name; ?></option>
                            <?php } ?>
                        </select>
                        <div style="margin: 20px 0px;">
                            <input style="border: 1px solid black; margin-bottom: 20px;" type="submit" value="Добавить ребенка" />
                        </div>
                    </form>
                </div>
                <?php # </editor-fold> ?>
            </div>
        </section>
    </div>
</main>

