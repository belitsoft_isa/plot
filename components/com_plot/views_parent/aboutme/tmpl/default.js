function eventPhotoUploadedChange()
{
    var filename = jQuery('#plot-event-form #parent-event-upl-1').val();
    jQuery('#plot-event-form .upload-img-label').html(filename);
}

function validateImage(imgInput)
{
    var error = false;
    if(!imgInput.val()){
        error = 'Добавьте изображение';
    }
    return error;
}
function validateVideoFile(videoInput)
{
    var error = false;
    if(!videoInput.val()){
        error = 'Загрузите видео';
    }
    return error;
}
function validateVideoLink(videoInput)
{
    var error = false;
    if(!videoInput.val()){
        error = 'Введите ссылку на видео';
    }
    return error;
}

function videoUploadedChange()
{
    var filename = jQuery('#form-parent-add-video-upload #parent-video-upl-1').val();
    jQuery('#form-parent-add-video-upload .upload-video-label').html(filename);
}

jQuery(function () {
    jQuery("#parent-event-from, #parent-portfolio").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd.mm.yy",
        nextText: ">>",
        prevText: "<<",
        selectOtherMonths: true,
        firstDay: 1,
        showOtherMonths: true,
        monthNamesShort: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        onClose: function (selectedDate) {
            jQuery("#parent-event-to").datepicker("option", "minDate", selectedDate);
        }
    }).mask('00.00.0000', {placeholder: "__.__.____"});
    jQuery('#parent-event-from-time').mask('00:00', {placeholder: "__:__"});
    jQuery('#parent-event-to-time').mask('00:00', {placeholder: "__:__"});
    jQuery("#parent-certificate-date").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd.mm.yy",
        nextText: ">>",
        prevText: "<<",
        selectOtherMonths: true,
        firstDay: 1,
        showOtherMonths: true,
        monthNamesShort: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"]
    }).mask('00.00.0000', {placeholder: "__.__.____"});
    
    jQuery("#parent-event-to").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd.mm.yy",
        nextText: ">>",
        prevText: "<<",
        selectOtherMonths: true,
        firstDay: 1,
        showOtherMonths: true,
        monthNamesShort: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        onClose: function (selectedDate) {
            jQuery("#parent-event-from").datepicker("option", "maxDate", selectedDate);
        }
    }).mask('00.00.0000', {placeholder: "__.__.____"});
}); 

jQuery(document).ready(function(){
    
    jQuery('#plot-photo-form').submit(function(e){
        var errors = [];
       /* if (error = validateImage( jQuery('#parent-photo-upl-1') )) {
            errors.push(error);
        }*/
        if (errors.length !== 0) {
            jPlot.showEnqueuedMessage(errors);
            e.preventDefault();
        }
    });
    jQuery('#form-parent-add-video-upload').submit(function(e){
        var errors = [];
        if (error = validateVideoFile( jQuery('#parent-video-upl-1') )) {
            errors.push(error);
        }
        if (errors.length !== 0) {
            jPlot.showEnqueuedMessage(errors);
            e.preventDefault();
        }
    });  
    jQuery('#form-parent-add-video-link').submit(function(e){
        var errors = [];
        if (error = validateVideoLink( jQuery('#parent-video-link') )) {
            errors.push(error);
        }
        if (errors.length !== 0) {
            jPlot.showEnqueuedMessage(errors);
            e.preventDefault();
        }
    });
    jQuery('#plot-event-form').submit(function(e){
        var errors = [];
       /* if (error = validateImage( jQuery('#plot-event-form #parent-event-upl-1') )) {
            errors.push(error);
        }*/
        if (errors.length !== 0) {
            jPlot.showEnqueuedMessage(errors);
            e.preventDefault();
        }
    });    
    jQuery('#plot-certificate-form').submit(function(e){
        var errors = [];
        /*if (error = validateImage( jQuery('#plot-certificate-form #parent-certificate-upl-1') )) {
            errors.push(error);
        }*/
        if (errors.length !== 0) {
            jPlot.showEnqueuedMessage(errors);
            e.preventDefault();
        }
    });    
    
    
    
});