<?php
defined('_JEXEC') or die;

class PlotViewProfile extends PlotViewParentLegacy
{

    public function display($tpl = null)
    {
        $this->my = plotUser::factory();
        return parent::display($tpl);
    }

}
