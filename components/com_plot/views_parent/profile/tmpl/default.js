function eventPhotoUploadedChange()
{
    var filename = jQuery('#plot-event-form #parent-event-upl-1').val();
   jQuery('#plot-event-form .upload-img-label').html(filename);


}
jQuery('body').addClass('parent-profile');
function validateImage(imgInput)
{
    var error = false;
    if(!imgInput.val()){
        error = 'Добавьте изображение';
    }
    return error;
}
function validateVideoFile(videoInput)
{
    var error = false;
    if(!videoInput.val()){
        error = 'Загрузите видео';
    }
    return error;
}
function validateVideoLink(videoInput)
{
    var error = false;
    if(!videoInput.val()){
        error = 'Введите ссылку на видео';
    }
    return error;
}

function videoUploadedChange()
{
    var filename = jQuery('#form-parent-add-video-upload #parent-video-upl-1').val();
    jQuery('#form-parent-add-video-upload .upload-video-label').html(filename);
}

jQuery(function () {
    jQuery("#parent-event-from, #parent-portfolio").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd.mm.yy",
        nextText: ">>",
        prevText: "<<",
        selectOtherMonths: true,
        firstDay: 1,
        showOtherMonths: true,
        monthNamesShort: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        onClose: function (selectedDate) {
            jQuery("#parent-event-to").datepicker("option", "minDate", selectedDate);
        }
    }).mask('00.00.0000', {placeholder: "__.__.____"});
    jQuery('#parent-event-from-time, #parent-event-to-time').mask('00:00', {placeholder: "__:__"});
    jQuery("#parent-certificate-date").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd.mm.yy",
        nextText: ">>",
        prevText: "<<",
        selectOtherMonths: true,
        firstDay: 1,
        showOtherMonths: true,
        monthNamesShort: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"]
    }).mask('00.00.0000', {placeholder: "__.__.____"});
    
    jQuery("#parent-event-to").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd.mm.yy",
        nextText: ">>",
        prevText: "<<",
        selectOtherMonths: true,
        firstDay: 1,
        showOtherMonths: true,
        monthNamesShort: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        onClose: function (selectedDate) {
            jQuery("#parent-event-from").datepicker("option", "maxDate", selectedDate);
        }
    }).mask('00.00.0000', {placeholder: "__.__.____"});
});

jQuery(document).ready(function(){
    
    jQuery('#plot-photo-form').submit(function(e){
        var errors = [];
        if (errors.length !== 0) {
            jPlot.showEnqueuedMessage(errors);
            e.preventDefault();
        }
    });
    jQuery('#form-parent-add-video-link').submit(function(e){
        var errors = [];
        if (error = validateVideoLink( jQuery('#parent-video-link') )) {
            errors.push(error);
        }
        if (errors.length !== 0) {
            jPlot.showEnqueuedMessage(errors);
            e.preventDefault();
        }
    });
    jQuery('#plot-event-form').submit(function(e){
        var errors = [];
        if (errors.length !== 0) {
            jPlot.showEnqueuedMessage(errors);
            e.preventDefault();
        }
    });    
    jQuery('#plot-certificate-form').submit(function(e){
        var errors = [];
        if (errors.length !== 0) {
            jPlot.showEnqueuedMessage(errors);
            e.preventDefault();
        }
    });

    jQuery('#add-course-form').submit(function(){
        var errors = [];
        var isVideoLink = jQuery('#add-course-form #parent-add-course-video ul.video > li[aria-controls=parent-add-course-video-link]').hasClass('ui-state-active');
        
        if ( !jQuery.trim(jQuery('#add-course-form [name=title]').val()) ) {
            errors.push('Введите название курса');
        }
        if ( jQuery('#add-course-form #add-course-category').val() == 0 ) {
            errors.push('Выберите категорию курса');
        }
        if ( isVideoLink ) {
            // add video link
            if ( !jQuery.trim(jQuery('#add-course-form [name=video-link]').val()) ) {
                errors.push('Укажите youtube-ссылку на видео');
            }
            if ( !jQuery.trim(jQuery('#add-course-form [name=video-title]').val()) ) {
                errors.push('Укажите заголовок для видео');
            }
            if ( !jQuery.trim(jQuery('#add-course-form [name=video-description]').val()) ) {
                errors.push('Укажите описание видео');
            }
        } else {
            // add uploaded video
            if ( !jQuery('#add-course-form #course-videofile-image-area > img').length ) {
                errors.push('Загрузите видео файл');
            }
            if ( !jQuery.trim( jQuery('#add-course-form input[name=uploaded-video-title]').val() ) ) {
                errors.push('Укажите заголовок для видео');
            }
            if ( !jQuery.trim( jQuery('#add-course-form textarea[name=uploaded-video-description]').val() ) ) {
                errors.push('Укажите описание для видео');
            }
        }
        
        // questions / answers validation
        if (
            !jQuery.trim(jQuery('#add-course-form textarea[name=question1]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question1-answer1]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question1-answer2]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question1-answer3]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question1-answer4]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question1-answer5]').val()) || 
            !jQuery.trim(jQuery('#add-course-form textarea[name=question2]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question2-answer1]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question2-answer2]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question2-answer3]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question2-answer4]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question2-answer5]').val()) || 
            !jQuery.trim(jQuery('#add-course-form textarea[name=question3]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question3-answer1]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question3-answer2]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question3-answer3]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question3-answer4]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question3-answer5]').val())
        ) {
            errors.push('Укажите 3 вопроса к видео и по 5 вариантов ответов на каждый из вопросов');
        }
        
        if ( !jQuery('#add-course-form #new_course_ages').val() ) {
            errors.push('Выберите хотя бы один возрастной период');
        } 

        if ( !jQuery('#add-course-form #new_course_tags').val() ) {
            errors.push('Выберите хотя бы один интерес');
        } 
        
        if (errors.length !== 0) {
            alert(errors.join('\n'));
            return false;
        } 
        
        if (isVideoLink){
            jQuery('#add-course-form').append('<input type="hidden" name="video-img" value="'+jQuery('#add-course-form #video-youtube-img').attr('src')+'" />');
        } else {
            jQuery('#add-course-form').append('<input type="hidden" name="video-img" value="'+jQuery('#add-course-form #course-videofile-image-area > img').attr('src')+'" />');
        }
        jQuery('#add-course-form button.add').attr('disabled', 'disabled');
        return true;
    });
    
    function strpos(haystack, needle, offset) {
        var i = (haystack + '')
            .indexOf(needle, (offset || 0));
        return i === -1 ? false : i;
    }    
    
    function strripos(haystack, needle, offset) {
        haystack = (haystack + '')
            .toLowerCase();
        needle = (needle + '')
            .toLowerCase();

        var i = -1;
        if (offset) {
            i = (haystack + '')
                .slice(offset)
                .lastIndexOf(needle); // strrpos' offset indicates starting point of range till end,
            // while lastIndexOf's optional 2nd argument indicates ending point of range from the beginning
            if (i !== -1) {
                i += offset;
            }
        } else {
            i = (haystack + '')
                .lastIndexOf(needle);
        }
        return i >= 0 ? i : false;
    }

    function substr(str, start, len) {
        var i = 0,
            allBMP = true,
            es = 0,
            el = 0,
            se = 0,
            ret = '';
        str += '';
        var end = str.length;

        // BEGIN REDUNDANT
        this.php_js = this.php_js || {};
        this.php_js.ini = this.php_js.ini || {};
        // END REDUNDANT
        switch ((this.php_js.ini['unicode.semantics'] && this.php_js.ini['unicode.semantics'].local_value.toLowerCase())) {
            case 'on':
                // Full-blown Unicode including non-Basic-Multilingual-Plane characters
                // strlen()
                for (i = 0; i < str.length; i++) {
                    if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
                        allBMP = false;
                        break;
                    }
                }

                if (!allBMP) {
                    if (start < 0) {
                        for (i = end - 1, es = (start += end); i >= es; i--) {
                            if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
                                start--;
                                es--;
                            }
                        }
                    } else {
                        var surrogatePairs = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;
                        while ((surrogatePairs.exec(str)) != null) {
                            var li = surrogatePairs.lastIndex;
                            if (li - 2 < start) {
                                start++;
                            } else {
                                break;
                            }
                        }
                    }

                    if (start >= end || start < 0) {
                        return false;
                    }
                    if (len < 0) {
                        for (i = end - 1, el = (end += len); i >= el; i--) {
                            if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
                                end--;
                                el--;
                            }
                        }
                        if (start > end) {
                            return false;
                        }
                        return str.slice(start, end);
                    } else {
                        se = start + len;
                        for (i = start; i < se; i++) {
                            ret += str.charAt(i);
                            if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
                                se++;
                            }
                        }
                        return ret;
                    }
                    break;
                }
            case 'off':
            default:
                if (start < 0) {
                    start += end;
                }
                end = typeof len === 'undefined' ? end : (len < 0 ? len + end : len + start);
                return start >= str.length || start < 0 || start > end ? !1 : str.slice(start, end);
        }
        return undefined; // Please Netbeans
    }    
    
});

jQuery(document).ready(function (jQuery) {
    modalLoad();

    jQuery(".fancybox").fancybox({
        maxWidth	: 800,
        maxHeight	: 600,
        fitToView	: false,
        width		: '70%',
        height		: '70%',
        autoSize	: false,
        closeClick	: false,
        openEffect	: 'none',
        closeEffect	: 'none',
        nextEffect: 'feed',
        helpers:  {
            title : {
                type : 'inside',
                position: 'top'
            }
        },
        beforeLoad: function() {
            this.title = jQuery(this.element).attr('data-title'),
                this.date = jQuery(this.element).attr('data-date'),
                this.description = jQuery(this.element).attr('data-description'),
                this.share = jQuery(this.element).attr('data-share');
        }
    });

    jQuery('#certificate-close-button').hide();
    jQuery('#photo-close-button').hide();
    jQuery('#event-close-button').hide();
    jQuery('#portfolio-close-button').hide();
    jQuery('#videofile-close-button').hide();
    jQuery('#youtube-close-button').hide();
    jQuery("#parent-add-activity, #parent-add-video, #activity-feed, #parent-add-course-video").tabs();
    jQuery('#parent-photo-upl-1').preimage();
    jQuery('#parent-certificate-upl-1').preimage();

    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });

});