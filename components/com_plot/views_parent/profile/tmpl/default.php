<?php
defined('_JEXEC') or die;
$app = JFactory::getApplication();
JHtml::_('formbehavior.chosen', 'select');
?>
<link href="<?php echo JUri::root() . 'components/com_plot/views_parent/profile/tmpl/default.css'; ?>" type="text/css" rel="stylesheet">

<script type="text/javascript" src="<?php echo JURI::base(); ?>media/jui/js/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.min.js"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.basic.js"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/libraries/preview_master_plugin/jquery.preimage.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/libraries/jquery_input_mask_plugin/jquery.mask.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/views_parent/profile/tmpl/default.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JURI::base() . 'components/com_plot'; ?>/assets/js/jquery.fancybox.js"></script>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">
    
jQuery.post(
    'https://graph.facebook.com', 
    {
        id: '<?php echo PlotHelper::getAbsRoutedUri('index.php?option=com_plot&view=profile&id='.$this->user->id); ?>/',
        scrape: 'true'
    }
);      
    
jQuery(document).ready(function(){
    allEventsLoad();
    paginationCarouselCheck('parent-read');
    paginationCarouselCheck('parent-studied');
    paginationCarouselCheck('parent-certificates');
});

function modalLoad() {
    var modal = jQuery('.modal-overlay');
    <?php
     if ($this->video_links) {
     foreach($this->video_links AS $video){
     ?>
    jQuery('#parent-video-id<?php echo $video->id; ?>, a[href="#parent-video-id<?php echo $video->id; ?>"]').click(function () {
        modal.css('display', 'block');
    });
    jQuery('#parent-video-id<?php echo $video->id; ?>, a[href="#parent-video-id<?php echo $video->id; ?>"]').click(function () {
        modal.css('display', 'block');
    });
    <?php
     }
     }
    ?>
    jQuery('#parent-video-id1, a[href="#parent-video-id1"]').click(function () {
        modal.css('display', 'block');
    });
    jQuery('.modal-close').click(function () {
        modal.css('display', 'none');
    });
    modal.click(function (event) {
        e = event || window.event;
        if (e.target == this) {
            jQuery(modal).css('display', 'none');
        }
    });
}

function allEventsLoad() {
    jQuery.post('index.php?option=com_plot&task=profile.ajaxAllEventsLoad&id=<?php echo $this->user->id;?>', function(html){
        jQuery('#parent-all-event').html(html);
        allEventsCarouselInitialize();
    });
}

function allEventsCarouselInitialize() {
    var element = jQuery('#parent-all-event #events-jcarousel').jcarousel();
    var width = element.innerWidth()*0.235,//23.5% ширина li в случае, если выводится по 4 элемента
        margin=element.innerWidth()*0.02;//2% значение margin в случае
    element.jcarousel('items').css('width', width + 'px');
    element.jcarousel('items').css('margin-right', margin + 'px');
        
    jQuery('#parent-all-event #activity-jcarousel-pagination').jcarouselPagination({
        item: function(page) {
            return '<a href="#' + page + '"></a>';
        },
        'perPage': 4
    });
    jQuery('#parent-all-event #activity-jcarousel-pagination > a:first').addClass('active');
    
    jQuery('#parent-all-event #activity-jcarousel-pagination')
        .on('jcarouselpagination:active', 'a', function() {
            jQuery(this).addClass('active');
        })
        .on('jcarouselpagination:inactive', 'a', function() {
            jQuery(this).removeClass('active');
        });
    jQuery('#parent-all-event #events-jcarousel').jcarousel('reload');
}

function paginationCarouselCheck(elId) {
    var arr =new Array();
    jQuery('#'+elId+' .parent-profile').find('li').each(function(){
        arr.push(this);
    });
if(arr.length<5){
  jQuery('#'+elId).find('.jcarousel-pagination').hide();
}

}
</script>
<?php # </editor-fold> ?>

<main class="parent-profile">
<?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_header')); ?>
    <?php if ($this->my->id == $this->user->id) {
        echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_top_menu'));
    } ?>
<?php if ($this->my->id != $this->user->id) { ?>
    <?php if ($this->user->isParent()) { ?>
            <div class="looking-at-user-header parent-profile">
                <div class="wrap">
                    <div class="name-row">
                        <span>Привет!</span><i>Я - <?php echo $this->user->name; ?><?php echo ($this->user->getSocialFieldData('ADDRESS')->city) ?  ',' : ''; ?></i> <span><?php echo ($this->user->getSocialFieldData('ADDRESS')->city) ?  ($this->user->getSocialFieldData('ADDRESS')->city) : ''; ?></span>
                    </div>
                    <div class="user-top">
                        <a class="circle-img modal"
                           href="<?php echo JRoute::_('index.php?option=com_plot&task=profileedit.ajaxGetForeignChildProfilePopupHtml&id=' . (int)$this->user->id); ?>"
                           rel="{size: {x: 744, y: 500}, handler:'iframe', iframeOptions: {scrolling: 'no'}}">
                            <img src="<?php echo $this->user->getSquareAvatarUrl(); ?>"
                                 alt="<?php echo $this->user->name; ?>">
                        </a>
                    </div>
                </div>
            </div>
    <?php } else { ?>
            <div class="looking-at-user-header child-profile">
                <div class="wrap">
                    <div class="name-row">
                        <span>Привет!</span><i>Я - <?php echo $this->user->name; ?><?php echo ($this->user->getSocialFieldData('ADDRESS')->city) ?  ',' : ''; ?> </i><span><?php echo ($this->user->getSocialFieldData('ADDRESS')->city) ?  ($this->user->getSocialFieldData('ADDRESS')->city) : ''; ?></span>
                    </div>
                    <div class="user-top">
                        <div>
                            <p class="level">
                                <i><span><?php echo $this->user->level->title; ?>
                                        й </span> <?php echo JText::_('COM_PLOT_MY_LEVEL'); ?></i>
                                <svg viewBox="0 0 19.8 30" preserveAspectRatio="xMinYMin meet" class="cup">
                                    <use xlink:href="#cup"></use>
                                </svg>
                            </p>
                        </div>
                        <a class="user-photo modal"
                           rel="{size: {x: 744, y: 525}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
                           href="<?php echo JRoute::_('index.php?option=com_plot&task=profileedit.ajaxGetForeignChildProfilePopupHtml&id=' . $this->user->id); ?>">
                            <img src="<?php echo $this->user->getSquareAvatarUrl(); ?>" alt="">
                        </a>
                    </div>
                </div>
            </div>
    <?php } ?>
<?php } ?>

<div class="wrap main-wrap parent-profile">
<div class="add-left aside-left parent-profile"></div>
<div class="add-right aside-right parent-profile"></div>
<section class="activity-feed">
<div id="activity-feed">
<ul class="activity-feed-tabs">
    <li><a href="#parent-all-event">Все события</a></li>
    <li><a href="#parent-all-progress">Все достижения</a></li>
    <li><a href="#parent-all-meetings">Все встречи</a></li>
    <li><a href="#parent-all-photos">Все фото</a></li>
    <li><a href="#parent-all-videos">Все видео</a></li>
    <li style="display: none;"><a href="#parent-all-portfolio">Мое портфолио</a></li>
    <?php if ($this->my->id == $this->user->id) { ?>
        <li><a href="#parent-add-activity" class="add">Добавить</a></li>
    <?php } else { ?>
        <li></li>
    <?php } ?>
</ul>
<?php # <editor-fold defaultstate="collapsed" desc="EVENTS"> ?>
<div id="parent-all-event" class="jcarousel-wrapper parent-profile"></div>
<?php # </editor-fold> ?>
<?php # <editor-fold defaultstate="collapsed" desc="CERTIFICATES"> ?>
<div id="parent-all-progress" class="jcarousel-wrapper parent-profile">
    <?php if ($this->progress['books'] || $this->progress['courses'] || $this->progress['certificates']) { ?>
    <div class="jcarousel">
        <ul class="parent-profile parent-certificates">
                <?php foreach ($this->progress AS $key => $value) { ?>
                    <?php if ($key == 'certificates') {
                        foreach ($value AS $val) { ?>
                            <li>
                                <a class="fancybox" rel="certificate-set" data-title="<?php echo $val->title; ?>"
                                   data-description="<?php echo $val->caption; ?>" data-date="<?php echo JFactory::getDate($val->assigned_date)->format('d.m.Y'); ?>"
                                   data-share=" data-image='<?php echo $val->imageUrl; ?>' data-url='<?php echo $val->imageUrl; ?>' data-title='<?php echo  addslashes($val->title);?>' "
                                   href="<?php echo $val->imageUrl; ?>">
                                    <h6>
                                        <span class="action-title"></span>
                                        <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags( $val->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                    </h6>
                                    <div class="circle-img">
                                        <svg class="clip-svg">
                                            <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                                   xlink:href="<?php echo $val->thumb; ?>"/>
                                        </svg>
                                        <i class="activity-icons">
                                            <svg viewBox="0 0 24.9 25.8" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);"><use xlink:href="#certificate"></use></svg>
                                        </i>
                                    </div>
                                <?php if ($val->caption) { ?>
                                    <p><?php echo PlotHelper::cropStr(strip_tags($val->caption), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                                <?php } ?>
                                </a>
                            </li>
                        <?php }
                    } elseif ($key == 'courses') {
                        foreach ($value AS $val) {
                            ?>
                            <li>
                                <a href="<?php echo  JRoute::_('index.php?option=com_plot&view=course&id='.$val->course_id); ?>">
                                    <h6>
                                        <span class="action-title"></span>
                                        <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags( $val->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?>
                                    </p>
                                    </h6>
                                    <div class="circle-img">
                                        <svg class="clip-svg-course">
                                            <image style="clip-path: url(#clipping-circle-course);" width="100%"
                                                   height="100%"
                                                   xmlns:xlink="http://www.w3.org/1999/xlink"
                                                   xlink:href="<?php echo JUri::root() . $val->image; ?>"
                                                   alt=""/>
                                        </svg>
                                        <i class="activity-icons">
                                            <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);">
                                                <use xlink:href="#academic-hat"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <?php if($val->course_description){ ?>
                                        <p><?php echo PlotHelper::cropStr(strip_tags($val->course_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                                    <?php } ?>
                                </a>
                            </li>
                        <?php } ?>
                    <?php } elseif ($key == 'books') {
                        foreach ($value AS $val) {
                            ?>
                            <li>
                                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $val->c_id); ?>">
                                    <h6>
                                        <span class="action-title"></span>
                                        <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags( $val->c_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                        <p class="stream-author"><?php echo $val->c_author; ?></p>
                                    </h6>
                                    <div class="circle-img">
                                        <svg class="clip-svg">
                                            <?php if (file_exists(JPATH_BASE . '/media/com_html5flippingbook/thumbs/thimb_' . $val->c_thumb)) { ?>
                                                <image style="clip-path: url(#clipping-circle);" width="100%" height="100%" 
                                                       xlink:href="<?php echo JUri::root() . 'media/com_html5flippingbook/thumbs/thimb_' . $val->c_thumb; ?>"/>
                                            <?php } else { ?>
                                                <image style="clip-path: url(#clipping-circle);" width="100%" height="100%" 
                                                       xlink:href="<?php echo JUri::root() . 'images/com_plot/def_book.jpg'; ?>"/>
                                            <?php } ?>
                                        </svg>
                                        <i class="activity-icons">
                                            <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                                <use xlink:href="#book"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <?php if($val->c_pub_descr){ ?>
                                        <p><?php echo PlotHelper::cropStr(strip_tags($val->c_pub_descr), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                                    <?php } ?>
                                </a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>

        </ul>
    </div>
    <?php if (count($this->progress['books']) + count($this->progress['courses']) + count($this->progress['certificates']) > 4) { ?>
    <p class="jcarousel-pagination"></p>
    <?php } ?>
    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id='.$this->user->id);?>#certificates" class="show-more">Показать больше сертификатов</a>
    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id='.$this->user->id);?>#courses" class="show-more">Показать больше курсов</a>
    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id='.$this->user->id);?>#publications" class="show-more">Показать больше книг</a>
    <?php } else {
        echo PlotHelper::getDefaultText('activity', $this->user->id);
    } ?>
</div>
<?php # </editor-fold> ?>
<?php # <editor-fold defaultstate="collapsed" desc="MEETINGS"> ?>
<div id="parent-all-meetings" class="jcarousel-wrapper parent-profile">
    <?php if ($this->events) { ?>
    <div class="jcarousel">
        <ul class="parent-profile">
            <?php foreach ($this->events AS $item) { ?>
                <li id="plot-event-id-<?php echo $item->id; ?>" class="parent-meetings">
                    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=event&id=' . $item->id); ?>">
                        <h6>
                            <span class="action-title"></span>
                            <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($item->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow'));?>
                            </p>
                        </h6>
                        <hr/>
                        <data><?php echo JFactory::getDate($item->create_date)->format('d.m.Y H:i'); ?></data>
                        <div class="circle-img">
                            <?php if ($item->img) {
                                ?>
                                <svg class="clip-svg">
                                    <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                           xlink:href="<?php echo JUri::root() . 'images/com_plot/events/' . $item->user_id . '/thumb/' . $item->img; ?>"/>
                                </svg>
                            <?php } ?>
                            <i class="activity-icons">
                                <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);">
                                    <use xlink:href="#lamp-meeting"></use>
                                </svg>
                            </i>
                        </div>
                        <?php if ($item->description) { ?>
                            <p><?php echo PlotHelper::cropStr(strip_tags($item->description),  plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                        <?php } ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
    <?php if (count($this->events) > 4) { ?>
    <p class="jcarousel-pagination"></p>
    <?php } ?>
    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id='.$this->user->id).'#events';?>" class="show-more">Показать больше встреч</a>
    <?php } else {
        echo PlotHelper::getDefaultText('activity', $this->user->id);
    } ?>
</div>
<?php # </editor-fold> ?>
<?php # <editor-fold defaultstate="collapsed" desc="PHOTOS"> ?>
<div id="parent-all-photos" class="jcarousel-wrapper parent-profile">
    <?php if ($this->photos) { ?>
        <div class="jcarousel">
            <ul class="parent-profile">
                <?php foreach ($this->photos AS $photo) { ?>
                    <li class="parent-books">
                        <a class="fancybox" rel="photos-set" data-title="<?php echo $photo->title; ?>" data-description="<?php echo $photo->caption; ?>" data-date="<?php echo JFactory::getDate($photo->assigned_date)->format('d.m.Y H:i'); ?>"
                            <?php
                            if($photo->img_type=='plot-essay'){
                            ?>
                           data-share=" data-image='<?php echo JURI::root() . 'media/com_plot/essay/' . $photo->value; ?>' data-url='<?php echo JURI::root() . 'media/com_plot/essay/' . $photo->value; ?>' data-title='<?php echo  addslashes($photo->title);?>' "
                           href="<?php echo JURI::root() . 'media/com_plot/essay/' . $photo->value; ?>">

                            <?php
                            }else{
                                ?>
                                data-share=" data-image='<?php echo JURI::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . PlotHelper::getImageOriginalById($photo->id)->value; ?>' data-url='<?php echo JURI::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . PlotHelper::getImageOriginalById($photo->id)->value; ?>' data-title='<?php echo  addslashes($photo->title);?>' "
                                href="<?php echo JURI::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . PlotHelper::getImageOriginalById($photo->id)->value; ?>">
                            <?php
                            }
                            ?>
                            <h6>
                            <span class="action-title"></span>
                            <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags( $photo->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?>
                            </p>
                            </h6>
                            <hr/>
                            <data><?php echo JFactory::getDate($photo->assigned_date)->format('d.m.Y  H:i'); ?></data>
                            <div class="circle-img">
                                <svg class="clip-svg">
                                    <?php
                                    if($photo->img_type=='plot-essay'){
                                        ?>
                                        <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                               xlink:href="<?php echo JFile::exists(JPATH_BASE.'/media/com_plot/essay/'.$photo->value) ? JURI::root() . 'media/com_plot/essay/'.$photo->value : JUri::root().'templates/plot/img/blank150x150.jpg'; ?>"  alt=""/>
                                    <?php }else{
                                        ?>
                                        <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                               xlink:href="<?php echo JURI::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . $photo->value; ?>"
                                               alt=""/> <?php
                                    } ?>


                                </svg>
                                <i class="activity-icons">
                                    <svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                        <use xlink:href="#zoom"></use>
                                    </svg>
                                </i>
                            </div>

                                <?php if ($photo->caption) { ?>
                                    <p><?php echo PlotHelper::cropStr(strip_tags($photo->caption),  plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                                <?php } ?>

                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <?php if (count($this->photos) > 4) { ?>
        <p class="jcarousel-pagination"></p>
        <?php } ?>
        <a href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id='.$this->user->id);?>#parent-photos" class="show-more">показать больше фото</a>
    <?php } else { ?>
            <?php echo PlotHelper::getDefaultText('activity', $this->user->id); ?>
    <?php } ?>
</div>
<?php # </editor-fold> ?>
<?php # <editor-fold defaultstate="collapsed" desc="VIDEOS"> ?>
<div id="parent-all-videos" class="jcarousel-wrapper parent-profile">
    <?php if ($this->video_links) { ?>
        <div class="jcarousel">
            <ul class="parent-profile">
                <?php foreach ($this->video_links AS $video) { ?>
                  <?php  if($video->type=='link' || $video->type=='course'){ ?>
                    <li class="parent-video">
                        <?php if (strripos($video->path, 'youtube.com/') !== FALSE) {
                            $youtubeNumber = substr($video->path, strripos($video->path, '?v=') + 3);
                            if (($pos = strpos($youtubeNumber, '&')) !== FALSE) $youtubeNumber = substr($youtubeNumber, 0, $pos);
                        } ?>
                        <a class="fancybox" data-fancybox-type="ajax" rel="videos-set"
                           data-title="<?php echo $video->title; ?>" data-description="<?php echo $video->description; ?>" data-date="<?php echo JFactory::getDate($video->date)->format('d.m.Y H:i'); ?>"
                           data-share=" data-image='https://youtu.be/<?php echo $youtubeNumber; ?>' data-url='https://youtu.be/<?php echo $youtubeNumber; ?>' data-title='<?php echo  addslashes($video->title);?>' "
                           href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxOpenVideo&youtubeNumber=' . $youtubeNumber);?>"   >
                        <h6> 
                            <span class="action-title"></span>
                            <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags( $video->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                        </h6>
                        <hr/>
                        <data><?php echo JFactory::getDate($video->date)->format('d.m.Y  H:i'); ?></data>
                        <div class="circle-img">
                            <svg class="clip-svg">
                                <?php
                      if($video->type=='course'){
                          if (file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'coursevideo' . DIRECTORY_SEPARATOR . $video->uid . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $youtubeNumber . '.jpg')) {
                              $path = JUri::root() . 'media/com_plot/coursevideo/' . $video->uid . '/thumb/' . $youtubeNumber . '.jpg';
                          } else {
                              $path = JUri::root() . 'images/com_plot/def_video.jpg';
                          }
                      }else{
                          if (file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR . $video->uid . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $youtubeNumber . '.jpg')) {
                              $path = JUri::root() . 'images/com_plot/video/' . $video->uid . '/thumb/' . $youtubeNumber . '.jpg';
                              $path = str_replace("\\images", "/images", $path);
                          } else {
                              $path = JUri::root() . 'images/com_plot/def_video.jpg';
                          }
                      }
                                ?>
                                <image style="clip-path: url(#clipping-circle);" width="100%" height="100%" xlink:href="<?php echo $path; ?>"/>
                            </svg>
                            <i class="activity-icons">
                                <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet" style="fill:url(#svg-gradient);"><use xlink:href="#play"></use></svg>
                            </i>
                        </div>
                        <?php if ($video->description) { ?>
                        <p><?php echo PlotHelper::cropStr(strip_tags($video->description),  plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                        <?php } ?>
                        </a>
                    </li>
                    <?php } else { ?>
                        <li class="parent-video">
                            <a class="fancybox" data-fancybox-type="ajax" rel="videos-set"
                               data-title="<?php echo $video->title; ?>" data-description="<?php echo $video->description; ?>" 
                               data-date="<?php echo JFactory::getDate($video->date)->format('d.m.Y H:i'); ?>"
                               data-share=" data-image='https://youtu.be/<?php echo $youtubeNumber; ?>' data-url='https://youtu.be/<?php echo $youtubeNumber; ?>' data-title='<?php echo  addslashes($video->title);?>' "
                               href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.openVideoFile&videoId=' . $video->id);?>" >
                                <h6> 
                                    <span class="action-title"></span>
                                    <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags( $video->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                </h6>
                                <hr/>
                                <data><?php echo JFactory::getDate($video->date)->format('d.m.Y H:i'); ?></data>
                                <div class="circle-img">
                                    <svg class="clip-svg">
                                        <image style="clip-path: url(#clipping-circle);" width="100%" height="100%" xlink:href="<?php echo $video->imageThumb; ?>"/>
                                    </svg>
                                    <i class="activity-icons">
                                        <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet" style="fill:url(#svg-gradient);"><use xlink:href="#play"></use></svg>
                                    </i>
                                </div>
                                <?php if ($video->description) { ?>
                                <p><?php echo PlotHelper::cropStr(strip_tags($video->description),  plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                                <?php } ?>
                            </a>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
        <?php if (count($this->video_links) > 4) { ?>
        <p class="jcarousel-pagination"></p>
        <?php } ?>
        <a href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id='.$this->user->id);?>#videos" class="show-more">показать больше видео</a>
    <?php } else { ?>
        <?php echo PlotHelper::getDefaultText('activity', $this->user->id); ?>
    <?php } ?>
</div>
<?php # </editor-fold> ?>
<?php # <editor-fold defaultstate="collapsed" desc="PORTFOLIO"> ?>
<div id="parent-all-portfolio" class="jcarousel-wrapper parent-profile">
    <?php if ($this->portfolios) { ?>
        <div class="jcarousel">
            <ul class="parent-profile">


                <?php foreach ($this->portfolios AS $item) { ?>

                    <li class="parent-portfolio">
                        <a href="#">
                            <h6>
                                <span class="action-title"></span>
                                <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($item->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>

                            </h6>
                            <hr/>
                            <data><?php echo JHtml::date($item->date,'d.m.Y'); ?></data>

                            <div class="circle-img">

                                    <svg class="clip-svg">
                                        <?php
                                        if (file_exists(JPATH_SITE . '/images/com_plot/portfolio/' . $this->user->id . '/thumb/' . $item->img)){
                                            ?>
                                            <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                                   xlink:href="<?php echo JURI::root() . 'images/com_plot/portfolio/' . $this->user->id . '/thumb/' . $item->img; ?>"/>
                                        <?php
                                        }else{
                                            ?>
                                            <img src="<?php echo JUri::root().'images/com_plot/def_portfolio.jpg'; ?>"/>
                                        <?php
                                        }
                                        ?>



                                    </svg>

                                <i class="activity-icons">
                                    <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);">
                                        <use xlink:href="#portfolio"></use>
                                    </svg>
                                </i>
                            </div>

                                <?php if ($item->description) { ?>
                                    <p><?php echo PlotHelper::cropStr(strip_tags($item->description),  plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                                <?php } ?>


                        </a>
                    </li>

                <?php } ?>

            </ul>
        </div>
        <p class="jcarousel-pagination"></p>
        <a href="#" class="show-more">Показать больше портфолио</a>
    <?php
    } else {
        echo PlotHelper::getDefaultText('activity', $this->user->id);
    } ?>
</div>
<?php # </editor-fold> ?>
<?php # <editor-fold defaultstate="collapsed" desc="ADD ITEMS"> ?>
<?php if ($this->my->id == $this->user->id) {
    require_once JPATH_COMPONENT.'/views_parent/profile/tmpl/default_add.php';
 } ?>
<?php # </editor-fold> ?>
</div>

</section>

<section class="parent-library mainpage">
    <h4>Прочитанные книги</h4>
    <div id="parent-read" class="jcarousel-wrapper parent-profile">
        <div class="jcarousel">
            <ul class="parent-profile parent-library">
                <?php if ($this->readListBS) {
                    foreach ($this->readListBS AS $book) { ?>
                        <li>
                            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $book->c_id); ?>">
                                <figure>
                                    <?php if (file_exists(JPATH_BASE . '/media/com_html5flippingbook/thumbs/thimb_' . $book->c_thumb)) { ?>
                                        <img src="<?php echo JUri::root() . 'media/com_html5flippingbook/thumbs/thimb_' . $book->c_thumb; ?>"/>
                                    <?php } else { ?>
                                        <img src="<?php echo JUri::root() . 'images/com_plot/def_book.jpg'; ?>" />
                                    <?php } ?>
                                </figure>
                            </a>
                        </li>
                    <?php }
                } else { ?>
                    <li class="no-items">
                        Книг не найдено
                    </li>
                <?php } ?>
            </ul>
        </div>
        <?php if ($this->readListBS) { ?>
            <p class="jcarousel-pagination"></p>
            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id='.$this->user->id).'#publications';?>" class="show-more">показать больше</a>
        <?php } ?>
    </div>
</section>

<section class="parent-courses mainpage">
    <h4>Изученные курсы</h4>
    <div id="parent-studied" class="jcarousel-wrapper parent-profile">
        <div class="jcarousel">
            <ul class="parent-profile">
                <?php if ($this->finishedCourses) {
                    foreach ($this->finishedCourses AS $course) { ?>
                        <li>
                            <a href="<?php echo  JRoute::_('index.php?option=com_plot&view=course&id='.$course->course_id); ?>">
                                <h6><?php echo $course->title; ?></h6>

                                <div class="circle-img">
                                    <svg class="clip-svg-course">
                                        <image style="clip-path: url(#clipping-circle-course);" width="100%"
                                               height="100%"
                                               xlink:href="<?php echo JUri::root() . $course->image; ?>"
                                               alt=""/>
                                    </svg>
                                    <i class="activity-icons">
                                        <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);">
                                            <use xlink:href="#academic-hat"></use>
                                        </svg>
                                    </i>
                                </div>
                                <p><?php echo PlotHelper::cropStr(strip_tags($course->course_description), plotGlobalConfig::getVar('coursesDescriptionMaxSymbolsToShow')); ?></p>
                            </a>
                        </li>
                    <?php }
                } else { ?>
                    <li class="no-items">Нет курсов</li>
                <?php } ?>
            </ul>
        </div>
        <?php if ($this->finishedCourses) { ?>
            <p class="jcarousel-pagination"></p>
            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id='.$this->user->id).'#courses';?>" class="show-more">показать больше</a>
        <?php } ?>
    </div>
</section>

<section class="parent-certificates mainpage">
    <h4>Мои достижения</h4>
    <div id="parent-certificates" class="jcarousel-wrapper parent-profile">
        <div class="jcarousel">
            <ul class="parent-profile parent-certificates">
                <?php if ($this->certificates) {
                    foreach ($this->certificates AS $certificate) { ?>
                        <li>
                            <a class="fancybox" rel="certificate-shelf-set" data-title="<?php echo $certificate->title; ?>"
                               data-description="<?php echo $certificate->caption; ?>" data-date="<?php echo JFactory::getDate($certificate->assigned_date)->format('d.m.Y H:i'); ?>"
                               data-share=" data-image='<?php echo $certificate->imageUrl; ?>' data-url='<?php echo  $certificate->imageUrl;?>' data-title='<?php echo  addslashes($certificate->title);?>' "
                               href="<?php echo $certificate->imageUrl; ?>">


                                <figure>
                                    <img
                                        src="<?php echo  $certificate->imageUrl; ?>"
                                        alt=""/>
                                    <i class="activity-icons">
                                        <svg viewBox="0 0 24.9 25.8" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);">
                                            <use xlink:href="#certificate"></use>
                                        </svg>
                                    </i>
                                </figure>
                            </a>
                        </li>
                    <?php }
                } else { ?>
                    <li class="no-items">
                        нет сертификатов
                    </li>
                <?php } ?>
            </ul>
        </div>
        <?php if ($this->certificates) { ?>
            <p class="jcarousel-pagination"></p>
            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id='.$this->user->id);?>#certificates" class="show-more">показать больше</a>
        <?php } ?>
    </div>
</section>

</div>
    
</main>