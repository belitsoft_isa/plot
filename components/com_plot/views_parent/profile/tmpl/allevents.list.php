<?php
defined('_JEXEC') or die('Unauthorized Access');

?>
<?php if ($this->user->stream) { ?>
    <div class="jcarousel" id="events-jcarousel">
    <ul class="parent-profile">
    <?php
    foreach ($this->user->stream AS $streamObj) {
        switch ($streamObj->command) {
            case 'buy.book':
            case 'read.book':
                ?>
                <li class="parent-books">
                    <a href="<?php echo $streamObj->original; ?>">
                        <h6>
                            <span
                                class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                            <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>

                            <p class="stream-author"><?php echo $streamObj->author; ?></p>
                        </h6>
                        <hr/>
                        <span><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></span>

                        <div class="circle-img">
                            <?php if ($streamObj->image_url) {
                                ?>
                                <svg class="clip-svg">
                                    <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                           xlink:href="<?php echo $streamObj->image_url; ?>"/>
                                </svg>
                            <?php } ?>
                            <i class="activity-icons">
                                <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"
                                     style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                    <use xlink:href="#book"></use>
                                </svg>
                            </i>
                        </div>
                        <?php if ($streamObj->bottom_description) { ?>
                            <p><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                        <?php } ?>
                    </a>
                </li>
                <?php
                break;
            case 'course.finish':
                ?>
                <li class="parent-course">
                    <a href="<?php echo $streamObj->original; ?>">
                        <h6>
                            <span
                                class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                            <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                        </h6>
                        <hr/>
                        <span><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></span>

                        <div class="circle-img">
                            <?php if ($streamObj->image_url) { ?>
                                <svg class="clip-svg-course">
                                    <image style="clip-path: url(#clipping-circle-course);" width="100%" height="100%"
                                           xlink:href="<?php echo $streamObj->image_url; ?>"/>
                                </svg>
                            <?php } ?>
                            <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"
                                 style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                <use xlink:href="#academic-hat"></use>
                            </svg>
                        </div>
                        <?php if ($streamObj->bottom_description) { ?>
                            <p><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                        <?php } ?>
                    </a>
                </li>
                <?php
                break;
            case 'add.meeting':
            case 'subscribe.meeting':
            case 'unsubscribe.meeting':
                ?>
                <li class="parent-meetings">
                    <a href="<?php echo $streamObj->original; ?>">
                        <h6>
                            <span
                                class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                            <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                        </h6>
                        <hr/>
                        <span><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></span>

                        <div class="circle-img">
                            <?php if ($streamObj->image_url) { ?>
                                <svg class="clip-svg">
                                    <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                           xlink:href="<?php echo $streamObj->image_url; ?>"/>
                                </svg>
                            <?php } ?>
                            <i class="activity-icons">
                                <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet"
                                     style="fill:url(#svg-gradient);">
                                    <use xlink:href="#lamp-meeting"></use>
                                </svg>
                            </i>
                        </div>
                        <?php if ($streamObj->bottom_description) { ?>
                            <p><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                        <?php } ?>
                    </a>
                </li>
                <?php
                break;
            case 'add.certificate':
            case 'add.old.certificate':
                ?>
                <li class="parent-sertificates">
                    <a class="fancybox" rel="stream-set" data-title="<?php echo $streamObj->title; ?>"
                       data-description="<?php echo $streamObj->bottom_description; ?>"
                       data-date="<?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?>"
                       data-share=" data-image='<?php echo $streamObj->original; ?>' data-url='<?php echo $streamObj->original; ?>' data-title='<?php echo  addslashes($streamObj->title);?>' "
                       href="<?php echo $streamObj->original; ?>">
                        <h6>
                            <span
                                class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                            <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                        </h6>
                        <hr/>
                        <span><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></span>

                        <div class="circle-img">
                            <?php if ($streamObj->image_url) { ?>
                                <svg class="clip-svg">
                                    <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                           xlink:href="<?php echo $streamObj->image_url; ?>"/>
                                </svg>
                            <?php } ?>
                            <i class="activity-icons">
                                <svg viewBox="0 0 24.9 25.8" preserveAspectRatio="xMidYMid meet"
                                     style="fill:url(#svg-gradient);">
                                    <use xlink:href="#certificate"></use>
                                </svg>
                            </i>
                        </div>
                        <?php if ($streamObj->bottom_description) { ?>
                            <p><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                        <?php } ?>
                    </a>
                </li>
                <?php
                break;
            case 'avatar.change':
                ?>
                <li class="parent-photos">
                    <a class="fancybox" rel="stream-set" data-title="<?php echo $streamObj->title; ?>"
                       data-description="<?php echo $streamObj->bottom_description; ?>"
                       data-date="<?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?>"
                       data-share=" data-image='<?php echo $streamObj->original; ?>' data-url='<?php echo $streamObj->original; ?>' data-title='<?php echo  addslashes($streamObj->title);?>' "
                       href="<?php echo $streamObj->original; ?>">
                        <h6>
                            <span
                                class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                            <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                        </h6>
                        <hr/>
                        <span><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></span>

                        <div class="circle-img">
                            <?php if ($streamObj->image_url) {
                                ?>
                                <svg class="clip-svg">
                                    <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                           xlink:href="<?php echo pathinfo($streamObj->image_url, PATHINFO_DIRNAME) . '/cropped.jpg'; ?>"/>
                                </svg>
                            <?php } ?>
                            <i class="activity-icons">
                                <svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid meet"
                                     style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                    <use xlink:href="#zoom"></use>
                                </svg>
                            </i>
                        </div>
                        <?php if ($streamObj->bottom_description) { ?>
                            <p><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                        <?php } ?>
                    </a>
                </li>
                <?php
                break;
            case 'add.photo':
                ?>
                <li class="parent-photos">
                    <a class="fancybox" rel="stream-set" data-title="<?php echo $streamObj->title; ?>"
                       data-description="<?php echo $streamObj->bottom_description; ?>"
                       data-date="<?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?>"
                       data-share=" data-image='<?php echo $streamObj->original; ?>' data-url='<?php echo $streamObj->original; ?>' data-title='<?php echo  addslashes($streamObj->title);?>' "
                       href="<?php echo $streamObj->original; ?>">
                        <h6>
                            <span
                                class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                            <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                        </h6>
                        <hr/>
                        <span><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></span>

                        <div class="circle-img">
                            <?php if ($streamObj->image_url) {
                                ?>
                                <svg class="clip-svg">
                                    <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                           xlink:href="<?php echo $streamObj->image_url; ?>"/>
                                </svg>
                            <?php } ?>
                            <i class="activity-icons">
                                <svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid meet"
                                     style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                    <use xlink:href="#zoom"></use>
                                </svg>
                            </i>
                        </div>
                        <?php if ($streamObj->bottom_description) { ?>
                            <p><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                        <?php } ?>
                    </a>
                </li>
                <?php
                break;
            case 'add.essay':
                ?>
                <?php  $essay = PlotHelper::getEssayById($streamObj->entity_id);
                if ($essay) {
                    ?>
                    <?php if ($essay->status == 1 || (plotUser::factory()->id == (int)$essay->user_id || plotUser::factory()->id == plotUser::factory()->isBookAuthor($essay->book_id))) { ?>
                        <li class="parent-photos">
                            <a class="fancybox" rel="stream-set" data-title=""
                               data-description="<?php echo $streamObj->bottom_description; ?>"
                               data-date="<?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?>"
                        <?php if($essay->status == 1){ ?>
                               data-share=" data-image='<?php echo $streamObj->original; ?>' data-url='<?php echo $streamObj->original; ?>' data-title='<?php echo  addslashes($streamObj->title);?>' "
                              <?php } ?>
                               href="<?php echo $streamObj->original; ?>">
                                <h6>
                                    <span
                                        class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>
                                </h6>
                                <hr/>
                                <span><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></span>

                                <div class="circle-img">
                                    <?php if ($streamObj->image_url) {
                                        ?>
                                        <svg class="clip-svg-course">
                                            <image style="clip-path: url(#clipping-circle-course);" width="100%"
                                                   height="100%"
                                                   xlink:href="<?php echo JURI::base() . 'templates/plot'; ?>/img/default-essay.jpg"/>"
                                            alt="эссе"/>
                                        </svg>
                                    <?php } ?>
                                    <i class="activity-icons">
                                        <svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid meet"
                                             style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                            <use xlink:href="#zoom"></use>
                                        </svg>
                                    </i>
                                </div>
                                <?php if ($streamObj->bottom_description) { ?>
                                    <p><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                                <?php } ?>
                            </a>
                        </li>
                    <?php
                    }
                } ?>
                <?php
                break;
            case 'add.video':
                if ($streamObj->video_type == 'link') {
                    ?>
                    <li class="parent-video">
                        <a class="fancybox" data-fancybox-type="ajax" rel="stream-set"
                           data-title="<?php echo $streamObj->bottom_title; ?>"
                           data-description="<?php echo $streamObj->bottom_description; ?>"
                           data-date="<?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?>"
                           data-share=" data-image='<?php echo $streamObj->image_url; ?>' data-url='https://youtu.be/<?php echo $streamObj->youtubeNumber; ?>' data-title='<?php echo  addslashes($streamObj->title);?>' "

                           href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxOpenVideo&youtubeNumber=' . $streamObj->youtubeNumber); ?>">
                            <h6>
                                <span
                                    class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                                <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                            </h6>
                            <hr/>
                            <span><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></span>

                            <div class="circle-img">
                                <?php if ($streamObj->image_url) { ?>
                                    <svg class="clip-svg">
                                        <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                               xlink:href="<?php echo $streamObj->image_url; ?>"/>
                                    </svg>
                                <?php } ?>
                                <i class="activity-icons">
                                    <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet"
                                         style="fill:url(#svg-gradient);">
                                        <use xlink:href="#play"></use>
                                    </svg>
                                </i>
                            </div>
                        </a>
                    </li>
                <?php } else { ?>
                    <li class="parent-video">
                        <a class="fancybox" data-fancybox-type="ajax" rel="stream-set"
                           data-title="<?php echo $streamObj->bottom_title; ?>"
                           data-description="<?php echo $streamObj->bottom_description; ?>"
                           data-date="<?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?>"
                           data-share=" data-image='<?php echo $streamObj->image_url; ?>' data-url='https://youtu.be/<?php echo $streamObj->youtubeNumber; ?>' data-title='<?php echo  addslashes($streamObj->title);?>' "

                           href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.openVideoFile&videoId=' . $streamObj->youtubeNumber); ?>">
                            <h6>
                                <span><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                                <p><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                            </h6>
                            <hr/>
                            <span><?php echo $streamObj->created; ?></span>

                            <div class="circle-img">
                                <?php if ($streamObj->image_url) {
                                    ?>
                                    <svg class="clip-svg">
                                        <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                               xlink:href="<?php echo $streamObj->image_url; ?>"/>
                                    </svg>
                                <?php } ?>
                                <i class="activity-icons">
                                    <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet"
                                         style="fill:url(#svg-gradient);">
                                        <use xlink:href="#play"></use>
                                    </svg>
                                </i>
                            </div>
                            <?php if ($streamObj->bottom_description) { ?>
                                <p><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                            <?php } ?>
                        </a>
                    </li>
                <?php
                }
                break;
            case 'add.coursevideo':
                $coursevideo = PlotHelper::getCourseVideoById($streamObj->entity_id);
                if ($coursevideo->status == 1 || plotUser::factory()->id == (int)$coursevideo->user_id || plotUser::factory()->id == $coursevideo->reviewer) {
                    ?>
                    <li class="parent-video">

                    <?php if (plotUser::factory()->id == $coursevideo->reviewer && $coursevideo->status == 0) { ?>
                        <a href="<?php echo JRoute::_("index.php?option=com_plot&task=course.ajaxCourse") . '&id=' . $coursevideo->id; ?>">

                       <?php }else{
                           ?>
                        <a class="fancybox" data-fancybox-type="ajax" rel="stream-set"
                           data-title="<?php echo $streamObj->bottom_title; ?>"
                           data-description="<?php echo $streamObj->bottom_description; ?>"
                           data-date="<?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?>"
                           data-share=" data-image='<?php echo $streamObj->image_url; ?>' data-url='https://youtu.be/<?php echo $streamObj->youtubeNumber; ?>' data-title='<?php echo  addslashes($streamObj->title);?>' "

                           href="<?php echo $streamObj->original; ?>">
                        <?php
                        }?>
                            <h6>
                                <span
                                    class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                                <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                            </h6>
                            <hr/>
                            <span><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></span>

                            <div class="circle-img">
                                <?php if ($streamObj->image_url) { ?>
                                    <svg class="clip-svg">
                                        <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                               xlink:href="<?php echo $streamObj->image_url; ?>"/>
                                    </svg>
                                <?php } ?>
                                <i class="activity-icons">
                                    <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet"
                                         style="fill:url(#svg-gradient);">
                                        <use xlink:href="#play"></use>
                                    </svg>
                                </i>
                            </div>
                        </a>
                    </li>
                <?php }
                break;
            case 'add.portfolio':
                ?>
                <li class="parent-portfolio">
                    <a href="<?php echo $streamObj->original; ?>">
                        <h6>
                            <span
                                class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                            <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                        </h6>
                        <hr/>
                        <span><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></span>

                        <div class="circle-img">
                            <?php if ($streamObj->image_url) { ?>
                                <svg class="clip-svg">
                                    <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                           xlink:href="<?php echo $streamObj->image_url; ?>"/>
                                </svg>
                            <?php } ?>
                            <i class="activity-icons">
                                <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"
                                     style="fill:url(#svg-gradient);">
                                    <use xlink:href="#portfolio"></use>
                                </svg>
                            </i>
                        </div>
                        <?php if ($streamObj->bottom_description) { ?>
                            <p><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                        <?php } ?>
                    </a>
                </li>
                <?php
                break;
            case 'add.tag':
            case 'assess.tag':
            case 'describe.tag':
                ?>
                <li class="event-whithout-link">
                    <h6>
                        <span
                            class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                        <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                    </h6>
                    <hr/>
                    <span><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></span>

                    <div class="circle-img">
                        <?php if ($streamObj->image_url) { ?>
                            <svg class="clip-svg">
                                <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                       xlink:href="<?php echo $streamObj->image_url; ?>"/>
                            </svg>
                        <?php } ?>
                        <?php if ($streamObj->bottom_description) { ?>
                            <p><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                        <?php } ?>
                    </div>
                </li>
                <?php
                break;
            case 'finished.program':
                ?>
                <li class="parent-photos">
                    <a class="fancybox" rel="stream-set" data-title="<?php echo $streamObj->title; ?>"
                       data-description="<?php echo $streamObj->bottom_description; ?>"
                       data-date="<?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?>"
                       href="<?php echo $streamObj->original; ?>">
                        <h6>
                            <span
                                class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                            <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                        </h6>
                        <hr/>
                        <span><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></span>

                        <div class="circle-img">
                            <?php if ($streamObj->image_url) {
                                ?>
                                <svg class="clip-svg">
                                    <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                           xlink:href="<?php echo $streamObj->image_url; ?>"/>
                                </svg>
                            <?php } ?>
                            <i class="activity-icons">
                                <svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid meet"
                                     style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                    <use xlink:href="#zoom"></use>
                                </svg>
                            </i>
                        </div>
                        <?php if ($streamObj->bottom_description) { ?>
                            <p><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')); ?></p>
                        <?php } ?>
                    </a>
                </li>
                <?php
                break;
        }
    } ?>
    </ul>
    </div>
    <?php if (count($this->user->stream) > 4) { ?>
        <p id="actyvity-jcarousel-pagination" class="jcarousel-pagination"></p>
    <?php } ?>
<?php
} else {
    echo PlotHelper::getDefaultText('activity', $this->my->id);
}