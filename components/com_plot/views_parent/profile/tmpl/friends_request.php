<?php
defined('_JEXEC') or die;
jimport('joomla.html.html.bootstrap');
?>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />


<link type="text/css" href="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate() . '/css/style.css'; ?>" rel="stylesheet">

<script src="<?php echo JUri::root(); ?>media/jui/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>templates/plot/js/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/js/jquery.jscrollpane.min.js"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/mootools-core.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/core.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/mootools-more.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/modal.js" type="text/javascript"></script>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('body').addClass('popup-style');
        jQuery( "#tag-id").selectmenu({
            open: function( event, ui ) { jQuery('#tag-id-menu').jScrollPane(); }
        });
    });
    function closeErrorPopup() {
        jQuery("#avatar-box").html('');
    }
    function errorAddPlotMessage(status,error_message) {
        window.parent.document.getElementById('sbox-window').style.position = 'fixed';
        var overlay = '<div class="avatar-overlay" id="sbox-overlay2" tabindex="-1"></div>',
            str = '<div id="sbox-window2" role="dialog" aria-hidden="false" class="shadow avatar-sbox">' +
                '<div id="sbox-content2" class="sbox-content-adopt" style="opacity: 1;"><div id="enqueued-message"><div>' +
                '<p>' + error_message + '</p>';
                    if (status) {
                        str+= '</div></div></div><a id="sbox-btn-close2" href="#" onclick="window.parent.document.getElementById(\'sbox-btn-close\').click();" role="button" aria-controls="sbox-window"></a></div>';
                    } else {
                        str+= '</div></div></div><a id="sbox-btn-close2" href="#" onclick="closeErrorPopup();" role="button" aria-controls="sbox-window"></a></div>';
                    }
        jQuery("#avatar-box").html(str+overlay);
    }

    function sendRequest() {
jQuery('.pre-loader').show();
        var tag_id=jQuery('#tag-id').val(),
            id="<?php echo  $this->id; ?>",
            wrapp=window.parent.document.getElementById('friend-button-wrapp');
        if (tag_id=='') {
            errorAddPlotMessage(0, 'Выберите интерес');
            jQuery('.pre-loader').hide();
            return false;
        } else {
            jQuery.post('index.php?option=com_plot&task=profile.request', {
                tag_id: tag_id,
                id: id
            }, function (response) {
                jQuery('.pre-loader').hide();
                jQuery(wrapp).html('<svg viewBox="0 0 34.5 30.8" preserveAspectRatio="xMinYMin meet" class="wait-for-friend"><use xlink:href="#wait-for-friend"></use></svg>запрос дружбы отправлен');
                errorAddPlotMessage(response.status, response.message);
            });
        }
    }

</script>
<style>
    #sbox-window2 {
        left:15%;
        top:15%;
    }
</style>
<?php # <editor-fold defaultstate="collapsed" desc="SVG"> ?>
<svg xmlns="http://www.w3.org/2000/svg" style="position: absolute; left: -99999px;">
    <symbol id="friend" viewBox="0 0 31.7 30.8" >
        <path class="st0" d="M10.7,18.5c5.6,0,10.7,2.8,10.7,6c0,3.5-5,6.3-10.7,6.3C4.7,30.8,0,28,0,24.5C0,21.4,4.7,18.5,10.7,18.5 L10.7,18.5z M20.7,13.5h3.1v-3.1c0-0.9,0.6-1.3,1.3-1.3h0.6c0.9,0,1.3,0.3,1.3,1.3v3.1h3.4c0.6,0,1.3,0.6,1.3,1.3v0.6 c0,0.9-0.6,1.3-1.3,1.3H27v3.5c0,0.6-0.3,1.3-1.3,1.3h-0.6c-0.6,0-1.3-0.6-1.3-1.3v-3.5h-3.1c-0.9,0-1.3-0.3-1.3-1.3v-0.6 C19.4,14.1,19.8,13.5,20.7,13.5L20.7,13.5z M10.3,0c4.7,0,8.5,3.8,8.5,8.5s-3.8,8.2-8.5,8.2c-4.4,0-8.2-3.5-8.2-8.2S6,0,10.3,0 L10.3,0z"/>
    </symbol>
</svg>
<?php // </editor-fold> ?>
<div class="send-friend-request child-profile">
    <label>Выберите ваш общий интерес</label>
    <div id="avatar-box"></div>
       <form method="post" name="adminForm" id="adminForm"
              action="<?php echo 'index.php?option=com_plot&task=profile.request'; ?>" method="post" autocomplete="off" onsubmit="return false;">
            <input type="hidden" value="<?php echo  $this->id; ?>" name="id">
            <input type="hidden" value="<?php echo  $this->Itemid; ?>" name="Itemid">
            <input type="hidden" name="boxchecked" value="0"/>
            <?php echo JHtml::_('form.token'); ?>

            <?php
            if (!empty($this->tags)) {
                ?>
                <select name="tag_id" id="tag-id">
                    <option value=""><?php echo JText::_('COM_PLOT_SELECT_TAG'); ?></option>
                    <?php
                    foreach ($this->tags AS $tag) {
                        ?>
                        <option value="<?php echo  $tag->id; ?>"><?php echo $tag->title; ?></option>
                    <?php
                    }
                    ?>
                </select><br>
            <?php
            }else{ ?>
               <p><?php echo JText::_('COM_PLOT_DO_NOT_HAVE_JOINT_TAGS'); ?></p><?php
            }
            ?>

       </form>
    <img src="<?php echo JURI::base() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/pre-loader-1.gif" alt="" class="pre-loader" style="opacity: 0;"/>
    <button onclick="sendRequest();" class="button accept hover-shadow" value="<?php echo  JText::_('COM_PLOT_ADD_AS_FRIEND')?>">
        <svg viewBox="0 0 31.7 30.8" preserveAspectRatio="xMidYMid meet" class="friend"><use xlink:href="#friend"></use></svg>
        <span>
            <?php echo  JText::_('COM_PLOT_ADD_AS_FRIEND')?>
        </span>
    </button>
</div>

