<?php
defined('_JEXEC') or die;
?>

<script type="text/javascript" src="<?php echo JUri::root() . 'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate() . '/js/jquery.jscrollpane.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate().'/js/jquery.mousewheel.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate() . '/js/jcarousel.min.js'; ?>"></script>
<!--<script type="text/javascript"
        src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/js/jcarousel.basic.js"></script>-->
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/assets/js/plot.js'; ?>"></script>

<script
    src="<?php echo JURI::base() . 'components/com_plot'; ?>/libraries/scrolling_ajax_pagination/javascript.js"></script>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">
function showFilteredAndSortedCourses()
{
    setAxaxScrollPagination();

    var tagsIds = jQuery("[id^=filter-courses-tag]:checked").map(function(){
        return jQuery(this).val();
    }).get();

    jQuery('#courses-list').html('<li class="p5_20">Загрузка...</li>');

    jQuery.post(
        'index.php?option=com_plot&task=events.ajaxGetFilteredAndSortedCourses',
        {
            ageId: jQuery('#parent-my-age').val(),
            tagsIds: tagsIds,
            categoryId: jQuery('#par-sel-course-1').val(),
            myCoursesOnly: jQuery('.show-only-my-courses').hasClass('ui-state-active') ? 1 : 0,
            sort: jQuery('#parent-course-filter').val(),
            limit: '<?php echo plotGlobalConfig::getVar('meetingsParentPageShowFirstCount') + plotGlobalConfig::getVar('meetingsParentPageShowFirstCount'); ?>'
        },
        function(response) {
            var data = jQuery.parseJSON(response);
            jQuery('#courses-count-founded').html('Найдено '+jPlot.helper.getWordEnding(data.countCourses, 'встреча', 'встречи', 'встреч'));
            jQuery('#courses-list').html(data.renderedCourses);

        }
    );
}

function jcaruselCurrentMeetingsPaginations(){
    jQuery('.jcarousel-control-prev')
        .on('jcarouselcontrol:active', function() {
            jQuery(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function() {
            jQuery(this).addClass('inactive');
        })
        .jcarouselControl({
            target: '-=3'
        });
    jQuery('.jcarousel-control-next')
        .on('jcarouselcontrol:active', function() {
            $(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function() {
            jQuery(this).addClass('inactive');
        })
        .jcarouselControl({
            target: '+=3'
        });

    jQuery('.jcarousel-pagination-meetings')
        .on('jcarouselpagination:active', 'a', function() {
            jQuery(this).addClass('active');
        })
        .on('jcarouselpagination:inactive', 'a', function() {
            jQuery(this).removeClass('active');
        });
}

function showFilteredCurrentEvents(){

    jQuery('#current_events').html('<div class="p5_20">Загрузка...</div>');
        jQuery.post(
            'index.php?option=com_plot&task=events.ajaxGetFilteredCurrentEvents',
            {
                sort: jQuery('#parent-meetings-actuality-1').val()

            },
            function(response) {
                var data = jQuery.parseJSON(response);
                var jcarousel = jQuery('#current_events').jcarousel();

                jQuery('#current_events')
                    .on('jcarousel:create jcarousel:reload', function() {
                        var element = jQuery(this),
                            width = element.innerWidth()*0.235,//23.5% ширина li в случае, если выводится по 4 элемента
                        margin=element.innerWidth()*0.02;//2% значение margin в случае

                        element.jcarousel('items').css('width', width + 'px');
                        element.jcarousel('items').css('margin-right', margin + 'px');

                    })
                    .jcarousel({});

                jcaruselCurrentMeetingsPaginations();

        // Append items
                jcarousel
                    .html('<ul class="parent-profile">'+data.renderedEvents+'</ul>');

                // Reload carousel
                jcarousel
                    .jcarousel('reload');
                jQuery('.jcarousel-pagination-meetings').jcarouselPagination({
                    item: function(page) {
                        return '<a href="#' + page + '"></a>';
                    }
                });
                jQuery('.jcarousel-pagination-meetings').jcarouselPagination('reloadCarouselItems');

                jQuery( window ).resize();
                caruseltags();
            }
        );
    }

function caruseltags(){
    var data=jQuery('#events-jcarousel').find('.parent-profile').html(),
        tagsCarusel=jQuery('#events-jcarousel').jcarousel();
    tagsCarusel
        .on('jcarousel:reload jcarousel:create', function () {
            var element = jQuery(this),
                width = element.innerWidth()*0.235,//23.5% ширина li в случае, если выводится по 4 элемента
                margin=element.innerWidth()*0.02;//2% значение margin в случае

            element.jcarousel('items').css('width', width + 'px');
            element.jcarousel('items').css('margin-right', margin + 'px');
        })
        .jcarousel({
            wrap: 'circular'
        });

    jcaruselCurrentMeetingsPaginations()
    tagsCarusel.html('<ul class="parent-profile">'+data+'</ul>');
    tagsCarusel.jcarousel('reload');
    jQuery('.jcarousel-pagination')
        .on('jcarouselpagination:active', 'a', function() {
            jQuery(this).addClass('active');
        })
        .on('jcarouselpagination:inactive', 'a', function() {
            jQuery(this).removeClass('active');
        })
        .on('click', function(e) {
            e.preventDefault();
        })
        .jcarouselPagination({
            perPage: 4,
            item: function(page) {
                return '<a href="#' + page + '"></a>';
            }
        });
    jQuery( window ).resize();
}

function setAxaxScrollPagination()
{
    var tagsIds = jQuery("[id^=filter-courses-tag]:checked").map(function(){
        return jQuery(this).val();
    }).get();
    var ajaxScrollData = {
        ageId: jQuery('#parent-my-age').val(),
        tagsIds: tagsIds,
        categoryId: jQuery('#par-sel-course-1').val(),
        myCoursesOnly: jQuery('.show-only-my-courses').hasClass('ui-state-active') ? 1 : 0,
        sort: jQuery('#parent-course-filter').val()
    };
    jQuery(window).unbind('scroll');

    jQuery('#courses-list').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('meetingsParentPageShowFirstCount'); ?>,
        offset: <?php echo plotGlobalConfig::getVar('meetingsParentPageShowFirstCount')+plotGlobalConfig::getVar('meetingsParentPageShowFirstCount'); ?>,
        error: 'No More Posts!',
        delay: 50,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=events.ajaxLoadMore'); ?>',
        appendDataTo: 'courses-list',
        userData: ajaxScrollData,
        afterLoad: {
            afterLoadAction: function () {
                jPlotUp.Arrow.initialize('positionCameras');
            }
        }
    });



    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });

}

function checkAllFilterCoursesTagsCheckboxes() {
    jQuery("[id^=filter-courses-tag]").prop('checked', true);
    showFilteredAndSortedCourses();
}

function clearAllFilterCoursesTagsCheckboxes() {
    jQuery("[id^=filter-courses-tag]").prop('checked', false);
    showFilteredAndSortedCourses();
}
function eventRemoveConfirm(eventId){
    SqueezeBox.open('<?php echo  JRoute::_("index.php?option=com_plot&task=photos.deleteConfirm", 0)?>&entity=eventList'+'&id='+eventId, {size:{x:300, y:150}, handler:'ajax'});
}

jQuery( "#parent-meetings" ).tabs();

jQuery(document).ready(function () {

    showFilteredAndSortedCourses();
    showFilteredCurrentEvents()

    // courses tabs click
    jQuery('.show-only-my-courses > a').click(function(){
        jQuery( window ).resize();
        jQuery('.show-only-my-courses').addClass('ui-state-active');
        showFilteredAndSortedCourses();
    });
    jQuery('.show-all-courses > a').click(function(){
        jQuery( window ).resize();
        jQuery('.show-only-my-courses').removeClass('ui-state-active');
        showFilteredAndSortedCourses();
    });

    jQuery('.show-new-events > a').click(function(){
        jQuery( window ).resize();
    });

    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });

    jQuery( "#parent-meetings" ).tabs();
    jQuery( "#parent-my-interest-1, #parent-my-age, #parent-course-filter" ).selectmenu({
        appendTo:'.main-content',
        close: function( event, ui ) {}
    });
    jQuery("#parent-my-interest-1, #parent-my-age, #parent-course-filter").on( "selectmenuclose", function( event, ui ) {} );

    <?php
    if(!$this->curr_events){
    ?>
    jQuery('.show-all-courses > a').click();
    <?php
    }
    ?>

});

function eventRemoveConfirm(eventId){
    SqueezeBox.open('<?php echo  JRoute::_("index.php?option=com_plot&task=photos.deleteConfirm&entity=eventList", 0)?>'+'&id='+eventId, {size:{x:300, y:150}, handler:'ajax'});
}

document.addEventListener("touchmove", function(){
    jQuery('.wrap.child-library').trigger('scroll');
    console.log('ScrollStart');
}, false);

</script>
<?php # </editor-fold> ?>

<main class="parent-profile">
    <div class="top parent-profile">
        <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_header') ); ?>
        <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_top_menu') ); ?>
    </div>
    <div class="wrap main-wrap parent-profile">
        <div class="add-left aside-left parent-profile"></div>
        <div class="add-right aside-right parent-profile"></div>
        <section class="parent-meetings">
            <div id="parent-meetings">
                <ul class="parent-courses-think-tabs">
                    <li class="show-new-events"><a href="#parent-meetings-jcarousel">Ближайшие встречи</a></li>
                    <li class="show-all-courses"><a href="#parent-all-site-meetings">Общий раздел всех встреч</a></li>
                    <li class="show-only-my-courses"><a href="#parent-all-site-meetings">Все мои встречи</a></li>
                </ul>

                <div id="parent-meetings-jcarousel" class="jcarousel-wrapper parent-profile">
                <div class="select-feed">
                    <form>
                        <fieldset>
                            <select id="parent-meetings-actuality-1">
                                <option value="now" selected="selected">Сейчас проходят</option>
                                <option value="new">Скоро будут</option>
                            </select>
                            <script type="text/javascript">
                                jQuery( "#parent-meetings-actuality-1" ).selectmenu({
                                    appendTo:'.main-content',
                                    change: function(){ showFilteredCurrentEvents()  }
                                });
                            </script>
                        </fieldset>
                    </form>
                </div>

                    <div class="jcarousel" id="current_events" >

                    </div>

                <p class="jcarousel-pagination-meetings"></p>
                </div>

                <?php # <editor-fold defaultstate="collapsed" desc="ALL COURSES"> ?>
                <div id="parent-all-site-meetings">

                    <div class="select-feed">
                        <form>
                            <fieldset>

                                <select id="parent-my-interest-1">
                                    <option value="all" selected="selected">Все интересы</option>
                                    <option value="my">Мои интересы</option>
                                    <?php foreach ($this->my->childrensIds AS $childId) { ?>
                                        <option value="child-<?php echo $childId; ?>">Интересы ребенка <?php echo plotUser::factory($childId)->name; ?></option>
                                    <?php } ?>
                                </select>
                                <script type="text/javascript">
                                    jQuery( "#parent-my-interest-1" ).selectmenu({
                                        appendTo:'.main-content',
                                        open: function( event, ui ) { jQuery('#parent-my-interest-1-menu').jScrollPane();},
                                        change: function(){ var selectedValue = jQuery('#parent-my-interest-1').val();
                                            switch (selectedValue) {
                                                case ('my'):
                                                    jQuery('[id^=filter-courses-tag]').prop('checked', false);


                                                    jQuery.post(
                                                        '<?php echo JRoute::_('index.php?option=com_plot&task=publications.ajaxGetMyTags'); ?>',
                                                        function(response) {
                                                            var data = jQuery.parseJSON(response),
                                                                i= 0,
                                                                data_count=data.length,
                                                                str='';
                                                            jQuery('[id^=filter-courses-tag]').prop('checked', false);
                                                            for (i; i<data_count; i++) {
                                                                str+='<div>'+data[i].id+'</div>';
                                                            }
                                                            jQuery('hidden-data.interests-data .my').html(str);
                                                            jQuery('hidden-data.interests-data .my div').each(function(){
                                                                var tagId = jQuery(this).html();
                                                                jQuery('#filter-courses-tag'+tagId).prop('checked', true);

                                                            });
                                                            showFilteredAndSortedCourses();
                                                        }
                                                    );

                                                    break;
                                                case ('all'):
                                                    jQuery('[id^=filter-courses-tag]').prop('checked', true);
                                                    showFilteredAndSortedCourses();
                                                    break;
                                                default:
                                                    jQuery('[id^=filter-courses-tag]').prop('checked', false);
                                                    jQuery('hidden-data.interests-data .'+jQuery(this).val()+' div').each(function(){
                                                        var tagId = jQuery(this).html();
                                                        jQuery('#filter-courses-tag'+tagId).prop('checked', true);
                                                    });
                                                    showFilteredAndSortedCourses();
                                            }
                                        }
                                    });
                                </script>
                                <!-- hidden my interests and my children's interests for javascript and fast no-ajax choosing interests -->
                                <hidden-data class="hidden interests-data">
                                    <item class="all">
                                        <?php foreach ($this->plotTags AS $i=>$tag) { ?>
                                            <div><?php echo $tag->id; ?></div>
                                        <?php } ?>
                                    </item>
                                    <item class="my">
                                        <?php foreach ($this->my->getTags() AS $tag) { ?>
                                            <div><?php echo $tag->id; ?></div>
                                        <?php } ?>
                                    </item>
                                    <?php foreach ($this->my->childrensIds AS $childId) { ?>
                                        <item class="child-<?php echo $childId; ?>">
                                            <?php foreach (plotUser::factory($childId)->getTags() as $childTag) { ?>
                                                <div><?php echo $childTag->id; ?></div>
                                            <?php } ?>
                                        </item>
                                    <?php } ?>
                                </hidden-data>
                                <select id="parent-my-age">
                                    <option value="0"><?php echo JText::_('COM_PLOT_ALL_AGES'); ?></option>
                                    <?php foreach ($this->plotAges AS $age) { ?>
                                        <option value="<?php echo $age->id; ?>"><?php echo $age->title; ?></option>
                                    <?php } ?>
                                </select>
                                <script type="text/javascript">
                                    jQuery( "#parent-my-age" ).selectmenu({
                                        appendTo:'.main-content',
                                        open: function( event, ui ) { jQuery('#parent-my-age-menu').jScrollPane();},
                                        change: function(){ showFilteredAndSortedCourses(); }
                                    });
                                </script>
                            </fieldset>
                        </form>
                        <a class="link-back" href="<?php echo $this->referrerUrl; ?>">Назад</a>
                    </div>
                    <div class="checkbox-interest jcarousel-wrapper">
                        <div class="jcarousel" id="events-jcarousel">
                            <ul class="parent-profile">
                                <?php foreach ($this->plotTags AS $i=>$tag) { ?>
                                    <?php echo ($i % 6) ? '' : '<li>'; ?>
                                    <input type="checkbox" checked="checked" id="filter-courses-tag<?php echo $tag->id; ?>" value="<?php echo $tag->id; ?>" onchange="showFilteredAndSortedCourses();" />
                                    <label for="filter-courses-tag<?php echo $tag->id; ?>"><?php echo $tag->title; ?></label>
                                <?php } ?>
                                <?php echo ($i % 6) ? '' : '</li>'; ?>
                            </ul>
                        </div>
                        <button onclick="checkAllFilterCoursesTagsCheckboxes();">Выбрать все</button>
                        <button onclick="clearAllFilterCoursesTagsCheckboxes();">Очистить</button>
                        <p class="jcarousel-pagination"></p>
                    </div>
                    <div class="filter-result parent-meetings">
                        <form>
                            <fieldset>

                                <p>
                                    <label for="par-sel-course-1" id="courses-count-founded">Найдено 39 встреч</label>
                                    <select id="par-sel-course-1">
                                        <option value="now" selected="selected">Сейчас проходят</option>
                                        <option value="new">Скоро будут</option>
                                        <option value="old">Уже прошли</option>
                                    </select>
                                    <script type="text/javascript">
                                        jQuery( "#par-sel-course-1" ).selectmenu({
                                            appendTo:'.main-content',
                                            change: function(){ showFilteredAndSortedCourses();  }
                                        });
                                    </script>

                                </p>
                                <p>
                                    <label for="parent-course-filter">Сортировать:</label>
                                    <select id="parent-course-filter">
                                        <option value="`e`.`title` ASC">От А до Я</option>
                                        <option value="`e`.`title` DESC">От Я до А</option>
                                        <option value="`e`.`create_date` DESC">Последние добавленные</option>
                                        <option value="sum DESC">Популярность по убыванию</option>
                                    </select>
                                    <script type="text/javascript">
                                        jQuery( "#parent-course-filter" ).selectmenu({
                                            appendTo:'.main-content',
                                            open: function( event, ui ) { jQuery('#parent-course-filter-menu').jScrollPane();},
                                            change: function(){ showFilteredAndSortedCourses(); }
                                        });
                                    </script>
                                </p>
                            </fieldset>
                        </form>
                    </div>
                    <ul class="parent-profile no-jcarousel" id="courses-list"></ul>
                </div>
                <?php # </editor-fold> ?>

            </div>
        </section>
    </div>
</main>
