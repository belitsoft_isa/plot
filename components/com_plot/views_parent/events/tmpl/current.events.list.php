<?php
defined('_JEXEC') or die;

# Категория: echo $course->c_category;
# Мин стоимость: echo $course->total_min_cost;
# Найдено всего: echo count($this->courses);
?>

<?php

if ($this->courses) {
    foreach ($this->courses AS $key => $item) {
        ?>
        <li class="parent-meetings" id="current-plot-event-id-<?php echo $item->id; ?>" data-event-id="<?php echo $item->id; ?>" >
            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=event&id='.$item->id);?>">
                <?php
                if ($item->create_date != '0000-00-00 00:00:00') {
                    if (strtotime($item->create_date . '+'.plotGlobalConfig::getVar("meetingIsNewMaxDays").' day') > strtotime("now")) {
                        ?>
                        <span class="arrow-bottom">Новая!</span>
                    <?php
                    }
                }
                ?>
                <h6><?php echo $item->title; ?></h6>
                <i class="author">автор:
                    <?php echo (strlen(plotUser::factory($item->user_id)->name) > 80) ? substr(plotUser::factory($item->user_id)->name, 0, 80) . '...' : plotUser::factory($item->user_id)->name; ?>
                </i>
                <?php if (PlotHelper::is_date($item->start_date)) { ?>
                    <span> <?php echo PlotHelper::russian_date(JHtml::date($item->start_date, 'd.m.Y')); ?> </span>
                <?php } ?>
                <span class="quantity"><?php echo PlotHelper::countEventSubscription($item->id); ?>
                    <svg viewBox="0 0 23 23" preserveAspectRatio="xMinYMin meet">
                        <use xlink:href="#people"></use>
                    </svg>
               </span>

                <div class="counter">
                    <p class="start-time">
                        <b>Время начала</b>
                        <?php if (PlotHelper::is_date($item->start_date)) { ?>
                            <time><?php echo JHtml::date($item->start_date, 'H:m'); ?></time>
                        <?php } ?>
                    </p>
                    <?php
                    if (PlotHelper::is_date($item->start_date) && PlotHelper::is_date($item->end_date)) {
                        if (PlotHelper::compareDates(JHtml::date($item->start_date, 'Y-m-d')) == -1) {
                            ?>
                            <p class="duration"><b>Длительность</b>
                                <time><?php echo PlotHelper::diffDates(JHtml::date($item->start_date, 'Y-m-d H:m'), JHtml::date($item->end_date, 'Y-m-d H:m')); ?></time>
                            </p>
                        <?php
                        } else if (PlotHelper::compareDates(JHtml::date($item->start_date, 'Y-m-d')) == 1) {
                            ?>
                            <p class="before-start"><b>До начала:</b>
                                <time><?php echo PlotHelper::downcounter($item->start_date); ?></time>
                            </p>
                        <?php
                        } else if (PlotHelper::compareDates(JHtml::date($item->start_date, 'Y-m-d')) == 0) {
                            ?>
                            <p class="before-end"><b>До окончания:</b>
                                <time><?php echo PlotHelper::downcounter($item->start_date); ?></time>
                            </p>
                        <?php
                        }
                    }
                    ?></div>
                <div class="address">Место проведения:
                    <address><?php echo $item->place; ?></address>
                </div>
                <div class="circle-img">
                    <svg class="clip-svg">
                       <?php  if (file_exists(JPATH_SITE . '/images/com_plot/events/' .  $item->user_id .  '/thumb/'.$item->img)){
                       ?>
                           <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                   xlink:href="<?php echo JUri::root().'images/com_plot/events/' .  $item->user_id .  '/thumb/'.$item->img; ?>" alt=""/>
                       <?php }else{
                           ?>
                           <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                  xlink:href="<?php echo JUri::root().'images/com_plot/def_meeting.jpg' ?>" alt=""/>
                     <?php
                        }?>

                    </svg>
                    <i class="activity-icons">
                        <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);">
                            <use xlink:href="#lamp-meeting"></use>
                        </svg>
                    </i>
                </div>
                <p><?php echo $item->description; ?></p>
            </a>

            <div id="wrap-btn-event-<?php echo $item->id; ?>">
                <?php
                if ((int)PlotHelper::chechEventSubscription($item->id)) {
                    if ((int)PlotHelper::isOwnerEvent($item->id)) {
                        ?>
                        <button class="add hover-shadow" onclick="eventRemoveConfirm(<?php echo $item->id;?>); return false;"
                                data-id="<?php echo $item->id; ?>"><?php echo JText::_('COM_PLOT_REMOVE_EVENT'); ?></button>
                    <?php
                    } else {
                        ?>
                        <button class="add hover-shadow" onclick="App.Event.RemoveSubscribe(this); return false;"
                                data-id="<?php echo $item->id; ?>"><?php echo JText::_('COM_PLOT_REMOVE_SUBSCRIBE_EVENT'); ?></button>
                    <?php
                    }

                } else {
                    ?>
                    <button class="add hover-shadow" onclick="App.Event.Subscribe(this);return false;"
                            data-id="<?php echo $item->id; ?>"><?php echo JText::_('COM_PLOT_SUBSCRIBE_ON_EVENT'); ?></button>
                <?php
                } ?>
            </div>
        </li>
    <?php
    } ?>
<?php

} ?>
<?php
if (!$this->courses) {
    echo '<li class="no-items">Встреч не найдено</li>';
}
?>
