<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewEvents extends PlotViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'events';

    public function display($tpl = null)
    {
        $this->componentUrl = JURI::base() . 'components/com_plot';
        $this->templateUrl = JURI::base() . 'templates/' . JFactory::getApplication()->getTemplate();

        $this->referrerUrl = JRoute::_('index.php?option=com_plot&view=events');

        $this->my = plotUser::factory();
        $this->my->level = $this->my->getLevel();
        $this->my->childrensIds = $this->my->getChildrenIds();

        $this->plotAges = plotAges::getList();
        $this->plotTags = plotTags::getK2TagsList();

        $this->showOnlyMyCourses = JRequest::getVar('onlymy', false);
        $model = $this->getModel();
        $curr_events = $model->getCurrentEvents('now');
        $this->curr_events = false;
        if (count($curr_events['items'])) {
            $this->curr_events = true;
        }

        $this->setOgMeta();
        return parent::display($tpl);
    }

    private function setOgMeta()
    {
        require_once JPATH_ADMINISTRATOR . '/components/com_plot/plot_social_config.php';
        $metaOgUrl = '<meta property="og:url" content="http://naplotu.com/events/" />';
        $metaOgTitle = '<meta property="og:title" content="' . plotSocialConfig::get('eventsTitle') . '" />';
        $metaOgDescription = '<meta property="og:description" content="' . plotSocialConfig::get('eventsDesc') . '" />';
        $metaOgImage = '<meta property="og:image" content="' . JUri::root() . plotSocialConfig::get('eventsImagePath') . '" />';
        $metaOgImageType = '<meta property="og:image:type" content="image/jpeg" />';
        $metaOgImageWidth = '<meta property="og:image:width" content="' . plotSocialConfig::get('eventsImageWidth') . '" />';
        $metaOgImageHeight = '<meta property="og:image:height" content="' . plotSocialConfig::get('eventsImageHeight') . '" />';
        $metaOgSiteName = '<meta property="og:site_name" content="' . JFactory::getConfig()->get('sitename') . '" />';
        $metaOgFbAppId = '<meta property="fb:app_id" content="473991812742077" />';
        $metaOgType = '<meta property="og:type" content="website" />';
        $metaOgLocale = '<meta property="og:locale" content="ru_RU" />';
        JFactory::getDocument()->addCustomTag($metaOgTitle . $metaOgSiteName . $metaOgUrl . $metaOgDescription . $metaOgFbAppId . $metaOgType . $metaOgLocale . $metaOgImage . $metaOgImageType . $metaOgImageWidth . $metaOgImageHeight);
        return true;
    }

}
