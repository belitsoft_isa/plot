<?php

require_once JPATH_COMPONENT.'/views_parent/view.php';

class PlotViewEvents extends PlotViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'events';

    public function display($tpl = null)
    {
        return parent::display($tpl);
    }
    
    public function ajaxRenderCoursesList()
    {
        $this->my = plotUser::factory();
        return $this->loadTemplate();
    }

}
