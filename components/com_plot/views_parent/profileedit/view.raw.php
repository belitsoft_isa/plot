<?php
defined('_JEXEC') or die;
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/friends.php');
require_once JPATH_COMPONENT.'/views_parent/view.php';

class PlotViewProfileEdit extends PlotViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'profileEdit';

    public function display($tpl = null)
    {
        require_once JPATH_COMPONENT.'/helpers/countries.php';
        Foundry::import('fields:/user/country/country');
        $app = JFactory::getApplication();

        $id=JRequest::getInt('id',0);
        $this->my = plotUser::factory();
        if ($id) {
            $this->my = plotUser::factory($id);
        }

        $this->my->birthday = $this->getMyBirthdayObject($this->my);
        $this->my->tags = PlotHelper::myTags();
        
        $this->tags = plotTags::getK2TagsList();

        $this->templateUrl = JURI::base() . 'templates/' . $app->getTemplate();
        $this->componentUrl = JURI::base() . 'components/com_plot';

        $modelFriends = JModelLegacy::getInstance('friends', 'EasySocialModel');
        $this->friends = $modelFriends->getPendingRequests($this->my->id);
        if (!$this->friends) {
            $this->friends = array();
        }

        $modelAboutme = $modelFriends = JModelLegacy::getInstance('Aboutme', 'PlotModel');
        $this->childrenIds = $modelAboutme->getAllChildrenIds();


        return parent::display($tpl);
    }
    
    public function displayFriendsList($tpl = null)
    {


        $this->tagId = JRequest::getVar('tagId');
        $userId = JRequest::getVar('userId');




        $this->tags = PlotHelper::myTags();

        $this->user = plotUser::factory($userId);
        
        $modelFriends = JModelLegacy::getInstance('friends', 'EasySocialModel');
        $this->friends = $modelFriends->getPendingRequests($this->user->id);
        if (!$this->friends) {
            $this->friends = array();
        } 
        foreach ($this->friends AS $i=>$friend) {
            if ($friend->actor_id == $this->user->id) {
                $friendId = $friend->target_id;
            } else {
                $friendId = $friend->actor_id;
            }
            $this->friends[$i]->friendshipTag = $this->user->getFriendshipTag($friendId);
            if ($this->tagId && $this->friends[$i]->friendshipTag != $this->tagId) {
                unset($this->friends[$i]);
            }
        }
        
        $this->oldfriends = $modelFriends->getFriends($this->user->id);
        if (!$this->oldfriends) {
            $this->oldfriends = array();
        }        
        foreach ($this->oldfriends AS $i=>$friend) {
            $this->oldfriends[$i]->friendshipTag = $this->user->getFriendshipTag($friend->id);
            if ($this->tagId && $this->oldfriends[$i]->friendshipTag != $this->tagId) {
                unset($this->oldfriends[$i]);
            }
        }



        return parent::display($tpl);
    }

    public function displayForeign($tpl = null)
    {
        require_once JPATH_COMPONENT.'/helpers/countries.php';
        Foundry::import('fields:/user/country/country');
        $app = JFactory::getApplication();
        
        $userId = JRequest::getVar('id', 0);

        $this->user = plotUser::factory( $userId );
        $this->user->birthday = $this->getMyBirthdayObject($this->user);

        //$this->user->tags = $this->user->getTags();
        $this->user->tags = PlotHelper::myTags($userId);

        foreach ($this->user->tags AS $myTag) {
            switch ($myTag->smiley) {
                case 1:
                    $myTag->smileyInputClass = 'dont-like';
                    break;
                case 2:
                    $myTag->smileyInputClass = 'so-so';
                    break;
                case 3:
                    $myTag->smileyInputClass = 'like';
                    break;
                default:
                    $myTag->smileyInputClass = '';
                    break;
            }
        }

        $this->tags = plotTags::getK2TagsList();

        $this->templateUrl = JURI::base() . 'templates/' . $app->getTemplate();
        $this->componentUrl = JURI::base() . 'components/com_plot';

        $modelFriends = JModelLegacy::getInstance('friends', 'EasySocialModel');
        $this->friends = $modelFriends->getPendingRequests($this->user->id);
        if (!$this->friends) {
            $this->friends = array();
        }
        $this->oldfriends = $modelFriends->getFriends($this->user->id);
        if (!$this->oldfriends) {
            $this->oldfriends = array();
        }


        return parent::display($tpl);
    }

    public function displayTagItem()
    {
        $result = $this->loadTemplate();
        if ($result instanceof Exception) {
            return $result;
        }
        return $result;
    }
    
    public function getMyBirthdayObject($user)
    {
        $birthday = $user->getSocialFieldData('BIRTHDAY');
        if (!$birthday) {
            $birthday = new stdClass();
            $birthday->day = '01';
            $birthday->month = '01';
            $birthday->year = '1900';
        }        
        return $birthday;
    }

}
