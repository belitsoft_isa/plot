<?php
defined('_JEXEC') or die;
?>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link type="text/css" href="<?php echo JUri::root(); ?>templates/plot/css/style.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo JUri::root(); ?>media/jui/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>templates/plot/js/jquery-ui.min.js"></script>
<script src="<?php // echo JUri::root(); ?>components/com_plot/views_parent/profileedit/tmpl/profile_edit_popup.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/js/jquery.jscrollpane.min.js"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/mootools-core.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/core.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/mootools-more.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/modal.js" type="text/javascript"></script>
<script src="<?php echo $this->templateUrl; ?>/js/all.fineuploader-5.0.8.min.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/plot/js/svgsprite.js"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate() . '/js/jcarousel.min.js'; ?>"></script>

<script src="<?php echo JUri::root(); ?>templates/plot/js/jplot.js" type="text/javascript"></script>

<?php # <editor-fold defaultstate="collapsed" desc="CSS"> ?>
<style type="text/css">
    #country-edit-value-button {
        width: 150px !important;
    }
</style>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">
    jQuery(document).ready(function () {
        filterMyFriendsByTags();
    });

    function removeAvatar() {

        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxRemoveAvatar', function () {
            jQuery('#about-me-container .parent-avatar').attr('src', '<?php echo JUri::root(); ?>media/com_easysocial/defaults/avatars/user/square.png');
            window.parent.document.getElementById('plot-my-avatar').src="<?php echo JUri::root().'media/com_easysocial/defaults/avatars/user/square.png'; ?>";
        });
    }
    function filterMyFriendsByTags()
    {
        var tagId = jQuery('#friends-filter-tags').val();

        jQuery('.loading-friends-list').html('Загрузка...');
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxFilterMyFriendsByTags', {userId: <?php echo $this->my->id; ?>, tagId: tagId}, function(jsonResponce){
            var data = jQuery.parseJSON(jsonResponce);
            jQuery('#tab-1').html( data.html );
            jPlot.profile.showPopupDataAndRefreshSelectsAndScrollpanes();
        });
    }
    function toggleUserTag(tagId) {
        var tagInput = jQuery('#tag' + tagId);
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxToggleTag', {tagId: tagId, checked: tagInput.is(':checked')}, function (error) {
            if (error) {
                alert('tags limit: <?php echo plotGlobalConfig::getVar('maxTagsForUser'); ?>');
                tagInput.attr('checked', false);
            }
        });
    }

    function addTag()
    {
        var tag_id = parseInt( jQuery("#plot-tags option:selected").val() );
        jQuery('#add-tag-pre-loader').css('opacity', 1);
        if (tag_id) {
            var text = jQuery("#plot-tags option:selected").text(),
                count_tags = parseInt( jQuery('#plot-list-my-tags .my-interest').length );

            if (count_tags >= <?php echo plotGlobalConfig::getVar('maxTagsForUser'); ?>) {
                jQuery('#add-tag-pre-loader').css('opacity', 0);
                jQuery('#add-tags').show();
                alert("<?php echo JText::_('COM_PLOT_YOU_HAVE_MAX_COUNT_TAGS')?>");
            } else {
                var checkedSmile = null;
                checkedSmileVal = 'not-checked';
                if (jQuery('.new-interest .smile-interest [name=el]:checked').length) {
                    var checkedSmileVal = jQuery('.new-interest .smile-interest [name=el]:checked').val();
                    switch (checkedSmileVal) {
                        case 'dont-like':
                            checkedSmile = 1;
                            break;
                        case 'so-so':
                            checkedSmile = 2;
                            break;
                        case 'like':
                            checkedSmile = 3;
                            break;
                    }
                }

                var tagDesc = jQuery('.new-interest .new-tag-description').val();

                jQuery.post(
                    'index.php?option=com_plot&task=profileedit.ajaxAddTag',
                    {tag_id: tag_id, tag_smile: checkedSmile, tag_desc: tagDesc, checkedSmileVal: checkedSmileVal, tagName: text },
                    function (response) {
                        var data = jQuery.parseJSON(response),
                            filter_tags=document.getElementById("friends-filter-tags");
                        jQuery('#plot-list-my-tags .jspPane').append(data.html);
                        updateAddTagsOptions(jQuery('#plot-tags'));
                        clearAddTagArea();
                        jQuery('#plot-list-my-tags').jScrollPane();
                        filter_tags.options[filter_tags.options.length] = new Option(text, tag_id);
                        jQuery('#friends-filter-tags').selectmenu('refresh');
                        updateListsTags();
                        parent.allEventsLoad();
                        jQuery('#add-tag-pre-loader').css('opacity', 0);
                        jQuery('#add-tags').show();
                    }
                );
            }
        }else{
            jQuery('#add-tag-pre-loader').css('opacity', 0);
            jQuery('#add-tags').show();
        }
    }


    function updateListsTags(){
        jQuery.post(
            '<?php echo JRoute::_('index.php?option=com_plot&task=publications.ajaxGetMyTags'); ?>',
            function(response) {
                var data = jQuery.parseJSON(response),
                    i= 0,
                    data_count=data.length,
                    str='',
                    tag,
                    tags=window.parent.document.getElements('[id^=filter-courses-tag]');
                jQuery(tags).each(function(){
                     jQuery(this).prop('checked', false);
                });
                for (i; i<data_count; i++) {
                    tag=window.parent.document.getElementById('filter-courses-tag'+data[i].id);
                    jQuery(tag).prop('checked', true);
                }
            }
        );
    }

    function childRemove(id, that){

        jQuery.post(
            '<?php echo JRoute::_('index.php?option=com_plot&task=index.php?option=com_plot&task=aboutme.removeChild'); ?>',
            {
                id:id
            },
            function (response) {
                if(response.status){
                    jQuery(that).parent().remove();
                }else{

                }
            }
        );
    }

    function clearAddTagArea(){
        jQuery('.new-interest .smile-interest').find(jQuery("[name='el']").removeAttr("checked"));
        jQuery('.new-tag-description').val('');
    }

    jQuery(document).ready(function(){
        var manualUploaderAvatar = new qq.FineUploader({
            element: document.getElementById("upload-avatar-form"),
        //jQuery('#upload-avatar-form').fineUploader({
            request: {
                endpoint: '<?php echo JRoute::_("index.php?option=com_plot&task=profileedit.avatarImageUpload")?>'
            },
            multiple: false,
            validation: {
                sizeLimit: parseInt('<?php echo (int)PlotHelper::returnBytes(ini_get('upload_max_filesize'));?>')
            },
            callbacks: {
                onComplete: function (id, filename, responseJSON) {
                    if (responseJSON.status) {

                        jQuery('#avatar-id').val(responseJSON.id);
                        jQuery('#avatar-modal-link').attr('href', '<?php echo JRoute::_('index.php?option=com_plot&task=profileedit.ajaxAvatarImage'); ?>&img_id=' + responseJSON.id);

                        window.parent.document.getElementById('sbox-window').style.position='fixed';
                        document.getElementById('avatar-modal-link').click();

                    } else {

                        var overlay='<div class="avatar-overlay" id="sbox-overlay2" tabindex="-1"></div>',
                            str='<div id="sbox-window2" role="dialog" aria-hidden="false" class="shadow avatar-sbox">' +
                                '<div id="sbox-content2" class="sbox-content-adopt" style="opacity: 1;"><div id="enqueued-message">'+responseJSON.message+
                                '</div></div><a id="sbox-btn-close2" href="#" onclick="window.parent.document.getElementById(\'sbox-window\').style.position=\'fixed\';jQuery(this.parentNode.previousSibling).remove(); jQuery(this.parentNode).remove();" role="button" aria-controls="sbox-window"></a></div>';

                        jQuery("#avatar-box").after(str);
                        jQuery("#avatar-box").after(overlay);
                    }
                }
            }
        });
    });
</script>

<?php # </editor-fold> ?>

<div id="svg-sprite"></div>
<div id="about-me-container" class="wrap">

<div id="tabs" class="user-info parent-profile">
<ul class="popup-header parent-profile">
    <li><a href="#tab-1">Все мои друзья<?php if ($this->friends) {echo '<b class="count-friend-requests">'.count($this->friends).'</b>';} ?></a></li>
    <li><a href="#tab-2">Мои интересы</a></li>
    <li><a href="#tab-3">Обо мне</a></li>
</ul>

<?php # <editor-fold defaultstate="collapsed" desc="Friends Tab">?>
<div id="tab-1">
    <div>
        <p>
            <span class="loading-friends-list"></span>
        </p>
        <ul class="scroll-pane"></ul>
    </div>
</div>
<?php // </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="Interests Tab">?>
<div id="tab-2">
    <div class="new-interest">
        <form>
            <fieldset>
                <select id="plot-tags">
                    <option value="0">-- Выберите Ваше увлечение --</option>
                    <?php foreach ($this->tags AS $tag) {
                        if (!plotTags::checkIsTagExist($tag->id, 'user', $this->my->id)) { ?>
                            <option value="<?php echo $tag->id; ?>"><?php echo $tag->title; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
                <div class="smile-interest">
                    <input type="radio" id="el-dont-like" name="el" value="dont-like"/>
                    <label for="el-dont-like">
                        <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-dont-like radio-icon">
                            <use xlink:href="#smile-dont-like"></use>
                        </svg>
                    </label>
                    <input type="radio" id="el-so-so" name="el" value="so-so" />
                    <label for="el-so-so">
                        <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-so-so radio-icon ">
                            <use xlink:href="#smile-so-so"></use>
                        </svg>
                    </label>
                    <input type="radio" id="el-like" name="el" value="like"/>
                    <label for="el-like">
                        <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-like radio-icon">
                            <use xlink:href="#smile-like"></use>
                        </svg>
                    </label>
                    <p>оцените его</p>
                </div>
                <input type="text" class="new-tag-description" placeholder="Объясните почему..." />
                <a class="add hover-shadow" href="javascript:void(0)" onclick="jQuery('#add-tags').hide(); addTag()" id="add-tags">
                    Добавить
                </a>
                <img id="add-tag-pre-loader" src="<?php echo JURI::root() . 'templates/plot/img/pre-loader-1.gif'; ?>" alt="" style="opacity: 0;" class="pre-loader"
                    />
            </fieldset>
        </form>
    </div>
    <ul id="plot-list-my-tags" class="scroll-pane">
        <?php foreach ($this->my->tags AS $tag) { ?>

            <li class="my-interest my-interest-<?php echo $tag->id; ?>">
                <span><?php echo $tag->title;?></span>
                <div>
                    <form class="smile-interest" onsubmit="return false;">
                        <fieldset>
                            <input type="radio" id="el_<?php echo $tag->id;?>-dont-like" name="el_<?php echo $tag->id;?>" value="dont-like" />
                            <label for="el_<?php echo $tag->id;?>-dont-like" onclick="tagSmileyChange('<?php echo $tag->id;?>','1');" >
                                <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-dont-like radio-icon js-smile-<?php echo $tag->id.'1';?>" <?php if ((int)$tag->smiley == 1) {echo 'style="fill: #eb3d00;"';};?> >
                                    <use xlink:href="#smile-dont-like"></use>
                                </svg>
                            </label>
                            <input type="radio" id="el_<?php echo $tag->id;?>-so-so" name="el_<?php echo $tag->id;?>" value="so-so" selected="true" />
                            <label for="el_<?php echo $tag->id;?>-so-so" onclick="tagSmileyChange('<?php echo $tag->id;?>','2');">
                                <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-so-so radio-icon js-smile-<?php echo $tag->id.'2';?>" <?php if ((int)$tag->smiley == 2) {echo 'style="fill: #f48000;"';};?>>
                                    <use xlink:href="#smile-so-so"></use>
                                </svg>
                            </label>
                            <input type="radio" id="el_<?php echo $tag->id;?>-like" name="el_<?php echo $tag->id;?>" value="like"/>
                            <label for="el_<?php echo $tag->id;?>-like" onclick="tagSmileyChange('<?php echo $tag->id;?>','3');">
                                <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-like radio-icon js-smile-<?php echo $tag->id.'3';?>" <?php if ((int)$tag->smiley == 3) {echo 'style="fill: #009049;"';};?>>
                                    <use xlink:href="#smile-like"></use>
                                </svg>
                            </label>
                            <div class="interest-explain">
                                <input type="text"  placeholder="напишите почему" value="<?php echo $tag->reason; ?>" <?php if ((int)$tag->smiley && trim($tag->reason)) {echo 'class="'.$tag->smileyInputClass.' plot-reason"';} else {echo 'class="not-checked plot-reason"';};?>/>
                                <input class="like-before-change" type="hidden" value="<?php echo $tag->reason; ?>" />
                                <button class="edit-pencil" onclick="jQuery('#plot-list-my-tags .my-interest-<?php echo $tag->id; ?> .plot-reason').focus();">
                                    <svg viewBox="0 0 8.3 14.6" preserveAspectRatio="xMinYMin meet"class="edit">
                                        <use xlink:href="#edit"></use>
                                    </svg>
                                </button>
                                <button class="apply" onmousedown="tagTitleSave('<?php echo $tag->id;?>');"></button>
                                <button class="cancel" onmousedown="tagTitleCancel('<?php echo $tag->id;?>');">&#215</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <button class="delete" onclick="deleteTag('<?php echo $tag->id;?>');">&#215</button>
            </li>
        <?php } ?>
    </ul>

    <!--?php // <editor-fold defaultstate="collapsed" desc="TAGS FOOTER"> ?-->

    <?php // </editor-fold> ?>
</div>
<?php // </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="AboutMe Tab">?>
<div id="tab-3">
<div class="tab-3-wrapper scroll-pane parent-profile">
<div class="resume">
    <p>
        <img class="avatar parent-avatar" src="<?php echo $this->my->getSquareAvatarUrl(); ?>"  alt=""/>
        <i class="close" onclick="removeAvatar();">&#215</i>
    </p>

    <input type="hidden" name="avatar-id" id="avatar-id"/>
    <a style="display: none;" id="avatar-modal-link"
       rel="{size: {x: 550, y: 500}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
       href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxCertificateImage'); ?>"
       class="modal"></a>
    <label class="upload-img-label" id="upload-avatar-form">Загрузить фото</label>

    <script type="text/template" id="qq-template">
        <div class="qq-uploader-selector qq-uploader">
            <div class="qq-upload-button-selector qq-upload-button">
                Поменять фото
            </div>
    <span class="qq-drop-processing-selector qq-drop-processing" style="display: none;">

      <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
    </span>
            <ul class="qq-upload-list-selector qq-upload-list" style="display: none;">
                <li>
                    <div class="qq-progress-bar-container-selector">
                        <div class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                                                <span
                                                    class="qq-edit-filename-icon-selector qq-edit-filename-icon"></span>
                    <span class="qq-upload-file-selector qq-upload-file"></span>
                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0"
                           type="text">
                    <span class="qq-upload-size-selector qq-upload-size"></span>
                    <a class="qq-upload-cancel-selector qq-upload-cancel" href="#"></a>
                    <a class="qq-upload-retry-selector qq-upload-retry" href="#"></a>
                    <a class="qq-upload-delete-selector qq-upload-delete" href="#"></a>
                                                <span
                                                    class="qq-upload-status-text-selector qq-upload-status-text"></span>
                </li>
            </ul>

    </script>

    <div id="about-me-edit-block">
        <label for="resume">Обо мне</label>
        <a href="#resume-1" onclick="changeField('about-me');" class="input-data">
            <div class="about-me-show-value"><?php echo PlotHelper::cropStr($this->my->getSocialFieldData('ABOUT_ME'), 450); ?></div>
            <button class="edit-pencil" onclick="changeField('about-me');">
                <svg viewBox="0 0 8.3 14.6" preserveAspectRatio="xMinYMin meet" class="edit">
                    <use xlink:href="#edit"></use>
                </svg>
            </button>
        </a>
        <a href="#x" class="overlay" onclick="cancelField('about-me');" id="resume-1"></a>
        <textarea  maxlength="<?php echo plotGlobalConfig::getVar('maxLenghtAboutMe')?>" class="popup about-me-edit-value" id="resume" autofocus /><?php echo $this->my->getSocialFieldData('ABOUT_ME'); ?></textarea>
        <button class="apply" onclick="aboutMeSave();"></button>
        <button class="cancel" onclick="cancelField('about-me');">&#215</button>
    </div>
</div>
<ul>
    <div class="my-children">
        <p>Мои дети:</p>
        <?php
        foreach ($this->my->getChildrenIds() AS $childId) {
            ?>
            <div class="child-image" child-id="<?php echo $childId; ?>">
                <img class="avatar" src="<?php echo plotUser::factory($childId)->getSquareAvatarUrl(); ?>" alt="<?php echo plotUser::factory($childId)->name; ?>" />
                <i class="close" onclick="childRemove(<?php echo $childId; ?>,this);">&#215</i>
            </div>
        <?php
        }
        ?>

        <a class="modal add-recipient" href="<?php echo JRoute::_('index.php?option=com_plot&task=searchchildren.showPopup'); ?>" >
            <svg viewBox="0 0 31.7 30.8" preserveAspectRatio="xMinYMin meet" class="friend"><use xlink:href="#friend"></use></svg>
            Добавить
        </a>

    </div>
    <div>
        <li class="about-me" id="last-name-edit-block">
            <label for="surname">Фамилия:</label>
            <a href="#surname-1" onclick="changeField('last-name');" class="input-data">
                    <span class="last-name-show-value"><?php echo PlotHelper::cropStr($this->my->getSocialFieldData('last'), 30) ; ?>                                    </span>
                <button class="edit-pencil" onclick="changeField('last-name');">
                    <svg viewBox="0 0 8.3 14.6" preserveAspectRatio="xMinYMin meet" class="edit">
                        <use xlink:href="#edit"></use>
                    </svg>
                </button>
            </a>
            <a href="#x" class="overlay" onclick="cancelField('last-name');" id="surname-1"></a>
            <textarea  maxlength="<?php echo plotGlobalConfig::getVar('maxLenghtSurname')?>" class="popup last-name-edit-value" id="surname" placeholder="фамилия" autofocus ><?php echo $this->my->getSocialFieldData('last'); ?></textarea>
            <button class="apply" onclick="lastNameSave();"></button>
            <button class="cancel" onclick="cancelField('last-name');">&#215</button>
        </li>
        <li class="about-me" id="first-name-edit-block">
            <label for="name">Имя:</label>
            <a href="#name-1" onclick="changeField('first-name');" class="input-data">
                    <span class="first-name-show-value"><?php echo PlotHelper::cropStr($this->my->getSocialFieldData('first'), 30); ?></span>
                <button class="edit-pencil" onclick="changeField('first-name');">
                    <svg viewBox="0 0 8.3 14.6" preserveAspectRatio="xMinYMin meet" class="edit">
                        <use xlink:href="#edit"></use>
                    </svg>
                </button>
            </a>
            <a href="#x" class="overlay" onclick="cancelField('first-name');" id="name-1"></a>
            <textarea maxlength="<?php echo plotGlobalConfig::getVar('maxLenghtFirstName')?>" class="popup first-name-edit-value" id="name" placeholder="имя" autofocus/><?php  echo $this->my->getSocialFieldData('first'); ?></textarea>
            <button class="apply" onclick="firstNameSave();"></button>
            <button class="cancel" onclick="cancelField('first-name');">&#215</button>
        </li>
        <li class="about-me" id="middle-name-edit-block">
            <label for="patronymic">Отчество:</label>
            <a href="#patronymic-1" onclick="changeField('middle-name');" class="input-data">
                    <span class="middle-name-show-value"><?php echo PlotHelper::cropStr($this->my->getSocialFieldData('middle'), 30); ?></span>
                <button class="edit-pencil" onclick="changeField('middle-name');">
                    <svg viewBox="0 0 8.3 14.6" preserveAspectRatio="xMinYMin meet" class="edit">
                        <use xlink:href="#edit"></use>
                    </svg>
                </button>
            </a>
            <a href="#x" class="overlay" onclick="cancelField('middle-name');" id="patronymic-1"></a>
            <textarea  maxlength="<?php echo plotGlobalConfig::getVar('maxLenghtPatronymic')?>" class="popup middle-name-edit-value" id="patronymic" placeholder="отчество" autofocus/><?php echo $this->my->getSocialFieldData('middle'); ?></textarea>
            <button class="apply" onclick="middleNameSave();"></button>
            <button class="cancel" onclick="cancelField('middle-name');">&#215</button>
        </li>
        <li class="about-me" id="birthday-edit-block">
            <div class="datepicker">
                <label for="birth-date-1">Дата рождения:</label>
                <a href="#birth-date-1" onclick="changeField('birthday');" class="input-data">
                    <input type="text" id="my-birthday" name="my-birthday" value="<?php echo $this->my->birthday->day.'.'.$this->my->birthday->month.'.'.$this->my->birthday->year; ?>" />
                    <svg viewBox="0 0 26 24" preserveAspectRatio="xMinYMin meet"><use xlink:href="#datepicker"></use></svg>
                </a>
            </div>
        </li>
        <li class="about-me" id="country-edit-block">
            <div>
                <label for="state">Страна:</label>
            </div>
            <select id="country-edit-value">
                <?php foreach (PlotHelperCountries::getCountries() AS $code => $country) { ?>
                    <option <?php if ($this->my->getCountryCode() == $code) {echo 'selected="selected"';} ?> value="<?php echo $code; ?>">
                        <?php echo $country; ?>
                    </option>
                <?php } ?>
            </select>
        </li>
        <li class="about-me" id="state-edit-block">
            <div>
                <label for="region">Регион:</label>
                <a href="#region-1" onclick="changeField('state');" class="input-data">
                        <span class="state-show-value"><?php echo PlotHelper::cropStr($this->my->getSocialFieldData('state'), 30); ?></span>
                    <button class="edit-pencil" onclick="changeField('state');">
                        <svg viewBox="0 0 8.3 14.6" preserveAspectRatio="xMinYMin meet" class="edit">
                            <use xlink:href="#edit"></use>
                        </svg>
                    </button>
                </a>
            </div>
            <a href="#x" class="overlay" onclick="cancelField('state');" id="region-1"></a>
            <textarea  maxlength="<?php echo plotGlobalConfig::getVar('maxLenghtRegion')?>" class="popup state-edit-value" id="region" placeholder="регион" autofocus/><?php echo ($this->my->getSocialFieldData('state')) ? $this->my->getSocialFieldData('state') : ''; ?></textarea>
            <button class="apply" onclick="stateSave();"></button>
            <button class="cancel" onclick="cancelField('state');">&#215</button>
        </li>
        <li class="about-me" id="city-edit-block">
            <div>
                <label for="city">Город:</label>
                <a href="#city-1" onclick="changeField('city');" class="input-data">

                        <span class="city-show-value"><?php echo PlotHelper::cropStr($this->my->getSocialFieldData('city'), 30) ; ?></span>

                    <button class="edit-pencil" onclick="changeField('city');">
                        <svg viewBox="0 0 8.3 14.6" preserveAspectRatio="xMinYMin meet" class="edit">
                            <use xlink:href="#edit"></use>
                        </svg>
                    </button>
                </a>
            </div>
            <a href="#x" class="overlay" onclick="cancelField('city');" id="city-1"></a>
            <textarea  maxlength="<?php echo plotGlobalConfig::getVar('maxLenghtCity')?>" class="popup city-edit-value" id="city" placeholder="город" autofocus/><?php echo ($this->my->getSocialFieldData('city')) ? $this->my->getSocialFieldData('city') : ''; ?></textarea>
            <button class="apply" onclick="citySave();"></button>
            <button class="cancel" onclick="cancelField('city');">&#215</button>
        </li>
        <li class="about-me" id="email-edit-block">
            <div>
                <label for="e-mail-1">Email:</label>
                <a href="#e-mail-1" onclick="changeField('email');" class="input-data">
                    <span class="email-show-value"><?php echo PlotHelper::cropStr($this->my->email, 30); ?></span>
                    <button class="edit-pencil" onclick="changeField('email');">
                        <svg viewBox="0 0 8.3 14.6" preserveAspectRatio="xMinYMin meet" class="edit">
                            <use xlink:href="#edit"></use>
                        </svg>
                    </button>
                </a>
            </div>
            <a href="#x" class="overlay" onclick="cancelField('email');" id="e-mail-1"></a>
            <textarea  maxlength="<?php echo plotGlobalConfig::getVar('maxLenghtEmail')?>" class="popup email-edit-value" id="patronymic" placeholder="email" autofocus/><?php echo $this->my->email; ?></textarea>
            <button class="apply" onclick="emailSave();"></button>
            <button class="cancel" onclick="cancelField('email');">&#215</button>
        </li>
        <li class="about-me" id="phone-edit-block">
            <div>
                <label for="phone">Телефон:</label>
                <a href="#phone-1" onclick="changeField('phone');" class="input-data">
                    <span class="phone-show-value"><?php echo $this->my->getSocialFieldData('phone'); ?></span>
                    <button class="edit-pencil" onclick="changeField('phone');">
                        <svg viewBox="0 0 8.3 14.6" preserveAspectRatio="xMinYMin meet" class="edit">
                            <use xlink:href="#edit"></use>
                        </svg>
                    </button>
                </a>
            </div>
            <a href="#x" class="overlay" onclick="cancelField('phone');" id="phone-1"></a>
            <textarea maxlength="<?php echo plotGlobalConfig::getVar('maxLenghtPhone')?>" class="popup phone-edit-value" id="phone" placeholder="телефон" autofocus/><?php echo $this->my->getSocialFieldData('phone'); ?></textarea>
            <button class="apply" onclick="phoneSave();"></button>
            <button class="cancel" onclick="cancelField('phone');">&#215</button>
        </li>
        <li class="about-me" id="password-edit-block">
            <label for="password">Пароль:</label>
            <a href="#password-1" onclick="changeField('password');" class="input-data">
                <span class="password-show-value">********</span>
                <button class="edit-pencil" onclick="changeField('password');">
                    <svg viewBox="0 0 8.3 14.6" preserveAspectRatio="xMinYMin meet" class="edit">
                        <use xlink:href="#edit"></use>
                    </svg>
                </button>
            </a>
            <a href="#x" class="overlay" onclick="passwordCancel();" id="password-1"></a>
            <form class="popup password" onsubmit="return false;">
                <div>
                    <label for="password">Пароль:</label>
                    <input maxlength="<?php echo plotGlobalConfig::getVar('maxLenghtPassword')?>" type="password" class="password-edit-value" id="password" title="кликни, чтобы отредактировать" placeholder=""/>
                </div>
                <div>
                    <label for="repeat-password">Пароль еще раз:</label>
                    <input maxlength="<?php echo plotGlobalConfig::getVar('maxLenghtPassword')?>" type="password" class="password-edit-value-check" id="repeat-password" title="кликни, чтобы отредактировать" placeholder=""/>
                </div>
                <button class="apply" onclick="passwordSave();"></button>
                <button class="cancel" onclick="passwordCancel();">&#215</button>
            </form>
        </li>
        <li class="about-me" id="unsubscribe-edit-block">
            <div>
                <input type="checkbox" id="unsubscribe-email" onchange="unsubscribeEmailSave()" name="unsubscribe" <?php echo  ($this->my->getSocialFieldData('UNSUBSCRIBE')==NULL || (int)$this->my->getSocialFieldData('UNSUBSCRIBE')==1)?'checked':'';?> />
                <label for="unsubscribe-email">
                    <i><svg viewBox="0 0 32 32"><use xlink:href="#checkmark"></use></svg></i>
                    <?php echo  JText::_('COM_PLOT_GET_MSG_ON_EMAIL');?>
                </label>
            </div>
        </li>

    </div>
</ul>
</div>
</div>
<?php // </editor-fold> ?>
</div>
<div id="avatar-box"></div>
</div>
