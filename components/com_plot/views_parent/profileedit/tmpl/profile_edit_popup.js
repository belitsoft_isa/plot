// <editor-fold desc="VALIDATION" defaultstate="collapsed"> ?>
function isDate(date) {
    var re = /^\d{2}\.\d{2}\.\d{4}$/;
    if (date && date.match(re)) {
        return true;
    } else {
        return false;
    }
}
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="ABOUT ME FIELDS">
function hideOverlayAndCancelAllEdits() {
    jQuery('#about-me-container [id$="-edit-block"]').find('.popup').css('opacity', '0').css('visibility', 'hidden');
    jQuery('#about-me-container [id$="-edit-block"]').find('.apply, .cancel').css('display', 'none');
    jQuery('#about-me-container .overlay').css('opacity', '0').css('visibility', 'hidden');
}

function changeField(fieldName) {

    jQuery('#' + fieldName + '-edit-block').find('.popup').css('opacity', '1').css('visibility', 'visible');
    jQuery('#' + fieldName + '-edit-block').find('.apply, .cancel').css('display', 'block');
    jQuery('#' + fieldName + '-edit-block').find('.overlay').css('opacity', '1').css('visibility', 'visible');
    window.parent.SqueezeBox.asset.setStyles({
        position: 'fixed'
        //top: window.parent.document.getHeight()/2
    });
    // window.parent.document.getElementById('sbox-window').style.top=window.parent.document.getHeight()/2;
    window.parent.document.getElementById('sbox-window').style.position = 'fixed';
    var dpicer = window.document.getElementById('ui-datepicker-div'),
        dpicer_m = jQuery(dpicer).find('.ui-datepicker-month'),
        dpicer_y = jQuery(dpicer).find('.ui-datepicker-year'),
        mybirthday = jQuery('#my-birthday').val();
    jQuery(dpicer_m).on('change', function () {
        var mouth = parseInt(jQuery(this).val()) + 1;
        mybirthday = mybirthday.split('.');
        if (mouth < 10) {
            mouth = '0' + mouth;
        }
        jQuery('#my-birthday').val(mybirthday[0] + '.' + mouth + '.' + mybirthday[2]);
        birthdaySave(jQuery('#my-birthday').val());
    });

    jQuery(dpicer_y).on('change', function () {
        var years = parseInt(jQuery(this).val());
        mybirthday = mybirthday.split('.');
        jQuery('#my-birthday').val(mybirthday[0] + '.' + mybirthday[1] + '.' + years);
        birthdaySave(jQuery('#my-birthday').val());
    });
}

function cancelField(fieldName) {
    var nameBeforeEdit = jQuery('#about-me-container .' + fieldName + '-show-value').html();
    jQuery('#about-me-container .' + fieldName + '-edit-value').val(nameBeforeEdit);
    hideOverlayAndCancelAllEdits();
}

function lastNameSave() {
    var lastName = jQuery('#about-me-container .last-name-edit-value').val();
    if (!lastName) {
        alert('Введите фамилию');
        return false;
    }
    if (jQuery('#about-me-container #last-name-edit-block .last-name-show-value').html() !== jQuery('#about-me-container #last-name-edit-block .last-name-edit-value').val()) {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxChangeLastName', {lastName: lastName}, function (newLastName) {

            jQuery('#about-me-container .last-name-show-value').html(newLastName.length > 40 ? newLastName.substring(0, 40) + '...' : newLastName);
            jQuery('#about-me-container .last-name-edit-value').val(newLastName);
            hideOverlayAndCancelAllEdits();
        });
    } else {
        cancelField('last-name');
    }
}

function firstNameSave() {
    var firstName = jQuery('#about-me-container .first-name-edit-value').val();

    if (!firstName) {
        alert('Введите имя');
        return false;
    }
    if (jQuery('#about-me-container #first-name-edit-block .first-name-show-value').html() !== jQuery('#about-me-container #first-name-edit-block .first-name-edit-value').val()) {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxChangeFirstName', {firstName: firstName}, function (newFirstName) {

            jQuery('#about-me-container .first-name-show-value').html(newFirstName.length > 25 ? newFirstName.substring(0, 25) + '...' : newFirstName);
            jQuery('#about-me-container .first-name-edit-value').val(newFirstName);
            hideOverlayAndCancelAllEdits();
        });
    } else {
        cancelField('first-name');
    }
}

function middleNameSave() {
    var middleName = jQuery('#about-me-container .middle-name-edit-value').val();

    if (jQuery('#about-me-container #middle-name-edit-block .middle-name-show-value').html() !== jQuery('#about-me-container #middle-name-edit-block .middle-name-edit-value').val()) {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxChangeMiddleName', {middleName: middleName}, function (newMiddleName) {

            jQuery('#about-me-container .middle-name-show-value').html(newMiddleName.length > 30 ? newMiddleName.substring(0, 30) + '...' : newMiddleName);
            jQuery('#about-me-container .middle-name-edit-value').val(newMiddleName);
            hideOverlayAndCancelAllEdits();
        });
    } else {
        cancelField('middle-name');
    }
}

function birthdaySave(birthday) {
    if (!isDate(birthday)) {
        alert('Введите дату рождения в формате ДД.ММ.ГГГГ');
        return false;
    }

    jQuery.post('index.php?option=com_plot&task=profileedit.ajaxChangeBirthday', {birthday: birthday}, function (newBirthday) {
        jQuery('#about-me-container .birthday-show-value').html(newBirthday);
        jQuery('#about-me-container .birthday-edit-value').val(newBirthday);
        hideOverlayAndCancelAllEdits();
    });
}

function emailSave() {
    var email = jQuery('#about-me-container .email-edit-value').val();

    if (!isEmail(email)) {
        alert('Введите корректный email.');
        return false;
    }

    if (email === jQuery('#about-me-container .email-show-value').html()) {
        cancelField('email');
    } else {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxChangeEmail', {email: email}, function (newEmail) {
            if (newEmail) {

                jQuery('#about-me-container .email-show-value').html(newEmail.length > 35 ? newEmail.substring(0, 35) + '...' : newEmail);

                jQuery('#about-me-container .email-edit-value').val(newEmail);
                hideOverlayAndCancelAllEdits();
            } else {
                alert('Email занят. Введите другой email.');
            }
        });
    }
}

function phoneSave() {
    var phone = jQuery('#about-me-container .phone-edit-value').val();

    if (phone === jQuery('#about-me-container #phone-edit-block .phone-show-value').html()) {
        cancelField('phone');
    } else {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxChangePhone', {phone: phone}, function (newPhone) {
            jQuery('#about-me-container .phone-show-value').html(newPhone);
            jQuery('#about-me-container .phone-edit-value').val(newPhone);
            hideOverlayAndCancelAllEdits();
        });
    }
}

// <editor-fold defaultstate="collapsed" desc="Password Change">
function passwordCancel() {
    jQuery('#about-me-container .password-edit-value').val('');
    jQuery('#about-me-container .password-edit-value-check').val('');
    hideOverlayAndCancelAllEdits();
}

function passwordSave() {
    var password = jQuery('#about-me-container .password-edit-value').val();
    var passwordCheck = jQuery('#about-me-container .password-edit-value-check').val();

    if (!password || password !== passwordCheck) {
        alert('Пароли должны совпадать и не быть пустыми');
        return false;
    }

    jQuery.post('index.php?option=com_plot&task=profileedit.ajaxChangePassword', {password: password}, function (newPassword) {
        if (newPassword) {
            alert('Пароль успешно изменен');
            passwordCancel();
        }
    });
}
// </editor-fold>

function statusSave() {
    var status = jQuery('#about-me-container .status-edit-value').val();

    if (status === jQuery('#about-me-container #status-edit-block .status-show-value').html()) {
        cancelField('status');
    } else {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxChangeStatus', {status: status}, function (newStatus) {

            jQuery('#about-me-container .status-show-value').html(newStatus.length > 35 ? newStatus.substring(0, 35) + '...' : newStatus);
            jQuery('#about-me-container .status-edit-value').val(newStatus);
            hideOverlayAndCancelAllEdits();
        });
    }
}

function countrySave() {
    var country = jQuery('#about-me-container #country-edit-value').val();
    jQuery.post('index.php?option=com_plot&task=profileedit.ajaxChangeCountry', {country: country}, function (newCountry) {
        hideOverlayAndCancelAllEdits();
    });
}

function stateSave() {
    var state = jQuery('#about-me-container .state-edit-value').val();

    if (state === jQuery('#about-me-container #state-edit-block .state-show-value').html()) {
        cancelField('state');
    } else {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxChangeState', {state: state}, function (newState) {

            jQuery('#about-me-container .state-show-value').html(newState.length > 35 ? newState.substring(0, 35) + '...' : newState);
            jQuery('#about-me-container .state-edit-value').val(newState);
            hideOverlayAndCancelAllEdits();
        });
    }
}

function citySave() {
    var city = jQuery('#about-me-container .city-edit-value').val();

    if (city === jQuery('#about-me-container .city-show-value').html()) {
        cancelField('city');
    } else {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxChangeCity', {city: city}, function (newCity) {

            jQuery('#about-me-container .city-show-value').html(newCity.length > 35 ? newCity.substring(0, 35) + '...' : newCity);
            jQuery('#about-me-container .city-edit-value').val(newCity);
            hideOverlayAndCancelAllEdits();
        });
    }
}

function schoolSave() {
    var school = jQuery('#about-me-container .school-edit-value').val();

    if (school === jQuery('#about-me-container .school-show-value').html()) {
        cancelField('school');
    } else {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxChangeSchool', {school: school}, function (newSchool) {

            jQuery('#about-me-container .school-show-value').html(newSchool.length > 35 ? newSchool.substring(0, 35) + '...' : newSchool);
            jQuery('#about-me-container .school-edit-value').val(newSchool);
            hideOverlayAndCancelAllEdits();
        });
    }
}

function schoolAddressSave() {
    var schoolAddress = jQuery('#about-me-container .school-address-edit-value').val();

    if (schoolAddress === jQuery('#about-me-container .school-address-show-value').html()) {
        cancelField('school-address');
    } else {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxChangeSchoolAddress', {schoolAddress: schoolAddress}, function (newSchoolAddress) {

            jQuery('#about-me-container .school-address-show-value').html(newSchoolAddress.length > 35 ? newSchoolAddress.substring(0, 35) + '...' : newSchoolAddress);
            jQuery('#about-me-container .school-address-edit-value').val(newSchoolAddress);
            hideOverlayAndCancelAllEdits();
        });
    }
}

function aboutMeSave() {
    var aboutMe = jQuery('#about-me-container .about-me-edit-value').val();

    if (aboutMe === jQuery('#about-me-container .about-me-show-value').html()) {
        aboutMeCancel();
    } else {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxChangeAboutMe', {aboutMe: aboutMe}, function (newAboutMe) {

            jQuery('#about-me-container .about-me-show-value').html(newAboutMe.length > 450 ? newAboutMe.substring(0, 450) + '...' : newAboutMe);
            jQuery('#about-me-container .about-me-edit-value').val(newAboutMe);
            hideOverlayAndCancelAllEdits();
        });
    }
}


// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="TAGS">
function deleteTag(tagId) {
    jQuery.post('index.php?option=com_plot&task=profileedit.ajaxDeleteTag', {tag_id: tagId}, function (response) {
        var data = jQuery.parseJSON(response),
            msg = data.msg,
            filter_tags = document.getElementById("friends-filter-tags"),
            i = 0,
            filter_tags_count = filter_tags.options.length;
        if (msg) {
            jQuery('.my-interest-' + tagId).remove();
            updateAddTagsOptions();
            jQuery('#plot-list-my-tags').jScrollPane();
            for (i = 0; i < filter_tags_count; i++) {
                if (filter_tags.options[i].value == tagId) {
                    filter_tags.remove(i);
                    break;
                }
            }
            jQuery('#friends-filter-tags').selectmenu('refresh');
            updateListsTags();
        } else {
            jQuery('#sbox-content', window.parent.document).css('opacity', '0');
            jQuery('#about-me-container').html('');
            window.parent.SqueezeBox.setContent('string', 'Чтобы удалить интерес нужно сначала удалить всех друзей, привязанных к этому интересу.');
            window.parent.SqueezeBox.resize({x: 300, y: 150}, true);
        }
    });

}

function tagSmileyChange(tagId, smileValue) {

    jQuery('.my-interest-' + tagId).find('input.plot-reason').removeClass('so-so').removeClass('dont-like').removeClass('like');

    jQuery.post('index.php?option=com_plot&task=profileedit.ajaxTagSmileySave', {tag_id: tagId, tag_smiley: smileValue});
    jQuery('label[for="el_' + tagId + '-dont-like"] svg').css('fill', '#ccc');
    jQuery('label[for="el_' + tagId + '-so-so"] svg').css('fill', '#ccc');
    jQuery('label[for="el_' + tagId + '-like"] svg').css('fill', '#ccc');

    switch (smileValue) {
        case '1':
            jQuery('.js-smile-' + tagId + smileValue).css('fill', '#eb3d00');
            jQuery('.my-interest-' + tagId).find('input.plot-reason').addClass('dont-like');
            jQuery('.my-interest-' + tagId).find('input').css('color', '#eb3d00');
            break;
        case '2':
            jQuery('.js-smile-' + tagId + smileValue).css('fill', '#f48000');
            jQuery('.my-interest-' + tagId).find('input.plot-reason').addClass('so-so');
            jQuery('.my-interest-' + tagId).find('input').css('color', '#f48000');
            break;
        case '3':
            jQuery('.js-smile-' + tagId + smileValue).css('fill', '#009049');
            jQuery('.my-interest-' + tagId).find('input.plot-reason').addClass('like');
            jQuery('.my-interest-' + tagId).find('input').css('color', '#009049');
            break;
    }
}

function tagTitleSave(tagId) {
    var tag_title = jQuery('.my-interest-' + tagId + ' .plot-reason').val();
    if (tag_title != '') {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxTagTitleSave', {tag_id: tagId, tag_title: tag_title}, function (response) {
            if (response) {
                jQuery('.my-interest-' + tagId + ' .like-before-change').val(tag_title);
            }
        });
    } else {
        alert('Введите данные');
    }
}

function tagTitleCancel(tagId) {
    jQuery('.my-interest-' + tagId + ' .like').val(jQuery('.my-interest-' + tagId + ' .like-before-change').val());
}

function updateAddTagsOptions() {
    jQuery.post('index.php?option=com_plot&task=profileedit.ajaxGetNotAddedTags', function (response) {
        var data = jQuery.parseJSON(response);
        var options = '<option value="0">-- Выберите Ваше увлечение --</option>';
        jQuery.each(data.tags, function (index, tag) {
            options += '<option value="' + tag.id + '">' + tag.title + '</option>';
        });
        jQuery('#plot-tags').html(options);
        jQuery('#plot-tags').selectmenu('destroy');
        jQuery('#plot-tags').selectmenu({
            open: function (event, ui) {
                if (!jQuery('#plot-tags-menu .jspContainer').length) {
                    jQuery('#plot-tags-menu').jScrollPane();
                }
            }
        });
        
    });
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="FRIENDS">
function rejectFriendRequest(friendId) {
    jQuery.post('index.php?option=com_plot&task=profileedit.ajaxRejectFriendRequest', {friendId: friendId}, function (jsonResponce) {
        responce = jQuery.parseJSON(jsonResponce);
        if (!responce.error) {
            var friendElement = jQuery('#about-me-container .myfriend-' + responce.friendId);
            friendElement.fadeOut(500, function () {
                friendElement.remove();
            });
            decreaseFriendRequestsShowedValue();
            decreaseFriendRequestsShowedValueNearAvatar();
        } else {
            alert('Error');
        }
    });
}

function approveFriendRequest(friendId) {
    jQuery.post('index.php?option=com_plot&task=profileedit.ajaxApproveFriendRequest', {friendId: friendId}, function (jsonResponce) {
        responce = jQuery.parseJSON(jsonResponce);
        if (!responce.error) {
            var newActions = '<div><a href="index.php?option=com_plot&task=conversations.newMessage&id=' + friendId + '" rel="{size: {x: 744, y: 525}, handler:\'iframe\'}" class="button green h30">' +
                '<span><svg class="letter" viewBox="0 0 40.7 29.5" preserveAspectRatio="xMinYMin meet"><use xlink:href="#letter"/></svg>Написать мне письмо</span>' +
                '</a><button class="close" onclick="removeFriend(\'' + friendId + '\');">×</button></div>';
            var actionsElement = jQuery('#about-me-container .myfriend-' + responce.friendId + ' .actions');
            actionsElement.fadeOut(500, function () {
                actionsElement.replaceWith(newActions);
                actionsElement.fadeIn(1000);
            });
            decreaseFriendRequestsShowedValue();
            jQuery('#about-me-container .myfriend-' + responce.friendId).animate({backgroundColor: "#FFFFFF"}, 500);
            decreaseFriendRequestsShowedValueNearAvatar();
        } else {
            alert('Error');
        }
    });
}

function decreaseFriendRequestsShowedValue() {
    var countNewFriendsRequestsElement = jQuery('#about-me-container .count-friend-requests');
    var countNewFriendsRequestsValue = countNewFriendsRequestsElement.html();

    if (countNewFriendsRequestsValue > 1) {
        countNewFriendsRequestsElement.fadeOut(500, function () {
            countNewFriendsRequestsElement.html(countNewFriendsRequestsValue - 1);
            countNewFriendsRequestsElement.fadeIn(500);
        });
    } else {
        countNewFriendsRequestsElement.fadeOut(500, function () {
            countNewFriendsRequestsElement.remove();
        });
    }
}

function decreaseFriendRequestsShowedValueNearAvatar(){
    var countNewFriendsRequestsElement=jQuery(window.parent.document.getElementsByClassName('friends')),
        countNewFriendsRequestsValue = countNewFriendsRequestsElement.html();

    if (countNewFriendsRequestsValue > 1) {
        countNewFriendsRequestsElement.fadeOut(500, function(){
            countNewFriendsRequestsElement.html(countNewFriendsRequestsValue - 1);
            countNewFriendsRequestsElement.fadeIn(500);
        });
    } else {
        countNewFriendsRequestsElement.fadeOut(500, function(){
            countNewFriendsRequestsElement.remove();
        });
    }
}

function removeFriend(friendId) {
    jQuery.post('index.php?option=com_plot&task=profileedit.ajaxRemoveFriend', {friendId: friendId}, function (jsonResponce) {
        responce = jQuery.parseJSON(jsonResponce);
        if (!responce.error) {
            var friendElement = jQuery('#about-me-container .myfriend-' + responce.friendId);
            friendElement.fadeOut(500, function () {
                friendElement.remove();
            });
        } else {
            alert('Error');
        }
    });
}

function foreignRemoveFriend(friendId){
    jQuery.post('index.php?option=com_plot&task=profileedit.ajaxRemoveFriend', {friendId: friendId}, function (jsonResponce) {
        responce = jQuery.parseJSON(jsonResponce);
        if (!responce.error) {
            jQuery('#foreign-friend-button').html('<a class="add hover-shadow modal" href="#" onclick="friend();"><svg viewBox="0 0 31.7 30.8" preserveAspectRatio="xMinYMin meet" class="friend"><use xlink:href="#friend"></use></svg>Дружить</a>');
        } else {
            alert('Error');
        }
    });
}
// </editor-fold>

function redirectParentWindow(href) {
    window.top.location = href;
}


jQuery(document).ready(function () {
    
    jQuery('body').addClass('popup-style parent-profile');
    jQuery("#tabs").tabs({
        activate: function (event, ui) {
            jQuery('.scroll-pane').jScrollPane();
            jQuery('#friends-filter-tags').selectmenu('refresh');
        }
    });

    jQuery("#country-edit-value").selectmenu({
        open: function (event, ui) {
            jQuery('#country-edit-value-menu').jScrollPane();
        },
        change: function () {
            countrySave();
        }
    });
    
    jQuery('#plot-tags').selectmenu({
        open: function (event, ui) {
            if (!jQuery('#plot-tags-menu .jspContainer').length) {
                jQuery('#plot-tags-menu').jScrollPane();
            }
        }
    });

    jQuery("#plot-tags, #country-edit-value").on("selectmenuclose");
    filterMyFriendsByTags();

    document.getElementById("svg-sprite").innerHTML = SVG_SPRITE;

    // tags change functions
    jQuery('#about-me-container').on('focusin', '.interest-explain .like', function () {
        jQuery(this).parent().find('.apply, .cancel').css('display', 'inline-block');
        jQuery(this).parent().find('.edit-pencil').css('display', 'none');
        jQuery(this).parent().find('input[type="text"]')
    });
    jQuery('#about-me-container').on('focusout', '.interest-explain .like', function () {
        var applyCancelButtons = jQuery(this).parent().find('.apply, .cancel');
        var editPencilButton = jQuery(this).parent().find('.edit-pencil');
        var tagTitleInput = jQuery(this).parent().find('input[type="text"]');

        var hideTagEditInterval = setInterval(function () {
            applyCancelButtons.css('display', 'none');
            editPencilButton.css('display', 'inline-block');
            tagTitleInput
            jQuery('#about-me-container').focus();
            clearInterval(hideTagEditInterval);
        }, 10);
    });


});

jQuery(function () {
    jQuery("#my-birthday").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        dateFormat: "dd.mm.yy",
        nextText: ">>",
        prevText: "<<",
        selectOtherMonths: true,
        firstDay: 1,
        showOtherMonths: true,
        monthNamesShort: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        onSelect: function (selectedDate) {
            birthdaySave(selectedDate);
        }
    });
});

function insertActivityItem(tag_id, checkedSmile, tagDesc) {
    var str = '',
        progress = jQuery(window.parent.document.getElementById('events-jcarousel')).jcarousel({'animation': 'slow','wrap': 'circular'});
    if (tag_id) {
        jQuery.post(
            'index.php?option=com_plot&task=profileedit.getUserStream', {tag_id: tag_id},
            function (response) {
                var data = jQuery.parseJSON(response);
                if (data.items) {
                    progress.on('jcarousel:reload jcarousel:create', function () {
                        var element = jQuery(this),
                            width = element.innerWidth() * 0.235,//23.5% ширина li в случае, если выводится по 4 элемента
                            margin = element.innerWidth() * 0.02;//2% значение margin в случае
                        element.jcarousel('items').css('width', width + 'px');
                        element.jcarousel('items').css('margin-right', margin + 'px');
                        jQuery(element).find('li').each(function () {
                            jQuery(this).css('width', width + 'px');
                            jQuery(this).css('margin-right', margin + 'px');
                        });
                    })
                        .jcarousel({
                            wrap: 'circular'
                        });
                    jcaruselPaginations();
                    progress.html('<ul class="parent-profile">' + data.items + '</ul>');

                   //progress.jcarousel('reload');
                    jQuery(window.parent.document.getElementById('actyvity-jcarousel-pagination')).jcarouselPagination({
                        item: function(page) {
                            return '<a href="#' + page + '"></a>';
                        }
                    });
                    jQuery(window.parent.document.getElementById('actyvity-jcarousel-pagination')).jcarouselPagination('reloadCarouselItems');
                    progress.jcarousel('reload');

                }

            }
        );

    }

}

function jcaruselPaginations() {
    jQuery(window.parent.document.getElementsByClassName('.jcarousel-control-prev'))
        .on('jcarouselcontrol:active', function () {
            jQuery(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function () {
            jQuery(this).addClass('inactive');
        })
        .jcarouselControl({
            target: '-=3'
        });
    jQuery('.jcarousel-control-next')
        .on('jcarouselcontrol:active', function () {
            $(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function () {
            jQuery(this).addClass('inactive');
        })
        .jcarouselControl({
            target: '+=3'
        });

    jQuery(window.parent.document.getElementsByClassName('.jcarousel-pagination-meetings'))
        .on('jcarouselpagination:active', 'a', function () {
            jQuery(this).addClass('active');
        })
        .on('jcarouselpagination:inactive', 'a', function () {
            jQuery(this).removeClass('active');
        });
}


function unsubscribeEmailSave() {
    var chek = jQuery('#unsubscribe-email').is(':checked'),
        unsubscribe;
    if (chek) {
        unsubscribe = 1;
    } else {
        unsubscribe = 0;
    }
    jQuery.post('index.php?option=com_plot&task=profileedit.ajaxChangeUnsubscribeEmail', {unsubscribe: unsubscribe}, function (response) {


    });

}