<?php
defined('_JEXEC') or die;
jimport('joomla.html.html.bootstrap');
$proportion=plotGlobalConfig::getVar('avatarCropWidthMin')/plotGlobalConfig::getVar('avatarCropHeightMin');
?>

    <link type="text/css" href="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate().'/css/style.css'; ?>" rel="stylesheet">
    <link type="text/css" href="<?php echo JUri::root(); ?>components/com_plot/libraries/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet">
    <script src="<?php echo JUri::root(); ?>media/jui/js/jquery.min.js" type="text/javascript"></script>

    <script src="<?php echo JUri::root(); ?>components/com_plot/libraries/jcrop/js/jquery.Jcrop.min.js" type="text/javascript"></script>

    <script type="text/javascript">

        function showCoords(coords)
        {
            jQuery('input[name=x]').val(coords.x);
            jQuery('input[name=y]').val(coords.y);
            jQuery('input[name=x2]').val(coords.x2);
            jQuery('input[name=y2]').val(coords.y2);
            jQuery('input[name=w]').val(coords.w);
            jQuery('input[name=h]').val(coords.h);
        };

        function saveCroppedAvatar()
        {
            jQuery('input[name=img-width]').val(jQuery('.jcrop-holder img').width());
            jQuery('input[name=img-height]').val(jQuery('.jcrop-holder img').height());

            jQuery.post('<?php echo JRoute::_('index.php?option=com_plot&task=profileedit.ajaxSaveCroppedAvatar');?>',{data: jQuery('#crop-image').serialize()}, function(data){
               if (data.status) {
                    window.parent.document.getElementById('plot-my-avatar').src=data.image;
                    goAboutMe();
                } else {
                    alert('<?php echo JText::_('COM_PLOT_ERROR');?>');
                    window.parent.SqueezeBox.close();
                }
            });
        }

        function goAboutMe()
        {
            window.parent.SqueezeBox.open('<?php echo JRoute::_('index.php?option=com_plot&task=profileedit.ajaxGetEditProfilePopupHtml#tab-3');?>', {size: {x:744, y:525}, handler: 'iframe',  iframeOptions: {scrolling: 'no'}});
        }

        jQuery(document).ready(function(){
            jQuery('.jcrop').Jcrop({
                minSize:   [ <?php echo plotGlobalConfig::getVar('avatarCropWidthMin'); ?>, <?php echo plotGlobalConfig::getVar('avatarCropHeightMin'); ?> ],
                setSelect:   [ 0, 0, <?php echo plotGlobalConfig::getVar('avatarCropWidthMin'); ?>, <?php echo plotGlobalConfig::getVar('avatarCropHeightMin'); ?> ],
                allowSelect: true,
                onChange: showCoords,
                onSelect: showCoords,
                aspectRatio: <?php echo $proportion; ?>
            });

            jQuery('input[name=img-width]').val(jQuery('.jcrop-holder img').width());
            jQuery('input[name=img-height]').val(jQuery('.jcrop-holder img').height());

            jQuery('body').addClass('popup-style');
        });
    </script>


    <div id="avatar-crop">
        <h6>Создай аватар с помощью рамки:</h6>
        <img class="jcrop" src="<?php echo $this->originalPhotoUrl;?>" />

        <form method="POST" id="crop-image" action="<?php echo JRoute::_('index.php?option=com_plot&task=profileedit.ajaxSaveCroppedAvatar');?>" >
            <input class="add hover-shadow" type="button" value="Создать аватар" onclick="saveCroppedAvatar();" />
            <input class="no-friend hover-shadow" type="button" value="Отмена" onclick="goAboutMe();" />

            <input type="hidden" name="x" value="" />
            <input type="hidden" name="y" value="" />
            <input type="hidden" name="x2" value="" />
            <input type="hidden" name="y2" value="" />
            <input type="hidden" name="w" value="" />
            <input type="hidden" name="h" value="" />

            <input type="hidden" name="img-width" value="" />
            <input type="hidden" name="img-height" value="" />

            <input type="hidden" name="id" value="<?php echo $this->photoId; ?>" />
        </form>
    </div>
