<?php
defined('_JEXEC') or die;
$id = JRequest::getInt('id', 0);
if ($id) {
    $this->my = plotUser::factory($id);
} else {
    $this->my = plotUser::factory();
}

$mytags = PlotHelper::getMyTags($this->my->id);

?>

<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />

<link type="text/css" href="<?php echo JUri::root(); ?>templates/plot/css/style.css" rel="stylesheet" />

<script src="<?php echo JUri::root(); ?>media/jui/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>templates/plot/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>components/com_plot/views_parent/profileedit/tmpl/profile_edit_popup.js"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/plot/js/svgsprite.js"></script>
<script src="<?php echo JUri::root(); ?>templates/plot/js/jplot.js" type="text/javascript"></script>


<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">
    function changeAvatar() {
        jQuery('#upload-avatar-form').submit();
    }
    function removeAvatar() {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxRemoveAvatar', function () {
            jQuery('#about-me-container .avatar').attr('src', '<?php echo JUri::root(); ?>media/com_easysocial/defaults/avatars/users/square.png');
        });
    }
    function toggleUserTag(tagId) {
        var tagInput = jQuery('#tag' + tagId);
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxToggleTag', {
            tagId: tagId,
            checked: tagInput.is(':checked')
        }, function (error) {
            if (error) {
                alert('tags limit: <?php echo plotGlobalConfig::getVar('maxTagsForUser'); ?>');
                tagInput.attr('checked', false);
            }
        });
    }

    function complain() {

        SqueezeBox.initialize({
            size: {x: 300, y: 150}
        });

        SqueezeBox.open('<?php echo JRoute::_('index.php?option=com_plot&task=profileedit.ajaxComplain&userId='.(int)$this->user->id); ?>', {handler: 'iframe'})
        SqueezeBox.resize({x: 300, y: 150});
    }

    function friend() {
        var overlay = '<div class="avatar-overlay" id="sbox-overlay2" aria-hidden="false" tabindex="-1"></div>',
            text_msg='<?php echo JText::_("COM_PLOT_DO_NOT_HAVE_JOINT_TAGS"); ?>',
            str = '<div id="sbox-window2"  style="width: 400px; height: 400px; top:10%; left: 10%;"role="dialog" aria-hidden="false" class="shadow avatar-sbox">' +
                '<div class="send-friend-request parent-profile">' +
                '<label>Выберите ваш общий интерес</label>'+
                    '<form onsubmit="return false;">'+
                '<input type="hidden" value="<?php echo $this->user->id; ?>" name="id" id="user_id">' +
                <?php
                    if (!empty($mytags)) {
                    ?>
                '<select name="tag_id" id="tag-id">' +
                '<option value=""><?php echo JText::_('COM_PLOT_SELECT_TAG'); ?></option>' +
                <?php
                    foreach ($mytags AS $tag) {
                ?>
                '<option value="<?php echo $tag->id; ?>"><?php echo $tag->title; ?></option>' +
                <?php
        }
     ?>
                '</select><br>' +
                <?php
                } else {
                   ?>
                    text_msg+
        <?php
                }

                ?>
                '</form>'+
                '<img src="<?php echo JURI::base() . "templates/" . JFactory::getApplication()->getTemplate(); ?>/img/ajax-loader.gif" alt="" class="pre-loader" style="display:none"/>'+
                '<button onclick="sendRequest();" class="button accept hover-shadow" >' +
                '<svg viewBox="0 0 31.7 30.8" preserveAspectRatio="xMidYMid meet" class="friend">' +
                '<use xlink:href="#friend"></use>' +
                '</svg>' +
                '<span>' +
                '<?php echo JText::_('COM_PLOT_ADD_AS_FRIEND') ?>' +
                '</span>' +

                '</button>' +
                '</div>' +
                '<a id="sbox-btn-close2" href="#" onclick="window.parent.document.getElementById(\'sbox-window\').style.position=\'fixed\'; jQuery(this.parentNode.previousSibling).remove(); jQuery(this.parentNode).remove();" role="button" aria-controls="sbox-window"></a></div>';

        jQuery("#avatar-box").after(str);
        jQuery("#avatar-box").after(overlay);
        jQuery( "#tag-id").selectmenu({
            appendTo:'.send-friend-request',
            open: function( event, ui ) { jQuery('#tag-id-menu').jScrollPane(); }
        });
    }

    function sendRequest() {
        jQuery('.pre-loader').show();
        var tag_id = jQuery(document.getElementById('tag-id')).val(),
            id = '<?php echo $this->user->id; ?>',
            wrapp=window.parent.document.getElementById('friend-button-wrapp');
        console.log('view_parent/profileedit')
        if (tag_id == '') {
            jQuery('.pre-loader').hide();
            answer(0,'Не выбран интерес');


        } else {
            jQuery.post('index.php?option=com_plot&task=profile.request', {
                tag_id: tag_id,
                id: id
            }, function (response) {
                jQuery('.pre-loader').hide();
               jQuery('#friend-button-wrapp').html('<span id="foreign-friend-button"><svg viewBox="0 0 34.5 30.8" preserveAspectRatio="xMinYMin meet" class="wait-for-friend"><use xlink:href="#wait-for-friend"></use></svg>запрос дружбы отправлен</span>');
                //jQuery(wrapp).html('<span><svg viewBox="0 0 34.5 30.8" preserveAspectRatio="xMinYMin meet" class="wait-for-friend"><use xlink:href="#wait-for-friend"></use></svg>запрос дружбы отправлен</span>');
                answer(response.status, response.message);
            });
        }
    }

    function filterMyFriendsByTags() {
        var tagId = jQuery('#friends-filter-tags').val()

        jQuery('.loading-friends-list').html('Загрузка...');
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxFilterMyFriendsByTags', {
            userId: <?php echo $this->my->id; ?>,
            tagId: tagId
        }, function (jsonResponce) {
            var data = jQuery.parseJSON(jsonResponce);
            jQuery('#tab-1').html(data.html);
            jPlot.profile.showPopupDataAndRefreshSelectsAndScrollpanes();
        });
    }
    jQuery(document).ready(function () {
        jQuery("#friends-filter-tags").selectmenu({
            open: function (event, ui) {
                jQuery('#friends-filter-tags-menu').jScrollPane();
            },
            change: function (event, ui) {
                filterMyFriendsByTags('<?php echo $this->my->id; ?>');
            }
        });
    });
    function addTag() {
        var tag_id = jQuery("#plot-tags option:selected").val();
        if (tag_id) {
            var text = jQuery("#plot-tags option:selected").text(),
                count_tags = jQuery('#plot-list-my-tags .my-interest').length;

            if (count_tags >=<?php echo plotGlobalConfig::getVar('maxTagsForUser'); ?>) {
                alert("<?php echo JText::_('COM_PLOT_YOU_HAVE_MAX_COUNT_TAGS')?>");
            } else {
                jQuery.post('index.php?option=com_plot&task=profileedit.ajaxAddTag', {tag_id: tag_id}, function (response) {
                    var str = '';
                    if (response) {
                        str = '' +
                        '<li class="my-interest my-interest-' + tag_id + '">' +
                        '<span>' + text + '</span> ' +
                        '<div>' +
                        '<form class="smile-interest" onsubmit="return false;">' +
                        '<fieldset>' +
                        '<input type="radio" id="el_' + tag_id + '-dont-like" name="el_' + tag_id + '" value="dont-like" checked="none"/>' +
                        '<label for="el_' + tag_id + '-dont-like" onclick="tagSmileyChange(\'' + tag_id + '\',\'1\');" >' +
                        '<svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" style="fill: #ccc;" class="smile-dont-like radio-icon js-smile-' + tag_id + '1" >' +
                        '<use xlink:href="#smile-dont-like"></use>' +
                        '</svg>' +
                        '</label> ' +
                        '<input type="radio" id="el_' + tag_id + '-so-so" name="el_' + tag_id + '" value="so-so" selected="true" />' +
                        '<label for="el_' + tag_id + '-so-so" onclick="tagSmileyChange(\'' + tag_id + '\',\'2\');">' +
                        '<svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-so-so radio-icon js-smile-' + tag_id + '2" >' +
                        '<use xlink:href="#smile-so-so"></use>' +
                        '</svg>' +
                        '</label> ' +
                        '<input type="radio" id="el_' + tag_id + '-like" name="el_' + tag_id + '" value="like"/>' +
                        '<label for="el_' + tag_id + '-like" onclick="tagSmileyChange(\'' + tag_id + '\',\'3\');">' +
                        '<svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-like radio-icon js-smile-' + tag_id + '3">' +
                        '<use xlink:href="#smile-like"></use>' +
                        '</svg>' +
                        '</label> ' +
                        '<p class="not-checked" style="display: block;" >не выбрано</p>' +
                        '<div class="interest-explain" style="display: none;">' +
                        '<input class="like" style="width: 165px;" type="text" title="кликни, чтобы отредактировать" placeholder="напиши почему" value="" />' +
                        '<input class="like-before-change" type="hidden" value="" />' +
                        '<button class="edit-pencil" style="visibility: visible;">' +
                        '<svg viewBox="0 0 8.3 14.6" preserveAspectRatio="xMinYMin meet"class="edit">' +
                        '<use xlink:href="#edit"></use>' +
                        '</svg>' +
                        '</button>' +
                        '<button class="apply" onclick="tagTitleSave(\'' + tag_id + '\');" style="display: inline-block;"></button>' +
                        '<button class="cancel" onclick="tagTitleCancel(\'' + tag_id + '\');" style="display: inline-block;">&#215</button>' +
                        '</div>' +
                        '</fieldset>' +
                        '</form>' +
                        '</div>' +
                        '<button class="delete" onclick="deleteTag(\'' + tag_id + '\');">&#215</button>' +
                        '</li>';
                    }
                    jQuery('#plot-list-my-tags .interests-blank-area').before(str);
                    updateAddTagsOptions(jQuery('#plot-tags'));
                });
            }
        }
    }

    function createComplain() {
        var overlay = '<div class="avatar-overlay" id="sbox-overlay2" aria-hidden="false" tabindex="-1"></div>',
            str = '<div id="sbox-window2"  style="width: 500px; height: 350px; top:10%; left: 10%;"role="dialog" aria-hidden="false" class="shadow avatar-sbox">' +
                '<div id="complain-form">' +
                '<fieldset>' +
                '<input type="hidden" name="uid" id="uid" value="<?php echo  (int)$this->user->id; ?>">' +
                '<label for="message">Пожаловаться на пользователя <?php echo plotUser::factory((int)$this->user->id)->name; ?></label>' +
                '<textarea id="message" name="message" placeholder="Почему Вы хотите пожаловаться на этого пользователя?"></textarea>' +
                '<button class="button brown hover-shadow" onclick="sendComplain(<?php echo (int)$this->user->id; ?>)">' +
                '<span>' +
                '<svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-dont-like"><use xlink:href="#smile-dont-like"></use></svg>' +
                'Отправить сообщение' +
                '</span>' +
                '</button>' +
                '</fieldset></div>' +
                '<a id="sbox-btn-close2" href="#" onclick="window.parent.document.getElementById(\'sbox-window\').style.position=\'fixed\'; jQuery(this.parentNode.previousSibling).remove(); jQuery(this.parentNode).remove();" role="button" aria-controls="sbox-window"></a></div>';

        jQuery("#avatar-box").after(str);
        jQuery("#avatar-box").after(overlay);
    }

    function sendComplain(uid) {
        var message = jQuery('#message').val();
        if (message == '') {
            alert('Заполните все поля');
        } else {
            jQuery.post('index.php?option=com_plot&task=profile.sendReport', {
                uid: uid,
                message: message
            }, function (response) {
                answer(response.status, response.message);
            });
        }
    }

    function answer(status, message){
        if (status) {

            window.parent.document.getElementById('sbox-window').style.position = 'fixed';
            document.getElementById('sbox-btn-close2').parentNode.previousSibling.remove();
            document.getElementById('sbox-btn-close2').parentNode.remove();
            var overlay = '<div class="avatar-overlay" id="sbox-overlay2" tabindex="-1"></div>',
                str = '<div id="sbox-window2" role="dialog" aria-hidden="false" class="shadow avatar-sbox">' +
                    '<div id="sbox-content2" class="sbox-content-adopt" style="opacity: 1;"><div id="enqueued-message">' + message +
                    '</div></div><a id="sbox-btn-close2" href="#" onclick="window.parent.document.getElementById(\'sbox-window\').style.position=\'fixed\';jQuery(this.parentNode.previousSibling).remove(); jQuery(this.parentNode).remove();" role="button" aria-controls="sbox-window"></a></div>';

            jQuery("#avatar-box").after(str);
            jQuery("#avatar-box").after(overlay);
        } else {
            window.parent.document.getElementById('sbox-window').style.position = 'fixed';
            document.getElementById('sbox-btn-close2').parentNode.previousSibling.remove();
            document.getElementById('sbox-btn-close2').parentNode.remove();
            var overlay = '<div class="avatar-overlay" id="sbox-overlay2" tabindex="-1"></div>',
                str = '<div id="sbox-window2" role="dialog" aria-hidden="false" class="shadow avatar-sbox">' +
                    '<div id="sbox-content2" class="sbox-content-adopt" style="opacity: 1;"><div id="enqueued-message">' + message +
                    '</div></div><a id="sbox-btn-close2" href="#" onclick="window.parent.document.getElementById(\'sbox-window\').style.position=\'fixed\';jQuery(this.parentNode.previousSibling).remove(); jQuery(this.parentNode).remove();" role="button" aria-controls="sbox-window"></a></div>';

            jQuery("#avatar-box").after(str);
            jQuery("#avatar-box").after(overlay);
        }
    }
</script>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="CSS">?>
<?php # </editor-fold> ?>

<div id="svg-sprite"></div>
<div id="about-me-container" class="wrap about-friend">

<div id="tabs" class="user-info parent-profile">
<ul class="popup-header parent-profile">
    <li><a href="#tab-1">Все мои друзья</a></li>
    <li><a href="#tab-2">Мои интересы</a></li>
    <li><a href="#tab-3">Обо мне</a></li>
</ul>

<?php # <editor-fold defaultstate="collapsed" desc="Friends Tab">?>
<div id="tab-1">
    <div id="system-message-container-popup"></div>
    <div>
        <?php if ($this->friends || $this->oldfriends) { ?>
            <p style="padding: 10px 20px 0 20px;">
                <select id="friends-filter-tags" onchange="filterMyFriendsByTags();" style="margin-bottom: 5px;">
                    <option value="0">-- Все увлечения --</option>
                    <?php foreach ($this->tags AS $tag) {

                        ?>
                        <option value="<?php echo $tag->id; ?>"><?php echo $tag->title; ?></option>
                    <?php } ?>
                </select>
                <span class="loading-friends-list"></span>
            </p>

        <?php } ?>
        <ul class="scroll-pane">
            <?php foreach ($this->friends AS $friend) { ?>
                <li class="myfriends myfriend-<?php echo $friend->actor_id; ?>">
                    <a onclick="redirectParentWindow('<?php echo JRoute::_('index.php?option=com_plot&view=profile&id=' . $friend->actor_id); ?>')"
                       href="javascript:void(0);" class="name">
                        <img src="<?php echo plotUser::factory($friend->actor_id)->getSquareAvatarUrl(); ?>"
                             alt="<?php echo Foundry::user((int)$friend->actor_id)->name; ?>"/>
                        <span><?php echo PlotHelper::cropStr(Foundry::user((int)$friend->actor_id)->name, plotGlobalConfig::getvar('profileEditPopupFriendsMaxNameLength')); ?></span>
                    </a>

                    <div class="actions">
                        <a class="add hover-shadow" onclick="approveFriendRequest('<?php echo $friend->actor_id; ?>');">
                            <svg viewBox="0 0 31.7 30.8" preserveAspectRatio="xMinYMin meet" class="friend">
                                <use xlink:href="#friend"></use>
                            </svg>
                            Дружить со мной
                        </a>
                        <a class="no-friend hover-shadow"
                           onclick="rejectFriendRequest('<?php echo $friend->actor_id; ?>');">
                            <svg viewBox="0 0 46.3 45.6" preserveAspectRatio="xMinYMin meet">
                                <use xlink:href="#no-friends"></use>
                            </svg>
                            Не дружить
                        </a>
                    </div>
                </li>
            <?php } ?>

            <?php foreach ($this->oldfriends as $friend) { ?>
                <li class="myfriends myfriend-<?php echo $friend->id; ?>">
                    <a href="#" class="name">
                        <img src="<?php echo plotUser::factory($friend->id)->getSquareAvatarUrl(); ?>"
                             alt="<?php echo $friend->name; ?>"/>
                        <span><?php echo $friend->name; ?></span>
                    </a>

                </li>
            <?php } ?>

            <?php if (!$this->friends && !$this->oldfriends) { ?>
                <li class="myfriends">
                    <p class="no-items">У вас пока нет друзей.</p>
                </li>
            <?php } ?>

        </ul>
    </div>
</div>
<?php // </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="Interests Tab">?>
<div id="tab-2">
    <ul id="plot-list-my-tags" class="scroll-pane friends-list">
        <?php foreach ($this->user->tags AS $tag) {
            ?>
            <li class="my-interest my-interest-<?php echo $tag->id; ?>">
                <span><?php echo $tag->title; ?></span>

                <div>
                    <form class="smile-interest" onsubmit="return false;">
                        <fieldset>
                            <?php if (isset($tag->smiley)) { ?>
                                <?php if ($tag->smiley == 1) { ?>
                                    <label for="el_<?php echo $tag->id; ?>-dont-like">
                                        <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet"
                                             class="smile-dont-like radio-icon js-smile-<?php echo $tag->id . '1'; ?>" <?php if (isset($tag->smiley) && $tag->smiley == 1) {
                                            echo 'style="fill: #eb3d00;"';
                                        }; ?> >
                                            <use xlink:href="#smile-dont-like"></use>
                                        </svg>
                                    </label>
                                <?php } else if ($tag->smiley == 2) { ?>
                                    <label for="el_<?php echo $tag->id; ?>-so-so">
                                        <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet"
                                             class="smile-so-so radio-icon js-smile-<?php echo $tag->id . '2'; ?>" <?php if (isset($tag->smiley) && $tag->smiley == 2) {
                                            echo 'style="fill: #f48000;"';
                                        }; ?>>
                                            <use xlink:href="#smile-so-so"></use>
                                        </svg>
                                    </label>
                                <?php } else if ($tag->smiley == 3) { ?>
                                    <label for="el_<?php echo $tag->id; ?>-like">
                                        <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet"
                                             class="smile-like radio-icon js-smile-<?php echo $tag->id . '3'; ?>" <?php if (isset($tag->smiley) && $tag->smiley == 3) {
                                            echo 'style="fill: #009049;"';
                                        }; ?>>
                                            <use xlink:href="#smile-like"></use>
                                        </svg>
                                    </label>
                                <?php } else { ?>

                                    <label for="el_<?php echo $tag->id; ?>-so-so">
                                        <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet"
                                             class="smile-so-so radio-icon js-smile-<?php echo $tag->id . '2'; ?>">
                                            <use xlink:href="#smile-so-so"></use>
                                        </svg>
                                    </label>
                                <?php } ?>



                            <?php } ?>

                            <?php $tag->tagTitle=isset($tag->tagTitle) ? $tag->tagTitle:$tag->title; ?>
                            <div class="interest-explain <?php if ((int)$tag->smiley && trim($tag->tagTitle)) {
                                echo '' . $tag->smileyInputClass . ' plot-reason';
                            } else {
                                echo 'not-checked plot-reason';
                            }; ?>">
                                <?php echo $tag->tagTitle ?>
                            </div>
                        </fieldset>
                    </form>
                </div>

            </li>
        <?php } ?>

        <?php if(empty($this->user->tags)){ ?>
            <p class="no-items"><?php echo JText::_('COM_PLOT_USER_DOES_NOT_HAVE_TAGS'); ?></p>
        <?php }?>
    </ul>

</div>
<?php // </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="AboutMe Tab">?>
<div id="tab-3">
    <div class="tab-3-wrapper scroll-pane parent-profile">
        <div class="resume">
            <p class="circle-img">
                <img class="avatar" src="<?php echo $this->user->getSquareAvatarUrl(); ?>" alt=""/>
            </p>

            <?php if (plotUser::factory()->id) { ?>
                <a class="add hover-shadow modal"
                   href="<?php echo JRoute::_('index.php?option=com_plot&task=conversations.newMessage&id=' . (int)$this->user->id); ?>"
                   rel="{size: {x: 744, y: 525}, handler:'iframe'}">
                    <svg viewBox="0 0 40.7 29.5" preserveAspectRatio="xMinYMin meet" class="letter">
                        <use xlink:href="#letter"></use>
                    </svg>
                    Написать мне
                </a>
                <span id="friend-button-wrapp">
                <?php $friend_status = PlotHelper::isFriend();
                switch ($friend_status) {
                    case 'pending':
                        echo '<span id="foreign-friend-button">
                                    <svg viewBox="0 0 34.5 30.8" preserveAspectRatio="xMinYMin meet" class="wait-for-friend"><use xlink:href="#wait-for-friend"></use></svg>
                                    запрос дружбы отправлен
                                </span>';
                        break;
                    case 'unfriend':
                        ?>
<!--                        <span id="foreign-friend-button">-->
                            <a id="foreign-friend-button" class="add hover-shadow modal" href="#" onclick="friend();">
                            <svg viewBox="0 0 31.7 30.8" preserveAspectRatio="xMinYMin meet" class="friend">
                                <use xlink:href="#friend"></use>
                            </svg>
                            <?php echo JText::_('COM_PLOT_ACCEPT'); ?>
                        </a>
<!--                        </span>-->
                        <?php
                        break;
                    case 'friend':
                        ?>

<!--                        <span >-->
                            <a id="foreign-friend-button" class="add hover-shadow" href="#"
                           onclick="foreignRemoveFriend('<?php echo $this->user->id; ?>');">
                            <svg viewBox="0 0 46.3 45.6" preserveAspectRatio="xMinYMin meet" class="no-friendship">
                                <use xlink:href="#no-friends"></use>
                            </svg>
                            <?php echo JText::_('COM_PLOT_REJECT'); ?>
                        </a>
<!--                        </span>-->

                        <?php
                        break;
                }
                ?>
            </span>
                <a class="hover-shadow" href="javascript:void(0)" onclick="createComplain();">
                    <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-dont-like">
                        <use xlink:href="#smile-dont-like"></use>
                    </svg>
                    Пожаловаться
                </a>
            <?php } ?>

        </div>
        <ul>
            <div class="foreign-children my-children">
                <p>Мои дети:</p>
                <?php
                foreach ($this->my->getChildrenIds() AS $childId) {
                    ?>
                    <div>
                        <div class="child-image">
                            <img class="avatar" src="<?php echo plotUser::factory($childId)->getSquareAvatarUrl(); ?>"
                                 alt="<?php echo plotUser::factory($childId)->name; ?>"/>
                        </div>
                        <p class="my-child-name">
                            <?php echo plotUser::factory($childId)->name; ?><br/>
                                    <span><?php
                                        $birthday = $this->getMyBirthdayObject(plotUser::factory($childId));
                                        if ($birthday->year != '1900') {
                                            $age = PlotHelper::calcutateAge($birthday->year . '-' . $birthday->month . '-' . $birthday->day);
                                            echo (int)$age . ' ' . PlotHelper::getWordEnding($age, 'год', 'года', 'лет');
                                        }
                                        ?></span>
                        </p>

                    </div>
                <?php
                }
                ?>

            </div>
            <div>
                <div class="foreign-name">
                    <li class="about-me" id="last-name-edit-block">
                        <a href="#surname-1" class="last-name-show-value">
                                <?php echo PlotHelper::cropStr($this->user->getSocialFieldData('last'), 30) ; ?>
                        </a>
                    </li>
                    <li class="about-me" id="first-name-edit-block">
                        <a href="#name-1" class="first-name-show-value">

                                <?php echo PlotHelper::cropStr($this->user->getSocialFieldData('first'), 30); ?>

                        </a>
                    </li>
                    <li class="about-me" id="middle-name-edit-block">
                        <a href="#patronymic-1" class="middle-name-show-value">
                                <?php echo PlotHelper::cropStr($this->user->getSocialFieldData('middle'), 30); ?>
                        </a>
                    </li>
                </div>
                <div class="foreign-country-city">
                    <li class="about-me" id="country-edit-block">
                        <a href="#state-1" class="input-data country-show-value">
                            <?php echo PlotHelperCountries::getCountryName($this->user->getCountryCode()); ?>
                        </a>
                    </li>
                    <li class="about-me" id="state-edit-block">
                        <a href="#region-1" class="input-data state-show-value">
                                <?php echo PlotHelper::cropStr($this->user->getSocialFieldData('state'), 30); ?>
                        </a>
                    </li>
                    <li class="about-me" id="city-edit-block">
                        <a href="#city-1" class="input-data city-show-value">

                                <?php echo PlotHelper::cropStr($this->user->getSocialFieldData('city'), 30); ?>

                        </a>
                    </li>
                </div>
                <li class="about-me" id="birthday-edit-block">
                    <label for="birth-date-1">Дата рождения:</label>
                    <?php
                    if ($this->user->birthday->day . '.' . $this->user->birthday->month . '.' . $this->user->birthday->year != '01.01.1900') {
                        ?>
                        <a href="#birth-date-1"
                           class="input-data birthday-show-value"><?php echo $this->user->birthday->day . '.' . $this->user->birthday->month . '.' . $this->user->birthday->year; ?></a>
                    <?php
                    }
                    ?>
                </li>
                <li class="about-me" id="about-me-edit-block">
                    <label for="resume">Обо мне:</label>
                    <a href="#resume-1" class="input-data about-me-show-value">
                        <?php echo PlotHelper::cropStr($this->user->getSocialFieldData('ABOUT_ME'), 200); ?>
                    </a>
                    <textarea class="popup about-me-edit-value" id="resume"
                              autofocus/><?php echo $this->user->getSocialFieldData('ABOUT_ME'); ?></textarea>
                </li>
            </div>
        </ul>
    </div>
</div>
<?php // </editor-fold> ?>
</div>

<div id="avatar-box"></div>
</div>
<input type="hidden" value="<?php echo $this->my->id; ?>" id="user-id"/>

<div id="fb-root"></div>

<script>
    window.fbAsyncInit = function () {
        // for js api initialization
        FB.init({
            appId: '{283742071785556}',
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true, // parse XFBML
            oauth: true // enable OAuth 2.0
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        console.log(js)
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=1234567891234567";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<script>!function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    }(document, 'script', 'twitter-wjs');</script>

<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://apis.google.com/js/platform.js" async defer></script>

<!-- Place this tag where you want the share button to render. -->
