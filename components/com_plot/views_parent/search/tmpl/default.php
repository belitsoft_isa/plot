<?php
defined('_JEXEC') or die('Restricted access');
?>

<script type="text/javascript" src="<?php echo JUri::root() . 'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate() . '/js/jquery.jscrollpane.min.js'; ?>"></script>

<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.min.js"></script>
<script src="<?php echo JUri::root(); ?>/components/com_plot/libraries/scrolling_ajax_pagination/javascript.js"></script>
<script type="text/javascript">

jQuery(document).ready(function (jQuery) {

    jQuery("#plot-search").val('<?php echo $this->search; ?>');

    function removeSearchActiveClassTab() {
        jQuery('#children-btn').removeClass('ui-tabs-active');
        jQuery('#parent-search-children').css({'display': 'none'});
        jQuery('#adults-btn').removeClass('ui-tabs-active');
        jQuery('#parent-search-adults').css({'display': 'none'});
        jQuery('#books-btn').removeClass('ui-tabs-active');
        jQuery('#parent-search-books').css({'display': 'none'});
        jQuery('#courses-btn').removeClass('ui-tabs-active');
        jQuery('#parent-search-courses').css({'display': 'none'});
        jQuery('#events-btn').removeClass('ui-tabs-active');
        jQuery('#parent-search-meetings').css({'display': 'none'});
    }

    function setActiveTab(tab, search_area) {
        jQuery('#' + tab).addClass('ui-tabs-active');
        jQuery('#' + search_area).css({'display': 'block'});
    }

    //load children
    function setChildrenAxaxScrollPagination() {
        var widthItem = jQuery('#search-all-children li.found-item:first-child').width(),
            heightItem = jQuery('#search-all-children li.found-item:first-child').height(),
            HeightScreen = parseInt(screen.height);
        if (!widthItem) {
            if (HeightScreen <= 768) {
                widthItem = 236;
                heightItem = 72;
            } else if (HeightScreen > 768 && HeightScreen <= 980) {
                widthItem = 294;
                heightItem = 86;
            } else if (HeightScreen > 980) {
                widthItem = 307;
                heightItem = 101;
            }
        }
        removeSearchActiveClassTab();
        setActiveTab('children-btn', 'parent-search-children');

        var ajaxScrollData = {
            search: '<?php echo $this->search;?>',
            screenHeight: parseInt(screen.height) - parseInt(jQuery('div.parent-profile').height()),
            screenWidth: jQuery('#parent-search-children').outerWidth(),
            itemHeight: heightItem,
            itemWidth: widthItem
        };

        jQuery(window).unbind('scroll');

        jQuery('#search-all-children').scrollPagination({
            nop: <?php echo plotGlobalConfig::getVar('limitSearchChildrenItems'); ?>,
            error: '',
            delay: 20,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=search.ajaxChildrenLoadMore'); ?>',
            appendDataTo: 'search-all-children',
            userData: ajaxScrollData,
            afterLoad: {
                afterLoadAction: function () {
                    jPlotUp.Arrow.initialize('positionCameras');
                }
            }
        });

        jQuery(window).scroll(function(){
            jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
        });
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    }

    //load adults
    function setAdultsAxaxScrollPagination() {
        var widthItem = jQuery('#search-all-adults li.found-item:first-child').width(),
            heightItem = jQuery('#search-all-adults li.found-item:first-child').height(),
            HeightScreen = parseInt(screen.height);
        if (!widthItem) {
            if (HeightScreen <= 768) {
                widthItem = 236;
                heightItem = 72;
            } else if (HeightScreen > 768 && HeightScreen <= 980) {
                widthItem = 294;
                heightItem = 86;
            } else if (HeightScreen > 980) {
                widthItem = 307;
                heightItem = 101;
            }
        }
        removeSearchActiveClassTab();
        setActiveTab('adults-btn', 'parent-search-adults');
        var ajaxScrollData = {
            search: '<?php echo $this->search;?>',
            screenHeight: parseInt(screen.height) - parseInt(jQuery('div.parent-profile').height()),
            screenWidth: jQuery('#parent-search-adults').outerWidth(),
            itemHeight: heightItem,
            itemWidth: widthItem
        };

        jQuery(window).unbind('scroll');

        jQuery('#search-all-adults').scrollPagination({
            nop: <?php echo plotGlobalConfig::getVar('limitSearchAdultsItems'); ?>,
            error: '',
            delay: 20,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=search.ajaxAdultsLoadMore'); ?>',
            appendDataTo: 'search-all-adults',
            userData: ajaxScrollData,
            afterLoad: {
                afterLoadAction: function () {
                    jPlotUp.Arrow.initialize('positionCameras');
                }
            }
        });

        jQuery(window).scroll(function(){
            jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
        });
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    }

    //load books
    function setBooksAxaxScrollPagination() {
        removeSearchActiveClassTab();
        setActiveTab('books-btn', 'parent-search-books');
        var ajaxBooksScrollData = {
            search: '<?php echo $this->search;?>'
        };
        jQuery(window).unbind('scroll');
        jQuery('#search-all-books').scrollPagination({
            nop: <?php echo plotGlobalConfig::getVar('limitSearchBooksItems'); ?>,
            error: '',
            delay: 20,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=search.ajaxBooksLoadMore'); ?>',
            appendDataTo: 'search-all-books',
            userData: ajaxBooksScrollData,
            afterLoad: {
                afterLoadAction: function () {
                    jPlotUp.Arrow.initialize('positionCameras');
                }
            }
        });
        jQuery(window).scroll(function(){
            jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
        });
    }

    //load courses
    function setCoursesAxaxScrollPagination() {
        removeSearchActiveClassTab();
        setActiveTab('courses-btn', 'parent-search-courses');
        jQuery(window).unbind('scroll');
        jQuery('#search-all-courses').scrollPagination({
            nop: <?php echo plotGlobalConfig::getVar('limitSearchCoursesItems'); ?>,
            error: '',
            delay: 20,
            scroll: true,
            postUrl: 'index.php?option=com_plot&task=search.ajaxcoursesloadmore',
            appendDataTo: 'search-all-courses',
            userData: { search: '<?php echo $this->search;?>',
                afterLoad: {
                    afterLoadAction: function () {
                        jPlotUp.Arrow.initialize('positionCameras');
                    }
                } }
        });
        jQuery(window).scroll(function(){
            jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
        });
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    }

    //load events
    function setEventsAxaxScrollPagination() {
        removeSearchActiveClassTab();
        setActiveTab('events-btn', 'parent-search-meetings');

        var ajaxScrollEventsData = {
            search: '<?php echo $this->search;?>'
        };

        jQuery(window).unbind('scroll');

        jQuery('#search-all-events').scrollPagination({
            nop: <?php echo plotGlobalConfig::getVar('limitSearchMeetingsItems'); ?>,
            error: '',
            delay: 20,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=search.ajaxEventsLoadMore'); ?>',
            appendDataTo: 'search-all-events',
            userData: ajaxScrollEventsData,
            afterLoad: {
                afterLoadAction: function () {
                    jPlotUp.Arrow.initialize('positionCameras');
                }
            }
        });

        jQuery(window).scroll(function(){
            jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
        });
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    }

    jQuery('#courses-btn').on('click', function () {
        jQuery('#search-all-courses').html('');
        setCoursesAxaxScrollPagination();
        jQuery('.main-wrap-overflow').trigger('scroll');
    });
    jQuery('#events-btn').on('click', function () {
        jQuery('#search-all-events').html('');
        setEventsAxaxScrollPagination();
        jQuery('.main-wrap-overflow').trigger('scroll');

    });
    jQuery('#children-btn').on('click', function () {
        jQuery('#search-all-children').html('');
        setChildrenAxaxScrollPagination();
        jQuery('.main-wrap-overflow').trigger('scroll');

    });
    jQuery('#adults-btn').on('click', function () {
        jQuery('#search-all-adults').html('');
        setAdultsAxaxScrollPagination();
        jQuery('.main-wrap-overflow').trigger('scroll');

    });
    jQuery('#books-btn').on('click', function () {
        jQuery('#search-all-books').html('');
        setBooksAxaxScrollPagination();
        jQuery('.main-wrap-overflow').trigger('scroll');

    });

    <?php if($this->active){

    switch ($this->active) {
case 'books':
   ?>
    removeSearchActiveClassTab();
    jQuery('#books-btn').addClass('ui-tabs-active');
    jQuery('#parent-search-books').css({'display': 'block'});
    jQuery('#books-btn').trigger('click');
    <?php
    break;
case 'adults':
   ?>
    removeSearchActiveClassTab();
    jQuery('#adults-btn').addClass('ui-tabs-active');
    jQuery('#parent-search-adults').css({'display': 'block'});
    jQuery('#adults-btn').trigger('click');
    <?php
    break;
case 'children':
    ?>
    removeSearchActiveClassTab();
    jQuery('#children-btn').addClass('ui-tabs-active');
    jQuery('#parent-search-children').css({'display': 'block'});
    jQuery('#children-btn').trigger('click');
    <?php
    break;
    case 'courses':
    ?>
    removeSearchActiveClassTab();
    jQuery('#courses-btn').addClass('ui-tabs-active');
    jQuery('#parent-search-courses').css({'display': 'block'});
    jQuery('#courses-btn').trigger('click');
    <?php
    break;
    case 'events':
    ?>
    removeSearchActiveClassTab();
    setActiveTab('events-btn', 'parent-search-meetings');
    jQuery('#events-btn').trigger('click');
    <?php
    break;
}
    }else{ ?>
    removeSearchActiveClassTab();
    setActiveTab('events-btn', 'parent-search-meetings');
    jQuery('#children-btn').trigger('click');
    <?php } ?>

    jQuery('.main-wrap-overflow').trigger('scroll');

    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });

});

document.addEventListener("touchmove", function(){
    jQuery('.main-wrap-overflow').trigger('scroll');
    console.log('ScrollStart');
}, false);
</script>

<main class="parent-profile">
    <div class="top parent-profile">
        <?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_header')); ?>
        <?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_top_menu')); ?>
    </div>
    <div class="wrap main-wrap parent-profile">
        <div class="add-left aside-left parent-profile"></div>
        <div class="add-right aside-right parent-profile"></div>
        <section class="parent-search">
            <div id="parent-search">
                <ul class="parent-courses-think-tabs">
                    <li id="children-btn"><a href="#parent-search-children">Дети</a></li>
                    <li id="adults-btn"><a href="#parent-search-adults">Взрослые</a></li>
                    <li id="books-btn"><a href="#parent-search-books">Книги</a></li>
                    <li id="courses-btn"><a href="#parent-search-courses">Курсы</a></li>
                    <li id="events-btn"><a href="#parent-search-meetings">Встречи</a></li>
                </ul>
                <div id="loading-bar"><img src="<?php echo $this->baseurl ?>/templates/plot/img/pre-loader-1.gif" /></div>
                <div id="parent-search-children" class="jcarousel-wrapper parent-profile">
                    <p class="search-result">Результат поиска раздела "Дети" - по запросу "<?php echo $this->search; ?>"
                        -
                        Найдено <?php echo PlotHelper::declension((int)$this->items['countChildren'], array('результат', 'результата', 'результатов')); ?> </p>
                    <ul class="parent-profile no-jcarousel search-children" id="search-all-children">

                    </ul>

                </div>
                <div id="parent-search-adults" class="jcarousel-wrapper parent-profile">
                    <p class="search-result">Результат поиска раздела "Взрослые" - по запросу
                        "<?php echo $this->search; ?>" -
                        Найдено <?php echo PlotHelper::declension((int)$this->items['countAdults'], array('результат', 'результата', 'результатов')); ?></p>
                    <ul class="parent-profile no-jcarousel search-adults" id="search-all-adults">

                    </ul>
                </div>
                <div id="parent-search-books" class="jcarousel-wrapper parent-profile">
                    <p class="search-result">Результат поиска раздела "Книги" - по запросу "<?php echo $this->search; ?>
                        " -
                        Найдено <?php echo PlotHelper::declension((int)$this->items['countBooks'], array('результат', 'результата', 'результатов')); ?></p>
                    <ul class="parent-profile no-jcarousel search-books" id="search-all-books">

                    </ul>

                </div>
                <div id="parent-search-courses" class="jcarousel-wrapper parent-profile">
                    <p class="search-result">Результат поиска раздела "Курсы" - по запросу "<?php echo $this->search; ?>
                        " -
                        Найдено <?php echo PlotHelper::declension((int)$this->items['countCourses'], array('результат', 'результата', 'результатов')); ?></p>
                    <ul class="parent-profile no-jcarousel search-courses" id="search-all-courses">

                    </ul>

                </div>
                <div id="parent-search-meetings" class="jcarousel-wrapper parent-profile">
                    <p class="search-result">Результат поиска раздела "Встречи" - по запросу
                        "<?php echo $this->search; ?>" -
                        Найдено <?php echo PlotHelper::declension((int)$this->items['countEvents'], array('результат', 'результата', 'результатов')); ?></p>
                    <ul class="parent-profile no-jcarousel search-meetings" id="search-all-events">

                    </ul>

                    <script type="text/javascript">

                        jQuery(document).ready(function () {
                            jQuery('.parent-menu li.active').removeClass('active');

                            jQuery('[data-toggle="tooltip"]').tooltip();
                        });


                    </script>

                    <script src="<?php echo JURI::base(); ?>components/com_plot/assets/js/plot.js"></script>

                </div>
            </div>
        </section>
    </div>
</main>

