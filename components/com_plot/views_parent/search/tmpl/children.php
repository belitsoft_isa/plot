<?php
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
?>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('[data-toggle="tooltip"]').tooltip();
    });
</script>

<?php
if ($this->items) {
    foreach ($this->items as $i => $item) {
        ?>
        <div>
            <span><?php echo $item->name; ?></span><br>
            <?php if (plotUser::factory($item->id)->avatars && plotUser::factory($item->id)->avatars['large']) { ?>
                <img src="<?php echo JUri::root().'media/com_easysocial/avatars/users/'.(int) $item->id.'/'.plotUser::factory($item->id)->avatars['large']; ?>">
            <?php } else { ?>
                <img src="<?php echo JUri::root().'media/com_easysocial/defaults/avatars/users/child_medium.jpg'; ?>">
            <?php } ?>

        </div>
        <?php
    }
}