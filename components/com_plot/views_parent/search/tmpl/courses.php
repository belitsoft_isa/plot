<?php
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.tooltip');


?>

<script type="text/javascript">

    jQuery(document).ready(function () {


        jQuery('[data-toggle="tooltip"]').tooltip();
    });
</script>


<form method="post" name="adminForm" id="adminForm" action="<?php echo 'index.php?option=com_plot&view=portfolios'; ?>"
      method="post" autocomplete="off">


    <input type="hidden" name="task" value=""/>
    <input type="hidden" name="boxchecked" value="0"/>

    <?php echo JHtml::_('form.token'); ?>

    <?php
    if ($this->items) {
        foreach ($this->items as $i => $item) {
        ?>
            <div>
                <span><?php echo $item->course_name; ?></span><br>
                <?php echo $item->course_description; ?><br>

            </div>
    <?php
        }
    }
    ?>

</form>

