<?php
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.tooltip');


?>
<style>
    body {
        color: #000000;
    }
</style>
<script type="text/javascript">

    jQuery(document).ready(function () {


        jQuery('[data-toggle="tooltip"]').tooltip();
    });


    function rejectEvent(event_id) {
        jQuery('#plot-buttons-event-' + event_id).html('<button onclick="App.Event.Subscribe(this);return false;" data-id="' + event_id + '"><?php echo  JText::_("COM_PLOT_SUBSCRIBE_ON_EVENT");?></button>');
    }

    function addEvent(event_id) {
        jQuery('#plot-buttons-event-' + event_id).html(' <button   onclick="App.Event.RemoveSubscribe(this); return false;" data-id="' + event_id + '"><?php echo  JText::_('COM_PLOT_REMOVE_SUBSCRIBE_EVENT');?></button>');
    }

    function removeEvent(event_id) {
        jQuery('#plot-event-id-' + event_id).remove();
    }
</script>
<script src="<?php echo JURI::base(); ?>components/com_plot/assets/js/plot.js"></script>

<?php
if ($this->items) {
    foreach ($this->items as $i => $item) {
        ?>
        <div class="plot-events" id="plot-event-id-<?php echo $item->id; ?>">
            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=event&id=' . $item->id); ?>"><?php echo $item->title; ?></a><br>
            start <?php echo JHtml::date($item->start_date, 'g-m-Y Y:m'); ?>
            end <?php echo JHtml::date($item->end_date, 'g-m-Y Y:m'); ?><br>
            <img style="width: 100px;"
                 src="<?php echo JURI::root() . 'images/com_plot/events/' . $item->user_id . '/' . $item->img; ?>"><br>
            author: <a
                href="<?php echo JRoute::_('index.php?option=com_plot&view=profile&id=' . $item->user_id); ?>"><?php echo plotUser::factory($item->user_id)->name; ?></a>
            <br>
            количество участноков: <?php echo PlotHelper::countEventSubscription($item->id); ?><br>

            <div id="plot-buttons-event-<?php echo $item->id; ?>">
                <?php
                if ((int)PlotHelper::chechEventSubscription($item->id)) {
                    if ((int)PlotHelper::isOwnerEvent($item->id)) {
                        ?>
                        <button onclick="App.Event.DeleteEvent(this); return false;"
                                data-id="<?php echo $item->id; ?>"><?php echo JText::_('COM_PLOT_REMOVE_EVENT'); ?></button>
                    <?php
                    } else {
                        ?>
                        <button onclick="App.Event.RemoveSubscribe(this); return false;"
                                data-id="<?php echo $item->id; ?>"><?php echo JText::_('COM_PLOT_REMOVE_SUBSCRIBE_EVENT'); ?></button>
                    <?php
                    }
                    ?>

                <?php
                } else {
                    ?>
                    <button onclick="App.Event.Subscribe(this);return false;"
                            data-id="<?php echo $item->id; ?>"><?php echo JText::_('COM_PLOT_SUBSCRIBE_ON_EVENT'); ?></button>

                <?php
                }
                ?>
            </div>

        </div>
    <?php
    }
}
?>

