<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewSearch extends PlotViewParentLegacy
{

    function display($tpl = null)
    {
        $this->state = $this->get('State');
        $app = JFactory::getApplication();
        $this->templateUrl = JURI::base().'templates/'.$app->getTemplate();
        $this->search = urldecode(JRequest::getString('search', ''));
        $jinput = JFactory::getApplication()->input;
        $layout = $jinput->get('layout', '');
        if (!$layout) {
            $this->items = $this->get('AllResults');
        } else {
            $this->items = $this->get('Items');
        }

        $this->active = '';

        JRequest::getInt('books') ? $this->active = 'books' : $this->active;
        JRequest::getInt('adults') ? $this->active = 'adults' : $this->active;
        JRequest::getInt('children') ? $this->active = 'children' : $this->active;
        JRequest::getInt('courses') ? $this->active = 'courses' : $this->active;
        JRequest::getInt('events') ? $this->active = 'events' : $this->active;

        $this->pagination = $this->get('Pagination');
        parent::display($tpl);
        $this->setDocument();
    }

    protected function setDocument()
    {
        $document = JFactory::getDocument();
    }

}
