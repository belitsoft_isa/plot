<?php
defined('_JEXEC') or die;
$count_all_books = count($this->programs);
$i = 0;

if(!$this->countItems){
    ?>
    <script>
        jQuery('.es-content').html('<?php echo  JText::_("COM_PLOT_PROGRAMS_NOT_FOUND");?>');
    </script>
    <?php
}else{
if ($this->programs) { ?>

            <ul class="plot-program-entity">
                <?php foreach ($this->programs AS $key => $item) { ?>
                <li>
                    <a href="<?php echo $item->link; ?>">
                        <figure><img src="<?php echo $item->thumbnailUrl; ?>" alt="<?php echo $item->title; ?>"/></figure>
                    </a>
                    <p><?php echo  $item->c_title;?></p>
                    <?php
                    if((int)$item->book_author){
                        echo plotUser::factory($item->book_author)->name;
                    }else{
                        echo $item->c_author;
                    }
                    ?>
                    <div>
                        <span><?php echo $item->readed; ?></span>
                        <span><?php echo $item->essay_approved; ?></span>
                    </div>
                    <div class="elem-descr">';
                        <?php echo $item->c_pub_descr; ?>
                        </div>
                </li>
                <?php } ?>
            </ul>

<?php }
} ?>
