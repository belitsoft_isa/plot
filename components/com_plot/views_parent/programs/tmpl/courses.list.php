<?php
defined('_JEXEC') or die;
$count_all_books = count($this->programs);
$i = 0;

if(!$this->countItems){
    ?>
    <script>
        jQuery('.es-content').html('<?php echo  JText::_("COM_PLOT_PROGRAMS_NOT_FOUND");?>');
    </script>
    <?php
}else{
if ($this->programs) { ?>

            <ul class="plot-program-entity" >
                <?php foreach ($this->programs AS $key => $item) { ?>
                <li>
                    <a href="<?php echo $item->link; ?>">
                        <figure><img src="<?php echo $item->image; ?>" alt="<?php echo $item->course_name; ?>"/></figure>
                    </a>
                    <?php echo plotUser::factory($item->owner_id)->name; ?>
<p><?php echo $item->finished; ?></p>
                    <p><?php echo $item->video_approved; ?></p>
                    <div class="elem-descr">';
                        <?php echo $item->course_description; ?>
                        </div>
                </li>
                <?php } ?>
            </ul>

<?php }
} ?>
