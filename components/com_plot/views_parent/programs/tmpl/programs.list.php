<?php
defined('_JEXEC') or die;
$count_all_books = count($this->programs);
$i = 0;

if(!$this->countItems){
    ?>
    <script>
        jQuery('.es-content-wrap').html('<?php echo  JText::_("COM_PLOT_PROGRAMS_NOT_FOUND");?>');
    </script>
    <?php
}else{
if ($this->programs) { ?>

            <ul class="plot-program-entity">
                <?php foreach ($this->programs AS $key => $item) { ?>
                <li>
                    <a href="<?php echo $item->link; ?>">
                        <figure><img src="<?php echo $item->img; ?>" alt="<?php echo $item->title; ?>"/></figure>
                    </a>
                    <p><?php echo  $item->ages; ?></li></p>
                    <p><?php echo $item->level_title; ?></p>
                    <div class="plot-programs-progress" data-progress="<?php echo $item->percent; ?>"></div>

                </li>
                <?php } ?>
            </ul>

<?php }
} ?>
