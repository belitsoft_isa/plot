<?php
defined('_JEXEC') or die;


$url = JFactory::getURI();
$request_url = $url->toString();

?>

<script type="text/javascript" src="<?php echo JUri::root().'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate().'/js/jquery.jscrollpane.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate().'/js/jquery.mousewheel.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root().'components/com_plot/assets/js/plot.js'; ?>"></script>
<script src="<?php echo  JURI::base().'components/com_plot'; ?>/libraries/scrolling_ajax_pagination/javascript.js"></script>

<?php # <editor-fold defaultstate="collapsed" desc="JS">  ?>
<script type="text/javascript">
    function showFilteredAndSortedBooks() {
        setAxaxScrollPagination();

        var tagsIds = jQuery("[id^=filter-books-tag]:checked").map(function(){
            return jQuery(this).val();
        }).get();

        jQuery('#books-list').html('<div class="p5_20"><?php echo JText::_('COM_PLOT_LOADING...'); ?></div>');

        jQuery.post(
            '<?php echo JRoute::_('index.php?option=com_plot&task=publications.ajaxGetFilteredAndSortedBooks'); ?>',
            {
                ageId: jQuery('#childlib-alter').val(),
                tagsIds: tagsIds,
                categoryId: jQuery('#childlib-category').val(),
                myCoursesOnly: jQuery('[name="my_books"]:checked').val(),//jQuery('#filter-books-my').prop('checked') ? 1 : 0,
                sort: jQuery('#childlib-filter').val()
            },
            function(response) {
                var data = jQuery.parseJSON(response);

                jQuery('.info-board .info-bottom .count-books').html('Найдено <span>'+jPlot.helper.getNumEnding(data.countBooks, ['книга', 'книги', 'книг'])+'</span>');
                jQuery('#books-list').show();
                jQuery('#books-list').html(data.renderedBooks);
                if(data.countBooks==0){
                    //jQuery('#books-list').html('Книг не найдено');
                    jQuery('#books-list').hide();
                }
            }
        );
    }
    function checkOnlyMyInterestsCheckboxes() {
        jQuery("[id^=filter-books-tag]").prop('checked', false);
        jQuery.post(
            '<?php echo JRoute::_('index.php?option=com_plot&task=publications.ajaxGetMyTags'); ?>',
            function(response) {
                var data = jQuery.parseJSON(response),
                    i= 0,
                    data_count=data.length;
                jQuery("[id^=filter-books-tag]").prop('checked', false);
                for (i; i<data_count; i++) {
                    jQuery('#filter-books-tag'+data[i].id).prop('checked', true);
                }
                showFilteredAndSortedBooks();
            }
        );
    }
    jQuery(document).ready(function(){
        showFilteredAndSortedBooks();
    });
    function setAxaxScrollPagination() {
        var tagsIds = jQuery("[id^=filter-books-tag]:checked").map(function(){
            return jQuery(this).val();
        }).get();
        var ajaxScrollData = {
            ageId: jQuery('#childlib-alter').val(),
            tagsIds: tagsIds,
            categoryId: jQuery('#childlib-category').val(),
            myCoursesOnly:  jQuery('[name="my_books"]:checked').val(),//jQuery('#filter-books-my').prop('checked') ? 1 : 0,
            sort: jQuery('#childlib-filter').val()
        };

        jQuery(window).unbind('scroll');

        jQuery('.wrap.child-library').scrollPagination({
            nop: <?php echo plotGlobalConfig::getVar('booksResultsShowFirstCountChild'); ?>,
            offset: <?php echo plotGlobalConfig::getVar('booksResultsShowFirstCountChild'); ?>,
            error: 'No More Posts!',
            delay: 50,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=publications.ajaxLoadMore'); ?>',
            appendDataTo: 'books-list',
            userData: ajaxScrollData
        });

        jQuery(window).scroll(function(){
            jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
        });

    }
    jQuery(function() {
        jQuery('.scroll-pane').jScrollPane({
                verticalDragMinHeight: 28,
                verticalDragMaxHeight: 28
            }
        );
    });

    // fix bug when style select and cant using select onchange function
    jQuery('#childlib-alter-menu li').click(function(){
        alert('zz');
    });


    function checkAllFilterBooksTagsCheckboxes() {
        jQuery("[id^=filter-books-tag]").prop('checked', true);
        showFilteredAndSortedBooks();
    }

    function clearAllFilterBooksTagsCheckboxes() {
        jQuery("[id^=filter-books-tag]").prop('checked', false);
        showFilteredAndSortedBooks();
    }

    jQuery(document).ready(function () {
        jQuery(window).scroll(function(){
            jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
        });
        jQuery( "#childlib-category" ).selectmenu({
            appendTo:'.main-content',
            open: function( event, ui ) { jQuery('#childlib-category-menu').jScrollPane(); }
        });
        jQuery( "#childlib-alter" ).selectmenu({
            appendTo:'.main-content',
            open: function( event, ui ) { jQuery('#childlib-alter-menu').jScrollPane(); }
        });
        jQuery( "#childlib-filter" ).selectmenu({
            appendTo:'.main-content',
            open: function( event, ui ) { jQuery('#childlib-filter-menu').jScrollPane(); },
        });
    });
    jQuery(window).resize(function() {
            jQuery(function() {
            jQuery('.scroll-pane').jScrollPane({
                    verticalDragMinHeight: 28,
                    verticalDragMaxHeight: 28
                }
            );
        });
    });

    // profile menu, bulb menus
    jQuery(document).on('click', function(event) {
        if (!jQuery(event.target).closest('.bulb-menu').length && !jQuery(event.target).closest('.bulb-menu-open').length) {
            jQuery('.bulb-menu').hide();
        }
        document.documentElement.scrollTop = document.documentElement.scrollTop + 3;
        document.documentElement.scrollTop = document.documentElement.scrollTop - 3;
        document.body.scrollTop = document.body.scrollTop + 3;
        document.body.scrollTop = document.body.scrollTop - 3;
    });
</script>

<?php # </editor-fold>  ?>

<?php # <editor-fold defaultstate="collapsed" desc="MAIN"> ?>
<main class="child-library">

    <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_header') ); ?>

    <div class="main-wrap wrap child-library" id="content">
        <div class="add-left aside-left child-library"></div>
        <div class="add-right aside-right child-library"></div>
        <section class="child-library interest-filter-board">
            <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_top_menu') ); ?>
            <div class="info-board child-library">
                <a href="#"   onclick="history.go(-1);return false;" class="link-back hover-shadow">
                    Вернуться
                    <svg viewBox="0 0 22.9 46" preserveAspectRatio="xMinYMin meet" class="butterfly">
                        <use xlink:href="#butterfly"></use>
                    </svg>
                </a>
                <svg viewBox="0 0 37.1 38.9" preserveAspectRatio="xMinYMin meet" class="leaf">
                    <use xlink:href="#leaf"></use>
                </svg>
                <object name="bobr" data="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/img/bobr-first-level.svg" type="image/svg+xml"></object>
                <form class="select-wood">
                    <select id="childlib-alter" onchange="showFilteredAndSortedBooks();">
                        <option value="0"><?php echo JText::_('COM_PLOT_ALL_AGES'); ?></option>
                        <?php foreach ($this->plotAges AS $age) {
                            ?>
                            <option value="<?php echo $age->id; ?>"><?php echo $age->title; ?></option>
                        <?php } ?>
                    </select>
                    <script type="text/javascript">
                        jQuery( "#childlib-alter" ).selectmenu({
                            change: function(){ showFilteredAndSortedBooks(); }
                        });
                    </script>
                </form>
                <div class="interest-board-wrapper">
                    <div class="interest-board">
                        <h3>Интересы</h3>
                        <form class="scroll-pane">
                            <fieldset>
                                <?php foreach ($this->tags AS $tag) { ?>
                                    <input type="checkbox" checked="checked" id="filter-books-tag<?php echo $tag->id; ?>" value="<?php echo $tag->id; ?>" onchange="showFilteredAndSortedBooks();" />
                                    <label for="filter-books-tag<?php echo $tag->id; ?>"><?php echo $tag->title; ?></label>
                                <?php } ?>
                            </fieldset>
                        </form>
                        <div class="interest-board-under">
                            <button onclick="checkAllFilterBooksTagsCheckboxes();"><?php echo  JText::_('COM_PLOT_SELECT_ALL');?></button>
                            <input id="filter-my-tags" type="checkbox"/>
                            <label for="filter-my-tags" onclick="checkOnlyMyInterestsCheckboxes();">Мои интересы</label>
                            <button onclick="clearAllFilterBooksTagsCheckboxes();"><?php echo  JText::_('COM_POLT_CLEAR'); ?></button>
                        </div>
                    </div>
                </div>
                <div class="info-top">
                    <form>
                        <select id="childlib-category" onchange="showFilteredAndSortedBooks();">
                            <option value="0"><?php echo JText::_('COM_PLOT_ALL_CATEGORIES'); ?></option>
                            <?php foreach ( $this->cats AS $category) { ?>
                                <option value="<?php echo $category->value; ?>"><?php echo $category->text; ?></option>
                            <?php } ?>
                        </select>
                        <script type="text/javascript">
                            jQuery( "#childlib-category" ).selectmenu({ change: function( event, ui ) { showFilteredAndSortedBooks(); } });
                        </script>
                    </form>
                    <input id="filter-books-all" type="radio" value="0" name="my_books" <?php if(!$this->my_books){echo 'checked="checked"'; } ?> style="display: none" onchange="showFilteredAndSortedBooks();" />
                    <label for="filter-books-all">Общий раздел книг</label>
                    <input id="filter-books-my" type="radio" value="1" name="my_books" <?php if($this->my_books){echo 'checked="checked"'; } ?> style="display: none" onchange="showFilteredAndSortedBooks();" />
                    <label for="filter-books-my">Мои Книги</label>
                </div>
                <div class="info-bottom">
                    <p class="count-books"><?php echo  JText::_('COM_PLOT_BOOKS_FOUND');?> </p>
                    <form>
                        <?php $sortFilds = array("pub.c_title asc" => JText::_('COM_PLOT_SORT_BY_A_Z'),
                          //  "pub.c_title desc" => JText::_('COM_PLOT_SORT_BY_Z_A'),
                            "pub.c_created_time desc" => JText::_('COM_PLOT_SORT_BY_LAST_ADDED'),
                            "COUNT(b.book_id) asc" => JText::_('COM_PLOT_SORT_BY_POPULAR'),
                            "book_cost desc" => JText::_('COM_PLOT_SORT_BY_PRICE')
                        ); ?>
                        <label for="childlib-filter"><?php echo  JText::_('COM_PLOT_SORT_BY');?></label>
                        <select id="childlib-filter">
                            <?php foreach ($sortFilds AS $key => $value) { ?>
                                <option
                                    <?php if ($this->state->get('sort')) {
                                        if ($this->state->get('sort') == $key) {
                                            echo 'selected="selected"';
                                        }
                                    }?>
                                    value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                        <script type="text/javascript">
                            jQuery( "#childlib-filter" ).selectmenu({ change: function( event, ui ) { showFilteredAndSortedBooks(); } });
                        </script>
                    </form>
                </div>
            </div>
        </section>
        <section class="my-progress child-library">
            <div id="books-list" class="my-progress-wrapper"></div>
        </section>
    </div>
</main>
<?php # </editor-fold> ?>

<div class="modal-loading"><!-- Place at bottom of page --></div>

