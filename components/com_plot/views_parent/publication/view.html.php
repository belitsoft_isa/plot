<?php
defined('_JEXEC') or die;

class PlotViewPublication extends PlotViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'publication';

    public function display($tpl = null)
    {
        $this->templateUrl = JURI::base().'templates/'.JFactory::getApplication()->getTemplate();
        $this->componentUrl = JURI::base() . 'components/com_plot';
        $this->referrerUrl =JRoute::_('index.php?option=com_plot&view=publications');
        $app = JFactory::getApplication();

        $bookModel = JModelLegacy::getInstance('publication', 'plotModel');
        
        $this->my = plotUser::factory();
        $this->my->level = $this->my->getLevel();
        
        $this->book = $bookModel->getBook();
        $this->bookobj= new plotBook($this->book->c_id);
        if (!$this->book || !$this->book->published) {
            $app->enqueueMessage(JText::_('COM_PLOT_REQUESTED_PAGE_NOT_EXISTS_ANYMORE'), 'message');

            $app->redirect(JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('error404K2ItemId'), false));
        }
        $this->book->costsForBuyers = $this->bookobj->getBuyerCosts(plotGlobalConfig::getVar('payForBookCompleteDefault'));
        $this->book->isBoughtForMe = false;
        $this->book->price=$this->bookobj->getBookMinCost();
        $this->my->childrenIdsHavntCurrentBook = array();
        foreach ($this->my->getChildrenIds() AS $childId) {
            $child = plotUser::factory($childId);
            $coursesBoughtForChild = $child->getBooksIdsBoughtForMe();
            if (!in_array($this->book->c_id, $coursesBoughtForChild)) {
                $this->my->childrenIdsHavntCurrentBook[] = $child->id;
            }
        }
        $this->bookratingBay=(int)$bookModel->ratingBay($this->book->c_id);
        $this->ratingRead=$bookModel->ratingRead($this->book->c_id);
        $this->bookFiles=$this->get('BookFiles');


        $this->setOgMeta();
        $this->bookPrograms=$this->bookobj->bookPrograms();

        foreach($this->bookPrograms AS $item){
        $item->percent=(int)$this->my->programProgress($item->id);
        }

        if($this->my->id && $this->book->c_id && ((int)$this->my->isNewBook($this->book->c_id)==-1)){
            $this->bookobj->setIsNotNewBook($this->my->id);
        }
        if(JRequest::getVar('orderSumAmount', 0)){
            $app->redirect(JRoute::_('index.php?option=com_plot&view=publication&bookId='.$this->book->c_id, false));
        }
        return parent::display($tpl);
    }
    
    private function setOgMeta()
    {
        $db = JFactory::getDbo();
        require_once JPATH_ADMINISTRATOR.'/components/com_plot/plot_social_config.php';
        $metaOgUrl = '<meta property="og:url" content="'.PlotHelper::getAbsRoutedUri('index.php?option=com_plot&view=publication&bookId='.$this->book->c_id).'" />';
        $metaOgTitle = '<meta property="og:title" content="'.$db->escape($this->book->c_title).'" />';
        $metaOgDescription = '<meta property="og:description" content="'.$db->escape(strip_tags($this->book->c_pub_descr)).'" />';
        $metaOgImage = '<meta property="og:image" content="'.PlotHelper::getAbsRoutedUri('index.php?option=com_plot&view=publication&task=publication.pickoutimage&bookId='.$this->book->c_id).'&canv_width=200&canv_height=200" />';
        $metaOgSiteName = '<meta property="og:site_name" content="'.JFactory::getConfig()->get('sitename').'" />';
        $metaOgFbAppId = '<meta property="fb:app_id" content="473991812742077" />';
        $metaOgType = '<meta property="og:type" content="website" />';
        $metaOgLocale = '<meta property="og:locale" content="ru_RU" />';
        JFactory::getDocument()->addCustomTag($metaOgTitle.$metaOgSiteName.$metaOgUrl.$metaOgDescription.$metaOgFbAppId.$metaOgType.$metaOgLocale.$metaOgImage);
        return true;
    }        
    
}
