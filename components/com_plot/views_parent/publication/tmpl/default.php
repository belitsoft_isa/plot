<?php
defined('_JEXEC') or die;
$id = JFactory::getApplication()->input->get('bookId', 0, 'INT');
?>

<link href="<?php echo $this->componentUrl; ?>/views_parent/course/tmpl/default.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo JUri::root() . 'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.min.js"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.basic.js"></script>
<script type="text/javascript"
        src="<?php echo JUri::root() . 'components/com_plot/libraries/jquery_input_mask_plugin/jquery.mask.min.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/js/jquery.mousewheel.js"></script>
<script type="text/javascript"
        src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/js/jquery.jscrollpane.min.js"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/mootools-core.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/core.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/mootools-more.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/modal.js" type="text/javascript"></script>


<script type="text/javascript"
        src="<?php echo JUri::root(); ?>components/com_plot/assets/js/plotEssay.js"></script>
<script
    src="<?php echo JURI::base() . 'components/com_plot'; ?>/libraries/scrolling_ajax_pagination/javascript.js"></script>
<script src="<?php echo JUri::root(); ?>components/com_plot/assets/js/plot.js"></script>
<script type="text/javascript"
        src="<?php echo JUri::root(); ?>components/com_plot/assets/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>templates/plot/js/jquery_simple_progressbar.js"></script>
<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">

    function noEssayFind() {
        jQuery('#essay-list').html('');
        jQuery('#essay-list')
            .html("<div class='no-items'><?php echo JText::sprintf('COM_PLOT_NOT_FOUND_WHAT_BOOKS', JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar("footerLinkK2idForChildrens"))); ?></div>");
    }

    jQuery(document).ready(function () {
        jQuery("#paymentType").selectmenu({
            appendTo: '.payment-type'
        });
        jQuery("#paymentType").on("selectmenuclose", function (event, ui) {
        });


        jQuery('.plot-book-programs').each(function () {
            var percent = parseInt(jQuery(this).attr('data-progress'));
            jQuery(this).simple_progressbar({
                value: percent,
                height: '10px',
                internalPadding: '3px',
                normalColor: '#C56B35',
                backgroundColor: '#ffffff'
            });
        });
    });

    function bookAccessDenied() {
        SqueezeBox.initialize({
            size: {x: 300, y: 150}
        });
        var str = '<div id="enqueued-message">Эта книга пока не куплена вам</div>';
        SqueezeBox.setContent('adopt', str);
        SqueezeBox.resize({x: 300, y: 150});
    }

    function courseBuy() {
        var userIdsCourseBuyFor = [];
        jQuery('#course-children-for .users-course-for-list .user-course-for').each(function (i, v) {
            userIdsCourseBuyFor.push(jQuery(this).attr('userid'));
        });

        if (userIdsCourseBuyFor.length) {
            jQuery.post(
                '<?php echo JRoute::_('index.php?option=com_plot&task=book.createOrder'); ?>',
                {
                    bookId: <?php echo $this->book->c_id; ?>,
                    usersIds: JSON.stringify(userIdsCourseBuyFor),
                    amount: jQuery('#reward').val()
                },
                function (response) {
                    var data = jQuery.parseJSON(response);
                    jQuery('#buy_book_hidden_form [name=userIds]').val(userIdsCourseBuyFor.join('-'));
                    jQuery('#buy_book_hidden_form [name=orderNumber]').val(data.orderId);
                    jQuery('#buy_book_hidden_form [name=rewardAmountForEach]').val(jQuery('#reward').val());
                    jQuery('#buy_book_hidden_form').submit();
                }
            );
        } else {
            SqueezeBox.initialize({
                size: {x: 300, y: 150}
            });
            var str = '<div id="enqueued-message">Не выбраны пользователи, для которых Вы хотите купить данную книгу</div>';
            SqueezeBox.setContent('adopt', str);
            SqueezeBox.resize({x: 300, y: 150});
        }
    }

    function calculateCourseCost() {
        jQuery.post('index.php?option=com_plot&task=book.calculateCosts', {
            bookId: <?php echo $this->book->c_id; ?>,
            enteredAmount: jQuery('#reward').val(),
            countChilds: jQuery('#course-children-for .users-course-for-list .user-course-for').length
        }, function (response) {
            var data = jQuery.parseJSON(response);
            jQuery('.admin-cost-calculated').html(data.costs.admin);
            jQuery('.author-cost-calculated').html(data.costs.author);
            jQuery('.payment-system-cost-calculated').html(data.costs.paymentSystem);
            jQuery('.total-cost-calculated').html(data.costs.total);
            jQuery('#buy_book_hidden_form [name=sum]').val(data.costs.total);
        });
    }


    function ajaxLoadEssay() {
        var essayFilterData = {
            bookId: '<?php echo $id; ?>',
            screenHeight: window.innerHeight,
            screenWidth: window.innerWidth
        };
        jQuery.post(
            'index.php?option=com_plot&task=essay.ajaxEssayLoadMore',
            {userData: essayFilterData},
            function (response) {
                if (response) {
                    jQuery('#essay-list').append(response);
                    jQuery('#loading-bar').hide();
                } else {
                    jQuery('#loading-bar').hide();
                }
            }
        );
        jQuery('#essay-list').scrollPagination({
            nop: <?php echo plotGlobalConfig::getVar('childEssayCount'); ?>,
            offset: <?php echo plotGlobalConfig::getVar('childEssayCount'); ?>,
            error: '',
            delay: 10,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=essay.ajaxEssayLoadMore'); ?>',
            appendDataTo: 'essay-list',
            userData: essayFilterData,
            afterLoad: {
                afterLoadAction: function () {
                    jPlotUp.Arrow.initialize('positionCameras');
                }
            }
        });
    }

    function ajaxLoadBeforePublications() {
        var beforeFilterData = {
            bookId: '<?php echo $id; ?>'
        };
        jQuery.post(
            'index.php?option=com_plot&task=publication.ajaxGetBeforePublications',
            {userData: beforeFilterData},
            function (response) {
                if (response) {
                    var data = jQuery.parseJSON(response);
                    jQuery('#child-beforebook').append(data.renderedBooks);
                    jQuery('#loading-bar').hide();
                } else {
                    jQuery('#loading-bar').hide();
                }
            }
        );

        jQuery(window).unbind('scroll');
        jQuery('#child-beforebook').scrollPagination({
            nop: <?php echo plotGlobalConfig::getVar('booksResultsShowFirstCountChild'); ?>,
            offset: <?php echo plotGlobalConfig::getVar('booksResultsShowFirstCountChild'); ?>,
            error: '',
            delay: 10,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=publication.ajaxGetBeforePublications'); ?>',
            appendDataTo: 'child-beforebook',
            userData: beforeFilterData,
            afterLoad: {
                afterLoadAction: function () {
                    jPlotUp.Arrow.initialize('positionCameras');
                }
            }
        });
        jQuery('.main-wrap-overflow').trigger('scroll');
    }

    function ajaxLoadAfterPublications() {
        var beforeFilterData = {
            bookId: '<?php echo $id; ?>'
        };
        jQuery.post(
            'index.php?option=com_plot&task=publication.ajaxGetAfterPublications',
            {userData: beforeFilterData},
            function (response) {
                if (response) {
                    var data = jQuery.parseJSON(response);
                    jQuery('#child-afterbook').append(data.renderedBooks);
                    jQuery('#loading-bar').hide();
                } else {
                    jQuery('#loading-bar').hide();
                }
            }
        );
        jQuery(window).unbind('scroll');
        jQuery('#child-afterbook').scrollPagination({
            nop: <?php echo plotGlobalConfig::getVar('booksResultsShowFirstCountChild'); ?>,
            offset: <?php echo plotGlobalConfig::getVar('booksResultsShowFirstCountChild'); ?>,
            error: '',
            delay: 10,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=publication.ajaxGetAfterPublications'); ?>',
            appendDataTo: 'child-afterbook',
            userData: beforeFilterData,
            afterLoad: {
                afterLoadAction: function () {
                    jPlotUp.Arrow.initialize('positionCameras');
                }
            }
        });
        jQuery('.main-wrap-overflow').trigger('scroll');
    }

    function fixFFBugWithFavicon() {
        jQuery('head link[href="<?php echo JUri::root(true);?>/templates/plot/favicon.ico"]').attr('href', '<?php echo JUri::root(true);?>/templates/plot/favicon.ico');
    }


    document.addEventListener("touchmove", function(){
        jQuery('.wrap.child-library').trigger('scroll');
        console.log('ScrollStart');
    }, false);
</script>





<?php # </editor-fold> ?>

<style>
    .ui360 .sm2-canvas.hi-dpi {
        top: 0% !important;
        left: 0% !important;
    }

    .plot-book-url > svg use {
        filter: url("#inactive");
    }

    .plot-book-url:not(.denied):active > svg use {
        filter: url("#active");
    }
</style>
<!-- required -->
<link rel="stylesheet" type="text/css"
      href="<?php echo JUri::root() . 'components/com_plot/libraries/soundmanagerv2/demo/360-player/360player.css'; ?>"/>

<!-- special IE-only canvas fix -->
<!--[if IE]>
<script type="text/javascript"
        src="<?php echo JUri::root().'components/com_plot/libraries/soundmanagerv2/demo/360-player/script/excanvas.js'; ?>"></script><![endif]-->

<!-- Apache-licensed animation library -->
<script type="text/javascript"
        src="<?php echo JUri::root() . 'components/com_plot/libraries/soundmanagerv2/demo/360-player/script/berniecode-animator.js'; ?>"></script>

<!-- the core stuff -->
<script type="text/javascript"
        src="<?php echo JUri::root() . 'components/com_plot/libraries/soundmanagerv2/script/soundmanager2.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo JUri::root() . 'components/com_plot/libraries/soundmanagerv2/demo/360-player/script/360player.js'; ?>"></script>

<script type="text/javascript">
    soundManager.setup({
        // path to directory containing SM2 SWF
        url: '<?php echo JUri::root();?>components/com_plot/libraries/swf/'
    });
</script>

<script type="text/javascript"
        src="<?php echo JUri::root(); ?>components/com_plot/views_parent/publication/tmpl/default.js"></script>

<main class="parent-profile parent-one-elem">
<div class="top parent-profile">
    <?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_header')); ?>
    <?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_top_menu')); ?>
</div>
<div class="wrap main-wrap parent-profile">
<div class="add-left aside-left parent-profile"></div>
<div class="add-right aside-right parent-profile"></div>
<section class="parent-one-elem parent-library">
<div class="parent-courses-think-tabs">
    <button class="link-back" onclick="window.location.href='<?php echo $this->referrerUrl; ?>';">Назад</button>
    <p><?php
        echo $this->book->c_category;
        ?></p>
    <?php if (plotUser::factory()->isSiteAdmin() || plotUser::factory()->isBookAuthor($this->book->c_id) || PlotHelper::bookReadAccess($this->book->c_id)) {
        if ((int)PlotHelper::quizAccess((int)$this->book->id_quiz)) {
            ?>

            <button class="add hover-shadow"
                    onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_joomlaquiz&view=quiz&quiz_id=' . (int)$this->book->id_quiz); ?>'">
                Пройти тест
            </button>

        <?php
        } else {
            ?>
            <button class="add hover-shadow"
                    onclick="createMsg('<?php echo JText::_("COM_PLOT_FE_AUTHORIZE_RIGHTS_VIEW_QUIZ"); ?>');  return false;">Пройти тест
            </button>

        <?php
        }

    }else{
        ?>
        <button class="add hover-shadow"
            <?php if(plotUser::factory()->id){?>
                onclick="createMsg('<?php echo JText::_("COM_PLOT_FE_AUTHORIZE_RIGHTS_VIEW_QUIZ"); ?>');  return false;">Пройти тест
            <?php }else{ ?>
                onclick="createMsg('<?php echo JText::_("COM_PLOT_LOGIN_FIRST"); ?>');  return false;">Пройти тест
            <?php } ?>
        </button>
    <?php
    } ?>

    <?php if ($this->book->c_id && $this->bookobj->isGetPages()) { ?>
        <button class="add-recipient hover-shadow"
                onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_html5flippingbook&view=publication&id=' . (int)$this->book->c_id . '&fullscreen=1'); ?>'"><?php echo JText::_("COM_PLOT_BOOK_ABOUT_BOOK"); ?></button>
    <?php } ?>

</div>
<div class="elem-info parent-book">
<div <?php echo ($this->bookobj->isGetPages()) ? 'class="elem-picture thumbnail"' : 'class="elem-picture nothumbnail"'; ?> >
    <a class="modal" rel="{handler: 'iframe', size: {x: auto, y:auto}}"
        <?php if (($this->bookobj->isGetPages())){ ?>
       href="<?php echo JRoute::_("index.php?option=com_html5flippingbook&view=publication&id=" . (int)$this->book->c_id . "&fullscreen=1"); ?>">
        <?php
        } else {
            ?>
            href="javascript:void(0)">
        <?php
        }?>
        <?php if (PlotHelper::bookReadAccess($this->book->c_id)) { ?>
            <?php if ($this->book->new || $this->book->percent == 0) { ?>
                <span class="arrow-bottom">Новая</span>
            <?php } ?>
        <?php } ?>
        <figure>
            <img src="<?php echo $this->book->thumbnailUrl; ?>"/>
        </figure>
    </a>
</div>
<div class="st-bon-wrap">
    <div class="sticker">
        Рейтинг книги:<br/>

        <div class="bought">Куплено: <?php echo $this->bookratingBay; ?></div>
        <div class="studied">Прочитано: <?php echo $this->ratingRead; ?></div>
        <div class="clr"></div>
    </div>
    <?php if ($this->book && $this->my->getFinishedPriceForMyBook($this->book->c_id)) { ?>
        <div class="bonus">
            <p><?php echo  JText::_('COM_PLOT_AFTER_BOOK_READ')?></p>

            <div>
                <p class="finance-info"><?php echo $this->my->getFinishedPriceForMyBook($this->book->c_id); ?>
                    <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                        <use xlink:href="#munze"></use>
                    </svg>
                </p>
            </div>
        </div>
    <?php } ?>

</div>
<div class="title-wrap">
    <h6><?php echo $this->book ? $this->book->c_title : ""; ?></h6>
    <strong>
        <i>Автор:</i>
        <?php
        # echo plotUser::factory($this->book->pay_author)->name;
        echo $this->book ? $this->book->c_author : "";
        ?>
    </strong>
</div>
<?php if ($this->bookFiles) { ?>
    <ul class="plot-book-files">
        <?php  foreach ($this->bookFiles AS $file) {

            ?>
            <li>
                <?php
                switch ($file->type) {
                    case 'audio':
                        ?>
                        <?php if (PlotHelper::bookReadAccess($this->book->c_id)) { ?>
                        <div class="ui360 plot-book-<?php echo $file->type; ?>"><a
                                href="<?php echo '/media/com_plot/bookfiles/' . $file->path; ?>"></a>
                        </div>
                    <?php
                    } else {
                        ?>
                        <a class="denied audio" href="javascript:void(0);" onclick="bookAccessDenied();"></a>
                    <?php

                    }
                        break;
                    case 'url':
                        if (PlotHelper::bookReadAccess($this->book->c_id)) {
                            ?>
                            <a class="plot-book-<?php echo $file->type; ?>" href="<?php echo $file->path; ?>"
                               target="_blank">
                                <svg viewBox="0 0 153 155" preserveAspectRatio="xMidYMax meet">
                                    <g>
                                        <use xlink:href="#url-icon"></use>
                                    </g>
                                </svg>
                            </a>
                        <?php
                        } else {
                            ?>
                            <a class="denied plot-book-<?php echo $file->type; ?>" href="javascript:void(0);"
                               onclick="bookAccessDenied();">
                                <svg viewBox="0 0 153 155" preserveAspectRatio="xMidYMax meet">
                                    <g>
                                        <use xlink:href="#url-icon"></use>
                                    </g>
                                </svg>
                            </a>
                        <?php
                        }
                        break;
                    default:
                        if (PlotHelper::bookReadAccess($this->book->c_id)) {
                            ?>
                            <a class="button-3d plot-book-<?php echo $file->type; ?>"
                               href="<?php echo '/media/com_plot/bookfiles/' . $file->path; ?>"
                               download="<?php echo $file->path; ?>"
                               target="_blank"><?php echo $file->type; ?></a>
                        <?php
                        } else {
                            ?>
                            <a class="button-3d denied plot-book-<?php echo $file->type; ?>" href="javascript:void(0);"
                               onclick="bookAccessDenied();"><?php echo $file->type; ?></a>
                        <?php
                        }
                }
                ?>
            </li>
        <?php
        }
        ?>
    </ul>
<?php
}?>
<div class="clr"></div>
<h4>Информация:</h4>
<div class="elem-about">
    <?php

    if ((int)PlotHelper::quizAccess((int)$this->book->id_quiz) && !$this->my->isBookTestFinished($this->book->c_id) && !$this->my->getFinishedBookByBookId($this->book->c_id)) {
        // $money = (int)$this->my->getFinishedPriceForMyBook($this->book->c_id) ? (int)$this->my->getFinishedPriceForMyBook($this->book->c_id) : 0;
        ?>
        <div>
            <?php echo JText::sprintf('COM_PLOT_BOOK_BUY_BUT_TEST_NOT_FINISHED', '<a href="'.JRoute::_("index.php?option=com_plot&view=wallet").'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>
        </div>
    <?php
    }?>

    <?php

    if ((int)PlotHelper::quizAccess((int)$this->book->id_quiz) && $this->my->isBookTestFinished($this->book->c_id) && !$this->my->getFinishedBookByBookId($this->book->c_id) && !$this->my->isEssayIsset($this->book->c_id)) {

        ?>
        <div>
            <?php
            echo JText::_('COM_PLOT_BOOK_BUY_BUT_TEST_FINISHED');

            ?>

        </div>
    <?php
    }?>
    <?php
    if((int)PlotHelper::quizAccess((int)$this->book->id_quiz) && $this->my->isBookTestFinished($this->book->c_id) && !$this->my->getFinishedBookByBookId($this->book->c_id) && $this->my->isEssayIsset($this->book->c_id)){
        $buyer=$this->bookobj->whoBoughtBook($this->book->c_id, $this->my->id);
        echo JText::sprintf('COM_PLOT_BOOK_AND_TEST_FINISHED_ESSAY_CREATED','<br/><a href="'.JRoute::_("index.php?option=com_plot&view=profile&id=".(int)$buyer).'">'.plotUser::factory($buyer)->name.'</a>');
    }
    ?>

    <?php
    if ((int)PlotHelper::quizAccess((int)$this->book->id_quiz) && $this->my->isBookTestFinished($this->book->c_id) && $this->my->getFinishedBookByBookId($this->book->c_id)) {
        ?>
        <div>
            <?php echo JText::sprintf('COM_PLOT_BOOK_AND_TEST_FINISHED', '<a href="'.JRoute::_("index.php?option=com_plot&view=wallet").'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>
        </div>
    <?php
    }
    ?>
</div>
<h4>Аннотация:</h4>

<div class="elem-about">
    <p><?php echo $this->book ? $this->book->c_pub_descr : ""; ?></p>
</div>
<h4>Купить эту книгу:</h4>

<div class="buy">
    <form id="course-children-for">
        <fieldset>
            <label for="recipients-course">Выберите кому:</label>

            <div class="users-course-for-list"></div>
            <button class="add-recipient" name="choose"
                    onclick="SqueezeBox.open('<?php echo JRoute::_('index.php?option=com_plot&task=searchusersbook.showPopup&bookId=' . JRequest::getInt('bookId', 0)); ?>', {size: {x: 600, y: 450}, handler: 'ajax'});">
                <svg viewBox="0 0 31.7 30.8" preserveAspectRatio="xMinYMin meet" class="friend">
                    <use xlink:href="#friend"></use>
                </svg>
                Добавить
            </button>
        </fieldset>
    </form>
    <div>
        <div>
            <p>Вознаграждение за прочтение:</p>
            <span class="abs">1 руб = 1 монета</span>
            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                <use xlink:href="#munze"></use>
            </svg>
            <input type="text" id="reward"
                   value="<?php echo plotGlobalConfig::getVar('payForBookCompleteDefault'); ?>"/>
        </div>
        <div>
            <p><?php echo JText::_("COM_PLOT_ADMINISTRATOR_DONATION"); ?></p>
            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                <use xlink:href="#munze"></use>
            </svg>
            <i class="admin-cost-calculated"><?php echo $this->book->costsForBuyers['admin']; ?></i>
        </div>
        <div>
            <p><?php echo  JText::_('COM_PLOT_BOOK_REMUNERATION_TO_AUTHOR_TEST')?>:</p>
            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                <use xlink:href="#munze"></use>
            </svg>
            <i class="author-cost-calculated"><?php echo $this->book->costsForBuyers['author']; ?></i>
        </div>
        <div>
            <p>Комиссия платежной системы:</p>
            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                <use xlink:href="#munze"></use>
            </svg>
            <i class="payment-system-cost-calculated"><?php echo $this->book->costsForBuyers['paymentSystem']; ?></i>
        </div>
        <span>Итого:</span>
        <b><span class="total-cost-calculated"><?php echo $this->book->costsForBuyers['total']; ?></span> руб</b>

        <form action="https://money.yandex.ru/eshop.xml" method="post" name="buy_book_hidden_form"
              id="buy_book_hidden_form">
            <!-- Обязательные поля -->
            <input name="shopId" class="hidden" value="<?php echo plotGlobalConfig::getVar('yandexShopId'); ?>"
                   type="text"/>
            <input name="scid" class="hidden" value="<?php echo plotGlobalConfig::getVar('yandexScid'); ?>"
                   type="text"/>
            <input name="sum" class="hidden" value="0" type="text"/>
            <input name="customerNumber" class="hidden" value="<?php echo plotUser::factory()->id; ?>" type="text"/>
            <!-- Необязательные поля -->
            <div class="payment-type">
                <p>Способ оплаты:</p>
                <select name="paymentType" id="paymentType">
                    <option value="AC">Банковская карта</option>
                    <option value="PC">Яндекс кошелек</option>
                    <option value="GP">Через кассы и терминалы</option>
                    <option value="WM">Кошелек WM</option>
                </select>
            </div>
            <input name="orderNumber" class="hidden" value="0" type="text"/>
            <input name="cps_email" class="hidden" value="<?php echo  plotUser::factory()->email; ?>" type="text"/>
            <input name="shopSuccessURL" class="hidden"
                   value="http://naplotu.com/publication/<?php echo $this->book->c_id; ?>/success=1"
                   type="text"/>
            <input name="shopFailURL" class="hidden"
                   value="http://naplotu.com/publication/<?php echo $this->book->c_id; ?>/success=0"
                   type="text"/>
            <input name="bookId" class="hidden" value="<?php echo $this->book->c_id; ?>" type="text"/>
            <input name="entity" class="hidden" value="book" type="text"/>
            <input name="rewardAmountForEach" class="hidden" value="0" type="text"/>
            <input name="userIds" class="hidden" value="" type="text"/>
        </form>
        <input type="submit" onclick="courseBuy();" value="Оплатить"/>
    </div>
</div>
<h4>Книга включена в программы:</h4>

<div class="programs-list" id="parent-programs-list">
    <div class="jcarousel">
        <?php if ($this->bookPrograms) { ?>
            <ul class="parent-profile">
                <?php
                foreach ($this->bookPrograms AS $program) {
                    ?>
                    <li>
                        <a href="<?php echo $program->link; ?>">
                            <img src="<?php echo $program->img; ?>">
                            <!-- <div class="progress-bar-wrap">
                                 <div class="progress">
                                     <div class="progress-bar"></div>
                                 </div>
                                 <span>0</span>
                                 <span>50%</span>
                                 <span>100%</span>
                             </div>-->
                            <h5><?php echo $program->title; ?></h5>
                        </a>

                        <div class="plot-book-programs" data-progress="<?php echo $program->percent; ?>"></div>
                    </li>
                <?php
                }
                ?>
            </ul>
        <?php } else { ?>
            <p class="no-items"><?php echo JText::sprintf('COM_PLOT_THIS_BOOK_NOT_INCLUDE_IN PROGRAMS', $this->book->c_title); ?></p>
        <?php
        }
        ?>
    </div>
    <p class="jcarousel-pagination"></p>
</div>

<div class="parent-essay-courses">
    <ul class="parent-courses-think-tabs" id="all-tabs">
        <li id="link-parent-essay"><a href="#all-parent-essay" class="active">Эссе</a></li>
        <!--<li><a href="#all-child-beforebook" id="link-parent-beforebook">Книги до</a></li>
        <li><a href="#all-child-afterbook" id="link-parent-afterbook">Книги после</a></li>-->
        <li id="link-parent-disqus"><a href="#all-parent-disqus"><?php echo JText::_("COM_PLOT_COURSE_DISQUS"); ?></a>
        </li>
    </ul>

    <div id="loading-bar" style="margin-left: 5%;"><img
            src="<?php echo $this->baseurl ?>/templates/plot/img/pre-loader-1.gif"/></div>
    <div id="all-parent-essay" class="essay-inner parent-profile">
        <?php
        if (plotUser::factory()->isBookFinished($this->book->c_id)==1 && plotUser::factory()->id && !plotUser::factory()->essayIsWritten($this->book->c_id)) {
            ?>
            <div class="essay-theme">
                <?php if ($this->book && $this->book->text) { ?>
                    <h5><?php echo  JText::_("COM_PLOT_COURSE_LIST_TOPICS_FOR_ESSAY");?> </h5>
                    <div> <?php   echo  html_entity_decode($this->book->text); ?></div>
                <?php  }?>

            </div>
            <div class="create-essay">
                <button class="add hover-shadow" id="plot-essay-create-button"
                        onclick="plotEssay.showElement('essay-area').hideElement('plot-essay-create-button')"><?php echo JText::_('COM_PLOT_CREATE_ESSAY'); ?></button>
                <div id="essay-area" style="display: none;">
                    <textarea id="plot-user-textarea" placeholder="Заполните ессе" maxlength="<?php echo plotGlobalConfig::getVar('essayTextareaMaxSymbols')?>"></textarea>
                    <?php echo  JText::_('COM_PLOT_COUNT_ENTER_SYMBOLS');?><span id="textarea_message"><?php echo plotGlobalConfig::getVar('essayTextareaMaxSymbols')?></span>
                    <button class="button cancel-act hover-shadow"
                            onclick="plotEssay.hideElement('essay-area').showElement('plot-essay-create-button');"><?php echo JText::_('COM_PLOT_CANCEL') ?></button>
                    <button class="button add hover-shadow"
                            onclick="plotEssay.essayValidate('plot-user-textarea','<?php echo $this->book->c_id; ?>','#link-parent-essay').hideElement('essay-area').showElement('plot-essay-create-button');"><?php echo JText::_('COM_PLOT_SEND'); ?></button>

                </div>
            </div>
        <?php
        }
        ?>
        <ul class="no-jcarousel" id="essay-list"></ul>
    </div>
    <!--    <div id="all-child-beforebook" class="no-jcarousel new-event">

            <ul class="child-profile" id="child-beforebook"></ul>
        </div>
        <div id="all-child-afterbook" class="no-jcarousel new-event">

            <ul class="child-profile" id="child-afterbook"></ul>
        </div>-->
    <div id="all-parent-disqus">
        <?php echo PlotHelper::renderDisqus('-publication_' . $id); ?>
        <div id="disqus_thread"></div>
    </div>

</div>
</div>
</section>
</div>


</main>


