<?php
defined('_JEXEC') or die;

if ($this->essay['items']) { ?>

    <?php foreach ($this->essay['items'] AS $key => $essay) { ?>
        <li>
            <?php
            if($essay->reviewer==plotUser::factory()->id && $essay->status==0){
            ?>
            <a href="javascript:void(0);" onclick="SqueezeBox.open('<?php echo  JRoute::_("index.php?option=com_plot&task=essay.ajaxEssay"); ?>&id=<?php echo  $essay->id; ?>', {size:{x:<?php echo $essay->screenWidth; ?>, y:<?php echo $essay->screenHeight;?>}, handler:'iframe'})">
                <?php  }else{
                ?>
                <a class="fancybox  essay-<?php echo $essay->id; ?>" rel="photos-set"
                    <?php if($essay->status){ ?>
                        data-share=" data-image='<?php echo $essay->original; ?>' data-url='<?php echo $essay->original; ?>' "
                    <?php } ?>
                   href="<?php echo JURI::root() . 'media/com_plot/essay/' . $essay->id.'/'.$essay->img; ?>"  >
                    <?php
                    }
                    ?>
                    <h6><?php echo  plotUser::factory($essay->user_id)->name?></h6>
                    <hr/>
                    <span><?php echo JFactory::getDate($essay->date)->format('d.m.Y'); ?></span>
                    <div class="circle-img">
                        <svg class="clip-svg-course">
                            <image style="clip-path: url(#clipping-circle-course);" width="100%"
                                   height="100%"
                                   xlink:href="<?php echo JURI::base() . 'templates/plot'; ?>/img/default-essay.jpg"/>"
                            alt="эссе"/>
                        </svg>
                        <i class="activity-icons">
                            <svg viewBox="0 0 30.3 34" style="fill:url(#svg-gradient); stroke:url(#svg-gradient);" preserveAspectRatio="xMidYMid meet">
                                <use xlink:href="#zoom"></use>
                            </svg>
                        </i>
                    </div>
                    <?php if ($essay->text) { ?>
                        <p><?php echo PlotHelper::cropStr(strip_tags($essay->text), plotGlobalConfig::getVar('booksDescriptionMaxSymbolsToShow')); ?></p>
                    <?php } ?>
                </a>
        </li>
    <?php } ?>

<?php } ?>

