<?php
defined('_JEXEC') or die;
?>
<?php if($this->books['items']) { ?>
    <?php foreach ($this->books['items'] AS $key => $book) { ?>
        <li>
            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId='.(int)$book->c_id);?>">
                <figure><img src="<?php echo $book->thumbnailUrl; ?>" /></figure>
            </a>
            <div class="elem-descr">
                <?php echo PlotHelper::cropStr(strip_tags($book->c_pub_descr), plotGlobalConfig::getVar('booksDescriptionMaxSymbolsToShow')) ; ?>
                <b class="price"><?php echo $book->price; ?> руб</b>
            </div>
        </li>
    <?php } ?>
<?php } ?>
