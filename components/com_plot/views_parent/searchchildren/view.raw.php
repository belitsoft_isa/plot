<?php
defined('_JEXEC') or die('Restricted access');

class PlotViewSearchChildren extends PlotViewParentLegacy
{
    
    function display($tpl = null)
    {
        $model = JModelLegacy::getInstance('searchChildren', 'plotModel');
        $this->courseId = JRequest::getInt('courseId', '0');
        
        $this->users = $model->getUsers(array(), '`u`.`name` ASC', $limit = array('limitstart' => '0', 'limit' => '10'));
        $this->totalUser = $model->totalUsers;
        
        $this->pagination = new JPagination( $this->totalUser, 0, plotGlobalConfig::getVar('usersCountSearchForBuyCourse') );
        
        
        
        parent::display();
        die;
    }

}
