<?php
defined('_JEXEC') or die('Restricted access');
?>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<link type="text/css" href="<?php echo JUri::root(); ?>templates/plot/css/style.css" rel="stylesheet">
<script src="<?php echo JUri::root(); ?>media/jui/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/jui/js/jquery.ui.core.min.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/mootools-core.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/core.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/mootools-more.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/modal.js" type="text/javascript"></script>

<script src="<?php echo JUri::root(); ?>templates/plot/js/jplot.js" type="text/javascript"></script>
<script type="text/javascript">
    function addNewUserCourseBuyFor(userId, userName)
    {
        jQuery('#course-children-for .users-course-for-list').append(
            '<div class="user-course-for" userid="'+userId+'">'+
                '<a class="remove">&#215</a>'+
                '<div class="name">'+jPlot.helper.cropString(userName, 28)+'</div>'+
            '</div>'
        );

        SqueezeBox.close();
    }
    
    jQuery(document).ready(function(){
        jQuery('body').addClass('popup-style parent-profile');
        jQuery('#search-users-to-buy .search-value').keyup(function(){
            jQuery('#search-users-to-buy .search-users-loading').html('Загрузка...');
            jQuery.post(
                '<?php echo JRoute::_('index.php?option=com_plot&task=searchchildren.ajaxGetUsersList&courseId='.$this->courseId); ?>',
                {
                    searchValue: jQuery('#search-users-to-buy .search-value').val(),
                    page: 1
                }, 
                function(response){
                    var data = jQuery.parseJSON(response);
                    jQuery('#search-users-to-buy .users-list').html(data.html);
                    jQuery('#search-users-to-buy .search-users-loading').html('');
                }
            );
        });
        
        jQuery('#search-users-to-buy .users-list').on('click', '.user-row:not(.inactive)', function(){
            var userId = jQuery(this).attr('userid');
            jQuery.post(
                '<?php echo JRoute::_('index.php?option=com_plot&task=aboutme.addChild'); ?>',
                {
                    child_id: userId
                },
                function(response){
                    if(response.status){
                        document.getElementById('link-back').click();
                    }else{
                        SqueezeBox.initialize({
                            size: {x: 200, y: 50}
                        });
                        var str = '<div id="enqueued-message">Ребенок не добавлен</div>';
                        SqueezeBox.setContent('adopt', str);
                        SqueezeBox.resize({x: 200, y: 50});
                        document.getElementById('link-back').click();
                    }

                }
            );

        });
        
        jQuery('#search-users-to-buy .users-list').on('click', '.pagination .page-item', function(){
            var page = jQuery(this).attr('page');
            var searchValue = jQuery.trim( jQuery('#search-users-to-buy .search-value').val() );
            jQuery('#search-users-to-buy .search-users-loading').html('Загрузка...');
            jQuery.post(
                '<?php echo JRoute::_('index.php?option=com_plot&task=searchchildren.ajaxGetUsersList&courseId='.$this->courseId); ?>',
                {
                    searchValue: searchValue,
                    page: page
                },
                function(response){
                    var data = jQuery.parseJSON(response);
                    jQuery('#search-users-to-buy .users-list').html(data.html);
                    jQuery('#search-users-to-buy .search-users-loading').html('');
                }
            );
        });
    });
</script>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="CSS"> ?>

<?php # </editor-fold> ?>
<a id="link-back" class="modal" href="<?php echo  JRoute::_('index.php?option=com_plot&amp;task=profileedit.ajaxGetEditProfilePopupHtml#tab-3');?>"></a>
<div id="search-users-to-buy">
    
    <div class="search-users">
        <input class="search-value" type="text" placeholder="Введите имя..."/>
        <div class="search-users-loading"></div>
    </div>

    <div class="users-list search-for-my-children">
        <?php require 'users.list.php'; ?>
    </div>    
    
</div>
