<?php
defined('_JEXEC') or die('Restricted access');
?>

<?php foreach ($this->users AS $user) { ?>
<div class="user-row search-for-my-children <?php echo in_array($user->id, plotUser::factory()->getChildrenIds()) ? 'inactive' : ''; ?>" userid="<?php echo $user->id; ?>">
    <div class="user-avatar">
        <img src="<?php echo plotUser::factory($user->id)->getSquareAvatarUrl(); ?>" />
    </div>
    <div class="user-name">
        <?php echo $user->name; ?>
    </div>
    <?php if ( in_array($user->id, plotUser::factory()->getChildrenIds()) ) { ?>
    <div class="message">Пользователь уже является вашим ребенком</div>
    <?php } ?>
</div>
<?php } ?>

<div class="pagination">
    <?php for ( $i = $this->pagination->pagesStart; $i <= $this->pagination->pagesStop; $i++ ) { ?>
    <div page="<?php echo $i; ?>" class="page-item <?php echo $this->pagination->pagesCurrent == $i ? 'active' : ''; ?>">
        <?php echo $i; ?>
    </div>
    <?php } ?>
</div>