<?php
defined('_JEXEC') or die('Restricted access');

class PlotViewParentLegacy extends JViewLegacy
{
    
    public function __construct($config = array())
    {
        parent::__construct($config);
        foreach (array_keys($this->_path['template']) AS $key) {
            $this->_path['template'][$key] = str_replace('\\components\\com_plot\\views\\', '\\components\\com_plot\\views_parent\\', $this->_path['template'][$key]);
            $this->_path['template'][$key] = str_replace('/components/com_plot/views/', '/components/com_plot/views_parent/', $this->_path['template'][$key]);
        }
        
        $this->componentUrl = JURI::base() . 'components/com_plot';
        $this->templateUrl = JURI::base().'templates/'.JFactory::getApplication()->getTemplate();
    }
    
}
