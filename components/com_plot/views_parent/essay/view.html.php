<?php

defined('_JEXEC') or die;

class PlotViewEssay extends JViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'profileEdit';

    public function display($tpl = null)
    {
        $app = JFactory::getApplication();


        $this->templateUrl = JURI::base().'templates/'.$app->getTemplate();
        $this->componentUrl = JURI::base().'components/com_plot';



        return parent::display($tpl);
    }

}
