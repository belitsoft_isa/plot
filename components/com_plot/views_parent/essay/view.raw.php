<?php
defined('_JEXEC') or die;
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/friends.php');
require_once JPATH_COMPONENT.'/views_parent/view.php';

class PlotViewEssay extends PlotViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'essay';

    public function display($tpl = null)
    {

        $app = JFactory::getApplication();

        return parent::display($tpl);
    }


}
