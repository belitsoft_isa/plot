<?php
defined('_JEXEC') or die;
require_once JPATH_COMPONENT.'/views_parent/view.php';

class PlotViewPublications extends PlotViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'publications';

    public function display($tpl = null)
    {
        return parent::display($tpl);
    }

    public function ajaxRenderBooksList()
    {
        $this->my = plotUser::factory();
        return $this->loadTemplate();
    }

}