<?php
defined('_JEXEC') or die;

?>

<script type="text/javascript" src="<?php echo JUri::root() . 'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate() . '/js/jquery.jscrollpane.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate().'/js/jquery.mousewheel.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate() . '/js/jcarousel.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/js/jcarousel.basic.js"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/assets/js/plot.js'; ?>"></script>

<!--<script type="text/javascript" src="<?php echo JUri::root()  . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/js/jplot.js'; ?>"></script>-->

<script src="<?php echo JURI::base() . 'components/com_plot'; ?>/libraries/scrolling_ajax_pagination/javascript.js"></script>

<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function () {
       // showFilteredAndSortedBooks();
        //setAxaxScrollPagination();

        // courses tabs click
        jQuery('.show-only-my-courses > a').click(function(){
            jQuery(this).addClass('ui-state-active');
            jQuery('.show-only-my-courses').addClass('ui-state-active')
            showFilteredAndSortedBooks();
            jQuery('#parent-all-site-books').css('display', 'block');
            jQuery('.jcarousel').jcarousel('reload');
        });
        jQuery('.show-all-courses > a').click(function(){
            jQuery('.show-only-my-courses').removeClass('ui-state-active');
            showFilteredAndSortedBooks();
            jQuery('#parent-all-site-books').css('display', 'block');
            jQuery('.jcarousel').jcarousel('reload');
        });
        jQuery('.show-my-reading-books > a').click(function(){
            jQuery('#parent-library-jcarousel').css('display', 'block');
            jQuery('.jcarousel').jcarousel('reload');
        });
        jQuery('#par-sel-course-1, #parent-course-filter, #parent-my-age').selectmenu({
            appendTo:'.main-content',
            change: function(){ showFilteredAndSortedBooks(); }
        });
        jQuery('#par-sel-course-1').selectmenu({open: function( event, ui ) { jQuery('#par-sel-course-1-menu').jScrollPane(); }});
        jQuery('#parent-course-filter').selectmenu({open: function( event, ui ) { jQuery('#parent-course-filter-menu').jScrollPane(); }});
        jQuery('#parent-my-age').selectmenu({open: function( event, ui ) { jQuery('#parent-my-age-menu').jScrollPane(); }});
        jQuery(window).scroll(function(){
            jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
        });
        jQuery("#parent-library").tabs();
        jQuery("#parent-all-courses-site, #parent-my-interest,#parent-my-age, #par-sel-course-1, #parent-course-filter").selectmenu();
        jQuery("#parent-my-courses, #parent-my-interest-1, #parent-my-age-1, #par-sel-course-1-1, #parent-course-filter-1").selectmenu();

        <?php
        if(plotUser::factory()->isHaveBooks()){
        ?>
        jQuery('.show-only-my-courses > a').click();
        <?php
        }else{
        ?>
        jQuery('.show-all-courses > a').click();
        <?php
        }
        ?>


    });
    
    function showFilteredAndSortedBooks() {
        setAxaxScrollPagination();

        var tagsIds = jQuery("[id^=filter-courses-tag]:checked").map(function () {
            return jQuery(this).val();
        }).get();
        jQuery('#courses-count-founded').css('visibility', 'hidden');
        jQuery('#books-list').html('<div id="loading-bar"><img src="<?php echo $this->baseurl ?>/templates/plot/img/pre-loader-1.gif" /></div>');

        jQuery.post(
            'index.php?option=com_plot&task=publications.ajaxGetFilteredAndSortedBooks',
            {
                ageId: jQuery('#parent-my-age').val(),
                tagsIds: tagsIds,
                categoryId: jQuery('#par-sel-course-1').val(),
                myCoursesOnly: jQuery('.show-only-my-courses').hasClass('ui-state-active') ? 1 : 0,
                sort: jQuery('#parent-course-filter').val(),
                limit: '<?php echo plotGlobalConfig::getVar('booksResultsShowFirstCountParent') + plotGlobalConfig::getVar('booksResultsShowFirstCountParent'); ?>'
            },
            function (response) {
                var data = jQuery.parseJSON(response);
                jQuery('#courses-count-founded').html('Найдено <span>'+jPlot.helper.declOfNum(data.countBooks, ['книга', 'книги', 'книг'])+'</span>');
                jQuery('#books-list').html(data.renderedBooks);
                jQuery('#courses-count-founded').css('visibility', 'visible');
                jPlotUp.Arrow.initialize('positionCameras');
            }


        );

    }
    
    function setAxaxScrollPagination() {
        var tagsIds = jQuery("[id^=filter-courses-tag]:checked").map(function () {
            return jQuery(this).val();
        }).get();
        var ajaxScrollData = {
            ageId: jQuery('#parent-my-age').val(),
            tagsIds: tagsIds,
            categoryId: jQuery('#par-sel-course-1').val(),
            myCoursesOnly: jQuery('.show-only-my-courses').hasClass('ui-state-active') ? 1 : 0,
            sort: jQuery('#parent-course-filter').val()
        };

        jQuery(window).unbind('scroll');

        jQuery('#books-list').scrollPagination({
            nop: <?php echo plotGlobalConfig::getVar('booksResultsShowFirstCountParent');?>,
            offset: <?php echo plotGlobalConfig::getVar('booksResultsShowFirstCountParent');?>,
            error: 'No More Posts!',
            delay: 50,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=publications.ajaxLoadMore'); ?>',
            appendDataTo: 'books-list',
            userData: ajaxScrollData,
            afterLoad: {
                afterLoadAction: function () {
                    jPlotUp.Arrow.initialize('positionCameras');
                }
            }
        });

        jQuery(window).scroll(function(){
            jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
        });

    }

    function checkAllFilterCoursesTagsCheckboxes() {
        jQuery("[id^=filter-courses-tag]").prop('checked', true);
        showFilteredAndSortedBooks();
    }

    function clearAllFilterCoursesTagsCheckboxes() {
        jQuery("[id^=filter-courses-tag]").prop('checked', false);
        showFilteredAndSortedBooks();
    }

    document.addEventListener("touchmove", function(){
        jQuery('#books-list').trigger('scroll');
        console.log('ScrollStart');
    }, false);
</script>

<main class="parent-profile">
    <div class="top parent-profile">
        <?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_header')); ?>
        <?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_top_menu')); ?>
    </div>
    <div class="wrap main-wrap parent-profile">
        <div class="add-left aside-left parent-profile"></div>
        <div class="add-right aside-right parent-profile"></div>
        <section class="parent-library">
            <div id="parent-library">
                <ul class="parent-courses-think-tabs">
                    <!--<li class="show-my-reading-books"><a href="#parent-library-jcarousel">Раздел читать</a></li>-->
                    <li class="show-all-courses"><a href="#parent-all-site-books">Общий раздел всех книг</a></li>
                    <li class="show-only-my-courses"><a href="#parent-all-site-books">Все мои книги</a></li>
                </ul>
                <!--<div id="parent-library-jcarousel" class="jcarousel-wrapper parent-profile">
                    <div class="jcarousel">
                        <ul class="parent-profile parent-library">
                            <?php if ($this->books_on_reading_shelf) { ?>
                                <?php foreach ($this->books_on_reading_shelf AS $currentBook) { ?>
                                    <li>
                                        <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $currentBook->c_id); ?>">
                                            <?php if ($currentBook->new) { ?>
                                                <span style="z-index: 10; margin-top: -0.8em;" class="arrow-bottom">Новая</span>
                                            <?php } else { ?>
                                                <span class="in-progress">Прочитано <?php echo round($currentBook->percent); ?> %</span>
                                            <?php } ?>
                                            <figure><img src="<?php echo $currentBook->thumbnailUrl;; ?>"/></figure>
                                        </a>
                                        <button class="add hover-shadow"
                                                onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $currentBook->c_id); ?>'">
                                            Читать
                                        </button>
                                    </li>
                                <?php } ?>
                            <?php } else { ?>
                                <li>
                                    <div class="no-items"><?php echo JText::_('COM_PLOT_NO_BOOKS_FOUND'); ?></div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <p class="jcarousel-pagination"></p>
                </div>-->
                <div id="parent-all-site-books">
                    <div class="select-feed">
                        <form>
                            <fieldset>
                                <select id="parent-my-interest-1">
                                    <option value="all" selected="selected">Все интересы</option>
                                    <option value="my">Мои интересы</option>
                                    <?php foreach ($this->my->childrensIds AS $childId) { ?>
                                        <option value="child-<?php echo $childId; ?>">Интересы
                                            ребенка <?php echo plotUser::factory($childId)->name; ?></option>
                                    <?php } ?>
                                </select>
                                <script type="text/javascript">
                                    jQuery( "#parent-my-interest-1" ).selectmenu({
                                        appendTo:'.main-content',
                                        open: function( event, ui ) { jQuery('#parent-my-interest-1-menu').jScrollPane(); },
                                        select: function(event, ui){

                                            switch (ui.item.value) {
                                                case ('my'):
                                                    jQuery('[id^=filter-courses-tag]').prop('checked', false);
                                                    jQuery.post(
                                                        '<?php echo JRoute::_('index.php?option=com_plot&task=publications.ajaxGetMyTags'); ?>',
                                                        function(response) {
                                                            var data = jQuery.parseJSON(response),
                                                                i= 0,
                                                                data_count=data.length,
                                                                str='';
                                                            jQuery('[id^=filter-courses-tag]').prop('checked', false);
                                                            for (i; i<data_count; i++) {
                                                                str+='<div>'+data[i].id+'</div>';
                                                            }
                                                            jQuery('hidden-data.interests-data .my').html(str);
                                                            jQuery('hidden-data.interests-data .my div').each(function(){
                                                                var tagId = jQuery(this).html();
                                                                jQuery('#filter-courses-tag'+tagId).prop('checked', true);

                                                            });
                                                            showFilteredAndSortedBooks();
                                                        }
                                                    );
                                                    break;
                                                case ('all'):
                                                    jQuery('[id^=filter-courses-tag]').prop('checked', true);
                                                    showFilteredAndSortedBooks();
                                                    break;
                                                default:
                                                    jQuery('[id^=filter-courses-tag]').prop('checked', false);
                                                    jQuery('hidden-data.interests-data .'+jQuery(this).val()+' div').each(function(){
                                                        var tagId = jQuery(this).html();
                                                        jQuery('#filter-courses-tag'+tagId).prop('checked', true);
                                                    });
                                                    showFilteredAndSortedBooks();
                                            }
                                        }
                                    });
                                </script>
                                <!-- hidden my interests and my children's interests for javascript and fast no-ajax choosing interests -->
                                <hidden-data class="hidden interests-data">
                                    <item class="all">
                                        <?php foreach ($this->plotTags AS $i => $tag) { ?>
                                            <div><?php echo $tag->id; ?></div>
                                        <?php } ?>
                                    </item>
                                    <item class="my">
                                        <?php foreach ($this->my->getTags() AS $tag) { ?>
                                            <div><?php echo $tag->id; ?></div>
                                        <?php } ?>
                                    </item>
                                    <?php foreach ($this->my->childrensIds AS $childId) { ?>
                                        <item class="child-<?php echo $childId; ?>">
                                            <?php foreach (plotUser::factory($childId)->getTags() as $childTag) { ?>
                                                <div><?php echo $childTag->id; ?></div>
                                            <?php } ?>
                                        </item>
                                    <?php } ?>
                                </hidden-data>
                                <select id="parent-my-age" onchange="showFilteredAndSortedBooks();">
                                    <option value="0"><?php echo JText::_('COM_PLOT_ALL_AGES'); ?></option>
                                    <?php foreach ($this->plotAges AS $age) { ?>
                                        <option value="<?php echo $age->id; ?>"><?php echo $age->title; ?></option>
                                    <?php } ?>
                                </select>
                            </fieldset>
                        </form>
                        <a class="link-back" href="<?php echo $this->referrerUrl; ?>">Назад</a>
                    </div>
                    <div class="checkbox-interest jcarousel-wrapper">
                        <div class="jcarousel">
                            <ul class="parent-profile">
                                <?php foreach ($this->plotTags AS $i=>$tag) { ?>
                                    <?php echo ($i % 6) ? '' : '<li>'; ?>
                                    <input type="checkbox" checked="checked" id="filter-courses-tag<?php echo $tag->id; ?>" value="<?php echo $tag->id; ?>" onchange="showFilteredAndSortedBooks();" />
                                    <label for="filter-courses-tag<?php echo $tag->id; ?>"><?php echo $tag->title; ?></label>
                                <?php } ?>
                                <?php echo ($i % 6) ? '' : '</li>'; ?>
                            </ul>
                        </div>
                        <button onclick="checkAllFilterCoursesTagsCheckboxes();">Выбрать все</button>
                        <button onclick="clearAllFilterCoursesTagsCheckboxes();">Очистить</button>
                        <p class="jcarousel-pagination"></p>
                    </div>
                    <div class="filter-result">
                        <form>
                            <fieldset>
                                <p>
                                    <label for="par-sel-course-1" id="courses-count-founded" >Найдено 39 курсов</label>
                                    <select id="par-sel-course-1" onchange="showFilteredAndSortedBooks();">
                                        <option value="0"><?php echo JText::_('COM_PLOT_ALL_CATEGORIES'); ?></option>
                                        <?php foreach ($this->booksCategories AS $bookCategory) { ?>
                                            <option
                                                value="<?php echo $bookCategory->value; ?>"><?php echo $bookCategory->text; ?></option>
                                        <?php } ?>
                                    </select>
                                </p>
                                <p>
                                    <label for="parent-course-filter">Сортировать:</label>
                                    <select id="parent-course-filter" onchange="showFilteredAndSortedBooks();">
                                        <option
                                            value="pub.c_title asc"><?php echo JText::_('COM_PLOT_SORT_BY_A_Z'); ?></option>
                                        <!--<option
                                            value="pub.c_title desc"><?php echo JText::_('COM_PLOT_SORT_BY_PROGRAMMS_AND_COMPANIES'); ?></option>-->
                                        <option
                                            value="pub.c_created_time desc"><?php echo JText::_('COM_PLOT_SORT_BY_LAST_ADDED'); ?></option>
                                        <option
                                            value="COUNT(b.book_id) asc"><?php echo JText::_('COM_PLOT_SORT_BY_POPULAR'); ?></option>
                                        <option
                                            value="book_cost desc"><?php echo JText::_('COM_PLOT_SORT_BY_PRICE'); ?></option>
                                    </select>
                                </p>
                            </fieldset>
                        </form>
                    </div>
                    <ul class="parent-profile no-jcarousel parent-library" id="books-list">
                    </ul>
                </div>

            </div>
        </section>
    </div>
</main>


<div class="modal-loading"><!-- Place at bottom of page --></div>
