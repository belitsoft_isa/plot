<?php
defined('_JEXEC') or die;

require_once(JPATH_SITE . '/administrator/components/com_html5flippingbook/libs/VarsHelper.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/conversations.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/friends.php');
require_once(JPATH_SITE . '/components/com_plot/helpers/html5fbfront.php');

class PlotViewPublications extends PlotViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'publications';

    public function display($tpl = null)
    {   
        $model = JModelLegacy::getInstance('profile', 'plotModel');
        $modelPublications = JModelLegacy::getInstance('publications', 'plotModel');
        $this->my = plotUser::factory();
        $this->readingBookListBS = $this->getReadingBookListBS();
        
        $this->readListBS = $model->getReadBooks();
        $this->newBookListBS = $model->getNewBooks();

        $this->books_on_reading_shelf = array();
        $this->count_new = count($this->newBookListBS);
        $count_reading = count($this->readingBookListBS);

        if ($this->count_new <= (int) plotGlobalConfig::getVar('countBooksOnShelfReadDepartment')) {
            $this->books_on_reading_shelf = $this->newBookListBS;
            
            $bookOnShelfIds = array();
            foreach ($this->books_on_reading_shelf AS $bookOnShelf) {
                $bookOnShelfIds[] = $bookOnShelf->c_id;
            }
            
            if ($this->readingBookListBS) {
                if ($count_reading <= ((int) plotGlobalConfig::getVar('countBooksOnShelfReadDepartment') - $this->count_new)) {
                    for ($i = 0; $i < $count_reading; $i++) {
                        if (!in_array($this->readingBookListBS[$i]->c_id, $bookOnShelfIds)) {
                            $this->books_on_reading_shelf[] = $this->readingBookListBS[$i];
                        }
                    }
                } else {
                    for ($i = 0; $i < ((int) plotGlobalConfig::getVar('countBooksOnShelfReadDepartment') - $this->count_new); $i++) {
                        if (!in_array($this->readingBookListBS[$i]->c_id, $bookOnShelfIds)) {
                            $this->books_on_reading_shelf[] = $this->readingBookListBS[$i];
                        }
                    }
                }
            }
        } else {
            for ($i = 0; $i < (int) getVar('countBooksOnShelfReadDepartment') - 1; $i++) {
                if (!in_array($this->newBookListBS[$i]->c_id, $bookOnShelfIds)) {
                    $this->books_on_reading_shelf[] = $this->newBookListBS[$i];
                }
            }
        }
        
        $this->referrerUrl = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : JRoute::_('index.php?option=com_plot&view=river');

        $this->my = plotUser::factory();
        $this->my->level = $this->my->getLevel();
        $this->my->childrensIds = $this->my->getChildrenIds();

        $this->plotAges = plotAges::getList();
        $this->plotTags = plotTags::getK2TagsList();

        $this->booksCategories = $modelPublications->getCategories();
        $this->setOgMeta();
        return parent::display($tpl);
    }
    
    private function getReadingBookListBS()
    {
        $model = JModelLegacy::getInstance('profile', 'plotModel');
        $this->readingBookListBS = $model->getItems();
        if ($this->readingBookListBS) {
            foreach ($this->readingBookListBS AS $row) {
                $thumbnailPath = JPATH_SITE.'/media/com_html5flippingbook'.'/thumbs/'.$row->c_thumb;
                if ($row->c_thumb == "" || !is_file($thumbnailPath)) {
                    $row->thumbnailUrl = JURI::root()."images/com_plot/def_book.jpg";
                } else {
                    if (file_exists(JPATH_BASE.'/media/com_html5flippingbook'.'/thumbs/thimb_'.$row->c_thumb)) {
                        $row->thumbnailUrl = JURI::root()."media/com_html5flippingbook/thumbs/thimb_".$row->c_thumb;
                    } else {
                        $row->thumbnailUrl = JURI::root()."images/com_plot/def_book.jpg";
                    }
                }
                $row->new = 0;
                if ($row->read == 1) {
                    $row->percent = 100;
                }


                $row->publicationLink = 'index.php?option=com_html5flippingbook&view=publication&id='.$row->c_id;
            }
        }            
    }

    private function setOgMeta()
    {
        require_once JPATH_ADMINISTRATOR.'/components/com_plot/plot_social_config.php';
        $metaOgUrl = '<meta property="og:url" content="http://naplotu.com/publications/" />';
        $metaOgTitle = '<meta property="og:title" content="'.plotSocialConfig::get('publicationsTitle').'" />';
        $metaOgDescription = '<meta property="og:description" content="'.plotSocialConfig::get('publicationsDesc').'" />';
        $metaOgImage = '<meta property="og:image" content="'.JUri::root().plotSocialConfig::get('publicationsImagePath').'" />';
        $metaOgImageType = '<meta property="og:image:type" content="image/jpeg" />';
        $metaOgImageWidth = '<meta property="og:image:width" content="'.plotSocialConfig::get('publicationsImageWidth').'" />';
        $metaOgImageHeight = '<meta property="og:image:height" content="'.plotSocialConfig::get('publicationsImageHeight').'" />';
        $metaOgSiteName = '<meta property="og:site_name" content="'.JFactory::getConfig()->get('sitename').'" />';
        $metaOgFbAppId = '<meta property="fb:app_id" content="473991812742077" />';
        $metaOgType = '<meta property="og:type" content="website" />';
        $metaOgLocale = '<meta property="og:locale" content="ru_RU" />';
        JFactory::getDocument()->addCustomTag($metaOgTitle.$metaOgSiteName.$metaOgUrl.$metaOgDescription.$metaOgFbAppId.$metaOgType.$metaOgLocale.$metaOgImage.$metaOgImageType.$metaOgImageWidth.$metaOgImageHeight);
        return true;
    }    
    
    
}