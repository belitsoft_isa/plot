<?php

defined('_JEXEC') or die;
require_once JPATH_COMPONENT.'/views_parent/view.php';

class PlotViewPhotos extends PlotViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'courses';

    public function display($tpl = null)
    {

        return parent::display($tpl);
    }
    
    public function ajaxRenderCoursesList()
    {
        return $this->loadTemplate();
    }
    
    public function ajaxRenderList()
    {
        return $this->loadTemplate();
    }

    public function social($url, $title,$description,$img='')
    {
        ob_start(); ?>

        <div class="share42init" data-zero-counter="1" data-image="<?php echo $img; ?>" data-url="<?php echo  $url;?>" data-title="<?php echo  $title;?>"  ></div>

        <?php
        return ob_get_clean();
    }
}
