<?php
defined('_JEXEC') or die;
?>

<?php foreach ($this->publications AS $publication) { ?>
<li class="parent-books">
    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&id='.$publication->c_id); ?>">
        <h6>
            <p class="stream-title"><?php echo $publication->c_title; ?></p>
        </h6>
        <hr>
        <!--<span><?php echo $publication->finished_date;?></span>-->
        <div class="circle-img">
            <svg class="clip-svg">
                <image style="clip-path: url(#clipping-circle);" width="100%" height="100%" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $publication->thumbnailUrl; ?>"></image>
            </svg>
            <i class="activity-icons">
                <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#book"></use>
                </svg>
            </i>
        </div>
    </a>
</li>
<?php } ?>

