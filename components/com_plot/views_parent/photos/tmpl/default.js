//<editor-fold defaultstate="collapsed" desc="STRING FUNCTIONS">
function strpos(haystack, needle, offset) {
    var i = (haystack + '')
        .indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
}

function strripos(haystack, needle, offset) {
    haystack = (haystack + '')
        .toLowerCase();
    needle = (needle + '')
        .toLowerCase();
    var i = -1;
    if (offset) {
        i = (haystack + '')
            .slice(offset)
            .lastIndexOf(needle); // strrpos' offset indicates starting point of range till end,
        // while lastIndexOf's optional 2nd argument indicates ending point of range from the beginning
        if (i !== -1) {
            i += offset;
        }
    } else {
        i = (haystack + '')
            .lastIndexOf(needle);
    }
    return i >= 0 ? i : false;
}

function substr(str, start, len) {
    var i = 0,
        allBMP = true,
        es = 0,
        el = 0,
        se = 0,
        ret = '';
    str += '';
    var end = str.length;

    // BEGIN REDUNDANT
    this.php_js = this.php_js || {};
    this.php_js.ini = this.php_js.ini || {};
    // END REDUNDANT
    switch ((this.php_js.ini['unicode.semantics'] && this.php_js.ini['unicode.semantics'].local_value.toLowerCase())) {
        case 'on':
            // Full-blown Unicode including non-Basic-Multilingual-Plane characters
            // strlen()
            for (i = 0; i < str.length; i++) {
                if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
                    allBMP = false;
                    break;
                }
            }

            if (!allBMP) {
                if (start < 0) {
                    for (i = end - 1, es = (start += end); i >= es; i--) {
                        if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
                            start--;
                            es--;
                        }
                    }
                } else {
                    var surrogatePairs = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;
                    while ((surrogatePairs.exec(str)) != null) {
                        var li = surrogatePairs.lastIndex;
                        if (li - 2 < start) {
                            start++;
                        } else {
                            break;
                        }
                    }
                }

                if (start >= end || start < 0) {
                    return false;
                }
                if (len < 0) {
                    for (i = end - 1, el = (end += len); i >= el; i--) {
                        if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
                            end--;
                            el--;
                        }
                    }
                    if (start > end) {
                        return false;
                    }
                    return str.slice(start, end);
                } else {
                    se = start + len;
                    for (i = start; i < se; i++) {
                        ret += str.charAt(i);
                        if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
                            se++; // Go one further, since one of the "characters" is part of a surrogate pair
                        }
                    }
                    return ret;
                }
                break;
            }
        // Fall-through
        case 'off':
        // assumes there are no non-BMP characters;
        //    if there may be such characters, then it is best to turn it on (critical in true XHTML/XML)
        default:
            if (start < 0) {
                start += end;
            }
            end = typeof len === 'undefined' ? end : (len < 0 ? len + end : len + start);
            // PHP returns false if start does not fall within the string.
            // PHP returns false if the calculated end comes before the calculated start.
            // PHP returns an empty string if start and end are the same.
            // Otherwise, PHP returns the portion of the string from start to end.
            return start >= str.length || start < 0 || start > end ? !1 : str.slice(start, end);
    }
    return undefined; // Please Netbeans
}
//</editor-fold>

function removeSearchActiveClassTab() {
    jQuery('#children-btn').removeClass('ui-tabs-active');
    jQuery('#photos-list').css({'display': 'none'});
    jQuery('#adults-btn').removeClass('ui-tabs-active');
    jQuery('#child-videos').css({'display': 'none'});
    jQuery('#link-child-certificates').removeClass('ui-tabs-active');
    jQuery('#child-certificates').css({'display': 'none'});
}

function setActiveTab(tab, search_area) {
    jQuery('#' + tab).addClass('active');
    jQuery('#' + search_area).css({'display': 'block'});
}

jQuery(document).ready(function (jQuery) {
    
    SqueezeBox.initialize();

    jQuery(".fancybox").fancybox({
        maxWidth	: 800,
        maxHeight	: 600,
        fitToView	: false,
        width		: '70%',
        height		: '70%',
        autoSize	: false,
        closeClick	: false,
        openEffect	: 'none',
        closeEffect	: 'none',
        helpers:  {
            title : {
                type : 'inside',
                position: 'top'
            }
        },
        beforeLoad: function() {
            this.title = jQuery(this.element).attr('data-title'),
            this.description = jQuery(this.element).attr('data-description'),
            this.date = jQuery(this.element).attr('data-date'),
                this.share = jQuery(this.element).attr('data-share');
        }
    });

    jQuery('#certificate-close-button').hide();
    jQuery('#photo-close-button').hide();
    jQuery('#event-close-button').hide();
    jQuery('#portfolio-close-button').hide();
    jQuery('#videofile-close-button').hide();
    jQuery('#youtube-close-button').hide();

    jQuery('#link-child-photos').on('click', function () {
        jQuery('#photos-list').html('');
        setPhotoAxaxScrollPagination();
        jQuery('.main-wrap-overflow').trigger('scroll');
        jQuery('#count_all_photos').html(jQuery('#photos-count').val());
    });
    jQuery('#link-child-certificates').on('click', function () {
        jQuery('#child-certificates').html('');
        setCertificatesAxaxScrollPagination();
        jQuery('.main-wrap-overflow').trigger('scroll');
        jQuery('#count_all_certificates').html(jQuery('#certificates-count').val());
    });
    jQuery('#link-child-videos').on('click', function () {
        jQuery('#child-videos').html('');
        setVideosAxaxScrollPagination();
        jQuery('.main-wrap-overflow').trigger('scroll');
        jQuery('#count_all_videos').html(jQuery('#videos-count').val());
    });
    jQuery('#link-child-courses').on('click', function () {
        jQuery('#child-courses').html('');
        ajaxLoadCourses();
    });
    jQuery('#link-child-publications').on('click', function () {
        jQuery('#child-publications').html('');
        ajaxLoadPublications();
    });
    jQuery('#link-child-events').on('click', function () {
        jQuery('#child-events').html('');
        jQuery('#loading-bar').show();
        ajaxLoadEvents();
    });
    
    jQuery('#link-child-activity').on('click', function () {
        jQuery('#child-videos').html('');
        jQuery('#child-certificates').html('');
        jQuery('#photos-list').html('');
        removeSearchActiveClassTab();
        setActiveTab('link-child-activity', 'child-add-activity');
    });

    // modalLoad();
    jQuery("#parent-add-activity, #parent-add-video").tabs();

    jQuery('#parent-video-link').blur(function () {
        var link = jQuery('#parent-video-link').val(),
            expression = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gi,
            regex = new RegExp(expression),
            youtubeNumber = '',
            video_img = jQuery('#video-youtube-img').attr('src'),
            pos = '',
            youTubeURL,
            json;

        if (video_img.indexOf("blank150x150.jpg") != -1) {
            removeErrorClass();
            jQuery('.pre-loader').show();
            if (link.match(regex)) {
                if (strpos(link, 'youtube.com') !== false || strpos(link, 'youtu.be') !== false) {
                    if (strripos(link, 'youtube.com') !== false) {
                        youtubeNumber = substr(link, strripos(link, '?v=') + 3);
                        if ((pos = strpos(youtubeNumber, '&')) !== false) youtubeNumber = substr(youtubeNumber, 0, pos)
                        {
                            youTubeURL ='https://www.googleapis.com/youtube/v3/videos?id=' + youtubeNumber + '&key=AIzaSyBDjpX4FVkv1CqrGSZqzGAxU1aTrwWA7y4&part=snippet';

                            json = (function () {
                                var json = null;
                                jQuery.ajax({
                                    'async': false,
                                    'global': false,
                                    'url': youTubeURL,
                                    'dataType': "json",
                                    'success': function (data) {
                                        json = data;
                                        if (json) {
                                            jQuery('#parent-video-link-ahead-1').val(json.items[0].snippet.title);
                                            jQuery('#parent-video-link-descr-1').val(json.items[0].snippet.description);
                                        }
                                    }
                                });
                            })();
                            jQuery.post('index.php?option=com_plot&task=profile.uploadVideoImg', {youtubeNumber: youtubeNumber}, function (response) {
                                if (response.status) {
                                    jQuery('#video-modal-link').attr('href', 'index.php?option=com_plot&task=profile.ajaxVideoImage&img_name=' + youtubeNumber);
                                    document.getElementById('video-modal-link').click();
                                    jQuery('.pre-loader').css('display', 'none');
                                } else {
                                    jQuery('.plot-video-link-required').each(function() {
                                        if(jQuery(this).val()==''){
                                            addErrorClass(jQuery(this));
                                        }
                                    });
                                }
                            });
                        }
                    } else {
                        alert('youtube.com');
                        jQuery('.pre-loader').css('display', 'none');
                    }
                } else {
                    alert('youtube.com');
                    jQuery('.pre-loader').css('display', 'none');
                }
            } else {
                jQuery('.plot-video-link-required').each(function() {
                    if (jQuery(this).val()=='') {
                        addErrorClass(jQuery(this));
                    }
                });
                jQuery('.pre-loader').css('display', 'none');
            }
        }
    });
    
    jQuery( "#parent-media-tabs" ).tabs();
    
    var urlHash = window.location.hash;
    switch(urlHash) {
        case '#videos':
            document.getElementById('link-child-videos').click();
            break;
        case '#certificates':
            document.getElementById('link-child-certificates').click();
            break;
        case '#courses':
            document.getElementById('link-child-courses').click();
            break;
        case '#publications':
            document.getElementById('link-child-publications').click();
            break;
        case '#events':
            document.getElementById('link-child-events').click();
            break;
        default:
            document.getElementById('link-child-photos').click();
    }    
});