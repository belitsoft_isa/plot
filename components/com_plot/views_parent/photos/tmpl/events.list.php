<?php
defined('_JEXEC') or die;
?>

<?php foreach ($this->events AS $event) { ?>
<li class="parent-meetings">
    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=event&id='.$event->id);?>">
        <h6>
            <span class="action-title">Добавлена встреча</span>
            <p class="stream-title"><?php echo $event->title;?></p>
        </h6>
        <hr>
        <span><?php echo $event->create_date;?></span>
        <div class="circle-img">
            <svg class="clip-svg">
            <image style="clip-path: url(#clipping-circle);" width="100%" height="100%" xmlns:xlink="http://www.w3.org/1999/xlink" 
                   xlink:href="<?php echo JUri::root().'images/com_plot/events/'.$event->user_id.'/thumb/'.$event->img; ?>">
            </image>
            </svg>
            <i class="activity-icons">
                <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#lamp-meeting"></use></svg>
            </i>
        </div>
        <p><?php echo PlotHelper::cropStr(strip_tags($event->description),  plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></p>
    </a>
</li>
<?php } ?>
