<?php
defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
JHtml::_('formbehavior.chosen', 'select');
$id = JFactory::getApplication()->input->get('id', 0, 'INT');
?>

<script type="text/javascript" src="<?php echo JUri::root() . 'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate() . '/js/jquery.jscrollpane.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate() . '/js/jcarousel.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/js/jcarousel.basic.js"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/assets/js/plot.js'; ?>"></script>
<script src="<?php echo JURI::base() . 'components/com_plot'; ?>/libraries/scrolling_ajax_pagination/javascript.js"></script>
<script src="<?php echo JURI::base() . 'components/com_plot'; ?>/views_parent/photos/tmpl/default.js"></script>
<script type="text/javascript" src="<?php echo JURI::base() . 'components/com_plot'; ?>/assets/js/jquery.fancybox.js"></script>

<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/libraries/preview_master_plugin/jquery.preimage.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/libraries/jquery_input_mask_plugin/jquery.mask.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/views_parent/profile/tmpl/default.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/js/jquery.mousewheel.js"></script>
<script src="<?php echo $this->templateUrl; ?>/js/all.fineuploader-5.0.8.min.js"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/assets/js/plotEssay.js'; ?>"></script>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">


function modalLoad() {
    var modal = jQuery('.modal-overlay');
    <?php
     if (isset($this->video_links) && $this->video_links) {
     foreach($this->video_links AS $video){
     ?>
    jQuery('#parent-video-id<?php echo $video->id; ?>, a[href="#parent-video-id<?php echo $video->id; ?>"]').click(function () {
        modal.css('display', 'block');
    });
    jQuery('#parent-video-id<?php echo $video->id; ?>, a[href="#parent-video-id<?php echo $video->id; ?>"]').click(function () {
        modal.css('display', 'block');
    });
    <?php
     }
     }
    ?>
    jQuery('#parent-video-id1, a[href="#parent-video-id1"]').click(function () {
        modal.css('display', 'block');
    });
    jQuery('.modal-close').click(function () {
        modal.css('display', 'none');
    });
    modal.click(function (event) {
        e = event || window.event;
        if (e.target == this) {
            jQuery(modal).css('display', 'none');
        }
    });
}

function setPhotoAxaxScrollPagination() {
    var widthItem = 182,
        heightItem = 409,
        HeightScreen = parseInt(screen.height);
    removeSearchActiveClassTab();
    setActiveTab('link-child-photos', 'photos-list');

    var ajaxScrollData = {
        search: jQuery("#plot-search").val(),
        screenHeight: parseInt(screen.height) - parseInt(jQuery('div.parent-profile').height()),
        screenWidth: jQuery('#photos-list').outerWidth(),
        itemHeight: heightItem,
        itemWidth: widthItem,
        id:<?php echo $id; ?>,
        afterLoad: {
            afterLoadAction:function(){
                social();
            }
        }
    };

    jQuery(window).unbind('scroll');

    jQuery('#photos-list').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('childPhotoCount'); ?>,
        offset: 0,
        error: 'No More Posts!',
        delay: 10,
        scroll: true,
        postUrl: 'index.php?option=com_plot&task=photos.ajaxPhotosLoadMore',
        appendDataTo: 'photos-list',
        userData: ajaxScrollData
    });

    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });
social();
}

function setVideosAxaxScrollPagination() {
    var widthItem = 182,
        heightItem = 409;
    removeSearchActiveClassTab();
    setActiveTab('link-child-videos', 'child-videos');

    var ajaxScrollData = {
        screenHeight: parseInt(screen.height) - parseInt(jQuery('div.parent-profile').height()),
        screenWidth: jQuery('#child-videos').outerWidth(),
        itemHeight: heightItem,
        itemWidth: widthItem,
        id:<?php echo $id; ?>
    };

    jQuery(window).unbind('scroll');

    jQuery('#child-videos').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('parentVideoCount'); ?>,
        offset: 0,
        error: 'No More Posts!',
        delay: 10,
        scroll: true,
        postUrl: 'index.php?option=com_plot&task=photos.ajaxVideosLoadMore',
        appendDataTo: 'child-videos',
        userData: ajaxScrollData,
        afterLoad: {
            afterLoadAction:function(){
                social();
            }
        }
    });

    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });
    social();
}

function setCertificatesAxaxScrollPagination() {
    var widthItem = 182,
        heightItem = 409;
    removeSearchActiveClassTab();
    setActiveTab('link-child-certificates', 'child-certificates');

    var ajaxScrollData = {
        screenHeight: parseInt(screen.height) - parseInt(jQuery('div.parent-profile').height()),
        screenWidth: jQuery('#child-certificates').outerWidth(),
        itemHeight: heightItem,
        itemWidth: widthItem,
        id:<?php echo $id; ?>
    };

    jQuery(window).unbind('scroll');

    jQuery('#child-certificates').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('parentCertificateCount'); ?>,
        offset: 0,
        error: 'No More Posts!',
        delay: 10,
        scroll: true,
        postUrl: 'index.php?option=com_plot&task=photos.ajaxCertificatesLoadMore',
        appendDataTo: 'child-certificates',
        userData: ajaxScrollData,
        afterLoad: {
            afterLoadAction:function(){
                social();
            }
        }
    });

    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });
    social();
}

function photoRemoveConfirm(photoId){
    SqueezeBox.open('<?php echo  JRoute::_("index.php?option=com_plot&task=photos.deleteConfirm&entity=photo", 0)?>'+'&id='+photoId, {size:{x:300, y:150}, handler:'ajax'});
}

function videoRemoveConfirm(videoId){
    SqueezeBox.open('<?php echo  JRoute::_("index.php?option=com_plot&task=photos.deleteConfirm&entity=video", 0);?>'+'&id='+videoId, {size:{x:300, y:150}, handler:'ajax'});
}

function certificateRemoveConfirm(certificateId) {
    SqueezeBox.open('<?php echo  JRoute::_("index.php?option=com_plot&task=photos.deleteConfirm&entity=certificate", 0)?>'+'&id='+certificateId, {size:{x:300, y:150}, handler:'ajax'});
}

function ajaxLoadCourses() {
    var courseFilterData = {
        userId: '<?php echo JRequest::getInt('id', plotUser::factory()->id);?>'
    };
    jQuery.post(
        'index.php?option=com_plot&task=photos.ajaxGetCourses',
        {userData: courseFilterData},
        function(response){
            if (response) {
                var data = jQuery.parseJSON(response);
                jQuery('#child-courses').append(data.renderedCourses);
                jQuery('#count_all_courses').html(jPlot.helper.getWordEnding(data.countCourses, 'курс', 'курса', 'курсов'));
            } else {
                jQuery('#count_all_courses').html('0 курсов');
            }
            jQuery('#loading-bar').hide();
        }
    );
    jQuery('#child-courses').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('photosPageChildCoursesCount'); ?>,
        offset: <?php echo plotGlobalConfig::getVar('photosPageChildCoursesCount'); ?>,
        error: '',
        delay: 10,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=photos.ajaxGetCourses'); ?>',
        appendDataTo: 'child-courses',
        userData: courseFilterData,
        afterLoad: {
            afterLoadAction:function(){
                social();
            }
        }
    });
    social();
}

function ajaxLoadPublications() {
    var courseFilterData = {
        userId: '<?php echo JRequest::getInt('id', plotUser::factory()->id);?>'
    };
    jQuery.post(
        'index.php?option=com_plot&task=photos.ajaxGetPublications',
        {userData: courseFilterData},
        function(response){
            if (response) {
                var data = jQuery.parseJSON(response);
                jQuery('#child-publications').append(data.renderedPublications);
                jQuery('#count_all_publications').html(jPlot.helper.getWordEnding(data.countPublications, 'книга', 'книги', 'книг'));
                jQuery('#loading-bar').hide();
            } else {
                jQuery('#loading-bar').hide();
                jQuery('#count_all_publications').html('0 книг');
            }
        }
    );
    jQuery('#child-publications').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('photosPageChildCoursesCount'); ?>,
        offset: <?php echo plotGlobalConfig::getVar('photosPageChildCoursesCount'); ?>,
        error: '',
        delay: 10,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=photos.ajaxGetPublications'); ?>',
        appendDataTo: 'child-publications',
        userData: courseFilterData,
        afterLoad: {
            afterLoadAction:function(){
                social();
            }
        }
    });
    social();
}

function ajaxLoadEvents() {
    var courseFilterData = {
        userId: '<?php echo JRequest::getInt('id', plotUser::factory()->id);?>'
    };
    jQuery.post(
        'index.php?option=com_plot&task=photos.ajaxGetEvents',
        {userData: courseFilterData},
        function(response){
            if (response) {
                var data = jQuery.parseJSON(response);
                jQuery('#child-events').append(data.renderedEvents);
                jQuery('#count_all_events').html(jPlot.helper.getWordEnding(data.countEvents, 'встреча', 'встречи', 'встреч'));
                jQuery('#loading-bar').hide();
            } else {
                jQuery('#loading-bar').hide();
                jQuery('#count_all_events').html('0 встреч');
            }
        }
    );
    social();
}



document.addEventListener("touchmove", function(){
    jQuery('.main-wrap-overflow').trigger('scroll');
    console.log('ScrollStart');
}, false);
</script>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="CSS"> ?>
<style type="text/css">
    .com_plot {
        color: #FCFCFC;
    }

    .more {
        margin: 5px;
    }

    .parent-courses-think-tabs a:not(.add):not(.add-recipient) {

        margin: 0.4em;
    }

    .parent-courses-think-tabs a {

        padding: 0 0.5em;

    }
</style>
<?php if ($this->isChildSeeToOtherParent) { ?>
<style type="text/css">
header.child-profile > .wrap.child-profile {
    background-color: #fff;
    background-image: none;
}
</style>
<?php } ?>
<?php # </editor-fold> ?>

<input type="hidden" id="photos-count" value="<?php echo PlotHelper::declension((int)$this->total_photos, array('фото', 'фото', 'фото')); ?>" />
<input type="hidden" id="videos-count" value="<?php echo PlotHelper::declension((int)$this->total_videos, array('видео', 'видео', 'видео')); ?>" />
<input type="hidden" id="certificates-count" value="<?php echo PlotHelper::declension((int)$this->total_certificates , array('сертификат', 'сертификата', 'сертификатов')); ?>" />

<main class="parent-profile">
    <div class="top parent-profile">
        <?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_header')); ?>
        <?php if ($this->isChildSeeToOtherParent || $this->isParentSeeToOtherParent) { ?>
        <div class="top">
            <div class="looking-at-user-header">
                <div class="wrap parent-profile">
                    <div class="name-row">
                        <span>Привет!</span>
                        <i>Я - <?php echo $this->user->name; ?><?php echo ($this->user->getSocialFieldData('ADDRESS')->city) ?  ',' : ''; ?></i>
                        <span><?php echo ($this->user->getSocialFieldData('ADDRESS')->city) ?  ($this->user->getSocialFieldData('ADDRESS')->city) : ''; ?></span>
                    </div>
                    <div class="user-top">
                        <a class="circle-img modal"
                           href="<?php echo JRoute::_('index.php?option=com_plot&task=profileedit.ajaxGetForeignChildProfilePopupHtml&id=' . (int)$this->user->id); ?>"
                           rel="{size: {x: 744, y: 500}, handler:'iframe', iframeOptions: {scrolling: 'no'}}">
                            <img src="<?php echo $this->user->getSquareAvatarUrl(); ?>" alt="<?php echo $this->user->name; ?>" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if (!$this->isChildSeeToOtherParent && !$this->isParentSeeToOtherParent) { ?>
            <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_top_menu') ); ?>
        <?php } ?>
    </div>
    <div class="wrap main-wrap parent-profile">
        <div class="aside-left parent-profile"></div>
        <div class="aside-right parent-profile"></div>
        <section class="parent-media">
            <div id="parent-media-tabs">
                <ul class="parent-courses-think-tabs">
                    <li><a href="#parent-photos" id="link-child-photos">Все мои фото</a></li>
                    <li><a href="#parent-videos" id="link-child-videos">Все мои видео</a></li>
                    <li><a href="#parent-certificates" id="link-child-certificates">Все мои сертификаты</a></li>
                    <li><a href="#parent-courses" id="link-child-courses">Все мои курсы</a></li>
                    <li><a href="#parent-publications" id="link-child-publications">Все мои книги</a></li>
                    <li><a href="#parent-events" id="link-child-events">Все мои встречи</a></li>
                    <?php if ($this->my->id == $this->user->id) { ?>
                    <li><a href="#parent-add-activity" class="add" id="link-child-activity">Добавить</a></li>
                    <?php } else { ?>
                        <li></li>
                    <?php } ?>
                </ul>
                <div id="loading-bar" style="margin:1em 0 0 45%;"><img src="<?php echo $this->baseurl ?>/templates/plot/img/pre-loader-1.gif" /></div>
                <div id="parent-photos">
                    <h5>Всего <span id="count_all_photos"><?php echo PlotHelper::declension((int)$this->total_photos, array('фото', 'фото', 'фото')); ?></span></h5>
                    <ul class="parent-profile no-jcarousel" id="photos-list"></ul>
                </div>
                <div id="parent-videos">
                    <h5>Всего <span id="count_all_videos"><?php echo PlotHelper::declension((int)$this->total_videos, array('видео', 'видео', 'видео')); ?></span></h5>
                    <ul class="parent-profile no-jcarousel" id="child-videos"></ul>
                </div>
                <div id="parent-certificates">
                    <h5>Всего <span id="count_all_certificates"><?php echo PlotHelper::declension((int)$this->total_certificates , array('сертификат', 'сертификата', 'сертификатов')); ?></span></h5>
                    <ul class="parent-profile no-jcarousel" id="child-certificates"></ul>
                </div>
                <div id="parent-courses">
                    <h5>Всего <span id="count_all_courses"></span></h5>
                    <ul class="parent-profile no-jcarousel" id="child-courses"></ul>
                </div>
                <div id="parent-publications">
                    <h5>Всего <span id="count_all_publications"></span></h5>
                    <ul class="parent-profile no-jcarousel" id="child-publications"></ul>
                </div>
                <div id="parent-events">
                    <h5>Всего <span id="count_all_events"></span></h5>
                    <ul class="parent-profile no-jcarousel" id="child-events"></ul>
                </div>
                <?php if ($this->my->id == $this->user->id) {
                    require_once JPATH_COMPONENT.'/views_parent/profile/tmpl/default_add.php';
                } ?>
            </div>
        </section>
    </div>
</main>


<!--1453 1450 1449 1448 
1447 1446 1445 1443 
1444 1442 1441 1439 
1440 1438 1437 1436 
1435 1434 1433 1432 
1431 1430 1429 1428 
1427 1426 1423 1422 
1418 1416 1415 1414 
1079 907 902 888 
887 886 885 884 
883 881 880 879 
878 877 876 844 
836 712 711 710 
687 686 670 666 
661 660 659 655 
654 651 650 649 
648 647 646 640 
639 628 627 626 
625 624 623 622 
615 575 468 464 
300 298 245 242 
239 238 237 236 
235 234 233 232 -->