<?php
defined('_JEXEC') or die;
?>

<?php foreach ($this->courses AS $course) { ?>
<li>
    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=course&id='.$course->id); ?>">
        <h6><?php echo $course->course_name; ?></h6>
        <div class="circle-img">
            <svg class="clip-svg-course">
                <image style="clip-path: url(#clipping-circle-course);" width="100%" height="100%" xmlns:xlink="http://www.w3.org/1999/xlink" 
                       xlink:href="<?php echo JFile::exists(JPATH_ROOT.'/'.$course->image) ? JUri::root().$course->image : JUri::root().'templates/plot/img/blank150x150.jpg'; ?>">
                </image>
            </svg>
            <i class="activity-icons">
                <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#academic-hat"></use></svg>
            </i>
        </div>
        <p><?php echo PlotHelper::cropStr($course->course_description, plotGlobalConfig::getVar('courseDescriptionMaxSymbolsForDisplay'));?></p>
    </a>
</li>
<?php } ?>
