<?php
defined('_JEXEC') or die;
?>

<?php foreach ($this->certificates as $item) {
    ?>
    <li data-certificate-id="<?php echo $item->id; ?> ">
        <a class="fancybox" rel="certificates-set" data-title="<?php echo $item->title; ?>"
           data-description="<?php echo $item->caption; ?>"
           data-date="<?php echo JHtml::date($item->created, 'd.m.Y'); ?>"
           data-share=" data-image='<?php echo $item->original; ?>' data-url='<?php echo $item->original; ?>' data-title='<?php echo  addslashes($item->title);?>' "
           href="<?php echo $item->original; ?>">
            <figure>
                <img src="<?php echo $item->thumb; ?>" alt=""/>
                <i class="activity-icons">
                    <svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid meet"
                         style="fill:url(#svg-gradient);stroke:url(#svg-gradient);">
                        <use xlink:href="#zoom"></use>
                    </svg>
                </i>
            </figure>
        </a>
        <?php  if (plotUser::factory()->id && ((int)$this->id == plotUser::factory()->id)) {

            if ($item->album_type == 'plot-programs') {
                ?>
                <button class="add hover-shadow"
                        onclick="plotEssay.createMessage('Нельзя удалить сертификат'); return false;">Удалить
                </button>
            <?php } else { ?>
                <button class="add hover-shadow"
                        onclick="certificateRemoveConfirm('<?php echo $item->id; ?> '); return false;">Удалить
                </button>
            <?php
            }
        } ?>

    </li>
<?php
} ?>
