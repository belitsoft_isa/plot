<?php
defined('_JEXEC') or die;
?>
<?php foreach ($this->photos as $item) {
    ?>
    <li data-photo-id="<?php echo $item->id; ?> ">
        <a class="fancybox" rel="photo-set"
            <?php if ($item->type != 'plot-essay') { ?>
                data-title="<?php echo $item->title; ?>"
                data-description="<?php echo $item->caption; ?>"
            <?php } ?>

           data-date="<?php echo JHtml::date($item->created, 'd.m.Y'); ?>"
          data-share=" data-image='<?php echo $item->original; ?>' data-url='<?php echo $item->original; ?>' data-title='<?php echo  addslashes($item->title);?>' "
           href="<?php echo $item->original; ?>">
            <h6><?php echo $item->title; ?> </h6>
            <hr/>
            <span><?php echo JHtml::date($item->created, "Y-m-d H:m"); ?></span>

            <div class="circle-img">
                <svg class="clip-svg">
                    <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                           xlink:href="<?php echo $item->original; ?>" alt=""></image>

                </svg>
                <i class="activity-icons">
                    <svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid"
                         style="fill:url(#svg-gradient);stroke:url(#svg-gradient);">
                        <use xlink:href="#zoom"></use>
                    </svg>
                </i>
            </div>
            <p><?php echo PlotHelper::cropStr(strip_tags($item->caption), plotGlobalConfig::getVar('childPageAllDescriptionMaxSymbolsToShow')); ?></p>
        </a>
        <?php if (plotUser::factory()->id && ((int)$this->id == plotUser::factory()->id)) {
            if ($item->type == 'plot-essay') {
                ?>
                <button class="add hover-shadow"
                        onclick="plotEssay.createMessage('Нельзя удалить эссе'); return false;">Удалить
                </button>
            <?php } else { ?>
                <button class="add hover-shadow"
                        onclick="photoRemoveConfirm('<?php echo $item->id; ?> '); return false;">Удалить
                </button>
            <?php
            }

        } ?>

    </li>
<?php
} ?>
