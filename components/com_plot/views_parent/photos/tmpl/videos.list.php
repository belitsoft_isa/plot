<?php
defined('_JEXEC') or die;
?>

<?php foreach ($this->videos as $item) {
    if ($item->type == 'link' || $item->type == 'course') {
        if (strripos($item->path, 'youtube.com/') !== FALSE) {
            $youtubeNumber = substr($item->path, strripos($item->path, '?v=') + 3);
            if (($pos = strpos($youtubeNumber, '&')) !== FALSE)
                $youtubeNumber = substr($youtubeNumber, 0, $pos);
        } ?>
        <li data-video-id="<?php echo $item->id; ?>">
            <a class="fancybox" rel="videos-set" data-fancybox-type="ajax" data-title="<?php echo $item->title; ?>"
               data-description="<?php echo $item->description; ?>"
               data-date="<?php echo JHtml::date($item->date, 'd.m.Y'); ?>"
               data-share=" data-image='https://youtu.be/<?php echo $youtubeNumber; ?>' data-url='https://youtu.be/<?php echo $youtubeNumber; ?>' data-title='<?php echo addslashes($item->title); ?>'"
               href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxOpenVideo&youtubeNumber=' . $youtubeNumber); ?>">
                <h6><?php echo $item->title; ?></h6>
                <hr/>
                <span><?php echo JHtml::date($item->date, "d.m.Y"); ?></span>

                <div class="circle-img">
                    <svg class="clip-svg">
                        <?php
                        if ($item->type == 'course') {
                            if (file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'coursevideo' . DIRECTORY_SEPARATOR . $item->uid . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $youtubeNumber . '.jpg')) {
                                $img = JUri::root() . 'media/com_plot/coursevideo/' . $item->uid . '/thumb/' . $youtubeNumber . '.jpg'; ?>
                                <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                       xlink:href="<?php echo $img; ?>" alt=""></image>
                            <?php
                            } else {
                                ?>
                                <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                       xlink:href="<?php echo JUri::root() . 'images/com_plot/def_video.jpg'; ?>"
                                       alt=""></image>
                            <?php
                            }
                        } else {
                            if (file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR . $item->uid . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $youtubeNumber . '.jpg')) {
                                $img = JUri::root() . 'images/com_plot/video/' . $item->uid . '/thumb/' . $youtubeNumber . '.jpg'; ?>
                                <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                       xlink:href="<?php echo str_replace("\\images", "/images", $img); ?>"
                                       alt=""></image>
                            <?php
                            } else {
                                ?>
                                <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                       xlink:href="<?php echo JUri::root() . 'images/com_plot/def_video.jpg' ?>"
                                       alt=""></image>
                            <?php
                            }
                        }
                        ?>
                    </svg>
                    <i class="activity-icons">
                        <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet"
                             style="fill:url(#svg-gradient);">
                            <use xlink:href="#play"></use>
                        </svg>
                    </i>
                </div>
                <p><?php echo PlotHelper::cropStr(strip_tags($item->description), plotGlobalConfig::getVar('childPageAllDescriptionMaxSymbolsToShow')); ?></p>
            </a>
            <?php
            if (plotUser::factory()->id && ((int)$this->id == plotUser::factory()->id)) {
                if ($item->type = "course") {
                    ?>
                    <button class="add hover-shadow"
                            onclick="plotEssay.createMessage('Нельзя удалить видео к курсу'); return false;">Удалить
                    </button>
                <?php
                } else {
                    ?>
                    <button class="add hover-shadow"
                            onclick="videoRemoveConfirm('<?php echo $item->id; ?> '); return false;">Удалить</button>
                <?php
                }


            } ?>
        </li>
    <?php
    } else {

        if (file_exists(JPATH_SITE . '/media/com_plot/videos/' . $item->uid . '/thumb/' . substr($item->path, 0, -3) . 'jpg')) {
            $img1 = JUri::root() . 'media/com_plot/videos/' . $item->uid . '/thumb/' . substr($item->path, 0, -3) . 'jpg';
            $img1 = str_replace("\\images", "/images", $img1);
        } else {
            $img1 = JUri::root() . 'images/com_plot/def_video.jpg';
        } ?>
        <li data-video-id="<?php echo $item->id; ?>">
            <a class="fancybox" rel="videos-set" data-fancybox-type="ajax" data-title="<?php echo $item->title; ?>"
               data-description="<?php echo $item->description; ?>"
               data-date="<?php echo JHtml::date($item->date, 'd.m.Y'); ?>"
               data-share=" data-image='https://youtu.be/<?php echo $youtubeNumber; ?>' data-url='https://youtu.be/<?php echo $youtubeNumber; ?>' data-title='<?php echo addslashes($item->title); ?>'"
               href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.openVideoFile&videoId=' . $item->id); ?>">
                <h6><?php echo $item->title; ?></h6>
                <hr/>
                <span><?php echo JHtml::date($item->date, "d.m.Y"); ?></span>

                <div class="circle-img">
                    <svg class="clip-svg">
                        <?php
                        if (file_exists(JPATH_SITE . '/media/com_plot/videos/' . $item->uid . '/thumb/' . substr($item->path, 0, -3) . 'jpg')) {
                            $img = JUri::root() . 'media/com_plot/videos/' . $item->uid . '/thumb/' . substr($item->path, 0, -3) . 'jpg'; ?>
                            <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                   xlink:href="<?php echo str_replace("\\images", "/images", $img); ?>" alt=""></image>
                        <?php
                        } else {
                            ?>
                            <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"
                                   xlink:href="<?php echo JUri::root() . 'images/com_plot/def_video.jpg' ?>"
                                   alt=""></image>
                        <?php
                        } ?>
                    </svg>
                    <i class="activity-icons">
                        <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet"
                             style="fill:url(#svg-gradient);">
                            <use xlink:href="#play"></use>
                        </svg>
                    </i>
                </div>
                <p><?php echo PlotHelper::cropStr(strip_tags($item->description), plotGlobalConfig::getVar('childPageAllDescriptionMaxSymbolsToShow')); ?></p>
            </a>
            <?php
            if (plotUser::factory()->id && ((int)$item->uid == plotUser::factory()->id)) {
                ?>
                <button class="add hover-shadow"
                        onclick="videoRemoveConfirm('<?php echo $item->id; ?>'); return false;">Удалить
                </button>
            <?php
            }
            ?></li>
    <?php
    }
} ?>
