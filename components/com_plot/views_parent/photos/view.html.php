<?php
defined('_JEXEC') or die;

class PlotViewPhotos extends PlotViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'courses';

    public function display($tpl = null)
    {
        $this->componentUrl = JURI::base().'components/com_plot';
        $this->referrerUrl = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : JRoute::_('index.php?option=com_plot&view=river');
        $this->templateUrl = JURI::base().'templates/'.JFactory::getApplication()->getTemplate();

        $ages = new plotAges();
        $this->ages = $ages->getList();
        $this->tags = plotTags::getK2TagsList();
        
        $this->user = plotUser::factory(JFactory::getApplication()->input->getInt('id', 0));
        $this->my = plotUser::factory();
        $this->my->level = $this->my->getLevel();

        $photosModel = $this->getModel('Photos');
        $this->total_photos = $photosModel->getPhotos(array('offset' => 0, 'limit' => 1), $this->user->id)['countItems'];
        $this->total_videos = $photosModel->getVideos(array('limit' => 1, 'offset' => 0), $this->user->id)['countItems'];
        $this->total_certificates = $photosModel->getCertificates(array('limit' => 1, 'offset' => 0), $this->user->id)['countItems'];
        
        $this->isChildSeeToOtherParent = false;
        if ($this->user->id && $this->my->id != $this->user->id && !$this->my->isParent() && plotUser::factory($this->user->id)->isParent()) {
            $this->isChildSeeToOtherParent = true;
        }
        $this->isParentSeeToOtherParent = false;
        if ($this->user->id && $this->my->id != $this->user->id && $this->my->isParent() && plotUser::factory($this->user->id)->isParent()) {
            $this->isParentSeeToOtherParent = true;
        }
        
        $coursesModel = JModelLegacy::getInstance('courses', 'plotModel');
        $this->coursesCategories = $coursesModel->getCoursesCategoriesWhichHavePublishedCourses();

        $this->setOgMeta();
        return parent::display($tpl);
    }
    
    private function setOgMeta()
    {
        $db = JFactory::getDbo();
        require_once JPATH_ADMINISTRATOR.'/components/com_plot/plot_social_config.php';
        $metaOgUrl = '<meta property="og:url" content="'.PlotHelper::getAbsRoutedUri('index.php?option=com_plot&view=photos&id='.$this->user->id).'" />';
        $metaOgTitle = '<meta property="og:title" content="'.$db->escape($this->user->name).'" />';
        $metaOgDescription = '<meta property="og:description" content="'.$db->escape(strip_tags($this->user->getSocialFieldData('ABOUT_ME'))).'" />';
        $metaOgImage = '<meta property="og:image" content="'.PlotHelper::getAbsRoutedUri('index.php?option=com_plot&view=profile&task=profile.pickoutimage&id='.$this->user->id).'&canv_width=200&canv_height=200" />';
        $metaOgSiteName = '<meta property="og:site_name" content="'.JFactory::getConfig()->get('sitename').'" />';
        $metaOgFbAppId = '<meta property="fb:app_id" content="473991812742077" />';
        $metaOgType = '<meta property="og:type" content="website" />';
        $metaOgLocale = '<meta property="og:locale" content="ru_RU" />';
        JFactory::getDocument()->addCustomTag($metaOgTitle.$metaOgSiteName.$metaOgUrl.$metaOgDescription.$metaOgFbAppId.$metaOgType.$metaOgLocale.$metaOgImage);
        return true;
    }
    

}
