<?php
defined('_JEXEC') or die;
?>

<script type="text/javascript" src="<?php echo JUri::root().'components/com_plot/views_parent/wallet/tmpl/default.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root().'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo juri::root();?>templates/plot/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo juri::root();?>templates/plot/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="<?php echo juri::root();?>components/com_plot/libraries/jquery_input_mask_plugin/jquery.mask.min.js"></script>

<main class="parent-profile">
    
    <div class="top parent-profile">
        <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_header') ); ?>
        <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_top_menu') ); ?>
    </div>
    
    <div class="wrap main-wrap parent-profile">
        <div class="aside-left parent-profile"></div>
        <div class="aside-right parent-profile"></div>
        <section class="parent-cash">
            <div id="parent-cash">
                <ul class="parent-courses-think-tabs">
                    <li><a href="#parent-my-cash">Мой кошелек</a></li>
                    <li><a href="#parent-cash-in">История заработанных денег</a></li>
                    <li><a href="#parent-cash-out">История вывода денег</a></li>
                </ul>
                <div id="parent-my-cash">
                    <svg viewBox="0 0 17.3 22.2" preserveAspectRatio="xMinYMin meet">
                        <use xlink:href="#cash"></use>
                    </svg>
                    <div class="payment-description">
                        <div class="balance">
                        <span>Мой баланс:
                            <b><?php echo $this->my->money; ?>
                                <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                                    <use xlink:href="#munze"></use>
                                </svg>
                            </b>
                        </span>
                        </div>
                        <?php echo PlotHelper::getk2item( plotGlobalConfig::getVar('walletParentK2ItemTextId') )->introtext; ?>
                    </div>

                </div>
                <div id="parent-cash-in">
                    <h2>Всего заработано денег: <span><?php echo $this->sumOfAllFinishedPrices; ?></span>
                        <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                            <use xlink:href="#munze"></use>
                        </svg>
                    </h2>
                    <table>
                        <thead>
                            <tr>
                                <th>№</th>
                                <th>Дата</th>
                                <th>Мои достижения</th>
                                <th>Сумма</th>
                            </tr>
                        </thead>
                        <tbody class="scroll-pane wrap-for-scrollscript">
                            <?php $i = 1; foreach ($this->my->finishedBooksCourses AS $finishedEvent) { ?>
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td class="date"><?php echo JFactory::getDate($finishedEvent->finished_date)->format('d.m.Y H:i'); ?></td>
                                    <td>
                                        <?php
                                        if (isset($finishedEvent->course_id)) { ?>
                                            Пройден курс
                                            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=course&id='.$finishedEvent->course_id); ?>">"<?php echo $finishedEvent->title; ?>"</a>
                                        <?php } elseif (isset($finishedEvent->book_id)) { ?>
                                            Прочитал(а) книгу
                                            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId='.$finishedEvent->book_id); ?>">"<?php echo $finishedEvent->title; ?>"</a>
                                        <?php }elseif(isset($finishedEvent->entityId)){
                                            if($finishedEvent->entity=='book'){
                                                ?>
                                                Авторское вознаграждение за тесты к книге <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId='.$finishedEvent->entityId); ?>">"<?php echo $finishedEvent->title; ?>"</a>
                                        <?php
                                            }elseif($finishedEvent->entity=='course'){
                                                ?>
                                                Авторское вознаграждение за курс <a href="<?php echo JRoute::_('index.php?option=com_plot&view=course&id='.$finishedEvent->entityId); ?>">"<?php echo $finishedEvent->title; ?>"</a>
                                        <?php
                                            }
                                        } ?>
                                    </td>
                                    <td class="sum"> <?php echo $finishedEvent->finished_price; ?>
                                        <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze"><use xlink:href="#munze"></use></svg>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div id="parent-cash-out">
                    <h2>Всего выведено денег: <span><?php echo $this->my->getCashoutAmountTotal(); ?></span>
                        <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                            <use xlink:href="#munze"></use>
                        </svg>
                    </h2>
                    <table>
                        <thead>
                            <tr>
                                <th>№</th>
                                <th>Дата</th>
                                <th>Способы вывода денег</th>
                                <th>Сумма</th>
                            </tr>
                        </thead>
                        <tbody class="scroll-pane wrap-for-scrollscript">
                            <?php $i =1; foreach ($this->my->cashouts AS $cashout) { ?>
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td class="date"><?php echo JFactory::getDate($cashout->date)->format('d.m.Y H:i'); ?></td>
                                    <td>
                                        <?php if ($cashout->cashoutMethod == 'card') { ?>
                                            На банковскую карту
                                        <?php } ?>
                                        <?php if ($cashout->cashoutMethod == 'yandexWallet') { ?>
                                            на кошелек в Яндекс.Деньгах
                                        <?php } ?>
                                    </td>
                                    <td class="sum"> <?php echo $cashout->amount; ?>
                                        <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze"><use xlink:href="#munze"></use></svg>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="cash-operation">
                    <h2>
                        <svg viewBox="0 0 17.3 22.2" preserveAspectRatio="xMinYMin meet"><use xlink:href="#cash"></use></svg>
                        Мой кошелек
                    </h2>
                    <span>Мой баланс:</span>
                    <b><?php echo $this->my->money; ?> руб</b>
                    <form id="cashout-form" action="<?php echo JRoute::_('index.php?option=com_plot&task=wallet.cashout'); ?>" method="POST">
                        <fieldset>
                            <div>
                                <label for="amount">Сумма:</label>
                                <input type="text" id="amount" name="amount" />
                                <span class="abs">1 руб = 1 монета</span>
                                <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze"><use xlink:href="#munze"></use></svg>
                            </div>
                            <div>
                                <label for="cash-out-method">Способ вывода:</label>
                                <select id="cash-out-method" name="cash-out-method">
                                    <option value="yandexWallet">Яндекс кошелек</option>
                                    <option value="phone">Номер телефона</option>
                                </select>
                                <label for="cash-out-destination">Номер Яндекс кошелька:</label>
                                <input type="text" id="cash-out-destination" name="cash-out-destination" />
                            </div>
                        </fieldset>
                        <input type="submit" value="Вывести"/>
                    </form>
                </div>
            </div>
        </section>
    </div>
</main>
