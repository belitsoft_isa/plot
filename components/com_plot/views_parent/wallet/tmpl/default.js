jQuery(document).ready(function(){

    jQuery('.scroll-pane').jScrollPane();
    jQuery( "#parent-cash" ).tabs();
    
    jQuery( "#cash-out-method").selectmenu({
        appendTo: '.main-content',
        change: function(event, ui) {
            if (ui.item.value === 'yandexWallet') {
                jQuery('label[for=cash-out-destination]').html('Номер Яндекс кошелька:');
            } else {
                jQuery('label[for=cash-out-destination]').html('Номер телефона:');
            }
        }

    });

    jQuery( "#cash-out-method").on( "selectmenuclose", function( event, ui ) {} );
    
    jQuery('#cashout-form #amount').mask('######');

});
