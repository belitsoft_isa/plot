<?php
defined('_JEXEC') or die;

class PlotViewWallet extends PlotViewParentLegacy
{

    public function display($tpl = null)
    {
        $this->my = plotUser::factory();
        if (!$this->my->id) {
            JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_plot&view=river'));
        }
        $this->my->money = $this->my->getMoney();
        $this->my->finishedCourses = $this->my->getFinishedCourses();
        $this->my->finishedBooks = $this->my->getFinishedBooks();
        $this->my->authorship=$this->my->getAuthorship();

        $this->my->finishedBooksCourses = array_merge($this->my->finishedCourses, $this->my->finishedBooks, $this->my->authorship);
        usort($this->my->finishedBooksCourses, function($a, $b) {
            return $a->finished_date >= $b->finished_date ? -1 : 1;
        });

        $this->sumOfAllFinishedPrices = 0;
        foreach ($this->my->finishedBooksCourses AS $finishedEvent) {

            $this->sumOfAllFinishedPrices += $finishedEvent->finished_price;
            if(isset($finishedEvent->entity)){
                if($finishedEvent->entity=='book'){
                    $book=new plotBook((int)$finishedEvent->entityId);
                    $finishedEvent->title=$book->c_title;
                }elseif($finishedEvent->entity=='course'){
                    $coures=new plotCourse((int)$finishedEvent->entityId);
                    $finishedEvent->title=$coures->course_name;
                }
            }
        }
//die(var_dump($this->my->finishedBooksCourses));
        $this->my->cashouts = $this->my->getCashouts();

        return parent::display($tpl);
    }

}
