<?php
defined('_JEXEC') or die('Restricted access');

class PlotViewSearchUsersBook extends PlotViewParentLegacy
{
    
    function display($tpl = null)
    {
        $model = JModelLegacy::getInstance('SearchUsersBook', 'plotModel');
        $this->bookId = JRequest::getInt('id', '0');
        $publicationModel=JModelLegacy::getInstance('Publication', 'plotModel');
        $filter = array();
        if($this->bookId){
            $filter[] = array('field' => 'u.id', 'type' => 'NOT IN', 'value' => $publicationModel->getBookAuthorId($this->bookId));
        }
        $this->users = $model->getUsers($filter, '`u`.`name` ASC', $limit = array('limitstart' => '0', 'limit' => '10'));
        $this->totalUser = $model->totalUsers;
        $this->pagination = new JPagination( $this->totalUser, 0, plotGlobalConfig::getVar('usersCountSearchForBuyBook') );

        parent::display();
        die;
    }

}
