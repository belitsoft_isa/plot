<?php
defined('_JEXEC') or die;
$id = JFactory::getApplication()->input->get('id', 0, 'INT');
?>

<script type="text/javascript" src="<?php echo JUri::root().'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.min.js"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.basic.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'components/com_plot/libraries/jquery_input_mask_plugin/jquery.mask.min.js'; ?>"></script>

<script src="<?php echo JUri::root(); ?>media/system/js/mootools-core.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/core.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/mootools-more.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/modal.js" type="text/javascript"></script>


<script  src="<?php echo JURI::base() . 'components/com_plot'; ?>/libraries/scrolling_ajax_pagination/javascript.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>components/com_plot/views_parent/course/tmpl/default.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>components/com_plot/assets/js/plotEssay.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>templates/plot/js/jquery_simple_progressbar.js"></script>
<script type="text/javascript"
        src="<?php echo JUri::root(); ?>components/com_plot/assets/js/jquery.fancybox.js"></script>
<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">

    function isSmall(){

        return <?php echo $this->courseObj->isSmall(); ?>

    }
    function noCoursesFind(){
        jQuery('#videos-list').html('');
        jQuery('#videos-list').html("<div class='no-items'><?php echo JText::sprintf('COM_PLOT_NOT_FOUND_WHAT_COURSES', JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('footerLinkK2idForChildrens'))); ?></div>");
    }

function courseBuy() 
{


    /* getting */
    var userIdsCourseBuyFor = [];
    jQuery('#course-children-for .users-course-for-list .user-course-for').each(function(i, v){
        userIdsCourseBuyFor.push(jQuery(this).attr('userid'));
    });

    if (userIdsCourseBuyFor.length) {
        jQuery.post(
            '<?php echo JRoute::_('index.php?option=com_plot&task=course.createOrder'); ?>', 
            {
                courseId: <?php echo $this->course->id; ?>, 
                usersIds: JSON.stringify(userIdsCourseBuyFor), 
                amount: jQuery('#reward').val()
            }, 
            function(response){
                var data = jQuery.parseJSON(response);
                jQuery('#buy_course_hidden_form [name=userIds]').val(userIdsCourseBuyFor.join('-'));
                jQuery('#buy_course_hidden_form [name=orderNumber]').val(data.orderId);
                jQuery('#buy_course_hidden_form [name=rewardAmountForEach]').val(jQuery('#reward').val());
                jQuery('#buy_course_hidden_form').submit();
            }
        );
    } else {
        SqueezeBox.initialize({
            size: {x: 300, y: 150}
        });
        var str = '<div id="enqueued-message">Не выбраны пользователи, для которых Вы хотите купить даннный курс</div>';
        SqueezeBox.setContent('adopt', str);
        SqueezeBox.resize({x: 300, y: 150});
    }

}

function calculateCourseCost()
{
    jQuery.post('index.php?option=com_plot&task=course.calculateCosts', {courseId: <?php echo $this->course->id; ?>, enteredAmount: jQuery('#reward').val(), countChilds: jQuery('#course-children-for .users-course-for-list .user-course-for').length}, function(response){
        var data = jQuery.parseJSON(response);
        jQuery('.admin-cost-calculated').html(data.costs.admin);
        jQuery('.author-cost-calculated').html(data.costs.author);
        jQuery('.payment-system-cost-calculated').html(data.costs.paymentSystem);
        jQuery('.total-cost-calculated').html(data.costs.total);
        jQuery('#buy_course_hidden_form [name=sum]').val(data.costs.total);
    });
}


function ajaxLoadBefore(callback) {
    var beforeFilterData = {
        bookId: '<?php echo $id; ?>'
    };
    jQuery.post(
        'index.php?option=com_plot&task=course.ajaxGetBeforeCourse',
        {userData: beforeFilterData},
        function (response) {
            if (response) {
                jQuery('#beforecourses-list').html('').show();
                jQuery('#beforecourses-list').append(response);
                callback();
                jQuery('#loading-bar').hide();
            } else {
                jQuery('#loading-bar').hide();
            }
        }
    );


    jQuery('#beforecourses-list').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('coursesResultsShowFirstCountParent'); ?>,
        offset: <?php echo plotGlobalConfig::getVar('coursesResultsShowFirstCountParent'); ?>,
        error: '',
        delay: 10,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=course.ajaxGetBeforeCourse'); ?>',
        appendDataTo: 'beforecourses-list',
        userData: beforeFilterData,
        afterLoad: {
            afterLoadAction: function () {
                jPlotUp.Arrow.initialize('positionCameras');
            }
        }
    });

}

function ajaxLoadAfter(callback) {
    var afterFilterData = {
        bookId: '<?php echo $id; ?>'
    };
    jQuery.post(
        'index.php?option=com_plot&task=course.ajaxGetAfterCourse',
        {userData: afterFilterData},
        function (response) {
            if (response) {
                jQuery('#aftercourses-list').html('').show();
                jQuery('#aftercourses-list').append(response);
                callback();
                jQuery('#loading-bar').hide();
            } else {
                jQuery('#loading-bar').hide();
            }
        }
    );

    jQuery('#aftercourses-list').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('coursesResultsShowFirstCountParent'); ?>,
        offset: <?php echo plotGlobalConfig::getVar('coursesResultsShowFirstCountParent'); ?>,
        error: '',
        delay: 10,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=course.ajaxGetAfterCourse'); ?>',
        appendDataTo: 'aftercourses-list',
        userData: afterFilterData,
        afterLoad: {
            afterLoadAction: function () {
                jPlotUp.Arrow.initialize('positionCameras');
            }
        }
    });

}

function ajaxLoadVideos(callback) {
    var videosFilterData = {
        courseId:'<?php echo  $id; ?>'
    };
    jQuery.post(
        'index.php?option=com_plot&task=course.ajaxVideosLoadMore',
        {userData:videosFilterData},
        function (response) {
            if (response) {
                jQuery('#videos-list').html('').show();
                jQuery('#videos-list').append(response);
                callback();
                jQuery('#loading-bar').hide();
            } else {
                jQuery('#loading-bar').hide();
            }
        }
    );
    jQuery('#videos-list').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('parentEssayCount'); ?>,
        offset: <?php echo plotGlobalConfig::getVar('parentEssayCount'); ?>,
        error: '',
        delay: 10,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=course.ajaxVideosLoadMore'); ?>',
        appendDataTo: 'videos-list',
        userData: videosFilterData,
        afterLoad: {
            afterLoadAction: function () {
                jPlotUp.Arrow.initialize('positionCameras');
                updateFancybox();
            }
        }
    });
}

function fixFFBugWithFavicon() {
    jQuery('head link[href="<?php echo JUri::root(true);?>/templates/plot/favicon.ico"]').attr('href', '<?php echo JUri::root(true);?>/templates/plot/favicon.ico');
}

    document.addEventListener("touchmove", function(){
        jQuery('body').trigger('scroll');
        console.log('ScrollStart');
    }, false);





</script>
<?php # </editor-fold> ?>

<main class="parent-profile parent-one-elem">
    <div class="top parent-profile">
        <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_header') ); ?>
        <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_top_menu') ); ?>
    </div>
    <div class="wrap main-wrap parent-profile">
        <div class="add-left aside-left parent-profile"></div>
        <div class="add-right aside-right parent-profile"></div>
        <section class="parent-one-elem">
            <div class="parent-courses-think-tabs">
                <button class="link-back" onclick="window.location.href='<?php echo $this->referrerUrl; ?>';">Назад</button>
                <p><?php echo $this->course->c_category; ?></p>

                <?php if ( $this->course->isBoughtForMe && in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe(true)) ) {  ?>
                    <a class="add hover-shadow" href="<?php echo $this->course->routedLinkToLearningPath; ?>"><?php echo JText::_('COM_PLOT_DO_COURSE'); ?></a>
                <?php } elseif ($this->course->isBoughtForMe) { ?>
                    <a class="add hover-shadow"  href="<?php echo $this->course->routedLinkToLearningPath; ?>"><?php echo JText::_('COM_PLOT_REPEAT_COURSE'); ?></a>
                <?php }elseif(!$this->course->isBoughtForMe){
                    ?>
                    <a class="add hover-shadow" href="#" onclick="createMsg('<?php echo JText::_("COM_PLOT_THIS_COURSE_NOT_BUY"); ?>');  return false;"><?php echo JText::_('COM_PLOT_DO_COURSE'); ?></a>
                <?php
                } ?>


                <?php if ($this->couresBook && (int)$this->couresBook->c_id && $this->courseObj->isGetPages() && $this->courseObj->id_pub) { ?>
                    <a class="add-recipient hover-shadow"  href='<?php echo JRoute::_("index.php?option=com_html5flippingbook&view=publication&id=" . (int)$this->couresBook->c_id . "&fullscreen=1"); ?>'><?php echo JText::_('COM_PLOT_COURSE_ABOUT_COURSE'); ?></a>
                <?php } ?>

            </div>
            <div class="elem-info parent-course">
                    <div
                        <?php echo ($this->courseObj->isGetPages() && $this->courseObj->id_pub )?'class="elem-picture thumbnail"':'class="elem-picture nothumbnail"';?>
                        <?php if ($this->couresBook && (int)$this->couresBook->c_id && $this->courseObj->isGetPages() && $this->courseObj->id_pub) { ?>
                            onclick="window.location.href='<?php echo JRoute::_("index.php?option=com_html5flippingbook&view=publication&id=" . (int)$this->couresBook->c_id . "&fullscreen=1"); ?>'"
                        <?php } ?>
                    >
                        <img src="<?php echo JUri::root().$this->course->image; ?>" />

                        <?php if ($this->my->isCourseNew($this->course->id)) { ?>
                        <i class="in-progress"><br/><?php echo  JText::_('COM_PLOT_COURSE_LERNING_NEW'); ?><br/></i>
                        <?php } elseif ( in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe()) ) { ?>
                        <i class="in-progress">
                            <?php if((int)$this->my->getCoursesCompletePercent($this->course->id)==100){
                                echo JText::_('COM_PLOT_COURSE_LERNING_ON_100');
                            }else{?>
                                <?php echo  JText::_('COM_PLOT_COURSE_LERNING'); ?><b><?php echo $this->my->getCoursesCompletePercent($this->course->id); ?>%</b>
                        <?php }
                        } ?>
                        </i>
                    </div>
                <h6><?php echo $this->course->course_name; ?></h6>
                <div class="st-bon-wrap">
                    <div class="sticker">
                        Рейтинг курса:<br/>
                        <div class="bought">Куплено: <?php echo $this->course->getCountPushcases(); ?></div>
                        <div class="studied">Изучено: <?php echo $this->course->getCountSuccesfullyStudied(); ?></div>
                        <div class="clr"></div>
                    </div>
                </div>
                <?php if (in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe($onlyNotFinished = true))) { ?>
                    <div class="bonus">
                        <p><?php  echo  JText::_('COM_PLOT_AFTER_COURSE_FINISH');?></p>
                        <div>
                            <p class="finance-info"><?php echo $this->my->getFinishedPriceForMyCourse($this->course->id); ?>
                                <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                                    <use xlink:href="#munze"></use>
                                </svg>
                            </p>
                        </div>
                    </div>
                <?php } ?>
                <div class="clr"></div>
                    <h4>Информация:</h4>
                    <div class="elem-about">
                        <?php
                        if (in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe($onlyNotFinished = false))) {

                        if (in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe()) && !$this->my->isCourseTestFinished($this->course->id)) {

                            if($this->courseObj->isSmall()){ ?>
                                <?php echo JText::sprintf('COM_PLOT_SMALL_COURSE_BUY_BUT_TEST_NOT_FINISHED', '<a href="'.JRoute::_("index.php?option=com_plot&view=wallet").'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>

                            <?php }else{
                                ?>

                                <?php echo JText::sprintf('COM_PLOT_COURSE_BUY_BUT_TEST_NOT_FINISHED', '<a href="'.JRoute::_("index.php?option=com_plot&view=wallet").'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>

                            <?php
                            }
                        }else{

                            if ($this->courseObj->isSmall()) {
                                ?>
                                <?php echo JText::sprintf('COM_PLOT_SMALL_COURSE_BUY_BUT_TEST_FINISHED', '<a href="'.JRoute::_("index.php?option=com_plot&view=wallet").'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>

                            <?php
                            } else {

                                if ($this->my->isCourseVideoAdd($this->course->id) && (int)$this->my->courseVideoApproved($this->course->id)) {
                                    ?>

                                    <?php echo JText::sprintf('COM_PLOT_COURSE_BUY_AND_TEST_AND_VIDEO_FINISHED', '<a class="modal" rel="{handler: \'iframe\', size: {x: 744, y: 469}}" href="'.JRoute::_("index.php?option=com_plot&view=wallet", false).'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>


                                <?php
                                } elseif($this->my->isCourseVideoAdd($this->course->id) && !(int)$this->my->courseVideoApproved($this->course->id)) {

                                    $who_bought = plotUser::factory($this->courseObj->whoBoughtCourse($this->my->id));
                                    ?>

                                        <?php echo   JText::sprintf('COM_PLOT_COURSE_BUY_AND_TEST_AND_VIDEO_NOT_APPROVED', '<br/><a href="' . JRoute::_("index.php?option=com_plot&view=profile&id=" . $who_bought->id) . '">' . $who_bought->name . '</a>'); ?>

                                <?php
                                }elseif(!$this->my->isCourseVideoAdd($this->course->id) && !(int)$this->my->courseVideoApproved($this->course->id)){
                                    ?>

                                        <?php echo   JText::_('COM_PLOT_COURSE_BUY_BUT_TEST_FINISHED'); ?>

                                <?php
                                }
                            }
                        }
                        }else{

                            if($this->courseObj->isSmall()){ ?>
                                <?php echo JText::sprintf('COM_PLOT_SMALL_COURSE_BUY_BUT_TEST_NOT_FINISHED', '<a href="'.JRoute::_("index.php?option=com_plot&view=wallet").'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>

                                <?php }else{
                                ?>
                                <div>
                                    <?php echo JText::sprintf('COM_PLOT_COURSE_BUY_BUT_TEST_NOT_FINISHED', '<a href="'.JRoute::_("index.php?option=com_plot&view=wallet").'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>

                                </div>
                            <?php
                            }

                        }






                        /*if (in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe()) && !$this->my->isCourseFinished($this->course->id)) {

                            if($this->courseObj->isSmall()){
                                echo JText::_('COM_PLOT_SMALL_COURSE_BUY_BUT_TEST_NOT_FINISHED');
                            }else{
                                ?>
                                <div>
                                    <?php echo JText::sprintf('COM_PLOT_COURSE_BUY_BUT_TEST_NOT_FINISHED', '<a href="'.JRoute::_("index.php?option=com_plot&view=wallet").'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>
                                </div>
                            <?php
                            }
                        }

                        if (in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe()) && $this->my->isCourseFinished($this->course->id)) {

                            if ($this->courseObj->isSmall()) { ?>
                                <?php echo JText::sprintf('COM_PLOT_SMALL_COURSE_BUY_BUT_TEST_FINISHED', '<a href="'.JRoute::_("index.php?option=com_plot&view=wallet").'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>
                            <?php } else {

                                if ($this->my->isCourseVideoAdd($this->course->id) && (int)$this->my->courseVideoApproved($this->course->id)) { ?>
                                    <?php echo JText::sprintf('COM_PLOT_COURSE_BUY_AND_TEST_AND_VIDEO_FINISHED', '<a href="'.JRoute::_("index.php?option=com_plot&view=wallet").'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>
                                <?php } else {
                                    $who_bought = plotUser::factory($this->courseObj->whoBoughtCourse($this->my->id));
                                    echo   JText::sprintf('COM_PLOT_COURSE_BUY_AND_TEST_AND_VIDEO_NOT_APPROVED', '<br/><a href="' . JRoute::_("index.php?option=com_plot&view=profile&id=" . $who_bought->id) . '">' . $who_bought->name . '</a>');
                                }


                            }
                        }*/

                        ?>
                    </div>


                <h4><?php echo JText::_("COM_PLOT_COURSE_ANNOTATION");?></h4>
                <div class="elem-about">
                    <p><?php echo $this->course->course_description; ?></p>
                </div>
                <h4>Купить курс:</h4>
                <div class="buy">
                    <form id="course-children-for">
                        <fieldset>
                            <label for="recipients-course">Выберите кому:</label>
                            <div class="users-course-for-list"></div>
                            <button class="add-recipient" name="choose" onclick="SqueezeBox.open('<?php echo JRoute::_('index.php?option=com_plot&task=searchusers.showPopup&courseId='.JRequest::getInt('id', 0)); ?>', {size: {x: 600, y: 450}, handler: 'ajax'});" >
                                <svg viewBox="0 0 31.7 30.8" preserveAspectRatio="xMinYMin meet" class="friend"><use xlink:href="#friend"></use></svg>
                                Добавить
                            </button>
                        </fieldset>                        
                    </form>
                    <div>
                        <div>
                            <p>Вознаграждение за прочтение:</p>
                            <span class="abs">1 руб = 1 монета</span>
                            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                                <use xlink:href="#munze"></use>
                            </svg>
                            <input type="text" id="reward" value="<?php echo plotGlobalConfig::getVar('payForCourseCompleteDefault'); ?>" />
                        </div>
                        <div>
                            <p><?php echo JText::_("COM_PLOT_ADMINISTRATOR_DONATION");?></p>
                            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                                <use xlink:href="#munze"></use>
                            </svg>
                            <i class="admin-cost-calculated"><?php echo $this->course->costsForBuyers['admin']; ?></i>
                        </div>
                        <div>
                            <p>Вознаграждение автору:</p>
                            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                                <use xlink:href="#munze"></use>
                            </svg>
                            <i class="author-cost-calculated"><?php echo $this->course->costsForBuyers['author']; ?></i>
                        </div>
                        <div>
                            <p>Комиссия платежной системы:</p>
                            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                                <use xlink:href="#munze"></use>
                            </svg>
                            <i class="payment-system-cost-calculated"><?php echo $this->course->costsForBuyers['paymentSystem']; ?></i>
                        </div>
                        <span>Итого:</span>
                        <b><span class="total-cost-calculated"><?php echo $this->course->costsForBuyers['total']; ?></span> руб</b>

                        <form action="https://money.yandex.ru/eshop.xml" method="post" name="buy_course_hidden_form" id="buy_course_hidden_form">
                            <!-- Обязательные поля -->
                            <input name="shopId" class="hidden" value="<?php echo plotGlobalConfig::getVar('yandexShopId'); ?>" type="text" />
                            <input name="scid" class="hidden" value="<?php echo plotGlobalConfig::getVar('yandexScid'); ?>" type="text" />
                            <input name="sum" class="hidden" value="0" type="text" />
                            <input name="customerNumber" class="hidden" value="<?php echo plotUser::factory()->id; ?>" type="text" />
                            <!-- Необязательные поля -->
                            <div class="payment-type">
                                <p>Способ оплаты:</p>
                                <select name="paymentType" id="paymentType">
                                    <option value="AC">Банковская карта</option>
                                    <option value="PC">Яндекс кошелек</option>
                                    <option value="GP">Через кассы и терминалы</option>
                                    <option value="WM">Кошелек WM</option>
                                </select>
                            </div>
                            <input name="orderNumber" class="hidden" value="0" type="text" />
                            <input name="cps_email" class="hidden" value="<?php echo $this->my->email; ?>" type="text" />
                            <input name="shopSuccessURL" class="hidden" value="http://naplotu.com/course/<?php echo $this->course->id; ?>/success=1" type="text" />
                            <input name="shopFailURL" class="hidden" value="http://naplotu.com/course/<?php echo $this->course->id; ?>/success=0" type="text" />
                            <input name="courseId" class="hidden" value="<?php echo $this->course->id; ?>" type="text" />
                            <input  name="entity" class="hidden" value="course" type="text" />
                            <input name="rewardAmountForEach" class="hidden" value="0" type="text" />
                            <input name="userIds" class="hidden" value="" type="text" />
                        </form>
                        <script>
                            jQuery( "#paymentType").selectmenu({
                                appendTo:'.payment-type',
                                close: function( event, ui ) {}
                            });
                            jQuery( "#paymentType").on( "selectmenuclose", function( event, ui ) {} );
                        </script>
                        <?php if((int)$this->my->id){ ?>
                        <input type="submit" onclick="courseBuy();" value="<?php echo  JText::_('COM_PLOT_CHECKOUT')?>"/>
                        <?php }else{
                            ?>
                            <input type="submit" onclick="createMsg('<?php echo JText::_("COM_PLOT_LOGIN_FIRST"); ?>');  return false;" value="<?php echo  JText::_('COM_PLOT_CHECKOUT')?>"/>
                        <?php
                        } ?>
                    </div>
                </div>
                <h4>Курс включен в программы:</h4>
                <div class="programs-list" id="parent-programs-list">
                    <div class="jcarousel">
                        <?php if ($this->coursePrograms) { ?>
                            <ul>
                                <?php
                                foreach ($this->coursePrograms AS $program) {
                                    ?>
                                    <li>
                                        <a href="<?php echo $program->link; ?>">
                                            <img src="<?php echo $program->img; ?>">
                                            <div class="plot-course-programs" data-progress="<?php echo $program->percent;?>"></div>
                                            <h5><?php echo  $program->title; ?></h5>
                                        </a>
                                    </li>
                                <?php
                                }
                                ?>
                            </ul>
                        <?php
                        } else{?>
                            <p class="no-items"><?php echo JText::sprintf('COM_PLOT_THIS_COURSE_NOT_INCLUDE_IN PROGRAMS', $this->course->course_name); ?></p>
                        <?php
                        }
                        ?>
                        <p class="jcarousel-pagination"></p>
                    </div>
                </div>


                <div class="parent-essay-courses">
                    <ul class="parent-courses-think-tabs" id="all-tabs">
                        <?php if(!$this->courseObj->isSmall()){ ?>
                        <li id="link-parent-videos"><a href="#all-parent-videos"  class="active"><?php echo  JText::_('COM_PLOT_COURSE_VIDEOS'); ?></a></li>
                        <?php } ?>
                        <?php if($this->courseBefore){?>
                        <li id="link-beforecourses"><a href="#all-beforecourse" ><?php echo JText::_('COM_PLOT_COORSES_BEFORE'); ?></a></li>
                        <?php } ?>
                        <?php if($this->courseAfter){?>
                        <li id="link-aftercourses"><a href="#all-aftercourse" ><?php echo JText::_('COM_PLOT_COORSES_AFTER'); ?></a></li>
                        <?php } ?>
                        <li id="link-parent-disqus"><a href="#all-parent-disqus" ><?php echo JText::_('COM_PLOT_COURSE_DISQUS'); ?></a></li>
                    </ul>
                    <div id="loading-bar" style="margin-left: 5%;"><img
                            src="<?php echo $this->baseurl ?>/templates/plot/img/pre-loader-1.gif"/></div>
                    <?php if(!$this->courseObj->isSmall()){ ?>
                    <div id="all-parent-videos">
                        <div class="add-activity parent-profile">
                            <?php //if (in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe($onlyNotFinished = true)) && !$this->my->courseVideoApproved($this->course->id) && !$this->my->isCourseFinished($this->course->id) && $this->my->isCourseTestFinished($this->course->id) && !$this->courseObj->isSmall()) {
                                require_once JPATH_COMPONENT . '/views_parent/course/tmpl/add.video.php';
                            //} ?>
                        </div>
                        <ul class="no-jcarousel parent-profile" id="videos-list"></ul>
                    </div>
                    <?php } ?>
                    <?php if($this->courseBefore){?>
                    <div id="all-beforecourse">
                        <ul class="no-jcarousel parent-profile" id="beforecourses-list"></ul>
                    </div>
                    <?php } ?>
                    <?php if($this->courseAfter){?>
                    <div id="all-aftercourse">
                        <ul class="no-jcarousel parent-profile" id="aftercourses-list"></ul>
                    </div>
                    <?php } ?>
                    <div id="all-parent-disqus">
                        <?php echo PlotHelper::renderDisqus('course_'.$id); ?>
                            <div id="disqus_thread"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
