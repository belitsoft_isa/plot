<?php
defined('_JEXEC') or die;
if($this->video){
$id=(int)$this->video->id;
}

?>

<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link type="text/css" href="<?php echo JUri::root(); ?>templates/plot/css/style.css" rel="stylesheet">
<script src="<?php echo JUri::root(); ?>media/jui/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>templates/plot/js/jquery-ui.min.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/js/jquery.jscrollpane.min.js"></script>



<script type="text/javascript"  src="<?php echo JUri::root(); ?>components/com_plot/assets/js/plotEssay.js"></script>

<script type="text/javascript" src="<?php echo JUri::root(); ?>media/system/js/mootools-core.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>media/system/js/core.js"></script>

<script type="text/javascript" src="<?php echo JUri::root(); ?>media/system/js/mootools-more.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>media/system/js/modal.js"></script>
<script src="<?php echo JUri::root(); ?>templates/plot/js/jplot.js" type="text/javascript"></script>
<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('body').addClass('popup-style');
    });

    function acceptEssay(){
        jQuery.post(
            'index.php?option=com_plot&task=course.approve',
            {id: '<?php echo $id; ?>'},
            function (response) {
                createPopup(response.message);
                jQuery('.pre-loader').hide();
            }
        );
    }

    function rejectEssay(){
        var msg=jQuery('#videoreason-text').val();
        msg.trim()
        if(msg){
            jQuery('.pre-loader').show();
            jQuery.post(
                'index.php?option=com_plot&task=course.reject',
                {id: '<?php echo $id; ?>',
                msg:msg.trim()},
                function (response) {
                    createPopup(response.message);
                    jQuery('.pre-loader').hide();
                }
            );
        }else{
            createPopup2('Заполните поле');
        }

    }

    function createPopup(msg){
        var overlay='<div class="avatar-overlay" id="sbox-overlay2" aria-hidden="false" tabindex="-1"></div>',
            str='<div id="sbox-window2" role="dialog" aria-hidden="false" class="shadow avatar-sbox">'+
                '<div id="sbox-content2" class="sbox-content-adopt" style="opacity: 1;"><div id="enqueued-message">' +msg+
                '</div></div><a id="sbox-btn-close2" href="#" onclick="window.parent.document.getElementById(\'sbox-btn-close\').click();" role="button" aria-controls="sbox-window"></a></div>';
        jQuery("#avatar-box").after(str);
        jQuery("#avatar-box").after(overlay);
    }

    function createPopup2(msg){
        var overlay='<div class="avatar-overlay" id="sbox-overlay2" aria-hidden="false" tabindex="-1"></div>',
            str='<div id="sbox-window2" role="dialog" aria-hidden="false" class="shadow avatar-sbox">'+
                '<div id="sbox-content2" class="sbox-content-adopt" style="opacity: 1;"><div id="enqueued-message">' +msg+
                '</div></div><a id="sbox-btn-close2" href="#" onclick="document.getElementById(\'sbox-overlay2\').remove();document.getElementById(\'sbox-window2\').remove();" role="button" aria-controls="sbox-window"></a></div>';
        jQuery("#avatar-box").after(str);
        jQuery("#avatar-box").after(overlay);
    }

</script>

<?php # </editor-fold> ?>
<div id="message-form" class="parent-profile video-modal">
    <h6>Видео</h6>
    <div id="video-area" class="iframe-wrap">
        <iframe  scrolling="no" width="100%" height="99%" src="//www.youtube.com/embed/<?php echo $this->youtubeNumber; ?>?autoplay=0?showinfo=0"
                 frameborder="0" allowfullscreen="yes"></iframe>
    </div>
    <div id="essy-negative-reason" style="display: none;">
        <textarea id="videoreason-text" placeholder="Заполните поле"></textarea>
        <div class="plot-button-wrapp">
            <button class="button accept hover-shadow" onclick="rejectEssay();"><span><?php echo  JText::_('COM_PLOT_SEND');?></span></button>
            <button class="button cancel-act hover-shadow" onclick="plotEssay.hideElement('essy-negative-reason').showElement('video-buttons').showElement('video-area');"><span><?php echo  JText::_('COM_PLOT_CANCEL');?></span></button>
        </div>
    </div>
        <?php if(!$this->video->status && $this->video->user_id!=plotUser::factory()->id){ ?>
            <div class="plot-button-wrapp" id="video-buttons">
                <button class="button accept hover-shadow" onclick="acceptEssay();"><span><?php echo  JText::_('COM_PLOT_VIDEO_ACCEPT');?></span></button>
                <button class="button cancel-act hover-shadow" onclick="plotEssay.hideElement('video-buttons').hideElement('video-area').showElement('essy-negative-reason');"><span><?php echo  JText::_('COM_PLOT_VIDEO_REJECT');?></span></button>
            </div>
        <?php }else{
            ?>
            <p class="info"><?php echo  JText::_('COM_PLOT_YOU_APPROVED_THIS_VIDEO');?></p>
        <?php
        } ?>

        <div id="avatar-box"></div>
        <img src="<?php echo JURI::base() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/pre-loader-1.gif" alt="" class="pre-loader" style="display: none;"/>
</div>
