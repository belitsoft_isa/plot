<?php
defined('_JEXEC') or die;

# Категория: echo $course->c_category;
# Мин стоимость: echo $course->total_min_cost;
# Найдено всего: echo count($this->courses);

?>
<?php foreach ($this->coursesGroups AS $coursesGroup) { ?>

            <?php foreach ($coursesGroup as $course) { ?>
        <?php $plotCourse = new plotCourse($course->id); ?>
        <li>
            <a  href="<?php echo JRoute::_("index.php?option=com_plot&view=course&id=$course->id"); ?>">


                <h6><?php echo $course->course_name; ?></h6>
                <div class="circle-img">
                    <svg class="clip-svg-course">
                        <image style="clip-path: url(#clipping-circle-course);" width="100%" height="100%" xlink:href="<?php echo JUri::root().$course->image; ?>" />
                    </svg>
                    <i class="activity-icons"><svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);"><use xlink:href="#academic-hat"></use></svg></i>
                </div>
                <p ><?php echo PlotHelper::cropStr(strip_tags($course->course_description), plotGlobalConfig::getVar('coursesDescriptionMaxSymbolsToShow')) ; ?></p>
                <hr/>

            </a>
        </li>
            <?php } ?>                                

<?php } ?>

