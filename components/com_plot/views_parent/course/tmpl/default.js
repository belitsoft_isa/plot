function changeSelectChildMenu(event, ui)
{
    var selectedOption = jQuery(event.target).find('option:selected');
    var allOptionsExcludeSelectedChildrensIds = []; 
    var currentSelectId = jQuery(event.target).prop('id');
    
    jQuery(event.target).find('option:not(:selected)').each(function(){
        allOptionsExcludeSelectedChildrensIds.push(jQuery(this).val());
    });
    
    jQuery('select[id^=recipient-]').each(function(){
        if (jQuery(this).prop('id') != currentSelectId) {
            var elementIterator = jQuery(this);
            allOptionsExcludeSelectedChildrensIds.each(function(v){
                if (!elementIterator.find('option[value='+v+']').length) {
                    elementIterator.append('<option value="' + v + '">' + jQuery(event.target).find('option[value='+v+']').text() + '</option>');
                }
            });
            jQuery(this).find('option[value='+selectedOption.val()+']').remove();
        }
    });
    
    jQuery('select[id^=recipient-]').selectmenu('refresh');
}

function removeSearchActiveClassTab() {
    jQuery('#link-parent-videos').removeClass('ui-tabs-active');
    jQuery('#all-parent-videos').css({'display': 'none'});
    jQuery('#link-beforecourses').removeClass('ui-tabs-active');
    jQuery('#all-beforecourse').css({'display': 'none'});
    jQuery('#link-aftercourses').removeClass('ui-tabs-active');
    jQuery('#all-aftercourse').css({'display': 'none'});
    jQuery('#link-parent-disqus').removeClass('ui-tabs-active');
    jQuery('#all-parent-disqus').css({'display': 'none'});

}

function setActiveTab(tab, search_area) {

    jQuery('#'+tab).addClass('ui-tabs-active');

    jQuery('#'+search_area).css({'display': 'block'});

}
jQuery(document).ready(function() {

    jQuery('.plot-course-programs').each(function(){
        var percent=parseInt(jQuery(this).attr('data-progress'));
        jQuery(this).simple_progressbar({value: percent,
            height: '10px',
            internalPadding: '3px',
            normalColor: '#C56B35',
            backgroundColor: '#ffffff'});
    });

    jQuery("#recipient-1").selectmenu({ change: function( event, ui ) {changeSelectChildMenu(event, ui);} });
    
    // add / remove new children course buy for
    jQuery('#course-children-for').submit(function(){
        if ( jQuery('#course-children-for [name=add]').val() == '1' ) {
            var lastSelectIdNum = parseInt(jQuery('select[id^=recipient-]:last').attr('id').replace(/[^0-9]/g, ''));
            var selectIdNew = 'recipient-' + (lastSelectIdNum + 1);
            var selectedChildrens = [];
            var newOptions = '';

            jQuery('select[id^=recipient-] > option:selected').each(function(){
                selectedChildrens.push(jQuery(this).val());
            });

            jQuery('#recipient-1 option').each(function () {
                if (jQuery.inArray(jQuery(this).val(), selectedChildrens) === -1) {
                    newOptions += jQuery(this)[0].outerHTML;
                }
            });

            if (newOptions) {
                jQuery('<select id="' + selectIdNew + '">' + newOptions + '</div>').insertBefore('#course-children-for > fieldset .add-recipient:first');
                //jQuery('#course-children-for > fieldset').append('<select id="' + selectIdNew + '">' + newOptions + '</div>');
                jQuery("#" + selectIdNew).selectmenu({ change: function( event, ui ) {changeSelectChildMenu(event, ui);} });
            }
            // remove selected in new from each other selects
            var selectedValInNewText = jQuery('select[id^=recipient-]:last > option:selected').val();
            
            jQuery('select[id^=recipient-]:not(:last) option:not(:selected)').each(function(){
                if (jQuery(this).val() == selectedValInNewText) {
                    jQuery(this).remove();
                }
            });
            calculateCourseCost();
        } else if (jQuery('select[id^=recipient-]').length > 1) {
            var selectedOptionHtml = jQuery('<div>').append(jQuery('select[id^=recipient-]:last option:selected').clone()).html();
            jQuery('select[id^=recipient-]:not(:last)').each(function(){
                jQuery(this).append(selectedOptionHtml);
            });
            jQuery('select[id^=recipient-]:last').remove();
            calculateCourseCost();
        }
        
        jQuery('#course-children-for [name=add]').val('0');
        jQuery('select[id^=recipient-]').selectmenu('refresh');
        
        return false;
    });
    
    jQuery('#reward').mask('0000000');

    calculateCourseCost();
    jQuery('#reward').keyup(function(){ calculateCourseCost(); });
    jQuery('#reward').change(function(){ calculateCourseCost(); });
    
    jQuery('#course-children-for .users-course-for-list').on('click', '.remove', function(){
        jQuery(this).parent().fadeOut(300, function(){
            jQuery(this).remove();
            calculateCourseCost();
        })
    });



    SqueezeBox.initialize();
    jQuery('#link-parent-videos').on('click', function () {
        jQuery('#videos-list').html('').hide();
        ajaxLoadVideos(function () {
            jQuery('body').trigger('scroll');
        });
    });


    jQuery('#link-beforecourses').on('click', function () {
        jQuery('#beforecourses-list').html('').hide();
        ajaxLoadBefore(function () {
            jQuery('body').trigger('scroll');
        });
    });

    jQuery('#link-aftercourses').on('click', function () {
        jQuery('#aftercourses-list').html('').hide();
        ajaxLoadAfter(function () {
            jQuery('body').trigger('scroll');
        });
    });
    jQuery('#link-parent-disqus').on('click', function () {
        jQuery('#loading-bar').hide();
        removeSearchActiveClassTab();
        setActiveTab('link-parent-disqus', 'all-parent-disqus');


    });

    var win_hash = window.location.hash;
console.log(win_hash);
    switch(win_hash) {

        case '#all-beforecourse':
        case '#beforecourse':
            document.getElementById('link-beforecourses').click();
            break;
        case '#all-aftercourse':
        case '#aftercourse':
            document.getElementById('link-aftercourses').click();
            break;
        case '#videos':
            if(parseInt(isSmall())){

                document.getElementById('link-parent-disqus').click();
            }else {
                document.getElementById('link-parent-videos').click();
            }
            break;
        case '#disqus':
            document.getElementById('link-parent-disqus').click();
            break;
        default:
            if(isSmall()){
                document.getElementById('link-parent-disqus').click();
            }else{
                document.getElementById('link-parent-videos').click();
            }


    }
    jQuery("#parent-add-video, .parent-essay-courses").tabs();
    updateFancybox();
});

function createMsg(msg){
    SqueezeBox.initialize({
        size: {x: 300, y: 150}
    });
    var str = '<div id="enqueued-message">'+msg+'</div>';
    SqueezeBox.setContent('adopt', str);
    SqueezeBox.resize({x: 300, y: 150});
}


function updateFancybox(){
    jQuery(".fancybox").fancybox({
        maxWidth	: 800,
        maxHeight	: 600,
        fitToView	: false,
        width		: '70%',
        height		: '70%',
        autoSize	: false,
        closeClick	: false,
        openEffect	: 'none',
        closeEffect	: 'none',
        helpers:  {
            title : {
                type : 'inside',
                position: 'top'
            }
        },
        beforeLoad: function() {
            this.title = jQuery(this.element).attr('data-title'),
                this.date = jQuery(this.element).attr('data-date'),
                this.description = jQuery(this.element).attr('data-description'),
                this.share = jQuery(this.element).attr('data-share');
        }
    });
}