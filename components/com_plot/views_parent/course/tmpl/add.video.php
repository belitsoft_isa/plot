
<script src="<?php echo JURI::base() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/js/all.fineuploader-5.0.8.min.js"></script>

<?php # <editor-fold defaultstate="collapsed" desc="ADD VIDEO"> ?>
<button id="plot-add-course-video" class="add hover shadow" onclick="plotEssay.showElement('parent-add-video').hideElement('plot-add-course-video')"><?php echo  JText::_('COM_PLOT_ADD_COURSE_VIDEO');?></button>
<div id="parent-add-video" class="add-video" style="display: none;">
    <ul class="add-activity-tabs video">
        <li><a href="#parent-add-video-link">Добавить ссылку на видео</a></li>
        <li><a href="#parent-add-video-upload">Загрузить видео</a></li>

    </ul>
    <div id="parent-add-video-link">
        <div  id="form-parent-add-video-link">

            <input type="hidden" name="add-video"  value="video-link"/>
            <fieldset>
                <a id="video-modal-link"
                   rel="{size: {x: 550, y: 500}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
                   href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxVideoImage'); ?>"
                   class="modal">
                </a>
                <div class="input-wrapper">
                    <p class="als-label">
                        <svg id="video-link-close-button"  onclick="videolinkDelete()" viewBox="0 0 31.5 31.5" preserveAspectRatio="xMinYMin meet" style="fill:url(#gradient-blue); display:none;" class="close-svg">
                            <use xlink:href="#close"></use>
                        </svg>
                        <img id="video-youtube-img" src="<?php echo JUri::root() . 'templates/plot/img/blank150x150.jpg'; ?>" alt=""/>
                    </p>
                </div>
                <div class="rght-div">
                    <input type="url" id="parent-video-link" name="video-link" class="ahead plot-video-link-required"
                           placeholder="Вставьте ссылку на видео"/>
                    <svg viewBox="0 0 150 62.6" preserveAspectRatio="xMinYMin meet" class="video-link-icon">
                        <use xlink:href="#youtube"></use>
                    </svg>
                    <img src="<?php echo JURI::base() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/pre-loader-1.gif" alt="" class="pre-loader" style="opacity: 0;"/>
                    <input type="text" id="parent-video-link-ahead-1" name="video-title" class="ahead plot-video-link-required"
                           placeholder="Добавить заголовок"/>
                    <textarea id="parent-video-link-descr-1" name="video-description"
                              placeholder="Добавить описание" class="ahead  plot-video-link-required"/></textarea>
                    <button type="button" onclick="videoValidate();" class="add hover-shadow">Добавить</button>
                </div>
            </fieldset>
        </div>
    </div>
    <div id="parent-add-video-upload">
        <form id="form-parent-add-video-upload" method="post" enctype="multipart/form-data" onsubmit="return videoFileValidate();"
              action="<?php echo JRoute::_('index.php?option=com_plot&view=course&task=course.videoFileSave'); ?>">

            <fieldset>
                <div class="input-wrapper">
                    <svg id="video-file-close-button"  onclick="videoFileDelete()" viewBox="0 0 31.5 31.5" preserveAspectRatio="xMinYMin meet" style="fill:url(#gradient-blue); display:none;" class="close-svg">
                        <use xlink:href="#close"></use>
                    </svg>
                    <label id="plot-video-file-image">
                            <span id="videofile-image-area">
                                <svg viewBox="0 0 25.3 22.3" preserveAspectRatio="xMinYMin meet"><use xlink:href="#video"></use></svg>
                            </span>
                        <span class="upload-videofile-label">Загрузить видео</span>
                        <span id="videofile-upload-img"></span>
                    </label>
                    <input type="hidden" name="videofile-id" id="videofile-id"/>
                    <input type="hidden" name="course_id" value="<?php echo $id; ?>"/>
                    <a id="videofile-modal-link"
                       rel="{size: {x: 550, y: 500}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
                       href="<?php echo JRoute::_('index.php?option=com_plot&task=course.ajaxVideofileImage'); ?>"
                       class="modal"></a>
                    <script type="text/template" id="qq-template">
                        <div class="qq-uploader-selector qq-uploader">

                            <div class="qq-upload-button-selector qq-upload-button">
                                <img src="<?php echo $this->templateUrl; ?>/img/pre-loader-1.gif" alt=""style="opacity: 0;" class="pre-loader"
                                    />

                            </div>
        <span class="qq-drop-processing-selector qq-drop-processing" style="display: none;">

          <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
        </span>
                            <ul class="qq-upload-list-selector qq-upload-list" style="display: none;">
                                <li>
                                    <div class="qq-progress-bar-container-selector">
                                        <div class="qq-progress-bar-selector qq-progress-bar"></div>
                                    </div>
                                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                                    <span class="qq-edit-filename-icon-selector qq-edit-filename-icon"></span>
                                    <span class="qq-upload-file-selector qq-upload-file"></span>
                                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0"
                                           type="text">
                                    <span class="qq-upload-size-selector qq-upload-size"></span>
                                    <a class="qq-upload-cancel-selector qq-upload-cancel" href="#"></a>
                                    <a class="qq-upload-retry-selector qq-upload-retry" href="#"></a>
                                    <a class="qq-upload-delete-selector qq-upload-delete" href="#"></a>
                                    <span class="qq-upload-status-text-selector qq-upload-status-text"></span>
                                </li>
                            </ul>

                    </script>

                </div>
                <div class="rght-div">
                    <input type="text" id="parent-video-upload-ahead-1" name="video-title" class="ahead plot-video-file-required"
                           placeholder="Добавить заголовок"/>
                    <textarea id="parent-video-upload-descr-1" class="ahead plot-video-file-required" name="video-description"
                              placeholder="Добавить описание" class="ahead"/></textarea>

                    <button type="submit" class="add hover-shadow">Добавить</button>
                </div>
            </fieldset>

            <input type="hidden" name="add-video" value="video-file"/>

            <?php echo JHtml::_('form.token'); ?>
        </form>
    </div>
</div>
<?php # </editor-fold> ?>

<script type="text/javascript">
jQuery(document).ready(function () {


    jQuery('#parent-video-link').blur(function () {
        console.log(123)

        var link = jQuery('#parent-video-link').val(),
            expression = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gi,
            regex = new RegExp(expression),
            youtubeNumber = '',
            video_img = jQuery('#video-youtube-img').attr('src'),
            pos = '',
            youTubeURL,
            json;

        if (video_img.indexOf("blank150x150.jpg") != -1) {
            removeErrorClass();
            jQuery('.pre-loader').css('opacity', '0');
            if (link.match(regex)) {
                if (strpos(link, 'youtube.com') !== false || strpos(link, 'youtu.be') !== false) {
                    if (strripos(link, 'youtube.com') !== false) {
                        youtubeNumber = substr(link, strripos(link, '?v=') + 3);
                        if ((pos = strpos(youtubeNumber, '&')) !== false) youtubeNumber = substr(youtubeNumber, 0, pos)
                        {

                            youTubeURL ='https://www.googleapis.com/youtube/v3/videos?id=' + youtubeNumber + '&key=AIzaSyBDjpX4FVkv1CqrGSZqzGAxU1aTrwWA7y4&part=snippet';

                            json = (function () {
                                var json = null;
                                jQuery.ajax({
                                    'async': false,
                                    'global': false,
                                    'url': youTubeURL,
                                    'dataType': "json",
                                    'success': function (data) {
                                        json = data;
                                        if (json) {
                                            jQuery('#parent-video-link-ahead-1').val(json.items[0].snippet.title);
                                            jQuery('#parent-video-link-descr-1').val(json.items[0].snippet.description);
                                        }

                                    }
                                });

                            })();

                            jQuery.post('index.php?option=com_plot&task=course.uploadVideoImg', {youtubeNumber: youtubeNumber}, function (response) {
                                if (response.status) {
                                    jQuery('#video-modal-link').attr('href', '<?php echo JRoute::_('index.php?option=com_plot&task=course.ajaxVideoImage');?>' + '&img_name=' + youtubeNumber);
                                    console.log( '<?php echo JRoute::_('index.php?option=com_plot&task=course.ajaxVideoImage');?>' + '&img_name=' + youtubeNumber );
                                    document.getElementById('video-modal-link').click();
                                    jQuery('.pre-loader').css('opacity', '0');
                                } else {
                                    jQuery('.plot-video-link-required').each(function() {
                                        if(jQuery(this).val()==''){
                                            addErrorClass(jQuery(this));
                                        }
                                    });
                                }
                            });


                        }
                    } else {
                        alert('youtube.com');
                        jQuery('.pre-loader').css('opacity', '0');
                    }
                } else {
                    alert('youtube.com');
                    jQuery('.pre-loader').css('opacity', '0');
                }

            } else {
                jQuery('.plot-video-link-required').each(function() {
                    if(jQuery(this).val()==''){
                        addErrorClass(jQuery(this));
                    }
                });
                jQuery('.pre-loader').css('opacity', '0');
            }
        }
    });



});

    var manualUploaderVideofile = new qq.FineUploader({
        element: document.getElementById("videofile-upload-img"),
//jQuery('#videofile-upload-img').fineUploader({
    request: {
        endpoint: '<?php echo JRoute::_("index.php?option=com_plot&task=course.videoFileUpload")?>'
    },
    multiple: false,
        validation: {
            sizeLimit: parseInt('<?php echo (int)PlotHelper::returnBytes(ini_get('upload_max_filesize'));?>')
        },
    callbacks: {
        onComplete: function (id, filename, responseJSON) {
            if (responseJSON.status) {
                jQuery('#videofile-modal-link').attr('href', '<?php echo JRoute::_('index.php?option=com_plot&task=course.ajaxVideofileImage'); ?>&img_name=' + responseJSON.name);
                jQuery('#videofile-id').val(responseJSON.name);
                document.getElementById('videofile-modal-link').click();
                jQuery('.pre-loader').css('opacity','0');
                jQuery('#plot-dinamicaly-loading').css({'z-index': '75555'}).show();
            } else {
                errorAddPlotMessage(responseJSON.message);
            }
        }
    }
});


function videoFileValidate() {
    var video_file_error=true,
        videofile_id=jQuery('#videofile-id').val();
    removeErrorClass();
    jQuery('#form-parent-add-video-upload .plot-video-file-required').each(function() {
        if (jQuery(this).val() == '') {
            video_file_error = false;
            addErrorClass(jQuery(this));
        }
    });

    if(!videofile_id){
        video_file_error=false;
        addErrorClass(jQuery(jQuery('#plot-video-file-image')));
    }

    return video_file_error;
}

function videoFileDelete() {
    var file_name = jQuery('#videofile-id').val();
    if (file_name) {
        jQuery.post('index.php?option=com_plot&task=course.videoFileDeleteImg', {
            file_name: file_name
        }, function(response) {
            var data = jQuery.parseJSON(response);
            if (data.status) {
                jQuery('#videofile-id').val('');
                jQuery('#videofile-image-area').html('<svg viewBox="0 0 25.3 22.3" preserveAspectRatio="xMinYMin meet"><use xlink:href="#video"></use></svg>');
                jQuery('#video-file-close-button').hide();
            } else {
                errorAddPlotMessage(data.message);
            }
        });
    }
}

function videoValidate() {
    var noVideoLinkError = true;
    removeErrorClass();
    jQuery('#form-parent-add-video-upload .plot-video-link-required').each(function() {
        if(jQuery(this).val()==''){
            noVideoLinkError=false;
            addErrorClass(jQuery(this));
        }
    });
    if (noVideoLinkError) {

        saveVideo();
    }

    return noVideoLinkError;
}

function saveVideo(){
    var video_link=jQuery('#parent-video-link').val(),
        video_title=jQuery('#parent-video-link-ahead-1').val(),
        video_desc=jQuery('#parent-video-link-descr-1').val(),
        course_id='<?php echo  $id; ?>';


    jQuery.post('index.php?option=com_plot&task=course.saveCourseVideo', {
        video_link: video_link,
        video_title:video_title,
        video_desc:video_desc,
        course_id:course_id
    }, function(response) {
        console.log(response);
        var data = jQuery.parseJSON(response);
        if (data.status) {
            errorAddPlotMessage(data.message);
            plotEssay.showElement('plot-add-course-video').hideElement('parent-add-video')
            jQuery('#video-link-close-button').hide();
            jQuery('#parent-video-link').val('');
            jQuery('#parent-video-link-ahead-1').val('');
            jQuery('#parent-video-link-descr-1').val('');
            jQuery('#video-youtube-img').attr('src','<?php echo JURI::root()."templates/plot/img/blank150x150.jpg"?>');
            jQuery('#link-parent-videos').click();
            updateFancybox();
        } else {
            errorAddPlotMessage(data.message);
        }
    });
}


function videolinkDelete() {
    var  video_img = jQuery('#video-youtube-img').attr('src'),
        link=jQuery('#parent-video-link').val();

    if (video_img.indexOf("blank150x150.jpg") == -1) {
        jQuery.post('index.php?option=com_plot&task=profile.videolinkDeleteImg', {
            link: link
        }, function(response) {
            var data = jQuery.parseJSON(response);
            if (data.status) {
                jQuery('#video-youtube-img').attr("src","<?php echo JUri::root() . 'templates/plot/img/blank150x150.jpg'; ?>");
                jQuery('#video-link-close-button').hide();
            } else {
                errorAddPlotMessage(data.message);
            }
        });
    }
}






function errorAddPlotMessage(error_message){
    SqueezeBox.initialize({
        size: {x: 300, y: 150}
    });
    var str = '<div id="enqueued-message">' + error_message + '</div>';
    SqueezeBox.setContent('adopt', str);
    SqueezeBox.resize({x: 300, y: 150});
    jQuery('.pre-loader').css('opacity','0');
}

function addErrorClass(jquery_element){
    jQuery(jquery_element).addClass('plot_error');
    jQuery(jquery_element).attr("placeholder", "Заполните поле");
}

function removeErrorClass(){
    jQuery('.plot_error').removeClass('plot_error');
}

function strpos(haystack, needle, offset) {
    var i = (haystack + '')
        .indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
}

function strripos(haystack, needle, offset) {
    haystack = (haystack + '')
        .toLowerCase();
    needle = (needle + '')
        .toLowerCase();

    var i = -1;
    if (offset) {
        i = (haystack + '')
            .slice(offset)
            .lastIndexOf(needle); // strrpos' offset indicates starting point of range till end,
        // while lastIndexOf's optional 2nd argument indicates ending point of range from the beginning
        if (i !== -1) {
            i += offset;
        }
    } else {
        i = (haystack + '')
            .lastIndexOf(needle);
    }
    return i >= 0 ? i : false;
}

function substr(str, start, len) {
    var i = 0,
        allBMP = true,
        es = 0,
        el = 0,
        se = 0,
        ret = '';
    str += '';
    var end = str.length;

    // BEGIN REDUNDANT
    this.php_js = this.php_js || {};
    this.php_js.ini = this.php_js.ini || {};
    // END REDUNDANT
    switch ((this.php_js.ini['unicode.semantics'] && this.php_js.ini['unicode.semantics'].local_value.toLowerCase())) {
        case 'on':
            // Full-blown Unicode including non-Basic-Multilingual-Plane characters
            // strlen()
            for (i = 0; i < str.length; i++) {
                if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
                    allBMP = false;
                    break;
                }
            }

            if (!allBMP) {
                if (start < 0) {
                    for (i = end - 1, es = (start += end); i >= es; i--) {
                        if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
                            start--;
                            es--;
                        }
                    }
                } else {
                    var surrogatePairs = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;
                    while ((surrogatePairs.exec(str)) != null) {
                        var li = surrogatePairs.lastIndex;
                        if (li - 2 < start) {
                            start++;
                        } else {
                            break;
                        }
                    }
                }

                if (start >= end || start < 0) {
                    return false;
                }
                if (len < 0) {
                    for (i = end - 1, el = (end += len); i >= el; i--) {
                        if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
                            end--;
                            el--;
                        }
                    }
                    if (start > end) {
                        return false;
                    }
                    return str.slice(start, end);
                } else {
                    se = start + len;
                    for (i = start; i < se; i++) {
                        ret += str.charAt(i);
                        if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
                            se++; // Go one further, since one of the "characters" is part of a surrogate pair
                        }
                    }
                    return ret;
                }
                break;
            }
            break;
        // Fall-through
        case 'off':
        // assumes there are no non-BMP characters;
        //    if there may be such characters, then it is best to turn it on (critical in true XHTML/XML)
            break;
        default:
            if (start < 0) {
                start += end;
            }
            end = typeof len === 'undefined' ? end : (len < 0 ? len + end : len + start);
            // PHP returns false if start does not fall within the string.
            // PHP returns false if the calculated end comes before the calculated start.
            // PHP returns an empty string if start and end are the same.
            // Otherwise, PHP returns the portion of the string from start to end.
            return start >= str.length || start < 0 || start > end ? !1 : str.slice(start, end);
    }
    return undefined; // Please Netbeans
}

</script>