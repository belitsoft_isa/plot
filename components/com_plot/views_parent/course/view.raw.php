<?php

defined('_JEXEC') or die;
require_once JPATH_COMPONENT.'/views_parent/view.php';

class PlotViewCourse extends PlotViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'course';

    public function display($tpl = null)
    {
        return parent::display($tpl);
    }

    public function ajaxRenderList()
    {
        return $this->loadTemplate();
    }

}
