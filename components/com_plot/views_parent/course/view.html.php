<?php
defined('_JEXEC') or die;

class PlotViewCourse extends PlotViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'course';

    public function display($tpl = null)
    {
        $app = JFactory::getApplication();
        $this->templateUrl = JURI::base().'templates/'.JFactory::getApplication()->getTemplate();
        $this->componentUrl = JURI::base() . 'components/com_plot';
        $this->referrerUrl = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : JRoute::_('index.php?option=com_plot&view=river');
        
        $courseModel = $this->getModel();
        $coursesModel = JModelLegacy::getInstance('courses', 'plotModel');
        
        $this->my = plotUser::factory();
        $this->my->level = $this->my->getLevel();

        $this->course = $courseModel->getCourse(JRequest::getInt('id', 0));
        $this->course->costsForBuyers = $this->course->getBuyerCosts(plotGlobalConfig::getVar('payForCourseCompleteDefault'));
        if (!$this->course->isPublished()) {
            $app->enqueueMessage(JText::_('COM_PLOT_REQUESTED_PAGE_NOT_EXISTS_ANYMORE'), 'message');
            if ($this->my->id) {

                $app->redirect(JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('error404K2ItemId'), false));
                // $app->redirect( JRoute::_('index.php?option=com_plot&view=profile&id='.$this->my->id, false) );
            } else {
                $app->redirect(JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('error404K2ItemId'), false));
                //$app->redirect( JRoute::_('index.php?option=com_plot&view=river', false) );
            }
        }
        $this->course->isBoughtForMe = false;
        $coursesBoughtForMe = $coursesModel->getCoursesPaidNewTop( array('userId' => $this->my->id) );
        foreach ($coursesBoughtForMe AS $courseboughtForMe) {
            if ( $this->course->id == $courseboughtForMe->id ) {
                $this->course->isBoughtForMe = true;
                $this->course->percentCompleted = $courseboughtForMe->percentCompleted;
                $this->course->is_new = $courseboughtForMe->is_new;
            }
        }
        
        $this->course->routedLinkToLearningPath = JRoute::_("index.php?option=com_joomla_lms&Itemid=0&task=show_lpath&course_id=".$this->course->id."&id=".PlotHelperJLMS::getLearningPathId($this->course->id));

        $this->my->childrenIdsHavntCurrentCourse = array();
        foreach ($this->my->getChildrenIds() AS $childId) {
            $child = plotUser::factory($childId);
            $coursesBoughtForChild = $child->getCoursesIdsBoughtForMe();
            if (!in_array($this->course->id, $coursesBoughtForChild)) {
                $this->my->childrenIdsHavntCurrentCourse[] = $child->id;
            }
        }


        $this->couresBook= new plotBook($this->course->id_pub);

        if ($this->couresBook) {
            $this->couresBook->popupWidth = $this->couresBook->width * 2 + 66;
            $this->couresBook->popupHeight = $this->couresBook->height + 100;
            $thumbnailPath = JPATH_SITE.'/media/com_html5flippingbook'.'/thumbs/'.$this->couresBook->c_thumb;
            if ($this->couresBook->c_thumb == "" || !is_file($thumbnailPath)) {
                $this->couresBook->thumbnailUrl = JURI::root()."images/com_plot/def_book.jpg";
            } else {
                if (file_exists(JPATH_BASE.'/media/com_html5flippingbook'.'/thumbs/thimb_'.$this->couresBook->c_thumb)) {
                    $this->couresBook->thumbnailUrl = JURI::root()."media/com_html5flippingbook/thumbs/thimb_".$this->couresBook->c_thumb;
                } else {
                    $this->couresBook->thumbnailUrl = JURI::root()."images/com_plot/def_book.jpg";
                }
            }


        }
        $coursesModel = $this->getModel('course');
        $courseBefore=$coursesModel->getBeforeHashtagsCourses(array(),$this->course->id);
        $this->courseBefore=(int)$courseBefore['countItems'];

        $courseAfter=$coursesModel->getAfterHashtagsCourses(array(),$this->course->id);
        $this->courseAfter=(int)$courseAfter['countItems'];
        $this->courseObj=new plotCourse($this->course->id);
        $this->coursePrograms=$this->courseObj->coursePrograms();
        foreach($this->coursePrograms AS $item){
            $item->percent=(int)$this->my->programProgress($item->id);
        }

        $this->setOgMeta();
        if(JRequest::getVar('orderSumAmount', 0)){
            $app->redirect(JRoute::_('index.php?option=com_plot&view=course&id='.$this->course->id, false));
        }
        return parent::display($tpl);
    }
    
    private function setOgMeta()
    {
        $db = JFactory::getDbo();
        require_once JPATH_ADMINISTRATOR.'/components/com_plot/plot_social_config.php';
        $metaOgUrl = '<meta property="og:url" content="'.PlotHelper::getAbsRoutedUri('index.php?option=com_plot&view=course&id='.$this->course->id).'" />';
        $metaOgTitle = '<meta property="og:title" content="'.$db->escape($this->course->course_name).'" />';
        $metaOgDescription = '<meta property="og:description" content="'.$db->escape(strip_tags($this->course->course_description)).'" />';
        $metaOgImage = '<meta property="og:image" content="'.PlotHelper::getAbsRoutedUri('index.php?option=com_plot&view=course&task=course.pickoutimage&id='.$this->course->id).'&canv_width=200&canv_height=200" />';
        $metaOgSiteName = '<meta property="og:site_name" content="'.JFactory::getConfig()->get('sitename').'" />';
        $metaOgFbAppId = '<meta property="fb:app_id" content="473991812742077" />';
        $metaOgType = '<meta property="og:type" content="website" />';
        $metaOgLocale = '<meta property="og:locale" content="ru_RU" />';
        JFactory::getDocument()->addCustomTag($metaOgTitle.$metaOgSiteName.$metaOgUrl.$metaOgDescription.$metaOgFbAppId.$metaOgType.$metaOgLocale.$metaOgImage);
        return true;
    }
    
}
