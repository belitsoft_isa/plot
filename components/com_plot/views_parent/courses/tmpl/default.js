function checkAllFilterCoursesTagsCheckboxes()
{
    jQuery("[id^=filter-courses-tag]").prop('checked', true);
    showFilteredAndSortedCourses();
}
function clearAllFilterCoursesTagsCheckboxes()
{
    jQuery("[id^=filter-courses-tag]").prop('checked', false);
    showFilteredAndSortedCourses();
}


