<?php
defined('_JEXEC') or die;
?>

<script src="<?php echo $this->componentUrl; ?>/libraries/scrolling_ajax_pagination/javascript.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'media/jui/js/jquery.ui.core.min.js'; ?>"></script>

<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.min.js"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.basic.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate().'/js/jquery.jscrollpane.min.js'; ?>" async="async" ></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate().'/js/jquery.mousewheel.js'; ?>" async="async" ></script>
<script type="text/javascript" src="<?php echo JUri::root().'components/com_plot/views_parent/courses/tmpl/default.js'; ?>"></script>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">
    function showFilteredAndSortedCourses()
    {
        setAxaxScrollPagination();

        var tagsIds = jQuery("[id^=filter-courses-tag]:checked").map(function(){
            return jQuery(this).val();
        }).get();

        jQuery('#courses-list').html('<div class="p5_20">Загрузка...</div>');

        jQuery.post(
            'index.php?option=com_plot&task=courses.ajaxGetFilteredAndSortedCourses',
            {
                ageId: jQuery('#parent-my-age').val(),
                tagsIds: tagsIds,
                categoryId: jQuery('#par-sel-course-1').val(),
                myCoursesOnly: jQuery('.show-only-my-courses').hasClass('ui-state-active') ? 1 : 0, 
                sort: jQuery('#parent-course-filter').val(),
                limit: '<?php echo plotGlobalConfig::getVar('coursesResultsShowFirstCountParent') + plotGlobalConfig::getVar('coursesResultsShowFirstCountParent'); ?>'
            },
            function(response) {
                var data = jQuery.parseJSON(response);
                jQuery('#courses-count-founded').html('Найдено '+jPlot.helper.declOfNum(data.countCourses, ['курс', 'курса', 'курсов']));
                jQuery('#courses-list').html(data.renderedCourses);
            }
        );
    }    
    function setAxaxScrollPagination()
    {
        var tagsIds = jQuery("[id^=filter-courses-tag]:checked").map(function(){
            return jQuery(this).val();
        }).get();
        var ajaxScrollData = {
            ageId: jQuery('#parent-my-age').val(),
            tagsIds: tagsIds,
            categoryId: jQuery('#par-sel-course-1').val(), 
            myCoursesOnly: jQuery('.show-only-my-courses').hasClass('ui-state-active') ? 1 : 0, 
            sort: jQuery('#parent-course-filter').val()
        };
        
        jQuery(window).unbind('scroll');
        
        jQuery('#courses-list').scrollPagination({
            nop: <?php echo plotGlobalConfig::getVar('coursesResultsShowFirstCountParent'); ?>,
            offset: <?php echo plotGlobalConfig::getVar('coursesResultsShowFirstCountParent')+plotGlobalConfig::getVar('coursesResultsShowFirstCountParent'); ?>,
            error: 'No More Posts!',
            delay: 50,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=courses.ajaxLoadMore'); ?>',
            appendDataTo: 'courses-list',
            userData: ajaxScrollData
        });

        jQuery(window).scroll(function(){
            jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
        });

    }
    jQuery(document).ready(function(){
        //showFilteredAndSortedCourses();
        //setAxaxScrollPagination();
        jQuery( "#parent-courses" ).tabs();

        // courses tabs click
        jQuery('.show-only-my-courses > a').click(function(){
            jQuery(this).addClass('ui-state-active');
            showFilteredAndSortedCourses();
        });
        jQuery('.show-all-courses > a').click(function(){
            jQuery('.show-only-my-courses').removeClass('ui-state-active');
            showFilteredAndSortedCourses();
        });


        jQuery('#par-sel-course-1, #parent-course-filter, #parent-my-age').selectmenu({
            appendTo:'.main-content',
            change: function() { showFilteredAndSortedCourses(); }
        });
        jQuery('#par-sel-course-1').selectmenu({open: function( event, ui ) { jQuery('#par-sel-course-1-menu').jScrollPane(); }});
        jQuery('#parent-course-filter').selectmenu({open: function( event, ui ) { jQuery('#parent-course-filter-menu').jScrollPane(); }});
        jQuery('#parent-my-age').selectmenu({open: function( event, ui ) { jQuery('#parent-my-age-menu').jScrollPane(); }});
        jQuery('#parent-my-interest-1').selectmenu({
            appendTo:'.main-content',
            open: function( event, ui ) { jQuery('#parent-my-interest-1-menu').jScrollPane(); },
            change: function() {
                var selectedValue = jQuery('#parent-my-interest-1').val();
                switch (selectedValue) {
                    case ('my'):
                        jQuery('[id^=filter-courses-tag]').prop('checked', false);

                        jQuery.post(
                            'index.php?option=com_plot&task=publications.ajaxGetMyTags',
                            function(response) {
                                var data = jQuery.parseJSON(response),
                                    i= 0,
                                    data_count=data.length,
                                    str='';
                                jQuery('[id^=filter-courses-tag]').prop('checked', false);
                                for (i; i<data_count; i++) {
                                    str+='<div>'+data[i].id+'</div>';
                                }
                                jQuery('hidden-data.interests-data .my').html(str);
                                jQuery('hidden-data.interests-data .my div').each(function(){
                                    var tagId = jQuery(this).html();
                                    jQuery('#filter-courses-tag'+tagId).prop('checked', true);
                                });
                                showFilteredAndSortedCourses();
                            }
                        );
                        break;
                    case ('all'):
                        jQuery('[id^=filter-courses-tag]').prop('checked', true);
                        showFilteredAndSortedCourses();
                        break;
                    default:
                        jQuery('[id^=filter-courses-tag]').prop('checked', false);
                        jQuery('hidden-data.interests-data .'+jQuery(this).val()+' div').each(function(){
                            var tagId = jQuery(this).html();
                            jQuery('#filter-courses-tag'+tagId).prop('checked', true);
                        });
                        showFilteredAndSortedCourses();
                }
            }
        });

        jQuery(window).scroll(function(){
            jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
        });

        <?php if(plotUser::factory()->isHaveCourses()){
       ?>
        jQuery('.show-only-my-courses > a').click();
        <?php
        }else{ ?>
        jQuery('.show-all-courses > a').click();
    <?php }?>

    });

    document.addEventListener("touchmove", function(){
        jQuery('body').trigger('scroll');
        console.log('ScrollStart');
    }, false);
</script>
<?php # </editor-fold> ?>

<main class="parent-profile">
    <div class="top parent-profile">
        <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_header') ); ?>
        <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_top_menu') ); ?>
    </div>
    <div class="wrap main-wrap parent-profile">
        <div class="add-left aside-left parent-profile"></div>
        <div class="add-right aside-right parent-profile"></div>
        <section class="parent-courses">
            <div id="parent-courses">
                <ul class="parent-courses-think-tabs">
                    <!--<li><a href="#parent-courses-jcarousel">Раздел курсов</a></li>-->
                    <li class="show-all-courses"><a href="#parent-all-site-courses">Общий раздел всех курсов</a></li>
                    <li class="show-only-my-courses"><a href="#parent-all-site-courses">Все мои курсы</a></li>
                </ul>
                <?php # <editor-fold defaultstate="collapsed" desc="COURSES"> ?>
               <!-- <div id="parent-courses-jcarousel" class="jcarousel-wrapper parent-profile">
                    <div class="jcarousel">
                        <ul class="parent-profile">
                            <?php foreach ($this->my->coursesNewTop AS $courseNewTop) { ?>
                            <li>
                                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=course&id='.$courseNewTop->id); ?>">
                                    <?php if ($courseNewTop->is_new) { ?>
                                    <span class="arrow-bottom">Новый!</span>
                                    <?php } else { ?>
                                    <span class="in-progress">Завершено <?php echo $courseNewTop->percentCompleted; ?>%</span>
                                    <?php } ?>
                                    <h6><?php echo $courseNewTop->course_name; ?></h6>
                                    <div class="circle-img">
                                        <svg class="clip-svg-course">
                                            <image style="clip-path: url(#clipping-circle-course);" width="100%" height="100%" xlink:href="<?php echo JUri::root().$courseNewTop->image; ?>" alt="" />
                                        </svg>
                                        <i class="activity-icons">
                                            <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);">
                                                <use xlink:href="#academic-hat"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <p><?php echo strip_tags($courseNewTop->course_description); ?></p>
                                </a>
                                <?php if (in_array($courseNewTop->id, $this->my->getCoursesIdsBoughtForMe())) { ?>
                                <button onclick="window.location.href='<?php echo $courseNewTop->routedLinkToLearningPath; ?>'" 
                                        class="add hover-shadow">
                                        <?php if (in_array($courseNewTop->id, $this->my->getCoursesIdsBoughtForMe(true))) { ?>
                                            <?php echo JText::_('COM_PLOT_DO_COURSE'); ?>
                                        <?php } else { ?>
                                            <?php echo JText::_('COM_PLOT_REPEAT_COURSE'); ?>
                                        <?php } ?>
                                </button>
                                <?php } ?>                                
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <p class="jcarousel-pagination"></p>
                    <?php if (!$this->my->coursesNewTop) { ?>
                    <div class="no-items">Нет курсов</div>
                    <?php } ?>
                </div>     -->
                <?php # </editor-fold> ?>
                <?php # <editor-fold defaultstate="collapsed" desc="ALL COURSES"> ?>
                <div id="parent-all-site-courses">
                    <div class="select-feed">
                        <form>
                            <fieldset>
                                <select id="parent-my-interest-1">
                                    <option value="all" selected="selected">Все интересы</option>
                                    <option value="my">Мои интересы</option>
                                    <?php foreach ($this->my->childrensIds AS $childId) { ?>
                                    <option value="child-<?php echo $childId; ?>">Интересы ребенка <?php echo plotUser::factory($childId)->name; ?></option>
                                    <?php } ?>
                                </select>
                                <!-- hidden my interests and my children's interests for javascript and fast no-ajax choosing interests -->
                                <hidden-data class="hidden interests-data">
                                    <item class="all">
                                        <?php foreach ($this->plotTags AS $i=>$tag) { ?>
                                        <div><?php echo $tag->id; ?></div>
                                        <?php } ?>
                                    </item>
                                    <item class="my">
                                        <?php foreach ($this->my->getTags() AS $tag) { ?>
                                        <div><?php echo $tag->id; ?></div>
                                        <?php } ?>
                                    </item>
                                    <?php foreach ($this->my->childrensIds AS $childId) { ?>
                                    <item class="child-<?php echo $childId; ?>">
                                        <?php foreach (plotUser::factory($childId)->getTags() as $childTag) { ?>
                                        <div><?php echo $childTag->id; ?></div>
                                        <?php } ?>
                                    </item>
                                    <?php } ?>
                                </hidden-data>
                                <select id="parent-my-age">
                                    <option value="0"><?php echo JText::_('COM_PLOT_ALL_AGES'); ?></option>
                                    <?php foreach ($this->plotAges AS $age) { ?>
                                        <option value="<?php echo $age->id; ?>"><?php echo $age->title; ?></option>
                                    <?php } ?>
                                </select>
                            </fieldset>
                        </form>
                        <a class="link-back" href="<?php echo $this->referrerUrl; ?>">Назад</a>
                    </div>
                    <div class="checkbox-interest jcarousel-wrapper">
                        <div class="jcarousel">
                            <ul class="parent-profile">
                                <?php foreach ($this->plotTags AS $i=>$tag) { ?>
                                <?php echo ($i % 6) ? '' : '<li>'; ?>
                                <input type="checkbox" checked="checked" id="filter-courses-tag<?php echo $tag->id; ?>" value="<?php echo $tag->id; ?>" onchange="showFilteredAndSortedCourses();" />
                                <label for="filter-courses-tag<?php echo $tag->id; ?>"><?php echo $tag->title; ?></label>
                                <?php } ?>                            
                                <?php echo ($i % 6) ? '' : '</li>'; ?>
                            </ul>
                        </div>
                        <button onclick="checkAllFilterCoursesTagsCheckboxes();">Выбрать все</button>
                        <button onclick="clearAllFilterCoursesTagsCheckboxes();">Очистить</button>
                        <p class="jcarousel-pagination"></p>
                    </div>
                    <div class="filter-result">
                        <form>
                            <fieldset>
                                <p>
                                    <label for="par-sel-course-1" id="courses-count-founded">Найдено 39 курсов</label>
                                    <select id="par-sel-course-1">
                                        <option value="0"><?php echo JText::_('COM_PLOT_ALL_CATEGORIES'); ?></option>
                                        <?php foreach ($this->coursesCategories AS $courseCategory) { ?>
                                        <option value="<?php echo $courseCategory->id; ?>"><?php echo $courseCategory->c_category; ?></option>
                                        <?php } ?>                                    
                                    </select>
                                </p>
                                <p>
                                    <label for="parent-course-filter">Сортировать:</label>
                                    <select id="parent-course-filter">
                                        <option value="`total_min_cost` ASC">Цена с дешевых</option>
                                        <option value="`total_min_cost` DESC">Цена с дорогих</option>
                                        <option value="`c`.`start_date` DESC">Дата добавления курса(с новых)</option>
                                    </select>
                                </p>
                            </fieldset>
                        </form>
                    </div>
                    <div id="courses-list"></div>
                </div>
                <?php # </editor-fold> ?>
            </div>
        </section>
    </div>
</main>
