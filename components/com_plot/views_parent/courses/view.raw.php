<?php

require_once JPATH_COMPONENT.'/views_parent/view.php';

class PlotViewCourses extends PlotViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'courses';

    public function display($tpl = null)
    {
        return parent::display($tpl);
    }
    
    public function ajaxRenderCoursesList()
    {
        $this->my = plotUser::factory();
        return $this->loadTemplate();
    }

}
