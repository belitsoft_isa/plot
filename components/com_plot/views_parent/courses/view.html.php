<?php
defined('_JEXEC') or die;

class PlotViewCourses extends PlotViewParentLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'courses';

    public function display($tpl = null)
    {
        $coursesModel = $this->getModel();
        $this->componentUrl = JURI::base().'components/com_plot';
        $this->templateUrl = JURI::base().'templates/'.JFactory::getApplication()->getTemplate();

        $this->referrerUrl =  JRoute::_('index.php?option=com_plot&view=courses');
        
        $this->my = plotUser::factory();
        $this->my->level = $this->my->getLevel();
        $this->my->coursesNewTop = $coursesModel->getCoursesPaidNewTop( array('userId' => $this->my->id, 'limit' => plotGlobalConfig::getVar('coursesNewShowForParent')) );
        $this->my->childrensIds = $this->my->getChildrenIds();

        $this->plotAges = plotAges::getList();
        $this->plotTags = plotTags::getK2TagsList();
        $this->coursesCategories = $coursesModel->getCoursesCategoriesWhichHavePublishedCourses();
        
        $this->showOnlyMyCourses = JRequest::getVar('onlymycourses', false);
        
        $this->setOgMeta();
        return parent::display($tpl);
    }

    private function setOgMeta()
    {
        require_once JPATH_ADMINISTRATOR.'/components/com_plot/plot_social_config.php';
        $metaOgUrl = '<meta property="og:url" content="http://naplotu.com/courses/" />';
        $metaOgTitle = '<meta property="og:title" content="'.plotSocialConfig::get('coursesTitle').'" />';
        $metaOgDescription = '<meta property="og:description" content="'.plotSocialConfig::get('coursesDesc').'" />';
        $metaOgImage = '<meta property="og:image" content="'.JUri::root().plotSocialConfig::get('coursesImagePath').'" />';
        $metaOgImageType = '<meta property="og:image:type" content="image/jpeg" />';
        $metaOgImageWidth = '<meta property="og:image:width" content="'.plotSocialConfig::get('coursesImageWidth').'" />';
        $metaOgImageHeight = '<meta property="og:image:height" content="'.plotSocialConfig::get('coursesImageHeight').'" />';
        $metaOgSiteName = '<meta property="og:site_name" content="'.JFactory::getConfig()->get('sitename').'" />';
        $metaOgFbAppId = '<meta property="fb:app_id" content="473991812742077" />';
        $metaOgType = '<meta property="og:type" content="website" />';
        $metaOgLocale = '<meta property="og:locale" content="ru_RU" />';
        JFactory::getDocument()->addCustomTag($metaOgTitle.$metaOgSiteName.$metaOgUrl.$metaOgDescription.$metaOgFbAppId.$metaOgType.$metaOgLocale.$metaOgImage.$metaOgImageType.$metaOgImageWidth.$metaOgImageHeight);
        return true;
    }
        
    
}
