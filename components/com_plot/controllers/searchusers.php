<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class PlotControllerSearchUsers extends PlotController
{

    public function showPopup()
    {
        $view = $this->getView('SearchUsers', 'raw');

        $view->display();
        die;
    }

    public function ajaxGetUsersList()
    {
        $model = JModelLegacy::getInstance('searchUsers', 'plotModel');
        
        $view = $this->getView('SearchUsers', 'raw');
        $view->setLayout('users.list');
        $view->courseId = JRequest::getInt('courseId', '0');
        
        $filter = array();
        $searchValue = JRequest::getVar('searchValue', '');
        $page = JRequest::getVar('page', '0');
        
        if ($searchValue) {
            $filter[] = array('field' => 'u.name', 'type' => 'like', 'value' => $searchValue);
        }
        $view->users = $model->getUsers( $filter, 'u.name ASC', $limit = array('limitstart' => ($page - 1) * plotGlobalConfig::getVar('usersCountSearchForBuyCourse'), 'limit' => plotGlobalConfig::getVar('usersCountSearchForBuyCourse')) );
        $view->totalUser = $model->totalUsers;
        
        $view->pagination = new JPagination( $view->totalUser, ($page - 1) * plotGlobalConfig::getVar('usersCountSearchForBuyCourse'), plotGlobalConfig::getVar('usersCountSearchForBuyCourse') );
        
        $result = $view->loadTemplate();
        if ($result instanceof Exception) {
            return $result;
        }
        
        $data['html'] = $result;

        echo json_encode($data);
        die;
    }
    
}
