<?php defined('_JEXEC') or die('Restricted access');


class PlotControllerPortfolio extends JControllerForm
{
    //----------------------------------------------------------------------------------------------------
    public function add()
    {
        $app = JFactory::getApplication();
        $context = "$this->option.edit.$this->context";

        // Clear the record edit information from the session.
        $app->setUserState($context . '.data', null);

        // Redirect to the edit screen.
        $this->setRedirect(
            JRoute::_(
                'index.php?option=' . $this->option . '&view=' . $this->view_item
                    . $this->getRedirectToItemAppend(), false
            )
        );

        return true;
    }

//-------------------------------------------------------------------------------------------------------


    public function edit($key = null, $urlVar = null)
    {
        $app = JFactory::getApplication();
        $model = $this->getModel();
        $table = $model->getTable();
        $cid = $this->input->post->get('cid', array(), 'array');
        $context = "$this->option.edit.$this->context";

        // Determine the name of the primary key for the data.
        if (empty($key)) {
            $key = $table->getKeyName();
        }

        // To avoid data collisions the urlVar may be different from the primary key.
        if (empty($urlVar)) {
            $urlVar = $key;
        }

        // Get the previous record id (if any) and the current record id.
        $recordId = (int)(count($cid) ? $cid[0] : $this->input->getInt($urlVar));
        $checkin = property_exists($table, 'checked_out');


        // Attempt to check-out the new record for editing and redirect.
        if ($checkin && !$model->checkout($recordId)) {
            // Check-out failed, display a notice but allow the user to see the record.
            $this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_CHECKOUT_FAILED', $model->getError()));
            $this->setMessage($this->getError(), 'error');

            $this->setRedirect(
                JRoute::_(
                    'index.php?option=' . $this->option . '&view=' . $this->view_item
                        . $this->getRedirectToItemAppend($recordId, $urlVar), false
                )
            );

            return false;
        } else {
            // Check-out succeeded, push the new record id into the session.
            $this->holdEditId($context, $recordId);
            $app->setUserState($context . '.data', null);

            $this->setRedirect(
                JRoute::_(
                    'index.php?option=' . $this->option . '&view=' . $this->view_item
                        . $this->getRedirectToItemAppend($recordId, $urlVar), false
                )
            );

            return true;
        }
    }

    //----------------------------------------------------------------------------------------------------
    public function save($key = null, $urlVar = null)
    {

        $jinput = JFactory::getApplication()->input;

        $requestData = $this->input->post->get('jform', array(), 'array');

        $task = $this->input->post->get('task', '', 'CMD');
        $model = $this->getModel();

        // Validate the posted data.
        $form = $model->getForm();

        if (!$form) {
            JError::raiseError(500, $model->getError());
            return false;
        }

        if ($model->save($requestData)) {


            $this->setMessage('User successfully saved.');


            $this->setRedirect('index.php?option=com_plot&view=portfolios');

        } else {
            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('COM_SCHOOL_INSCHRIJVEN_SAVE_FAILED', $model->getError()), 'warning');
            $this->setRedirect('index.php?option=com_school&view=schoollicense&layout=edit&id=' . (int)$requestData['id']);
            return false;
        }

        return true;
        $model->save();
        parent::save($key, $urlVar);
    }

    public function delete()
    {

        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Get items to remove from the request.
        $cid = $this->input->get('cid', array(), 'array');

        if (!is_array($cid) || count($cid) < 1) {
            JError::raiseWarning(500, JText::_($this->text_prefix . '_NO_ITEM_SELECTED'));
        } else {
            // Get the model.
            $model = $this->getModel();

            // Remove the items.
            if ($model->delete($cid)) {
                $this->setMessage(JText::plural($this->text_prefix . '_N_ITEMS_DELETED', count($cid)));
            } else {
                $this->setMessage($model->getError());
            }
        }

        $this->setRedirect(JRoute::_('index.php?option=com_plot&view=portfolios', false));
    }

    public function deleteImg()
    {
        $id = $this->input->get('id', 0, 'INT');
        $model = $this->getModel();
        if( $model->deleteImg($id)){
            echo true;
        }else{
           echo false;
        }
        die;
    }

    public function portfolioImageUpload(){
        $file = JRequest::getVar('qqfile', '', 'FILES');
        $data = array();
        $db = Foundry::db();
        $rEFileTypes = "/^\.(jpg|jpeg|gif|png|bmp|xcf|odg){1}$/i";
        $access = Foundry::access(Foundry::user()->id, SOCIAL_TYPE_USER);
        $maxFilesizeBytes = (int)$access->get('photos.uploader.maxsize') * 1048576;

        if (!empty($file['tmp_name'])) {
            if (($file['size']) <= $maxFilesizeBytes ) {

            $image_path = JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'original';
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio')) {
                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio');
            }
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio' . DIRECTORY_SEPARATOR . Foundry::user()->id)) {
                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio' . DIRECTORY_SEPARATOR . Foundry::user()->id);
            }
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'original')) {
                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'original');
            }
            $ext = JFile::getExt($file['name']);
            $new_name = md5(time() . $file['name']) . '.' . $ext;
            if (preg_match($rEFileTypes, strrchr($new_name, '.'))) {
                if (JFile::copy($file['tmp_name'], $image_path . '/' . $new_name)) {
                    list($width, $height) = getimagesize($image_path . '/' . $new_name);

                    if (plotGlobalConfig::getVar('portfolioCropWidthMin') > $width  || plotGlobalConfig::getVar('portfolioCropHeightMin') > $height ) {
                        $data['status'] = 0;
                        $data['message']='Минимальный размер картинки должен быть '.plotGlobalConfig::getVar('portfolioCropWidthMin').'x'.plotGlobalConfig::getVar('portfolioCropHeightMin');
                        header('Content-Type: application/json');
                        echo json_encode($data);
                        die;

                    }else{
                        $data['status'] = 1;
                        $event = new stdClass();
                        $event->user_id = Foundry::user()->id;
                        $event->img = $new_name;
                        $db->insertObject('#__plot_portfolio', $event);
                        $id = $db->insertid();
                        $data['id'] = $id;
                        $data['name'] = $new_name;
                        header('Content-Type: application/json');
                        echo json_encode($data);
                        die;

                    }

                } else {
                    header('Content-Type: application/json');
                    $data['status'] = 0;
                    $data['message']='error';
                    echo json_encode($data);
                    die;
                }
            } else {
                $data['status'] = 0;
                $data['message']=JText::_('COM_PLOT_INVALID_IMG_FORMAT');
                header('Content-Type: application/json');
                echo json_encode($data);
                die;
            }
            }else{
                $data['status'] = 0;
                $data['message']=JText::_('COM_PLOT_INVALID_FILE SIZE');
                header('Content-Type: application/json');
                echo json_encode($data);
                die;
            }


        } else {
            $data['status'] = 0;
            $data['message']=JText::_('COM_PLOT_INVALID_IMG');
            header('Content-Type: application/json');
            echo json_encode($data);
            die;
        }
    }

    public function ajaxPortfolioImage()
    {
        $profileEditView = $this->getView('portfolio', 'html');
        $profileEditView->setLayout('ajax_portfolio_image');
        $originalPhotoUrl = JUri::root() . 'images/com_plot/portfolio/' . Foundry::user()->id . '/original/' . JRequest::getString('img_name');
        $profileEditView->set('originalPhotoUrl', $originalPhotoUrl);
        $profileEditView->set('photoUrl', JRequest::getString('img_name'));
        list($width, $height) = getimagesize($originalPhotoUrl);
        if($width<plotGlobalConfig::getVar('portfolioCropWidthMin') || $height<plotGlobalConfig::getVar('portfolioCropHeightMin')){
            @unlink(JPATH_BASE .'/images/com_plot/portfolio/' . Foundry::user()->id . '/original/' . JRequest::getString('img_name'));
            $db = JFactory::getDbo();
            $query = $db->getQuery(true)
                ->delete('`#__plot_portfolio`')
                ->where('`img` = "' .JRequest::getString('img_name').'"')
                ->where('`user_id` = '.(int)Foundry::user()->id);
            $db->setQuery($query);
            try {
                $db->execute();
            } catch (RuntimeException $e) {
                $profileEditView->display();
                die;
            }

        }
        $profileEditView->display();
        die;
    }

    public function ajaxSaveCroppedImg()
    {
        Foundry::requireLogin();
        require_once JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/image/image.php';
        $formData = array();
        parse_str(JRequest::getVar('data'), $formData);
        $imgWidth = $formData['img-width'];
        $imgHeight = $formData['img-height'];
        $width = $formData['w'] / $imgWidth;
        $height = $formData['h'] / $imgHeight;
        $left = $formData['x'] / $imgWidth;
        $top = $formData['y'] / $imgHeight;
        if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'thumb')) {
            mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'thumb');
        }
        $image = new SocialImage();
        $image = $image->load(JPATH_BASE . '/images/com_plot/portfolio/' . Foundry::user()->id . '/original/' . $formData['img_src']);

        $imageWidth = $image->getWidth();
        $imageHeight = $image->getHeight();
        if (!is_null($top) && !is_null($left) && !is_null($width) && !is_null($height)) {
            $actualX = $imageWidth * $left;
            $actualY = $imageHeight * $top;
            $actualWidth = $imageWidth * $width;
            $actualHeight = $imageHeight * $height;

            // Now we'll need to crop the image
            $image->crop($actualX, $actualY, $actualWidth, $actualHeight);
        } else {
            if ($imageWidth > $imageHeight) {
                $x = ($imageWidth - $imageHeight) / 2;
                $y = 0;
                $image->crop($x, $y, $imageHeight, $imageHeight);
            } else {
                $x = 0;
                $y = ($imageHeight - $imageWidth) / 2;
                $image->crop($x, $y, $imageWidth, $imageWidth);
            }
        }

        $tmpImagePath = JPATH_BASE . '/images/com_plot/portfolio/' . Foundry::user()->id . '/thumb/' . $formData['img_src'];
        $image->save($tmpImagePath);

        // Unset the image to free up some memory
        unset($image);

        // Reload the image again to get the correct resource pointing to the cropped image.
        $image = Foundry::image();
        $image->load($tmpImagePath);
        $data = array();
        $data['img'] = JUri::root() . 'images/com_plot/portfolio/' . Foundry::user()->id . '/thumb/' . $formData['img_src'];
        echo json_encode($data);
        die;
    }

    public function updatePortfolio(){
        $db = Foundry::db();
        $app = JFactory::getApplication();
        $jinput = JFactory::getApplication()->input;
        $portfolio = new stdClass();
        $id=$jinput->get('id',0,'INT');
        if($id){
            $portfolio->title = $jinput->get('title','','STRING');
            $portfolio->description =$jinput->get('description','','STRING');
            $portfolio->user_id = plotUser::factory();
            $portfolio->date = date('Y-m-d', strtotime($jinput->get('date',0,'STRING')));
            $portfolio->id = $id;
            $db->updateObject('#__plot_portfolio', $portfolio, 'id');
            $this->setMessage('Портфолио добавлено');
            plotPoints::assign('add.portfolio', 'com_plot', plotUser::factory()->id, $id);
            $this->setRedirect(JRoute::_('index.php?option=com_plot&view=profile', false));
            return;
        }else{
                $this->setMessage('Error');
                $this->setRedirect(JRoute::_('index.php?option=com_plot&view=profile', false));
                return;
        }

    }

    public function portfolioDelete(){
        $id=JRequest::getInt('portfolio_id');
        $photo = PlotHelper::getPortfolioById($id);
        $data=array();
        @unlink($data['img'] = JUri::root() . 'images/com_plot/portfolio/' . Foundry::user()->id . '/thumb/' . $photo->img);
        @unlink(JPATH_BASE . '/images/com_plot/events/' . Foundry::user()->id . '/original/' . $photo->img);
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->delete('`#__plot_events`')
            ->where('`id` = ' . (int)$id);
        $db->setQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
            echo json_encode($data);
            exit();
        }
        $data['status'] = 1;
        echo json_encode($data);
        exit();
    }



}