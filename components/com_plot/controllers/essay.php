<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');


class PlotControllerEssay extends PlotController
{
    public function save()
    {
        $model = $this->getModel('essay');

        $jinput = JFactory::getApplication()->input;
        $data = $jinput->get('data', array(), 'ARRAY');
        $text = $data['text'];
        $bookId = $data["bookId"];
        $price=plotUser::factory()->getFinishedPriceForMyBook((int)$bookId);
        if ($text) {

           $img= $model->save($text, $bookId);
            if(!empty($img)){
                $data['img']=JUri::root().'media/com_plot/essay/'.$img['essay'].'/'.$img['img'];
                $data['essayId']=(int)$img['essay'];
            }else{
                $data['img']=false;
            }
            $data['status'] = 1;
            $data['message'] = JText::sprintf('COM_PLOT_ESSAY_SAVED_AND_SEND',$price);
           // header('Content-Type: application/json');
            echo json_encode($data);
            die;
        } else {
            $data['status'] = 0;
            $data['message'] = 'Заполните поле';
            //header('Content-Type: application/json');
            echo json_encode($data);
            die;
        }

    }

    public function ajaxEssayLoadMore()
    {

        $userData = JRequest::getVar('userData');

        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');
        }
        $screenHeight=(int)$userData['screenHeight'];
        $screenWidth=(int)$userData['screenWidth'];
        $bookId = (int)$userData['bookId'];

        $essayModel = $this->getModel('essay');
        $view = $this->getView('publication', 'raw');

        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');

            $limitCount = plotGlobalConfig::getVar('childEssayCount');
        } else {
            $limitCount = plotGlobalConfig::getVar('parentEssayCount');
        }

        $offset = JRequest::getInt('offset');
        $limit = array('offset' => $offset, 'limit' => $limitCount);

        $essayData = $essayModel->getEssay($limit, $bookId);
        foreach($essayData['items'] AS $item){
            $item->screenHeight=($screenHeight-60)?($screenHeight-60):744;
            if($screenWidth<1024){
                $item->screenWidth=($screenWidth-24)?($screenWidth-24):744;
            } else{
                $item->screenWidth=1000;
            }
        }

        $view->essay = $essayData;

        $view->setLayout('essay.list');

        $data = array();
        $data['renderedBooks'] = $view->ajaxRenderList();
        if(!(int)$essayData['countItems']){
            $data['renderedBooks'] .= "<script>noEssayFind();</script>";
        }

        echo $data['renderedBooks'];
        die;
    }

    public function replaceText()
    {
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');

        $essayModel = $this->getModel('essay');
        $essay = $essayModel->replaceForConversation($ids);

        echo json_encode($essay);
        exit();
    }

    public function ajaxEssay()
    {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        $user = plotUser::factory();
        if ($user->isParent()) {
            $this->addViewPath(JPATH_COMPONENT . '/views_parent');
        }
        $essayModel = $this->getModel('essay');
        $essay = $essayModel->getEssayById($id);
        if (!$essay) {
            $essay = new stdClass();
        }

        $essayView = $this->getView('essay', 'raw');
        $essayView->setLayout('essay');
        $essayView->set('essay', $essay);
        $essayView->display();
        die;
    }

    public function approve()
    {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        $msg = $jinput->get('msg', JText::_("COM_PLOT_ESSAY_WAS_APPROVE"), 'STRING');
        $essayModel = $this->getModel('essay');
        $user = plotUser::factory();
        if ($user->isParent()) {
            $this->addViewPath(JPATH_COMPONENT . '/views_parent');
        }
        $essay = $essayModel->getEssayById($id);
        $my = plotUser::factory($essay->user_id);
        $conversationModel = JModelLegacy::getInstance('conversations', 'PlotModel');
        $bookObj = new plotBook($essay->book_id);
        $mailer = JFactory::getMailer();
        $config = JFactory::getConfig();
        $sender = array(
            $config->get('mailfrom'),
            $config->get('fromname')
        );
        //проверить начисление денег
        if (!$this->isAlreadyPaidForBookReading($id)) {
            $countPoints = $this->getCountPointsForBookReading($essay->book_id);
            $countMoney = $this->getCountMoneyForBookReading($essay->book_id, $my->id);
            $this->updateBookStatus($essay->book_id, $my->id);

            $my->addMoney($countMoney, $my->id);
            Foundry::points()->assignCustom($my->id, $countPoints, 'Книга прочитана');
            $this->updatePointsHistory($essay->book_id);
            $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $essay->user_id) . '">' . plotUser::factory($essay->user_id)->name . '</a>';
            $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $essay->book_id) . '">' . $bookObj->c_title . '</a>';
            $reviewer = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $essay->reviewer) . '">' . plotUser::factory($essay->reviewer)->name . '</a>';
            if(plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                $conversationModel->sendMessageSystem($essay->user_id, 'plot-money-book-get-' . $essay->id);
            }
            if(plotUser::factory($essay->reviewer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($essay->reviewer)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                $conversationModel->sendMessageSystem($essay->reviewer, 'plot-money-book-get-' . $essay->id);
            }


            $mailer->setSender($sender);
            if(plotUser::factory($essay->reviewer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($essay->reviewer)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                $mailer->addRecipient(plotUser::factory($essay->reviewer)->email);
            }
            if(plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                $mailer->addRecipient(plotUser::factory($essay->user_id)->email);
            }
            $mailer->setSubject(JText::_('COM_PLOT_SEND_MONEY_GET_SUBJECT'));
            $mailer->isHTML(true);
            $mailer->setBody(JText::sprintf("COM_PLOT_SEND_MONEY_BOOK_GET", $actor, $target));
            if(plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==NULL || plotUser::factory($essay->reviewer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($essay->reviewer)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                $mailer->Send();
            }
        }

        $essayModel->essayUpdateStatus($id);
        $essayModel->updateEssaySocialImgStatus($id);
        $programs = $bookObj->getBookProgramsIds();

        if(plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
            $conversationModel->sendMessageSystem($essay->user_id, 'plot-essay-approved-' . $essay->id);
            $mailer->setSender($sender);
            $mailer->addRecipient(plotUser::factory($essay->user_id)->email);
            $mailer->setSubject(JText::_('COM_PLOT_SEND_ESSAY_SUBJECT'));
            $mailer->isHTML(true);
            $mailer->setBody(JText::sprintf("COM_PLOT_SEND_ESSAY_APPROVED", $reviewer, $target));
            $mailer->Send();
        }
        if ($programs) {

            foreach ($programs AS $prog) {
                $program = new plotProgram($prog);
                if ($program->isProgramFinished($essay->user_id)) {
                    if(plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                        $conversationModel->sendMessageSystem($essay->user_id, 'plot-program-finished-' . $prog . '-' . $essay->user_id);
                    }
                    $mailer = JFactory::getMailer();
                    $config = JFactory::getConfig();
                    $sender = array(
                        $config->get('mailfrom'),
                        $config->get('fromname')
                    );
                    $group = FD::group($prog);
                    $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' .  $essay->user_id) . '">' . plotUser::factory( $essay->user_id)->name . '</a>';
                    $target = '<a href="' . FRoute::groups(array('layout' => 'item', 'id' => $group->getAlias()), false) . '?plot=1">' . $program->title . '</a>';
                    if(plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                        $mailer->setSender($sender);
                        $mailer->addRecipient(plotUser::factory($essay->user_id)->email);
                        $mailer->setSubject(JText::_('COM_PLOT_PROGRAM_FINISHED'));
                        $mailer->isHTML(true);
                        $mailer->setBody(JText::sprintf("COM_PLOT_MSG_PROGRAM_FINISHED", $actor, $target));
                        $mailer->Send();
                    }
                }
            }
        }
        $arr=array();
        $arr['message']=JText::_('COM_PLOT_ESSAY_WAS_APPROVE_REVIEWER');

        header('Content-Type: application/json');
        echo json_encode($arr);
        die;
    }

    public function reject()
    {
        $user = plotUser::factory();
        if ($user->isParent()) {
            $this->addViewPath(JPATH_COMPONENT . '/views_parent');
        }
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        $msg = $jinput->get('msg', JText::_("COM_PLOT_ESSAY_WAS_REJECT"), 'STRING');

        $essayModel = $this->getModel('essay');
        $conversationsModel = JModelLegacy::getInstance('conversations', 'PlotModel');
        $essay = $essayModel->getEssayById($id);
        $essayModel->essayUpdateReason($id,$msg);
        $bookObj = new plotBook($essay->book_id);
        $m = JText::sprintf('COM_PLOT_MSG_ESSAY_WAS_REJECT', plotUser::factory($essay->reviewer)->name, $msg);
        if( plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==1 ||  plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
            $message = $conversationsModel->sendMessageSystem($essay->user_id, 'plot-essay-reject-' . $id);
            $mailer = JFactory::getMailer();
            $config = JFactory::getConfig();
            $sender = array(
                $config->get('mailfrom'),
                $config->get('fromname')
            );

            $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $essay->reviewer) . '">' . plotUser::factory($essay->reviewer)->name . '</a>';
            $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $essay->book_id) . '">' . $bookObj->c_title . '</a>';
            $mailer->setSender($sender);
            $mailer->addRecipient(plotUser::factory($essay->user_id)->email);
            $mailer->setSubject(JText::_('COM_PLOT_ESSAY_REJECT'));
            $mailer->isHTML(true);
            $mailer->setBody(JText::sprintf('COM_PLOT_MSG_ESSAY_WAS_REJECT', $actor, $msg));
            $mailer->Send();
        }
        if(!isset($message)){
            $message=$m;
        }
        $arr=array();
        $arr['message']=JText::_('COM_PLOT_ESSAY_WAS_REJECT_REVIEWER');
        header('Content-Type: application/json');
        echo json_encode($message);
        die;
    }


    private function isAlreadyPaidForBookReading($essayId)
    {
        $finishedBooks = plotUser::factory()->getFinishedEssay($essayId);

        if ($finishedBooks) {
            return true;
        }
        return false;
    }

    private function getCountPointsForBookReading($essayId)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true);
        $query->select('p.count_points');
        $query->from('`#__plot_essay` AS a');
        $query->innerJoin('`#__plot_count_points` AS p ON a.book_id=p.entity_id AND p.entity="book"');
        $query->where('a.`id` = ' . (int)$essayId);

        $db->setQuery($query);
        $count_points = (int)$db->loadResult();
        return $count_points;
    }

    private function getCountMoneyForBookReading($bookId, $userid)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true);
        $query->select('finished_price');
        $query->from('`#__plot_books_paid` ');
        $query->where('`child_id` = ' . (int)$userid);
        $query->where('`book_id`=' . (int)$bookId);
        $db->setQuery($query);

        $count_money = (int)$db->loadResult();
        return $count_money;
    }

    private function updatePointsHistory($bookId)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true);
        $query->select('id');
        $query->from('`#__social_points_history` ');
        $query->where('`points_id` = 0');
        $query->where('`user_id`=' . (int)plotUser::factory()->id);
        $query->where('message="Книга прочитана"');
        $query->order('created DESC');
        $db->setQuery($query);
        $pointsHistory = (int)$db->loadResult();
        if ($pointsHistory) {
            $social_history = (object)array(
                'id' => (int)$pointsHistory,
                'points_id' => 37
            );
            $db->updateObject('#__social_points_history', $social_history, 'id');
            $plotPoints = new stdClass();
            $plotPoints->sph_id = $pointsHistory;
            $plotPoints->entity_id = (int)$bookId;
            $db->insertObject('#__plot_points_history', $plotPoints);
        }
    }


    public function updateBookStatus($bookId, $userId){
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->update('`#__plot_books_paid`')
            ->set("`finished` = 1")
            ->set("`finished_date` = NOW()")
            ->where('`child_id` ='.(int)$userId)
            ->where('`book_id` ='.(int)$bookId);

        $db->setQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }



    }

    public function bookMoneyReplaceText()
    {
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');
        $essayModel = $this->getModel('Essay');
        $essay = $essayModel->booksReplaceText($ids, "COM_PLOT_MSG_USER_GET_BOOK_MONEY");

        echo json_encode($essay);
        exit();
    }

    public function essayApprovedReplaceText()
    {
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');

        $essayModel = $this->getModel('Essay');
        $essay = $essayModel->essayApprovedReplaceText($ids );

        echo json_encode($essay);
        exit();
    }

    public function essayRejectedReplaceText()
    {
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');

        $essayModel = $this->getModel('Essay');
        $essay = $essayModel->essayRejectedReplaceText($ids );

        echo json_encode($essay);
        exit();
    }

}
