<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');
require_once(JPATH_ROOT.'/administrator/components/com_easysocial/includes/foundry.php');

class PlotControllerEvents extends JControllerLegacy
{

    public function rejectEvent()
    {
        $jinput = JFactory::getApplication()->input;
        $event_id = $jinput->get('event_id', 0, 'INT');
        $model = $this->getModel('Events');
        plotPoints::assign('unsubscribe.meeting', 'com_plot', plotUser::factory()->id, $event_id);
        echo $model->rejectEvent($event_id);
        exit();
    }

    public function sudscribeEvent()
    {
        $jinput = JFactory::getApplication()->input;
        $event_id = $jinput->get('event_id', 0, 'INT');
        $model = $this->getModel('Events');
        plotPoints::assign('subscribe.meeting', 'com_plot', plotUser::factory()->id, $event_id);
        echo $model->sudscribeEvent($event_id);
        exit();
    }

    public function deleteEvent()
    {
        $jinput = JFactory::getApplication()->input;
        $event_id = $jinput->get('eventId', 0, 'INT');
        $model = $this->getModel('Events');
        $data = array();
        if ($model->deleteEvent($event_id)) {
            $data['status'] = 1;
            $data['msg'] = 'Событие удалено успешно';
        } else {
            $data['status'] = 0;
            $data['msg'] = 'Событие не удалено';
        }

        echo json_encode($data);
        exit();
    }

    public function searchByDate()
    {
        $data = array();
        $jinput = JFactory::getApplication()->input;
        $search = $jinput->get('search', '', 'STRING');
        $user_id = $jinput->get('user_id', 0, 'INT');
        $model = $this->getModel('Events');
        $events = $model->searchByDate($search, $user_id);
        $data['events'] = $events;
        echo json_encode($data);
        exit();
    }

    public function ajaxGetFilteredAndSortedCourses()
    {
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');
        }

        $eventsModel = $this->getModel('events');
        $view = $this->getView('events', 'raw');
        $my = plotUser::factory();

        $agesIds = ($ageId = JRequest::getInt('ageId')) ? array($ageId) : '';
        $tagsIds = JRequest::getVar('tagsIds', array(0));
        $isOnlyMyCourses = JRequest::getVar('myCoursesOnly', false);

        $categoryId = JRequest::getVar('categoryId', 'STRING');

        $order = JRequest::getVar('sort', '`e`.`create_date` DESC');
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');
            $limitCount = plotGlobalConfig::getVar('meetingsParentPageShowFirstCount');
        } else {
            $limitCount = plotGlobalConfig::getVar('meetingsChildPageShowFirstCount');
        }
        $limit = array('offset' => 0, 'limit' => JRequest::getVar('limit', $limitCount));
        $eventsData = $eventsModel->getEvents($categoryId, $order, $agesIds, $tagsIds, $limit, $isOnlyMyCourses);

        $view->courses =  $eventsData['items'];
        $view->isOnlyMyCourses = $isOnlyMyCourses;

        $view->setLayout('events.list');

        $data = array();
        $data['renderedCourses'] = $view->ajaxRenderCoursesList();
        $data['renderedCourses'] .= "<script>jPlotUp.Arrow.initialize('positionCameras');</script>";

        $data['countCourses'] =  $eventsData['countItems'];
        echo json_encode($data);
        die;
    }

    public function ajaxLoadMore()
    {
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');
            $limitCount = plotGlobalConfig::getVar('meetingsParentPageShowFirstCount');
        } else {
            $limitCount = plotGlobalConfig::getVar('meetingsChildPageShowFirstCount');
        }

        $offset = JRequest::getInt('offset');
        $coursesModel = $this->getModel('events');
        $view = $this->getView('events', 'raw');

        $userData = JRequest::getVar('userData');

        $agesIds = (isset($userData['ageId']) && $userData['ageId']) ? array($userData['ageId']) : '';
        $tagsIds = isset($userData['tagsIds']) ? $userData['tagsIds'] : array(0);
        $isOnlyMyCourses = isset($userData['myCoursesOnly']) ? $userData['myCoursesOnly'] : false;

        $filter = $userData['categoryId'];

        $order = isset($userData['sort']) ? $userData['sort'] : '`total_min_cost` ASC';
        $limit = array('offset' => $offset, 'limit' => $limitCount);
        $coursesData = $coursesModel->getEvents($filter, $order, $agesIds, $tagsIds, $limit, $isOnlyMyCourses);
        $view->courses = $coursesData['items'];
        $view->isOnlyMyCourses = $isOnlyMyCourses;

        if (!$view->courses) {
            die;
        }

        $view->setLayout('events.list');
        echo $view->ajaxRenderCoursesList();
       // echo "<script>jPlotUp.Arrow.initialize('positionCameras');</script>";
        die;
    }

    public function ajaxGetFilteredCurrentEvents()
    {
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');
        }

        $coursesModel = $this->getModel('events');
        $view = $this->getView('events', 'raw');
        $my = plotUser::factory();

        $order = JRequest::getVar('sort', 'now');

        $coursesData = $coursesModel->getCurrentEvents($order);
        
        $view->courses = $coursesData['items'];
        $view->setLayout('current.events.list');

        $data = array();
        $data['renderedEvents'] = $view->ajaxRenderCoursesList();
        $data['countEvents'] = count($view->courses);

        echo json_encode($data);
        die;
    }

}
