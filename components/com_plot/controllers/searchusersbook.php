<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class PlotControllerSearchUsersBook extends PlotController
{

    public function showPopup()
    {
        $view = $this->getView('SearchUsersBook', 'raw');
        $view->display();
        die;
    }

    public function ajaxGetUsersList()
    {
        $model = JModelLegacy::getInstance('searchUsersBook', 'plotModel');
        $publicationModel=JModelLegacy::getInstance('Publication', 'plotModel');
        $view = $this->getView('SearchUsersBook', 'raw');
        $view->setLayout('users.list');
        
        $view->bookId = JRequest::getInt('id', '0');

        $filter = array();
        $searchValue = JRequest::getVar('searchValue', '');
        $page = JRequest::getVar('page', '0');
        
        if ($searchValue) {
            $filter[] = array('field' => 'u.name', 'type' => 'like', 'value' => $searchValue);
        }
        if($view->bookId){
            $filter[] = array('field' => 'u.id', 'type' => 'NOT IN', 'value' => $publicationModel->getBookAuthorId(JRequest::getInt('id', '0')));
        }
        $view->users = $model->getUsers( $filter, 'u.name ASC', $limit = array('limitstart' => ($page - 1) * plotGlobalConfig::getVar('usersCountSearchForBuyBook'), 'limit' => plotGlobalConfig::getVar('usersCountSearchForBuyBook')) );
        $view->totalUser = $model->totalUsers;
        
        $view->pagination = new JPagination( $view->totalUser, ($page - 1) * plotGlobalConfig::getVar('usersCountSearchForBuyBook'), plotGlobalConfig::getVar('usersCountSearchForBuyBook') );
        
        $result = $view->loadTemplate();
        if ($result instanceof Exception) {
            return $result;
        }
        
        $data['html'] = $result;

        echo json_encode($data);
        die;
    }

}
