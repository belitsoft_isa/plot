<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class PlotControllerContacts extends PlotController
{

    public function sendMessage()
    {
        $name = JRequest::getString('name');
        $email = JRequest::getVar('email');
        $phone = JRequest::getVar('phone');
        $city = JRequest::getVar('city');
        $message = JRequest::getVar('message');
        
        $sitename = JFactory::getConfig()->get('sitename');

        $body = "Сообщение от: $name \nE-mail: $email \nТелефон: $phone \nГород: $city \nТекст сообщения: \n$message";

        $messageSent = PlotHelper::sendEmailToAdmin(array($email, $name), "$sitename - сообщение пользователя", $body);
        
        if (!$messageSent) {
            $this->redirectWithMessage('index.php?option=com_plot&view=profile', 'COM_PLOT_SEND_MESSAGE_ERROR');
        }
        $this->redirectWithMessage('index.php?option=com_plot&view=profile', 'COM_PLOT_MESSAGE_SENT');
    }

}
