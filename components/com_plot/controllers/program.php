<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');


class PlotControllerProgram extends PlotController
{
    public function getVideo(){

        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        $program=new plotProgram($id);
        $data=array();

        if($program){
            if (strripos($program->link, 'youtube.com/') !== FALSE) {
                $youtubeNumber = substr($program->link, strripos($program->link, '?v=') + 3);
                if (($pos = strpos($youtubeNumber, '&')) !== FALSE) {
                    $youtubeNumber = substr($youtubeNumber, 0, $pos);
                }
            }
            $program->youtubeNumber=$youtubeNumber;

            $data['item']=$program;
        }
       header('Content-Type: application/json');
        echo json_encode($data);
        exit();

    }

    public function getCourses(){

        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        $model=$this->getModel('Program');
        $items=$model->getCourses($id);
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');
        }
        $view = $this->getView('programs', 'raw');
        $view->setLayout('courses.list');
        $view->programs = $items['items'];
        $view->countItems= $items['countItems'];
        $data = array();
        $data['renderedPrograms'] = $view->ajaxRenderProgramsList();

        echo $data['renderedPrograms'];
        exit();
    }

    public function getBooks(){
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        $model=$this->getModel('Program');
        $items=$model->getBooks($id);
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');
        }
        $view = $this->getView('programs', 'raw');
        $view->setLayout('books.list');
        $view->programs = $items['items'];
        $view->countItems= $items['countItems'];
        $data = array();
        $data['renderedPrograms'] = $view->ajaxRenderProgramsList();

        echo $data['renderedPrograms'];
        exit();
    }

    public function getPrograms(){
        $jinput = JFactory::getApplication()->input;
        $userData=$jinput->get('userData', array(), 'ARRAY');
        //$id=isset($userData['id'])?(int)$userData['id']:0;
        $id = $jinput->get('id', 0, 'INT');
        $offset=JRequest::getInt('offset', 0);
        if($offset==plotGlobalConfig::getVar('programsLimit')){
            $offset=0;
        }else{
            $offset=$offset-(int)plotGlobalConfig::getVar('programsLimit');
        }
        $limit = array('offset' => $offset, 'limit' => JRequest::getInt('number', 0));
        $model=$this->getModel('Program');
        $items=$model->getPrograms($id);
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');
        }
        $view = $this->getView('programs', 'raw');
        $view->setLayout('programs.list');
        $view->programs = $items['items'];
        $view->countItems= $items['countItems'];
        $data = array();
        $data['renderedPrograms'] = $view->ajaxRenderProgramsList();

        echo $data['renderedPrograms'];
        die;
    }

    public function getAllPrograms(){
        $jinput = JFactory::getApplication()->input;
        $model=$this->getModel('Program');
        $offset=JRequest::getInt('offset', 0);

        if($offset==plotGlobalConfig::getVar('programsLimit')){
            $offset=0;
        }else{
            $offset=$offset-(int)plotGlobalConfig::getVar('programsLimit');
        }
        $limit = array('offset' => $offset, 'limit' => JRequest::getInt('number', 0));

        $items=$model->getAllPrograms($limit);
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');
        }
        $view = $this->getView('programs', 'raw');
        $view->setLayout('allprograms.list');
        $view->programs = $items['items'];
        $view->countItems= $items['countItems'];
        $data = array();
        $data['renderedPrograms'] = $view->ajaxRenderProgramsList();

        echo $data['renderedPrograms'];
        die;
        /*header('Content-Type: application/json');
        echo json_encode($items);
        exit();*/
    }

    public function getFeaturedPrograms(){
        $jinput = JFactory::getApplication()->input;
        $model=$this->getModel('Program');
        $offset=JRequest::getInt('offset', 0);

        if($offset==plotGlobalConfig::getVar('programsLimit')){
            $offset=0;
        }else{
            $offset=$offset-(int)plotGlobalConfig::getVar('programsLimit');
        }
        $limit = array('offset' => $offset, 'limit' => JRequest::getInt('number', 0));

        $items=$model->getFeaturedPrograms($limit);
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');
        }
        $view = $this->getView('programs', 'raw');
        $view->setLayout('allprograms.list');
        $view->programs = $items['items'];
        $view->countItems= $items['countItems'];
        $data = array();
        $data['renderedPrograms'] = $view->ajaxRenderProgramsList();

        echo $data['renderedPrograms'];
        die;
    }

    public function getMyPrograms(){
        $jinput = JFactory::getApplication()->input;
        $model=$this->getModel('Program');
        $offset=JRequest::getInt('offset', 0);
        if($offset==plotGlobalConfig::getVar('programsLimit')){
            $offset=0;
        }else{
            $offset=$offset-(int)plotGlobalConfig::getVar('programsLimit');
        }
        $limit = array('offset' => $offset, 'limit' => JRequest::getInt('number', 0));
        $items=$model->getMyPrograms($limit);
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');
        }
        $view = $this->getView('programs', 'raw');
        $view->setLayout('allprograms.list');
        $view->programs = $items['items'];
        $view->countItems= $items['countItems'];
        $data = array();
        $data['renderedPrograms'] = $view->ajaxRenderProgramsList();

        echo $data['renderedPrograms'];
        die;

    }

    public function getGroupsByCategory(){
        $jinput = JFactory::getApplication()->input;
        $userData=$jinput->get('userData', array(), 'ARRAY');
        $id=isset($userData['id'])?(int)$userData['id']:0;
        $offset=JRequest::getInt('offset', 0);
        if($offset==plotGlobalConfig::getVar('programsLimit')){
            $offset=0;
        }else{
            $offset=$offset-(int)plotGlobalConfig::getVar('programsLimit');
        }
        $limit = array('offset' => $offset, 'limit' => JRequest::getInt('number', 0));
        $model=$this->getModel('Program');
        $items=$model->getGroupsByCategory($id, $limit);
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');
        }
        $view = $this->getView('programs', 'raw');
        $view->setLayout('allprograms.list');
        $view->programs = $items['items'];
        $view->countItems= $items['countItems'];
        $data = array();
        $data['renderedPrograms'] = $view->ajaxRenderProgramsList();

        echo $data['renderedPrograms'];
        die;
    }

    public function getGroupsBylevel(){
        $jinput = JFactory::getApplication()->input;
        $userData=$jinput->get('userData', array(), 'ARRAY');
        $id=isset($userData['id'])?(int)$userData['id']:0;
        $level=isset($userData['level'])?(int)$userData['level']:0;
        $age=isset($userData['age'])?(int)$userData['age']:0;
        $entity=isset($userData['entity'])?$userData['entity']:'all-groups';
        $offset=JRequest::getInt('offset', 0);
        if($offset==plotGlobalConfig::getVar('programsLimit')){
            $offset=0;
        }else{
            $offset=$offset-(int)plotGlobalConfig::getVar('programsLimit');
        }
        $limit = array('offset' => $offset, 'limit' => JRequest::getInt('number', 0));
        $model=$this->getModel('Program');
        switch ($entity) {
            case 'all-groups':
                $items=$model->getAllPrograms($limit, $level, $age);
                break;
            case 'my-groups':
                $items=$model->getMyPrograms($limit, $level, $age);
                break;
            case 'featured-groups':
                $items=$model->getFeaturedPrograms($limit, $level, $age);
                break;
            case 'cats':
                $items=$model->getGroupsByCategory($id, $limit, $level, $age);
                break;
        }
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');
        }
        $view = $this->getView('programs', 'raw');
        $view->setLayout('allprograms.list');
        $view->programs = $items['items'];
        $view->countItems= $items['countItems'];
        $data = array();
        $data['renderedPrograms'] = $view->ajaxRenderProgramsList();

        echo $data['renderedPrograms'];
        die;
    }

    function getGroupsByAge(){
        $jinput = JFactory::getApplication()->input;
        $userData=$jinput->get('userData', array(), 'ARRAY');
        $id=isset($userData['id'])?(int)$userData['id']:0;
        $level=isset($userData['level'])?(int)$userData['level']:0;
        $age=isset($userData['age'])?(int)$userData['age']:0;
        $entity=isset($userData['entity'])?$userData['entity']:'all-groups';

        $offset=JRequest::getInt('offset', 0);
        if($offset==plotGlobalConfig::getVar('programsLimit')){
            $offset=0;
        }else{
            $offset=$offset-(int)plotGlobalConfig::getVar('programsLimit');
        }
        $limit = array('offset' => $offset, 'limit' => JRequest::getInt('number', 0));
        $model=$this->getModel('Program');

        switch ($entity) {
            case 'all-groups':

                $items=$model->getAllPrograms($limit, $level, $age);
                break;
            case 'my-groups':
                $items=$model->getMyPrograms($limit, $level, $age);
                break;
            case 'featured-groups':
                $items=$model->getFeaturedPrograms($limit, $level, $age);
                break;
            case 'cats':
                $items=$model->getGroupsByCategory($id, $limit, $level, $age);
                break;
        }
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');
        }
        //$items=$model->getGroupsByAge($id, $limit);
        $view = $this->getView('programs', 'raw');
        $view->setLayout('allprograms.list');
        $view->programs = $items['items'];
        $view->countItems= $items['countItems'];
        $data = array();
        $data['renderedPrograms'] = $view->ajaxRenderProgramsList();

        echo $data['renderedPrograms'];
        die;
    }

    public function programFinishedReplaceText()
    {
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');

        $programModel = $this->getModel('Program');
        $program = $programModel->programFinishedReplaceText($ids );

        echo json_encode($program);
        exit();
    }

    function getClearGroup(){

        $jinput = JFactory::getApplication()->input;
        $userData=$jinput->get('userData', array(), 'ARRAY');
        $id=isset($userData['id'])?(int)$userData['id']:0;
        $entity=isset($userData['entity'])?$userData['entity']:'all-groups';
        $offset=JRequest::getInt('offset', 0);

        if($offset==plotGlobalConfig::getVar('programsLimit')){
            $offset=0;
        }else{
            $offset=$offset-(int)plotGlobalConfig::getVar('programsLimit');
        }
        $limit = array('offset' => $offset, 'limit' => JRequest::getInt('number', 0));
        $model=$this->getModel('Program');
        switch ($entity) {
            case 'all-groups':
                $items=$model->getAllPrograms($limit);
                break;
            case 'my-groups':
                $items=$model->getMyPrograms($limit);
                break;
            case 'featured-groups':
                $items=$model->getFeaturedPrograms($limit);
                break;
            case 'cats':
                $items=$model->getGroupsByCategory($id, $limit);
                break;
        }
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');
        }
        $view = $this->getView('programs', 'raw');
        $view->setLayout('allprograms.list');
        $view->programs = $items['items'];
        $view->countItems= $items['countItems'];
        $data = array();
        $data['renderedPrograms'] = $view->ajaxRenderProgramsList();

        echo $data['renderedPrograms'];
        die;
    }

}
