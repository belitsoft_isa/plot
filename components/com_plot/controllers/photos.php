<?php

defined('_JEXEC') or die('Restricted access');

class PlotControllerPhotos extends JControllerLegacy
{

    public function ajaxPhotosLoadMore()
    {
        $userData = JRequest::getVar('userData');
        $user = plotUser::factory((int)$userData['id']);

        $limit = array('offset' => JRequest::getInt('offset', 0), 'limit' => JRequest::getInt('number', 0));
        $model = $this->getModel('Photos');

        $photos = $model->getPhotos($limit, $user->id)['items'];
        if (plotUser::factory((int)$userData['id'])->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');
        }
        $view = $this->getView('photos', 'raw');
        $view->photos = $photos;

        $view->id=(int)$userData['id'];
        $view->setLayout('photos.list');

        $data = array();
        $data['renderedPhotos'] = $view->ajaxRenderList();
        $data['renderedPhotos'] .= "<script>jPlotUp.Arrow.initialize('positionCameras');</script>";
        echo $data['renderedPhotos'];
        die;

    }



    public function ajaxCertificatesLoadMore()
    {
        $userData = JRequest::getVar('userData');
        $user = plotUser::factory((int)$userData['id']);

        $limit = array('offset' => JRequest::getInt('offset', 0), 'limit' => JRequest::getInt('number', 0));
        $model = $this->getModel('Photos');
        $certificates = $model->getCertificates($limit, $user->id)['items'];
        if (plotUser::factory((int)$userData['id'])->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');
        }
        $view = $this->getView('photos', 'raw');
        $view->certificates = $certificates;

        $view->id=(int)$userData['id'];
        $view->setLayout('certifications.list');

        $data = array();
        $data['renderedCertificates'] = $view->ajaxRenderList();
        $data['renderedCertificates'] .= "<script>jPlotUp.Arrow.initialize('positionCameras');</script>";
        echo $data['renderedCertificates'];
        die;

    }


    public function ajaxVideosLoadMore()
    {
        $userData = JRequest::getVar('userData');
        $user = plotUser::factory((int)$userData['id']);
        $limit = array('offset' => JRequest::getInt('offset', 0), 'limit' => JRequest::getInt('number', 0));
        $model = $this->getModel('Photos');
        $videos = $model->getVideos($limit, $user->id)['items'];


       if (plotUser::factory((int)$userData['id'])->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');
        }
        $view = $this->getView('photos', 'raw');
        $view->videos = $videos;

        $view->id=(int)$userData['id'];
        $view->setLayout('videos.list');

        $data = array();
        $data['renderedVideos'] = $view->ajaxRenderList();
        $data['renderedVideos'] .= "<script>jPlotUp.Arrow.initialize('positionCameras');</script>";
        echo $data['renderedVideos'];
        die;


    }


    public function removePhoto()
    {
        $photoId = JRequest::getInt('photoId');
        $model = $this->getModel('Photos');
        $uid = plotHelper::getImageById($photoId)->uid;
        if ($model->deletePhoto($photoId, 'add.photo')) {
            $response['status'] = 1;
            $response['msg'] = 'Фото успешно удалено';
            $response['photoId'] = $photoId;
        } else {
            $response['status'] = 0;
            $response['msg'] = 'Фото не удалено';
        }
        echo json_encode($response);
        die;
    }

    public function deleteConfirm()
    {
        $entity = JRequest::getVar('entity');
        $id = JRequest::getInt('id');

        $view = $this->getView('photos', 'raw');
        $view->setLayout('ajax.confirm');
        $view->set('entity', $entity);
        $view->set('id', $id);
        $view->display();
        die;
    }

    public function removeVideo()
    {
        $videoId = JRequest::getInt('videoId');
        $model = $this->getModel('Photos');
        $uid = plotHelper::getVideoById($videoId)->uid;
        if ($model->deleteVideo($videoId)) {
            $response['status'] = 1;
            $response['msg'] = 'Видео успешно удалено';
            $response['photoId'] = $videoId;
        } else {
            $response['status'] = 0;
            $response['msg'] = 'Видео не удалено';
        }
        echo json_encode($response);
        die;
    }

    public function removeCertificate()
    {
        $certificateId = JRequest::getInt('certificateId');
        $model = $this->getModel('Photos');
        $uid = plotHelper::getImageById($certificateId)->uid;
        if ($model->deletePhoto($certificateId, 'add.certificate')) {
            $response['status'] = 1;
            $response['msg'] = 'Сертификат успешно удален';
            $response['photoId'] = $certificateId;
            plotPoints::assign('certificate.delete', 'com_plot', plotUser::factory()->id, $certificateId);
        } else {
            $response['status'] = 0;
            $response['msg'] = 'Сертификат не удален';
        }
        echo json_encode($response);
        die;
    }

    public function ajaxGetCourses()
    {
        $userData = JRequest::getVar('userData');
        $user = plotUser::factory(isset($userData['userId']) ? (int)$userData['userId'] : 0);

        $filter = array();
        $filter[] = array('field' => 'published', 'value' => 1);
        $userCoursesIds = $user->getCoursesIdsBoughtForMe();
        $userCoursesIds[] = 0;
        $filter[] = array('field' => 'id', 'table' => 'c', 'type' => 'IN', 'value' => $userCoursesIds);

        $tags = plotTags::getK2TagsList();
        $tagsIds = array(0);
        foreach ($tags AS $tag) {
            $tagsIds[] = $tag->id;
        }

        $order = JRequest::getVar('sort', '`total_min_cost` ASC');
        $limit = array('offset' => JRequest::getInt('offset', 0), 'limit' => JRequest::getVar('limit', plotGlobalConfig::getVar('photosPageChildCoursesCount')));
        $coursesModel = $this->getModel('courses');
        $coursesData = $coursesModel->getCourses($filter, $order, '', $tagsIds, $limit);

        # add finished date to course if course is finished
        $userFinishedCourses = $user->getFinishedCourses();
        foreach ($coursesData['items'] AS $course) {
            $course->finished_date = '';
            foreach ($userFinishedCourses AS $finishedCourse) {
                if ($course->id == $finishedCourse->course_id) {
                    $course->finished_date = JFactory::getDate($finishedCourse->finished_date)->format('d.m.Y H:i');
                }
            }
        }

        if ($user->isParent()) {
            $this->addViewPath(JPATH_ROOT . '/components/com_plot/views_parent/');
        }
        $view = $this->getView('photos', 'raw');
        $view->courses = $coursesData['items'];
        $view->setLayout('courses.list');

        if (!$view->courses) {
            die;
        }

        $data = array();
        $data['renderedCourses'] = $view->ajaxRenderList();
        $data['countCourses'] = $coursesData['countItems'];

        if (JRequest::getVar('action') == 'scrollpagination') {
            echo $data['renderedCourses'];
        } else {
            echo json_encode($data);
        }

        die;
    }

    public function ajaxGetPublications()
    {
        $userData = JRequest::getVar('userData');
        $user = plotUser::factory(isset($userData['userId']) ? (int)$userData['userId'] : 0);

        $filter = array();
        $filter[] = array('field' => 'published', 'value' => 1);

        $order = JRequest::getVar('sort', '`pub`.c_title ASC');
        $limit = array('offset' => JRequest::getInt('offset', 0), 'limit' => JRequest::getVar('limit', plotGlobalConfig::getVar('photosPageChildCoursesCount')));
        $booksModel = $this->getModel('publications');
        $booksData = $booksModel->getPublications(0, $order, '', '', $limit, '', $user->id);

        # add finished date to course if course is finished
        $userFinishedPublications = $user->getFinishedBooks();
        foreach ($booksData['items'] AS $publication) {
            $publication->finished_date = '';
            foreach ($userFinishedPublications AS $finishedPublication) {
                if ($publication->c_id == $finishedPublication->book_id) {
                    $publication->finished_date = JFactory::getDate($finishedPublication->finished_date)->format('d.m.Y H:i');
                }
            }
        }

        if ($user->isParent()) {
            $this->addViewPath(JPATH_ROOT . '/components/com_plot/views_parent/');
        }
        $view = $this->getView('photos', 'raw');
        $view->publications = $booksData['items'];
        $view->setLayout('publications.list');

        if (!$view->publications) {
            die;
        }

        $data = array();
        $data['renderedPublications'] = $view->ajaxRenderList();
        $data['countPublications'] = $booksData['countItems'];

        if (JRequest::getVar('action') == 'scrollpagination') {
            echo $data['renderedPublications'];
        } else {
            echo json_encode($data);
        }

        die;
    }

    public function ajaxGetEvents()
    {
        $userData = JRequest::getVar('userData');
        $user = plotUser::factory(isset($userData['userId']) ? (int)$userData['userId'] : 0);

        $modelEvent = JModelLegacy::getInstance('events', 'PlotModel');
        $events = $modelEvent->getUserEvents($user->id);

        if ($user->isParent()) {
            $this->addViewPath(JPATH_ROOT . '/components/com_plot/views_parent/');
        }
        $view = $this->getView('photos', 'raw');
        $view->events = $events;
        $view->setLayout('events.list');

        if (!$events) {
            die;
        }

        $data = array();
        $data['renderedEvents'] = $view->ajaxRenderList();
        $data['countEvents'] = count($events);

        if (JRequest::getVar('action') == 'scrollpagination') {
            echo $data['renderedEvents'];
        } else {
            echo json_encode($data);
        }

        die;
    }

    public function social($url, $title,$description,$img='')
    {
ob_start(); ?>

        <div class="share42init" data-zero-counter="1" data-image="<?php echo $img; ?>" data-url="<?php echo  $url;?>" data-title="<?php echo  $title;?>"  ></div>

        <?php
        return ob_get_clean();
    }

}
