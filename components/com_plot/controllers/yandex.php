<?php
defined('_JEXEC') or die('Restricted access');

class PlotControllerYandex extends PlotController
{

    public function check()
    {



        $this->invoiceId = JRequest::getVar('invoiceId','');
        $entity = JRequest::getVar('entity', '');

        if (!$this->isMD5Valid()) {
            $this->sendResponseCheck(100);
        }

        $this->markExpiredOrdersWithCheckStatusAsDenied();

        if ($entity == 'book') {
            $responseCode = $this->checkBookOrder();
        } else {
            $responseCode = $this->checkCourseOrder();
        }

        $this->logPostFromYandexToDb( json_encode(JRequest::get()), 'check', $responseCode );
        $this->sendResponseCheck($responseCode);
        die;
    }

    public function aviso()
    {
        $this->invoiceId = JRequest::getVar('invoiceId','');
        $entity = JRequest::getVar('entity', '');

        if (!$this->isMD5Valid()) {
            $this->sendResponseAviso(100);
        }

        if ($entity == 'book') {
            if (!$this->isPaymentAlreadySuccesfullyProcessed()) {
                $this->setOrderAsPaidAndSetUsersToBook();
            }
        } else {
            if (!$this->isPaymentAlreadySuccesfullyProcessed()) {
                $this->setOrderAsPaidAndSetUsersToCourse();
            }
        }

        $this->logPostFromYandexToDb( json_encode(JRequest::get()), 'aviso', 0 );
        $this->sendResponseAviso(0);
        die;
    }

    private function checkBookOrder()
    {
        if ($this->isAlreadyBookBoughtForOneOfUsers()) {
            $this->setOrderAsDenied();
            return 100;
        }
        $this->setOrderAsChecked();
        return 0;
    }

    private function checkCourseOrder()
    {
        if ($this->isAlreadyCourseBoughtForOneOfUsers()) {
            $this->setOrderAsDenied();
            return 100;
        }
        $this->setOrderAsChecked();
        return 0;
    }

    private function isMD5Valid()
    {
        $md5String = JRequest::getVar('action','').';'.JRequest::getVar('orderSumAmount','').';'.JRequest::getVar('orderSumCurrencyPaycash','').';'.JRequest::getVar('orderSumBankPaycash','').';'
            .plotGlobalConfig::getVar('yandexShopId').';'.$this->invoiceId.';'.JRequest::getVar('customerNumber','').';'.plotGlobalConfig::getVar('yandexShopPassword');
        return strtoupper( md5($md5String) ) === strtoupper( JRequest::getVar('md5') ) ? true : false;
    }

    private function isAlreadyCourseBoughtForOneOfUsers()
    {
        $db = JFactory::getDbo();
        $courseId = JRequest::getInt('courseId', 0);
        $usersIdsString = JRequest::getVar('userIds');
        $usersIds = explode('-', $usersIdsString);

        $likeStr = '`usersIds` LIKE ';
        $likeArr = array();
        foreach ($usersIds AS $userId) {
            $likeArr[] = "`usersIds` LIKE '%\"$userId\"%'";
        }
        $likeStr = '('.implode(' OR ', $likeArr).')';

        $query = "SELECT COUNT(*) FROM `#__plot_goods_orders` WHERE (`status` = 'checked' OR `status` = 'completed') AND `entity`='course' AND `entityId` = $courseId AND $likeStr";
        $result = $db->setQuery($query)->loadResult();

        return $result;
    }

    private function isAlreadyBookBoughtForOneOfUsers()
    {
        $db = JFactory::getDbo();
        $courseId = JRequest::getInt('bookId', 0);
        $usersIdsString = JRequest::getVar('userIds');
        $usersIds = explode('-', $usersIdsString);

        $likeStr = '`usersIds` LIKE ';
        $likeArr = array();
        foreach ($usersIds AS $userId) {
            $likeArr[] = "`usersIds` LIKE '%\"$userId\"%'";
        }
        $likeStr = '('.implode(' OR ', $likeArr).')';

        $query = "SELECT COUNT(*) FROM `#__plot_goods_orders` WHERE (`status` = 'checked' OR `status` = 'completed') AND `entity`='book' AND `entityId` = $courseId AND $likeStr";
        $result = $db->setQuery($query)->loadResult();

        return $result;
    }

    private function setOrderAsChecked()
    {
        $db = JFactory::getDbo();
        $updateQuery =  "UPDATE `#__plot_goods_orders` SET `status` = 'checked', `yandexInvoiceId`=".$db->quote( $this->invoiceId )." WHERE `id`= ".$db->quote(JRequest::getVar('orderNumber','0'));
        $db->setQuery($updateQuery)->query();
    }

    private function setOrderAsDenied()
    {
        $db = JFactory::getDbo();
        $updateQuery =  "UPDATE `#__plot_goods_orders` SET `status` = 'denied' WHERE `id`= ".$db->quote(JRequest::getVar('orderNumber','0'));
        $db->setQuery($updateQuery)->query();
    }

    private function logPostFromYandexToDb($text, $text2, $responseCode)
    {
        $db = JFactory::getDbo();
        $responseCode .= ' '.JFactory::getDate()->toSql();
        $query = "INSERT INTO `#__plot_payments` SET `text` = ".$db->quote($text).", `text2` = ".$db->quote($text2).", `text3` = '$responseCode'";
        $db->setQuery($query)->query();
    }

    private function sendResponseCheck($responseCode)
    {
        header("Content-type: text/xml");
        echo "<?xml version='1.0' encoding='UTF-8'?>";
        echo '<checkOrderResponse '
            .'performedDatetime="'.JFactory::getDate()->toISO8601().'" '
            .'code="'.$responseCode.'" '
            .'invoiceId="'.$this->invoiceId.'" '
            .'shopId="'.plotGlobalConfig::getVar('yandexShopId').'" '
            .'orderSumAmount="'.JRequest::getVar('orderSumAmount').'" '
            .'message="Покупка электронной копии товара" '
            .'techMessage="techMessage" '
            .'/>';
    }

    private function sendResponseAviso($responseCode)
    {
        header("Content-type: text/xml");
        echo "<?xml version='1.0' encoding='UTF-8'?>";
        echo '<paymentAvisoResponse '
            .'performedDatetime="'.JFactory::getDate()->toISO8601().'" '
            .'code="'.$responseCode.'" '
            .'invoiceId="'.$this->invoiceId.'" '
            .'shopId="'.plotGlobalConfig::getVar('yandexShopId').'" '
            .'message="Покупка электронной копии товара" '
            .'techMessage="techMessage" '
            .'/>';
    }

    private function isPaymentAlreadySuccesfullyProcessed()
    {
        $db = JFactory::getDbo();
        $result = false;
        if ($this->invoiceId) {
            $result = $db->setQuery("SELECT `id` FROM `#__plot_goods_orders` WHERE `yandexInvoiceId`= ".$db->quote($this->invoiceId)." AND `status`='completed'")->loadResult();
        }
        return $result;
    }

    public  function setOrderAsPaidAndSetUsersToCourse()
    {
        $db = JFactory::getDbo();


        $updateQuery = "UPDATE `#__plot_goods_orders` SET `status` = 'completed', `yandexInvoiceId`=".$db->quote($this->invoiceId)." WHERE `id`= ".$db->quote(JRequest::getVar('orderNumber', '0'));
        $db->setQuery($updateQuery)->query();

        $choosedUsersQuery = "SELECT `usersIds` FROM `#__plot_goods_orders` WHERE `id`= ".$db->quote(JRequest::getVar('orderNumber', '0'));
        $choosedUsersIds = json_decode($db->setQuery($choosedUsersQuery)->loadResult());
        $courseId = JRequest::getVar('courseId', 0);

        $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
        fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__." choosedUsersIds  ".$choosedUsersIds );
        fclose($fp);




        $price = JRequest::getVar('rewardAmountForEach', 0);

        $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
        fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__." include" .require_once JPATH_BASE."/components/com_plot/models/course.php" );
        fclose($fp);


        require_once JPATH_BASE.'/components/com_plot/models/course.php';
        $courseModel = JModelLegacy::getInstance('course', 'plotModel');

        $courseModel->addUsersToJoomlaLMSCourse($choosedUsersIds, $courseId);
        $courseModel->markCoursesForUsersAsPaid($choosedUsersIds, $courseId, $price, JRequest::getVar('customerNumber', ''));



        for ($i = 0; $i < count($choosedUsersIds); $i++) {
            $courseModel->addPercentOfCourseCostToAuthor($courseId, $price);
        }



    }

    public function setOrderAsPaidAndSetUsersToBook()
    {
        $db = JFactory::getDbo();


            $updateQuery = "UPDATE `#__plot_goods_orders` SET `status` = 'completed', `yandexInvoiceId`=".$db->quote($this->invoiceId)." WHERE `id`= ".$db->quote(JRequest::getVar('orderNumber', '0'));
            $db->setQuery($updateQuery)->query();

            $choosedUsersQuery = "SELECT `usersIds` FROM `#__plot_goods_orders` WHERE `id`= ".$db->quote(JRequest::getVar('orderNumber', '0'));
            $choosedUsersIds = json_decode($db->setQuery($choosedUsersQuery)->loadResult());


            $bookId = JRequest::getVar('bookId', 0);


        $price = JRequest::getVar('rewardAmountForEach', 0);

        require_once JPATH_BASE.'/components/com_plot/models/book.php';
        $bookModel = JModelLegacy::getInstance('book', 'plotModel');
        $bookModel->markBooksForUsersAsPaid($choosedUsersIds, $bookId, $price, JRequest::getVar('customerNumber', ''));
        for ($i = 0; $i < count($choosedUsersIds); $i++) {
            $bookModel->addPercentOfBookCostToAuthor( $bookId, $price);
        }

    }

    private function markExpiredOrdersWithCheckStatusAsDenied()
    {
        $db = JFactory::getDbo();
        $now = JFactory::getDate();

        $checkedOrders = $db->setQuery("SELECT `id`, `created` FROM `#__plot_goods_orders` WHERE (`status` = 'checked')")->loadObjectList();

        foreach ($checkedOrders AS $checkOrder) {
            $orderCreated = JFactory::getDate($checkOrder->created);
            $diff = $now->diff($orderCreated, true);
            if ($diff->d >= plotGlobalConfig::getVar('yandexDaysToMarkOrderAsExpired')) {
                $checkOrder->status = 'denied';
                $db->updateObject('#__plot_goods_orders', $checkOrder, 'id');
            }
        }
        return true;
    }

}
