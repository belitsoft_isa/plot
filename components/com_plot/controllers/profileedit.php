<?php

defined('_JEXEC') or die('Restricted access');
require_once(JPATH_ROOT.'/administrator/components/com_easysocial/includes/foundry.php');

class PlotControllerProfileEdit extends PlotController
{

    public function ajaxGetEditProfilePopupHtml()
    {
        $user=plotUser::factory();
        if($user->isParent()){
            $this->addViewPath(JPATH_COMPONENT.'/views_parent');
        }
        $profileEditView = $this->getView('profileedit', 'raw');
        $profileEditView->setLayout('profile_edit_popup');
        $profileEditView->display();
        die;
    }

    public function ajaxGetForeignChildProfilePopupHtml()
    {
        $id = JRequest::getInt('id');
        if ($id) {
            $user = plotUser::factory($id);
        } else {
            $user = plotUser::factory();
        }
        
        if ($user->isParent()) {
            $this->addViewPath(JPATH_COMPONENT.'/views_parent');
        } else {
            $this->setPath("view", JPATH_COMPONENT.'/views');
        }

        $profileEditView = $this->getView('profileedit', 'raw');
        $profileEditView->setLayout('profile.foreign');
        $profileEditView->displayForeign();
        die;
    }
    
    public function ajaxToggleTag()
    {
        $my = plotUser::factory();

        $tagId = JRequest::getInt('tagId', 0);
        $checked = JRequest::getVar('checked') == 'true' ? true : false;

        if ($checked) {
            if (count($my->getTags()) < plotGlobalConfig::getVar('maxTagsForUser')) {
                plotTags::add($tagId, 'user', $my->id);
            } else {
                die('cant add more tags for this user: check limit');
            }
        } else {
            plotTags::remove($tagId, 'user', $my->id);
        }
        die;
    }

    # <editor-fold defaultstate="collapsed" desc="PROFILE FIELDS CHANGE">
    public function ajaxChangeLastName()
    {
        $my = plotUser::factory();

        $myName = $my->getSocialFieldData('JOOMLA_FULLNAME');
        if(!$myName){
            $myName=new stdClass();
        }
        $myName->last = mb_substr(JRequest::getVar('lastName'), 0, plotGlobalConfig::getVar('maxLenghtSurname'),"UTF-8" );
        
        if ($myName->last && $my->id && $my->saveName($myName)) {
            die($myName->last);
        }
        die(JText::_('COM_PLOT_ERROR'));
    }

    public function ajaxChangeFirstName()
    {
        $my = plotUser::factory();

        $myName = $my->getSocialFieldData('JOOMLA_FULLNAME');
        if(!$myName){
            $myName=new stdClass();
        }
        $myName->first = mb_substr(JRequest::getVar('firstName'), 0, plotGlobalConfig::getVar('maxLenghtFirstName'),"UTF-8");

        if ($myName->first && $my->id && $my->saveName($myName)) {
            die($myName->first);
        }
        die(JText::_('COM_PLOT_ERROR'));
    }

    public function ajaxGetUserFullName()
    {
        $my = plotUser::factory();
        die($my->name);
    }

    public function ajaxChangeMiddleName()
    {
        $my = plotUser::factory();
        $myName = $my->getSocialFieldData('JOOMLA_FULLNAME');
        if(!$myName){
            $myName=new stdClass();

           if($my->getSocialFieldData('middle')===NULL){
               $db = Foundry::db();
            $middle=new stdClass();
               $middle->field_id=22;
               $middle->uid=$my->id;
               $middle->type='user';
               $middle->datakey='middle';
               $middle->data=NULL;
               $middle->params=NULL;
               $middle->raw=NULL;
               $db->insertObject('#__social_fields_data', $middle);
           }
            $myName->middle=$my->getSocialFieldData('middle');
        }
        $oldMiddleName = $myName->middle;
        $newMiddleName = mb_substr(JRequest::getVar('middleName'), 0, plotGlobalConfig::getVar('maxLenghtPatronymic'),"UTF-8");
        $myName->middle = $newMiddleName;
        if ($my->id && $my->saveName($myName)) {
            if (!trim($oldMiddleName) && trim($newMiddleName)) {
                plotPoints::assign('field.aboutme.fill', 'com_plot', $my->id, $my->getSocialFieldId('JOOMLA_FULLNAME'));
            }
            die($myName->middle);
        }
        die(JText::_('COM_PLOT_ERROR'));
    }

    public function ajaxChangeBirthday()
    {
        $my = plotUser::factory();

        $newBirthday = JRequest::getVar('birthday');
        $newBirthdayArr = explode('.', $newBirthday);

        $birthday = $my->getSocialFieldData('BIRTHDAY');

        if (!$birthday) {
            $birthday = new stdClass();
            plotPoints::assign('field.aboutme.fill', 'com_plot', $my->id, $my->getSocialFieldId('BIRTHDAY'));
        }
        $birthday->day = $newBirthdayArr[0];
        $birthday->month = $newBirthdayArr[1];
        $birthday->year = $newBirthdayArr[2];

        $fieldId = $my->getSocialFieldId('BIRTHDAY');
        $my->saveSocialField($fieldId,'BIRTHDAY',  $birthday->day.'.'.$birthday->month.'.'.$birthday->year);
        $my->saveSocialField($fieldId, 'day', $birthday->day);
        $my->saveSocialField($fieldId, 'month', $birthday->month);
        $my->saveSocialField($fieldId, 'year', $birthday->year);
        echo $birthday->day.'.'.$birthday->month.'.'.$birthday->year;
        die;
    }

    public function ajaxChangeEmail()
    {
        $my = plotUser::factory();
        $newEmail = JRequest::getVar('email');

        if ($my->saveEmail($newEmail)) {
            echo $newEmail;
        }
        die;
    }

    public function ajaxChangePhone()
    {
        $my = plotUser::factory();
        $oldPhone = $my->getSocialFieldData('TEXTBOX');
        $newPhone = JRequest::getVar('phone');

        $fieldId = $my->getSocialFieldId('TEXTBOX');

        $my->saveSocialField($fieldId, 'phone', $newPhone);
        if (!trim($oldPhone) && trim($newPhone)) {
            plotPoints::assign('field.aboutme.fill', 'com_plot', $my->id, $my->getSocialFieldId('TEXTBOX'));
        }

        echo $newPhone;
        die;
    }

    public function ajaxChangePassword()
    {
        $my = plotUser::factory();
        $newPassword = JRequest::getVar('password');

        $my->saveNewPassword($newPassword);
        die('ok');
    }

    public function ajaxChangeStatus()
    {
        $my = plotUser::factory();
        $oldStatus = $my->getSocialFieldData('STATUS');
        $newStatus = JRequest::getVar('status');

        $fieldId = $my->getSocialFieldId('STATUS');

        $my->saveSocialField($fieldId, 'STATUS', $newStatus);
        if (!trim($oldStatus) && trim($newStatus)) {
            plotPoints::assign('field.aboutme.fill', 'com_plot', $my->id, $my->getSocialFieldId('STATUS'));
        }
        
        echo $newStatus;
        die;
    }

    public function ajaxChangeCountry()
    {
        $my = plotUser::factory();

        $myAddress = $my->getSocialFieldData('ADDRESS');
        $newCountry = JRequest::getVar('country');
        
        if ($myAddress) {
            $oldCountry = $myAddress->country;
            $myAddress->country = $newCountry;
        } else {
            $oldCountry = 'ZZ';
            $myAddress = new stdClass();
            $myAddress->address1 = '';
            $myAddress->address2 = '';
            $myAddress->city = '';
            $myAddress->state = '';
            $myAddress->zip = '';
            $myAddress->country = $newCountry;
        }

        if ($my->saveAddress($myAddress)) {
            require_once JPATH_COMPONENT.'/helpers/countries.php';
            echo PlotHelperCountries::getCountryName($myAddress->country);
            if ($oldCountry == 'ZZ' && $newCountry != 'ZZ') {
                plotPoints::assign('field.aboutme.fill', 'com_plot', $my->id, $my->getSocialFieldId('ADDRESS'));
            }
        }
        die;
    }

    public function ajaxChangeState()
    {
        $my = plotUser::factory();
        $newState = JRequest::getVar('state');

        $myAddress = $my->getSocialFieldData('ADDRESS');
        if ($myAddress) {
            $oldState = $myAddress->state;
            $myAddress->state = $newState;
        } else {
            $oldState = '';
            $myAddress = new stdClass();
            $myAddress->address1 = '';
            $myAddress->address2 = '';
            $myAddress->city = '';
            $myAddress->state = $newState;
            $myAddress->zip = '';
            $myAddress->country = '';
        }

        if ($my->saveAddress($myAddress)) {
            if (!$oldState && $newState) {
                plotPoints::assign('field.aboutme.fill', 'com_plot', $my->id, $my->getSocialFieldId('ADDRESS'));
            }
            echo $myAddress->state;
        }
        die;
    }

    public function ajaxChangeCity()
    {
        $my = plotUser::factory();
        $newCity = JRequest::getVar('city');

        $myAddress = $my->getSocialFieldData('ADDRESS');
        if ($myAddress) {
            $oldCity = $myAddress->city;
            $myAddress->city = $newCity;
        } else {
            $oldCity = '';
            $myAddress = new stdClass();
            $myAddress->address1 = '';
            $myAddress->address2 = '';
            $myAddress->city = $newCity;
            $myAddress->state = '';
            $myAddress->zip = '';
            $myAddress->country = '';
        }

        if ($my->saveAddress($myAddress)) {
            if (!$oldCity && $newCity) {
                plotPoints::assign('field.aboutme.fill', 'com_plot', $my->id, $my->getSocialFieldId('ADDRESS'));
            }
            echo $myAddress->city;
        }
        die;
    }

    public function ajaxChangeSchool()
    {
        $my = plotUser::factory();
        $oldSchool = $my->getSocialFieldData('SCHOOL');
        $newSchool = JRequest::getVar('school');

        $fieldId = $my->getSocialFieldId('SCHOOL');

        $my->saveSocialField($fieldId, 'SCHOOL', $newSchool);
        if (!trim($oldSchool) && trim($newSchool)) {
            plotPoints::assign('field.aboutme.fill', 'com_plot', $my->id, $my->getSocialFieldId('SCHOOL'));
        }
        echo $newSchool;
        die;
    }

    public function ajaxChangeSchoolAddress()
    {
        $my = plotUser::factory();
        $oldSchoolAddress = $my->getSocialFieldData('SCHOOL_ADDRESS');
        $newSchoolAddress = JRequest::getVar('schoolAddress');

        $fieldId = $my->getSocialFieldId('SCHOOL_ADDRESS');

        $my->saveSocialField($fieldId, 'SCHOOL_ADDRESS', $newSchoolAddress);
        if (!trim($oldSchoolAddress) && trim($newSchoolAddress)) {
            plotPoints::assign('field.aboutme.fill', 'com_plot', $my->id, $my->getSocialFieldId('SCHOOL_ADDRESS'));
        }
        echo $newSchoolAddress;
        die;
    }

    public function ajaxChangeAboutMe()
    {
        $my = plotUser::factory();
        $oldAboutMe = $my->getSocialFieldData('ABOUT_ME');
        $newAboutMe = JRequest::getVar('aboutMe');

        $fieldId = $my->getSocialFieldId('ABOUT_ME');

        $my->saveSocialField($fieldId, 'ABOUT_ME', $newAboutMe);
        if (!trim($oldAboutMe) && trim($newAboutMe)) {
            plotPoints::assign('field.aboutme.fill', 'com_plot', $my->id, $my->getSocialFieldId('ABOUT_ME'));
        }
        echo $newAboutMe;
        die;
    }

    public function ajaxRemoveAvatar()
    {
        $my = plotUser::factory();
        $my->removeAvatar();
        $data = array('avatar' => $my->plotGetAvatar());
        echo json_encode($data);
        die;
    }

    public function avatarImageUpload()
    {

        Foundry::requireLogin();
        $config = Foundry::config();
        $rEFileTypes = "/^\.(jpg|jpeg|gif|png|bmp|xcf|odg){1}$/i";
        $my = Foundry::user();
        $file = JRequest::getVar('qqfile', '', 'FILES');

        if (!isset($file['tmp_name'])) {
            die('uploading error');
        }

        $access = Foundry::access($my->id, SOCIAL_TYPE_USER);
        $maxFilesize = $access->get('photos.uploader.maxsize');
        $maxFilesizeBytes = (int) $access->get('photos.uploader.maxsize') * 1048576;

        if ($file['size'] > $maxFilesizeBytes) {
            die('file size error');
        }

        if (!preg_match($rEFileTypes, strrchr($file['name'], '.'))) {
            $data['status'] = 0;
            $data['message'] = JText::_('COM_PLOT_INVALID_IMG_FORMAT');
            header('Content-Type: application/json');
            echo json_encode($data);
            die;
        }
        $image = Foundry::image();
        $image->load($file['tmp_name'], $file['name']);

        $albumModel = Foundry::model('Albums');

        $album = $albumModel->getDefaultAlbum($my->id, SOCIAL_TYPE_USER, SOCIAL_ALBUM_PROFILE_PHOTOS);

        $photo = Foundry::table('Photo');
        $photo->uid = $my->id;
        $photo->type = SOCIAL_TYPE_USER;
        $photo->album_id = $album->id;
        $photo->title = $file['name'];
        $photo->caption = '';
        $photo->ordering = 0;

        $photo->assigned_date = Foundry::date()->toMySQL();
        $photo->state = SOCIAL_PHOTOS_STATE_TMP;
        $state = $photo->store();

        // Bind any exif data if there are any. Only bind exif data for jpg files (if want to add tiff, then do add it here)
        if ($image->hasExifSupport()) {
            $photo->mapExif($file);
        }

        if (!$state) {
            $data['status'] = 0;
            $data['message'] = 'error creating image';
            header('Content-Type: application/json');
            echo json_encode($data);
            die;
        }

        $photosModel = Foundry::model('photos');
        $photosModel->pushPhotosOrdering($album->id, $photo->id);

        $photoLib = Foundry::get('Photos', $image);
        $storage = $photoLib->getStoragePath($album->id, $photo->id);
        $paths = $photoLib->create($storage);

        foreach ($paths as $type => $fileName) {
            $meta = Foundry::table('PhotoMeta');
            $meta->photo_id = $photo->id;
            $meta->group = SOCIAL_PHOTOS_META_PATH;
            $meta->property = $type;
            $meta->value = $storage.'/'.$fileName;
            $meta->store();
        }

        $image = $photo->getImageObject('original');

        $originalPhotoUrl = JUri::root().'media/com_easysocial/photos/'.$photo->album_id.'/'.$photo->id.'/'.$image->getName();
        list($width, $height) = getimagesize($originalPhotoUrl);
        if($width<plotGlobalConfig::getVar('avatarCropWidthMin') || $height<plotGlobalConfig::getVar('avatarCropHeightMin')){
            $data['status'] = 0;
            $data['message'] = 'Минимальный размер картинки должен быть '.plotGlobalConfig::getVar('avatarCropWidthMin').'x'.plotGlobalConfig::getVar('avatarCropHeightMin');

        }else{
            $data['status'] = 1;
            $data['id'] = $photo->id;
            $data['name'] = $image->getName();
        }

        header('Content-Type: application/json');
        echo json_encode($data);
        die;
    }

    public function ajaxSaveCroppedAvatar()
    {
        Foundry::requireLogin();
        $data=array();
        $formData = array();
        parse_str(JRequest::getVar('data'), $formData);

        $id = $formData['id'];

        $photo = Foundry::table('Photo');
        $photo->load($id);
        
        if (!$id || !$photo->id) {
            header('Content-Type: application/json');
            $data['status'] = 0;
            $data['message'] = 'error getting photo';
            echo json_encode($data);
            die;
        }

        // Use "original" not "stock" because it might be rotated before this.
        $image = $photo->getImageObject('stock');
        $imageOriginal = $photo->getImageObject('original');

        // Need to rotate as necessary here because we're loading up using the stock photo and the stock photo
        // is as is when the user initially uploaded.
        $image->rotate($photo->getAngle());

        $tmp = JFactory::getConfig()->get('tmp_path');
        $tmpPath = $tmp.'/'.md5($photo->id).$image->getExtension();
        $image->save($tmpPath);

        $imageForAvatar = Foundry::image();
        $imageForAvatar->load($tmpPath);

        // Get the current user.
        $my = plotUser::factory();
        $avatar = Foundry::avatar($imageForAvatar, $my->id, SOCIAL_TYPE_USER);

        // Crop the image to follow the avatar format. Get the dimensions from the request.
        $imgWidth = $formData['img-width'];
        $imgHeight = $formData['img-height'];
        $width = $formData['w'] / $imgWidth;
        $height = $formData['h'] / $imgHeight;
        $left = $formData['x'] / $imgWidth;
        $top = $formData['y'] / $imgHeight;

        $avatar->crop($top, $left, $width, $height);
        $avatar->store($photo);
        $avatarId = $my->getAvatarPhotoId();

        $origImageX = $imageOriginal->getWidth() * $left;
        $origImageY = $imageOriginal->getHeight() * $top;
        $origImageWidth = $imageOriginal->getWidth() * $width;
        $origImageHeight = $imageOriginal->getHeight() * $height;
        
        $imageOriginal->crop(round($origImageX), round($origImageY), round($origImageWidth), round($origImageHeight));
        $imageOriginal->save("media/com_easysocial/photos/{$photo->album_id}/{$photo->id}/cropped.jpg");
        JFile::delete($tmpPath);

        plotPoints::assign('avatar.change', 'com_plot', $my->id, $avatarId);
        $db = JFactory::getDbo();
        $query = "SELECT `square` FROM `#__social_avatars` WHERE `uid` = ".(int) $my->id;
        $square = $db->setQuery($query)->loadResult();
        if ($square && file_exists(JPATH_BASE.'/media/com_easysocial/avatars/users/' . $my->id . '/' . $square)) {
            $avatarUrl = JUri::root() . 'media/com_easysocial/avatars/users/' . $my->id . '/' . $square;
        } else {
            $avatarUrl = JUri::root() . 'media/com_easysocial/defaults/avatars/users/square.png';
        }
        header('Content-Type: application/json');
        $data['status'] = 1;
        $data['image'] = $avatarUrl;
        echo json_encode($data);
        die;
    }

    public function ajaxAvatarImage()
    {

        $id = JRequest::getInt('img_id', 0);
        $photo = PlotHelper::getImageOriginalById($id);
        $profileEditView = $this->getView('profileedit', 'raw');
        $profileEditView->setLayout('avatar.crop');
        $originalPhotoUrl = JUri::root() . 'media/com_easysocial/photos/' . (int)$photo->album_id . '/' . $id . '/' . $photo->value;
        $profileEditView->set('originalPhotoUrl', $originalPhotoUrl);
        $profileEditView->set('photoUrl', $photo->value);
        $profileEditView->set('albumId', $photo->album_id);
        $profileEditView->set('photoId', $id);
        list($width, $height) = getimagesize($originalPhotoUrl);
        $profileEditView->set('img_width', $width);
        $profileEditView->set('img_height', $height);
        $profileEditView->display();
        die;
    }
    # </editor-fold>

    public function ajaxAddTag()
    {

        $profileEditView = $this->getView('profileedit', 'raw');
        $profileEditView->setLayout('tag.item');
        
        $profileEditView->tag_id = JRequest::getInt('tag_id');
        $profileEditView->tag_smile = JRequest::getInt('tag_smile', 0);
        $profileEditView->checkedSmileVal = JRequest::getVar('checkedSmileVal');
        $profileEditView->tagName = JRequest::getVar('tagName');
        $profileEditView->tag_desc = JRequest::getVar('tag_desc');

        $model = $this->getModel('ProfileEdit');

        if ($profileEditView->tag_id && $model->addTag($profileEditView->tag_id, $profileEditView->tag_smile, $profileEditView->tag_desc)) {
            $user=plotUser::factory();
            $response['html'] = $profileEditView->displayTagItem();
            $response['points']=$user->getUserPoints();
            $response['points_next_level']=$user->getLevel()->pointsRemainToNextLevel;
        } else {
            $response['html'] = '';
        }
        
        echo json_encode($response);
        die;
    }





    public function ajaxDeleteTag()
    {

        $jinput = JFactory::getApplication()->input;
        $tag_id = $jinput->get('tag_id', 0, 'INT');
        $model = $this->getModel('ProfileEdit');
        $data = array();
        if ($tag_id) {
            if (!$model->checkFrendsForTag($tag_id)) {
                $data['tag'] = $model->deleteTag($tag_id);
                $data['msg'] = true;
                $user=plotUser::factory();
                $data['points']=$user->getUserPoints();
                $data['points_next_level']=$user->getLevel()->pointsRemainToNextLevel;
            } else {
                $data['msg'] = false;

            }
        }

        echo json_encode($data);
        die;
    }

    public function ajaxTagTitleSave()
    {
        $jinput = JFactory::getApplication()->input;
        $tag_id = $jinput->get('tag_id', 0, 'INT');
        $tag_title = $jinput->get('tag_title', '', 'STRING');
        $model = $this->getModel('ProfileEdit');
        $data = array();

        if ($model->saveTagTitle($tag_id, $tag_title)) {
            $data['msg'] = true;
            $user=plotUser::factory();
            $data['points']=$user->getUserPoints();
            $data['points_next_level']=$user->getLevel()->pointsRemainToNextLevel;
            echo json_encode($data);
            die;
        }
        $data['msg'] = false;
        echo json_encode($data);
        die;
    }

    public function ajaxTagSmileySave()
    {
        $jinput = JFactory::getApplication()->input;
        $tag_id = $jinput->get('tag_id', 0, 'INT');
        $tag_smiley = $jinput->get('tag_smiley', 0, 'INT');
        $model = $this->getModel('ProfileEdit');
        $data = array();
        if ($model->saveTagSmiley($tag_id, $tag_smiley)) {
            $data['msg'] = true;
            $user=plotUser::factory();
            $data['points']=$user->getUserPoints();
            $data['points_next_level']=$user->getLevel()->pointsRemainToNextLevel;
            echo json_encode($data);
            die;
        }
        $data['msg'] = false;
        echo json_encode($data);
        die;
    }
    
    public function ajaxGetNotAddedTags()
    {
        $my = plotUser::factory();
        
        $allTags = plotTags::getK2TagsList();
        $myTags = $my->getTags();
        
        $allTagsExceptMyTags = array();
        foreach ($allTags AS $allTag) {
            $isMyTag = false;
            foreach ($myTags AS $myTag) {
                if ($myTag->id == $allTag->id) {
                    $isMyTag = true;
                }
            }
            if (!$isMyTag) {
                $allTagsExceptMyTags[] = $allTag;
            }
        }
        
        $responce = array();
        $responce['tags'] = $allTagsExceptMyTags;
        
        echo json_encode($responce);
        die;
    }

    public function ajaxRejectFriendRequest()
    {
        $my = plotUser::factory();
        $friendId = JRequest::getVar('friendId', 0);
        $isRejected = $my->rejectFriendRequest($friendId);

        $result = array('error' => !$isRejected, 'friendId' => $friendId);

        $conversationModel = $this->getModel('Conversations');
        if(plotUser::factory($friendId)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($friendId)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
            $conversationModel->sendMessageSystem($friendId, 'plot-user-friend-reject-' . $my->id . '-' . $friendId);
            $mailer = JFactory::getMailer();
            $config = JFactory::getConfig();
            $sender = array(
                $config->get('mailfrom'),
                $config->get('fromname')
            );
            $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $my->id) . '">' . plotUser::factory($my->id)->name . '</a>';
            $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $friendId) . '">' . plotUser::factory($friendId)->name . '</a>';
            $mailer->setSender($sender);
            $mailer->addRecipient(plotUser::factory($friendId)->email);
            $mailer->setSubject(JText::_('COM_PLOT_SEND_FRIEND_REQUEST_SUBJECT'));
            $mailer->isHTML(true);
            $mailer->setBody(JText::sprintf("COM_PLOT_FRIEND_REJECT_MSG", $actor, $target));
            $mailer->Send();
        }
        echo json_encode($result);
        die;
    }

    public function ajaxApproveFriendRequest()
    {
        $my = plotUser::factory();
        $friendId = JRequest::getVar('friendId', 0);
        $isApproved = $my->approveFriendRequest($friendId);
        $result = array('error' => !$isApproved, 'friendId' => $friendId);

        $conversationModel = $this->getModel('Conversations');
        if(plotUser::factory($friendId)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($friendId)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
            $conversationModel->sendMessageSystem($friendId, 'plot-user-friend-approve-' . $my->id . '-' . $friendId);
            $mailer = JFactory::getMailer();
            $config = JFactory::getConfig();
            $sender = array(
                $config->get('mailfrom'),
                $config->get('fromname')
            );
            $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $my->id) . '">' . plotUser::factory($my->id)->name . '</a>';
            $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $friendId) . '">' . plotUser::factory($friendId)->name . '</a>';
            $mailer->setSender($sender);
            $mailer->addRecipient(plotUser::factory($friendId)->email);
            $mailer->setSubject(JText::_('COM_PLOT_SEND_FRIEND_REQUEST_SUBJECT'));
            $mailer->isHTML(true);
            $mailer->setBody(JText::sprintf("COM_PLOT_FRIEND_APPROVE_MSG", $actor, $target));
            $mailer->Send();
        }
        echo json_encode($result);
        die;
    }

    public function ajaxRemoveFriend()
    {
        $my = plotUser::factory();
        $friendId = JRequest::getVar('friendId', 0);
        $isApproved = $my->removeFriend($friendId);
        $result = array('error' => !$isApproved, 'friendId' => $friendId);
        $conversationModel = $this->getModel('Conversations');
        if(plotUser::factory($friendId)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($friendId)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
        $conversationModel->sendMessageSystem($friendId, 'plot-user-friend-reject-' . $my->id . '-' . $friendId);
        $mailer = JFactory::getMailer();
        $config = JFactory::getConfig();
        $sender = array(
            $config->get('mailfrom'),
            $config->get('fromname')
        );
        $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $my->id) . '">' . plotUser::factory($my->id)->name . '</a>';
        $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $friendId) . '">' . plotUser::factory($friendId)->name . '</a>';
        $mailer->setSender($sender);
        $mailer->addRecipient(plotUser::factory($friendId)->email);
        $mailer->setSubject(JText::_('COM_PLOT_SEND_FRIEND_REQUEST_SUBJECT'));
        $mailer->isHTML(true);
        $mailer->setBody(JText::sprintf("COM_PLOT_FRIEND_REJECT_MSG", $actor, $target));
        $mailer->Send();
        }
        echo json_encode($result);
        die;
    }
    
    public function ajaxFilterMyFriendsByTags()
    {
        $profileEditView = $this->getView('profileedit', 'raw');
        $id = JRequest::getInt('userId', 0);
        if ($id) {
            $profileEditView->my = plotUser::factory($id);
        } else {
            $profileEditView->my = plotUser::factory();
        }

        $profileEditView->setLayout('friends.list');

        ob_start();
        $profileEditView->displayFriendsList();
        $friendsList = ob_get_clean();

        $data = array('html' => $friendsList);
        echo json_encode($data);
        die;
    }

    public function ajaxComplain()
    {
        $jinput = JFactory::getApplication()->input;
        $userId = $jinput->get('userId', 0, 'INT');

        $profileView = $this->getView('profile', 'raw');
        $profileView->setLayout('ajaxComplain');
        $profileView->set('userId', $userId);
        $profileView->display();

        die;
    }

    public function getUserStream()
    {
        $jinput = JFactory::getApplication()->input;
        $tag_id = $jinput->get('tag_id', 0, 'INT');
        $user = plotUser::factory();

        $tags = PlotHelper::getTagById($tag_id);
        $tags->tag_img = JUri::root().'images/com_plot/def_tag.jpg';
        $tags->date = JHtml::date('now', 'd.m.Y H:m');
        //$data = array('items' => $tags);
        if ($user->isParent()) {
            $stream = $user->getStream(array('limitstart' => 0, 'limit' => 12));
            $data = array('items' => PlotHelper::renderParentStream($stream));
        } else {
            $stream = $user->getStream(array('limitstart' => 0, 'limit' => 12));
            $data = array('items' => PlotHelper::renderChildStream($stream));
        }

        echo json_encode($data);
        die;
    }

    public function getTagById()
    {
        $jinput = JFactory::getApplication()->input;
        $tag_id = $jinput->get('tag_id', 0, 'INT');
        $user = plotUser::factory();

        $tags = PlotHelper::getTagById($tag_id);
        $tags->tag_img = JUri::root().'images/com_plot/def_tag_avatar.jpg';
        $tags->date = JHtml::date('now', 'd.m.Y H:m');
        $data = array('items' => $tags);
        echo json_encode($data);
        die;
    }

    public function ajaxChangeUnsubscribeEmail()
    {
        $my = plotUser::factory();

        $newUnsubscribe = JRequest::getInt('unsubscribe');

        $fieldId = $my->getSocialFieldId('UNSUBSCRIBE');

        $my->saveSocialField($fieldId, 'UNSUBSCRIBE', $newUnsubscribe);

        echo $newUnsubscribe;
        die;
    }

}
