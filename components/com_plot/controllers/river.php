<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class PlotControllerRiver extends JControllerLegacy
{

    public function ajaxNextRiverPart()
    {
        $offset = JRequest::getInt('offset', -1);
        $riversTextsModel = $this->getModel('river_texts');
        $riversTextsModel->filter[] = array('field' => 'rt.published', 'value' => '1');
        $riverTexts = $riversTextsModel->getItems();
        
        $isOdd = $offset % 4; # offset = 2, so isOdd - % 4
        $isLast = !isset($riverTexts[$offset + 2]);
        
        if (!isset($riverTexts[$offset])) {
            die;
        }
        
        ob_start();
        ?>

        <section class="bg2">
            <?php if ($isOdd) {  ?>
            <object type="image/svg+xml" data="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate();?>/img/group3.svg" name="group3" class="frog"></object>
            <?php } else { ?>
            <object type="image/svg+xml" data="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate();?>/img/group1.svg" name="group3" class="boat"></object>
            <?php } ?>
            <object type="image/svg+xml" data="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate();?>/img/group2.svg" name="group2" class="duck"></object>
            <?php if ($isLast) { ?>
            <div class="bobrs-2" onmousedown="return false" onselectstart="return false"><object type="image/svg+xml" data="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate();?>/img/2bobrs.svg" name="2bobrs"></object>
                <p class="level"><span>Уровни роста</span></p>
            </div>
            <?php } ?>
            <article>
                <h2><?php echo $riverTexts[$offset]->title; ?></h2>
                <p><?php echo $riverTexts[$offset]->text; ?></p>
            </article>
            <?php if (isset($riverTexts[$offset+1])) { ?>
            <article>
                <h2><?php echo $riverTexts[$offset+1]->title; ?></h2>
                <p><?php echo $riverTexts[$offset+1]->text; ?></p>
            </article>
            <?php } ?>
        </section>
        <script type="text/javascript">Resizer();</script>
        <script>jPlotUp.Arrow.initialize('positionCameras');</script>
        <?php
        $html = ob_get_clean();
        echo $html;
        die;
    }

}
