<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');
require_once(JPATH_ROOT . '/administrator/components/com_easysocial/includes/foundry.php');

class PlotControllerConversations extends JControllerLegacy
{

    public function newMessage()
    {
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        $id = $jinput->get('id', 0, 'INT');
        $dashboardView = $this->getView('profile', 'html');
        $dashboardView->setLayout('new_message_popup');
        $dashboardView->set('id', $id);
        $dashboardView->set('Itemid', $Itemid);
        $dashboardView->display();
        die;
    }

    public function complaint()
    {
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        $id = $jinput->get('id', 0, 'INT');
        $dashboardView = $this->getView('profile', 'html');
        $dashboardView->setLayout('complaint_popup');
        $dashboardView->set('id', $id);
        $dashboardView->set('Itemid', $Itemid);
        $dashboardView->display();
        die;
    }



    public function message()
    {
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        $id = $jinput->get('id', 0, 'INT');
        $dashboardView = $this->getView('profile', 'html');
        $dashboardView->setLayout('message_popup');
        $dashboardView->set('id', $id);
        $dashboardView->set('Itemid', $Itemid);
        $dashboardView->display();
        die;
    }

    public function sendReport()
    {
        $app = JFactory::getApplication();
// Check for request forgeries
        Foundry::checkToken();

        // Get data from $_POST
        $post = JRequest::get('post');

        // Get the current logged in user
        $my = Foundry::user();

        // Determine if this user has the permissions to submit reports.
        $access = Foundry::access();

        if (!$access->allowed('reports.submit')) {

            $app->enqueueMessage(JText::_('COM_EASYSOCIAL_REPORTS_NOT_ALLOWED_TO_SUBMIT_REPORTS'), SOCIAL_MSG_ERROR);
            return;
        }

        // Get the reports model
        $model = Foundry::model('Reports');

        // Determine if this user has exceeded the number of reports that they can submit
        $total = $model->getCount(array('created_by' => $my->id));

        if ($access->exceeded('reports.limit', $total)) {

            $app->enqueueMessage(JText::_('COM_EASYSOCIAL_REPORTS_LIMIT_EXCEEDED'), SOCIAL_MSG_ERROR);
            return;
        }

        // Create the report
        $report = Foundry::table('Report');
        $report->bind($post);

        // Try to get the user's ip address.
        $report->ip = JRequest::getVar('REMOTE_ADDR', '', 'SERVER');

        // Set the creator id.
        $report->created_by = $my->id;

        // Set the default state of the report to new
        $report->state = 0;

        $report->type = 'user';
        // Try to store the report.
        $state = $report->store();

        // If there's an error, throw it
        if (!$state) {

            $app->enqueueMessage($report->getError(), SOCIAL_MSG_ERROR);
            return;
        }

        // @badge: reports.create
        // Add badge for the author when a report is created.
        $badge = Foundry::badges();
        $badge->log('com_easysocial', 'reports.create', $my->id, JText::_('COM_EASYSOCIAL_REPORTS_BADGE_CREATED_REPORT'));

        // @points: reports.create
        // Add points for the author when a report is created.
        $points = Foundry::points();
        $points->assign('reports.create', 'com_easysocial', $my->id);

        // Determine if we should send an email
        $config = Foundry::config();

        if ($config->get('reports.notifications.moderators')) {
            $report->notify();
        }
        $doc = JFactory::getDocument();
        $app->enqueueMessage(JText::_('COM_EASYSOCIAL_REPORTS_STORED_SUCCESSFULLY'), SOCIAL_MSG_SUCCESS);
        $doc->addScriptDeclaration("window.parent.location.reload();");
        //return;

    }

    public function sendMessage()
    {
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();

        // Ensure that the user is logged in.
        Foundry::requireLogin();

        // Get the current logged in user.
        $my = Foundry::user();

        // Get list of recipients.
        $recipients = JRequest::getVar('uid');

        // Ensure that the recipients is an array.
        $recipients = Foundry::makeArray($recipients);

        // The user might be writing to a friend list.
        $lists = JRequest::getVar('list_id');

        // Go through each of the list and find the member id's.
        if ($lists) {
            $ids = array();
            $listModel = Foundry::model('Lists');

            foreach ($lists as $listId) {
                $members = $listModel->getMembers($listId, true);

                // Merge the result set.
                $ids = array_merge($ids, $members);
            }

            if ($recipients === false) {
                $recipients = array();
            }

            // Merge the id's with the recipients and ensure that they are all unique
            $recipients = array_merge($ids, $recipients);
            $recipients = array_unique($recipients);
        }


        // Get configuration
        $config = Foundry::config();


        // If recipients is not provided, we need to throw an error.
        if (empty($recipients)) {
            $data['message'] = JText::_('COM_EASYSOCIAL_CONVERSATIONS_ERROR_EMPTY_RECIPIENTS');
            header('Content-Type: application/json');
            echo json_encode($data);
            die;
        }

        // Check if the creator is allowed to send a message to the target
        $privacy = $my->getPrivacy();

        // Ensure that the recipients is not only itself.
        foreach ($recipients as $recipient) {
            // When user tries to enter it's own id, we should just break out of this function.
            if ($recipient == $my->id) {
                $data['message'] = JText::_('COM_PLOT_CONVERSATIONS_ERROR_CANNOT_SEND_TO_SELF');
                header('Content-Type: application/json');
                echo json_encode($data);
                die;
            }


        }

        // Get the message that is being posted.
        $msg = JRequest::getVar('message', '', 'REQUEST', 'none', JREQUEST_ALLOWHTML);

        // Message should not be empty.
        if (empty($msg)) {
            $data['message'] = JText::_('COM_PLOT_CONVERSATIONS_ERROR_EMPTY_MESSAGE');
            header('Content-Type: application/json');
            echo json_encode($data);
            die;

        }

        // Filter recipients and ensure all the user id's are proper!
        $total = count($recipients);

        // If there is more than 1 recipient and group conversations is disabled, throw some errors
        if ($total > 1 && !$config->get('conversations.multiple')) {
            $data['message'] = JText::_('COM_EASYSOCIAL_CONVERSATIONS_GROUP_CONVERSATIONS_DISABLED');
            header('Content-Type: application/json');
            echo json_encode($data);
            die;

        }

        // Go through all the recipient and make sure that they are valid.
        for ($i = 0; $i < $total; $i++) {
            $userId = $recipients[$i];
            $user = Foundry::user($userId);

            if (!$user || empty($userId)) {
                unset($recipients[$i]);
            }
        }

        // After processing the recipients list, and no longer has any recipients, stop the user.
        if (empty($recipients)) {
            $data['message'] = JText::_('COM_EASYSOCIAL_CONVERSATIONS_ERROR_EMPTY_RECIPIENTS');
            header('Content-Type: application/json');
            echo json_encode($data);
            die;

        }

        // Get the conversation table.
        $conversation = Foundry::table('Conversation');

        // Determine the type of message this is by the number of recipients.
        $type = count($recipients) > 1 ? SOCIAL_CONVERSATION_MULTIPLE : SOCIAL_CONVERSATION_SINGLE;

        // For single recipients, we try to reuse back previous conversations
        // so that it will be like a long chat of history.
        if ($type == SOCIAL_CONVERSATION_SINGLE) {
            // We know that the $recipients[0] is always the target user.
            $state = $conversation->loadByRelation($my->id, $recipients[0], SOCIAL_CONVERSATION_SINGLE);
        }

        // @points: conversation.create.group
        // Assign points when user starts new group conversation
        if (count($recipients) > 1) {
            $points = Foundry::points();
            $points->assign('conversation.create.group', 'com_easysocial', $my->id);
        }

        // Set the conversation creator.
        $conversation->created_by = $my->id;

        // Set the last replied date.
        $conversation->lastreplied = Foundry::date()->toMySQL();

        // Set the conversation type.
        $conversation->type = $type;

        // Let's try to create the conversation now.
        $state = $conversation->store();

        // If there's an error storing the conversation, break.
        if (!$state) {
            $data['message'] = JText::_($conversation->getError());
            header('Content-Type: application/json');
            echo json_encode($data);
            die;

        }

        // @rule: Store conversation message
        $message = Foundry::table('ConversationMessage');
        $post = JRequest::get('POST');

        // Bind the message data.
        $message->bind($post);

        // Set the conversation id since we have the conversation id now.
        $message->conversation_id = $conversation->id;

        // Sets the message type.
        $message->type = SOCIAL_CONVERSATION_TYPE_MESSAGE;

        // Set the creation date.
        $message->created = Foundry::date()->toMySQL();

        // Set the creator.
        $message->created_by = $my->id;

        // Try to store the message now.
        $state = $message->store();

        if (!$state) {
            $data['message'] = JText::_($message->getError());
            header('Content-Type: application/json');
            echo json_encode($data);
            die;

        }

        // Add users to the message maps.
        array_unshift($recipients, $my->id);

        $model = Foundry::model('Conversations');

        // Add the recipient as a participant of this conversation.
        $model->addParticipants($conversation->id, $recipients);

        // Add the message maps so that the recipient can view the message
        $model->addMessageMaps($conversation->id, $message->id, $recipients, $my->id);

        // Process attachments here.
        if ($config->get('conversations.attachments.enabled')) {
            $attachments = JRequest::getVar('upload-id');

            // If there are attachments, store them appropriately.
            if ($attachments) {
                $message->bindTemporaryFiles($attachments);
            }
        }

        // Bind message location if necessary.
        if ($config->get('conversations.location')) {
            $address = JRequest::getVar('address', '');
            $latitude = JRequest::getVar('latitude', '');
            $longitude = JRequest::getVar('longitude', '');

            if (!empty($address) && !empty($latitude) && !empty($longitude)) {
                $location = Foundry::table('Location');
                $location->loadByType($message->id, SOCIAL_TYPE_CONVERSATIONS, $my->id);

                $location->address = $address;
                $location->latitude = $latitude;
                $location->longitude = $longitude;
                $location->user_id = $this->created_by;
                $location->type = SOCIAL_TYPE_CONVERSATIONS;
                $location->uid = $message->id;

                $state = $location->store();
            }
        }

        // Send notification email to recipients
        foreach ($recipients as $recipientId) {
            // We should not send a notification to ourself.
            if ($recipientId != $my->id) {
                $recipient = Foundry::user($recipientId);

                // Add new notification item
                $mailParams = Foundry::registry();
                $mailParams->set('name', $recipient->getName());
                $mailParams->set('authorName', $my->getName());
                $mailParams->set('authorAvatar', $my->plotGetAvatar());
                $mailParams->set('authorLink', $my->getPermalink(true, true));
                $mailParams->set('message', $message->message);
                $mailParams->set('messageDate', $message->created);
                $mailParams->set('conversationLink', $conversation->getPermalink(true, true));

                // Send a notification for all participants in this thread.
                $state = Foundry::notify('conversations.new', array($recipientId), array('title' => JText::sprintf('COM_EASYSOCIAL_CONVERSATIONS_NEW_EMAIL_TITLE', $my->getName()), 'params' => $mailParams), false);
            }
        }
        $data['message'] = JText::_('COM_PLOT_CONVERSATIONS_MESSAGE_SENT');
        header('Content-Type: application/json');
        echo json_encode($data);
        die;
    }

    /**
     * Creates a new friend request to a target

     */
    public function request()
    {
        // Check for request forgeries.
       // Foundry::checkToken();

        // User needs to be logged in
        Foundry::requireLogin();

        // Get the target user that is being added.
        $id = JRequest::getInt('id');
        $user = Foundry::user($id);

        // Get the current view.
       // $view = $this->getCurrentView();

        // @TODO: Check if target user blocks this.

        // If the user doesn't exist;
        if (!$user || !$id) {

            $send_data['message']= JText::_('COM_EASYSOCIAL_FRIENDS_UNABLE_TO_LOCATE_USER');
            echo $send_data['message'];

            exit();
        }

        // Get the current viewer.
        $my = Foundry::user();

        // Load up the model to check if they are already friends.
        $model = Foundry::model('Friends');

        $friend = Foundry::table('Friend');

        // Do not allow user to create a friend request to himself
        if ($my->id == $user->id) {

            $send_data['message']= JText::_('COM_EASYSOCIAL_FRIENDS_UNABLE_TO_ADD_YOURSELF');
            echo $send_data['message'];

            exit();
        }

        // If they are already friends, ignore this.
        if ($model->isFriends($my->id, $user->id)) {

            $send_data['message']= JText::_('COM_EASYSOCIAL_FRIENDS_ERROR_ALREADY_FRIENDS');
            echo $send_data['message'];

            exit();
        }

        // Check if user has already previously requested this.
        if ($model->isFriends($my->id, $user->id, SOCIAL_FRIENDS_STATE_PENDING)) {

            $send_data['message']= JText::_('COM_EASYSOCIAL_FRIENDS_ERROR_ALREADY_REQUESTED');
            echo $send_data['message'];

            exit();
        }

        // If everything is okay, we proceed to add this request to the friend table.
        $friend->setActorId($my->id);

        // Set the target's id.
        $friend->setTargetId($user->id);

        // @TODO: Configurable. Set the state.
        $friend->setState(SOCIAL_FRIENDS_STATE_PENDING);

        // Store the friend request
        $state = $friend->store();

        // Send notification to the target when a user requests to be his / her friend.
        $params = array(
            'requesterId' => $my->id,
            'requesterAvatar' => $my->getAvatar(SOCIAL_AVATAR_LARGE),
            'requesterName' => $my->getName(),
            'requesterLink' => $my->getPermalink(true, true),
            'requestDate' => Foundry::date()->toMySQL(),
            'totalFriends' => $my->getTotalFriends(),
            'totalMutualFriends' => $my->getTotalMutualFriends($user->id)
        );
        // Email template
        $emailOptions = array(
            'title' => JText::sprintf('COM_EASYSOCIAL_EMAILS_SUBJECT_FRIENDS_NEW_REQUEST', $my->getName()),
            'template' => 'site/friends/request',
            'params' => $params
        );


        Foundry::notify('friends.request', array($user->id), $emailOptions, false);

        // @badge: friends.create
        // Assign badge for the person that initiated the friend request.
        $badge = Foundry::badges();
        $badge->log('com_easysocial', 'friends.create', $user->id, JText::_('COM_EASYSOCIAL_FRIENDS_BADGE_REQUEST_TO_BE_FRIEND'));

        $allowedCallbacks = array(__FUNCTION__, 'usersRequest', 'popboxRequest');
        $callback = JRequest::getVar('viewCallback', __FUNCTION__);

        if (!in_array($callback, $allowedCallbacks)) {
            $callback = __FUNCTION__;
        }
        $send_data['message']= JText::_('COM_PLOT_FRIEND_REQUESTED_SEND');
        echo $send_data['message'];

        exit();
    }

    /**
     * Processes a new reply for an existing conversation
     *
     * @since	1.0
     * @access	public
     * @param	null
     * @return	null
     */
    public function reply()
    {
        // Check for request forgeries.
        Foundry::checkToken();

        // We know for the fact that guests can never access conversations.
        Foundry::requireLogin();
        $app = JFactory::getApplication();
        // Get the current logged in user.
        $my 		= Foundry::user();

        // Get the message from the request. It should support raw codes.
        $msg		= JRequest::getVar( 'message' , '' );

        // Get the conversation id from the request.
        $id 		= JRequest::getInt( 'id' );

        $Itemid 		= JRequest::getInt( 'Itemid' );
        // Get the configuration object.
        $config 	= Foundry::config();

        // Try to load the conversation.
        $conversation 	= Foundry::table( 'Conversation' );
        $state			= $conversation->load( $id );

        // If conversation id is invalid or not supplied, we need to throw some errors.
        if( !$id || !$conversation->load( $id ) )
        {

            $app->enqueueMessage(JText::_( 'COM_EASYSOCIAL_CONVERSATIONS_ERROR_INVALID_ID' ) , SOCIAL_MSG_ERROR);
            $this->setRedirect('index.php?option=com_plot&view=conversations&layout=read&id='. (int)$id .'&Itemid='.$Itemid);
        }

        // Load the conversation model.
        $model 			= Foundry::model( 'Conversations' );

        // Let's try to store the message now.
        $message 		= $model->addReply( $id , $msg , $my->id );

        if( !$message )
        {

            $app->enqueueMessage($model->getError(), SOCIAL_MSG_ERROR);
            $this->setRedirect('index.php?option=com_plot&view=conversations&layout=read&id='. (int)$id .'&Itemid='.$Itemid);
        }

        //set the message->day to 0. This 'day' variable is need in themes file.
        $message->day = 0;

        if( $config->get( 'conversations.attachments.enabled' ) )
        {
            // Process attachments here.
            $attachments	= JRequest::getVar( 'upload-id' );

            // If there are attachments, store them appropriately.
            if( $attachments )
            {
                $message->bindTemporaryFiles( $attachments );
            }
        }

        if( $config->get( 'conversations.location' ) )
        {
            // Let's try to process the location if necessary.
            $address 		= JRequest::getVar( 'address' , '' );
            $latitude 		= JRequest::getVar( 'latitude' , '' );
            $longitude 		= JRequest::getVar( 'longitude' , '' );

            if( !empty( $address ) && !empty( $latitude ) && !empty( $longitude ) )
            {
                $location 				= Foundry::table( 'Location' );
                $location->loadByType( $message->id , SOCIAL_TYPE_CONVERSATIONS , $my->id );

                $location->address		= $address;
                $location->latitude		= $latitude;
                $location->longitude	= $longitude;
                $location->user_id 		= $my->id;
                $location->type 		= SOCIAL_TYPE_CONVERSATIONS;
                $location->uid 			= $message->id;

                $location->store();
            }
        }

        // Get recipients of this conversation.
        $recipients 	= $conversation->getParticipants( array( $my->id ) );

        foreach( $recipients as $recipient )
        {
            // Add new notification item
            $mailParams 	= Foundry::registry();
            $title 			= JText::sprintf( 'COM_EASYSOCIAL_CONVERSATIONS_REPLY_EMAIL_TITLE' , $my->getName() );
            $mailParams->set( 'name'			, $recipient->getName() );
            $mailParams->set( 'authorName'		, $my->getName() );
            $mailParams->set( 'authorAvatar'	, $my->plotGetAvatar() );
            $mailParams->set( 'authorLink'		, $my->getPermalink( true, true ) );
            $mailParams->set( 'message'			, $message->message );
            $mailParams->set( 'messageDate'		, $message->created );
            $mailParams->set( 'conversationLink', $conversation->getPermalink( true, true ) );

            // Send a notification for all participants in this thread.
            $state 	= Foundry::notify( 'conversations.reply' , array( $recipient ) , array( 'title' => $title , 'params' => $mailParams ) , false );
        }
        $this->setRedirect('index.php?option=com_plot&view=conversations&layout=read&id='. (int)$id .'&Itemid='.$Itemid);

    }



}
