<?php
defined('_JEXEC') or die('Restricted access');

class PlotControllerPublication extends JControllerLegacy
{

    public function pickoutImage()
    {
        $publicationModel = $this->getModel('publication');
        $this->book = $publicationModel->getBook();
        PlotHelper::echoNewImageForSocials( $this->book->thumbnailUrl, JRequest::getInt('canv_width'), JRequest::getInt('canv_height') );
        die;
    }



    public function ajaxGetBeforePublications()
    {
        $userData = JRequest::getVar('userData');
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');
        }
        $booksModel = $this->getModel('publications');
        $view = $this->getView('publications', 'raw');
        $bookId=(isset($userData['bookId'])) ? $userData['bookId'] : 0;

        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');

            $limitCount = plotGlobalConfig::getVar('booksResultsShowFirstCountParent');
        } else {
            $limitCount = plotGlobalConfig::getVar('booksResultsShowFirstCountChild');
        }

        $offset = JRequest::getInt('offset');
        if((int)$offset==(int)$limitCount){
            $offset=0;
        }
        $limit = array('offset' => $offset, 'limit' => $limitCount);

        $booksData = $booksModel->getBeforeHashtagsPublications( $limit, $bookId);

        $view->books = $booksData;


        $view->setLayout('books.list');

        $data = array();
        $data['renderedBooks'] = $view->ajaxRenderBooksList();
        $data['renderedBooks'] .= "<script>jPlotUp.Arrow.initialize('positionCameras');</script>";
       echo $data['renderedBooks'];
        die;
    }

    public function ajaxGetAfterPublications()
    {
        $userData = JRequest::getVar('userData');
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');
        }
        $booksModel = $this->getModel('publications');
        $view = $this->getView('publications', 'raw');
        $bookId=(isset($userData['bookId'])) ? $userData['bookId'] : 0;

        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');

            $limitCount = plotGlobalConfig::getVar('booksResultsShowFirstCountParent');
        } else {
            $limitCount = plotGlobalConfig::getVar('booksResultsShowFirstCountChild');
        }

        $offset = JRequest::getInt('offset');
        if((int)$offset==(int)$limitCount){
            $offset=0;
        }
        $limit = array('offset' => $offset, 'limit' => $limitCount);

        $booksData = $booksModel->getAfterHashtagsPublications( $limit, $bookId);

        $view->books = $booksData;


        $view->setLayout('books.list');

        $data = array();
        $data['renderedBooks'] = $view->ajaxRenderBooksList();
        //$data['renderedBooks'] .= "<script>jPlotUp.Arrow.initialize('positionCameras');</script>";
        echo $data['renderedBooks'];
        die;
    }

    public function bookReadedReplaceText()
    {
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');

        $bookModel = $this->getModel('Publication');
        $book = $bookModel->bookReadedReplaceText($ids, JText::_("COM_PLOT_SEND_READED_BOOK"));

        echo json_encode($book);
        exit();
    }

    public function bookBuyReplaceText()
    {
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');

        $bookModel = $this->getModel('Publication');
        $book = $bookModel->bookBuyReplaceText($ids, JText::_("COM_PLOT_BUY_BOOK_EMAIL"));

        echo json_encode($book);
        exit();
    }

}
