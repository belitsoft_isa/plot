<?php

defined('_JEXEC') or die('Restricted access');


class PlotControllerNotifications extends JControllerLegacy
{
    private function getEventsToSubmit()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->clear()
            ->select('`e`.*')
            ->select('`aem`.age_id')
            ->select('`uem`.`uid`')
            ->select('`t`.`tagId`')
            ->from('`#__plot_events` AS `e`')
            ->innerJoin('`#__plot_age_event_map` AS aem ON e.id=aem.event_id')
            ->innerJoin('`#__plot_tags` AS t ON t.entityId=e.id AND entity="meeting"')
            ->innerJoin('`#__plot_user_event_map` AS `uem` ON `uem`.`event_id`=`e`.`id`')
            ->where('DATE(e.create_date) >=  DATE(CURDATE() - INTERVAL ' . (int)plotGlobalConfig::getVar("meetingsDaysForSend") . ' DAY)');

        $events = $db->setQuery($query)->loadObjectList();

        return $events;
    }


    private function getUsersUnsubscribeOnEvent()
    {
        $events = $this->getEventsToSubmit();

        //get all users, which subscribe on event
        $u_ids = array();
        $u_tags = array();
        $str_ids = '';
        $str_tags = '';
        $age_ids = array();
        foreach ($events AS $event) {
            //if(!in_array($event->user_id,$u_ids)){
            $u_ids[] = $event->user_id;
            //}
            if (!in_array($event->uid, $u_ids)) {
                $u_ids[] = $event->uid;
            }
            // if(!in_array($event->tagId,$u_tags)){
            $u_tags[] = $event->tagId;
            // }
            // if(!in_array($event->age_id,$age_ids)){
            $age_ids[$event->id] = $event->age_id;
            // }
        }
        $str_ids = implode(",", $u_ids);
        $str_tags = implode(",", $u_tags);

        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->clear()
            ->select('`u`.`id`')
            ->from('`#__users` AS `u`')
            ->innerJoin('`#__plot_tags` AS t ON t.entityId=u.id AND entity="user" AND t.tagId IN(' . $str_tags . ')')
            ->where('u.id NOT IN(' . $str_ids . ')');
        $users = $db->setQuery($query)->loadObjectList();


        $ages_ids = plotAges::getUserAgeId($users);

        $ages = array();
        // $events_ids = array();
        foreach ($age_ids AS $k => $age) {
            foreach ($ages_ids AS $key => $value) {
                if ($age == $value) {
                    $ages[] = array('uid' => $key, 'event' => $k);

                }
            }
        }


        return $ages;

    }

    public function sendEventsNotification()
    {
        $file= JPATH_BASE.'/logs/notification_log.txt';
        $data = $this->getUsersUnsubscribeOnEvent();

        $db = JFactory::getDbo();
        $i = 0;
        file_put_contents($file, 'total notifications: '.count($data).'\r\n', FILE_APPEND | LOCK_EX);
        foreach ($data AS $key => $value) {
            if ($value['uid'] && $value['event']) {
                $notification = new stdClass();
                $notification->entity = 'meeting';
                $notification->entityId = (int)$value['event'];
                $notification->user_id = (int)$value['uid'];
                $notification->date_created=Foundry::date()->toMySQL();
               if(!$this->checkSendNotifikation($notification)){
                   $db->insertObject('#__plot_notifications', $notification);
                   $notification->id=$db->insertid();
                   $this->sendMessage($notification);
                   $i++;
               }else{
                   file_put_contents($file, "Notifications: ".$notification->entity." ".$notification->entityId." sent user ".$notification->user_id." yet \r\n", FILE_APPEND | LOCK_EX);
               }
            }
        }

        return $i;
    }

    public function sendNotifications()
    {
        JFactory::getDbo()->setQuery("INSERT INTO `#__plot_payments` SET `text` = 'cron started', `text2` = 'time: ".date('Y-m-d h:i:s')."', `text3` = ''")->query();
        echo  $this->sendEventsNotification();
        JFactory::getDbo()->setQuery("INSERT INTO `#__plot_payments` SET `text` = 'cron finished', `text2` = 'time: ".date('Y-m-d h:i:s')."', `text3` = ''")->query();
    }

    public function sendMessage($notification)
    {
        JModelLegacy::addIncludePath(JPATH_ROOT.DS.'components'.DS.'com_plot'.DS.'models', 'Plot');
        $eventsModel = JModelLegacy::getInstance('events', 'plotModel');
        $file= JPATH_BASE.'/logs/notification_log.txt';
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();

        // Get the current logged in user.
        $my = Foundry::user(859);

        // Get list of recipients.
        $recipients = $notification->user_id;

        // Ensure that the recipients is an array.
        $recipients = Foundry::makeArray($recipients);

        // The user might be writing to a friend list.
        $lists = JRequest::getVar('list_id');



        // Get configuration
        $config = Foundry::config();

        // Filter recipients and ensure all the user id's are proper!
        $total = count($recipients);



        // Go through all the recipient and make sure that they are valid.
        for ($i = 0; $i < $total; $i++) {
            $userId = $recipients[$i];
            $user = Foundry::user($userId);

            if (!$user || empty($userId)) {
                unset($recipients[$i]);
            }
        }

        // After processing the recipients list, and no longer has any recipients, stop the user.
        if (empty($recipients)) {

            $app->enqueueMessage(JText::_('COM_EASYSOCIAL_CONVERSATIONS_ERROR_EMPTY_RECIPIENTS'), SOCIAL_MSG_ERROR);
            return;
        }

        // Get the conversation table.
        $conversation = Foundry::table('Conversation');

        // Determine the type of message this is by the number of recipients.
        $type = count($recipients) > 1 ? SOCIAL_CONVERSATION_MULTIPLE : SOCIAL_CONVERSATION_SINGLE;

        // For single recipients, we try to reuse back previous conversations
        // so that it will be like a long chat of history.
        if ($type == SOCIAL_CONVERSATION_SINGLE) {
            // We know that the $recipients[0] is always the target user.
            $state = $conversation->loadByRelation($my->id, $recipients[0], SOCIAL_CONVERSATION_SINGLE);
        }

        // Set the conversation creator.
        $conversation->created_by = $my->id;

        // Set the last replied date.
        $conversation->lastreplied = Foundry::date()->toMySQL();

        // Set the conversation type.
        $conversation->type = $type;

        // Let's try to create the conversation now.
        $state = $conversation->store();

        // If there's an error storing the conversation, break.
        if (!$state) {
            file_put_contents($file,  $app->enqueueMessage(JText::_($conversation->getError()), SOCIAL_MSG_ERROR).'\r\n', FILE_APPEND | LOCK_EX);
            return;
        }

        // @rule: Store conversation message
        $message = Foundry::table('ConversationMessage');
        if($notification->entity=='meeting'){
            $event=$eventsModel->getEvent($notification->entityId);
            $message_text='plot-notification-'.$notification->entityId;
        }

        $post = array('message'=>$message_text,'uid'=>$notification->user_id);

        // Bind the message data.
        $message->bind($post);

        // Set the conversation id since we have the conversation id now.
        $message->conversation_id = $conversation->id;

        // Sets the message type.
        $message->type = SOCIAL_CONVERSATION_TYPE_MESSAGE;

        // Set the creation date.
        $message->created = Foundry::date()->toMySQL();

        // Set the creator.
        $message->created_by = $my->id;

        // Try to store the message now.
        $state = $message->store();

        if (!$state) {
            file_put_contents($file,  $app->enqueueMessage(JText::_($message->getError()), SOCIAL_MSG_ERROR).'\r\n', FILE_APPEND | LOCK_EX);

            return;
        }

        // Add users to the message maps.
        array_unshift($recipients, $my->id);

        $model = Foundry::model('Conversations');

        // Add the recipient as a participant of this conversation.
        $model->addParticipants($conversation->id, $recipients);

        // Add the message maps so that the recipient can view the message
        $model->addMessageMaps($conversation->id, $message->id, $recipients, $my->id);

        // Process attachments here.
        if ($config->get('conversations.attachments.enabled')) {
            $attachments = JRequest::getVar('upload-id');

            // If there are attachments, store them appropriately.
            if ($attachments) {
                $message->bindTemporaryFiles($attachments);
            }
        }

        // Bind message location if necessary.
        if ($config->get('conversations.location')) {
            $address = JRequest::getVar('address', '');
            $latitude = JRequest::getVar('latitude', '');
            $longitude = JRequest::getVar('longitude', '');

            if (!empty($address) && !empty($latitude) && !empty($longitude)) {
                $location = Foundry::table('Location');
                $location->loadByType($message->id, SOCIAL_TYPE_CONVERSATIONS, $my->id);

                $location->address = $address;
                $location->latitude = $latitude;
                $location->longitude = $longitude;
                $location->user_id = $this->created_by;
                $location->type = SOCIAL_TYPE_CONVERSATIONS;
                $location->uid = $message->id;

                $state = $location->store();
            }
        }

        // Send notification email to recipients
        foreach ($recipients as $recipientId) {
            // We should not send a notification to ourself.
            if ($recipientId != $my->id) {
                $recipient = Foundry::user($recipientId);

                // Add new notification item
                $mailParams = Foundry::registry();
                $mailParams->set('name', $recipient->getName());
                $mailParams->set('authorName', $my->getName());
                $mailParams->set('authorAvatar', $my->plotGetAvatar());
                $mailParams->set('authorLink', $my->getPermalink(true, true));
                $mailParams->set('message', $message->message);
                $mailParams->set('messageDate', $message->created);
                $mailParams->set('conversationLink', $conversation->getPermalink(true, true));

                // Send a notification for all participants in this thread.
                $state = Foundry::notify('conversations.new', array($recipientId), array('title' => JText::sprintf('COM_EASYSOCIAL_CONVERSATIONS_NEW_EMAIL_TITLE', $my->getName()), 'params' => $mailParams), false);
            }
        }
        file_put_contents($file, 'send '.$notification->user_id .' \r\n', FILE_APPEND | LOCK_EX);

        return;
    }

    public function replaceText(){
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');

        $essayModel = $this->getModel('notifications');

        $essay = $essayModel->replaceForConversation($ids);


        echo json_encode($essay);
        exit();

    }

    private function checkSendNotifikation($notification){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->clear()
            ->select('`id`')
            ->from('`#__plot_notifications`')
            ->where('entity="'.$notification->entity.'" AND user_id='.(int)$notification->user_id.' AND entityId='.(int)$notification->entityId);
       return $db->setQuery($query)->loadResult();

    }


    public function test(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->clear()
            ->select('`e`.id')
            ->from('`#__plot_events` AS `e`');
        $events=$db->setQuery($query)->loadObjectList();
       // $this->addAgeToEvent($events);
        $this->addTagToEvent($events);
    }

    private function addAgeToEvent($events){
        $ages=$this->getAges();
       foreach($events AS $event){
           foreach($ages AS $age){
               if(!$this->ageEventExist($event->id,$age->id)){
                $this->saveAgeEventMap($event->id,$age->id);
               }
           }

       }
    }

    private function addTagToEvent($events){

        $tags=$this->getTags();

        foreach($events AS $event){
            foreach($tags AS $tag){
                if(!$this->tagEventExist($event->id,$tag->id)){
                   $this->saveTagEventMap($event->id,$tag->id);
                }
            }

        }
    }

    private function getAges(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->clear()
            ->select('`a`.id')
            ->from('`#__plot_ages` AS `a`');
        $ages=$db->setQuery($query, 0, 3)->loadObjectList();
        return $ages;
    }

    private function ageEventExist($eventId, $ageId){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->clear()
            ->select('`a`.event_id')
            ->from('`#__plot_age_event_map` AS `a`')
            ->where('a.event_id='.(int)$eventId)
            ->where('a.age_id='.(int)$ageId);
        return $db->setQuery($query, 0, 3)->loadResult();
    }

    private function saveAgeEventMap($eventId, $ageId){
        $db = JFactory::getDbo();
        $map = new stdClass();
        $map->event_id = (int)$eventId;
        $map->age_id = (int)$ageId;
        $db->insertObject('#__plot_age_event_map', $map);
    }

    private function getTags(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->clear()
            ->select('`a`.id')
            ->from('`#__k2_items` AS `a`')
            ->where('a.catid=1');

        $tags=$db->setQuery($query, 0, 3)->loadObjectList();

        return $tags;
    }

    private function tagEventExist($eventId, $tagId){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->clear()
            ->select('`a`.id')
            ->from('`#__plot_tags` AS `a`')
            ->where('a.entityId='.(int)$eventId)
            ->where('a.tagId='.(int)$tagId)
            ->where('a.entity="meeting"');
        return $db->setQuery($query)->loadResult();
    }

    private function saveTagEventMap($eventId, $tagId){
        $db = JFactory::getDbo();
        $map = new stdClass();
        $map->entityId = (int)$eventId;
        $map->tagId = (int)$tagId;
        $map->entity = "meeting";
        $map->title='';
        $map->smiley='';

        $db->insertObject('#__plot_tags', $map);
    }
}
