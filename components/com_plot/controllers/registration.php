<?php

defined('_JEXEC') or die('Restricted access');

class PlotControllerRegistration extends PlotController
{

    public function ajaxGetLoginPopupHtml()
    {
        JFactory::getSession()->set('plotProfileTypeForRegistration', plotGlobalConfig::getVar('childProfileId'));
        $loginView = $this->getView('registration', 'html');
        $loginView->setLayout('login_popup');
        $loginView->display();
        die;
    }
    
    public function ajaxChangeProfileTypeForRegistration()
    {
        $profileType = JRequest::getInt('profileType', 0);
        JFactory::getSession()->set('plotProfileTypeForRegistration', $profileType);
        die;
    }

    public function loginWithSocial()
    {
        $this->checkIsLoggedAndIfTrueRaiseError();
        
        require_once( JPATH_ROOT.'/administrator/components/com_easysocial/includes/foundry.php' );
        $app = JFactory::getApplication();
        
        $uLoginReturnedData = json_decode(file_get_contents('http://ulogin.ru/token.php?token='.$_POST['token'].'&host='.$_SERVER['HTTP_HOST']), true);




        if (!isset($uLoginReturnedData['error'])) {

            $this->isUsernameExist($uLoginReturnedData['network'].$uLoginReturnedData['uid'], $uLoginReturnedData['email']);
            # ok result has been returned
            $userData = array(
                'username' => $uLoginReturnedData['network'].$uLoginReturnedData['uid'],
                'email' => $uLoginReturnedData['email'],
                'password' => JUserHelper::genRandomPassword(),
                'first_name' => $uLoginReturnedData['first_name'],
                'middle_name' => '',
                'last_name' => $uLoginReturnedData['last_name'],
                'profile_type_id' => JFactory::getSession()->get('plotProfileTypeForRegistration', plotGlobalConfig::getVar('childProfileId'))
            );

            Foundry::import('fields:/user/joomla_username/helper');

            if (SocialFieldsUserJoomlaUsernameHelper::exists($userData['username'])) {

                if (JRequest::getVar('login')) {
                    $this->changePasswordAndLogin($userData['username']);
                } else {
                    $app->setUserState( 'first', 0 );
                    $app->enqueueMessage('Пользователь уже существует.', 'error');
                }
            } else {

                if (JRequest::getVar('login')) {
                    $app->enqueueMessage('Пользователя не существует. Пожалуйста, зарегистрируйтесь.', 'error');
                } else {

                    $this->registerAndLogin($userData);
                }                
            }

            if($app->getUserState( 'first', 0 )){
                $app->redirect('/dobro-pozalovat');
                return;
            }else{
                $app->redirect(JRoute::_('index.php?option=com_plot&view=river', false));
            }

        } else {
            $app->enqueueMessage(JText::_('COM_PLOT_ERROR_WHILE_GETTING_DATA_FROM_SOCIAL'), 'error');
            $app->redirect(JRoute::_('index.php?option=com_plot&view=river', false));
        }
    }

    private function getPasswordUserFieldIdFromFields($fields)
    {
        foreach ($fields AS $field) {
            if ($field->unique_key == 'JOOMLA_PASSWORD') {
                return $field->id;
            }
        }
        return 0;
    }

    private function changePasswordAndLogin($username)
    {

        $db = JFactory::getDbo();
        $app = JFactory::getApplication();
        $userId = $db->setQuery("SELECT `id` FROM `#__users` WHERE `username` = '$username'")->loadResult();
        $user = Foundry::user($userId);

        $newPassword = JUserHelper::genRandomPassword();
        $salt 		= JUserHelper::genRandomPassword( 32 );
        $crypted	= JUserHelper::getCryptedPassword( $newPassword , $salt );
        $newPasswordEncrypted	= $crypted . ':' . $salt;

        $user->password = $newPasswordEncrypted;
        $user->username=$user->email;
        $user->password_clear	= $newPassword;
        $user->save(true);
        $db = JFactory::getDbo();
        $u = new stdClass();
        $u->id = (int)$userId;
        $u->password = $newPasswordEncrypted;
        $db->updateObject('#__users', $u, 'id');

        if ($app->login(array('username' => $user->email, 'password' => $newPassword))) {

            $this->addToStreamOnLoggedIn();
        }

        $user->username=$username;
        $user->save(true);

        $app->redirect(JRoute::_('index.php?option=com_plot&view=river', false));
    }

    public function registerAndLoginNewUser()
    {
        $this->checkForm();
        $app = JFactory::getApplication();
       if(!JRequest::getVar('user-agreement')){
           $app->enqueueMessage('Необходимо принять пользовательское соглашение.', 'error');
           $app->redirect(JRoute::_('index.php?option=com_plot&view=river', false));
       }
        $this->checkIsLoggedAndIfTrueRaiseError();
        $this->validateRegistrationInputAndRedirectIfErrors();
        
        $userData = array(
            'username' => JRequest::getVar('email'),
            'email' => JRequest::getVar('email'),
            'password' => JRequest::getVar('password'),
            'first_name' => JRequest::getVar('name'),
            'middle_name' => '',
            'last_name' => JRequest::getVar('surname'),
            'profile_type_id' => JFactory::getSession()->get('plotProfileTypeForRegistration', plotGlobalConfig::getVar('childProfileId'))
        );      
        $this->registerAndLogin($userData);

        $app->setUserState( 'first', 1 );
        $app->redirect('/dobro-pozalovat');
       //$app->redirect(JRoute::_('index.php?option=com_plot&view=river', false));
    }
    
    private function registerAndLogin($data)
    {
        $data['cid'] = "[\"18\",\"19\",\"20\"]";
        $data['currentStep'] = "3";
        $data['controller'] = "registration";
        $data['task'] = "saveStep";
        $data['option'] = "com_easysocial";
        $data['password_clear'] = $data['password'];
        $data['first']=$data['first_name'];
        $data['last']=$data['last_name'];
        $data['name']=$data['first_name'].' '.$data['last_name'];



        $registrationModel = Foundry::model('Registration');
        $fields = $registrationModel->getCustomFieldsForProfiles($data['profile_type_id']);

        $joomlaPasswordFieldId = $this->getPasswordUserFieldIdFromFields($fields);
        
        $data["es-fields-$joomlaPasswordFieldId-input"] = $data['password'];
        $data["es-fields-$joomlaPasswordFieldId-reconfirm"] = $data['password'];

        $user = Foundry::user();
        $args = array(&$data, &$user);

        $lib = Foundry::getInstance('Fields');
        $handler = $lib->getHandler();
        $errors = $lib->trigger('onRegisterBeforeSave', SOCIAL_FIELDS_GROUP_USER, $fields, $args, array($handler, 'beforeSave'));

        if (is_array($errors)) {
            if (in_array(false, $errors, true)) {
                $registrationModel->setError($errors);
                return false;
            }
        }

        $profile = Foundry::table('Profile');
        $profile->load($data['profile_type_id']);

        $json = Foundry::json();
        $groups = $json->decode($profile->gid);
        $data['gid'] = $groups;

        $user->bind($data, SOCIAL_POSTED_DATA);


        $user->set('state', constant('SOCIAL_REGISTER_AUTO'));
        $state = $user->save();
        if (!$state) {
            $registrationModel->setError($user->getError());
            JFactory::getApplication()->enqueueMessage($user->getError(), 'error');
            return false;
        }

        $user->profile_id = $profile->id;
        $profile->addUser($user->id);

        $argsBeforeSaveFields = array(&$data, &$user);

        $lib->trigger('onRegisterAfterSave', SOCIAL_FIELDS_GROUP_USER, $fields, $argsBeforeSaveFields);
        $user->bindCustomFields($data);
        $argsAfterSaveFields = array(&$data, &$user);
        $lib->trigger('onRegisterAfterSaveFields', SOCIAL_FIELDS_GROUP_USER, $fields, $argsAfterSaveFields);
        $this->updateUserRegistrationInfo($data,$user);

        $this->triggerFirstLoginActionsAndLoginUser($user->id, $user->password_clear);
    }
    
    private function triggerFirstLoginActionsAndLoginUser($userId, $passwordClear)
    {

        $my = Foundry::user($userId);
        $my->password_clear = $passwordClear;

        $points = Foundry::points();
        $points->assign('user.registration', 'com_easysocial', $my->id);

        $badge = Foundry::badges();
        $badge->log('com_easysocial', 'registration.create', $my->id, JText::_('COM_EASYSOCIAL_REGISTRATION_BADGE_REGISTERED'));

        // Add activity logging when a uer registers on the site.
        $config = Foundry::config();
        if ($config->get('registrations.stream.create')) {
            $stream = Foundry::stream();
            $streamTemplate = $stream->getTemplate();
            $streamTemplate->setActor($my->id, SOCIAL_TYPE_USER);
            $streamTemplate->setContext($my->id, SOCIAL_TYPE_PROFILES);
            $streamTemplate->setVerb('register');
            $streamTemplate->setSiteWide();
            $streamTemplate->setPublicStream('core.view');
            $stream->add($streamTemplate);
        }

        if (JFactory::getApplication()->login(array('username' => $my->username, 'password' => $my->password_clear))) {
            $this->addToStreamOnLoggedIn();
        }
    }
    
    private function checkIsLoggedAndIfTrueRaiseError()
    {
        $app = JFactory::getApplication();
        if (JFactory::getUser()->id) {
            $app->enqueueMessage(JText::_('COM_PLOT_ERROR'), 'error');
            $app->redirect(JRoute::_('index.php?option=com_plot&view=river', false));
        }
    }
    
    private function validateRegistrationInputAndRedirectIfErrors()
    {
        $app = JFactory::getApplication();
        $validationErrors = $this->validateRegistrationInputAndGetErrors();
        if ($validationErrors) {
            foreach ($validationErrors as $error) {
                $app->enqueueMessage($error, 'error');
            }
            $app->redirect(JRoute::_('index.php?option=com_plot&view=river', false));
        }
    }

    private function validateRegistrationInputAndGetErrors()
    {
        Foundry::import('fields:/user/joomla_username/helper');
        Foundry::import('fields:/user/joomla_email/helper');
        $validationErrors = array();
        if ((trim(JRequest::getVar('name', '')) == '')) {
            $validationErrors[] = JText::_('COM_PLOT_PLEASE_ENTER_NAME');
        }
        if ((trim(JRequest::getVar('surname', '')) == '')) {
            $validationErrors[] = JText::_('COM_PLOT_PLEASE_ENTER_SURNAME');
        }
        if (SocialFieldsUserJoomlaUsernameHelper::exists(JRequest::getVar('email', '')) || (trim(JRequest::getVar('email', '')) == '')) {
            $validationErrors[] = JText::_('COM_PLOT_WRONG_EMAIL');
        }
        
        $params = new Joomla\Registry\Registry();
        if (SocialFieldsUserJoomlaEmailHelper::exists(JRequest::getVar('email', '')) ||
                SocialFieldsUserJoomlaEmailHelper::isDisallowed(JRequest::getVar('email', ''), $params) ||
                !SocialFieldsUserJoomlaEmailHelper::isValid(JRequest::getVar('email', ''))) {
            $validationErrors[] = JText::_('COM_PLOT_WRONG_EMAIL');
        }
        if ((trim(JRequest::getVar('password', '')) == '') || (JRequest::getVar('password', '') !== JRequest::getVar('password-confirm', ''))) {
            $validationErrors[] = JText::_('COM_PLOT_WRONG_PASSWORD');
        }
        
        return $validationErrors;
    }

    public function logout()
    {
        $app = JFactory::getApplication();
        $app->logout();
        $app->redirect(JRoute::_('index.php?option=com_plot&view=river', false));
    }
    
    public function login()
    {
        $this->checkForm();
        $app = JFactory::getApplication();
        if (!$app->login(array('username' => JRequest::getVar('login', ''), 'password' => JRequest::getVar('password', '')))) {

            # login failed
        } else {
            $this->addToStreamOnLoggedIn();
            $app->redirect(JRoute::_('index.php?option=com_plot&view=profile&id='.plotUser::factory()->id, false));
        }
        $app->redirect(JRoute::_('index.php?option=com_plot&view=river', false));
    }
    
    private function addToStreamOnLoggedIn()
    {
        $my         = plotUser::factory();
        $stream     = Foundry::stream();
        $template   = $stream->getTemplate();

        $template->setActor( $my->id , 'user' );
        $template->setVerb( 'logged-in' );
        $template->setTitle( '' );
        $template->setTarget( $my->id );
        $template->setContext( $my->id , 'logged-in' );
        
        $stream->add( $template );        
        
        return true;
    }

    public function ajaxShowUserAgreement()
    {
        $registrationView = $this->getView('registration', 'raw');
        $registrationView->setLayout('user.agreement');
        $registrationView->set('k2Item', PlotHelper::getk2item(plotGlobalConfig::getVar('footerLinkK2idUsersAgreement')));
        $registrationView->display();
    }
    
    public function ajaxShowPasswordRestore()
    {
        $registrationView = $this->getView('registration', 'html');
        $registrationView->setLayout('password.restore');
        $registrationView->display();
        die;        
    }
    
    public function restorePassword()
    {
        $email = JRequest::getVar('email');
        $userId = PlotHelper::getUserIdByEmail( $email );
        if (!$userId) {
            $this->redirectWithMessage('index.php?option=com_plot&view=river', 'COM_PLOT_NO_USER_WITH_CURRENT_EMAIL_CHECK_EMAIL');
        }
        
        $newPassword = $this->changeUserPasswordToRandom($userId);
        $isEmailSent = PlotHelper::sendEmailFromAdmin(
            $email, JText::sprintf('COM_PLOT_PASSWORD_CHANGING', JFactory::getConfig()->get('sitename')), JText::sprintf('COM_PLOT_PASSWORD_CHANGED_YOUR_NEW_PASSWORD_IS', $newPassword)
        );
        if (!$isEmailSent) {
            $this->redirectWithMessage('index.php?option=com_plot&view=river', 'COM_PLOT_SEND_EMAIL_ERROR');
        }
        $user=plotUser::factory($userId);
        $user->saveNewPassword($newPassword);
        $this->redirectWithMessage('index.php?option=com_plot&view=river', 'COM_PLOT_PASSWORD_CHANGED_AND_SENT_TO_EMAIL');
    }
    
    private function changeUserPasswordToRandom($userId)
    {
        jimport('joomla.user.helper');
        $user = plotUser::factory($userId);
        $salt = JUserHelper::genRandomPassword(32);
        $newPassword = JUserHelper::genRandomPassword();
        $crypt = JUserHelper::getCryptedPassword($newPassword, $salt);
        $newPasswordEncrypted = $crypt.':'.$salt;
        $user->password = $newPasswordEncrypted;
        $user->save(true);
        return $newPassword;
    }

    private function updateUserRegistrationInfo($data,$user){

        $db = JFactory::getDbo();
        if($data['first']){

            $fieldData = new stdClass();
            $fieldData->field_id = 22;
            $fieldData->uid = $user->id;
            $fieldData->type = 'user';
            $fieldData->data = $data['first'];
            $fieldData->datakey='first';
            $fieldData->params = '';
            $fieldData->raw = $data['first'];
            $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');

        }
        if($data['last']){
            $fieldData = new stdClass();
            $fieldData->field_id = 22;
            $fieldData->uid = $user->id;
            $fieldData->type = 'user';
            $fieldData->data = $data['last'];
            $fieldData->datakey='last';
            $fieldData->params = '';
            $fieldData->raw = $data['last'];

            $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
        }
        if($data['last'] && $data['first']){
            $fieldData = new stdClass();
            $fieldData->field_id = 42;
            $fieldData->uid = $user->id;
            $fieldData->type = 'user';
            $fieldData->data =$data['first'].' '. $data['last'];
            $fieldData->datakey='name';
            $fieldData->params = '';
            $fieldData->raw = $data['first'].' '. $data['last'];

            $result = $db->insertObject('#__social_fields_data', $fieldData, 'id');
        }
       return;
    }

    private function isUsernameExist($username, $email){
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('id')
            ->from('#__users')
            ->where('username=' . $db->quote($username));
        $result = $db->setQuery($query)->loadResult();
        if(!$result){
            JFactory::getApplication()->setUserState( 'first', 1 );
        }
        return;
    }

}

