<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class PlotControllerAboutme extends JControllerLegacy
{

    public function addChild()
    {
        $app = JFactory::getApplication();
        
        $my = plotUser::factory();
        $childId = JRequest::getVar('child_id', 0);

        if ($my->addChild($childId)) {
            $data['status']=1;
            header('Content-Type: application/json');
            echo json_encode($data);
            die;
        } else {
            $data['status']=0;
            header('Content-Type: application/json');
            echo json_encode($data);
            die;
            die;
        }
    }

    public function removeChild(){
        $my = plotUser::factory();
        $childId = JRequest::getVar('id', 0);
        $data=array();
        if ($my->removeChild($childId)) {
            $data['status']=1;
            header('Content-Type: application/json');
            echo json_encode($data);
            die;

        } else {
           $data['status']=0;
            header('Content-Type: application/json');
            echo json_encode($data);
            die;
        }
    }

}
