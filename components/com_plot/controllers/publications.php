<?php
defined('_JEXEC') or die('Restricted access');

class PlotControllerPublications extends JControllerLegacy
{

    public function ajaxGetFilteredAndSortedBooks()
    {
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');
            $self=plotGlobalConfig::getVar('booksParentResultsOnSelf');
        }else{
            $self=plotGlobalConfig::getVar('booksChildResultsOnSelf');
        }
        $booksModel = $this->getModel('publications');
        $view = $this->getView('publications', 'raw');
        $my = plotUser::factory();

        $agesIds = ($ageId = JRequest::getInt('ageId')) ? array($ageId) : '';
        $tagsIds = JRequest::getVar('tagsIds', array(0));
        $isOnlyMyCourses = JRequest::getVar('myCoursesOnly', false);
        $filter = array();
        $filter[] = array('field' => 'published', 'value' => 1);

        $categoryId = JRequest::getInt('categoryId', 0);

        $order = JRequest::getVar('sort', '`pub`.c_title ASC');
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');
            $limitCount = plotGlobalConfig::getVar('booksResultsShowFirstCountParent');
        } else {
            $limitCount = plotGlobalConfig::getVar('booksResultsShowFirstCountChild');
        }

        $offset = JRequest::getInt('offset');
        $limit = array('offset' => 0, 'limit' => $limitCount);

        $booksData = $booksModel->getPublications($categoryId, $order, $agesIds, $tagsIds, $limit, $isOnlyMyCourses);

        $view->books = $booksData['items'];
        $view->isOnlyMyCourses = $isOnlyMyCourses;
        $view->booksGroups = array();

        foreach (array_keys($view->books) AS $i) {
            if ($i % $self == 0) {
                $view->booksGroups[] = array_slice($view->books, $i, $self);
            }
        }
        $view->setLayout('books.list');

        $data = array();
        $data['renderedBooks'] = $view->ajaxRenderBooksList();
        $data['countBooks'] = $booksData['countItems'];

        echo json_encode($data);
        die;
    }

    public function ajaxLoadMore()
    {
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');
            $self=plotGlobalConfig::getVar('booksParentResultsOnSelf');
            $limitCount = plotGlobalConfig::getVar('booksResultsShowFirstCountParent');
        } else {
            $limitCount = plotGlobalConfig::getVar('booksResultsShowFirstCountChild');
            $self=plotGlobalConfig::getVar('booksChildResultsOnSelf');
        }

        $offset = JRequest::getInt('offset');
        $publicationsModel = $this->getModel('publications');
        $view = $this->getView('publications', 'raw');
        $my = plotUser::factory();

        $userData = JRequest::getVar('userData');
        $agesIds = (isset($userData['ageId']) && $userData['ageId']) ? array($userData['ageId']) : '';
        $tagsIds = isset($userData['tagsIds']) ? $userData['tagsIds'] : array(0);
        $isOnlyMyCourses = isset($userData['myCoursesOnly']) ? $userData['myCoursesOnly'] : false;

        $filter = array();
        $filter[] = array('field' => 'published', 'value' => 1);

        $categoryId = $userData['categoryId'];

        if ($categoryId) {
            $filter[] = array('field' => 'cat_id', 'value' => $categoryId);
        }

        $order = isset($userData['sort']) ? $userData['sort'] : '`pub`.c_title ASC';
        $limit = array('offset' => $offset, 'limit' => $limitCount);

        $bookData = $publicationsModel->getPublications($categoryId, $order, $agesIds, $tagsIds, $limit, $isOnlyMyCourses);

        $view->books = $bookData['items'];
        $view->isOnlyMyCourses = $isOnlyMyCourses;
        
        if (empty($bookData['items'])) {
            die;
        }
        $view->booksGroups = array();
        foreach (array_keys($view->books) AS $i) {
            if ($i % $self == 0) {
                $view->booksGroups[] = array_slice($view->books, $i, $self);
            }
        }
        # divide courses to groups by 3 courses in each group (for template)

        $view->setLayout('books.list');
        echo $view->ajaxRenderBooksList();

        die;
    }

    public function ajaxGetMyTags()
    {
        $mytags = plotUser::factory()->getTags();
        echo json_encode($mytags);
        die;
    }

}
