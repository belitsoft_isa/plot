<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');
require_once(JPATH_ROOT . '/administrator/components/com_easysocial/includes/foundry.php');

class PlotControllerEvent extends JControllerLegacy
{

    public function pickoutImage()
    {
        $eventsModel = $this->getModel('events');
        $event = $eventsModel->getEvent( JRequest::getInt('id', 0) );
        PlotHelper::echoNewImageForSocials( JUri::root().'images/com_plot/events/'.$event->user_id.'/thumb/'.$event->img, JRequest::getInt('canv_width'), JRequest::getInt('canv_height') );
        die;
    }    
    
}
