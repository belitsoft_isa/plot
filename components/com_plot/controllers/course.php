<?php

defined('_JEXEC') or die('Restricted access');

class PlotControllerCourse extends PlotController
{
    public function pickoutImage()
    {
        $courseModel = $this->getModel('course');
        $course = $courseModel->getCourse(JRequest::getInt('id', 0));
        PlotHelper::echoNewImageForSocials(JUri::root() . $course->image, JRequest::getInt('canv_width'), JRequest::getInt('canv_height'));
        die;
    }

    public function createSmallCourse()
    {
        $my = plotUser::factory();
        $videoFilePath = JPATH_ROOT . '/media/com_plot/videos/' . $my->id . '/' . JRequest::getVar('course-videofile-id');
        if (JRequest::getVar('add-uploaded-file') && !JFile::exists($videoFilePath)) {
            $this->redirectWithMessage('index.php?option=com_plot&view=profile&id=' . plotUser::factory()->id, 'COM_PLOT_NEED_UPLOAD_CORRECT_VIDEO_FILE_OR_ADD_YOUTUBE_LINK');
        }

        $newCourseId = PlotHelperJLMS::createBlankCourse(
            array(
                'title' => JRequest::getString('title'),
                'description' => JRequest::getVar('video-description') ? JRequest::getVar('video-description') : JRequest::getVar('uploaded-video-description'),
                'categoryId' => JRequest::getInt('catogoryId')
            )
        );
        $learningPathId = PlotHelperJLMS::createLearningPath($newCourseId);

        if (JRequest::getVar('add-uploaded-file')) {
            $documentId = PlotHelperJLMS::createDocumentWithUploadedVideo($newCourseId, $videoFilePath, JRequest::getVar('uploaded-video-title'));
            PlotHelperJLMS::addDocumentToLearningPath($documentId, $learningPathId);
        } else {
            PlotHelperJLMS::createContentStepWithVideoLinkFromYoutube($learningPathId, JRequest::getString('video-link'), JRequest::getVar('video-title'));
        }

        $newQuizId = PlotHelperJLMS::createQuizWithThreeQuestions($newCourseId);
        PlotHelperJLMS::addQuizStepToLearningPath($newQuizId, $learningPathId);
        PlotHelperJLMS::sendEmailAboutNewCourseToAdmin($newCourseId);

        $this->saveCourseToPlotTables($newCourseId);

        if ($newCourseId) {
            $this->addUsersToJoomlaLMSCourse(array(plotUser::factory()->id), $newCourseId);
            $this->markCoursesForUsersAsPaid(array(plotUser::factory()->id), $newCourseId, 0);
        }

        $this->redirectWithMessage('index.php?option=com_plot&view=profile&id=' . plotUser::factory()->id, 'COM_PLOT_COURSE_SUCCESFULLY_ADDED_PLEASE_WAIT_FOR_ADMIN_APPROVAL');
    }

    private function saveCourseToPlotTables($courseId)
    {
        $courseImage = JRequest::getVar('video-img') ? JRequest::getVar('video-img') : 'templates/plot/img/default_course.jpg';
        if (strpos($courseImage, JUri::base()) !== false) {
            $courseImage = substr($courseImage, strlen(JUri::base()));
        }

        $course = new plotCourse();
        $course->id = $courseId;
        $course->admin_min_cost = 0;
        $course->admin_cost = 0;
        $course->admin_max_cost = 0;
        $course->author_min_cost = plotGlobalConfig::getVar('coursesSmallDefaultAuthorMinCostAbs');
        $course->author_cost = plotGlobalConfig::getVar('coursesSmallDefaultAuthorPercent');
        $course->author_max_cost = plotGlobalConfig::getVar('coursesSmallDefaultAuthorMaxCostAbs');
        $course->min_cost = 0;
        $course->count_points = plotGlobalConfig::getVar('coursesSmallDefaultFirsCount');
        $course->tags = JRequest::getVar('new_course_tags');
        $course->ages = JRequest::getVar('new_course_ages');
        $course->image = $courseImage;
        $course->small=1;
        $course->save();

        return true;
    }

    private function addUsersToJoomlaLMSCourse($userIds, $courseId)
    {
        $this->requireJoomlaLMSFiles();

        $_JLMS_PLUGINS = &JLMSFactory::getPlugins();
        $JLMS_CONFIG = JLMSFactory::getConfig();
        $db = JFactory::getDBO();

        $msg = '';
        $group_id = 0;

        $role_id = plotGlobalConfig::getVar('joomlaLMSRoleStudentId');

        $p_s = 0;
        $d_s = '0000-00-00';
        $p_e = 0;
        $d_e = '0000-00-00';

        if ($userIds && $courseId) {
            global $license_lms_users;
            foreach ($userIds as $user_id) {
                $do_add = false;

                $query = "SELECT count(*) FROM #__lms_users_in_groups WHERE course_id = '" . $courseId . "' AND user_id = '" . $user_id . "'";
                $db->SetQuery($query);
                $c = $db->LoadResult();

                $query = "SELECT count(*) FROM #__lms_user_courses WHERE course_id = '" . $courseId . "' AND user_id = '" . $user_id . "'";
                $db->SetQuery($query)->loadResult();
                if ((int)$c == 0) {
                    if ($license_lms_users) {
                        $query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
                        $db->SetQuery($query);
                        $total_students = $db->LoadResult();
                        if (intval($total_students) < intval($license_lms_users)) {
                            $do_add = true;
                        }
                        if (!$do_add) {
                            $query = "SELECT count(*) FROM #__lms_users_in_groups WHERE user_id = '" . $user_id . "'";
                            $db->SetQuery($query);
                            if ($db->LoadResult()) {
                                $do_add = true;
                            }
                        }
                    } else {
                        $do_add = true;
                    }
                    if ($do_add) {
                        $query = "INSERT INTO #__lms_users_in_groups"
                            . "\n (course_id, group_id, user_id, role_id, teacher_comment, publish_start, start_date, publish_end, end_date, enrol_time)"
                            . "\n VALUES"
                            . "\n ('" . $courseId . "', '" . $group_id . "', '" . $user_id . "', '" . $role_id . "', '', $p_s, '" . $d_s . "', $p_e, '" . $d_e . "', '" . JLMS_gmdate() . "')";
                        $db->SetQuery($query);
                        if ($db->query()) {
                            //send email to import user						
                            $course = new stdClass();
                            $course->course_alias = '';
                            $course->course_name = '';

                            $query = "SELECT course_name, name_alias FROM #__lms_courses WHERE id = '" . $courseId . "'";
                            $db->setQuery($query);
                            $course = $db->loadObject();

                            $user_group = new stdClass();
                            $user_group->ug_name = '';

                            $query = "SELECT ug_name  FROM #__lms_usergroups WHERE id = '" . $group_id . "'";
                            $db->setQuery($query);
                            $user_group = $db->loadObject();

                            $params['user_id'] = $user_id;
                            $params['course_id'] = $courseId;

                            $user = new stdClass();
                            $user->email = '';
                            $user->name = '';
                            $user->username = '';

                            $query = "SELECT email, name, username FROM #__users WHERE id = '" . $user_id . "'";
                            $db->setQuery($query);
                            $user = $db->loadObject();

                            $params['markers']['{email}'] = $user->email;
                            $params['markers']['{name}'] = $user->name;
                            $params['markers']['{username}'] = $user->username;
                            $params['markers']['{coursename}'] = $course->course_name;//( $course->course_alias )?$course->course_alias:$course->course_name;

                            if ($user_group)
                                $params['markers']['{groupname}'] = $user_group->ug_name;

                            $Itemid = $JLMS_CONFIG->getItemid();
                            $params['markers']['{courselink}'] = JURI::root() . "index.php?option=com_joomla_lms&Itemid=$Itemid&task=details_course&id=$courseId";

                            $params['action_name'] = 'OnEnrolmentInCourseByJoomlaAdmin';

                            $_JLMS_PLUGINS->loadBotGroup('emails');
                            $_JLMS_PLUGINS->loadBotGroup('system');
                            $params['course_ids'] = array($courseId);
                            $plugin_result_array = $_JLMS_PLUGINS->trigger('OnEnrolmentInCourseByJoomlaAdmin', array(& $params));
                        }

                        $user_info = new stdClass();
                        $user_info->user_id = $user_id;
                        $user_info->group_id = $group_id;
                        $user_info->course_id = $courseId;
                        $_JLMS_PLUGINS->loadBotGroup('user');
                        $_JLMS_PLUGINS->trigger('onCourseJoin', array($user_info));
                    } else {
                        $msg = _JLMS_USERS_MSG_USR_L_EXCEEDED;
                    }
                }
            }
        }

        return true;
    }

    private function requireJoomlaLMSFiles()
    {
        if (!defined('_JOOMLMS_FRONT_HOME')) {
            define('_JOOMLMS_FRONT_HOME', JPATH_SITE . '/components/com_joomla_lms');
        }

        require_once(JPATH_ROOT . "/components/com_joomla_lms/joomla_lms.main.php");
        require_once(_JOOMLMS_FRONT_HOME . "/includes/component.legacy.php");
        require_once(_JOOMLMS_FRONT_HOME . "/includes/classes/lms.factory.php");
    }

    private function markCoursesForUsersAsPaid($choosedUsersIds, $courseId, $price)
    {
        $courseModel = $this->getModel('course');
        $my = plotUser::factory();

        foreach ($choosedUsersIds AS $userId) {
            $paidCourse = new stdClass();
            $paidCourse->parent_id = $my->id;
            $paidCourse->child_id = $userId;
            $paidCourse->course_id = $courseId;
            $paidCourse->paid_date = JFactory::getDate()->toSql();
            $paidCourse->finished_price = $price;
            $paidCourse->finished = 0;
            $courseModel->insertPaidCourseIfNotExist($paidCourse);
        }
        return true;
    }

    public function calculateCosts()
    {
        $course = new plotCourse(JRequest::getVar('courseId', 0));
        $countChilds = JRequest::getVar('countChilds', 0);

        $costs = $course->getBuyerCosts(JRequest::getVar('enteredAmount', 0));

        $costs['admin'] = $costs['admin'] * $countChilds;
        $costs['author'] = $costs['author'] * $countChilds;
        $costs['paymentSystem'] = $costs['paymentSystem'] * $countChilds;
        $costs['total'] = $costs['total'] * $countChilds;
        $data = array(
            'costs' => $costs
        );
        echo json_encode($data);
        die;
    }

    public function createOrder()
    {
        $courseId = JRequest::getInt('courseId', 0);
        $amount = JRequest::getInt('amount', 0);
        $usersIdsEncoded = JRequest::getVar('usersIds', '');

        $course = new plotCourse($courseId);
        $costs = $course->getBuyerCosts($amount);
        $countChilds = count(json_decode($usersIdsEncoded));
        $costs['total'] = $costs['total'] * $countChilds;

        $my = plotUser::factory();
        $db = JFactory::getDbo();
        $orderObj = new stdClass();
        $orderObj->payerId = $my->id;
        $orderObj->entity = 'course';
        $orderObj->entityId = $courseId;
        $orderObj->usersIds = $usersIdsEncoded;
        $orderObj->amount = $costs['total'];
        $orderObj->created = JFactory::getDate()->toSql();
        $orderObj->status = 'created';
        $db->insertObject('#__plot_goods_orders', $orderObj);

        $orderId = $db->insertid();

        $data = array();
        $data['orderId'] = $orderId;

        echo json_encode($data);
        die;
    }

    public function ajaxGetBeforeCourse()
    {
        $userData = JRequest::getVar('userData');
        $data = array();

        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');
        }
        $booksModel = $this->getModel('course');
        $view = $this->getView('course', 'raw');
        $bookId = (isset($userData['bookId'])) ? $userData['bookId'] : 0;

        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');

            $limitCount = plotGlobalConfig::getVar('coursesResultsShowFirstCountParent');
        } else {
            $limitCount = plotGlobalConfig::getVar('coursesResultsShowFirstCount');
        }

        $offset = JRequest::getInt('offset');;
        $limit = array('offset' => $offset, 'limit' => $limitCount);

        $booksData = $booksModel->getBeforeHashtagsCourses($limit, $bookId);

        $view->courses = $booksData['items'];
        $view->countitems=$booksData['countItems'];
        $view->coursesGroups = array();

        foreach (array_keys($view->courses) AS $i) {
            if ($i % 3 == 0) {
                $view->coursesGroups[] = array_slice($view->courses, $i, 3);
            }
        }

        $view->setLayout('courses.list');


        $data['renderedBooks'] = $view->ajaxRenderList();
       // $data['renderedBooks'] .= "<script>jPlotUp.Arrow.initialize('positionCameras');</script>";
// echo json_encode($data);
        echo $data['renderedBooks'];
        die;
    }

    public function ajaxGetAfterCourse()
    {
        $userData = JRequest::getVar('userData');


        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');
        }
        $courseModel = $this->getModel('course');
        $view = $this->getView('course', 'raw');
        $bookId = (isset($userData['bookId'])) ? $userData['bookId'] : 0;

        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');

            $limitCount = plotGlobalConfig::getVar('coursesResultsShowFirstCountParent');
        } else {
            $limitCount = plotGlobalConfig::getVar('coursesResultsShowFirstCount');
        }

        $offset = JRequest::getInt('offset');

        $limit = array('offset' => $offset, 'limit' => $limitCount);

        $coursesData = $courseModel->getAfterHashtagsCourses($limit, $bookId);

        $view->courses = $coursesData['items'];
        $view->countitems=$coursesData['countItems'];
        $view->coursesGroups = array();

        foreach (array_keys($view->courses) AS $i) {
            if ($i % 3 == 0) {
                $view->coursesGroups[] = array_slice($view->courses, $i, 3);
            }
        }

        $view->setLayout('courses.list');


        $data['renderedBooks'] = $view->ajaxRenderList();
        // $data['renderedBooks'] .= "<script>jPlotUp.Arrow.initialize('positionCameras');</script>";
// echo json_encode($data);
        echo $data['renderedBooks'];
        die;
    }

    public function uploadVideoImg()
    {
        $youtubeNumber = JRequest::getString('youtubeNumber');

        $thumbnailUrl = 'http://img.youtube.com/vi/' . $youtubeNumber . '/hqdefault.jpg';
        $pathToUserVideoFolder = JPATH_SITE . '/media/com_plot/coursevideo/' . Foundry::user()->id;

        if (file_exists($pathToUserVideoFolder . '/' . $youtubeNumber . '.jpg')) {
            unlink($pathToUserVideoFolder . '/' . $youtubeNumber . '.jpg');
        }
        if (!file_exists(JPATH_SITE . '/media/com_plot/coursevideo')) {
            mkdir(JPATH_SITE . '/media/com_plot/coursevideo');
        }
        if (!file_exists($pathToUserVideoFolder)) {
            mkdir($pathToUserVideoFolder);
        }

        $data = array();
        header('Content-Type: application/json');
        if (!@copy($thumbnailUrl, $pathToUserVideoFolder . '/' . $youtubeNumber . '.jpg')) {
            $data['status'] = 0;
            $data['message'] = 'Файл не найден';
            echo json_encode($data);
            die;
        }
        list($width, $height) = getimagesize($pathToUserVideoFolder . '/' . $youtubeNumber . '.jpg');

        if ((int)plotGlobalConfig::getVar('videoCropWidthMin') > (int)$width || (int)plotGlobalConfig::getVar('videoCropHeightMin') > $height) {
            $data['status'] = 0;
            $data['message'] = 'Минимальный размер картинки должен быть ' . plotGlobalConfig::getVar('videoCropWidthMin') . 'x' . plotGlobalConfig::getVar('videoCropHeightMin');
            echo json_encode($data);
            die;
        }

        $data['status'] = 1;
        $data['message'] = 'ok';
        echo json_encode($data);
        die;
    }

    public function ajaxVideoImage()
    {
        $profileEditView = $this->getView('course', 'raw');
        $profileEditView->setLayout('ajax_videolink_image');
        $originalPhotoUrl = JUri::root() . 'media/com_plot/coursevideo/' . Foundry::user()->id . '/' . JRequest::getString('img_name') . '.jpg';
        $profileEditView->set('originalPhotoUrl', $originalPhotoUrl);
        $profileEditView->set('photoUrl', JRequest::getString('img_name'));
        list($width, $height) = getimagesize($originalPhotoUrl);
        $profileEditView->set('img_width', $width);
        $profileEditView->set('img_height', $height);
        $profileEditView->display();
        die;
    }


    public function ajaxSaveCroppedVideoFileImage()
    {
        Foundry::requireLogin();
        require_once JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/image/image.php';
        $formData = array();
        parse_str(JRequest::getVar('data'), $formData);
        $imgWidth = $formData['img-width'];
        $imgHeight = $formData['img-height'];
        $width = $formData['w'] / $imgWidth;
        $height = $formData['h'] / $imgHeight;
        $left = $formData['x'] / $imgWidth;
        $top = $formData['y'] / $imgHeight;

        if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'coursevideo' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'thumb')) {
            mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'coursevideo' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'thumb');
        }
        $image = new SocialImage();
        $image = $image->load(JPATH_BASE . '/media/com_plot/coursevideo/' . Foundry::user()->id . '/original/' . $formData['img_src']);

        $imageWidth = $image->getWidth();
        $imageHeight = $image->getHeight();
        if (!is_null($top) && !is_null($left) && !is_null($width) && !is_null($height)) {
            $actualX = $imageWidth * $left;
            $actualY = $imageHeight * $top;
            $actualWidth = $imageWidth * $width;
            $actualHeight = $imageHeight * $height;

            // Now we'll need to crop the image
            $image->crop($actualX, $actualY, $actualWidth, $actualHeight);
        } else {
            if ($imageWidth > $imageHeight) {
                $x = ($imageWidth - $imageHeight) / 2;
                $y = 0;
                $image->crop($x, $y, $imageHeight, $imageHeight);
            } else {
                $x = 0;
                $y = ($imageHeight - $imageWidth) / 2;
                $image->crop($x, $y, $imageWidth, $imageWidth);
            }
        }

        $tmpImagePath = JPATH_BASE . '/media/com_plot/coursevideo/' . Foundry::user()->id . '/thumb/' . $formData['img_src'];
        $image->resize(plotGlobalConfig::getVar('courseCropWidthMin'), plotGlobalConfig::getVar('courseCropHeightMin'));

        if (JFile::exists($tmpImagePath)) {
            jfile::delete($tmpImagePath);
        }
        @$image->save($tmpImagePath);

        // Unset the image to free up some memory
        unset($image);

        // Reload the image again to get the correct resource pointing to the cropped image.
        $image = Foundry::image();
        $image->load($tmpImagePath);
        $data = array();
        $data['img'] = JUri::root() . 'media/com_plot/coursevideo/' . Foundry::user()->id . '/thumb/' . $formData['img_src'];
        echo json_encode($data);
        die;
    }

    public function ajaxSaveCroppedVideo()
    {
        Foundry::requireLogin();
        require_once JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/image/image.php';
        $formData = array();
        parse_str(JRequest::getVar('data'), $formData);
        $imgWidth = $formData['img-width'];
        $imgHeight = $formData['img-height'];
        $width = $formData['w'] / $imgWidth;
        $height = $formData['h'] / $imgHeight;
        $left = $formData['x'] / $imgWidth;
        $top = $formData['y'] / $imgHeight;
        if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'coursevideo' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'thumb')) {
            mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'coursevideo' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'thumb');
        }
        $imageEmpty = new SocialImage();
        $image = $imageEmpty->load(JPATH_BASE . '/media/com_plot/coursevideo/' . Foundry::user()->id . '/' . $formData['img_src'] . '.jpg');

        $imageWidth = $image->getWidth();
        $imageHeight = $image->getHeight();
        if (!is_null($top) && !is_null($left) && !is_null($width) && !is_null($height)) {
            $actualX = $imageWidth * $left;
            $actualY = $imageHeight * $top;
            $actualWidth = $imageWidth * $width;
            $actualHeight = $imageHeight * $height;
            // Now we'll need to crop the image
            $image->crop($actualX, $actualY, $actualWidth, $actualHeight);
        } else {
            if ($imageWidth > $imageHeight) {
                $x = ($imageWidth - $imageHeight) / 2;
                $y = 0;
                $image->crop($x, $y, $imageHeight, $imageHeight);
            } else {
                $x = 0;
                $y = ($imageHeight - $imageWidth) / 2;
                $image->crop($x, $y, $imageWidth, $imageWidth);
            }
        }
        if (file_exists(JPATH_BASE . '/media/com_plot/coursevideo/' . Foundry::user()->id . '/thumb/' . $formData['img_src'] . '.jpg')) {
            unlink(JPATH_BASE . '/media/com_plot/coursevideo/' . Foundry::user()->id . '/thumb/' . $formData['img_src'] . '.jpg');
        }
        $tmpImagePath = JPATH_BASE . '/media/com_plot/coursevideo/' . Foundry::user()->id . '/thumb/' . $formData['img_src'] . '.jpg';
        $image->resize(plotGlobalConfig::getVar('courseCropWidthMin'), plotGlobalConfig::getVar('courseCropHeightMin'));
        $image->save($tmpImagePath);

        // Unset the image to free up some memory
        unset($image);

        $data = array('img' => JUri::root() . 'media/com_plot/coursevideo/' . Foundry::user()->id . '/thumb/' . $formData['img_src'] . '.jpg');
        echo json_encode($data);
        die;
    }


    public function saveCourseVideo()
    {
        $my=plotUser::factory();
        $data = array();
        $data['message']='';
        $jinput = JFactory::getApplication()->input;
        $link = $jinput->get('video_link', '', 'STRING');
        $video_title = $jinput->get('video_title', '', 'STRING');
        $video_description = $jinput->get('video_desc', '', 'STRING');
        $courseId = $jinput->get('course_id', '', 'STRING');
        $model = $this->getModel('Course');
        $user_data=array();
        $user_data['link']=$link;
        $user_data['title']= $video_title;
        $user_data['desc']= $video_description;
        $user_data['type']= 'link';

        if(!$my->id){
            $data['message']=JText::_('COM_PLOT_LOGIN_PLEASE');
        }
        if(!$video_title || !$video_description || !$courseId){
            $data['message']=JText::_('COM_PLOT_VIDEO_LINK_WAS_NOT_SAVED');
        }
        if (strpos($link, 'youtube.com') !== false || strpos($link, 'youtu.be') !== false) {

            $model->saveCourseVideo($courseId, $user_data);

            $data['status']=1;
            $data['message']=JText::_('COM_PLOT_VIDEO_LINK_WAS_SAVED');

            echo json_encode($data);
            die;
        } else {

            $data['status']=0;
            $data['message']=JText::_('COM_PLOT_VIDEO_LINK_SHOULD_BY_VALID_LINK_HAS_NOT_BEEN_SAVED');
            echo json_encode($data);
            die;
        }

        $data['status']=0;
        echo json_encode($data);
        die;

    }

    public function ajaxVideosLoadMore(){

        $userData = JRequest::getVar('userData');

        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');
        }
        $courseId=(int)$userData['courseId'];

        $essayModel = $this->getModel('course');
        $view = $this->getView('course', 'raw');

        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');

            $limitCount = plotGlobalConfig::getVar('childEssayCount');
        } else {
            $limitCount = plotGlobalConfig::getVar('parentEssayCount');
        }

        $offset = JRequest::getInt('offset');
        $limit = array('offset' => $offset, 'limit' => $limitCount);

        $videosData = $essayModel->getCourses($limit,$courseId);

        $view->videos = $videosData;
        $view->countItems = $videosData['countItems'];


        $view->setLayout('video.list');

        $data = array();


        $data['renderedBooks'] = $view->ajaxRenderList();
        if(!(int)$view->countItems){
            $data['renderedBooks'] .= "<script>noCoursesFind();</script>";
        }

// echo json_encode($data);
        echo $data['renderedBooks'];
        die;

    }

    public function ajaxCourse()
    {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        $user = plotUser::factory();
        if ($user->isParent()) {
            $this->addViewPath(JPATH_COMPONENT . '/views_parent');
        }
        $videoModel = $this->getModel('course');
        $video = $videoModel->getCourseVideoById($id);

        if (!$video) {
            $video = new stdClass();
        }
        if (strripos($video->link, 'youtube.com/') !== FALSE) {
            $youtubeNumber = substr($video->link, strripos($video->link, '?v=') + 3);
            if (($pos = strpos($youtubeNumber, '&')) !== FALSE) {
                $youtubeNumber = substr($youtubeNumber, 0, $pos);
            }
        }
        $videoView = $this->getView('course', 'raw');
        $videoView->setLayout('video');
        $videoView->set('video', $video);
        $videoView->set('youtubeNumber', $youtubeNumber);
        $videoView->display();
        die;
    }


    public function approve()
    {

        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        $msg = $jinput->get('msg', JText::_("COM_PLOT_COURSE_WAS_APPROVE"), 'STRING');
        $courseModel = $this->getModel('course');
        $user = plotUser::factory();
        if ($user->isParent()) {
            $this->addViewPath(JPATH_COMPONENT . '/views_parent');
        }

        $essay = $courseModel->getCourseVideoById($id);
        $my=plotUser::factory($essay->user_id);
        $mailer = JFactory::getMailer();
        $config = JFactory::getConfig();
        $sender = array(
            $config->get( 'mailfrom' ),
            $config->get( 'fromname' )
        );
        $conversationsModel=JModelLegacy::getInstance('conversations', 'PlotModel');
        //проверить начисление денег
       if(!$this->isAlreadyPaidForCourse($id)){

           $finishedPrice = $my->getFinishedPriceForMyCourse($essay->course_id);
           $my->addMoney($finishedPrice, $my->id);
           plotPoints::assign( 'course.finish' , 'com_plot' , $my->id, $essay->course_id);

           if(plotUser::factory($essay->reviewer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($essay->reviewer)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
               $conversationsModel->sendMessageSystem($essay->reviewer, 'plot-money-course-get-' . $essay->course_id . '-' . (int)$my->id);
           }
           if($my->getSocialFieldData('UNSUBSCRIBE')==1 || $my->getSocialFieldData('UNSUBSCRIBE')==NULL) {
               $conversationsModel->sendMessageSystem($my->id, 'plot-money-course-get-' . $essay->course_id . '-' . $my->id);
           }
           $courseObj=new plotCourse($essay->course_id);
           $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $my->id) . '">' . $my->name . '</a>';
           $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=course&id=' . $essay->course_id) . '">' . $courseObj->course_name . '</a>';

               $mailer->setSender($sender);
           if(plotUser::factory($essay->reviewer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($essay->reviewer)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
               $mailer->addRecipient(plotUser::factory($essay->reviewer)->email);
           }
           if($my->getSocialFieldData('UNSUBSCRIBE')==1 || $my->getSocialFieldData('UNSUBSCRIBE')==NULL) {
               $mailer->addRecipient($my->email);
           }
               $mailer->setSubject(JText::_('COM_PLOT_SEND_MONEY_GET_SUBJECT'));
               $mailer->isHTML(true);
               $mailer->setBody(JText::sprintf("COM_PLOT_SEND_MONEY_COURSE_GET", $actor, $target));
           if(plotUser::factory($essay->reviewer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($essay->reviewer)->getSocialFieldData('UNSUBSCRIBE')==NULL || $my->getSocialFieldData('UNSUBSCRIBE')==1 || $my->getSocialFieldData('UNSUBSCRIBE')==NULL) {
               $mailer->Send();
           }
           $conversationsModel->sendMessageSystem($essay->reviewer, 'plot-course-finished-' . $essay->course_id.'-'.$my->id);
           if($my->getSocialFieldData('UNSUBSCRIBE')==1 || $my->getSocialFieldData('UNSUBSCRIBE')==NULL) {
               $mailer->setSender($sender);
               $mailer->addRecipient($my->email);
               $mailer->setSubject(JText::_('COM_PLOT_SEND_COURSE_SUBJECT'));
               $mailer->isHTML(true);
               $mailer->setBody(JText::sprintf("COM_PLOT_SEND_COURSE_FINISHED", $actor, $target));
               $mailer->Send();
           }
          //  $this->updatePointsHistory($essay->book_id);
        }
        $courseModel->courseVideoUpdateStatus($id);

        $message = $conversationsModel->sendMessageSystem($essay->user_id, $msg);

        $courseModel->generateCertificate($essay->user_id, $essay->course_id);
        $bookObj=new plotCourse($essay->course_id);
        $programs=$bookObj->getCourseProgramsIds();
        if($programs){

            foreach($programs AS $prog){
                $program= new plotProgram($prog);
                if($program->isProgramFinished($essay->user_id)){
                    $conversationModel = JModelLegacy::getInstance('conversations', 'PlotModel');
                    if(plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                        $conversationModel->sendMessageSystem($essay->user_id, 'plot-program-finished-' . $prog . '-' . $essay->user_id);
                    }
                    $group = FD::group($prog);
                    $actor='<a href="'.JRoute::_('index.php?option=com_plot&view=profile&id='.$my->id).'">'.plotUser::factory($my->id)->name.'</a>';
                    $target='<a href="'.FRoute::groups(array('layout' => 'item', 'id' => $group->getAlias()), false).'?plot=1">'.$program->title.'</a>';
                    if(plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                        $mailer->setSender($sender);
                        $mailer->addRecipient(plotUser::factory($essay->user_id)->email);
                        $mailer->setSubject(JText::_('COM_PLOT_PROGRAM_FINISHED'));
                        $mailer->isHTML(true);
                        $mailer->setBody(JText::sprintf("COM_PLOT_MSG_PROGRAM_FINISHED", $actor, $target));
                        $mailer->Send();
                    }
                }
            }
        }

        header('Content-Type: application/json');
        echo json_encode($message);
        die;
    }

    public function reject()
    {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        $user = plotUser::factory();
        if ($user->isParent()) {
            $this->addViewPath(JPATH_COMPONENT . '/views_parent');
        }
        $mailer = JFactory::getMailer();
        $config = JFactory::getConfig();
        $sender = array(
            $config->get( 'mailfrom' ),
            $config->get( 'fromname' )
        );
        $msg = $jinput->get('msg', JText::_("COM_PLOT_VIDEO_WAS_REJECT"), 'STRING');
        $videoModel = $this->getModel('course');
        $essay = $videoModel->getCourseVideoById($id);
        $conversationsModel=JModelLegacy::getInstance('conversations', 'PlotModel');
        $actot='<a href="'.JRoute::_("index.php?option=com_plot&view=profile&id=".(int)$essay->reviewer).'">'.plotUser::factory($essay->reviewer)->name.'</a>';
        $m=JText::sprintf('COM_PLOT_MSG_COURSE_WAS_REJECT',$actot, $msg);
        if(plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($essay->user_id)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
            $message = $conversationsModel->sendMessageSystem($essay->user_id, $m);
            $mailer->setSender($sender);
            $mailer->addRecipient(plotUser::factory($essay->user_id)->email);
            $mailer->setSubject('Курс отклонен');
            $mailer->isHTML(true);
            $mailer->setBody(JText::sprintf('COM_PLOT_MSG_COURSE_WAS_REJECT',$actot, $msg));
            $mailer->Send();

        }
        $message=$message?$message:$m;
        header('Content-Type: application/json');
       echo json_encode($message);
        die;
    }
    public function isAlreadyPaidForCourse($courseId){
        $finishedBooks = plotUser::factory()->getFinishedCourse($courseId);

        if ($finishedBooks) {
            return true;
        }
        return false;
    }

    public function replaceText()
    {
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');

        $courseModel = $this->getModel('course');
        $course = $courseModel->replaceForConversation($ids);


        echo json_encode($course);
        exit();
    }


    public function videoFileUpload()
    {
        $my = Foundry::user();
        JFolder::create(JPATH_SITE . "/media/com_plot/coursevideo/" . $my->id . '/original');
        $filesVideos = JRequest::getVar('qqfile', '', 'FILES');
        $allowedTypes = explode(',', plotGlobalConfig::getVar('videoTypes'));
        if ($filesVideos['type']) {
            list($type, $format) = explode('/', $filesVideos['type']);
        } else {
            list($type, $format) = array('', '');
        }

        if ($type != 'video') {
            $data['status'] = 0;
            $data['message'] = JText::_('COM_PLOT_SOME_VIDEO_FILES_COULD_BE_CORRUPTED_CHECK');
            echo json_encode($data);
            die;
        }

        if (!in_array($format, $allowedTypes)) {
            $data['status'] = 0;
            $data['message'] = JText::_('COM_PLOT_SOME_VIDEOS_HAVE_NOT_ALLOWED_FORMAT');
            $data['message'] .= ' ' . JText::sprintf('COM_PLOT_ALLOWED_VIDEO_FORMATS', implode(', ', explode(',', plotGlobalConfig::getVar('videoTypes'))));
            echo json_encode($data);
            die;
        }

        if ($filesVideos['size'] > (int)plotGlobalConfig::getVar('maxVideoSize') * 1024 * 1024) {
            $data['status'] = 0;
            $data['message'] = JText::_('COM_PLOT_SOME_VIDEOS_HAVE_NOT_ALLOWED_FILESIZE');
            echo json_encode($data);
            die;
        }

        require_once JPATH_COMPONENT . '/helpers/video.php';

        $fileName = md5($filesVideos["tmp_name"]) . "." . (($format == 'mpeg') ? 'mp3' : $format);
        JFile::move($filesVideos["tmp_name"], JPATH_SITE . "/media/com_plot/coursevideo/" . $my->id . "/" . $fileName);

        plotVideoHelper::convert_media(JPATH_SITE . "/media/com_plot/coursevideo/" . $my->id . "/" . $fileName, JPATH_SITE . "/media/com_plot/coursevideo/" . $my->id . "/" . pathinfo($fileName, PATHINFO_FILENAME) . '.mp4');
        if ($format != 'mp4') {
            JFile::delete(JPATH_SITE . "/media/com_plot/coursevideo/" . $my->id . "/" . $fileName);
        }
        $fileName = pathinfo($fileName, PATHINFO_FILENAME) . '.mp4';

        if (!file_exists(JPATH_SITE . "/media/com_plot/coursevideo/" . $my->id . "/" . $fileName)) {
            $data['status'] = 0;
            $data['message'] = JText::_('COM_PLOT_ERROR');
            echo json_encode($data);
            die;
        }

        chmod(JPATH_SITE . "/media/com_plot/coursevideo/" . $my->id . "/" . $fileName, 0644);
        $img = pathinfo($fileName, PATHINFO_FILENAME) . '.jpg';
        plotVideoHelper::createVideoFileThumb(JPATH_SITE . "/media/com_plot/coursevideo/" . $my->id . "/" . $fileName, JPATH_SITE . "/media/com_plot/coursevideo/" . $my->id . "/original/" . $img);
        $data['status'] = 1;
        $data['name'] = $fileName;
        echo json_encode($data);
        die;
    }

    public function ajaxVideofileImage()
    {
        $profileEditView = $this->getView('course', 'raw');
        $profileEditView->setLayout('ajax_videofile_image');
        $originalPhotoUrl = JUri::root() . '/media/com_plot/coursevideo/' . Foundry::user()->id . '/original/' . pathinfo(JRequest::getString('img_name'), PATHINFO_FILENAME) . '.jpg';
        $profileEditView->set('originalPhotoUrl', $originalPhotoUrl);
        $profileEditView->set('photoUrl', pathinfo(JRequest::getString('img_name'), PATHINFO_FILENAME) . '.jpg');
        $profileEditView->display();
        die;
    }

    public function videoFileSave()
    {
        Foundry::checkToken();

        // Only registered users should be allowed to upload photos
        Foundry::requireLogin();

        $app = JFactory::getApplication();

        $my = plotUser::factory();
        $db = Foundry::db();
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        $video_title = $jinput->get('video-title', '', 'STRING');
        $video_description = $jinput->get('video-description', '', 'STRING');
        $videofile_name = $jinput->get('videofile-id', '', 'STRING');
        $course_id = $jinput->get('course_id', 0, 'INT');
        $model = $this->getModel('Course');
        $user_data=array();
        $user_data['link']=$videofile_name;
        $user_data['title']= $video_title;
        $user_data['desc']= $video_description;
        $user_data['type']= 'file';
        $model->saveCourseVideo($course_id, $user_data);

        $app->enqueueMessage(JText::_('COM_PLOT_VIDEO_WAS_SAVED'));
        if ($my->id) {
            $app->redirect(JRoute::_("index.php?option=com_plot&view=course&id=" . $course_id, false, -1));
        } else {
            $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
        }
    }

    public function videoupload()
    {


        // Only registered users should be allowed to upload photos
        Foundry::requireLogin();

        $app = JFactory::getApplication();
        $jRequest = JRequest::get();
        $my = plotUser::factory();
        $db = Foundry::db();
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        $video_type = $jinput->get('add-video', '');
        $video_title = $jinput->get('video-title', '', 'STRING');
        $video_description = $jinput->get('video-description', '', 'STRING');


            //save vidio file
            if (!JFolder::exists(JPATH_SITE . "/media/com_plot")) {

                JFolder::create(JPATH_SITE . "/media/com_plot");

            }
            if (!JFolder::exists(JPATH_SITE . "/media/com_plot/videos")) {

                JFolder::create(JPATH_SITE . "/media/com_plot/videos");

            }
            if (!JFolder::exists(JPATH_SITE . "/media/com_plot/videos/" . $my->id)) {

                JFolder::create(JPATH_SITE . "/media/com_plot/videos/" . $my->id);

            }

            $filesVideos = JRequest::getVar('video', array(), 'files');
            $allowedTypes = explode(',', plotGlobalConfig::getVar('videoTypes'));
            if ($filesVideos['type']) list($type, $format) = explode('/', $filesVideos['type']);
            else list($type, $format) = array('', '');
            if ($type == 'video') {
                if (in_array($format, $allowedTypes)) {
                    if ($filesVideos['size'] <= (int)plotGlobalConfig::getVar('maxVideoSize') * 1024 * 1024) {
                        $fileName = md5($filesVideos["tmp_name"]) . "." . (($format == 'mpeg') ? 'mp3' : $format);

                        JFile::move($filesVideos["tmp_name"], JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/" . $fileName);

                        chmod(JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/" . $fileName, 0644);

                        $video = new stdClass();
                        $video->path = $fileName;
                        $video->type = 'file';
                        $video->uid = $my->id;
                        $video->title = $video_title;
                        $video->description = $video_description;
                        $video->date = Foundry::date()->toMySQL();
                        $video->status = 1;
                        $db->insertObject('#__plot_video', $video);
                        plotPoints::assign('add.video', 'com_plot', plotUser::factory()->id, $db->insertid());

                        $app->enqueueMessage(JText::_('COM_PLOT_VIDEO_WAS_SAVED'));
                        if ($my->id) {
                            $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
                        } else {
                            $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
                        }
                    } else {
                        $app->enqueueMessage(JText::_('COM_PLOT_SOME_VIDEOS_HAVE_NOT_ALLOWED_FILESIZE'));
                        if ($my->id) {
                            $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
                        } else {
                            $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
                        }
                    }
                } else {
                    $app->enqueueMessage(JText::_('COM_PLOT_SOME_VIDEOS_HAVE_NOT_ALLOWED_FORMAT'));
                    if ($my->id) {
                        $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
                    } else {
                        $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
                    }
                }
            } else {
                $app->enqueueMessage(JText::_('COM_PLOT_SOME_VIDEO_FILES_COULD_BE_CORRUPTED_CHECK'));
                if ($my->id) {
                    $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
                } else {
                    $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
                }
            }


    }

    public function openVideoFile()
    {
        $profileEditView = $this->getView('course', 'raw');
        $profileEditView->setLayout('ajax_open_videofile');
        $video = PlotHelper::getVideoById(JRequest::getInt('videoId'));
        $profileEditView->set('video', $video);
        $profileEditView->display();
        die;
    }

    public function courseFinishedReplaceText()
    {
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');

        $courseModel = $this->getModel('Course');
        $course = $courseModel->courseReplaceText($ids, "COM_PLOT_SEND_COURSE_FINISHED");

        echo json_encode($course);
        exit();
    }

    public function coursePassedReplaceText()
    {
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');

        $courseModel = $this->getModel('Course');
        $course = $courseModel->courseReplaceText($ids, "COM_PLOT_SEND_COURSE_PASSED");

        echo json_encode($course);
        exit();
    }

    public function courseMoneyReplaceText()
    {
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');

        $courseModel = $this->getModel('Course');
        $course = $courseModel->courseReplaceText($ids, "COM_PLOT_SEND_MONEY_COURSE_GET");

        echo json_encode($course);
        exit();
    }

    public function courseBuyReplaceText()
    {
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');

        $courseModel = $this->getModel('Course');
        $course = $courseModel->courseReplaceText($ids, "COM_PLOT_BUY_COURSE_EMAIL");

        echo json_encode($course);
        exit();
    }

}
