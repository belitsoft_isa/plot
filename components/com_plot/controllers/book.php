<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');
require_once(JPATH_ROOT.'/administrator/components/com_easysocial/includes/foundry.php');

class PlotControllerBook extends JControllerLegacy
{

    public function buy()
    {
        $app = JFactory::getApplication();

        $choosedUsers = array_unique(array_merge(JRequest::getVar('choosed-childs', array()), JRequest::getVar('choosed-friends', array())));
        $courseId = JRequest::getVar('course-id', 0);
        $price = JRequest::getVar('amount-for-each-child', 0);

        $this->markCoursesForUsersAsPaid($choosedUsers, $courseId, $price);
        $this->addUsersToBooks($choosedUsers, $courseId);
        $app->enqueueMessage('Поздравляем! Покупка книги произведена успешно!');
        $app->redirect(JRoute::_('index.php?option=com_plot&view=publication&bookId='.(int) $courseId, false));
    }

    private function markCoursesForUsersAsPaid($choosedUsersIds, $courseId, $price)
    {
        $courseModel = $this->getModel('book');
        $my = plotUser::factory();
        $conversationModel = JModelLegacy::getInstance('conversations', 'PlotModel');

        foreach ($choosedUsersIds AS $userId) {
            $paidCourse = new stdClass();
            $paidCourse->parent_id = $my->id;
            $paidCourse->child_id = $userId;
            $paidCourse->book_id = $courseId;
            $paidCourse->paid_date = JFactory::getDate()->toSql();
            $paidCourse->finished_price = $price;
            $paidCourse->finished = 0;
            $res=$courseModel->insertPaidCourseIfNotExist($paidCourse);
            if($res){
                if(plotUser::factory($userId)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($userId)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                    $conversationModel->sendMessageSystem($userId, 'plot-book-buy-' . $courseId);
                }
                $bookObk = new plotBook((int)$courseId);
                $book = '<a href="' . JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $courseId) . '">' . $bookObk->c_title . '</a>';
                $user = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $userId) . '">' . plotUser::factory($userId)->name . '</a>';
                $mailer = JFactory::getMailer();
                $config = JFactory::getConfig();
                $sender = array(
                    $config->get( 'mailfrom' ),
                    $config->get( 'fromname' )
                );
                $mailer->setSender($sender);
                if(plotUser::factory($userId)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($userId)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                    $mailer->addRecipient(plotUser::factory($userId)->email);
                }

                $mailer->setSubject(JText::_('COM_PLOT_SEND_MONEY_GET_SUBJECT'));
                $mailer->isHTML(true);
                $mailer->setBody(JText::sprintf("COM_PLOT_MSG_BOOK_BUY", $book,$user));
                if(plotUser::factory($userId)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($userId)->getSocialFieldData('UNSUBSCRIBE')==NULL ) {
                    $mailer->Send();
                }
            }
        }
        return true;
    }

    public function calculateCosts()
    {
        $course = new plotBook(JRequest::getVar('bookId', 0));
        $countChilds = JRequest::getVar('countChilds', 0);

        $costs = $course->getBuyerCosts(JRequest::getVar('enteredAmount', 0));

        $costs['admin'] = $costs['admin'] * $countChilds;
        $costs['author'] = $costs['author'] * $countChilds;
        $costs['paymentSystem'] = $costs['paymentSystem'] * $countChilds;
        $costs['total'] = $costs['total'] * $countChilds;
        $data = array(
            'costs' => $costs
        );
        echo json_encode($data);
        die;
    }

    private function addUsersToBooks($choosedUsers, $courseId)
    {
        $db = JFactory::getDBO();
        foreach ($choosedUsers AS $user) {
            $bookToDb = new stdClass();
            $bookToDb->parent_id = Foundry::user()->id;
            $bookToDb->child_id = (int) $user;
            $bookToDb->book_id = (int) $courseId;
            $bookToDb->read = -1;
            $db->insertObject('#__plot_books', $bookToDb);
        }
    }

    public function createOrder()
    {
        $courseId = JRequest::getInt('bookId', 0);
        $amount = JRequest::getInt('amount', 0);
        $usersIdsEncoded = JRequest::getVar('usersIds', '');

        $course = new plotBook($courseId);
        $costs = $course->getBuyerCosts($amount);
        $countChilds = count(json_decode($usersIdsEncoded));
        $costs['total'] = $costs['total'] * $countChilds;

        $my = plotUser::factory();
        $db = JFactory::getDbo();
        $orderObj = new stdClass();
        $orderObj->payerId = $my->id;
        $orderObj->entity = 'book';
        $orderObj->entityId = $courseId;
        $orderObj->usersIds = $usersIdsEncoded;
        $orderObj->amount = $costs['total'];
        $orderObj->created = JFactory::getDate()->toSql();
        $orderObj->status = 'created';
        $db->insertObject('#__plot_goods_orders', $orderObj);
        $orderId = $db->insertid();
        $data = array();
        $data['orderId'] = $orderId;
        echo json_encode($data);
        die;
    }

}
