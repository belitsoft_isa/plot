<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class PlotControllerSearch extends JControllerLegacy
{

    public function ajaxSearch()
    {
        $data = array();
        $jinput = JFactory::getApplication()->input;
        $model = $this->getModel('Search');
        $search = trim($jinput->get('search', '', 'STRING'));

        $data['books'] = $model->getLimitedBooks($search);
        $data['books_link'] = '<a  href="' . JRoute::_('index.php?option=com_plot&view=search&books=1&search=' . $search) . '">' . JText::_("COM_PLOT_SEARCH_MORE_RESULTS") . '</a>' . 'Книги ';
        $data['adults'] = $model->getLimitedAdult($search);
        $data['adults_link'] = '<a c href="' . JRoute::_('index.php?option=com_plot&view=search&adults=1&search=' . $search) . '">' . JText::_("COM_PLOT_SEARCH_MORE_RESULTS") . '</a>' . 'Взрослые ';
        $data['children'] = $model->getLimitedChildren($search);
        $data['children_link'] = '<a c href="' . JRoute::_('index.php?option=com_plot&view=search&children=1&search=' . $search) . '">' . JText::_("COM_PLOT_SEARCH_MORE_RESULTS") . '</a>' . 'Дети ';
        $data['courses'] = $model->getLimitedCourses($search);
        $data['courses_link'] = '<a c href="' . JRoute::_('index.php?option=com_plot&view=search&courses=1&search=' . $search) . '">' . JText::_("COM_PLOT_SEARCH_MORE_RESULTS") . '</a>' . 'Курсы ';
        $data['events'] = $model->getLimitedEvents($search);
        $data['events_link'] = '<a  href="' . JRoute::_('index.php?option=com_plot&view=search&events=1&search=' . $search) . '">' . JText::_("COM_PLOT_SEARCH_MORE_RESULTS") . '</a>' . 'Встречи ';
        $data['empty'] = JText::_("COM_PLOT_SEARCH_NOT_FOUND");
        $data['svg']= $_SERVER['REQUEST_URI'];

        echo json_encode($data);
        JFactory::getApplication()->close();
    }

    public function ajaxChildrenLoadMore()
    {

        $userData = JRequest::getVar('userData');
        $search = $userData['search'];
        $screenHeight = (int)$userData['screenHeight'];
        $itemHeight = (int)$userData['itemHeight'];
        $screenWidth = (int)$userData['screenWidth'];
        $itemWidth = (int)$userData['itemWidth'];
        $width = (int)($screenWidth / $itemWidth);
        $height = round($screenHeight / $itemHeight, 0);
        $limitCount = JRequest::getInt('number',0);
        $offset = JRequest::getInt('offset',0);

        $model = $this->getModel('Search');
        $limit = array('offset' => $offset, 'limit' => $limitCount);
        $childrenData = $model->getChildren($search, $limit);

        if (!$childrenData) {
            die;
        }

            echo $this->ajaxRenderUserList($childrenData, 1);


        die;
    }

    //render users for parent search
    public function ajaxRenderUserList($childrenData, $child = 0)
    {
        $str = '';
        foreach ($childrenData as $i => $item) {

            $str .= ' <li class="found-item">';
            $str .= '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $item->id) . '">';
            if ($child) {
                $str .= '<div class="child-image">';

            } else {
                $str .= '<div class="circle-img">';
            }

            if (plotUser::factory($item->id)->avatars && plotUser::factory($item->id)->avatars['large']) {
                $str .= '<img
                        src="' . JUri::root() . 'media/com_easysocial/avatars/users/' . (int)$item->id . '/' . plotUser::factory($item->id)->avatars['large'] . '">';
            } else {


                if ($child) {
                    $str .= '<img
                        src="' . JUri::root() . 'media/com_easysocial/defaults/avatars/user/child_medium.jpg' . '">';

                } else {
                    $str .= '<img
                        src="' . JUri::root() . 'media/com_easysocial/defaults/avatars/users/small.png' . '">';
                }



            }
            $str .= '</div>';
            $str .= '<div class="title">' . $item->name . '<p>' . $item->about . '</p></div>';
            $str .= '</a>';
            $str .= '</li>';

        }
        return $str;
    }

    public function ajaxAdultsLoadMore()
    {

        $userData = JRequest::getVar('userData');
        $search = $userData['search'];
        $screenHeight = (int)$userData['screenHeight'];
        $itemHeight = (int)$userData['itemHeight'];
        $screenWidth = (int)$userData['screenWidth'];
        $itemWidth = (int)$userData['itemWidth'];
        $width = (int)($screenWidth / $itemWidth);
        $height = round($screenHeight / $itemHeight, 0);
        $limitCount = JRequest::getInt('number',0);
        $offset = JRequest::getInt('offset',0);
        $model = $this->getModel('Search');
        $limit = array('offset' => $offset, 'limit' => $limitCount);
        $childrenData = $model->getAdults($search, $limit);


        if (!$childrenData) {
            die;
        }


        echo $this->ajaxRenderUserList($childrenData);
        die;
    }

    public function ajaxBooksLoadMore()
    {

        // $limitCount = plotGlobalConfig::getVar('limitSearchBooksItems');
        $userData = JRequest::getVar('userData');
        $search = $userData['search'];
        $limitCount = JRequest::getInt('number',0);
        $offset = JRequest::getInt('offset',0);

        $model = $this->getModel('Search');
        $limit = array('offset' => $offset, 'limit' => $limitCount);
        $booksData = $model->getBooks($search, $limit);


        if (!$booksData) {
            die;
        }
        if (plotUser::factory()->isParent()) {
            echo $this->ajaxRenderBooksList($booksData);
        } else {
            echo $this->ajaxRenderChildBooksList($booksData);
        }


        die;
    }

    #view for adult search book tab
    public function ajaxRenderBooksList($booksData)
    {
        $str = '';
        foreach ($booksData as $i => $item) {
            $str .= '<li class="found-item">';
            $str .= '<a href="' . JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $item->c_id) . '">';
            $str .= '<figure>';
            $str .= '<img src="' .  $item->thumbnailUrl . '">';
            $str .= '</figure>';
            $str .= '</a>';
            $str .= '<div class="elem-descr">' . $item->c_pub_descr . '<b class="price">' . $item->price . ' руб</b></div>';

            $str .= '</li>';

        }

        return $str;
    }

    #view for child search book tab
    public function ajaxRenderChildBooksList($booksData)
    {
        $str = '';
        $j = 0;
        $count = count($booksData);
        foreach ($booksData as $i => $item) {
            if ($j == 0) {
                $str .= '<div class="jcarousel-wrapper my-books child-library">';
                $str.='<ul class="no-jcarousel search-books">';
            }
            $str .= '<li class="found-item">';
            $str .= '<a href="' . JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $item->c_id) . '">';
            $str .= '<figure>';
            $str .= '<img src="' . $item->thumbnailUrl . '">';
            $str .= '</figure>';
            $str .= '</a>';
            $str .= '<div class="elem-descr">' . $item->c_pub_descr . '<b class="price">' . $item->price . ' руб</b></div>';
            $str .= '</li>';
            if ($j == 3 || $i == $count) {
                $j = 0;
                $str.='</ul>';
                $str .= '</div>';
            } else {
                $j++;
            }
        }
        return $str;
    }

    public function ajaxCoursesLoadMore()
    {

        $limitCount = plotGlobalConfig::getVar('limitSearchCoursesItems');
        $userData = JRequest::getVar('userData');
        $search = $userData['search'];
        $limitCount = JRequest::getInt('number',0);
        $offset = JRequest::getInt('offset',0);
        $model = $this->getModel('Search');
        $limit = array('offset' => $offset, 'limit' => $limitCount);
        $coursesData = $model->getCourses($search, $limit);

        if (!$coursesData) {
            die;
        }

        if (plotUser::factory()->isParent()) {
            echo $this->ajaxRenderCoursesList($coursesData);
        } else {
            echo $this->ajaxRenderChildCoursesList($coursesData);
        }

        die;
    }

    #view for adult search courses tab
    public function ajaxRenderCoursesList($coursesData)
    {
        $str = '';
        foreach ($coursesData as $i => $item) {

            $str .= ' <li class="parent-course">';
            $str .= '<a href="' . JRoute::_('index.php?option=com_plot&view=course&id=' . $item->id) . '">';
            $str .= '<h6>' . $item->course_name . '</h6><hr/>';
            $str .= '<span>' . PlotHelper::russian_date(JHtml::date($item->start_date, 'd.m.Y')) . '</span>';
            $str .= '<div class="circle-img">';
            $str .= '<svg class="clip-svg-course">';
            $str .= '<image style="clip-path: url(#clipping-circle-course);" width="100%" height="100%" xlink:href="'.$item->img.'" alt=""/>';
            $str .= '</svg>';
            $str .= '<i class="activity-icons">';
            $str .= '<svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);">';
            $str .= '<use xlink:href="#academic-hat"></use>';
            $str .= '</svg>';
            $str .= '</i>';
            $str .= '</div>';
            $str .= '<p>' . $item->course_description . '</p>';
            $str .= '</a>';
            $str .= '</li>';

        }

        return $str;
    }

#view for child search courses tab
    public function ajaxRenderChildCoursesList($coursesData)
    {
        $str = '';
        $count = count($coursesData);
        $j=0;
        foreach ($coursesData as $i => $item) {
            if ($j == 0) {
                $str .= '<div class="jcarousel-wrapper my-courses">';
                $str.='<ul class="parent-profile no-jcarousel search-courses">';
            }
            $str .= ' <li class="found-item">';
            $str .= '<a href="' . JRoute::_('index.php?option=com_plot&view=course&id=' . $item->id) . '">';
            $str .= '<figure>';
            $str .= '<img src="'.$item->img.'" />';
            $str .= '<figcaption>'.$item->course_name.'</figcaption>';
            $str .= '</figure>';
            $str .= '</a>';
            $str .= '</li>';
            if ($j == 2 || $i == $count) {
                $j = 0;
                $str.='</ul>';
                $str .= '</div>';
            } else {
                $j++;
            }
        }

        return $str;
    }
    public function ajaxEventsLoadMore()
    {

        if(plotUser::factory()->isParent()){
            $limitCount = plotGlobalConfig::getVar('limitSearchMeetingsItems');
        }else{
            $limitCount = plotGlobalConfig::getVar('limitSearchMeetingsChildrenItems');
        }

        $userData = JRequest::getVar('userData');
        $search = $userData['search'];

        $limitCount = JRequest::getInt('number',0);
        $offset = JRequest::getInt('offset',0);
        $model = $this->getModel('Search');
        $limit = array('offset' => $offset, 'limit' => $limitCount);
        $eventsData = $model->getEvents($search, $limit);


        if (!$eventsData) {
            die;
        }

        if (plotUser::factory()->isParent()) {
            echo $this->ajaxRenderEventsList($eventsData);
        } else {
            echo $this->ajaxRenderChildEventsList($eventsData);
        }


        die;
    }
    #view for adult search events tab
    public function ajaxRenderEventsList($eventsData)
    {
        $str = '';
        foreach ($eventsData as $i => $item) {
            $str .= '<li class="parent-meetings" id="plot-event-id-' . $item->id . '">';
            $str .= '<a href="' . JRoute::_('index.php?option=com_plot&view=event&id=' . $item->id) . '">';
            $str .= '<h6>' . $item->title_croped . '</h6>';
            $str .= '<i class="author">автор:  ';
            $str .= (strlen(plotUser::factory($item->user_id)->name) > 80) ? substr(plotUser::factory($item->user_id)->name, 0, 80) . '...' : plotUser::factory($item->user_id)->name;

            $str .= '</i>';
            if(PlotHelper::is_date($item->start_date)){
            $str .= '<span>' . PlotHelper::russian_date(JHtml::date($item->start_date, 'd.m.Y')) . '</span>';
            }
            $str .= '<span class="quantity">' . PlotHelper::countEventSubscription($item->id);
            $str .= ' <svg viewBox="0 0 23 23" preserveAspectRatio="xMinYMin meet">';
            $str .= ' <use xlink:href="#people"></use>';
            $str .= '</svg>';
            $str .= '</span>';
            $str .= '<div class="counter">';
            $str .= '<p class="start-time">';
            $str .= ' <b>Время начала</b>';
            if(PlotHelper::is_date($item->start_date)){
            $str .= '<time>' . JHtml::date($item->start_date, 'H:m') . '</time>';
            }
            $str .= '</p>';

            if(PlotHelper::is_date($item->start_date) && PlotHelper::is_date($item->end_date)){
            if(PlotHelper::compareDates(JHtml::date($item->start_date, 'Y-m-d'))==-1){
                $str .= '<p class="duration"><b>Длительность</b>';
                $str .= '<time>' . PlotHelper::diffDates(JHtml::date($item->start_date, 'Y-m-d H:m'),JHtml::date($item->end_date, 'Y-m-d H:m')) . '</time></p>';
            }else if(PlotHelper::compareDates(JHtml::date($item->start_date, 'Y-m-d'))==1){
                $str .= '<p class="before-start"><b>До начала:</b>';
                $str .= '<time>' . PlotHelper::downcounter($item->start_date) . '</time></p>';
            }
            else if(PlotHelper::compareDates(JHtml::date($item->start_date, 'Y-m-d'))==0){

                $str .= '<p class="before-end"><b>До окончания:</b>';
                $str .= '<time>' . PlotHelper::downcounter($item->start_date) . '</time></p>';
            }
            }
            $str .= '</div>';
            $str .= '<div class="address">Место проведения:';
            $str .= ' <address>' . $item->place . '</address>';
            $str .= ' </div>';
            $str .= '<div class="circle-img">';
            $str .= '<svg class="clip-svg">';
            $str .= '<image style="clip-path: url(#clipping-circle);" width="100%" height="100%" xlink:href="' .  $item->img . '" alt="" />';
            $str .= '</svg>';
            $str .= '<i class="activity-icons">';
            $str .= '<svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);">';
            $str .= '<use xlink:href="#lamp-meeting"></use>';
            $str .= '</svg>';
            $str .= '</i>';
            $str .= '</div>';
            $str .= '<p>' . $item->description . '</p>';
            $str .= '</a>';
            $str .= '<div id="wrap-btn-event-' . $item->id . '">';
            if ((int)PlotHelper::chechEventSubscription($item->id)) {
                if ((int)PlotHelper::isOwnerEvent($item->id)) {

                    $str .= '<button class="add hover-shadow" onclick="App.Event.DeleteEvent(this); return false;"
                data-id="' . $item->id . '">' . JText::_('COM_PLOT_REMOVE_EVENT') . '</button>';

                } else {
                    $str .= '<button class="add hover-shadow" onclick="App.Event.RemoveSubscribe(this); return false;"
                data-id="' . $item->id . '">' . JText::_('COM_PLOT_REMOVE_SUBSCRIBE_EVENT') . '</button>';

                }

            } else {
                $str .= '<button class="add hover-shadow" onclick="App.Event.Subscribe(this);return false;"
        data-id="' . $item->id . '">' . JText::_('COM_PLOT_SUBSCRIBE_ON_EVENT') . '</button>';

            }
            $str .= '</div>';
            $str .= '</li>';

        }

        return $str;
    }

    #view for child search events tab
    public function ajaxRenderChildEventsList($eventsData)
    {
        $str = '';
        $j=0;
        $count=count($eventsData);

        foreach ($eventsData as $i => $item) {
            if ($j == 0) {
                $str .= '<div class="jcarousel-wrapper my-thinking">';
                $str.='<ul class="child-profile no-jcarousel search-meetings">';
            }
            $str .= '<li j="'.$j.'" i="'.$i.'" class="found-item" id="plot-event-id-' . $item->id . '">';
           $str .= '<a href="' . JRoute::_('index.php?option=com_plot&view=event&id=' . $item->id) . '">';
            $str .= '<figure>';
            $str .= '<img src="' . $item->img . '" />';
            $str .= '<figcaption>'.$item->title_croped.'</figcaption>';
            $str .= '</figure>';
            $str .= '<div class="elem-descr">';
            $str .= '<i>Дата проведения:</i>';
            if(PlotHelper::is_date($item->start_date)){
                $str .= '<span>'.PlotHelper::russian_date(JHtml::date($item->start_date, 'd.m.Y')).'</span>';
            }else{
                $str .= '<span></span>';
            }
           // $str .= '<span>'.PlotHelper::russian_date(JHtml::date($item->start_date, 'd.m.Y')).'</span>';
            $str .= '<div class="counter">';
            $str .= '<p class="start-time">';
            $str .= '<b>Время начала:</b>';
            if(PlotHelper::is_date($item->start_date)){
           $str .= '<time>' . JHtml::date($item->start_date, 'H:m') . '</time>';
            }
            $str .= '</p>';
            if(PlotHelper::is_date($item->start_date) && PlotHelper::is_date($item->end_date)){
            if(PlotHelper::compareDates(JHtml::date($item->start_date, 'Y-m-d'))==-1){
                $str .= '<p class="duration"><b>Длительность</b>';
                $str .= '<time>' . PlotHelper::diffDates(JHtml::date($item->start_date, 'Y-m-d H:m'),JHtml::date($item->end_date, 'Y-m-d H:m')) . '</time></p>';
            }else if(PlotHelper::compareDates(JHtml::date($item->start_date, 'Y-m-d'))==1){
                $str .= '<p class="before-start"><b>До начала:</b>';
                $str .= '<time>' . PlotHelper::downcounter($item->start_date) . '</time></p>';
            }
            else if(PlotHelper::compareDates(JHtml::date($item->start_date, 'Y-m-d'))==0){
                    $str .= '<p class="before-end"><b>До окончания:</b>';
                    $str .= '<time>' . PlotHelper::downcounter($item->end_date) . '</time></p>';
            }
            }

            $str .= '</p>';
            $str .= '</div>';
            $str .= '<span class="quantity">'. PlotHelper::countEventSubscription($item->id);
            $str .= ' <svg viewBox="0 0 23 23" preserveAspectRatio="xMinYMin meet">';
            $str .= '<use xlink:href="#people"></use>';
            $str .= '</svg>';
            $str .= '</span>';
            $str .= '</div>';
            $str .= '</a>';
            $str .= '</li>';



            if ($j == 2 || $i == $count) {
                $j = 0;
                $str.='</ul>';
                $str .= '</div>';
            } else {
                $j++;
            }
        }

        return $str;

    }
}
