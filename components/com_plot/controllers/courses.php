<?php
defined('_JEXEC') or die('Restricted access');

class PlotControllerCourses extends JControllerLegacy
{
    
    public function ajaxGetFilteredAndSortedCourses()
    {
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');
        }
        
        $coursesModel = $this->getModel('courses');
        $view = $this->getView('courses', 'raw');
        $my = plotUser::factory();
        
        $agesIds = ($ageId = JRequest::getInt('ageId')) ? array($ageId) : '';
        $tagsIds = JRequest::getVar('tagsIds', array(0));
        $isOnlyMyCourses = JRequest::getVar('myCoursesOnly', false);

        $filter = array();
        $filter[] = array('field'=>'published','value'=>1);
        
        $categoryId = JRequest::getVar('categoryId', 0);
        if ($categoryId) {
            $filter[] = array('field' => 'cat_id', 'value' => $categoryId);
        }
        if ($isOnlyMyCourses) {
            $myCoursesIds = $my->getCoursesIdsBoughtForMe();
            $myCoursesIds[] = 0;
            $filter[] = array('field' => 'id', 'table' => 'c', 'type' => 'IN', 'value' => $myCoursesIds);
        }
        
        $order = JRequest::getVar('sort', '`total_min_cost` ASC');
        
        $limit = array('offset' => 0, 'limit' => JRequest::getVar('limit', plotGlobalConfig::getVar('coursesResultsShowFirstCount')));
        $coursesData = $coursesModel->getCourses($filter, $order, $agesIds, $tagsIds, $limit );
        $view->courses = $coursesData['items'];
        $view->isOnlyMyCourses = $isOnlyMyCourses;

        # divide courses to groups by 3 courses in each group (for template)
        $view->coursesGroups = array();
        foreach (array_keys($view->courses) AS $i) {
            if ($i % 3 == 0) {
                $view->coursesGroups[] = array_slice($view->courses, $i, 3);
            }
        }

        $view->setLayout('courses.list');
        
        $data = array();
        $data['renderedCourses'] = $view->ajaxRenderCoursesList();
        $data['countCourses'] = $coursesData['countItems'];
        echo json_encode($data);
        die;
    }
    
    public function ajaxLoadMore()
    {
        if (plotUser::factory()->isParent()) {
            $this->setPath('view', JPATH_COMPONENT.'/views_parent/');
            $limitCount = plotGlobalConfig::getVar('coursesResultsShowFirstCountParent');
        } else {
            $limitCount = plotGlobalConfig::getVar('coursesResultsShowFirstCount');
        }
        
        $offset = JRequest::getInt('offset');
        $coursesModel = $this->getModel('courses');
        $view = $this->getView('courses', 'raw');
        $my = plotUser::factory();
        
        $userData = JRequest::getVar('userData');
        $agesIds = (isset($userData['ageId']) && $userData['ageId'])  ? array($userData['ageId']) : '';
        $tagsIds = isset($userData['tagsIds']) ? $userData['tagsIds'] : array(0);
        $isOnlyMyCourses = isset($userData['myCoursesOnly']) ? $userData['myCoursesOnly'] : false;

        $filter = array();
        $filter[] = array('field'=>'published','value'=>1);
        
        $categoryId = $userData['categoryId'];
        if ($categoryId) {
            $filter[] = array('field' => 'cat_id', 'value' => $categoryId);
        }
        if ($isOnlyMyCourses) {
            $myCoursesIds = $my->getCoursesIdsBoughtForMe();
            $myCoursesIds[] = 0;
            $filter[] = array('field' => 'id', 'table' => 'c', 'type' => 'IN', 'value' => $myCoursesIds);
        }
        
        $order = isset($userData['sort']) ? $userData['sort'] : '`total_min_cost` ASC';
        $limit = array('offset' => $offset, 'limit' => $limitCount);
        $coursesData = $coursesModel->getCourses($filter, $order, $agesIds, $tagsIds, $limit );
        $view->courses = $coursesData['items'];
        $view->isOnlyMyCourses = $isOnlyMyCourses;
        
        if (!$view->courses) {
            die;
        }
        
        # divide courses to groups by 3 courses in each group (for template)
        $view->coursesGroups = array();
        foreach (array_keys($view->courses) AS $i) {
            if ($i % 3 == 0) {
                $view->coursesGroups[] = array_slice($view->courses, $i, 3);
            }
        }

        $view->setLayout('courses.list');
        echo $view->ajaxRenderCoursesList();
        die;
    }
    
}
