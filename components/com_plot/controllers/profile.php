<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');
require_once(JPATH_ROOT . '/administrator/components/com_easysocial/includes/foundry.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/conversations.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/friends.php');

class PlotControllerProfile extends PlotController
{

    public function photoupload()
    {
        JSession::checkToken() or die('Invalid Token');

        $isAvatar = false;
        Foundry::requireLogin();
        $app = JFactory::getApplication();
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        $title = $jinput->get('img_title', '', 'STRING');
        $caption = $jinput->get('img_description', '', 'STRING');
        $img_type = $jinput->get('img-type', '');
        $img_src = $jinput->get('img-src', '');

        $my = plotUser::factory();

        if (!$title && $caption) {
            $title = $caption;
        }
        $db = Foundry::db();
        if ($img_type != 'photo') {
            $query = $db->getQuery(true)
                ->select('`a`.*')
                ->from('`#__social_albums` AS `a`')
                ->where('`a`.`uid` = ' . (int)$my->id)
                ->where('`a`.`type` = "plot-certificate" ');
            $db->setQuery($query);
            $album = $db->loadObject();

            if (!$album) {
                $album = Foundry::table('Album');
                $album->uid = $my->id;
                $album->type = 'plot-certificate';
                $album->created = Foundry::date()->toMySQL();
                $album->ordering = 0;
                $album->assigned_date = Foundry::date()->toMySQL();
                $db->insertObject('#__social_albums', $album);
                $albumId = $db->insertid();
            } else {
                $albumId = $album->id;
            }
        } else {
            $query = $db->getQuery(true)
                ->select('`a`.*')
                ->from('`#__social_albums` AS `a`')
                ->where('`a`.`uid` = ' . (int)$my->id)
                ->where('`a`.`type` = "plot-photos"');
            $db->setQuery($query);
            $album = $db->loadObject();

            if (!$album) {
                $album = Foundry::table('Album');
                $album->uid = $my->id;
                $album->type = 'plot-photos';
                $album->created = Foundry::date()->toMySQL();
                $album->ordering = 0;
                $album->assigned_date = Foundry::date()->toMySQL();
                $db->insertObject('#__social_albums', $album);
                $albumId = $db->insertid();
            } else {
                $albumId = $album->id;
            }
        }
        $album = Foundry::table('Album');
        $album->load($albumId);
        $photo = Foundry::table('Photo');

        // Set the creation date alias
        $photo->assigned_date = Foundry::date()->toMySQL();
        if ($img_type != 'photo') {
            $date = $jinput->get('img-date', '');
            if ($date) {
                $date1 = date_create($date);
                $date2 = date_create("now");
                if (($date1) > ($date2)) {
                    $app->enqueueMessage(JText::_('Дата получения сертификата больше текущей'));
                    $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
                    return;
                }
                $photo->created = date('Y-m-d', strtotime($date));
            } else {
                $date = Foundry::date()->toMySQL();
                $photo->created = $date;
            }
        }

        $image = Foundry::image();
        $image->load(JPATH_SITE . '/' . 'images' . '/' . 'com_plot' . '/' . 'photos' . '/' . Foundry::user()->id . '/' . 'original' . '/' . $img_src);

        $photo->uid = $my->id;
        $photo->type = SOCIAL_TYPE_USER;
        $photo->album_id = $albumId;
        $photo->ordering = 0;
        $photo->state = SOCIAL_STATE_PUBLISHED;
        $photo->title = $title;
        $photo->caption = $caption;

        // Set the creation date alias
        $photo->assigned_date = Foundry::date()->toMySQL();

        // Let's test if exif exists
        $exif = Foundry::get('Exif');
        $photo->cleanupTitle();

        // Detect the photo caption and title if exif is available.
        if ($exif->isAvailable() && $image->hasExifSupport()) {
            // Load the image
            $exif->load(JPATH_SITE . '/' . 'images' . '/' . 'com_plot' . '/' . 'photos' . '/' . Foundry::user()->id . '/' . 'original' . '/' . $img_src);
            $createdAlias = $exif->getCreationDate();
            if ($createdAlias) {
                $photo->assigned_date = $createdAlias;
            }
            if ($title) {
                $photo->title = $title;
            }
            if ($caption) {
                $photo->caption = $caption;
            }
        }
        $state = $photo->store();

        // Push all the ordering of the photo down
        $photosModel = Foundry::model('photos');
        $photosModel->pushPhotosOrdering($albumId, $photo->id);

        // Detect location for the photo
        if ($exif->isAvailable() && $image->hasExifSupport()) {
            $exif->load(JPATH_SITE . '/' . 'images' . '/' . 'com_plot' . '/' . 'photos' . '/' . Foundry::user()->id . '/' . 'original' . '/' . $img_src);
            $locationCoordinates = $exif->getLocation();
            // Once we have the coordinates, we need to reverse geocode it to get the address.
            if ($locationCoordinates) {
                $geocode = Foundry::get('GeoCode');
                $address = $geocode->reverse($locationCoordinates->latitude, $locationCoordinates->longitude);
                $location = Foundry::table('Location');
                $location->loadByType($photo->id, SOCIAL_TYPE_PHOTO, $my->id);
                $location->address = $address;
                $location->latitude = $locationCoordinates->latitude;
                $location->longitude = $locationCoordinates->longitude;
                $location->user_id = $my->id;
                $location->type = SOCIAL_TYPE_PHOTO;
                $location->uid = $photo->id;
                $state = $location->store();
            }
            $photosModel->storeCustomMeta($photo, $exif);
        }

        // If album doesn't have a cover, set the current photo as the cover.
        if (!$album->hasCover()) {
            $album->cover_id = $photo->id;
            $album->store();
        }

        $photoLib = Foundry::get('Photos', $image);

        $storage = $photoLib->getStoragePath($album->id, $photo->id);
        $paths = $photoLib->create($storage);

        // Create metadata about the photos
        foreach ($paths as $type => $fileName) {
            $meta = Foundry::table('PhotoMeta');
            $meta->photo_id = $photo->id;
            $meta->group = SOCIAL_PHOTOS_META_PATH;
            $meta->property = $type;
            $meta->value = $storage . '/' . $fileName;
            $meta->store();
        }
        rename(
            JPATH_SITE . '/images/com_plot/photos/' . Foundry::user()->id . '/thumb/' . $img_src,
            JPATH_SITE . '/media/com_easysocial/photos/' . $album->id . '/' . $photo->id . '/' . md5(time() . $img_src) . '_thumbnail.jpg'
        );
        $query = $db->getQuery(true)
            ->update('`#__social_photos_meta`')
            ->set("`value` = '" . JPATH_SITE . '/media/com_easysocial/photos/' . $album->id . '/' . $photo->id . '/' . md5(time() . $img_src) . '_thumbnail.jpg' . "'")
            ->where('`property` ="thumbnail"')
            ->where('`photo_id`=' . $photo->id)
            ->where('`group`="path"');
        $db->setQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }
        // Synchronize Indexer
        $indexer = Foundry::get('Indexer');
        $template = $indexer->getTemplate();
        $template->setContent($photo->title, $photo->caption);

        $url = FRoute::photos(array('layout' => 'item', 'id' => $photo->getAlias()));
        $url = '/' . ltrim($url, '/');
        $url = str_replace('/administrator/', '/', $url);

        $template->setSource($photo->id, SOCIAL_INDEXER_TYPE_PHOTOS, $photo->uid, $url);
        $template->setThumbnail($photo->getSource('thumbnail'));

        $indexer->index($template);
        @unlink(JPATH_SITE . '/' . 'images' . '/' . 'com_plot' . '/' . 'photos' . '/' . Foundry::user()->id . '/' . 'thumb' . '/' . $img_src);
        @unlink(JPATH_SITE . '/' . 'images' . '/' . 'com_plot' . '/' . 'photos' . '/' . Foundry::user()->id . '/' . 'original' . '/' . $img_src);

        if ($img_type == 'photo') {
            plotPoints::assign('add.photo', 'com_plot', plotUser::factory()->id, $photo->id);
        } else {
            $date1 = date_create($date);
            $date2 = date_create("now");
            if (!$this->isCertificateDateBeforeUserRegisterDate($date) && ((int)$date1->diff($date2)->format('%a')) == 0) {
                plotPoints::assign('add.certificate', 'com_plot', plotUser::factory()->id, $photo->id);
            } elseif (!$this->isCertificateDateBeforeUserRegisterDate($date)) {
                plotPoints::assign('add.old.certificate', 'com_plot', plotUser::factory()->id, $photo->id);
            }
        }

        if ($img_type == 'photo') {
            $app->enqueueMessage(JText::_('COM_PLOT_PHOTOS_FILE_WAS ADDED'));
        } else {
            $app->enqueueMessage(JText::_('Сертификат успешно загружен'));
        }

        if ($my->id) {
            $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
        } else {
            $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
        }

    }

    private function isCertificateDateBeforeUserRegisterDate($date)
    {
        $my = plotUser::factory();
        $certDate = date_create($date);
        $userRegisteredDate = date_create($my->registerDate);
        if ($certDate->getTimestamp() < $userRegisteredDate->getTimestamp()) {
            return true;
        }
        return false;
    }

    public function ajaxSaveCroppedEvents()
    {
        Foundry::requireLogin();
        require_once JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/image/image.php';
        $formData = array();
        parse_str(JRequest::getVar('data'), $formData);
        $imgWidth = $formData['img-width'];
        $imgHeight = $formData['img-height'];
        $width = $formData['w'] / $imgWidth;
        $height = $formData['h'] / $imgHeight;
        $left = $formData['x'] / $imgWidth;
        $top = $formData['y'] / $imgHeight;
        if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'thumb')) {
            mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'thumb');
        }
        $image = new SocialImage();
        $image = $image->load(JPATH_BASE . '/images/com_plot/events/' . Foundry::user()->id . '/original/' . $formData['img_src']);

        $imageWidth = $image->getWidth();
        $imageHeight = $image->getHeight();
        if (!is_null($top) && !is_null($left) && !is_null($width) && !is_null($height)) {
            $actualX = $imageWidth * $left;
            $actualY = $imageHeight * $top;
            $actualWidth = $imageWidth * $width;
            $actualHeight = $imageHeight * $height;

            // Now we'll need to crop the image
            $image->crop($actualX, $actualY, $actualWidth, $actualHeight);
        } else {
            if ($imageWidth > $imageHeight) {
                $x = ($imageWidth - $imageHeight) / 2;
                $y = 0;
                $image->crop($x, $y, $imageHeight, $imageHeight);
            } else {
                $x = 0;
                $y = ($imageHeight - $imageWidth) / 2;
                $image->crop($x, $y, $imageWidth, $imageWidth);
            }
        }

        $tmpImagePath = JPATH_BASE . '/images/com_plot/events/' . Foundry::user()->id . '/thumb/' . $formData['img_src'];
        $image->resize(plotGlobalConfig::getVar('meetingCropWidthMin'), plotGlobalConfig::getVar('meetingCropHeightMin'));
        $image->save($tmpImagePath);

        // Unset the image to free up some memory
        unset($image);

        // Reload the image again to get the correct resource pointing to the cropped image.
        $image = Foundry::image();
        $image->load($tmpImagePath);
        $data = array();
        $data['img'] = JUri::root() . 'images/com_plot/events/' . Foundry::user()->id . '/thumb/' . $formData['img_src'];
        echo json_encode($data);
        die;
    }

    public function ajaxSaveCroppedCertificate()
    {
        Foundry::requireLogin();
        require_once JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/image/image.php';
        $my = Foundry::user();

        $formData = array();
        parse_str(JRequest::getVar('data'), $formData);

        $imgWidth = $formData['img-width'];
        $imgHeight = $formData['img-height'];
        $img_coefficient_w = $formData['img-coefficient-w'];
        $img_coefficient_h = $formData['img-coefficient-h'];
        $width = $formData['w'] / ($imgWidth);
        $height = $formData['h'] / ($imgHeight);
        $left = $formData['x'] / $imgWidth;
        $top = $formData['y'] / $imgHeight;

        $image = new SocialImage();
        $image = $image->load(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'original' . DIRECTORY_SEPARATOR . $formData['img_src']);

        $imageWidth = $image->getWidth();
        $imageHeight = $image->getHeight();
        if (!is_null($top) && !is_null($left) && !is_null($width) && !is_null($height)) {
            $actualX = $imageWidth * $left;
            $actualY = $imageHeight * $top;
            $actualWidth = $imageWidth * $width;
            $actualHeight = $imageHeight * $height;
            // Now we'll need to crop the image
            $image->crop($actualX, $actualY, $actualWidth, $actualHeight);
        } else {
            if ($imageWidth > $imageHeight) {
                $x = ($imageWidth - $imageHeight) / 2;
                $y = 0;
                $image->crop($x, $y, $imageHeight, $imageHeight);
            } else {
                $x = 0;
                $y = ($imageHeight - $imageWidth) / 2;
                $image->crop($x, $y, $imageWidth, $imageWidth);
            }
        }

        $tmpImagePath = JPATH_BASE . '/images/com_plot/photos/' . $my->id . '/thumb/' . $formData['img_src'];
        $image->resize(plotGlobalConfig::getVar('certificateCropWidthMin'), plotGlobalConfig::getVar('certificateCropHeightMin'));
        $image->save($tmpImagePath);

        // Unset the image to free up some memory
        unset($image);
        // Reload the image again to get the correct resource pointing to the cropped image.
        $image = Foundry::image();
        $image->load($tmpImagePath);
        $data['img'] = JUri::root() . 'images/com_plot/photos/' . $my->id . '/thumb/' . $formData['img_src'];
        echo json_encode($data);
        die;
    }

    public function certificatImageUpload()
    {
        $file = JRequest::getVar('qqfile', '', 'FILES');
        $data = array();
        $db = Foundry::db();
        $rEFileTypes = "/^\.(jpg|jpeg|gif|png|bmp|xcf|odg){1}$/i";
        $access = Foundry::access(Foundry::user()->id, SOCIAL_TYPE_USER);
        $maxFilesizeBytes = (int)$access->get('photos.uploader.maxsize') * 1048576;

        if (!empty($file['tmp_name'])) {
            if (($file['size']) <= $maxFilesizeBytes) {

                $image_path = JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'original';
                if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos')) {
                    mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos');
                }
                if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos' . DIRECTORY_SEPARATOR . Foundry::user()->id)) {
                    mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos' . DIRECTORY_SEPARATOR . Foundry::user()->id);
                }
                if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'original')) {
                    mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'original');
                }
                $ext = JFile::getExt($file['name']);
                $new_name = md5(time() . $file['name']) . '.' . $ext;
                if (preg_match($rEFileTypes, strrchr($new_name, '.'))) {
                    if (JFile::copy($file['tmp_name'], $image_path . '/' . $new_name)) {
                        list($width, $height) = getimagesize($image_path . '/' . $new_name);

                        if (plotGlobalConfig::getVar('photoCropWidthMin') > $width || plotGlobalConfig::getVar('photoCropHeightMin') > $height) {
                            $data['status'] = 0;
                            $data['message'] = 'Минимальный размер картинки должен быть ' . plotGlobalConfig::getVar('photoCropWidthMin') . 'x' . plotGlobalConfig::getVar('photoCropHeightMin');
                            header('Content-Type: application/json');
                            echo json_encode($data);
                            die;

                        } else {
                            $data['status'] = 1;
                            $data['name'] = $new_name;
                            header('Content-Type: application/json');
                            echo json_encode($data);
                            die;

                        }

                    } else {
                        header('Content-Type: application/json');
                        $data['status'] = 0;
                        $data['message'] = 'error';
                        echo json_encode($data);
                        die;
                    }
                } else {
                    $data['status'] = 0;
                    $data['message'] = JText::_('COM_PLOT_INVALID_IMG_FORMAT');
                    header('Content-Type: application/json');
                    echo json_encode($data);
                    die;
                }
            } else {
                $data['status'] = 0;
                $data['message'] = JText::_('COM_PLOT_INVALID_FILE SIZE');
                header('Content-Type: application/json');
                echo json_encode($data);
                die;
            }


        } else {
            $data['status'] = 0;
            $data['message'] = JText::_('COM_PLOT_INVALID_IMG');
            header('Content-Type: application/json');
            echo json_encode($data);
            die;
        }

    }

    public function photoImageUpload()
    {
        $file = JRequest::getVar('qqfile', '', 'FILES');
        $data = array();
        $db = Foundry::db();

        $rEFileTypes = "/^\.(jpg|jpeg|gif|png|bmp|xcf|odg){1}$/i";
        $access = Foundry::access(Foundry::user()->id, SOCIAL_TYPE_USER);
        $maxFilesizeBytes = (int)$access->get('photos.uploader.maxsize') * 1048576;

        if (!empty($file['tmp_name'])) {
            if (($file['size']) <= $maxFilesizeBytes) {

                $image_path = JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'original';
                if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos')) {
                    mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos');
                }
                if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos' . DIRECTORY_SEPARATOR . Foundry::user()->id)) {
                    mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos' . DIRECTORY_SEPARATOR . Foundry::user()->id);
                }
                if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'original')) {
                    mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'original');
                }
                if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'thumb')) {
                    mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'photos' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'thumb');
                }
                $ext = JFile::getExt($file['name']);
                $new_name = md5(time() . $file['name']) . '.' . $ext;
                if (preg_match($rEFileTypes, strrchr($new_name, '.'))) {
                    if (JFile::copy($file['tmp_name'], $image_path . '/' . $new_name)) {
                        list($width, $height) = getimagesize($image_path . '/' . $new_name);

                        if (plotGlobalConfig::getVar('photoCropWidthMin') > $width || plotGlobalConfig::getVar('photoCropHeightMin') > $height) {
                            $data['status'] = 0;
                            $data['success']=false;
                            $data['message'] = 'Минимальный размер картинки должен быть ' . plotGlobalConfig::getVar('photoCropWidthMin') . 'x' . plotGlobalConfig::getVar('photoCropHeightMin');
                            header('Content-Type: application/json');
                            echo json_encode($data);
                            die;

                        } else {
                            $data['status'] = 1;
                            $data['success']=true;
                            $data['name'] = $new_name;
                            header('Content-Type: application/json');
                            echo json_encode($data);
                            die;
                        }

                    } else {
                        header('Content-Type: application/json');
                        $data['status'] = 0;
                        $data['success']=false;
                        $data['message'] = 'error';
                        echo json_encode($data);
                        die;
                    }
                } else {
                    $data['status'] = 0;
                    $data['success']=false;
                    $data['message'] = JText::_('COM_PLOT_INVALID_IMG_FORMAT');
                    header('Content-Type: application/json');
                    echo json_encode($data);
                    die;
                }
            } else {
                $data['status'] = 0;
                $data['success']=false;
                $data['message'] = JText::_('COM_PLOT_INVALID_FILE SIZE');
                header('Content-Type: application/json');
                echo json_encode($data);
                die;
            }
        } else {
            $data['status'] = 0;
            $data['success']=false;
            $data['message'] = JText::_('COM_PLOT_INVALID_IMG');
            header('Content-Type: application/json');
            echo json_encode($data);
            die;
        }


        $data['status'] = 0;
            $data['success']=false;
        $data['message'] = 'invalid img';
        header('Content-Type: application/json');
        echo json_encode($data);
        die;



    }

    public function videoupload()
    {
        // Check for request forgeries
        Foundry::checkToken();

        // Only registered users should be allowed to upload photos
        Foundry::requireLogin();

        $app = JFactory::getApplication();
        $jRequest = JRequest::get();
        $my = plotUser::factory();
        $db = Foundry::db();
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        $video_type = $jinput->get('add-video', '');
        $video_title = $jinput->get('video-title', '', 'STRING');
        $video_description = $jinput->get('video-description', '', 'STRING');

        if ($video_type == 'video-link') {
            if (isset($jRequest['video-link'])) {
                $link = $jRequest['video-link'];
                if (strpos($link, 'youtube.com') !== false || strpos($link, 'youtu.be') !== false) {
                    $video = new stdClass();
                    $video->path = $link;
                    $video->type = 'link';
                    $video->uid = $my->id;
                    $video->title = $video_title;
                    $video->description = $video_description;
                    $video->date = Foundry::date()->toMySQL();
                    $video->status = 1;
                    $db->insertObject('#__plot_video', $video);
                    plotPoints::assign('add.video', 'com_plot', plotUser::factory()->id, $db->insertid());
                    $app->enqueueMessage(JText::sprintf('COM_PLOT_VIDEO_LINK_WAS_SAVED', $link));
                    if ($my->id) {
                        $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
                    } else {
                        $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
                    }
                } else {
                    $app->enqueueMessage(JText::sprintf('COM_PLOT_VIDEO_LINK_SHOULD_BY_VALID_LINK_HAS_NOT_BEEN_SAVED', $link));
                    if ($my->id) {
                        $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
                    } else {
                        $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
                    }
                }

            } else {
                $app->enqueueMessage(JText::_('COM_PLOT_VIDEO_LINK_SHOULD_BY_VALID_LINK_HAS_NOT_BEEN_SAVED'));
                if ($my->id) {
                    $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
                } else {
                    $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
                }
            }
        } else {
            //save vidio file
            if (!JFolder::exists(JPATH_SITE . "/media/com_plot")) {

                JFolder::create(JPATH_SITE . "/media/com_plot");

            }
            if (!JFolder::exists(JPATH_SITE . "/media/com_plot/videos")) {

                JFolder::create(JPATH_SITE . "/media/com_plot/videos");

            }
            if (!JFolder::exists(JPATH_SITE . "/media/com_plot/videos/" . $my->id)) {

                JFolder::create(JPATH_SITE . "/media/com_plot/videos/" . $my->id);

            }

            $filesVideos = JRequest::getVar('video', array(), 'files');
            $allowedTypes = explode(',', plotGlobalConfig::getVar('videoTypes'));
            if ($filesVideos['type']) list($type, $format) = explode('/', $filesVideos['type']);
            else list($type, $format) = array('', '');
            if ($type == 'video') {
                if (in_array($format, $allowedTypes)) {
                    if ($filesVideos['size'] <= (int)plotGlobalConfig::getVar('maxVideoSize') * 1024 * 1024) {
                        $fileName = md5($filesVideos["tmp_name"]) . "." . (($format == 'mpeg') ? 'mp3' : $format);

                        JFile::move($filesVideos["tmp_name"], JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/" . $fileName);

                        chmod(JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/" . $fileName, 0644);

                        $video = new stdClass();
                        $video->path = $fileName;
                        $video->type = 'file';
                        $video->uid = $my->id;
                        $video->title = $video_title;
                        $video->description = $video_description;
                        $video->date = Foundry::date()->toMySQL();
                        $video->status = 1;
                        $db->insertObject('#__plot_video', $video);
                        plotPoints::assign('add.video', 'com_plot', plotUser::factory()->id, $db->insertid());

                        $app->enqueueMessage(JText::_('COM_PLOT_VIDEO_WAS_SAVED'));
                        if ($my->id) {
                            $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
                        } else {
                            $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
                        }
                    } else {
                        $app->enqueueMessage(JText::_('COM_PLOT_SOME_VIDEOS_HAVE_NOT_ALLOWED_FILESIZE'));
                        if ($my->id) {
                            $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
                        } else {
                            $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
                        }
                    }
                } else {
                    $app->enqueueMessage(JText::_('COM_PLOT_SOME_VIDEOS_HAVE_NOT_ALLOWED_FORMAT'));
                    if ($my->id) {
                        $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
                    } else {
                        $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
                    }
                }
            } else {
                $app->enqueueMessage(JText::_('COM_PLOT_SOME_VIDEO_FILES_COULD_BE_CORRUPTED_CHECK'));
                if ($my->id) {
                    $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
                } else {
                    $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
                }
            }
        }

    }

    public function videoFileSave()
    {
        Foundry::checkToken();

        // Only registered users should be allowed to upload photos
        Foundry::requireLogin();

        $app = JFactory::getApplication();

        $my = plotUser::factory();
        $db = Foundry::db();
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        $video_title = $jinput->get('video-title', '', 'STRING');
        $video_description = $jinput->get('video-description', '', 'STRING');
        $videofile_name = $jinput->get('videofile-id', '', 'STRING');
        $video = new stdClass();
        $video->path = $videofile_name;
        $video->type = 'file';
        $video->uid = $my->id;
        $video->title = $video_title;
        $video->description = $video_description;
        $video->date = Foundry::date()->toMySQL();
        $video->status = 1;
        $db->insertObject('#__plot_video', $video);
        plotPoints::assign('add.video', 'com_plot', plotUser::factory()->id, $db->insertid());
        $app->enqueueMessage(JText::_('COM_PLOT_VIDEO_WAS_SAVED'));
        if ($my->id) {
            $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
        } else {
            $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
        }
    }

    public function confirmReport()
    {
        $ajax = Foundry::ajax();

        // Check if user is really allowed to submit any reports.
        $access = Foundry::access();

        if (!$access->allowed('reports.submit')) {
            $this->setMessage(JText::_('COM_EASYSOCIAL_REPORTS_NOT_ALLOWED_TO_SUBMIT_REPORTS'), SOCIAL_MSG_ERROR);
            return $ajax->reject($this->getMessage());
        }

        $title = JRequest::getVar('title', JText::_('COM_EASYSOCIAL_REPORTS_DIALOG_TITLE'));
        $description = JRequest::getVar('description', '');

        $theme = Foundry::themes();

        $theme->set('title', $title);
        $theme->set('description', $description);

        $html = $theme->output('site/reports/dialog.form');

        return $ajax->resolve($html);
    }

    public function conversations()
    {
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        $profileView = $this->getView('profile', 'html');
        $profileView->setLayout('conversations_popup');
        $options = array();
        $options['limit'] = 5;
        $options['filter'] = 'unread';
        $profileView->set('Itemid', $Itemid);
        $conversations = JModelLegacy::getInstance('conversations', 'EasySocialModel');
        //$conversations = JModelLegacy::getInstance('conversations', 'PlotModel');
        //get unread conversations
        $profileView->set('conversations', $conversations->getConversations(Foundry::user()->id, $options));
        $profileView->display();

        die;
    }

    public function friends()
    {
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        $profileView = $this->getView('profile', 'html');
        $profileView->setLayout('friends_popup');
        $friends = JModelLegacy::getInstance('friends', 'EasySocialModel');
        //Retrieves a list of friends that are in pending approval state
        $profileView->set('friends', $friends->getPendingRequests(Foundry::user()->id));
        $profileView->set('oldfriends', $friends->getFriends(Foundry::user()->id));
        $profileView->set('Itemid', $Itemid);
        $profileView->display();

        die;
    }

    /**
     * Approves a friend request
     */
    public function approve()
    {
        $app = JFactory::getApplication();
        $send_data = array();
        // Check for request forgeries.
        // Foundry::checkToken();

        // Do not allow non registered user access
        Foundry::requireLogin();

        // Get the connection id.
        $id = JRequest::getInt('id');
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        // Get the current user.
        $my = Foundry::user();

        // Get the view.
        // $view 	= $this->getCurrentView();
        // Try to load up the friend table
        $friend = Foundry::table('Friend');

        // Load the connection.
        if (!$friend->load($id)) {
            $app->enqueueMessage(JText::_('COM_EASYSOCIAL_FRIENDS_ERROR_INVALID_ID'));
            $profileView = $this->getView('profile', 'html');
            $profileView->setLayout('friends_popup');
            $friends = JModelLegacy::getInstance('friends', 'EasySocialModel');
            //Retrieves a list of friends that are in pending approval state
            $profileView->set('friends', $friends->getPendingRequests(Foundry::user()->id));
            $profileView->set('oldfriends', $friends->getFriends(Foundry::user()->id));
            $profileView->set('Itemid', $Itemid);
            $profileView->display();

            die;

        }

        // Get the person that initiated the friend request.
        $actor = Foundry::user($friend->actor_id);

        // Test if the target is really the current user.
        if ($friend->target_id != $my->id) {

            $app->enqueueMessage(JText::_('COM_EASYSOCIAL_FRIENDS_ERROR_NOT_YOUR_REQUEST'));
            $profileView = $this->getView('profile', 'html');
            $profileView->setLayout('friends_popup');
            $friends = JModelLegacy::getInstance('friends', 'EasySocialModel');
            //Retrieves a list of friends that are in pending approval state
            $profileView->set('friends', $friends->getPendingRequests(Foundry::user()->id));
            $profileView->set('oldfriends', $friends->getFriends(Foundry::user()->id));
            $profileView->set('Itemid', $Itemid);
            $profileView->display();

            die;
        }

        // Try to approve the request.
        if (!$friend->approve()) {

            $app->enqueueMessage(JText::_('COM_EASYSOCIAL_FRIENDS_ERROR_APPROVING_REQUEST'));
            $profileView = $this->getView('profile', 'html');
            $profileView->setLayout('friends_popup');
            $friends = JModelLegacy::getInstance('friends', 'EasySocialModel');
            //Retrieves a list of friends that are in pending approval state
            $profileView->set('friends', $friends->getPendingRequests(Foundry::user()->id));
            $profileView->set('oldfriends', $friends->getFriends(Foundry::user()->id));
            $profileView->set('Itemid', $Itemid);
            $profileView->display();

            die;

            //return $view->call( __FUNCTION__ , $friend );
        }

        //$view->setMessage( JText::sprintf( 'COM_EASYSOCIAL_FRIENDS_NOW_FRIENDS_WITH' , $actor->getName() ) , SOCIAL_MSG_SUCCESS );

        $callback = JRequest::getVar('viewCallback', __FUNCTION__);
        $allowedCallbacks = array(__FUNCTION__, 'notificationsApprove');

        if (!in_array($callback, $allowedCallbacks)) {
            $callback = __FUNCTION__;
        }


        $profileView = $this->getView('profile', 'html');
        $profileView->setLayout('friends_popup');
        $friends = JModelLegacy::getInstance('friends', 'EasySocialModel');
        //Retrieves a list of friends that are in pending approval state
        $profileView->set('friends', $friends->getPendingRequests(Foundry::user()->id));
        $profileView->set('oldfriends', $friends->getFriends(Foundry::user()->id));
        $profileView->set('Itemid', $Itemid);
        $profileView->display();

        die;
        /* $send_data['message']= JText::_('COM_PLOT_FRIEND_ACCEPT');
         echo $send_data['message'];

         exit();*/

    }

    /**
     * Reject friend request
     */
    public function reject()
    {
        // Check for request forgeries
        // Foundry::checkToken();
        $app = JFactory::getApplication();
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        // Guests shouldn't be able to view this.
        Foundry::requireLogin();

        // Get current logged in user.
        $my = Foundry::user();

        // Get the friend id.
        $id = JRequest::getInt('id');

        // Get the current view
        // $view	= $this->getCurrentView();

        // Try to load up the friend table
        $friend = Foundry::table('Friend');

        if (!$friend->load($id) || !$id) {


            $app->enqueueMessage(JText::_('COM_EASYSOCIAL_FRIENDS_ERROR_INVALID_ID'));
            $profileView = $this->getView('profile', 'html');
            $profileView->setLayout('friends_popup');
            $friends = JModelLegacy::getInstance('friends', 'EasySocialModel');
            //Retrieves a list of friends that are in pending approval state
            $profileView->set('friends', $friends->getPendingRequests(Foundry::user()->id));
            $profileView->set('oldfriends', $friends->getFriends(Foundry::user()->id));
            $profileView->set('Itemid', $Itemid);
            $profileView->display();

            die;
        }

        // Test if the target is really the current user.
        if ($friend->target_id != $my->id) {
            $app->enqueueMessage(JText::_('COM_EASYSOCIAL_FRIENDS_ERROR_NOT_YOUR_REQUEST'));
            $profileView = $this->getView('profile', 'html');
            $profileView->setLayout('friends_popup');
            $friends = JModelLegacy::getInstance('friends', 'EasySocialModel');
            //Retrieves a list of friends that are in pending approval state
            $profileView->set('friends', $friends->getPendingRequests(Foundry::user()->id));
            $profileView->set('oldfriends', $friends->getFriends(Foundry::user()->id));
            $profileView->set('Itemid', $Itemid);
            $profileView->display();

            die;
        }

        // @task: Run approval
        if (!$friend->reject()) {

            $app->enqueueMessage(JText::_('COM_EASYSOCIAL_FRIENDS_ERROR_REJECTING_REQUEST'));
            $profileView = $this->getView('profile', 'html');
            $profileView->setLayout('friends_popup');
            $friends = JModelLegacy::getInstance('friends', 'EasySocialModel');
            //Retrieves a list of friends that are in pending approval state
            $profileView->set('friends', $friends->getPendingRequests(Foundry::user()->id));
            $profileView->set('oldfriends', $friends->getFriends(Foundry::user()->id));
            $profileView->set('Itemid', $Itemid);
            $profileView->display();

            die;

        }
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->delete('#__plot_friends')
            ->where('social_id=' . (int)$friend->id);
        $db->SetQuery($query);

        try {
            $db->execute();
        } catch (RuntimeException $e) {

            $send_data['message'] = $e->getMessage();
            echo $send_data['message'];
            exit();
        }
        $conversationModel = $this->getModel('Conversations');
        if(plotUser::factory($friend->target_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($friend->target_id)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
            $conversationModel->sendMessageSystem($friend->target_id, 'plot-user-friend-reject-' . $my->id . '-' . $friend->target_id);
            $mailer = JFactory::getMailer();
            $config = JFactory::getConfig();
            $sender = array(
                $config->get('mailfrom'),
                $config->get('fromname')
            );
            $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $my->id) . '">' . plotUser::factory($my->id)->name . '</a>';
            $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $friend->target_id) . '">' . plotUser::factory($friend->target_id)->name . '</a>';
            $mailer->setSender($sender);
            $mailer->addRecipient(plotUser::factory($friend->target_id)->email);
            $mailer->setSubject(JText::_('COM_PLOT_SEND_FRIEND_REQUEST_SUBJECT'));
            $mailer->isHTML(true);
            $mailer->setBody(JText::sprintf("COM_PLOT_FRIEND_REJECT_MSG", $actor, $target));
            $mailer->Send();
        }
        $app->enqueueMessage(JText::_('COM_PLOT_FRIEND_REJECT'));
        $profileView = $this->getView('profile', 'html');
        $profileView->setLayout('friends_popup');
        $friends = JModelLegacy::getInstance('friends', 'EasySocialModel');
        //Retrieves a list of friends that are in pending approval state
        $profileView->set('friends', $friends->getPendingRequests(Foundry::user()->id));
        $profileView->set('oldfriends', $friends->getFriends(Foundry::user()->id));
        $profileView->set('Itemid', $Itemid);
        $profileView->display();

        die;

    }

    public function sendReport()
    {

        $jinput = JFactory::getApplication()->input;
        $data = array();
        // Get data from $_POST
        $post = array();
        $uid = $jinput->get('uid', 0, 'INT');
        $message = $jinput->get('message', '', 'STRING');
        $post['uid'] = $uid;
        $post['message'] = $message;

        // Get the current logged in user
        $my = Foundry::user();

        // Determine if this user has the permissions to submit reports.
        $access = Foundry::access();


        // Get the reports model
        $model = Foundry::model('Reports');


        // Create the report
        $report = Foundry::table('Report');
        $report->bind($post);

        // Try to get the user's ip address.
        $report->ip = JRequest::getVar('REMOTE_ADDR', '', 'SERVER');

        // Set the creator id.
        $report->created_by = $my->id;
        $report->message = $message;
        $report->title = Foundry::user($uid)->email;

        // Set the default state of the report to new
        $report->state = 0;

        $report->type = 'user';
        // Try to store the report.
        $state = $report->store();

        // If there's an error, throw it
        if (!$state) {
            $data['status'] = 0;
            $data['message'] = $report->getError();
            header('Content-Type: application/json');
            echo json_encode($data);
            die;

        }
        $stream = Foundry::stream();
        $template = $stream->getTemplate();
        $template->setActor($my->id, 'user');
        $template->setVerb('send');
        $template->setContext($my->id, 'report');
        $template->setTarget($report->uid);
        $template->setDate(Foundry::date()->toMySQL());
        $stream->add($template);

        // @badge: reports.create
        // Add badge for the author when a report is created.
        $badge = Foundry::badges();
        $badge->log('com_easysocial', 'reports.create', $my->id, JText::_('COM_EASYSOCIAL_REPORTS_BADGE_CREATED_REPORT'));

        // @points: reports.create
        // Add points for the author when a report is created.
        $mailer = JFactory::getMailer();
        $config = JFactory::getConfig();
        $sender = array(
            $config->get( 'mailfrom' ),
            $config->get( 'fromname' )
        );

        $actor='<a href="'.JRoute::_('index.php?option=com_plot&view=profile&id='.$my->id).'">'.plotUser::factory($my->id)->name.'</a>';
        $mailer->setSender($sender);
        $mailer->addRecipient($config->get( 'mailfrom' ));
        $mailer->setSubject(JText::_('COM_PLOT_SEND_REPORT_SUBJECT'));
        $mailer->isHTML(true);
        $mailer->setBody(JText::sprintf("COM_PLOT_SEND_REPORT_MSG",$actor));
        $mailer->Send();

        // Determine if we should send an email
        $config = Foundry::config();

        if ($config->get('reports.notifications.moderators')) {
            $report->notify();
        }
        $data['status'] = 1;
        $data['message'] = 'Жалоба отправлена администратору';
        header('Content-Type: application/json');
        echo json_encode($data);
        die;

    }

    public function addFriendAjax()
    {
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        $id = $jinput->get('id', 0, 'INT');
        $tags = PlotHelper::getMyTags($id);
        $profileView = $this->getView('profile', 'raw');
        $profileView->setLayout('friends_request');
        $profileView->set('tags', $tags);
        $profileView->set('id', $id);
        $profileView->set('Itemid', $Itemid);
        $profileView->display();
        die;
    }

    public function cancelRequestFriendAjax()
    {
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        $id = $jinput->get('id', 0, 'INT');

        $profileView = $this->getView('profile', 'html');
        $profileView->setLayout('friends_cancel_request');
        $profileView->set('id', $id);
        $profileView->set('Itemid', $Itemid);
        $profileView->display();

        die;
    }

    public function unfriendAjax()
    {
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        $id = $jinput->get('id', 0, 'INT');

        $profileView = $this->getView('profile', 'html');
        $profileView->setLayout('unfriends');
        $profileView->set('id', $id);
        $profileView->set('Itemid', $Itemid);
        $profileView->display();

        die;
    }

    /**
     * Creates a new friend request to a target
     */
    public function request()
    {
        // Check for request forgeries.
        //Foundry::checkToken();

        // User needs to be logged in
        Foundry::requireLogin();
        $conversationModel = $this->getModel('Conversations');

        // Get the target user that is being added.
        $id = JRequest::getInt('id');

        $tag_id = JRequest::getInt('tag_id');

        $user = plotUser::factory($id);
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();
        // Get the current view.
        // $view = $this->getCurrentView();
if(!plotUser::factory()->isHasTag($tag_id)){
    $data['status'] = 0;
    $data['message'] = JText::_('COM_PLOT_YOU_DO_NOT_HAVE_THIS_TAG');
    header('Content-Type: application/json');
    echo json_encode($data);
    die;
}
        // @TODO: Check if target user blocks this.

        // If the user doesn't exist;
        if (!$user || !$id) {
            $data['status'] = 0;
            $data['message'] = JText::_('COM_EASYSOCIAL_FRIENDS_UNABLE_TO_LOCATE_USER');
            header('Content-Type: application/json');
            echo json_encode($data);
            die;
        }

        // Get the current viewer.
        $my = Foundry::user();

        // Load up the model to check if they are already friends.
        $model = Foundry::model('Friends');

        $friend = Foundry::table('Friend');

        // Do not allow user to create a friend request to himself
        if ($my->id == $user->id) {
            $data['status'] = 0;
            $data['message'] = JText::_('COM_EASYSOCIAL_FRIENDS_UNABLE_TO_ADD_YOURSELF');
            header('Content-Type: application/json');
            echo json_encode($data);
            die;

        }

        // If they are already friends, ignore this.
        if ($model->isFriends($my->id, $user->id)) {

            $data['status'] = 0;
            $data['message'] = JText::_('COM_EASYSOCIAL_FRIENDS_ERROR_ALREADY_FRIENDS');
            header('Content-Type: application/json');
            echo json_encode($data);
            die;
        }

        // Check if user has already previously requested this.
        if ($model->isFriends($my->id, $user->id, SOCIAL_FRIENDS_STATE_PENDING)) {

            $data['status'] = 0;
            $data['message'] = JText::_('COM_EASYSOCIAL_FRIENDS_ERROR_ALREADY_REQUESTED');
            header('Content-Type: application/json');
            echo json_encode($data);
            die;

        }

        // If everything is okay, we proceed to add this request to the friend table.
        $friend->setActorId($my->id);

        // Set the target's id.
        $friend->setTargetId($user->id);

        // @TODO: Configurable. Set the state.
        $friend->setState(SOCIAL_FRIENDS_STATE_PENDING);

        // Store the friend request
        $state = $friend->store();

        // Send notification to the target when a user requests to be his / her friend.
        $params = array(
            'requesterId' => $my->id,
            'requesterAvatar' => $my->getAvatar(SOCIAL_AVATAR_LARGE),
            'requesterName' => $my->getName(),
            'requesterLink' => $my->getPermalink(true, true),
            'requestDate' => Foundry::date()->toMySQL(),
            'totalFriends' => $my->getTotalFriends(),
            'totalMutualFriends' => $my->getTotalMutualFriends($user->id)
        );
        // Email template
        $emailOptions = array(
            'title' => JText::sprintf('COM_EASYSOCIAL_EMAILS_SUBJECT_FRIENDS_NEW_REQUEST', $my->getName()),
            'template' => 'site/friends/request',
            'params' => $params
        );


        Foundry::notify('friends.request', array($user->id), $emailOptions, false);

        // @badge: friends.create
        // Assign badge for the person that initiated the friend request.
        $badge = Foundry::badges();
        $badge->log('com_easysocial', 'friends.create', $user->id, JText::_('COM_EASYSOCIAL_FRIENDS_BADGE_REQUEST_TO_BE_FRIEND'));

        $allowedCallbacks = array(__FUNCTION__, 'usersRequest', 'popboxRequest');
        $callback = JRequest::getVar('viewCallback', __FUNCTION__);

        if (!in_array($callback, $allowedCallbacks)) {
            $callback = __FUNCTION__;
        }
        $db = Foundry::db();
        $plotfriend = new stdClass();
        $plotfriend->social_id = $friend->id;
        $plotfriend->tag_id = $tag_id;
        $db->insertObject('#__plot_friends', $plotfriend);
        $data['status'] = 1;
        $data['message'] = 'Запрос на дружбу отправлен';
        if($user->getSocialFieldData('UNSUBSCRIBE')==1 || $user->getSocialFieldData('UNSUBSCRIBE')==NULL) {
            $conversationModel->sendMessageSystem($user->id, 'plot-user-friend-request-' . $my->id . '-' . $user->id);
            $mailer = JFactory::getMailer();
            $config = JFactory::getConfig();
            $sender = array(
                $config->get('mailfrom'),
                $config->get('fromname')
            );
            $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $my->id) . '">' . plotUser::factory($my->id)->name . '</a>';
            $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $user->id) . '">' . plotUser::factory($user->id)->name . '</a>';
            $mailer->setSender($sender);
            $mailer->addRecipient(plotUser::factory($user->id)->email);
            $mailer->setSubject(JText::_('COM_PLOT_SEND_FRIEND_REQUEST_SUBJECT'));
            $mailer->isHTML(true);
            $mailer->setBody(JText::sprintf("COM_PLOT_FRIEND_REQUEST_MSG", $actor, $target));
            $mailer->Send();
        }
        header('Content-Type: application/json');
        echo json_encode($data);
        die;

    }

    public function suggestWithList($privacy = null)
    {
        // Check for valid tokens.
        // Foundry::checkToken();

        // Only valid registered user has friends.
        Foundry::requireLogin();

        $my = Foundry::user();

        // Load friends model.
        $model = Foundry::model('Friends');

        // Load the view.
        // $view 		= $this->getCurrentView();

        // Properties
        $search = JRequest::getVar('search');
        $exclude = JRequest::getVar('exclude');
        $includeme = JRequest::getVar('includeme', 0);

        // Determine what type of string we should search for.
        $config = Foundry::config();
        $type = $config->get('users.displayName');

        //check if we need to apply privacy or not.
        $options = array();

        if ($privacy) {
            $options['privacy'] = $privacy;
        }

        if ($exclude) {
            $options['exclude'] = $exclude;
        }


        if ($includeme) {
            $options['includeme'] = $includeme;
        }

        // Try to get the search result.
        $friends = $model->search($my->id, $search, $type, $options);

        // Try to search a list of friends
        $listModel = Foundry::model('Lists');

        if (!$friends) {
            $friends = $listModel->search($my->id, $search);
        }
        $data = array();
        $data = $friends;
        echo json_encode($data);
        exit();

    }

    public function unfriend()
    {

        // Check for request forgeries.
        Foundry::checkToken();
        // User needs to be logged in.
        Foundry::requireLogin();
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();

        // Get the target user that will be removed.
        $target_id = JRequest::getInt('id');
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->select('`s`.`id`')
            ->from('`#__social_friends` AS `s`')
            ->where('`s`.`actor_id` = ' . (int)Foundry::user()->id)
            ->where('`s`.`target_id` = ' . $target_id)
            ->where('`s`.`state` = 1');
        $db->setQuery($query);
        $id = (int)$db->loadResult();


        // Get the current user.
        $my = Foundry::user();

        // Try to load up the friend table
        $friend = Foundry::table('Friend');
        $state = $friend->load($id);

        if (!$state || !$id) {
            $app->enqueueMessage(JText::_('COM_EASYSOCIAL_FRIENDS_INVALID_ID_PROVIDED'));
            return;

        }

        // Need to ensure that the target or source of the friend belongs to the current user.
        if ($friend->actor_id != $my->id && $friend->target_id != $my->id) {
            $app->enqueueMessage(JText::_('COM_EASYSOCIAL_FRIENDS_ERROR_NOT_YOUR_FRIEND'));
            return;

        }

        // Throw errors when there's a problem removing the friends
        if (!$friend->unfriend($my->id)) {

            $app->enqueueMessage($friend->getError());
            return;

        }
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->delete('#__plot_friends')
            ->where('social_id=' . (int)$id);
        $db->SetQuery($query);

        try {
            $db->execute();
        } catch (RuntimeException $e) {


            $app->enqueueMessage($e->getMessage());
            return;
        }

        $app->enqueueMessage(JText::_('COM_PLOT_USER_UNFRIEND'));
        $doc->addScriptDeclaration("window.parent.location.reload();");
        return;
    }

    public function cancelRequest()
    {

        // Check for request forgeries.
        Foundry::checkToken();
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();
        // Guests shouldn't be here.
        Foundry::requireLogin();

        // Get the current logged in user.
        $my = Foundry::user();


        // Get the friend id.
        $target_id = JRequest::getInt('id');
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->select('`s`.`id`')
            ->from('`#__social_friends` AS `s`')
            ->where('`s`.`actor_id` = ' . (int)Foundry::user()->id)
            ->where('`s`.`target_id` = ' . $target_id)
            ->where('`s`.`state` = -1');
        $db->setQuery($query);
        $id = (int)$db->loadResult();

        // Get the model
        $friends = Foundry::model('Friends');

        $table = Foundry::table('Friend');

        $table->load($id);

        if (!$id || !$table->id) {

            $app->enqueueMessage(JText::_('COM_EASYSOCIAL_FRIENDS_INVALID_ID_PROVIDED'));
            return;

        }

        // Check if the user is allowed to cancel the request.
        if (!$table->isInitiator()) {

            $app->enqueueMessage(JText::_('COM_EASYSOCIAL_FRIENDS_NOT_ALLOWED_TO_CANCEL_REQUEST'));
            return;
        }

        // Try to cancel the request.
        $state = $friends->cancel($id);

        if (!$state) {

            $app->enqueueMessage($friends->getError());
            return;
        }
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->delete('#__plot_friends')
            ->where('social_id=' . (int)$id);
        $db->SetQuery($query);

        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

        $app->enqueueMessage(JText::_('COM_PLOT_FRIEND_REQUEST_REMOVED'));
        $doc->addScriptDeclaration("window.parent.location.reload();");
        return;
    }

    //crop popup for certificate
    public function ajaxCertificateImage()
    {
        $id = JRequest::getVar('img_id', '');
        $profileEditView = $this->getView('profile', 'raw');
        $profileEditView->setLayout('ajax_certificate_image');
        $originalPhotoUrl = JUri::root() . 'images/com_plot/photos/' . plotUser::factory()->id . '/original/' . $id;
        $profileEditView->set('originalPhotoUrl', $originalPhotoUrl);
        $profileEditView->set('photoUrl', $id);
        $profileEditView->set('id', $id);
        list($width, $height) = getimagesize($originalPhotoUrl);
        $profileEditView->set('img_width', $width);
        $profileEditView->set('img_height', $height);
        $profileEditView->display();
        die;
    }

    //crop popup for photo
    public function ajaxPhotoImage()
    {
        $id = JRequest::getVar('img_id', '');
        $profileEditView = $this->getView('profile', 'raw');
        $profileEditView->setLayout('ajax_photo_image');
        $originalPhotoUrl = JUri::root() . 'images/com_plot/photos/' . plotUser::factory()->id . '/original/' . $id;
        $profileEditView->set('originalPhotoUrl', $originalPhotoUrl);
        $profileEditView->set('photoUrl', $id);
        $profileEditView->set('id', $id);
        list($width, $height) = getimagesize($originalPhotoUrl);
        $profileEditView->set('img_width', $width);
        $profileEditView->set('img_height', $height);
        $profileEditView->display();
        die;
    }

    public function ajaxVideoImage()
    {
        $profileEditView = $this->getView('profile', 'raw');
        $profileEditView->setLayout('ajax_videolink_image');
        $originalPhotoUrl = JUri::root() . 'images/com_plot/video/' . Foundry::user()->id . '/' . JRequest::getString('img_name') . '.jpg';
        $profileEditView->set('originalPhotoUrl', $originalPhotoUrl);
        $profileEditView->set('photoUrl', JRequest::getString('img_name'));
        list($width, $height) = getimagesize($originalPhotoUrl);
        $profileEditView->set('img_width', $width);
        $profileEditView->set('img_height', $height);
        $profileEditView->display();
        die;
    }

    public function cropCourseImageFromYoutubeLink()
    {
        $profileEditView = $this->getView('profile', 'raw');
        $profileEditView->setLayout('ajax_course_videolink_image');

        $originalPhotoUrl = JUri::root() . 'images/com_plot/video/' . Foundry::user()->id . '/' . JRequest::getString('img_name') . '.jpg';
        $profileEditView->set('originalPhotoUrl', $originalPhotoUrl);
        $profileEditView->set('photoUrl', JRequest::getString('img_name'));
        list($width, $height) = getimagesize($originalPhotoUrl);
        $profileEditView->set('img_width', $width);
        $profileEditView->set('img_height', $height);
        $profileEditView->display();
        die;
    }

    public function ajaxOpenVideo()
    {
        $profileEditView = $this->getView('profile', 'raw');
        $profileEditView->setLayout('ajax_open_video');
        $youtubeNumber = JRequest::getString('youtubeNumber');
        $profileEditView->set('youtubeNumber', $youtubeNumber);
        $profileEditView->display();
        die;
    }

    public function ajaxSaveCroppedVideo()
    {
        Foundry::requireLogin();
        require_once JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/image/image.php';
        $formData = array();
        parse_str(JRequest::getVar('data'), $formData);
        $imgWidth = $formData['img-width'];
        $imgHeight = $formData['img-height'];
        $width = $formData['w'] / $imgWidth;
        $height = $formData['h'] / $imgHeight;
        $left = $formData['x'] / $imgWidth;
        $top = $formData['y'] / $imgHeight;
        if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'thumb')) {
            mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'thumb');
        }
        $imageEmpty = new SocialImage();
        $image = $imageEmpty->load(JPATH_BASE . '/images/com_plot/video/' . Foundry::user()->id . '/' . $formData['img_src'] . '.jpg');

        $imageWidth = $image->getWidth();
        $imageHeight = $image->getHeight();
        if (!is_null($top) && !is_null($left) && !is_null($width) && !is_null($height)) {
            $actualX = $imageWidth * $left;
            $actualY = $imageHeight * $top;
            $actualWidth = $imageWidth * $width;
            $actualHeight = $imageHeight * $height;
            // Now we'll need to crop the image
            $image->crop($actualX, $actualY, $actualWidth, $actualHeight);
        } else {
            if ($imageWidth > $imageHeight) {
                $x = ($imageWidth - $imageHeight) / 2;
                $y = 0;
                $image->crop($x, $y, $imageHeight, $imageHeight);
            } else {
                $x = 0;
                $y = ($imageHeight - $imageWidth) / 2;
                $image->crop($x, $y, $imageWidth, $imageWidth);
            }
        }
        if (file_exists(JPATH_BASE . '/images/com_plot/video/' . Foundry::user()->id . '/thumb/' . $formData['img_src'] . '.jpg')) {
            unlink(JPATH_BASE . '/images/com_plot/video/' . Foundry::user()->id . '/thumb/' . $formData['img_src'] . '.jpg');
        }
        $tmpImagePath = JPATH_BASE . '/images/com_plot/video/' . Foundry::user()->id . '/thumb/' . $formData['img_src'] . '.jpg';
        $image->resize(plotGlobalConfig::getVar('courseCropWidthMin'), plotGlobalConfig::getVar('courseCropHeightMin'));
        $image->save($tmpImagePath);

        // Unset the image to free up some memory
        unset($image);

        $data = array('img' => JUri::root() . 'images/com_plot/video/' . Foundry::user()->id . '/thumb/' . $formData['img_src'] . '.jpg');
        echo json_encode($data);
        die;
    }

    public function eventImageUpload()
    {
        $file = JRequest::getVar('qqfile', '', 'FILES');
        $data = array();

        $db = Foundry::db();
        $rEFileTypes = "/^\.(jpg|jpeg|gif|png|bmp|xcf|odg){1}$/i";
        $access = Foundry::access(Foundry::user()->id, SOCIAL_TYPE_USER);
        $maxFilesizeBytes = (int)$access->get('photos.uploader.maxsize') * 1048576;

        if (!empty($file['tmp_name'])) {
            if (($file['size']) <= $maxFilesizeBytes) {
                $image_path = JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'original';
                if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events')) {
                    mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events');
                }
                if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events' . DIRECTORY_SEPARATOR . Foundry::user()->id)) {
                    mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events' . DIRECTORY_SEPARATOR . Foundry::user()->id);
                }
                if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'original')) {
                    mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'original');
                }
                $ext = JFile::getExt($file['name']);
                $new_name = md5(time() . $file['name']) . '.' . $ext;
                if (preg_match($rEFileTypes, strrchr($new_name, '.'))) {
                    if (JFile::copy($file['tmp_name'], $image_path . '/' . $new_name)) {

                        list($width, $height) = getimagesize($image_path . '/' . $new_name);
                        if ($width < plotGlobalConfig::getVar('certificateCropWidthMin') || $height < plotGlobalConfig::getVar('certificateCropHeightMin')) {
                            @unlink(JPATH_BASE . '/images/com_plot/events/' . Foundry::user()->id . '/original/' . JRequest::getString('img_name'));
                            header('Content-Type: application/json');
                            $data['status'] = 0;
                            $data['message'] = 'Минимальный размер картинки должен быть ' . plotGlobalConfig::getVar('certificateCropWidthMin') . 'x' . plotGlobalConfig::getVar('certificateCropHeightMin');
                            echo json_encode($data);
                            die;
                        } else {

                            $data['name'] = $new_name;
                            $data['status'] = 1;
                            $event1 = new stdClass();
                            $event1->user_id = Foundry::user()->id;
                            $event1->img = $new_name;

                            JFactory::getDbo()->insertObject('#__plot_events', $event1);
                            $id = $db->insertid();
                            $data['id'] = $id;
                            header('Content-Type: application/json');
                            echo json_encode($data);
                            die;
                        }

                    } else {
                        header('Content-Type: application/json');
                        $data['status'] = 0;
                        $data['message'] = 'error';
                        echo json_encode($data);
                        die;
                    }
                } else {
                    $data['status'] = 0;
                    $data['message'] = JText::_('COM_PLOT_INVALID_IMG_FORMAT');
                    header('Content-Type: application/json');
                    echo json_encode($data);
                    die;
                }
            } else {
                $data['status'] = 0;
                $data['message'] = JText::_('COM_PLOT_INVALID_FILE SIZE');
                header('Content-Type: application/json');
                echo json_encode($data);
                die;
            }

        } else {
            $data['status'] = 0;
            $data['message'] = JText::_('COM_PLOT_INVALID_IMG');
            header('Content-Type: application/json');
            echo json_encode($data);
            die;
        }
    }

    public function eventupload()
    {
        $app = JFactory::getApplication();
        $model = $this->getModel('Profile');
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        $end_date = $jinput->get('end_date', '', 'STRING');
        $event_id = $jinput->get('event-id', 0, 'INT');
        $d2 = new DateTime($end_date);
        $now = new DateTime();
        $db = JFactory::getDbo();
        $my = plotUser::factory();

        if (!$event_id) {
            $app->enqueueMessage(JText::_('COM_PLOT_EVENT_ERROR_UPLOAD_IMG'), 'error');
            if ($my->id) {
                $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
            } else {
                $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
            }
            return;
        }

        if ($d2 < $now) {
            $query = $db->getQuery(true)
                ->delete('`#__plot_events`')
                ->where('`id` = ' . $event_id);
            $db->setQuery($query);
            try {
                $db->execute();
            } catch (RuntimeException $e) {
                $app->enqueueMessage(JText::_('COM_PLOT_DO_NOT_CREATE_OLD_EVENT'), 'error');
                if ($my->id) {
                    $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
                } else {
                    $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
                }
                return;
            }
            $app->enqueueMessage(JText::_('COM_PLOT_DO_NOT_CREATE_OLD_EVENT'), 'error');
            if ($my->id) {
                $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
            } else {
                $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
            }
            return;
        }
        $model->updateEvent($event_id);

        $app->enqueueMessage(JText::_('COM_PLOT_EVENT_SAVE'));
        if ($my->id) {
            $app->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . $my->id, false, -1));
        } else {
            $app->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
        }

    }

    public function deletefriend()
    {
        $data = array();
        // User needs to be logged in.
        Foundry::requireLogin();
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();

        // Get the target user that will be removed.
        $target_id = JRequest::getInt('id');
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->select('`s`.`id`')
            ->from('`#__social_friends` AS `s`')
            ->where('`s`.`actor_id` = ' . (int)Foundry::user()->id)
            ->where('`s`.`target_id` = ' . $target_id)
            ->where('`s`.`state` = 1');
        $db->setQuery($query);
        $id = (int)$db->loadResult();


        // Get the current user.
        $my = Foundry::user();

        // Try to load up the friend table
        $friend = Foundry::table('Friend');
        $state = $friend->load($id);

        if (!$state || !$id) {
            $data['status'] = false;
            $data['message'] = JText::_('COM_EASYSOCIAL_FRIENDS_INVALID_ID_PROVIDED');
            echo json_encode($data);
            exit();

        }

        // Need to ensure that the target or source of the friend belongs to the current user.
        if ($friend->actor_id != $my->id && $friend->target_id != $my->id) {
            $data['status'] = false;
            $data['message'] = JText::_('COM_EASYSOCIAL_FRIENDS_ERROR_NOT_YOUR_FRIEND');
            echo json_encode($data);
            exit();

        }

        // Throw errors when there's a problem removing the friends
        if (!$friend->unfriend($my->id)) {
            $data['status'] = false;
            $data['message'] = $friend->getError();
            echo json_encode($data);
            exit();


        }
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->delete('#__plot_friends')
            ->where('social_id=' . (int)$id);
        $db->SetQuery($query);

        try {
            $db->execute();
        } catch (RuntimeException $e) {

            $data['status'] = false;
            $data['message'] = $e->getMessage();
            echo json_encode($data);
            exit();
        }
        $data['status'] = true;
        $data['message'] = JText::_('COM_PLOT_USER_UNFRIEND');
        echo json_encode($data);
        exit();

    }

    public function stream()
    {
        $data = array();
        $jinput = JFactory::getApplication()->input;
        $limit = plotGlobalConfig::getVar('streamDefaultLimit') + $jinput->get('limit', 25, 'INT');
        $user_id = $jinput->get('user_id', 0, 'INT');
        $options = array("userid" => array(0 => $user_id), "list" => NULL,
            "context" => "all", "type" => "user", "limitstart" => "", "limitend" => "", "viewer" => Foundry::user()->id, "isfollow" => false, "direction" => "older", "guest" => false, "tag" => false, "limit" => $limit);
        $model = $this->getModel('Stream');
        $data['streams'] = $model->streamPrepareForRender($model->getStreamData($options));
        $data['limit'] = $limit;
        $count_results = $model->getStreamCount($options);
        $data['count_results'] = $count_results;
        echo json_encode($data);
        exit();
    }

    public function setState()
    {
        JFactory::getApplication()->setUserState("my_books", 1);
        exit();
    }

    public function resetState()
    {
        JFactory::getApplication()->setUserState("my_books", 0);
        exit();
    }

    public function ajaxGetVideoPopupHtml()
    {
        $youtubeNumber = JRequest::getString('youtubeNumber');
        $profileEditView = $this->getView('profile', 'html');
        $profileEditView->setLayout('profile_video_popup');
        $profileEditView->set('youtubeNumber', $youtubeNumber);
        $profileEditView->display();
        die;
    }

    public function uploadVideoImg()
    {
        $youtubeNumber = JRequest::getString('youtubeNumber');

        $thumbnailUrl = 'http://img.youtube.com/vi/' . $youtubeNumber . '/hqdefault.jpg';
        $pathToUserVideoFolder = JPATH_SITE . '/images/com_plot/video/' . Foundry::user()->id;

        if (file_exists($pathToUserVideoFolder . '/' . $youtubeNumber . '.jpg')) {
            unlink($pathToUserVideoFolder . '/' . $youtubeNumber . '.jpg');
        }
        if (!file_exists($pathToUserVideoFolder)) {
            mkdir($pathToUserVideoFolder);
        }

        $data = array();
        header('Content-Type: application/json');
        if (!@copy($thumbnailUrl, $pathToUserVideoFolder . '/' . $youtubeNumber . '.jpg')) {
            $data['status'] = 0;
            $data['message'] = 'Файл не найден';
            echo json_encode($data);
            die;
        }
        list($width, $height) = getimagesize($pathToUserVideoFolder . '/' . $youtubeNumber . '.jpg');
        if ((int)plotGlobalConfig::getVar('videoCropWidthMin') > (int)$width || (int)plotGlobalConfig::getVar('videoCropHeightMin') > $height) {
            $data['status'] = 0;
            $data['message'] = 'Минимальный размер картинки должен быть ' . plotGlobalConfig::getVar('videoCropWidthMin') . 'x' . plotGlobalConfig::getVar('videoCropHeightMin');
            echo json_encode($data);
            die;
        }

        $data['status'] = 1;
        $data['message'] = 'ok';

        echo json_encode($data);
        die;
    }

    public function videoFileUpload()
    {
        $my = Foundry::user();
        JFolder::create(JPATH_SITE . "/media/com_plot/videos/" . $my->id . '/original');
        $filesVideos = JRequest::getVar('qqfile', '', 'FILES');
        $allowedTypes = explode(',', plotGlobalConfig::getVar('videoTypes'));
        if ($filesVideos['type']) {
            list($type, $format) = explode('/', $filesVideos['type']);
        } else {
            list($type, $format) = array('', '');
        }

        if ($type != 'video') {
            $data['status'] = 0;
            $data['message'] = JText::_('COM_PLOT_SOME_VIDEO_FILES_COULD_BE_CORRUPTED_CHECK');
            echo json_encode($data);
            die;
        }

        if (!in_array($format, $allowedTypes)) {
            $data['status'] = 0;
            $data['message'] = JText::_('COM_PLOT_SOME_VIDEOS_HAVE_NOT_ALLOWED_FORMAT');
            $data['message'] .= ' ' . JText::sprintf('COM_PLOT_ALLOWED_VIDEO_FORMATS', implode(', ', explode(',', plotGlobalConfig::getVar('videoTypes'))));
            echo json_encode($data);
            die;
        }

        if ($filesVideos['size'] > (int)plotGlobalConfig::getVar('maxVideoSize') * 1024 * 1024) {
            $data['status'] = 0;
            $data['message'] = JText::_('COM_PLOT_SOME_VIDEOS_HAVE_NOT_ALLOWED_FILESIZE');
            echo json_encode($data);
            die;
        }

        require_once JPATH_COMPONENT . '/helpers/video.php';

        $fileName = md5($filesVideos["tmp_name"]) . "." . (($format == 'mpeg') ? 'mp3' : $format);
        JFile::move($filesVideos["tmp_name"], JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/" . $fileName);

        plotVideoHelper::convert_media(JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/" . $fileName, JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/" . pathinfo($fileName, PATHINFO_FILENAME) . '.mp4');

        if ($format != 'mp4') {
            JFile::delete(JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/" . $fileName);
        }
        $fileName = pathinfo($fileName, PATHINFO_FILENAME) . '.mp4';

        if (!file_exists(JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/" . $fileName)) {
            $data['status'] = 0;
            $data['message'] = JText::_('COM_PLOT_ERROR');
            echo json_encode($data);
            die;
        }

        chmod(JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/" . $fileName, 0644);
        $img = pathinfo($fileName, PATHINFO_FILENAME) . '.jpg';
        plotVideoHelper::createVideoFileThumb(JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/" . $fileName, JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/original/" . $img);
        $data['status'] = 1;
        $data['name'] = $fileName;
        echo json_encode($data);
        die;
    }

    public function ajaxVideofileImage()
    {
        $profileEditView = $this->getView('profile', 'raw');
        $profileEditView->setLayout('ajax_videofile_image');
        $originalPhotoUrl = JUri::root() . '/media/com_plot/videos/' . Foundry::user()->id . '/original/' . pathinfo(JRequest::getString('img_name'), PATHINFO_FILENAME) . '.jpg';
        $profileEditView->set('originalPhotoUrl', $originalPhotoUrl);
        $profileEditView->set('photoUrl', pathinfo(JRequest::getString('img_name'), PATHINFO_FILENAME) . '.jpg');
        $profileEditView->display();
        die;
    }

    public function ajaxEventImage()
    {
        $profileEditView = $this->getView('profile', 'raw');
        $profileEditView->setLayout('ajax_event_image');
        $originalPhotoUrl = JUri::root() . 'images/com_plot/events/' . Foundry::user()->id . '/original/' . JRequest::getString('img_name');
        $profileEditView->set('originalPhotoUrl', $originalPhotoUrl);
        $profileEditView->set('photoUrl', JRequest::getString('img_name'));
        list($width, $height) = getimagesize($originalPhotoUrl);
        $profileEditView->set('img_width', $width);
        $profileEditView->set('img_height', $height);
        if ($width < plotGlobalConfig::getVar('meetingCropWidthMin') || $height < plotGlobalConfig::getVar('meetingCropHeightMin')) {
            @unlink(JPATH_BASE . '/images/com_plot/events/' . Foundry::user()->id . '/original/' . JRequest::getString('img_name'));
            $db = JFactory::getDbo();
            $query = $db->getQuery(true)
                ->delete('`#__plot_events`')
                ->where('`img` = "' . JRequest::getString('img_name') . '"')
                ->where('`user_id` = ' . (int)Foundry::user()->id);
            $db->setQuery($query);
            try {
                $db->execute();
            } catch (RuntimeException $e) {
                $profileEditView->display();
                die;
            }

        }
        $profileEditView->display();
        die;
    }

    public function ajaxAvatarImage()
    {
        $profileEditView = $this->getView('profileedit', 'raw');
        $profileEditView->setLayout('avatar.crop');
        $originalPhotoUrl = JUri::root() . 'media/com_easysocial/avatars/users/' . Foundry::user()->id . '/' . JRequest::getString('img_name');
        $profileEditView->set('originalPhotoUrl', $originalPhotoUrl);
        $profileEditView->set('photoUrl', JRequest::getString('img_name'));
        list($width, $height) = getimagesize($originalPhotoUrl);
        $profileEditView->set('img_width', $width);
        $profileEditView->set('img_height', $height);
        if ($width < plotGlobalConfig::getVar('meetingCropWidthMin') || $height < plotGlobalConfig::getVar('meetingCropHeightMin')) {
            @unlink(JPATH_BASE . '/media/com_easysocial/avatars/users/' . Foundry::user()->id . '/' . JRequest::getString('img_name'));
            $db = JFactory::getDbo();
            $query = $db->getQuery(true)
                ->delete('`#__plot_events`')
                ->where('`img` = "' . JRequest::getString('img_name') . '"')
                ->where('`user_id` = ' . (int)Foundry::user()->id);
            $db->setQuery($query);
            try {
                $db->execute();
            } catch (RuntimeException $e) {
                $profileEditView->display();
                die;
            }

        }
        $profileEditView->display();
        die;
    }

    public function certificateDelete()
    {
        $id = JRequest::getInt('cert_id');
        $photo = PlotHelper::getImageOriginalById($id);
        $data = array();

        $dirPath = JPATH_BASE . '/media/com_easysocial/photos/' . (int)$photo->album_id . '/' . $id;
        if (is_dir($dirPath)) {
            $objects = scandir($dirPath);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dirPath . DIRECTORY_SEPARATOR . $object) == "dir") {
                        deleteDirectory($dirPath . DIRECTORY_SEPARATOR . $object);
                    } else {
                        @unlink($dirPath . DIRECTORY_SEPARATOR . $object);
                    }
                }
            }
            reset($objects);
            rmdir($dirPath);
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->delete('`#__social_photos`')
            ->where('`id` = ' . (int)$id);
        $db->setQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
            echo json_encode($data);
            die;
        }
        $query = $db->getQuery(true)
            ->delete('`#__social_photos_meta`')
            ->where('`photo_id` = ' . (int)$id);
        $db->setQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
            echo json_encode($data);
            die;
        }
        $data['status'] = 1;
        echo json_encode($data);
        die;
    }

    public function eventDelete()
    {
        $id = JRequest::getInt('event_id');
        $photo = PlotHelper::getEventById($id);
        $data = array();
        @unlink(JPATH_BASE . '/images/com_plot/events/' . Foundry::user()->id . '/original/' . $photo->img);
        @unlink(JPATH_BASE . '/images/com_plot/events/' . Foundry::user()->id . '/thumb/' . $photo->img);
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->delete('`#__plot_events`')
            ->where('`id` = ' . (int)$id);
        $db->setQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
            echo json_encode($data);
            exit();
        }
        $data['status'] = 1;
        echo json_encode($data);
        exit();
    }

    public function videolinkDeleteImg()
    {
        $jinput = JFactory::getApplication()->input;
        $link = $jinput->get('link', '', 'RAW');
        $youtubeNumber = substr($link, strripos($link, '?v=') + 3);
        $path = JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR . Foundry::user()->id;
        if (file_exists($path . DIRECTORY_SEPARATOR . $youtubeNumber . '.jpg')) {
            @unlink($path . DIRECTORY_SEPARATOR . $youtubeNumber . '.jpg');
        }

        if (file_exists($path . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $youtubeNumber . '.jpg')) {
            @unlink($path . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $youtubeNumber . '.jpg');
        }
        $data = array();
        $data['status'] = 1;
        echo json_encode($data);
        exit();
    }

    public function videoFileDeleteImg()
    {
        $jinput = JFactory::getApplication()->input;
        $file_name = $jinput->get('file_name', '');

        $path = JPATH_SITE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR . Foundry::user()->id;
        $search = '.';
        $res = strripos($file_name, $search);
        $img = substr($file_name, 0, $res);

        if (file_exists($path . DIRECTORY_SEPARATOR . $file_name)) {
            @unlink($path . DIRECTORY_SEPARATOR . $file_name);
        }

        if (file_exists($path . DIRECTORY_SEPARATOR . 'original' . DIRECTORY_SEPARATOR . $img . '.jpg')) {
            @unlink($path . DIRECTORY_SEPARATOR . 'original' . DIRECTORY_SEPARATOR . $img . '.jpg');
        }

        if (file_exists($path . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $img . '.jpg')) {
            @unlink($path . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $img . '.jpg');
        }

        $data = array();
        $data['status'] = 1;
        echo json_encode($data);
        exit();
    }

    public function ajaxSaveCroppedVideoFileImage()
    {
        Foundry::requireLogin();
        require_once JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/image/image.php';
        $formData = array();
        parse_str(JRequest::getVar('data'), $formData);
        $imgWidth = $formData['img-width'];
        $imgHeight = $formData['img-height'];
        $width = $formData['w'] / $imgWidth;
        $height = $formData['h'] / $imgHeight;
        $left = $formData['x'] / $imgWidth;
        $top = $formData['y'] / $imgHeight;

        if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'thumb')) {
            mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR . Foundry::user()->id . DIRECTORY_SEPARATOR . 'thumb');
        }
        $image = new SocialImage();
        $image = $image->load(JPATH_BASE . '/media/com_plot/videos/' . Foundry::user()->id . '/original/' . $formData['img_src']);

        $imageWidth = $image->getWidth();
        $imageHeight = $image->getHeight();
        if (!is_null($top) && !is_null($left) && !is_null($width) && !is_null($height)) {
            $actualX = $imageWidth * $left;
            $actualY = $imageHeight * $top;
            $actualWidth = $imageWidth * $width;
            $actualHeight = $imageHeight * $height;

            // Now we'll need to crop the image
            $image->crop($actualX, $actualY, $actualWidth, $actualHeight);
        } else {
            if ($imageWidth > $imageHeight) {
                $x = ($imageWidth - $imageHeight) / 2;
                $y = 0;
                $image->crop($x, $y, $imageHeight, $imageHeight);
            } else {
                $x = 0;
                $y = ($imageHeight - $imageWidth) / 2;
                $image->crop($x, $y, $imageWidth, $imageWidth);
            }
        }

        $tmpImagePath = JPATH_BASE . '/media/com_plot/videos/' . Foundry::user()->id . '/thumb/' . $formData['img_src'];
        $image->resize(plotGlobalConfig::getVar('courseCropWidthMin'), plotGlobalConfig::getVar('courseCropHeightMin'));

        if (JFile::exists($tmpImagePath)) {
            jfile::delete($tmpImagePath);
        }
        @$image->save($tmpImagePath);

        // Unset the image to free up some memory
        unset($image);

        // Reload the image again to get the correct resource pointing to the cropped image.
        $image = Foundry::image();
        $image->load($tmpImagePath);
        $data = array();
        $data['img'] = JUri::root() . 'media/com_plot/videos/' . Foundry::user()->id . '/thumb/' . $formData['img_src'];
        echo json_encode($data);
        die;
    }

    public function openVideoFile()
    {
        $profileEditView = $this->getView('profile', 'raw');
        $profileEditView->setLayout('ajax_open_videofile');
        $video = PlotHelper::getVideoById(JRequest::getInt('videoId'));
        $profileEditView->set('video', $video);
        $profileEditView->display();
        die;
    }

    public function pickoutImage()
    {
        $this->user = plotUser::factory(JRequest::getInt('id', 0));
        PlotHelper::echoNewImageForSocials($this->user->getSquareAvatarUrl(), JRequest::getInt('canv_width'), JRequest::getInt('canv_height'));
        die;
    }

    public function uploadSmallCourseVideo()
    {
        $data = array('success' => true);
        $my = plotUser::factory();

        JFolder::create(JPATH_SITE . "/media/com_plot/videos/" . $my->id . '/original');
        $uploadedVideoFile = JRequest::getVar('uploaded-video-file', '', 'FILES');
        $allowedVideoTypes = explode(',', plotGlobalConfig::getVar('videoTypes'));
        if ($uploadedVideoFile['type']) {
            list($type, $format) = explode('/', $uploadedVideoFile['type']);
        } else {
            list($type, $format) = array('', '');
        }

        # validation
        $errors = array();
        header('Content-Type: application/json');
        if ($type != 'video') {
            $errors[] = JText::_('COM_PLOT_SOME_VIDEO_FILES_COULD_BE_CORRUPTED_CHECK');
        }
        if (!in_array($format, $allowedVideoTypes)) {
            $errors[] = JText::_('COM_PLOT_SOME_VIDEOS_HAVE_NOT_ALLOWED_FORMATD') . ' ' . JText::sprintf('COM_PLOT_ALLOWED_VIDEO_FORMATS', implode(', ', explode(',', plotGlobalConfig::getVar('videoTypes'))));
        }
        if ($uploadedVideoFile['size'] > (int)plotGlobalConfig::getVar('maxVideoSize') * 1024 * 1024) {
            $errors[] = JText::_('COM_PLOT_SOME_VIDEOS_HAVE_NOT_ALLOWED_FILESIZE');
        }
        if (count($errors)) {
            $data['success'] = false;
            $data['message'] = implode('<br />', $errors);
            echo json_encode($data);
            die;
        }

        $fileName = md5($uploadedVideoFile["tmp_name"]) . "." . (($format == 'mpeg') ? 'mp3' : ($format == 'x-flv' ? 'flv' : ($format == 'x-ms-wmv' ? 'wmv' : $format)));
        JFile::move($uploadedVideoFile["tmp_name"], JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/" . $fileName);
        chmod(JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/" . $fileName, 0644);
        $img = pathinfo($fileName, PATHINFO_FILENAME) . '.jpg';
        require_once JPATH_COMPONENT . '/helpers/video.php';
        plotVideoHelper::createVideoFileThumb(JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/" . $fileName, JPATH_SITE . "/media/com_plot/videos/" . $my->id . "/original/" . $img);
        $data['name'] = $fileName;
        $data['imageUrl'] = JUri::root() . "media/com_plot/videos/" . $my->id . "/original/" . $img;

        echo json_encode($data);
        die;
    }

    public function ajaxCourseVideofileImage()
    {
        $profileEditView = $this->getView('profile', 'html');
        $profileEditView->setLayout('ajax_course_videofile_image');
        $profileEditView->set('originalPhotoUrl', JRequest::getString('imgUrl'));
        $profileEditView->display();
        die;
    }

    public function ajaxAllEventsLoad()
    {

        $userId = JRequest::getInt('id');

        $user = plotUser::factory($userId);

        if ($user->isParent()) {
            require_once JPATH_COMPONENT . '/views_parent/view.php';

            $this->setPath('view', JPATH_COMPONENT . '/views_parent/');
            $this->addViewPath(JPATH_COMPONENT . '/views_parent');

        }

        $user->stream = $user->getStream(array('limitstart' => 0, 'limit' => plotGlobalConfig::getVar('maxStreamItemsCountProfilePage')));
        $profileView = $this->getView('profile', 'raw');

        $profileView->setLayout('allevents.list');
        $profileView->set('user', $user);

        $profileView->display();
        die;
    }

    public function friendRequestReplaceText()
    {
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');

        $essayModel = $this->getModel('Profile');
        $essay = $essayModel->friendRequestReplaceText($ids,"COM_PLOT_FRIEND_REQUEST_MSG");

        echo json_encode($essay);
        exit();
    }

    public function friendRejectReplaceText()
    {
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');

        $essayModel = $this->getModel('Profile');
        $essay = $essayModel->friendRequestReplaceText($ids,"COM_PLOT_FRIEND_REJECT_MSG");

        echo json_encode($essay);
        exit();
    }

    public function friendApproveReplaceText()
    {
        $jinput = JFactory::getApplication()->input;
        $ids = $jinput->get('ids', array(), 'ARRAY');

        $essayModel = $this->getModel('Profile');
        $essay = $essayModel->friendRequestReplaceText($ids,"COM_PLOT_FRIEND_APPROVE_MSG");

        echo json_encode($essay);
        exit();
    }
}
