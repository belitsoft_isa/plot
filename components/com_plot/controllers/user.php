<?php

defined('_JEXEC') or die('Restricted access');

class PlotControllerUser extends JControllerLegacy
{

    public function addChild()
    {
        $app = JFactory::getApplication();
        $my = plotUser::factory();

        $my->addChild(JRequest::getInt('childId', 0));

        $app->redirect(JRoute::_('index.php?option=com_plot&view=profileedit', false));
    }

    public function removeChild()
    {
        $app = JFactory::getApplication();
        $my = plotUser::factory();

        $my->removeChild(JRequest::getInt('childId', 0));

        $app->redirect(JRoute::_('index.php?option=com_plot&view=profileedit', false));
    }

}
