<?php
defined('_JEXEC') or die('Restricted access');

class PlotControllerWallet extends PlotController
{

    public function cashout()
    {
        $app = JFactory::getApplication();
        $my = plotUser::factory();
        
        $this->cashoutMethod = JRequest::getString('cash-out-method');
        $this->cashout_amount = JRequest::getInt('amount');
        
        $this->redirectUrl = $my->isParent() ? JRoute::_('index.php?option=com_plot&view=wallet', false) : JRoute::_('index.php?option=com_plot&view=profile&id='.$my->id, false);
        
        if (!$this->cashout_amount) {
            $this->raiseError(JText::_('COM_PLOT_ENTER_VALID_AMOUNT'));
        }
        if ($my->getMoney() < $this->cashout_amount) {die("2");
            $this->raiseError(JText::_('COM_PLOT_NOT_ENOUGHT_MONEY_IN_WALLET'));
        }
        if (!$my->checkMoneyValue()) {
            $this->raiseError(JText::_('COM_PLOT_CASHOUT_ERROR_PLEASE_CONTACT_ADMINISTRATOR'));
        }
        if (!$this->processCashout()) {
            $this->raiseError(JText::_('COM_PLOT_ERROR_CASHOUTING_PLEASE_CHECK_ALL_DATA'));
        }
        
        $my->cashout($this->cashout_amount, $this->cashoutMethod);
        $app->enqueueMessage(JText::_('COM_PLOT_CASHOUT_SUCCESFULL'));
        $app->redirect($this->redirectUrl);
    }
 
    private function processCashout()
    {
        # $someRealYandexWallet = '410011161616877';
        # $someRealPhoneNumber = '79219990099';
        # $realPhoneNumber2 = '79219990093';

        $methodName = 'processCashoutTo'.$this->cashoutMethod;
                
        if (method_exists($this, $methodName)) {
            require_once JPATH_ROOT.'/components/com_plot/libraries/yandex/lib/api.php';
            $this->ya_cashOutDesctination = JRequest::getVar('cash-out-destination'); 
            $this->ya_api = new YandexMoney\API( plotGlobalConfig::getVar('yandexAppAccessToken') );
            return $this->$methodName();
        } 
        
        return false;
    }
    
    private function processCashoutToYandexWallet() 
    {
        $requestPaymentOptions = array(
            'pattern_id' => 'p2p',
            'to' => $this->ya_cashOutDesctination,
            'amount' => $this->cashout_amount,
            'ext_action_uri' => 'https://naplotu.com'
        );
        $requestPaymentResult = $this->requestYandexPayment( $requestPaymentOptions );
        $this->processYandexPayment( array( 'request_id' => $requestPaymentResult->request_id, 'money_source' => 'wallet' ) );
        return true;
    }
    
    private function processCashoutToPhone() 
    {
        $requestPaymentOptions = array(
            'pattern_id' => 'phone-topup',
            'phone-number' => $this->ya_cashOutDesctination,
            'amount' => $this->cashout_amount
        );
        $requestPaymentResult = $this->requestYandexPayment( $requestPaymentOptions );
        $this->processYandexPayment( array( 'request_id' => $requestPaymentResult->request_id, 'money_source' => 'wallet' ) );
        return true;
    }
    
    private function requestYandexPayment($options)
    {
        $requestPaymentResult = $this->ya_api->requestPayment($options);
        if ($requestPaymentResult->status != 'success') {
            $this->raiseError('Ошибка на стороне Yandex: '.$requestPaymentResult->error);
        }
        return $requestPaymentResult;
    }
    
    private function processYandexPayment($options)
    {
        $processPaymentResult = $this->ya_api->processPayment( $options );
        if ($processPaymentResult->status != 'success') {
            $this->raiseError('Ошибка на стороне Yandex: '.$processPaymentResult->error.' Попробуйте позже. При повторении ошибки обратитесь к администраторам.');
        }
        return $processPaymentResult;
    }
    
    private function raiseError($errorMessage)
    {
        $app = JFactory::getApplication();
        $app->enqueueMessage($errorMessage);
        $app->redirect($this->redirectUrl);
    }
    
}
