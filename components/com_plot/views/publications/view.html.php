<?php

defined('_JEXEC') or die;
require_once JPATH_COMPONENT . '/views/view.php';

require_once(JPATH_SITE . '/administrator/components/com_html5flippingbook/libs/VarsHelper.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/conversations.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/friends.php');

class PlotViewPublications extends JViewLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'plot';

    public function display($tpl = null)
    {
        $this->my = plotUser::factory();
        $this->my->level = $this->my->getLevel();
        $app = JFactory::getApplication();
        $this->templateUrl = JURI::base().'templates/'.$app->getTemplate();
        $this->componentUrl = JURI::base().'components/com_plot';

        $this->config = $this->GetConfig();

        $this->books = $this->get('Items');

        if ($this->books) {
            foreach ($this->books AS $row) {
                $thumbnailPath = JPATH_SITE.'/media/com_html5flippingbook'.'/thumbs/'.$row->c_thumb;

                if ($row->c_thumb == "" || !is_file($thumbnailPath)) {
                    $row->thumbnailUrl = JURI::root()."images/com_plot/def_book.jpg";
                } else {
                    if (file_exists(JPATH_BASE.'/media/com_html5flippingbook'.'/thumbs/thimb_'.$row->c_thumb)) {
                        $row->thumbnailUrl = JURI::root()."media/com_html5flippingbook/thumbs/thimb_".$row->c_thumb;
                    } else {
                        $row->thumbnailUrl = JURI::root()."images/com_plot/def_book.jpg";
                    }
                }
            }
        }

        $this->state = $this->get('State');

        $this->cats = $this->get('Categories');
        $tags = new plotTags();
        $this->tags = $tags->getK2TagsList();
        if ((int) Foundry::user()->profile_id == (int) plotGlobalConfig::getVar('childProfileId')) {
            $this->mytags = $this->get('MyTags');
        } else {
            $this->mytags = $this->get('MyChildrenTags');
        }

        $ages = new plotAges();
        $this->plotAges = $ages->getList();

        $this->my_books = JRequest::getInt('my_books', 0);

        $this->setOgMeta();
        parent::display($tpl);
    }

    protected function prepareDocument()
    {
        parent::prepareDocument();
        parent::addFeed();
    }

    public function getConfig()
    {
        $db = JFactory::getDBO();

        $query = "SELECT * FROM `#__html5fb_config`" .
            " ORDER BY `setting_name`";
        $db->setQuery($query);
        $rows = $db->loadObjectList();

        $config = (object)array();

        if (isset($rows)) {
            foreach ($rows as $row) {
                $config->{$row->setting_name} = $row->setting_value;
            }
        }

        return $config;
    }
    
    private function setOgMeta()
    {
        require_once JPATH_ADMINISTRATOR.'/components/com_plot/plot_social_config.php';
        $metaOgUrl = '<meta property="og:url" content="http://naplotu.com/publications/" />';
        $metaOgTitle = '<meta property="og:title" content="'.plotSocialConfig::get('publicationsTitle').'" />';
        $metaOgDescription = '<meta property="og:description" content="'.plotSocialConfig::get('publicationsDesc').'" />';
        $metaOgImage = '<meta property="og:image" content="'.JUri::root().plotSocialConfig::get('publicationsImagePath').'" />';
        $metaOgImageType = '<meta property="og:image:type" content="image/jpeg" />';
        $metaOgImageWidth = '<meta property="og:image:width" content="'.plotSocialConfig::get('publicationsImageWidth').'" />';
        $metaOgImageHeight = '<meta property="og:image:height" content="'.plotSocialConfig::get('publicationsImageHeight').'" />';
        $metaOgSiteName = '<meta property="og:site_name" content="'.JFactory::getConfig()->get('sitename').'" />';
        $metaOgFbAppId = '<meta property="fb:app_id" content="473991812742077" />';
        $metaOgType = '<meta property="og:type" content="website" />';
        $metaOgLocale = '<meta property="og:locale" content="ru_RU" />';
        JFactory::getDocument()->addCustomTag($metaOgTitle.$metaOgSiteName.$metaOgUrl.$metaOgDescription.$metaOgFbAppId.$metaOgType.$metaOgLocale.$metaOgImage.$metaOgImageType.$metaOgImageWidth.$metaOgImageHeight);
        return true;
    }    
    
}
