<?php
defined('_JEXEC') or die;
$count_all_books = count($this->books);

foreach ($this->booksGroups AS $booksGroup) { ?>
    <div class="jcarousel-wrapper my-books child-library">
        <div class="no-jcarousel">
            <ul class="child-library">
                <?php foreach ($booksGroup as $book) { ?>
                    <li>
                        <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId='.$book->c_id); ?>">
                            <figure><img src="<?php echo $book->thumbnailUrl; ?>" alt="<?php htmlspecialchars($book->c_title); ?>"/></figure>
                        </a>
                        <?php if ($book->c_pub_descr) { ?>
                            <div class="elem-descr">
                                <p><?php echo PlotHelper::cropStr(strip_tags($book->c_pub_descr), plotGlobalConfig::getVar('booksChildDescriptionMaxSymbolsToShow')).'...'; ?></p>
                            </div>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
<?php }
?>
