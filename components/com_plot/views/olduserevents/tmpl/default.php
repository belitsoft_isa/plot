<?php
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.tooltip');

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$sortFields = array(
    'a.id' => JText::_('COM_PLOT_ID')
);
$sortedByOrder = ($listOrder == 'a.ordering');


?>
<style>
    body {
        color: #000000;
    }
</style>
<script type="text/javascript">

    Joomla.orderTable = function () {
        table = document.getElementById('sortTable');
        direction = document.getElementById('directionTable');
        order = table.options[table.selectedIndex].value;

        if (order != '<?php echo $listOrder; ?>') {
            dirn = 'asc';
        }
        else {
            dirn = direction.options[direction.selectedIndex].value;
        }

        Joomla.tableOrdering(order, dirn, '');
    }
    jQuery(document).ready(function () {


        jQuery('[data-toggle="tooltip"]').tooltip();
    });


</script>

<form method="post" name="adminForm" id="adminForm" action="<?php echo 'index.php?option=com_plot&view=olduserevents'; ?>"
      method="post" autocomplete="off">


    <input type="hidden" name="task" value=""/>
    <input type="hidden" name="boxchecked" value="0"/>
    <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
    <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
    <?php echo JHtml::_('form.token'); ?>
    <div id="filter-bar" class="btn-toolbar">
        <div class="filter-search btn-group pull-left">
            <input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('COM_PLOT_SEARCH_BY_TITLE'); ?>" value="<?php
            echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('COM_PLOT_SEARCH_BY_TITLE'); ?>" />
        </div>
    </div>
    <div>
        <?php
        if ($this->items) {
            ?>
            Всего событий:  <span id="plot-count-events"><?php echo count($this->items); ?></span>
            <?php
            foreach ($this->items as $i => $item) {
                ?>
                <div class="plot-events" id="plot-event-id-<?php echo $item->id; ?>">
                    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=event&id=' . $item->id); ?>"><?php echo $item->title; ?></a><br>
                    start <?php echo JHtml::date($item->start_date, 'g-m-Y Y:m'); ?>
                    end <?php echo JHtml::date($item->end_date, 'g-m-Y Y:m'); ?><br>
                    <img style="width: 100px;"
                         src="<?php echo JURI::root() . 'images/com_plot/events/' . $item->user_id . '/' . $item->img; ?>"><br>
                    author: <a
                        href="<?php echo JRoute::_('index.php?option=com_plot&view=profile&id=' . $item->user_id); ?>"><?php echo plotUser::factory($item->user_id)->name; ?></a>
                    <br>
                    количество участноков: <?php echo PlotHelper::countEventSubscription($item->id); ?><br>


                </div>

            <?php
            }
        }
        ?>

    </div>
</form>

