<?php
defined('_JEXEC') or die;
?>

<script src="<?php echo $this->componentUrl; ?>/libraries/scrolling_ajax_pagination/javascript.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate().'/js/jquery.jscrollpane.min.js'; ?>" async="async"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate().'/js/jquery.mousewheel.js'; ?>" async="async" ></script>
<script type="text/javascript" src="<?php echo JUri::root().'components/com_plot/views/courses/tmpl/default.js'; ?>"></script>

<?php # <editor-fold defaultstate="collapsed" desc="JS">  ?>
<script type="text/javascript">
    function showFilteredAndSortedCourses()
    {
        setAxaxScrollPagination();
        var tagsIds = jQuery("[id^=filter-courses-tag]:checked").map(function(){
            return jQuery(this).val();
        }).get();
        
        jQuery('#courses-list').html('<div id="loading-bar"><img src="<?php echo $this->baseurl ?>/templates/plot/img/pre-loader-1.gif" /></div>');
        
        jQuery.post(
            '<?php echo JRoute::_('index.php?option=com_plot&task=courses.ajaxGetFilteredAndSortedCourses'); ?>',
            {
                ageId: jQuery('#childlib-alter').val(),
                tagsIds: tagsIds,
                categoryId: jQuery('#childlib-category').val(),
                myCoursesOnly: jQuery('[name="my_courses"]:checked').val(),
                sort: jQuery('#childlib-filter').val()
            },
            function(response) {
                jPlotUp.Arrow.initialize('positionCameras');
                var data = jQuery.parseJSON(response);
                jQuery('.info-board .info-bottom .count-courses').html('Найдено <span>'+jPlot.helper.declOfNum(data.countCourses, ['курс', 'курса', 'курсов'])+'</span>');
                jQuery('#courses-list').show();
                jQuery('#courses-list').html(data.renderedCourses);
                if(data.countCourses==0){
                    jQuery('#courses-list').hide();
                }
                jPlotUp.Arrow.initialize('positionCameras');
            }
        );
    }
    
    function checkOnlyMyInterestsCheckboxes()
    {
        jQuery("[id^=filter-courses-tag]").prop('checked', false);
        jQuery.post(
            '<?php echo JRoute::_('index.php?option=com_plot&task=publications.ajaxGetMyTags'); ?>',
            function(response) {
                var data = jQuery.parseJSON(response),
                    i= 0,
                    data_count=data.length;
                jQuery("[id^=filter-courses-tag]").prop('checked', false);
                for (i; i<data_count; i++) {
                    jQuery('#filter-courses-tag'+data[i].id).prop('checked', true);
                }
                showFilteredAndSortedCourses();
            }
        );

    }


    function setAxaxScrollPagination()
    {
        var tagsIds = jQuery("[id^=filter-courses-tag]:checked").map(function(){
            return jQuery(this).val();
        }).get();        
        var ajaxScrollData = {
            ageId: jQuery('#childlib-alter').val(), 
            tagsIds: tagsIds, 
            categoryId: jQuery('#childlib-category').val(),
            myCoursesOnly: jQuery('[name="my_courses"]:checked').val(),
            sort: jQuery('#childlib-filter').val()
        };

        jQuery(window).unbind('scroll');

        jQuery('.child-library > .wrap.child-library').scrollPagination({
            nop: <?php echo plotGlobalConfig::getVar('coursesResultsShowFirstCount'); ?>,
            offset: <?php echo plotGlobalConfig::getVar('coursesResultsShowFirstCount'); ?>,
            error: 'No More Posts!',
            delay: 50,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=courses.ajaxLoadMore'); ?>',
            appendDataTo: 'courses-list',
            userData: ajaxScrollData,
            afterLoad: {
                afterLoadAction: function () {
                    jPlotUp.Arrow.initialize('positionCameras');
                }
            }
        });

        jQuery(window).scroll(function(){
            jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
        });
    }

    jQuery(document).ready(function(){

        jQuery( "#childlib-category" ).selectmenu({
            appendTo:'.main-content',
            open: function( event, ui ) { jQuery('#childlib-category-menu').jScrollPane();
            },
            change: function( event, ui ) { showFilteredAndSortedCourses(); }
        });
        jQuery( "#childlib-alter" ).selectmenu({
            appendTo:'.main-content',
            open: function( event, ui ) { jQuery('#childlib-alter-menu').jScrollPane(); },
            change: function(){showFilteredAndSortedCourses();}
        });
        jQuery( "#childlib-filter" ).selectmenu({
            appendTo:'.main-content',
            open: function( event, ui ) { jQuery('#childlib-filter-menu').jScrollPane(); },
            change: function( event, ui ) { showFilteredAndSortedCourses(); }
        });

        jQuery('.bulb-menu-open').click(function(){
            var bulbMenu = jQuery(this).parent().find('.bulb-menu');
            if ( bulbMenu.is(':visible') ) {
                bulbMenu.hide();
            } else {
                bulbMenu.show();
            }
        });

        jQuery(function() {
            jQuery('.scroll-pane').jScrollPane({
                    verticalDragMinHeight: 28,
                    verticalDragMaxHeight: 28
                }
            );
        });
        jQuery(window).scroll(function(){
            jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
        });

        <?php

         if(plotUser::factory()->id){
         if($this->showOnlyMyCourses){ //if have parametr
             if($this->showOnlyMyCourses==1){
             ?>
        jQuery('#filter-courses-all').prop('checked', false);
        jQuery('#filter-courses-my').prop('checked', true);
        showFilteredAndSortedCourses();
        <?php
            }else{
            ?>
        jQuery('#filter-courses-all').prop('checked', true);
        showFilteredAndSortedCourses();
        <?php
            }

        }else{ //if not have parametr
            if(plotUser::factory()->isHaveCourses()){ //if have books
           ?>
        jQuery('#filter-courses-all').prop('checked', false);
        jQuery('#filter-courses-my').prop('checked', true);
        showFilteredAndSortedCourses();
        <?php
            }else{ //if not have books
       ?>
        jQuery('#filter-courses-all').prop('checked', true);
        jQuery('#filter-courses-my').prop('checked', false);
        showFilteredAndSortedCourses();
        <?php
            }
        }

        }else{
        ?>

        //if not login
        jQuery('#filter-courses-all').prop('checked', true);
        showFilteredAndSortedCourses();
        <?php
        }?>

    });

    document.addEventListener("touchmove", function(){
        jQuery('.child-library > .wrap.child-library').trigger('scroll');
        console.log('ScrollStart');
    }, false);

</script>
<?php # </editor-fold>  ?>

<?php # <editor-fold defaultstate="collapsed" desc="CSS"> ?>
<style type="text/css">
    .com_plot {
        color: #FCFCFC;
    }
    .more {
        margin: 5px;
    }

</style>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="MAIN"> ?>
<main class="child-library">
    
    <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_header') ); ?>

    <div class="wrap main-wrap child-library">
        <div class="add-left aside-left child-library"></div>
        <div class="add-right aside-right child-library"></div>
        <section class="child-library interest-filter-board">
            <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_top_menu') ); ?>
            <div class="info-board child-library">
                <?php if ($this->referrerUrl) { ?>
                <a href="<?php echo $this->referrerUrl; ?>"class="link-back hover-shadow">
                    Вернуться
                    <svg viewBox="0 0 22.9 46" preserveAspectRatio="xMinYMin meet" class="butterfly">
                        <use xlink:href="#butterfly"></use>
                    </svg>
                </a>
                <?php } ?>
                <svg viewBox="0 0 37.1 38.9" preserveAspectRatio="xMinYMin meet" class="leaf">
                    <use xlink:href="#leaf"></use>
                </svg>
                <object name="bobr" data="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/img/bobr-first-level.svg" type="image/svg+xml"></object>
                <form class="select-wood">
                    <select id="childlib-alter" onchange="showFilteredAndSortedCourses();">
                        <option value="0"><?php echo JText::_('COM_PLOT_ALL_AGES'); ?></option>
                        <?php foreach ($this->plotAges AS $age) { ?>
                            <option value="<?php echo $age->id; ?>"><?php echo $age->title; ?></option>
                        <?php } ?>
                    </select>
                </form>
                <div class="interest-board-wrapper">
                    <div class="interest-board">
                        <h3>Интересы</h3>
                        <form class="scroll-pane">
                            <fieldset>
                                <?php foreach ($this->plotTags AS $tag) { ?>
                                <input type="checkbox" checked="checked" id="filter-courses-tag<?php echo $tag->id; ?>" value="<?php echo $tag->id; ?>" onchange="showFilteredAndSortedCourses();" />
                                <label for="filter-courses-tag<?php echo $tag->id; ?>"><?php echo $tag->title; ?></label>
                                <?php } ?>
                            </fieldset>
                        </form>
                        <div class="interest-board-under">
                            <button onclick="checkAllFilterCoursesTagsCheckboxes();">Выбрать все</button>
                            <?php if(plotUser::factory()->id){ ?>
                            <input id="filter-my-tags" type="checkbox"/>
                            <label for="filter-my-tags" onclick="checkOnlyMyInterestsCheckboxes();">Мои интересы</label>
                            <?php } ?>
                            <button onclick="clearAllFilterCoursesTagsCheckboxes();">Очистить</button>
                        </div>
                    </div>
                </div>
                <div class="info-top">
                    <form>
                        <select id="childlib-category" onchange="showFilteredAndSortedCourses();">
                            <option value="0"><?php echo JText::_('COM_PLOT_ALL_CATEGORIES'); ?></option>
                            <?php foreach ($this->coursesCategories AS $courseCategory) { ?>
                                <option value="<?php echo $courseCategory->id; ?>"><?php echo $courseCategory->c_category; ?></option>
                            <?php } ?>
                        </select>
                    </form>
                    <?php if ($this->my->id) {
                        ?>

                        <input id="filter-courses-all" type="radio" value="0" name="my_courses"  style="display: none" onchange="showFilteredAndSortedCourses();" />
                        <label for="filter-courses-all">Общий раздел курсов</label>
                        <input id="filter-courses-my" type="radio" value="1" name="my_courses"  style="display: none" onchange="showFilteredAndSortedCourses();" />
                        <label for="filter-courses-my">Мои Курсы</label>
                    <?php } ?>

                </div>
                <div class="info-bottom">
                    <p class="count-courses"></p>
                    <form>
                        <label for="childlib-filter">Сортировать по</label>
                        <select id="childlib-filter">
                            <option value="`total_min_cost` ASC">Цена с дешевых</option>
                            <option value="`total_min_cost` DESC">Цена с дорогих</option>
                            <option value="`c`.`start_date` DESC">Дата добавления курса (с новых)</option>
                        </select>                        
                    </form>						
                </div>
            </div>
        </section>
        <section class="my-progress child-library">
            <div id="courses-list" class="my-progress-wrapper"></div>
        </section>
    </div>
    
</main>
<?php # </editor-fold> ?>

