function checkAllFilterCoursesTagsCheckboxes()
{
    jQuery("[id^=filter-courses-tag]").prop('checked', true);
    showFilteredAndSortedCourses();
}

function clearAllFilterCoursesTagsCheckboxes()
{
    jQuery("[id^=filter-courses-tag]").prop('checked', false);
    showFilteredAndSortedCourses();
}



jQuery(window).resize(function() {

    jQuery(function() {
        jQuery('.scroll-pane').jScrollPane({
                verticalDragMinHeight: 28,
                verticalDragMaxHeight: 28
            }
        );
    });
});    

// profile menu, bulb menus
jQuery(document).on('click', function(event) {
    if (!jQuery(event.target).closest('.bulb-menu').length && !jQuery(event.target).closest('.bulb-menu-open').length) {
        jQuery('.bulb-menu').hide();
    }
});


