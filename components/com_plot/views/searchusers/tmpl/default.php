<?php
defined('_JEXEC') or die('Restricted access');
?>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">
    function addNewUserCourseBuyFor(userId, userName)
    {
        jQuery('#course-children-for .users-course-for-list').append(
            '<div class="user-course-for" userid="'+userId+'">'+
                '<a class="remove">&#215</a>'+
                '<div class="name">'+jPlot.helper.cropString(userName, 28)+'</div>'+
            '</div>'
        );
        calculateCourseCost();
        SqueezeBox.close();
    }
    
    jQuery(document).ready(function(){
        
        jQuery('#search-users-to-buy .search-value').keyup(function(){
            jQuery('#search-users-to-buy .search-users-loading').html('Загрузка...');
            jQuery.post(
                '<?php echo JRoute::_('index.php?option=com_plot&task=searchusers.ajaxGetUsersList&courseId='.$this->courseId, false); ?>', 
                {
                    searchValue: jQuery('#search-users-to-buy .search-value').val(),
                    page: 1
                }, 
                function(response){
                    var data = jQuery.parseJSON(response);
                    jQuery('#search-users-to-buy .users-list').html(data.html);
                    jQuery('#search-users-to-buy .search-users-loading').html('');
                }
            );
        });
        
        jQuery('#search-users-to-buy .users-list').on('click', '.user-row:not(.inactive)', function(){
            var userId = jQuery(this).attr('userid');
            var userName = jQuery.trim( jQuery(this).find('.user-name').html() );
            var alreadyAddedUsers = jQuery('.user-course-for', window.parent.document).map(function(i, elem){
                return jQuery(this).attr('userid');
            }).get();
            if (jQuery.inArray(userId, alreadyAddedUsers) === -1) {
                addNewUserCourseBuyFor(userId, userName);
            } else {
                alert('Пользователь уже добавлен.');
            }
        });
        
        jQuery('#search-users-to-buy .users-list').on('click', '.pagination .page-item', function(){
            var page = jQuery(this).attr('page');
            var searchValue = jQuery.trim( jQuery('#search-users-to-buy .search-value').val() );
            jQuery('#search-users-to-buy .search-users-loading').html('Загрузка...');
            jQuery.post(
                '<?php echo JRoute::_('index.php?option=com_plot&task=searchusers.ajaxGetUsersList&courseId='.$this->courseId, false); ?>', 
                {
                    searchValue: searchValue,
                    page: page
                },
                function(response){
                    var data = jQuery.parseJSON(response);
                    jQuery('#search-users-to-buy .users-list').html(data.html);
                    jQuery('#search-users-to-buy .search-users-loading').html('');
                }
            );
        });
    });
</script>
<?php # </editor-fold> ?>

<div id="search-users-to-buy">
    <div class="search-users">
        <input class="search-value" type="text" placeholder="Введите имя..."/>
        <div class="search-users-loading"></div>
    </div>
    <div class="users-list">
        <?php require 'users.list.php'; ?>
    </div>    
</div>
