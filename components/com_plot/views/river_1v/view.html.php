<?php

defined('_JEXEC') or die;
require_once JPATH_COMPONENT.'/views/view.php';

class PlotViewRiver extends JViewLegacy
{

    public function display($tpl = null)
    {
        $app = JFactory::getApplication();
        $this->template = JFactory::getApplication()->getTemplate();
        $this->templateUrl = JURI::base().'templates/'.$app->getTemplate();
        $this->componentUrl = JURI::base().'components/com_plot';
        
        $this->my = Foundry::user();
        
        if ($this->my->id) {
            $app->redirect(JRoute::_('index.php?option=com_plot&view=profile&id='.$this->my->id, false));
        }
        
        JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_plot/models/');
        $riversTextsModel = JModelLegacy::getInstance('river_texts', 'PlotModel');
        $riversTextsModel->filter[] = array('field' => 'rt.published', 'value' => '1');
        $this->riverTexts = $riversTextsModel->getItems();                
        
        $this->setOgMeta();
        return parent::display($tpl);
    }
    
    private function setOgMeta()
    {
        require_once JPATH_ADMINISTRATOR.'/components/com_plot/plot_social_config.php';
        $metaOgUrl = '<meta property="og:url" content="http://naplotu.com/" />';
        $metaOgTitle = '<meta property="og:title" content="'.plotSocialConfig::get('riverTitle').'" />';
        $metaOgDescription = '<meta property="og:description" content="'.plotSocialConfig::get('riverDesc').'" />';
        $metaOgImage = '<meta property="og:image" content="'.JUri::root().plotSocialConfig::get('riverImagePath').'?rand='.rand().'" />';
        $metaOgImageType = '<meta property="og:image:type" content="image/jpeg" />';
        $metaOgImageWidth = '<meta property="og:image:width" content="'.plotSocialConfig::get('riverImageWidth').'" />';
        $metaOgImageHeight = '<meta property="og:image:height" content="'.plotSocialConfig::get('riverImageHeight').'" />';
        $metaOgSiteName = '<meta property="og:site_name" content="'.JFactory::getConfig()->get('sitename').'" />';
        $metaOgFbAppId = '<meta property="fb:app_id" content="473991812742077" />';
        $metaOgType = '<meta property="og:type" content="website" />';
        $metaOgLocale = '<meta property="og:locale" content="ru_RU" />';
        JFactory::getDocument()->addCustomTag($metaOgTitle.$metaOgSiteName.$metaOgUrl.$metaOgDescription.$metaOgFbAppId.$metaOgType.$metaOgLocale.$metaOgImage.$metaOgImageType.$metaOgImageWidth.$metaOgImageHeight);
        return true;
    }

}
