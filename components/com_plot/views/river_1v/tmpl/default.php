<?php
defined('_JEXEC') or die;
?>
<link rel="canonical" href="http://naplotu.com/" />
<script src="<?php echo $this->componentUrl; ?>/libraries/scrolling_ajax_pagination/javascript.js"></script>
<script src="<?php echo $this->templateUrl; ?>/js/river_resizer.js"></script>

<?php # <editor-fold defaultstate="collapsed" desc="CSS"> ?>
<style type="text/css">
    /* hide footer. Show only if no more ajax content */
    .river-footer {
        display: none;
    }
    .push {
        display: none;
    }
    .river-bottom-loading-bar {
        background-color: #3F6C19;
        text-align: center;
        height: 20px;
        padding: 5px 0 10px;
    }
</style>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">

    jQuery(document).ready(function() {
        jQuery('#content').scrollPagination({
            nop: 2,
            offset: 2,
            error: 'No More Posts!',
            delay: 50,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=river.ajaxNextRiverPart'); ?>',
            appendDataTo: 'content',
            noMorePostScript: "jQuery('.river-bottom-loading-bar').hide();jQuery('.river-footer').show();jQuery('.push').show();"
        });
        
        Resizer();
        jQuery('.main-content').append('<div class="river-bottom-loading-bar"><img src="http://naplotu.com/templates/plot/img/pre-loader-1.gif"></div>');
    });
    jQuery(window).resize(function() {
        Resizer();
    });
</script>
<?php # </editor-fold> ?>

<?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_header') ); ?>
    
<?php # <editor-fold defaultstate="collapsed" desc="TOP"> ?>
<div class="top">
   <div class="wrap home-page">
        <div class="add-left home-page aside-left"></div>
        <div class="add-right home-page aside-right"></div>
        <a class="bobr-top modal" rel="{size: {x: 580, y: 700}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
           href="<?php echo JRoute::_('index.php?option=com_plot&task=registration.ajaxGetLoginPopupHtml&activeTab=1');?>">
            <object style="visibility: visible;" type="image/svg+xml" data="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate();?>/img/bobr-first-level.svg"></object>
            <div class="brevno">
                <span class="brevno-text"><?php echo JText::_('COM_PLOT_LETS_FLOAT?'); ?></span>
                <svg viewBox="0 0 37.1 38.9" preserveAspectRatio="xMinYMin meet" class="leaf">
                    <use xlink:href="#leaf"></use>
                </svg>
            </div>
        </a>
	   <iframe width="420" height="315" src="//www.youtube.com/embed/HzBWu0l5xHY?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>
</div>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="MAIN"> ?>
<main class="home-page">
    <div class="wrap main-wrap" id="content">
        <section class="bg1">
            <object style="visibility: visible;" type="image/svg+xml" data="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate();?>/img/group1.svg" name="group1"></object>
            <object style="visibility: visible;" type="image/svg+xml" data="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate();?>/img/group2.svg" name="group2"></object>
            <object style="visibility: visible;" type="image/svg+xml" data="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate();?>/img/bobr-in-water.svg" name="bobr-in-river"></object>
            <article>
                <h2><?php echo $this->riverTexts[0]->title;?></h2>
                <p><?php echo $this->riverTexts[0]->text;?></p>
            </article>
            <article>
                <h2><?php echo $this->riverTexts[1]->title;?></h2>
                <p><?php echo $this->riverTexts[1]->text;?></p>
            </article>
        </section>
    </div>
</main>
<?php # </editor-fold> ?>

