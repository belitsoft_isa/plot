<?php

defined('_JEXEC') or die;
require_once JPATH_COMPONENT.'/views/view.php';

class PlotViewPublication extends JViewLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'publication';

    public function display($tpl = null)
    {
        return parent::display($tpl);
    }
    
    public function ajaxRenderList()
    {
        return $this->loadTemplate();
    }

}
