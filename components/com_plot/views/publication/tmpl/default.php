<?php
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
$url = JFactory::getURI();
$request_url = $url->toString();
require_once(JPATH_SITE . '/components/com_plot/helpers/html5fbfront.php');
$id = JFactory::getApplication()->input->get('bookId', 0, 'INT');

?>

<script type="text/javascript" src="<?php echo JUri::root() . 'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate() . '/js/jquery.jscrollpane.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.min.js"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.basic.js"></script>

<script type="text/javascript"
        src="<?php echo JUri::root() . 'components/com_plot/libraries/jquery_input_mask_plugin/jquery.mask.min.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo JUri::root(); ?>components/com_plot/assets/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>templates/plot/js/jquery_simple_progressbar.js"></script>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>


<![endif]-->
<script type="text/javascript">
    function noEssayFind() {
        jQuery('#essay-list').html('');
        jQuery('#essay-list')
            .html("<div class='no-items'><?php echo JText::sprintf('COM_PLOT_NOT_FOUND_WHAT_BOOKS', JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar("footerLinkK2idForChildrens"))); ?></div>");
    }
    function bookAccessDenied() {
        SqueezeBox.initialize({
            size: {x: 300, y: 150}
        });
        var str = '<div id="enqueued-message">Эта книга пока не куплена вам</div>';
        SqueezeBox.setContent('adopt', str);
        SqueezeBox.resize({x: 300, y: 150});
    }
    jQuery(document).ready(function ($) {//обязательная запись, чтобы можно было использовать $ вместо jQuery

        jQuery('.arrow-top').click(function () {
            document.getElementById('book_link').click();
        });

        jQuery('.bulb-menu-open').click(function () {
            var bulbMenu = jQuery(this).parent().find('.bulb-menu');
            if (bulbMenu.is(':visible')) {
                bulbMenu.hide();
            } else {
                bulbMenu.show();
            }
        });
        jQuery("#paymentType").selectmenu({
            appendTo: '.payment-type'
        });
        jQuery("#paymentType").on("selectmenuclose", function (event, ui) {
        });

        jQuery('.plot-book-programs').each(function () {
            var percent = parseInt(jQuery(this).attr('data-progress'));
            jQuery(this).simple_progressbar({
                value: percent,
                height: '10px',
                internalPadding: '3px',
                normalColor: '#C56B35',
                backgroundColor: '#ffffff'
            });
        });

    });

    // profile menu, bulb menus
    jQuery(document).on('click', function (event) {
        if (!jQuery(event.target).closest('.bulb-menu').length && !jQuery(event.target).closest('.bulb-menu-open').length) {
            jQuery('.bulb-menu').hide();
        }
    });


    document.addEventListener("touchmove", function(){
        jQuery('.main-wrap-overflow').trigger('scroll');
        console.log('ScrollStart');
    }, false);
</script>

<script type="text/javascript">

    function ajaxLoadEssay() {
        var essayFilterData = {
            bookId: '<?php echo  $id; ?>',
            screenHeight: window.innerHeight,
            screenWidth: window.innerWidth
        };
        jQuery.post(
            'index.php?option=com_plot&task=essay.ajaxEssayLoadMore',
            {userData: essayFilterData},
            function (response) {
                if (response) {
                    jQuery('#essay-list').append(response);
                    jQuery('#loading-bar').hide();
                } else {
                    jQuery('#loading-bar').hide();
                }

            }
        );
        jQuery('#essay-list').scrollPagination({
            nop: <?php echo plotGlobalConfig::getVar('childEssayCount'); ?>,
            offset: <?php echo plotGlobalConfig::getVar('childEssayCount'); ?>,
            error: '',
            delay: 10,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=essay.ajaxEssayLoadMore'); ?>',
            appendDataTo: 'essay-list',
            userData: essayFilterData,
            afterLoad: {
                afterLoadAction: function () {
                    jPlotUp.Arrow.initialize('positionCameras');
                }
            }
        });
    }

    function ajaxLoadBeforePublications() {
        var beforeFilterData = {
            bookId: '<?php echo $id; ?>'
        };
        jQuery.post(
            'index.php?option=com_plot&task=publication.ajaxGetBeforePublications',
            {userData: beforeFilterData},
            function (response) {
                if (response) {
                    var data = jQuery.parseJSON(response);
                    jQuery('#child-beforebook').append(data.renderedBooks);
                    jQuery('#loading-bar').hide();
                } else {
                    jQuery('#loading-bar').hide();
                }
            }
        );

        jQuery(window).unbind('scroll');
        jQuery('#child-beforebook').scrollPagination({
            nop: <?php echo plotGlobalConfig::getVar('booksResultsShowFirstCountChild'); ?>,
            offset: <?php echo plotGlobalConfig::getVar('booksResultsShowFirstCountChild'); ?>,
            error: '',
            delay: 10,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=publication.ajaxGetBeforePublications'); ?>',
            appendDataTo: 'child-beforebook',
            userData: beforeFilterData,
            afterLoad: {
                afterLoadAction: function () {
                    jPlotUp.Arrow.initialize('positionCameras');
                }
            }
        });
        jQuery('.main-wrap-overflow').trigger('scroll');
    }

    function ajaxLoadAfterPublications() {
        var beforeFilterData = {
            bookId: '<?php echo $id; ?>'
        };
        jQuery.post(
            'index.php?option=com_plot&task=publication.ajaxGetAfterPublications',
            {userData: beforeFilterData},
            function (response) {
                if (response) {
                    var data = jQuery.parseJSON(response);
                    jQuery('#child-afterbook').append(data.renderedBooks);
                    jQuery('#loading-bar').hide();
                } else {
                    jQuery('#loading-bar').hide();
                }
            }
        );
        jQuery(window).unbind('scroll');
        jQuery('#child-afterbook').scrollPagination({
            nop: <?php echo plotGlobalConfig::getVar('booksResultsShowFirstCountChild'); ?>,
            offset: <?php echo plotGlobalConfig::getVar('booksResultsShowFirstCountChild'); ?>,
            error: '',
            delay: 10,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=publication.ajaxGetAfterPublications'); ?>',
            appendDataTo: 'child-afterbook',
            userData: beforeFilterData,
            afterLoad: {
                afterLoadAction: function () {
                    jPlotUp.Arrow.initialize('positionCameras');
                }
            }
        });
        jQuery('.main-wrap-overflow').trigger('scroll');
    }
    function fixFFBugWithFavicon() {
        jQuery('head link[href="<?php echo JUri::root(true);?>/templates/plot/favicon.ico"]').attr('href', '<?php echo JUri::root(true);?>/templates/plot/favicon.ico');
    }


    function courseBuy() {
        var userIdsCourseBuyFor = [];
        jQuery('#course-children-for .users-course-for-list .user-course-for').each(function (i, v) {
            userIdsCourseBuyFor.push(jQuery(this).attr('userid'));
        });

        if (userIdsCourseBuyFor.length) {
            jQuery.post(
                '<?php echo JRoute::_('index.php?option=com_plot&task=book.createOrder'); ?>',
                {
                    bookId: <?php echo $this->book->c_id; ?>,
                    usersIds: JSON.stringify(userIdsCourseBuyFor),
                    amount: jQuery('#reward').val()
                },
                function (response) {
                    var data = jQuery.parseJSON(response);
                    jQuery('#buy_book_hidden_form [name=userIds]').val(userIdsCourseBuyFor.join('-'));
                    jQuery('#buy_book_hidden_form [name=orderNumber]').val(data.orderId);
                    jQuery('#buy_book_hidden_form [name=rewardAmountForEach]').val(jQuery('#reward').val());
                    jQuery('#buy_book_hidden_form').submit();
                }
            );
        } else {
            SqueezeBox.initialize({
                size: {x: 300, y: 150}
            });
            var str = '<div id="enqueued-message">Не выбраны пользователи, для которых Вы хотите купить данную книгу</div>';
            SqueezeBox.setContent('adopt', str);
            SqueezeBox.resize({x: 300, y: 150});
        }
    }

    function calculateCourseCost() {
        jQuery.post('index.php?option=com_plot&task=book.calculateCosts', {
            bookId: <?php echo $this->book->c_id; ?>,
            enteredAmount: jQuery('#reward').val(),
            countChilds: jQuery('#course-children-for .users-course-for-list .user-course-for').length
        }, function (response) {
            var data = jQuery.parseJSON(response);
            jQuery('.admin-cost-calculated').html(data.costs.admin);
            jQuery('.author-cost-calculated').html(data.costs.author);
            jQuery('.payment-system-cost-calculated').html(data.costs.paymentSystem);
            jQuery('.total-cost-calculated').html(data.costs.total);
            jQuery('#buy_book_hidden_form [name=sum]').val(data.costs.total);
        });
    }

    jQuery(window).resize(function() {
        jQuery('.scroll-pane').jScrollPane();
    });

</script>


<style>
    .ui360 .sm2-canvas.hi-dpi {
        top: 0% !important;
        left: 0% !important;
    }

    .plot-book-url > svg use {
        filter: url("#inactive");
    }

    .plot-book-url:not(.denied):active > svg use {
        filter: url("#active");
    }
</style>


<!-- required -->
<link rel="stylesheet" type="text/css"
      href="<?php echo JUri::root() . 'components/com_plot/libraries/soundmanagerv2/demo/360-player/360player.css'; ?>"/>

<!-- special IE-only canvas fix -->
<!--[if IE]>
<script type="text/javascript"
        src="<?php echo JUri::root().'components/com_plot/libraries/soundmanagerv2/demo/360-player/script/excanvas.js'; ?>"></script><![endif]-->

<!-- Apache-licensed animation library -->
<script type="text/javascript"
        src="<?php echo JUri::root() . 'components/com_plot/libraries/soundmanagerv2/demo/360-player/script/berniecode-animator.js'; ?>"></script>

<!-- the core stuff -->
<script type="text/javascript"
        src="<?php echo JUri::root() . 'components/com_plot/libraries/soundmanagerv2/script/soundmanager2.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo JUri::root() . 'components/com_plot/libraries/soundmanagerv2/demo/360-player/script/360player.js'; ?>"></script>

<script type="text/javascript">
    soundManager.setup({
        // path to directory containing SM2 SWF
        url: '<?php echo JUri::root();?>components/com_plot/libraries/swf/'
    });
</script>

<script
    src="<?php echo JURI::base() . 'components/com_plot'; ?>/libraries/scrolling_ajax_pagination/javascript.js"></script>
<script src="<?php echo JUri::root(); ?>components/com_plot/assets/js/plot.js"></script>
<script type="text/javascript"
        src="<?php echo JUri::root(); ?>components/com_plot/views/publication/tmpl/default.js"></script>
<script type="text/javascript"
        src="<?php echo JUri::root(); ?>components/com_plot/assets/js/plotEssay.js"></script>
<main class="child-library">

<?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_header')); ?>

<div class="wrap main-wrap child-library">
<div class="add-left aside-left child-library"></div>
<div class="add-right aside-right child-library"></div>
<section class="child-library">
    <?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_top_menu')); ?>
    <div class="info-board child-library">
        <a href="#" onclick="window.location.href='<?php echo $this->referrerUrl; ?>';"
           class="link-back hover-shadow">
            Вернуться
            <svg viewBox="0 0 22.9 46" preserveAspectRatio="xMinYMin meet" class="butterfly">
                <use xlink:href="#butterfly"></use>
            </svg>
        </a>
        <svg viewBox="0 0 37.1 38.9" preserveAspectRatio="xMinYMin meet" class="leaf">
            <use xlink:href="#leaf"></use>
        </svg>
        <object style="visibility: visible;" name="bobr"
                data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/bobr-first-level.svg"
                type="image/svg+xml"></object>
        <div class="info-top">
            <form>
                <?php if ($this->book) { ?>
                    <span style="width: 267px;" class="no-before-content ui-selectmenu-button book-category">
                            <span class="ui-selectmenu-text"><?php echo $this->book->c_category; ?></span>
                        </span>
                <?php } ?>
            </form>
        </div>
        <div class="info-bottom">

            <?php if (  plotUser::factory()->isSiteAdmin() || plotUser::factory()->isBookAuthor($this->book->c_id) ||  PlotHelper::bookReadAccess($this->book->c_id)) { ?>
                <button class="test-button"
                    <?php if((int)PlotHelper::quizAccess((int)$this->book->id_quiz)){?>
                        onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_joomlaquiz&view=quiz&quiz_id=' . (int)$this->book->id_quiz); ?>'">
                    <?php }else{ ?>
                        onclick="createMsg('<?php echo JText::_("COM_PLOT_FE_AUTHORIZE_RIGHTS_VIEW_QUIZ"); ?>');  return false;">
                    <?php
                    }?>
                    <svg viewBox="0 0 57.5 57.1" preserveAspectRatio="xMidYMid meet">
                        <use xlink:href="#test"></use>
                    </svg>
                    Пройти тест
                </button>
            <?php }else{ ?>
                <button class="test-button"
                    <?php if(plotUser::factory()->id){?>
                        onclick="createMsg('<?php echo JText::_("COM_PLOT_FE_AUTHORIZE_RIGHTS_VIEW_QUIZ"); ?>');  return false;">
                    <?php }else{ ?>
                        onclick="createMsg('<?php echo JText::_("COM_PLOT_LOGIN_FIRST"); ?>');  return false;">
                    <?php   } ?>
                    <svg viewBox="0 0 57.5 57.1" preserveAspectRatio="xMidYMid meet">
                        <use xlink:href="#test"></use>
                    </svg>
                    Пройти тест
                </button>
            <?php   } ?>
            <?php if ($this->book && $this->bookobj->isGetPages()) { ?>
                <button class="test-button"
                        onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_html5flippingbook&view=publication&id=' . (int)$this->book->c_id . '&fullscreen=1'); ?>'">
                    <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet">
                        <use xlink:href="#book"></use>
                    </svg>
                    <?php echo JText::_("COM_PLOT_BOOK_ABOUT_BOOK"); ?>
                </button>

            <?php } ?>
        </div>
    </div>
</section>

<section class="my-progress child-library one-elem">
<div class="my-progress-wrapper">
<h3><?php echo $this->book ? $this->book->c_title : ""; ?></h3>
<?php if ($this->book) { ?>
<div class="jcarousel-wrapper my-books child-one-elem">

<div class="main-elem-info-wrap">
    <?php if ($this->book) { ?>

        <a id="book_link"  <?php echo ($this->bookobj->isGetPages()) ? 'class="thumbnail"' : 'class="nothumbnail"'; ?>
           rel="{handler: 'iframe', size: {x: auto, y:auto}}"
            <?php if (($this->bookobj->isGetPages())){ ?>
           href="<?php echo JRoute::_("index.php?option=com_html5flippingbook&view=publication&id=" . (int)$this->book->c_id . "&fullscreen=1"); ?>">
            <?php
            } else {
                ?>
                href="javascript:void(0)">
            <?php
            }?>
            <?php if (plotUser::factory()->id && PlotHelper::bookReadAccess($this->book->c_id)) { ?>
                <?php
                if (plotUser::factory()->id && ($this->book->new || $this->book->percent == 0)) {
                    ?>
                    <i class="in-progress">Новая</i>
                <?php
                }
                ?>
            <?php } ?>
            <figure>
                <img src="<?php echo $this->book->thumbnailUrl; ?>"/>
            </figure>
        </a>

    <?php } ?>
    <div class="bonus">
        <em class="pin-btn green"></em>
        <em class="pin-btn green"></em>

        <p><?php echo  JText::_('COM_PLOT_AFTER_BOOK_READ')?></p>

        <div>
            <i class="elka">+<?php echo $this->book ? (int)$this->book->count_points : 0; ?>
                <svg viewBox="0 0 14.6 23.1" preserveAspectRatio="xMinYMin meet">
                    <use xlink:href="#elka-interest"></use>
                </svg>
            </i>
            <?php if ($this->book && $this->my->getFinishedPriceForMyBook($this->book->c_id)) { ?>
                <p class="finance-info"><?php echo $this->my->getFinishedPriceForMyBook($this->book->c_id); ?>
                    <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet"
                         class="munze">
                        <use xlink:href="#munze"></use>
                    </svg>
                </p>
            <?php } ?>
        </div>
        <?php

        if ((int)PlotHelper::quizAccess((int)$this->book->id_quiz) && !$this->my->isBookTestFinished($this->book->c_id) && !$this->my->getFinishedBookByBookId($this->book->c_id)) {
            // $money = (int)$this->my->getFinishedPriceForMyBook($this->book->c_id) ? (int)$this->my->getFinishedPriceForMyBook($this->book->c_id) : 0;
            ?>
            <div class="weiter-schritt">
                <!--<?php echo   JText::sprintf('COM_PLOT_BOOK_BUY_BUT_TEST_NOT_FINISHED', '<a href="' . JRoute::_("index.php?option=com_plot&view=wallet") . '">'. '</a>'); ?>-->
                <?php echo JText::sprintf('COM_PLOT_BOOK_BUY_BUT_TEST_NOT_FINISHED', '<a class="modal" rel="{handler: \'iframe\', size: {x: 744, y: 469}}" href="'.JRoute::_("index.php?option=com_plot&view=wallet", false).'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>
            </div>
        <?php
        }?>

        <?php

        if ((int)PlotHelper::quizAccess((int)$this->book->id_quiz) && $this->my->isBookTestFinished($this->book->c_id) && !$this->my->getFinishedBookByBookId($this->book->c_id) && !$this->my->isEssayIsset($this->book->c_id)) {

            ?>
            <div class="weiter-schritt">
                <?php
                echo JText::_('COM_PLOT_BOOK_BUY_BUT_TEST_FINISHED');

                ?>

            </div>
        <?php
        }?>

        <?php
        if((int)PlotHelper::quizAccess((int)$this->book->id_quiz) && $this->my->isBookTestFinished($this->book->c_id) && !$this->my->getFinishedBookByBookId($this->book->c_id) && $this->my->isEssayIsset($this->book->c_id)){
            $buyer=$this->bookobj->whoBoughtBook($this->book->c_id, $this->my->id);
            ?>
            <div class="weiter-schritt">
                <?php echo JText::sprintf('COM_PLOT_BOOK_AND_TEST_FINISHED_ESSAY_CREATED','<br/><a href="'.JRoute::_("index.php?option=com_plot&view=profile&id=".(int)$buyer).'">'.plotUser::factory($buyer)->name.'</a>');?>
            </div>
        <?php
        }
        ?>

        <?php
        if ((int)PlotHelper::quizAccess((int)$this->book->id_quiz) && $this->my->getFinishedBookByBookId($this->book->c_id)) {
            ?>
            <div class="weiter-schritt">
                <?php echo JText::sprintf('COM_PLOT_BOOK_AND_TEST_FINISHED', '<a class="modal" rel="{handler: \'iframe\', size: {x: 744, y: 469}}" href="'.JRoute::_("index.php?option=com_plot&view=wallet", false).'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>
            </div>
        <?php
        }
        ?>
    </div>
    <div class="wrapper">
        <div class="sticker">
            Рейтинг книги:<br/>

            <div class="bought">Куплено: <?php echo $this->bookratingBay; ?></div>
            <div class="studied">Прочитано: <?php echo $this->ratingRead; ?></div>
            <div class="clr"></div>
        </div>
    </div>
    <?php if ($this->bookFiles) {
        ?>
        <ul class="plot-book-files">
            <?php  foreach ($this->bookFiles AS $file) {

                ?>
                <li>
                    <?php
                    switch ($file->type) {
                        case 'audio':
                            ?>
                            <?php if (PlotHelper::bookReadAccess($this->book->c_id)) { ?>
                            <div class="ui360 plot-book-<?php echo $file->type; ?>"><a
                                    href="<?php echo '/media/com_plot/bookfiles/' . $file->path; ?>"></a>
                            </div>
                        <?php
                        } else {
                            ?>
                            <a class="denied audio" href="javascript:void(0);" onclick="bookAccessDenied();"></a>
                        <?php

                        }
                            break;
                        case 'url':
                            if (PlotHelper::bookReadAccess($this->book->c_id)) {
                                ?>
                                <a class="plot-book-<?php echo $file->type; ?>" href="<?php echo $file->path; ?>"
                                   target="_blank">
                                    <svg viewBox="0 0 153 155" preserveAspectRatio="xMidYMax meet">
                                        <g>
                                            <use xlink:href="#url-icon"></use>
                                        </g>
                                    </svg>
                                </a>
                            <?php
                            } else {
                                ?>
                                <a class="denied plot-book-<?php echo $file->type; ?>" href="javascript:void(0);"
                                   onclick="bookAccessDenied();">
                                    <svg viewBox="0 0 153 155" preserveAspectRatio="xMidYMax meet">
                                        <g>
                                            <use xlink:href="#url-icon"></use>
                                        </g>
                                    </svg>
                                </a>
                            <?php
                            }
                            break;
                        default:
                            if (PlotHelper::bookReadAccess($this->book->c_id)) {
                                ?>
                                <a class="button-3d plot-book-<?php echo $file->type; ?>"
                                   href="<?php echo '/media/com_plot/bookfiles/' . $file->path; ?>"
                                   download="<?php echo $file->path; ?>"
                                   target="_blank"><?php echo $file->type; ?></a>
                            <?php
                            } else {
                                ?>
                                <a class="button-3d denied plot-book-<?php echo $file->type; ?>"
                                   href="javascript:void(0);"
                                   onclick="bookAccessDenied();"><?php echo $file->type; ?></a>
                            <?php
                            }
                    }
                    ?>
                </li>
            <?php
            }
            ?>
        </ul>
    <?php
    }?>
</div>
<h4>Аннотация:</h4>

<div class="about">
    <em class="pin-btn green"></em>
    <em class="pin-btn green"></em>
    <strong>Автор: <?php
        #echo plotUser::factory($this->book->pay_author)->name;
        echo $this->book ? $this->book->c_author : "";
        ?></strong>

    <p>
        <?php echo $this->book ? $this->book->c_pub_descr : ""; ?>
    </p>
</div>
<h4>Купить эту книгу:</h4>

<div class="buy">
    <em class="pin-btn green"></em>
    <em class="pin-btn green"></em>

    <form id="course-children-for">
        <fieldset>
            <label for="recipients-course">Выберите кому:</label>

            <div class="users-course-for-list"></div>
            <button class="add-recipient" name="choose"
                    onclick="SqueezeBox.open('<?php echo JRoute::_('index.php?option=com_plot&task=searchusersbook.showPopup&bookId=' . JRequest::getInt('bookId', 0)); ?>', {size: {x: 600, y: 450}, handler: 'ajax'});">
                <svg viewBox="0 0 31.7 30.8" preserveAspectRatio="xMinYMin meet" class="friend">
                    <use xlink:href="#friend"></use>
                </svg>
                Добавить
            </button>
        </fieldset>
    </form>
    <div>
        <div>
            <p>Вознаграждение за прочтение:</p>
            <span class="abs">1 руб = 1 монета</span>
            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                <use xlink:href="#munze"></use>
            </svg>
            <input type="text" id="reward"
                   value="<?php echo plotGlobalConfig::getVar('payForBookCompleteDefault'); ?>"/>
        </div>
        <div>
            <p><?php echo JText::_("COM_PLOT_ADMINISTRATOR_DONATION"); ?></p>
            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                <use xlink:href="#munze"></use>
            </svg>
            <i class="admin-cost-calculated"><?php echo $this->book->costsForBuyers['admin']; ?></i>
        </div>
        <div>
            <p><?php echo  JText::_('COM_PLOT_BOOK_REMUNERATION_TO_AUTHOR_TEST')?>:</p>
            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                <use xlink:href="#munze"></use>
            </svg>
            <i class="author-cost-calculated"><?php echo $this->book->costsForBuyers['author']; ?></i>
        </div>
        <div>
            <p>Комиссия платежной системы:</p>
            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                <use xlink:href="#munze"></use>
            </svg>
            <i class="payment-system-cost-calculated"><?php echo $this->book->costsForBuyers['paymentSystem']; ?></i>
        </div>
        <span>Итого:</span>
        <b><span class="total-cost-calculated"><?php echo $this->book->costsForBuyers['total']; ?></span> руб</b>

        <form action="https://money.yandex.ru/eshop.xml" method="post" name="buy_book_hidden_form"
              id="buy_book_hidden_form">
            <!-- Обязательные поля -->
            <input name="shopId" class="hidden" value="<?php echo plotGlobalConfig::getVar('yandexShopId'); ?>"
                   type="text"/>
            <input name="scid" class="hidden" value="<?php echo plotGlobalConfig::getVar('yandexScid'); ?>"
                   type="text"/>
            <input name="sum" class="hidden" value="0" type="text"/>
            <input name="customerNumber" class="hidden" value="<?php echo plotUser::factory()->id; ?>" type="text"/>
            <!-- Необязательные поля -->
            <div class="payment-type">
                <p>Способ оплаты:</p>
                <select name="paymentType" id="paymentType">
                    <option value="AC">Банковская карта</option>
                    <option value="PC">Яндекс кошелек</option>
                    <option value="GP">Через кассы и терминалы</option>
                    <option value="WM">Кошелек WM</option>
                </select>
            </div>
            <input name="orderNumber" class="hidden" value="0" type="text"/>
            <input name="cps_email" class="hidden" value="<?php echo  plotUser::factory()->email; ?>" type="text"/>
            <input name="shopSuccessURL" class="hidden"
                   value="http://naplotu.com/publication/<?php echo $this->book->c_id; ?>/success=1"
                   type="text"/>
            <input name="shopFailURL" class="hidden"
                   value="http://naplotu.com/publication/<?php echo $this->book->c_id; ?>/success=0"
                   type="text"/>
            <input name="bookId" class="hidden" value="<?php echo $this->book->c_id; ?>" type="text"/>
            <input name="entity" class="hidden" value="book" type="text"/>
            <input name="rewardAmountForEach" class="hidden" value="0" type="text"/>
            <input name="userIds" class="hidden" value="" type="text"/>
        </form>
        <?php if((int)$this->my->id){ ?>
            <input type="submit" onclick="courseBuy();" value="<?php echo  JText::_('COM_PLOT_CHECKOUT')?>"/>
        <?php }else{
            ?>
            <input type="submit" onclick="createMsg('<?php echo JText::_("COM_PLOT_LOGIN_FIRST"); ?>');" value="<?php echo  JText::_('COM_PLOT_CHECKOUT')?>"/>
        <?php
        } ?>
    </div>
</div>
<h4>Книга включена в программы:</h4>

<div class="programs-list" id="child-programs-list">
    <em class="pin-btn yellow"></em>
    <em class="pin-btn yellow"></em>

    <div class="jcarousel">
        <?php if ($this->bookPrograms) { ?>
            <ul class="child-profile">
                <?php
                foreach ($this->bookPrograms AS $program) {
                    ?>
                    <li>
                        <a href="<?php echo $program->link; ?>">
                            <img src="<?php echo $program->img; ?>">

                            <div class="plot-book-programs" data-progress="<?php echo $program->percent; ?>"></div>
                            <h5><?php echo $program->title; ?></h5>

                        </a>
                    </li>
                <?php
                }
                ?>
                ?>
            </ul>
        <?php } else { ?>
            <p class="no-items"><?php echo JText::sprintf('COM_PLOT_THIS_BOOK_NOT_INCLUDE_IN PROGRAMS', $this->book->c_title); ?></p>
        <?php
        }
        ?>
    </div>
    <a href="javascript:void(0);" class="jcarousel-control-prev">&#60</a>
    <a href="javascript:void(0);" class="jcarousel-control-next">&#62</a>
</div>


<div id="child-media-tabs">
    <div class="info-board essay">
        <ul class="child-media-tabs" id="all-tabs">
            <li id="link-child-essay"><a href="#all-child-essay" class="active">Эссе</a></li>
            <!--<li><a href="#all-child-beforebook" id="link-child-beforebook">Книги до</a></li>
            <li><a href="#all-child-afterbook" id="link-child-afterbook">Книги после</a></li>-->
            <li id="link-child-disqus"><a href="#all-child-disqus">Комментарии</a></li>
        </ul>
    </div>
    <div id="loading-bar" style="margin-left: 5px;"><img
            src="<?php echo $this->baseurl ?>/templates/plot/img/pre-loader-1.gif"/></div>
    <div id="all-child-essay" class="essay-inner child-profile  new-event">

        <?php

        if ((int)plotUser::factory()->isBookFinished($this->book->c_id)==1 && plotUser::factory()->id && !plotUser::factory()->essayIsWritten($this->book->c_id)) {
            ?>
            <div class="essay-theme">

                <?php if ($this->book && $this->book->text) { ?>
                    <h5><?php echo  JText::_("COM_PLOT_COURSE_LIST_TOPICS_FOR_ESSAY");?> </h5>
                    <div> <?php   echo  html_entity_decode($this->book->text); ?></div>
                <?php  }?>
            </div>
            <div class="create-essay">
                <button class="add-recipient hover-shadow" id="plot-essay-create-button"
                        onclick="plotEssay.showElement('essay-area').hideElement('plot-essay-create-button')"><?php echo JText::_('COM_PLOT_CREATE_ESSAY'); ?></button>
                <div id="essay-area" style="display: none;">
                    <textarea id="plot-user-textarea" placeholder="Заполните ессе" maxlength="<?php echo plotGlobalConfig::getVar('essayTextareaMaxSymbols')?>"></textarea>
                    <?php echo  JText::_('COM_PLOT_COUNT_ENTER_SYMBOLS');?><span id="textarea_message"><?php echo plotGlobalConfig::getVar('essayTextareaMaxSymbols')?></span>
                    <button class="button cancel-act hover-shadow"
                            onclick="plotEssay.hideElement('essay-area').showElement('plot-essay-create-button');">
                        <span><?php echo JText::_('COM_PLOT_CANCEL') ?></span></button>
                    <button class="button accept hover-shadow"
                            onclick="plotEssay.essayValidate('plot-user-textarea','<?php echo $this->book->c_id; ?>','#link-child-essay').hideElement('essay-area').showElement('plot-essay-create-button');">
                        <span><?php echo JText::_('COM_PLOT_SEND'); ?></span>
                    </button>
                </div>
            </div>
        <?php
        }
        ?>

        <?php
        } else {
            echo 'Книга не найдена';
        }?>

        <ul class="no-jcarousel" id="essay-list"></ul>
    </div>

    <!--    <div id="all-child-beforebook" class="no-jcarousel new-event">

            <ul class="child-profile" id="child-beforebook"></ul>
        </div>
        <div id="all-child-afterbook" class="no-jcarousel new-event">

            <ul class="child-profile" id="child-afterbook"></ul>
        </div>-->
    <div id="all-child-disqus" class="new-event">
        <?php echo PlotHelper::renderDisqus('publication_' . $id); ?>
        <div id="disqus_thread"></div>
    </div>
</div>
</div>


</section>
</div>
</main>

