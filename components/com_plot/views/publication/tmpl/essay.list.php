<?php
defined('_JEXEC') or die;

if ((int)$this->essay['countItems']) { ?>

    <?php foreach ($this->essay['items'] AS $key => $essay) { ?>
        <li>
            <?php
            if($essay->reviewer==plotUser::factory()->id && $essay->status==0){
            ?>
            <a href="javascript:void(0);" onclick="SqueezeBox.open('<?php echo  JRoute::_("index.php?option=com_plot&task=essay.ajaxEssay"); ?>&id=<?php echo  $essay->id; ?>', {size:{x:<?php echo $essay->screenWidth; ?>, y:<?php echo $essay->screenHeight;?>})">
                <?php  }else{
                ?>
                <a class="fancybox essay-<?php echo $essay->id; ?>" rel="photos-set"
                   <?php if($essay->status){ ?>
                   data-share=" data-image='<?php echo $essay->original; ?>' data-url='<?php echo $essay->original; ?>' "
                   <?php } ?>
                   href="<?php echo $essay->original; ?>"  >
                    <?php
                    }
                    ?>
                    <h6>
                        <i><svg viewBox="0 0 24.5 26.4"><use xlink:href="#photo"></use></svg></i>
                        <span class="action-title">Добавил:</span>
                        <p class="stream-title"><?php echo  plotUser::factory($essay->user_id)->name?></p>
                    </h6>
                    <data><?php echo JFactory::getDate($essay->date)->format('d.m.Y H:i'); ?></data>
                    <div>
                        <img class="essay" src="<?php echo JURI::base() . 'templates/plot'; ?>/img/default-essay.jpg"/>
                        <i class="activity-icons">
                            <svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                <use xlink:href="#zoom"></use>
                            </svg>
                        </i>
                    </div>
                    <?php if ($essay->text) { ?>
                        <div>
                            <?php echo PlotHelper::cropStr(strip_tags($essay->text), plotGlobalConfig::getVar('booksDescriptionMaxSymbolsToShow')); ?>
                        </div>
                    <?php } ?>

                </a>


        </li>
    <?php } ?>

<?php } ?>

<?php
if (!$this->essay['countItems']) {
    echo '<div class="p5_20">'.JText::_('COM_PLOT_NO_COURSES_FOUND').'</div>';
}
?>