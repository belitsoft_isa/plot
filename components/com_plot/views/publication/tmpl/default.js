//<editor-fold defaultstate="collapsed" desc="STRING FUNCTIONS">
function strpos(haystack, needle, offset) {
    var i = (haystack + '')
        .indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
}

function strripos(haystack, needle, offset) {
    haystack = (haystack + '')
        .toLowerCase();
    needle = (needle + '')
        .toLowerCase();

    var i = -1;
    if (offset) {
        i = (haystack + '')
            .slice(offset)
            .lastIndexOf(needle); // strrpos' offset indicates starting point of range till end,
        // while lastIndexOf's optional 2nd argument indicates ending point of range from the beginning
        if (i !== -1) {
            i += offset;
        }
    } else {
        i = (haystack + '')
            .lastIndexOf(needle);
    }
    return i >= 0 ? i : false;
}

function substr(str, start, len) {
    var i = 0,
        allBMP = true,
        es = 0,
        el = 0,
        se = 0,
        ret = '';
    str += '';
    var end = str.length;

    // BEGIN REDUNDANT
    this.php_js = this.php_js || {};
    this.php_js.ini = this.php_js.ini || {};
    // END REDUNDANT
    switch ((this.php_js.ini['unicode.semantics'] && this.php_js.ini['unicode.semantics'].local_value.toLowerCase())) {
        case 'on':
            // Full-blown Unicode including non-Basic-Multilingual-Plane characters
            // strlen()
            for (i = 0; i < str.length; i++) {
                if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
                    allBMP = false;
                    break;
                }
            }
            if (!allBMP) {
                if (start < 0) {
                    for (i = end - 1, es = (start += end); i >= es; i--) {
                        if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
                            start--;
                            es--;
                        }
                    }
                } else {
                    var surrogatePairs = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;
                    while ((surrogatePairs.exec(str)) != null) {
                        var li = surrogatePairs.lastIndex;
                        if (li - 2 < start) {
                            start++;
                        } else {
                            break;
                        }
                    }
                }

                if (start >= end || start < 0) {
                    return false;
                }
                if (len < 0) {
                    for (i = end - 1, el = (end += len); i >= el; i--) {
                        if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
                            end--;
                            el--;
                        }
                    }
                    if (start > end) {
                        return false;
                    }
                    return str.slice(start, end);
                } else {
                    se = start + len;
                    for (i = start; i < se; i++) {
                        ret += str.charAt(i);
                        if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
                            se++; // Go one further, since one of the "characters" is part of a surrogate pair
                        }
                    }
                    return ret;
                }
                break;
            }
        // Fall-through
        case 'off':
        default:
            if (start < 0) {
                start += end;
            }
            end = typeof len === 'undefined' ? end : (len < 0 ? len + end : len + start);
            return start >= str.length || start < 0 || start > end ? !1 : str.slice(start, end);
    }
    return undefined;
}
//</editor-fold>

function removeSearchActiveClassTab() {
    jQuery('#link-child-beforebook').removeClass('ui-tabs-active');
    jQuery('#child-beforebook').css({'display': 'none'});
    jQuery('#link-child-afterbook').removeClass('ui-tabs-active');
    jQuery('#child-afterbook').css({'display': 'none'});
    jQuery('#link-child-essay').removeClass('ui-tabs-active');
    jQuery('#child-essay').css({'display': 'none'});
    jQuery('#link-child-disqus').removeClass('ui-tabs-active');
    jQuery('#child-disqus').css({'display': 'none'});
}

function setActiveTab(tab, search_area) {
    jQuery('#' + tab).addClass('active');
    jQuery('#' + search_area).css({'display': 'block'});
}

function createMsg(msg){
    SqueezeBox.initialize({
        size: {x: 300, y: 150}
    });
    var str = '<div id="enqueued-message">'+msg+'</div>';
    SqueezeBox.setContent('adopt', str);
    SqueezeBox.resize({x: 300, y: 150});
}

jQuery(document).ready(function (jQuery) {
    

    SqueezeBox.initialize();

    jQuery('#link-child-essay').on('click', function () {
        jQuery('#essay-list').html('');
        ajaxLoadEssay();
        jQuery('.main-wrap-overflow').trigger('scroll');
    });

    jQuery('#link-child-beforebook').on('click', function () {
        jQuery('#child-beforebook').html('');
        ajaxLoadBeforePublications();
    });
    jQuery('#link-child-afterbook').on('click', function () {
        jQuery('#child-afterbook').html('');
        ajaxLoadAfterPublications();
    });

  var win_hash = window.location.hash;
    switch(win_hash) {
        case '#all-child-essay':
        case '#essay':
            document.getElementById('link-child-essay').click();
            break;

        case '#all-child-disqus':
        case '#disqus':
            document.getElementById('link-child-disqus').click();
            break;
        default:
            document.getElementById('link-child-essay').click();
    }
        // modalLoad();
    jQuery("#activity-feed, #child-add-activity, #child-add-video, #child-media-tabs").tabs();

    fixFFBugWithFavicon();


    jQuery("#recipient-1").selectmenu({ change: function( event, ui ) {changeSelectChildMenu(event, ui);} });

    // add / remove new children course buy for
    jQuery('#course-children-for').submit(function(){
        if ( jQuery('#course-children-for [name=add]').val() == '1' ) {
            var lastSelectIdNum = parseInt(jQuery('select[id^=recipient-]:last').attr('id').replace(/[^0-9]/g, ''));
            var selectIdNew = 'recipient-' + (lastSelectIdNum + 1);
            var selectedChildrens = [];
            var newOptions = '';

            jQuery('select[id^=recipient-] > option:selected').each(function(){
                selectedChildrens.push(jQuery(this).val());
            });

            jQuery('#recipient-1 option').each(function () {
                if (jQuery.inArray(jQuery(this).val(), selectedChildrens) === -1) {
                    newOptions += jQuery(this)[0].outerHTML;
                }
            });

            if (newOptions) {
                jQuery('<select id="' + selectIdNew + '">' + newOptions + '</div>').insertBefore('#course-children-for > fieldset .add-recipient:first');
                //jQuery('#course-children-for > fieldset').append('<select id="' + selectIdNew + '">' + newOptions + '</div>');
                jQuery("#" + selectIdNew).selectmenu({ change: function( event, ui ) {changeSelectChildMenu(event, ui);} });
            }
            // remove selected in new from each other selects
            var selectedValInNewText = jQuery('select[id^=recipient-]:last > option:selected').val();

            jQuery('select[id^=recipient-]:not(:last) option:not(:selected)').each(function(){
                if (jQuery(this).val() == selectedValInNewText) {
                    jQuery(this).remove();
                }
            });
            calculateCourseCost();
        } else if (jQuery('select[id^=recipient-]').length > 1) {
            var selectedOptionHtml = jQuery('<div>').append(jQuery('select[id^=recipient-]:last option:selected').clone()).html();
            jQuery('select[id^=recipient-]:not(:last)').each(function(){
                jQuery(this).append(selectedOptionHtml);
            });
            jQuery('select[id^=recipient-]:last').remove();
            calculateCourseCost();
        }

        jQuery('#course-children-for [name=add]').val('0');
        jQuery('select[id^=recipient-]').selectmenu('refresh');

        return false;
    });

    jQuery('#reward').mask('0000000');

    calculateCourseCost();
    jQuery('#reward').keyup(function(){ calculateCourseCost(); });
    jQuery('#reward').change(function(){ calculateCourseCost(); });

    jQuery('#course-children-for .users-course-for-list').on('click', '.remove', function(){
        jQuery(this).parent().fadeOut(300, function(){
            jQuery(this).remove();
            calculateCourseCost();
        })
    });



    jQuery(".fancybox").fancybox({
        maxWidth	: 800,
        maxHeight	: 600,
        fitToView	: false,
        width		: '70%',
        height		: '70%',
        autoSize	: false,
        closeClick	: false,
        openEffect	: 'none',
        closeEffect	: 'none',
        helpers:  {
            title : {
                type : 'inside',
                position: 'top'
            }
        },
        beforeLoad: function() {
            this.title = jQuery(this.element).attr('data-title'),
                this.date = jQuery(this.element).attr('data-date'),
                this.description = jQuery(this.element).attr('data-description'),
                this.share = jQuery(this.element).attr('data-share');
        }
    });


      jQuery('#plot-user-textarea').keyup(function(){
          var maxLength = jQuery(this).attr('maxlength');
          if(this.value.length < (maxLength+2)){
              jQuery('#textarea_message').html(maxLength-this.value.length);
          }
    });



});

function updateFancybox(){
    jQuery(".fancybox").fancybox({
        maxWidth	: 800,
        maxHeight	: 600,
        fitToView	: false,
        width		: '70%',
        height		: '70%',
        autoSize	: false,
        closeClick	: false,
        openEffect	: 'none',
        closeEffect	: 'none',
        helpers:  {
            title : {
                type : 'inside',
                position: 'top'
            }
        },
        beforeLoad: function() {
            this.title = jQuery(this.element).attr('data-title'),
                this.date = jQuery(this.element).attr('data-date'),
                this.description = jQuery(this.element).attr('data-description'),
                this.share = jQuery(this.element).attr('data-share');
        }
    });
}


function changeSelectChildMenu(event, ui)
{
    var selectedOption = jQuery(event.target).find('option:selected');
    var allOptionsExcludeSelectedChildrensIds = [];
    var currentSelectId = jQuery(event.target).prop('id');

    jQuery(event.target).find('option:not(:selected)').each(function(){
        allOptionsExcludeSelectedChildrensIds.push(jQuery(this).val());
    });

    jQuery('select[id^=recipient-]').each(function(){
        if (jQuery(this).prop('id') != currentSelectId) {
            var elementIterator = jQuery(this);
            allOptionsExcludeSelectedChildrensIds.each(function(v){
                if (!elementIterator.find('option[value='+v+']').length) {
                    elementIterator.append('<option value="' + v + '">' + jQuery(event.target).find('option[value='+v+']').text() + '</option>');
                }
            });
            jQuery(this).find('option[value='+selectedOption.val()+']').remove();
        }
    });

    jQuery('select[id^=recipient-]').selectmenu('refresh');
}



