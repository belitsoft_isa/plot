<?php
defined('_JEXEC') or die;

if ($this->books['items']) { ?>

                <?php foreach ($this->books['items'] AS $key => $book) { ?>
                <li>
                    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId='.$book->c_id); ?>">
                        <figure><img src="<?php echo $book->thumbnailUrl; ?>" alt="<?php htmlspecialchars($book->c_title); ?>"/></figure>
                    </a>
                    <?php if ($book->c_pub_descr) { ?>
                    <div class="elem-descr">
                        <?php echo PlotHelper::cropStr(strip_tags($book->c_pub_descr), plotGlobalConfig::getVar('booksDescriptionMaxSymbolsToShow')); ?>
                    </div>
                    <?php } ?>
                </li>
                <?php } ?>

<?php } ?>

