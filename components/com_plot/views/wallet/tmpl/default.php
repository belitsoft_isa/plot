<?php
defined('_JEXEC') or die;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->templateUrl; ?>/css/style.css" />

<script src="<?php echo JUri::root();?>media/jui/js/jquery.min.js"></script>
<script src="<?php echo JUri::root(); ?>templates/plot/js/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo juri::root();?>templates/plot/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo juri::root();?>templates/plot/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/libraries/jquery_input_mask_plugin/jquery.mask.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root().'components/com_plot/views/wallet/tmpl/default.js'; ?>"></script>

<?php # <editor-fold defaultstate="collapsed" desc="CSS"> ?>
<style type="text/css">
#child-cash-container {
    padding-bottom: 0;
}
#child-cash .cash-operation {
    padding-bottom: 0;
}
#child-cash .cash-operation {
    width: 34.5%;
}
#cashout-form .cashout-method {
    margin-bottom: 0px;
}
#cashout-form input[type=submit] {
    margin-top: 8px; 
    margin-bottom: 7px;
}
</style>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="SVG"> ?>
<svg xmlns="http://www.w3.org/2000/svg" style="position: absolute; left: -9999px;">
    <symbol id="munze" viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet">
        <ellipse class="st0" cx="12.6" cy="15.3" rx="12.1" ry="5.4"/>
        <ellipse class="st0" cx="12.6" cy="12.3" rx="12.1" ry="5.4"/>
        <ellipse class="st0" cx="12.6" cy="9.1" rx="12.1" ry="5.4"/>
        <ellipse class="st0" cx="12.6" cy="5.9" rx="12.1" ry="5.4"/>
    </symbol>
    <symbol id="cash" viewBox="0 0 17.3 22.2" preserveAspectRatio="xMinYMin meet">
        <path class="st0" d="M1.2,3.9L11,0c0.9,0,1.2,0.3,1.2,1.2v15c0,0.6-0.3,1.8-1.2,1.8l-6.9,1.5c-0.6,0-0.6,0.6,0,0.6l8.1-1.5c0.9,0,1.2-0.9,1.2-1.5V4.2h0.9h0.6h0.6c0.9,0,1.8,0.6,1.8,1.5v15c0,0.9-0.9,1.5-1.8,1.5L2.4,21.9c-0.3,0-1.2,0-1.5,0c-0.6,0-0.9-0.6-0.9-1.2v-15C0,5.1,0.6,4.2,1.2,3.9L1.2,3.9z M6.9,7.5c0.6-0.6,1.8-0.6,2.4,0c0.6,0.6,0.3,1.8-0.6,2.4c-0.6,0.6-1.8,0.6-2.4,0C5.7,9.3,6,8.1,6.9,7.5L6.9,7.5z M7.2,8.1c0.6-0.6,1.2-0.6,1.5,0C9,8.4,9,9,8.4,9.6c-0.6,0.3-1.2,0.3-1.5,0C6.6,9,6.6,8.4,7.2,8.1L7.2,8.1z M14.3,5.1v12.6c0,0.6-0.3,1.2-1.2,1.5l-9,1.5c-1.8,0.3-1.8-1.5-0.6-1.5l7.2-1.8c0.6,0,0.9-0.9,0.9-1.5V2.1c0-0.9-0.3-1.2-0.9-0.9L1.5,4.5C0.9,4.8,0.6,5.4,0.6,6.3v13.8c0,0.6,0.3,1.2,0.9,1.2h13.4c0.9,0,1.5-0.6,1.5-1.2V6.3c0-0.6-0.6-1.2-1.5-1.2H14.3z"/>
    </symbol>
</svg>
<?php # </editor-fold> ?>

<div id="child-cash-container" class="wrap popup-style">
    <div id="child-cash">
        <ul class="child-cash-header">
            <li><a href="#child-my-cash">
                <svg viewBox="0 0 17.3 22.2" preserveAspectRatio="xMinYMin meet"><use xlink:href="#cash"></use></svg>
                Мой кошелек
            </a>
            </li>
            <li><a href="#child-cash-in">История заработанных денег</a></li>
            <li><a href="#child-cash-out">История вывода денег</a></li>
        </ul>
        <div id="child-my-cash">
            <div class="scroll-pane wrap-for-scrollscript">
                <div class="payment-description">
                    <?php echo PlotHelper::getk2item( plotGlobalConfig::getVar('walletChildK2ItemTextId') )->introtext; ?>
                </div>
            </div>
            <div class="cash-operation">
                <h2>
                    <svg viewBox="0 0 17.3 22.2" preserveAspectRatio="xMinYMin meet"><use xlink:href="#cash"></use></svg>
                    Мой кошелек
                </h2>
                <span>Мой баланс:</span>
                <b><?php echo $this->my->money; ?> руб</b>
                <form id="cashout-form" action="<?php echo JRoute::_('index.php?option=com_plot&task=wallet.cashout'); ?>" method="POST">
                    <fieldset>
                        <div>
                            <label for="amount">Сумма:</label>
                            <input type="text" id="amount" name="amount" />
                            <span class="abs">1 руб = 1 монета</span>
                            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze"><use xlink:href="#munze"></use></svg>
                        </div>
                        <div class="cashout-method">
                            <label for="cash-out-method">Способ вывода:</label>
                            <select id="cash-out-method" name="cash-out-method">
                                <option value="yandexWallet">Яндекс кошелек</option>
                                <option value="yandexPhone">Номер телефона</option>
                            </select>
                            <label for="cash-out-destination">Номер Яндекс кошелька:</label>
                            <input type="text" id="cash-out-destination" name="cash-out-destination" />
                        </div>
                    </fieldset>
                    <input type="submit" value="Вывести" />
                </form>
                
            </div>
        </div>
        <div id="child-cash-in">
            <h2>Всего заработано денег: <span><?php echo $this->sumOfAllFinishedPrices; ?></span>
                <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze"><use xlink:href="#munze"></use></svg>
            </h2>
                <table>
                    <thead>
                        <tr>
                            <th>№</th>
                            <th>Дата</th>
                            <th>Мои достижения</th>
                            <th>Сумма</th>
                        </tr>
                    </thead>
                    <tbody class="scroll-pane wrap-for-scrollscript">
                            <?php $i = 1; foreach ($this->my->finishedBooksCourses AS $finishedEvent) { ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td class="date"><?php echo JFactory::getDate($finishedEvent->finished_date)->format('d.m.Y H:i'); ?></td>
                                <td>
                                    <?php if (isset($finishedEvent->course_id)) { ?>
                                        Пройден курс 
                                        <a onclick="window.parent.location.href='<?php echo JRoute::_('index.php?option=com_plot&view=course&id='.$finishedEvent->course_id); ?>';" 
                                           href="javascript:void(0);">
                                            "<?php echo $finishedEvent->title; ?>"
                                        </a>
                                    <?php } elseif (isset($finishedEvent->book_id)) { ?>
                                        Прочитал(а) книгу 
                                        <a onclick="window.parent.location.href='<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId='.$finishedEvent->book_id); ?>';" 
                                           href="javascript:void(0);">
                                            "<?php echo $finishedEvent->title; ?>"
                                        </a>
                                    <?php }elseif(isset($finishedEvent->entityId)){
                                        if($finishedEvent->entity=='book'){
                                            ?>
                                            Авторское вознаграждение за тесты к книге <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId='.$finishedEvent->entityId); ?>">"<?php echo $finishedEvent->title; ?>"</a>
                                        <?php
                                        }elseif($finishedEvent->entity=='course'){
                                            ?>
                                            Авторское вознаграждение за курс <a href="<?php echo JRoute::_('index.php?option=com_plot&view=course&id='.$finishedEvent->entityId); ?>">"<?php echo $finishedEvent->title; ?>"</a>
                                        <?php
                                        }
                                    } ?>
                                </td>
                                <td class="sum"> <?php echo $finishedEvent->finished_price; ?>
                                    <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze"><use xlink:href="#munze"></use></svg>
                                </td>
                            </tr>
                            <?php } ?>                        
                    </tbody>
                </table>
        </div>
        <div id="child-cash-out">
            <h2>Всего выведено денег: <span><?php echo $this->my->getCashoutAmountTotal(); ?></span>
                <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                    <use xlink:href="#munze"></use>
                </svg>
            </h2>
            <table>
                <thead>
                    <tr>
                        <th>№</th>
                        <th>Дата</th>
                        <th>Способы вывода денег</th>
                        <th>Сумма</th>
                    </tr>
                </thead>
                <tbody class="scroll-pane wrap-for-scrollscript">
                    <?php $i =1; foreach ($this->my->cashouts AS $cashout) { ?>
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td class="date"><?php echo JFactory::getDate($cashout->date)->format('d.m.Y H:i'); ?></td>
                        <td>
                            <?php if ($cashout->cashoutMethod == 'card') { ?>
                            На банковскую карту
                            <?php } ?>
                            <?php if ($cashout->cashoutMethod == 'yandexWallet') { ?>
                            на кошелек в Яндекс.Деньгах
                            <?php } ?>
                        </td>
                        <td class="sum"> <?php echo $cashout->amount; ?>
                            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze"><use xlink:href="#munze"></use></svg>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
