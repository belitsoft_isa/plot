jQuery(document).ready(function(){

    jQuery('#cashout-form').submit(function(){
        var amount = jQuery(this).find('#amount').val();
        var cashoutMethod = jQuery(this).find('#cash-out-method').val();
        var cashoutDestination = jQuery(this).find('#cash-out-destination').val();
        var formAction = jQuery(this).attr('action');

        jQuery('<form>', {
            "id": "child-iframe-cashout-form",
            "method": "POST",
            "class": "hidden",
            "html": '<input type="text" name="amount" value="'+amount+'" /><input type="text" name="cash-out-method" value="'+cashoutMethod+'" /><input type="text" name="cash-out-destination" value="'+cashoutDestination+'" />',
            "action": formAction
        }).appendTo(parent.document.body).submit();

        jQuery(this).find('input[type=submit]').attr('disabled', true);

        return false;
    });

    jQuery('#cashout-form #amount').mask('######');
    jQuery('body').addClass('popup-style');
    jQuery('.scroll-pane').jScrollPane();
    jQuery( "#child-cash" ).tabs();
    
    jQuery("#cash-out-method").selectmenu({
        change: function(event, ui) {
            if (ui.item.value === 'yandexWallet') {
                jQuery('label[for=cash-out-destination]').html('Номер Яндекс кошелька:');
            } else {
                jQuery('label[for=cash-out-destination]').html('Номер телефона:');
            }
        }
    });
    
});