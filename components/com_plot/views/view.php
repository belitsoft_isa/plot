<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewLegacy extends JViewLegacy
{
    
    public function __construct($config = array())
    {
        $this->templateUrl = JUri::root().'templates/plot';
        parent::__construct($config);
    }
    
    public function redirect404()
    {
        JFactory::getApplication()->redirect( JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('error404K2ItemId')) );
    }
    
}
