<?php defined('_JEXEC') or die('Restricted Access');
/*
* HTML5FlippingBook Component
* @package HTML5FlippingBook
* @author JoomPlace Team
* @copyright Copyright (C) JoomPlace, www.joomplace.com
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/

//require_once(JPATH_COMPONENT_ADMINISTRATOR.'/libs/HtmlHelper.php');

class PlotViewPortfolio extends JViewLegacy
{
	protected $state;
	protected $item;
	protected $form;
	//----------------------------------------------------------------------------------------------------
	public function display($tpl = null) 
	{

		$this->state = $this->get('State');
		$this->item = $this->get('Item');
		$this->form = $this->get('Form');

        $document = JFactory::getDocument();
        $document->addScript(JURI::root().'administrator/components/com_plot/assets/js/BootstrapFormHelper.js');
        $document->addScript(JURI::root().'administrator/components/com_plot/assets/js/BootstrapFormValidator.js');

		parent::display($tpl);
	}



}