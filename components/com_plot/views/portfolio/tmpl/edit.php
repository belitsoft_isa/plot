<?php defined('_JEXEC') or die('Restricted Access');

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.formvalidation');
?>

<script type="text/javascript">

function deleteImg(id){
    jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_plot&task=portfolio.deleteImg",
        data: {
            'id': id

        },
        success: function (response) {
            if(response){
                jQuery('#plot-img').html(' <input type="file" name="img">');
            }else{
                alert('error');
            }

        }
    });
}




</script>


<form action="<?php echo JRoute::_('index.php?option=com_plot&task=portfolio.save'); ?>" method="post" name="adminForm" id="adminform"  enctype="multipart/form-data" >
    <input type="hidden" name="option" value="com_plot"/>
    <input type="hidden" name="view" value="portfolio"/>
    <input type="hidden" name="layout" value="edit"/>

    <input type="hidden" name="id" value="<?php echo $this->item->id; ?>"/>
    <input type="hidden" name="jform[user_id]" id="jform_user_id" value="<?php echo  (int)plotUser::factory()->id; ?> ">
    <?php echo JHtml::_('form.token'); ?>

    <?php echo $this->form->getInput('id'); ?>


   <table class="table table-striped">
        <tbody>
        <tr>
            <td><?php echo $this->form->getLabel('title'); ?></td>
            <td><?php echo $this->form->getInput('title'); ?></td>
        </tr>
        <tr>
            <td><?php echo $this->form->getLabel('description'); ?></td>
            <td><?php echo $this->form->getInput('description'); ?></td>
        </tr>
        <tr>
            <td><?php echo $this->form->getLabel('link'); ?></td>
            <td><?php echo $this->form->getInput('link'); ?></td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="plot-img">
                <?php if($this->form->getValue('img')==''){ ?>
                    <input type="file" name="img">
                <?php }else{ ?>
                    <img src="<?php echo JURI::root() . 'images/com_plot/portfolio/'.(int)plotUser::factory()->id.'/'.$this->form->getValue('img'); ?>" >
                    <a href="javascript:void(0)" onclick="deleteImg(<?php echo $this->item->id; ?>);">delete</a>
                <?php
                }
                ?>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

<input type="submit" value="save">
</form>