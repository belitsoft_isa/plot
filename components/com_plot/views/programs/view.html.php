<?php

defined('_JEXEC') or die;
require_once JPATH_COMPONENT . '/views/view.php';



class PlotViewPrograms extends JViewLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'plot';

    public function display($tpl = null)
    {

        parent::display($tpl);
    }


    
}
