<?php
defined('_JEXEC') or die;
$count_all_books = count($this->programs);
$i = 0;

if(!$this->countItems){
    ?>
    <script>
        jQuery('.es-content').html('<div class="no-items"><?php echo  JText::_("COM_PLOT_PROGRAMS_NOT_FOUND");?></div>');
    </script>
    <?php
}else{
if ($this->programs) { ?>

            <ul class="plot-program-entity">
                <?php foreach ($this->programs AS $key => $item) { ?>
                <li>
                    <a class="elem-info parent-book" href="<?php echo $item->link; ?>">
                        <figure><img src="<?php echo $item->thumbnailUrl; ?>" alt="<?php echo $item->title; ?>"/></figure>
                    </a>
                    <div class="plot-main-program-info">
                        <p class="pr-title"><?php echo  $item->c_title;?></p>
                        <i><span>Автор:</span>
                            <?php
                            if((int)$item->book_author){
                                echo plotUser::factory($item->book_author)->name;
                            }else{
                                echo $item->c_author;
                            }
                            ?>
                        </i>
                        <p class="about"><?php echo PlotHelper::cropStr(strip_tags($item->c_pub_descr), plotGlobalConfig::getVar('programsBookMaxSymbolsToShow')); ?></p>
                        <div class="erfolg">
                            <p <?php echo ($item->readed)?'class="yes"':'class="no"';?>>
                                <svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checkmark"></use></svg>
                                тест пройден
                            </p>
                            <p <?php echo ($item->essay_approved)?'class="yes"':'class="no"';?>>
                                <svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checkmark"></use></svg>
                                эссе написано</p>
                        </div>
                    </div>
                </li>
                <?php } ?>
            </ul>

<?php }
} ?>


