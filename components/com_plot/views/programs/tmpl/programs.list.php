<?php
defined('_JEXEC') or die;
$count_all_books = count($this->programs);
$i = 0;

if(!$this->countItems){
    ?>
    <script>
        jQuery('.es-content-wrap').html('<div class="no-items"><?php echo  JText::_("COM_PLOT_PROGRAMS_NOT_FOUND");?></div>');
    </script>
    <?php
}else{
if ($this->programs) { ?>

            <ul class="plot-program-entity recommend-programs">
                <?php foreach ($this->programs AS $key => $item) { ?>
                <li>
                    <a href="<?php echo $item->link; ?>">
                        <div class="plot-main-program-info">
                            <p class="pr-title" title="<?php echo  $item->title; ?>"><?php echo  $item->title; ?></p>
                            <div class="main-program-age"><span>Возраст:</span><i><?php echo  $item->title_age; ?></i></div>
                            <div class="main-program-level"><span>Уровень сложности:</span><i><?php echo $item->level_title; ?></i></div>
                        </div>
                        <img src="<?php echo $item->img; ?>" alt="<?php echo $item->title; ?>"/>
                        <div class="plot-programs-progress" data-progress="<?php echo $item->percent; ?>"></div>
                    </a>
                </li>
                <?php } ?>
            </ul>

<?php }
} ?>
