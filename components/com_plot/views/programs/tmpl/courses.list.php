<?php
defined('_JEXEC') or die;
$count_all_books = count($this->programs);
$i = 0;
if(!$this->countItems){
    ?>
    <script>
        jQuery('.es-content').html('<div class="no-items"><?php echo  JText::_("COM_PLOT_PROGRAMS_NOT_FOUND");?></div>');
    </script>
    <?php
}else{
if ($this->programs) { ?>
    <ul class="plot-program-entity" >
        <?php foreach ($this->programs AS $key => $item) { ?>
            <li>
                <a class="elem-info parent-course" href="<?php echo $item->link; ?>">
                    <img src="<?php echo $item->image; ?>" alt="<?php echo $item->course_name; ?>" />
                </a>
                <div class="plot-main-program-info">
                    <p class="pr-title"><?php echo $item->course_name; ?></p>
                    <i><span>Автор:</span><?php echo plotUser::factory($item->owner_id)->name; ?></i>
                    <p class="about"><?php echo PlotHelper::cropStr(strip_tags($item->course_description), plotGlobalConfig::getVar('programsCourseMaxSymbolsToShow')); ?></p>
                    <div class="erfolg">
                        <p <?php echo ($item->finished)?'class="yes"':'class="no"';?>>
                            <svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>"  viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checkmark"></use></svg>
                            курс пройден
                        </p>
                        <p <?php echo ($item->video_approved)?'class="yes"':'class="no"';?>>
                            <svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>"  viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#checkmark"></use></svg>
                            испыташка пройдена</p>
                    </div>




                    <!--<object data="<?php echo JUri::root()?>templates/plot/img/child-bottom-left.png" type="image/svg+xml">-->



                </div>
            </li>
        <?php } ?>
    </ul>

<?php }
} ?>
