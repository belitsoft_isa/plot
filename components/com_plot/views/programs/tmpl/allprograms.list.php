<?php
defined('_JEXEC') or die;
$count_all_books = count($this->programs);
$i = 0;

if(!$this->countItems){
    ?>
    <script>
        jQuery('.es-content').html('<div class="no-items"><?php echo  JText::_("COM_PLOT_PROGRAMS_NOT_FOUND");?></div>');
    </script>
    <?php
}else{
if ($this->programs) { ?>

            <ul class="all-plot-programs">
                <?php foreach ($this->programs AS $key => $item) { ?>
                <li>
                    <a href="<?php echo $item->link; ?>">
                        <img src="<?php echo $item->img; ?>" alt="<?php echo $item->title; ?>"/>
                    </a>
                    <div class="plot-main-program-info">
                        <p class="pr-title"><?php echo  $item->title; ?></p>
                        <div class="main-program-age"><span>Возраст:</span><i><?php echo  $item->ages; ?></i></div>
                        <div class="main-program-level"><span>Уровень сложности:</span><i><?php echo $item->level_title; ?></i></div>
                        <div class="plot-programs-progress" data-progress="<?php echo $item->percent; ?>"></div>
                        <p class="es-profile-header-meta">
                            <i class="ies-folder-3 muted"></i><b><?php echo $item->cats_title; ?></b>
                            <i class="ies-users muted"></i></i><b><?php echo $item->members; ?> участников</b>
                        </p>
                    </div>
                </li>
                <?php } ?>
            </ul>

<?php }
} ?>
