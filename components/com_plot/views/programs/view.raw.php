<?php

defined('_JEXEC') or die;
require_once JPATH_COMPONENT.'/views/view.php';

class PlotViewPrograms extends JViewLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'books';

    public function display($tpl = null)
    {
        return parent::display($tpl);
    }
    
    public function ajaxRenderProgramsList()
    {
        return $this->loadTemplate();
    }

}
