<?php
defined('_JEXEC') or die;

class PlotViewRegistration extends JViewLegacy
{

    public function display($tpl = null)
    {
        $this->templateUrl = JURI::base().'templates/'.JFactory::getApplication()->getTemplate();
        $this->componentUrl = JURI::base().'components/com_plot';
        
        return parent::display($tpl);
    }

}
