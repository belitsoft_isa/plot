<?php
defined('_JEXEC') or die;
jimport('joomla.html.html.bootstrap');
?>

<script src="//ulogin.ru/js/ulogin.js"></script>

<script src="<?php echo JUri::root();?>media/jui/js/jquery.min.js"></script>
<script src="<?php echo juri::root();?>components/com_plot/libraries/jquery/jquery-ui-1.11.0.min.js"></script>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->templateUrl; ?>/css/style.css" />

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">

   /* function setLocalStorageValue(cname, val,d) {
        localStorage.setItem(cname, val);
    }

    function getLocalStorageValue(cname) {
        return localStorage.getItem(cname);
    }
    function deleteLocalStorageValue( name ) {
        localStorage.removeItem(name);
    }
    function checkLocalStorage() {
        var user_agreement = getLocalStorageValue("user_agreement"),
            user_profile= getLocalStorageValue("user_profile"),
            user_name= getLocalStorageValue("user_name"),
            user_surname= getLocalStorageValue("user_surname"),
            user_email= getLocalStorageValue("user_email");
        if(user_agreement!=null){
            jQuery("label[for='user-agreement-checkbox']").click();
            deleteLocalStorageValue(user_agreement);
        }
        if(user_profile!=null){
            if(parseInt(user_profile)==3){
                jQuery("label[for='user-type-parent']").click();
            }else{
                jQuery("label[for='user-type-child']").click();
            }
            deleteLocalStorageValue(user_profile);
        }
        if(user_name!=null){
            jQuery("#name").val(user_name);
            deleteLocalStorageValue(user_name);
        }

        if(user_surname!=null){
            jQuery("#surname").val(user_surname);
            deleteLocalStorageValue(user_surname);
        }
        if(user_email!=null){
            jQuery(".user_email").val(user_email);
            deleteLocalStorageValue(user_email);
        }
    }
*/
    function validateRegisterForm()
    {
        var errorMsg = '',
            error=true;
        if (!jQuery('#registration-form [name=name]').val()) {
            errorMsg += 'Укажите имя. \n';
            error=false;
        }
        if (!jQuery('#registration-form [name=surname]').val()) {
            errorMsg += 'Укажите фамилию. \n';
            error=false;
        }
        if (!jQuery('#registration-form [name=email]').val()) {
            errorMsg += 'Укажите e-mail. \n';
            error=false;
        }
        if (!jQuery('#registration-form [name=password]').val()) {
            errorMsg += 'Укажите пароль. \n';
            error=false;
        }
        if (jQuery('#registration-form [name=password-confirm]').val() !== jQuery('#registration-form [name=password]').val() ) {
            errorMsg += 'Поля "пароль" и "подтверждение пароля" должны совпадать. \n';
            error=false;
        }
        if (!jQuery('#registration-form #user-agreement-checkbox').prop('checked')) {
            errorMsg += 'Необходимо принять пользовательское соглашение. \n';
            error=false;
        }

        if (errorMsg || !jQuery('[name=user-type]:checked').length) {
            if (!jQuery('[name=user-type]:checked').length) {
                errorMsg += 'Выберите тип профиля. \n';
                jQuery('.choose-usertype-error').show();
                error=false;
            }
            error=false;

        }
        if(error){
            return error;
        }else{
            alert(errorMsg);
            return error;
        }

    }

    function resizeIframeToShort()
    {
        window.parent.SqueezeBox.resize({y: 451});
        jQuery('iframe', window.parent.document).attr('height', 451);
        window.parent.SqueezeBox.resize({x: 580});
        jQuery('iframe', window.parent.document).attr('width', 580);
    }
    function resizeIframeToTall()
    {
        window.parent.SqueezeBox.resize({y: 640});
        jQuery('iframe', window.parent.document).attr('height', 640);
        window.parent.SqueezeBox.resize({x: 580});
        jQuery('iframe', window.parent.document).attr('width', 580);
    }

    function customizeULoginOtherButton()
    {
        var changeDefaultOtherButtonInterval = setInterval(function(){
            if (jQuery('#uLoginLoginOther > img').length) {
                jQuery('#uLoginLoginOther > div > div').remove();
                jQuery('#uLoginLoginOther').click(function(){
                    jQuery('#uLoginLoginOther > img').css('background', 'url("https://ulogin.ru/img/small.png?version=1.3.00") no-repeat scroll 0 0 rgba(0, 0, 0, 0)');
                    jQuery('#uLoginLoginOther > div').css('top', '-250px');
                });
                clearInterval(changeDefaultOtherButtonInterval);
            }
        }, 100);
        var changeDefaultOtherButtonIntervalRegistration = setInterval(function(){
            if (jQuery('#uLoginRegistrationOther > img').length) {
                jQuery('#uLoginRegistrationOther > div > div').remove();
                jQuery('#uLoginRegistrationOther').click(function(){
                    jQuery('#uLoginRegistrationOther>img').css('background', 'url("https://ulogin.ru/img/small.png?version=1.3.00") no-repeat scroll 0 0 rgba(0, 0, 0, 0)');
                    jQuery('#uLoginRegistrationOther > div').css('top', '-150px');
                });
                clearInterval(changeDefaultOtherButtonIntervalRegistration);
            }
        }, 100);
    }

    jQuery(document).ready(function(){
        //checkLocalStorage();
        jQuery('body').addClass('login-popup-style');

        <?php if (($activeTab = JRequest::getVar('activeTab', '')) == 1 && JRequest::getVar('resize', '') == 1) { ?>
        resizeIframeToTall();
        <?php } ?>
        <?php if (($activeTab = JRequest::getVar('activeTab', '')) == 0 && JRequest::getVar('resize', '') == 1) { ?>
        resizeIframeToShort();
        <?php } ?>

        var activeTab = <?php echo ($activeTab = JRequest::getVar('activeTab', '')) ? $activeTab : 0; ?>;
        jQuery( "#tabs" ).tabs({active: activeTab});

        jQuery('[name=user-type]').change(function(){
            //setLocalStorageValue('user_profile',jQuery(this).val());
            jQuery.post('index.php?option=com_plot&task=registration.ajaxChangeProfileTypeForRegistration', {profileType: jQuery(this).val()});

        });



        customizeULoginOtherButton();

        jQuery('label[for=user-type-child], label[for=user-type-parent]').click(function(){
            jQuery('.choose-usertype-error').hide();
        });

        jQuery('#tab-2 .socnet-button > li').click(function(event){
            if (!jQuery('[name=user-type]:checked').length) {
                jQuery('.choose-usertype-error').show();
                event.stopImmediatePropagation();
            }
        });

       /* jQuery('#name').blur(function(){
            setLocalStorageValue('user_name',jQuery(this).val());

        });

        jQuery('#surname').blur(function(){
            setLocalStorageValue('user_surname',jQuery(this).val());

        });

        jQuery('.user_email').blur(function(){
            console.log(jQuery(this).val());
            setLocalStorageValue('user_email',jQuery(this).val());

        });

        jQuery('#user-agreement-checkbox').change(function(){
            setLocalStorageValue('user_agreement',jQuery(this).prop('checked'));
        });*/

    });

    window.parent.addEventListener("resize", function() {
        window.parent.SqueezeBox.resize();
    }, false);

</script>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="SVG"> ?>
<svg xmlns="http://www.w3.org/2000/svg" style="position: absolute; left: -99999px;">
    <symbol id="odnoklassniki" viewBox="0 0 21 21" preserveAspectRatio="xMinYMin meet">
        <path class="st0" d="M21,10.5C21,16.3,16.3,21,10.5,21l0,0C4.7,21,0,16.3,0,10.5l0,0C0,4.7,4.7,0,10.5,0l0,0C16.3,0,21,4.7,21,10.5L21,10.5z"/>
        <path class="st1" d="M10.6,3.4c2.1,0,3.8,1.7,3.8,3.8c0,2.1-1.7,3.8-3.8,3.8c-2.1,0-3.8-1.7-3.8-3.8C6.8,5.1,8.5,3.4,10.6,3.4L10.6,3.4z M9.6,14.3c-0.7,0.5-3.8,3.4-1.8,3.7c1.2,0.2,1.3-1,2.7-2.3c1.4,0.6,2,2.9,3.5,2.2c1.4-0.6-2-3.3-2.4-3.7c0.8-0.8,1.2-0.7,2.2-1.2c0.6-0.1,1.9-1.3,0.7-2.2c-1.2-0.8-1.8,2-6.2,0.3C6.1,10.3,6,12,6.6,12.4C7.8,13.3,8.4,13,9.6,14.3L9.6,14.3z M10.6,5.1c1.1,0,2,0.9,2,2s-0.9,2-2,2c-1.1,0-2-0.9-2-2S9.5,5.1,10.6,5.1L10.6,5.1z"/>
    </symbol>
</svg>
<?php // </editor-fold> ?>

<div class="wrap">
    <div id="tabs" class="login-popup-tabs auth-form">
        <ul class="popup-header authform-header">
            <li><a href="#tab-1" onclick="resizeIframeToShort();customizeULoginOtherButton();"><?php echo JText::_('COM_PLOT_LOGIN_TO_SITE'); ?></a></li>
            <li><a href="#tab-2" onclick="resizeIframeToTall();customizeULoginOtherButton();"><?php echo JText::_('COM_PLOT_REGISTRATION'); ?></a></li>
        </ul>
        <?php # <editor-fold defaultstate="collapsed" desc="LOGIN"> ?>
        <div id="tab-1">
            <div class="socnet-id">
                <h3><?php echo JText::_('COM_PLOT_LOGIN_VIA_SOCIAL_NETWORK'); ?></h3>
                <p><?php echo JText::_('COM_PLOT_IF_YOU_HAVE_DATA_FOR_ONE_SOCIAL_YOU_CAN_LOGIN_USING_IT'); ?> <?php echo JText::_('COM_PLOT_CHOOSE_SOCIAL_NETWORK'); ?></p>
                <div id="uLoginLogin" data-ulogin="display=buttons;lang=ru;fields=first_name,last_name,email;providers=facebook,mailru,vkontakte,odnoklassniki;hidden=;redirect_uri=<?php echo urlencode(JRoute::_('index.php?option=com_plot&task=registration.loginWithSocial&login=1', false, false));?>">
                    <ul class="socnet-button">
                        <li class="fb" data-uloginbutton="facebook">
                            <a href="#">
                                <span>f</span>
                                <?php echo JText::_('COM_PLOT_LOGIN_VIA'); ?> <b>Facebook</b>
                            </a>
                        </li>
                        <li class="mailru" data-uloginbutton="mailru">
                            <a href="#"><?php echo JText::_('COM_PLOT_LOGIN_VIA'); ?> <b>Мой мир</b><i><span>@</span>mail<span>.ru</span></i>
                            </a>
                        </li>
                        <li class="vk" data-uloginbutton="vkontakte">
                            <span>B</span>
                            <a href="#"><?php echo JText::_('COM_PLOT_LOGIN_VIA'); ?> <b>Vkontakte</b>
                            </a>
                        </li>
                        <li class="odnoklassniki" data-uloginbutton="odnoklassniki">
                            <a href="#">
                                <svg viewBox="0 0 21 21" preserveAspectRatio="xMinYMin meet" class="icon-auth">
                                    <use xlink:href="#odnoklassniki"></use>
                                </svg>
                                <?php echo JText::_('COM_PLOT_LOGIN_VIA'); ?> <b>Odnoklassniki</b>
                            </a>
                        </li>
                        <li class="other-socnetwork">
                            <a href="#">
                                <div id="uLoginLoginOther" data-ulogin="display=small;fields=first_name,last_name,email;providers=;hidden=other;redirect_uri=<?php echo urlencode(JRoute::_('index.php?option=com_plot&task=registration.loginWithSocial', false, false));?>"></div>
                                <em><?php echo JText::_('COM_PLOT_ANOTHER_SOCIAL_NETWORKS');?></em>
                            </a>
                        </li>
                    </ul>
                </div>
<!--                <div id="uLoginLoginOther" style="float: right;" data-ulogin="display=small;fields=first_name,last_name,email;providers=;hidden=other;redirect_uri=--><?php //echo urlencode(JRoute::_('index.php?option=com_plot&task=registration.loginWithSocial', false, false));?><!--"></div>-->
                <div class="clr"></div>
            </div>
            <div class="site-id">
                <h3><?php echo JText::_('COM_PLOT_LOGIN_USUALLY'); ?></h3>
                <p><?php echo JText::_('COM_PLOT_IF_YOU_DOES_NOT_REGISTERED_WITH_OUR_SITE_USUALLY_CASE_ENTER_DATA');?></p>
                <form method="POST" action="<?php echo JRoute::_('index.php?option=com_plot&task=registration.login'); ?>" target="_parent">
                    <fieldset>
                        <input type="text" placeholder="<?php echo JText::_('COM_PLOT_ENTER_EMAIL'); ?>" name="login" id="email" />
                        <input type="password" placeholder="<?php echo JText::_('COM_PLOT_ENTER_PASSWORD'); ?>" name="password" id="password" />
                        <input type="password" name="password2" class="hidden" value="" />
                        <a rel="{size: {x: 400, y: 200}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
                           href="<?php echo JRoute::_('index.php?option=com_plot&task=registration.ajaxShowPasswordRestore'); ?>" class="modal">
                            <?php echo JText::_('COM_PLOT_DID_YOU_FORGET_PASSWORD'); ?>
                        </a>
                        <span class="button brown h35"><input type="submit" value="<?php echo JText::_('COM_PLOT_ENTER_POINT_TO_SITE'); ?>" /></span>
                    </fieldset>
                    <?php echo JHtml::_('form.token'); ?>
                </form>
            </div>
        </div>
        <?php # </editor-fold> ?>
        <?php # <editor-fold defaultstate="collapsed" desc="REGISTRATION"> ?>
        <div id="tab-2">
            <form class="reg-radio">
                <fieldset>
                    <input type="radio" id="user-type-child" name="user-type" value="<?php echo plotGlobalConfig::getVar('childProfileId'); ?>" />
                    <label for="user-type-child"><i class="circle circle-border"></i><?php echo JText::_('COM_PLOT_IM_A_CHILD'); ?></label>
                    <input type="radio" id="user-type-parent" name="user-type" value="<?php echo plotGlobalConfig::getVar('parentProfileId'); ?>" />
                    <label for="user-type-parent" ><i class="circle-border"></i><?php echo JText::_('COM_PLOT_IM_A_PARENT'); ?></label>
                </fieldset>
                <div class="choose-usertype-error">Выберите тип профиля</div>
            </form>
            <div class="socnet-id">
                <h3><?php echo JText::_('COM_PLOT_REGISTRATION_VIA_SOCIAL_NETWORK'); ?></h3>
                <?php echo PlotHelper::getDefaultText('social_login', 0); ?>
                <div id="uLoginRegistration" data-ulogin="display=buttons;fields=first_name,last_name,email;providers=facebook,mailru,vkontakte,odnoklassniki;hidden=;redirect_uri=<?php echo urlencode(JRoute::_('index.php?option=com_plot&task=registration.loginWithSocial', false, false));?>">
                    <ul class="socnet-button">
                        <li class="fb" data-uloginbutton="facebook">
                            <a href="#">
                                <span>f</span>
                                <?php echo JText::_('COM_PLOT_LOGIN_VIA'); ?> <b>Facebook</b>
                            </a>
                        </li>
                        <li class="mailru" data-uloginbutton="mailru">
                            <a href="#"><?php echo JText::_('COM_PLOT_LOGIN_VIA'); ?> <b>Мой мир</b><i><span>@</span>mail<span>.ru</span></i>
                            </a>
                        </li>
                        <li class="vk" data-uloginbutton="vkontakte">
                            <span>B</span>
                            <a href="#"><?php echo JText::_('COM_PLOT_LOGIN_VIA'); ?> <b>Vkontakte</b>
                            </a>
                        </li>
                        <li class="odnoklassniki" data-uloginbutton="odnoklassniki">
                            <a href="#">
                                <svg viewBox="0 0 21 21" preserveAspectRatio="xMinYMin meet" class="icon-auth">
                                    <use xlink:href="#odnoklassniki"></use>
                                </svg>
                                <?php echo JText::_('COM_PLOT_LOGIN_VIA'); ?> <b>Odnoklassniki</b>
                            </a>
                        </li>
                        <li class="other-socnetwork">
                            <a href="#">
                                <div id="uLoginRegistrationOther" style="float: right;" data-ulogin="display=small;fields=first_name,last_name,email;providers=;hidden=other;redirect_uri=<?php echo urlencode(JRoute::_('index.php?option=com_plot&task=registration.loginWithSocial', false, false));?>"></div>
                                <em><?php echo JText::_('COM_PLOT_ANOTHER_SOCIAL_NETWORKS'); ?></em>
                            </a>
                        </li>
                    </ul>
                </div>
<!--                <div id="uLoginRegistrationOther" style="float: right;" data-ulogin="display=small;fields=first_name,last_name,email;providers=;hidden=other;redirect_uri=--><?php //echo urlencode(JRoute::_('index.php?option=com_plot&task=registration.loginWithSocial', false, false));?><!--"></div>-->
                <div class="clr"></div>
            </div>
            <div class="site-id">
                <h3><?php echo JText::_('COM_PLOT_REGISTRATION'); ?></h3>
                <?php echo PlotHelper::getDefaultText('email_login', 0); ?>
                <form id="registration-form" method="POST" onsubmit="return validateRegisterForm();" action="<?php echo JRoute::_('index.php?option=com_plot&task=registration.registerAndLoginNewUser'); ?>" target="_parent">
                    <fieldset>
                        <input type="text" name="name" id="name" placeholder="<?php echo JText::_('COM_PLOT_YOUR_NAME'); ?>" />
                        <input type="text" name="surname" id="surname" placeholder="<?php echo JText::_('COM_PLOT_YOUR_LAST_NAME'); ?>"  />
                        <input type="text" name="email" class="user_email" id="email" placeholder="<?php echo JText::_('COM_PLOT_YOUR_EMAIL'); ?>" />
                        <input type="password" name="password" id="password" placeholder="<?php echo JText::_('COM_PLOT_CHOOSE_PASSWORD'); ?>" />
                        <input type="password" name="password-confirm" id="password-confirm" placeholder="<?php echo JText::_('COM_PLOT_REPEAT_PASSWORD'); ?>" />
                        <input type="password" name="password2" class="hidden" value="" />
                        <p class="user-agreement">
                            <input type="checkbox" name="user-agreement" id="user-agreement-checkbox" />
                            <label for="user-agreement-checkbox"><?php echo JText::_('COM_PLOT_I_ACCEPT'); ?></label>
                            <a class="modal" onclick=""
                               rel="{size: {x: 500, y: 400}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
                               href="<?php echo JRoute::_('index.php?option=com_plot&task=registration.ajaxShowUserAgreement'); ?>">
                                <?php echo JText::_('COM_PLOT_USER_AGREEMENT'); ?>
                            </a>
                        </p>
                        <span class="button brown h35"><input type="submit" value="<?php echo JText::_('COM_PLOT_REGISTRATION_AND_LOGIN'); ?>" /></span>
                    </fieldset>
                    <?php echo JHtml::_('form.token'); ?>
                </form>
            </div>
        </div>
        <?php # </editor-fold> ?>
    </div>
</div>    