<?php
defined('_JEXEC') or die;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->templateUrl; ?>/css/style.css" />
<script src="<?php echo JUri::root();?>media/jui/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/js/jquery.jscrollpane.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function(){
        window.parent.SqueezeBox.resize({y: 640});
        jQuery('body').addClass('login-popup-style');
        jQuery('iframe', window.parent.document).attr('height', 640);
        jQuery('.user-agreement-scroll-pane').jScrollPane({
            verticalDragMinHeight: 77,
            verticalDragMaxHeight: 77
        });
    });
</script>

<div id="user-agreement">
    <div class="user-agreement-scroll-pane">
        <?php echo $this->k2Item->introtext; ?>
    </div>
    <a class="back modal" href="<?php echo JRoute::_('index.php?option=com_plot&task=registration.ajaxGetLoginPopupHtml&activeTab=1&resize=1'); ?>" 
       rel="{size: {x: 580, y: 640}, handler:'iframe', iframeOptions: {scrolling: 'no'}}">
        Назад
    </a>
</div>



