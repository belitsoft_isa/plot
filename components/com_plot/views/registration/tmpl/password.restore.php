<?php
defined('_JEXEC') or die;
?>
<script src="<?php echo JUri::root();?>media/jui/js/jquery.min.js"></script>
<script src="<?php echo juri::root();?>components/com_plot/libraries/jquery/jquery-ui-1.11.0.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo $this->templateUrl; ?>/css/style.css" />

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">
jQuery(document).ready(function(){
    window.parent.SqueezeBox.resize({y: 200});
    window.parent.SqueezeBox.resize({x: 400});
    jQuery('body').addClass('login-popup-style').css('width', '400px').css('min-width', '400px');
    jQuery('iframe', window.parent.document).attr('height', 200);
    jQuery('iframe', window.parent.document).attr('width', 400);

    jQuery('#password-restore form').submit(function(){
        jQuery('#sbox-content', window.parent.document).fadeOut();
        jQuery(this).submit();
    });
});
</script>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="CSS"> ?>
<style type="text/css">
#password-restore {
    padding: 10px;
    color: black;
}
#password-restore * {
    vertical-align: baseline;
}
#password-restore * label {
    display: inline-block;
}
#password-restore {
    width: 100%;
    height: 100%;
    box-sizing: border-box;
    padding: 20px 20px;
    font: normal 400 14px/18px 'open sans';
    color: #000;
    border: 3px solid #4DB54D;
    border-radius: 15px;
    position: relative;
    background-color: #fff;
}
#password-restore h1 {
    font-size: 1.1em;
    font-weight: 600;
    margin: 1em 0;
}
#password-restore div {
    overflow-y: auto;
}    
#password-restore .back {
    left: 20px;
    margin-bottom: 10px;
    width: 35px;
}    
#password-restore .sent {
    bottom: 0;
    color: #007bb2;
    font: italic 400 14px/16px "open sans";
    right: 20px;
    padding: 10px;
    position: absolute;
    text-decoration: underline;
    margin-bottom: 10px;
}
#password-restore input {
    border: 2px solid #4db54d;
    color: #999999;
    height: 42px;
    padding: 3px 10px;
    border-radius: 5px;
    box-sizing: border-box;
    margin: 5px 0;
    width: 350px;
    margin: 20px 0;
}
</style>
<?php # </editor-fold> ?>

<div id="password-restore">
    <form action="<?php echo JRoute::_('index.php?option=com_plot&task=registration.restorePassword'); ?>" method="POST" target="_parent">
        <h3>
            Восстановление пароля
        </h3>
        <div>
            <input type="text" placeholder="Введите Ваш e-mail" name="email" />
        </div>
        <a class="back modal" href="<?php echo JRoute::_('index.php?option=com_plot&task=registration.ajaxGetLoginPopupHtml&activeTab=0&resize=1'); ?>"
           rel="{size: {x: 580, y: 700}, handler:'iframe', iframeOptions: {scrolling: 'no'}}">
            Назад
        </a>
        <button class="sent" type="submit">Восстановить</button>
    </form>
</div>