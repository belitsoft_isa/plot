<?php
defined('_JEXEC') or die;
?>

    <?php if ($this->courses) {
        ?>
        <div class="jcarousel-wrapper my-thinking">
            <ul class="child-profile no-jcarousel" >
    <?php
   foreach ($this->courses AS $key => $item) {
        ?>

        <li id="plot-event-id-<?php echo $item->id; ?>">
            <?php if ($item->user_id == plotUser::factory()->id) { ?>
            <button onclick="eventRemoveConfirm(<?php echo $item->id; ?>); return false;" class="close">×</button>
            <?php } ?>
            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=event&id=' . $item->id); ?>">
                <figure>
                    <?php  if (file_exists(JPATH_SITE . '/images/com_plot/events/' .  $item->user_id .  '/thumb/'.$item->img)){
                        ?>
                        <img
                            src="<?php echo JUri::root() . 'images/com_plot/events/' . $item->user_id . '/thumb/' . $item->img; ?>"/>
                    <?php }else{
                        ?>
                        <img
                            src="<?php echo JUri::root().'images/com_plot/def_meeting.jpg' ?>"/>
                    <?php
                    }?>

                    <figcaption><?php echo $item->title; ?></figcaption>
                </figure>
                <?php
                if ($item->create_date != '0000-00-00 00:00:00') {
                    if (strtotime($item->create_date . '+' . plotGlobalConfig::getVar("meetingIsNewMaxDays") . ' day') > strtotime("now")) {
                        ?>
                        <i class="in-progress">
                            <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMinYMin meet">
                                <use xlink:href="#lamp-meeting"></use>
                            </svg>
                            Новая
                        </i>
                    <?php
                    }
                }
                ?>
                <div class="elem-descr">
                    <i>Дата проведения:</i>
                    <?php if (PlotHelper::is_date($item->start_date)) { ?>
                        <span> <?php echo PlotHelper::russian_date(JHtml::date($item->start_date, 'd.m.Y')); ?> </span>
                    <?php } ?>
                    <div class="counter">
                        <p class="start-time">
                            <b>Время начала</b>
                            <?php if (PlotHelper::is_date($item->start_date)) { ?>
                                <time><?php echo JHtml::date($item->start_date, 'H:m'); ?></time>
                            <?php } ?>
                        </p>
                        <?php

                        if (PlotHelper::is_date($item->start_date) && PlotHelper::is_date($item->end_date)) {
                            echo  PlotHelper::renderEventDuration($item);
                        }
                        ?></div>
                                             <span
                                                 class="quantity"><?php echo PlotHelper::countEventSubscription($item->id); ?>
                                                 <svg viewBox="0 0 23 23" preserveAspectRatio="xMinYMin meet">
                                                     <use xlink:href="#people"></use>
                                                 </svg>
                                            </span>
                </div>


            </a>
        </li>
    <?php
    } ?>
    </ul>
</div>
<?php

} ?>

<?php
if (!$this->courses) {
    echo '<div class="no-items">Встреч не найдено</div>';
}
?>
