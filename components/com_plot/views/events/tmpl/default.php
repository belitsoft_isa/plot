<?php
defined('_JEXEC') or die;
?>

<script src="<?php echo $this->componentUrl; ?>/libraries/scrolling_ajax_pagination/javascript.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate().'/js/jquery.jscrollpane.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate().'/js/jquery.mousewheel.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root().'components/com_plot/views/events/tmpl/default.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root().'components/com_plot/assets/js/plot.js'; ?>"></script>
<?php # <editor-fold defaultstate="collapsed" desc="JS">  ?>
<script type="text/javascript">
function showFilteredAndSortedCourses()
{

    setAxaxScrollPagination();

    var tagsIds = jQuery("[id^=filter-courses-tag]:checked").map(function(){
        return jQuery(this).val();
    }).get();

    jQuery('#child-meetings').html('<div id="loading-bar"><img src="<?php echo $this->baseurl ?>/templates/plot/img/pre-loader-1.gif" /></div>');

    jQuery.post(
        '<?php echo JRoute::_('index.php?option=com_plot&task=events.ajaxGetFilteredAndSortedCourses'); ?>',
        {
            ageId: jQuery('#childlib-alter').val(),
            tagsIds: tagsIds,
            categoryId: jQuery('#childlib-category').val(),
            myCoursesOnly: jQuery('[name="my_courses"]:checked').val(),
            sort: jQuery('#childlib-filter').val()
        },
        function(response) {
            var data = jQuery.parseJSON(response);
            jQuery('.info-board .info-bottom .count-courses').html('Найдено <span>'+data.countCourses+' '+App.Words.Ending(data.countCourses, ['думалка', 'думалки', 'думалок'])+'</span>');
            jQuery('#child-meetings').show();
            jQuery('#child-meetings').html(data.renderedCourses);
            if(data.countCourses==0){
               // jQuery('#child-meetings').html('Думалок не найдено');
                jQuery('#child-meetings').hide();
            }
        }
    );
}

function checkOnlyMyInterestsCheckboxes()
{
    jQuery("[id^=filter-courses-tag]").prop('checked', false);
    jQuery.post(
        '<?php echo JRoute::_('index.php?option=com_plot&task=publications.ajaxGetMyTags'); ?>',
        function(response) {
            var data = jQuery.parseJSON(response),
                i= 0,
                data_count=data.length;
            jQuery("[id^=filter-courses-tag]").prop('checked', false);
            for (i; i<data_count; i++) {
                jQuery('#filter-courses-tag'+data[i].id).prop('checked', true);
            }
            showFilteredAndSortedCourses();
        }
    );

}
jQuery(document).ready(function(){

    setAxaxScrollPagination();

    jQuery('.show-only-my-courses > a').click(function(){
        jQuery(this).addClass('ui-state-active');
        showFilteredAndSortedCourses();
    });
    jQuery('.show-all-courses > a').click(function(){
        jQuery('.show-only-my-courses').removeClass('ui-state-active');
        showFilteredAndSortedCourses();
    });

    jQuery('#parent-my-interest-1').selectmenu({
        change: function() {
            var selectedValue = jQuery('#parent-my-interest-1').val();
            switch (selectedValue) {
                case ('my'):
                    jQuery('[id^=filter-courses-tag]').prop('checked', false);
                    jQuery('hidden-data.interests-data .my div').each(function(){
                        var tagId = jQuery(this).html();
                        jQuery('#filter-courses-tag'+tagId).prop('checked', true);
                    });
                    showFilteredAndSortedCourses();
                    break;
                case ('all'):
                    jQuery('[id^=filter-courses-tag]').prop('checked', true);
                    showFilteredAndSortedCourses();
                    break;
                default:
                    jQuery('[id^=filter-courses-tag]').prop('checked', false);
                    jQuery('hidden-data.interests-data .'+jQuery(this).val()+' div').each(function(){
                        var tagId = jQuery(this).html();
                        jQuery('#filter-courses-tag'+tagId).prop('checked', true);
                    });
                    showFilteredAndSortedCourses();
            }
        }
    });


    <?php

       if(plotUser::factory()->id){
       if($this->showOnlyMyCourses){ //if have parametr
           if($this->showOnlyMyCourses==1){
           ?>
    jQuery('#filter-courses-all').prop('checked', false);
    jQuery('#filter-courses-my').prop('checked', true);
    showFilteredAndSortedCourses();
    <?php
        }else{
        ?>
    jQuery('#filter-courses-all').prop('checked', true);
    showFilteredAndSortedCourses();
    <?php
        }

    }else{ //if not have parametr
        if(plotUser::factory()->isHaveEvents()){ //if have events current or future
       ?>
    jQuery('#filter-courses-all').prop('checked', false);
    jQuery('#filter-courses-my').prop('checked', true);
    showFilteredAndSortedCourses();
    <?php
        }else{ //if not have events
   ?>
    jQuery('#filter-courses-all').prop('checked', true);
    jQuery('#filter-courses-my').prop('checked', false);
    showFilteredAndSortedCourses();
    <?php
        }
    }

    }else{
    ?>

    //if not login
    jQuery('#filter-courses-all').prop('checked', true);
    showFilteredAndSortedCourses();
    <?php
    }?>


    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });

});

function eventRemoveConfirm(eventId){
    SqueezeBox.open('<?php echo  JRoute::_("index.php?option=com_plot&task=photos.deleteConfirm", 0)?>&entity=eventList'+'&id='+eventId, {size:{x:300, y:150}, handler:'ajax'});
}

function setAxaxScrollPagination()
{
    var tagsIds = jQuery("[id^=filter-courses-tag]:checked").map(function(){
        return jQuery(this).val();
    }).get();        
    var ajaxScrollData = {
        ageId: jQuery('#childlib-alter').val(), 
        tagsIds: tagsIds, 
        categoryId: jQuery('#childlib-category').val(),
        myCoursesOnly: jQuery('[name="my_courses"]:checked').val(),
        sort: jQuery('#childlib-filter').val()
    };

    jQuery(window).unbind('scroll');

    jQuery('.wrap.child-library').scrollPagination({
        nop: 3,
        offset: 3,
        error: 'No More Posts!',
        delay: 50,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=events.ajaxLoadMore'); ?>',
        appendDataTo: 'child-meetings',
        userData: ajaxScrollData,
        afterLoad: {
            afterLoadAction: function () {
                jPlotUp.Arrow.initialize('positionCameras');
            }
        }
    });

    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });

}
function checkAllFilterCoursesTagsCheckboxes() {
    jQuery("[id^=filter-courses-tag]").prop('checked', true);
    showFilteredAndSortedCourses();
}

function clearAllFilterCoursesTagsCheckboxes() {
    jQuery("[id^=filter-courses-tag]").prop('checked', false);
    showFilteredAndSortedCourses();
}

jQuery(document).ready(function(){

    jQuery( "#childlib-category" ).selectmenu({
        appendTo:'.main-content',
        open: function( event, ui ) { jQuery('#childlib-category-menu').jScrollPane(); },
        change: function( event, ui ) { showFilteredAndSortedCourses(); }
    });
    jQuery( "#childlib-alter" ).selectmenu({
        appendTo:'.main-content',
        open: function( event, ui ) { jQuery('#childlib-alter-menu').jScrollPane(); },
        change: function(){showFilteredAndSortedCourses();}
    });
    jQuery( "#childlib-filter" ).selectmenu({
        appendTo:'.main-content',
        open: function( event, ui ) { jQuery('#childlib-filter-menu').jScrollPane(); },
        change: function( event, ui ) { showFilteredAndSortedCourses(); }
    });


    jQuery('.bulb-menu-open').click(function(){
        var bulbMenu = jQuery(this).parent().find('.bulb-menu');
        if ( bulbMenu.is(':visible') ) {
            bulbMenu.hide();
        } else {
            bulbMenu.show();
        }
    });

    jQuery(function() {
        jQuery('.scroll-pane').jScrollPane({
                verticalDragMinHeight: 28,
                verticalDragMaxHeight: 28
            }
        );
    });

    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });


    <?php

          if(plotUser::factory()->id){
          if($this->showOnlyMyCourses){ //if have parametr
              if($this->showOnlyMyCourses==1){
              ?>
    jQuery('#filter-courses-all').prop('checked', false);
    jQuery('#filter-courses-my').prop('checked', true);
    showFilteredAndSortedCourses();
    <?php
        }else{
        ?>
    jQuery('#filter-courses-all').prop('checked', true);
    showFilteredAndSortedCourses();
    <?php
        }

    }else{ //if not have parametr
        if(plotUser::factory()->isHaveBooks()){ //if have books
       ?>
    jQuery('#filter-courses-all').prop('checked', false);
    jQuery('#filter-courses-my').prop('checked', true);
    showFilteredAndSortedCourses();
    <?php
        }else{ //if not have books
   ?>
    jQuery('#filter-courses-all').prop('checked', true);
    jQuery('#filter-courses-my').prop('checked', false);
    showFilteredAndSortedCourses();
    <?php
        }
    }

    }else{
    ?>

    //if not login
    jQuery('#filter-courses-all').prop('checked', true);
    showFilteredAndSortedCourses();
    <?php
    }?>

});


document.addEventListener("touchmove", function(){
    jQuery('.wrap.child-library').trigger('scroll');
    console.log('ScrollStart');
}, false);
</script>
<?php # </editor-fold>  ?>

<?php # <editor-fold defaultstate="collapsed" desc="CSS"> ?>
<style type="text/css">
    .com_plot {
        color: #FCFCFC;
    }
    .more {
        margin: 5px;
    }
    #child-meetings [id^=plot-event-id-] .close {
        left: 10px;
        position: absolute;
        top: 2px;
        z-index: 5;        
    }
</style>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="MAIN"> ?>
<main class="child-library">
    
    <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_header') ); ?>

    <div class="wrap main-wrap child-library">
        <div class="add-left aside-left child-library"></div>
        <div class="add-right aside-right child-library"></div>
        <section class="child-library interest-filter-board">
            <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_top_menu') ); ?>
            <div class="info-board child-library">
                <?php if ($this->referrerUrl) { ?>
                <a href="<?php echo $this->referrerUrl; ?>"class="link-back hover-shadow">
                    Вернуться
                    <svg viewBox="0 0 22.9 46" preserveAspectRatio="xMinYMin meet" class="butterfly">
                        <use xlink:href="#butterfly"></use>
                    </svg>
                </a>
                <?php } ?>
                <svg viewBox="0 0 37.1 38.9" preserveAspectRatio="xMinYMin meet" class="leaf">
                    <use xlink:href="#leaf"></use>
                </svg>
                <object name="bobr" data="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/img/bobr-first-level.svg" type="image/svg+xml"></object>
                <form class="select-wood">
                    <select id="childlib-alter" onchange="showFilteredAndSortedCourses();">
                        <option value="0"><?php echo JText::_('COM_PLOT_ALL_AGES'); ?></option>
                        <?php foreach ($this->plotAges AS $age) { ?>
                            <option value="<?php echo $age->id; ?>"><?php echo $age->title; ?></option>
                        <?php } ?>
                    </select>
                </form>
                <div class="interest-board-wrapper">
                    <div class="interest-board">
                        <h3>Интересы</h3>
                        <form class="scroll-pane">
                            <fieldset>
                                <?php foreach ($this->plotTags AS $tag) { ?>
                                <input type="checkbox" checked="checked" id="filter-courses-tag<?php echo $tag->id; ?>" value="<?php echo $tag->id; ?>" onchange="showFilteredAndSortedCourses();" />
                                <label for="filter-courses-tag<?php echo $tag->id; ?>"><?php echo $tag->title; ?></label>
                                <?php } ?>
                            </fieldset>
                        </form>
                        <div class="interest-board-under">
                            <button onclick="checkAllFilterCoursesTagsCheckboxes();">Выбрать все</button>
                            <?php if(plotUser::factory()->id){ ?>
                            <input id="filter-my-tags" type="checkbox"/>
                            <label for="filter-my-tags" onclick="checkOnlyMyInterestsCheckboxes();">Мои интересы</label>
                            <?php } ?>
                            <button onclick="clearAllFilterCoursesTagsCheckboxes();">Очистить</button>
                        </div>
                    </div>
                </div>
                <div class="info-top">
                    <form>
                        <select id="childlib-category" onchange="showFilteredAndSortedCourses();">
                            <option value="now" >Думалки сейчас проходят</option>
                            <option value="new">Думалки скоро будут</option>
                            <option value="old">Думалки уже прошли</option>
                        </select>
                    </form>
                    <?php if ($this->my->id) {
                        ?>


                        <input id="filter-courses-all" type="radio" value="0" name="my_courses"  style="display: none" onchange="showFilteredAndSortedCourses();" />
                        <label for="filter-courses-all">Общий раздел думалок</label>
                        <input id="filter-courses-my" type="radio" value="1" name="my_courses" style="display: none" onchange="showFilteredAndSortedCourses();" />
                        <label for="filter-courses-my">Мои думалки</label>
                    <?php } ?>

                </div>
                <div class="info-bottom">
                    <p class="count-courses">Найдено <span>39 думалок</span></p>
                    <form>
                        <label for="childlib-filter">Сортировать по</label>
                        <select id="childlib-filter">
                            <option value="`e`.`title` ASC">Названию думалки А-Я</option>
                            <option value="`e`.`title` DESC">Названию думалки Я-А</option>
                            <option value="sum DESC">Популярности</option>
                        </select>
                    </form>						
                </div>
            </div>
        </section>
        <section class="my-progress child-library">
            <div class="my-progress-wrapper" id="child-meetings">

                </div>
        </section>
    </div>
    
</main>
<?php # </editor-fold> ?>

