<?php
defined('_JEXEC') or die;
?>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('form#contact').submit(function(){
            var errors = [];
            if (!jQuery('form#contact [name=name]').val().length) {
                errors.push('Введите ФИО');
            }
            if (!jPlot.validate.email(jQuery('form#contact [name=email]').val())) {
                errors.push('Введите корректный e-mail');
            }
            if (!jQuery('form#contact [name=city]').val().length) {
                errors.push('Введите город');
            }
            if (!jQuery('form#contact [name=message]').val().length) {
                errors.push('Введите текст сообщения');
            }
            if (errors.length !== 0) {
                alert(errors.join('\n'));
                return false;
            }
            return true;
        });
    });
</script>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="CSS"> ?>
<style type="text/css">
    form#contact {
        margin-top: 20px;
    }
    form#contact label {
        color: #333;
        display: inline-block;
        font: italic 600 1em/1.2em "open sans";
        margin: 1em 0;
        width: 15%;
    }
    form#contact input, form#contact textarea {
        border: 1px solid #007bb2;
        width: 70%;
        font: 400 1em/1.2em "open sans";
    }
    form#contact button[type=submit] {
        margin-right: 14.6%;
        margin-top: 10px;        
    }
</style>
<?php # </editor-fold> ?>

<h1><?php echo $this->k2Item->title; ?></h1>
<div>
    <?php echo $this->k2Item->introtext; ?>
</div>

<div class="add-activity">
    <form id="contact" action="<?php echo JRoute::_('index.php?option=com_plot&task=contacts.sendMessage'); ?>" method="POST">
        <div>
            <label>ФИО</label>
            <input type="text" name="name" value="<?php echo ($this->my->id) ? $this->my->name : '';?>" />
        </div>
        <div>
            <label>E-mail</label>
            <input type="text" name="email" value="<?php echo ($this->my->id) ? $this->my->email : '';?>" />
        </div>
        <div>
            <label>Телефон</label>
            <input type="text" name="phone" value="<?php echo ($this->my->id) ? $this->my->getSocialFieldData('TEXTBOX') : '';?>" />
        </div>
        <div>
            <label>Город</label>
            <input type="text" name="city" value="<?php echo ($this->my->id) ? $this->my->getSocialFieldData('ADDRESS')->city : '';?>" />
        </div>
        <div>
            <label>Текст</label>
            <textarea name="message"></textarea>
        </div>
        <button class="button brown" type="submit"><span>Отправить</span></button>
    </form>
</div>
