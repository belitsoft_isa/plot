<?php
defined('_JEXEC') or die;
require_once JPATH_COMPONENT . '/views/view.php';
require_once(JPATH_SITE . '/administrator/components/com_html5flippingbook/libs/VarsHelper.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/conversations.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/friends.php');

class PlotViewContacts extends PlotViewLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'plot';

    public function display($tpl = null)
    {
        $this->my = plotUser::factory();
        $this->k2Item = PlotHelper::getk2item( plotGlobalConfig::getVar('footerLinkK2idContacts') );
        parent::display($tpl);
    }

}
