<?php
defined('_JEXEC') or die;
require_once(JPATH_SITE.'/administrator/components/com_easysocial/models/friends.php');

class PlotViewProfile extends JViewLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'profile';

    public function display($tpl = null)
    {
        $app = JFactory::getApplication();
        $this->my = plotUser::factory();
        $this->templateUrl = JURI::base().'templates/'.$app->getTemplate();
        $this->componentUrl = JURI::base().'components/com_plot';
        return parent::display($tpl);
    }

}
