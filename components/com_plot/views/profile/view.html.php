<?php
defined('_JEXEC') or die;

require_once(JPATH_SITE . '/administrator/components/com_html5flippingbook/libs/VarsHelper.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/conversations.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/friends.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/stream.php');
require_once(JPATH_SITE . '/components/com_plot/helpers/html5fbfront.php');

class PlotViewProfile extends JViewLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'profile';
    
    private $_coursesModel;
    
    public function __construct($config = array())
    {
        $this->_coursesModel = JModelLegacy::getInstance('courses', 'plotModel');
        parent::__construct($config);
    }

    public function display($tpl = null)
    {

        $app = JFactory::getApplication();
        $this->templateUrl = JURI::base() . 'templates/' . $app->getTemplate();
        $this->componentUrl = JURI::base() . 'components/com_plot';

        // Get the user's id.
        $id    = JRequest::getInt( 'id' , 0 );
        $this->Itemid   = JRequest::getInt( 'Itemid ' , 0 );
        // The current logged in user might be viewing their own profile.
        if( $id == 0 ) {
            $id = Foundry::user()->id;
        }

        // When the user tries to view his own profile but if he isn't logged in, throw a login page.
       if( $id == 0 ) {
            $app->redirect(JRoute::_('index.php?option=com_plot&view=river', false));
        }
        if($app->getUserState( 'first', 0 )){
            $app->redirect('/dobro-pozalovat');
            return;
        }
        // Get the user's object.
        $this->user = plotUser::factory( $id );
        $my = plotUser::factory();




        if (!$this->user->id || $this->user->block) {
            $app->enqueueMessage(JText::_('COM_PLOT_REQUESTED_PAGE_NOT_EXISTS_ANYMORE'), 'message');
            if ($my->id) {
                $app->redirect(JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('error404K2ItemId'), false));
               // $app->redirect( JRoute::_('index.php?option=com_plot&view=profile&id='.$my->id, false) );
            } else {
                $app->redirect(JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('error404K2ItemId'), false));
                //$app->redirect( JRoute::_('index.php?option=com_plot&view=river', false) );
            }
        }               

        Foundry::page()->title( Foundry::string()->escape( $this->user->getName() ) );
        Foundry::page()->breadcrumb( Foundry::string()->escape( $this->user->getName() ) );
        Foundry::opengraph()->addProfile( $this->user );

        $this->user->level = $this->user->getLevel();
        
        $conversations = JModelLegacy::getInstance('conversations', 'EasySocialModel');

        $this->count_conversations=$conversations->getNewCount(Foundry::user()->id, 'unread');

        $this->emaillayout = new JLayoutFile('email', $basePath = JPATH_COMPONENT .'/layouts');
        $this->sharelayout = new JLayoutFile('share', $basePath = JPATH_COMPONENT .'/layouts');

        $this->fbConfig = $this->GetFbConfig();

        $this->video_links = $this->get('VideoLinks');
        if ($this->video_links) {
            foreach ($this->video_links AS $i=>$video) {
                $video->imageThumb = JUri::root() . 'images/com_plot/def_video.jpg';
                if($video->type=="course"){
                    if (file_exists(JPATH_SITE . '/media/com_plot/coursevideo/' . $video->uid . '/thumb/' . substr($video->path, 0, -3).'jpg')) {
                        $video->imageThumb = JUri::root() . 'media/com_plot/coursevideo/' . $video->uid . '/thumb/' . substr($video->path, 0, -3).'jpg';
                    }
                }else{
                    if (file_exists(JPATH_SITE . '/media/com_plot/videos/' . $video->uid . '/thumb/' . substr($video->path, 0, -3).'jpg')) {
                        $video->imageThumb = JUri::root() . 'media/com_plot/videos/' . $video->uid . '/thumb/' . substr($video->path, 0, -3).'jpg';
                    }
                }

            }
        }
        $this->video_files = $this->get('VideoFiles');
        $this->photos = $this->get('ListPhotos');
        $this->certificates = $this->get('ListCertificates');

        if ($this->fbConfig->social_jomsocial_use) {
            $this->userFriends = $this->get('UserJSFriends');
        }

        $this->readListBS = $this->get('ReadBooks');

        $this->newBookListBS = $this->get('NewBooks');
        $this->readingBookListBS = $this->get('Items');

        if ($this->readingBookListBS) {
            foreach ($this->readingBookListBS AS $row) {
                $thumbnailPath = JPATH_SITE.'/media/com_html5flippingbook'.'/thumbs/'.$row->c_thumb;
                if ($row->c_thumb == "" || !is_file($thumbnailPath)) {
                    $row->thumbnailUrl = JURI::root()."components/com_html5flippingbook/assets/images/no_image.png";
                } else {
                    if (file_exists(JPATH_BASE.'/media/com_html5flippingbook'.'/thumbs/thimb_'.$row->c_thumb)) {
                        $row->thumbnailUrl = JURI::root()."media/com_html5flippingbook/thumbs/thimb_".$row->c_thumb;
                    } else {
                        $row->thumbnailUrl = JURI::root()."media/com_html5flippingbook/thumbs/".$row->c_thumb;
                    }
                }
                $row->new = 0;

                $row->publicationLink = 'index.php?option=com_html5flippingbook&view=publication&id='.$row->c_id;
            }
        }

        $this->books_on_reading_shelf=array();
        $this->count_new=count($this->newBookListBS);
        $count_reading=count($this->readingBookListBS);



        if ($this->count_new <= (int) plotGlobalConfig::getVar('countBooksOnShelfReadDepartment')) {
            $this->books_on_reading_shelf = $this->newBookListBS;

            if ($this->readingBookListBS) {
                if ($count_reading <= ((int) plotGlobalConfig::getVar('countBooksOnShelfReadDepartment') - $this->count_new)) {
                    for ($i = 0; $i < $count_reading; $i++) {


                            $this->books_on_reading_shelf[] = $this->readingBookListBS[$i];



                    }
                }
            }
        } else {
            for ($i = 0; $i < (int) getVar('countBooksOnShelfReadDepartment') - 1; $i++) {
                $this->books_on_reading_shelf[] = $this->newBookListBS[$i];
            }
        }
        $unique = array();
        foreach ($this->books_on_reading_shelf as $object) {
            if (isset($unique[$object->c_id])) {
                continue;
            }
            $unique[$object->c_id] = $object;
        }
        $this->books_on_reading_shelf=array();
        foreach($unique AS $key=>$val){
            $this->books_on_reading_shelf[]=$val;
        }


        $this->points = $this->get('Points');
        $this->state = $this->get('State');
        $this->money = (int) $this->get('Money');

        $modelPortf= JModelLegacy::getInstance('portfolios', 'PlotModel');
        $this->portfolios=$modelPortf->getLastPortfolios($this->user->id);

        $modelEvent = JModelLegacy::getInstance('events', 'PlotModel');
        $this->events = $modelEvent->getUserEvents($this->user->id);
        //get stream

        $modelPublications = JModelLegacy::getInstance('publications', 'PlotModel');

        $this->my = $this->getLoggedInUserWithAssignedData();

        $this->user->currentAge = $this->user->getCurrentAge();

        $this->user->myStudiedCoursesByYears = $this->_coursesModel->getChildStudiedCoursesByYears($this->user->id);
        $this->user->myReadedBooksByYears = $modelPublications->getChildStudiedBooksByYears($this->user->id);
        $this->user->myCertificatesByYears = PlotHelper::photosObject();
        $this->user->allCurrentBooks=$modelPublications->getChildStudiedBooksByYears($this->user->id);
        $this->user->yearsoldsWithData = array_unique(array_merge(
            array_keys($this->user->myStudiedCoursesByYears), array_keys($this->user->myReadedBooksByYears), array_keys($this->user->myCertificatesByYears)
        ));
        $this->user->yearsoldsToShow = array_diff($this->user->yearsoldsWithData, array($this->user->currentAge));

        $this->issetCurrentYearProgress = (isset($this->user->allCurrentBooks[$this->user->currentAge]) || isset($this->user->myStudiedCoursesByYears[$this->user->currentAge]) || $this->certificates) ? true : false;
        $this->finishedCourses = $this->user->getFinishedCourses();
        $this->progress = array();

        $this->progress['books'] = isset($this->user->allCurrentBooks[$this->user->currentAge]) ? $this->user->allCurrentBooks[$this->user->currentAge] : array();
        $this->progress['courses'] = $this->finishedCourses ? $this->finishedCourses : array();
        $this->progress['certificates'] = $this->certificates ? $this->certificates : array();
        
        # transform to sorted by date array
        $booksTransformedArray = array();
        foreach ($this->progress['books'] AS $i => $book) {
            $booksTransformedArray['book'.$i] = $book;
        }
        $coursesTransformedArray = array();
        foreach ($this->progress['courses'] AS $i => $course) {
            $coursesTransformedArray['course'.$i] = $course;
        }
        $certificatesTransformedArray = array();
        foreach ($this->progress['certificates'] AS $i => $certificates) {
            $certificatesTransformedArray['certificate'.$i] = $certificates;
        }
        $this->mergedAndSortedProgressArray = array_merge($booksTransformedArray, $coursesTransformedArray, $certificatesTransformedArray);
        uasort($this->mergedAndSortedProgressArray, function($a, $b){
            $dateA = isset($a->finished_date) ? $a->finished_date : (isset($a->assigned_date) ? $a->assigned_date : '');
            $dateB = isset($b->finished_date) ? $b->finished_date : (isset($b->assigned_date) ? $b->assigned_date : '');
            return $dateA > $dateB ? -1 : 1;
        });

        $ages = new plotAges();
        $this->ages = $ages->getList();
        $this->tags = plotTags::getK2TagsList();

        $modelEvents = JModelLegacy::getInstance('events', 'PlotModel');
        $this->meetings = $modelEvents->getCurentAndFutureEvents();
        
        $this->coursesCategories = $this->_coursesModel->getCoursesCategoriesWhichHavePublishedCourses();


        $this->setOgMeta();
        parent::display($tpl);
    }
    
    private function getLoggedInUserWithAssignedData()
    {
        $loggedInUser = plotUser::factory();
        $loggedInUser->level = $loggedInUser->getLevel();
        $loggedInUser->coursesNewTop = $this->_coursesModel->getCoursesPaidNewTop( array('userId' => $loggedInUser->id, 'limit' => plotGlobalConfig::getVar('coursesNewTopShowInTopMenuCount')) );
        $loggedInUser->countNewCourses = $this->_coursesModel->getNewCoursesCount($loggedInUser->id);
        $loggedInUser->coursesNewTopRow1 = array_slice($loggedInUser->coursesNewTop, 0, 2);
        $loggedInUser->coursesNewTopRow2 = array_slice($loggedInUser->coursesNewTop, 2, 2);
        return $loggedInUser;
    }

    protected function prepareDocument()
    {
        parent::prepareDocument();
        parent::addFeed();
    }

    public function GetFbConfig()
    {
        $db = JFactory::getDBO();

        $query = "SELECT * FROM `#__html5fb_config` ORDER BY `setting_name`";
        $rows = $db->setQuery($query)->loadObjectList();

        $config = (object)array();

        if (isset($rows)) {
            foreach ($rows as $row) {
                $config->{$row->setting_name} = $row->setting_value;
            }
        }

        return $config;
    }

    private function setOgMeta()
    {
        $db = JFactory::getDbo();
        require_once JPATH_ADMINISTRATOR.'/components/com_plot/plot_social_config.php';
        $metaDesc=$db->escape(strip_tags($this->user->getSocialFieldData('ABOUT_ME')))?$db->escape(strip_tags($this->user->getSocialFieldData('ABOUT_ME'))):$db->escape(strip_tags($this->user->name));
        $metaOgUrl = '<meta property="og:url" content="'.PlotHelper::getAbsRoutedUri('index.php?option=com_plot&view=profile&id='.$this->user->id).'" />';
        $metaOgTitle = '<meta property="og:title" content="'.$db->escape($this->user->name).'" />';
        $metaOgDescription = '<meta property="og:description" content="'.$metaDesc.'" />';
        $metaOgImage = '<meta property="og:image" content="'.PlotHelper::getAbsRoutedUri('index.php?option=com_plot&view=profile&task=profile.pickoutimage&id='.$this->user->id).'&canv_width=200&canv_height=200&rand='.rand().'" />';
        $metaOgSiteName = '<meta property="og:site_name" content="'.JFactory::getConfig()->get('sitename').'" />';
        $metaOgFbAppId = '<meta property="fb:app_id" content="473991812742077" />';
        $metaOgType = '<meta property="og:type" content="website" />';
        $metaOgLocale = '<meta property="og:locale" content="ru_RU" />';
        JFactory::getDocument()->addCustomTag($metaOgTitle.$metaOgSiteName.$metaOgUrl.$metaOgDescription.$metaOgFbAppId.$metaOgType.$metaOgLocale.$metaOgImage);
        return true;
    }
    
}
