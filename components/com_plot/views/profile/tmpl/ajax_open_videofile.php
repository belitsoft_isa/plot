<?php
defined('_JEXEC') or die;
?>
<link rel="stylesheet" href="<?php echo JURI::root(); ?>components/com_plot/assets/css/video-js.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate();?>/css/style.css" />

<script src="<?php echo JURI::base(); ?>components/com_plot/assets/js/video.js"></script>

<style type="text/css">
    #example_video_1 {
        height: 100% !important;
        width: 100% !important;
        overflow-x: hidden !important;
    }
    #example_video_1 .vjs-big-play-button {
        left: 43%;
        top: 38%;
    }
</style>

<script type="text/javascript">
    videojs.options.flash.swf = "<?php echo JURI::root(); ?>components/com_plot/assets/js/video-js.swf"
    document.createElement('video');
    document.createElement('audio');
    document.createElement('track');
</script>

<video id="example_video_1" class="video-js vjs-default-skin" controls preload="auto"
       poster="<?php echo JUri::root() . "media/com_plot/videos/" . $this->video->uid . "/thumb/" .pathinfo($this->video->path, PATHINFO_FILENAME).".jpg"; ?>"
       data-setup='{"example_option":true}'>
    <source src="<?php echo JUri::root() . "media/com_plot/videos/" . $this->video->uid . "/" . $this->video->path; ?>" type='video/<?php echo substr($this->video->path, strrpos($this->video->path, '.') + 1); ?>' />
</video>

