<?php
defined('_JEXEC') or die;
?>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" href="<?php echo JUri::root(); ?>templates/plot/css/style.css" rel="stylesheet">
<link type="text/css" href="<?php echo JUri::root(); ?>components/com_plot/libraries/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet">

<iframe  scrolling="no" width="100%" height="99%" src="//www.youtube.com/embed/<?php echo $this->youtubeNumber; ?>?autoplay=1"
        frameborder="0" allowfullscreen="yes"></iframe>