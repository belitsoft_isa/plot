<?php
defined('_JEXEC') or die;
jimport('joomla.html.html.bootstrap');
?>

<script src="/media/jui/js/jquery.min.js" type="text/javascript"></script>

<?php
$user = Foundry::user($this->id);

?>
<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery('#plot-friends').keyup(function () {
            if (jQuery(this).val().length > 1) {
                jQuery.post('index.php?option=com_plot&view=profile&task=profile.suggestWithList', {search: jQuery(this).val()}, function (data) {
                    var i,
                        friends = JSON.parse(data),
                        count_friends = friends.length,
                        str = '';
                    for (i = 0; i < count_friends; i++) {
                        str += '<div class="plot-list-friends" ><span class="textboxlist-itemContent " data-id="' + friends[i].id + '" >' + friends[i].name + '<input type="hidden" name="uid[]" value="' + friends[i].id + '">';
                        str += '</span> <i class="plot-remove" onclick="jQuery(this).parent().remove()">remove</i></div>'
                    }
                    jQuery('#plot-friend-tags').html(str);
                });
            } else {
                jQuery('#plot-friend-tags').html('');
            }
        });


        //add to recipients list
        jQuery('#plot-friend-tags').on('click', '.plot-list-friends', function () {
            jQuery('#plot-recipients').append(this);
        });


    });
</script>
<form enctype="multipart/form-data" method="post"
      action="<?php echo JRoute::_('index.php?option=com_plot&view=profile&task=conversations.sendMessage'); ?>">

    <!-- Participants -->
    <div id="plot-recipients"></div>
    <label class="control-label"><?php echo JText::_('COM_EASYSOCIAL_CONVERSATIONS_RECIPIENTS'); ?>:</label>
    <input type="text" autocomplete="off" id="plot-friends"
           placeholder="<?php echo JText::_('COM_EASYSOCIAL_CONVERSATIONS_START_TYPING'); ?>"/>

    <div id="plot-friend-tags"></div>


    <!-- Editor kicks in here -->

    <label class="control-label"><?php echo JText::_('COM_EASYSOCIAL_CONVERSATIONS_MESSAGE'); ?>:</label>


    <textarea  name="message"
              placeholder="<?php echo JText::_('COM_EASYSOCIAL_CONVERSATIONS_MESSAGE_PLACEHOLDER'); ?>"><?php //echo $message;?></textarea>

                <button class="btn btn-es-primary btn-medium"
                        data-composer-submit><?php echo JText::_('COM_EASYSOCIAL_SUBMIT_BUTTON');?></button>


    <input type="hidden" name="<?php echo Foundry::token(); ?>" value="1"/>

</form>

