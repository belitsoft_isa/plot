<?php
defined('_JEXEC') or die('Unauthorized Access');
$jinput = JFactory::getApplication()->input;
$Itemid = $jinput->get('Itemid', 0, 'INT');
$my = Foundry::user();
?>
<script type="text/javascript">
    function rejectUserEvent(event_id) {
        jQuery('#plot-buttons-event-' + event_id).html('<button onclick="App.Event.Subscribe(this);return false;" data-id="' + event_id + '"><?php echo JText::_("COM_PLOT_SUBSCRIBE_ON_EVENT"); ?></button>');
    }

    function removeUserEvent(event_id) {
        jQuery('#plot-event-id-' + event_id).remove();
    }

    function addUserEvent(event_id) {
        jQuery('#plot-buttons-event-' + event_id).html(' <button   onclick="App.Event.RemoveSubscribe(this); return false;" data-id="' + event_id + '"><?php echo JText::_('COM_PLOT_REMOVE_SUBSCRIBE_EVENT'); ?></button>');
    }
    jQuery(document).ready(function () {
        jQuery('.plot-button-search').on('click', function () {
            var search = jQuery('#plot-serach-event-by-date').val(),
                    user_id =<?php echo $this->user->id; ?>

            if (!App.Validation.IsEmpty(search)) {
                alert('error');
            } else {
                jQuery.post('index.php?option=com_plot&view=profile&task=events.searchByDate', {search: search, user_id: user_id}, function (response) {
                    var data = jQuery.parseJSON(response),
                            events = data.events,
                            i,
                            str = '',
                            event_count = events.length;
                    if (event_count) {
                        for (i = 0; i < event_count; i++) {
                            str += '<div id="plot-event-id-' + events[i].id + '">';
                            str += '<a href="' + events[i].link + '">' + events[i].title + '</a><br>';
                            str += events[i].description + '<br>';
                            str += events[i].start + '<br>';
                            str += events[i].end + '<br>';
                            str += '<img style="width: 100px" src="' + events[i].img + '"><br>';
                            str += '<div id="plot-buttons-event-' + events[i].id + '">';
                            str += events[i].button;
                            str += '</div>';
                            str += '</div>';
                        }

                    } else {
                        str += '<?php echo JText::_("COM_PLOT_SEARCH_NOT_FOUND"); ?>';
                    }

                    jQuery('#plot-user-events').html(str);
                });
            }
        })
    });
</script>
my events<br>
<div id="plot-user-events">
    <?php if ($this->events) {
        foreach ($this->events AS $event) { ?>
            <div id="plot-event-id-<?php echo $event->id; ?>">
                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=event&id='.$event->id); ?>"><?php echo $event->title; ?></a><br>
                <?php
                echo $event->description.'<br>';
                echo $event->start_date.'<br>';
                echo $event->end_date.'<br>';
                ?>
                <img style="width: 100px"
                     src="<?php echo JURI::root().'images/com_plot/events/'.$event->user_id.'/'.$event->img; ?>">
                <br>

                <div id="plot-buttons-event-<?php echo $event->id; ?>">
                    <?php
                    if ((int) $event->user_id == (int) Foundry::user()->id) {
                        ?>
                        <button onclick="App.Event.DeleteEvent(this)"
                                data-id="<?php echo $event->id; ?>"><?php echo JText::_('COM_PLOT_REMOVE_EVENT'); ?></button>

                        <?php
                    } else {
                        //check if user subscribe on event
                        if (PlotHelper::chechEventSubscription($event->id)) {
                            ?>
                            <button onclick="App.Event.RemoveSubscribe(this)"
                                    data-id="<?php echo $event->id; ?>"><?php echo JText::_('COM_PLOT_REMOVE_SUBSCRIBE_EVENT'); ?></button>
                                    <?php
                                } else {
                                    ?>
                            <button onclick="App.Event.Subscribe(this);
                                                    return false;"
                                    data-id="<?php echo $event->id; ?>"><?php echo JText::_('COM_PLOT_SUBSCRIBE_ON_EVENT'); ?></button>
                                    <?php
                                }
                                ?>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
</div>


