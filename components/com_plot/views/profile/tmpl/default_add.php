<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/js/jquery.jscrollpane.min.js"></script>
<!--<script type="text/javascript" src="--><?php //echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?><!--/js/jquery.mask.js"></script>-->

<div id="child-add-activity" style="display: none;" class="add-activity child-profile">
    
    <ul class="add-activity-tabs child-profile">
        <li><a href="#child-add-photo"><svg viewBox="0 0 24.5 26.4" preserveAspectRatio="xMinYMin meet" class="media-icon"><use xlink:href="#photo"></use></svg>Фото</a></li>
        <li><a href="#child-add-video"><svg viewBox="0 0 25.3 22.3" preserveAspectRatio="xMinYMin meet" class="media-icon"><use xlink:href="#video"></use></svg>Видео</a></li>
        <li><a href="#child-add-certificate"><svg viewBox="0 0 24.9 25.8" preserveAspectRatio="xMinYMin meet" class="media-icon"><use xlink:href="#certificate"></use></svg>Сертификат</a></li>
        <li><a href="#child-add-event"><svg viewBox="0 0 26 24" preserveAspectRatio="xMinYMin meet" class="media-icon"><use xlink:href="#datepicker"></use></svg>Встреча</a></li>
        <li><a href="#child-add-course"><svg viewBox="0 0 32 32" preserveAspectRatio="xMinYMin meet" class="media-icon"><use xlink:href="#academic-hat"></use></svg>Курс</a></li>
    </ul>

    <?php # <editor-fold defaultstate="collapsed" desc="ADD PHOTO"> ?>
    <div id="child-add-photo" class="add-photo">
        <form id="plot-photo-form" method="POST" enctype="multipart/form-data" onsubmit="return photoValidate();" action="<?php echo JRoute::_('index.php?option=com_plot&view=profile&task=profile.photoupload'); ?>">
            <input type="hidden" name="img-type" value="photo">
            <input type="hidden" name="img-src" id="photo_src">
            <fieldset>
                <div class="input-wrapper">
                    <div id="prev_parent-photo-upl-1"></div>
                    <svg id="photo-close-button" onclick="photoDelete()" viewBox="0 0 31.5 31.5" preserveAspectRatio="xMinYMin meet" style="fill:url(#gradient-blue); display:none;" class="close-svg">
                        <use xlink:href="#close"></use>
                    </svg>
                    <label id="plot-photo-image">
                        <span id="photo-image-area"><svg viewBox="0 0 24.5 26.4" preserveAspectRatio="xMinYMin meet"><use xlink:href="#photo"></use></svg></span>
                        <span class="upload-photo-label" >Загрузить фотографию</span>
                        <span id="photo-upload-img"></span>
                    </label>
                    <a id="photo-modal-link"
                       rel="{size: {x: 550, y: 500}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
                       href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxPhotoImage'); ?>"
                       class="modal"></a>
                    <script type="text/template" id="qq-template">
                        <div class="qq-uploader-selector qq-uploader">
                            <div class="qq-upload-button-selector qq-upload-button">
                                <img src="<?php echo $this->templateUrl; ?>/img/pre-loader-1.gif" alt="" style="opacity: 0;" class="pre-loader"
                                    />

                            </div>
        <span class="qq-drop-processing-selector qq-drop-processing" style="display: none;">

          <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
        </span>
                            <ul class="qq-upload-list-selector qq-upload-list" style="display: none;">
                                <li>
                                    <div class="qq-progress-bar-container-selector">
                                        <div class="qq-progress-bar-selector qq-progress-bar"></div>
                                    </div>
                                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                                    <span class="qq-edit-filename-icon-selector qq-edit-filename-icon"></span>
                                    <span class="qq-upload-file-selector qq-upload-file"></span>
                                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0"
                                           type="text">
                                    <span class="qq-upload-size-selector qq-upload-size"></span>
                                    <a class="qq-upload-cancel-selector qq-upload-cancel" href="#"></a>
                                    <a class="qq-upload-retry-selector qq-upload-retry" href="#"></a>
                                    <a class="qq-upload-delete-selector qq-upload-delete" href="#"></a>
                                    <span class="qq-upload-status-text-selector qq-upload-status-text"></span>
                                </li>
                            </ul>

                    </script>

                </div>
                <div class="rght-div">
                    <input name="img_title" type="text" id="child-photo-ahead-1" class="ahead plot-photo-required"
                           placeholder="Добавить заголовок"/>
                    <textarea name="img_description" id="child-photo-descr-1" placeholder="Добавить описание"
                              class="ahead plot-photo-required"/></textarea>
                    <button type="submit" class="button brown"><span>Добавить</span></button>
                </div>
            </fieldset>
            <?php echo JHtml::_('form.token'); ?>
        </form>
    </div>
    <?php # </editor-fold> ?>

    <?php # <editor-fold defaultstate="collapsed" desc="ADD VIDEO"> ?>
    <div id="child-add-video" class="add-video">
        <ul class="add-activity-tabs video">
            <li><a href="#child-add-video-link">Добавить ссылку на видео</a></li>
            <li><a href="#child-add-video-upload">Загрузить видео</a></li>
        </ul>
        <div id="child-add-video-link">
            <form id="form-child-add-video-link" method="post"
                  action="<?php echo JRoute::_('index.php?option=com_plot&view=profile&task=profile.videoupload'); ?>"
                  onsubmit="return videoValidate();">
                <input type="hidden" name="add-video" value="video-link"/>
                <fieldset>
                    <a id="video-modal-link"
                       rel="{size: {x: 550, y: 500}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
                       href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxVideoImage'); ?>"
                       class="modal">
                    </a>
                    <div class="input-wrapper">
                        <p class="als-label">
                            <svg id="video-link-close-button"  onclick="videolinkDelete()" viewBox="0 0 31.5 31.5" preserveAspectRatio="xMinYMin meet" style="fill:url(#gradient-blue); display:none;" class="close-svg">
                                <use xlink:href="#close"></use>
                            </svg>
                            <img id="video-youtube-img" src="<?php echo JUri::root() . 'templates/plot/img/blank150x150.jpg'; ?>" alt=""/>
                        </p>
                    </div>
                    <div class="rght-div">
                        <input type="url" id="child-video-link" name="video-link" class="ahead plot-video-link-required"
                               placeholder="Вставьте ссылку на видео"/>
                        <svg viewBox="0 0 150 62.6" preserveAspectRatio="xMinYMin meet" class="video-link-icon">
                            <use xlink:href="#youtube"></use>
                        </svg>
                        <img src="<?php echo $this->templateUrl; ?>/img/pre-loader-1.gif" alt="" class="pre-loader" style="opacity: 0;"/>
                        <input type="text" id="child-video-link-ahead-1" name="video-title" class="ahead plot-video-link-required"
                               placeholder="Добавить заголовок"/>
                        <textarea id="child-video-link-descr-1" name="video-description"
                                  placeholder="Добавить описание" class="ahead  plot-video-link-required"/></textarea>
                        <button type="submit" class="button brown"><span>Добавить</span></button>
                    </div>
                    <?php echo JHtml::_('form.token'); ?>
                </fieldset>
            </form>
        </div>
        <div id="child-add-video-upload">
            <form id="form-child-add-video-upload" method="post" enctype="multipart/form-data" onsubmit="return videoFileValidate();"
                  action="<?php echo JRoute::_('index.php?option=com_plot&view=profile&task=profile.videoFileSave'); ?>">

                <fieldset>
                    <div class="input-wrapper">
                        <svg id="video-file-close-button"  onclick="videoFileDelete()" viewBox="0 0 31.5 31.5" preserveAspectRatio="xMinYMin meet" style="fill:url(#gradient-blue); display:none;" class="close-svg">
                            <use xlink:href="#close"></use>
                        </svg>
                        <label id="plot-video-file-image">
                            <span id="videofile-image-area">
                                <svg viewBox="0 0 25.3 22.3" preserveAspectRatio="xMinYMin meet"><use xlink:href="#video"></use></svg>
                            </span>
                            <span class="upload-videofile-label">Загрузить видео</span>
                            <span id="videofile-upload-img"></span>
                        </label>
                        <input type="hidden" name="videofile-id" id="videofile-id"/>
                        <a id="videofile-modal-link"
                           rel="{size: {x: 550, y: 500}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
                           href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxVideofileImage'); ?>"
                           class="modal"></a>
                        <script type="text/template" id="qq-template">
                            <div class="qq-uploader-selector qq-uploader">

                                <div class="qq-upload-button-selector qq-upload-button">
                                    <img src="<?php echo $this->templateUrl; ?>/img/pre-loader-1.gif" alt=""style="opacity: 0;" class="pre-loader"
                                        />

                                </div>
        <span class="qq-drop-processing-selector qq-drop-processing" style="display: none;">

          <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
        </span>
                                <ul class="qq-upload-list-selector qq-upload-list" style="display: none;">
                                    <li>
                                        <div class="qq-progress-bar-container-selector">
                                            <div class="qq-progress-bar-selector qq-progress-bar"></div>
                                        </div>
                                        <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                                        <span class="qq-edit-filename-icon-selector qq-edit-filename-icon"></span>
                                        <span class="qq-upload-file-selector qq-upload-file"></span>
                                        <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0"
                                               type="text">
                                        <span class="qq-upload-size-selector qq-upload-size"></span>
                                        <a class="qq-upload-cancel-selector qq-upload-cancel" href="#"></a>
                                        <a class="qq-upload-retry-selector qq-upload-retry" href="#"></a>
                                        <a class="qq-upload-delete-selector qq-upload-delete" href="#"></a>
                                        <span class="qq-upload-status-text-selector qq-upload-status-text"></span>
                                    </li>
                                </ul>

                        </script>

                    </div>
                    <div class="rght-div">
                        <input type="text" id="child-video-upload-ahead-1" name="video-title" class="ahead plot-video-file-required"
                               placeholder="Добавить заголовок"/>
                        <textarea id="child-video-upload-descr-1" class="ahead plot-video-file-required" name="video-description"
                                  placeholder="Добавить описание" class="ahead"/></textarea>

                        <button type="submit" class="button brown"><span>Добавить</span></button>
                    </div>
                </fieldset>

                <input type="hidden" name="add-video" value="video-file"/>

                <?php echo JHtml::_('form.token'); ?>
            </form>
        </div>
    </div>    
    <?php # </editor-fold> ?>

    <?php # <editor-fold defaultstate="collapsed" desc="ADD CERTIFICATE"> ?>
    <div id="child-add-certificate" class="add-certificate">
        <form id="plot-certificate-form" method="post" enctype="multipart/form-data"
              action="<?php echo JRoute::_('index.php?option=com_plot&view=profile&task=profile.photoupload'); ?>"
              onsubmit="return certificatValidate();">
            <input type="hidden" name="img-type" value="certificate">
            <input type="hidden" name="img-src" id="certificate_src">
            <fieldset>
                <div class="input-wrapper">
                    <div id="prev_child-certificate-upl-1"></div>
                    <svg id="certificate-close-button" onclick="certificatDelete();" viewBox="0 0 31.5 31.5" preserveAspectRatio="xMinYMin meet" style="fill:url(#gradient-blue); display: none;" class="close-svg">
                        <use xlink:href="#close"></use>
                    </svg>
                    <label id="plot-certificate-image">
                            <span id="certificate-image-area">
                            <svg viewBox="0 0 24.5 26.4" preserveAspectRatio="xMinYMin meet">
                                <use xlink:href="#photo"></use>
                            </svg>
                            </span>
                         <span class="upload-certificate-label"
                              >Загрузить сертификат</span>
                                <span id="certificate-upload-img"></span>
                    </label>
                    <a id="sertificate-modal-link" rel="{size: {x: 550, y: 500}, handler:'iframe', iframeOptions: {scrolling: 'no'}}" 
                       href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxCertificateImage'); ?>" class="modal"></a>
                    <script type="text/template" id="qq-template">
                        <div class="qq-uploader-selector qq-uploader">

                            <div class="qq-upload-button-selector qq-upload-button">
                                <img src="<?php echo $this->templateUrl; ?>/img/pre-loader-1.gif" alt="" style="opacity: 0;" class="pre-loader"
                                    />
                            </div>
        <span class="qq-drop-processing-selector qq-drop-processing" style="display: none;">

          <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
        </span>
                            <ul class="qq-upload-list-selector qq-upload-list" style="display: none;">
                                <li>
                                    <div class="qq-progress-bar-container-selector">
                                        <div class="qq-progress-bar-selector qq-progress-bar"></div>
                                    </div>
                                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                                    <span class="qq-edit-filename-icon-selector qq-edit-filename-icon"></span>
                                    <span class="qq-upload-file-selector qq-upload-file"></span>
                                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0"
                                           type="text">
                                    <span class="qq-upload-size-selector qq-upload-size"></span>
                                    <a class="qq-upload-cancel-selector qq-upload-cancel" href="#"></a>
                                    <a class="qq-upload-retry-selector qq-upload-retry" href="#"></a>
                                    <a class="qq-upload-delete-selector qq-upload-delete" href="#"></a>
                                    <span class="qq-upload-status-text-selector qq-upload-status-text"></span>
                                </li>
                            </ul>

                    </script>
                </div>
                <div class="rght-div">
                    <input type="text" id="child-certificate-ahead-1" class="ahead plot-certificate-required"
                           placeholder="Добавить заголовок"
                           name="img_title"/>
                    <textarea id="child-certificate-descr-1" placeholder="Добавить описание" class="ahead plot-certificate-required"
                              name="img_description"></textarea>
                    <button type="submit" class="button brown"><span>Добавить</span></button>
                    <div class="datepicker">
                        <span>Дата получения:</span>
                        <input type="text" id="child-certificate-date" name="img-date" class="plot-certificate-required" />
                        <label for="child-certificate-date">
                            <svg viewBox="0 0 26 24" preserveAspectRatio="xMinYMin meet">
                                <use xlink:href="#datepicker" />
                            </svg>
                        </label>
                    </div>
                </div>
            </fieldset>
            <?php echo JHtml::_('form.token'); ?>
        </form>
    </div>
    <?php # </editor-fold> ?>

    <?php # <editor-fold defaultstate="collapsed" desc="ADD EVENT"> ?>
    <div id="child-add-event" class="add-event child-profile">
        <form id="plot-event-form" method="POST" enctype="multipart/form-data" action="<?php echo JRoute::_('index.php?option=com_plot&view=profile&task=profile.eventupload'); ?>"   onsubmit="return eventValidate();">
            <fieldset>
                <div class="child-profile datepicker">
                    <div class="one-row-wrap">
                        <span>Начало:</span>
                        <input type="text" id="child-event-from" name="start_date" class="plot-event-required"/>
                        <label for="child-event-from">
                            <svg viewBox="0 0 26 24" preserveAspectRatio="xMinYMin meet">
                                <use xlink:href="#datepicker"/>
                            </svg>
                        </label>
                    </div>
                    <div class="one-row-wrap">
                        <span>Время:</span>
                        <input type="text" id="child-event-from-time" name="start_date_time" class="plot-event-required" maxlength="5" autocomplete="off"/>
                        <label for="child-event-from-time">
                            <svg viewBox="0 0 25.7 26.5" preserveAspectRatio="xMinYMin meet">
                                <use xlink:href="#clock"></use>
                            </svg>
                        </label>
                    </div>
                    <div class="one-row-wrap">
                        <span>Окончание:</span>
                        <input type="text" id="child-event-to" name="end_date" class="plot-event-required"/>
                        <label for="child-event-to">
                            <svg viewBox="0 0 26 24" preserveAspectRatio="xMinYMin meet">
                                <use xlink:href="#datepicker"/>
                            </svg>
                        </label>
                    </div>
                    <div class="one-row-wrap">
                        <span>Время:</span>
                        <input type="text" id="child-event-to-time" name="end_date_time" class="plot-event-required" maxlength="5" autocomplete="off"/>
                        <label for="child-event-from-time">
                            <svg viewBox="0 0 25.7 26.5" preserveAspectRatio="xMinYMin meet">
                                <use xlink:href="#clock"></use>
                            </svg>
                        </label>
                    </div>
                </div>
                <div class="rght-div">
                    <input type="text" id="child-event-ahead-1" class="ahead plot-event-required" placeholder="Добавить тему"
                           name="title"/>
                    <input type="text" id="child-event-place-1" placeholder="Добавить место" class=" ahead plot-event-required" name="place"/>

                </div>
                <textarea id="child-event-descr-1" class="plot-event-required" placeholder="Добавить описание" name="description"/></textarea>
                <input type="hidden" name="event-id" id="event-id" />
                <div class="input-wrapper add-event">
                    <svg id="event-close-button"  onclick="eventDelete();" viewBox="0 0 31.5 31.5" preserveAspectRatio="xMinYMin meet" style="fill:url(#gradient-blue); display: none;" class="close-svg">
                        <use xlink:href="#close"></use>
                    </svg>
                    <label id="event-lbl">
                            <span id="event-image-area">
                                <svg viewBox="0 0 54.2 47.1" preserveAspectRatio="xMinYMin meet">
                                    <use xlink:href="#picture-upl"></use>
                                </svg>
                            </span>
                        <span class="upload-img-label" >Загрузить фото</span>
                        <span id="event-upload-img"></span>
                        <a id="event-modal-link"
                           rel="{size: {x: 550, y: 500}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
                           href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxEventImage'); ?>"
                           class="modal"></a>
                        <script type="text/template" id="qq-template">
                            <div class="qq-uploader-selector qq-uploader">

                                <div class="qq-upload-button-selector qq-upload-button">
                                    <img src="<?php echo $this->templateUrl; ?>/img/pre-loader-1.gif" alt="" style="opacity: 0;" class="pre-loader"
                                        />
                                </div>
                                    <span class="qq-drop-processing-selector qq-drop-processing" style="display: none;">

          <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
        </span>
                                <ul class="qq-upload-list-selector qq-upload-list" style="display: none;">
                                    <li>
                                        <div class="qq-progress-bar-container-selector">
                                            <div class="qq-progress-bar-selector qq-progress-bar"></div>
                                        </div>
                                        <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                                                    <span
                                                        class="qq-edit-filename-icon-selector qq-edit-filename-icon"></span>
                                        <span class="qq-upload-file-selector qq-upload-file"></span>
                                        <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0"
                                               type="text">
                                        <span class="qq-upload-size-selector qq-upload-size"></span>
                                        <a class="qq-upload-cancel-selector qq-upload-cancel" href="#"></a>
                                        <a class="qq-upload-retry-selector qq-upload-retry" href="#"></a>
                                        <a class="qq-upload-delete-selector qq-upload-delete" href="#"></a>
                                        <span class="qq-upload-status-text-selector qq-upload-status-text"></span>
                                    </li>
                                </ul>
                            </div>
                        </script>
                    </label>
                    <button type="submit" class="button brown"><span>Добавить</span></button>
                </div>
                <div class="chosen-wrapper">
                    <span>Возраст:</span>
                    <?php if($this->ages){ ?>
                        <select multiple="multiple" id="event_ages" name="event_age[]">
                            <?php foreach($this->ages AS $age){ ?>
                            <option value="<?php echo $age->id;?>"><?php echo $age->title; ?></option>
                            <?php } ?>
                        </select>
                    <?php } ?>
                </div>
                <div class="chosen-wrapper">
                    <span>Интересы:</span>
                    <?php
                    if($this->tags){
                        ?>
                        <select multiple="multiple" id="event-tags" name="tags[]">
<!--                            <option value="1">Игрушки</option>-->
                            <?php foreach($this->tags AS $tag){?>
                            <option value="<?php echo $tag->id;?>"><?php echo $tag->title; ?></option>
                            <?php } ?>
                        </select>
                    <?php } ?>
                </div>
            </fieldset>
        </form>
    </div>    
    <?php # </editor-fold> ?>

    <?php # <editor-fold defaultstate="collapsed" desc="ADD COURSE"> ?>
    <div id="child-add-course" class="add-course child-profile">
        <form action="<?php echo JRoute::_('index.php?option=com_plot&task=course.createSmallCourse'); ?>" method="POST" enctype="multipart/form-data" id="add-course-form">
            <fieldset>
                <input type="text" name="title" class="ahead" placeholder="Добавить название курса/объясняшки"/>
                <div class="chosen-wrapper add-course">
                    <select id="add-course-category" name="catogoryId">
                        <option value="0" selected="selected">--Выберите категорию--</option>
                        <?php foreach ($this->coursesCategories AS $courseCategory) { ?>
                        <option value="<?php echo $courseCategory->id; ?>"><?php echo $courseCategory->c_category; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div id="child-add-course-video" class="add-video">
                    <ul class="add-activity-tabs video">
                        <li><a href="#child-add-course-video-link">Добавить ссылку на видео</a></li>
                        <li><a href="#child-add-course-video-upload">Загрузить видео</a></li>
                    </ul>
                    <div id="child-add-course-video-link">
                        <fieldset>
                            <a rel="{size: {x: 550, y: 500}, handler:'iframe', iframeOptions: {scrolling: 'no'}}" href="#" class="modal"></a>
                            <div class="input-wrapper">
                                <p class="als-label course">
                                    <svg id="video-course-close-button" onclick="videolinkCourseDelete()" viewBox="0 0 31.5 31.5" preserveAspectRatio="xMinYMin meet" 
                                         style="fill:url(#gradient-blue); display: none;" class="close-svg">
                                        <use xlink:href="#close"></use>
                                    </svg>
                                    <img id="video-youtube-img" src="<?php echo JUri::root() . 'templates/plot/img/blank150x150.jpg'; ?>" alt=""/></p>
                            </div>
                            <div class="rght-div-course">
                                <input name="video-link" type="url" class="ahead plot-video-link-required" placeholder="Вставьте ссылку на видео"/>
                                <svg viewBox="0 0 150 62.6" preserveAspectRatio="xMinYMin meet" class="video-link-icon"><use xlink:href="#youtube"></use></svg>
                                <img class="pre-loader" style="opacity: 0;"/>
                                <input name="video-title" type="text"  id="course-video-link" class="ahead plot-video-link-required" placeholder="Добавить заголовок"/>
                                <textarea name="video-description" placeholder="Добавить описание" class="ahead"/></textarea>
                            </div>
                        </fieldset>
                    </div>
                    <div id="child-add-course-video-upload">
                        <fieldset>
                            <div class="input-wrapper add-course">
                                <label id="plot-course-video-file-image">
                                    <span id="course-videofile-image-area"><svg viewBox="0 0 25.3 22.3" preserveAspectRatio="xMinYMin meet"><use xlink:href="#video"></use></svg></span>
                                    <span class="upload-videofile-label">Загрузить видео</span>
                                    <span id="course-video-uploader"></span>
                                </label>
                                <input type="hidden" name="course-videofile-id" id="course-videofile-id"/>
                                <a id="course-videofile-modal-link" rel="{size: {x: 550, y: 500}, handler:'iframe', iframeOptions: {scrolling: 'no'}}" href="#" class="modal"></a>
                            </div>
                            <div class="rght-div-course">
                                <input type="text" class="ahead plot-video-file-required" name="uploaded-video-title" placeholder="Добавить заголовок"/>
                                <textarea class="ahead plot-video-file-required" name="uploaded-video-description" placeholder="Добавить описание" class="ahead"/></textarea>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="questions-block">
                    <div class="questions">
                        <span>Вопросы к видео:</span>
                        <p>Укажите 3 вопроса</p>
                        <textarea name="question1" placeholder="Напишите вопрос" class="ahead"></textarea>
                    </div>
                    <div class="answers">
                        <span>Ответы:</span>
                        <p>Укажите ответы и задайте правильный ответ</p>
                        <p>
                            <input name="question1-right-answer" type="radio" id="radio-answ-1-1" name="q-1" value="1" checked/>
                            <label for="radio-answ-1-1"></label>
                            <input name="question1-answer1" type="text" id="answ-1-1" placeholder="Вариант ответа" />
                        </p>
                        <p>
                            <input name="question1-right-answer" type="radio" id="radio-answ-1-2" name="q-1" value="2"/>
                            <label for="radio-answ-1-2"></label>
                            <input name="question1-answer2" type="text" id="answ-1-2" placeholder="Вариант ответа" />
                        </p>
                        <p>
                            <input name="question1-right-answer" type="radio" id="radio-answ-1-3" name="q-1" value="3"/>
                            <label for="radio-answ-1-3"></label>
                            <input name="question1-answer3" type="text" id="answ-1-3" placeholder="Вариант ответа" />
                        </p>
                        <p>
                            <input name="question1-right-answer" type="radio" id="radio-answ-1-4" name="q-1" value="4"/>
                            <label for="radio-answ-1-4"></label>
                            <input name="question1-answer4" type="text" id="answ-1-4" placeholder="Вариант ответа" />
                        </p>
                        <p>
                            <input name="question1-right-answer" type="radio" id="radio-answ-1-5" name="q-1" value="5"/>
                            <label for="radio-answ-1-5"></label>
                            <input name="question1-answer5" type="text" id="answ-1-5" placeholder="Вариант ответа" />
                        </p>
                    </div>
                </div>
                <div class="questions-block">
                    <div class="questions">
                        <span>Вопросы к видео:</span>
                        <p>Укажите 3 вопроса</p>
                        <textarea name="question2" placeholder="Напишите вопрос" class="ahead"></textarea>
                    </div>
                    <div class="answers">
                        <span>Ответы:</span>
                        <p>Укажите ответы и задайте правильный ответ</p>
                        <p>
                            <input name="question2-right-answer" type="radio" id="radio-answ-2-1" name="q-2" value="1" checked/>
                            <label for="radio-answ-2-1"></label>
                            <input name="question2-answer1" type="text" id="answ-2-1" placeholder="Вариант ответа" />
                        </p>
                        <p>
                            <input name="question2-right-answer" type="radio" id="radio-answ-2-2" name="q-2" value="2"/>
                            <label for="radio-answ-2-2"></label>
                            <input name="question2-answer2" type="text" id="answ-2-2" placeholder="Вариант ответа" />
                        </p>
                        <p>
                            <input name="question2-right-answer" type="radio" id="radio-answ-2-3" name="q-2" value="3"/>
                            <label for="radio-answ-2-3"></label>
                            <input name="question2-answer3" type="text" id="answ-2-3" placeholder="Вариант ответа" />
                        </p>
                        <p>
                            <input name="question2-right-answer" type="radio" id="radio-answ-2-4" name="q-2" value="4"/>
                            <label for="radio-answ-2-4"></label>
                            <input name="question2-answer4" type="text" id="answ-2-4" placeholder="Вариант ответа" />
                        </p>
                        <p>
                            <input name="question2-right-answer" type="radio" id="radio-answ-2-5" name="q-2" value="5"/>
                            <label for="radio-answ-2-5"></label>
                            <input name="question2-answer5" type="text" id="answ-2-5" placeholder="Вариант ответа" />
                        </p>
                    </div>
                </div>
                <div class="questions-block">
                    <div class="questions">
                        <span>Вопросы к видео:</span>
                        <p>Укажите 3 вопроса</p>
                        <textarea name="question3" placeholder="Напишите вопрос" class="ahead"></textarea>
                    </div>
                    <div class="answers">
                        <span>Ответы:</span>
                        <p>Укажите ответы и задайте правильный ответ</p>
                        <p>
                            <input name="question3-right-answer" type="radio" id="radio-answ-3-1" name="q-3" value="1" checked/>
                            <label for="radio-answ-3-1"></label>
                            <input name="question3-answer1" type="text" id="answ-3-1" placeholder="Вариант ответа" />
                        </p>
                        <p>
                            <input name="question3-right-answer" type="radio" id="radio-answ-3-2" name="q-3" value="2"/>
                            <label for="radio-answ-3-2"></label>
                            <input name="question3-answer2" type="text" id="answ-3-2" placeholder="Вариант ответа" />
                        </p>
                        <p>
                            <input name="question3-right-answer" type="radio" id="radio-answ-3-3" name="q-3" value="3"/>
                            <label for="radio-answ-3-3"></label>
                            <input name="question3-answer3" type="text" id="answ-3-3" placeholder="Вариант ответа" />
                        </p>
                        <p>
                            <input name="question3-right-answer" type="radio" id="radio-answ-3-4" name="q-3" value="4"/>
                            <label for="radio-answ-3-4"></label>
                            <input name="question3-answer4" type="text" id="answ-3-4" placeholder="Вариант ответа" />
                        </p>
                        <p>
                            <input name="question3-right-answer" type="radio" id="radio-answ-3-5" name="q-3" value="5"/>
                            <label for="radio-answ-3-5"></label>
                            <input name="question3-answer5" type="text" id="answ-3-5" placeholder="Вариант ответа" />
                        </p>
                    </div>
                </div>
                <div class="chosen-wrapper">
                    <span>Возраст:</span>
                    <?php if($this->ages) { ?>
                        <select multiple="multiple" id="new_course_ages" name="new_course_ages[]">
                            <?php foreach($this->ages AS $age){ ?>
                                <option value="<?php echo $age->id;?>"><?php echo $age->title; ?></option>
                            <?php } ?>
                        </select>
                    <?php } ?>
                </div>
                <div class="chosen-wrapper">
                    <span>Интересы:</span>
                    <?php if($this->tags){ ?>
                        <select multiple="multiple" id="new_course_tags" name="new_course_tags[]">
<!--                            <option value="1">Игрушки</option>-->
                            <?php foreach($this->tags AS $tag){ ?>
                                <option value="<?php echo $tag->id;?>"><?php echo $tag->title; ?></option>
                            <?php } ?>
                        </select>
                    <?php } ?>
                </div>
                <button class="button brown" type="submit"><span>Добавить</span></button>
            </fieldset>
        </form>
    </div>
    <?php # </editor-fold> ?>
    
</div>

<script src="<?php echo $this->templateUrl; ?>/js/all.fineuploader-5.0.8.min.js"></script>

<script type="text/javascript">
jQuery(document).ready(function () {

    jQuery('#event_ages_chzn, #event_tags_chzn, #add_course_category_chzn, #new_course_ages_chzn, #new_course_tags_chzn').live('click', function(){
        jQuery(this).find('.chzn-drop').jScrollPane();
    });

    var manualUploaderVideofile = new qq.FineUploader({
        element: document.getElementById("videofile-upload-img"),
   //jQuery('#videofile-upload-img').fineUploader({
        request: {
            endpoint: '<?php echo JRoute::_("index.php?option=com_plot&task=profile.videoFileUpload")?>'
        },
        validation: {
            sizeLimit: parseInt('<?php echo (int)PlotHelper::returnBytes(ini_get('upload_max_filesize'));?>')
        },
        multiple: false,
        callbacks: {
            onComplete: function (id, filename, responseJSON) {
                if (responseJSON.status) {
                    jQuery('#videofile-modal-link').attr('href', '<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxVideofileImage'); ?>&img_name=' + responseJSON.name);
                    jQuery('#videofile-id').val(responseJSON.name);
                    document.getElementById('videofile-modal-link').click();
                    jQuery('.pre-loader').css('opacity','0');
                    jQuery('#plot-dinamicaly-loading').css({'z-index': '75555'}).show();
                } else {
                    errorAddPlotMessage(responseJSON.message);
                }
            }
        }
    });

    var manualUploaderCourse = new qq.FineUploader({
        element: document.getElementById("course-video-uploader"),
    //jQuery('#course-video-uploader').fineUploader({
        request: {
            endpoint: '<?php echo JRoute::_("index.php?option=com_plot&task=profile.uploadSmallCourseVideo")?>',
            inputName: 'uploaded-video-file'
        },
        validation: {
            sizeLimit: parseInt('<?php echo (int)PlotHelper::returnBytes(ini_get('upload_max_filesize'));?>')
        },
        multiple: false,
        callbacks: {
            onComplete: function (id, filename, responseJSON) {
                if (responseJSON.success) {
                    jQuery('#add-course-form #course-videofile-modal-link').attr('href', '<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxCourseVideofileImage'); ?>&imgUrl=' + responseJSON.imageUrl);
                    jQuery('#add-course-form #course-videofile-id').val(responseJSON.name);
                    jQuery('#add-course-form #course-videofile-modal-link')[0].click();
                    jQuery('.pre-loader').css('opacity','0');
                    jQuery('#plot-dinamicaly-loading').css({'z-index': '75555'}).show();
                } else {
                    errorAddPlotMessage(responseJSON.message);
                }
            }
        }
    });    

    //upload image for event
    var manualUploaderEvent = new qq.FineUploader({
        element: document.getElementById("event-upload-img"),
    //jQuery('#event-upload-img').fineUploader({
        request: {
            endpoint: '<?php echo JRoute::_("index.php?option=com_plot&task=profile.eventImageUpload")?>'
        },
        validation: {
            sizeLimit: parseInt('<?php echo (int)PlotHelper::returnBytes(ini_get('upload_max_filesize'));?>')
        },
        multiple: false,
        callbacks: {
            onComplete: function (id, filename, responseJSON) {
                if (responseJSON.status) {
                    jQuery('#event-id').val(responseJSON.id);
                    jQuery('#event-modal-link').attr('href', '<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxEventImage'); ?>&img_name=' + responseJSON.name);
                    document.getElementById('event-modal-link').click();
                    jQuery('.pre-loader').css('opacity','0');
                    jQuery('#plot-dinamicaly-loading').css({'z-index': '75555'}).show();
                } else {
                    errorAddPlotMessage(responseJSON.message);
                }
            }
        }
    });

    //upload image foe certificate
    var manualUploaderCertificate = new qq.FineUploader({

        element: document.getElementById("certificate-upload-img"),
    //jQuery('#certificate-upload-img').fineUploader({
        request: {
            endpoint: '<?php echo JRoute::_("index.php?option=com_plot&task=profile.certificatImageUpload")?>'
        },
        validation: {
            sizeLimit: parseInt('<?php echo (int)PlotHelper::returnBytes(ini_get('upload_max_filesize'));?>')
        },
        multiple: false,
        callbacks: {
            onComplete: function (id, filename, responseJSON) {
                if (responseJSON.status) {
                    jQuery('#sertificate-id').val(responseJSON.id);
                    jQuery('#sertificate-modal-link').attr('href', '<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxCertificateImage'); ?>&img_id=' + responseJSON.name);
                    document.getElementById('sertificate-modal-link').click();
                    jQuery('.pre-loader').fadeOut("slow").css("opacity", 1);
                    jQuery('#plot-dinamicaly-loading').css({'z-index': '75555'}).show();
                } else {
                    errorAddPlotMessage(responseJSON.message);
                }
            }
        }
    });

    //upload image for photo
    var manualUploaderPhoto = new qq.FineUploader({

    //jQuery('#photo-upload-img').fineUploader({
        element: document.getElementById("photo-upload-img"),
        request: {
            endpoint: '<?php echo JRoute::_("index.php?option=com_plot&task=profile.photoImageUpload")?>'
        },
        validation: {
           sizeLimit: parseInt('<?php echo (int)PlotHelper::returnBytes(ini_get('upload_max_filesize'));?>')
        },
        multiple: false,
        callbacks: {
            onComplete: function (id, filename, responseJSON, xhr) {
                if (responseJSON.status) {
                    jQuery('#photo-id').val(responseJSON.id);
                    jQuery('#photo-modal-link').attr('href', '<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxPhotoImage'); ?>&img_id=' + responseJSON.name);
                    document.getElementById('photo-modal-link').click();
                    jQuery('.pre-loader').fadeOut("slow").css("opacity", 1);

                } else {
                    errorAddPlotMessage(responseJSON.message);
                }
            }
        }
    });
    
    jQuery('#child-video-link').blur(function () {
            var link = jQuery('#child-video-link').val(),
                expression = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gi,
                regex = new RegExp(expression),
                youtubeNumber = '',
                video_img = jQuery('#video-youtube-img').attr('src'),
                pos = '',
                youTubeURL,
                json;

            if (video_img.indexOf("blank150x150.jpg") != -1) {
                removeErrorClass();
                jQuery('.pre-loader').css('opacity', '0');
                if (link.match(regex)) {
                    if (strpos(link, 'youtube.com') !== false || strpos(link, 'youtu.be') !== false) {
                        if (strripos(link, 'youtube.com') !== false) {
                            youtubeNumber = substr(link, strripos(link, '?v=') + 3);
                            if ((pos = strpos(youtubeNumber, '&')) !== false) youtubeNumber = substr(youtubeNumber, 0, pos)
                            {
                                youTubeURL ='https://www.googleapis.com/youtube/v3/videos?id=' + youtubeNumber + '&key=AIzaSyBDjpX4FVkv1CqrGSZqzGAxU1aTrwWA7y4&part=snippet';
                                //youTubeURL = 'http://gdata.youtube.com/feeds/api/videos/' + youtubeNumber + '?v=2&alt=json';
                                json = (function () {
                                    var json = null;
                                    jQuery.ajax({
                                        'async': false,
                                        'global': false,
                                        'url': youTubeURL,
                                        'dataType': "jsonp",
                                        'success': function (data) {
                                            json = data;
                                            if (json) {
                                                jQuery('#child-video-link-ahead-1').val(json.items[0].snippet.title);
                                                jQuery('#child-video-link-descr-1').val(json.items[0].snippet.description);
                                            }

                                        }
                                    });

                                })();

                                jQuery.post('index.php?option=com_plot&task=profile.uploadVideoImg', {youtubeNumber: youtubeNumber}, function (response) {
                                    if (response.status) {
                                        jQuery('#video-modal-link').attr('href', '<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxVideoImage');?>' + '&img_name=' + youtubeNumber);
                                        console.log( '<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxVideoImage');?>' + '&img_name=' + youtubeNumber );
                                        document.getElementById('video-modal-link').click();
                                        jQuery('.pre-loader').css('opacity', '0');
                                } else {
                                        jPlot.showMessage(response.message);
                                        jQuery('.plot-video-link-required').each(function() {
                                            if(jQuery(this).val()==''){
                                                addErrorClass(jQuery(this));
                                            }
                                        });
                                }
                            });


                        }
                    } else {
                        alert('youtube.com');
                        jQuery('.pre-loader').css('opacity', '0');
                    }
                } else {
                    alert('youtube.com');
                    jQuery('.pre-loader').css('opacity', '0');
                }

            } else {
                jQuery('.plot-video-link-required').each(function() {
                    if(jQuery(this).val()==''){
                        addErrorClass(jQuery(this));
                    }
                });
                jQuery('.pre-loader').css('opacity', '0');
            }
        }
    });    
    
    jQuery('#add-course-form input[name=video-link]').change(function () {
        var link = jQuery('#add-course-form input[name=video-link]').val(),
            youtubeNumber = '',
            pos = '',
            youTubeURL;

        jQuery('.pre-loader').css('opacity', '0');

        if (jPlot.youtube.isValidUrl(link)) {
            jQuery('#add-course-form [name=video-link]').removeClass('plot_error');
            youtubeNumber = substr(link, strripos(link, '?v=') + 3);
            pos = strpos(youtubeNumber, '&');
            if (pos !== false) {
                youtubeNumber = substr(youtubeNumber, 0, pos);
            }
            youTubeURL ='https://www.googleapis.com/youtube/v3/videos?id=' + youtubeNumber + '&key=AIzaSyBDjpX4FVkv1CqrGSZqzGAxU1aTrwWA7y4&part=snippet';
            jQuery.ajax({
                'async': false,
                'global': false,
                'url': youTubeURL,
                'dataType': "json",
                'success': function (data) {
                    jQuery('#add-course-form [name=video-title]').val(data.items[0].snippet.title);
                    jQuery('#add-course-form [name=video-description]').val(data.items[0].snippet.description);
                }
            });
            jQuery.post('index.php?option=com_plot&task=profile.uploadVideoImg', {youtubeNumber: youtubeNumber}, function (response) {
                if (response.status) {
                    jQuery('#video-modal-link').attr('href', '<?php echo JRoute::_('index.php?option=com_plot&task=profile.cropCourseImageFromYoutubeLink');?>' + '&img_name=' + youtubeNumber);
                    document.getElementById('video-modal-link').click();
                    jQuery('.pre-loader').css('opacity', '0');
                }
            });
        } else {
            jQuery('#add-course-form [name=video-link]').addClass('plot_error');
        }
    });    
    
});

function eventValidate() {
    var event_tags = jQuery("select[name='tags[]'] option:selected").length,
        noEventError = true,
        event_ages = jQuery("#event_ages_chzn").find('.chzn-choices').find('.search-choice').length,
        interests = jQuery("#event_tags_chzn").find('.chzn-choices').find('.search-choice').length,
        event_id = jQuery('#event-id').val(),
        current_date=new Date(),
        pattern = /(\d{2})\.(\d{2})\.(\d{4})/,
        event_date_from=jQuery('#child-event-from').val(),
        dt_from;
    removeErrorClass();
    jQuery('.plot-event-required').each(function() {
        if(jQuery(this).val()==''){
            noEventError=false;
            addErrorClass(jQuery(this));
        }
    });
    if (event_tags==0) {
        noEventError = false;
        addErrorClass(jQuery("#tags_chzn").find('.chzn-choices'));
    }
    if (event_ages==0) {
        noEventError = false;
        addErrorClass(jQuery("#event_ages_chzn").find('.chzn-choices'));
    }
    if (interests==0) {
        noEventError = false;
        addErrorClass(jQuery("#event_tags_chzn").find('.chzn-choices'));
    }
    if (!event_id) {
        noEventError = false;
        addErrorClass(jQuery("#event-lbl"));
    }

    if(event_date_from){
        dt_from=new Date(event_date_from.replace(pattern,'$3-$2-$1'));
        if(current_date > dt_from){
            noEventError=false;
            addErrorClass(jQuery("#child-event-from"));
            errorAddPlotMessage('<?php echo JText::_("COM_PLOT_DO_NOT_CREATE_OLD_EVENT"); ?>');
        }
    }

    if (noEventError) {
        jQuery('#plot-event-form button[type="submit"]').attr('disabled', 'disabled');
    }
    return noEventError;
}

function certificatValidate() {
    var noCertificateError=true,
        certificate_src = jQuery('#certificate_src').val();
    removeErrorClass();

    jQuery('.plot-certificate-required').each(function() {
        if(jQuery(this).val()==''){
            noCertificateError=false;
            addErrorClass(jQuery(this));
        }
    });

   if (jQuery('#child-certificate-date').val() != '') {
        if (!compareDateWithToday(jQuery('#child-certificate-date').val())) {
            noCertificateError=false;
            errorAddPlotMessage('Дата получения сертификата больше текущей');
            addErrorClass(jQuery('#child-certificate-date'));
        }
    }

    if (!certificate_src) {
        noCertificateError = false;
        addErrorClass(jQuery('#plot-certificate-image'));
    }
    if (noCertificateError) {
        jQuery('#plot-certificate-form button[type="submit"]').attr('disabled', 'disabled');
    }
    return noCertificateError;
}

function photoValidate() {
    var noPhotoError = true,
        photo_src = jQuery('#photo_src').val();
    removeErrorClass();

    jQuery('.plot-photo-required').each(function() {
        if (jQuery(this).val() == '') {
            noPhotoError = false;
            addErrorClass(jQuery(this));
        }
    });

    if (!photo_src) {
        noPhotoError = false;
        addErrorClass(jQuery('#plot-photo-image'));
    }
    if (noPhotoError) {
        jQuery('#plot-photo-form button[type="submit"]').attr('disabled', 'disabled');
    }

    return noPhotoError;
}

function videoValidate() {

    var no_video_link_error = true,
        video_img = jQuery('#video-youtube-img').attr('src');
    removeErrorClass();
    jQuery('#form-child-add-video-link .plot-video-link-required').each(function() {
        if (jQuery(this).val() == '') {
            no_video_link_error = false;
            addErrorClass(jQuery(this));
        }
    });
    if (video_img.indexOf("blank150x150.jpg") != -1) {
        no_video_link_error = false;
        addErrorClass(jQuery(jQuery('#video-youtube-img')));
    }
    if (no_video_link_error) {
        jQuery('#form-child-add-video-link button.add').attr('disabled', 'disabled');
    }

    return no_video_link_error;


}

function videoFileValidate() {
    var video_file_error=true,
        videofile_id=jQuery('#videofile-id').val();
    removeErrorClass();
    jQuery('#form-child-add-video-upload .plot-video-file-required').each(function() {
        if (jQuery(this).val() == '') {
            video_file_error = false;
            addErrorClass(jQuery(this));
        }
    });

    if(!videofile_id){
        video_file_error=false;
        addErrorClass(jQuery(jQuery('#plot-video-file-image')));
    }
    
    return video_file_error;
}

function videoFileDelete() {
    var file_name = jQuery('#videofile-id').val();
    if (file_name) {
        jQuery.post('index.php?option=com_plot&task=profile.videoFileDeleteImg', {
            file_name: file_name
        }, function(response) {
            var data = jQuery.parseJSON(response);
            if (data.status) {
                jQuery('#videofile-id').val('');
                jQuery('#videofile-image-area').html('<svg viewBox="0 0 25.3 22.3" preserveAspectRatio="xMinYMin meet"><use xlink:href="#video"></use></svg>');
                jQuery('#video-file-close-button').hide();
            } else {
                errorAddPlotMessage(data.message);
            }
        });
    }
}

function videolinkDelete() {
    var  video_img = jQuery('#video-youtube-img').attr('src'),
        link=jQuery('#child-video-link').val();

    if (video_img.indexOf("blank150x150.jpg") == -1) {
        jQuery.post('index.php?option=com_plot&task=profile.videolinkDeleteImg', {
            link: link
        }, function(response) {
            var data = jQuery.parseJSON(response);
            if (data.status) {
                jQuery('#video-youtube-img').attr("src","<?php echo JUri::root() . 'templates/plot/img/blank150x150.jpg'; ?>");
                jQuery('#video-link-close-button').hide();
            } else {
                errorAddPlotMessage(data.message);
            }
        });
    }
}

function videolinkCourseDelete() {
    var  video_img = jQuery('#add-course-form #video-youtube-img').attr('src'),
        link=jQuery('#course-video-link').val();

    if (video_img.indexOf("blank150x150.jpg") == -1) {
        jQuery.post('index.php?option=com_plot&task=profile.videolinkDeleteImg', {
            link: link
        }, function(response) {
            var data = jQuery.parseJSON(response);
            if (data.status) {
                jQuery('#add-course-form #video-youtube-img').attr("src","<?php echo JUri::root() . 'templates/plot/img/blank150x150.jpg'; ?>");
                jQuery('#video-course-close-button').hide();
            } else {
                errorAddPlotMessage(data.message);
            }
        });
    }
}

function certificatDelete() {
    var cert_id = jQuery('#sertificate-id').val();
    jQuery('#sertificate-id').val('');
    jQuery('#certificate_src').val('');
    jQuery('#certificate-image-area').html('<svg viewBox="0 0 24.5 26.4" preserveAspectRatio="xMinYMin meet"><use xlink:href="#photo"></use></svg>');
    jQuery('#certificate-close-button').hide();
    if (cert_id) {
        jQuery.post('index.php?option=com_plot&task=profile.certificateDelete', {cert_id: cert_id}, function (response) {
            var data = jQuery.parseJSON(response);
            if (!data.status) {
                errorAddPlotMessage(data.message);
            }
        });
    }
}

function eventDelete(){
    var event_id=jQuery('#event-id').val();
    console.log(event_id);
    if(event_id){
        jQuery.post('index.php?option=com_plot&task=profile.eventDelete', {event_id: event_id}, function (response) {
            var data=jQuery.parseJSON(response);
            if (data.status) {
                jQuery('#event-id').val('');
                jQuery('#event-image-area').html('<svg viewBox="0 0 54.2 47.1" preserveAspectRatio="xMinYMin meet"><use xlink:href="#picture-upl"></use></svg>');
                jQuery('#event-close-button').hide();
            }else{
                errorAddPlotMessage(data.message);
            }
        });
    }
}

function photoDelete() {
    var cert_id = jQuery('#photo-id').val();
    jQuery('#photo-id').val('');
    jQuery('#photo_src').val('');
    jQuery('#photo-image-area').html('<svg preserveAspectRatio="xMinYMin meet" viewBox="0 0 24.5 26.4"><use xlink:href="#photo"/></svg>');
    jQuery('#photo-close-button').hide();
    if (cert_id) {
        jQuery.post('index.php?option=com_plot&task=profile.certificateDelete', {cert_id: cert_id}, function (response) {
            var data = jQuery.parseJSON(response);
            if (!data.status) {
                errorAddPlotMessage(data.message);
            }
        });
    }
}

function errorAddPlotMessage(error_message){
    SqueezeBox.initialize({
        size: {x: 300, y: 150}
    });
    var str = '<div id="enqueued-message">' + error_message + '</div>';
    SqueezeBox.setContent('adopt', str);
    SqueezeBox.resize({x: 300, y: 150});
    jQuery('.pre-loader').css('opacity','0');
}

function addErrorClass(jquery_element){
    //var currentPlaceholder = jQuery(jquery_element).attr("placeholder");
    jQuery(jquery_element).addClass('plot_error');
    //jQuery(jquery_element).attr("placeholder", "Заполните поле \""+currentPlaceholder+"\"");
}

function removeErrorClass(){
    jQuery('.plot_error').removeClass('plot_error');
}

function compareDateWithToday(str){
    var pattern = /(\d{2})\.(\d{2})\.(\d{4})/,
    dt = new Date(str.replace(pattern,'$3-$2-$1')),
    today = new Date();
    if(dt.getTime() <= today){
      return true;
    } else {
       return false;
    }
}
function strpos(haystack, needle, offset) {
    var i = (haystack + '')
        .indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
}    
    
function strripos(haystack, needle, offset) {
    haystack = (haystack + '')
        .toLowerCase();
    needle = (needle + '')
        .toLowerCase();

    var i = -1;
    if (offset) {
        i = (haystack + '')
            .slice(offset)
            .lastIndexOf(needle); // strrpos' offset indicates starting point of range till end,
        // while lastIndexOf's optional 2nd argument indicates ending point of range from the beginning
        if (i !== -1) {
            i += offset;
        }
    } else {
        i = (haystack + '')
            .lastIndexOf(needle);
    }
    return i >= 0 ? i : false;
}

function substr(str, start, len) {
    var i = 0,
        allBMP = true,
        es = 0,
        el = 0,
        se = 0,
        ret = '';
    str += '';
    var end = str.length;

    // BEGIN REDUNDANT
    this.php_js = this.php_js || {};
    this.php_js.ini = this.php_js.ini || {};
    // END REDUNDANT
    switch ((this.php_js.ini['unicode.semantics'] && this.php_js.ini['unicode.semantics'].local_value.toLowerCase())) {
        case 'on':
            // Full-blown Unicode including non-Basic-Multilingual-Plane characters
            // strlen()
            for (i = 0; i < str.length; i++) {
                if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
                    allBMP = false;
                    break;
                }
            }

            if (!allBMP) {
                if (start < 0) {
                    for (i = end - 1, es = (start += end); i >= es; i--) {
                        if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
                            start--;
                            es--;
                        }
                    }
                } else {
                    var surrogatePairs = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;
                    while ((surrogatePairs.exec(str)) != null) {
                        var li = surrogatePairs.lastIndex;
                        if (li - 2 < start) {
                            start++;
                        } else {
                            break;
                        }
                    }
                }

                if (start >= end || start < 0) {
                    return false;
                }
                if (len < 0) {
                    for (i = end - 1, el = (end += len); i >= el; i--) {
                        if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
                            end--;
                            el--;
                        }
                    }
                    if (start > end) {
                        return false;
                    }
                    return str.slice(start, end);
                } else {
                    se = start + len;
                    for (i = start; i < se; i++) {
                        ret += str.charAt(i);
                        if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
                            se++; // Go one further, since one of the "characters" is part of a surrogate pair
                        }
                    }
                    return ret;
                }
                break;
            }
        // Fall-through
        case 'off':
        // assumes there are no non-BMP characters;
        //    if there may be such characters, then it is best to turn it on (critical in true XHTML/XML)
        default:
            if (start < 0) {
                start += end;
            }
            end = typeof len === 'undefined' ? end : (len < 0 ? len + end : len + start);
            // PHP returns false if start does not fall within the string.
            // PHP returns false if the calculated end comes before the calculated start.
            // PHP returns an empty string if start and end are the same.
            // Otherwise, PHP returns the portion of the string from start to end.
            return start >= str.length || start < 0 || start > end ? !1 : str.slice(start, end);
    }
    return undefined; // Please Netbeans
}

</script>
