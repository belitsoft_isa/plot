<?php
defined('_JEXEC') or die;
JHtml::_('formbehavior.chosen', 'select');
$app = JFactory::getApplication();
$doc = JFactory::getDocument();

?>

<link href="<?php echo $this->componentUrl; ?>/views/profile/tmpl/default.css" type="text/css" rel="stylesheet">

<script src="<?php echo $this->componentUrl; ?>/libraries/scrolling_ajax_pagination/javascript.js"></script>

<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.min.js"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.basic.js"></script>

<script src="<?php echo $this->componentUrl; ?>/assets/js/plot.js"></script>
<script type="text/javascript" src="<?php echo $this->componentUrl; ?>/views/profile/tmpl/default.js"></script>

<script src="<?php echo JURI::base(); ?>components/com_plot/assets/js/video.js"></script>
<script type="text/javascript"
        src="<?php echo JUri::root() . 'components/com_plot/libraries/preview_master_plugin/jquery.preimage.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo JUri::root() . 'components/com_plot/libraries/jquery_input_mask_plugin/jquery.mask.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $this->componentUrl; ?>/assets/js/jquery.fancybox.js"></script>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>

<script type="text/javascript">
    jQuery.post(
        'https://graph.facebook.com',
        {
            id: '<?php echo PlotHelper::getAbsRoutedUri('index.php?option=com_plot&view=profile&id='.$this->user->id); ?>/',
            scrape: 'true'
        }
    );

    videojs.options.flash.swf = "<?php echo JURI::base(); ?>components/com_plot/assets/js/video-js.swf";
    document.createElement('video');
    document.createElement('audio');
    document.createElement('track');

    jQuery(document).ready(function () {

        jQuery(".fancybox").fancybox({
            maxWidth: 800,
            maxHeight: 600,
            fitToView: false,
            width: '70%',
            height: '70%',
            autoSize: false,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none',
            helpers: {
                title: {
                    type: 'inside',
                    position: 'top'
                }
            },
            beforeLoad: function () {
                this.title = jQuery(this.element).attr('data-title'),
                    this.date = jQuery(this.element).attr('data-date'),
                    this.description = jQuery(this.element).attr('data-description'),
                    this.share = jQuery(this.element).attr('data-share');
            }
        });

        jQuery('#content').scrollPagination({
            nop: 1,
            offset: 0,
            error: 'No More Posts!',
            delay: 500,
            scroll: true,
            postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=river.ajaxNextRiverPart'); ?>'
        });


        //cancel request
        jQuery('.actions').on('click', '.plot-cancel-request', function (e) {
            e.preventDefault();
            var id = jQuery(this).attr('data-plot-friend-id'),
                that = this;
            jQuery.ajax({
                type: "POST",
                url: "index.php?option=com_plot&task=profile.cancelRequest",
                data: {
                    'id': id
                },
                success: function (response) {
                    var str = '';
                    str += '<div id="system-message">';
                    str += '<div class="alert alert-warning">';
                    str += '<a class="close" data-dismiss="alert">×</a>';
                    str += '<h4 class="alert-heading"></h4>';
                    str += '<div>';
                    str += '<p>' + response + '</p>';
                    str += '</div>';
                    str += '</div>';
                    str += '</div>';
                    jQuery('#system-message-container').html(str);
                    jQuery(that).removeClass('plot-cancel-request').addClass('plot-add-friend').text("<?php echo JText::_('COM_PLOT_ADD_AS_FRIEND'); ?>");
                }
            });
        });

        modalLoad();
        jQuery('#child-photo-upl-1').preimage();
        jQuery('#child-certificate-upl-1').preimage();

        allEventsLoad();

    });

    function removeFriend(friendId) {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxRemoveFriend', {friendId: friendId}, function (jsonResponce) {
            var responce = jQuery.parseJSON(jsonResponce),
                str = '',
                url = '<?php echo JRoute::_("index.php?option=com_plot&task=profile.addFriendAjax&id=".$this->user->id); ?>'
            if (!responce.error) {
                str += '<a class="button green modal" onclick="SqueezeBox.open(\'' + url + '\', {size: {x: 400, y: 400}, handler:\'iframe\'})" >';
                str += '<span>';
                str += '<svg viewBox="0 0 31.7 30.8" preserveAspectRatio="xMinYMin meet" class="friend"><use xlink:href="#friend"></use></svg>Дружить со мной</span></a>';
                jQuery('#friend-button-wrapp').html(str);
                SqueezeBox.initialize();
            } else {
                jPlot.showMessage(responce.error);
            }
        });
    }

    function rejectFriendRequest(friendId) {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxRejectFriendRequest', {friendId: friendId}, function (jsonResponce) {
            responce = jQuery.parseJSON(jsonResponce);
            if (!responce.error) {
                jQuery('.actions').html('<a class="button green h30" onclick="approveFriendRequest(' + responce.friendId + ');"><span>Дружить</span></a>');
            } else {
                alert('Error');
            }
        });
    }

    function addFriend(friendId) {
        jQuery.post('index.php?option=com_plot&task=profile.addFriendAjax', {id: friendId}, function (jsonResponce) {
            responce = jQuery.parseJSON(jsonResponce);
            if (!responce.error) {
                jQuery('.actions').html('<a class="button green h30" onclick="approveFriendRequest(' + responce.friendId + ');"><span>Дружить</span></a>');
            } else {
                alert('Error');
            }
        });
    }

    function cancelFriendRequest(friendId) {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxApproveFriendRequest', {friendId: friendId}, function (jsonResponce) {
            var responce = jQuery.parseJSON(jsonResponce);
            if (!responce.error) {
                jQuery('.actions').html('<a class="button brown h25" onclick="rejectFriendRequest(' + responce.friendId + ');"><span>Не дружить</span></a>');
            } else {
                alert('Error');
            }
        });
    }

    function modalLoad() {
        var modal = jQuery('.modal-overlay');
        <?php if ($this->video_links) { foreach($this->video_links AS $video) { ?>
        jQuery('#parent-video-id<?php echo $video->id; ?>, a[href="#parent-video-id<?php echo $video->id; ?>"]').click(function () {
            modal.css('display', 'block');
        });
        jQuery('#parent-video-id<?php echo $video->id; ?>, a[href="#parent-video-id<?php echo $video->id; ?>"]').click(function () {
            modal.css('display', 'block');
        });
        <?php } } ?>
        jQuery('#parent-video-id1, a[href="#parent-video-id1"]').click(function () {
            modal.css('display', 'block');
        });
        jQuery('.modal-close').click(function () {
            modal.css('display', 'none');
        });
        modal.click(function (event) {
            e = event || window.event;
            if (e.target == this) {
                jQuery(modal).css('display', 'none');
            }
        });
    }

    function allEventsLoad() {
        jQuery.post('index.php?option=com_plot&task=profile.ajaxAllEventsLoad&id=<?php echo $this->user->id;?>', function (html) {
            jQuery('#show-all-events .new-event').html(html);
            allEventsCarouselInitialize();
        });
    }

    function allEventsCarouselInitialize() {
        var element = jQuery('#show-all-events #events-jcarousel').jcarousel();
        var width = element.innerWidth() * 0.32,//32% ширина li в случае, если выводится по 3 элемента
            margin = element.innerWidth() * 0.02;//2% значение margin в случае
        element.jcarousel('items').css('width', width + 'px');
        element.jcarousel('items').css('margin-right', margin + 'px');
        jQuery('#show-all-events .jcarousel-control-prev').jcarouselControl({target: '-=3'});
        jQuery('#show-all-events .jcarousel-control-prev').on('jcarouselcontrol:active', function () {
            jQuery(this).addClass('active').removeClass('inactive');
        });
        jQuery('#show-all-events .jcarousel-control-prev').on('jcarouselcontrol:inactive', function () {
            jQuery(this).addClass('inactive').removeClass('active');
        });
        jQuery('#show-all-events .jcarousel-control-next').jcarouselControl({target: '+=3'});
        jQuery('#show-all-events .jcarousel-control-next').on('jcarouselcontrol:active', function () {
            jQuery(this).addClass('active').removeClass('inactive');
        });
        jQuery('#show-all-events .jcarousel-control-next').on('jcarouselcontrol:inactive', function () {
            jQuery(this).addClass('inactive').removeClass('active');
        });
        jQuery('#show-all-events #events-jcarousel').jcarousel('reload');
    }

</script>
<?php # </editor-fold> ?>

<?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_header')); ?>

<?php if ($this->my->id != $this->user->id) { ?>
    <?php if ($this->user->isParent()) { ?>
        <div class="looking-at-user-header parent-profile">
            <div class="wrap">
                <div class="name-row">
                    <span>Привет!</span><i>Я
                        - <?php echo $this->user->name; ?><?php echo ($this->user->getSocialFieldData('ADDRESS')->city) ? ',' : ''; ?> </i><span><?php echo ($this->user->getSocialFieldData('ADDRESS')->city) ? ($this->user->getSocialFieldData('ADDRESS')->city) : ''; ?></span>
                </div>
                <div class="user-top">
                    <a class="circle-img modal"
                       href="<?php echo JRoute::_('index.php?option=com_plot&task=profileedit.ajaxGetForeignChildProfilePopupHtml&id=' . (int)$this->user->id); ?>"
                       rel="{size: {x: 744, y:525}, handler:'iframe', iframeOptions: {scrolling: 'no'}}">
                        <img src="<?php echo $this->user->getSquareAvatarUrl(); ?>"
                             alt="<?php echo $this->user->name; ?>">
                    </a>
                    <button class="arrow-right"
                            onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_plot&task=registration.logout'); ?>';">
                        <?php echo JText::_('MOD_PLOT_HEADER_EXIT'); ?>
                    </button>
                </div>
            </div>
        </div>
    <?php } else { ?>
        <div class="looking-at-user-header child-profile">
            <div class="wrap">
                <div class="name-row">
                    <span>Привет!</span><i>Я
                        - <?php echo $this->user->name; ?><?php echo ($this->user->getSocialFieldData('ADDRESS')->city) ? ',' : ''; ?> </i><span><?php echo ($this->user->getSocialFieldData('ADDRESS')->city) ? ($this->user->getSocialFieldData('ADDRESS')->city) : ''; ?></span>
                </div>
                <a class="user-photo modal"
                   rel="{size: {x: 744, y: 500}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
                   href="<?php echo JRoute::_('index.php?option=com_plot&task=profileedit.ajaxGetForeignChildProfilePopupHtml&id=' . $this->user->id); ?>">
                    <img src="<?php echo $this->user->getSquareAvatarUrl(); ?>" alt="">
                </a>
                <ul class="header-profile-menu">
                    <li>
                        <a href="<?php echo JRoute::_('index.php?option=com_plot&task=registration.logout'); ?>"><?php echo JText::_('COM_PLOT_LOGOUT'); ?></a>
                    </li>
                </ul>
            </div>
        </div>
    <?php } ?>
<?php } ?>

<?php # <editor-fold defaultstate="collapsed" desc="TOP"> ?>
<?php if ($this->my->id == $this->user->id) { ?>
    <div class="top">

    <div class="wrap main-wrap child-profile">
    <div class="add-left aside-left child-profile"></div>
    <div class="add-right aside-right child-profile"></div>
    <?php # <editor-fold defaultstate="collapsed" desc="MENU LEFT TREE"> ?>
    <?php if ($this->my->id == $this->user->id) { ?>
        <menu class="child-menu">
            <li onclick="childMenuShow('courses');">
                <a href="#child-menu-courses" class="main-a"><?php echo JText::_('COM_PLOT_TO_LEARN'); ?></a>
                <?php if ($this->my->countNewCourses) { ?>
                    <b><?php echo $this->my->countNewCourses; ?></b>
                <?php } ?>
            </li>
            <li onclick="childMenuShow('read');">
                <a href="#child-menu-read" class="main-a"><?php echo JText::_('COM_PLOT_READ'); ?></a>
                <?php if ((int)$this->count_new) { ?>
                    <b><?php echo (int)$this->count_new; ?></b>
                <?php } ?>
            </li>
            <li onclick="childMenuShow('think');">
                <a href="#child-menu-think" class="main-a"><?php echo JText::_('COM_PLOT_TO_THINK'); ?></a>
            </li>
            <li>
                <a href="http://naplotu.com/joomblog/post/konkurs-voprosov-na-livelib"
                   class="main-a"><?php echo JText::_('COM_PLOT_COMPETITION'); ?></a>
            </li>
            <!--<li>
                <a href="<?php echo JRoute::_('index.php?option=com_easysocial&view=groups&Itemid=122'); ?>" class="main-a"><?php echo JText::_('MOD_PLOT_TOP_MENU_PROGRAM'); ?></a>
            </li>-->
            <li>
                <a href="<?php echo JRoute::_('index.php?option=com_easysocial&view=conversations'); ?>" class="main-a">Общаться</a>
                <?php if ($this->count_conversations) { ?>
                    <b><?php echo (int)$this->count_conversations; ?></b>
                <?php } ?>
            </li>
            <li>
                <a class="modal main-a" rel="{handler: 'iframe', size: {x: 744, y: 469}}"
                   href="<?php echo JRoute::_('index.php?option=com_plot&view=wallet', false); ?>"><?php echo JText::_('COM_PLOT_WALLET'); ?></a>
            </li>
        </menu>
    <?php } ?>
    <a href="#x" class="overlay" id="child-menu-courses" onclick="childMenuHide();"></a>

    <div class="shelf-menu-top my-courses">
        <div class="my-progress-wrapper">
            <div class="jcarousel-wrapper my-courses">
                <div class="no-jcarousel">
                    <ul>
                        <?php foreach ($this->my->coursesNewTopRow1 AS $courseNewTopRow1) { ?>
                            <li>
                                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=course&id=' . $courseNewTopRow1->id); ?>">
                                    <figure>
                                        <img class="course-image"
                                             src="<?php echo JUri::root() . ($courseNewTopRow1->image ? $courseNewTopRow1->image : 'templates/plot/img/blank300x200.jpg'); ?>"/>
                                        <figcaption><?php echo $courseNewTopRow1->course_name; ?></figcaption>
                                    </figure>
                                </a>



                                <?php if ($this->my->isCourseNew($courseNewTopRow1->id)) { ?>
                                    <div class="elem-descr"><?php echo JText::_('COM_PLOT_COURSE_LERNING_NEW') ?></div>
                                <?php } elseif (in_array($courseNewTopRow1->id, $this->my->getCoursesIdsBoughtForMe())) { ?>
                                    <i class="in-progress">
                                        <?php if ((int)$this->my->getCoursesCompletePercent($courseNewTopRow1->id) == 100) {
                                            echo JText::_("COM_PLOT_COURSE_LERNING_ON_100");
                                        } else {
                                            ?>
                                            <?php echo JText::_('COM_PLOT_COURSE_LERNING'); ?>
                                            <b><?php echo $this->my->getCoursesCompletePercent($courseNewTopRow1->id); ?>
                                                %</b>
                                        <?php
                                        }?>

                                    </i>
                                <?php } ?>


                            </li>
                        <?php } ?>
                        <li class="a-wrap-top">
                            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=courses&onlymycourses=1'); ?>">
                                <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet">
                                    <use xlink:href="#academic-hat"></use>
                                </svg>
                                <?php echo JText::_('COM_PLOT_SHOW_ALL_MY_COURSES'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="jcarousel-wrapper my-courses">
                <div class="no-jcarousel">
                    <ul>
                        <?php foreach ($this->my->coursesNewTopRow2 AS $courseNewTopRow2) { ?>
                            <li>
                                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=course&id=' . $courseNewTopRow2->id); ?>">
                                    <figure>
                                        <img class="course-image"
                                             src="<?php echo JUri::root() . ($courseNewTopRow2->image ? $courseNewTopRow2->image : 'templates/plot/img/blank300x200.jpg'); ?>"/>
                                        <figcaption><?php echo $courseNewTopRow2->course_name; ?></figcaption>
                                    </figure>
                                </a>
                                <?php if ($this->my->isCourseNew($courseNewTopRow2->id)) { ?>
                                    <div class="elem-descr"><?php echo JText::_('COM_PLOT_COURSE_LERNING_NEW') ?></div>
                                <?php } elseif (in_array($courseNewTopRow2->id, $this->my->getCoursesIdsBoughtForMe())) { ?>
                                    <i class="in-progress">
                                        <?php if ((int)$this->my->getCoursesCompletePercent($courseNewTopRow2->id) == 100) {
                                            echo JText::_("COM_PLOT_COURSE_LERNING_ON_100");
                                        } else {
                                            ?>
                                            <?php echo JText::_('COM_PLOT_COURSE_LERNING'); ?>
                                            <b><?php echo $this->my->getCoursesCompletePercent($courseNewTopRow2->id); ?>
                                                %</b>
                                        <?php
                                        }?>

                                    </i>
                                <?php } ?>
                            </li>
                        <?php } ?>
                        <li class="a-wrap-bottom">
                            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=courses&onlymycourses=2'); ?>">
                                <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet">
                                    <use xlink:href="#academic-hat"></use>
                                </svg>
                                <?php echo JText::_('COM_PLOT_TO_ALL_COURSES_PAGE'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <button class="close" onclick="childMenuHide();">&#215</button>
    </div>

    <a href="#x" class="overlay" id="child-menu-read" onclick="childMenuHide();"></a>

    <div class="shelf-menu-top my-books">
        <div class="my-progress-wrapper">
            <?php $count_book_shelf = count($this->books_on_reading_shelf);
            if ($this->books_on_reading_shelf) {
                if ($count_book_shelf > 4) {
                    for ($i = 0; $i < 4; $i++) {
                        if ($i == 0) {
                            ?>
                            <div class="jcarousel-wrapper my-books">
                            <div class="no-jcarousel">
                            <ul>
                        <?php } ?>
                        <li>
                            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $this->books_on_reading_shelf[$i]->c_id); ?>">
                                <figure><img src="<?php echo $this->books_on_reading_shelf[$i]->thumbnailUrl; ?>"/>
                                </figure>
                                <?php if ($this->books_on_reading_shelf[$i]->new) { ?>
                                    <div class="elem-descr">Новая</div>
                                    <div class="elem-descr"><?php echo JText::_('COM_PLOT_NEW_BOOK'); ?></div>
                                <?php } ?>
                            </a>
                        </li>
                        <?php if ($i == 3) { ?>
                            <li class="a-wrap-top">
                                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publications&my_books=1'); ?>"
                                   class="book-link-top show_all_my_books"><?php echo JText::_('COM_PLOT_SHOW_MY_ALL_BOOKS'); ?></a>
                            </li>
                            </ul>
                            </div>
                            </div>
                        <?php
                        }
                    }
                    for ($i = 4; $i < $count_book_shelf; $i++) {
                        if ($i == 4) {
                            ?>
                            <div class="jcarousel-wrapper my-books">
                            <div class="no-jcarousel">
                            <ul>
                        <?php } ?>
                        <li>
                            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $this->books_on_reading_shelf[$i]->c_id); ?>">
                                <figure><img src="<?php echo $this->books_on_reading_shelf[$i]->thumbnailUrl; ?>"/>
                                </figure>
                                <?php
                                if ($this->books_on_reading_shelf[$i]->new) {
                                    ?>
                                    <div class="elem-descr">Новая</div>
                                    <div class="elem-descr"><?php echo JText::_('COM_PLOT_NEW_BOOK'); ?></div>
                                <?php } ?>
                            </a>
                        </li>
                        <?php if ($i == $count_book_shelf - 1) { ?>
                            <li class="a-wrap-bottom">
                                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publications&my_books=2'); ?>"
                                   class="book-link-bottom show-all-books"><?php echo JText::_('COM_PLOT_GENERAL_BOOKS_SECTION'); ?></a>
                            </li>
                            </ul>
                            </div>
                            </div>
                        <?php
                        }
                    }
                } else {
                    ?>
                    <div class="jcarousel-wrapper my-books">
                        <div class="no-jcarousel">
                            <ul>
                                <?php for ($i = 0; $i < $count_book_shelf; $i++) { ?>
                                    <li>
                                        <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $this->books_on_reading_shelf[$i]->c_id); ?>">
                                            <figure><img
                                                    src="<?php echo $this->books_on_reading_shelf[$i]->thumbnailUrl; ?>"/>
                                            </figure>
                                            <?php
                                            if ($this->books_on_reading_shelf[$i]->new) {
                                                ?>
                                                <div class="elem-descr">Новая</div>
                                                <div
                                                    class="elem-descr"><?php echo JText::_('COM_PLOT_NEW_BOOK'); ?></div>
                                            <?php } ?>
                                        </a>
                                    </li>
                                <?php } ?>
                                <li class="a-wrap-top">
                                    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publications&my_books=1'); ?>"
                                       class="book-link-top show_all_my_books"><?php echo JText::_('COM_PLOT_SHOW_MY_ALL_BOOKS'); ?></a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="jcarousel-wrapper my-books">
                        <div class="no-jcarousel">
                            <ul>
                                <li class="a-wrap-bottom">
                                    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publications&my_books=2'); ?>"
                                       class="book-link-bottom show-all-books"><?php echo JText::_('COM_PLOT_GENERAL_BOOKS_SECTION'); ?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                <?php
                }
            } else {
                ?>
                <div class="jcarousel-wrapper my-books">
                    <div class="no-jcarousel">
                        <ul>
                            <li class="a-wrap-top">
                                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publications&my_books=1'); ?>"
                                   class="book-link-top show_all_my_books"><?php echo JText::_('COM_PLOT_SHOW_MY_ALL_BOOKS'); ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="jcarousel-wrapper my-books">
                    <div class="no-jcarousel">
                        <ul>
                            <li class="a-wrap-bottom">
                                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publications&my_books=2'); ?>"
                                   class="book-link-bottom show-all-books"><?php echo JText::_('COM_PLOT_GENERAL_BOOKS_SECTION'); ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php } ?>
        </div>
        <button class="close" onclick="childMenuHide();">&#215</button>
    </div>

    <a href="#x" class="overlay" id="child-menu-think" onclick="childMenuHide();"></a>

    <div class="shelf-menu-top my-thinking">
    <div class="my-progress-wrapper">

    <?php
    $count_meeting_shelf = count($this->meetings);
    if ($this->meetings) {
        if ($count_meeting_shelf > 4) {
            for ($i = 0; $i < 4; $i++) {
                if ($i == 0) {
                    ?>
                    <div class="jcarousel-wrapper my-thinking">
                    <div class="no-jcarousel">
                    <ul>
                <?php } ?>
                <li>
                    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=event&id=' . $this->meetings[$i]->id); ?>">
                        <figure>
                            <img src="<?php echo JUri::root() . 'images/com_plot/events/' . $this->meetings[$i]->user_id . '/thumb/' . $this->meetings[$i]->img; ?>"/>
                            <figcaption><?php echo $this->meetings[$i]->title; ?></figcaption>
                        </figure>
                    </a>
                    <?php if ($this->meetings[$i]->create_date != '0000-00-00 00:00:00') { ?>
                        <?php if (strtotime($this->meetings[$i]->create_date . '+' . plotGlobalConfig::getVar("meetingIsNewMaxDays") . ' day') > strtotime("now")) { ?>
                            <span class="elem-descr">Новая!</span>
                        <?php } ?>
                    <?php } ?>
                </li>
                <?php if ($i == 3) { ?>
                    <li class="a-wrap-top">
                        <a href="<?php echo JRoute::_('index.php?option=com_plot&view=events&onlymy=1'); ?>">
                            <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet"><use xlink:href="#lamp-meeting"></use></svg>
                            Показать все мои думалки
                        </a>
                    </li>
                    </ul>
                    </div>
                    </div>
                <?php
                }
            }
            for ($i = 4; $i < $count_meeting_shelf; $i++) {
                if ($i == 4) {
                    ?>
                    <div class="jcarousel-wrapper my-thinking">
                    <div class="no-jcarousel">
                    <ul>
                <?php } ?>
                <li>
                    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=event&id=' . $this->meetings[$i]->id); ?>">
                        <figure>
                            <img src="<?php echo JUri::root() . 'images/com_plot/events/' . $this->meetings[$i]->user_id . '/thumb/' . $this->meetings[$i]->img; ?>"/>
                            <figcaption><?php echo $this->meetings[$i]->title; ?></figcaption>
                        </figure>
                    </a>
                    <?php if ($this->meetings[$i]->create_date != '0000-00-00 00:00:00') { ?>
                        <?php if (strtotime($this->meetings[$i]->create_date . '+' . plotGlobalConfig::getVar("meetingIsNewMaxDays") . ' day') > strtotime("now")) { ?>
                            <span class="elem-descr">Новая!</span>
                        <?php } ?>
                    <?php } ?>
                </li>
                <?php if ($i == $count_meeting_shelf - 1) { ?>
                    <li class="a-wrap-bottom">
                        <a href="<?php echo JRoute::_('index.php?option=com_plot&view=events&onlymy=2'); ?>">
                            <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet">
                                <use xlink:href="#lamp-meeting"></use>
                            </svg>
                            В общий раздел думалок
                        </a>
                    </li>
                    </ul>
                    </div>
                    </div>
                <?php
                }
            }
        } else {
            ?>
            <div class="jcarousel-wrapper my-books">
                <div class="no-jcarousel">
                    <ul>
                        <?php for ($i = 0; $i < $count_meeting_shelf; $i++) { ?>
                            <li>
                                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=event&id=' . $this->meetings[$i]->id); ?>">
                                    <figure>
                                        <img src="<?php echo JUri::root() . 'images/com_plot/events/' . $this->meetings[$i]->user_id . '/thumb/' . $this->meetings[$i]->img; ?>"/>
                                        <figcaption><?php echo $this->meetings[$i]->title; ?></figcaption>
                                    </figure>
                                </a>
                                <?php if ($this->meetings[$i]->create_date != '0000-00-00 00:00:00') { ?>
                                    <?php if (strtotime($this->meetings[$i]->create_date . '+' . plotGlobalConfig::getVar("meetingIsNewMaxDays") . ' day') > strtotime("now")) { ?>
                                        <span class="elem-descr">Новая!</span>
                                    <?php } ?>
                                <?php } ?>
                            </li>
                        <?php } ?>
                        <li class="a-wrap-top">
                            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=events&onlymy=1'); ?>">
                                <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet"><use xlink:href="#lamp-meeting"></use></svg>
                                Показать все мои думалки
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="jcarousel-wrapper my-books">
                <div class="no-jcarousel">
                    <ul>
                        <li class="a-wrap-bottom">
                            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=events&onlymy=2'); ?>">
                                <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet">
                                    <use xlink:href="#lamp-meeting"></use>
                                </svg>
                                В общий раздел думалок
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        <?php
        }
    } else {
        ?>
        <div class="jcarousel-wrapper my-books">
            <div class="no-jcarousel">
                <ul>
                    <li class="a-wrap-top">
                        <a href="<?php echo JRoute::_('index.php?option=com_plot&view=events&onlymy=1'); ?>">
                            <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet"><use xlink:href="#lamp-meeting"></use></svg>
                            Показать все мои думалки
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="jcarousel-wrapper my-books">
            <div class="no-jcarousel">
                <ul>
                    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=events&onlymy=2'); ?>">
                        <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet">
                            <use xlink:href="#lamp-meeting"></use>
                        </svg>
                        В общий раздел думалок
                    </a>
                </ul>
            </div>
        </div>
    <?php } ?>
























    </div>
    </div>
    <?php # </editor-fold> ?>
    <?php # <editor-fold defaultstate="collapsed" desc="BOBR / HOUSE / BOAT BY LEVELS"> ?>
    <?php if ($this->user->level->title < 5) { ?>
        <div class="user-level level-1">
            <object style="visibility: visible" type="image/svg+xml"
                    data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/bobr-first-level.svg"
                    name="bobr"></object>
            <object style="visibility: visible" type="image/svg+xml"
                    data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/house-first-level.svg"
                    name="house"></object>
            <div class="house-label">
                <object style="visibility: visible;" type="image/svg+xml"
                        data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/label-first-level.svg"
                        name="label"></object>
                <span>Мой домик</span>
            </div>
            <object style="visibility: visible" type="image/svg+xml"
                    data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/ship-first-level.svg"
                    name="ship"></object>
        </div>
    <?php } elseif ($this->user->level->title >= 5 && $this->user->level->title < 10) { ?>
        <div class="user-level level-2">
            <object style="visibility: visible" type="image/svg+xml"
                    data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/bobr-second-level.svg"
                    name="bobr"></object>
            <img
                src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/house-second-level.png"
                alt="" name="house">

            <div class="house-label">
                <object style="visibility: visible;" type="image/svg+xml"
                        data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/label-first-level.svg"
                        name="label"></object>
                <span>Мой домик</span>
            </div>
            <object style="visibility: visible" type="image/svg+xml"
                    data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/ship-second-level.svg"
                    name="ship"></object>
        </div>
    <?php } elseif ($this->user->level->title >= 10) { ?>
        <div class="user-level level-3">
            <object style="visibility: visible" type="image/svg+xml"
                    data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/bobr-third-level.svg"
                    name="bobr"></object>
            <img
                src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/house-third-level.png"
                alt="" name="house">

            <div class="house-label">
                <object type="image/svg+xml"
                        data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/label-third-level.svg"
                        name="label"></object>
                <span>Мой домик</span>
            </div>
            <object style="visibility: visible" type="image/svg+xml"
                    data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/ship-third-level.svg"
                    name="ship"></object>
        </div>
    <?php } ?>
    <?php # </editor-fold> ?>
    </div>

    </div>
<?php } ?>
<?php # </editor-fold> ?>

<main class="child-profile">

<div class="wrap main-wrap child-profile">
<?php # <editor-fold defaultstate="collapsed" desc="USER INFO"> ?>
<section class="main-user-info">
    <div class="info-board">
        <?php if ($this->my->id != $this->user->id) { ?>
            <div class="main-user-info-top">
                <?php if ($this->my->id) { ?>
                    <div class="actions">
                        <a class="button green h30 modal"
                           href="<?php echo JRoute::_('index.php?option=com_plot&task=conversations.newMessage&id=' . (int)$this->user->id); ?>"
                           rel="{size: {x: 744, y: 500}, handler:'iframe'}">
                        <span>
                            <svg viewBox="0 0 40.7 29.5" preserveAspectRatio="xMinYMin meet" class="letter">
                                <use xlink:href="#letter"></use>
                            </svg>
                            <?php echo JText::_('COM_PLOT_WRITE_ME_MESSAGE'); ?>
                        </span>
                        </a>
                    </div>
                    <div class="actions" id="friend-button-wrapp">
                        <?php $friend_status = PlotHelper::isFriend();
                        switch ($friend_status) {
                            case 'pending':
                                ?>
                                <span>
                                <svg viewBox="0 0 34.5 30.8" preserveAspectRatio="xMinYMin meet"
                                     class="wait-for-friend">
                                    <use xlink:href="#wait-for-friend"></use>
                                </svg>
                                запрос дружбы отправлен
                            </span>
                                <?php
                                break;
                            case 'unfriend':
                                ?>
                                <a class="button green modal"
                                   href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.addFriendAjax&id=' . $this->user->id); ?>"
                                   rel="{size: {x: 400, y: 400}, handler:'iframe'}">
                                <span>
                                    <svg viewBox="0 0 31.7 30.8" preserveAspectRatio="xMinYMin meet" class="friend">
                                        <use xlink:href="#friend"></use>
                                    </svg>
                                    Дружить со мной
                                </span>
                                </a>
                                <?php
                                break;
                            case 'friend':
                                ?>
                                <a class="button brown" onclick="removeFriend('<?php echo $this->user->id; ?>');">
                                <span>
                                    <svg viewBox="0 0 46.3 45.6" preserveAspectRatio="xMinYMin meet"
                                         class="no-friendship">
                                        <use xlink:href="#no-friends"></use>
                                    </svg>
                                    Не дружить
                                </span>
                                </a>
                                <?php
                                break;
                        } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } else { ?>
            <div class="main-user-info-top">
                <svg viewBox="0 0 37.1 38.9" preserveAspectRatio="xMidYMid meet" class="leaf">
                    <use xlink:href="#leaf"></use>
                </svg>
                <i class="round-3-borders"><?php echo $this->user->level->title; ?>й</i>
                <span id="plot-user-points"><?php echo $this->user->getPoints(); ?></span>
                <i class="elka">+
                    <svg viewBox="0 0 14.6 23.1" preserveAspectRatio="xMinYMin meet">
                        <use xlink:href="#elka-interest"></use>
                    </svg>
                </i>
                <span id="plot-user-nex-level"> еще <?php echo $this->user->level->pointsRemainToNextLevel; ?></span>
                <i class="round-3-borders">+
                    <svg class="cup" viewBox="0 0 19.8 30" preserveAspectRatio="xMinYMin meet">
                        <use xlink:href="#cup"></use>
                    </svg>
                </i>
                <?php if ($levelNewHouse = $this->user->getLevelNewHouse()) { ?>
                    <p>Получи новый домик на <?php echo $levelNewHouse->title; ?> уровне</p>
                <?php } ?>
                <?php if ($this->my->id && $this->user->id == $this->my->id) { ?>
                    <a class="modal" rel="{handler: 'iframe', size: {x: 744, y: 469}}"
                       href="<?php echo JRoute::_('index.php?option=com_plot&view=wallet', false); ?>">
                        <p class="finance-info"><?php echo $this->user->getMoney(); ?>
                            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                                <use xlink:href="#munze"></use>
                            </svg>
                            получено
                        </p>
                    </a>
                <?php } else { ?>
                    <p class="finance-info"><?php echo $this->user->getMoney(); ?>
                        <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                            <use xlink:href="#munze"></use>
                        </svg>
                        получено
                    </p>
                <?php } ?>
            </div>
        <?php } ?>
        <nav id="tabs-nav">
            <a data-action="events" class="stream-nav active" href="javascript:void(0);">Все события</a>
            <a data-action="sertificates" class="stream-nav" href="javascript:void(0);">Все достижения</a>
            <a data-action="meetings" class="stream-nav" href="javascript:void(0)">Все встречи</a>
            <a data-action="photos" class="stream-nav" href="javascript:void(0);">все фото</a>
            <a data-action="video" class="stream-nav" href="javascript:void(0);">все видео</a>
            <?php if ($this->my->id == $this->user->id) { ?>
                <a data-action="add" class="toggle-add-items-block-button add stream-nav"
                   onclick="toggleAddItemsBlock();" href="javascript:void(0);">Добавить</a>
            <?php } ?>
        </nav>
    </div>

    <?php if ($this->my->id == $this->user->id) {
        require_once JPATH_COMPONENT . '/views/profile/tmpl/default_add.php';
    } ?>

</section>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="STREAM"> ?>
<?php # <editor-fold defaultstate="collapsed" desc="ALLEVENTS"> ?>
<div id="show-all-events">
    <section class="new-event"></section>
</div>
<?php # </editor-fold> ?>
<?php # <editor-fold defaultstate="collapsed" desc="CERTIFICATES"> ?>
<div id="show-all-certificates">
    <section class="new-event">
        <?php if ($this->mergedAndSortedProgressArray) { ?>
            <div class="jcarousel-wrapper">
                <div class="jcarousel">
                    <ul class="child-profile">
                        <?php foreach ($this->mergedAndSortedProgressArray AS $key => $val) { ?>
                            <?php if (strpos($key, 'certificate') !== false) { ?>

                                <li>
                                    <a class="fancybox" rel="certificate-set" data-title="<?php echo $val->title; ?>"
                                       data-description="<?php echo $val->caption; ?>"
                                       data-date="<?php echo JFactory::getDate($val->assigned_date)->format('d.m.Y H:i'); ?>"
                                       data-share=" data-image='<?php echo $val->imageUrl; ?>' data-url='<?php echo $val->imageUrl; ?>' data-title='<?php echo addslashes($val->title); ?>' "
                                       href="<?php echo $val->imageUrl; ?>">

                                        <h6>
                                            <i>
                                                <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet"
                                                     class="star">
                                                    <use xlink:href="#star"></use>
                                                </svg>
                                            </i>
                                            <span class="action-title"></span>

                                            <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($val->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                        </h6>
                                        <data><?php echo JFactory::getDate($val->assigned_date)->format('d.m.Y H:i'); ?></data>
                                        <div>
                                            <figure><img src="<?php echo $val->thumb; ?>"/>
                                                <i class="activity-icons">
                                                    <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet"
                                                         style="fill:url(#svg-gradient);">
                                                        <use xlink:href="#lamp-meeting"></use>
                                                    </svg>
                                                </i>
                                            </figure>
                                        </div>
                                        <div>
                                            <?php if ($val->caption) { ?>
                                                <blockquote><?php echo PlotHelper::cropStr(strip_tags($val->caption), plotGlobalConfig::getVar('coursesDescriptionMaxSymbolsToShow')); ?></blockquote>
                                            <?php } ?>
                                        </div>
                                    </a>
                                </li>
                            <?php } elseif (strpos($key, 'course') !== false) { ?>
                                <li>
                                    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=course&id=' . (int)$val->course_id); ?>">
                                        <h6>
                                            <i>
                                                <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet"
                                                     class="star">
                                                    <use xlink:href="#star"></use>
                                                </svg>
                                            </i>
                                            <span class="action-title"></span>

                                            <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($val->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                        </h6>
                                        <data><?php echo JFactory::getDate($val->finished_date)->format('d.m.Y H:i'); ?></data>
                                        <div>
                                            <figure><img
                                                    src="<?php echo JFile::exists(JPATH_ROOT . '/' . $val->image) ? JUri::root() . $val->image : JUri::root() . 'images/com_plot/def_course.jpg'; ?>"/>
                                                <i class="activity-icons">
                                                    <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"
                                                         style="fill:url(#svg-gradient);">
                                                        <use xlink:href="#academic-hat"></use>
                                                    </svg>
                                                </i>
                                            </figure>
                                        </div>
                                        <div>
                                            <?php if ($val->course_description) { ?>
                                                <blockquote><?php echo PlotHelper::cropStr(strip_tags($val->course_description), plotGlobalConfig::getVar('coursesDescriptionMaxSymbolsToShow')); ?></blockquote>
                                            <?php } ?>
                                        </div>
                                    </a>
                                </li>
                            <?php } elseif (strpos($key, 'book') !== false) { ?>
                                <li>
                                    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId=' . (int)$val->c_id); ?>">
                                        <h6>
                                            <i>
                                                <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet"
                                                     class="star">
                                                    <use xlink:href="#star"></use>
                                                </svg>
                                            </i>
                                            <span class="action-title"></span>

                                            <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($val->c_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                        </h6>
                                        <data><?php echo JFactory::getDate($val->finished_date)->format('d.m.Y H:i'); ?></data>
                                        <div>
                                            <figure>
                                                <?php if (file_exists(JPATH_SITE . '/media/com_html5flippingbook/thumbs/thimb_' . $val->c_thumb)) { ?>
                                                    <img
                                                        src="<?php echo JFile::exists(JPATH_ROOT . '/media/com_html5flippingbook/thumbs/thimb_' . $val->c_thumb) ? JUri::root() . 'media/com_html5flippingbook/thumbs/thimb_' . $val->c_thumb : JUri::root() . 'images/com_plot/def_tag.jpg'; ?>"/>
                                                <?php } else { ?>
                                                    <img
                                                        src="<?php echo JFile::exists(JPATH_ROOT . '/images/com_plot/def_book.jpg') ? JUri::root() . 'images/com_plot/def_book.jpg' : JUri::root() . 'images/com_plot/def_tag.jpg'; ?>"/>
                                                <?php } ?>
                                                <i class="activity-icons">
                                                    <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"
                                                         style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                                        <use xlink:href="#book"></use>
                                                    </svg>
                                                </i>
                                            </figure>
                                        </div>
                                        <div>
                                            <?php if ($val->c_pub_descr) { ?>
                                                <blockquote><?php echo PlotHelper::cropStr(strip_tags($val->c_pub_descr), plotGlobalConfig::getVar('coursesDescriptionMaxSymbolsToShow')); ?></blockquote>
                                            <?php } ?>
                                            <p class="stream-author">><b>Автор: </b><?php echo $val->c_author; ?></p>
                                        </div>
                                    </a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
                <a href="javascript:void(0);" class="jcarousel-control-prev">&#60</a>
                <a href="javascript:void(0);" class="jcarousel-control-next">&#62</a>
                <a class="show-more hover-shadow"
                   href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id=' . $this->user->id); ?>#<?php echo $this->my->isParent() ? 'parent' : 'all-child'; ?>-certificates">Показать
                    еще сертификаты</a>
                <a class="show-more hover-shadow"
                   href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id=' . $this->user->id); ?>#<?php echo $this->my->isParent() ? 'parent' : 'all-child'; ?>-courses">Показать
                    еще курсы</a>
                <a class="show-more hover-shadow"
                   href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id=' . $this->user->id); ?>#<?php echo $this->my->isParent() ? 'parent' : 'all-child'; ?>-publications">Показать
                    еще книги</a>
            </div>
        <?php
        } else {
            echo PlotHelper::getDefaultText('activity', $this->user->id);
        } ?>
    </section>
</div>
<?php # </editor-fold> ?>
<?php # <editor-fold defaultstate="collapsed" desc="EVENTS"> ?>
<div id="show-all-meetings">
    <section class="new-event">
        <?php if ($this->events) { ?>
            <div class="jcarousel-wrapper">
                <div class="jcarousel">
                    <ul class="child-profile">
                        <?php foreach ($this->events AS $key => $item) { ?>
                            <li class="stream-item">
                                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=event&id=' . $item->id); ?>">
                                    <h6>
                                        <i>
                                            <svg viewBox="0 0 26 24" preserveAspectRatio="xMidYMid meet">
                                                <use xlink:href="#datepicker"></use>
                                            </svg>
                                        </i>
                                        <span class="action-title"></span>

                                        <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($item->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                    </h6>
                                    <data><?php echo JFactory::getDate($item->create_date)->format('d.m.Y H:i'); ?></data>
                                    <div>
                                        <figure>
                                            <img
                                                src="<?php echo JUri::root() . 'images/com_plot/events/' . $item->user_id . '/thumb/' . $item->img; ?>"/>
                                            <i class="activity-icons">
                                                <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet"
                                                     style="fill:url(#svg-gradient);">
                                                    <use xlink:href="#lamp-meeting"></use>
                                                </svg>
                                            </i>
                                        </figure>
                                    </div>
                                    <div>
                                        <?php if ($item->description) { ?>
                                            <blockquote><?php echo PlotHelper::cropStr(strip_tags($item->description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                        <?php } ?>
                                    </div>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <a href="javascript:void(0);" class="jcarousel-control-prev">&#60</a>
                <a href="javascript:void(0);" class="jcarousel-control-next">&#62</a>
                <a class="show-more hover-shadow"
                   href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id=' . $this->user->id); ?>#<?php echo $this->my->isParent() ? 'parent' : 'all-child'; ?>-events">Показать
                    еще</a>
            </div>
        <?php
        } else {
            echo PlotHelper::getDefaultText('activity', $this->user->id);
        } ?>
    </section>
</div>
<?php # </editor-fold> ?>
<?php # <editor-fold defaultstate="collapsed" desc="PHOTOS"> ?>
<div id="show-all-photos">
    <section class="new-event">
        <?php if ($this->photos) { ?>
            <div class="jcarousel-wrapper">
                <div class="jcarousel">
                    <ul class="child-profile">
                        <?php foreach ($this->photos AS $photo) { ?>

                            <li class="stream-item">
                                <a class="fancybox" rel="photos-set"
                                    <?php
                                    if ($photo->img_type != 'plot-essay') {
                                        ?>
                                        data-title="<?php echo $photo->title; ?>"
                                        data-description="<?php echo $photo->caption; ?>"
                                    <?php
                                    }
                                    ?>
                                   data-date="<?php echo JFactory::getDate($photo->assigned_date)->format('d.m.Y H:i'); ?>"

                                    <?php
                                    if ($photo->img_type == 'plot-essay'){
                                    ?>
                                   data-share=" data-image='<?php echo JURI::root() . 'media/com_plot/essay/' . $photo->value; ?>' data-url='<?php echo JURI::root() . 'media/com_plot/essay/' . $photo->value; ?>' data-title='<?php echo addslashes($photo->title); ?>' "

                                   href="<?php echo JURI::root() . 'media/com_plot/essay/' . $photo->value; ?>">
                                    <?php
                                    } else {
                                        ?>
                                        data-share=" data-image='<?php echo JURI::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . PlotHelper::getImageOriginalById($photo->id)->value; ?>' data-url='<?php echo JURI::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . PlotHelper::getImageOriginalById($photo->id)->value; ?>' data-title='<?php echo addslashes($photo->title); ?>' "
                                        href="<?php echo JURI::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . PlotHelper::getImageOriginalById($photo->id)->value; ?>">
                                    <?php
                                    }
                                    ?>
                                    <h6>
                                        <i>
                                            <svg viewBox="0 0 24.5 26.4" preserveAspectRatio="xMidYMid meet">
                                                <use xlink:href="#photo"></use>
                                            </svg>
                                        </i>
                                        <span class="action-title"></span>

                                        <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($photo->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?>
                                        </p>
                                    </h6>
                                    <data><?php echo JFactory::getDate($photo->assigned_date)->format('d.m.Y H:i'); ?></data>
                                    <div>
                                        <figure>
                                            <?php
                                            if ($photo->img_type == 'plot-essay') {
                                                ?>
                                                <img
                                                    src="<?php echo JFile::exists(JPATH_BASE . '/media/com_plot/essay/' . $photo->value) ? JURI::root() . 'media/com_plot/essay/' . $photo->value : JUri::root() . 'images/com_plot/def_essay.jpg'; ?>"/>
                                            <?php
                                            } else {
                                                ?>
                                                <img
                                                    src="<?php echo JFile::exists(JPATH_BASE . '/media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . $photo->value) ? JURI::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . $photo->value : JUri::root() . 'images/com_plot/def_essay.jpg'; ?>"/>
                                            <?php
                                            } ?>
                                            <i class="activity-icons">
                                                <svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid meet"
                                                     style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                                    <use xlink:href="#zoom"></use>
                                                </svg>
                                            </i>
                                        </figure>
                                    </div>
                                    <div>
                                        <?php if ($photo->caption) { ?>
                                            <blockquote><?php echo PlotHelper::cropStr(strip_tags($photo->caption), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                        <?php } ?>
                                    </div>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <a href="javascript:void(0);" class="jcarousel-control-prev">&#60</a>
                <a href="javascript:void(0);" class="jcarousel-control-next">&#62</a>
                <a class="show-more hover-shadow"
                   href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id=' . $this->user->id); ?>#all-child-photos">Показать
                    еще</a>
            </div>
        <?php
        } else {
            echo PlotHelper::getDefaultText('activity', $this->user->id);
        } ?>
    </section>
</div>
<?php # </editor-fold> ?>
<?php # <editor-fold defaultstate="collapsed" desc="VIDEOS"> ?>
<div id="show-all-video">
    <section class="new-event">
        <?php if ($this->video_links) { ?>
            <div class="jcarousel-wrapper">
                <div class="jcarousel">
                    <ul class="child-profile">
                        <?php foreach ($this->video_links AS $video) { ?>
                            <?php if ($video->type == 'link' || $video->type == 'course') { ?>
                                <li class="stream-item">
                                    <?php if (strripos($video->path, 'youtube.com/') !== FALSE) {
                                        $youtubeNumber = substr($video->path, strripos($video->path, '?v=') + 3);
                                        if (($pos = strpos($youtubeNumber, '&')) !== FALSE) {
                                            $youtubeNumber = substr($youtubeNumber, 0, $pos);
                                        }
                                    } ?>
                                    <a class="fancybox" data-fancybox-type="ajax" rel="videos-set"
                                       data-title="<?php echo $video->title; ?>"
                                       data-description="<?php echo $video->description; ?>"
                                       data-date="<?php echo JFactory::getDate($video->date)->format('d.m.Y H:i'); ?>"
                                       data-share=" data-image='https://youtu.be/<?php echo $youtubeNumber; ?>' data-url='https://youtu.be/<?php echo $youtubeNumber; ?>' data-title='<?php echo addslashes($video->title); ?>' "
                                       href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxOpenVideo&youtubeNumber=' . $youtubeNumber); ?>">
                                        <h6>
                                            <i>
                                                <svg viewBox="0 0 25.3 22.3" preserveAspectRatio="xMidYMid meet"
                                                     class="star">
                                                    <use xlink:href="#video"></use>
                                                </svg>
                                            </i>
                                            <span class="action-title"></span>

                                            <p class="stream-title"> <?php echo PlotHelper::cropStr(strip_tags($video->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                        </h6>
                                        <hr/>
                                        <data><?php echo JFactory::getDate($video->date)->format('d.m.Y H:i'); ?></data>
                                        <div>
                                            <figure>
                                                <?php
                                                if ($video->type == 'course') {
                                                    if (file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'coursevideo' . DIRECTORY_SEPARATOR . $video->uid . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $youtubeNumber . '.jpg')) {
                                                        $path = JUri::root() . 'media/com_plot/coursevideo/' . $video->uid . '/thumb/' . $youtubeNumber . '.jpg';
                                                    } else {
                                                        $path = JUri::root() . 'images/com_plot/def_video.jpg';
                                                    }
                                                } else {
                                                    if (file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR . $video->uid . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $youtubeNumber . '.jpg')) {
                                                        $path = JUri::root() . 'images/com_plot/video/' . $video->uid . '/thumb/' . $youtubeNumber . '.jpg';
                                                        $path = str_replace("\\images", "/images", $path);
                                                    } else {
                                                        $path = JUri::root() . 'images/com_plot/def_video.jpg';
                                                    }
                                                } ?>
                                                <img src="<?php echo $path; ?>"/>
                                                <i class="activity-icons">
                                                    <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet"
                                                         style="fill:url(#svg-gradient);">
                                                        <use xlink:href="#play"></use>
                                                    </svg>
                                                </i>
                                            </figure>
                                        </div>
                                        <div>
                                            <?php if ($video->description) { ?>
                                                <blockquote><?php echo PlotHelper::cropStr(strip_tags($video->description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                            <?php } ?>
                                        </div>
                                    </a>
                                </li>
                            <?php } else { ?>
                                <li class="stream-item">
                                    <a class="fancybox" data-fancybox-type="ajax" rel="videos-set"
                                       data-title="<?php echo $video->title; ?>"
                                       data-description="<?php echo $video->description; ?>"
                                       data-date="<?php echo JFactory::getDate($video->date)->format('d.m.Y H:i'); ?>"
                                       data-share=" data-image='<?php echo $video->imageThumb; ?>' data-url='<?php echo JRoute::_('index.php?option=com_plot&task=profile.openVideoFile&videoId=' . $video->id); ?>' data-title='<?php echo addslashes($video->title); ?>' "
                                       href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.openVideoFile&videoId=' . $video->id); ?>">
                                        <h6>
                                            <i>
                                                <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet"
                                                     class="star">
                                                    <use xlink:href="#star"></use>
                                                </svg>
                                            </i>
                                            <span class="action-title"></span>

                                            <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($video->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                        </h6>
                                        <hr/>
                                        <data><?php echo JFactory::getDate($video->date)->format('d.m.Y H:i'); ?></data>
                                        <div>
                                            <figure>
                                                <img src="<?php echo $video->imageThumb; ?>"/>
                                                <i class="activity-icons">
                                                    <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet"
                                                         style="fill:url(#svg-gradient);">
                                                        <use xlink:href="#play"></use>
                                                    </svg>
                                                </i>
                                            </figure>
                                        </div>
                                        <div>
                                            <?php if ($video->description) { ?>
                                                <blockquote><?php echo PlotHelper::cropStr(strip_tags($video->description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                            <?php } ?>
                                        </div>
                                    </a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
                <a href="javascript:void(0);" class="jcarousel-control-prev">&#60</a>
                <a href="javascript:void(0);" class="jcarousel-control-next">&#62</a>
                <a class="show-more hover-shadow"
                   href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id=' . $this->user->id); ?>#all-child-videos">Показать
                    еще</a>
            </div>
        <?php
        } else {
            echo PlotHelper::getDefaultText('activity', $this->user->id);
        } ?>
    </section>
</div>
<?php # </editor-fold> ?>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="CURRENT YEAR PROGRESS"> ?>
<?php if ($this->issetCurrentYearProgress) { ?>
    <section class="my-progress">
        <div class="my-progress-wrapper">
            <?php if (isset($this->user->allCurrentBooks) && isset($this->user->allCurrentBooks[$this->user->currentAge]) && $this->user->allCurrentBooks) { ?>
                <div class="jcarousel-wrapper my-books">
                    <div class="jcarousel">
                        <ul class="child-profile">
                            <?php


                            foreach ($this->user->allCurrentBooks[$this->user->currentAge] AS $book) {

                                ?>
                                <li>
                                    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $book->c_id); ?>">
                                        <figure><img src="<?php echo $book->thumbnailUrl; ?>"
                                                     alt="<?php htmlspecialchars($book->c_title); ?>"/></figure>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <a href="#" class="jcarousel-control-prev">&#60</a>
                    <a href="#" class="jcarousel-control-next">&#62</a>

                    <div class="shelf-info">
                        <span>Мои прочитанные книги</span>
                        <button class="settings no-box-shadow" style="display: none;">
                            <svg viewBox="0 0 37.1 37.1" preserveAspectRatio="xMidYMid meet">
                                <use xlink:href="#settings"></use>
                            </svg>
                        </button>
                        <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publications&my_books=1'); ?>">
                            Всего: <?php echo PlotHelper::getWordEnding(count($this->user->allCurrentBooks), 'книга', 'книги', 'книг'); ?>
                        </a>
                    </div>
                </div>
            <?php } ?>
            <?php if (isset($this->user->myStudiedCoursesByYears[$this->user->currentAge])) { ?>
                <div class="jcarousel-wrapper my-courses">
                    <div class="jcarousel">
                        <ul class="child-profile">
                            <?php foreach ($this->user->myStudiedCoursesByYears[$this->user->currentAge] AS $myStudiedCourse) { ?>
                                <li>
                                    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=course&id=' . $myStudiedCourse->id); ?>">
                                        <figure>
                                            <img
                                                src="<?php echo ($myStudiedCourse->image && jfile::exists(JPATH_BASE . '/' . $myStudiedCourse->image)) ? JUri::root() . $myStudiedCourse->image : JUri::root() . 'templates/plot/img/blank300x200.jpg'; ?>"/>
                                            <figcaption><?php echo $myStudiedCourse->course_name; ?></figcaption>
                                        </figure>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <a href="javascript:void(0);" class="jcarousel-control-prev">&#60</a>
                    <a href="javascript:void(0);" class="jcarousel-control-next">&#62</a>

                    <div class="shelf-info">
                        <span>Мои изученные курсы</span>
                        <button class="settings no-box-shadow bulb-menu-open" style="display: none;">
                            <svg viewBox="0 0 37.1 37.1" preserveAspectRatio="xMidYMid meet">
                                <use xlink:href="#settings"></use>
                            </svg>
                        </button>
                        <ul class="bulb-menu">
                            <li>
                                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=courses'); ?>"><?php echo JText::_('COM_PLOT_ALL_COURSES'); ?></a>
                            </li>
                        </ul>
                        <a href="<?php echo JRoute::_('index.php?option=com_plot&view=courses&onlymycourses=1'); ?>">
                            Всего: <?php echo PlotHelper::getWordEnding(count($this->user->myStudiedCoursesByYears[$this->user->currentAge]), 'курс', 'курса', 'курсов'); ?>
                        </a>
                    </div>
                </div>
            <?php } ?>
            <?php if ($this->certificates) {
                ?>
                <div class="jcarousel-wrapper my-certificates">
                    <div class="jcarousel">
                        <ul class="child-profile">
                            <?php foreach ($this->certificates AS $certificate) { ?>
                                <?php if ($certificate) { ?>
                                    <li>
                                        <a class="fancybox" rel="certificate-shelf-set"
                                           data-title="<?php echo $certificate->title; ?>"
                                           data-description="<?php echo $certificate->caption; ?>"
                                           data-date="<?php echo JFactory::getDate($certificate->assigned_date)->format('d.m.Y H:i'); ?>"
                                           data-share=" data-image='<?php echo $certificate->imageUrl; ?>' data-url='<?php echo $certificate->imageUrl; ?>' data-title='<?php echo addslashes($certificate->title); ?>' "
                                           href="<?php echo $certificate->imageUrl; ?>">
                                            <figure><img class="certificate-img"
                                                         src="<?php echo $certificate->thumb; ?>"/></figure>
                                        </a>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                    <a href="javascript:void(0);" class="jcarousel-control-prev">&#60</a>
                    <a href="javascript:void(0);" class="jcarousel-control-next">&#62</a>

                    <div class="shelf-info">
                        <span>Мои достижения</span>
                        <a href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id=' . $this->user->id); ?>#<?php echo $this->my->isParent() ? 'parent' : 'all-child'; ?>-certificates">
                            Всего: <?php echo PlotHelper::getWordEnding(count($this->certificates), 'достижение', 'достижения', 'достижений'); ?>
                        </a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </section>
<?php } ?>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="PAST YEARS PROGRESS"> ?>
<?php if ($this->user->yearsoldsToShow) { ?>
    <section class="progress-history">
        <div id="accordion">

            <?php foreach ($this->user->yearsoldsToShow as $yearsOld) { ?>
                <h3 class="brevno">
                    <span
                        class="brevno-text">Мне <?php echo PlotHelper::getWordEnding($yearsOld, 'год', 'года', 'лет'); ?></span>
                    <svg viewBox="0 0 37.1 38.9" preserveAspectRatio="xMidYMid meet" class="leaf">
                        <use xlink:href="#leaf"></use>
                    </svg>
                    <button class="accordion-close">Закрыть &#215</button>
                </h3>
                <div class="my-progress-wrapper">
                    <?php
                    $birthday = $this->user->getSocialFieldData('BIRTHDAY');
                    if (!$birthday) {
                        $defaultChildBirthday = plotGlobalConfig::getVar('defaultChildBirthday');
                        $dateOfThisYearsOldBirthday = ((int)$defaultChildBirthday['year'] + $yearsOld) . '-' . $defaultChildBirthday['month'] . '-' . $defaultChildBirthday['day'];
                    } else {
                        $dateOfThisYearsOldBirthday = ($birthday->year + $yearsOld) . '-' . $birthday->month . '-' . $birthday->day;
                    }
                    $levelOfThisYearsOld = $this->user->getLevel($dateOfThisYearsOldBirthday);
                    ?>
                    <?php if ($levelOfThisYearsOld->title < 5) { ?>
                        <div class="user-level level-1">
                            <object style="visibility: visible" type="image/svg+xml"
                                    data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/bobr-first-level.svg"
                                    name="bobr"></object>
                            <object style="visibility: visible" type="image/svg+xml"
                                    data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/house-first-level.svg"
                                    name="house"></object>
                            <div class="house-label">
                                <object type="image/svg+xml"
                                        data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/label-first-level.svg"
                                        name="label"></object>
                                <span>Мой домик</span>
                            </div>
                            <object style="visibility: visible" type="image/svg+xml"
                                    data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/ship-first-level.svg"
                                    name="ship"></object>
                            <div class="brevno">
                                <span class="brevno-text">&nbsp</span>
                                <svg viewBox="0 0 37.1 38.9" preserveAspectRatio="xMidYMid meet" class="leaf">
                                    <use xlink:href="#leaf"></use>
                                </svg>
                            </div>
                        </div>
                    <?php } elseif ($levelOfThisYearsOld->title >= 5 && $levelOfThisYearsOld->title < 10) { ?>
                        <div class="user-level level-2">
                            <object style="visibility: visible" type="image/svg+xml"
                                    data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/bobr-second-level.svg"
                                    name="bobr"></object>
                            <img
                                src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/house-second-level.png"
                                alt="" name="house">

                            <div class="house-label">
                                <object type="image/svg+xml"
                                        data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/label-first-level.svg"
                                        name="label"></object>
                                <span>Мой домик</span>
                            </div>
                            <object style="visibility: visible" type="image/svg+xml"
                                    data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/ship-second-level.svg"
                                    name="ship"></object>
                            <div class="brevno">
                                <span class="brevno-text">&nbsp</span>
                                <svg viewBox="0 0 37.1 38.9" preserveAspectRatio="xMidYMid meet" class="leaf">
                                    <use xlink:href="#leaf"></use>
                                </svg>
                            </div>
                        </div>
                    <?php } elseif ($levelOfThisYearsOld->title >= 10) { ?>
                        <div class="user-level level-3">
                            <object style="visibility: visible" type="image/svg+xml"
                                    data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/bobr-third-level.svg"
                                    name="bobr"></object>
                            <img
                                src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/house-third-level.png"
                                alt="" name="house">

                            <div class="house-label">
                                <object type="image/svg+xml"
                                        data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/label-third-level.svg"
                                        name="label"></object>
                                <span>Мой домик</span>
                            </div>
                            <object style="visibility: visible" type="image/svg+xml"
                                    data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/ship-third-level.svg"
                                    name="ship"></object>
                            <div class="brevno">
                                <span class="brevno-text">&nbsp</span>
                                <svg viewBox="0 0 37.1 38.9" preserveAspectRatio="xMidYMid meet" class="leaf">
                                    <use xlink:href="#leaf"></use>
                                </svg>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="history-carousel">
                        <?php if (isset($this->user->myReadedBooksByYears[$yearsOld])) { ?>
                            <div class="jcarousel-wrapper my-books">
                                <div class="jcarousel">
                                    <ul class="child-profile">
                                        <?php foreach ($this->user->myReadedBooksByYears[$yearsOld] AS $book) { ?>
                                            <li>
                                                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $book->c_id); ?>">
                                                    <figure><img src="<?php echo $book->thumbnailUrl; ?>"
                                                                 alt="<?php htmlspecialchars($book->c_title); ?>"/>
                                                    </figure>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <a href="javascript:void(0);" class="jcarousel-control-prev">&#60</a>
                                <a href="javascript:void(0);" class="jcarousel-control-next">&#62</a>

                                <div class="shelf-info">
                                    <span>Мои прочитанные книги</span>
                                    <button class="settings no-box-shadow bulb-menu-open" style="display: none;">
                                        <svg viewBox="0 0 37.1 37.1" preserveAspectRatio="xMinYMin meet">
                                            <use xlink:href="#settings"></use>
                                        </svg>
                                    </button>
                                    <ul class="bulb-menu">
                                        <li>
                                            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publications'); ?>"><?php echo JText::_('COM_PLOT_ALL_BOOKS'); ?></a>
                                        </li>
                                    </ul>
                                    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publications&my_books=1'); ?>">
                                        Всего: <?php echo PlotHelper::getWordEnding(count($this->user->myReadedBooksByYears[$yearsOld]), 'книга', 'книги', 'книг'); ?>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (isset($this->user->myStudiedCoursesByYears[$yearsOld])) { ?>
                            <div class="jcarousel-wrapper my-courses">
                                <div class="jcarousel">
                                    <ul class="child-profile">
                                        <?php foreach ($this->user->myStudiedCoursesByYears[$yearsOld] AS $myStudiedCourse) { ?>
                                            <li>
                                                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=course&id=' . $myStudiedCourse->id); ?>">
                                                    <figure>
                                                        <img
                                                            src="<?php echo $myStudiedCourse->image ? JUri::root() . $myStudiedCourse->image : JUri::root() . 'templates/plot/img/blank300x200.jpg'; ?>"/>
                                                        <figcaption><?php echo $myStudiedCourse->course_name; ?></figcaption>
                                                    </figure>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <a href="javascript:void(0);" class="jcarousel-control-prev">&#60</a>
                                <a href="javascript:void(0);" class="jcarousel-control-next">&#62</a>

                                <div class="shelf-info">
                                    <span>Мои изученные курсы</span>
                                    <button class="settings no-box-shadow bulb-menu-open" style="display: none;">
                                        <svg viewBox="0 0 37.1 37.1" preserveAspectRatio="xMinYMin meet">
                                            <use xlink:href="#settings"></use>
                                        </svg>
                                    </button>
                                    <ul class="bulb-menu">
                                        <li>
                                            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=courses'); ?>"><?php echo JText::_('COM_PLOT_ALL_COURSES'); ?></a>
                                        </li>
                                    </ul>
                                    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=courses&onlymycourses=1'); ?>">
                                        Всего: <?php echo PlotHelper::getWordEnding(count($this->user->myStudiedCoursesByYears[$yearsOld]), 'курс', 'курса', 'курсов'); ?>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (isset($this->user->myCertificatesByYears[$yearsOld])) { ?>
                            <div class="jcarousel-wrapper my-certificates">
                                <div class="jcarousel">
                                    <ul class="child-profile">
                                        <?php foreach ($this->user->myCertificatesByYears[$yearsOld] AS $certificate) { ?>
                                            <li>
                                                <a class="fancybox" rel="certificate-shelf-set"
                                                   data-title="<?php echo $certificate->title; ?>"
                                                   data-description="<?php echo $certificate->caption; ?>"
                                                   data-date="<?php echo JFactory::getDate($certificate->assigned_date)->format('d.m.Y H:i'); ?>"
                                                   data-share=" data-image='<?php echo $certificate->imageUrl; ?>' data-url='<?php echo $certificate->imageUrl; ?>' data-title='<?php echo addslashes($certificate->title); ?>' "
                                                   href="<?php echo JUri::root() . 'media/com_easysocial/photos/' . $certificate->album_id . '/' . $certificate->id . '/' . PlotHelper::getImageOriginalById($certificate->id)->value; ?>">
                                                    <figure>
                                                        <img
                                                            src="<?php echo JURI::root() . 'media/com_easysocial/photos/' . $certificate->album_id . '/' . $certificate->id . '/' . $certificate->value; ?>">
                                                    </figure>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <a href="javascript:void(0);" class="jcarousel-control-prev">&#60</a>
                                <a href="javascript:void(0);" class="jcarousel-control-next">&#62</a>

                                <div class="shelf-info">
                                    <span>Мои достижения</span>
                                    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=photos&id=' . $this->user->id); ?>#<?php echo $this->my->isParent() ? 'parent' : 'all-child'; ?>-certificates">
                                        Всего: <?php echo PlotHelper::getWordEnding(count($this->user->myCertificatesByYears[$yearsOld]), 'достижение', 'достижения', 'достижений'); ?>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                </div>
            <?php } ?>
        </div>
    </section>
<?php } ?>
<?php # </editor-fold> ?>
</div>

<?php if (!$this->issetCurrentYearProgress) { ?>
    <div></div>
<?php } ?>

</main>