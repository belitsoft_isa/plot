<?php
defined('_JEXEC') or die('Unauthorized Access');

$jinput = JFactory::getApplication()->input;
$Itemid = $jinput->get('Itemid', 0, 'INT');
$my = Foundry::user();
?>

<!--section videos-->
My videos
<?php if ($this->video_links) { foreach ($this->video_links AS $video) { ?>
    <?php if ($video->type == 'link' && strripos($video->path, 'youtube.com/') !== FALSE) {
        $youtubeNumber = substr($video->path, strripos($video->path, '?v=') + 3);
        if (($pos = strpos($youtubeNumber, '&')) !== FALSE) $youtubeNumber = substr($youtubeNumber, 0, $pos); ?>
        <div class="image_tn">
            <a href="<?php echo '//www.youtube.com/embed/' . $youtubeNumber; ?>" data-webm="" class="html5lightbox" title="">
                <img alt="Thumbnail" src="//i1.ytimg.com/vi/<?php echo $youtubeNumber; ?>/mqdefault.jpg" width="175">
            </a>
        </div>
    <?php } ?>
<?php } } ?>

<script src="<?php echo JURI::base(); ?>components/com_plot/assets/js/video.js"></script>
<link rel="stylesheet" href="<?php echo JURI::base(); ?>components/com_plot/assets/cssvideo-js.css"/>
<script>
    videojs.options.flash.swf = "<?php echo JURI::base(); ?>components/com_plot/assets/js/video-js.swf"
</script>
<script type="text/javascript">
    document.createElement('video');
    document.createElement('audio');
    document.createElement('track');
</script>

<?php if ($this->video_files) { foreach ($this->video_files AS $video) { ?>
    <video id="plot_video_<?php echo $video->id; ?>" class="video-js vjs-default-skin" data-setup='{"controls": true, "autoplay": false, "preload": "auto" }'>
        <source src="<?php echo JUri::root() . "media/com_plot/videos/" . $my->id . "/" . $video->path; ?>" type='video/<?php echo substr($video->path, strrpos($video->path, '.') + 1); ?>'/>
    </video>
<?php } } ?>

<!--photos section-->
My photos
<br>
<?php if ($this->photos) { foreach ($this->photos AS $photo) { ?>
    <a data-group="plot-current-photos" href="<?php echo JURI::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . $photo->value; ?>" class="html5lightbox">
        <img src="<?php echo JURI::root() . 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . $photo->value; ?>">
    </a>
    <br>
<?php } } ?>
    
<!--portfolios section-->
MY portfolios<br>
<?php if ($this->user->id != $my->id) { ?>
   <?php if($this->user->profile_id == (int)plotGlobalConfig::getVar('parentProfileId')){ ?>
    <a href="<?php echo JRoute::_("index.php?option=com_plot&view=userportfolios&id=" . $this->user->id); ?>"> <?php echo JText::_('COM_PLOT_VIEW_ALL_USER_PORTFOLIOS'); ?></a>
    <?php } ?>
<?php } ?>
    
<?php if ($this->portfolios) { foreach ($this->portfolios AS $portfolio) {
    echo $portfolio->title . '<br>';
    if ($portfolio->img) { ?>
        <img src="<?php echo JURI::root() . '/images/com_plot/portfolio/' . $this->user->id . '/' . $portfolio->img; ?>">
        <br>
    <?php }
    echo $portfolio->description . '<br>';?>
    <a href="<?php echo $portfolio->link; ?>"><?php echo $portfolio->link; ?></a><br>
<?php } } ?>