<?php
defined('_JEXEC') or die;
jimport('joomla.html.html.bootstrap');
?>

<link rel="stylesheet" href="templates/plot/css/template.css" type="text/css">

<script src="media/jui/js/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.close').click(function(){});
        jQuery('#plot-list-pending-friends').on('click', '.plot-friends-accept', function (e) {
            e.preventDefault();
            jQuery('#task').val('profile.approve');
            jQuery('#user-id').val(jQuery(this).attr('data-plot-friend-id'));
            jQuery('#admin-form').submit();
        });
        jQuery('#plot-list-pending-friends').on('click', '.plot-friends-reject', function (e) {
            e.preventDefault();
            jQuery('#task').val('profile.reject');
            jQuery('#user-id').val(jQuery(this).attr('data-plot-friend-id'));
            jQuery('#admin-form').submit();
        });
    });
</script>
<div id="system-message-container-popup"></div>

<?php
$count_friends = count($this->friends);

if ($this->friends) {
    ?>
    <div id="plot-list-pending-friends">
        <form id="admin-form" action="<?php echo JRoute::_('index.php?option=com_plot' );?>">
            <input name="task" value="" type="hidden" id="task">
            <input name="id" type="hidden" id="user-id" value="">
        <?php
        foreach ($this->friends AS $friend) {

            if(isset($friend->avatars) && $friend->avatars['small']){ ?>
                <img src="<?php echo JUri::root().'media/com_easysocial/avatars/users/'.(int)$friend->id.'/'.$friend->avatars['small']; ?>">
                 <?php   }else{
                ?>
                <img src="<?php echo JUri::root().'media/com_easysocial/defaults/avatars/users/small.png'; ?>" >
            <?php } ?>

            <a onclick="window.parent.location.href='<?php echo JRoute::_('index.php?option=com_plot&view=profile&id=' . (int)$friend->actor_id); ?>'" href="javascript:void(0)">
                <?php echo Foundry::user((int)$friend->actor_id)->name; ?>
            </a>
            <a class="plot-friends-reject" data-plot-friend-id="<?php echo (int)$friend->id; ?>" href="javascript:void(0)"><?php echo JText::_('COM_PLOT_REJECT'); ?></a>
            <a class="plot-friends-accept" data-plot-friend-id="<?php echo (int)$friend->id; ?>" href="javascript:void(0)"><?php echo JText::_('COM_PLOT_ACCEPT'); ?></a>
        <?php
        }
        ?>
        </form>
    </div>
<?php
}
echo JText::_('COM_PLOT_FRIENDS');
if($this->oldfriends){
    foreach ($this->oldfriends as $friend) {
        //pre($friend->avatars);
        ?>
        <div>
            <div class="plot-avatar">
                <a onclick="window.parent.location.href='<?php echo JRoute::_('index.php?option=com_plot&view=profile&id=' . (int)$friend->id); ?>'" href="javascript:void(0)">
            <?php

                    if(isset($friend->avatars) && $friend->avatars['small']){?>
                        <img src="<?php echo JUri::root().'media/com_easysocial/avatars/users/'.(int)$friend->id.'/'.$friend->avatars['small']; ?>">
                 <?php   }else{
                    ?>
                <img src="<?php echo JUri::root().'media/com_easysocial/defaults/avatars/users/small.png'; ?>" >
                <?php } ?>
                        </a>

            </div>
            <div class="plot-user-name">
                <?php echo Foundry::user((int)$friend->id)->name; ?>
            </div>
            <a href="<?php echo JRoute::_('index.php?option=com_plot&task=conversations.newMessage&id='.(int)$friend->id.'&Itemid='.(int)$this->Itemid);?>" rel="{size: {x: 744, y: 525}, handler:'iframe'}" class="modal"><?php echo JText::_('COM_PLOT_WRITE_ME_MESSAGE');?></a>

        </div>

   <?php }
}
