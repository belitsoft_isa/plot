<?php
defined('_JEXEC') or die;


?>

<link type="text/css" href="/plot.tst/media/jui/css/bootstrap.min.css" rel="stylesheet">

<form id="plot-photo-form"    action="<?php echo JRoute::_('index.php?option=com_plot&view=profile&task=profile.sendReport'); ?>"    method="post" enctype="multipart/form-data">
<input name="uid" value="<?php echo $this->id; ?>" type="hidden">


            <textarea name="message" data-reports-message class="input-full mt-20" style="width: 100%;height: 100px;"></textarea>

            <div class="mt-20 small">
                <?php echo JText::_( 'COM_PLOT_REPORTS_FOR_ADMIN' );?>
            </div>
            <buttons>

                <button data-report-button type="submit" class="btn btn-es-primary"><?php echo JText::_('COM_PLOT_SEND_REPORT'); ?></button>

            </buttons>
            <?php echo JHtml::_('form.token'); ?>
</form>




