<?php
defined('_JEXEC') or die;
jimport('joomla.html.html.bootstrap');
?>

<link type="text/css" href="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate().'/css/style.css'; ?>" rel="stylesheet">
<link type="text/css" href="<?php echo JUri::root(); ?>components/com_plot/libraries/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet">
<script src="<?php echo JUri::root(); ?>media/jui/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/jui/js/jquery.ui.core.min.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/mootools-core.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/core.js" type="text/javascript"></script>

<script src="<?php echo JUri::root(); ?>media/system/js/mootools-more.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/modal.js" type="text/javascript"></script>

<script type="text/javascript">
    function sendComplain(uid){
        var message=jQuery('#message').val();
        if(message==''){
        alert('Заполните все поля');
        }else{
            jQuery.post('index.php?option=com_plot&task=profile.sendReport', {uid: uid, message:message}, function (response) {
                if(response.status){
                    SqueezeBox.initialize({
                        size: {x: 744, y: 525}
                    });
                    window.parent.document.getElementById('sbox-window').style.position='fixed';
                    var str = '<div id="enqueued-message">' + response.message + '</div>';
                    SqueezeBox.setContent('adopt', str);
                    SqueezeBox.resize({x: 300, y: 150});

                    window.location.href="<?php echo JRoute::_('index.php?option=com_plot&task=profileedit.ajaxGetForeignChildProfilePopupHtml&id='.$this->userId.'#tab-3');?>"
                }else{
                   SqueezeBox.initialize({
                        size: {x: 744, y: 525}
                    });
                    window.parent.document.getElementById('sbox-window').style.position='fixed';
                    var str = '<div id="enqueued-message">' + response.message + '</div>';
                    SqueezeBox.setContent('adopt', str);
                    SqueezeBox.resize({x: 200, y: 50});

                    window.location.href="<?php echo JRoute::_('index.php?option=com_plot&task=profileedit.ajaxGetForeignChildProfilePopupHtml&id='.$this->userId.'#tab-3');?>"
                }
            });
        }
    }
    jQuery(document).ready(function(){
      /*  var complain_iframe=window.parent.document.getElementsByTagName('iframe')[0],
       sbox_content=window.parent.document.getElementById('sbox-window');
        jQuery('body').addClass('popup-style');
        complain_iframe.width=500;
        complain_iframe.height=350;
        sbox_content.width=500;
        sbox_content.height=350;*/


    });
</script>
<?php # <editor-fold defaultstate="collapsed" desc="SVG"> ?>
<svg xmlns="http://www.w3.org/2000/svg" style="position: absolute; left: -9999px;">
    <symbol id="smile-dont-like" viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet">
        <path class="st0" d="M10.5,0C16,0,20.9,4.9,20.9,10.5S16,21,10.5,21C4.2,21,0,16.1,0,10.5S4.2,0,10.5,0L10.5,0z M17.4,13.3c-4.2,0-8.4,0.7-11.9,2.1C10.5,10.5,10.5,10.5,17.4,13.3L17.4,13.3z M7,5.6c1.4,0,2.1,0.7,2.1,2.1c0,0.7-0.7,2.1-2.1,2.1c-1.4,0-2.1-1.4-2.1-2.1C4.9,6.3,5.6,5.6,7,5.6L7,5.6z M13.2,5.6c1.4,0,2.1,0.7,2.1,2.1c0,0.7-0.7,2.1-2.1,2.1c-0.7,0-2.1-1.4-2.1-2.1C11.2,6.3,12.5,5.6,13.2,5.6L13.2,5.6z"></path>
    </symbol>
</svg>
<?php  //</editor-fold> ?>

<div id="complain-form">
    <fieldset>
        <input type="hidden" name="uid" id="uid" value="<?php echo  $this->userId; ?>">
        <label for="message">Пожаловаться на пользователя <?php plotUser::factory($this->userId)->name; ?></label>
        <textarea id="message" name="message" placeholder="Почему Вы хотите пожаловаться на этого пользователя?"></textarea>
        <button class="button brown hover-shadow" onclick="sendComplain(<?php echo $this->userId; ?>)">
            <span>
                <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-dont-like"><use xlink:href="#smile-dont-like"></use></svg>
                Отправить сообщение
            </span>
        </button>
    </fieldset>
</div>
