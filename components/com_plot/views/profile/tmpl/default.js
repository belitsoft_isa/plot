function toggleAddItemsBlock()
{
    if (jQuery('#child-add-activity').is(':visible')) {
        jQuery('#child-add-activity').hide();
        jQuery('.main-user-info .toggle-add-items-block-button').removeClass('active');
    } else {
        jQuery('#child-add-activity').show();
        jQuery('.main-user-info .toggle-add-items-block-button').addClass('add').addClass('active');
    }
}
jQuery('body').addClass('child-profile');
function filterMyFriendsByTags(userId)
{
    var tagId = jQuery('#friends-filter-tags').val()

    jQuery('.loading-friends-list').html('Загрузка...');
    jQuery.post('index.php?option=com_plot&task=profileedit.ajaxFilterMyFriendsByTags', {userId: userId, tagId: tagId}, function(jsonResponce){
        var data = jQuery.parseJSON(jsonResponce);
        jQuery('#tab-1').html( data.html );
        });
    }

function childMenuShow(menu)
{
    jQuery('.child-profile #child-menu-'+menu).css('opacity', '1').css('visibility', 'visible');
    jQuery('.child-profile a[href=#child-menu-'+menu+']').parent().addClass('active');
    
    switch(menu) {
        case 'courses':
            jQuery('.child-profile .shelf-menu-top.my-courses').css('opacity', '1').css('visibility', 'visible');
            break;
        case 'read':
            jQuery('.child-profile .shelf-menu-top.my-books').css('opacity', '1').css('visibility', 'visible');
            break;
        case 'think':
            jQuery('.child-profile .shelf-menu-top.my-thinking').css('opacity', '1').css('visibility', 'visible');
            break;
    }
}

function childMenuHide()
{
    jQuery('.child-profile .overlay[id^=child-menu-]').css('opacity', '0').css('visibility', 'hidden');
    jQuery('.child-profile .child-menu > li').removeClass('active');
    jQuery('.child-profile .shelf-menu-top').css('opacity', '0').css('visibility', 'hidden');
}

// profile menu, bulb menus
jQuery(document).on('click', function (event) {
    if (!jQuery(event.target).closest('.bulb-menu').length && !jQuery(event.target).closest('.bulb-menu-open').length) {
        jQuery('.bulb-menu').hide();
    }
});
jQuery(document).ready(function(){
    jQuery('#show-all-certificates').hide();
    jQuery('#show-all-photos').hide();
    jQuery('#show-all-video').hide();
    jQuery('#show-all-meetings').hide();
    jQuery('#child-add-activity').hide();
    jQuery('#tabs-nav').on('click','a.stream-nav',function(){
        var tab_type=jQuery(this).attr('data-action');
        jQuery('#show-all-meetings').hide();
        jQuery('#show-all-events').hide();
        jQuery('#show-all-certificates').hide();
        jQuery('#show-all-photos').hide();
        jQuery('#show-all-video').hide();
        jQuery('#child-add-activity').hide();

        jQuery('a.stream-nav').removeClass('active');
        switch (tab_type) {
            case 'events':
                jQuery('#show-all-events').show();
                jQuery(this).addClass('active');
                break;
           case 'sertificates':
                jQuery('#show-all-certificates').show();
               jQuery(this).addClass('active');
                break;
           case 'photos':
                jQuery('#show-all-photos').show();
               jQuery(this).addClass('active');
                break;
            case 'video':
                jQuery('#show-all-video').show();
                jQuery(this).addClass('active');
                break;
            case 'meetings':
                jQuery('#show-all-meetings').show();
                jQuery(this).addClass('active');
                break;
            case 'add':
                jQuery('#child-add-activity').show();
                jQuery(this).addClass('active');
                jQuery('#show-all-meetings').hide();
                jQuery('#show-all-events').hide();
                jQuery('#show-all-certificates').hide();
                jQuery('#show-all-photos').hide();
                jQuery('#show-all-video').hide();

                break;
        }
    });    


    jQuery('#submit-sertificate').click(function (e) {
        if (!jQuery('#certificate-img').val()) {
            e.preventDefault();
            alert('Загрузите картинку');
        }
    });

    jQuery('#submit-photo').click(function (e) {
        if (!jQuery('#photo-img').val()) {
            e.preventDefault();
            alert('Загрузите картинку');
        }
    });

    jQuery('#submit-video').click(function (e) {
        if (!jQuery('#plot-video-file').val()) {
            e.preventDefault();
            alert('Загрузите видео');
        }
    });

    jQuery("#child-add-activity, #child-media-tabs, #child-add-video, #child-add-course-video").tabs();

    jQuery('.bulb-menu-open').click(function () {
        var bulbMenu = jQuery(this).parent().find('.bulb-menu');
        if (bulbMenu.is(':visible')) {
            bulbMenu.hide();
        } else {
            bulbMenu.show();
        }
    });
    
    //datepicker
    jQuery('#child-certificate-date').datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd.mm.yy",
        nextText: ">>",
        prevText: "<<",
        selectOtherMonths: true,
        firstDay: 1,
        showOtherMonths: true,
        monthNamesShort: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        onClose: function (selectedDate) {
            jQuery("#to").datepicker("option", "minDate", selectedDate);
        }
    }).mask('00.00.0000', {placeholder: "__.__.____"});
    jQuery("#child-event-from").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd.mm.yy",
        nextText: ">>",
        prevText: "<<",
        selectOtherMonths: true,
        firstDay: 1,
        showOtherMonths: true,
        monthNamesShort: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        onClose: function (selectedDate) {
            jQuery("#to").datepicker("option", "minDate", selectedDate);
        }
    }).mask('00.00.0000', {placeholder: "__.__.____"});
    jQuery("#child-event-to").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd.mm.yy",
        nextText: ">>",
        prevText: "<<",
        selectOtherMonths: true,
        firstDay: 1,
        showOtherMonths: true,
        monthNamesShort: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        onClose: function (selectedDate) {
            jQuery("#from").datepicker("option", "maxDate", selectedDate);
        }
    }).mask('00.00.0000', {placeholder: "__.__.____"});

    jQuery("#accordion").accordion({
        heightStyle: "content",
        collapsible: true,
        active: false
    });
    jQuery('#child-event-from-time, #child-event-to-time').mask('00:00', {placeholder: "__:__"});


    jQuery('#plot-photo-form #photo-img').change(function () {
        var filename = jQuery(this).val();
        jQuery('#plot-photo-form .upload-photo-label').html(filename);
    });
    jQuery('#plot-certificate-form #certificate-img').change(function () {
        var filename = jQuery(this).val();
        jQuery('#plot-certificate-form .upload-photo-label').html(filename);
    });
    jQuery('#event-img').change(function () {
        var filename = jQuery(this).val();
        jQuery(this).parent().find('.upload-photo-label').html(filename);
    });

    jQuery('#plot-video-file').change(function () {
        var filename = jQuery('#plot-video-file').val();
        jQuery('#plot-video-form .upload-video-label').html(filename);
    });

    jQuery('.descr .input-data').click(function () {
        jQuery(this).parent().find('.overlay').css('visibility', 'visible').css('opacity', '1');
        jQuery(this).parent().find('textarea').css('visibility', 'visible').css('opacity', '1');
        jQuery(this).parent().find('.apply, .cancel').css('visibility', 'visible');
    });

    jQuery('.descr .overlay').click(function () {
        cancelAddedItemDescription('#' + jQuery(this).attr('id'));
    });


    jQuery('.jcarousel .elem-descr').click(function () {

        this.previousElementSibling.click();
    });


    jQuery('.in-progress').click(function () {
        this.previousElementSibling.click();
    });

    jQuery('.close').click(function () {
        childMenuHide();
    })    
    
    jQuery('#submit-sertificate').click(function(e){
        if(!jQuery('#certificate-img').val()){
            e.preventDefault();
            alert('Загрузите картинку');
        }
    });
    jQuery('#submit-photo').click(function(e){
        if(!jQuery('#photo-img').val()){
            e.preventDefault();
            alert('Загрузите картинку');
        }
    });
    jQuery('#submit-video').click(function(e){
        if(!jQuery('#plot-video-file').val()){
            e.preventDefault();
            alert('Загрузите видео');
        }
    });

    jQuery('#add-course-form').submit(function(){
        var errors = [];
        var isVideoLink = jQuery('#add-course-form #child-add-course-video ul.video > li[aria-controls=child-add-course-video-link]').hasClass('ui-state-active');
                
        if ( !jQuery.trim(jQuery('#add-course-form [name=title]').val()) ) {
            errors.push('Введите название курса');
        }
        if ( jQuery('#add-course-form #add-course-category').val() == 0 ) {
            errors.push('Выберите категорию курса');
        }
        
        jQuery('#add-course-form input[name="add-video-link"]').remove();
        jQuery('#add-course-form input[name="add-uploaded-file"]').remove();
        if ( isVideoLink ) {
            // add video link
            if ( !jQuery.trim(jQuery('#add-course-form [name=video-link]').val()) ) {
                errors.push('Укажите youtube-ссылку на видео');
            }
            if ( !jQuery.trim(jQuery('#add-course-form [name=video-title]').val()) ) {
                errors.push('Укажите заголовок для видео');
            }
            if ( !jQuery.trim(jQuery('#add-course-form [name=video-description]').val()) ) {
                errors.push('Укажите описание видео');
            }
            jQuery('#add-course-form').append('<input type="hidden" name="add-video-link" value="1" />');
        } else {
            // add uploaded video
            if ( !jQuery('#add-course-form #course-videofile-id').val() ) {
                errors.push('Загрузите видео файл');
            }
            if ( !jQuery.trim( jQuery('#add-course-form input[name=uploaded-video-title]').val() ) ) {
                errors.push('Укажите заголовок для видео');
            }
            if ( !jQuery.trim( jQuery('#add-course-form textarea[name=uploaded-video-description]').val() ) ) {
                errors.push('Укажите описание для видео');
            }
            jQuery('#add-course-form').append('<input type="hidden" name="add-uploaded-file" value="1" />');
        }
        
        // questions / answers validation
        if (
            !jQuery.trim(jQuery('#add-course-form textarea[name=question1]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question1-answer1]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question1-answer2]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question1-answer3]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question1-answer4]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question1-answer5]').val()) || 
            !jQuery.trim(jQuery('#add-course-form textarea[name=question2]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question2-answer1]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question2-answer2]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question2-answer3]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question2-answer4]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question2-answer5]').val()) || 
            !jQuery.trim(jQuery('#add-course-form textarea[name=question3]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question3-answer1]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question3-answer2]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question3-answer3]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question3-answer4]').val()) || 
            !jQuery.trim(jQuery('#add-course-form input[name=question3-answer5]').val())
        ) {
            errors.push('Укажите 3 вопроса к видео и по 5 вариантов ответов на каждый из вопросов');
        }
        
        if ( !jQuery('#add-course-form #new_course_ages').val() ) {
            errors.push('Выберите хотя бы один возрастной период');
        } 

        if ( !jQuery('#add-course-form #new_course_tags').val() ) {
            errors.push('Выберите хотя бы один интерес');
        } 
        
        if (errors.length !== 0) {
            alert(errors.join('\n'));
            return false;
        }
        
        if (isVideoLink){
            jQuery('#add-course-form').append('<input type="hidden" name="video-img" value="'+jQuery('#add-course-form #video-youtube-img').attr('src')+'" />');
        } else {
            jQuery('#add-course-form').append('<input type="hidden" name="video-img" value="'+jQuery('#add-course-form #course-videofile-image-area > img').attr('src')+'" />');
        }
        jQuery('#add-course-form button[type="submit"]').attr('disabled', 'disabled');
        return true;
    });
    
});

function endRedactingAddedEntityDescription(elem) {
    elem.find('.overlay, textarea, .apply, .cancel').css('visibility', 'hidden').css('opacity', '0');
}
function startRedactingAddedEntityDescription(elem) {
    elem.find('.overlay, textarea, .apply, .cancel').css('visibility', 'visible').css('opacity', '1');
}

function redacAddedItemDescription(href) {
    startRedactingAddedEntityDescription(jQuery('a[href=' + href + ']').parent());
}
function saveAddedItemDescription(href) {
    var shownDesc = jQuery('a[href=' + href + ']');
    var redactedDesc = shownDesc.parent().find('textarea');

    shownDesc.html(redactedDesc.val());
    endRedactingAddedEntityDescription(shownDesc.parent());
}
function cancelAddedItemDescription(href) {
    var shownDesc = jQuery('a[href=' + href + ']');
    var redactedDesc = shownDesc.parent().find('textarea');

    redactedDesc.val(shownDesc.html());
    endRedactingAddedEntityDescription(shownDesc.parent());
}


