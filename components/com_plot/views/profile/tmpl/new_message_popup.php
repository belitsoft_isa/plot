<?php
defined('_JEXEC') or die;

jimport('joomla.html.html.bootstrap');
?>

<?php
$user = Foundry::user($this->id);

?>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link type="text/css" href="<?php echo JUri::root(); ?>templates/plot/css/style.css" rel="stylesheet">
<link type="text/css" href="<?php echo JUri::root(); ?>components/com_plot/libraries/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet">
<script src="<?php echo JUri::root(); ?>media/jui/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/jui/js/jquery.ui.core.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo JURI::root(); ?>media/jui/js/jquery.ui.core.min.js"></script>

<script src="<?php echo JUri::root(); ?>media/system/js/mootools-core.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/core.js" type="text/javascript"></script>

<script src="<?php echo JUri::root(); ?>media/system/js/mootools-more.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/modal.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function(){
        jQuery('body').addClass('popup-style');
    });
    function sendMessage() {
        var uid = jQuery('#uid').val(),
            message_text = jQuery('#message_text').val();

        jQuery.post('index.php?option=com_plot&task=conversations.sendMessage', {
            uid: uid,
            message: message_text
        }, function (response) {

            window.parent.document.getElementById('sbox-window').style.position = 'fixed';
            var overlay = '<div class="avatar-overlay" id="sbox-overlay2" tabindex="-1"></div>',
                str = '<div id="sbox-window2" role="dialog" aria-hidden="false" class="shadow avatar-sbox">' +
                    '<div id="sbox-content2" class="sbox-content-adopt" style="opacity: 1;"><div id="enqueued-message">'+ response.message +
                    '</div></div><a id="sbox-btn-close2" href="#" onclick="window.parent.document.getElementById(\'sbox-btn-close\').click();" role="button" aria-controls="sbox-window"></a></div>';

            jQuery("#avatar-box").after(str);
            jQuery("#avatar-box").after(overlay);

        });
    }

</script>
<!--<form id="plot-new-message"
      action="<?php echo JRoute::_('index.php?option=com_plot&view=profile&task=conversations.sendMessage'); ?>"-->

<?php # <editor-fold defaultstate="collapsed" desc="SVG"> ?>
<svg xmlns="http://www.w3.org/2000/svg" style="position: absolute; left: -99999px;">
    <symbol id="letter-send" viewBox="0 0 40 27" preserveAspectRatio="xMinYMin meet">
        <g>
            <path d="M31,4.8v-2C31,1.4,29.4,0,27.8,0H3.7C2.1,0,0,1.4,0,2.9v2.4l13.5,7.3L31,4.8z"/>
            <path d="M27.5,19.5c-7.2,0-7.7-0.7-13.1,3c0.3-1.1,0.6-3.2,1.2-4.1C19.3,13.4,25,11.2,31,11V7.8l-17.5,7.8L0,7.8v16.2C0,25.4,2.1,27,3.7,27h24.1c1.6,0,3.2-1.6,3.2-3.1V20C30,19.8,28.9,19.5,27.5,19.5z"/>
        </g>
        <path fill-rule="evenodd" clip-rule="evenodd" d="M31.9,12.9c-5.3,0-10.1,1-13.3,4.5c-0.6,0.6-1.8,2-2,2.9c4.1-2.8,6.3-2.5,11.7-2.5c1.5,0,2.3,0.4,3.6,0.5v4.1l6.2-5c0.5-0.4,1.7-1.4,2-1.7l-8.1-6.8V12.9z"/>
    </symbol>
</svg>
<?php // </editor-fold> ?>
<div id="message-form" <?php echo  (plotUser::factory()->isParent()) ? 'class="parent-profile"' : 'class="child-profile"'; ?> >

    <h6>
        <svg viewBox="0 0 40 27" preserveAspectRatio="xMinYMin meet" class="send-message"><use xlink:href="#letter-send"></use></svg>
        Отправить сообщение
    </h6>
    <fieldset>
        <input type="hidden" name="uid" id="uid" value="<?php echo  $user->id; ?>">
        <label for="message_text">
            <?php echo JText::_('COM_PLOT_MESSAGE_FOR_USER_PRETEXT'); ?>
            <?php plotUser::factory($user->id)->name; ?>
            <a onclick="window.parent.location.href='<?php echo JRoute::_('index.php?option=com_plot&view=profile&id=' . $user->id)?>'" href="javascript:void(0)">
                <img src="<?php echo plotUser::factory($user->id)->getSquareAvatarUrl(); ?>" alt="<?php echo $user->name; ?>"/>
                <p><?php echo $user->name; ?></p>
            </a>
        </label>

        <textarea name="message" id="message_text" placeholder="Напишите Ваше сообщение"></textarea>
        <input type="hidden" id="uid" name="uid" value="<?php echo $user->id; ?>">

        <button class="button accept hover-shadow" onclick="sendMessage()" value="<?php echo JText::_('COM_PLOT_SEND'); ?>">
            <span>
                <svg viewBox="0 0 40 27" preserveAspectRatio="xMinYMin meet" class="send-message"><use xlink:href="#letter-send"></use></svg>
                Отправить сообщение
            </span>
        </button>
        <button class="button cancel-act hover-shadow" onclick="window.parent.document.getElementById('sbox-btn-close').click();">
            <span>
                &#215&nbsp&nbsp&nbspОтменить
            </span>
        </button>
        <?php echo JHtml::_('form.token'); ?>
        <div id="avatar-box"></div>
    </fieldset>

</div>





