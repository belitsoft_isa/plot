<?php
defined('_JEXEC') or die('Unauthorized Access');

?>
<?php if ($this->user->stream) { ?>
    <div class="jcarousel-wrapper">
        <div class="jcarousel" id="events-jcarousel">
            <ul class="child-profile" id="events-jcarousel">
            <?php foreach ($this->user->stream AS $streamObj) { ?>
                <?php switch ($streamObj->command) {
                    case 'buy.book':
                    case 'read.book':
                        ?>
                        <li class="stream-item">
                            <a href="<?php echo $streamObj->original; ?>">
                                <h6>
                                    <i>
                                        <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">
                                            <use xlink:href="#star"></use>
                                        </svg>
                                    </i>
                                    <span
                                        class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                                    <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>


                                </h6>
                                <data><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></data>
                                <div>
                                    <figure class="book">
                                        <?php if ($streamObj->image_url) { ?>
                                            <img src="<?php echo $streamObj->image_url; ?>"/>
                                        <?php } ?>
                                        <i class="activity-icons">
                                            <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"
                                                 style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                                <use xlink:href="#book"></use>
                                            </svg>
                                        </i>
                                    </figure>
                                </div>
                                <div>
                                    <?php if ($streamObj->bottom_description) { ?>
                                        <blockquote><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                    <?php } ?>
                                    <p class="stream-author"><b>Автор: </b><?php echo $streamObj->author; ?></p>
                                </div>
                            </a>
                        </li>
                        <?php
                        break;
                    case 'course.finish':
                        ?>
                        <li class="stream-item">
                            <a href="<?php echo $streamObj->original; ?>">
                                <h6>
                                    <i>
                                        <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">
                                            <use xlink:href="#star"></use>
                                        </svg>
                                    </i>
                                    <span
                                        class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                                    <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                </h6>
                                <data><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></data>
                                <div>
                                    <figure>
                                        <?php if ($streamObj->image_url) { ?>
                                            <img src="<?php echo $streamObj->image_url; ?>"/>
                                            <i class="activity-icons">
                                                <svg style="fill:url(#svg-gradient); stroke:url(#svg-gradient);"
                                                     preserveAspectRatio="xMidYMid meet" viewBox="0 0 30.3 34">
                                                    <use xlink:href="#zoom"/>
                                                </svg>
                                            </i>
                                        <?php } ?>
                                    </figure>
                                </div>
                                <div>
                                    <?php if ($streamObj->bottom_description) { ?>
                                        <blockquote><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                    <?php } ?>
                                </div>
                            </a>
                        </li>
                        <?php
                        break;
                    case 'add.meeting':
                    case 'subscribe.meeting':
                    case 'add.meeting':
                        ?>
                        <li class="stream-item">
                            <a href="#">
                                <h6>
                                    <i>
                                        <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">
                                            <use xlink:href="#star"></use>
                                        </svg>
                                    </i>
                                    <span
                                        class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                                    <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                </h6>
                                <data><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></data>
                                <div>
                                    <figure>
                                        <?php if ($streamObj->image_url) {
                                            ?>
                                            <img src="<?php echo $streamObj->image_url; ?>"/>
                                        <?php } ?>
                                        <i class="activity-icons">
                                            <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet"
                                                 style="fill:url(#svg-gradient);">
                                                <use xlink:href="#lamp-meeting"></use>
                                            </svg>
                                        </i>
                                    </figure>
                                </div>
                                <div>
                                    <?php if ($streamObj->bottom_description) { ?>
                                        <blockquote><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                    <?php } ?>
                                </div>
                            </a>
                        </li>
                        <?php
                        break;
                    case 'add.certificate':
                    case 'add.old.certificate':
                        ?>
                        <li class="stream-item">
                            <a class="fancybox" rel="stream-set" data-title="<?php echo $streamObj->title; ?>"
                               data-description="<?php echo $streamObj->bottom_description; ?>"
                               data-date="<?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?>"
                               data-share=" data-image='<?php echo $streamObj->original; ?>' data-url='<?php echo $streamObj->original; ?>' data-title='<?php echo  addslashes($streamObj->title);?>' "

                               href="<?php echo $streamObj->original; ?>">
                                <h6>
                                    <i>
                                        <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">
                                            <use xlink:href="#star"></use>
                                        </svg>
                                    </i>
                                    <span
                                        class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                                    <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                </h6>
                                <data><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></data>
                                <div>
                                    <figure>
                                        <?php if ($streamObj->image_url) {
                                            ?>
                                            <img src="<?php echo $streamObj->image_url; ?>"/>
                                        <?php } ?>
                                        <i class="activity-icons">
                                            <svg viewBox="0 0 24.9 25.8" preserveAspectRatio="xMidYMid meet"
                                                 style="fill:url(#svg-gradient);">
                                                <use xlink:href="#certificate"></use>
                                            </svg>
                                        </i>
                                    </figure>
                                </div>
                                <div>
                                    <?php if ($streamObj->bottom_description) { ?>
                                        <blockquote><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                    <?php } ?>
                                </div>
                            </a>
                        </li>
                        <?php
                        break;
                    case 'add.photo':
                        ?>
                        <li class="stream-item">
                            <a class="fancybox" rel="stream-set" data-title="<?php echo $streamObj->title; ?>"
                               data-description="<?php echo $streamObj->bottom_description; ?>"
                               data-date="<?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?>"
                               data-share=" data-image='<?php echo $streamObj->original; ?>' data-url='<?php echo $streamObj->original; ?>' data-title='<?php echo  addslashes($streamObj->title);?>' "
                               href="<?php echo $streamObj->original; ?>">
                                <h6>
                                    <i>
                                        <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">
                                            <use xlink:href="#star"></use>
                                        </svg>
                                    </i>
                                    <span
                                        class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                                    <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                </h6>
                                <data><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></data>
                                <div>
                                    <figure>
                                        <?php if ($streamObj->image_url) {
                                            ?>
                                            <img src="<?php echo $streamObj->image_url; ?>"/>
                                        <?php } ?>
                                        <i class="activity-icons">
                                            <svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid meet"
                                                 style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                                <use xlink:href="#zoom"></use>
                                            </svg>
                                        </i>
                                    </figure>
                                </div>
                                <div>
                                    <?php if ($streamObj->bottom_description) { ?>
                                        <blockquote><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                    <?php } ?>
                                </div>
                            </a>
                        </li>
                        <?php
                        break;
                    case 'avatar.change':
                        ?>
                        <li class="stream-item">
                            <a class="fancybox" rel="stream-set" data-title="<?php echo $streamObj->title; ?>"
                               data-description="<?php echo $streamObj->bottom_description; ?>"
                               data-date="<?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?>"
                               data-share=" data-image='<?php echo $streamObj->original; ?>' data-url='<?php echo $streamObj->original; ?>' data-title='<?php echo  addslashes($streamObj->title);?>' "
                               href="<?php echo $streamObj->original; ?>">
                                <h6>
                                    <i>
                                        <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">
                                            <use xlink:href="#star"></use>
                                        </svg>
                                    </i>
                                    <span
                                        class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                                    <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                </h6>
                                <data><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></data>
                                <div>
                                    <figure>
                                        <?php if ($streamObj->image_url) { ?>
                                            <img
                                                src="<?php echo pathinfo($streamObj->image_url, PATHINFO_DIRNAME) . '/cropped.jpg'; ?>"/>
                                        <?php } ?>
                                        <i class="activity-icons">
                                            <svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid meet"
                                                 style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                                <use xlink:href="#zoom"></use>
                                            </svg>
                                        </i>
                                    </figure>
                                </div>
                                <div>
                                    <?php if ($streamObj->bottom_description) { ?>
                                        <blockquote><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                    <?php } ?>
                                </div>
                            </a>
                        </li>
                        <?php
                        break;
                    case 'add.video':
                        ?>
                        <li class="stream-item">
                            <a class="fancybox" data-fancybox-type="ajax" rel="stream-set"
                               data-title="<?php echo $streamObj->bottom_title; ?>"
                               data-description="<?php echo $streamObj->bottom_description; ?>"
                               data-date="<?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?>"
                                <?php if ($streamObj->video_type == 'link') { ?>
                                    data-share=" data-image='<?php echo $streamObj->image_url; ?>' data-url='https://youtu.be/<?php echo $streamObj->youtubeNumber; ?>' data-title='<?php echo  addslashes($streamObj->title);?>' "
                                    href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxOpenVideo&youtubeNumber=' . $streamObj->youtubeNumber); ?>"
                                <?php } else { ?>
                                    data-share=" data-image='<?php echo $streamObj->image_url; ?>' data-url='<?php echo JRoute::_('index.php?option=com_plot&task=profile.openVideoFile&videoId=' . $streamObj->youtubeNumber); ?>' data-title='<?php echo  addslashes($streamObj->title);?>' "
                                    href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.openVideoFile&videoId=' . $streamObj->youtubeNumber); ?>"
                                <?php } ?> >
                                <h6>
                                    <i>
                                        <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">
                                            <use xlink:href="#star"></use>
                                        </svg>
                                    </i>
                                    <span class="action-title">
                                        <?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?>
                                    </span>

                                    <p class="stream-title">
                                        <?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?>
                                    </p>
                                </h6>
                                <data><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></data>
                                <div>
                                    <figure>
                                        <?php if ($streamObj->image_url) { ?>
                                            <img src="<?php echo $streamObj->image_url; ?>"/>
                                        <?php } ?>
                                        <i class="activity-icons">
                                            <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet"
                                                 style="fill:url(#svg-gradient);">
                                                <use xlink:href="#play"></use>
                                            </svg>
                                        </i>
                                    </figure>
                                </div>
                                <div>
                                    <?php if ($streamObj->bottom_description) { ?>
                                        <blockquote><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                    <?php } ?>
                                </div>
                            </a>
                        </li>
                        <?php
                        break;
                    case 'add.coursevideo':
                        ?>
                        <?php
                        $coursevideo = PlotHelper::getCourseVideoById($streamObj->entity_id); ?>
                        <?php if ($coursevideo->status == 1 || plotUser::factory()->id == (int)$coursevideo->user_id || plotUser::factory()->id == $coursevideo->reviewer) { ?>

                        <li class="stream-item">
                            <?php if (plotUser::factory()->id == $coursevideo->reviewer && $coursevideo->status == 0) { ?>

                                <a href="<?php echo JRoute::_("index.php?option=com_plot&task=course.ajaxCourse") . '&id=' . $coursevideo->id; ?>"
                            <?php } else { ?>
                                <a class="fancybox" data-fancybox-type="ajax" rel="stream-set"
                                   data-title="<?php echo $streamObj->bottom_title; ?>"
                                   data-description="<?php echo $streamObj->bottom_description; ?>"
                                   href="<?php echo $streamObj->original; ?>"
                                   data-share=" data-image='<?php echo $streamObj->image_url; ?>' data-url='<?php echo $streamObj->original; ?>' data-title='<?php echo  addslashes($streamObj->title);?>' "
                                <?php } ?> >
                            <h6>
                                <i>
                                    <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">
                                        <use xlink:href="#star"></use>
                                    </svg>
                                </i>
                                    <span class="action-title">
                                        <?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?>
                                    </span>

                                <p class="stream-title">
                                    <?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?>
                                </p>
                            </h6>

                            <div>
                                <figure>
                                    <?php if ($streamObj->image_url) { ?>
                                        <img src="<?php echo $streamObj->image_url; ?>"/>
                                    <?php } ?>
                                    <i class="activity-icons">
                                        <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet"
                                             style="fill:url(#svg-gradient);">
                                            <use xlink:href="#play"></use>
                                        </svg>
                                    </i>
                                </figure>
                            </div>
                            <div>
                                <?php if ($streamObj->bottom_description) { ?>
                                    <blockquote><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                <?php } ?>
                            </div>
                            </a>
                        </li>
                    <?php } ?>
                        <?php
                        break;
                    case 'add.portfolio':
                        ?>
                        <li class="stream-item">
                            <a href="<?php echo $streamObj->original; ?>">
                                <h6>
                                    <i>
                                        <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">
                                            <use xlink:href="#star"></use>
                                        </svg>
                                    </i>
                                    <span
                                        class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                                    <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                </h6>
                                <data><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></data>
                                <div>
                                    <figure>
                                        <?php if ($streamObj->image_url) { ?>
                                            <img src="<?php echo $streamObj->image_url; ?>"/>
                                        <?php } ?>
                                    </figure>
                                </div>
                                <div>
                                    <?php if ($streamObj->bottom_description) { ?>
                                        <blockquote><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                    <?php } ?>
                                </div>
                            </a>
                        </li>
                        <?php
                        break;
                    case 'add.tag':
                    case 'assess.tag':
                    case 'describe.tag':
                        ?>
                        <li class="stream-item">
                            <h6>
                                <i>
                                    <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">
                                        <use xlink:href="#star"></use>
                                    </svg>
                                </i>
                                <span
                                    class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                                <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                            </h6>
                            <data><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></data>
                            <div>
                                <figure>
                                    <?php if ($streamObj->image_url) { ?>
                                        <img src="<?php echo $streamObj->image_url; ?>"/>
                                    <?php } ?>
                                </figure>
                            </div>
                            <div>
                                <?php if ($streamObj->bottom_description) { ?>
                                    <blockquote><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                <?php } ?>
                            </div>
                        </li>
                        <?php
                        break;
                    case 'add.essay':
                        ?>
                        <?php  $essay = PlotHelper::getEssayById($streamObj->entity_id);
                        if ($essay) {

                        ?>
                        <?php if ($essay->status == 1 || (plotUser::factory()->id == (int)$essay->user_id || plotUser::factory()->id == plotUser::factory()->isBookAuthor($essay->book_id))) { ?>
                        <li class="stream-item">
                            <a class="fancybox" rel="stream-set" data-title=""
                               data-description="<?php echo $streamObj->bottom_description; ?>"
                               data-date="<?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?>"
                               <?php if($essay->status == 1){ ?>
                               data-share=" data-image='<?php echo $streamObj->original; ?>' data-url='<?php echo $streamObj->original; ?>' data-title='<?php echo  addslashes($streamObj->title);?>' "
                               <?php } ?>
                               href="<?php echo $streamObj->original; ?>">
                                <h6>
                                    <i>
                                        <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">
                                            <use xlink:href="#star"></use>
                                        </svg>
                                    </i>
                                    <span
                                        class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                                </h6>
                                <data><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></data>
                                <div>
                                    <figure>
                                        <?php if ($streamObj->image_url) {
                                            ?>
                                            <img src="<?php echo $streamObj->image_url; ?>"/>
                                        <?php } ?>
                                        <i class="activity-icons">
                                            <svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid meet"
                                                 style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                                <use xlink:href="#zoom"></use>
                                            </svg>
                                        </i>
                                    </figure>
                                </div>
                                <div>
                                    <?php if ($streamObj->bottom_description) { ?>
                                        <blockquote><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                    <?php } ?>
                                </div>
                            </a>
                        </li>
                    <?php
                    }
                }
                        ?>
                        <?php
                        break;
                    case 'finished.program':
                        ?>
                        <li class="stream-item">
                            <a class="fancybox" rel="stream-set" data-title="<?php echo $streamObj->title; ?>"
                               data-description="<?php echo $streamObj->bottom_description; ?>"
                               data-date="<?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?>"
                               href="<?php echo $streamObj->original; ?>">
                                <h6>
                                    <i>
                                        <svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">
                                            <use xlink:href="#star"></use>
                                        </svg>
                                    </i>
                                    <span
                                        class="action-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></span>

                                    <p class="stream-title"><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')); ?></p>
                                </h6>
                                <data><?php echo JFactory::getDate($streamObj->created)->format('d.m.Y H:i'); ?></data>
                                <div>
                                    <figure>
                                        <?php if ($streamObj->image_url) {
                                            ?>
                                            <img src="<?php echo $streamObj->image_url; ?>"/>
                                        <?php } ?>
                                        <i class="activity-icons">
                                            <svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid meet"
                                                 style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                                                <use xlink:href="#zoom"></use>
                                            </svg>
                                        </i>
                                    </figure>
                                </div>
                                <div>
                                    <?php if ($streamObj->bottom_description) { ?>
                                        <blockquote><?php echo PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
                                    <?php } ?>
                                </div>
                            </a>
                        </li>
                        <?php
                        break;
                }
            } ?>
            </ul>
        </div>
        <a href="javascript:void(0);" class="jcarousel-control-prev inactive">&#60</a>
        <a href="javascript:void(0);" class="jcarousel-control-next">&#62</a>
    </div>
<?php } else { ?>
    <?php echo PlotHelper::getDefaultText('activity', $this->user->id); ?>
<?php } ?>
