<?php

defined('_JEXEC') or die('Unauthorized Access');

?>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('#plot-stream-limit').on('click', function () {
            var limit = jQuery(this).attr('data-stream-limit'),
                user_id =<?php echo  $this->user->id; ?>;
            jQuery.ajax({
                type: "POST",
                url: "index.php?option=com_plot&task=profile.stream",
                data: {
                    'limit': limit,
                    'user_id': user_id
                },
                success: function (response) {
                    var data = jQuery.parseJSON(response),
                        streams = data.streams,
                        limit = data.limit,
                        count_results = data.count_results;
                    if (streams != '') {
                        jQuery('#plot-stream-list').html(streams);
                        jQuery('#plot-stream-limit').attr('data-stream-limit', limit)
                        if (limit >= count_results) {
                            jQuery('#plot-stream-limit').hide();
                        }
                    }

                }
            });
        })
    });

</script>
<ul id="plot-stream-list">
    <?php if ($this->stream) { echo $this->stream; } ?>
</ul>
<?php if ($this->stream) { ?>
    <button id="plot-stream-limit" data-stream-limit="<?php echo plotGlobalConfig::getVar('streamDefaultLimit'); ?>">
        <?php echo  JText::_('COM_PLOT_SHOW_MORE');?>
    </button><br>
<?php } ?>
