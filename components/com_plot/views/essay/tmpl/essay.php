<?php
defined('_JEXEC') or die;
if($this->essay){
$id=(int)$this->essay->id;
}
?>

<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
<link type="text/css" href="<?php echo JUri::root(); ?>templates/plot/css/style.css" rel="stylesheet">

<script src="<?php echo JUri::root(); ?>media/jui/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>templates/plot/js/jquery-ui.min.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/js/jquery.jscrollpane.min.js"></script>

<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/plot/js/svgsprite.js"></script>
<script type="text/javascript"  src="<?php echo JUri::root(); ?>components/com_plot/assets/js/plotEssay.js"></script>

<script type="text/javascript" src="<?php echo JUri::root(); ?>media/system/js/mootools-core.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>media/system/js/core.js"></script>

<script type="text/javascript" src="<?php echo JUri::root(); ?>media/system/js/mootools-more.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>media/system/js/modal.js"></script>
<script src="<?php echo JUri::root(); ?>templates/plot/js/jplot.js" type="text/javascript"></script>
<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('body').addClass('popup-style');
        jQuery('#essay-img').jScrollPane();

    });

    function acceptEssay(){
        jQuery('.pre-loader').css('opacity', '1');
        jQuery.post(
            'index.php?option=com_plot&task=essay.approve',
            {id: '<?php echo $id; ?>'},
            function (response) {
                createPopup(response.message);
                jQuery('.pre-loader').css('opacity', '0');
            }
        );
    }

    function rejectEssay(){
        var msg=jQuery('#essayreason-text').val();
        msg.trim();
        if(msg ){
            jQuery('.pre-loader').css('opacity', '1');
            jQuery.post(
                'index.php?option=com_plot&task=essay.reject',
                {id: '<?php echo $id; ?>',
                msg:msg.trim()},
                function (response) {
                    createPopup(response.message);
                    jQuery('.pre-loader').css('opacity', '0');
                }
            );
        }else{
            createPopup2('Заполните поле');
        }

    }

    function createPopup(msg){
        var overlay='<div class="avatar-overlay" id="sbox-overlay2" aria-hidden="false" tabindex="-1"></div>',
            str='<div id="sbox-window2" role="dialog" aria-hidden="false" class="shadow avatar-sbox">' +
                '<div id="sbox-content2" class="sbox-content-adopt" style="opacity: 1;"><div id="enqueued-message">' + msg+
                '</div></div><a id="sbox-btn-close2" href="#" onclick="window.parent.document.getElementById(\'sbox-btn-close\').click();" role="button" aria-controls="sbox-window"></a></div>';
        jQuery("#avatar-box").after(str);
        jQuery("#avatar-box").after(overlay);
    }

    function createPopup2(msg){
        var overlay='<div class="avatar-overlay" id="sbox-overlay2" aria-hidden="false" tabindex="-1"></div>',
            str='<div id="sbox-window2" role="dialog" aria-hidden="false" class="shadow avatar-sbox">'+
                '<div id="sbox-content2" class="sbox-content-adopt" style="opacity: 1;"><div id="enqueued-message">' +msg+
                '</div></div><a id="sbox-btn-close2" href="#" onclick="document.getElementById(\'sbox-overlay2\').remove();document.getElementById(\'sbox-window2\').remove();" role="button" aria-controls="sbox-window"></a></div>';
        jQuery("#avatar-box").after(str);
        jQuery("#avatar-box").after(overlay);
    }

</script>

<?php # </editor-fold> ?>

<div id="message-form" class="child-profile essay-modal">
    <h6>Эссе</h6>
    <div id="essay-img">
        <img src="<?php echo  JUri::root().'media/com_plot/essay/'.$id.'/'.$this->essay->img;?>">
    </div>
    <div id="essy-negative-reason" style="display: none;">
        <textarea id="essayreason-text" placeholder="Заполните поле"></textarea>
        <div class="plot-button-wrapp">
            <button class="button accept hover-shadow" onclick="rejectEssay();"><span><?php echo  JText::_('COM_PLOT_SEND');?></span></button>
            <button class="button cancel-act hover-shadow" onclick="plotEssay.hideElement('essy-negative-reason').showElement('essay-buttons').showElement('essay-img');"><span><?php echo  JText::_('COM_PLOT_CANCEL');?></span></button>
        </div>
    </div>

        <?php if(!$this->essay->status && $this->essay->user_id!=plotUser::factory()->id){ ?>
            <div class="plot-button-wrapp" id="essay-buttons">
                <button class="button accept hover-shadow" onclick="acceptEssay();"><span><?php echo  JText::_('COM_PLOT_ESSAY_ACCEPT');?></span></button>
                <button class="button cancel-act hover-shadow" onclick="plotEssay.hideElement('essay-buttons').hideElement('essay-img').showElement('essy-negative-reason');"><span><?php echo  JText::_('COM_PLOT_ESSAY_REJECT');?></span></button>
            </div>
        <?php } ?>

        <div id="avatar-box"></div>
        <img src="<?php echo JURI::base() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/pre-loader-1.gif" alt="" class="pre-loader" style="display: none;"/>

</div>

