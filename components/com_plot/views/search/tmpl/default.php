<?php
defined('_JEXEC') or die('Restricted access');
?>

<script type="text/javascript" src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate() . '/js/jquery.jscrollpane.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.min.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>/components/com_plot/libraries/scrolling_ajax_pagination/javascript.js"></script>
<script type="text/javascript">
function setActiveTab(tab, search_area) {
    jQuery('#children-btn, #adults-btn, #books-btn, #courses-btn, #events-btn').removeClass('ui-tabs-active');
    jQuery('#child-search-children, #child-search-adults, #child-search-books, #child-search-courses, #child-search-meetings').css({'display': 'none'});
    jQuery('#' + tab).addClass('ui-tabs-active');
    jQuery('#' + tab).addClass('ui-tabs-active');
    jQuery('#' + search_area).css({'display': 'block'});
}

//load children
function setChildrenAxaxScrollPagination() {
    var widthItem = jQuery('#search-all-children li.found-item:first-child').width(),
        heightItem = jQuery('#search-all-children li.found-item:first-child').height(),
        HeightScreen = parseInt(screen.height);
    if (!widthItem) {
        if (HeightScreen <= 768) {
            widthItem = 236;
            heightItem = 72;
        } else if (HeightScreen > 768 && HeightScreen <= 980) {
            widthItem = 294;
            heightItem = 86;
        } else if (HeightScreen > 980) {
            widthItem = 307;
            heightItem = 101;
        }
    }
    setActiveTab('children-btn', 'child-search-children');
    
    var ajaxScrollData = {
        search: '<?php echo $this->search;?>',
        screenHeight: parseInt(screen.height) - parseInt(jQuery('div.parent-profile').height()),
        screenWidth: jQuery('#child-search-children').outerWidth(),
        itemHeight: heightItem,
        itemWidth: widthItem
    };

    jQuery(window).unbind('scroll');

    jQuery('#search-all-children').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('limitSearchAdultsItems'); ?>,
        error: '',
        delay: 1,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=search.ajaxChildrenLoadMore'); ?>',
        appendDataTo: 'search-all-children',
        userData: ajaxScrollData,
        afterLoad: {
            afterLoadAction: function () {
                jPlotUp.Arrow.initialize('positionCameras');
            }
        }
    });
    jQuery('#search-department').html('"Дети"');
    jQuery('#count-result').html(jQuery('#children-count').val());
    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });
    jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
}

//load adults
function setAdultsAxaxScrollPagination() {
    var widthItem = jQuery('#search-all-adults li.found-item:first-child').width(),
        heightItem = jQuery('#search-all-adults li.found-item:first-child').height(),
        HeightScreen = parseInt(screen.height);
    if (!widthItem) {
        if (HeightScreen <= 768) {
            widthItem = 236;
            heightItem = 72;
        } else if (HeightScreen > 768 && HeightScreen <= 980) {
            widthItem = 294;
            heightItem = 86;
        } else if (HeightScreen > 980) {
            widthItem = 307;
            heightItem = 101;
        }
    }
    setActiveTab('adults-btn', 'child-search-adults');
    var ajaxScrollData = {
        search: '<?php echo $this->search;?>',
        screenHeight: parseInt(screen.height) - parseInt(jQuery('div.parent-profile').height()),
        screenWidth: jQuery('#child-search-adults').outerWidth(),
        itemHeight: heightItem,
        itemWidth: widthItem
    };

    jQuery(window).unbind('scroll');

    jQuery('#search-all-adults').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('limitSearchAdultsItems'); ?>,
        error: '',
        delay: 1,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=search.ajaxAdultsLoadMore'); ?>',
        appendDataTo: 'search-all-adults',
        userData: ajaxScrollData,
        afterLoad: {
            afterLoadAction: function () {
                jPlotUp.Arrow.initialize('positionCameras');
            }
        }
    });
    jQuery('#search-department').html('"Взрослые"');
    jQuery('#count-result').html(jQuery('#adults-count').val());

    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });
    jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
}

//load books
function setBooksAxaxScrollPagination() {
    setActiveTab('books-btn', 'child-search-books');
    jQuery('#search-department').html('"Книги"');
    jQuery('#count-result').html(jQuery('#book-count').val());
    var ajaxBooksScrollData = {
        search: '<?php echo $this->search;?>'
    };

    jQuery(window).unbind('scroll');

    jQuery('#child-search-books').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('limitSearchBooksChildrenItems'); ?>,
        error: '',
        delay: 1,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=search.ajaxBooksLoadMore'); ?>',
        appendDataTo: 'child-search-books',
        userData: ajaxBooksScrollData
    });

    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });
    jPlotUp.Arrow.ShowOrHide(this,'positionCameras');

}

//load courses
function setCoursesAxaxScrollPagination() {
    setActiveTab('courses-btn', 'child-search-courses');
    jQuery('#search-department').html('"Курсы"');
    jQuery('#count-result').html(jQuery('#course-count').val());
    var ajaxCoursesScrollData = {
        search: '<?php echo $this->search;?>'
    };

    jQuery(window).unbind('scroll');

    jQuery('#child-search-courses').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('limitSearchCoursesItems'); ?>,
        error: '',
        delay: 1,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=search.ajaxCoursesLoadMore'); ?>',
        appendDataTo: 'child-search-courses',
        userData: ajaxCoursesScrollData,
        afterLoad: {
            afterLoadAction: function () {
                jPlotUp.Arrow.initialize('positionCameras');
            }
        }
    });

    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });
    jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
}

//load events
function setEventsAxaxScrollPagination() {
    setActiveTab('events-btn', 'child-search-meetings');
    jQuery('#search-department').html('"Встречи"');
    jQuery('#count-result').html(jQuery('#event-count').val());
    var ajaxScrollEventsData = {
        search: '<?php echo $this->search;?>'
    };

    jQuery(window).unbind('scroll');

    jQuery('#child-search-meetings').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('limitSearchMeetingsChildrenItems'); ?>,
        error: '',
        delay: 10,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=search.ajaxEventsLoadMore'); ?>',
        appendDataTo: 'child-search-meetings',
        userData: ajaxScrollEventsData,
        afterLoad: {
            afterLoadAction: function () {
                jPlotUp.Arrow.initialize('positionCameras');
            }
        }
    });

    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });
    jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
}

jQuery(document).ready(function (jQuery) {
    jQuery("#plot-search").val('<?php echo $this->search; ?>');

    jQuery('#children-btn').on('click', function () {
        jQuery('#search-all-children').html('');
        setChildrenAxaxScrollPagination();
        jQuery('.main-wrap-overflow').trigger('scroll');
    });
    jQuery('#adults-btn').on('click', function () {
        jQuery('#search-all-adults').html('');
        setAdultsAxaxScrollPagination();
        jQuery('.main-wrap-overflow').trigger('scroll');
    });
    jQuery('#books-btn').on('click', function () {
        jQuery( '#child-search-books' ).html('');
        setBooksAxaxScrollPagination();
        jQuery('.main-wrap-overflow').trigger('scroll');
    });
    jQuery('#courses-btn').on('click', function () {
        jQuery( '#child-search-courses' ).html('');
        setCoursesAxaxScrollPagination();
        jQuery('.main-wrap-overflow').trigger('scroll');
    });
    jQuery('#events-btn').on('click', function () {
        jQuery( '#child-search-meetings' ).html('');
        setEventsAxaxScrollPagination();
        jQuery('.main-wrap-overflow').trigger('scroll');
    });

    <?php if ($this->active == 'events') { ?>
    setActiveTab('events-btn', 'child-search-meetings');
    jQuery('#events-btn').trigger('click');
    <?php } else { ?>
        console.log('#<?php echo $this->active;?>-btn');
    jQuery('#<?php echo $this->active;?>-btn').addClass('ui-tabs-active');
    jQuery('#child-search-<?php echo $this->active;?>').css({'display': 'block'});
    jQuery('#<?php echo $this->active;?>-btn').trigger('click');
    <?php } ?>

    jQuery('.main-wrap-overflow').trigger('scroll');

});


document.addEventListener("touchmove", function(){
    jQuery('.main-wrap-overflow').trigger('scroll');
    console.log('ScrollStart');
}, false);

</script>

<main class="child-profile">

    <?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_header')); ?>

    <input type="hidden" id="adults-count" value="<?php echo PlotHelper::declension((int) $this->items['countAdults'], array('результат', 'результата', 'результатов')); ?>" />
    <input type="hidden" id="children-count" value="<?php echo PlotHelper::declension((int) $this->items['countChildren'], array('результат', 'результата', 'результатов')); ?>" />
    <input type="hidden" id="event-count" value="<?php echo PlotHelper::declension((int) $this->items['countEvents'], array('результат', 'результата', 'результатов')); ?>" />
    <input type="hidden" id="book-count" value="<?php echo PlotHelper::declension((int) $this->items['countBooks'], array('результат', 'результата', 'результатов')); ?>" />
    <input type="hidden" id="course-count" value="<?php echo PlotHelper::declension((int) $this->items['countCourses'], array('результат', 'результата', 'результатов')); ?>" />

    <div class="wrap main-wrap child-search">
        <div class="add-left aside-left child-search"></div>
        <div class="add-right aside-right child-search"></div>
        <section class="child-search">
            <?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_top_menu')); ?>
            <div id="child-search">
                <div class="info-board">
                    <svg viewBox="0 0 37.1 38.9" preserveAspectRatio="xMinYMin meet" class="leaf"><use xlink:href="#leaf"></use></svg>
                    <ul class="child-search-tabs">
                        <li id="children-btn"><a href="javascript:void(0);">Дети</a></li>
                        <li id="adults-btn"><a href="javascript:void(0);">Взрослые</a></li>
                        <li id="books-btn"><a href="javascript:void(0);">Книги</a></li>
                        <li id="courses-btn"><a href="javascript:void(0);">Курсы</a></li>
                        <li id="events-btn"><a href="javascript:void(0);">Встречи</a></li>
                    </ul>
                    <p class="search-result info-bottom">
                        Результат поиска раздела <span id="search-department">"Дети"</span> - по запросу <span>"<?php echo $this->search; ?>" </span>
                        найдено <span id="count-result"><?php echo PlotHelper::declension((int) $this->items['countChildren'], array('результат', 'результата', 'результатов')); ?></span>
                    </p>
                </div>
                <div id="loading-bar"><img src="<?php echo $this->baseurl ?>/templates/plot/img/pre-loader-1.gif" /></div>
                <div id="child-search-children" class="jcarousel-wrapper search-children child-profile">
                    <ul class="child-profile no-jcarousel search-children" id="search-all-children"></ul>
                </div>
                <div id="child-search-adults" class="jcarousel-wrapper search-adults child-profile">
                    <ul class="child-profile no-jcarousel search-adults" id="search-all-adults"></ul>
                </div>
                <div id="child-search-books"></div>
                <div id="child-search-courses"></div>
                <div id="child-search-meetings"></div>
            </div>
        </section>
    </div>
</main>

