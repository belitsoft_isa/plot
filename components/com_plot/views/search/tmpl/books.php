<?php
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.tooltip');
?>

<script type="text/javascript">
jQuery(document).ready(function () {
    jQuery('[data-toggle="tooltip"]').tooltip();
});
</script>

<input type="hidden" name="task" value=""/>
<input type="hidden" name="boxchecked" value="0"/>

<?php echo JHtml::_('form.token'); ?>

<?php if ($this->items) { foreach ($this->items as $i => $item) { ?>
    <div>
        <span><?php echo $item->c_title; ?></span><br>
        <img style="float:left;" src="<?php echo JURI::root().'media/com_html5flippingbook/thumbs/'.$item->c_thumb; ?>">
        <?php echo $item->c_pub_descr ?><br>
    </div>
<?php } } ?>


