<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewSearch extends JViewLegacy
{

    function display($tpl = null)
    {
        $this->state = $this->get('State');
        $app = JFactory::getApplication();
        $this->templateUrl = JURI::base().'templates/'.$app->getTemplate();
        $this->search = urldecode(JRequest::getString('search', ''));
        $jinput = JFactory::getApplication()->input;
        $layout = $jinput->get('layout', '');

        if (!$layout) {
            $this->items = $this->get('AllResults');
        } else {
            $this->items = $this->get('Items');
        }
        
        $this->active = 'children';
        if (JRequest::getInt('books')) {
            $this->active = 'books';
        }
        if (JRequest::getInt('adults')) {
            $this->active = 'adults';
        }
        if (JRequest::getInt('courses')) {
            $this->active = 'courses';
        }
        if (JRequest::getInt('events')) {
            $this->active = 'events';
        }
        
        $this->pagination = $this->get('Pagination');
        parent::display($tpl);
    }

}
