<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewUserportfolios extends JViewLegacy
{



    function display($tpl = null)
    {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }
        $this->user=$user;
        $this->state = $this->get('State');
        $this->items = $this->get('Items');

        $this->pagination = $this->get('Pagination');


        parent::display($tpl);
        $this->setDocument();
    }



    protected function setDocument()
    {
        $document = JFactory::getDocument();

    }

}
