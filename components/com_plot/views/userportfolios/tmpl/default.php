<?php
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');


$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$sortFields = array(
    'a.id' => JText::_('COM_PLOT_ID'),
    'a.title' => JText::_('COM_PLOT_PORTFOLIO_TITLE'),
    'a.link' => JText::_('COM_PLOT_PORTFOLIO_LINK')
);
$sortedByOrder = ($listOrder == 'a.ordering');


?>


<form method="post" name="adminForm" id="adminForm" action="<?php echo 'index.php?option=com_plot&view=userportfolios'; ?>" method="post" autocomplete="off">

    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
    <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
    <?php echo JHtml::_('form.token'); ?>


   <?php if($this->items){
        foreach($this->items AS $item){
            echo '<br>'.$item->title.'<br>';
            if($item->img){ ?>
                <img
                    src="<?php echo  JURI::root() . '/images/com_plot/portfolio/' . $this->user->id . '/' . $item->img; ?>">
                <br>
          <?  }
            echo $item->description.'<br>';?>
          <a href="<?php echo $item->link; ?>"><?php echo $item->link; ?></a><br>
        <?php }
    }else{
       echo  JText::_('COM_PLOT_USER_DO_NOT_HAVE_PORTFOLIO');
   }
    ?>
 </form>

