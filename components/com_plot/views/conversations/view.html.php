<?php

defined('_JEXEC') or die;
require_once JPATH_COMPONENT . '/views/view.php';
require_once(JPATH_SITE . '/administrator/components/com_html5flippingbook/models/configuration.php');
require_once(JPATH_SITE . '/administrator/components/com_html5flippingbook/libs/VarsHelper.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/conversations.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/friends.php');

class PlotViewConversations extends JViewLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'plot';

    public function display($tpl = null)
    {
        $app = JFactory::getApplication();

        // We know for user that the guest cannot access conversations.
        Foundry::requireLogin();
        // Load the conversation model.
        $model = Foundry::model('Conversations');

        $my = Foundry::user();
        $this->config = Foundry::config();
        $jinput = JFactory::getApplication()->input;
        $layout = $jinput->get('layout', '');
        $Itemid = $jinput->get('Itemid', 0,'INT');
        if ($layout) {
        $this->read();
        } else {
            $options = array(
                'sorting' => 'created', //$this->themeConfig->get( 'conversation_sorting' ),
                'ordering' => 'desc', //$this->themeConfig->get( 'conversation_ordering' ),
                'limit' => 20, //$this->themeConfig->get( 'conversation_limit' )
                'maxlimit'=>10
            );


            $filter = JRequest::getWord('filter', '');
            if ($filter == 'all') {
                $filter = '';
            }

            if ($filter) {
                $options['filter'] = $filter;
            }


            $this->state = $this->get('State');

            $this->pagination = $this->get('Pagination');
           // $this->conversations = $model->getConversations($my->id, $options);

            $this->conversations=$this->get('Items');

            // Set the page title
            $title = JText::_('COM_EASYSOCIAL_PAGE_TITLE_CONVERSATIONS_INBOX');

            Foundry::page()->title($title);

            // Set breadcrumbs
            Foundry::page()->breadcrumb($title);

            //$this->pagination = $pagination;
            $filtres=$this->get('Statuses');
            JHtmlSidebar::addFilter(
                JText::_('COM_PLOT_CONVERSATIONS_FILTER'),
                'filter_filter',
                JHtml::_('select.options',  $filtres, 'value', 'text', $this->state->get('filter.filter'))
            );

            $this->sidebar = JHtmlSidebar::render();
            $this->newMessages=$this->get('CountNewMessages');
            $this->filter = $filter;
            $this->my=$my;
            $this->Itemid=$Itemid;
            return parent::display($tpl);
        }


    }

    protected function prepareDocument()
    {
        parent::prepareDocument();
        parent::addFeed();
    }


    public function read()
    {
        $tpl = null;
        // Prevent unauthorized access.
        Foundry::requireLogin();

        $id = JRequest::getInt('id');
        $my = Foundry::user();

        $conversation = Foundry::table('Conversation');
        $loaded = $conversation->load($id);
        $info = Foundry::info();

        // Check if the conversation id provided is valid.
        if (!$id || !$loaded) {
            Foundry::info()->set(JText::_('COM_EASYSOCIAL_CONVERSATIONS_ERROR_INVALID_ID'), SOCIAL_MSG_ERROR);

            $url = FRoute::conversations(array(), false);

            return $this->redirect($url);
        }

        // Check if the user has access to read this discussion.
        if (!$conversation->isReadable($my->id)) {
            Foundry::info()->set(JText::_('COM_EASYSOCIAL_CONVERSATIONS_NOT_ALLOWED_TO_READ'), SOCIAL_MSG_ERROR);

            $url = FRoute::conversations(array(), false);

            return $this->redirect($url);
        }

        // Retrieve conversations model.
        $model = Foundry::model('Conversations');

        // always reset the limistart to 0 so that when the page refresh, system will not get the 'previous' saved limitstart.
        $model->setState('limitstart', 0);

        // Get list of files in this conversation
        $filesModel = Foundry::model('Files');

        // Get a list of all the message ids from this conversation.
        $files = $filesModel->getFiles($model->getMessageIds($conversation->id), SOCIAL_TYPE_CONVERSATIONS);

        // Push the files to the template.
        $this->set('files', $files);

        // Get a list of participants for this particular conversation except myself.
        $participants = $model->getParticipants($conversation->id);

        // Fetch a list of messages for this particular conversation
        $messages = $model->setLimit(5)->getMessages($conversation->id, $my->id);

        $participantNames = Foundry::string()->namesToStream($participants, false, 3, false);

        $title = JText::sprintf('COM_EASYSOCIAL_PAGE_TITLE_CONVERSATIONS_READ', $participantNames);

        // Set title
        Foundry::page()->title($title);

        // Set breadcrumbs
        Foundry::page()->breadcrumb(JText::_('COM_EASYSOCIAL_PAGE_TITLE_CONVERSATIONS_INBOX'), FRoute::conversations());
        Foundry::page()->breadcrumb($title);

        // @trigger: onPrepareConversations
        $dispatcher = Foundry::dispatcher();
        $args = array(&$messages);

        $dispatcher->trigger(SOCIAL_TYPE_USER, 'onPrepareConversations', $args);

        // Get pagination
        $pagination = $model->getPagination();

        // Determine if load previous messages should appear.
        $loadPrevious = $pagination->total > $pagination->limit;

        // Mark conversation as read because the viewer is already reading the conversation.
        $conversation->markAsRead($my->id);

        // @points: conversation.read
        // Assign points when user reads a conversation
        $points = Foundry::points();
        $points->assign('conversation.read', 'com_easysocial', $my->id);

        $this->loadPrevious=$loadPrevious;

        $this->conversation=$conversation;
        $this->participants=$participants;
        $this->messages=$messages;
        $this->pagination=$pagination;
        $this->my=$my;

        return parent::display($tpl);
    }

}
