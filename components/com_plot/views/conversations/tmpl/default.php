<?php
defined('_JEXEC') or die;
?>
<style>
    body{
        color: black;
    }
</style>
<form name="adminForm" id="adminForm" action="<?php echo 'index.php?option=com_plot&view=conversations'; ?>"
      method="post" autocomplete="off">
    <input type="hidden" name="task" value=""/>
    <input type="hidden" name="boxchecked" value="0"/>
    <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
    <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>

    <?php echo JHtml::_('form.token'); ?>
    <?php echo $this->sidebar; ?>
    <a href="javascript: void(0)" onclick="jQuery('#filter_filter').val('unread'); jQuery('#adminForm').submit();">
        <?php echo JText::_('COM_PLOT_COUNT_NEW_MESSAGES'); ?> <?php echo $this->newMessages ?></a>

    <div data-conversations-content>
        <?php
        if ($this->conversations) {
            ?>
            <ul>
                <?php
                foreach ($this->conversations as $conversation) {

                    ?>
                    <li>
                        <p data-conversation-id="<?php echo $conversation->id; ?>">
                            <a href="<?php echo JRoute::_('index.php?option=com_plot&view=conversations&layout=read&id=' . $conversation->id . '&Itemid=' . $this->Itemid); ?>"><?php echo Foundry::user($conversation->created_by)->name; ?></a>
                        </p>

                        <div><?php echo $conversation->message; ?></div>
                        <p><?php echo $conversation->created; ?></p>
                    </li>
                <?php } ?>
            </ul>
        <?php
        } else {
            ?>
            <?php echo JText::_('COM_PLOT_MESSAGESS_NOT_FOUND'); ?>

        <?php
        }

        ?>

    </div>
    <?php echo $this->pagination->getLimitBox(); ?>
    <?php echo $this->pagination->getListFooter(); ?>
</form>

