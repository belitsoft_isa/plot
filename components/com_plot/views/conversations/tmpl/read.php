<?php
defined( '_JEXEC' ) or die( 'Unauthorized Access' );
?>
<div class="es-container" data-readConversation data-id="<?php echo $conversation->id;?>">


</div>
<style>
    body{
        color: black;
    }
</style>
<script>
    function checkForm(){
        if(jQuery('#plot-message').val()!=''){
            jQuery('#plot-form').submit();
        }else{
            alert('<?php echo JText::_("COM_PLOT_WRITE_TEXT")?>');
        }
    }
</script>
<div class="es-content">

    <div class="row-fluid">

        <?php

        if( $this->loadPrevious ){ ?>

            <div class="center">

                <i class="loading-indicator small"></i>

                <a  href="javascript:void(0);"
                    class="btn btn-xblock btn-link"
                    data-readconversation-load-more
                    data-id="<?php echo $this->conversation->id; ?>"
                    data-limitstart="<?php echo $this->pagination->limit; ?>"
                    >
                   <i class="ies-arrow-up ies-small"></i>
                </a>
            </div>
        <?php }?>

        <ul class="unstyled conversation-messages" data-conversationMessages data-readConversation-items>

            <?php if( $this->messages ){ ?>
                <?php
               $curDay = '';

                foreach( $this->messages as $message ){
                    if( $curDay != $message->day )
                    {
                        $curDay = $message->day;
                        $date 	= Foundry::date( $message->created );

                        $dateText = ( $message->day > 0 ) ? $date->toFormat( 'F d Y' ) : JText::_('COM_EASYSOCIAL_CONVERSATIONS_TODAY');

                        ?>

                        <li class="conversation-date">
                            <span class="conversation-timestamp"><?php echo $dateText; ?></span>
                        </li>
                    <?php } ?>
                    <div class="media">

                        <div class="media-object">
                            <div class="es-avatar-wrap">
                                <a href="

                                <?php
                                if($message->getCreator()->isBlock()){
                                  echo  'javascript:void(0);';
                                }else{
                                    echo JRoute::_('index.php?option=com_plot&view=profile&id='.$message->created_by);

                                }
                                ?>
                                ">
                                    <img src="<?php echo $message->getCreator()->plotGetAvatar();?>" class="es-avatar es-avatar-small es-borderless" />
                                </a>

                            </div>
                        </div>
                     <div class="media-body">
                            <div class="row-fluid">
                                <div class="message-user-name">
                                    <?php if( !$message->getCreator()->isBlock() ) { ?>
                                        <a href="<?php echo $message->getCreator()->getPermalink();?>"><?php echo $message->getCreator()->getStreamName();?></a>
                                    <?php } else { ?>
                                        <?php echo $message->getCreator()->getStreamName();?>
                                    <?php } ?>
                                </div>
                                <div class="message-time-wrap">
					<span class="message-time">
						<?php
                        $msgDate 		= Foundry::date( $message->created );
                        $msgDateText 	= ( $message->day > 0 ) ? $msgDate->toFormat('H:i a') : $msgDate->toLapsed();
                        $msgDateTitle 	= ( $message->day > 0 ) ? $msgDate->toLapsed() . JText::_('COM_EASYSOCIAL_CONVERSATIONS_AT') . $msgDateText : $msgDateText;

                        ?>
                        <time title="<?php echo $msgDateTitle; ?>">
                            <i class="ies-clock-2 ies-small"></i> <?php echo $msgDateText; ?>
                        </time>
					</span>
                                </div>
                            </div>

                            <div class="mail-content mt-10">
                                <p><?php echo $message->getContents();?></p>
                            </div>

                        </div>

                    </div>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>

    <?php  if( $this->conversation->isWritable( $this->my->id ) ){ ?>
        <div class="row-fluid mt-10" data-readConversation-composer>
            <form action="<?php  echo JRoute::_('index.php?option=com_plot&view=conversations&task=conversations.reply');?>" enctype="multipart/form-data" method="post" id="plot-form">

                <div class="reply-form mb-20">
                    <h5>
                        <i class="icon-es-chatgroup"></i>
                        <?php echo JText::_( 'COM_EASYSOCIAL_CONVERSATIONS_REPLY_TITLE' );?>
                    </h5>
                    <hr />
                    <div data-readConversation-replyNotice></div>

                    <div class="composer-textarea">
                        <textarea id="plot-message" class="input-shape" name="message" placeholder="<?php echo JText::_( 'COM_EASYSOCIAL_CONVERSATIONS_WRITE_YOUR_MESSAGE_HERE' , true ); ?>" data-composer-editor></textarea>
                    </div>
                </div>

                <div class="form-actions">
                    <div class="row-fluid">
                        <div class="pull-right">
                            <input type="button" onclick="checkForm();" value="<?php echo JText::_( 'COM_EASYSOCIAL_SUBMIT_BUTTON' );?>" >
                        </div>
                    </div>
                </div>

                <input type="hidden" name="id" value="<?php echo $this->conversation->id; ?>" />

                <?php echo JHTML::_( 'form.token' ); ?>
            </form>
        </div>
    <?php } else { ?>
        <div class="small">
            <?php echo JText::_( 'COM_EASYSOCIAL_CONVERSATIONS_YOU_CANNOT_REPLY_TO_THIS_CONVERSATION' );?>
        </div>

    <?php }  ?>





</div>