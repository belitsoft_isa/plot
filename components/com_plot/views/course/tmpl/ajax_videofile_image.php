<?php
defined('_JEXEC') or die;
$proportion=plotGlobalConfig::getVar('meetingCropWidthMin')/plotGlobalConfig::getVar('meetingCropHeightMin');
 ?>

<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />

<link rel="stylesheet" type="text/css" href="css/style.css" />
<link type="text/css" href="<?php echo JUri::root(); ?>templates/plot/css/style.css" rel="stylesheet">
<link type="text/css" href="<?php echo JUri::root(); ?>components/com_plot/libraries/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet">
<script src="<?php echo JUri::root(); ?>media/jui/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/jui/js/jquery.ui.core.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo JURI::root(); ?>media/jui/js/jquery.ui.core.min.js"></script>

<script src="<?php echo JUri::root(); ?>components/com_plot/libraries/jcrop/js/jquery.Jcrop.js" type="text/javascript"></script>

<script type="text/javascript">

    function showCoords(coords)
    {
        jQuery('input[name=x]').val(coords.x);
        jQuery('input[name=y]').val(coords.y);
        jQuery('input[name=x2]').val(coords.x2);
        jQuery('input[name=y2]').val(coords.y2);
        jQuery('input[name=w]').val(coords.w);
        jQuery('input[name=h]').val(coords.h);
    };

    function saveCroppedAvatar()
    {
        jQuery('input[name=img-width]').val(jQuery('.jcrop-holder img').width());
        jQuery('input[name=img-height]').val(jQuery('.jcrop-holder img').height());

        jQuery.post('<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxSaveCroppedVideoFileImage');?>',{data: jQuery('#crop-image').serialize()}, function(error){
            var data;
            if (jQuery.parseJSON(error).img) {
                data=jQuery.parseJSON(error).img,
                img = new Image();
                img.src = data;
                window.parent.document.getElementById('videofile-image-area').innerHTML='<img src="'+  data+'" />';
                jQuery(window.parent.document.getElementById('video-file-close-button')).show();
                window.parent.SqueezeBox.close();
            } else {
                alert('<?php echo JText::_('COM_PLOT_ERROR');?>');
                window.parent.SqueezeBox.close();
            }
        });
    }

    function goAboutMe()
    {
        window.parent.SqueezeBox.close();
    }

    jQuery(document).ready(function(){
        hidePlotDinamicalyLoading();
        jQuery('.jcrop').Jcrop({
            minSize:   [ <?php echo plotGlobalConfig::getVar('meetingCropWidthMin'); ?>, <?php echo plotGlobalConfig::getVar('meetingCropHeightMin'); ?> ],
            setSelect:   [ 0, 0, <?php echo plotGlobalConfig::getVar('meetingCropWidthMin'); ?>, <?php echo plotGlobalConfig::getVar('meetingCropHeightMin'); ?> ],
            allowSelect: true,
            onChange: showCoords,
            onSelect: showCoords,
            allowRelese:false,
            minSelect:[<?php echo plotGlobalConfig::getVar('meetingCropWidthMin'); ?>, <?php echo plotGlobalConfig::getVar('meetingCropHeightMin'); ?>],
            aspectRatio: <?php echo $proportion; ?>
        });

        jQuery('input[name=img-width]').val(jQuery('.jcrop-holder img').width());
        jQuery('input[name=img-height]').val(jQuery('.jcrop-holder img').height());

        jQuery('body').addClass('popup-style');
    });


    function hidePlotDinamicalyLoading(){
        var element = window.parent.document.getElementById('plot-dinamicaly-loading');
        if(element){
            jQuery(element).css({'display':'none', 'z-index':'0'});
        }
        jQuery('#avatar-crop').show();
    }
</script>

<style type="text/css">
    #avatar-crop {
        background-color: #fff;
        border-radius: 13px;
        margin: 1px;
        padding: 10px 20px;
    }
    #avatar-crop img{
        max-width: 500px;
        max-height: 500px;
    }
    #avatar-crop form input {
        margin-right: 10px;
        font-size: 15px !important;
    }
    #avatar-crop h6 {
        font-size: 18px;
    }
    #about-me-container #tabs {
        border: none;
    }
    #about-me-container {
        top: -1px;
    }
</style>

<div id="about-me-container" class="wrap">
    <div id="tabs">

       <div id="avatar-crop" style="display: none;">
            <h6 class="image-preview">Создай превью фотографии с помощью рамки:</h6>
            <img class="jcrop" src="<?php echo $this->originalPhotoUrl;?>" />

            <form method="POST" id="crop-image" action="<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxSaveCroppedVideoFileImage');?>" >
                <input class="ui-selectmenu-button add hover-shadow" type="button" value="Сохранить" onclick="saveCroppedAvatar();" />
                <input class="ui-selectmenu-button no-friend hover-shadow" type="button" value="Отмена" onclick="goAboutMe();" />
                <input type="hidden" name="x" value="" id="imgx" />
                <input type="hidden" name="y" value="" id="imgy"/>
                <input type="hidden" name="x2" value="" id="imgx2"/>
                <input type="hidden" name="y2" value="" id="imgy2"/>
                <input type="hidden" name="w" value="" id="imgw"/>
                <input type="hidden" name="h" value="" id="imgh"/>
                <input type="hidden" name="img_src" value="<?php echo $this->photoUrl;?>">
                <input type="hidden" name="img-width" value="" />
                <input type="hidden" name="img-height" value="" />

            </form>
        </div>
    </div>
</div>
