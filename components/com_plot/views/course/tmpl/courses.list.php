<?php
defined('_JEXEC') or die;

# Категория: echo $course->c_category;
# Мин стоимость: echo $course->total_min_cost;
# Найдено всего: echo count($this->courses);

?>
<?php /* foreach ($this->coursesGroups AS $coursesGroup) { ?>
    <div class="jcarousel-wrapper my-courses child-courses">
        <div class="jcarousel">
            <ul>
                <?php foreach ($coursesGroup as $course) { ?>
                    <li>
                        <a href="<?php echo JRoute::_("index.php?option=com_plot&view=course&id=$course->id"); ?>">
                            <figure>
                                <img src="<?php echo JUri::root().$course->image; ?>" />
                                <figcaption><?php echo $course->course_name; ?></figcaption>
                            </figure>
                        </a>
                        <div class="elem-descr">
                            <?php echo PlotHelper::cropStr(strip_tags($course->course_description), plotGlobalConfig::getVar('coursesDescriptionMaxSymbolsToShow')) ; ?>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
<?php }*/ ?>

<?php foreach ($this->coursesGroups AS $coursesGroup) { ?>
    <div class="jcarousel-wrapper my-courses child-courses">
        <div class="jcarousel">
            <ul>
                <?php foreach ($coursesGroup as $course) { ?>
                    <li>
                        <a href="<?php echo JRoute::_("index.php?option=com_plot&view=course&id=$course->id"); ?>">
                            <figure>
                                <img src="<?php echo JUri::root().$course->image; ?>" />
                                <figcaption><?php echo $course->course_name; ?></figcaption>
                            </figure>
                        </a>
                        <div class="elem-descr">
                            <?php echo PlotHelper::cropStr(strip_tags($course->course_description), plotGlobalConfig::getVar('coursesDescriptionMaxSymbolsToShow')) ; ?>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
<?php } ?>


