<?php
defined('_JEXEC') or die;

if ($this->videos['items']) { ?>

    <?php foreach ($this->videos['items'] AS $key => $video) { ?>
        <li>
            <?php
            if($video->reviewer==plotUser::factory()->id){
            ?>

                <a href="javascript:void(0);" onclick="SqueezeBox.open('<?php echo  JRoute::_("index.php?option=com_plot&task=course.ajaxCourse"); ?>&id=<?php echo  $video->id; ?>', {size:{x:744, y:744}, handler:'iframe'})">

                <?php  }else{
                ?>
                    <?php
                    if($video->type=="link"){
                    if (strripos($video->link, 'youtube.com/') !== FALSE) {
                        $youtubeNumber = substr($video->link, strripos($video->link, '?v=') + 3);
                        if (($pos = strpos($youtubeNumber, '&')) !== FALSE) {
                            $youtubeNumber = substr($youtubeNumber, 0, $pos);
                        }
                    } ?>
                    <a class="fancybox" data-fancybox-type="ajax" rel="videos-set"
                       data-title="<?php echo $video->title; ?>" data-description="<?php echo $video->text; ?>"
                       href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxOpenVideo&youtubeNumber=' . $youtubeNumber);?>"   >
                        <?php
                    }else{
?>

                                <a class="fancybox" data-fancybox-type="ajax" rel="videos-set"
                                   data-title="<?php echo $video->title; ?>" data-description="<?php echo $video->description; ?>"
                                   href="<?php echo JRoute::_('index.php?option=com_plot&task=course.openVideoFile&videoId=' . $video->id);?>"   >
                        <?php
                    }
                    ?>
                <?php
                    }
                    ?>
                        <h6>
                            <i><svg viewBox="0 0 24.5 26.4" preserveAspectRatio="xMidYMid meet"><use xlink:href="#photo"></use></svg></i>
                            Добавил:
                            <p class="stream-title"><?php echo plotUser::factory($video->user_id)->name; ?></p>
                        </h6>
                        <div>
                            <img src="<?php echo $video->img; ?>"/>
                            <i class="activity-icons">
                                <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet" style="fill:url(#svg-gradient);">
                                    <use xlink:href="#play"></use>
                                </svg>
                            </i>
                        </div>
                        <?php if ($video->text) { ?>
                            <div>
                                <?php echo PlotHelper::cropStr(strip_tags($video->text), plotGlobalConfig::getVar('booksDescriptionMaxSymbolsToShow')); ?>
                            </div>
                        <?php } ?>
                    </a>


        </li>
    <?php } ?>

<?php } ?>

