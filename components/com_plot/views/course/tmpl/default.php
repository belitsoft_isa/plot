<?php
defined('_JEXEC') or die;
$id = (int)$this->course->id;

?>


<script type="text/javascript" src="<?php echo JUri::root() . 'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo JUri::root() . 'components/com_plot/libraries/jquery_input_mask_plugin/jquery.mask.min.js'; ?>"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/mootools-core.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/core.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/mootools-more.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/modal.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>components/com_plot/assets/js/plot.js"></script>
<script
    src="<?php echo JURI::base() . 'components/com_plot'; ?>/libraries/scrolling_ajax_pagination/javascript.js"></script>
<script type="text/javascript"
        src="<?php echo JUri::root(); ?>components/com_plot/views/course/tmpl/default.js"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.min.js"></script>
<script type="text/javascript" src="<?php echo $this->templateUrl; ?>/js/jcarousel.basic.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>components/com_plot/assets/js/plotEssay.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>templates/plot/js/jquery_simple_progressbar.js"></script>
<script type="text/javascript"
        src="<?php echo JUri::root(); ?>components/com_plot/assets/js/jquery.fancybox.js"></script>
<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">

function isSmall(){

   return <?php echo $this->courseObj->isSmall(); ?>

}

    function calculateAmounts() {
        var countChildren = (jQuery('#choosed-childs').val() ? jQuery('#choosed-childs').val().length : 0) + (jQuery('#choosed-friends').val() ? jQuery('#choosed-friends').val().length : 0);
        var amountForEachChildFriend = jQuery('#amount-for-each-child').val();

        var totalCostByPercents = 100 * amountForEachChildFriend / (100 - <?php echo $this->course->admin_cost + $this->course->author_cost; ?>);
        var adminCost = totalCostByPercents * <?php echo $this->course->admin_cost ? $this->course->admin_cost : 0 ; ?> / 100;
        var authorCost = totalCostByPercents * <?php echo $this->course->author_cost ? $this->course->author_cost : 0; ?> / 100;

        if (adminCost < <?php echo $this->course->admin_min_cost; ?>) {
            var adminCost = <?php echo $this->course->admin_min_cost; ?>;
        }
        if (adminCost > <?php echo $this->course->admin_max_cost; ?>) {
            var adminCost = <?php echo $this->course->admin_max_cost; ?>;
        }

        if (authorCost < <?php echo $this->course->author_min_cost; ?>) {
            var authorCost = <?php echo $this->course->author_min_cost; ?>;
        }
        if (authorCost > <?php echo $this->course->author_max_cost; ?>) {
            var authorCost = <?php echo $this->course->author_max_cost; ?>;
        }

        jQuery('#displayed_admin_cost').html(adminCost * countChildren);
        jQuery('#displayed_author_cost').html(authorCost * countChildren);
        jQuery('#displayed_prize_cost').html(parseFloat(adminCost) * countChildren + parseFloat(authorCost) * countChildren + parseFloat(amountForEachChildFriend) * countChildren);
    }


    function courseBuy() {
        /* getting */
        var userIdsCourseBuyFor = [];
        jQuery('#course-children-for .users-course-for-list .user-course-for').each(function (i, v) {
            userIdsCourseBuyFor.push(jQuery(this).attr('userid'));
        });

        if (userIdsCourseBuyFor.length) {
            jQuery.post(
                '<?php echo JRoute::_('index.php?option=com_plot&task=course.createOrder'); ?>',
                {
                    courseId: <?php echo $this->course->id; ?>,
                    usersIds: JSON.stringify(userIdsCourseBuyFor),
                    amount: jQuery('#reward').val()
                },
                function (response) {
                    var data = jQuery.parseJSON(response);
                    jQuery('#buy_course_hidden_form [name=userIds]').val(userIdsCourseBuyFor.join('-'));
                    jQuery('#buy_course_hidden_form [name=orderNumber]').val(data.orderId);
                    jQuery('#buy_course_hidden_form [name=rewardAmountForEach]').val(jQuery('#reward').val());
                    jQuery('#buy_course_hidden_form').submit();
                }
            );
        } else {
            SqueezeBox.initialize({
                size: {x: 300, y: 150}
            });
            var str = '<div id="enqueued-message">Не выбраны пользователи, для которых Вы хотите купить даннный курс</div>';
            SqueezeBox.setContent('adopt', str);
            SqueezeBox.resize({x: 300, y: 150});
        }

    }

    function calculateCourseCost() {
       jQuery.post('index.php?option=com_plot&task=course.calculateCosts', {
            courseId: <?php echo $this->course->id; ?>,
            enteredAmount: jQuery('#reward').val(),
            countChilds: jQuery('#course-children-for .users-course-for-list .user-course-for').length
        }, function (response) {
            var data = jQuery.parseJSON(response);
            jQuery('.admin-cost-calculated').html(data.costs.admin);
            jQuery('.author-cost-calculated').html(data.costs.author);
            jQuery('.payment-system-cost-calculated').html(data.costs.paymentSystem);
            jQuery('.total-cost-calculated').html(data.costs.total);
            jQuery('#buy_course_hidden_form [name=sum]').val(data.costs.total);
        });
    }

    function noCoursesFind(){
        jQuery('#videos-list').html('');
        jQuery('#videos-list').html("<div class='no-items'><?php echo JText::sprintf('COM_PLOT_NOT_FOUND_WHAT_COURSES', JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('footerLinkK2idForChildrens'))); ?></div>");
    }

    jQuery(document).ready(function () {


        /* change value for prizes */
        jQuery('#amount-for-each-child').change(function () {
            calculateAmounts();
        });
        jQuery('#amount-for-each-child').keyup(function () {
            calculateAmounts();
        });
        jQuery('#choosed-childs').change(function () {
            calculateAmounts();
        });
        jQuery('#choosed-friends').change(function () {
            calculateAmounts();
        });

    });


function ajaxLoadBefore(callback) {
    var beforeFilterData = {
        bookId: '<?php echo $id; ?>'
    };
    jQuery.post(
        'index.php?option=com_plot&task=course.ajaxGetBeforeCourse',
        {userData: beforeFilterData},
        function (response) {
            if (response) {
                jQuery('#beforecourses-list').html('').show();
                jQuery('#beforecourses-list').append(response);
                callback();
                jQuery('#loading-bar').hide();
            } else {
                jQuery('#loading-bar').hide();
            }
        }
    );


    jQuery('#beforecourses-list').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('coursesResultsShowFirstCount'); ?>,
        offset: <?php echo plotGlobalConfig::getVar('coursesResultsShowFirstCount'); ?>,
        error: '',
        delay: 10,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=course.ajaxGetBeforeCourse'); ?>',
        appendDataTo: 'beforecourses-list',
        userData: beforeFilterData,
        afterLoad: {
            afterLoadAction: function () {
                jPlotUp.Arrow.initialize('positionCameras');

            }
        }
    });

}

function ajaxLoadAfter(callback) {
    var afterFilterData = {
        bookId: '<?php echo $id; ?>'
    };
    jQuery.post(
        'index.php?option=com_plot&task=course.ajaxGetAfterCourse',
        {userData: afterFilterData},
        function (response) {
            if (response) {
                jQuery('#aftercourses-list').html('').show();
                jQuery('#aftercourses-list').append(response);
                callback();
                jQuery('#loading-bar').hide();
            } else {
                jQuery('#loading-bar').hide();
            }
        }
    );

    jQuery('#aftercourses-list').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('coursesResultsShowFirstCount'); ?>,
        offset: <?php echo plotGlobalConfig::getVar('coursesResultsShowFirstCount'); ?>,
        error: '',
        delay: 10,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=course.ajaxGetAfterCourse'); ?>',
        appendDataTo: 'aftercourses-list',
        userData: afterFilterData,
        afterLoad: {
            afterLoadAction: function () {
                jPlotUp.Arrow.initialize('positionCameras');
            }
        }
    });

}

function ajaxLoadVideos(callback) {
    var videosFilterData = {
        courseId: '<?php echo  $id; ?>'
    };
    jQuery.post(
        'index.php?option=com_plot&task=course.ajaxVideosLoadMore',
        {userData: videosFilterData},
        function (response) {
            if (response) {
                jQuery('#videos-list').html('').show();
                jQuery('#videos-list').append(response);
                callback();
                jQuery('#loading-bar').hide();
            } else {
                jQuery('#loading-bar').hide();
            }
        }
    );
    jQuery('#videos-list').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('childEssayCount'); ?>,
        offset: <?php echo plotGlobalConfig::getVar('childEssayCount'); ?>,
        error: '',
        delay: 10,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=course.ajaxVideosLoadMore'); ?>',
        appendDataTo: 'videos-list',
        userData: videosFilterData,
        afterLoad: {
            afterLoadAction: function () {
                jPlotUp.Arrow.initialize('positionCameras');
                updateFancybox();
            }
        }
    });
}


    function fixFFBugWithFavicon() {
        jQuery('head link[href="<?php echo JUri::root(true);?>/templates/plot/favicon.ico"]').attr('href', '<?php echo JUri::root(true);?>/templates/plot/favicon.ico');
    }


document.addEventListener("touchmove", function(){
    jQuery('body').trigger('scroll');
    console.log('ScrollStart');
}, false);
</script>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="CSS"> ?>
<style type="text/css">
    .more {
        margin: 5px;
    }

    .info-board .course-category {
        cursor: default;
    }

    .child-library .course-image {
        max-width: 230px;
    }

    .child-one-elem .in-progress.new {
        padding-top: 27px;
    }
</style>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="MAIN"> ?>
<main class="child-library">


<?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_header')); ?>

<div class="wrap main-wrap child-library">
<div class="add-left aside-left child-library"></div>
<div class="add-right aside-right child-library"></div>
<section class="child-library">
    <?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_top_menu')); ?>
    <div class="info-board child-library">
        <a href="<?php echo $this->referrerUrl; ?>" class="link-back hover-shadow">
            <?php echo JText::_('COM_PLOT_GO_BACK'); ?>
            <svg viewBox="0 0 22.9 46" preserveAspectRatio="xMinYMin meet" class="butterfly">
                <use xlink:href="#butterfly"></use>
            </svg>
        </a>
        <svg viewBox="0 0 37.1 38.9" preserveAspectRatio="xMinYMin meet" class="leaf">
            <use xlink:href="#leaf"></use>
        </svg>
        <object name="bobr"
                data="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate() . '/'; ?>img/bobr-first-level.svg"
                type="image/svg+xml"></object>
        <div class="info-top">
            <form>
                        <span style="width: 267px;" class="no-before-content ui-selectmenu-button course-category">
                            <span class="ui-selectmenu-text"><?php echo $this->course->c_category; ?></span>
                        </span>
            </form>
        </div>
        <div class="info-bottom">
            <?php if (in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe())){ ?>
            <button
                onclick="window.location.href='<?php echo $this->course->routedLinkToLearningPath; ?>'"
                class="test-button hover-shadow">
                <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet">
                    <use xlink:href="#academic-hat"></use>
                </svg>
                <?php if (in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe($onlyNotFinished = true))) { ?>
                    <?php echo JText::_('COM_PLOT_TO_LEARN_FIRST_TIME'); ?>
                <?php } else { ?>
                    <?php echo JText::_('COM_PLOT_TO_REPEAT'); ?>
                <?php } ?>
            </button>
            <?php }else{
                ?>
                <button
                    <?php if(plotUser::factory()->id){?>
                        onclick="createMsg('<?php echo JText::_("COM_PLOT_THIS_COURSE_NOT_BUY"); ?>'); return false;"
                    <?php }else{
                        ?>
                        onclick="createMsg('<?php echo JText::_("COM_PLOT_LOGIN_FIRST"); ?>'); return false;"
                        <?php
                    }?>

                    class="test-button hover-shadow">
                    <svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet">
                        <use xlink:href="#academic-hat"></use>
                    </svg>
                        <?php echo JText::_('COM_PLOT_TO_LEARN_FIRST_TIME'); ?>
                </button>
            <?php
            } ?>
            <?php if ($this->couresBook && (int)$this->couresBook->c_id && $this->courseObj->isGetPages() && $this->courseObj->id_pub){ ?>
                <button  class="test-button hover-shadow"
                    onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_html5flippingbook&view=publication&id=' . (int)$this->couresBook->c_id . '&fullscreen=1'); ?>'" >
                    <svg viewBox="0 0 57.5 57.1" preserveAspectRatio="xMidYMid meet">
                        <use xlink:href="#test"></use>
                    </svg>
                    <?php echo JText::_("COM_PLOT_COURSE_ABOUT_COURSE");?>
                </button>
            <?php }?>

        </div>
    </div>
</section>
<section class="my-progress child-library one-elem">
<div class="my-progress-wrapper">
    <h3><?php echo $this->course->course_name; ?></h3>
    <div class="jcarousel-wrapper my-courses child-one-elem">
        <div class="main-elem-info-wrap">
        <?php if ($this->couresBook && (int)$this->couresBook->c_id){ ?>

        <a <?php echo ($this->courseObj->isGetPages() && $this->courseObj->id_pub )?'class="thumbnail"':'class="nothumbnail"';?> rel="{handler: 'iframe', size: {x: auto, y:auto}}"
         <?php if($this->courseObj->isGetPages() && $this->courseObj->id_pub){?>
           href="<?php echo JRoute::_("index.php?option=com_html5flippingbook&view=publication&id=" . (int)$this->couresBook->c_id . "&fullscreen=1"); ?>">
        <?php  }else{
             ?>
            href="javascript:void(0)">
             <?php
         }?>


            <?php
            }else{
            if (in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe())){
            ?>
            <a <?php echo ($this->courseObj->isGetPages() && $this->courseObj->id_pub )?'class="thumbnail"':'class="nothumbnail"';?> href="<?php echo $this->course->routedLinkToLearningPath; ?>">
                <?php
                }else{
                ?>
                <a <?php echo ($this->courseObj->isGetPages() && $this->courseObj->id_pub )?'class="thumbnail"':'class="nothumbnail"';?> href="javascript:void(0)">
                    <?php
                    }
                    }  ?>

                    <figure><img class="course-image"
                                 src="<?php echo JUri::root() . $this->course->image; ?>"/></figure>
                    <?php if ($this->my->isCourseNew($this->course->id)) { ?>
                        <i class="in-progress"><br/><?php echo  JText::_('COM_PLOT_COURSE_LERNING_NEW'); ?><br/></i>
                    <?php } elseif (in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe())) { ?>
                        <i class="in-progress">
                                <?php if((int)$this->my->getCoursesCompletePercent($this->course->id)==100){
                                    echo JText::_("COM_PLOT_COURSE_LERNING_ON_100");
                                }else{
                                    ?>
                                    <?php echo  JText::_('COM_PLOT_COURSE_LERNING'); ?>
                                    <b><?php echo $this->my->getCoursesCompletePercent($this->course->id); ?>
                                        %</b>
                                <?php
                                }?>

                            </i>
                    <?php } ?>
        </a>
<!--        --><?php //if (in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe())) { ?>
            <div class="wrapper">
                <div class="sticker">
                    Рейтинг курса:<br/>
                    <p class="bought">Куплено: <?php echo $this->course->getCountPushcases(); ?></p>
                    <p class="studied">Изучено: <?php echo $this->course->getCountSuccesfullyStudied(); ?></p>
                    <div class="clr"></div>
                </div>
            </div>
<!--        --><?php //} ?>



                <?php

               if (in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe($onlyNotFinished = false))) { ?>
                <div class="bonus">
                    <em class="pin-btn green"></em>
                    <em class="pin-btn green"></em>
                    <p><?php  echo  JText::_('COM_PLOT_AFTER_COURSE_FINISH');?></p>
                    <div>
                        <i class="elka">+<?php echo $this->course->count_points; ?>
                            <svg viewBox="0 0 14.6 23.1" preserveAspectRatio="xMinYMin meet">
                                <use xlink:href="#elka-interest"></use>
                            </svg>
                        </i>

                        <p class="finance-info"><?php echo $this->my->getFinishedPriceForMyCourse($this->course->id); ?>
                            <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                                <use xlink:href="#munze"></use>
                            </svg>
                        </p>
                    </div>
                <?php

                if (in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe()) && !$this->my->isCourseTestFinished($this->course->id)) {

                    if($this->courseObj->isSmall()){
                        ?>
                        <div class="weiter-schritt">
                            <?php echo JText::sprintf('COM_PLOT_SMALL_COURSE_BUY_BUT_TEST_NOT_FINISHED', '<a class="modal" rel="{handler: \'iframe\', size: {x: 744, y: 469}}" href="'.JRoute::_("index.php?option=com_plot&view=wallet", false).'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>
                        </div>
                    <?php
                    }else{
                        ?>
                        <div class="weiter-schritt">
                            <?php echo JText::sprintf('COM_PLOT_COURSE_BUY_BUT_TEST_NOT_FINISHED', '<a class="modal" rel="{handler: \'iframe\', size: {x: 744, y: 469}}" href="'.JRoute::_("index.php?option=com_plot&view=wallet", false).'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>
                        </div>
                    <?php
                    }
                }else{

                    if ($this->courseObj->isSmall()) {
                        ?>
                        <div class="weiter-schritt">
                            <?php echo JText::sprintf('COM_PLOT_SMALL_COURSE_BUY_BUT_TEST_FINISHED', '<a class="modal" rel="{handler: \'iframe\', size: {x: 744, y: 469}}" href="'.JRoute::_("index.php?option=com_plot&view=wallet", false).'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>
                        </div>
                    <?php
                    } else {

                        if ($this->my->isCourseVideoAdd($this->course->id) && (int)$this->my->courseVideoApproved($this->course->id)) {
                            ?>
                            <div class="weiter-schritt">
                                <?php echo JText::sprintf('COM_PLOT_COURSE_BUY_AND_TEST_AND_VIDEO_FINISHED', '<a class="modal" rel="{handler: \'iframe\', size: {x: 744, y: 469}}" href="'.JRoute::_("index.php?option=com_plot&view=wallet", false).'">'.JText::_('COM_PLOT_WALLET').'</a>' ); ?>
                            </div>
                        <?php
                        } elseif($this->my->isCourseVideoAdd($this->course->id) && !(int)$this->my->courseVideoApproved($this->course->id)) {

                            $who_bought = plotUser::factory($this->courseObj->whoBoughtCourse($this->my->id));
                            ?>
                            <div class="weiter-schritt">
                                <?php echo   JText::sprintf('COM_PLOT_COURSE_BUY_AND_TEST_AND_VIDEO_NOT_APPROVED', '<br/><a href="' . JRoute::_("index.php?option=com_plot&view=profile&id=" . $who_bought->id) . '">' . $who_bought->name . '</a>'); ?>
                            </div>
                        <?php
                        }elseif(!$this->my->isCourseVideoAdd($this->course->id) && !(int)$this->my->courseVideoApproved($this->course->id)){
                           ?>
                            <div class="weiter-schritt">
                                <?php echo   JText::_('COM_PLOT_COURSE_BUY_BUT_TEST_FINISHED'); ?>
                            </div>
                            <?php
                        }
                    }
                }
               }else{
                   ?>
                   <div class="bonus">
                       <em class="pin-btn green"></em>
                       <em class="pin-btn green"></em>
                       <p>После изучения получишь</p>
                       <div>
                           <i class="elka">+<?php echo $this->course->count_points; ?>
                               <svg viewBox="0 0 14.6 23.1" preserveAspectRatio="xMinYMin meet">
                                   <use xlink:href="#elka-interest"></use>
                               </svg>
                           </i>
                       </div>

                   </div>
                <?php
               }

                ?>


        </div>
        <h4><?php echo JText::_('COM_PLOT_COURSE_ANNOTATION'); ?></h4>
        <div class="about">
            <em class="pin-btn green"></em>
            <em class="pin-btn green"></em>
            <strong>Автор: <?php echo $this->course->getAuthor()->name; ?></strong>
            <p>
                <?php echo PlotHelper::cropStr($this->course->course_description, plotGlobalConfig::getVar('courseDescriptionMaxSymbolsForDisplay')); ?>
            </p>
        </div>
        <h4>Купить курс</h4>
        <div class="buy">
            <em class="pin-btn green"></em>
            <em class="pin-btn green"></em>
            <form id="course-children-for">
                <fieldset>
                    <label for="recipients-course">Выберите кому:</label>

                    <div class="users-course-for-list"></div>
                    <button class="add-recipient" name="choose"
                            onclick="SqueezeBox.open('<?php echo JRoute::_('index.php?option=com_plot&task=searchusers.showPopup&courseId=' . JRequest::getInt('id', 0)); ?>', {size: {x: 600, y: 450}, handler: 'ajax'});">
                        <svg viewBox="0 0 31.7 30.8" preserveAspectRatio="xMinYMin meet" class="friend">
                            <use xlink:href="#friend"></use>
                        </svg>
                        Добавить
                    </button>
                </fieldset>
            </form>
            <div>
                <div>
                    <p>Вознаграждение за прочтение:</p>
                    <span class="abs">1 руб = 1 монета</span>
                    <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                        <use xlink:href="#munze"></use>
                    </svg>
                    <input type="text" id="reward" value="<?php echo plotGlobalConfig::getVar('payForCourseCompleteDefault'); ?>"/>
                </div>
                <div>
                    <p><?php echo JText::_("COM_PLOT_ADMINISTRATOR_DONATION");?></p>
                    <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                        <use xlink:href="#munze"></use>
                    </svg>
                    <i class="admin-cost-calculated"><?php echo $this->course->costsForBuyers['admin']; ?></i>
                </div>
                <div>
                    <p>Вознаграждение автору:</p>
                    <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                        <use xlink:href="#munze"></use>
                    </svg>
                    <i class="author-cost-calculated"><?php echo $this->course->costsForBuyers['author']; ?></i>
                </div>
                <div>
                    <p>Комиссия платежной системы:</p>
                    <svg viewBox="0 0 25.1 21.1" preserveAspectRatio="xMinYMin meet" class="munze">
                        <use xlink:href="#munze"></use>
                    </svg>
                    <i class="payment-system-cost-calculated"><?php echo $this->course->costsForBuyers['paymentSystem']; ?></i>
                </div>
                <span>Итого:</span>
                <b><span class="total-cost-calculated"><?php echo $this->course->costsForBuyers['total']; ?></span> руб</b>
                <!--<form action="<?php echo JRoute::_('index.php?option=com_plot&task=yandex.setOrderAsPaidAndSetUsersToCourse')?>" method="post" name="buy_course_hidden_form"
                      id="buy_course_hidden_form">-->
                <form action="https://money.yandex.ru/eshop.xml" method="post" name="buy_course_hidden_form"
                      id="buy_course_hidden_form">
                    <!-- Обязательные поля -->
                    <input name="shopId" class="hidden" value="<?php echo plotGlobalConfig::getVar('yandexShopId'); ?>"
                           type="text"/>
                    <input name="scid" class="hidden" value="<?php echo plotGlobalConfig::getVar('yandexScid'); ?>" type="text"/>
                    <input name="sum" class="hidden" value="0" type="text"/>
                    <input name="customerNumber" class="hidden" value="<?php echo plotUser::factory()->id; ?>" type="text"/>
                    <!-- Необязательные поля -->
                    <div class="payment-type">
                        <p>Способ оплаты:</p>
                        <select name="paymentType" id="paymentType">
                            <option value="AC">Банковская карта</option>
                            <option value="PC">Яндекс кошелек</option>
                            <option value="GP">Через кассы и терминалы</option>
                            <option value="WM">Кошелек WM</option>
                        </select>
                    </div>
                    <input name="orderNumber" class="hidden" value="0" type="text"/>
                    <input name="cps_email" class="hidden" value="<?php echo $this->my->email; ?>" type="text"/>
                    <input name="shopSuccessURL" class="hidden"
                           value="http://naplotu.com/course/<?php echo $this->course->id; ?>/success=1"
                           type="text"/>
                    <input name="shopFailURL" class="hidden"
                           value="http://naplotu.com/course/<?php echo $this->course->id; ?>/success=0"
                           type="text"/>
                    <input name="courseId" class="hidden" value="<?php echo $this->course->id; ?>" type="text"/>
                    <input name="entity" class="hidden" value="course" type="text"/>
                    <input name="rewardAmountForEach" class="hidden" value="0" type="text"/>
                    <input name="userIds" class="hidden" value="" type="text"/>
                </form>
                <script>
                    jQuery("#paymentType").selectmenu({
                        appendTo: '.payment-type',
                        close: function (event, ui) {
                        }
                    });
                    jQuery("#paymentType").on("selectmenuclose", function (event, ui) {
                    });
                </script>
                <?php if((int)$this->my->id){ ?>
                    <input type="submit" onclick="courseBuy();" value="<?php echo  JText::_('COM_PLOT_CHECKOUT')?>"/>
                <?php }else{
                    ?>
                    <input type="submit" onclick="createMsg('<?php echo JText::_("COM_PLOT_LOGIN_FIRST"); ?>');" value="<?php echo  JText::_('COM_PLOT_CHECKOUT')?>"/>
                <?php
                } ?>
            </div>
        </div>
        <h4>Курс включен в программы:</h4>
        <div class="programs-list" id="child-programs-list">
            <em class="pin-btn yellow"></em>
            <em class="pin-btn yellow"></em>
                <div class="jcarousel">
                <?php if ($this->coursePrograms) { ?>
                    <ul>
                        <?php
                        foreach ($this->coursePrograms AS $program) {
                            ?>
                            <li>
                                <a href="<?php echo $program->link; ?>">
                                    <img src="<?php echo $program->img; ?>">
                                    <div class="plot-course-programs" data-progress="<?php echo $program->percent;?>"></div>
                                    <h5><?php echo  $program->title; ?></h5>
                                </a>
                            </li>
                        <?php
                        }
                        ?>
                    </ul>
                <?php
                } else{?>
                    <p class="no-items"><?php  echo JText::sprintf('COM_PLOT_THIS_COURSE_NOT_INCLUDE_IN PROGRAMS', $this->course->course_name); ?></p>
                <?php
                }
                ?>
                <a href="javascript:void(0);" class="jcarousel-control-prev">&#60</a>
                <a href="javascript:void(0);" class="jcarousel-control-next">&#62</a>
            </div>
        </div>
        <div id="child-media-tabs" class="child-library">

            <div class="info-board essay">
                <ul class="child-media-tabs" id="all-tabs">

                    <?php if(!$this->courseObj->isSmall()){ ?>
                        <li id="link-child-videos"><a href="#course-videos"
                           class="active"><?php echo JText::_('COM_PLOT_COURSE_VIDEOS'); ?></a></li>
                    <?php } ?>

                    <?php if($this->courseBefore){?>
                    <li id="link-beforecourses"><a href="#all-beforecourse"
                           ><?php echo JText::_('COM_PLOT_COORSES_BEFORE'); ?></a></li>
                    <?php } ?>
                    <?php if($this->courseAfter){?>
                    <li id="link-aftercourses"><a href="#all-aftercourse" ><?php echo JText::_('COM_PLOT_COORSES_AFTER'); ?></a>
                        <?php } ?>
                    </li>

                    <li id="link-child-disqus"><a href="#all-child-disqus" ><?php echo  JText::_('COM_PLOT_COURSE_DISQUS');?></a></li>

                </ul>
            </div>
            <div id="loading-bar" style="margin-left: 10px; margin-top: 10px;"><img
                    src="<?php echo $this->baseurl ?>/templates/plot/img/pre-loader-1.gif"/></div>

            <?php  if(!$this->courseObj->isSmall()){ ?>
            <div id="course-videos" class="essay-inner child-profile">
                <div class="add-activity child-profile">
                    <?php if (in_array($this->course->id, $this->my->getCoursesIdsBoughtForMe($onlyNotFinished = true)) && !$this->my->courseVideoApproved($this->course->id) && !$this->my->isCourseFinished($this->course->id) && $this->my->isCourseTestFinished($this->course->id) && !$this->courseObj->isSmall()) {
                    require_once JPATH_COMPONENT . '/views/course/tmpl/add.video.php';
                     } ?>
                </div>

                <div id="all-child-videos" class="new-event">
                    <ul class="no-jcarousel" id="videos-list"></ul>
                </div>
            </div>
            <?php }  ?>
            <?php if($this->courseBefore){?>
            <div id="all-beforecourse">
                <div id="beforecourses-list"></div>
            </div>
            <?php } ?>
            <?php if($this->courseAfter){?>
            <div id="all-aftercourse">
                <div id="aftercourses-list"></div>
            </div>
            <?php } ?>
            <div id="all-child-disqus" class="new-event">
                <?php echo PlotHelper::renderDisqus('course_' . $id); ?>
                <div id="disqus_thread"></div>
            </div>

        </div>
    </div>
</div>





</section>


</div>

</main>


<?php # </editor-fold> ?>

