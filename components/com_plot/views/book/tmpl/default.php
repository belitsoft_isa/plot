<?php
defined('_JEXEC') or die;
JHtml::_('bootstrap.framework');
JHtml::_('behavior.tooltip');

require_once(JPATH_SITE . '/components/com_plot/helpers/html5fbfront.php');

?>

<script src="<?php echo JURI::base(); ?>media/jui/js/bootstrap.min.js"></script>
<style>
    body{
        color: black;
    }
</style>

<div>
    <?php

    $jinput = JFactory::getApplication()->input;
    $Itemid = $jinput->get('Itemid', 0, 'INT');


    $doc = JFactory::getDocument();

    $app = JFactory::$application;
    $jinput = $app->input;

    require_once (JPATH_SITE . '/components/com_html5flippingbook/libs/Mobile_Detect.php');
    $detectMobile = new Mobile_Detect();

    // Exclude tablets.
    $isMobile = FALSE;
    if ($detectMobile->isMobile() && !$detectMobile->isTablet()) {
        $isMobile = TRUE;
    }

    $doc->addScriptDeclaration('ht5popupWindow = function (a, b, c, d, f) { window.open(a, b, "height=" + d + ",width=" + c + ",top=" + (screen.height - d) / 2 + ",left=" + (screen.width - c) / 2 + ",scrollbars=" + f + ",resizable").window.focus() };');
    $doc->addScript(JURI::root() . 'components/com_html5flippingbook/assets/js/jquery.cookie.min.js');

    JText::script('COM_HTML5FLIPPINGBOOK_FE_DISPLAY_UNREAD_PUBL');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_DISPLAY_READ_PUBL');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_ACTION_READ_TIP');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_ACTION_REMOVE_READ');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_ACTION_ERROR_USER');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_ACTION_ERROR_ACTION');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_MAILTO_ERROR');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_JSSHARE_PUB_ERROR');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_JSSHARE_FRIEND_ERROR');

    ?>
    <script type="text/javascript">
        function calculateAmounts()
        {
            var countChildren = (jQuery('#choosed-childs').val() ? jQuery('#choosed-childs').val().length : 0) + (jQuery('#choosed-friends').val() ? jQuery('#choosed-friends').val().length : 0),
                amountForEachChildFriend = jQuery('#amount-for-each-child').val(),
                totalCostByPercents = 100 * amountForEachChildFriend / (100 - <?php echo $this->book->admin_cost + $this->book->author_cost; ?>),
                adminCost = totalCostByPercents * <?php echo (int)$this->book->admin_cost; ?> / 100,
                authorCost = totalCostByPercents * <?php echo (int)$this->book->author_cost; ?> / 100;


            if (adminCost < <?php echo (int)$this->book->admin_min; ?>) {
                adminCost = <?php echo (int)$this->book->admin_min; ?>;
            }
            if (adminCost > <?php echo (int)$this->book->admin_max; ?>) {
                adminCost = <?php echo (int)$this->book->admin_max; ?>;
            }

            if (authorCost < <?php echo (int)$this->book->author_min; ?>) {
               authorCost = <?php echo (int)$this->book->author_min; ?>;
            }
            if (authorCost > <?php echo (int)$this->book->author_max; ?>) {
                authorCost = <?php echo (int)$this->book->author_max; ?>;
            }

            jQuery('#displayed_admin_cost').html(adminCost * countChildren);
            jQuery('#displayed_author_cost').html(authorCost * countChildren);
            jQuery('#displayed_prize_cost').html(parseFloat(adminCost) * countChildren + parseFloat(authorCost) * countChildren + parseFloat(amountForEachChildFriend) * countChildren);
            jQuery('#plot-all-cost').val(parseFloat(adminCost) * countChildren + parseFloat(authorCost) * countChildren + parseFloat(amountForEachChildFriend) * countChildren);
        }

        jQuery(document).ready(function() {
            /* change value for prizes */
            jQuery('#amount-for-each-child').change(function() {
                calculateAmounts();
            });
            jQuery('#amount-for-each-child').keyup(function() {
                calculateAmounts();
            });
            jQuery('#choosed-childs').change(function() {
                calculateAmounts();
            });
            jQuery('#choosed-friends').change(function() {
                calculateAmounts();
            });

        });
    </script>

    <div class="slide-navi" style="display: none">
        <a href="javascript: void(0)" class="htmlfb5-sl-prev">Prev</a>
        <a href="javascript: void(0)" class="htmlfb5-sl-next">Next</a>
    </div>

    <?php


    echo HTML5FlippingBookFrontHelper::createBook('reading', $this->book, $isMobile, 1, $this->config);?>
    <?php if ($this->boughtBook) { ?>
        <a href="<?php echo JRoute::_('index.php?option=com_html5flippingbook&view=publication&id=' . (int)$this->book->c_id . '&tmpl=component'); ?>"
           rel="{handler: 'iframe', size: {x: 1026, y:700}}" class=" modal">
            <?php echo JText::_('COM_PLOT_READ_BOOK'); ?></a>
    <?php } ?>
    <form id="buy-course-form" method="POST" action="index.php?option=com_plot&id=<?php echo $this->book->id; ?>&task=book.buy">
        <div>
            <?php echo  JText::_('COM_PLOT_CHOOSE_CHILDREN');?>
            <select name="choosed-childs[]" multiple="true" id="choosed-childs">
                <?php

                foreach ($this->children AS $childId) { ?>
                    <option value="<?php echo $childId; ?>"><?php echo plotUser::factory($childId)->name; ?></option>
                <?php } ?>
            </select>
        </div>

        <div>
            <?php echo  JText::_('COM_PLOT_CHOOSE_FRIEND');?>
           <select name="choosed-friends[]" multiple="true" id="choosed-friends">
                    <?php foreach ($this->friends AS $friend) { ?>
                     <option value="<?php echo $friend->id; ?>"><?php echo $friend->name; ?></option>
                    <?php } ?>

            </select>
        </div>
        <div>
            <label><?php echo JText::_('COM_PLOT_PRICE FOR EACH_CHILD_OR_FRIEND'); ?>:</label>
            <input type="text" name="amount-for-each-child" id="amount-for-each-child" value="0" />
        </div>
        <div>
            <div>
                <?php echo  JText::_('COM_PLOT_ADMIN_COMMISSION'); ?>: <span id="displayed_admin_cost">0</span>
            </div>
            <div>
                <?php echo  JText::_('COM_PLOT_AUTHOR_COMMISSION');?>: <span id="displayed_author_cost">0</span>
            </div>
            <div>
                <?php echo  JText::_('COM_PLOT_TOTAL_PRICE'); ?> <span id="displayed_prize_cost">0</span>
            </div>

        </div>
<input type="hidden" name="all-cost" id="plot-all-cost">
        <input type="hidden" name="bookId" value="<?php echo $this->book->c_id; ?>" />

        <input type="submit" value="купить" />
    </form>

    <script type="text/javascript"
            src="<?php echo JURI::root() . 'components/com_html5flippingbook/assets/js/'; ?>profile.min.js"></script>
    <script type="text/javascript"
            src="<?php echo JURI::root() . 'components/com_html5flippingbook/assets/js/'; ?>jquery.actual.min.js"></script>

    <?php
    if ($this->config->social_email_use) {
        echo $this->emaillayout->render(NULL);
    }

    if ($this->config->social_jomsocial_use) {
        $data['friends'] = $this->userFriends;
        echo $this->sharelayout->render($data);
    }
    ?>



