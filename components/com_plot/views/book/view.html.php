<?php

defined('_JEXEC') or die;
require_once JPATH_COMPONENT . '/views/view.php';

require_once(JPATH_SITE . '/administrator/components/com_html5flippingbook/libs/VarsHelper.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/conversations.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/friends.php');

class PlotViewBook extends JViewLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'plot';

    public function display($tpl = null)
    {
        $app = JFactory::getApplication();
        $this->templateUrl = JURI::base() . 'templates/' . $app->getTemplate();
        $this->componentUrl = JURI::base() . 'components/com_plot';


        $this->my = plotUser::factory();
        $this->friends = $this->get('CheckFriends');
        $this->children = $this->get('CheckChildren');
        $document = JFactory::$document;

        $document->addStyleSheet(JURI::root() . 'components/com_html5flippingbook/assets/css/' . 'html5flippingbook.css');
        $document->addStyleSheet(JURI::root() . 'components/com_html5flippingbook/assets/css/' . 'font-awesome.min.css');

        $this->emaillayout = new JLayoutFile('email', $basePath = JPATH_COMPONENT . '/layouts');
        $this->sharelayout = new JLayoutFile('share', $basePath = JPATH_COMPONENT . '/layouts');


        $this->config = $this->GetConfig();
        $this->book = $this->get('Book');
        

        if ($this->config->social_jomsocial_use) {
            $this->userFriends = $this->get('UserJSFriends');
        }

        $this->bookBS = $this->get('Book');


            $thumbnailPath = JPATH_SITE . '/media/com_html5flippingbook' . '/thumbs/' . $this->bookBS->c_thumb;
        if ($row->c_thumb == "" || !is_file($thumbnailPath)) {
            $row->thumbnailUrl = JURI::root() . "components/com_html5flippingbook/assets/images/no_image.png";
        } else {
            if(file_exists(JPATH_BASE . '/media/com_html5flippingbook' . '/thumbs/thimb_' . $row->c_thumb)){
                $row->thumbnailUrl = JURI::root() . "media/com_html5flippingbook/thumbs/thimb_" . $row->c_thumb;
            }else{
                $row->thumbnailUrl = JURI::root() . "media/com_html5flippingbook/thumbs/" . $row->c_thumb;
            }

        }

        $this->boughtBook = $this->get('BoughtBook');

        parent::display($tpl);
    }

    protected function prepareDocument()
    {
        parent::prepareDocument();
        parent::addFeed();
    }


    public function getConfig()
    {
        $db = JFactory::getDBO();

        $query = "SELECT * FROM `#__html5fb_config`" .
            " ORDER BY `setting_name`";
        $db->setQuery($query);
        $rows = $db->loadObjectList();

        $config = (object)array();

        if (isset($rows)) {
            foreach ($rows as $row) {
                $config->{$row->setting_name} = $row->setting_value;
            }
        }

        return $config;
    }
}
