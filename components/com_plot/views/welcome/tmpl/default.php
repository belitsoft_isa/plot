<?php
defined('_JEXEC') or die;
?>
<script type="text/javascript" src="http://track.hubrus.com/pixel?id=35611&type=js"></script>
    <header class="welcome">
        <div class="wrap">
            <div class="logo-wrap">
                <a id="logo" href="<?php echo JRoute::_('index.php?option=com_plot&view=river'); ?>" title="naplotu.com">
                    <span>naplotu</span>
                    <i>образовательная соцсеть</i>
                </a>
            </div>
            <p class="text-congratulation">
                <b><?php echo  JText::_('COM_PLOT_WELCOME_CONGRATULATION');?></b>
                <span><?php echo  JText::_('COM_PLOT_WELCOME_TEXT');?></span>
            </p>
        </div>
    </header>
    <main class="welcome">
        <div class="wrap main-wrap">
            <article>
                <div class="img-wrap">
                    <img src="<?php echo JUri::root() . 'templates/plot/img/welcome-profile.png' ?>"/>
                </div>
                    <h4><?php echo JText::_('COM_PLOT_WELCOME_PROFILE'); ?></h4>

                    <p><?php echo JText::_('COM_PLOT_WELCOME_PROFILE_TEXT'); ?></p>
                    <a class="hover-shadow" href="<?php echo JRoute::_('index.php?option=com_plot&view=profile&id=' . plotUser::factory()->id); ?>"><?php echo JText::_('COM_PLOT_WELCOME_GO'); ?>
                        <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet">
                            <use xlink:href="#play"></use>
                        </svg>
                    </a>
            </article>
            <article>
                    <div class="img-wrap">
                        <img src="<?php echo JUri::root() . 'templates/plot/img/welcome-books.png' ?>"/>
                    </div>
                    <h4><?php echo JText::_('COM_PLOT_WELCOME_BOOKS'); ?></h4>

                    <p><?php echo JText::_('COM_PLOT_WELCOME_BOOKS_TEXT'); ?></p>
                    <a class="hover-shadow" href="<?php echo JRoute::_('index.php?option=com_plot&view=publications'); ?>"><?php echo JText::_('COM_PLOT_WELCOME_GO'); ?>
                        <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet">
                            <use xlink:href="#play"></use>
                        </svg>
                    </a>
            </article>
            <article>
                    <div class="img-wrap">
                        <img src="<?php echo JUri::root() . 'templates/plot/img/welcome-courses.png' ?>"/>
                    </div>
                    <h4><?php echo JText::_('COM_PLOT_WELCOME_COURSES'); ?></h4>

                    <p><?php echo JText::_('COM_PLOT_WELCOME_COURSES_TEXT'); ?></p>
                    <a class="hover-shadow" href="<?php echo JRoute::_('index.php?option=com_plot&view=courses'); ?>"><?php echo JText::_('COM_PLOT_WELCOME_GO'); ?>
                        <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet">
                            <use xlink:href="#play"></use>
                        </svg>
                    </a>
            </article>
        </div>
    </main>
