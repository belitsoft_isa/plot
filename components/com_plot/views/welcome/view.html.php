<?php

defined('_JEXEC') or die;
require_once JPATH_COMPONENT . '/views/view.php';

class PlotViewWelcome extends JViewLegacy
{
    protected $extension = 'com_plot';
    protected $viewName = 'welcome';
    public function display($tpl = null)
    {
        $app = JFactory::getApplication();
        if(!plotUser::factory()->id){
            $app->redirect(JRoute::_('index.php?option=com_plot&view=river', false));
        }

        if(!$app->getUserState( 'first', 0 )){
            $app->redirect(JRoute::_('index.php?option=com_plot&view=river', false));
        }
        $app->setUserState( 'first', 0 );
        $this->setOgMeta();
        return parent::display($tpl);
    }

    private function setOgMeta()
    {
        require_once JPATH_ADMINISTRATOR . '/components/com_plot/plot_social_config.php';
        $metaOgUrl = '<meta property="og:url" content="http://naplotu.com/" />';
        $metaOgTitle = '<meta property="og:title" content="' . plotSocialConfig::get('riverTitle') . '" />';
        $metaOgDescription = '<meta property="og:description" content="' . plotSocialConfig::get('riverDesc') . '" />';
        $metaOgImage = '<meta property="og:image" content="' . JUri::root() . plotSocialConfig::get('riverImagePath') . '?rand=' . rand() . '" />';
        $metaOgImageType = '<meta property="og:image:type" content="image/jpeg" />';
        $metaOgImageWidth = '<meta property="og:image:width" content="' . plotSocialConfig::get('riverImageWidth') . '" />';
        $metaOgImageHeight = '<meta property="og:image:height" content="' . plotSocialConfig::get('riverImageHeight') . '" />';
        $metaOgSiteName = '<meta property="og:site_name" content="' . JFactory::getConfig()->get('sitename') . '" />';
        $metaOgFbAppId = '<meta property="fb:app_id" content="473991812742077" />';
        $metaOgType = '<meta property="og:type" content="website" />';
        $metaOgLocale = '<meta property="og:locale" content="ru_RU" />';
        JFactory::getDocument()->addCustomTag($metaOgTitle . $metaOgSiteName . $metaOgUrl . $metaOgDescription . $metaOgFbAppId . $metaOgType . $metaOgLocale . $metaOgImage . $metaOgImageType . $metaOgImageWidth . $metaOgImageHeight);
        return true;
    }

}
