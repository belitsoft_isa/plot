<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class PlotViewPortfolios extends JViewLegacy
{



    function display($tpl = null)
    {
        $this->state = $this->get('State');
        $this->items = $this->get('Items');

        $this->pagination = $this->get('Pagination');

        $this->addToolBar();
        parent::display($tpl);
        $this->setDocument();
    }

    protected function addToolBar()
    {
        // add required stylesheets from admin template
        $document = JFactory::getDocument();
        $document->addStyleSheet('administrator/templates/isis/css/template.css');
        //now we add the necessary stylesheets from the administrator template
        //in this case i make reference to the isis default administrator template in joomla 3.2
        $document->addCustomTag( '<link href="administrator/templates/isis/css/template.css" rel="stylesheet" type="text/css" />'."\n\n" );
        //load the JToolBar library and create a toolbar
        jimport('cms.html.toolbar');
        $bar =new JToolBar( 'toolbar' );
        //and make whatever calls you require
        $bar->appendButton( 'Standard', 'new', JText::_('COM_PLOT_NEW'), 'portfolio.add', false );
        $bar->appendButton( 'Separator' );
        $bar->appendButton( 'Standard', 'delete', JText::_('COM_PLOT_DELETE'), 'portfolio.delete', false );
        $bar->appendButton( 'Standard', 'edit', JText::_('COM_PLOT_EDIT'), 'portfolio.edit', false );
        //generate the html and return
        return $bar->render();
    }

    protected function setDocument()
    {
        $document = JFactory::getDocument();
        $document->setTitle(JText::_('Plot Manager - Administrator'));
    }

}
