<?php

defined('_JEXEC') or die;

class PlotViewProfileEdit extends JViewLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'profileEdit';

    public function display($tpl = null)
    {
        $app = JFactory::getApplication();
        $profileEditModel = $this->getModel();

        $this->templateUrl = JURI::base().'templates/'.$app->getTemplate();
        $this->componentUrl = JURI::base().'components/com_plot';


        $id=JRequest::getInt('id',0);
        if($id){
            $this->my = plotUser::factory($id);

        }else{
            $this->my = plotUser::factory();
        }
        if (!$this->my->id) {
            echo 'please log in first';
            return false;
        }

        $myChildrenIds = $this->my->getChildrenIds();
        $this->my->children = array();
        foreach ($myChildrenIds AS $childId) {
            $this->my->children[] = plotUser::factory($childId);
        }

        $allChildrenProfilesIds = $profileEditModel->getAllChildrenProfilesIds();
        $this->allChildrenProfiles = array();
        foreach ($allChildrenProfilesIds AS $childProfileId) {
            $this->allChildrenProfiles[] = plotUser::factory($childProfileId);
        }

        return parent::display($tpl);
    }

}
