<?php
defined('_JEXEC') or die;
?>

<div>
    PROFILE EDITING PAGE
</div>
<div>
    <?php if ($this->my->id) { ?>
        <div>
            Hi, <?php echo $this->my->name; ?>
        </div>
        <div>
            <?php if ($this->my->children) { ?>
                <div>Your children:</div>
                <?php foreach ($this->my->children AS $i => $child) { ?>
                    <div>
                        <?php echo ($i + 1).". ".$child->name; ?>
                        <a href="<?php echo JRoute::_("index.php?option=com_plot&task=user.removeChild&childId=$child->id"); ?>">Remove</a>
                    </div>
                <?php } ?>
            <?php } else { ?>
                You have No children
            <?php } ?>
            <div>
                <form action="index.php" method="POST">
                    <select name="childId">
                        <?php foreach ($this->allChildrenProfiles AS $childProfile) { ?>
                        <option value="<?php echo $childProfile->id;?>"><?php echo $childProfile->name;?></option>
                        <?php } ?>
                    </select>
                    <input type="hidden" name="task" value="user.addChild" />
                    <input type="submit" value="Add child" />
                </form>
            </div>
        </div>
    <?php } else { ?>
        You are not logged in.
    <?php } ?>
</div>