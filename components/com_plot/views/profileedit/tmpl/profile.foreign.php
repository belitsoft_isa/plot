<?php
defined('_JEXEC') or die;
$id = JRequest::getInt('id', 0);
if ($id) {
    $this->my = plotUser::factory($id);
} else {
    $this->my = plotUser::factory();
}
$user = plotUser::factory();

?>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo JUri::root();?>templates/plot/css/style.css" />

<script type="text/javascript" src="<?php echo JUri::root(); ?>media/jui/js/jquery.min.js"></script>
<!--<script src="--><?php //echo JUri::root(); ?><!--media/jui/js/jquery.ui.core.min.js" type="text/javascript"></script>-->
<script type="text/javascript" src="<?php echo JUri::root(); ?>templates/plot/js/jquery-ui.min.js" ></script>
<script type="text/javascript" src="<?php echo JURI::root(); ?>media/jui/js/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>components/com_plot/views/profileedit/tmpl/profile_edit_popup.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/js/jquery.jscrollpane.min.js"></script>

<script type="text/javascript" src="<?php echo JUri::root(); ?>media/system/js/mootools-core.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>media/system/js/core.js"></script>

<script type="text/javascript" src="<?php echo JUri::root(); ?>media/system/js/mootools-more.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>media/system/js/modal.js"></script>
<script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/plot/js/svgsprite.js"></script>
<script type="text/javascript" src="<?php echo JUri::root(); ?>templates/plot/js/jplot.js"></script>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">
    // <editor-fold defaultstate="collapsed" desc="Avatar Change">
    function changeAvatar() {
        jQuery('#upload-avatar-form').submit();
    }
    function removeAvatar() {
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxRemoveAvatar', function () {
            jQuery('#about-me-container .avatar').attr('src', '<?php echo JUri::root(); ?>media/com_easysocial/defaults/avatars/users/square.png');
        });
    }
    // </editor-fold>

    function toggleUserTag(tagId) {
        var tagInput = jQuery('#tag' + tagId);
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxToggleTag', {tagId: tagId, checked: tagInput.is(':checked')}, function (error) {
            if (error) {
                alert('tags limit: <?php echo plotGlobalConfig::getVar('maxTagsForUser'); ?>');
                tagInput.attr('checked', false);
            }
        });
    }

    function filterMyFriendsByTags()
    {
        var tagId = jQuery('#friends-filter-tags').val()

        jQuery('.loading-friends-list').html('Загрузка...');
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxFilterMyFriendsByTags', {userId: <?php echo $this->my->id; ?>, tagId: tagId}, function(jsonResponce){
            var data = jQuery.parseJSON(jsonResponce);
            jQuery('#tab-1').html( data.html );
            jPlot.profile.showPopupDataAndRefreshSelectsAndScrollpanes();
        });
    }
    jQuery(document).ready(function(){
        jQuery( "#friends-filter-tags" ).selectmenu({
            open: function( event, ui ) {
                jQuery('#friends-filter-tags-menu').jScrollPane();
            },
            change: function(event, ui){ filterMyFriendsByTags('<?php echo $this->my->id; ?>'); }
        });
    });

    function addTag()
    {
        var tag_id = jQuery("#plot-tags option:selected").val();
        if (tag_id) {
            var text = jQuery("#plot-tags option:selected").text(),
                count_tags = jQuery('#plot-list-my-tags .my-interest').length;

            if (count_tags >=<?php echo plotGlobalConfig::getVar('maxTagsForUser'); ?>) {
                alert("<?php echo JText::_('COM_PLOT_YOU_HAVE_MAX_COUNT_TAGS')?>");
            } else {
                jQuery.post('index.php?option=com_plot&task=profileedit.ajaxAddTag', {tag_id: tag_id}, function (response) {
                    var str = '';
                    if (response) {
                        str = '' +
                        '<li class="my-interest my-interest-'+tag_id+'">' +
                        '<span>'+text+'</span> ' +
                        '<div>' +
                        '<form class="smile-interest" onsubmit="return false;">' +
                        '<fieldset>' +
                        '<input type="radio" id="el_'+tag_id+'-dont-like" name="el_'+tag_id+'" value="dont-like" checked="none"/>' +
                        '<label for="el_'+tag_id+'-dont-like" onclick="tagSmileyChange(\''+tag_id+'\',\'1\');" >' +
                        '<svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" style="fill: #ccc;" class="smile-dont-like radio-icon js-smile-'+tag_id+'1" >' +
                        '<use xlink:href="#smile-dont-like"></use>' +
                        '</svg>' +
                        '</label> ' +
                        '<input type="radio" id="el_'+tag_id+'-so-so" name="el_'+tag_id+'" value="so-so" selected="true" />' +
                        '<label for="el_'+tag_id+'-so-so" onclick="tagSmileyChange(\''+tag_id+'\',\'2\');">' +
                        '<svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-so-so radio-icon js-smile-'+tag_id+'2" >' +
                        '<use xlink:href="#smile-so-so"></use>' +
                        '</svg>' +
                        '</label> ' +
                        '<input type="radio" id="el_'+tag_id+'-like" name="el_'+tag_id+'" value="like"/>' +
                        '<label for="el_'+tag_id+'-like" onclick="tagSmileyChange(\''+tag_id+'\',\'3\');">' +
                        '<svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-like radio-icon js-smile-'+tag_id+'3">' +
                        '<use xlink:href="#smile-like"></use>' +
                        '</svg>' +
                        '</label> ' +
                        '<p class="not-checked" style="display: block;" >не выбрано</p>' +
                        '<div class="interest-explain" style="display: none;">' +
                        '<input class="like" style="width: 165px;" type="text" title="кликни, чтобы отредактировать" placeholder="напиши почему" value="" />' +
                        '<input class="like-before-change" type="hidden" value="" />' +
                        '<button class="edit-pencil" style="visibility: visible;">' +
                        '<svg viewBox="0 0 8.3 14.6" preserveAspectRatio="xMinYMin meet" class="edit">' +
                        '<use xlink:href="#edit"></use>' +
                        '</svg>' +
                        '</button>' +
                        '<button class="apply" onclick="tagTitleSave(\''+tag_id+'\');" style="display: inline-block;"></button>' +
                        '<button class="cancel" onclick="tagTitleCancel(\''+tag_id+'\');" style="display: inline-block;">&#215</button>' +
                        '</div>' +
                        '</fieldset>' +
                        '</form>' +
                        '</div>' +
                        '<button class="delete" onclick="deleteTag(\''+tag_id+'\');">&#215</button>' +
                        '</li>';
                    }
                    jQuery('#plot-list-my-tags .interests-blank-area').before(str);
                    updateAddTagsOptions(jQuery('#plot-tags'));
                });
            }
        }
    }

   function createComplain(){
        var overlay='<div class="avatar-overlay" id="sbox-overlay2" aria-hidden="false" tabindex="-1"></div>',
            str='<div id="sbox-window2"  style="width: 500px; height: 350px; top:10%; left: 10%;"role="dialog" aria-hidden="false" class="shadow avatar-sbox">' +
                '<div id="complain-form">'+
                '<fieldset>'+
        '<input type="hidden" name="uid" id="uid" value="<?php echo  (int)$this->user->id; ?>">'+
        '<label for="message">Пожаловаться на пользователя <?php echo plotUser::factory((int)$this->user->id)->name; ?></label>'+
        '<textarea id="message" name="message" placeholder="Почему Вы хотите пожаловаться на этого пользователя?"></textarea>'+
        '<button class="button brown hover-shadow" onclick="sendComplain(<?php echo (int)$this->user->id; ?>)">'+
        '<span>'+
        '<svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-dont-like"><use xlink:href="#smile-dont-like"></use></svg>'+
        'Отправить сообщение'+
        '</span>'+
        '</button>'+
        '</fieldset></div>'+
        '<a id="sbox-btn-close2" href="#" onclick="window.parent.document.getElementById(\'sbox-window\').style.position=\'fixed\';  jQuery(this.parentNode.previousSibling).remove(); jQuery(this.parentNode).remove();" role="button" aria-controls="sbox-window"></a></div>';

        jQuery("#avatar-box").after(str);
        jQuery("#avatar-box").after(overlay);
    }

    function sendComplain(uid){
        var message=jQuery('#message').val();
        if(message==''){
            alert('Заполните все поля');
        }else{
            jQuery.post('index.php?option=com_plot&task=profile.sendReport', {uid: uid, message:message}, function (response) {
                if(response.status){

                    window.parent.document.getElementById('sbox-window').style.position='fixed';
                    document.getElementById('sbox-btn-close2').parentNode.previousSibling.remove();
                    document.getElementById('sbox-btn-close2').parentNode.remove();
                    var overlay='<div class="avatar-overlay" id="sbox-overlay2" tabindex="-1"></div>',
                        str='<div id="sbox-window2" role="dialog" aria-hidden="false" class="shadow avatar-sbox">' +
                            '<div id="sbox-content2" class="sbox-content-adopt" style="opacity: 1;"><div id="enqueued-message">'+response.message+
                            '</div></div><a id="sbox-btn-close2" href="#" onclick="window.parent.document.getElementById(\'sbox-window\').style.position=\'fixed\'; jQuery(this.parentNode.previousSibling).remove(); jQuery(this.parentNode).remove();" role="button" aria-controls="sbox-window"></a></div>';

                    jQuery("#avatar-box").after(str);
                    jQuery("#avatar-box").after(overlay);
                }else{
                    window.parent.document.getElementById('sbox-window').style.position='fixed';
                    document.getElementById('sbox-btn-close2').parentNode.previousSibling.remove();
                    document.getElementById('sbox-btn-close2').parentNode.remove();
                    var overlay='<div class="avatar-overlay" id="sbox-overlay2" tabindex="-1"></div>',
                        str='<div id="sbox-window2" role="dialog" aria-hidden="false" class="shadow avatar-sbox">' +
                            '<div id="sbox-content2" class="sbox-content-adopt" style="opacity: 1;"><div id="enqueued-message">' +response.message+
                            '</div></div><a id="sbox-btn-close2" href="#" onclick="window.parent.document.getElementById(\'sbox-window\').style.position=\'fixed\';jQuery(this.parentNode.previousSibling).remove(); jQuery(this.parentNode).remove();" role="button" aria-controls="sbox-window"></a></div>';

                    jQuery("#avatar-box").after(str);
                    jQuery("#avatar-box").after(overlay);
                }
            });
        }
    }
</script>
<?php # </editor-fold> ?>

<div id="svg-sprite"></div>

<div id="about-me-container" class="wrap about-friend">
<div id="tabs" class="user-info friends-list">
<ul class="popup-header">
    <li><a href="#tab-1">Все мои друзья</a></li>
    <li><a href="#tab-2">Мои интересы</a></li>
    <li><a href="#tab-3">Обо мне</a></li>
</ul>

<?php # <editor-fold defaultstate="collapsed" desc="Friends Tab">?>
<div id="tab-1">
    <div id="system-message-container-popup"></div>
    <div>
        <?php

        if ($this->friends || $this->oldfriends) { ?>
            <p style="padding: 10px 20px 0 20px;">
                <select id="friends-filter-tags" onchange="filterMyFriendsByTags();" style="margin-bottom: 5px;">
                    <option value="0">-- Все увлечения --</option>
                    <?php foreach ($this->tags AS $tag) { ?>
                        <option value="<?php echo $tag->id; ?>"><?php echo $tag->title; ?></option>
                    <?php } ?>
                </select>
                <span class="loading-friends-list"></span>
            </p>
        <?php } ?>
        <ul class="scroll-pane">
            <?php foreach ($this->friends AS $friend) { ?>
                <li class="<?php ($this->my->id==$user->id) ? 'new-event' : ''?> myfriends myfriend-<?php echo $friend->actor_id;?>">
                    <a onclick="redirectParentWindow('<?php echo JRoute::_('index.php?option=com_plot&view=profile&id='.$friend->actor_id);?>')" href="javascript:void(0);" class="name">
                        <img src="<?php echo plotUser::factory($friend->actor_id)->getSquareAvatarUrl();?>" alt="<?php echo Foundry::user((int)$friend->actor_id)->name; ?>"/>
                        <span><?php echo PlotHelper::cropStr(Foundry::user((int)$friend->actor_id)->name, plotGlobalConfig::getvar('profileEditPopupFriendsMaxNameLength')); ?></span>
                    </a>
                    <?php if($this->my->id==$user->id){ ?>
                        <div class="actions">
                            <a class="button green h30" onclick="approveFriendRequest('<?php echo $friend->actor_id;?>');">
                                    <span>
                                        <svg viewBox="0 0 31.7 30.8" preserveAspectRatio="xMinYMin meet" class="friend"><use xlink:href="#friend"></use></svg>
                                        <?php echo JText::_('COM_PLOT_ACCEPT'); ?>
                                    </span>
                            </a>
                            <a class="button brown h25" onclick="rejectFriendRequest('<?php echo $friend->actor_id;?>');">
                                <span><?php echo JText::_('COM_PLOT_REJECT'); ?></span>
                            </a>
                        </div>
                    <?php } ?>
                </li>
            <?php } ?>
            <?php foreach ($this->oldfriends as $friend) { ?>
                <li class="myfriends myfriend-<?php echo $friend->id; ?>">
                    <a href="#" class="name">
                        <img src="<?php echo plotUser::factory($friend->id)->getSquareAvatarUrl(); ?>" alt="<?php echo $friend->name; ?>"/>
                        <span><?php echo $friend->name; ?></span>
                    </a>

                </li>
            <?php } ?>

            <?php if (!$this->friends && !$this->oldfriends) { ?>
                <li class="myfriends">
                    <p class="no-items"><?php echo JText::_('COM_PLOT_YOU_DO_NOT_HAVE_FRIENDS'); ?></p>
                </li>
            <?php } ?>

        </ul>
    </div>
</div>
<?php // </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="Interests Tab">?>
<div id="tab-2">
    <ul id="plot-list-my-tags" class="scroll-pane friends-list">
        <?php foreach ($this->user->tags AS $tag) {
            ?>
            <li class="my-interest my-interest-<?php echo $tag->id; ?>">
                <span><?php echo $tag->title;?></span>
                <div>
                    <form class="smile-interest" onsubmit="return false;">
                        <fieldset>
                            <?php if(isset($tag->smiley)){?>
                                <?php if( $tag->smiley == 1){?>
                                    <label for="el_<?php echo $tag->id;?>-dont-like"  >
                                        <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-dont-like radio-icon js-smile-<?php echo $tag->id.'1';?>" <?php if (isset($tag->smiley) && $tag->smiley == 1) {echo 'style="fill: #eb3d00;"';};?> >
                                            <use xlink:href="#smile-dont-like"></use>
                                        </svg>
                                    </label>
                                <?php }else if( $tag->smiley == 2){?>
                                    <label for="el_<?php echo $tag->id;?>-so-so" >
                                        <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-so-so radio-icon js-smile-<?php echo $tag->id.'2';?>" <?php if (isset($tag->smiley) && $tag->smiley == 2) {echo 'style="fill: #f48000;"';};?>>
                                            <use xlink:href="#smile-so-so"></use>
                                        </svg>
                                    </label>
                                <?php } else if( $tag->smiley == 3){?>
                                    <label for="el_<?php echo $tag->id;?>-like" >
                                        <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-like radio-icon js-smile-<?php echo $tag->id.'3';?>" <?php if (isset($tag->smiley) && $tag->smiley == 3) {echo 'style="fill: #009049;"';};?>>
                                            <use xlink:href="#smile-like"></use>
                                        </svg>
                                    </label>
                                <?php }else{ ?>

                                    <label for="el_<?php echo $tag->id;?>-so-so" >
                                        <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-so-so radio-icon js-smile-<?php echo $tag->id.'2';?>">
                                            <use xlink:href="#smile-so-so"></use>
                                        </svg>
                                    </label>
                                <?php  } ?>



                            <?php } ?>
                            <?php $tag->tagTitle=isset($tag->tagTitle) ? $tag->tagTitle:$tag->title; ?>
                            <div class="interest-explain <?php if ((int)$tag->smiley && trim($tag->tagTitle)) {echo ''.$tag->smileyInputClass.' plot-reason';} else {echo 'not-checked plot-reason';};?>" >
                                <?php echo $tag->tagTitle; ?>
                            </div>
                        </fieldset>
                    </form>
                </div>

            </li>
        <?php } ?>

        <?php if(empty($this->user->tags)){ ?>
            <p class="no-items"><?php echo JText::_('COM_PLOT_USER_DOES_NOT_HAVE_TAGS'); ?></p>
        <?php }?>

    </ul>

</div>
<?php // </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="AboutMe Tab">?>
<div id="tab-3">
    <div class="tab-3-wrapper scroll-pane friends-list">
        <div class="resume">
            <p>
                <img class="avatar" src="<?php echo $this->user->getSquareAvatarUrl(); ?>"  alt=""/>

            </p>



            <div id="about-me-edit-block">

                <a href="#resume-1"  class="input-data about-me-show-value">
                    <?php echo PlotHelper::cropStr($this->user->getSocialFieldData('ABOUT_ME'),200); ?>
                </a>

                <textarea class="popup about-me-edit-value" id="resume" autofocus/><?php echo $this->user->getSocialFieldData('ABOUT_ME'); ?></textarea>

            </div>
        </div>
        <ul>
            <div>
                <li class="about-me" id="last-name-edit-block">
                    <label for="surname">Фамилия:</label>
                    <a href="#surname-1" class="input-data last-name-show-value">
                       <?php echo PlotHelper::cropStr($this->user->getSocialFieldData('last'), 30) ; ?>

                    </a>
                    <br>
                </li>
                <li class="about-me" id="first-name-edit-block">
                    <label for="name">Имя:</label>
                    <a href="#name-1"  class="input-data first-name-show-value">
                            <?php echo PlotHelper::cropStr($this->user->getSocialFieldData('first'), 30); ?>
                    </a>
                    <br>
                </li>
                <li class="about-me" id="middle-name-edit-block">
                    <div>
                        <label for="patronymic">Отчество:</label>
                        <a href="#patronymic-1"  class="input-data middle-name-show-value">

                                <?php echo PlotHelper::cropStr($this->user->getSocialFieldData('middle'), 30); ?>

                        </a>
                        <div class="clr"></div>
                    </div>

                    <textarea class="popup middle-name-edit-value" id="patronymic" placeholder="не указано" autofocus/><?php echo $this->user->getSocialFieldData('middle'); ?></textarea>

                </li>
                <li class="about-me" id="birthday-edit-block">
                    <div>
                        <label for="birth-date-1">Дата рождения:</label>
                        <?php
                        if($this->user->birthday->day . '.' . $this->user->birthday->month . '.' . $this->user->birthday->year!='01.01.1900'){
                            ?>
                            <a href="#birth-date-1"  class="input-data birthday-show-value"><?php echo $this->user->birthday->day . '.' . $this->user->birthday->month . '.' . $this->user->birthday->year; ?></a>
                        <?php
                        }
                        ?>
                        <div class="clr"></div>
                    </div>

                    <textarea class="popup birthday-edit-value" id="patronymic" placeholder="не указано" autofocus/><?php echo $this->user->birthday->day . '.' . $this->user->birthday->month . '.' . $this->user->birthday->year; ?></textarea>

                </li>
                <li class="about-me" id="email-edit-block">
                    <div>
                        <label for="e-mail-1">Email:</label>
                        <a href="#e-mail-1"  class="input-data email-show-value">
                            <?php echo PlotHelper::cropStr($this->user->email,30); ?>
                        </a>
                        <div class="clr"></div>
                    </div>

                    <textarea class="popup email-edit-value" id="patronymic" placeholder="не указано" autofocus/><?php echo $this->user->email; ?></textarea>

                </li>
                <li class="about-me" id="phone-edit-block">
                    <div>
                        <label for="phone">Телефон:</label>
                        <a href="#phone-1"  class="input-data phone-show-value"><?php echo $this->user->getSocialFieldData('phone'); ?></a>
                        <div class="clr"></div>
                    </div>

                    <textarea class="popup phone-edit-value" id="phone" placeholder="не указано" autofocus/><?php echo $this->user->getSocialFieldData('phone'); ?></textarea>

                </li>
            </div>
            <div>
                <li class="about-me" id="status-edit-block">
                    <div>
                        <label for="status">Статус:</label>
                        <a href="#status-1"  class="input-data status-show-value">
                            <?php echo PlotHelper::cropStr($this->user->getSocialFieldData('STATUS'), 30) ; ?>
                        </a>
                        <div class="clr"></div>
                    </div>

                    <textarea class="popup status-edit-value" id="status" placeholder="не указано" autofocus/><?php echo $this->user->getSocialFieldData('STATUS'); ?></textarea>

                </li>
                <li class="about-me" id="country-edit-block">
                    <div>
                        <label for="state">Страна:</label>
                        <a href="#state-1"  class="input-data country-show-value">
                            <?php echo PlotHelperCountries::getCountryName($this->user->getCountryCode()); ?>
                        </a>
                        <div class="clr"></div>
                    </div>

                    <textarea style="display: none;" class="popup" id="state" placeholder="не указано" autofocus>
                        <?php echo PlotHelperCountries::getCountryName($this->user->getCountryCode()); ?>
                    </textarea>
                    <select class="popup country-edit-value">
                        <?php foreach (PlotHelperCountries::getCountries() AS $code => $country) { ?>
                            <option <?php if ($this->user->getCountryCode() == $code) {echo 'selected="selected"';} ?> value="<?php echo $code; ?>">
                                <?php echo $country; ?>
                            </option>
                        <?php } ?>
                    </select>

                </li>
                <li class="about-me" id="state-edit-block">
                    <div>
                        <label for="region">Регион:</label>
                        <a href="#region-1"  class="input-data state-show-value">

                                <?php echo PlotHelper::cropStr($this->user->getSocialFieldData('state'), 30); ?>

                        </a>
                        <div class="clr"></div>
                    </div>

                    <textarea class="popup state-edit-value" id="region" placeholder="не указано" autofocus/><?php echo $this->user->getSocialFieldData('state'); ?></textarea>

                </li>
                <li class="about-me" id="city-edit-block">
                    <div>
                        <label for="city">Город:</label>
                        <a href="#city-1"  class="input-data city-show-value">
                            <?php if(isset($this->user->getSocialFieldData('ADDRESS')->city)){ ?>
                                <?php echo PlotHelper::cropStr($this->user->getSocialFieldData('ADDRESS')->city, 30); ?>
                            <?php } ?>
                        </a>
                        <div class="clr"></div>
                    </div>

                    <textarea class="popup city-edit-value" id="city" placeholder="не указано" autofocus/><?php echo $this->user->getSocialFieldData('ADDRESS')->city; ?></textarea>

                </li>
                <li class="about-me" id="school-edit-block">
                    <div>
                        <label for="school">Школа №:</label>
                        <a href="#school-1"  class="input-data school-show-value">
                            <?php echo PlotHelper::cropStr($this->user->getSocialFieldData('SCHOOL'), 30) ; ?>
                        </a>
                        <div class="clr"></div>
                    </div>

                    <textarea class="popup school-edit-value" id="school" placeholder="не указано" autofocus/><?php echo $this->user->getSocialFieldData('SCHOOL'); ?></textarea>

                </li>
                <li class="about-me" id="school-address-edit-block">
                    <div>
                        <label for="school-address">Адрес школы:</label>
                        <a href="#school-address-1"  class="input-data school-address-show-value">
                            <?php echo PlotHelper::cropStr($this->user->getSocialFieldData('SCHOOL_ADDRESS'), 30) ; ?>
                        </a>
                        <div class="clr"></div>
                    </div>

                </li>
            </div>
        </ul>
    </div>
</div>
<?php // </editor-fold> ?>
<?php if(plotUser::factory()->id){ ?>
<a class="complain hover-shadow" href="javascript:void(0)" onclick="createComplain();">
    <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-dont-like">
        <use xlink:href="#smile-dont-like"></use>
    </svg>
    <?php echo JText::_('COM_PLOT_COMPLAINT'); ?>
</a>
<?php } ?>
</div>
<div id="avatar-box"></div>
</div>
<input type="hidden"  value="<?php echo $this->my->id; ?>" id="user-id" />
