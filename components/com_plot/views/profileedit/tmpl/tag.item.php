<?php
defined('_JEXEC') or die;
?>
<li class="my-interest my-interest-<?php echo $this->tag_id; ?>">
    <span><?php echo $this->tagName; ?></span>
    <div>
        <form class="smile-interest" onsubmit="return false;">
            <fieldset>
                <input id="el_<?php echo $this->tag_id; ?>-dont-like" type="radio" value="dont-like" name="el_<?php echo $this->tag_id; ?>" <?php echo $this->tag_smile == 1 ? 'checked="checked"' : ''; ?> />
                <label for="el_<?php echo $this->tag_id; ?>-dont-like" onclick="tagSmileyChange('<?php echo $this->tag_id; ?>','1');" >
                    <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-dont-like radio-icon js-smile-<?php echo $this->tag_id; ?>1" >
                        <use xlink:href="#smile-dont-like"></use>
                    </svg>
                </label> 
                <input type="radio" id="el_<?php echo $this->tag_id; ?>-so-so" name="el_<?php echo $this->tag_id; ?>" value="so-so" <?php echo $this->tag_smile == 2 ? 'checked="checked"' : ''; ?> />
                <label for="el_<?php echo $this->tag_id; ?>-so-so" onclick="tagSmileyChange('<?php echo $this->tag_id; ?>','2');">
                    <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-so-so radio-icon js-smile-<?php echo $this->tag_id; ?>2" >
                        <use xlink:href="#smile-so-so"></use>
                    </svg>
                </label> 
                <input type="radio" id="el_<?php echo $this->tag_id; ?>-like" name="el_<?php echo $this->tag_id; ?>" value="like" <?php echo $this->tag_smile == 3 ? 'checked="checked"' : ''; ?> />
                <label for="el_<?php echo $this->tag_id; ?>-like" onclick="tagSmileyChange('<?php echo $this->tag_id; ?>','3');">
                    <svg viewBox="0 0 22 21" preserveAspectRatio="xMinYMin meet" class="smile-like radio-icon js-smile-<?php echo $this->tag_id; ?>3">
                        <use xlink:href="#smile-like"></use>
                    </svg>
                </label> 
                <div class="interest-explain">
                    <input class="<?php echo $this->checkedSmileVal; ?> plot-reason" type="text" value="<?php echo $this->tag_desc; ?>" placeholder="напиши почему"  />
                    <input class="like-before-change" type="hidden" value="<?php echo $this->tag_desc; ?>" />
                    <button class="edit-pencil" onclick="jQuery('#plot-list-my-tags .my-interest-<?php echo $this->tag_id; ?> .plot-reason').focus();">
                        <svg viewBox="0 0 8.3 14.6" preserveAspectRatio="xMinYMin meet" class="edit">
                            <use xlink:href="#edit"></use>
                        </svg>
                    </button>
                    <button class="apply" onmousedown="tagTitleSave('<?php echo $this->tag_id; ?>');"></button>
                    <button class="cancel" onmousedown="tagTitleCancel('<?php echo $this->tag_id; ?>');">&#215</button>
                </div>
            </fieldset>
        </form>
    </div>
    <button class="delete" onclick="deleteTag('<?php echo $this->tag_id; ?>');">&#215</button>
</li>