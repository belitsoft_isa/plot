<?php
defined('_JEXEC') or die;

$id = JRequest::getInt('userId', 0);
if ($id) {
    $this->my = plotUser::factory($id);
} else {
    $this->my = plotUser::factory();
}
$user = plotUser::factory();
?>

<div id="system-message-container-popup"></div>
<div>
    <p style="padding: 10px 20px 0 20px;">
        <select id="friends-filter-tags" onchange="filterMyFriendsByTags();" style="margin-bottom: 5px;">
            <option value="0">-- Все увлечения --</option>
            <?php foreach ($this->tags AS $tag) { ?>
                <option value="<?php echo $tag->id; ?>" <?php if ($this->tagId == $tag->id) {echo 'selected="selected"';} ?>><?php echo $tag->title; ?></option>
            <?php } ?>
        </select>
        <span class="loading-friends-list"></span>
    </p>
    <ul class="scroll-pane">
        <?php foreach ($this->friends AS $friend) { ?>

            <li class="<?php ($this->my->id==$user->id) ? 'new-event' : ''?> myfriends myfriend-<?php echo $friend->actor_id;?>">
                <a onclick="redirectParentWindow('<?php echo JRoute::_('index.php?option=com_plot&view=profile&id='.$friend->actor_id);?>')" href="javascript:void(0);" class="name">
                    <img src="<?php echo plotUser::factory($friend->actor_id)->getSquareAvatarUrl();?>" alt="<?php echo Foundry::user((int)$friend->actor_id)->name; ?>"/>
                    <span><?php echo PlotHelper::cropStr(Foundry::user((int)$friend->actor_id)->name, plotGlobalConfig::getvar('profileEditPopupFriendsMaxNameLength')); ?>
                <?php if($this->my->id==$user->id){ ?>
                    </span>
                    </a>
                    <div class="actions">
                        <a class="button green h30" onclick="approveFriendRequest('<?php echo $friend->actor_id;?>');">
                        <span>
                            <svg viewBox="0 0 31.7 30.8" preserveAspectRatio="xMinYMin meet" class="friend"><use xlink:href="#friend"></use></svg>
                            <?php echo JText::_('COM_PLOT_ACCEPT'); ?>
                        </span>
                        </a>
                        <a class="button brown h25" onclick="rejectFriendRequest('<?php echo $friend->actor_id;?>');">
                            <span><?php echo JText::_('COM_PLOT_REJECT'); ?></span>
                        </a>
                    </div>
                <?php }else{
                    ?>
                    <?php if($friend->state==-1){ ?>
                        <i><svg viewBox="0 0 34.5 30.8" preserveAspectRatio="xMinYMin meet" class="wait-for-friend"><use xlink:href="#wait-for-friend"></use></svg>
                            <?php echo JText::_('COM_PLOT_PENDING_FRIEND_APPROVE'); ?>
                        </i>
                    <?php }?>
                    </span>
                    </a>
                    <div class="actions">
                        <?php if(plotUser::factory()->id){ ?>
                        <a class="button green h30 modal" href="<?php echo JRoute::_('index.php?option=com_plot&task=conversations.newMessage&id=' . (int)$friend->actor_id); ?>" rel="{size: {x: 744, y: 525}, handler:'iframe'}">
                        <span>
                            <svg viewBox="0 0 40.7 29.5" preserveAspectRatio="xMinYMin meet" class="letter"><use xlink:href="#letter"></use></svg>
                            <?php echo JText::_('COM_PLOT_WRITE_ME_MESSAGE');?>
                        </span>
                        </a>
                        <?php } ?>
                    </div>
                <?php
                } ?>
            </li>
        <?php } ?>

        <?php foreach ($this->oldfriends as $friend) { ?>
            <li class="myfriends myfriend-<?php echo $friend->id; ?>">
                <a onclick="window.parent.location.href='<?php echo JRoute::_('index.php?option=com_plot&view=profile&id=' . $friend->id)?>'" class="name">
                    <img src="<?php echo plotUser::factory($friend->id)->getSquareAvatarUrl(); ?>" alt="<?php echo $friend->name; ?>"/>
                    <span><?php echo $friend->name; ?></span>
                </a>
                <div>
                    <?php if(plotUser::factory()->id){ ?>

                    <a class="button green h30 modal" href="<?php echo JRoute::_('index.php?option=com_plot&task=conversations.newMessage&id=' . (int)$friend->id); ?>" rel="{size: {x: 744, y: 525}, handler:'iframe'}">
                        <span>
                            <svg viewBox="0 0 40.7 29.5" preserveAspectRatio="xMinYMin meet" class="letter"><use xlink:href="#letter"></use></svg>
                            <?php echo JText::_('COM_PLOT_WRITE_ME_MESSAGE');?>
                        </span>
                    </a>
                    <?php } ?>
                    <?php if($this->my->id==$user->id){ ?>
                        <button class="close" onclick="removeFriend('<?php echo $friend->id; ?>');">&#215</button>
                    <?php } ?>
                </div>
            </li>
        <?php } ?>

        <?php if (!$this->friends && !$this->oldfriends) { ?>
            <li class="myfriends">
                <p class="no-items"><?php echo JText::_('COM_PLOT_USER_DOES_NOT_HAVE_FRIENDS'); ?></p>
            </li>
        <?php } ?>

    </ul>
</div>
<input type="hidden"  value="<?php echo $this->my->id; ?>" id="user-id" />
<script type="text/javascript">
    jQuery('.scroll-pane').jScrollPane();
    jQuery( "#friends-filter-tags" ).selectmenu({
        open: function( event, ui ) {
            jQuery('#friends-filter-tags-menu').jScrollPane();
        },
        change: function(event, ui){ filterMyFriendsByTags('<?php echo $this->my->id; ?>'); }
    });
    function filterMyFriendsByTags()
    {
        var tagId = jQuery('#friends-filter-tags').val()

        jQuery('.loading-friends-list').html('Загрузка...');
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxFilterMyFriendsByTags', {userId: <?php echo $this->my->id; ?>, tagId: tagId}, function(jsonResponce){
            var data = jQuery.parseJSON(jsonResponce);
            jQuery('#tab-1').html( data.html );
        });
    }


    function filterFriendsByTags(){
        var tagId = jQuery('#friends-filter-tags').val()

        jQuery('.loading-friends-list').html('Загрузка...');
        jQuery.post('index.php?option=com_plot&task=profileedit.ajaxFilterMyFriendsByTags', {userId: <?php echo $this->my->id; ?>, tagId: tagId}, function(jsonResponce){
            var data = jQuery.parseJSON(jsonResponce);
            jQuery('#tab-1').html( data.html );
        });
    }

    jQuery(document).ready(function(){
        jQuery( "#country-edit-value").selectmenu({
            open: function( event, ui ) {
                jQuery('#country-edit-value-menu').jScrollPane();
            }
        });
        jQuery( "#plot-tags").selectmenu({
            open: function( event, ui ) {
                jQuery('#plot-tags-menu').jScrollPane();
            }
        });
    });

</script>