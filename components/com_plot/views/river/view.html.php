<?php

defined('_JEXEC') or die;
require_once JPATH_COMPONENT . '/views/view.php';

class PlotViewRiver extends JViewLegacy
{

    public function display($tpl = null)
    {

        //$this->template = JFactory::getApplication()->getTemplate();
        //$this->templateUrl = JURI::base() . 'templates/' . $app->getTemplate();
        //$this->componentUrl = JURI::base() . 'components/com_plot';

        $app = JFactory::getApplication();

        $this->my = Foundry::user();


        if ($this->my->id) {

            $app->redirect(JRoute::_('index.php?option=com_plot&view=profile&id=' . $this->my->id, false));
        }


        JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_plot/models/');
        $publicationsModel = JModelLegacy::getInstance('publications', 'PlotModel');
        $this->count_publications = $publicationsModel->getCountAllPublications();
        $coursesModel = JModelLegacy::getInstance('courses', 'PlotModel');
        $this->count_courses = $coursesModel->getCountAllCourses();
        $course_author = (array)$coursesModel->getCountAllAuthors();
        $eventsModel = JModelLegacy::getInstance('events', 'PlotModel');
        $this->count_events = $eventsModel->getCountAllEvents();
        $userModel = JModelLegacy::getInstance('user', 'PlotModel');
        $this->count_children = $userModel->getCountAllChildren();
        $this->count_parents = $userModel->getCountAllParents();
        $this->count_registred = $userModel->getCountAllSocialRegisteredUsers();
        $bookModel = JModelLegacy::getInstance('books', 'PlotModel');
        $book_author = (array)$bookModel->getCountAllAuthors();

        $authors = array();

        foreach ($course_author AS $val) {
            $authors[] = $val;
        }
        foreach ($book_author AS $val) {
            $authors[] = $val;
        }

        $this->authors = array_unique($authors);
        $this->setOgMeta();
        return parent::display($tpl);
    }

    private function setOgMeta()
    {
        require_once JPATH_ADMINISTRATOR . '/components/com_plot/plot_social_config.php';
        $metaOgUrl = '<meta property="og:url" content="http://naplotu.com/" />';
        $metaOgTitle = '<meta property="og:title" content="' . plotSocialConfig::get('riverTitle') . '" />';
        $metaOgDescription = '<meta property="og:description" content="' . plotSocialConfig::get('riverDesc') . '" />';
        $metaOgImage = '<meta property="og:image" content="' . JUri::root() . plotSocialConfig::get('riverImagePath') . '?rand=' . rand() . '" />';
        $metaOgImageType = '<meta property="og:image:type" content="image/jpeg" />';
        $metaOgImageWidth = '<meta property="og:image:width" content="' . plotSocialConfig::get('riverImageWidth') . '" />';
        $metaOgImageHeight = '<meta property="og:image:height" content="' . plotSocialConfig::get('riverImageHeight') . '" />';
        $metaOgSiteName = '<meta property="og:site_name" content="' . JFactory::getConfig()->get('sitename') . '" />';
        $metaOgFbAppId = '<meta property="fb:app_id" content="473991812742077" />';
        $metaOgType = '<meta property="og:type" content="website" />';
        $metaOgLocale = '<meta property="og:locale" content="ru_RU" />';
        JFactory::getDocument()->addCustomTag($metaOgTitle . $metaOgSiteName . $metaOgUrl . $metaOgDescription . $metaOgFbAppId . $metaOgType . $metaOgLocale . $metaOgImage . $metaOgImageType . $metaOgImageWidth . $metaOgImageHeight);
        return true;
    }

}
