<ul class="slides">
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('parentSliderSlide1Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('parentSliderSlide1Text'); ?></p>
    </li>

    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('parentSliderSlide3Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('parentSliderSlide3Text'); ?></p>
    </li>
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('parentSliderSlide4Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('parentSliderSlide4Text'); ?></p>
    </li>
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('parentSliderSlide5Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('parentSliderSlide5Text'); ?></p>
    </li>
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('parentSliderSlide6Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('parentSliderSlide6Text'); ?></p>
    </li>
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('parentSliderSlide7Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('parentSliderSlide7Text'); ?></p>
    </li>
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('parentSliderSlide2Img'); ?>"/>

        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('parentSliderSlide2Text'); ?></p>
    </li>
</ul>