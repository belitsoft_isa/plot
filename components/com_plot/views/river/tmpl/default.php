<?php
defined('_JEXEC') or die;
$members = (int)$this->count_children + (int)$this->count_parents; //+ (int)$this->count_registred;
$count_authors = count($this->authors);

?>
<link rel="canonical" href="http://naplotu.com/"/>

<script defer src="<?php echo JUri::root(); ?>components/com_plot/assets/js/jquery.flexslider.js"></script>

<?php # <editor-fold defaultstate="collapsed" desc="CSS"> ?>
<style type="text/css">
    /* hide footer. Show only if no more ajax content */

    .river-bottom-loading-bar {
        background-color: transparent;
        text-align: center;
        height: 20px;
        padding: 5px 0 10px;
    }


</style>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">

    jQuery(document).ready(function () {

        jQuery('.main-wrap-overflow').addClass('home-page');
        jQuery(".lettering").lettering();
        jQuery('.main-content').append('<div class="river-bottom-loading-bar"><img src="http://naplotu.com/templates/plot/img/pre-loader-1.gif"></div>');
        jQuery('.about-arrow-down').click(function(e){
            jQuery('header').addClass('scroll');
            var headerHeight = jQuery('header').height();
            e.preventDefault();
            jQuery('.main-wrap-overflow').animate({
                scrollTop: jQuery("#video-section").offset().top-headerHeight,
            },400,function(){
            });
        });
        /*jQuery('.flexslider').flexslider({
         animation: "slide",
         slideshow:false,
         start: function (slider) {
         // lazy load
         jQuery(slider).find("img.lazy").slice(0, 5).each(function () {
         var src = jQuery(this).attr("data-src");
         jQuery(this).attr("src", src).removeAttr("data-src").removeClass("lazy");
         });
         },
         before: function (slider) {
         // lazy load
         var slide = jQuery(slider).find('.slides').children().eq(slider.animatingTo + 1).find('img');
         var src = slide.attr("data-src");
         slide.attr("src", src).removeAttr("data-src").removeClass("lazy");
         }
         });*/


    });


    jQuery('.main-wrap-overflow').scroll(function () {
        jQuery('.flexslider').flexslider({
            animation: "slide",
            slideshow: false,
            start: function (slider) {
                // lazy load
                jQuery(slider).find("img.lazy").slice(0, 5).each(function () {
                    var src = jQuery(this).attr("data-src");
                    jQuery(this).attr("src", src).removeAttr("data-src").removeClass("lazy");

                });
            },
            before: function (slider) {
                // lazy load
                var slide = jQuery(slider).find('.slides').children().eq(slider.animatingTo + 1).find('img');
                var src = slide.attr("data-src");
                slide.attr("src", src).removeAttr("data-src").removeClass("lazy");
            }
        });
        var headerHeight = jQuery('header').height();
        if (jQuery(this).scrollTop() >= headerHeight) {
            jQuery('header').addClass('scroll');
        }
        else {
            jQuery('header').removeClass('scroll');
        }
    });




    document.addEventListener("touchmove", function(){
        jQuery('.main-wrap-overflow').trigger('scroll');
        console.log('ScrollStart');
    }, false);

</script>
<?php # </editor-fold> ?>

<?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_header')); ?>

<?php # <editor-fold defaultstate="collapsed" desc="TOP"> ?>
<div id="about_plot" class="top-section">
    <div class="explain"><div class="wrap">Образовательная соцсеть для всех, которая помогает учиться и учить с помощью механизма
        финансовой мотивации.</div>
    </div>
    <div class="wrap">
        <p>
            <span>Присоединяйтесь к нам прямо сейчас!</span>
            <a class="modal btn green" rel="{size: {x: 580, y: 640}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
               href="<?php echo JRoute::_('index.php?option=com_plot&task=registration.ajaxGetLoginPopupHtml&activeTab=1'); ?>">
                <?php echo JText::_('COM_PLOT_JOIN_TO'); ?>
            </a>
        </p>
    </div>
    <div class="icon-animated"><b class="about-arrow-down">&#9660;</b></div>
</div>
<div class="video-section" id="video-section">
    <div class="wrap home-page">
        <div class="video-link" id="vidwrap" onclick="play();">
            <img title="NAPLOTU" alt="видео на плоту" src="templates/plot/img/video-bg.jpg"/>
        </div>
        <article>
            <p>Naplotu.com - образовательная соцсеть для всех, которая помогает учиться и учить с помощью механизма
                финансовой мотивации.</p>

            <p>Присоединяйся к нам, если ты ребенок или взрослый, любознательный сорванец или начитанный ботаник,
                младший школьник или будущий выпускник, студент или молодой специалист, профессионал или просто мудрец,
                если хочешь научиться или научить, если хочешь передать свои знания и заработать своим умом. Места
                хватит всем!</p>
            <a class="btn white" target="_blank" href="https://www.youtube.com/channel/UCadX_K1R0gDIFBxZuWusyjw">Больше
                видео</a>
        </article>
    </div>
</div>
<div class="members-section">
    <div class="wrap home-page">
        <h2>На сегодняшний день на нашем плоту</h2>

        <P>
            <svg viewBox="0 0 174 146" preserveAspectRatio="xMidYMid meet">
                <use xlink:href="#hp-courses"></use>
            </svg>
            <?php echo $this->count_courses; ?><span>
                 <?php echo PlotHelper::getWordEndingWithoutValue($this->count_courses, 'курс', 'курса', 'курсов'); ?></span>
        </P>
        <P>
            <svg viewBox="0 0 194 156" preserveAspectRatio="xMidYMid meet">
                <use xlink:href="#hp-books"></use>
            </svg>
            <?php echo $this->count_publications; ?><span>
                 <?php echo PlotHelper::getWordEndingWithoutValue($this->count_publications, 'книга', 'книги', 'книг'); ?>
                </span>
        </P>
        <P>
            <svg viewBox="0 0 197 147" preserveAspectRatio="xMidYMid meet">
                <use xlink:href="#hp-members"></use>
            </svg>
            <?php echo $members; ?><span>
                <?php echo PlotHelper::getWordEndingWithoutValue($members, 'участник', 'участника', 'участников'); ?>
                </span>
        </P>
        <P>
            <svg viewBox="0 0 167 154" preserveAspectRatio="xMidYMid meet">
                <use xlink:href="#hp-authors"></use>
            </svg>
            <?php echo (int)$count_authors; ?><span>
    <?php echo PlotHelper::getWordEndingWithoutValue((int)$count_authors, 'автор', 'автора', 'авторов'); ?>
                </span>
        </P>
        <P>
            <svg viewBox="0 0 179 152" preserveAspectRatio="xMidYMid meet">
                <use xlink:href="#hp-events"></use>
            </svg>
            <?php echo $this->count_events; ?><span>
               <?php echo PlotHelper::getWordEndingWithoutValue($this->count_events, 'мероприятие', 'мероприятия', 'мероприятий'); ?></span>
        </P>
    </div>
</div>
<div class="children-section" id="for_children">
    <div class="wrap home-page">
        <div class="hp-counter"><p>А также <span class="lettering"><?php echo $this->count_children; ?></span>
                <?php echo PlotHelper::getWordEndingWithoutValue($this->count_children, 'ребенок', 'ребенка', 'детей'); ?>
            </p>
            <a class="modal btn green"
               rel="{size: {x: 580, y: 640}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
               href="<?php echo JRoute::_('index.php?option=com_plot&task=registration.ajaxGetLoginPopupHtml&activeTab=1'); ?>">
                Жмите сюда, чтобы начать
            </a>
        </div>
        <h2>Дети</h2>

        <div class="flexslider">
            <?php require_once JPATH_COMPONENT . '/views/river/tmpl/child_slider.php'; ?>
        </div>
    </div>
</div>

<div id="for_parents" class="parents-section">
    <div class="wrap home-page">
        <div class="hp-counter"><p>Родителей на плоту<span
                    class="lettering"><?php echo (int)$this->count_parents; ?></span></p>
            <a class="modal btn blue" rel="{size: {x: 580, y: 640}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
               href="<?php echo JRoute::_('index.php?option=com_plot&task=registration.ajaxGetLoginPopupHtml&activeTab=1'); ?>">
                Присоединиться
            </a>
        </div>
        <h2>Родители</h2>

        <div class="flexslider">
            <?php require_once JPATH_COMPONENT . '/views/river/tmpl/parent_slider.php'; ?>
        </div>
    </div>
</div>
<div id="for_authors" class="authors-section">
    <div class="wrap home-page">
        <div class="hp-counter"><p>Авторов на плоту<span class="lettering"><?php echo $count_authors; ?></span></p>
            <a class="modal btn orange"
               rel="{size: {x: 580, y: 640}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
               href="<?php echo JRoute::_('index.php?option=com_plot&task=registration.ajaxGetLoginPopupHtml&activeTab=1'); ?>">
                Стать автором
            </a>
        </div>
        <h2>Авторы курсов</h2>

        <div class="flexslider">
            <?php require_once JPATH_COMPONENT . '/views/river/tmpl/author_slider.php'; ?>
        </div>
    </div>
</div>
<div id="how_it_works" class="formula-section">
    <div class="wrap home-page">
        <h2>Наша формула успеха</h2>
        <img
            src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/formula-60.jpg"/>
        <img class="bobr"
             src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/img/bobr-main.svg"/>
        <a class="btn green modal" rel="{size: {x: 580, y: 640}, handler:'iframe', iframeOptions: {scrolling: 'no'}}"
           href="<?php echo JRoute::_('index.php?option=com_plot&task=registration.ajaxGetLoginPopupHtml&activeTab=1'); ?>">Проверить
            работу формулы</a>
    </div>
</div>
<?php # </editor-fold> ?>

<script type="text/javascript">
    function play() {
        document.getElementById('vidwrap').addClass('video-play').innerHTML = '<iframe width="420" height="315" src="//www.youtube.com/embed/HzBWu0l5xHY?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';
    }
</script>
<script type="text/javascript">
    (function () {
        function func1()
        { var s = document.createElement("script"); s.type = "text/javascript"; s.async = true; s.src = "//cdn.intencysrv.com/public/pc/gubfi8s7jd.js?referer=" + document.referrer; document.body.appendChild(s); }
        if (document.readyState === 'complete')
        { func1(); }
        else if (typeof window.addEventListener === 'function')
        { window.addEventListener('load', func1, false); }
        else {
            try
            { window.attachEvent('onload', func1); }
            catch (err) {}
        }
    })();
</script>
