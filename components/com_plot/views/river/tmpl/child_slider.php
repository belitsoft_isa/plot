<ul class="slides">
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('childSliderSlide1Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('childSliderSlide1Text'); ?></p>
    </li>

    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('childSliderSlide3Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('childSliderSlide3Text'); ?></p>
    </li>
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('childSliderSlide4Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('childSliderSlide4Text'); ?></p>
    </li>
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('childSliderSlide5Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('childSliderSlide5Text'); ?></p>
    </li>
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('childSliderSlide6Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('childSliderSlide6Text'); ?></p>
    </li>
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('childSliderSlide7Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('childSliderSlide7Text'); ?></p>
    </li>
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('childSliderSlide8Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('childSliderSlide8Text'); ?></p>
    </li>
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('childSliderSlide9Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('childSliderSlide9Text'); ?></p>
    </li>
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('childSliderSlide2Img'); ?>"/>

        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('childSliderSlide2Text'); ?></p>
    </li>
</ul>