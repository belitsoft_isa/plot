<ul class="slides">
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('authorSliderSlide1Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('authorSliderSlide1Text'); ?></p>
    </li>
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('authorSliderSlide2Img'); ?>"/>

        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('authorSliderSlide2Text'); ?></p>
    </li>
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('authorSliderSlide3Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('authorSliderSlide3Text'); ?></p>
    </li>
    <li>
        <img class="lazy" src="<?php echo JUri::root(); ?>templates/plot/images/ajax-loader.gif"
             data-src="<?php echo JUri::root(); ?>templates/plot/img/slider/<?php echo plotGlobalConfig::getVar('authorSliderSlide4Img'); ?>"/>
        <p class="flex-caption"><?php echo plotGlobalConfig::getVar('authorSliderSlide4Text'); ?></p>
    </li>

</ul>