<?php
defined('_JEXEC') or die;
?>

<link href="<?php echo $this->componentUrl; ?>/views_parent/course/tmpl/default.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/assets/js/plot.js'; ?>"></script>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">

    function createMsg(msg){
        SqueezeBox.initialize({
            size: {x: 300, y: 150}
        });
        var str = '<div id="enqueued-message">'+msg+'</div>';
        SqueezeBox.setContent('adopt', str);
        SqueezeBox.resize({x: 300, y: 150});
    }

function deleteEvent(that) {
    var event_id = jQuery(that).attr('data-id');
    jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_plot&task=events.deleteEvent",
        data: {
            'event_id': event_id
        },
        success: function (response) {
            if (response) {
                window.location.href="<?php echo JRoute::_('index.php?option=com_plot&view=events'); ?>"
            } else {
                alert('error');
            }
        }
    });
}

function eventRemoveConfirm(eventId){
    SqueezeBox.open('<?php echo  JRoute::_("index.php?option=com_plot&task=photos.deleteConfirm")?>&entity=event&id='+eventId, {size:{x:300, y:150}, handler:'ajax'});
}

function subscribeEvent(that) {
    var event_id = jQuery(that).attr('data-id');
    jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_plot&task=events.sudscribeEvent",
        data: {
            'event_id': event_id
        },
        success: function (response) {
            if (response) {
                jQuery('.info-bottom').html('<button class="test-button" onclick="removeSubscribeEvent(this); return false;" data-id="'+event_id+'"><svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMinYMin meet"><use xlink:href="#lamp-meeting"></use></svg>Отменить подписку на событие</button>');

            } else {
                alert('error');
            }
        }
    });
}

function removeSubscribeEvent(that) {
    var event_id = jQuery(that).attr('data-id');

    jQuery.ajax({
        type: "POST",
        url: "index.php?option=com_plot&task=events.rejectEvent",
        data: {
            'event_id': event_id
        },
        success: function (response) {
            if (response) {
                jQuery('.info-bottom').html('<button class="test-button" onclick="subscribeEvent(this); return false;" data-id="'+event_id+'"><svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMinYMin meet"><use xlink:href="#lamp-meeting"></use></svg>Записаться</button>');

            } else {
                alert('error');
            }
        }
    });
}

</script>
<?php # </editor-fold> ?>
    
<?php # <editor-fold defaultstate="collapsed" desc="CSS"> ?>
<style type="text/css">
.more {
    margin: 5px;
}    
.info-board .course-category {
    cursor: default;
}
.child-library .course-image {
    max-width: 230px;
}

</style>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="MAIN"> ?>
<main class="child-library">
    
    <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_header') ); ?>

    <div class="wrap main-wrap child-library">
        <div class="add-left aside-left child-library"></div>
        <div class="add-right aside-right child-library"></div>
        <section class="child-library">
            <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_top_menu') ); ?>
            <div class="info-board child-library">
                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=events');?>"class="link-back hover-shadow">
                    Вернуться
                    <svg viewBox="0 0 22.9 46" preserveAspectRatio="xMinYMin meet" class="butterfly">
                        <use xlink:href="#butterfly"></use>
                    </svg>
                </a>
                <svg viewBox="0 0 37.1 38.9" preserveAspectRatio="xMinYMin meet" class="leaf">
                    <use xlink:href="#leaf"></use>
                </svg>
                <object name="bobr" data="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/img/bobr-first-level.svg" type="image/svg+xml"></object>
                <div class="info-top">

                </div>
                <div class="info-bottom">
                    <?php if ($this->event) {

                    ?>
                        <?php
                        if(PlotHelper::isNotOldDate($this->event->end_date)) {
                            if ((int)PlotHelper::chechEventSubscription($this->event->id)) {
                                if ((int)PlotHelper::isOwnerEvent($this->event->id)) {
                                    ?>
                                    <button class="test-button" onclick="eventRemoveConfirm(<?php echo  $this->event->id; ?>); return false;"
                                            data-id="<?php echo $this->event->id; ?>">
                                        <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMinYMin meet">
                                            <use xlink:href="#lamp-meeting"></use>
                                        </svg>
                                        <?php echo JText::_('COM_PLOT_REMOVE_EVENT'); ?></button>
                                <?php
                                } else {
                                    ?>
                                    <button class="test-button"
                                            onclick="removeSubscribeEvent(this); return false;"
                                            data-id="<?php echo $this->event->id; ?>">
                                        <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMinYMin meet">
                                            <use xlink:href="#lamp-meeting"></use>
                                        </svg>
                                        <?php echo JText::_('COM_PLOT_REMOVE_SUBSCRIBE_EVENT'); ?></button>
                                <?php
                                }

                            } else {
                                if(plotUser::factory()->id){
                                    ?>
                                    <button class="test-button" onclick="subscribeEvent(this);return false;"
                                            data-id="<?php echo $this->event->id; ?>">
                                        <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMinYMin meet">
                                            <use xlink:href="#lamp-meeting"></use>
                                        </svg>
                                        <?php echo JText::_('COM_PLOT_SUBSCRIBE_ON_EVENT'); ?></button>
                                    <?php
                                }else {
                                    ?>
                                    <button class="test-button" onclick="createMsg('<?php echo JText::_("COM_PLOT_LOGIN_FIRST"); ?>'); return false;"
                                            data-id="<?php echo $this->event->id; ?>">
                                        <svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMinYMin meet">
                                            <use xlink:href="#lamp-meeting"></use>
                                        </svg>
                                        <?php echo JText::_('COM_PLOT_SUBSCRIBE_ON_EVENT'); ?></button>
                                <?php
                                }
                            }
                        } ?>
                    <?php } ?>
                </div>
            </div>
        </section>
        <section class="my-progress child-library one-elem">
            <div class="my-progress-wrapper">
                <h3>Встреча: <?php echo $this->event->title; ?></h3>
                <div class="jcarousel-wrapper my-meeting child-one-elem">
                    <?php if ($this->event) {
                ?>
                        <a class="thumbnail" href="#">
                            <figure>
                                <?php  if (file_exists(JPATH_SITE . '/images/com_plot/events/' .  $this->event->user_id .  '/thumb/'.$this->event->img)){
                                    ?>
                                    <img src="<?php echo JUri::root() .  'images/com_plot/events/' .  $this->event->user_id .  '/thumb/'.$this->event->img; ?>"/>
                                <?php }elseif(file_exists(JPATH_SITE . '/images/com_plot/events/' .  $this->event->user_id .  '/'.$this->event->img)){ ?>
                                    <img src="<?php echo JUri::root() .  'images/com_plot/events/' .  $this->event->user_id .  '/'.$this->event->img; ?>"/>
                                    <?php
                                }else{
                                    ?>
                                    <img src="<?php echo JUri::root().'images/com_plot/def_meeting.jpg' ?>"/>
                                <?php
                                }?>
                            </figure>
                            <div class="wrapper" id="wrap-btn-event-<?php echo $this->event->id; ?>">

                            </div>
                        </a>

                        <div class="bonus">
                            <em class="pin-btn green"></em>
                            <em class="pin-btn green"></em>
                            <p>Прими участие и получи</p>
                            <div>
                                <i class="elka">+20
                                    <svg viewBox="0 0 14.6 23.1" preserveAspectRatio="xMinYMin meet"><use xlink:href="#elka-interest"></use></svg>
                                </i>
                            </div>
                        </div>
                        <div class="elem-descr">
                            <div class="i-b">
                                <h6><?php echo plotUser::factory($this->event->user_id)->name ?></h6>
                                <i>Дата проведения:</i>
                                <?php if (PlotHelper::is_date($this->event->start_date)) { ?>
                                    <span> <?php echo PlotHelper::russian_date(JHtml::date($this->event->start_date, 'd.m.Y')); ?> </span>
                                <?php } ?><hr/>
                                        <span class="quantity">
                                            <i>Количество участников:</i>
                                            <?php echo PlotHelper::countEventSubscription($this->event->id); ?>
                                            <svg viewBox="0 0 23 23" preserveAspectRatio="xMinYMin meet">
                                                <use xlink:href="#people"></use>
                                            </svg>
                                        </span>
                            </div>
                            <div class="i-b">
                                <i>Время проведения:</i>
                                <div class="counter">
                                    <p class="start-time">
                                        <b>Время начала</b>
                                        <?php if (PlotHelper::is_date($this->event->start_date)) { ?>
                                            <time><?php echo JHtml::date($this->event->start_date, 'H:m'); ?></time>
                                        <?php } ?>
                                    </p>

                                    <?php
                                    if (PlotHelper::is_date($this->event->start_date) && PlotHelper::is_date($this->event->end_date)) {
                                        echo  PlotHelper::renderEventDuration($this->event);

                                    }
                                    ?>

                                </div>
                                <i>Место проведения:</i>
                                <span><?php  echo $this->event->place; ?></span><hr/>
                            </div>
                            <div><i>Подробное описание:</i>
                                <?php echo $this->event->description; ?>
                            </div>
                        </div>

                <?php } ?>

                </div>
            </div>
        </section>
    </div>
</main>
<?php # </editor-fold> ?>
