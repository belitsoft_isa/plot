<?php
defined('_JEXEC') or die;

//$u = plotUser::factory();
#$u->addChild(889);
//pre($u->getChildrenIds());

JHtml::_('bootstrap.framework');
JHtml::_('behavior.tooltip');

require_once(JPATH_SITE . '/components/com_plot/helpers/html5fbfront.php');

?>

<script src="<?php echo JURI::base(); ?>media/jui/js/bootstrap.min.js"></script>


<div>
    <?php

    $jinput = JFactory::getApplication()->input;
    $Itemid = $jinput->get('Itemid', 0, 'INT');


    $doc = JFactory::getDocument();

    $app = JFactory::$application;
    $jinput = $app->input;

    require_once (JPATH_SITE . '/components/com_html5flippingbook/libs/Mobile_Detect.php');
    $detectMobile = new Mobile_Detect();

    // Exclude tablets.
    $isMobile = FALSE;
    if ($detectMobile->isMobile() && !$detectMobile->isTablet()) {
        $isMobile = TRUE;
    }

    $doc->addScriptDeclaration('ht5popupWindow = function (a, b, c, d, f) { window.open(a, b, "height=" + d + ",width=" + c + ",top=" + (screen.height - d) / 2 + ",left=" + (screen.width - c) / 2 + ",scrollbars=" + f + ",resizable").window.focus() };');
    $doc->addScript(JURI::root() . 'components/com_html5flippingbook/assets/js/jquery.cookie.min.js');

    JText::script('COM_HTML5FLIPPINGBOOK_FE_DISPLAY_UNREAD_PUBL');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_DISPLAY_READ_PUBL');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_ACTION_READ_TIP');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_ACTION_REMOVE_READ');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_ACTION_ERROR_USER');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_ACTION_ERROR_ACTION');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_MAILTO_ERROR');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_JSSHARE_PUB_ERROR');
    JText::script('COM_HTML5FLIPPINGBOOK_FE_JSSHARE_FRIEND_ERROR');

    ?>

    <div class="slide-navi" style="display: none">
        <a href="javascript: void(0)" class="htmlfb5-sl-prev">Prev</a>
        <a href="javascript: void(0)" class="htmlfb5-sl-next">Next</a>
    </div>


    <div id="html5flippingbook" class="html5fb-profile">

        <a href='<?php echo JRoute::_("index.php?option=com_plot&view=profile&id=".$this->my->id); ?>'><?php echo JText::_('COM_PLOT_BACK');?></a>

        <div class="bookshelf">
            <div class="shelf">

                <div class="slide-navi" style="display: none">
                    <a href="javascript: void(0)" class="htmlfb5-sl-prev">Prev</a>
                    <a href="javascript: void(0)" class="htmlfb5-sl-next">Next</a>
                </div>
                category <?php echo $this->book->c_title; ?>
                <?php echo HTML5FlippingBookFrontHelper::startReadBookShelf('reading', $this->book, $isMobile, 1, $this->config, true);?>
                <div style="float: left"><?php //echo $this->book->c_pub_descr; ?></div>

                <?php
                $jinput = JFactory::getApplication()->input;
                $id = $jinput->get('id', 0, 'INT');
                if ($id) {
                    $user = Foundry::user($id);
                } else {
                    $user = Foundry::user();
                }

                if ($user->id == Foundry::user()->id) {
                    $percent=0;
                    if($this->book->params){
                        $params=json_decode($this->book->params,true);
                        $count_pages=HTML5FlippingBookFrontHelper::getCountPages($this->book->c_id);
                        $percent=HTML5FlippingBookFrontHelper::getPersent($count_pages,count($params));
                    }
                    $percentOpenTest=plotGlobalConfig::getVar('minPersentForOpenBookTest');

                    if ($this->quiz_id && ($percentOpenTest<=$percent || $this->book->read==1)) {
                        ?>
                        <div>
                            <a href='<?php echo JRoute::_("index.php?option=com_joomlaquiz&view=quiz&quiz_id=" . $this->quiz_id . "&Itemid=" . $Itemid); ?>'><?php echo JText::_('COM_PLOT_GO_TO_TEST');?></a>
                        </div>
                    <?php
                    }
                }



                ?>
                <?php
                $jinput = JFactory::getApplication()->input;
                $id = $jinput->get('id', 0, 'INT');
                if ($id) {
                    $user = Foundry::user($id);
                } else {
                    $user = Foundry::user();
                }
                if ($user->id == Foundry::user()->id) {
                    if ($this->checkBook) {
                        if ($this->bookPoints) {
                            echo JText::_("COM_PLOT_AFTER_READ") . ' ' . $this->bookPoints;
                        }

                        if ($this->bookPrice) {
                            echo JText::_("COM_PLOT_AFTER_READ") . ' ' . $this->bookPrice . '(money)';
                        }

                    }
                }

                ?>
<?php echo  $this->book->c_pub_descr?>
            </div>
        </div>

        <div class="content-loading">
            <img
                src="<?php echo JURI::root() . 'components/com_html5flippingbook/assets/images/progress.gif'; ?>"
                alt="Loading..."/>
        </div>
    </div>


    <script type="text/javascript">

        jQuery(document).ready(function () {
            jQuery('.hasTooltip').tooltip({

            });
        });
        var user = <?php echo Foundry::user()->id; ?>;
        var liveSite = '<?php echo JUri::root();?>';
    </script>
    <script type="text/javascript"
            src="<?php echo JURI::root() . 'components/com_html5flippingbook/assets/js/'; ?>profile.min.js"></script>
    <script type="text/javascript"
            src="<?php echo JURI::root() . 'components/com_html5flippingbook/assets/js/'; ?>jquery.actual.min.js"></script>

    <?php
    if ($this->config->social_email_use) {
        echo $this->emaillayout->render(NULL);
    }

    if ($this->config->social_jomsocial_use) {
        $data['friends'] = $this->userFriends;
        echo $this->sharelayout->render($data);
    }
    ?>



