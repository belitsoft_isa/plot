<?php

defined('_JEXEC') or die;
require_once JPATH_COMPONENT . '/views/view.php';

require_once(JPATH_SITE . '/administrator/components/com_html5flippingbook/libs/VarsHelper.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/conversations.php');
require_once(JPATH_SITE . '/administrator/components/com_easysocial/models/friends.php');

class PlotViewReadbook extends JViewLegacy
{

    protected $extension = 'com_plot';
    protected $defaultPageTitle = 'COM_PLOT_DEFAULT_PAGE_TITLE';
    protected $viewName = 'plot';

    public function display($tpl = null)
    {
        $app = JFactory::getApplication();
        $this->templateUrl = JURI::base() . 'templates/' . $app->getTemplate();
        $this->componentUrl = JURI::base() . 'components/com_plot';

        $this->my = plotUser::factory();

        $document = JFactory::$document;

        $document->addStyleSheet(JURI::root().'components/com_html5flippingbook/assets/css/' . 'html5flippingbook.css');
        $document->addStyleSheet(JURI::root().'components/com_html5flippingbook/assets/css/' . 'font-awesome.min.css');
        $document->addScript(JURI::root().'components/com_plot/assets/js/plot.js');

        $this->emaillayout = new JLayoutFile('email', $basePath = JPATH_COMPONENT .'/layouts');
        $this->sharelayout = new JLayoutFile('share', $basePath = JPATH_COMPONENT .'/layouts');


        $this->config = $this->GetConfig();
        $this->book = $this->get('Book');
        $this->checkBook=$this->get('checkBook');

        if($this->checkBook){
            $this->bookPoints=$this->get('BookPoints');
            $this->bookPrice=$this->get('BookPrice');
        }
        $this->quiz_id=(int)$this->get('QuizId');

       if ($this->config->social_jomsocial_use)
        {
            $this->userFriends = $this->get('UserJSFriends');
        }

        $this->bookBS = $this->get('Book');

        parent::display($tpl);
    }

    protected function prepareDocument()
    {
        parent::prepareDocument();
        parent::addFeed();
    }


    public function getConfig()
    {
        $db = JFactory::getDBO();

        $query = "SELECT * FROM `#__html5fb_config`" .
            " ORDER BY `setting_name`";
        $db->setQuery($query);
        $rows = $db->loadObjectList();

        $config = (object)array();

        if (isset($rows)) {
            foreach ($rows as $row) {
                $config->{$row->setting_name} = $row->setting_value;
            }
        }

        return $config;
    }
}
