<?php
defined('_JEXEC') or die;

class PlotViewK2Article extends PlotViewLegacy
{

    public function display($tpl = null)
    {   
        $id = JRequest::getVar('id', 0);
        $this->k2Item = PlotHelper::getk2item($id);
        if (!$this->k2Item) {
            $this->redirect404();
        }
        $this->k2Item->introtext = JHtml::_('content.prepare', $this->k2Item->introtext);
        $this->setOgMeta();
        parent::display($tpl);
    }
    
    private function setOgMeta()
    {
        require_once JPATH_ADMINISTRATOR.'/components/com_plot/plot_social_config.php';
        $metaOgUrl = '<meta property="og:url" content="'.PlotHelper::getAbsRoutedUri('index.php?option=com_plot&view=k2article&id='.$this->k2Item->id.'/').'" />';
        $metaOgTitle = '<meta property="og:title" content="'.$this->k2Item->title.'" />';
        $metaOgDescription = '<meta property="og:description" content="'.strip_tags($this->k2Item->introtext).'" />';
        $metaOgImage = '<meta property="og:image" content="'.JUri::root().plotSocialConfig::get('riverImagePath').'?rand='.rand().'" />';
        $metaOgImageType = '<meta property="og:image:type" content="image/jpeg" />';
        $metaOgImageWidth = '<meta property="og:image:width" content="'.plotSocialConfig::get('riverImageWidth').'" />';
        $metaOgImageHeight = '<meta property="og:image:height" content="'.plotSocialConfig::get('riverImageHeight').'" />';
        $metaOgSiteName = '<meta property="og:site_name" content="'.JFactory::getConfig()->get('sitename').'" />';
        $metaOgFbAppId = '<meta property="fb:app_id" content="473991812742077" />';
        $metaOgType = '<meta property="og:type" content="website" />';
        $metaOgLocale = '<meta property="og:locale" content="ru_RU" />';
        JFactory::getDocument()->addCustomTag($metaOgTitle.$metaOgSiteName.$metaOgUrl.$metaOgDescription.$metaOgFbAppId.$metaOgType.$metaOgLocale.$metaOgImage.$metaOgImageType.$metaOgImageWidth.$metaOgImageHeight);
        return true;
    }
    
}
