<?php
defined('_JEXEC') or die;
jimport('joomla.html.html.bootstrap');
?>

<!--<link type="text/css" href="--><?php //echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate() . '/css/style.css'; ?><!--" rel="stylesheet">-->
<script src="<?php echo JUri::root(); ?>media/jui/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/jui/js/jquery.ui.core.min.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/mootools-core.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/core.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo JUri::root().'components/com_plot/assets/js/plot.js'; ?>"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/mootools-more.js" type="text/javascript"></script>
<script src="<?php echo JUri::root(); ?>media/system/js/modal.js" type="text/javascript"></script>

<script type="text/javascript">
function findByAttributeValue(attribute, value)
{
    var allElements = window.parent.document.getElementsByTagName('*');

    for (var i = 0; i < allElements.length; i++)
    {
        if (allElements[i].getAttribute(attribute) == value)
        {
            return allElements[i];
        }
    }
}

function photoRemove(photoId) {
   jQuery.post('index.php?option=com_plot&task=photos.removePhoto', {photoId: photoId}, function (response) {
       var data = jQuery.parseJSON(response),
           photoCount = window.parent.document.getElementById('count_all_photos');
        if (data.status) {
            errorPlotMessage(data.msg);
            findByAttributeValue("data-photo-id",photoId).remove();
            jQuery(photoCount).html( (jQuery(photoCount).html().match(/[0-9]+/g)[0] - 1) + ' фото' );
            window.parent.document.getElementById('sbox-btn-close').click();
        } else {
            errorPlotMessage(data.msg);
        }
    });
}

function videoRemove(videoId) {
    jQuery.post('index.php?option=com_plot&task=photos.removeVideo', {videoId: videoId}, function (response) {
        var data = jQuery.parseJSON(response),
            videoCount = window.parent.document.getElementById('count_all_videos');
        if (data.status) {
            errorPlotMessage(data.msg);
            findByAttributeValue("data-video-id",videoId).remove();
            jQuery(videoCount).html( (jQuery(videoCount).html().match(/[0-9]+/g)[0] - 1) + ' видео' );
            window.parent.document.getElementById('sbox-btn-close').click();
        } else {
            errorPlotMessage(data.msg);
        }
    });
}

function eventRemove(eventId) {
   jQuery.ajax({
       type: "POST",
       url: "index.php?option=com_plot&task=events.deleteEvent",
       data: {
           'eventId': eventId
       },
       success: function (response) {
           var data = jQuery.parseJSON(response);
           if (data.status) {
               window.location.href="<?php echo JRoute::_('index.php?option=com_plot&view=events'); ?>"
           } else {
               errorPlotMessage(data.msg);
           }
       }
   });
}

function certificateRemove(certificateId) {
   jQuery.post('index.php?option=com_plot&task=photos.removeCertificate', {certificateId: certificateId}, function (response) {
       var data = jQuery.parseJSON(response),
           certificatesCount = window.parent.document.getElementById('count_all_certificates');
       if (data.status) {
           errorPlotMessage(data.msg);
           findByAttributeValue("data-certificate-id",certificateId).remove();
           jQuery(certificatesCount).html(jPlot.helper.getNumEnding(jQuery(certificatesCount).html().match(/[0-9]+/g)[0] - 1, ['сертификат', 'сертификата', 'сертификатов']));
           window.parent.document.getElementById('sbox-btn-close').click();
       } else {
           errorPlotMessage(data.msg);
       }
   });
}

function eventListRemove(eventId) {
    jQuery.post('index.php?option=com_plot&task=events.deleteEvent', {eventId: eventId}, function (response) {
        var data = jQuery.parseJSON(response);
        if (data.status) {
            errorPlotMessage(data.msg);
            jQuery('#plot-event-id-'+eventId).remove();
        } else {
            errorPlotMessage(data.msg);
        }
    });
}

function errorPlotMessage(error_message) {
    SqueezeBox.initialize({
        size: {x: 300, y: 150}
    });
    var str = '<div id="enqueued-message">' + error_message + '</div>';
    SqueezeBox.setContent('adopt', str);
    SqueezeBox.resize({x: 300, y: 150});
    SqueezeBox.initialize({
        size: {x: 744, y: 525}
    });
}
</script>



<div id="complain-form" style="font-size: 1.2rem;" <?php echo  (plotUser::factory()->isParent()) ? 'class="parent-profile"' : 'class="child-profile"'; ?>>
    <fieldset>
        <label>Вы действительно хотите удалить?</label>
        <button onclick="<?php echo $this->entity;?>Remove(<?php echo $this->id; ?>)" class="button accept hover-shadow confirm">
            <span>Да</span>
        </button>
        <button class="button cancel-act hover-shadow" onclick="window.parent.document.getElementById('sbox-btn-close').click();">
            <span>Нет</span>
        </button>
    </fieldset>
</div>
