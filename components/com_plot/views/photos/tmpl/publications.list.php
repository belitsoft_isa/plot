<?php
defined('_JEXEC') or die;
?>

<?php foreach ($this->publications AS $publication) { ?>
<li class="stream-item">
    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publication&id='.$publication->c_id); ?>">
        <h6>
            <i><svg class="star" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24.3 23"><use xlink:href="#star"/></svg></i>
            <p class="stream-title"><?php echo $publication->c_title; ?></p>
        </h6>
        <!--<data><?php echo $publication->finished_date;?></data>-->
        <div>
            <figure class="book">
                <img src="<?php echo $publication->thumbnailUrl; ?>" />
                <i class="activity-icons">
                    <svg style="fill:url(#svg-gradient); stroke:url(#svg-gradient);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 32 32"><use xlink:href="#book"/></svg>
                </i>
            </figure>
        </div>
    </a>
</li>
<?php } ?>

