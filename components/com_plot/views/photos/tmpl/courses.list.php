<?php
defined('_JEXEC') or die;
?>

<?php foreach ($this->courses AS $course) { ?>
<li>
    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=course&id='.$course->id); ?>">
        <h6>
            <i><svg class="star" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24.3 23"><use xlink:href="#star"/></svg></i>
            <span class="action-title"></span>
            <p class="stream-title"><?php echo $course->course_name; ?></p>
        </h6>
        <!--<data><?php echo $course->finished_date; ?></data>-->
        <div>
            <figure>
                <img src="<?php echo JFile::exists(JPATH_ROOT.'/'.$course->image) ? JUri::root().$course->image : JUri::root() . 'images/com_plot/def_course.jpg'; ?>" />
                <i class="activity-icons"><svg style="fill:url(#svg-gradient);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 32 32"><use xlink:href="#academic-hat"/></svg></i>
            </figure>
        </div>
        <div>
            <blockquote>
                <?php echo PlotHelper::cropStr(strip_tags($course->course_description),  plotGlobalConfig::getVar('photosChildMaxCourseDescriptionSymbols'));?>
            </blockquote>
        </div>
    </a>
</li>
<?php } ?>