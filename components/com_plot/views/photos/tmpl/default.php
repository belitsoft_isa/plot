<?php
defined('_JEXEC') or die;
JHtml::_('behavior.tooltip');
JHtml::_('formbehavior.chosen', 'select');
$id = JFactory::getApplication()->input->get('id', 0, 'INT');
?>
<script type="text/javascript" src="<?php echo JUri::root() . 'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JURI::root();?>components/com_plot/assets/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate() . '/js/jquery.jscrollpane.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate() . '/js/jcarousel.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'templates/' . JFactory::getApplication()->getTemplate(); ?>/js/jcarousel.basic.js"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/assets/js/plot.js'; ?>"></script>
<script src="<?php echo JURI::base() . 'components/com_plot'; ?>/libraries/scrolling_ajax_pagination/javascript.js"></script>
<script src="<?php echo JURI::base() . 'components/com_plot'; ?>/views/photos/tmpl/default.js"></script>

<script src="<?php echo $this->componentUrl; ?>/assets/js/plot.js"></script>
<script type="text/javascript" src="<?php echo $this->componentUrl; ?>/views/profile/tmpl/default.js"></script>
<script src="<?php echo JURI::base(); ?>components/com_plot/assets/js/video.js"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/libraries/preview_master_plugin/jquery.preimage.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/libraries/jquery_input_mask_plugin/jquery.mask.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'components/com_plot/assets/js/plotEssay.js'; ?>"></script>
<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">

function setPhotoAxaxScrollPagination() {
    var widthItem = 182,
        heightItem = 409;

    removeSearchActiveClassTab();
    setActiveTab('link-child-photos', 'photos-list');

    var ajaxScrollData = {
        search: jQuery("#plot-search").val(),
        screenHeight: parseInt(screen.height) - parseInt(jQuery('div.parent-profile').height()),
        screenWidth: jQuery('#photos-list').outerWidth(),
        itemHeight: heightItem,
        itemWidth: widthItem,
        id:<?php echo $id; ?>
    };

    jQuery(window).unbind('scroll');

    jQuery('#photos-list').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('childPhotoCount'); ?>,
        offset: 0,
        error: '',
        delay: 10,
        scroll: true,
        postUrl: 'index.php?option=com_plot&task=photos.ajaxPhotosLoadMore',
        appendDataTo: 'photos-list',
        userData: ajaxScrollData

    });

    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');


    });

}

function setVideosAxaxScrollPagination() {
    var widthItem = 182,
        heightItem = 409;
    removeSearchActiveClassTab();
    setActiveTab('link-child-videos', 'child-videos');

    var ajaxScrollData = {
        screenHeight: parseInt(screen.height) - parseInt(jQuery('div.parent-profile').height()),
        screenWidth: jQuery('#child-videos').outerWidth(),
        itemHeight: heightItem,
        itemWidth: widthItem,
        id: <?php echo $id; ?>
    };

    jQuery(window).unbind('scroll');

    jQuery('#child-videos').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('childVideoCount'); ?>,
        offset: 0,
        error: '',
        delay: 10,
        scroll: true,
        postUrl: 'index.php?option=com_plot&task=photos.ajaxVideosLoadMore',
        appendDataTo: 'child-videos',
        userData: ajaxScrollData,
        afterLoad: {
            afterLoadAction:function(){
                social();
            }
        }
    });

    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });
    social();
}

function setCertificatesAxaxScrollPagination() {
    var widthItem = 182,
        heightItem = 409;
    removeSearchActiveClassTab();
    setActiveTab('link-child-certificates', 'child-certificates');

    var ajaxScrollData = {
        screenHeight: parseInt(screen.height) - parseInt(jQuery('div.parent-profile').height()),
        screenWidth: jQuery('#child-certificates').outerWidth(),
        itemHeight: heightItem,
        itemWidth: widthItem,
        id:<?php echo $id; ?>
    };

    jQuery(window).unbind('scroll');

    jQuery('#child-certificates').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('childCertificateCount'); ?>,
        offset: 0,
        error: '',
        delay: 10,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=photos.ajaxCertificatesLoadMore'); ?>',
        appendDataTo: 'child-certificates',
        userData: ajaxScrollData,
        afterLoad: {
            afterLoadAction:function(){
                social();
            }
        }
    });

    jQuery(window).scroll(function(){
        jPlotUp.Arrow.ShowOrHide(this,'positionCameras');
    });

}

function photoRemoveConfirm(photoId) {
    SqueezeBox.open('<?php echo  JRoute::_("index.php?option=com_plot&task=photos.deleteConfirm&entity=photo", 0)?>'+'&id='+photoId, {size:{x:300, y:150}, handler:'ajax'});
}

function videoRemoveConfirm(videoId) {
    SqueezeBox.open('<?php echo  JRoute::_("index.php?option=com_plot&task=photos.deleteConfirm&entity=video", 0)?>'+'&id='+videoId, {size:{x:300, y:150}, handler:'ajax'});
}

function certificateRemoveConfirm(certificateId){
    SqueezeBox.open('<?php echo  JRoute::_("index.php?option=com_plot&task=photos.deleteConfirm&entity=certificate", 0)?>'+'&id='+certificateId, {size:{x:300, y:150}, handler:'ajax'});
}

function ajaxLoadCourses() {
    var courseFilterData = {
        userId: '<?php echo JRequest::getInt('id', plotUser::factory()->id);?>'
    };
    jQuery.post(
        'index.php?option=com_plot&task=photos.ajaxGetCourses',
        {userData: courseFilterData},
        function(response){
            if (response) {
                var data = jQuery.parseJSON(response);
                jQuery('#child-courses').append(data.renderedCourses);
                jQuery('#count_all_courses').html(jPlot.helper.getWordEnding(data.countCourses, 'курс', 'курса', 'курсов'));
            } else {
                jQuery('#count_all_courses').html('0 курсов');
            }
            jQuery('#loading-bar').hide();
        }
    );
    jQuery('#child-courses').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('photosPageChildCoursesCount'); ?>,
        offset: <?php echo plotGlobalConfig::getVar('photosPageChildCoursesCount'); ?>,
        error: '',
        delay: 10,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=photos.ajaxGetCourses'); ?>',
        appendDataTo: 'child-courses',
        userData: courseFilterData,
        afterLoad: {
            afterLoadAction:function(){
                social();
            }
        }
    });
    social();
}

function ajaxLoadPublications() {
    var courseFilterData = {
        userId: '<?php echo JRequest::getInt('id', plotUser::factory()->id);?>'
    };
    jQuery.post(
        'index.php?option=com_plot&task=photos.ajaxGetPublications',
        {userData: courseFilterData},
        function(response){
            if (response) {
                var data = jQuery.parseJSON(response);
                jQuery('#child-publications').append(data.renderedPublications);
                jQuery('#count_all_publications').html(jPlot.helper.getWordEnding(data.countPublications, 'книга', 'книги', 'книг'));
                jQuery('#loading-bar').hide();
            } else {
                jQuery('#loading-bar').hide();
                jQuery('#count_all_publications').html('0 книг');
            }
        }
    );
    jQuery('#child-publications').scrollPagination({
        nop: <?php echo plotGlobalConfig::getVar('photosPageChildPublicationsCount'); ?>,
        offset: <?php echo plotGlobalConfig::getVar('photosPageChildPublicationsCount'); ?>,
        error: '',
        delay: 10,
        scroll: true,
        postUrl: '<?php echo JRoute::_('index.php?option=com_plot&task=photos.ajaxGetPublications'); ?>',
        appendDataTo: 'child-publications',
        userData: courseFilterData,
        afterLoad: {
            afterLoadAction:function(){
                social();
            }
        }

    });

}

function ajaxLoadEvents() {
    var courseFilterData = {
        userId: '<?php echo JRequest::getInt('id', plotUser::factory()->id);?>'
    };
    jQuery.post(
        'index.php?option=com_plot&task=photos.ajaxGetEvents',
        {userData: courseFilterData},
        function(response){
            if (response) {
                var data = jQuery.parseJSON(response);
                jQuery('#child-events').append(data.renderedEvents);
                jQuery('#count_all_events').html(jPlot.helper.getWordEnding(data.countEvents, 'думалка', 'думалки', 'думалок'));
                jQuery('#loading-bar').hide();
            } else {
                jQuery('#loading-bar').hide();
                jQuery('#count_all_events').html('0 думалок');
            }
        }
    );
    social();
}

function fixFFBugWithFavicon() {
    jQuery('head link[href="<?php echo JUri::root(true);?>/templates/plot/favicon.ico"]').attr('href', '<?php echo JUri::root(true);?>/templates/plot/favicon.ico');
}


document.addEventListener("touchmove", function(){
    jQuery('.main-wrap-overflow').trigger('scroll');
    console.log('ScrollStart');
}, false);
</script>
<?php # </editor-fold> ?>

<?php # <editor-fold defaultstate="collapsed" desc="CSS"> ?>
<style type="text/css">
    .com_plot {
        color: #FCFCFC;
    }
    .more {
        margin: 5px;
    }
</style>
<?php if ($this->isParentSeeToOtherChild || $this->isChildSeeToOtherChild) { ?>
<style type="text/css">
.child-profile section, .child-library section {
    padding-top: 140px;
}
</style>
<?php } ?>
<?php # </editor-fold> ?>

<input type="hidden" id="photos-count" value="<?php echo PlotHelper::declension((int)$this->total_photos, array('фото', 'фото', 'фото')); ?>" />
<input type="hidden" id="videos-count" value="<?php echo PlotHelper::declension((int)$this->total_videos, array('видео', 'видео', 'видео')); ?>" />
<input type="hidden" id="certificates-count" value="<?php echo PlotHelper::declension((int)$this->total_certificates , array('сертификат', 'сертификата', 'сертификатов')); ?>" />

<main class="child-library child-profile">


    <!--<a onclick="Share.facebook('http://naplotu.com/media/com_easysocial/photos/39/519/777c67aff51cfd3f1624229715f21f26_thumbnail.png','123','http://naplotu.com/media/com_easysocial/photos/39/519/777c67aff51cfd3f1624229715f21f26_thumbnail.png','DESC')"> {шарь меня полностью}</a>-->
   <?php echo JModuleHelper::renderModule(JModuleHelper::getModule('mod_plot_header')); ?>
    
    <?php if ($this->isParentSeeToOtherChild || $this->isChildSeeToOtherChild) { ?>
    <div class="looking-at-user-header">
        <div class="wrap parent-profile">
            <div class="name-row">
                <span>Привет!</span><i>Я - <?php echo $this->user->name; ?><?php echo ($this->user->getSocialFieldData('ADDRESS')->city) ?  ',' : ''; ?> </i><span><?php echo ($this->user->getSocialFieldData('ADDRESS')->city) ? ($this->user->getSocialFieldData('ADDRESS')->city) : ''; ?></span>
            </div>
            <div class="user-top">
                <a class="circle-img modal"
                   href="<?php echo JRoute::_('index.php?option=com_plot&task=profileedit.ajaxGetForeignChildProfilePopupHtml&id=' . (int)$this->user->id); ?>"
                   rel="{size: {x: 744, y:525}, handler:'iframe', iframeOptions: {scrolling: 'no'}}">
                    <img src="<?php echo $this->user->getSquareAvatarUrl(); ?>"
                         alt="<?php echo $this->user->name; ?>">
                </a>
            </div>
        </div>
    </div>
    <?php } ?>

    <div class="wrap main-wrap child-library">
        <div class="aside-left child-library"></div>
        <div class="aside-right child-library"></div>
        <section class="child-library">
            <?php if (!$this->isParentSeeToOtherChild && !$this->isChildSeeToOtherChild) { ?>
            <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_top_menu') ); ?>
            <?php } ?>

            <div id="child-media-tabs" class="child-library">
                <div class="info-board">
                    <a href="<?php echo $this->referrerUrl; ?>"class="link-back hover-shadow">
                        Вернуться
                        <svg viewBox="0 0 22.9 46" preserveAspectRatio="xMinYMin meet" class="butterfly"><use xlink:href="#butterfly"></use></svg>
                    </a>
                    <svg viewBox="0 0 37.1 38.9" preserveAspectRatio="xMinYMin meet" class="leaf"><use xlink:href="#leaf"></use></svg>
                    <object name="bobr" data="<?php echo JUri::root().'templates/'.JFactory::getApplication()->getTemplate(); ?>/img/bobr-first-level.svg" type="image/svg+xml"></object>
                    <ul class="child-media-tabs" id="all-tabs">
                        <li><a href="#all-child-photos" id="link-child-photos" class="active">Мои фото</a></li>
                        <li><a href="#all-child-videos" id="link-child-videos">Мои видео</a></li>
                        <li><a href="#all-child-certificates" id="link-child-certificates">Мои сертификаты</a></li>
                        <li><a href="#all-child-courses" id="link-child-courses">Мои курсы</a></li>
                        <li><a href="#all-child-publications" id="link-child-publications">Мои книги</a></li>
                        <li><a href="#all-child-events" id="link-child-events">Мои думалки</a></li>
                        <?php if ($this->my->id == $this->user->id) { ?>
                            <li><a href="#child-add-activity" id="link-child-activity" class="add">Добавить</a></li>
                        <?php } else { ?>
                            <li></li>
                        <?php } ?>
                    </ul>
                </div>
                <div id="loading-bar" style="margin-left: 5px;"><img src="<?php echo $this->baseurl ?>/templates/plot/img/pre-loader-1.gif" /></div>
                <div id="all-child-photos" class="no-jcarousel new-event">
                    <h5>Всего <span id="count_all_photos"></span></h5>
                    <ul class="child-profile" id="photos-list"></ul>
                </div>
                <div id="all-child-videos" class="no-jcarousel new-event">
                    <h5>Всего <span id="count_all_videos"></span></h5>
                    <ul class="child-profile" id="child-videos"></ul>
                </div>
                <div id="all-child-certificates" class="no-jcarousel new-event">
                    <h5>Всего <span id="count_all_certificates"></span></h5>
                    <ul class="child-profile" id="child-certificates"></ul>
                </div>
                <div id="all-child-courses" class="no-jcarousel new-event">
                    <h5>Всего <span id="count_all_courses"></span></h5>
                    <ul class="child-profile" id="child-courses"></ul>
                </div>
                <div id="all-child-publications" class="no-jcarousel new-event">
                    <h5>Всего <span id="count_all_publications"></span></h5>
                    <ul class="child-profile" id="child-publications"></ul>
                </div>                
                <div id="all-child-events" class="no-jcarousel new-event">
                    <h5>Всего <span id="count_all_events"></span></h5>
                    <ul class="child-profile" id="child-events"></ul>
                </div>                
                <?php if ($this->my->id == $this->user->id) {
                    require_once JPATH_COMPONENT.'/views/profile/tmpl/default_add.php';
                } ?>
            </div>
        </section>
    </div>

</main>

