<?php
defined('_JEXEC') or die;
?>

<?php foreach ($this->events AS $event) { ?>

<li class="stream-item">
    <a href="<?php echo JRoute::_('index.php?option=com_plot&view=event&id='.$event->id);?>">
        <h6>
            <i><svg class="star" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24.3 23"><use xlink:href="#star"/></svg></i>
            <span class="action-title">Добавлена встреча</span>
            <p class="stream-title"><?php echo $event->title;?></p>
        </h6>
        <data><?php echo $event->create_date;?></data>
        <div>
            <figure>
                <img src="<?php echo JUri::root() . 'images/com_plot/events/' . $event->user_id . '/thumb/' . $event->img; ?>">
                <i class="activity-icons">
                    <svg style="fill:url(#svg-gradient);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 31.6 48.8"><use xlink:href="#lamp-meeting"/></svg>
                </i>
            </figure>
        </div>
        <div>
            <blockquote><?php echo PlotHelper::cropStr(strip_tags($event->description),  plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')); ?></blockquote>
        </div>
    </a>
</li>
<?php } ?>

