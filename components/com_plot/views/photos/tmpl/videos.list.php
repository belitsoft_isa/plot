<?php
defined('_JEXEC') or die;
?>

<?php foreach ($this->videos as $item) {
    if ($item->type == 'link' || $item->type == 'course') {
        if (strripos($item->path, 'youtube.com/') !== FALSE) {
            $youtubeNumber = substr($item->path, strripos($item->path, '?v=') + 3);
            if (($pos = strpos($youtubeNumber, '&')) !== FALSE) {
                $youtubeNumber = substr($youtubeNumber, 0, $pos);
            }
        } ?>
        <li data-video-id="<?php echo $item->id; ?>">
            <?php
            if (plotUser::factory()->id && ((int)$this->id == plotUser::factory()->id)) {
                if ($item->type == 'course') {
                    ?>
                    <button class="close"
                            onclick="plotEssay.createMessage('Нельзя удалить видео к курсу'); return false;">×
                    </button>
                <?php } else { ?>
                    <button class="close" onclick="videoRemoveConfirm('<?php echo $item->id; ?>'); return false;">×
                    </button>
                <?php
                }

            }

            ?>
            <a class="fancybox" rel="videos-set" data-fancybox-type="ajax" data-title="<?php echo $item->title; ?>"
               data-description="<?php echo $item->description; ?>"
               data-date="<?php echo JHtml::date($item->date, 'd.m.Y'); ?>"
               data-share=" data-image='https://youtu.be/<?php echo trim($youtubeNumber); ?>' data-url='https://youtu.be/<?php echo trim($youtubeNumber); ?>' data-title='<?php echo addslashes($item->title); ?>'"
               href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.ajaxOpenVideo&youtubeNumber=' . $youtubeNumber);?>"   >
                <h6>
                    <i>
                        <svg viewBox="0 0 25.3 22.3" preserveAspectRatio="xMidYMid meet">
                            <use xlink:href="#video"></use>
                        </svg>
                    </i><?php echo PlotHelper::cropStr(strip_tags($item->title), plotGlobalConfig::getVar("childActivityTitleMaxSymbolsToShow")); ?>
                </h6>
                <data><?php echo JHtml::date($item->date, "d.m.Y"); ?></data>
                <div>
                    <?php
                    if ($item->type == 'course') {

                        if (file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'coursevideo' . DIRECTORY_SEPARATOR . $item->uid . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $youtubeNumber . '.jpg')) {
                            $img = JUri::root() . 'media/com_plot/coursevideo/' . $item->uid . '/thumb/' . trim($youtubeNumber) . '.jpg'; ?>
                            <img src="<?php echo $img; ?>"/>
                        <?php
                        } else {
                            ?>
                            <img src="<?php echo JUri::root() . 'images/com_plot/def_video.jpg'; ?>"/>
                        <?php
                        }
                    } else {

                        if (file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'video' . DIRECTORY_SEPARATOR . $item->uid . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $youtubeNumber . '.jpg')) {
                            $img = JUri::root() . 'images/com_plot/video/' . $item->uid . '/thumb/' . trim($youtubeNumber) . '.jpg'; ?>
                            <img src="<?php echo str_replace("\\images", "/images", $img); ?>"/>
                        <?php
                        } else {
                            ?>
                            <img src="<?php echo JUri::root() . 'images/com_plot/def_video.jpg'; ?> "/>
                        <?php
                        }
                    } ?>
                    <i class="activity-icons">
                        <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet"
                             style="fill:url(#svg-gradient);">
                            <use xlink:href="#play"></use>
                        </svg>
                    </i>
                </div>
                <div>
                    <?php echo PlotHelper::cropStr(strip_tags($item->description), plotGlobalConfig::getVar('childPageAllDescriptionMaxSymbolsToShow')); ?>
                </div>
            </a>
        </li>
    <?php
    } else {
        if (file_exists(JPATH_SITE . '/media/com_plot/videos/' . $item->uid . '/thumb/' . substr($item->path, 0, -3) . 'jpg')) {
            $img1 = JUri::root() . 'media/com_plot/videos/' . $item->uid . '/thumb/' . substr($item->path, 0, -3) . 'jpg';
            $img1 = str_replace("\\images", "/images", $img1);
        } else {
            $img1 = JUri::root() . 'images/com_plot/def_video.jpg';
        } ?>
        <li data-video-id="<?php echo $item->id; ?>">
            <?php if (plotUser::factory()->id && ((int) $item->uid == plotUser::factory()->id)) { ?>
                <button class="close" onclick="videoRemoveConfirm('<?php echo $item->id; ?> '); return false;">×
                </button>
            <?php } ?>
            <a class="fancybox" rel="videos-set" data-fancybox-type="ajax" data-title="<?php echo $item->title; ?>"
               data-description="<?php echo $item->description; ?>"
               data-date="<?php echo JHtml::date($item->date, 'd.m.Y'); ?>"
               data-share=" data-image=<?php echo $img1; ?> data-url=<?php echo JRoute::_('index.php?option=com_plot&task=profile.openVideoFile&videoId=' . $item->id); ?> data-title=<?php echo addslashes($item->title); ?>"
               href="<?php echo JRoute::_('index.php?option=com_plot&task=profile.openVideoFile&videoId=' . $item->id); ?>">
                <h6>
                    <i>
                        <svg viewBox="0 0 25.3 22.3" preserveAspectRatio="xMidYMid meet">
                            <use xlink:href="#video"></use>
                        </svg>
                    </i><?php echo PlotHelper::cropStr(strip_tags($item->title), plotGlobalConfig::getVar("childActivityTitleMaxSymbolsToShow")); ?>
                </h6>
                <data><?php echo JHtml::date($item->date, "d.m.Y"); ?></data>
                <div>
                    <?php
                    if (file_exists(JPATH_SITE . '/media/com_plot/videos/' . $item->uid . '/thumb/' . substr($item->path, 0, -3) . 'jpg')) {
                        $img = JUri::root() . 'media/com_plot/videos/' . $item->uid . '/thumb/' . substr($item->path, 0, -3) . 'jpg'; ?>
                        <img src="<?php echo str_replace("\\images", "/images", $img); ?>"/>
                    <?php
                    } else {
                        ?>
                        <img src="<?php echo JUri::root() . 'images/com_plot/def_video.jpg'; ?>"/>
                    <?php } ?>
                    <i class="activity-icons">
                        <svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet"
                             style="fill:url(#svg-gradient);">
                            <use xlink:href="#play"></use>
                        </svg>
                    </i>
                </div>
                <div>
                    <?php echo PlotHelper::cropStr(strip_tags($item->description), plotGlobalConfig::getVar('childPageAllDescriptionMaxSymbolsToShow')); ?>
                </div>
            </a>
        </li>
    <?php
    }
} ?>
