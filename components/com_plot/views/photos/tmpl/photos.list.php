<?php
defined('_JEXEC') or die;
?>

<?php foreach ($this->photos as $item) {
    ?>
    <li data-photo-id="<?php echo $item->id; ?>">
        <?php   if (plotUser::factory()->id && ((int)$this->id == plotUser::factory()->id)) {
            if ($item->album_type == 'plot-essay') {
                ?>
                <button class="close" onclick="plotEssay.createMessage('Нельзя удалить эссе'); return false;">×</button>
            <?php } else { ?>
                <button class="close" onclick="photoRemoveConfirm('<?php echo $item->id; ?>'); return false;">×</button>
            <?php
            }

        }  ?>

        <a class="fancybox" rel="photo-set"
            <?php if ($item->album_type != 'plot-essay') { ?>
                data-title="<?php echo $item->title; ?>"
                data-description="<?php echo $item->caption; ?>"
                data-share=" data-image='<?php echo $item->original; ?>' data-url='<?php echo $item->original; ?>' data-title='<?php echo  addslashes($item->title);?>' "
            <?php } ?>

           data-date="<?php echo JHtml::date($item->created, 'd.m.Y'); ?>"
           href="<?php echo $item->original; ?>">
            <h6><i>
                    <svg viewBox="0 0 24.5 26.4" preserveAspectRatio="xMidYMid meet">
                        <use xlink:href="#photo"></use>
                    </svg>
                </i><?php echo PlotHelper::cropStr(strip_tags($item->title), plotGlobalConfig::getVar("childActivityTitleMaxSymbolsToShow")); ?>
            </h6>
            <data><?php echo JHtml::date($item->created, "d.m.Y"); ?></data>
            <div>
                <img src="<?php echo $item->thumb; ?>"/>
                <i class="activity-icons">
                    <svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid"
                         style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">
                        <use xlink:href="#zoom"></use>
                    </svg>
                </i>
            </div>
            <div><?php echo PlotHelper::cropStr(strip_tags($item->caption), plotGlobalConfig::getVar('childPageAllDescriptionMaxSymbolsToShow')); ?>
            </div>
        </a>
    </li>
<?php
} ?>
