<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class PlotController extends JControllerLegacy
{
    public function userReadBook()
    {
        $db    = JFactory::$database;
        $app   = JFactory::$application;
        $user  = JFactory::getUser();

        $sec        = $this->input->getInt('sec', 0);
        $page       = $this->input->getInt('page', 0);
        $publID     = $this->input->getInt('pubID', 0);
        $action     = $this->input->getWord('action', '');
        $response   = array();
        $update     = FALSE;

        if (!$publID)
        {
            $response = array("error" => 1, "message" => JText::_("COM_HTML5FLIPPINGBOOK_FE_ERROR_PUBL_ID"));
            echo json_encode($response);

            $app->close();
        }

        $query = $db->getQuery(true)
            ->select('1')
            ->from('`#__html5fb_users_publ`')
            ->where('`uid` = ' . $user->get('id') . ' AND `publ_id` = ' . $publID);
        $db->setQuery($query);

        if ($db->loadResult())
        {
            $update = TRUE;
            $query = $db->getQuery(true)
                ->update('`#__html5fb_users_publ`');

            switch ($action)
            {
                case 'lastopen':
                    $query->set('`lastopen` = ' . time());
                    break;
                case 'updateSpendTime':
                    $query->set('`spend_time` = `spend_time` + ' . $sec);
                    break;
                case 'updatePage':
                    $query->set('`page` = ' . $page);
                    break;
                case 'read':
                    $query->set('`read` = 1');
                    break;
                case 'read_remove':
                    $query->set('`read` = 0');
                    break;
                case 'favorite':
                    $query->set('`fav_list` = 1');
                    break;
                case 'favorite_remove':
                    $query->set('`fav_list` = 0');
                    break;
                case 'reading':
                    $query->set('`read_list` = 1');
                    break;
                case 'reading_remove':
                    $query->set('`read_list` = 0');
                    break;
            }

            $query->where('`uid` = ' . $user->get('id') . ' AND `publ_id` = ' . $publID);
        }
        else
        {
            $query = $db->getQuery(true)
                ->insert('`#__html5fb_users_publ`')
                ->columns(
                    array(
                        $db->quoteName('uid'), $db->quoteName('publ_id'),
                        $db->quoteName('read_list'), $db->quoteName('fav_list'),
                        $db->quoteName('read')
                    )
                )
                ->values($user->get('id') . ', ' . $publID . ', ' . ($action == 'reading' ? 1 : 0) . ', ' . ($action == 'favorite' ? 1 : 0) . ', ' . ($action == 'read' ? 1 : 0));
        }

        $db->setQuery($query);
        try
        {
            $db->execute();

            //Delete publication from user lists
            /*if ($update)
            {
                $query = $db->getQuery(true)
                    ->select('1')
                    ->from('`#__html5fb_users_publ`')
                    ->where('`uid` = ' . $user->get('id') . ' AND `publ_id` = ' . $publID . ' AND `read_list` = 0 AND `fav_list` = 0');
                $db->setQuery($query);
                if ($db->loadResult())
                {
                    $query = $db->getQuery(true)
                        ->delete('`#__html5fb_users_publ`')
                        ->where('`uid` = ' . $user->get('id') . ' AND `publ_id` = ' . $publID);
                    $db->setQuery($query);
                    try
                    {
                        $db->execute();
                    }
                    catch (RuntimeException $e)
                    {
                        $response = array("error" => 1, "message" => $e->getMessage());

                        echo json_encode($response);
                        $app->close();
                    }
                }
            }*/




            $response = array("error" => 0, "message" => "SUCCESS");
        }
        catch (RuntimeException $e)
        {
            $response = array("error" => 1, "message" => $e->getMessage());
        }

        echo json_encode($response);
        $app->close();
    }

    public function execute($task)
    {
        $id = JRequest::getInt('id');
        $view = JRequest::getWord('view');
        $my = plotUser::factory();


        if ($my->isParent() && !($task=='ajaxAllEventsLoad') && !( ($view == 'profile' || $view == 'events' || $view == 'photos') && $id && !plotUser::factory($id)->isParent()) ) {
            require_once JPATH_COMPONENT.'/views_parent/view.php';
           $this->addViewPath(JPATH_COMPONENT.'/views_parent');
        }

        parent::execute($task);
    }
    
    protected function createView($name, $prefix = '', $type = '', $config = array())
    {
        $view = JRequest::getWord('view');
        $id= JRequest::getInt('id', 0);
        //$userId=JRequest::getInt('userId', 0);
        $task=JRequest::getWord('task');





        if ( ($view == 'profile' && $id) || ($task == 'ajaxGetForeignChildProfilePopupHtml' && $id) || ($view == 'photos' && $id) ) {
            $my = plotUser::factory($id);
        } else {
            $my = plotUser::factory();
        }

        /*if($userId && $view == 'profile'){
            $my = plotUser::factory($userId);
        }*/

        if ($my->isParent()) {
            require_once JPATH_COMPONENT.'/views_parent/view.php';
            $this->addViewPath(JPATH_COMPONENT.'/views_parent');
        }

        // Clean the view name
        $viewName = preg_replace('/[^A-Z0-9_]/i', '', $name);
        $classPrefix = preg_replace('/[^A-Z0-9_]/i', '', $prefix);
        $viewType = preg_replace('/[^A-Z0-9_]/i', '', $type);
        
        // Build the view class name
        $viewClass = $classPrefix.$viewName;

        if (!class_exists($viewClass)) {
            jimport('joomla.filesystem.path');
            $path = JPath::find($this->paths['view'], $this->createFileName('view', array('name' => $viewName, 'type' => $viewType)));
            if ($path) {
                require_once $path;
                if (!class_exists($viewClass)) {
                    throw new Exception(JText::sprintf('JLIB_APPLICATION_ERROR_VIEW_CLASS_NOT_FOUND', $viewClass, $path), 500);
                }
            } else {
                return null;
            }
        }
        
        return new $viewClass($config);
    }    
    
    public function redirectWithMessage($url, $msg = '')
    {
        if ($msg) {
            JFactory::getApplication()->enqueueMessage( JText::_($msg) );
        }
        JFactory::getApplication()->redirect( JRoute::_($url) );
    }
    
    public function checkForm() 
    {
        $botField = JRequest::getVar('password2');
        if ($botField) {
            die('restricted access');
        }
    }
    
    public function getView($name = '', $type = '', $prefix = '', $config = array())
    {
        $view = parent::getView($name, $type, $prefix, $config);
        $paths = $view->get('_path');

        $paths['template'][] = JPATH_ROOT.'/components/com_plot/views/'.$view->get('_name').'/tmpl/';
        $view->set('_path', $paths);
        return $view;
    }
    
}