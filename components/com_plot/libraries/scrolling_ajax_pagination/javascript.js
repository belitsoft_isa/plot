(function ($) {

    $.fn.scrollPagination = function (options) {
        var settings = {
            nop: 10, // The number of posts per scroll to be loaded
            offset: 0, // Initial offset, begins at 0 in this case
            error: 'No More Posts!', // When the user reaches the end this is the message that is
            // displayed. You can change this if you want.
            delay: 500, // When you scroll down the posts will load after a delayed amount of time.
            // This is mainly for usability concerns. You can alter this as you see fit
            scroll: true, // The main bit, if set to false posts will not load as the user scrolls. 
            // but will still load if the user clicks.
            postUrl: 'ajax.php',
            appendDataTo: 'content',
            userData: '',
            noMorePostScript: '',
            afterLoad: {}
        };

        // Extend the options so they work with the plugin
        if (options) {
            $.extend(settings, options);
        }

        // For each so that we keep chainability.
        return this.each(function () {
            // Some variables
            $this = $(this);
            $settings = settings;
            var offset = $settings.offset;
            var busy = false;
            // so we don't run it multiple times

            // Custom messages based on settings
            if ($settings.scroll == true)
                $initmessage = '';
            else
                $initmessage = '';
            function getData($settings) {
                // Post data to postUrl
                $.post($settings.postUrl, {
                    action: 'scrollpagination',
                    number: $settings.nop,
                    offset: offset,
                    userData: $settings.userData
                }, function (data) {

                    // Change loading bar content (it may have been altered)
                    $this.siblings('.loading-bar').html($initmessage);

                    // If there is no data returned, there are no more posts to be shown. Show error
                    if (data == "") {
                        console.log('stop');
                        $settings.scroll=false;
                        $('.loading-bar').html($settings.error);
                        eval($settings.noMorePostScript);
                    } else {
                        // Offset increases
                        offset = offset + $settings.nop;
                        // Append the data to the content div
                        if($settings.appendDataTo[0]=='.'){
                            jQuery($settings.appendDataTo).append(data);

                        }else{
                            jQuery('#' + $settings.appendDataTo).append(data);
                        }

                        // No longer busy!	
                        busy = false;


                    }
                    $('#loading-bar').hide();
                    if (Object.getOwnPropertyNames($settings.afterLoad).length !== 0) {
                        $settings.afterLoad.afterLoadAction();
                    }
                });
            }

            if ($settings.scroll == true) {
                $('#loading-bar').show();
                // prevent multiple 'scroll' events
                $('.main-wrap-overflow').off('scroll');
                $('.main-wrap-overflow').scroll(function () {
                    // Check the user is at the bottom of the element
                    if ($('.main-wrap-overflow').scrollTop() + $('.main-wrap-overflow').height() > $this.height() && !busy) {
                        busy = true;
                        var child = document.getElementById('search-all-children');
                        // $this.find('.loading-bar').html('Loading...');

                        setTimeout(function () {
                            getData($settings);

                        }, $settings.delay);
                    }

                });
            }

            // Also content can be loaded by clicking the loading bar/
            // $this.find('.loading-bar').click(function ()  { if (busy == false) { busy = true; getData(); }  });

        });
    };

})(jQuery);

