<?php

class PHP_FFMPEG
{

    function getVideoInformation($videoPath)
    {
        $movie = new ffmpeg_movie($videoPath, false);

        $this->videoDuration = $movie->getDuration();
        $this->frameCount = $movie->getFrameCount();
        $this->frameRate = $movie->getFrameRate();
        $this->videoTitle = $movie->getTitle();
        $this->author = $movie->getAuthor();
        $this->copyright = $movie->getCopyright();
        $this->frameHeight = $movie->getFrameHeight();
        $this->frameWidth = $movie->getFrameWidth();
        $this->pixelFormat = $movie->getPixelFormat();
        $this->bitRate = $movie->getVideoBitRate();
        $this->videoCodec = $movie->getVideoCodec();
        $this->audioCodec = $movie->getAudioCodec();
        $this->hasAudio = $movie->hasAudio();
        $this->audSampleRate = $movie->getAudioSampleRate();
        $this->audBitRate = $movie->getAudioBitRate();


    }


    function getAudioInformation($videoPath)
    {
        $movie = new ffmpeg_movie($videoPath, false);

        $this->audioDuration = $movie->getDuration();
        $this->frameCount = $movie->getFrameCount();
        $this->frameRate = $movie->getFrameRate();
        $this->audioTitle = $movie->getTitle();
        $this->author = $movie->getAuthor();
        $this->copyright = $movie->getCopyright();
        $this->artist = $movie->getArtist();
        $this->track = $movie->getTrackNumber();
        $this->bitRate = $movie->getBitRate();
        $this->audioChannels = $movie->getAudioChannels();
        $this->audioCodec = $movie->getAudioCodec();
        $this->audSampleRate = $movie->getAudioSampleRate();
        $this->audBitRate = $movie->getAudioBitRate();

    }

    function getThumbImage($videoPath)
    {
        $movie = new ffmpeg_movie($videoPath, false);
        $this->videoDuration = $movie->getDuration();
        $this->frameCount = $movie->getFrameCount();
        $this->frameRate = $movie->getFrameRate();
        $this->videoTitle = $movie->getTitle();
        $this->author = $movie->getAuthor();
        $this->copyright = $movie->getCopyright();
        $this->frameHeight = $movie->getFrameHeight();
        $this->frameWidth = $movie->getFrameWidth();

        $capPos = ceil($this->frameCount / 4);

        if ($this->frameWidth > 120) {
            $cropWidth = ceil(($this->frameWidth - 120) / 2);
        } else {
            $cropWidth = 0;
        }
        if ($this->frameHeight > 90) {
            $cropHeight = ceil(($this->frameHeight - 90) / 2);
        } else {
            $cropHeight = 0;
        }
        if ($cropWidth % 2 != 0) {
            $cropWidth = $cropWidth - 1;
        }
        if ($cropHeight % 2 != 0) {
            $cropHeight = $cropHeight - 1;
        }

        $frameObject = $movie->getFrame($capPos);


        if ($frameObject) {
            $imageName = "tmb_vid_1212.jpg";
            $tmbPath = "/home/home_Dir/public_html/uploads/thumb/" . $imageName;
            $frameObject->resize(120, 90, 0, 0, 0, 0);
            imagejpeg($frameObject->toGDImage(), $tmbPath);
        } else {
            $imageName = "";
        }


        return $imageName;

    }


}


?>