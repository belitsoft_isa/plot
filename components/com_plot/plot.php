<?php
defined('_JEXEC') or die('Restricted access');

// Set the component css/js
$document = JFactory::getDocument();

// Require helper files
JLoader::register('PlotHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'plot.php');
JLoader::register('PlotHelperJLMS', dirname(__FILE__) . DS . 'helpers' . DS . 'jlms.php');

// import joomla controller library
jimport('joomla.application.component.controller');

require_once JPATH_ADMINISTRATOR.'/components/com_plot/requires.php';
require_once JPATH_COMPONENT.'/controller.php';
require_once JPATH_COMPONENT.'/views/view.php';

// Get an instance of the controller prefixed by Plot
$controller = JControllerLegacy::getInstance('Plot');

$controller->addModelPath(JPATH_COMPONENT_ADMINISTRATOR.DS.'models');

// Perform the request task
$controller->execute(JFactory::getApplication()->input->get('task', 'display'));

// Redirect if set by the controller
$controller->redirect();
