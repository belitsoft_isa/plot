<?php
defined('_JEXEC') or die('Restricted access');

function PlotBuildRoute(&$query)
{

    $segments = array();

    if (isset($query['view'])) {
        $segments[] = $query['view'];
        unset($query['view']);
    };

    if (isset($query['id'])) {
        $segments[] = $query['id'];
        unset($query['id']);
    };
    
    if (isset($query['bookId'])) {
        $segments[] = $query['bookId'];
        unset($query['bookId']);
    }


    $query['Itemid'] = plotGlobalConfig::getVar('plotMenuHomePageItemId');

    return $segments;
}

function PlotParseRoute($segments)
{
    $activeMenu = JFactory::getApplication()->getMenu()->getActive();
    $vars = array();

    switch ($segments[0]) {
        case 'publication':
            $activeMenu = JFactory::getApplication()->getMenu()->getItem(226);
            $vars['view'] = $activeMenu->query['view'];
            if (isset($segments[1])) {
                $vars['bookId'] = $segments[1];
            }
            break;
        case 'k2article':
            $vars['view'] = 'k2article';
            if (isset($segments[0])) {
                $vars['id'] = $segments[1];
            }
            break;
        case 'photos':
            $vars['view'] = 'photos';
            if (isset($segments[0])) {
                if(isset($segments[1])){
                    $vars['id'] = $segments[1];
                }
            }
            break;
        case 'welcome':
            $vars['view'] = 'welcome';

            break;
        default:
            if ($activeMenu->id) {
                $vars['view'] = $activeMenu->query['view'];
                if (isset($segments[0])) {
                    if ($vars['view'] == 'publication') {
                        $vars['bookId'] = $segments[0];
                    } else {
                        $vars['id'] = $segments[0];
                    }
                }
            } else {
                $vars['view'] = $segments[0];
                if (isset($segments[1])) {
                    $vars['id'] = $segments[1];
                }
            }
            break;
    }
  ;

    return $vars;
}
