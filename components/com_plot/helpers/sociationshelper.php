<?php

class SocialactionsHelper {
	var $shares;
	function __construct($link)
	{
		$this->link = JUri::root() . substr($link, 1);
	}

	public function get($method)
	{
		$methodname = 'get' . ucfirst($method);
		if (method_exists($this, $methodname)) {
			return $this->$methodname();
		}
		return false;
	}

	private function getFacebook() {
		$api = json_decode(@file_get_contents('http://graph.facebook.com/?id=' . urlencode($this->link)));
		return (!empty($api->shares)) ? $api->shares : 0;
	}

	private function getVk() {
		$api = file_get_contents('https://vk.com/share.php?act=count&index=1&url=' . urlencode($this->link));
		preg_match("/\\((.*?)\\)/", $api, $matches);
		list($response_id, $shares) = explode(',', $matches[1]);
		return (!empty($shares)) ? intval($shares) : 0;
	}

	private function getOk() {
		$api = file_get_contents('http://ok.ru/dk?st.cmd=extOneClickLike&uid=odklocs0&ref=' . urlencode($this->link));
		preg_match("/\\((.*?)\\)/", $api, $matches);
		list($response_id, $shares) = explode(',', $matches[1]);
		return (!empty($shares)) ? intval($shares) : 0;
	}

	function formatNumber($number) {
		if ($number > 1000) {
			return ceil($number / 1000) . 'K';
		}else {
			return intval($number);
		}
	}
}