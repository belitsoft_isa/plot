<?php
defined('_JEXEC') or die('Restricted access');
class PlotHelperJLMS
{

    static function createBlankCourse($params)
    {
        self::requireJLMSFiles();
        global $my, $JLMS_CONFIG;
        $JLMS_DB = JLMSFactory::getDB();
        
        if (!isset($params['title']) || !isset($params['categoryId'])) {
            return false;
        }

        $courseDescription = $params['description'];
        $courseName = $params['title'];
        $categoryId = $params['categoryId'];

        $JLMS_CONFIG = JLMSFactory::getConfig();
        $JLMS_ACL = JLMSFactory::getACL();
        $_JLMS_PLUGINS = & JLMSFactory::getPlugins();
        $_JLMS_PLUGINS->loadBotGroup('system');
        $request = JRequest::get();
        $i = 0;
        $_POST['cat_id'] = $categoryId;
        
        $row = new mos_Joomla_LMS_Course($JLMS_DB);
        $row->publish_start = 0;
        $row->publish_end = 0;
        
        $row->language = 13; # 13 - russian language for both (local and net) sites
        unset($row->spec_reg);
        $row->gid = 0;
        $row->start_date = self::JLMS_dateToDB($row->start_date);
        $row->end_date = self::JLMS_dateToDB($row->end_date);
        unset($row->course_price);

        $course_name_post = ampReplace(strip_tags((get_magic_quotes_gpc()) ? stripslashes($courseName) : $courseName ));
        $row->course_name = $course_name_post;
        $row->course_description = $courseDescription;
        $row->course_sh_description = '';
        $row->metadesc = ''; 
        $row->metakeys = '';
        $row->owner_id = $my->id;
        $row->cat_id = $categoryId;

        //Course Properties Event//
        $plugin_args = array($row);
        $lists['plugin_return'] = $_JLMS_PLUGINS->trigger('onSaveCourseProperties', $plugin_args);

        if (isset($lists['plugin_return']) && count($lists['plugin_return'])) {
            $fields = $lists['plugin_return'];
            if (isset($row->id) && $row->id) {
                $row_p = new mos_Joomla_LMS_Course($JLMS_DB);
                $row_p->load($row->id);
            }
            $params = strlen($row_p->params) ? explode("\n", $row_p->params) : array();
            $params_check = array();
            $i = 0;
            foreach ($params as $param) {
                preg_match('#(.*)=(.*)#', $param, $out);

                if (isset($out[0]) && strlen($out[0]) && $out[1] && isset($out[2])) {
                    $params_check[$i]->name = $out[1];
                    $params_check[$i]->value = $out[2];
                    $i++;
                }
            }

            $all_fields_tmp = $fields;
            $fields = array();
            foreach ($all_fields_tmp as $fields_tmp) {
                foreach ($fields_tmp as $field_tmp) {
                    $fields[] = $field_tmp;
                }
            }

            for ($i = 0; $i < count($fields); $i++) {
                $field = $fields[$i];
                $triger = true;
                $params_check = array();
                foreach ($params_check as $param_ch) {
                    if (strtolower($field->name) == $param_ch->name) {
                        if ($field->value == $param_ch->value) {
                            $triger = false;
                            break;
                        } else {
                            $triger = false;
                            $param_ch->value = $field->value;
                            break;
                        }
                    }
                }

                $params = array();
                foreach ($params_check as $param_ch) {
                    $params[] = strtolower($param_ch->name).'='.$param_ch->value;
                }

                if ($triger && strlen($field->name)) {
                    $params[] = strtolower($field->name).'='.$field->value;
                }
            }

            $row->params = count($params) ? implode("\n", $params) : $row->params;
        }
        //Course Properties Event//

        if ($JLMS_CONFIG->get('plugin_forum') != 1) {
            $row->add_forum = 0;
        }
        
        if (!$row->store()) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }
        $new_course_id = $row->id;

        $plugin_args2 = array($row);
        $lists['plugin_return'] = $_JLMS_PLUGINS->trigger('onAfterSaveCourseProperties', $plugin_args2);
        $course_id = $row->id;

        //Roles
        if ($JLMS_ACL->isAdmin()) {
            $default_role = $JLMS_ACL->defaultRole(2);
        } else {
            $default_role = $JLMS_ACL->GetRole(2);
        }

        if ($default_role) {
            $query = "INSERT INTO `#__lms_user_courses` (user_id, course_id, role_id) VALUES ('".$my->id."', '".$new_course_id."', '".$default_role."')";
            $JLMS_DB->SetQuery($query);
            $JLMS_DB->query();
        }

        $add_forum = intval(mosGetParam($request, 'add_forum', 0));

        $query = "UPDATE #__lms_forum_details SET is_active = ".$add_forum.", need_update = 1 WHERE course_id = $course_id";
        $JLMS_DB->setQuery($query);
        if ($JLMS_DB->query()) {
            $forum = & JLMS_SMF::getInstance();
            if ($forum) {
                $forum->applyChanges($course_id);
            }
        }

        $query = "UPDATE #__lms_usergroups SET group_forum = ".$add_forum." WHERE course_id = $course_id";
        $JLMS_DB->setQuery($query);
        $JLMS_DB->query();

        return $new_course_id;
    }
    
    static function createLearningPath($courseId)
    {
        self::requireJLMSFiles();
	global $my, $Itemid, $JLMS_CONFIG;
        $JLMS_DB = JLMSFactory::getDB();
	$JLMS_ACL = JLMSFactory::getACL();
        
	$course_id = $courseId;
        $learningPathName = 'Программа обучения курса';
        
        $id = 0; # learning path id 0 -> new LearningPath
	$lp_save_error = 'JLMS_saveLPath() function failed with error at line _line_. Reason: ';
        $_POST['course_id'] = $course_id;
	
        @set_time_limit(0);
        $row = new mos_Joomla_LMS_LearnPath( $JLMS_DB );
        
        if (!$row->bind( $_POST )) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }

        $row->lpath_name = $learningPathName;
        $row->owner_id = $my->id;
        $row->lpath_description = '';
        $row->lpath_shortdescription = '';
        $row->published = 1;

        $lpath_type = 1; # 2 - scorm; 1- lpath
        $scorm_params = '';

        $lp_params = '';
        $scorm_lpath_item_published = false;
        $lp_params_p = mosGetParam($_POST, 'lp_params', '');
        if (is_array($lp_params_p)) {
            $txt = array();
            foreach ($lp_params_p as $k => $v) {
                $txt[] = "$k=$v";
                if ($k == 'published' && $v) {
                    $scorm_lpath_item_published = true;
                }
            }
            $lp_params = implode("\n", $txt);
        }

        $lpath_completion_msg = '';
        $row->lp_params = $lp_params;
        $scorm_id = 0;
        $lp_params = new JParameter($row->lp_params);
        
        $query = "SELECT lp_type FROM #__lms_learn_paths WHERE id='".$id."'";
        $JLMS_DB->setQuery($query);
        $lp_type = $JLMS_DB->loadResult(); //lp_type == 2 - from library
        
        $row->lp_type = 0;
        if ($row->id && $lp_type != 2) {
            $row->lp_type = 1;
        }

        $days = intval(mosGetParam($_POST, 'days', ''));
        $hours = intval(mosGetParam($_POST, 'hours', ''));
        $mins = intval(mosGetParam($_POST, 'mins', ''));

        if ($row->is_time_related) {
            $row->show_period = JLMS_HTML::_('showperiod.getminsvalue', $days, $hours, $mins);
        }

        if (!$row->check()) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }
        if (!$row->store()) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }

        $isActive = isset($lp_params_p['add_forum']) ? $lp_params_p['add_forum'] : 0;

        $query = "UPDATE `#__lms_forum_details` AS fd SET fd.is_active = ".(int)$isActive.", fd.need_update = 1
                WHERE fd.board_type IN (SELECT f.id FROM `#__lms_forums` AS f WHERE f.forum_level = 1) 
                        AND fd.course_id = '".$row->course_id."' AND fd.group_id = '".$row->id."'";
        $JLMS_DB->setQuery($query);
        if( $JLMS_DB->query()) {
            $forum = & JLMS_SMF::getInstance();
            if ($forum) {
                $forum->applyChanges($row->course_id);
            }
        }

        //save prerequisites			
        $prereqs = JRequest::getVar('prereqs', array());

        if (!empty($prereqs)) {
            $query = "SELECT req_id FROM #__lms_learn_path_prerequisites WHERE lpath_id = '$id'";
            $JLMS_DB->SetQuery($query);
            $addedPrereqs = JLMSDatabaseHelper::loadResultArray();

            $addPrereqs = array_diff($prereqs, $addedPrereqs);
            $remPrereqs = array_diff($addedPrereqs, $prereqs);

            if (!empty($remPrereqs)) {
                $query = "DELETE FROM #__lms_learn_path_prerequisites WHERE lpath_id = '$id' AND req_id IN (".implode(',', $remPrereqs).")";
                $JLMS_DB->SetQuery($query);
                $JLMS_DB->query();
            }

            $values = array();
            foreach ($addPrereqs as $addPrereq) {
                if ($addPrereq) {
                    $values[] = "($id, $addPrereq, 0)";
                }
            }

            if (!empty($values)) {
                $query = "INSERT INTO #__lms_learn_path_prerequisites(lpath_id, req_id, time_minutes) VALUES ".implode(',', $values);
                $JLMS_DB->SetQuery($query);
                $JLMS_DB->query();
            }
        } else {
            $query = "DELETE FROM #__lms_learn_path_prerequisites WHERE lpath_id = '$id'";
            $JLMS_DB->SetQuery($query);
            $JLMS_DB->query();
        }

        return $row->id;
    }
    
    static function createContentStepWithVideoLinkFromYoutube($learningPathId, $videoLink, $videoTitle)
    {
        self::requireJLMSFiles();
        $JLMS_DB = JLMSFactory::getDB();

        $lpath_id = $learningPathId;
        $content = $videoLink;
        $new_content_name = $videoTitle;

        $learningPathsCourse = $JLMS_DB->SetQuery("SELECT course_id FROM #__lms_learn_paths WHERE id = '".$lpath_id."'")->LoadResult();
        $course_id = $learningPathsCourse;
        
        if (!$course_id || !$lpath_id || ($learningPathsCourse != $course_id)) {
            return false;
        }

        $row = new mos_Joomla_LMS_LearnPathStep($JLMS_DB);
        $row->course_id = $course_id;
        $row->lpath_id = $lpath_id;

        $row->step_description = strval('{youtube}'.$content.'{/youtube}');

        if (!$row->bind($_POST)) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }
        $row->id = 0; // only new record; !!!!!
        $row->lpath_id = $lpath_id;
        $row->step_type = 4;
        $row->item_id = 0;
        $row->step_name = $new_content_name;

        $row->step_shortdescription = '';

        $row->parent_id = 0;
        $row->ordering = 0;
        if (!$row->check()) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }
        if (!$row->store()) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }
        $parent_id = 0;
        $query = "SELECT id FROM #__lms_learn_path_steps WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND parent_id = '".$parent_id."' ORDER BY ordering, step_name, id";
        $JLMS_DB->SetQuery($query);
        $id_array = JLMSDatabaseHelper::LoadResultArray();
        if (count($id_array)) {
            $i = 0;
            foreach ($id_array as $lpath_id) {
                $query = "UPDATE #__lms_learn_path_steps SET ordering = '".$i."' WHERE id = '".$lpath_id."'";
                $JLMS_DB->SetQuery($query);
                $JLMS_DB->query();
                $i ++;
            }
        }

        return true;
    }
    
    static function createDocumentWithUploadedVideo($courseId, $videoFilePath, $videoTitle)
    {
        self::requireJLMSFiles();
        global $JLMS_CONFIG;
        $JLMS_CONFIG = JLMSFactory::getConfig();

        $Itemid = $JLMS_CONFIG->get('Itemid');
        $db = JFactory::getDbo();
        $user = JLMSFactory::getUser();
        $my_id = $user->get('id');
        $JLMS_CONFIG->set('files_skipped_by_extension', false);
        
        $_POST['course_id'] = $courseId;
        
        $course_id = $courseId;
        $_POST['doc_name0'] = $videoTitle;
        $id = 0;
        $folder_flag = intval( JRequest::getVar('folder_flag') );
        $if_zip_pack = 0;
        
        if ($course_id) {
            $stop_processing = false;
            $parent_id = 0;
            $rows = array();
            $possibilities = new stdClass();
            self::FillList($course_id, $rows, $possibilities);
            
            if (!self::CheckCourseID($rows, $parent_id, $course_id)) {
                $parent_id = 0;
            }
            if ($id && $parent_id == $id) {
                $parent_id = 0;
            }

            if (!self::CheckCourseID($rows, $id, $course_id)) {
                $stop_processing = true;
            }
            require_once JPATH_BASE.'/components/com_joomla_lms/joomla_lms.docs.hlpr.php';
            $item_permissions = & JLMSDocs::GetItemPermissions($rows, $id);
            $parent_permissions = & JLMSDocs::GetItemPermissions($rows, $parent_id);
            
            if ($stop_processing) {
                return false;
            }

            $upload_zip = intval(mosGetParam($_REQUEST, 'upload_zip', 0));
            $is_zip_pack = intval(mosGetParam($_REQUEST, 'zip_package', 0));
            if ($folder_flag) {
                $upload_zip = false;
                $is_zip_pack = false;
            }
            if ($id) {
                $upload_zip = false;
            }
            if ($upload_zip) {
                $is_zip_pack = false;
            }

            $query = "SELECT max(ordering) FROM #__lms_documents WHERE course_id = $course_id AND parent_id = $parent_id";
            $db->SetQuery($query);
            $max_ordering = $db->LoadResult() + 1;
            $ordering = array();
            $ordering[$parent_id] = $max_ordering;

            require_once(JPATH_SITE.DS."components".DS."com_joomla_lms".DS."includes".DS."libraries".DS."lms.lib.files.php");
            require_once(JPATH_SITE.DS."components".DS."com_joomla_lms".DS."includes".DS."libraries".DS."lms.lib.zip.php");

            $available_app_ext = array();
            $available_app_ext[] = 'application/zip';
            $available_app_ext[] = 'application/x-zip';
            $available_app_ext[] = 'application/zip-compressed';
            $available_app_ext[] = 'application/x-zip-compressed';

            $zip_files_array = array();
            $zip_files_names = array();
            $no_files_array = array();
            $temporary_files = array();
            
            $tmp_folder = JPATH_SITE."/tmp/";
            $convertedToFlvVideoFileName = self::convertVideoToFlvAndRemoveOldFile($videoFilePath);
            JFile::move($convertedToFlvVideoFileName, $tmp_folder.pathinfo($convertedToFlvVideoFileName, PATHINFO_BASENAME));
            $zip_files_array[$tmp_folder][pathinfo($convertedToFlvVideoFileName, PATHINFO_BASENAME)] = $convertedToFlvVideoFileName;
            $temporary_files[] = $tmp_folder.pathinfo($convertedToFlvVideoFileName, PATHINFO_BASENAME);
            $zip_files_names[$tmp_folder][pathinfo($convertedToFlvVideoFileName, PATHINFO_BASENAME)] = $videoTitle;
            
            /* populate default properties */
            $default_row = new stdClass();
            $default_row->published = intval(mosGetParam($_REQUEST, 'published', 0));
            $default_row->publish_start = 0;
            $default_row->publish_end = 0;
            $default_row->start_date = '';
            $default_row->end_date = '';
            $default_row->days = intval(mosGetParam($_POST, 'days', ''));
            $default_row->hours = intval(mosGetParam($_POST, 'hours', ''));
            $default_row->mins = intval(mosGetParam($_POST, 'mins', ''));
            $default_row->is_time_related = intval(mosGetParam($_REQUEST, 'is_time_related', 0));
            $default_row->show_period = 0;
            
            if ($default_row->is_time_related) {
                $default_row->show_period = JLMS_HTML::_('showperiod.getminsvalue', $default_row->days, $default_row->hours, $default_row->mins);
            }
            if ($id && $item_permissions->publish && $parent_permissions->publish_childs) {
                $i_can_publish_it = true;
            } elseif (!$id && $parent_permissions->publish_childs) {
                $i_can_publish_it = true;
            } else {
                $i_can_publish_it = false;
            }
            
            // start-end publishing
            $publish_start = intval(mosGetParam($_REQUEST, 'is_start', 0));
            $publish_end = intval(mosGetParam($_REQUEST, 'is_end', 0));
            $default_row->publish_start = $publish_start;
            $default_row->publish_end = $publish_end;
            if ($default_row->publish_start) {
                $default_row->start_date = mosGetParam($_REQUEST, 'start_date', '');
                $default_row->start_date = self::JLMS_dateToDB($default_row->start_date);
            } else {
                $default_row->start_date = '';
            }
            if ($default_row->publish_end) {
                $default_row->end_date = mosGetParam($_REQUEST, 'end_date', '');
                $default_row->end_date = self::JLMS_dateToDB($default_row->end_date);
            } else {
                $default_row->end_date = '';
            }
            $default_row->parent_id = $parent_id;
            $default_row->published = 1;
            $default_row->doc_name = $videoTitle;
            
            if (count($zip_files_array)) {
                $first_saved_id = 0;
                foreach ($zip_files_array as $k => $v) {
                    $docname = (isset($zip_files_names[$k]) && $zip_files_names[$k]) ? $zip_files_names[$k] : array();
                    $saved_id = 0;
                    $doc_id = self::JLMSDocs_save_file_from_zip_array($v, $parent_id, substr($k, 0, strlen($k) - 1), $ordering, $saved_id, $default_row, $is_zip_pack, $docname);
                    
                    if (!$first_saved_id && $saved_id) {
                        $first_saved_id = $saved_id;
                    }
                }

                if ($first_saved_id) {
                    $open_element = $first_saved_id;
                    if ($parent_id) {
                        $open_folder = $parent_id;
                    }
                }
                foreach ($temporary_files as $temporary_file) {
                    if (file_exists($temporary_file)) {
                        if (is_dir($temporary_file)) {
                            JLMS_Files::delFolder($temporary_file);
                        } elseif (is_file($temporary_file)) {
                            JLMS_Files::delFile($temporary_file);
                        }
                    }
                }
            }
        }
        
        $doc_id = isset($doc_id) ? $doc_id : 0;
        return $doc_id;
    }
    
    static function addDocumentToLearningPath($documentID, $learningPathId)
    {
        self::requireJLMSFiles();
        global $Itemid, $JLMS_DB, $JLMS_CONFIG;
        $JLMS_DB = JLMSFactory::getDB();

        $JLMS_ACL = JLMSFactory::getACL();
        $course_id = JFactory::getDbo()->setQuery("SELECT `course_id` FROM `#__lms_learn_paths` WHERE `id` = ".(int) $learningPathId)->loadResult();
        $lpath_id = $learningPathId;

        $lpath_chapter = 0;
        $lpath_order = -2;
        $cid = array($learningPathId);
        
        $new_order = 0;
        $query = "SELECT max(ordering) FROM #__lms_learn_path_steps WHERE lpath_id = '".$lpath_id."' AND course_id = '".$course_id."' AND parent_id = '".$lpath_chapter."'";
        $JLMS_DB->SetQuery($query);
        $ordering = $JLMS_DB->LoadResult();
        $new_order = ($ordering + 1);
        $i = 0;
        while ($i < count($cid)) {
            $cid[$i] = intval($cid[$i]);
            $i ++;
        }
        
        $cids = implode(',', $cid);
        for ($z = 0; $z < count($cid); $z++) {
            $query = "SELECT id, doc_name, doc_description, folder_flag, file_id"
                    ."\n FROM #__lms_documents WHERE id = '".$documentID."' AND course_id = '".$course_id."'";
            $JLMS_DB->setQuery($query);
            $tmp_obj = $JLMS_DB->loadObjectList();
            if (count($tmp_obj)) {
                $do_add_resource = true;
                if ($tmp_obj[0]->folder_flag == 3) {
                    $query = "SELECT a.*, b.file_name FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0 WHERE a.id=".$tmp_obj[0]->file_id;
                    $JLMS_DB->SetQuery($query);
                    $out_row = $JLMS_DB->LoadObjectList();

                    if (count($out_row) && isset($out_row[0]->allow_link) && $out_row[0]->allow_link == 1) {
                        $tmp_obj[0]->doc_name = $out_row[0]->doc_name;
                        $tmp_obj[0]->doc_description = $out_row[0]->doc_description;
                    } else {
                        
                        $do_add_resource = false;
                    }
                }
                if ($do_add_resource) {
                    
                    $query = "INSERT INTO #__lms_learn_path_steps"
                            ."\n (course_id, lpath_id, item_id, step_type, parent_id, step_name, step_description, ordering)"
                            ."\n VALUES('".$course_id."', '".$lpath_id."', '".$tmp_obj[0]->id."', 2, '".$lpath_chapter."', ".$JLMS_DB->quote($tmp_obj[0]->doc_name).", ".$JLMS_DB->quote($tmp_obj[0]->doc_description).", '".$new_order."')";
                    $JLMS_DB->SetQuery($query);
                    $JLMS_DB->query();
                }
            }
        }
        $query = "SELECT id FROM #__lms_learn_path_steps WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND parent_id = '".$lpath_chapter."' ORDER BY ordering, step_name, id";
        $JLMS_DB->SetQuery($query);
        $id_array = JLMSDatabaseHelper::LoadResultArray();
        if (count($id_array)) {
            JLMS_orderLPathStepList($id_array);
        }
        return true;
    }
    
    static function convertVideoToFlvAndRemoveOldFile($filePath)
    {
        $fileNameWithoutExt = pathinfo($filePath, PATHINFO_FILENAME);
        $extension = pathinfo($filePath, PATHINFO_EXTENSION);
        $filePathDir = pathinfo($filePath, PATHINFO_DIRNAME);
        if ($extension !== 'flv') {
            require_once JPATH_COMPONENT_SITE.'/helpers/video.php';
            $inputFile = $filePath;
            $outputFile = $filePathDir.'/'.$fileNameWithoutExt.'.flv';
            plotVideoHelper::convert_media($inputFile, $outputFile);
            JFile::delete($filePath);
            return $outputFile;
        }
        return $fileNameWithoutExt.'.'.$extension;
    }

    static function createQuizWithThreeQuestions($courseId)
    {
        self::requireJLMSFiles();
        require_once(_JOOMLMS_FRONT_HOME."/includes/quiz/joomlaquiz.class.php");
        require_once(_JOOMLMS_FRONT_HOME."/includes/quiz/admin.joomlaquiz.class.php");
        require_once(_JOOMLMS_FRONT_HOME."/includes/quiz/admin_html.joomlaquiz.class.php");
        global $my, $Itemid, $JLMS_CONFIG;
        $JLMS_DB = JLMSFactory::getDB();
        $JLMS_ACL = JLMSFactory::getACL();

        $course_id = $courseId;
        $testTitle = 'Тест';

        $c_id = intval(mosGetParam($_REQUEST, 'c_id', 0));
        $cid = mosGetParam($_REQUEST, 'cid', array(0));

        $row = new mos_JoomQuiz_Quiz($JLMS_DB);
        if (!$row->bind($_POST)) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }
        
        $row->course_id = $course_id;
        $row->c_user_id = $my->id;
        $row->c_skin = 3;
        $row->c_language = 1;
        $row->c_guest = 0;
        $params = mosGetParam($_POST, 'params', '');
        $quiz_params = '';
        if (is_array($params)) {
            $txt = array();
            foreach ($params as $k => $v) {
                $txt[] = "$k=$v";
            }
            $quiz_params = implode("\n", $txt);
        }
        $row->params = $quiz_params;

        if (!$row->c_id) {
            $date = time();
            $s_day = mktime(0, 0, 0, date('m', $date), date('d', $date), date('Y', $date));
            $row->c_created_time = date('Y-m-d', $s_day);
        } else {
            unset($row->c_created_time);
        }

        $row->c_title = $testTitle;
        $row->c_description = '';
        $row->c_right_message = '';
        $row->c_wrong_message = '';
        $row->c_pass_message = '';
        $row->c_unpass_message = '';
        $row->published = 1;
        $row->c_passing_score = plotGlobalConfig::getVar('coursesSmallDefaultQuizPercentToPass');
        
        $days = intval(mosGetParam($_POST, 'days', ''));
        $hours = intval(mosGetParam($_POST, 'hours', ''));
        $mins = intval(mosGetParam($_POST, 'mins', ''));

        if ($row->is_time_related) {
            $row->show_period = JLMS_HTML::_('showperiod.getminsvalue', $days, $hours, $mins);
        }

        if (!$row->check()) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }
        
        if (!$row->store()) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }

        $query = "DELETE FROM #__lms_quiz_t_quiz_pool WHERE quiz_id = $row->c_id";
        $JLMS_DB->SetQuery($query);
        $JLMS_DB->query();
        $pool_type = 1;

        if ($pool_type == 1) {
            $pool_num = intval(mosGetParam($_REQUEST, 'pool_qtype_number', 0));
            if ($pool_num) {
                $query = "INSERT INTO #__lms_quiz_t_quiz_pool (quiz_id, qcat_id, items_number)"
                        ."\n VALUES($row->c_id, 0, $pool_num)";
                $JLMS_DB->SetQuery($query);
                $JLMS_DB->query();
            }
        }
        
        self::addQuestionToQuiz(
            $row->c_id,
            JRequest::getString('question1'), 
            array(
                JRequest::getString('question1-answer1'), 
                JRequest::getString('question1-answer2'), 
                JRequest::getString('question1-answer3'), 
                JRequest::getString('question1-answer4'), 
                JRequest::getString('question1-answer5')
            ),
            JRequest::getInt('question1-right-answer')
        );
        
        self::addQuestionToQuiz(
            $row->c_id,
            JRequest::getString('question2'), 
            array(
                JRequest::getString('question2-answer1'), 
                JRequest::getString('question2-answer2'), 
                JRequest::getString('question2-answer3'), 
                JRequest::getString('question2-answer4'), 
                JRequest::getString('question2-answer5')
            ),
            JRequest::getInt('question2-right-answer')
        );        
        
        self::addQuestionToQuiz(
            $row->c_id,
            JRequest::getString('question3'), 
            array(
                JRequest::getString('question3-answer1'), 
                JRequest::getString('question3-answer2'), 
                JRequest::getString('question3-answer3'), 
                JRequest::getString('question3-answer4'), 
                JRequest::getString('question3-answer5')
            ),
            JRequest::getInt('question3-right-answer')
        );        
        
        return $row->c_id;
    }
    
    static function addQuestionToQuiz($quizId, $question, $answers, $rightAnswer)
    {
        self::requireJLMSFiles();
        require_once(_JOOMLMS_FRONT_HOME."/includes/quiz/joomlaquiz.class.php");
        require_once(_JOOMLMS_FRONT_HOME."/includes/quiz/admin.joomlaquiz.class.php");
        $JLMS_DB = JLMSFactory::getDB();

        $courseId = $JLMS_DB->setQuery("SELECT `course_id` FROM `#__lms_quiz_t_quiz` WHERE `c_id` = ".$JLMS_DB->quote($quizId))->loadResult();
        $id = $courseId;
        $questionText = $question;
        $questionAnswers = $answers;
        
        $row = new mos_JoomQuiz_Question($JLMS_DB);
        if (!$row->bind($_POST)) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }
        
        $row->c_type = 1;
        $row->params = "quest_text_pos=t\nrandom_answers=0\nsurvey_question=0";
        $row->c_quiz_id = $quizId;
        $row->c_point = plotGlobalConfig::getVar('joomlaLMSDefaultPointForQuizQuestion');

        $is_new = false;
        if (!$row->c_id) {
            $is_new = true;
        }

        $row->course_id = $id;
        $row->c_pool = 0;
        
        $row->c_question = $questionText;
        $row->c_explanation = '';

        if (!$row->check()) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }
        
        $q_ordering = -1; # -1 - last, 0 - first

        if (!$row->store()) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }

        $qid = $row->c_id;
        self::JQ_orderQuest_inside($id, $row->c_quiz_id, $qid, $q_ordering);
        
        $field_order = 0;
        $ans_right = array();
        
        foreach (array($rightAnswer) as $sss) {
            $ans_right[] = $sss;
        }

        $fids_arr = array();
        $mcounter = 0;
        foreach ($questionAnswers as $f_row) {
            $new_field = new mos_JoomQuiz_ChoiceField($JLMS_DB);
            $new_field->c_question_id = $qid;

            $f_row_p = (get_magic_quotes_gpc()) ? stripslashes($f_row) : $f_row;

            if (preg_match('%(?:
            [\xC2-\xDF][\x80-\xBF]
            |\xE0[\xA0-\xBF][\x80-\xBF]
            |[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}
            |\xED[\x80-\x9F][\x80-\xBF]
            |\xF0[\x90-\xBF][\x80-\xBF]{2}
            |[\xF1-\xF3][\x80-\xBF]{3}
            |\xF4[\x80-\x8F][\x80-\xBF]{2}
            )+%xs', $f_row_p)) {
                $f_row_p = str_replace(chr(226).chr(128).chr(153), "'", $f_row_p);
            } else {
                $f_row_p = str_replace(chr(146), "'", $f_row_p);
            }

            $new_field->c_choice = $f_row_p;
            $new_field->c_right = in_array(($field_order + 1), $ans_right) ? 1 : 0;
            $new_field->ordering = $field_order;

            if (!$new_field->check()) {
                echo "<script> alert('".$new_field->getError()."'); window.history.go(-1); </script>\n";
                exit();
            }
            if (!$new_field->store()) {
                echo "<script> alert('".$new_field->getError()."'); window.history.go(-1); </script>\n";
                exit();
            }
            $fids_arr[] = $new_field->c_id;
            $field_order ++;
            $mcounter ++;
        }
        $fieldss = implode(',', $fids_arr);
        $query = "DELETE FROM #__lms_quiz_t_choice WHERE c_question_id = '".$qid."' AND c_id NOT IN (".$fieldss.")";
        $JLMS_DB->setQuery($query);
        $JLMS_DB->query();

        $c_right_message = '';
        $c_wrong_message = '';
        
        $query = "DELETE FROM #__lms_quiz_t_question_fb WHERE quest_id = $qid";
        $JLMS_DB->SetQuery($query);
        $JLMS_DB->query();
        $query = "INSERT INTO #__lms_quiz_t_question_fb (quest_id, choice_id, fb_text)"
                ."\n VALUES ($qid, -1, ".$JLMS_DB->Quote($c_wrong_message)."),"
                ."\n ($qid, 0, ".$JLMS_DB->Quote($c_right_message).")";
        $JLMS_DB->SetQuery($query);
        $JLMS_DB->query();

        $query = "SELECT SUM(c_point) FROM #__lms_quiz_t_question WHERE c_quiz_id = $row->c_quiz_id";
        $JLMS_DB->SetQuery( $query );
        $total_score = $JLMS_DB->LoadResult();
        $query = "SELECT c_pool FROM #__lms_quiz_t_question WHERE c_quiz_id = $row->c_quiz_id";
        $JLMS_DB->SetQuery($query);
        $ar = JLMSDatabaseHelper::LoadResultArray();
        if (!empty($ar)) {
                $arc = implode(',',$ar);
                $query = "SELECT SUM(c_point) FROM #__lms_quiz_t_question WHERE c_id IN ($arc)";
                $JLMS_DB->SetQuery( $query );
                $total_score_pool = $JLMS_DB->LoadResult();
                $total_score = $total_score + $total_score_pool;
        }
        $query = "UPDATE #__lms_quiz_t_quiz SET c_full_score = '".$total_score."' WHERE c_id = $row->c_quiz_id";
        $JLMS_DB->SetQuery( $query );
        $JLMS_DB->query();

        return true;
    }

    static function addQuizStepToLearningPath($quizId, $learningPathId)
    {
        self::requireJLMSFiles();
        global $JLMS_CONFIG;

        $JLMS_DB = JLMSFactory::getDB();
        $courseId = $JLMS_DB->setQuery("SELECT `course_id` FROM `#__lms_quiz_t_quiz` WHERE `c_id` = ".$JLMS_DB->quote($quizId))->loadResult();
        
        $course_id = $courseId;
        $lpath_id = $learningPathId;

        $lpath_chapter = 0;

        $cid = array($quizId);
        $new_order = 0;
        $query = "SELECT max(ordering) FROM #__lms_learn_path_steps WHERE lpath_id = '".$lpath_id."' AND course_id = '".$course_id."' AND parent_id = '".$lpath_chapter."'";
        $JLMS_DB->SetQuery($query);
        $ordering = $JLMS_DB->LoadResult();
        $new_order = ($ordering + 1);
        
        $i = 0;
        while ($i < count($cid)) {
            $cid[$i] = intval($cid[$i]);
            $i ++;
        }
        $cids = implode(',', $cid);
        $query = "INSERT INTO #__lms_learn_path_steps"
                ."\n (course_id, lpath_id, item_id, step_type, parent_id, step_name, step_description, ordering)"
                ."\n SELECT '".$course_id."', '".$lpath_id."', c_id, 5, '".$lpath_chapter."', c_title, '', '".$new_order."'"
                ."\n FROM #__lms_quiz_t_quiz WHERE c_id IN ( $cids ) AND course_id = '".$course_id."'";
        $JLMS_DB->SetQuery($query);
        $JLMS_DB->query();
        
        $query = "SELECT id FROM #__lms_learn_path_steps WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND parent_id = '".$lpath_chapter."' ORDER BY ordering, step_name, id";
        $JLMS_DB->SetQuery($query);
        $id_array = JLMSDatabaseHelper::LoadResultArray();
        if (count($id_array)) {
            $i = 0;
            foreach ($id_array as $lpath_id) {
                $query = "UPDATE #__lms_learn_path_steps SET ordering = '".$i."' WHERE id = '".$lpath_id."'";
                $JLMS_DB->SetQuery($query);
                $JLMS_DB->query();
                $i ++;
            }
        }
        
        return true;
    }

    static function sendEmailAboutNewCourseToAdmin($courseId)
    {
        $mailer = JFactory::getMailer();
        $sender = array( JFactory::getConfig()->get('mailfrom'), 'Plot system' );
        $mailer->setSender($sender); 
        $mailer->addRecipient( JFactory::getConfig()->get('mailfrom') );
        $mailer->setSubject("Naplotu.com - добавлен новый курс (courseId - $courseId)");
        $mailer->setBody('Ссылка на добавленный курс - '.str_replace('https://', 'http://', JRoute::_("index.php?option=com_joomla_lms&task=details_course&id=$courseId", 0, 1)));
        $send = $mailer->Send();
        return true;
    }
    
    static function sendMessageAboutCoursePublishedToCourseCreator($courseId)
    {
//        $mailer = JFactory::getMailer();
//        $sender = array( JFactory::getConfig()->get('mailfrom'), 'Plot system' );
//        $mailer->setSender($sender); 
//        
//        $mailer->addRecipient( $creatorEmail );
//        $mailer->setSubject("Naplotu.com - добавлен новый курс (courseId - $courseId)");
//        $mailer->setBody('Ссылка на добавленный курс - '.str_replace('https://', 'http://', JRoute::_("index.php?option=com_joomla_lms&task=details_course&id=$courseId", 0, 1)));
//        $send = $mailer->Send();
        return true;
    }
    
    static function requireJLMSFiles()
    {
        if (!defined('_JOOMLMS_FRONT_HOME')) {
            define('_JOOMLMS_FRONT_HOME', JPATH_SITE.'/components/com_joomla_lms');
        }
        
        require_once(JPATH_ROOT."/components/com_joomla_lms/joomla_lms.main.php");
        require_once(_JOOMLMS_FRONT_HOME."/includes/component.legacy.php");
        require_once(_JOOMLMS_FRONT_HOME."/includes/classes/lms.factory.php");
        require_once(_JOOMLMS_FRONT_HOME . DS . "joomla_lms.class.php");
        
        $JLMS_CONFIG = JLMSFactory::getConfig();
        if (!defined('_JOOMLMS_DOC_FOLDER')) {
            define('_JOOMLMS_DOC_FOLDER', $JLMS_CONFIG->get('jlms_doc_folder').DS);
        }
        if (!defined('_JOOMLMS_SCORM_FOLDER')) {
            define('_JOOMLMS_SCORM_FOLDER', $JLMS_CONFIG->get('scorm_folder'));
        }
        if (!defined('_JOOMLMS_SCORM_FOLDER_PATH')) {
            define('_JOOMLMS_SCORM_FOLDER_PATH', JPATH_SITE.DS.$JLMS_CONFIG->get('scorm_folder'));
        }
        if (!defined('_JLMS_PATHWAY_HOME')) {
            define('_JLMS_PATHWAY_HOME', JPATH_BASE.'/components/com_joomla_lms');
        }
        if (!defined('_JLMS_TOOLBAR_DOCS')) {
            define('_JLMS_TOOLBAR_DOCS', JPATH_BASE.'/components/com_joomla_lms');
        }
        
        
        
    }
    
    static function JLMS_getParam_LowFilter(&$arr, $name, $def = '')
    {
        if (isset($arr[$name])) {
            $value = get_magic_quotes_gpc() ? stripslashes($arr[$name]) : $arr[$name];
            return $value;
        } else {
            return $def;
        }
    }

    static function JQ_orderQuest_inside($course_id, $c_quiz, $order_id, $quest_order)
    {
        global $my, $Itemid;
        $JLMS_DB = JLMSFactory::getDB();
        if ($c_quiz) {
            $JLMS_ACL = JLMSFactory::getACL();
            if (!$JLMS_ACL->CheckPermissions('quizzes', 'manage')) {
                if ($JLMS_ACL->CheckPermissions('quizzes', 'manage_pool')) {
                    JLMSRedirect(sefRelToAbs("index.php?option=com_joomla_lms&Itemid=$Itemid&task=quizzes&id=$course_id&page=setup_quest"));
                }
            }
        }
        $query = "SELECT c_id, ordering FROM #__lms_quiz_t_question WHERE course_id = '".$course_id."' AND c_quiz_id = '".$c_quiz."' ORDER BY ordering, c_id";
        $JLMS_DB->SetQuery($query);
        $id_array_obj = $JLMS_DB->LoadObjectList();
        $id_array = array();
        $id_ord_array = array();
        foreach ($id_array_obj as $iao) {
            $id_array[] = $iao->c_id;
            $id_ord_array[$iao->c_id] = $iao->ordering;
        }
        if (count($id_array)) {
            if ($quest_order && ($quest_order != -1)) {
                // we need to find edited question and question by which oredering is setted
                $finded = false;
                $new_id_array = array();
                foreach ($id_array as $ia) {
                    if ($ia == $quest_order) {
                        $new_id_array[] = $order_id;
                        $new_id_array[] = $quest_order;
                        $finded = true;
                    } elseif ($ia == $order_id) {
                        
                    } else {
                        $new_id_array[] = $ia;
                    }
                }
                if (!$finded) {
                    $new_id_array[] = $order_id;
                }
                $id_array = $new_id_array;
            } elseif (!$quest_order) {
                //place editable question as first item
                if (isset($id_array[0]) && $id_array[0] != $order_id) {
                    $new_id_array = array();
                    $new_id_array[] = $order_id;
                    foreach ($id_array as $ia) {
                        if ($ia != $order_id) {
                            $new_id_array[] = $ia;
                        }
                    }
                    $id_array = $new_id_array;
                }
            } elseif ($quest_order == -1) {
                //place editable question as last item
                $new_id_array = array();
                foreach ($id_array as $ia) {
                    if ($ia != $order_id) {
                        $new_id_array[] = $ia;
                    }
                }
                $new_id_array[] = $order_id;
                $id_array = $new_id_array;
            }
            // updating
            $i = 0;
            foreach ($id_array as $quest_id) {
                if (isset($id_ord_array[$quest_id]) && ($id_ord_array[$quest_id] == ($i + 1))) {
                    
                } else {
                    $query = "UPDATE #__lms_quiz_t_question SET ordering = '".($i + 1)."' WHERE c_id = '".$quest_id."' and course_id = '".$course_id."'";
                    $JLMS_DB->SetQuery($query);
                    $JLMS_DB->query();
                }
                $i ++;
            }
        }
    }

    static function JLMSDocs_save_single_file($doc_name_post, $folder_flag, $parent_id, $path, &$ordering, &$default_row, $elemname = '')
    {
        $db = JFactory::getDbo();
        $user = JLMSFactory::getUser();
        $JLMS_CONFIG = JLMSFactory::getConfig();
        
        $row = new mos_Joomla_LMS_Document($db);
        $row->publish_start = 0;
        $row->publish_end = 0;
        if (!$row->bind($_POST)) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }

        $row->folder_flag = $folder_flag;

        $row->id = intval($row->id);
        if ($row->id) {
            $query = "SELECT file_id FROM #__lms_documents WHERE id=".$row->id;
            $db->setQuery($query);
            $row->file_id = $db->loadResult();
        }

        $doc_name_post = (get_magic_quotes_gpc()) ? stripslashes($doc_name_post) : $doc_name_post;
        $doc_name_post = strip_tags($doc_name_post);
        
        if ($folder_flag == 1) {
            $row->doc_name = isset($default_row->doc_name) ? $default_row->doc_name : ($elemname ? $elemname : $doc_name_post);
        } else {
            $row->doc_name = isset($default_row->doc_name) ? $default_row->doc_name : ($elemname ? $elemname : $doc_name_post);
            if ($folder_flag == 2) {
                $file_id = JLMSDocs_uploadZIPPack_fromFile($row->course_id, $path.DS.$doc_name_post, $doc_name_post);
                if ($file_id) {
                    $JLMS_CONFIG->set('zippack_was_uploaded', 1);
                }
            } else {
                if ($path) {
                    $file_id = self::JLMSDocs_uploadFile_from_zip($path, $doc_name_post);
                    if ($file_id === false) {
                        $JLMS_CONFIG->set('files_skipped_by_extension', true);
                        return false;
                    }
                } else {
                    $file_id = 0;
                }
            }
            if ($file_id) {
                if ($row->file_id) {
                    if ($row->id) {
                        $query = "SELECT folder_flag FROM #__lms_documents WHERE id = ".$row->id;
                        $db->SetQuery($query);
                        $old_ff = $db->LoadResult();
                        if ($old_ff == 2) {
                            $zip_ids = array();
                            $zip_ids[] = $row->file_id;
                            JLMS_deleteZipPacks($row->course_id, $zip_ids);
                        } else {
                            JLMS_deleteFiles($row->file_id);
                        }
                    }
                }
                @unlink($path."/".$doc_name_post);
                $row->file_id = $file_id;
            }
        }
        
        $row->start_date = '';
        $row->end_date = '';
        if (isset($default_row->published)) {
            $row->published = $default_row->published;
            $row->is_time_related = $default_row->is_time_related;
            $row->show_period = $default_row->show_period;
        } else {
            unset($row->published);
            unset($row->is_time_related);
            unset($row->show_period);
        }
        if (isset($default_row->publish_start)) {
            $row->publish_start = $default_row->publish_start;
            $row->start_date = $default_row->start_date;
        } else {
            unset($row->publish_start);
        }
        if (isset($default_row->publish_end)) {
            $row->publish_end = $default_row->publish_end;
            $row->end_date = $default_row->end_date;
        } else {
            unset($row->publish_end);
        }

        $row->parent_id = $parent_id;

        if ($row->id) {
            unset($row->owner_id);
            unset($row->ordering);
        } else {
            $row->owner_id = $user->get('id');
            $doc_order = 0;
            if (isset($ordering[$parent_id])) {
                $doc_order = $ordering[$parent_id];
                $ordering[$parent_id] ++;
            } else {
                $ordering[$parent_id] = 1;//ordering for next element of this parent will be = 1 (the current element ordering = 0 )
            }
            $row->ordering = $doc_order;
        }
        $row->doc_description = strval(JRequest::getString('doc_description'));
        $row->doc_description = $row->doc_description;

        if (!$row->check()) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }
        
        if (!$row->store()) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }
        if ($JLMS_CONFIG->get('zippack_was_uploaded', 0)) {
            $JLMS_CONFIG->set('zippack_was_uploaded', $row->id);
        }

        if ($folder_flag || $file_id) {
            return $row->id;
        }
        return false;
    }

    static function JLMSDocs_uploadFile_from_zip($path, $name)
    {
        global $JLMS_DB;

        $is_supported_type = 1;
        $base_Dir = _JOOMLMS_DOC_FOLDER;
        $userfile_name = $name;

        $good_ext = false;
        $file_ext = '';

        if (preg_match('/\S+\.(\S+)$/', $userfile_name, $out)) {
            if (isset($out[0]) && isset($out[1]) && $out[1]) {
                $file_ext = $out[1];
                if (strlen($file_ext) <= 6) {
                    $good_ext = true;
                }
            }
        }

        if (!$good_ext) {
            mosErrorAlert(_JLMS_EM_BAD_FILEEXT);
        }
        $query = "SELECT LOWER(filetype) as filetype FROM #__lms_file_types";
        $JLMS_DB = & JLMSFactory::getDB();             
        $JLMS_DB->SetQuery($query);
        $ftypes = JLMSDatabaseHelper::LoadResultArray();

        if (empty($ftypes) || !in_array(strtolower($file_ext), $ftypes)) {
            //mosErrorAlert(_JLMS_EM_BAD_FILEEXT);
            $is_supported_type = 0;
        }

        if ($is_supported_type) {
            $file_unique_name = str_pad('0', 4, '0', STR_PAD_LEFT).'_'.md5(uniqid(rand(), true)).'.'.$file_ext;

            if (!copy($path."/".$name, $base_Dir.$file_unique_name) || !mosChmod($base_Dir.$file_unique_name)) {
                mosErrorAlert("Upload of ".$userfile_name." failed");
            } else {
                global $JLMS_DB, $my;
                $userfile_name = JLMSDatabaseHelper::GetEscaped($userfile_name);
                $query = "INSERT INTO #__lms_files (file_name, file_srv_name, owner_id)"
                        ."\n VALUES ('".$userfile_name."', '".$file_unique_name."', '".$my->id."')";
                $JLMS_DB = & JLMSFactory::getDB();             
                $JLMS_DB->SetQuery($query);
                $JLMS_DB->query();
                return $JLMS_DB->insertid();
            }
        } else {
            return false;
        }
    }

    static function JLMSDocs_save_file_from_zip_array($elem, $parent, $path, &$ordering, &$first_saved_id, &$default_row, $is_zip_pack, $elemname = array())
    {
        foreach ($elem as $k => $v) {
            
            if (is_array($v)) {
                $path1 = $path."/".$k;
                self::JLMSDocs_save_single_file($k, 1, $parent, '', $ordering, $default_row);
                if (!$first_saved_id) {
                    $first_saved_id = $parent1;
                }
                
                self::JLMSDocs_save_file_from_zip_array($v, $parent1, $path1, $ordering, $first_saved_id, $default_row, $new_elem_name);
            } else {
                $new_elem_name = '';
                if (is_array($elemname) && count($elemname)) {
                    $new_elem_name = isset($elemname[$k]) ? $elemname[$k] : '';
                }
                $out_fileext = strtolower(substr($k, - 4));
                $new_ff = ($is_zip_pack && $out_fileext == '.zip') ? 2 : 0;
                
                $saved_elem = self::JLMSDocs_save_single_file($k, $new_ff, $parent, $path, $ordering, $default_row, $new_elem_name);
                
                if (!$first_saved_id) {
                    $first_saved_id = $saved_elem;
                }
            }
        }
        return $saved_elem;
    }

    static function FillList($course_id, &$docs_rows, &$docs_possibilities, $folders = false, $show_unavailable = true)
    {
        global $JLMS_DB, $my;
        $JLMS_DB = & JLMSFactory::getDB();             
        $my = plotUser::factory();
        $JLMS_ACL = JLMSFactory::getACL();

        $AND_ST = "";
        if (false !== ( $enroll_period = JLMS_getEnrolPeriod($my->id, $course_id))) {
            $AND_ST = " AND IF(a.is_time_related, (a.show_period < '".$enroll_period."' ), 1) ";
        }

        $query = "SELECT a.*, b.file_name, c.name, c.username, c.name as author_name, dp.*, dp.doc_id as doc_perms_db, dp.doc_id as doc_perms, dp.p_manage as p_create, dp.p_publish as p_publish_childs"
                ."\n FROM #__lms_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0 LEFT JOIN #__users as c ON a.owner_id = c.id"
                ."\n LEFT JOIN #__lms_documents_perms as dp ON  a.id = dp.doc_id AND dp.role_id = ".$JLMS_ACL->GetRole()
                ."\n WHERE a.course_id = '".$course_id."'".$AND_ST
                .($folders ? "\n AND a.folder_flag = 1" : "") // select only folders, if necessary
                //. (($JLMS_ACL->CheckPermissions('docs', 'view_all')) ? '' : "\n AND a.published = 1")
                //. (($JLMS_ACL->CheckPermissions('docs', 'view_all')) ? '' : "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )")
                //. (($JLMS_ACL->CheckPermissions('docs', 'view_all')) ? '' : "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d')."')) OR (a.publish_end = 0) )")
                // this code is commented by DEN:
                // because you can reassign 'viewall' permission using folder's "custom permissions" feature
                // HERE: possibly we can insert 'ignore_permssions' cehck here in adition to 'view_all' check (to decrease number of selected records)
                ."\n ORDER BY a.parent_id, a.ordering, a.doc_name, c.username";
        
        $JLMS_DB->SetQuery($query);
        $rows = $JLMS_DB->LoadObjectList();

        if (!$folders) { // not only folders are present in 'rows' - we need to filter/remove unnecessary items
            $bad_in = array();
            $rows_n = array();
            for ($j = 0; $j < count($rows); $j++) {
                // TODO: redevelop this part of the code, DO NOT USE database queries within the for/while/foreach loops
                if ($rows[$j]->folder_flag == 3) { // this is a file-library link
                    $query = "SELECT a.*, b.file_name"
                            ."\n FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0 "
                            ."\n WHERE a.folder_flag = 0 AND a.id = ".$rows[$j]->file_id;

                    $JLMS_DB->SetQuery($query);
                    $out_row = $JLMS_DB->LoadObjectList();

                    if (count($out_row) && ($out_row[0]->allow_link == 1)) {
                        // resource is found in the Library
                        $rows[$j]->doc_name = $out_row[0]->doc_name;
                        $rows[$j]->doc_description = $out_row[0]->doc_description;
                        $rows[$j]->file_id = $out_row[0]->file_id;
                        $rows[$j]->file_name = $out_row[0]->file_name;
                    } else {
                        // there is no link in the Library (e.g. file-library file was removed or its permissions were changed)
                        if ($JLMS_ACL->CheckPermissions('docs', 'manage') && $show_unavailable) {
                            // show 'Resource is not available' message instead of a file
                            $rows[$j]->doc_name = _JLMS_LP_RESOURSE_ISUNAV;
                            $rows[$j]->is_link = 1;
                            $rows[$j]->author_name = '';
                        } else {
                            // remove item from the array
                            $g = 0;
                            $rows_n = array();
                            // What the fuck?....
                            for ($z = 0; $z < count($rows); $z++) {
                                if ($z != $j && !in_array($z, $bad_in)) {
                                    $g++;
                                } else {
                                    // populate array of the items which need to be removed from the list of files
                                    $bad_in[] = $j;
                                }
                            }
                        }
                    }
                }
            }
            if (count($bad_in)) {
                // remove 'not available' items from the list of files
                $rows_n = array();
                $g = 0;
                for ($z = 0; $z < count($rows); $z++) {
                    if (in_array($z, $bad_in)) {
                        
                    } else {
                        $rows_n[$g] = $rows[$z];
                        $g++;
                    }
                }
                $rows = $rows_n;
            }
        }

        $rows = JLMS_GetTreeStructure($rows);

        $rows = AppendFileIcons_toList($rows);

        $possibilities = new stdClass();
        if (true) {//proceed in any case, even if we have 'ignore_permissions'
            // check notes about custom permissions at the top of the 'documents' source file (after $task section)
            $bad_in = array();
            $permissions = array();
            $permissions[0] = new stdClass();
            $permissions[0]->active = 1; // not used any more ????
            $permissions[0]->p_view = $possibilities->view = $JLMS_ACL->CheckPermissions('docs', 'view') ? 1 : 0;
            $permissions[0]->p_viewall = $possibilities->viewall = $JLMS_ACL->CheckPermissions('docs', 'view_all') ? 1 : 0;
            $permissions[0]->p_order = $possibilities->order = $JLMS_ACL->CheckPermissions('docs', 'order') ? 1 : 0;
            $permissions[0]->p_publish = $possibilities->publish = $JLMS_ACL->CheckPermissions('docs', 'publish') ? 1 : 0;
            $permissions[0]->p_publish_childs = $possibilities->publish_childs = $JLMS_ACL->CheckPermissions('docs', 'publish') ? 1 : 0;
            $permissions[0]->p_manage = $possibilities->manage = $JLMS_ACL->CheckPermissions('docs', 'manage') ? 1 : 0;
            $permissions[0]->p_create = $possibilities->create = $JLMS_ACL->CheckPermissions('docs', 'manage') ? 1 : 0;
            $folder_id = 0;
            $j = 0;
            $n = count($rows);
            while ($j < $n) {
                if ($rows[$j]->folder_flag == 1 && isset($rows[$j]->doc_perms_db) && $rows[$j]->doc_perms_db) { // folder with configured custom permissions
                    $old_folder_id = $rows[$j]->parent_id;//$folder_id;
                    // process permissions (add them into array from which they will be associated with child items)
                    $folder_id = $rows[$j]->id;
                    $parent_id = $rows[$j]->parent_id;

                    if ($JLMS_ACL->CheckPermissions('docs', 'ignore_permissions')) {
                        $permissions[$folder_id] = new stdClass();
                        $permissions[$folder_id] = $permissions[0];
                        $rows[$j]->p_view = $permissions[0]->p_view;
                        $rows[$j]->p_viewall = $permissions[0]->p_viewall;
                        $rows[$j]->p_order = $permissions[0]->p_order;
                        $rows[$j]->p_publish = $permissions[0]->p_publish;
                        $rows[$j]->p_manage = $permissions[0]->p_manage;
                        $rows[$j]->p_create = $permissions[0]->p_create;
                        $rows[$j]->p_publish_childs = $permissions[0]->p_publish_childs;
                    } else {
                        $permissions[$folder_id] = new stdClass();
                        $permissions[$folder_id]->active = 1; // not used any more ????
                        $permissions[$folder_id]->p_view = $rows[$j]->p_view;
                        $permissions[$folder_id]->p_viewall = $permissions[$folder_id]->p_view ? $rows[$j]->p_viewall : 0;
                        $permissions[$folder_id]->p_order = $permissions[$folder_id]->p_view ? $rows[$j]->p_order : 0;
                        $permissions[$folder_id]->p_publish = $permissions[$folder_id]->p_view ? $rows[$j]->p_publish : 0;
                        $permissions[$folder_id]->p_publish_childs = $permissions[$folder_id]->p_view ? $rows[$j]->p_publish_childs : 0;
                        $permissions[$folder_id]->p_manage = $permissions[$folder_id]->p_view ? $rows[$j]->p_manage : 0;// "no view - no manage" // 'no martiny - no party ;)'
                        $permissions[$folder_id]->p_create = $permissions[$folder_id]->p_view ? $rows[$j]->p_create : 0;
                        // set parent's permissions for this folder
                        $rows[$j]->doc_perms = 1;
                        $rows[$j]->p_view = $permissions[$old_folder_id]->p_view ? $rows[$j]->p_view : 0;
                        $rows[$j]->p_viewall = $permissions[$old_folder_id]->p_viewall ? $rows[$j]->p_viewall : 0;
                        $rows[$j]->p_order = $permissions[$old_folder_id]->p_order ? $rows[$j]->p_order : 0;
                        $rows[$j]->p_publish = $permissions[$old_folder_id]->p_publish ? $rows[$j]->p_publish : 0;
                        $rows[$j]->p_manage = $permissions[$old_folder_id]->p_manage ? $rows[$j]->p_manage : 0;
                        $rows[$j]->p_create = $old_folder_id ? $permissions[$old_folder_id]->p_create : $rows[$j]->p_create;
                        $rows[$j]->p_publish_childs = $old_folder_id ? $permissions[$old_folder_id]->p_publish_childs : $rows[$j]->p_publish_childs;
                    }

                    // change 'view' permission regarding to the 'published' status of the item
                    if (!$rows[$j]->p_viewall) {
                        if ($rows[$j]->published && $permissions[$old_folder_id]->p_view &&
                                ( ($rows[$j]->publish_start && strtotime($rows[$j]->start_date) <= strtotime(date('Y-m-d'))) || !$rows[$j]->publish_start ) &&
                                ( ($rows[$j]->publish_end && strtotime($rows[$j]->end_date) >= strtotime(date('Y-m-d'))) || !$rows[$j]->publish_end )
                        ) {
                            // user can view this item
                        } elseif ($rows[$j]->owner_id == $my->id && $rows[$j]->p_manage) {
                            // this item is unpublished, but user is its owner - he can view it if he has 'manage' rights
                        } else {
                            $rows[$j]->p_view = 0;
                            $rows[$j]->p_viewall = 0;
                            $rows[$j]->p_order = 0;
                            $rows[$j]->p_publish = 0;
                            $rows[$j]->p_manage = 0;
                            $rows[$j]->p_create = 0;
                            $rows[$j]->p_publish_childs = 0;
                            $permissions[$folder_id]->p_view = 0;
                            $permissions[$folder_id]->p_viewall = 0;
                            $permissions[$folder_id]->p_order = 0;
                            $permissions[$folder_id]->p_publish = 0;
                            $permissions[$folder_id]->p_manage = 0;
                            $permissions[$folder_id]->p_create = 0;
                            $permissions[$folder_id]->p_publish_childs = 0;
                        }
                    }

                    $possibilities->create = $possibilities->create ? $possibilities->create : $rows[$j]->p_create;// we can create docs at least in this folder
                    $possibilities->publish_childs = $possibilities->publish_childs ? $possibilities->publish_childs : $rows[$j]->p_publish_childs;
                } else { // any other item: file,  file-library link or folder without custom permissions
                    $folder_id = $rows[$j]->parent_id;
                    if (isset($permissions[$folder_id]) && isset($permissions[$folder_id]->active) && $permissions[$folder_id]->active) {
                        // set parent's permissions for this item
                        $rows[$j]->doc_perms = 1;
                        $rows[$j]->p_view = $permissions[$folder_id]->p_view;
                        $rows[$j]->p_viewall = $permissions[$folder_id]->p_viewall;
                        $rows[$j]->p_order = $permissions[$folder_id]->p_order;
                        $rows[$j]->p_publish = $permissions[$folder_id]->p_publish;
                        $rows[$j]->p_manage = $permissions[$folder_id]->p_manage;
                        $rows[$j]->p_create = 0;//$permissions[$folder_id]->p_create;
                        $rows[$j]->p_publish_childs = 0;//$permissions[$folder_id]->p_publish_childs;
                        // change 'view' permission regarding to the 'published' status of the item
                        if (!$rows[$j]->p_viewall) {
                            if ($rows[$j]->published && $permissions[$folder_id]->p_view &&
                                    ( ($rows[$j]->publish_start && strtotime($rows[$j]->start_date) <= strtotime(date('Y-m-d'))) || !$rows[$j]->publish_start ) &&
                                    ( ($rows[$j]->publish_end && strtotime($rows[$j]->end_date) >= strtotime(date('Y-m-d'))) || !$rows[$j]->publish_end )
                            ) {
                                // user can view this item
                            } elseif ($rows[$j]->owner_id == $my->id && $rows[$j]->p_manage) {
                                // this item is unpublished, but user is its owner - he can view it if he has 'manage' rights
                            } else {
                                $rows[$j]->p_view = 0;
                                $rows[$j]->p_viewall = 0;
                                $rows[$j]->p_order = 0;
                                $rows[$j]->p_publish = 0;
                                $rows[$j]->p_manage = 0;
                                $rows[$j]->p_create = 0;
                                $rows[$j]->p_publish_childs = 0;
                                if ($rows[$j]->folder_flag == 1) { // just a folder (without custom permissions)
                                    $folder_id_current = $rows[$j]->id;
                                    if (!isset($permissions[$folder_id_current])) {
                                        $permissions[$folder_id_current] = new stdClass();
                                    }
                                    $permissions[$folder_id_current]->p_view = 0;
                                    $permissions[$folder_id_current]->p_viewall = 0;
                                    $permissions[$folder_id_current]->p_order = 0;
                                    $permissions[$folder_id_current]->p_publish = 0;
                                    $permissions[$folder_id_current]->p_manage = 0;
                                    $permissions[$folder_id_current]->p_create = 0;
                                    $permissions[$folder_id_current]->p_publish_childs = 0;
                                }
                            }
                        }
                        $possibilities->view = $possibilities->view ? $possibilities->view : $rows[$j]->p_view;
                        $possibilities->viewall = $possibilities->view ? $possibilities->viewall : $rows[$j]->p_viewall;
                        $possibilities->order = $possibilities->order ? $possibilities->order : $rows[$j]->p_order;
                        $possibilities->publish = $possibilities->publish ? $possibilities->publish : $rows[$j]->p_publish;
                        $possibilities->manage = $possibilities->manage ? $possibilities->manage : $rows[$j]->p_manage;
                        //$possibilities->create = $possibilities->create ? $possibilities->create : $rows[$j]->p_create;
                        //$possibilities->publish_childs = $possibilities->publish_childs ? $possibilities->publish_childs : $rows[$j]->p_publish_childs;
                    }
                    if ($rows[$j]->folder_flag == 1) { // just a folder (without custom permissions)
                        $rows[$j]->p_create = $permissions[$folder_id]->p_create;
                        $rows[$j]->p_publish_childs = $permissions[$folder_id]->p_publish_childs;
                        $possibilities->create = $possibilities->create ? $possibilities->create : $rows[$j]->p_create;
                        $possibilities->publish_childs = $possibilities->publish_childs ? $possibilities->publish_childs : $rows[$j]->p_publish_childs;
                        // HERE: inherit permissions from the parent folder
                        $folder_id = $rows[$j]->id;
                        $parent_id = $rows[$j]->parent_id;
                        if (isset($permissions[$parent_id]) && isset($permissions[$parent_id]->active) && $permissions[$parent_id]->active) {
                            if (!isset($permissions[$folder_id])) {
                                $permissions[$folder_id] = new stdClass();
                            }
                            $permissions[$folder_id]->active = 1;
                            $permissions[$folder_id]->p_view = ( (isset($permissions[$folder_id]->p_view) && $permissions[$folder_id]->p_view) || !isset($permissions[$folder_id]->p_view)) ? $permissions[$parent_id]->p_view : $permissions[$folder_id]->p_view;
                            $permissions[$folder_id]->p_viewall = ( (isset($permissions[$folder_id]->p_viewall) && $permissions[$folder_id]->p_viewall) || !isset($permissions[$folder_id]->p_viewall)) ? $permissions[$parent_id]->p_viewall : $permissions[$folder_id]->p_viewall;
                            $permissions[$folder_id]->p_order = ( (isset($permissions[$folder_id]->p_order) && $permissions[$folder_id]->p_order) || !isset($permissions[$folder_id]->p_order)) ? $permissions[$parent_id]->p_order : $permissions[$folder_id]->p_order;
                            $permissions[$folder_id]->p_publish = ( (isset($permissions[$folder_id]->p_publish) && $permissions[$folder_id]->p_publish) || !isset($permissions[$folder_id]->p_publish)) ? $permissions[$parent_id]->p_publish : $permissions[$folder_id]->p_publish;
                            $permissions[$folder_id]->p_manage = ( (isset($permissions[$folder_id]->p_manage) && $permissions[$folder_id]->p_manage) || !isset($permissions[$folder_id]->p_manage)) ? $permissions[$parent_id]->p_manage : $permissions[$folder_id]->p_manage;
                            $permissions[$folder_id]->p_create = ( (isset($permissions[$folder_id]->p_create) && $permissions[$folder_id]->p_create) || !isset($permissions[$folder_id]->p_create)) ? $permissions[$parent_id]->p_create : $permissions[$folder_id]->p_create;
                            $permissions[$folder_id]->p_publish_childs = ( (isset($permissions[$folder_id]->p_publish_childs) && $permissions[$folder_id]->p_publish_childs) || !isset($permissions[$folder_id]->p_publish_childs)) ? $permissions[$parent_id]->p_publish_childs : $permissions[$folder_id]->p_publish_childs;
                            if (!$permissions[$folder_id]->p_view) {
                                // "no view - no manage" // 'no martiny - no party ;)'
                                $permissions[$folder_id]->p_viewall = 0;
                                $permissions[$folder_id]->p_order = 0;
                                $permissions[$folder_id]->p_publish = 0;
                                $permissions[$folder_id]->p_manage = 0;
                                $permissions[$folder_id]->p_create = 0;
                                $permissions[$folder_id]->p_publish_childs = 0;
                            }
                        }
                    }
                }
                $j ++;
            }
        }

        $docs_rows = $rows;
        $docs_possibilities = $possibilities;
    }

    static function JLMS_dateToDB($date)
    {
        //09.03.2007
        $time = time();

        $format = JLMS_dateGetFormat(2);

        $timeSuff = false;

        if ($format == 'Y-m-d') {
            $time = strtotime($date);

            if ($time !== false) {
                $timeArr = getdate($time);
                if ($timeArr['hours'] > 0 || $timeArr['minutes'] > 0) {
                    $timeSuff = true;
                }
            } else {
                $time = time();
            }
        } elseif ($format == 'd-m-Y') {
            if ($date && preg_match("/([0-9]{2})-([0-9]{2})-([0-9]{4})\s+([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})/", $date, $regs)) {
                $timeSuff = true;
                $time = mktime($regs[4], $regs[5], $regs[6], $regs[2], $regs[1], $regs[3]);
            } else
            if ($date && preg_match("/([0-9]{2})-([0-9]{2})-([0-9]{4})\s+([0-9]{1,2}):([0-9]{1,2})/", $date, $regs)) {
                $timeSuff = true;
                $time = mktime($regs[4], $regs[5], 0, $regs[2], $regs[1], $regs[3]);
            } else if ($date && preg_match("/([0-9]{2})-([0-9]{2})-([0-9]{4})\s+([0-9]{1,2})/", $date, $regs)) {
                $time = mktime($regs[4], 0, 0, $regs[2], $regs[1], $regs[3]);
            } else if ($date && preg_match("/([0-9]{2})-([0-9]{2})-([0-9]{4})/", $date, $regs)) {
                $time = mktime(0, 0, 0, $regs[2], $regs[1], $regs[3]);
            }

            if ($time > -1) {
                // do nothing (all OK)
            } else {
                $time = time();
            }
        } elseif ($format == 'm-d-Y') {
            if ($date && preg_match("/([0-9]{2})-([0-9]{2})-([0-9]{4})\s+([0-9]{1,2}):([0-9]{1,2})/", $date, $regs)) {
                $timeSuff = true;
                $time = mktime($regs[4], $regs[5], 0, $regs[1], $regs[2], $regs[3]);
            } else if ($date && preg_match("/([0-9]{2})-([0-9]{2})-([0-9]{4})\s+([0-9]{1,2})/", $date, $regs)) {
                $time = mktime($regs[4], 0, 0, $regs[1], $regs[2], $regs[3]);
            } else if ($date && preg_match("/([0-9]{2})-([0-9]{2})-([0-9]{4})/", $date, $regs)) {
                $time = mktime(0, 0, 0, $regs[1], $regs[2], $regs[3]);
            }

            if ($time > -1) {
                // do nothing (all OK)
            } else {
                $time = time();
            }
        }

        if ($timeSuff) {
            $date_to_db = date('Y-m-d H:i:s', $time);
        } else {
            $date_to_db = date('Y-m-d', $time);
        }

        return $date_to_db;
    }

    static function CheckCourseID(&$rows, $id, $course_id) {
	if (!$course_id) { return false; }
	if (!$id) { return true; }
	$row = & JLMSDocs::GetItembyID($rows, $id);
	if (!is_null($row)) {
		if ($row->course_id == $course_id) {
			return true;
		}
	}
	return false;
}    
    
    static function checkIfFileIsVideo($uploadedFile)
    {
        if (strstr($uploadedFile['type'], "video/")) {
            return true;
        }
        return false;
    }

    static function getLearningPathId($courseId)
    {
        $db = JFactory::getDbo();
        $learningPathId = $db->setQuery("SELECT `id` FROM `#__lms_learn_paths` WHERE `course_id` = ".$db->quote($courseId))->loadResult();
        return $learningPathId;
    }
    
}

if (!function_exists('JLMS_orderLPathStepList')) {

    function JLMS_orderLPathStepList($id_array)
    {
        global $JLMS_DB;
        $i = 0;
        foreach ($id_array as $lpath_id) {
            $query = "UPDATE #__lms_learn_path_steps SET ordering = '".$i."' WHERE id = '".$lpath_id."'";
            $JLMS_DB->SetQuery($query);
            $JLMS_DB->query();
            $i ++;
        }
    }

}
