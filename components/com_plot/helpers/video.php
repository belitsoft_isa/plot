<?php
defined('_JEXEC') or die('Restricted access');

class plotVideoHelper
{

    static function convert_media($inputFile, $outputFile)
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            # windows server
            $ffmpeg = JPATH_ROOT.'/components/com_plot/libraries/ffmpeg2/ffmpeg/bin/ffmpeg';
        } else {
            $ffmpeg = 'ffmpeg';
        }            
        
        $cmd = "$ffmpeg -i $inputFile -vcodec libx264 -acodec aac -strict experimental -ar 44100 $outputFile";
        if (pathinfo($outputFile, PATHINFO_EXTENSION) == 'flv') {
            $cmd = "$ffmpeg -i $inputFile -acodec aac -strict experimental -ar 44100 $outputFile";
        }
        shell_exec($cmd);

        return $cmd;
    }
    
    static function createVideoFileThumb($srcFile, $destFile)
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            # windows server
            $ffmpeg = JPATH_ROOT.'/components/com_plot/libraries/ffmpeg2/ffmpeg/bin/ffmpeg';
        } else {
            $ffmpeg = 'ffmpeg';
        }        
        $cmd = "$ffmpeg -i $srcFile -r 1 -vframes 1 -ss 00:00:03 -s vga -f image2 $destFile";
        exec($cmd);
        return true;
    }    

}
