<?php
/**
 * HTML5FlippingBook Component
 * @package HTML5FlippingBook
 * @author JoomPlace Team
 * @copyright Copyright (C) JoomPlace, www.joomplace.com
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */

defined('_JEXEC') or die('Restricted access');

abstract class HTML5FlippingBookFrontHelper
{
    
    
	/**
	 * Method to make publication data
	 *
	 * @param bool   $mobile  Variable to determine if page opened on mobile phone
	 * @param object $item    Publication data
	 * @param bool   $link    TRUE if want to return only publication link, e.g. for email
	 *
	 * @return stdClass
	 */
	public static function htmlPublHelper($mobile, $item, $link = FALSE)
	{
		$user = JFactory::getUser();
		$data = new stdClass();

		if ($link)
		{
			$data->rawPublicationLink= 'index.php?option='.com_html5flippingbook.'&view=publication&id='.$item;
			$data->publicationLink = JRoute::_($data->rawPublicationLink.'&tmpl=component', false, -1);
			return $data;
		}

		$linkTitle = JText::_('COM_HTML5FLIPPINGBOOK_FE_VIEW_PUBLICATION');
		$popupWidth = $item->width * 2 + 66;
		$popupHeight = $item->height + 100;

		$data->rawPublicationLink= 'index.php?option=com_html5flippingbook&view=publication&id='.$item->c_id;

		if ($mobile)
		{
			$data->publicationLink = JRoute::_($data->rawPublicationLink.'&layout=mobile&tmpl=component', false, -1);
			$data->viewPublicationLink= '<a href="'.$data->publicationLink.'" target="_blank" target="_self">';
			$data->viewPublicationLinkWithTitle = '<a class="thumbnail" href="'.$data->publicationLink.'" target="_blank" target="_self" title="'.$linkTitle .'">';
		}
		elseif($item->c_popup == PublicationDisplayMode::DirectLink)
		{
			//$publicationLink = JRoute::_($rawPublicationLink, false, -1);
			$data->publicationLink = JRoute::_($data->rawPublicationLink.'&tmpl=component' . ($item->uid == $user->get('id') && ($item->page) ? '#page/' . $item->page : ''), false, -1);
			$data->viewPublicationLink= '<a href="'.$data->publicationLink.'" target="_blank" target="_self">';
			$data->viewPublicationLinkWithTitle = '<a class="thumbnail" href="'.$data->publicationLink.'" target="_blank" target="_self" title="'.$linkTitle .'">';
		}
		else if ($item->c_popup == PublicationDisplayMode::DirectLinkNoTmpl)
		{
			$data->publicationLink = JRoute::_($data->rawPublicationLink.'&tmpl=component' . ($item->uid == $user->get('id') && isset($item->page) ? '#page/' . $item->page : ''), false, -1);
			$data->viewPublicationLink= '<a href="'.$data->publicationLink.'" target="_blank">';
			$data->viewPublicationLinkWithTitle = '<a class="thumbnail" href="'.$data->publicationLink.'" target="_blank" title="'.$linkTitle .'">';
		}
		else if ($item->c_popup == PublicationDisplayMode::PopupWindow)
		{
			$data->publicationLink = JRoute::_($data->rawPublicationLink.'&tmpl=component' . ($item->uid == $user->get('id') && isset($item->page) ? '#page/' . $item->page : ''), false, -1);
			$data->viewPublicationLink= '<a href="javascript: ht5popupWindow(\''.$data->publicationLink.'\', \'fm_'.$item->c_id.'\', '.$popupWidth.', '.$popupHeight.', \'no\');">';
			$data->viewPublicationLinkWithTitle = '<a class="thumbnail" href="javascript: ht5popupWindow(\''.$data->publicationLink.'\', \''.$item->c_id.'\', '.$popupWidth.', '.$popupHeight.', \'no\');" title="'.$linkTitle .'">';
		}
		else if ($item->c_popup == PublicationDisplayMode::ModalWindow)
		{
			$data->publicationLink = JRoute::_($data->rawPublicationLink."&tmpl=component" . ($item->uid == $user->get('id') && isset($item->page) ? '#page/' . $item->page : ''), false, -1);
			$data->viewPublicationLink= '<a class="modal" rel="{handler: \'iframe\', size: {x: '.$popupWidth.', y:'.$popupHeight.'}}" href="'.$data->publicationLink.'">';
			$data->viewPublicationLinkWithTitle = '<a class="thumbnail modal" rel="{handler: \'iframe\', size: {x: '.$popupWidth.', y:'.$popupHeight.'}}" href="'.$data->publicationLink.'" title="'.$linkTitle .'">';
		}

		// Preparing Publication's thumbnail.

		$thumbnailPath = JPATH_SITE.'/media/com_html5flippingbook'.'/thumbs/'.$item->c_thumb;

		if ($item->c_thumb == "" || !is_file($thumbnailPath))
		{
			$data->thumbnailUrl = JURI::root()."components".DIRECTORY_SEPARATOR."com_html5flippingbook".DIRECTORY_SEPARATOR."assets".DIRECTORY_SEPARATOR."images".DIRECTORY_SEPARATOR."no_image.png";
		}
		else
		{
			$data->thumbnailUrl = JURI::root()."media/com_html5flippingbook/thumbs/".$item->c_thumb;
		}

		return $data;
	}

	/**
	 * Method to create button block
	 *
	 * @param object $item      Publication data
	 * @param string $list      Tab list
	 * @param string $position  Extra class for button block
	 * @param object $config    Component parameters
	 *
	 * @return string
	 */
	public static function publActionButtonBlock($item, $list, $position, $config)
	{
		$content = '
		<div class="btn-group ' . $position . '">

			<ul class="dropdown-menu">';

		if ($list == 'reading')
		{
			$content .= '';
		}
		elseif ($list == 'favorite')
		{
			$content .= '';
		}

		$content .= '
				<li>';


		$content .= '
				</li>';

		if ($config->social_email_use)
		{
			$content .= '
					<li>
						<a href="javascript: void(0);" onclick="userPublAction(' . $item->c_id . ', \'sendtofriend\', \'' . $list . '\'); return false;">
							<i class="fa fa-envelope"></i> ' . JText::_('COM_HTML5FLIPPINGBOOK_FE_ACTION_SEND_TO_FRIEND') . '
						</a>
					</li>';
		}

		if ($config->social_jomsocial_use)
		{
			$content .= '
					<li>
						<a href="javascript: void(0);" onclick="userPublAction(' . $item->c_id . ', \'share\', \'' . $list . '\'); return false;">
							<i class="fa fa-share-alt"></i> ' . JText::_('COM_HTML5FLIPPINGBOOK_FE_ACTION_JOMSOCIAL_SHARE') . '
						</a>
					</li>';
		}

		$content .= '</ul>
		</div>';

		return $content;
	}

	/**
	 * Method to create bookshelf
	 *
	 * @param string $list      Tab list
	 * @param array  $shelf     Publication data
	 * @param bool   $isMobile  Variable to determine if page opened on mobile phone
	 * @param int    $shelfN    Shelf number
	 * @param object $config    Component parameters
	 *
	 * @return string
	 */
	public static function createBookShelf($list = 'reading', $shelf = array(), $isMobile = FALSE, $shelfN = 1, $config, $buy=false)
	{
		$str = '';
		$rowCount = count($shelf);
		foreach ($shelf as $i => $item)
		{
			$data = self::htmlPublHelper($isMobile, $item);
			$tooltip = '';
			if (strlen($item->c_pub_descr) != "")
			{
				if (strlen($item->c_pub_descr) <= 990)
				{
					$tooltip = '<strong>' . $item->c_title . '</strong><br/>';
				}
				else
				{
					$tooltip = '<strong>' . $item->c_title . '</strong><br/>';
				}
			}

			if ($i == 0)
			{
				$str .= '<div class="row-' . $shelfN . '">';
			}

			if (($i + 4) % 4 == 0)
			{
				$str .= '   <div class="loc">';
			}

			//$str .= '<div class="' . $list . '-pub-' . $item->c_id .' ' . ($item->read ? 'hide-publ' : '') . '" ' . ($item->read ? 'style="display: none;"' : '') . '>';
            $str .= '<div class="' . $list . '-pub-' . $item->c_id .' '. '" ' . '>';
			$str .= self::publActionButtonBlock($item, $list, '', $config);
			$str .= '	<div class="book hasTooltip" title="' . $tooltip . '">';
			//$str .= str_replace("thumbnail", "", $data->viewPublicationLinkWithTitle);
			$str .= '		    <img class="pub-' . $item->c_id .'" src="' . $data->thumbnailUrl . '" alt="' . htmlspecialchars($item->c_title) . '" />';
			//$str .= '       </a>';
			$str .= '   </div>';
			$str .= '</div>';
            if((int)Foundry::user()->profile_id == (int)plotGlobalConfig::getVar('parentProfileId')){
                $id    = JRequest::getInt( 'id' , 0 );
                if( $id == 0 )
                {
                    $user= Foundry::user();
                }else{
                    $user=Foundry::user($id);
                }

                if( $user->id==Foundry::user()->id )
                {
                    $str.='<a href="'.JRoute::_("index.php?option=com_plot&view=readbook&bookId=".$item->c_id).'">'.JText::_("COM_PLOT_READ_BOOK").'</a>';
                }/*elseif(self::checkIsBoughtBook($item->c_id)){
                    $str.='<a href="'.JRoute::_("index.php?option=com_plot&view=readbook&bookId=".$item->c_id.'&id='.(int)$user->id).'">'.JText::_("COM_PLOT_READ_BOOK").'</a>';
                }*/else{
                    $str.='<a href="'.JRoute::_("index.php?option=com_plot&view=book&bookId=".$item->c_id.'&id='.(int)$user->id).'">'.JText::_("COM_PLOT_BUY_BOOK").'</a>';
                }

            }else{
                $id    = JRequest::getInt( 'id' , 0 );
                if( $id == 0 )
                {
                    $user= Foundry::user();
                }else{
                    $user=Foundry::user($id);
                }
                if( $user->id == Foundry::user()->id )
                {
                    $id 	= Foundry::user()->id;
                }
                $user 	= Foundry::user( $id );
                if($user->id==Foundry::user()->id){
                    $str.='<a href="'.JRoute::_("index.php?option=com_plot&view=readbook&bookId=".$item->c_id).'">'.JText::_("COM_PLOT_READ_BOOK").'</a>';
                }

            }
            //$str.='<div class="book-descr">'.  $item->c_pub_descr.'</div>';
			if (($i + 1) % 4 == 0 || (($i + 1) == $rowCount))
			{
				$str .= '   </div>';
			}

			if ($i + 1 == $rowCount)
			{
				$str .= '</div>';
			}


		}

		return $str;
	}

	/**
	 * Method to create publication list
	 *
	 * @param string $list           Tab list
	 * @param array  $listData       Publication data
	 * @param bool   $isMobile       Variable to determine if page opened on mobile phone
	 * @param string $publbuttontext Link text which can display instead of default text (View publication)
	 * @param object $config         Component parameters
	 *
	 * @return string
	 */
	public static function createPublicationList($list, $listData, $isMobile, $publbuttontext, $config)
	{
		$str = '';
		$k = 0;
		foreach ($listData as $item)
		{
			if ($item->read != 1) $k++;

			$data = self::htmlPublHelper($isMobile, $item);

			$str .= '<li class="html5fb-list-item ' . $list . '-pub-' . $item->c_id . ' ' . ($item->read ? 'hide-publ' : '') . '" ' . ($item->read ? 'style="display: none;"' : '') . '>';
			$str .= '   <div class="html5fb-top" style="display: none" onclick="backToTop();"><span class="fa fa-arrow-up"></span></div>';
			$str .= '	<div class="list-overlay" style="display: none;"></div>';
			$str .= '	<div class="html5fb-pict">';
			$str .= $data->viewPublicationLinkWithTitle;
			$str .= '           <img class="html5fb-img" src="' . $data->thumbnailUrl . '" alt="' . htmlspecialchars($item->c_title) . '" />';
			$str .= '       </a>';
			$str .= '   </div>';

			$str .= '	<div class="html5fb-descr">';
			$str .= '		<div class="pull-left">';
			$str .= '           <h3 class="html5fb-name pub-' . $item->c_id . '">';
			$str .= str_replace("thumbnail", "", $data->viewPublicationLinkWithTitle) . htmlspecialchars($item->c_title);
			$str .= '               </a><br/>';
			$str .= '               <small>' . $item->c_author . '</small>';
			$str .= '           </h3>';
			$str .= '       </div>';

			$str .= self::publActionButtonBlock($item, $list, 'pull-right', $config);

			$str .= '       <br clear="all">';

			if (strlen($item->c_pub_descr) != "")
			{
				$str .= '   <p>';
				if (strlen($item->c_pub_descr) <= 990)
				{
					$str .= $item->c_pub_descr;
				}
				else
				{
					$str .= substr($item->c_pub_descr, 0, strpos($item->c_pub_descr, ' ', 990)).' ...';
				}
				$str .= '   </p>';
			}

			$str .= '       <div class="html5fb-links">';
			$str .= $data->viewPublicationLink . (isset($item->page) && $item->page != 0 ? JText::_('COM_HTML5FLIPPINGBOOK_FE_CONT_READ') : htmlspecialchars((empty($publbuttontext) ? JText::_('COM_HTML5FLIPPINGBOOK_FE_VIEW_PUBLICATION') : $publbuttontext))) . '</a>';

			if ($item->c_enable_pdf)
			{
				$pdfLink = JURI::root().'index.php?option=com_html5flippingbook'
						.'&task=getpdf'
						.'&id='.$item->c_id
						.'&filename='.preg_replace('/[<>:"\/\\\|\?\*]/is', '', $item->c_background_pdf);

				$str .= '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;';
				$str .= '<a href="' . $pdfLink . '" class="icon_pdf">'.JText::_('COM_HTML5FLIPPINGBOOK_BE_DOWNLOAD_PDF').'</a>';
			}

			$str .= '       </div>';

			if ($item->c_show_cdate)
			{
				$date = new JDate($item->c_created_time);
				$date = $date->toUnix();
				$dateString = gmdate("Y-m-d", $date);

				$str .= '   <div class="html5fb-date">' . $dateString . '</div>';
			}

			$str .= '   </div>';
			$str .= '</li>';
		}

		return array($k, $str);
	}

	/**
	 * Method to convert seconds to human readable view
	 *
	 * @param $secs
	 *
	 * @return string
	 */
	public static function secsToString($secs)
	{
		$units = array(
			"week"   => 7*24*3600,
			"day"    =>   24*3600,
			"hour"   =>      3600,
			"minute" =>        60,
			"second" =>         1,
		);

		// specifically handle zero
		if ( $secs == 0 ) return "0 seconds";

		$s = "";

		foreach ( $units as $name => $divisor ) {
			if ( $quot = intval($secs / $divisor) ) {
				$s .= "$quot $name";
				$s .= (abs($quot) > 1 ? "s" : "") . ", ";
				$secs -= $quot * $divisor;
			}
		}

		return substr($s, 0, -2);
	}

    public static function createBook($list = 'reading', $item, $isMobile = FALSE, $shelfN = 1, $config)
    {
        $str = '';

            $data = self::htmlPublHelper($isMobile, $item);

                    $tooltip = '<strong>' . $item->c_title . '</strong><br/>' ;

                $str .= '<div class="row-' . $shelfN . '">';

            $str .= '<div class="' . $list . '-pub-' . $item->c_id .' '. '" ' . '>';
            $str .= self::publActionButtonBlock($item, $list, '', $config);
            $str .= '	<div class="hasTooltip" title="' . $tooltip . '">';
           // $str .= str_replace("thumbnail", "", $data->viewPublicationLinkWithTitle);
            $str .= '		    <img class="pub-' . $item->c_id .'" src="' . $data->thumbnailUrl . '" alt="' . htmlspecialchars($item->c_title) . '" />';
            //$str .= '       </a>';
            $str .= '   </div>';
            $str.='<div>'.$item->c_pub_descr.'</div>';
            $str .= '</div>';

        return $str;
    }

    public static function readBookShelf($list = 'reading', $item, $isMobile = FALSE, $shelfN = 1, $config, $get=false)
    {
        $str = '';
            $data = self::htmlPublHelper($isMobile, $item);
            $tooltip = '';
            if (strlen($item->c_pub_descr) != "")
            {
                if (strlen($item->c_pub_descr) <= 990)
                {
                    $tooltip = '<strong>' . $item->c_title . '</strong><br/>';
                }
                else
                {
                    $tooltip = '<strong>' . $item->c_title . '</strong><br/>';
                }
            }
                $str .= '<div class="row-' . $shelfN . '">';
                $str .= '   <div class="loc">';
            $str .= '<div class="' . $list . '-pub-' . $item->c_id .' '. '" ' . '>';
            $str .= self::publActionButtonBlock($item, $list, '', $config);

            $str .= '	<div class="book hasTooltip" title="' . $tooltip . '">';
            $str .= str_replace("thumbnail", "", $data->viewPublicationLinkWithTitle);
            $str .= '		    <img class="pub-' . $item->c_id .'" src="' . $data->thumbnailUrl . '" alt="' . htmlspecialchars($item->c_title) . '" />';
           $str .= '       </a>';
            $str .= '   </div>';
            $str .= '</div>';

        return $str;
    }
//all books on BookShelf
    public static function allBookShelf($list = 'reading', $shelf = array(), $isMobile = FALSE, $shelfN = 1, $config, $buy=false)
    {
        $str = '';
        $rowCount = count($shelf);
        foreach ($shelf as $i => $item)
        {
            $data = self::htmlPublHelper($isMobile, $item);
            $tooltip = '';
            if (strlen($item->c_pub_descr) != "")
            {
                if (strlen($item->c_pub_descr) <= 990)
                {
                    $tooltip = '<strong>' . $item->c_title . '</strong><br/>';
                }
                else
                {
                    $tooltip = '<strong>' . $item->c_title . '</strong><br/>';
                }
            }

            if ($i == 0)
            {
                $str .= '<div class="row-' . $shelfN . '">';
            }

            if (($i + 4) % 4 == 0)
            {
                $str .= '   <div class="loc">';
            }


            $str .= '<div class="' . $list . '-pub-' . $item->c_id .' '. '" ' . '>';
            $str .= self::publActionButtonBlock($item, $list, '', $config);
            $str .= '	<div class="book hasTooltip" title="' . $tooltip . '">';
            $id    = JRequest::getInt( 'id' , 0 );
            if( $id == 0 )
            {
                $user= Foundry::user();
            }else{
                $user=Foundry::user($id);
            }

            if( $user->id==Foundry::user()->id){
                $str.='<a href="'.JRoute::_("index.php?option=com_plot&view=publication&bookId=".$item->c_id).'">';
            }else{
                $str.='<a href="'.JRoute::_("index.php?option=com_plot&view=publication&bookId=".$item->c_id."&id=".(int)$user->id).'">';
            }

            //$str .= str_replace("thumbnail", "", $data->viewPublicationLinkWithTitle);
            $str .= '		    <img class="pub-' . $item->c_id .'" src="' . $data->thumbnailUrl . '" alt="' . htmlspecialchars($item->c_title) . '" />';
            $str .= '       </a>';
            $str .= '   </div>';
            $str .= '</div>';

            if (($i + 1) % 4 == 0 || (($i + 1) == $rowCount))
            {
                $str .= '   </div>';
            }

            if ($i + 1 == $rowCount)
            {
                $str .= '</div>';
            }


        }

        return $str;
    }

    public static function checkIsBoughtBook($book_id){
        $db = Foundry::db();

            $user = Foundry::user();

        $query = $db->getQuery(true)
            ->select('id')
            ->from('`#__plot_books`')
            ->where('parent_id = ' . (int)$user->id)
            ->where('book_id = '.(int)$book_id);
        $db->setQuery($query);
    return (int)$db->loadResult();
    }

    public static function startReadBookShelf($list = 'reading', $item, $isMobile = FALSE, $shelfN = 1, $config, $get=false)
    {
        $str = '';
        $data = self::htmlPublHelper($isMobile, $item);
        $tooltip = '';
        if (strlen($item->c_pub_descr) != "")
        {
            if (strlen($item->c_pub_descr) <= 990)
            {
                $tooltip = '<strong>' . $item->c_title . '</strong><br/>';
            }
            else
            {
                $tooltip = '<strong>' . $item->c_title . '</strong><br/>';
            }
        }
        $str .= '<div class="row-' . $shelfN . '">';
        $str .= '   <div class="loc">';
        $str .= '<div class="' . $list . '-pub-' . $item->c_id .' '. '" ' . '>';
        $str .= self::publActionButtonBlock($item, $list, '', $config);
        $str .= '	<div class="book hasTooltip" title="' . $tooltip . '">';
        $str .= str_replace("thumbnail", "", $data->viewPublicationLinkWithTitle);
        $str .= '		    <img  class="pub-' . $item->c_id .'" src="' . $data->thumbnailUrl . '" alt="' . htmlspecialchars($item->c_title) . '" />';
        $str .= '       </a>';
        $str .= '   </div>';
        $str .= '</div>';


        return $str;
    }

    public static function bookShelfWithPercent($list = 'reading', $shelf = array(), $isMobile = FALSE, $shelfN = 1, $config)
    {

        $str = '';
        $rowCount = count($shelf);
        foreach ($shelf as $i => $item)
        {
            $data = self::htmlPublHelper($isMobile, $item);
            $tooltip = '';
            if (strlen($item->c_pub_descr) != "")
            {
                if (strlen($item->c_pub_descr) <= 990)
                {
                    $tooltip = '<strong>' . $item->c_title . '</strong><br/>';
                }
                else
                {
                    $tooltip = '<strong>' . $item->c_title . '</strong><br/>';
                }
            }

            if ($i == 0)
            {
                $str .= '<div class="row-' . $shelfN . '">';
            }

            if (($i + 4) % 4 == 0)
            {
                $str .= '   <div class="loc">';
            }


            $str .= '<div class="' . $list . '-pub-' . $item->c_id .' '. '" ' . '>';
            $str .= self::publActionButtonBlock($item, $list, '', $config);
            $str .= '	<div class="book hasTooltip" title="' . $tooltip . '">';
            $str .= '		    <img class="pub-' . $item->c_id .'" src="' . $data->thumbnailUrl . '" alt="' . htmlspecialchars($item->c_title) . '" />';
            $str .= '   </div>';
            $str .= '</div>';
           if($item->read==1){
                $percent=100;
            }
            if($item->read==0 || $item->read==-1){
                if($item->params){
                    $params=json_decode($item->params,true);
                    $count_pages=self::getCountPages($item->c_id);
                    $percent=self::getPersent($count_pages,count($params));
                }else{
                    $percent=0;
                }
            }



            if((int)Foundry::user()->profile_id == (int)plotGlobalConfig::getVar('parentProfileId')){
                $id    = JRequest::getInt( 'id' , 0 );
                if( $id == 0 )
                {
                    $user= Foundry::user();
                }else{
                    $user=Foundry::user($id);
                }

                if( $user->id==Foundry::user()->id )
                {
                    $str.='<span>'. round($percent).'%</span>';
                    $str.='<a href="'.JRoute::_("index.php?option=com_plot&view=readbook&bookId=".$item->c_id).'">'.JText::_("COM_PLOT_READ_BOOK").'</a>';
                }elseif(self::checkIsBoughtBook($item->c_id)){
                    $str.='<a href="'.JRoute::_("index.php?option=com_plot&view=readbook&bookId=".$item->c_id.'&id='.(int)$user->id).'">'.JText::_("COM_PLOT_READ_BOOK").'</a>';
                }else{
                    $str.='<a href="'.JRoute::_("index.php?option=com_plot&view=book&bookId=".$item->c_id.'&id='.(int)$user->id).'">'.JText::_("COM_PLOT_BUY_BOOK").'</a>';
                }

            }else{
                $id    = JRequest::getInt( 'id' , 0 );
                if( $id == 0 )
                {
                    $user= Foundry::user();
                }else{
                    $user=Foundry::user($id);
                }
                if( $user->id == Foundry::user()->id )
                {
                    $id 	= Foundry::user()->id;
                }
                $user 	= Foundry::user( $id );
                if($user->id==Foundry::user()->id){

                    $str.='<span>'. round($percent).'%</span>';
                    $str.='<a href="'.JRoute::_("index.php?option=com_plot&view=readbook&bookId=".$item->c_id).'">'.JText::_("COM_PLOT_READ_BOOK").'</a>';
                }

            }
            //$str.='<div class="book-descr">'.  $item->c_pub_descr.'</div>';
            if (($i + 1) % 4 == 0 || (($i + 1) == $rowCount))
            {
                $str .= '   </div>';
            }

            if ($i + 1 == $rowCount)
            {
                $str .= '</div>';
            }


        }

        return $str;
    }

    public static function getCountPages($id){
        $db = JFactory::$database;
        $query = $db->getQuery(true)
            ->select('COUNT(*) AS count')
            ->from('`#__html5fb_pages`')
            ->where('`publication_id` = ' . $id);
        $db->setQuery($query);
        $count=$db->loadResult();
        return $count;
    }

    public static function getPersent($all=0, $pages=0){
        $max_percent=plotGlobalConfig::getVar('maxPersentForUnreadBook');
        if($all && $pages && $all==$pages){
            $percent =$max_percent;
        }else{
            if($all){
                $percent= ($max_percent*$pages)/$all;
            }else{
                $percent=0;
            }

        }
       return round($percent);
    }

}