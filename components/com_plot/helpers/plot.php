<?php
defined('_JEXEC') or die('Restricted access');

abstract class PlotHelper
{

    public static function isFriend()
    {
        $id = JRequest::getInt('id');
        $user = Foundry::user($id);
        $my = Foundry::user();
        $model = Foundry::model('Friends');
        if ($model->isFriends($my->id, $user->id)) {
            $status = 'friend';
        } else {
            if ($model->isFriends($my->id, $user->id, SOCIAL_FRIENDS_STATE_PENDING)) {
                $status = 'pending';
            } else {
                $status = 'unfriend';
            }
        }
        return $status;
    }

    public static function getBirthday()
    {
        $db = Foundry::db();
        $id = JRequest::getInt('id');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }

        $query = $db->getQuery(true)
            ->select('`fd`.data')
            ->from('`#__social_fields_data` AS `fd`')
            ->innerJoin('`#__social_fields` AS `f` ON `f`.`id` = `fd`.`field_id`')
            ->where('`fd`.uid = ' . (int)$user->id)
            ->where('`f`.unique_key = "BIRTHDAY"');

        $db->setQuery($query);
        $birth = $db->loadResult();
        if ($birth) {
            $birthday = json_decode($birth);
        } else {
            $birthday = '';
        }

        return $birthday;
    }

    public static function getRegistredDate()
    {
        $db = Foundry::db();
        $id = JRequest::getInt('id');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }

        $query = $db->getQuery(true)
            ->select('`a`.registerDate')
            ->from('`#__users` AS `a`')
            ->where('`a`.id = ' . (int)$user->id);

        $db->setQuery($query);
        $regDate = $db->loadResult();
        return $regDate;
    }

    public static function countYears($birth)
    {

        if ($birth) {
            $bithdayDate = (int)$birth->year . '-' . $birth->month . '-' . $birth->day;
            $date = new DateTime($bithdayDate);
            $now = new DateTime();
            $interval = $now->diff($date);
            $bithday = $interval->y;
        } else {
            $bithday = 0;
        }
        return $bithday;
    }

    public static function getPhotos($bithday)
    {

        $templateData = array();
        $db = Foundry::db();
        $id = JRequest::getInt('id');
        if ($id) {
            $user = plotUser::factory($id);
        } else {
            $user = plotUser::factory();
        }
        if (!$bithday) {
            $bithday = new stdClass();
            $bithday->year = '1900';
            $bithday->month = '01';
            $bithday->day = '01';
        }
        if ($user->profile_id == (int)plotGlobalConfig::getVar('parentProfileId')) {
            $bithday->month = '01';
            $bithday->day = '01';
        }
        $bithday_str = $bithday->year . '-' . $bithday->month . '-' . $bithday->day;
        $allphotos = array();
        //$countbith = self::countYears($bithday);
        $now = date('Y-m-d');

        // for ($i = $countbith; $i > 0; $i--) {
        // $date1 = strtotime($bithday->year . '-' . $bithday->month . '-' . $bithday->day . '+' . $i . ' year');
        //$date2 = strtotime($bithday->year . '-' . $bithday->month . '-' . $bithday->day . '+' . ($i - 1) . ' year');
        $date1 = strtotime($bithday->year . '-' . $bithday->month . '-' . $bithday->day);
        $date2 = strtotime($now);
        if (!$date1) {
            $date1 = '1356987600';
        }
        if (!$date2) {
            $date2 = '1356987600';
        }
        $query = $db->getQuery(true)
            ->select('`p`.* ')
            ->select('`p`.created AS year ')
            //->select($date1 . ' AS date1 ')
            //->select($date2 . ' AS date2 ')
            ->select('`m`.`value`')
            ->from('`#__users` AS `u`')
            ->innerJoin('`#__social_photos` AS `p` ON `u`.`id` =`p`.`uid`')
            ->leftJoin('`#__social_albums` AS `a` ON `a`.`id` = `p`.`album_id`')
            ->innerJoin('`#__social_photos_meta` AS `m` ON `m`.`photo_id` = `p`.`id`')
            ->where('`p`.`uid` = ' . (int)$user->id)
            ->where('`a`.`type` = "plot-certificate"')
            ->where('`m`.`property` = "thumbnail" AND `m`.`group`="path"')
            // ->where('`p`.created between "' . date('Y-m-d', $date2) . '" AND "' . date('Y-m-d', $date1) . '"')
            ->where('`p`.created between "' . date('Y-m-d', $date1) . '" AND "' . date('Y-m-d', $date2) . '"')
            ->order('`p`.created DESC');
        $db->setQuery($query);

        $photos = $db->loadObjectList();
        if ($photos) {
            $path = array();
            foreach ($photos AS $photo) {
                $path = explode("/", $photo->value);
                $photo->value = end($path);
            }

            array_push($allphotos, $photos);
        }

        //  }

        foreach ($allphotos as $rows) {
            foreach ($rows AS $row) {
                $birth = self::calcutateAge($bithday_str);
                $year = self::calcutateAge($row->year);

                $row->year = $birth - $year;
            }
        }

        $temp_years = array();

        foreach ($allphotos as $rows) {

            foreach ($rows as $row) {
                $temp_years[] = $row->year;
            }
        }
        $temp_years = array_unique($temp_years);
        foreach ($temp_years AS $y) {

            $templateData[$y] = array();
        }

        foreach ($allphotos as $row) {
            foreach ($row as $year) {
                array_push($templateData[$year->year], $year);
            }
        }
        $birth = self::calcutateAge($bithday_str);
        foreach ($templateData as $key => $value) {
            if ($key == $birth) {
                unset($templateData[$key]);
            }
        }

        return $templateData;
    }

    public static function getBooks($bithday)
    {

        $db = Foundry::db();
        $id = JRequest::getInt('id');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }
        $templateData = array();
        if (!$bithday) {
            $bithday = new stdClass();
            $bithday->year = 1900;
            $bithday->month = 01;
            $bithday->day = 01;
        }
        if ($user->profile_id == (int)plotGlobalConfig::getVar('parentProfileId')) {
            $bithday->month = 01;
            $bithday->day = 01;
        }
        $bithday_str = $bithday->year . '-' . $bithday->month . '-' . $bithday->day;
        $allbooks = array();

        // $countbith = self::countYears($bithday);
        $now = date('Y-m-d');
        // for ($i = $countbith; $i > 0; $i--) {
        //$date1 = strtotime($bithday->year . '-' . $bithday->month . '-' . $bithday->day . '+' . $i . ' year');
        // $date2 = strtotime($bithday->year . '-' . $bithday->month . '-' . $bithday->day . '+' . ($i - 1) . ' year');
        $date1 = strtotime($bithday->year . '-' . $bithday->month . '-' . $bithday->day);
        $date2 = strtotime($now);
        if (!$date1) {
            $date1 = '1356987600';
        }
        if (!$date2) {
            $date2 = '1356987600';
        }
        $query = $db->getQuery(true)
            ->select('`pub`.*, `res`.height, `res`.width')
            ->select('`b`.`date` AS `year` ')
            ->select($date1 . ' AS date1 ')
            ->select($date2 . ' AS date2 ')
            ->select((int)$user->id . '  AS uid ')
            ->from('`#__html5fb_publication` AS `pub`')
            ->leftJoin('`#__html5fb_resolutions` AS `res` ON `pub`.`c_resolution_id` = `res`.`id`')
            ->innerJoin('`#__plot_books` AS `b` ON `pub`.`c_id` = `b`.`book_id`');
        $query->where('`b`.`child_id` = ' . (int)$user->id);
        $query->where('`b`.`read` = 1')
            //->where('`b`.date between "' . date('Y-m-d', $date2) . '" AND "' . date('Y-m-d', $date1) . '"')
            ->where('`b`.date between "' . date('Y-m-d', $date1) . '" AND "' . date('Y-m-d', $date2) . '"')
            ->order('`b`.date DESC');
        $db->setQuery($query);

        $books = $db->loadObjectList();

        array_push($allbooks, $books);


        //  }

        foreach ($allbooks as $rows) {
            foreach ($rows AS $row) {

                $birth = self::calcutateAge($bithday_str);
                $year = self::calcutateAge($row->year);

                $row->year = $birth - $year;
                $thumbnailPath = JPATH_SITE . '/media/com_html5flippingbook' . '/thumbs/' . $row->c_thumb;
                if ($row->c_thumb == "" || !is_file($thumbnailPath)) {
                    $row->thumbnailUrl = JURI::root() . "components/com_html5flippingbook/assets/images/no_image.png";
                } else {
                    $row->thumbnailUrl = JURI::root() . "media/com_html5flippingbook/thumbs/" . $row->c_thumb;
                }
            }
        }

        $temp_years = array();

        foreach ($allbooks as $rows) {

            foreach ($rows as $row) {
                $temp_years[] = $row->year;
            }
        }
        $temp_years = array_unique($temp_years);
        foreach ($temp_years AS $y) {

            $templateData[$y] = array();
        }

        foreach ($allbooks as $row) {
            foreach ($row as $year) {
                array_push($templateData[$year->year], $year);
            }
        }
        $birth = self::calcutateAge($bithday_str);
        foreach ($templateData as $key => $value) {
            if ($key == $birth) {
                unset($templateData[$key]);
            }
        }

        return $templateData;
    }

    public static function calcutateAge($dob)
    {

        $dob = date("Y-m-d", strtotime($dob));

        $dobObject = new DateTime($dob);
        $nowObject = new DateTime();

        $diff = $dobObject->diff($nowObject);

        return $diff->y;
    }

    public static function getPoints($bithday)
    {
        $templateData = array();
        $db = Foundry::db();
        $id = JRequest::getInt('id');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }
        if (!$bithday) {
            $bithday = new stdClass();
            $bithday->year = 1900;
            $bithday->month = 01;
            $bithday->day = 01;
        }
        if ($user->profile_id == (int)plotGlobalConfig::getVar('parentProfileId')) {
            $bithday->month = 01;
            $bithday->day = 01;
        }
        $bithday_str = $bithday->year . '-' . $bithday->month . '-' . $bithday->day;
        $allpoints = array();
        $countbith = self::countYears($bithday);
        for ($i = $countbith; $i > 0; $i--) {
            $date1 = strtotime($bithday->year . '-' . $bithday->month . '-' . $bithday->day . '+' . $i . ' year');
            $date2 = strtotime($bithday->year . '-' . $bithday->month . '-' . $bithday->day . '+' . ($i - 1) . ' year');
            $query = $db->getQuery(true)
                ->select('SUM(points) AS sum')
                ->select('created AS year ')
                ->select($date1 . ' AS date1 ')
                ->select($date2 . ' AS date2 ')
                ->from('`#__social_points_history`')
                ->where('user_id = ' . (int)$user->id)
                ->where('created between "' . date('Y-m-d', $date2) . '" AND "' . date('Y-m-d', $date1) . '"')
                ->order('created DESC');
            $db->setQuery($query);
            $points = $db->loadObjectList();

            if ($points) {
                array_push($allpoints, $points);
            }
        }

        $count_allpoints = count($allpoints);
        for ($i = 0; $i < $count_allpoints; $i++) {
            foreach ($allpoints[$i] AS $row) {
                if (!$row->year) {
                    unset($allpoints[$i]);
                }
            }
        }

        foreach ($allpoints as $rows) {
            foreach ($rows AS $row) {
                $birth = self::calcutateAge($bithday_str);
                $year = self::calcutateAge($row->year);

                $row->year = $birth - $year;
            }
        }

        $temp_years = array();

        foreach ($allpoints as $rows) {

            foreach ($rows as $row) {
                $temp_years[] = $row->year;
            }
        }
        $temp_years = array_unique($temp_years);
        foreach ($temp_years AS $y) {

            $templateData[$y] = array();
        }

        foreach ($allpoints as $row) {
            foreach ($row as $year) {
                array_push($templateData[$year->year], $year);
            }
        }

        $birth = self::calcutateAge($bithday_str);
        foreach ($templateData as $key => $value) {
            if ($key == $birth) {
                unset($templateData[$key]);
            }
        }

        return $templateData;
    }

    public static function booksObject()
    {
        $birthday = PlotHelper::getBirthday();
        if (!$birthday) {
            $birthday = new stdClass();
            $birthday->day = '1';
            $birthday->month = '1';
            $birthday->year = '1900';
        }
        $books = self::getBooks($birthday);
        return $books;
    }

    public static function photosObject()
    {
        $birthday = PlotHelper::getBirthday();
        if (!$birthday) {
            $birthday = new stdClass();
            $birthday->day = '1';
            $birthday->month = '1';
            $birthday->year = '1900';
        }
        $photos = self::getPhotos($birthday);
        return $photos;
    }

    public static function pointsObject()
    {
        $bithday = PlotHelper::getBirthday();
        if (!$birthday) {
            $birthday = new stdClass();
            $birthday->day = '1';
            $birthday->month = '1';
            $birthday->year = '1900';
        }
        $points = self::getPoints($bithday);
        return $points;
    }

    public static function htmlPublHelper($mobile, $item, $link = FALSE)
    {
        $user = JFactory::getUser();
        $data = new stdClass();

        if ($link) {
            $data->rawPublicationLink = 'index.php?option=com_html5flippingbook&view=publication&id=' . $item['c_id'];
            $data->publicationLink = JRoute::_($data->rawPublicationLink . '&tmpl=component', false, -1);
            return $data;
        }

        $linkTitle = JText::_('COM_HTML5FLIPPINGBOOK_FE_VIEW_PUBLICATION');
        $popupWidth = $item['width'] * 2 + 66;
        $popupHeight = $item['height'] + 100;

        $data->rawPublicationLink = 'index.php?option=com_html5flippingbook&view=publication&id=' . $item['c_id'];


        $data->publicationLink = JRoute::_($data->rawPublicationLink . '&tmpl=component' . ($item['uid'] == $user->get('id') && isset($item->page) ? '#page/' . $item->page : ''), false, -1);
        $data->viewPublicationLink = '<a href="' . $data->publicationLink . '" target="_blank">';
        $data->viewPublicationLinkWithTitle = '<a class="thumbnail" href="' . $data->publicationLink . '" target="_blank" title="' . $linkTitle . '">';

        // Preparing Publication's thumbnail.

        $thumbnailPath = JPATH_SITE . '/media/com_html5flippingbook' . '/thumbs/' . $item['c_thumb'];

        if ($item['c_thumb'] == "" || !is_file($thumbnailPath)) {
            $data->thumbnailUrl = JURI::root() . "components/com_html5flippingbook/assets/images/no_image.png";
        } else {
            $data->thumbnailUrl = JURI::root() . "media/com_html5flippingbook/thumbs/" . $item['c_thumb'];
        }

        return $data;
    }

    public static function yearsBookShelf($list = 'reading', $item, $isMobile = FALSE, $shelfN = 1, $config, $buy = false)
    {
        $str = '';
        $rowCount = 1;
        $data = self::htmlPublHelper($isMobile, $item);
        $tooltip = '';
        $tooltip = '<strong>' . $item['c_title'] . '</strong><br/>';
        $str .= '<div class="row-' . $shelfN . '">';
        $str .= '<div class="' . $list . '-pub-' . $item['c_id'] . ' ' . '" ' . '>';
        $str .= '	<div class="book hasTooltip" title="' . $tooltip . '">';
        $str .= '		    <img class="pub-' . $item['c_id'] . '" src="' . $data->thumbnailUrl . '" alt="' . htmlspecialchars($item['c_title']) . '" />';
        $str .= '   </div>';
        $str .= '</div>';
        $str .= '</div>';
        return $str;
    }

    public static function Age($bithday, $year)
    {
        $bithdayDate = $bithday->year . '-' . $bithday->month . '-' . $bithday->day;
        $date = new DateTime($bithdayDate);
        $now = new DateTime($year . '-' . $bithday->month . '-' . $bithday->day);
        $interval = $now->diff($date);
        $bithday = $interval->y;
        return $bithday;
    }

    public static function AgeToStr($Age)
    {
        $db = Foundry::db();
        $id = JRequest::getInt('id');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }
        $str = '';
        $num = $Age > 100 ? substr($Age, -2) : $Age;
        if ($num >= 5 && $num <= 14)
            $str = JText::_('COM_PLOT_YEARS');
        else {
            $num = substr($Age, -1);
            if ($num == 0 || ($num >= 5 && $num <= 9))
                $str = JText::_('COM_PLOT_YEARS');
            if ($num == 1)
                $str = JText::_('COM_PLOT_YEAR');
            if ($num >= 2 && $num <= 4)
                $str = JText::_('COM_PLOT_YEAR2');
        }
        if ($user->profile_id == (int)plotGlobalConfig::getVar('parentProfileId')) {
            return $Age;
        } else {
            return $Age . ' ' . $str;
        }
    }

    public static function getConfigForBooks()
    {
        $db = JFactory::getDBO();

        $query = "SELECT * FROM `#__html5fb_config`" .
            " ORDER BY `setting_name`";
        $db->setQuery($query);
        $rows = $db->loadObjectList();

        $config = (object)array();

        if (isset($rows)) {
            foreach ($rows as $row) {
                $config->{$row->setting_name} = $row->setting_value;
            }
        }

        return $config;
    }

    public static function getUserLevel()
    {
        $db = Foundry::db();
        $id = JRequest::getInt('id');
        if ($id) {
            $user = plotUser::factory($id);
        } else {
            $user = plotUser::factory();
        }
        $points = (int)$user->getPoints();

        $query = $db->getQuery(true)
            ->select('`l`.id,`l`.title ')
            ->from('`#__plot_levels` AS `l`')
            ->where('`l`.`points` <= ' . (int)$points)
            ->order('`l`.`points` DESC');
        $db->setQuery($query);

        $level = $db->loadObject();

        $query = $db->getQuery(true)
            ->select('`l`.points ')
            ->from('`#__plot_levels` AS `l`')
            ->where('`l`.`id` > ' . (int)$level->id);
        $db->setQuery($query);

        $next_level = (int)$db->loadResult();
        $data = array();
        $data['level'] = $level->title;
        if ($points >= $next_level) {
            $data['next_level'] = 0;
        } else {
            $data['next_level'] = $next_level - $points;
        }

        $query = $db->getQuery(true)
            ->select('beaver,boat,house ')
            ->from('`#__plot_levels` ')
            ->where('points<= ' . (int)$points)
            ->order('id DESC');
        $db->setQuery($query, 0, 1);

        $level_imgs = $db->loadObjectList();
        $data['level_img'] = $level_imgs;
        return $data;
    }

    public static function getImageById($id)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->select('p.*, `m`.`value`')
            ->from('`#__social_photos` AS p')
            ->innerJoin('`#__social_photos_meta` AS `m` ON `m`.`photo_id` = `p`.`id` AND `m`.`property`="square"')
            ->where('p.id = ' . (int)$id);
        $db->setQuery($query);

        $photo = $db->loadObject();

        $path = array();

        if ($photo->value) {
            $path = explode("/", $photo->value);
            $photo->value = end($path);
        }

        return $photo;
    }

    public static function getImageThumbById($id)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->select('p.*, `m`.`value`')
            ->from('`#__social_photos` AS p')
            ->innerJoin('`#__social_photos_meta` AS `m` ON `m`.`photo_id` = `p`.`id` AND `m`.`property`="thumbnail"')
            ->where('p.id = ' . (int)$id);
        $db->setQuery($query);

        $photo = $db->loadObject();

        $path = array();

        if ($photo->value) {
            $path = explode("/", $photo->value);
            $photo->value = end($path);
        }

        return $photo;
    }

    public static function getImageOriginalById($id)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->select('p.*, `m`.`value`')
            ->from('`#__social_photos` AS p')
            ->innerJoin('`#__social_photos_meta` AS `m` ON `m`.`photo_id` = `p`.`id` AND `m`.`property`="original"')
            ->where('p.id = ' . (int)$id);
        $db->setQuery($query);

        $photo = $db->loadObject();

        $path = array();

        if ($photo && $photo->value) {
            $path = explode("/", $photo->value);
            $photo->value = end($path);
        }

        return $photo;
    }


    public static function getEssayByImg($img){
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->select('p.*')
            ->from('`#__plot_essay` AS p')
            ->where('p.img = "' . $db->escape($img).'"');
        $db->setQuery($query);
        $essay = $db->loadObject();


        return $essay;
    }

    public static function getVideoById($id)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->select('p.*')
            ->from('`#__plot_video` AS p')
            ->where('p.id = ' . (int)$id);
        $db->setQuery($query);
        return $db->loadObject();
    }




    public static function getTagById($id)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->select('*')
            ->from('`#__k2_items`')
            ->where('id = ' . (int)$id);
        $db->setQuery($query);
        return $db->loadObject();
    }

    public static function getBookById($id)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->select('p.c_title, `p`.`c_thumb`')
            ->from('`#__html5fb_publication` AS p')
            ->where('p.c_id = ' . (int)$id);
        $db->setQuery($query);
        return $db->loadObject();
    }

    public static function getEssayById($id){
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->select('e.*')
            ->from('`#__plot_essay` AS e')
            ->where('e.id = ' . (int)$id);
        $db->setQuery($query);
        return $db->loadObject();
    }


    public static function getCourseVideoById($id){
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->select('e.*')
            ->from('`#__plot_course_video` AS e')
            ->where('e.id = ' . (int)$id);
        $db->setQuery($query);
        return $db->loadObject();
    }

    //return joint tags current user and his future friend
    public static function getMyTags($target_id)
    {

        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->clear()
            ->select('`i`.id, `i`.title')
            ->from('`#__k2_items` AS `i`')
            ->innerJoin('`#__plot_tags` AS `t` ON `i`.id=`t`.tagId')
            ->where('`t`.entity="user"')
            ->where('`t`.entityId=' . (int)Foundry::user()->id)
            ->group('`i`.id')
            ->order('`i`.title');

        $db->setQuery($query);
        $my_tags = $db->loadObjectList();

        $query = $db->getQuery(true)
            ->clear()
            ->select('`i`.id, `i`.title')
            ->from('`#__k2_items` AS `i`')
            ->innerJoin('`#__plot_tags` AS `t` ON `i`.id=`t`.tagId')
            ->where('`t`.entity="user"')
            ->where('`t`.entityId=' . (int)$target_id)
            ->group('`i`.id')
            ->order('`i`.title');

        $db->setQuery($query);
        $user_tags = $db->loadObjectList();

        $tags = array();
        foreach ($my_tags AS $mtag) {
            foreach ($user_tags AS $utag) {
                if ($mtag->id == $utag->id) {
                    array_push($tags, $utag);
                }
            }
        }

        if (!empty($tags)) {
            $count_tags = count($tags);
            for ($i = 0; $i < $count_tags; $i++) {
                $query = $db->getQuery(true)
                    ->clear()
                    ->select('COUNT(pf.social_id)')
                    ->from('`#__plot_friends` AS pf')
                    ->innerJoin('`#__social_friends` AS sf ON pf.social_id=sf.id')
                    ->where('pf.tag_id=' . (int)$tags[$i]->id);
                $db->setQuery($query);
                $res = (int)$db->loadResult();

                if ($res && $res >= plotGlobalConfig::getVar('maxNumberOfFriends')) {
                    unset($tags[$i]);
                }
            }
        }
        return $tags;
    }

    //return current user's tags 
    public static function myTags($id=0)
    {
        if(!$id){
            $id = JRequest::getInt('userId', 0);
        }

        $user = plotUser::factory();
        if ($id) {
            $user = plotUser::factory($id);
        }

        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->clear()
            ->select('`i`.id, `i`.title, `t`.title AS reason, t.smiley')
            ->from('`#__k2_items` AS `i`')
            ->innerJoin('`#__plot_tags` AS `t` ON `i`.id=`t`.tagId')
            ->where('`t`.entity="user"')
            ->where('`t`.entityId=' . (int)$user->id)
           ->where('`i`.`published`=1')
            ->order('`i`.title ASC')
            ->group('`i`.id');

        $db->setQuery($query);
        $myTags = $db->loadObjectList();

        foreach ($myTags AS $myTag) {
            switch ($myTag->smiley) {
                case 1:
                    $myTag->smileyInputClass = 'dont-like';
                    break;
                case 2:
                    $myTag->smileyInputClass = 'so-so';
                    break;
                case 3:
                    $myTag->smileyInputClass = 'like';
                    break;
                default:
                    $myTag->smileyInputClass = '';
                    break;
            }
        }

        return $myTags;
    }




    public static function declension($int, $expressions, $showint = true)
    {
        settype($int, "integer");
        $count = $int % 100;
        if ($count >= 5 && $count <= 20) {
            $result = ($showint ? $int . " " : "") . $expressions['2'];
        } else {
            $count = $count % 10;
            if ($count == 1) {
                $result = ($showint ? $int . " " : "") . $expressions['0'];
            } elseif ($count >= 2 && $count <= 4) {
                $result = ($showint ? $int . " " : "") . $expressions['1'];
            } else {
                $result = ($showint ? $int . " " : "") . $expressions['2'];
            }
        }
        return $result;
    }

    public static function countEventSubscription($event_id)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->clear()
            ->select('COUNT(uid) AS total')
            ->from('`#__plot_user_event_map` ')
            ->where('event_id=' . (int)$event_id);

        $db->setQuery($query);
        return $db->loadResult();
    }

    public static function chechEventSubscription($event_id)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->clear()
            ->select('uid')
            ->from('`#__plot_user_event_map` ')
            ->where('event_id=' . (int)$event_id)
            ->where('uid=' . (int)Foundry::user()->id);
        $db->setQuery($query);
        return $db->loadResult();
    }

    public static function isOwnerEvent($event_id)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->clear()
            ->select('user_id')
            ->from('`#__plot_events` ')
            ->where('id=' . (int)$event_id)
            ->where('user_id=' . (int)Foundry::user()->id);
        $db->setQuery($query);

        return $db->loadResult();
    }

    public static function compareDateTime($date1, $date2)
    {
        $d1 = new DateTime($date1);
        $d2 = new DateTime($date2);
        if ($d1 > $d2) {
            return false;
        }
        if ($d1 == $d2) {
            return false;
        }
        if ($d1 < $d2) {
            return true;
        }
    }

    public static function getEventById($event_id)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->clear()
            ->select('*')
            ->from('`#__plot_events` ')
            ->where('id=' . (int)$event_id);
        $db->setQuery($query);

        return $db->loadObject();
    }

    public static function getPortfolioById($id)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->clear()
            ->select('*')
            ->from('`#__plot_portfolio` ')
            ->where('id=' . (int)$id);
        $db->setQuery($query);

        return $db->loadObject();
    }

    public static function renderUserLink($user_id)
    {
        $str = '<a href="' . JRoute::_("index.php?option=com_plot&view=profile&id=" . plotUser::factory($user_id)->id) . '">' . plotUser::factory($user_id)->name . '</a>';
        return $str;
    }

    public static function getWordEnding($value, $word0, $word1, $word2)
    {

        if (preg_match('/1\d$/', $value)) {
            return $value . ' ' . $word2;
        } elseif (preg_match('/1$/', $value)) {
            return $value . ' ' . $word0;
        } elseif (preg_match('/(2|3|4)$/', $value)) {
            return $value . ' ' . $word1;
        } else {
            return $value . ' ' . $word2;
        }
    }

    public static function getWordEndingWithoutValue($value, $word0, $word1, $word2)
    {

        if (preg_match('/1\d$/', $value)) {
            return  $word2;
        } elseif (preg_match('/1$/', $value)) {
            return $word0;
        } elseif (preg_match('/(2|3|4)$/', $value)) {
            return  $word1;
        } else {
            return $word2;
        }
    }

    public static function cropStr($str, $size)
    {
        if (mb_strlen($str) > $size) {
            $encoding = mb_detect_encoding($str);
                       if ($encoding !== false) {
                               $str = mb_substr($str, 0, $size+-1, $encoding);

                               return $str;
            } else {
                               return $str;
          }
           // return mb_convert_encoding(mb_substr($str, 0, mb_strrpos(mb_substr($str, 0, $size, 'utf-8'), ' ', 'utf-8'), 'utf-8').'...', "UTF-8");
        } else {
            return $str;
        }
    }

    public static function quizAccess($quiz_id)
    {
        $user = plotUser::factory();
        if ($user->id) {
            $db = Foundry::db();
            $query = $db->getQuery(true)
                ->clear()
                ->select('q.id')
                ->from('`#__plot_book_quiz_map` AS q ')
                ->innerJoin('`#__plot_books_paid` AS b ON b.book_id=q.id_pub')
                ->where('q.id_quiz=' . (int)$quiz_id)
                ->where('b.child_id=' . (int)$user->id);

            $db->setQuery($query);

            return $db->loadResult();
        } else {
            return false;
        }
    }

    public static function bookReadAccess($book_id)
    {
        $user = plotUser::factory();
        if (!(int)$user->id) {
            return false;
        }
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->clear()
            ->select('id')
            ->from('`#__plot_books_paid` ')
            ->where('book_id=' . (int)$book_id)
            ->where('child_id=' . (int)$user->id);
        $db->setQuery($query);
        return $db->loadResult();
    }

    public static function getk2item($id)
    {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM `#__k2_items` WHERE `id` = " . $db->quote($id);
        $k2item = $db->setQuery($query)->loadObject();
        return $k2item;
    }

    /**
     * Countdown
     * @param mixed $date
     */
    public static function downcounter($date)
    {

        $check_time = strtotime($date) - time();

        if ($check_time <= 0) {

            return false;
        }

        $days = floor($check_time / 86400);
        $hours = floor(($check_time % 86400) / 3600);
        $minutes = floor(($check_time % 3600) / 60);
        $str = '';

        if ($days > 0)
            $str .= self::declension($days, array('день', 'дня', 'дней')) . ' ';
        if ($hours >= 0) {
            if ($hours < 10) {
                $str .= '0' . self::declension($hours, array(':', ':', ':')) . ' ';
            } else {
                $str .= self::declension($hours, array(':', ':', ':')) . ' ';
            }
        }
        if ($minutes > 0)
            $str .= self::declension($minutes, array('', '', '')) . ' ';

        return $str;
    }

    public static function russian_date($date)
    {
        $date = explode(".", $date);
        switch ($date[1]) {
            case 1:
                $m = 'Январь';
                break;
            case 2:
                $m = 'Февраль';
                break;
            case 3:
                $m = 'Март';
                break;
            case 4:
                $m = 'Апрель';
                break;
            case 5:
                $m = 'Май';
                break;
            case 6:
                $m = 'Июнь';
                break;
            case 7:
                $m = 'Июль';
                break;
            case 8:
                $m = 'Август';
                break;
            case 9:
                $m = 'Сентябрь';
                break;
            case 10:
                $m = 'Октябрь';
                break;
            case 11:
                $m = 'Ноябрь';
                break;
            case 12:
                $m = 'Декабрь';
                break;
        }
        return $m . ' ' . $date[0] . ', ' . $date[2];
    }

    public static function compareDates($date)
    {


        if (strtotime($date) == strtotime("today"))
            return 0;
        else if (strtotime($date) > strtotime("today"))
            return 1;
        else if (strtotime($date) < strtotime("today"))
            return -1;
    }

    public static function diffDates($date_start, $date_end)
    {
        $str = '';
        $start_date = new DateTime($date_start);
        $since_start = $start_date->diff(new DateTime($date_end));

        if ((int)$since_start->h > 10) {
            $str .= $since_start->h . ':';
        } else {
            $str .= '0' . $since_start->h . ':';
        }

        if ((int)$since_start->i > 10) {
            $str .= $since_start->i;
        } else {
            $str .= '0' . $since_start->i;
        }

        return $str;
    }

    /**
     * The function is_date() validates the date and returns true or false
     * @param $str sting expected valid date format
     * @return bool returns true if the supplied parameter is a valid date
     * otherwise false
     */
    public static function is_date($str)
    {
        try {
            $dt = new DateTime(trim($str));
        } catch (Exception $e) {
            return false;
        }
        $month = $dt->format('m');
        $day = $dt->format('d');
        $year = $dt->format('Y');
        if (checkdate($month, $day, $year)) {
            return true;
        } else {
            return false;
        }
    }

    public static function getDefaultText($key, $id)
    {
        $user = plotUser::factory($id);
        if ($user->isParent()) {
            $profile = plotGlobalConfig::getVar('parentProfileId');
        } else {
            $profile = plotGlobalConfig::getVar('childProfileId');
        }
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->clear()
            ->select('`t`.`description`')
            ->from('`#__plot_texts` AS t')
            ->where('`t`.`key`="' . $key . '"')
            ->where('`t`.`profile`=' . (int)$profile);

        $db->setQuery($query);
        return $db->loadResult();
    }

    public static function isNotOldDate($date)
    {

        if (self::is_date($date)) {
            if (strtotime($date) > strtotime('now')) {
                return true;
            }
            return false;
        }

        return false;
    }

    public static function deleteDir($dirPath)
    {

        if (!is_dir($dirPath)) {
            return false;
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }

        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        chmod($dirPath, 0777);
        rmdir($dirPath);
    }

    static function getAbsRoutedUri($uri)
    {
        return JUri::base() . substr(JRoute::_($uri), strlen(JURI::base(true)) + 1);
    }

    static function echoNewImageForSocials($picout, $width, $height)
    {
        # get all params
        list($im_width, $im_height, $im_ext) = getimagesize($picout);

        $canv_width = ($width > $im_width) ? $width : $im_width;
        $canv_height = ($height > $im_height) ? $height : $im_height;

        $bg_color = 'ffffff';
        $no_resample = JRequest::getInt('no_resample');

        # convert background color from hexademical to decimal
        $canv_color['red'] = hexdec(substr($bg_color, 0, 2));
        $canv_color['green'] = hexdec(substr($bg_color, 2, 2));
        $canv_color['blue'] = hexdec(substr($bg_color, 4, 2));

        if (isset($no_resample)) {
            $factor = 1; # not resampled
        } else {
            $factor_x = $im_width / $canv_width;
            $factor_y = $im_height / $canv_height;
            $factor = max($factor_x, $factor_y);
        }

        # resample
        $resampleWidth = intval($im_width / $factor);
        $resampleHeight = intval($im_height / $factor);

        # check source image extension and create needed canvas
        switch ($im_ext) {
            case 1:
                $im = imagecreatefromgif($picout);
                $im_blank = imagecreatetruecolor($canv_width, $canv_height);
                $color = imagecolorallocate($im_blank, $canv_color['red'], $canv_color['green'], $canv_color['blue']);
                imagefill($im_blank, 0, 0, $color);
                imagecopyresampled($im_blank, $im, ($canv_width / 2 - $resampleWidth / 2), ($canv_height / 2 - $resampleHeight / 2), 0, 0, $resampleWidth, $resampleHeight, imagesx($im), imagesy($im));
                header("Content-type: image/gif");
                imagegif($im_blank);
                break;
            case 2:
                $im = imagecreatefromjpeg($picout);
                $im_blank = imagecreatetruecolor($canv_width, $canv_height);
                $color = imagecolorallocate($im_blank, $canv_color['red'], $canv_color['green'], $canv_color['blue']);
                imagefill($im_blank, 0, 0, $color);
                imagecopyresampled($im_blank, $im, ($canv_width / 2 - $resampleWidth / 2), ($canv_height / 2 - $resampleHeight / 2), 0, 0, $resampleWidth, $resampleHeight, imagesx($im), imagesy($im));
                header("Content-type: image/jpeg");
                imagejpeg($im_blank);
                break;
            case 3:
                $im = imagecreatefrompng($picout);
                $im_blank = imagecreatetruecolor($canv_width, $canv_height);
                $color = imagecolorallocate($im_blank, $canv_color['red'], $canv_color['green'], $canv_color['blue']);
                imagefill($im_blank, 0, 0, $color);
                imagecopyresampled($im_blank, $im, ($canv_width / 2 - $resampleWidth / 2), ($canv_height / 2 - $resampleHeight / 2), 0, 0, $resampleWidth, $resampleHeight, imagesx($im), imagesy($im));
                header("Content-type: image/png");
                imagepng($im_blank);
                break;
            default:
                $im = imagecreatefromjpeg("images/no_pic.jpg");
                $im_blank = imagecreatetruecolor($canv_width, $canv_height);
                $color = imagecolorallocate($im_blank, $canv_color['red'], $canv_color['green'], $canv_color['blue']);
                imagefill($im_blank, 0, 0, $color);
                imagecopyresampled($im_blank, $im, ($canv_width / 2 - $resampleWidth / 2), ($canv_height / 2 - $resampleHeight / 2), 0, 0, $resampleWidth, $resampleHeight, imagesx($im), imagesy($im));
                header("Content-type: image/jpeg");
                imagejpeg($im_blank);
                break;
        }
    }

    public static function saveLmsCertificate($id)
    {
        global $JLMS_DB;
        $JLMS_DB = JFactory::getDBO();
        if (!defined('_JOOMLMS_FRONT_HOME')) {
            define('_JOOMLMS_FRONT_HOME', JPATH_SITE . DS . 'components' . DS . 'com_joomla_lms');
        }

        require_once(JPATH_BASE . '/components/com_joomla_lms/includes/lms_certificates.php');
        require_once(JPATH_BASE . '/components/com_joomla_lms/includes/libraries/lms.lib.language.php');
        require_once(JPATH_BASE . '/components/com_joomla_lms/joomla_lms.func.php');

        self::createLmsCertificateImage($id);
        return;
    }

    public static function createLmsCertificateImage($id)
    {
        $db = JFactory::getDbo();
        
        $jlmsCertificateFileId = $db->setQuery("SELECT `file_id` FROM `#__lms_certificates` WHERE `course_id` = ". $db->quote($id))->loadResult();
        $cert_name = $db->SetQuery("SELECT file_srv_name FROM #__lms_files WHERE id = ".$db->quote($jlmsCertificateFileId))->LoadResult();

        $im = imagecreatefromjpeg("jlms/docs/$cert_name");
        $crt = self::getLmsCertificateParams($id);
        $white = imagecolorallocate($im, 255, 255, 255);
        $grey = imagecolorallocate($im, 128, 128, 128);
        $black = imagecolorallocate($im, 0, 0, 0);
        $my = plotUser::factory();

        $text_messages = array();
        $crtf_msg = new stdClass();
        $crtf_msg->text_size = $crt->text_size;
        $crtf_msg->text_x = $crt->text_x;
        $crtf_msg->text_y = $crt->text_y;
        $crtf_msg->crtf_font = (isset($crt->crtf_font) && $crt->crtf_font) ? $crt->crtf_font : 'arial.ttf';
        $crtf_msg->crtf_text = $crt->crtf_text;
        $crtf_msg->course_name = $crt->crtf_name;
        $crtf_msg->crtf_shadow = $crt->crtf_shadow;
        $crtf_msg->crtf_align = $crt->crtf_align;
        $text_messages[] = $crtf_msg;

        $query = $db->getQuery(true);
        $query->select('a.*')
            ->from('`#__lms_certificates` AS a')
            ->where('a.`course_id` = ' . (int)$id . ' AND a.crtf_type =1')
            ->order('a.crtf_align');
        $db->setQuery($query);
        $add_cert_msgs = $db->loadObjectList();
        foreach ($add_cert_msgs as $acms) {
            $crtf_msg = new stdClass();
            $crtf_msg->text_size = $acms->text_size;
            $crtf_msg->text_x = $acms->text_x;
            $crtf_msg->text_y = $acms->text_y;
            $crtf_msg->crtf_font = (isset($acms->crtf_font) && $acms->crtf_font) ? $acms->crtf_font : 'arial.ttf';
            $crtf_msg->crtf_text = $acms->crtf_text;
            $crtf_msg->course_name = $crt->crtf_name;
            $crtf_msg->crtf_shadow = $acms->crtf_shadow;
            $crtf_msg->crtf_align = 0;
            $text_messages[] = $crtf_msg;
        }
        foreach ($text_messages as $crt7) {
            $font_size = $crt7->text_size;
            $font_x = $crt7->text_x;
            $font_y = $crt7->text_y;
            $font_filename = $crt7->crtf_font;
            $inform = array();
            $font_text = $crt7->crtf_text;
            $username = plotUser::factory()->username;
            $name = plotUser::factory()->name;
            $course = new plotCourse($id);
            $course_name = $course->course_name;
            //$spec_answer = isset($txt_mes_obj->crtf_spec_answer)?$txt_mes_obj->crtf_spec_answer:'';
            $crtf_date = strtotime(gmdate("Y-m-d H:i:s"));

            $font_text = str_replace('#username#', $username, $font_text);
            $font_text = str_replace('#name#', $name, $font_text);
            $font_text = str_replace('#course#', $course_name, $font_text);

            $font_text = JLMS_Certificates::ReplaceCourseRegAnswers($font_text, null, $my->id, $id);
            //$font_text = str_replace('#reg_answer#', $spec_answer, $font_text);

            $font_text = JLMS_Certificates::ReplaceQuizAnswers($font_text, null, $my->id, $id);
            $font_text = JLMS_Certificates::ReplaceEventOptions($font_text, null, $my->id, $id);
            $font_text = JLMS_Certificates::ReplaceCBProfileOptions($font_text, null, $my->id, $id);

            // replace #date#
            $str_format = 'Y-m-d';
            $str_format_pre = '';
            $first_pos = strpos($font_text, '#date');
            if ($first_pos !== false) {
                $first_str = substr($font_text, $first_pos + 5, strlen($font_text) - $first_pos - 5);
                $sec_pos = strpos($first_str, '#');
                $str_format = substr($first_str, 0, $sec_pos);
                $str_format_pre = $str_format;
                if ($str_format) {
                    if (substr($str_format, 0, 1) == '(') {
                        $str_format = substr($str_format, 1);
                    }
                    if (substr($str_format, -1) == ')') {
                        $str_format = substr($str_format, 0, -1);
                    }
                }
            }
            if (!$str_format) {
                $str_format = 'Y-m-d';
            }

            $font_text = str_replace('#date' . $str_format_pre . '#', JLMS_cp1251_to_utf8(strftime(JLMS_formatDateToStrFTime($str_format), $crtf_date + (1 * 3600))), $font_text);

            // end of #date#
            $font = JPATH_SITE . "/media/arial.ttf";
            if (file_exists(JPATH_SITE . "/media/" . $font_filename)) {
                $font = JPATH_SITE . "/media/" . $font_filename;
            }
            $text_array = explode("\n", $font_text);
            #print_r($text_array);die;
            $count_lines = count($text_array);
            $text_lines_xlefts = array();
            $text_lines_xrights = array();
            $text_lines_heights = array();
            for ($i = 0; $i < $count_lines; $i++) {
                $font_box = imagettfbbox($font_size, 0, $font, $text_array[$i]);
                $text_lines_xlefts[$i] = $font_box[0];
                $text_lines_xrights[$i] = $font_box[2];
                $text_lines_heights[$i] = $font_box[1] - $font_box[7];
                if ($text_lines_heights[$i] < $font_size) {
                    $text_lines_heights[$i] = $font_size;
                }
            }
            $min_x = 0;
            $max_x = 0;
            $max_w = 0;
            for ($i = 0; $i < $count_lines; $i++) {
                if ($min_x > $text_lines_xlefts[$i])
                    $min_x = $text_lines_xlefts[$i];
                if ($max_x < $text_lines_xrights[$i])
                    $max_x = $text_lines_xrights[$i];
                if ($max_w < ($text_lines_xrights[$i] - $text_lines_xlefts[$i]))
                    $max_w = ($text_lines_xrights[$i] - $text_lines_xlefts[$i]);
            }

            $allow_shadow = ($crt7->crtf_shadow == 1);

            switch (intval($crt7->crtf_align)) {
                case 1:
                    for ($i = 0; $i < $count_lines; $i++) {
                        $cur_w = $text_lines_xrights[$i] - $text_lines_xlefts[$i];
                        $ad = intval(($max_w - $cur_w) / 2) - intval($max_w / 2);
                        if ($allow_shadow)
                        imagettftext($im, $font_size, 0, $font_x + $ad + 2, $font_y + 2, $grey, $font, $text_array[$i]);
                        imagettftext($im, $font_size, 0, $font_x + $ad, $font_y, $black, $font, $text_array[$i]);
                        $font_y = $font_y + $text_lines_heights[$i] + 3;
                    }
                    break;
                case 2:
                    for ($i = 0; $i < $count_lines; $i++) {
                        $cur_w = $text_lines_xrights[$i] - $text_lines_xlefts[$i];
                        $ad = intval($max_w - $cur_w) - intval($max_w);
                        if ($allow_shadow)
                        imagettftext($im, $font_size, 0, $font_x + $ad + 2, $font_y + 2, $grey, $font, $text_array[$i]);
                        imagettftext($im, $font_size, 0, $font_x + $ad, $font_y, $black, $font, $text_array[$i]);
                        $font_y = $font_y + $text_lines_heights[$i] + 3;
                    }
                    break;
                default:
                    for ($i = 0; $i < $count_lines; $i++) {
                        $cur_w = $text_lines_xrights[$i] - $text_lines_xlefts[$i];
                        $ad = 0; //intval(($max_w - $cur_w)/2);
                        if ($allow_shadow)
                        imagettftext($im, $font_size, 0, $font_x + $ad + 2, $font_y + 2, $grey, $font, $text_array[$i]);
                        imagettftext($im, $font_size, 0, $font_x + $ad, $font_y, $black, $font, $text_array[$i]);
                        $font_y = $font_y + $text_lines_heights[$i] + 3;
                    }
                    break;
            }
        }

        $database = Foundry::db();
        $query = $database->getQuery(true)
            ->select('`a`.*')
            ->from('`#__social_albums` AS `a`')
            ->where('`a`.`uid` = ' . (int)Foundry::user()->id)
            ->where('`a`.`type` = "plot-certificate"');
        $database->setQuery($query);
        $album = $database->loadObject();
        if (!$album) {
            $album = Foundry::table('Album');
            $album->uid = $my->id;
            $album->type = 'plot-certificate';
            $album->created = Foundry::date()->toMySQL();
            $album->ordering = 0;
            $album->assigned_date = Foundry::date()->toMySQL();
            $database->insertObject('#__social_albums', $album);
            $albumId = $database->insertid();
        } else {
            $albumId = $album->id;
        }

        $photo = Foundry::table('Photo');
        $photo->uid = $my->id;
        $photo->type = SOCIAL_TYPE_USER;
        $photo->album_id = $albumId;
        $photo->title = '';
        $photo->caption = '';
        $photo->ordering = 0;
        $photo->state = SOCIAL_STATE_PUBLISHED;

        // Set the creation date alias
        $photo->assigned_date = Foundry::date()->toMySQL();
        $photo->created = Foundry::date()->toMySQL();
        $database->insertObject('#__social_photos', $photo);
        $photoId = $database->insertid();

        $new_img_name = md5(time() . $my->id);

        $phototags5 = new stdClass();
        $phototags5->photo_id = $photoId;
        $phototags5->group = 'exif';
        $phototags5->property = 'angle';
        $phototags5->value = 0;
        $database->insertObject('#__social_photos_meta', $phototags5);
        $database->insertid();

        if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_easysocial/photos/' . $albumId)) {
            mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_easysocial/photos/' . $albumId);
        }

        if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_easysocial/photos/' . $albumId . DIRECTORY_SEPARATOR . $photoId)) {
            mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_easysocial/photos/' . $albumId . DIRECTORY_SEPARATOR . $photoId);
        }

        $prop = array('stock', 'original', 'square', 'thumbnail', 'featured', 'large');
        $phototags = new stdClass();
        $phototags->photo_id = $photoId;
        $phototags->group = 'path';
        $phototags->property = 'stock';
        $phototags->value = JPATH_ROOT . '/media/com_easysocial/photos/' . $albumId . '/' . $photoId . '/' . $new_img_name . '_stock.jpg';
        $database->insertObject('#__social_photos_meta', $phototags);
        $database->insertid();

        foreach ($prop AS $p) {
            $phototags = new stdClass();
            $phototags->photo_id = $photoId;
            $phototags->group = 'path';
            $phototags->property = $p;
            $phototags->value = JPATH_ROOT . '/media/com_easysocial/photos/' . $albumId . '/' . $photoId . '/' . $new_img_name . '_' . $p . '.jpg';
            $database->insertObject('#__social_photos_meta', $phototags);
            $database->insertid();
            @imagejpeg($im, 'media/com_easysocial/photos/' . $albumId . '/' . $photoId . '/' . $new_img_name . '_' . $p . '.jpg');
        }
        @imagedestroy($im);
    }

    public static function getLmsCertificateParams($quiz_id)
    {
        $database = Foundry::db();
        $query = $database->getQuery(true);
        $query->select('a.*');
        $query->from('`#__lms_certificates` AS a');
        $query->where('a.`course_id` = ' . (int)$quiz_id . ' AND published=1');
        $database->setQuery($query);
        $certificate = $database->loadObject();
        return $certificate;
    }
    
    static function isCourseCertificateExist($courseId)
    {
        $db = JFactory::getDbo();
        $result = $db->setQuery("SELECT `id` FROM `#__lms_certificates` WHERE `course_id` = ".$db->quote($courseId)." AND `published` = '1'")->loadResult();
        return $result ? true : false;
    }

    static function sendSystemMessage($userId, $message)
    {
        $my = Foundry::user();
        $recipients = array($userId);
        // The user might be writing to a friend list.
        $lists = JRequest::getVar('list_id');
        // Get configuration
        $config = Foundry::config();
        // Check if the creator is allowed to send a message to the target
        $privacy = $my->getPrivacy();

        // Ensure that the recipients is not only itself.
        foreach ($recipients as $recipient) {
            // When user tries to enter it's own id, we should just break out of this function.
            if ($recipient == $my->id) {
                return false;
            }
        }

        $msg = $message;

        // Message should not be empty.
        if (empty($msg)) {
            return false;
        }

        // Filter recipients and ensure all the user id's are proper!
        $total = count($recipients);

        // If there is more than 1 recipient and group conversations is disabled, throw some errors
        if ($total > 1 && !$config->get('conversations.multiple')) {
            return false;
        }

        // Go through all the recipient and make sure that they are valid.
        for ($i = 0; $i < $total; $i++) {
            $userId = $recipients[$i];
            $user = Foundry::user($userId);
            if (!$user || empty($userId)) {
                unset($recipients[$i]);
            }
        }

        // After processing the recipients list, and no longer has any recipients, stop the user.
        if (empty($recipients)) {
            return false;
        }

        // Get the conversation table.
        $conversation = Foundry::table('Conversation');

        // Determine the type of message this is by the number of recipients.
        $type = count($recipients) > 1 ? SOCIAL_CONVERSATION_MULTIPLE : SOCIAL_CONVERSATION_SINGLE;

        // For single recipients, we try to reuse back previous conversations
        // so that it will be like a long chat of history.
        if ($type == SOCIAL_CONVERSATION_SINGLE) {
            // We know that the $recipients[0] is always the target user.
            $state = $conversation->loadByRelation($my->id, $recipients[0], SOCIAL_CONVERSATION_SINGLE);
        }

        // Set the conversation creator.
        $conversation->created_by = $my->id;
        // Set the last replied date.
        $conversation->lastreplied = Foundry::date()->toMySQL();
        // Set the conversation type.
        $conversation->type = $type;

        // Let's try to create the conversation now.
        $state = $conversation->store();

        // If there's an error storing the conversation, break.
        if (!$state) {
            return false;
        }

        // @rule: Store conversation message
        $message = Foundry::table('ConversationMessage');
        $post = JRequest::get('POST');
        $post['message'] = $msg;
        $message->bind($post);

        // Set the conversation id since we have the conversation id now.
        $message->conversation_id = $conversation->id;

        // Sets the message type.
        $message->type = SOCIAL_CONVERSATION_TYPE_MESSAGE;

        // Set the creation date.
        $message->created = Foundry::date()->toMySQL();

        // Set the creator.
        $message->created_by = $my->id;

        // Try to store the message now.
        $state = $message->store();

        if (!$state) {
            return false;
        }

        // Add users to the message maps.
        array_unshift($recipients, $my->id);

        $model = Foundry::model('Conversations');

        // Add the recipient as a participant of this conversation.
        $model->addParticipants($conversation->id, $recipients);

        // Add the message maps so that the recipient can view the message
        $model->addMessageMaps($conversation->id, $message->id, $recipients, $my->id);
        return true;
    }

    static function getUserIdByEmail($email)
    {
        $db = JFactory::getDBO();
        $userId = $db->setQuery("SELECT `id` FROM `#__users` WHERE `email` = " . $db->quote($email))->loadResult();
        return $userId;
    }

    static function sendEmailFromAdmin($emailTo, $subject, $body)
    {
        $mailer = JFactory::getMailer();
        $sender = array(JFactory::getConfig()->get('mailfrom'), 'Plot system');
        $mailer->setSender($sender);
        $mailer->addRecipient($emailTo);
        $mailer->setSubject($subject);
        $mailer->setBody($body);
        $send = $mailer->Send();
        if ($send !== true) {
            return false;
        }
        return true;
    }
    
    static function sendEmailToAdmin($emailFrom, $subject, $body)
    {
        $mailer = JFactory::getMailer();
        $recipient = JFactory::getConfig()->get('mailfrom');
        $mailer->setSender($emailFrom);
        $mailer->addRecipient($recipient);
        $mailer->setSubject($subject);
        $mailer->setBody($body);
        $send = $mailer->Send();
        if ($send !== true) {
            return false;
        }
        return true;
    }

    public static function getCountPages($id)
    {
        $db = JFactory::$database;
        $query = $db->getQuery(true)
            ->select('COUNT(*) AS count')
            ->from('`#__html5fb_pages`')
            ->where('`publication_id` = ' . $id);
        $db->setQuery($query);
        $count = $db->loadResult();
        return $count;
    }

    public static function getPersent($all, $pages)
    {
        $max_percent = plotGlobalConfig::getVar('maxPersentForUnreadBook');
        if ($all == $pages) {
            $percent = $max_percent;
        } else {
            if($all) {
                $percent = ($max_percent * $pages * 2) / ($all);
            }else{
                $percent=0;
            }
        }
        if ($percent > 0 && $percent < 1) {
            $percent = 1;
        }
        if ($percent > $max_percent) {
            $percen = $max_percent;
        }
        return round($percent);
    }

    public static function renderEventDuration($event)
    {
        $str = '';
        switch (self::compareDates(JHtml::date($event->start_date, 'Y-m-d'))) {
            case -1:
                if (self::compareDates(JHtml::date($event->end_date, 'Y-m-d')) == -1) {
                    ?>
                    <p class="duration"><b>Длительность</b>
                        <time><?php echo self::diffDates(JHtml::date($event->start_date, 'Y-m-d H:m'), JHtml::date($event->end_date, 'Y-m-d H:m')); ?></time>
                    </p>
                <?php
                } else {
                    $str .= '<p class="before-end"><b>До окончания:</b>';
                    $str .= '<time>' . PlotHelper::downcounter($event->end_date) . '</time>';
                    $str .= '</p>';
                }
                break;
            case 1:
                $str .= '<p class="before-start"><b>До начала:</b>';
                $str .= '<time>' . self::downcounter($event->start_date) . '</time>';
                $str .= '</p>';
                break;
            case 0:
                if ((int)PlotHelper::compareDates(JHtml::date($event->end_date, 'Y-m-d')) == 1) {

                    $str .= '<p class="before-start"><b>До начала:</b>';
                    $str .= '<time>' . self::downcounter($event->start_date) . '</time>';
                    $str .= '</p>';
                } else if ((int)PlotHelper::compareDates(JHtml::date($event->end_date, 'Y-m-d')) == 0) {
                    $str .= '<p class="before-end"><b>До окончания:</b>';
                    $str .= '<time>' . self::downcounter($event->start_date) . '</time>';
                    $str .= '</p>';
                }
                break;
        }

        return $str;
    }

    public static function renderParentStream($stream)
    {
        $str = '';
        if ($stream) {
            foreach ($stream AS $streamObj) {
                switch ($streamObj->command) {
                    case 'buy.book':
                    case 'read.book':
                        $str .= '<li class="parent-books">';
                        $str .= '<a href="' . $streamObj->original . '">';
                        $str .= '<h6>';
                        $str .= '<span class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';
                        $str .= '<p class="stream-title">' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                        $str .= '</h6>';
                        $str .= '<hr/>';
                        $str .= '<span>' . JFactory::getDate($streamObj->created)->format("d.m.Y H:i") . '</span>';

                        $str .= '<div class="circle-img">';
                        if ($streamObj->image_url) {
                            $str .= '<svg class="clip-svg">';
                            $str .= ' <image style="clip-path: url(#clipping-circle);" width="100%" height="100%"  xlink:href="' . $streamObj->image_url . '"/>';
                            $str .= ' </svg>';
                        }

                        $str .= '<i class="activity-icons">';
                        $str .= '<svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">';
                        $str .= '<use xlink:href="#book"></use>';
                        $str .= '</svg>';
                        $str .= '</i>';
                        $str .= '</div>';
                        if ($streamObj->bottom_description) {
                            $str .= '<p>' . PlotHelper::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')) . '</p>';
                        }
                        $str .= '</a>';
                        $str .= '</li>';
                        break;
                    case 'course.finish':
                        $str .= '<li class="parent-course">';
                        $str .= '<a href="' . $streamObj->original . '">';
                        $str .= '<h6>';
                        $str .= '<span class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';
                        $str .= '<p class="stream-title">' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                        $str .= '</h6>';
                        $str .= ' <hr/>';
                        $str .= '<span>' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '</span>';
                        $str .= '<div class="circle-img">';
                        if ($streamObj->image_url) {
                            $str .= '<svg class="clip-svg-course">';
                            $str .= '<image style="clip-path: url(#clipping-circle-course);" width="100%" height="100%"  xlink:href="' . $streamObj->image_url . '"/>';
                            $str .= '</svg>';
                        }
                        $str .= '<svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"  style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">';
                        $str .= '<use xlink:href="#academic-hat"></use>';
                        $str .= '</svg>';
                        $str .= '</div>';
                        if ($streamObj->bottom_description) {
                            $str .= '<p>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar("parentActivityAllDescriptionMaxSymbolsToShow")) . '</p>';
                        }

                        $str .= '</a>';
                        $str .= '</li>';
                        break;
                    case 'add.meeting':
                    case 'subscribe.meeting':
                    case 'unsubscribe.meeting':

                        $str .= '<li class="parent-meetings">';
                        $str .= '<a href="' . $streamObj->original . '">';
                        $str .= '<h6>';
                        $str .= '<span class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';
                        $str .= '<p class="stream-title">' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                        $str .= '</h6>';
                        $str .= '<hr/>';
                        $str .= '<span>' . JFactory::getDate($streamObj->created)->format("d.m.Y H:i") . '</span>';
                        $str .= '<div class="circle-img">';
                        if ($streamObj->image_url) {

                            $str .= '<svg class="clip-svg">';
                            $str .= '<image style="clip-path: url(#clipping-circle);" width="100%" height="100%" xlink:href="' . $streamObj->image_url . '"/>';
                            $str .= ' </svg>';
                        }
                        $str .= '<i class="activity-icons">';
                        $str .= '<svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);">';
                        $str .= '<use xlink:href="#lamp-meeting"></use>';
                        $str .= '</svg>';
                        $str .= '</i>';
                        $str .= '</div>';

                        if ($streamObj->bottom_description) {
                            $str .= '<p>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')) . '</p>';
                        }

                        $str .= ' </a>';
                        $str .= '</li>';

                        break;
                    case 'add.certificate':
                    case 'add.old.certificate':

                        $str .= '<li class="parent-sertificates">';
                        $str .= '<a class="fancybox" rel="stream-set" data-title="' . $streamObj->title . '"';
                        $str .= 'data-description="' . $streamObj->bottom_description . '"';
                        $str .= 'data-date="' . JFactory::getDate($streamObj->created)->format("d.m.Y H:i") . '"';
                        $str .= ' href="' . $streamObj->original . '">';
                        $str .= '<h6>';
                        $str .= ' <span class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';

                        $str .= '<p class="stream-title">' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                        $str .= '</h6>';
                        $str .= ' <hr/>';
                        $str .= '<span>' . JFactory::getDate($streamObj->created)->format("d.m.Y H:i") . '</span>';

                        $str .= '<div class="circle-img">';
                        if ($streamObj->image_url) {
                            $str .= '<svg class="clip-svg">';
                            $str .= '<image style="clip-path: url(#clipping-circle);" width="100%" height="100%" xlink:href="' . $streamObj->image_url . '"/>';
                            $str .= '</svg>';
                        }
                        $str .= '<i class="activity-icons">';
                        $str .= '<svg viewBox="0 0 24.9 25.8" preserveAspectRatio="xMidYMid meet"  style="fill:url(#svg-gradient);">';
                        $str .= '<use xlink:href="#certificate"></use>';
                        $str .= '</svg>';
                        $str .= ' </i>';
                        $str .= '</div>';
                        if ($streamObj->bottom_description) {
                            $str .= ' <p>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')) . '</p>';
                        }

                        $str .= '</a>';
                        $str .= '</li>';

                        break;
                    case 'avatar.change':
                    case 'add.photo':
                        $str .= '<li class="parent-photos">';
                        $str .= '<a class="fancybox" rel="stream-set" data-title="' . $streamObj->title . '"';
                        $str .= 'data-description="' . $streamObj->bottom_description . '"';
                        $str .= 'data-date="' . JFactory::getDate($streamObj->created)->format("d.m.Y H:i") . '" href="' . $streamObj->original . '">';
                        $str .= '<h6>';
                        $str .= '<span class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar("childActivityTitleMaxSymbolsToShow")) . '</span>';

                        $str .= ' <p class="stream-title">' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                        $str .= '</h6>';
                        $str .= ' <hr/>';
                        $str .= '<span>' . JFactory::getDate($streamObj->created)->format("d.m.Y H:i") . '</span>';
                        $str .= '<div class="circle-img">';
                        if ($streamObj->image_url) {
                            $str .= '<svg class="clip-svg">';
                            $str .= '<image style="clip-path: url(#clipping-circle);" width="100%" height="100%" xlink:href="' . $streamObj->image_url . '"/>';
                            $str .= '</svg>';
                        }
                        $str .= '<i class="activity-icons">';
                        $str .= ' <svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">';
                        $str .= '<use xlink:href="#zoom"></use>';
                        $str .= '</svg>';
                        $str .= ' </i>';
                        $str .= '</div>';
                        if ($streamObj->bottom_description) {
                            $str .= '<p>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')) . '</p>';
                        }

                        $str .= '</a>';
                        $str .= '</li>';
                        break;
                    case 'add.video':
                        if ($streamObj->video_type == 'link') {

                            $str .= '<li class="parent-video">';
                            $str .= '<a class="fancybox" data-fancybox-type="ajax" rel="stream-set"';
                            $str .= 'data-title="' . $streamObj->bottom_title . '"';
                            $str .= 'data-description="' . $streamObj->bottom_description . '"';
                            $str .= 'data-date="' . JFactory::getDate($streamObj->created)->format("d.m.Y H:i") . '"';
                            $str .= 'href="' . JRoute::_('index.php?option=com_plot&task=profile.ajaxOpenVideo&youtubeNumber=' . $streamObj->youtubeNumber) . '">';
                            $str .= '<h6>';
                            $str .= '<span class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';

                            $str .= '<p class="stream-title">' . PlotHelper::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                            $str .= '</h6>';
                            $str .= '<hr/>';
                            $str .= '<span>' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '</span>';

                            $str .= '<div class="circle-img">';
                            if ($streamObj->image_url) {
                                $str .= '<svg class="clip-svg">';
                                $str .= '<image style="clip-path: url(#clipping-circle);" width="100%" height="100%" xlink:href="' . $streamObj->image_url . '"/>';
                                $str .= '</svg>';
                            }
                            $str .= '<i class="activity-icons">';
                            $str .= '<svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet" style="fill:url(#svg-gradient);">';
                            $str .= '<use xlink:href="#play"></use>';
                            $str .= '</svg>';
                            $str .= '</i>';
                            $str .= '</div>';

                            if ($streamObj->bottom_description) {
                                $str .= '<p><' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')) . '</p>';
                            }

                            $str .= '</a>';
                            $str .= '</li>';
                        } else {
                            $str .= ' <li class="parent-video">';
                            $str .= '<a class="fancybox" data-fancybox-type="ajax" rel="stream-set"';
                            $str .= ' data-title="' . $streamObj->bottom_title . '"';
                            $str .= ' data-description="' . $streamObj->bottom_description . '"';
                            $str .= 'data-date="' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '"';
                            $str .= 'href="' . JRoute::_('index.php?option=com_plot&task=profile.openVideoFile&videoId=' . $streamObj->youtubeNumber) . '"';
                            $str .= '<h6>';
                            $str .= '<span>' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';

                            $str .= '<p>' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                            $str .= '</h6>';
                            $str .= '<hr/>';
                            $str .= '<span>' . $streamObj->created . '</span>';

                            $str .= '<div class="circle-img">';
                            if ($streamObj->image_url) {

                                $str .= '<svg class="clip-svg">';
                                $str .= '<image style="clip-path: url(#clipping-circle);" width="100%" height="100%" xlink:href="' . $streamObj->image_url . '"/>';
                                $str .= '</svg>';
                            }
                            $str .= '<i class="activity-icons">';
                            $str .= '<svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet" style="fill:url(#svg-gradient);">';
                            $str .= '<use xlink:href="#play"></use>';
                            $str .= ' </svg>';
                            $str .= ' </i>';
                            $str .= '</div>';
                            if ($streamObj->bottom_description) {
                                $str .= '<p>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')) . '</p>';
                            }
                            $str .= '</a>';
                            $str .= '</li>';
                        }
                        break;
                    case 'add.portfolio':
                        $str .= '<li class="parent-portfolio">';
                        $str .= '<a href="' . $streamObj->original . '">';
                        $str .= '<h6>';
                        $str .= '<span  class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';

                        $str .= '<p class="stream-title">' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                        $str .= '</h6>';
                        $str .= '<hr/>';
                        $str .= '<span>' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '</span>';

                        $str .= '<div class="circle-img">';
                        if ($streamObj->image_url) {
                            $str .= '<svg class="clip-svg">';
                            $str .= '<image style="clip-path: url(#clipping-circle);" width="100%" height="100%" xlink:href="' . $streamObj->image_url . '"/>';
                            $str .= '</svg>';
                        }
                        $str .= '<i class="activity-icons">';
                        $str .= '<svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);">';
                        $str .= '<use xlink:href="#portfolio"></use>';
                        $str .= '</svg>';
                        $str .= '</i>';
                        $str .= '</div>';
                        if ($streamObj->bottom_description) {
                            $str .= '<p>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')) . '</p>';
                        }

                        $str .= '</a>';
                        $str .= '</li>';

                        break;
                    case 'add.tag':
                    case 'assess.tag':
                    case 'describe.tag':

                        $str .= '<li class="event-whithout-link">';
                        $str .= '<h6>';
                        $str .= ' <span class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';

                        $str .= ' <p class="stream-title">' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                        $str .= '</h6>';
                        $str .= '<hr/>';
                        $str .= '<span>' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '</span>';

                        $str .= '<div class="circle-img">';
                        if ($streamObj->image_url) {

                            $str .= '<svg class="clip-svg">';
                            $str .= '<image style="clip-path: url(#clipping-circle);" width="100%" height="100%"  xlink:href="' . $streamObj->image_url . '"/>';
                            $str .= '</svg>';
                        }

                        if ($streamObj->bottom_description) {
                            $str .= '<p>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('parentActivityAllDescriptionMaxSymbolsToShow')) . '</p>';
                        }
                        $str .= ' </li>';

                        break;
                }
            }
        }
        return $str;
    }

    public static function renderChildStream($stream)
    {
        $str = '';
        if ($stream) {
            foreach ($stream AS $streamObj) {
                switch ($streamObj->command) {
                    case 'buy.book':
                    case 'read.book':
                        $str .= '<li class="stream-item">';
                        $str .= '<a href="' . $streamObj->original . '">';
                        $str .= '<h6>';
                        $str .= '<i>';
                        $str .= '<svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">';
                        $str .= '<use xlink:href="#star"></use>';
                        $str .= '</svg>';
                        $str .= '</i>';
                        $str .= '<span class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';
                        $str .= '<p class="stream-title">' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                        $str .= '</h6>';
                        $str .= '<data>' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '</data>';
                        $str .= '<div>';
                        $str .= '<figure class="book">';
                        if ($streamObj->image_url) {
                            $str .= '<img src="' . $streamObj->image_url . '"/>';
                        }
                        $str .= ' <i class="activity-icons">';
                        $str .= '<svg viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">';
                        $str .= '<use xlink:href="#book"></use>';
                        $str .= '</svg>';
                        $str .= '</i>';
                        $str .= '</figure>';
                        $str .= '</div>';
                        $str .= '<div>';
                        if ($streamObj->bottom_description) {
                            $str .= '<blockquote>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')) . '</blockquote>';
                        }
                        $str .= '</div>';
                        $str .= '</a>';
                        $str .= '</li>';
                        break;
                    case 'course.finish':
                        $str .= '<li class="stream-item">';
                        $str .= '<a href="' . $streamObj->image_url . '"  class="modal" rel="{handler:\'image\'}">';
                        $str .= '<h6>';
                        $str .= ' <i>';
                        $str .= '<svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">';
                        $str .= '<use xlink:href="#star"></use>';
                        $str .= ' </svg>';
                        $str .= '</i>';
                        $str .= '<span class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';
                        $str .= '<p class="stream-title">' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                        $str .= '</h6>';
                        $str .= '<data>' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '</data>';
                        $str .= '<div>';
                        $str .= '<figure>';
                        if ($streamObj->image_url) {
                            $str .= '<img src="' . $streamObj->image_url . '" />';
                            $str .= '<i class="activity-icons">';
                            $str .= '<svg style="fill:url(#svg-gradient); stroke:url(#svg-gradient);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 30.3 34">';
                            $str .= '<use xlink:href="#zoom" />';
                            $str .= '</svg>';
                            $str .= '</i>';
                        }
                        $str .= '</figure>';
                        $str .= '</div>';
                        $str .= '<div>';
                        if ($streamObj->bottom_description) {
                            $str .= '<blockquote>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')) . '</blockquote>';
                        }
                        $str .= '</div>';
                        $str .= '</a>';
                        $str .= '</li>';

                        break;
                    case 'add.meeting':
                    case 'subscribe.meeting':
                    case 'add.meeting':
                        $str .= '<li class="stream-item" >';
                        $str .= '<a href="#">';
                        $str .= ' <h6>';
                        $str .= '<i>';
                        $str .= '<svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">';
                        $str .= ' <use xlink:href="#star"></use>';
                        $str .= '</svg>';
                        $str .= '</i>';
                        $str .= '<span class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';
                        $str .= '<p class="stream-title">' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                        $str .= '</h6>';
                        $str .= '<data>' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '</data>';
                        $str .= '<div>';
                        $str .= '<figure>';
                        if ($streamObj->image_url) {
                            $str .= '<img src="' . $streamObj->image_url . '"/>';
                        }
                        $str .= ' <i class="activity-icons">';
                        $str .= '<svg viewBox="0 0 31.6 48.8" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);">';
                        $str .= '<use xlink:href="#lamp-meeting"></use>';
                        $str .= '</svg>';
                        $str .= '</i>';
                        $str .= ' </figure>';
                        $str .= ' </div>';
                        $str .= ' <div>';
                        if ($streamObj->bottom_description) {
                            $str .= '<blockquote>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')) . '</blockquote>';
                        }
                        $str .= '</div>';
                        $str .= '</a>';
                        $str .= '</li>';
                        break;
                    case 'add.certificate':
                    case 'add.old.certificate':
                        $str .= '<li class="stream-item" >';
                        $str .= '<a class="fancybox" rel="stream-set" data-title="' . $streamObj->title . '"';
                        $str .= 'data-description="' . $streamObj->bottom_description . '" data-date="' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '"';
                        $str .= 'href="' . $streamObj->original . '">';
                        $str .= '<h6>';
                        $str .= '<i>';
                        $str .= '<svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">';
                        $str .= '<use xlink:href="#star"></use>';
                        $str .= '</svg>';
                        $str .= '</i>';
                        $str .= '<span class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';
                        $str .= '<p class="stream-title">' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                        $str .= '</h6>';
                        $str .= '<data>' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '</data>';
                        $str .= '<div>';
                        $str .= '<figure>';
                        if ($streamObj->image_url) {
                            $str .= '<img src="' . $streamObj->image_url . '"/>';
                        }
                        $str .= '<i class="activity-icons">';
                        $str .= '<svg viewBox="0 0 24.9 25.8" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient);">';
                        $str .= '<use xlink:href="#certificate"></use>';
                        $str .= '</svg>';
                        $str .= '</i>';
                        $str .= '</figure>';
                        $str .= '</div>';
                        $str .= '<div>';
                        if ($streamObj->bottom_description) {
                            $str .= '<blockquote>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')) . '</blockquote>';
                        }
                        $str .= '</div>';
                        $str .= '</a>';
                        $str .= '</li>';
                        break;
                    case 'add.photo':
                        $str .= '<li class="stream-item" >';
                        $str .= '<a class="fancybox" rel="stream-set" data-title="' . $streamObj->title . '"';
                        $str .= 'data-description="' . $streamObj->bottom_description . '" data-date="' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '"';
                        $str .= 'href="' . $streamObj->original . '">';
                        $str .= '<h6>';
                        $str .= ' <i>';
                        $str .= '<svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">';
                        $str .= '<use xlink:href="#star"></use>';
                        $str .= '</svg>';
                        $str .= '</i>';
                        $str .= '<span class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';
                        $str .= '<p class="stream-title">' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                        $str .= '</h6>';
                        $str .= '<data>' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '</data>';
                        $str .= '<div>';
                        $str .= ' <figure>';
                        if ($streamObj->image_url) {
                            $str .= '<img src="' . $streamObj->image_url . '"/>';
                        }
                        $str .= '<i class="activity-icons">';
                        $str .= '<svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">';
                        $str .= '<use xlink:href="#zoom"></use>';
                        $str .= ' </svg>';
                        $str .= '</i>';
                        $str .= '</figure>';
                        $str .= '</div>';
                        $str .= '<div>';
                        if ($streamObj->bottom_description) {
                            $str .= '<blockquote>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')) . '</blockquote>';
                        }
                        $str .= '</div>';
                        $str .= ' </a>';
                        $str .= ' </li>';
                        break;
                    case 'avatar.change':
                        $str .= '<li class="stream-item" >';
                        $str .= '<a class="fancybox" rel="stream-set" data-title="' . $streamObj->title . '"';
                        $str .= 'data-description="' . $streamObj->bottom_description . '" data-date="' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '"';
                        $str .= 'href="' . $streamObj->original . '">';
                        $str .= '<h6>';
                        $str .= '<i>';
                        $str .= '<svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">';
                        $str .= '<use xlink:href="#star"></use>';
                        $str .= '</svg>';
                        $str .= '</i>';
                        $str .= '<span class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';
                        $str .= '<p class="stream-title">' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                        $str .= ' </h6>';
                        $str .= '<data>' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '</data>';
                        $str .= '<div>';
                        $str .= ' <figure>';
                        if ($streamObj->image_url) {
                            $str .= '<img src="' . $streamObj->image_url . '"/>';
                        }
                        $str .= '<i class="activity-icons">';
                        $str .= '<svg viewBox="0 0 30.3 34" preserveAspectRatio="xMidYMid meet" style="fill:url(#svg-gradient); stroke:url(#svg-gradient);">';
                        $str .= '<use xlink:href="#zoom"></use>';
                        $str .= '</svg>';
                        $str .= '</i>';
                        $str .= '</figure>';
                        $str .= '</div>';
                        $str .= ' <div>';
                        if ($streamObj->bottom_description) {
                            $str .= '<blockquote>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')) . '</blockquote>';
                        }
                        $str .= '</div>';
                        $str .= '</a>';
                        $str .= '</li>';
                        break;
                    case 'add.video':
                        $str .= '<li class="stream-item">';
                        $str .= '<a  class="fancybox" data-fancybox-type="ajax" rel="stream-set"';
                        $str .= ' data-title="' . $streamObj->bottom_title . '" data-description="' . $streamObj->bottom_description . '"';
                        $str .= 'data-date="' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '"';
                        if ($streamObj->video_type == 'link') {
                            $str .= 'href="' . JRoute::_('index.php?option=com_plot&task=profile.ajaxOpenVideo&youtubeNumber=' . $streamObj->youtubeNumber) . '" >';
                        } else {
                            $str .= 'href="' . JRoute::_('index.php?option=com_plot&task=profile.openVideoFile&videoId=' . $streamObj->youtubeNumber) . '" > ';
                        }
                        $str .= '<h6>';
                        $str .= '<i>';
                        $str .= '<svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star"><use xlink:href="#star"></use></svg>';
                        $str .= ' </i>';
                        $str .= ' <span class="action-title">';
                        self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow'));
                        $str .= '</span>';
                        $str .= '<p class="stream-title">';
                        self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow'));
                        $str .= '</p>';
                        $str .= '</h6>';
                        $str .= '<data>' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '</data>';
                        $str .= ' <div>';
                        $str .= '<figure>';
                        if ($streamObj->image_url) {
                            $str .= '<img src="' . $streamObj->image_url . '"/>';
                        }
                        $str .= '<i class="activity-icons">';
                        $str .= '<svg viewBox="0 0 27.1 38.3" preserveAspectRatio="xMaxYMid meet" style="fill:url(#svg-gradient);"><use xlink:href="#play"></use></svg>';
                        $str .= '</i>';
                        $str .= '</figure>';
                        $str .= ' </div>';
                        $str .= '<div>';
                        if ($streamObj->bottom_description) {
                            $str .= '<blockquote>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')) . '</blockquote>';
                        }
                        $str .= '</div>';
                        $str .= '</a>';
                        $str .= '</li>';
                        break;
                    case 'add.portfolio':
                        $str .= '<li class="stream-item" >';
                        $str .= '<a href="' . $streamObj->original . '">';
                        $str .= '<h6>';
                        $str .= '<i>';
                        $str .= '<svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">';
                        $str .= '<use xlink:href="#star"></use>';
                        $str .= '</svg>';
                        $str .= '</i>';
                        $str .= ' <span class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';
                        $str .= '<p class="stream-title">' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                        $str .= '</h6>';
                        $str .= '<data>' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '</data>';
                        $str .= '<div>';
                        $str .= ' <figure>';
                        if ($streamObj->image_url) {
                            $str .= '<img src="' . $streamObj->image_url . '"/>';
                        }
                        $str .= '</figure>';
                        $str .= ' </div>';
                        $str .= '<div>';
                        if ($streamObj->bottom_description) {
                            $str .= '<blockquote>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')) . '</blockquote>';
                        }
                        $str .= '</div>';
                        $str .= '</a>';
                        $str .= '</li>';
                        break;
                    case 'add.tag':
                    case 'assess.tag':
                    case 'describe.tag':
                        $str .= '<li class="stream-item">';
                        $str .= '<h6>';
                        $str .= '<i>';
                        $str .= '<svg viewBox="0 0 24.3 23" preserveAspectRatio="xMidYMid meet" class="star">';
                        $str .= '<use xlink:href="#star"></use>';
                        $str .= '</svg>';
                        $str .= '</i>';
                        $str .= '<span class="action-title">' . self::cropStr(strip_tags($streamObj->title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</span>';
                        $str .= '<p class="stream-title">' . self::cropStr(strip_tags($streamObj->bottom_title), plotGlobalConfig::getVar('childActivityTitleMaxSymbolsToShow')) . '</p>';
                        $str .= '</h6>';
                        $str .= '<data>' . JFactory::getDate($streamObj->created)->format('d.m.Y H:i') . '</data>';
                        $str .= '<div>';
                        $str .= '<figure>';
                        if ($streamObj->image_url) {
                            $str .= '<img src="' . $streamObj->image_url . '"/>';
                        }
                        $str .= '</figure>';
                        $str .= '</div>';
                        $str .= ' <div>';
                        if ($streamObj->bottom_description) {
                            $str .= '<blockquote>' . self::cropStr(strip_tags($streamObj->bottom_description), plotGlobalConfig::getVar('childActivityAllDescriptionMaxSymbolsToShow')) . '</blockquote>';
                        }
                        $str .= '</div>';
                        $str .= '</li>';
                        break;
                }

            }
        }
        return $str;
    }


    public static function renderDisqus($text)
    {
        $disqus_name = 'naplotu';


        $disqus_uid = 'com_plot_' . $text;
        $disqus_url = JURI::getInstance()->toString();

        $script = "
        jQuery(document).ready(function(){
		var disqus_shortname = '" . $disqus_name . "';
		var disqus_developer = 1;
		var disqus_identifier = '" . $disqus_uid . "';
		var disqus_url = '" . $disqus_url . "';
		(function() {
			var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		})();
});
		";

        $document = JFactory::getDocument();
        $document->addScriptDeclaration($script);

        /*$out = '
		<div id="disqus_thread"></div>
		<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>';
        return $out;*/
    }

    public static function getAllUsersIds(){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('u.id AS childId')
            ->from('`#__users` AS `u`')
            ->where('`u`.`id` != '.JFactory::getUser()->id);
        $db->setQuery($query);
        return $db->loadColumn();
    }


    public static function paidInfo($bookId){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`p`.child_id')
            ->from('`#__plot_books_paid` AS `p`')
            ->where('`p`.`book_id` = ' . (int)$bookId)
            ->where('`p`.`parent_id`='.(int)plotUser::factory()->id);

        $buyer = $db->setQuery($query)->loadResult();
        return $buyer;
    }


public static function returnBytes($val) {
        $val = trim($val);
        $last = strtolower($val[strlen($val)-1]);

        switch($last) {
            // The 'G' modifier is available since PHP 5.1.0
            case 'g':
                $val *= 1024;
            case 'm':
                $val *= 1024;
            case 'k':
                $val *= 1024;
        }


        return $val;
    }


}
