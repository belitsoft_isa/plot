function searchInputFocus() {
    if (jQuery('#plot-search-results').html().trim()) {
        jQuery('#plot-search-results').show();
    }
}

jQuery(document).on('click', function (event) {
    if (!jQuery(event.target).closest('header .header-profile-menu').length && !jQuery(event.target).closest('header .header-profile-menu-show-button').length) {
        jQuery('header .header-profile-menu').hide();
    }
    if (!jQuery(event.target).closest('form.search').length) {
        jQuery('#plot-search-results').hide();
    }
});

jQuery(document).ready(function(){
    jQuery('header .header-profile-menu-show-button').click(function () {
        if (jQuery('header .header-profile-menu').is(':visible')) {
            jQuery('header .header-profile-menu').hide();
        } else {
            jQuery('header .header-profile-menu').show();
        }
    });
    jQuery('#plot-search').keyup(function () {
        var search=jQuery(this).val();
        if (jQuery(this).val().length > 1) {
            jQuery.post('index.php?option=com_plot&task=search.ajaxSearch', {search:search }, function (data) {
                var str = '',
                    results = JSON.parse(data),
                    books = results.books,
                    books_link=results.books_link,
                    adults = results.adults,
                    adults_link=results.adults_link,
                    children = results.children,
                    children_link=results.children_link,
                    courses = results.courses,
                    courses_link=results.courses_link,
                    events = results.events,
                    empty_res=results.empty,
                    svg_path=results.svg,
                    events_link=results.events_link,
                    count_books = books.length,
                    count_adults = adults.length,
                    count_children = children.length,
                    count_courses = courses.length,
                    count_events=events.length;

                if (!count_books && !count_adults && !count_children && !count_courses && !count_events) {
                    str += "<ul><li class='no-results'>"+empty_res+"</li></div></ul>";
                } else {
                    jQuery('#plot-search-results').show();
                    str += '<ul>';
                    if (count_books) {
                        str += renderBooksList(count_books, books, books_link);

                    }
                    if (count_adults) {
                        str += renderAdultsList(count_adults, adults, adults_link);

                    }
                    if (count_children) {
                        str += renderChildrenList(count_children, children,children_link);

                    }
                    if(count_courses){
                        str += renderCoursesList(count_courses, courses,courses_link, svg_path);

                    }
                    if(count_events){
                        str += renderEventsList(count_events, events,events_link, svg_path);

                    }
                    str += '</ul>';
                }
                jQuery('#plot-search-results').html(str);
            });
        }
    });
    function renderBooksList(count, book, books_link)
    {
        var i;
        var str = '<li class="header">'+ books_link+'</li>';

        for (i = 0; i < count; i++) {
            str +=
                '<li class="main">' +
                '<a href="' + book[i].link + '">' +
                '<div class="image-book">' +
                '<img src="'+book[i].thumbnailUrl+'" />' +
                '</div>' +
                '<div class="title">' +
                book[i].c_title_croped +
                '<p> '+book[i].c_pub_descr+
                '</p>'+
                '</div>' +
                '</a>'+
                '</li>';
        }

        return str;
    }
    function renderAdultsList(count, adult, adults_link)
    {
        var i;
        var str = '<li class="header">'+adults_link+'</li>';

        for (i = 0; i < count; i++) {
            str +=
                '<li class="main">' +
                '<a href="' + adult[i].link + '">' +
                '<div class="circle-img">' +
                '<img src="'+adult[i].avatar+'" />' +
                '</div>' +
                '<div class="title">' +
                adult[i].name_croped +
                '<p> '+adult[i].about+
                '</p>'+
                '</div>' +
                '</a>' +
                '</li>';
        }

        return str;
    }
    function renderChildrenList(count, child,children_link)
    {
        var i;
        var str = '<li class="header">'+children_link+'</li>';

        for (i = 0; i < count; i++) {
            str +=
                '<li class="main">' +
                '<a href="' + child[i].link + '">' +
                '<div class="child-image">' +
                '<img src="'+child[i].avatar+'" />' +
                '</div>' +
                '<div class="title">' +
                child[i].name_croped +
                '<p> '+child[i].about +
                '</p>'+
                '</div>' +
                '</a>' +
                '</li>';
        }
        return str;
    }
    function renderCoursesList(count, course,courses_link, svg_path)
    {
        var i;
        var str = '<li class="header">'+courses_link+'</li>';

        for (i = 0; i < count; i++) {
            str +=
                '<li class="main">' +
                '<a href="' + course[i].link + '">' +
                '<div class="image-meetings">' +
                '<svg xml:base="'+svg_path+'" class="clip-svg-course">'+
                '<image style="clip-path: url(#clipping-circle-course-small)" width="100%" height="100%" xlink:href="'+course[i].img+'" />'+
                '</svg>'+
                '</div>' +
                '<div class="title">' +
                course[i].course_name_croped +
                '<p> '+course[i].course_description+
                '</p>'+
                '</div>' +
                '</a>' +
                '</li>';
        }
        return str;
    }
    function renderEventsList(count, event,events_link, svg_path)
    {
        var i;
        var str = '<li class="header">'+events_link+'</li>';
        for (i = 0; i < count; i++) {
            str +=
                '<li class="main">' +
                '<a href="' + event[i].link + '">' +
                '<div class="image-meetings">' +
                '<svg xml:base="'+svg_path+'" class="clip-svg">'+
                '<image style="clip-path: url(#clipping-circle-small)" width="100%" height="100%" xlink:href="'+event[i].img+'" />'+
                '</svg>'+
                '</div>' +
                '<div class="title">' +
                event[i].title_croped +
                '<p> '+event[i].description+
                '</p>'+
                '<div class="time">начало: ' + event[i].start_date + '</div>' +
                '<div class="time">конец: ' + event[i].end_date + '</div>' +
                '<span class="price"> 1200 руб'+
                '</span>'+
                '</div>' +
                '</a>' +
                '</li>';
        }
        return str;
    }


    jQuery('#plot-search').on('focus', function(){
        if (jQuery('#plot-search-results').html().trim()) {
            jQuery('#plot-search-results').show();
        }
    });

});
