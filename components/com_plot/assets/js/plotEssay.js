window.plotEssay = window.plotEssay || {};

plotEssay = {
    showElement: function(elId) {
        jQuery('#'+elId).show();
        return plotEssay;
    },
    hideElement:function(elId){
        jQuery('#'+elId).hide();
        return plotEssay;
    },
    essayValidate:function(elId, bookId,selector){
       var essay_text=jQuery('#'+elId).val(),
           data={'text':essay_text.trim(),'bookId':bookId};
        if(essay_text.trim()){
            plotEssay.sendEssay(data,selector);
            jQuery('#'+elId).val('');
            return plotEssay;
        }else{
            plotEssay.createMessage('Заполните поле');
            return plotEssay;
        }

    },
    createMessage:function(msg){
        SqueezeBox.initialize({
            size: {x: 300, y: 150}
        });
        var str = '<div id="enqueued-message">'+msg+'</div>';
        SqueezeBox.setContent('adopt', str);
        SqueezeBox.resize({x: 300, y: 150});
    },

    sendEssay:function(data1,selector){
        jQuery.post(
            'index.php?option=com_plot&task=essay.save',
            {data: data1},
            function (response) {
                console.log(response)
                if (response) {
                    var data = jQuery.parseJSON(response);
                    if(!data.status){
                        jQuery(selector).click();
                        plotEssay.createMessage(data.message);
                    }else{
                        jQuery(selector).click();
                        plotEssay.createMessage(data.message);
                    }
                    updateFancybox();
                } else {
                    plotEssay.createMessage('не удалось сохранить данные');
                }
            }
        );
    }



};

