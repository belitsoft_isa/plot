jQuery(document).ready(function () {

    window.App = window.App || {};

    App.Event = {};

    App.Validation = {};

    App.Words={};
    //cancel subscribe on event
    App.Event.RemoveSubscribe = function (that) {
        var event_id = jQuery(that).attr('data-id');

        jQuery.ajax({
            type: "POST",
            url: "index.php?option=com_plot&task=events.rejectEvent",
            data: {
                'event_id': event_id
            },
            success: function (response) {
                var element=document.getElementById("wrap-btn-event-"+event_id);
                if (response) {
                    jQuery(element).html('<button class="add hover-shadow" onclick="App.Event.Subscribe(this); return false;" data-id="'+event_id+'">Записаться</button>');

                } else {
                    alert('error');
                }
            }
        });
    },
        //subscribe to event
        App.Event.Subscribe = function (that) {
            var event_id = jQuery(that).attr('data-id');
            jQuery.ajax({
                type: "POST",
                url: "index.php?option=com_plot&task=events.sudscribeEvent",
                data: {
                    'event_id': event_id
                },
                success: function (response) {
                    if (response) {
                      jQuery("#wrap-btn-event-"+event_id).html('<button class="add hover-shadow" onclick="App.Event.RemoveSubscribe(this); return false;" data-id="'+event_id+'">Отменить подписку на событие</button>');
                    } else {
                        alert('error');
                    }
                }
            });
        },
        //remove event
        App.Event.DeleteEvent = function (that) {
            var event_id = jQuery(that).attr('data-id');
            jQuery('#wrap-btn-event-'+event_id).parent();
            console.log();
           jQuery.ajax({
                type: "POST",
                url: "index.php?option=com_plot&task=events.deleteEvent",
                data: {
                    'event_id': event_id
                },
                success: function (response) {
                    if (response) {
                        document.getElementById('wrap-btn-event-'+event_id).parentNode.remove()
                    } else {
                        alert('error');
                    }
                }
            });
        },

        App.Validation.IsEmpty = function (field_value) {
            var error = true;
            if (field_value == '') {
                error = false;
            }
            return error;
        },

        App.Words.Ending=function(number, titles){
            cases = [2, 0, 1, 1, 1, 2];
            return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
        }
});