<?php

defined('_JEXEC') or die('Restricted access');

class PlotModelSearch extends JModelList
{

    public function __construct($config = array())
    {

        parent::__construct($config);
    }

    public function getAllResults()
    {
        $db = $this->_db;
        $results = array();
        $jinput = JFactory::getApplication()->input;
        $Itemid = $jinput->get('Itemid', 0, 'INT');
        $search = urldecode(JRequest::getString('search', ''));
        $sqlSearchStr = $db->Quote('%' . $db->escape($search, true) . '%');
        //get books
        $query = $db->getQuery(true)
            ->clear()
            ->select('SQL_CALC_FOUND_ROWS `pub`.*')
            ->from('`#__html5fb_publication` AS `pub`')
            ->leftJoin('`#__html5fb_resolutions` AS `res` ON `pub`.`c_resolution_id` = `res`.`id`');
        $query->join('LEFT', '`#__html5fb_category` AS `c` ON `c`.`c_id` = `pub`.`c_category_id`');
        $query->leftJoin('`#__plot_tags` AS `t` ON `t`.`entityId` = `pub`.`c_id` AND `t`.`entity`="book"');
        $query->leftJoin('`#__plot_age_book_map` AS `a` ON `a`.`book_id` = `pub`.`c_id`');
        $query->innerJoin('`#__plot_book_cost` AS `bc` ON `bc`.`book_id` = `pub`.`c_id`');
        $query->where('`pub`.`c_title` LIKE ' . $sqlSearchStr);
        $query->leftJoin('`#__plot_books` AS `b` ON `b`.`book_id` = `pub`.`c_id`');

        $query->where('`pub`.published=1');

        $query->group('`pub`.`c_id`');
        $books = $this->_db->setQuery($query)->loadObjectList();
        $totalBooks = $this->_db->setQuery('SELECT FOUND_ROWS();')->loadResult();
        $results['countBooks'] = $totalBooks;

        foreach ($books AS $book) {
            $book->link = JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $book->c_id . '&Itemid=' . (int)$Itemid);
            $book->img = JURI::root() . 'media/com_html5flippingbook/thumbs/' . $book->c_thumb;
            $book->c_title_croped = PlotHelper::cropStr($book->c_title, plotGlobalConfig::getVar('searchTitleLimitChars'));
            $book->c_pub_descr = strip_tags($book->c_pub_descr);
            $book->c_pub_descr = (strlen($book->c_pub_descr) > 50) ? PlotHelper::cropStr($book->c_pub_descr, 0, 50)  : $book->c_pub_descr;
            $bookobj = new plotBook($book->c_id);
            $book->price = $bookobj->getBookMinCost();
        }

//get adults
        $query = $db->getQuery(true)
            ->clear()
            ->select('a.*');
        $query->from('`#__users` AS `a`')
            ->innerJoin('`#__social_profiles_maps` AS `pm` ON `pm`.`user_id` = `a`.`id` AND `pm`.profile_id=' . (int)plotGlobalConfig::getVar('parentProfileId'))
            ->where('`a`.`name` LIKE ' . $sqlSearchStr);
        $db->setQuery($query);
        $results['countAdults'] = count($db->loadObjectList());
        $adults = $db->loadObjectList();
        foreach ($adults AS $adult) {
            $adult->link = JRoute::_('index.php?option=com_plot&view=profile&id=' . $adult->id);
            if (plotUser::factory($adult->id)->avatars && plotUser::factory($adult->id)->avatars['medium'] && JFile::exists(plotUser::factory($adult->id)->avatars['medium'])) {
                $adult->avatar = JUri::root() . 'media/com_easysocial/avatars/users/' . (int)$adult->id . '/' . plotUser::factory($adult->id)->avatars['medium'];
            } else {
                $adult->avatar = JUri::root() . 'media/com_easysocial/defaults/avatars/users/medium.png';
            }
            $adult->name_croped = PlotHelper::cropStr($adult->name, plotGlobalConfig::getVar('searchTitleLimitChars'));
            $adult->about = (string)plotUser::factory($adult->id)->getSocialFieldData('ABOUT_ME');
        }

        //get children
        $query = $db->getQuery(true)
            ->clear()
            ->select('a.*');
        $query->from('`#__users` AS `a`')
            ->innerJoin('`#__social_profiles_maps` AS `pm` ON `pm`.`user_id` = `a`.`id` AND `pm`.profile_id=' . (int)plotGlobalConfig::getVar('childProfileId'))
            ->where('`a`.`name` LIKE ' . $sqlSearchStr);
        $db->setQuery($query);
        $results['countChildren'] = count($db->loadObjectList());
        $children = $db->loadObjectList();
        foreach ($children AS $child) {
            $child->link = JRoute::_('index.php?option=com_plot&view=profile&id=' . $child->id);
            if (plotUser::factory($child->id)->avatars && plotUser::factory($child->id)->avatars['medium'] && JFile::exists(plotUser::factory($child->id)->avatars['medium'])) {
                $child->avatar = JUri::root() . 'media/com_easysocial/avatars/users/' . (int)$child->id . '/' . plotUser::factory($child->id)->avatars['medium'];
            } else {
                $child->avatar = JUri::root() . 'media/com_easysocial/defaults/avatars/users/medium.png';
            }
            $child->name_croped = PlotHelper::cropStr($child->name, plotGlobalConfig::getVar('searchTitleLimitChars'));
            $child->about = (string)plotUser::factory($child->id)->getSocialFieldData('ABOUT_ME');

        }

//get courses


        /* $query = $db->getQuery(true)
             ->clear()
             ->select('`a`.*')
             ->from('`#__lms_courses` AS `a`')
             ->where('`a`.`course_name` LIKE ' . $sqlSearchStr)
             ->where('`a`.`published` = 1');

         $db->setQuery($query);
         $results['countCourses'] = count($db->loadObjectList());
         $courses = $db->loadObjectList();*/


        $query = "SELECT SQL_CALC_FOUND_ROWS DISTINCT `c`.*, `cats`.`c_category`, `pc`.*, (`pc`.`admin_min_cost` + `pc`.`author_min_cost`) AS `total_min_cost` FROM `#__lms_courses` AS `c` "
            . "LEFT JOIN `#__lms_course_cats` AS `cats` ON (`c`.`cat_id`=`cats`.`id`) "
            . "LEFT JOIN `#__plot_courses` AS `pc` ON (`c`.`id` = `pc`.`id`) "
            . "INNER JOIN `#__plot_age_course_map` AS `ages` ON (`ages`.`course_id` = `c`.`id` ) "
            . "INNER JOIN `#__plot_tags` AS `tags` ON (`tags`.`entity` = 'course' AND `tags`.`entityId` = `c`.`id` ) "
            . "WHERE `c`.`course_name` LIKE " . $sqlSearchStr
            . " AND `c`.`published` = 1";


        $courses = $this->_db->setQuery($query)->loadObjectList();

        $results['countCourses'] = $this->_db->setQuery('SELECT FOUND_ROWS()')->loadResult();

        foreach ($courses AS $course) {
            $course->link = JRoute::_('index.php?option=com_plot&view=course&id=' . $course->id . '&Itemid=' . $Itemid);
            $course->course_name_croped = PlotHelper::cropStr($course->course_name, plotGlobalConfig::getVar('searchTitleLimitChars'));
            $plotCourseObj = new plotCourse($course->id);
            $course->img = $plotCourseObj->image ? JUri::root() . $plotCourseObj->image : JUri::root() . 'templates/plot/img/blank150x150.jpg';
            $course->course_description = strip_tags($course->course_description) ? strip_tags($course->course_description) : 0;
            $course->course_description = PlotHelper::cropStr($course->course_description, 0, 100);
        }
//events
        $query = $db->getQuery(true)
            ->clear()
            ->select('a.*');
        $query->from('`#__plot_events` AS `a`')
            ->where('`a`.`title` LIKE ' . $sqlSearchStr);
        $db->setQuery($query);
        $results['countEvents'] = count($db->loadObjectList());
        $events = $db->loadObjectList();
        foreach ($events AS $event) {
            $event->link = JRoute::_('index.php?option=com_plot&view=event&id=' . $event->id . '&Itemid=' . $Itemid);
            $event->img = JUri::root() . 'images/com_plot/events/' . $event->user_id . '/' . $event->img ? JUri::root() . 'images/com_plot/events/' . $event->user_id . '/' . $event->img : JUri::root() . 'templates/plot/img/blank150x150.jpg';
            $event->start_date = JHtml::date($event->start_date, 'd-m-Y, H:m');
            $event->end_date = JHtml::date($event->end_date, 'd-m-Y, H:m');
            $event->place = (strlen($event->place) > 50) ? PlotHelper::cropStr($event->place, 0, 50)  : $event->place;
            $event->title_croped = PlotHelper::cropStr($event->title, plotGlobalConfig::getVar('searchTitleLimitChars'));
            $event->description = (strlen($event->description) > 170) ? PlotHelper::cropStr($event->description, 0, 170)  : $event->description;
        }

        $results['books'] = $books;
        $results['adults'] = $adults;
        $results['children'] = $children;
        $results['courses'] = $courses;
        $results['events'] = $events;

        return $results;
    }

    protected function getListQuery()
    {
        $db = $this->_db;
        $jinput = JFactory::getApplication()->input;
        $layout = $jinput->get('layout', 'books');
        $search = JRequest::getString('search', '');
        $sqlSearchStr = $db->Quote('%' . $db->escape($search, true) . '%');
        $query = $db->getQuery(true)
            ->clear()
            ->select('a.*');
        switch ($layout) {
            case 'books':

                $query = $db->getQuery(true)
                    ->clear()
                    ->select('SQL_CALC_FOUND_ROWS `a`.*')
                    ->from('`#__html5fb_publication` AS `pub`')
                    ->leftJoin('`#__html5fb_resolutions` AS `res` ON `pub`.`c_resolution_id` = `res`.`id`');
                $query->join('LEFT', '`#__html5fb_category` AS `c` ON `c`.`c_id` = `pub`.`c_category_id`');
                $query->leftJoin('`#__plot_tags` AS `t` ON `t`.`entityId` = `pub`.`c_id` AND `t`.`entity`="book"');
                $query->leftJoin('`#__plot_age_book_map` AS `a` ON `a`.`book_id` = `pub`.`c_id`');
                $query->innerJoin('`#__plot_book_cost` AS `bc` ON `bc`.`book_id` = `pub`.`c_id`');
                $query->where('`pub`.`c_title` LIKE ' . $sqlSearchStr);
                $query->leftJoin('`#__plot_books` AS `b` ON `b`.`book_id` = `pub`.`c_id`');
                $query->where('`pub`.published=1');
                $query->group('`pub`.`c_id`');
                /*
                $query->from('`#__html5fb_publication` AS `a`')

                    ->where('`a`.`c_title` LIKE ' . $sqlSearchStr)
                    ->where('`a`.`published`=1');*/
                // ->group('`a`.c_id');
                break;
            case 'adults':
                $query->from('`#__users` AS `a`')
                    ->innerJoin('`#__social_profiles_maps` AS `pm` ON `pm`.`user_id` = `a`.`id` AND `pm`.profile_id=' . (int)plotGlobalConfig::getVar('parentProfileId'))
                    ->where('`a`.`name` LIKE ' . $sqlSearchStr);
                break;
            case 'children':
                $query->from('`#__users` AS `a`')
                    ->innerJoin('`#__social_profiles_maps` AS `pm` ON `pm`.`user_id` = `a`.`id` AND `pm`.profile_id=' . (int)plotGlobalConfig::getVar('childProfileId'))
                    ->where('`a`.`name` LIKE ' . $sqlSearchStr);
                break;
            case 'courses':
                $query->from('`#__lms_courses` AS `a`')
                    ->where('`a`.`course_name` LIKE ' . $sqlSearchStr);
                break;
            case 'events':
                $query->from('`#__plot_events` AS `a`')
                    ->where('`a`.`title` LIKE ' . $sqlSearchStr);
                break;
        }

        return $query;
    }

    public function getLimitedBooks($search)
    {
        $search = $this->_db->quote('%' . $search . '%', true);
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`pub`.*')
            ->from('`#__html5fb_publication` AS `pub`')
            ->where('`pub`.`c_title` LIKE ' . $search)
            ->where('`pub`.`published`=1');

        $db->setQuery($query, 0, plotGlobalConfig::getVar('limitSearchItems'));

        $books = $db->loadObjectList();
        foreach ($books AS $row) {
            $thumbnailPath = JPATH_SITE . '/media/com_html5flippingbook' . '/thumbs/thimb_' . $row->c_thumb;
            if ($row->c_thumb == "" || !is_file($thumbnailPath)) {
                $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
            } else {
                if (file_exists(JPATH_BASE . '/media/com_html5flippingbook' . '/thumbs/thimb_' . $row->c_thumb)) {
                    $row->thumbnailUrl = JURI::root() . "media/com_html5flippingbook/thumbs/thimb_" . $row->c_thumb;
                } else {
                    $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
                }
            }
            $bookobj = new plotBook($row->c_id);
            $row->price = $bookobj->getBookMinCost();
            $row->c_title_croped = PlotHelper::cropStr($row->c_title, plotGlobalConfig::getVar('searchTitleLimitChars'));
            $row->c_pub_descr = PlotHelper::cropStr(strip_tags($row->c_pub_descr), plotGlobalConfig::getVar('quickDescrBookSearch'));
            $row->link = JRoute::_('index.php?option=com_plot&view=publication&id=' . $row->c_id);
        }
        return $books;
    }

    public function getBooks($search, $limit)
    {
        $Itemid = JFactory::getApplication()->input->get('Itemid', 0, 'INT');
        $search = $this->_db->quote('%' . $search . '%', true);
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`pub`.c_title,`pub`.c_id, `pub`.c_thumb, `pub`.c_pub_descr')
            ->from('`#__html5fb_publication` AS `pub`')
            ->where('`pub`.`c_title` LIKE ' . $search)
            ->where('`pub`.`published`=1');
        $db->setQuery($query, $limit['offset'], $limit['limit']);

        $books = $db->loadObjectList();
        foreach ($books AS $row) {
            $thumbnailPath = JPATH_SITE . '/media/com_html5flippingbook' . '/thumbs/' . $row->c_thumb;
            if ($row->c_thumb == "" || !is_file($thumbnailPath)) {
                $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
            } else {
                if (file_exists(JPATH_BASE . '/media/com_html5flippingbook' . '/thumbs/thimb_' . $row->c_thumb)) {
                    $row->thumbnailUrl = JURI::root() . "media/com_html5flippingbook/thumbs/thimb_" . $row->c_thumb;
                } else {
                    $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
                }

            }
            $bookobj = new plotBook($row->c_id);
            $row->price = $bookobj->getBookMinCost();
            $row->link = JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $row->c_id);


            $row->c_title_croped = PlotHelper::cropStr($row->c_title, plotGlobalConfig::getVar('searchTitleLimitChars'));
            $row->c_pub_descr = PlotHelper::cropStr(strip_tags($row->c_pub_descr), plotGlobalConfig::getVar('booksDescriptionMaxSymbolsToShow'));

        }

        return $books;

    }

    public function getLimitedAdult($search)
    {
        $Itemid = JFactory::getApplication()->input->get('Itemid', 0, 'INT');
        $search = $this->_db->quote('%' . $search . '%', true);
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`u`.name,`u`.username,`u`.id')
            ->from('`#__users` AS `u`')
            ->innerJoin('`#__social_profiles_maps` AS `pm` ON `pm`.`user_id` = `u`.`id` AND `pm`.profile_id=' . (int)plotGlobalConfig::getVar('parentProfileId'))
            ->where('`u`.`name` LIKE ' . $search);
        $db->setQuery($query, 0, plotGlobalConfig::getVar('limitSearchItems'));
        $adults = $db->loadObjectList();
        foreach ($adults AS $adult) {
            $adult->link = JRoute::_('index.php?option=com_plot&view=profile&id=' . $adult->id);
            if (plotUser::factory($adult->id)->avatars && plotUser::factory($adult->id)->avatars['large']) {
                $adult->avatar = JUri::root() . 'media/com_easysocial/avatars/users/' . (int)$adult->id . '/' . plotUser::factory($adult->id)->avatars['large'];
            } else {
                $adult->avatar = JUri::root() . 'media/com_easysocial/defaults/avatars/users/medium.png';
            }
            $adult->name_croped = PlotHelper::cropStr($adult->name, plotGlobalConfig::getVar('searchTitleLimitChars'));
            $adult->about = PlotHelper::cropStr((string)plotUser::factory($adult->id)->getSocialFieldData('ABOUT_ME'), plotGlobalConfig::getVar('limitSearchAdultsAbout'));
        }
        return $adults;
    }

    public function getAdults($search, $limit)
    {
        $Itemid = JFactory::getApplication()->input->get('Itemid', 0, 'INT');
        $search = $this->_db->quote('%' . $search . '%', true);
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`u`.name,`u`.username,`u`.id')
            ->from('`#__users` AS `u`')
            ->innerJoin('`#__social_profiles_maps` AS `pm` ON `pm`.`user_id` = `u`.`id` AND `pm`.profile_id=' . (int)plotGlobalConfig::getVar('parentProfileId'))
            ->where('`u`.`name` LIKE ' . $search);

        $db->setQuery($query, $limit['offset'], $limit['limit']);
        $adults = $db->loadObjectList();
        foreach ($adults AS $adult) {
            $adult->link = JRoute::_('index.php?option=com_plot&view=profile&id=' . $adult->id);
            if (plotUser::factory($adult->id)->avatars && plotUser::factory($adult->id)->avatars['large']) {
                $adult->avatar = JUri::root() . 'media/com_easysocial/avatars/users/' . (int)$adult->id . '/' . plotUser::factory($adult->id)->avatars['large'];
            } else {
                $adult->avatar = JUri::root() . 'media/com_easysocial/defaults/avatars/users/medium.png';
            }
            $adult->name_croped = PlotHelper::cropStr($adult->name, plotGlobalConfig::getVar('searchTitleLimitChars'));
            $adult->about = (string)plotUser::factory($adult->id)->getSocialFieldData('ABOUT_ME');
        }
        return $adults;

    }

    public function getLimitedChildren($search)
    {
        $Itemid = JFactory::getApplication()->input->get('Itemid', 0, 'INT');
        $search = $this->_db->quote('%' . $search . '%', true);
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`u`.name,`u`.username,`u`.id')
            ->from('`#__users` AS `u`')
            ->innerJoin('`#__social_profiles_maps` AS `pm` ON `pm`.`user_id` = `u`.`id` AND `pm`.profile_id=' . (int)plotGlobalConfig::getVar('childProfileId'))
            ->where('`u`.`name` LIKE ' . $search);

        $db->setQuery($query, 0, plotGlobalConfig::getVar('limitSearchItems'));
        $children = $db->loadObjectList();
        foreach ($children AS $child) {
            $child->link = JRoute::_('index.php?option=com_plot&view=profile&id=' . $child->id);

            if (plotUser::factory($child->id)->avatars && plotUser::factory($child->id)->avatars['large']) {
                $child->avatar = JUri::root() . 'media/com_easysocial/avatars/users/' . (int)$child->id . '/' . plotUser::factory($child->id)->avatars['large'];
            } else {
                $child->avatar = JUri::root() . 'media/com_easysocial/defaults/avatars/user/child_medium.jpg';
            }
            $child->name_croped = PlotHelper::cropStr($child->name, plotGlobalConfig::getVar('searchTitleLimitChars'));
            $child->about = PlotHelper::cropStr((string)plotUser::factory($child->id)->getSocialFieldData('ABOUT_ME'), plotGlobalConfig::getVar('limitSearchChildrenAbout'));
        }
        return $children;
    }

    public function getChildren($search, $limit)
    {
        $Itemid = JFactory::getApplication()->input->get('Itemid', 0, 'INT');
        $search = $this->_db->quote('%' . $search . '%', true);
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`u`.name,`u`.username,`u`.id')
            ->from('`#__users` AS `u`')
            ->innerJoin('`#__social_profiles_maps` AS `pm` ON `pm`.`user_id` = `u`.`id` AND `pm`.profile_id=' . (int)plotGlobalConfig::getVar('childProfileId'))
            ->where('`u`.`name` LIKE ' . $search);

        $db->setQuery($query, $limit['offset'], $limit['limit']);

        $children = $db->loadObjectList();
        foreach ($children AS $child) {
            $child->link = JRoute::_('index.php?option=com_plot&view=profile&id=' . $child->id);
            if (plotUser::factory($child->id)->avatars && plotUser::factory($child->id)->avatars['large']) {
                $child->avatar = JUri::root() . 'media/com_easysocial/avatars/users/' . (int)$child->id . '/' . plotUser::factory($child->id)->avatars['large'];
            } else {
                $child->avatar = JUri::root() . 'media/com_easysocial/defaults/avatars/user/child_medium.jpg';
            }
            $child->name_croped = PlotHelper::cropStr($child->name, plotGlobalConfig::getVar('searchTitleLimitChars'));
            $child->about = (string)plotUser::factory($child->id)->getSocialFieldData('ABOUT_ME');
        }


        return $children;

    }

    public function getLimitedCourses($search)
    {
        $Itemid = JFactory::getApplication()->input->get('Itemid', 0, 'INT');
        $search = $this->_db->quote('%' . $search . '%', true);
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('DISTINCT `c`.*')
            ->from('`#__lms_courses` AS `c`')
            ->innerJoin('`#__plot_age_course_map` AS `ages` ON (`ages`.`course_id` = `c`.`id` )')
            ->innerJoin('`#__plot_tags` AS `tags` ON (`tags`.`entity` = "course" AND `tags`.`entityId` = `c`.`id`) ')
            ->where('`c`.`course_name` LIKE ' . $search)
            ->where('`c`.`published` = 1');

        $db->setQuery($query, 0, plotGlobalConfig::getVar('limitSearchItems'));
        $courses = $db->loadObjectList();
        foreach ($courses AS $course) {
            $course->link = JRoute::_('index.php?option=com_plot&view=course&id=' . $course->id . '&Itemid=' . $Itemid);
            $course->course_name_croped = PlotHelper::cropStr($course->course_name, plotGlobalConfig::getVar('searchTitleLimitChars'));
            $plotCourseObj = new plotCourse($course->id);
            $course->img = $plotCourseObj->image ? JUri::root() . $plotCourseObj->image : JUri::root() . 'templates/plot/img/blank150x150.jpg';
            $course->course_description = strip_tags($course->course_description);
            $course->course_description = PlotHelper::cropStr($course->course_description, plotGlobalConfig::getVar('coursesDescriptionMaxSymbolsToShow'));
        }

        return $courses;
    }

    public function getCourses($search, $limit)
    {
        $Itemid = JFactory::getApplication()->input->get('Itemid', 0, 'INT');
        $search = $this->_db->quote('%' . $search . '%', true);
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('DISTINCT `c`.* ')
            ->from('`#__lms_courses` AS `c`')
            ->innerJoin('`#__plot_age_course_map` AS `ages` ON (`ages`.`course_id` = `c`.`id` )')
            ->innerJoin('`#__plot_tags` AS `tags` ON (`tags`.`entity` = "course" AND `tags`.`entityId` = `c`.`id`) ')
            ->where('`c`.`course_name` LIKE ' . $search)
            ->where('`c`.`published` = 1');

        $db->setQuery($query, $limit['offset'], $limit['limit']);

        $courses = $db->loadObjectList();

        foreach ($courses AS $course) {
            $course->link = JRoute::_('index.php?option=com_plot&view=course&id=' . $course->id . '&Itemid=' . $Itemid);
            $course->course_name_croped = PlotHelper::cropStr($course->course_name, plotGlobalConfig::getVar('searchTitleLimitChars'));
            $plotCourseObj = new plotCourse($course->id);
            $course->img = $plotCourseObj->image ? JUri::root() . $plotCourseObj->image : JUri::root() . 'templates/plot/img/blank150x150.jpg';
            $course->course_description = strip_tags($course->course_description);
            $course->course_description = (strlen($course->course_description) > 100) ? PlotHelper::cropStr($course->course_description, 0, 100)  : $course->course_description;
        }

        return $courses;

    }

    public function getLimitedEvents($search)
    {
        $Itemid = JFactory::getApplication()->input->get('Itemid', 0, 'INT');
        $search = $this->_db->quote('%' . $search . '%', true);
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`e`.*')
            ->from('`#__plot_events` AS `e`')
            ->where('`e`.`title` LIKE ' . $search);
        if (plotUser::factory()->isParent()) {
            $db->setQuery($query, 0, plotGlobalConfig::getVar('limitSearchItems'));
        } else {
            $db->setQuery($query, 0, plotGlobalConfig::getVar('limitSearchItems'));
        }

        $events = $db->loadObjectList();
        foreach ($events AS $event) {
            $event->link = JRoute::_('index.php?option=com_plot&view=event&id=' . $event->id . '&Itemid=' . $Itemid);
            if (file_exists(JPATH_SITE . '/images/com_plot/events/' . $event->user_id . '/thumb/' . $event->img)) {
                $event->img = JUri::root() . 'images/com_plot/events/' . $event->user_id . '/thumb/' . $event->img;

            } elseif (file_exists(JPATH_SITE . '/images/com_plot/events/' . $event->user_id . '/' . $event->img)) {
                $event->img = JUri::root() . 'images/com_plot/events/' . $event->user_id . '/' . $event->img;

            } else {
                $event->img = JUri::root() . 'images/com_plot/def_meeting.jpg';
            }
            $event->start_date = JHtml::date($event->start_date, 'd-m-Y, H:m');
            $event->end_date = JHtml::date($event->end_date, 'd-m-Y, H:m');
            $event->place = (strlen($event->place) > 50) ? PlotHelper::cropStr($event->place, 0, 50)  : $event->place;
            $event->title_croped = PlotHelper::cropStr($event->title, plotGlobalConfig::getVar('searchTitleLimitChars'));
            $event->description = (strlen($event->description) > 170) ? PlotHelper::cropStr($event->description, 0, 170)  : $event->description;
        }
        return $events;
    }

    public function getEvents($search, $limit)
    {
        $Itemid = JFactory::getApplication()->input->get('Itemid', 0, 'INT');
        $search = $this->_db->quote('%' . $search . '%', true);
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`e`.*')
            ->from('`#__plot_events` AS `e`')
            ->where('`e`.`title` LIKE ' . $search);
        $db->setQuery($query, $limit['offset'], $limit['limit']);

        $events = $db->loadObjectList();

        foreach ($events AS $event) {
            $event->link = JRoute::_('index.php?option=com_plot&view=event&id=' . $event->id . '&Itemid=' . $Itemid);
            if (file_exists(JPATH_SITE . '/images/com_plot/events/' . $event->user_id . '/thumb/' . $event->img)) {
                $event->img = JUri::root() . 'images/com_plot/events/' . $event->user_id . '/thumb/' . $event->img;

            } elseif (file_exists(JPATH_SITE . '/images/com_plot/events/' . $event->user_id . '/' . $event->img)) {
                $event->img = JUri::root() . 'images/com_plot/events/' . $event->user_id . '/' . $event->img;

            } else {
                $event->img = JUri::root() . 'images/com_plot/def_meeting.jpg';
            }

            $event->start_date = JHtml::date($event->start_date, 'd-m-Y, H:m');
            $event->end_date = JHtml::date($event->end_date, 'd-m-Y, H:m');
            $event->description = (strlen($event->description) > 170) ? PlotHelper::cropStr($event->description, 0, 170)  : $event->description;
            $event->title_croped = PlotHelper::cropStr($event->title, plotGlobalConfig::getVar('searchTitleLimitChars'));
            $event->place = (strlen($event->place) > 50) ? PlotHelper::cropStr($event->place, 0, 50)  : $event->place;

        }
        return $events;

    }

}
