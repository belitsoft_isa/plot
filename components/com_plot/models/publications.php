<?php defined('_JEXEC') or die('Restricted access');


class PlotModelPublications extends JModelList
{

    protected $user_id;
    protected $_read_list = FALSE;
    protected $_fav_list = FALSE;
    protected $_lastOpen = FALSE;
    public $_startF = 0;
    public $_startR = 0;
    public $_limitF = 3;
    public $_limitR = 3;
    public $_bookshelf = TRUE;
    public $tags = array();
    public $ages = array();


    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'pub.c_title',
                'pub.c_created_time',
            );
        }
        $this->user_id = JFactory::getUser()->get('id');
        $this->getUserStateFromRequest('tags', 1);

        parent::__construct($config);
    }

    protected function populateState($ordering = null, $direction = null)
    {
        // List state information
        $this->setState('list.reading.limit', $this->_limitR);
        $this->setState('list.reading.start', $this->_startR);

        $this->setState('list.favorite.limit', $this->_limitF);
        $this->setState('list.favorite.start', $this->_startF);
        $categoryId = $this->getUserStateFromRequest($this->context . '.filter.category_id', 'filter_category_id', '');
        $this->setState('filter.category_id', $categoryId);
        $this->setState('tags', $this->tags);
        $this->setState('ages', $this->ages);
        if (JFactory::getApplication()->input->get('sortTable', '', 'STRING')) {
            $this->setState('sort', JFactory::getApplication()->input->get('sortTable', '', 'STRING'));
        }
        // parent::populateState('pub.c_id', 'asc');
    }

    protected function getListQuery()
    {
        $db = $this->_db;
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }
        $query = $db->getQuery(true)
            ->clear()
            ->select('`pub`.*')
            ->select('`t`.*')
            ->select($user->id . ' AS `uid`');
        $query->select('`res`.`width`, `res`.`height`')
            ->from('`#__html5fb_publication` AS `pub`')
            ->leftJoin('`#__html5fb_resolutions` AS `res` ON `pub`.`c_resolution_id` = `res`.`id`');
        $query->join('LEFT', '`#__html5fb_category` AS `c` ON `c`.`c_id` = `pub`.`c_category_id`');
        $query->leftJoin('`#__plot_tags` AS `t` ON `t`.`entityId` = `pub`.`c_id` AND `t`.`entity`="book"');
        $query->leftJoin('`#__plot_age_book_map` AS `a` ON `a`.`book_id` = `pub`.`c_id`');
        $query->leftJoin('`#__plot_books_paid` AS `b` ON `b`.`book_id` = `pub`.`c_id`');

        $query->where('`pub`.published=1');
        $categoryId = $this->getState('filter.category_id');

        if (is_numeric($categoryId) && $categoryId != 0) {
            $query->where('`pub`.`c_category_id` = ' . (int)$categoryId);
        }
        $sortTable = $jinput->get('sortTable', 'pub.c_title asc');
        if ($sortTable) {
            $this->setState('list.ordering', $sortTable);
        }

        $tags = $jinput->get('tags', array(), 'ARRAY');

        if ($tags) {
            $this->tags = $tags;
            $this->setState('tags', $tags);
            $str = implode(',', $tags);
            $query->where('`t`.`tagId` IN (' . $str . ')');
        }

        $ages = $jinput->get('ages', array(), 'ARRAY');

        if ($ages) {
            $this->ages = $ages;
            $this->setState('ages', $ages);
            $str_a = implode(',', $ages);
            if ($str_a != '') {
                $query->where('`a`.`age_id` IN (' . $str_a . ')');
            }
        }

        $query->group('`pub`.`c_id`');
        if ($jinput->get('sortTable', '', 'STRING')) {
            $query->order($db->escape($jinput->get('sortTable', '', 'STRING')));
        }
//die(var_dump($query->__toString()));
        return $query;
    }

    protected function getTotalRows($list)
    {
        if ($list == 'reading') {
            $this->_read_list = TRUE;
            $this->_fav_list = FALSE;
            $this->_lastOpen = FALSE;
        } elseif ($list == 'favorite') {
            $this->_fav_list = TRUE;
            $this->_read_list = FALSE;
            $this->_lastOpen = FALSE;
        }

        $query = $this->getListQuery();
        $this->_db->setQuery($query);

        return count($this->_db->loadObjectList());
    }

    /**
     * Method to get the starting number of items for the data set.
     *
     * @param   string   Tab name where publication should display
     * @return  integer  The starting number of items available in the data set.
     *
     * @since   12.2
     */
    public function getPageStart($list)
    {
        $start = $this->getState('list.' . $list . '.start');
        $limit = $this->getState('list.' . $list . '.limit');
        $total = $this->getTotalRows($list);

        if ($start > $total - $limit) {
            $start = max(0, (int)(ceil($total / $limit) - 1) * $limit);
        }

        return $start;
    }

    /**
     * Method to get a JPagination object for the data set.
     *
     * @return  JPagination  A JPagination object for the data set.
     *
     * @since   12.2
     */
    public function getReadingPagination()
    {
        // Create the pagination object.
        $limit = (int)$this->getState('list.reading.limit') - (int)$this->getState('list.reading.links');
        $page = new JPagination($this->getTotalRows('reading'), $this->getPageStart('reading'), $limit, 'reading');

        return $page;
    }

    /**
     * Method to get a JPagination object for the data set.
     *
     * @return  JPagination  A JPagination object for the data set.
     *
     * @since   12.2
     */
    public function getFavoritePagination()
    {
        // Create the pagination object.
        $limit = (int)$this->getState('list.favorite.limit') - (int)$this->getState('list.favorite.links');
        $page = new JPagination($this->getTotalRows('favorite'), $this->getPageStart('favorite'), $limit, 'favorite');

        return $page;
    }

    public function getCategories()
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('`pub`.c_id AS value, `pub`.c_category AS text')
            ->from('`#__html5fb_category` AS `pub`')
            ->where('`pub`.c_id!=' . (int)plotGlobalConfig::getVar("courseBookCategoryId"));
        $db->setQuery($query);

        return $cats = $db->loadObjectList();
    }

    public function getMyTags()
    {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('`i`.id, `i`.title')
            ->from('`#__k2_items` AS `i`')
            ->innerJoin('`#__plot_tags` AS `t` ON `i`.id=`t`.tagId')
            ->where('`t`.entity="user"')
            ->where('`t`.entityId=' . (int)$user->id);
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    public function getMyChildrenTags()
    {

        $my = plotUser::factory();
        $children = array();
        $chisldren_ids = $my->getChildrenIds();
        $db = $this->_db;
        foreach ($chisldren_ids AS $key => $value) {
            $query = $db->getQuery(true)
                ->clear()
                ->select('`i`.id, `i`.title')
                ->from('`#__k2_items` AS `i`')
                ->innerJoin('`#__plot_tags` AS `t` ON `i`.id=`t`.tagId')
                ->where('`t`.entity="user"')
                ->where('`t`.entityId=' . (int)$value);
            $db->setQuery($query);
            $children[$key]['tags'] = $db->loadObjectList();
            $children[$key]['id'] = $value;
            if ($this->getBirthday((int)$value)) {
                $years = PlotHelper::countYears($this->getBirthday((int)$value));

            } else {
                $years = 0;
                $children[$key]['age'] = 0;
            }
            if ($years != 0) {

                $query = $db->getQuery(true)
                    ->select('`a`.`id`')
                    ->from('`#__plot_ages` AS `a`')
                    ->where('`a`.`from`<=' . $years . ' AND `a`.to>=' . $years);
                $db->setQuery($query);
                $children[$key]['age'] = (int)$db->loadResult();

            }


        }


        return $children;
    }

    public function getBirthday($id)
    {
        $db = Foundry::db();
        $user = Foundry::user($id);


        $query = $db->getQuery(true)
            ->select('`fd`.data')
            ->from('`#__social_fields_data` AS `fd`')
            ->innerJoin('`#__social_fields` AS `f` ON `f`.`id` = `fd`.`field_id`')
            ->where('`fd`.uid = ' . (int)$user->id)
            ->where('`f`.unique_key = "BIRTHDAY"');

        $db->setQuery($query);
        $birth = $db->loadResult();
        if ($birth) {
            $bithday = json_decode($birth);
        } else {
            $bithday = '';
        }


        return $bithday;
    }

    public function getCountPublications($categoryId = 0, $sortTable = '`pub`.c_title ASC', $ages = array(), $tags = array(0), $limit = array(), $my_books = 0)
    {
        $db = $this->_db;
        $user = Foundry::user();

        $query = $db->getQuery(true)
            ->clear()
            ->select('`pub`.c_id')
            ->from('`#__html5fb_publication` AS `pub`')
            ->leftJoin('`#__html5fb_resolutions` AS `res` ON `pub`.`c_resolution_id` = `res`.`id`');
        $query->join('LEFT', '`#__html5fb_category` AS `c` ON `c`.`c_id` = `pub`.`c_category_id`');
        $query->leftJoin('`#__plot_tags` AS `t` ON `t`.`entityId` = `pub`.`c_id` AND `t`.`entity`="book"');
        $query->leftJoin('`#__plot_age_book_map` AS `a` ON `a`.`book_id` = `pub`.`c_id`');
        if ($my_books) {
            $query->innerJoin('`#__plot_books_paid` AS `b` ON `b`.`book_id` = `pub`.`c_id`');
            $query->where('`b`.`child_id` = ' . (int)$user->id);
        } else {
            $query->leftJoin('`#__plot_books_paid` AS `b` ON `b`.`book_id` = `pub`.`c_id`');
        }

        $query->where('`pub`.published=1');

        if (is_numeric($categoryId) && $categoryId != 0) {
            $query->where('`pub`.`c_category_id` = ' . (int)$categoryId);
        }

        if ($sortTable) {
            $this->setState('list.ordering', $sortTable);
        }

        if ($tags) {
            $this->tags = $tags;
            $this->setState('tags', $tags);
            $str = implode(',', $tags);
            $query->where('`t`.`tagId` IN (' . $str . ')');
        }

        if ($ages) {
            $this->ages = $ages;
            $this->setState('ages', $ages);
            $str_a = implode(',', $ages);
            if ($str_a != '') {
                $query->where('`a`.`age_id` IN (' . $str_a . ')');
            }
        }
        $query->group('`pub`.`c_id`');
        $books = $this->_db->setQuery($query)->loadObjectList();

        return count($books);
    }

    public function getPublications($categoryId = 0, $sortTable = '`pub`.c_title ASC', $ages = array(), $tags = array(0), $limit = array(), $my_books = 0, $userId = 0)
    {
        $data = array();
        $db = $this->_db;
        $jinput = JFactory::getApplication()->input;

        $user = Foundry::user();

        $query = $db->getQuery(true)
            ->clear()
            ->select('SQL_CALC_FOUND_ROWS `pub`.*')
            ->select('`t`.*')
            ->select($user->id . ' AS `uid`');
        $query->select('`res`.`width`, `res`.`height`')
            ->select('SUM(bc.author_max+bc.admin_max) AS book_cost')
            ->from('`#__html5fb_publication` AS `pub`')
            ->leftJoin('`#__html5fb_resolutions` AS `res` ON `pub`.`c_resolution_id` = `res`.`id`');
        $query->join('LEFT', '`#__html5fb_category` AS `c` ON `c`.`c_id` = `pub`.`c_category_id`');
        $query->leftJoin('`#__plot_tags` AS `t` ON `t`.`entityId` = `pub`.`c_id` AND `t`.`entity`="book"');
        $query->leftJoin('`#__plot_age_book_map` AS `a` ON `a`.`book_id` = `pub`.`c_id`');
        $query->innerJoin('`#__plot_book_cost` AS `bc` ON `bc`.`book_id` = `pub`.`c_id`');
        if ($my_books) {
            $query->innerJoin('`#__plot_books_paid` AS `b` ON `b`.`book_id` = `pub`.`c_id`');
            $query->where('`b`.`child_id` = ' . (int)$user->id);
        } else {
            $query->leftJoin('`#__plot_books_paid` AS `b` ON `b`.`book_id` = `pub`.`c_id`');
        }
        if ($userId) {
            $query->where('`b`.`child_id` = ' . (int)$userId);
        }

        $query->where('`pub`.published=1');

        if (is_numeric($categoryId) && $categoryId != 0) {
            $query->where('`pub`.`c_category_id` = ' . (int)$categoryId);
        }

        if ($sortTable) {
            $this->setState('list.ordering', $sortTable);
        }

        if ($tags) {
            $this->tags = $tags;
            $this->setState('tags', $tags);
            $str = implode(',', $tags);
            $query->where('`t`.`tagId` IN (' . $str . ')');
        }

        if ($ages) {
            $this->ages = $ages;
            $this->setState('ages', $ages);
            $str_a = implode(',', $ages);
            if ($str_a != '') {
                $query->where('`a`.`age_id` IN (' . $str_a . ')');
            }
        }
        $query->group('`pub`.`c_id`');
        if ($sortTable) {
            $query->order($db->escape($sortTable));
        }

        if (isset($limit['offset']) && isset($limit['limit'])) {
            $books = $this->_db->setQuery($query, $limit['offset'], $limit['limit'])->loadObjectList();
        } else {
            $books = $this->_db->setQuery($query)->loadObjectList();
        }

        $totalBooks = $this->_db->setQuery('SELECT FOUND_ROWS();')->loadResult();

        if ($books) {
            foreach ($books AS $row) {
                $thumbnailPath = JPATH_SITE . '/media/com_html5flippingbook' . '/thumbs/' . $row->c_thumb;
                if ($row->c_thumb == "" || !is_file($thumbnailPath)) {
                    $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
                } else {
                    if (file_exists(JPATH_BASE . '/media/com_html5flippingbook' . '/thumbs/thimb_' . $row->c_thumb)) {
                        $row->thumbnailUrl = JURI::root() . "media/com_html5flippingbook/thumbs/thimb_" . $row->c_thumb;
                    } else {
                        $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
                    }
                }
                $bookobj = new plotBook($row->c_id);
                $row->price = $bookobj->getBookMinCost();
            }
        }

        $data['items'] = $books;
        $data['countItems'] = $totalBooks;

        return $data;
    }

    public function getNewBooksCount($userId)
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear();
        $query->select('COUNT(`pub`.`c_id`)')
            ->from('`#__html5fb_publication` AS `pub`')
            ->leftJoin('`#__html5fb_resolutions` AS `res` ON `pub`.`c_resolution_id` = `res`.`id`');
        $query->innerJoin('`#__plot_books` AS `upub` ON `upub`.`book_id` = `pub`.`c_id`')
            ->where('`upub`.`child_id` = ' . (int)$userId)
            ->where('`upub`.`read` = -1');

        $query->where('`pub`.published=1');
        $db->setQuery($query);

        $new_books = $db->loadResult();

        return $new_books;
    }

    public function getBeforeHashtagsPublications($limit = array(), $bookId)
    {
        $data = array();
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('hashtag_id')
            ->from('`#__plot_before_hashtag_book_map`')
            ->where('book_id=' . (int)$bookId);

        $tags = $this->_db->setQuery($query)->loadColumn();
        $tags_str = implode(',', $tags);

        $books = $this->_db->setQuery($query)->loadObjectList();

        $query = $db->getQuery(true)
            ->clear()
            ->select('SQL_CALC_FOUND_ROWS `pub`.*')
            ->select('`t`.*');

        $query->select('`res`.`width`, `res`.`height`')
            ->from('`#__html5fb_publication` AS `pub`')
            ->leftJoin('`#__html5fb_resolutions` AS `res` ON `pub`.`c_resolution_id` = `res`.`id`');
        $query->join('LEFT', '`#__html5fb_category` AS `c` ON `c`.`c_id` = `pub`.`c_category_id`');
        $query->leftJoin('`#__plot_hashtag_book_map` AS `t` ON `t`.`book_id` = `pub`.`c_id`');
        $query->where('`pub`.published=1')
            ->where('t.book_id!=' . (int)$bookId)
            ->where('t.hashtag_id IN("' . $tags_str . '")')
            ->group('book_id');

        if (isset($limit['offset']) && isset($limit['limit'])) {
            $books = $this->_db->setQuery($query, $limit['offset'], $limit['limit'])->loadObjectList();
        } else {
            $books = $this->_db->setQuery($query)->loadObjectList();
        }
        if ($books) {
            foreach ($books AS $row) {
                $thumbnailPath = JPATH_SITE . '/media/com_html5flippingbook' . '/thumbs/' . $row->c_thumb;
                if ($row->c_thumb == "" || !is_file($thumbnailPath)) {
                    $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
                } else {
                    if (file_exists(JPATH_BASE . '/media/com_html5flippingbook' . '/thumbs/thimb_' . $row->c_thumb)) {
                        $row->thumbnailUrl = JURI::root() . "media/com_html5flippingbook/thumbs/thimb_" . $row->c_thumb;
                    } else {
                        $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
                    }
                }
                $bookobj = new plotBook($row->c_id);
                $row->price = $bookobj->getBookMinCost();
            }
        }

        $data['items'] = $books;
        return $data;
    }

    public function getAfterHashtagsPublications($limit = array(), $bookId)
    {
        $data = array();
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('hashtag_id')
            ->from('`#__plot_after_hashtag_book_map`')
            ->where('book_id=' . (int)$bookId);

        $tags = $this->_db->setQuery($query)->loadColumn();
        $tags_str = implode(',', $tags);

        $books = $this->_db->setQuery($query)->loadObjectList();

        $query = $db->getQuery(true)
            ->clear()
            ->select('SQL_CALC_FOUND_ROWS `pub`.*')
            ->select('`t`.*');

        $query->select('`res`.`width`, `res`.`height`')
            ->from('`#__html5fb_publication` AS `pub`')
            ->leftJoin('`#__html5fb_resolutions` AS `res` ON `pub`.`c_resolution_id` = `res`.`id`');
        $query->join('LEFT', '`#__html5fb_category` AS `c` ON `c`.`c_id` = `pub`.`c_category_id`');
        $query->leftJoin('`#__plot_hashtag_book_map` AS `t` ON `t`.`book_id` = `pub`.`c_id`');
        $query->where('`pub`.published=1')
            ->where('t.book_id!=' . (int)$bookId)
            ->where('t.hashtag_id IN("' . $tags_str . '")')
            ->group('book_id');

        if (isset($limit['offset']) && isset($limit['limit'])) {
            $books = $this->_db->setQuery($query, $limit['offset'], $limit['limit'])->loadObjectList();
        } else {
            $books = $this->_db->setQuery($query)->loadObjectList();
        }
        if ($books) {
            foreach ($books AS $row) {
                $thumbnailPath = JPATH_SITE . '/media/com_html5flippingbook' . '/thumbs/' . $row->c_thumb;
                if ($row->c_thumb == "" || !is_file($thumbnailPath)) {
                    $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
                } else {
                    if (file_exists(JPATH_BASE . '/media/com_html5flippingbook' . '/thumbs/thimb_' . $row->c_thumb)) {
                        $row->thumbnailUrl = JURI::root() . "media/com_html5flippingbook/thumbs/thimb_" . $row->c_thumb;
                    } else {
                        $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
                    }
                }
                $bookobj = new plotBook($row->c_id);
                $row->price = $bookobj->getBookMinCost();
            }
        }

        $data['items'] = $books;
        return $data;
    }


    /**
     * returns count all published publications
     *
     * @access    public
     * @return int
     */
    public function getCountAllPublications()
    {
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('SQL_CALC_FOUND_ROWS `pub`.*')

            ->from('`#__html5fb_publication` AS `pub`')
            ->leftJoin('`#__html5fb_resolutions` AS `res` ON `pub`.`c_resolution_id` = `res`.`id`');
        $query->join('LEFT', '`#__html5fb_category` AS `c` ON `c`.`c_id` = `pub`.`c_category_id`');
        $query->leftJoin('`#__plot_tags` AS `t` ON `t`.`entityId` = `pub`.`c_id` AND `t`.`entity`="book"');
        $query->leftJoin('`#__plot_age_book_map` AS `a` ON `a`.`book_id` = `pub`.`c_id`');
        $query->innerJoin('`#__plot_book_cost` AS `bc` ON `bc`.`book_id` = `pub`.`c_id`');
        $query->leftJoin('`#__plot_books_paid` AS `b` ON `b`.`book_id` = `pub`.`c_id`');
        $query->where('`pub`.published=1');
        $query->group('`pub`.`c_id`');
        $books = $this->_db->setQuery($query)->loadObjectList();
        $totalBooks = $this->_db->setQuery('SELECT FOUND_ROWS();')->loadResult();
        return (int)$totalBooks;
    }

    public function getChildStudiedBooksByYears($childId)
    {
        $db = JFactory::getDbo();
        $child = plotUser::factory($childId);

        $childBirthday = $child->getSocialFieldData('BIRTHDAY');
        if (!isset($childBirthday->day) || !isset($childBirthday->month)) {
            $childBirthday = new stdClass();
            $childBirthday->day = '01';
            $childBirthday->month = '01';
            $childBirthday->year = '1900';
        }
        $childBirthdayDate = JFactory::getDate($childBirthday->year . '-' . $childBirthday->month . '-' . $childBirthday->day);

        $query = "SELECT p.`book_id`, e.`date` AS finished_date "
            . "FROM `#__plot_books_paid` AS p "
            ." INNER JOIN #__plot_essay AS e ON e.book_id=p.book_id "
            . "WHERE e.`user_id`=" . (int)$childId . " "
            . "AND e.`status`=1 "
            ." GROUP BY p.`book_id` "
            . "ORDER BY e.`date` DESC";

        $myFinishedBooks = $db->setQuery($query)->loadObjectList();

        # sort by my yearsOld
        $myFinishedBooksByDate = array();
        foreach ($myFinishedBooks AS $book) {

            $bookFinishedDate = JFactory::getDate($book->finished_date);
            $dateDiff = $bookFinishedDate->diff($childBirthdayDate);

            if ($dateDiff->invert) {
                $bookObj = new plotBook($book->book_id);
                $bookObj->finished_date = $book->finished_date;
                $bookObj->thumbnailUrl=JPATH_SITE . '/media/com_html5flippingbook' . '/thumbs/' . $bookObj->c_thumb;
                $thumbnailPath = JPATH_SITE . '/media/com_html5flippingbook' . '/thumbs/' . $bookObj->c_thumb;
                if ($bookObj->c_thumb == "" || !is_file($thumbnailPath)) {
                    $bookObj->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
                } else {
                    if (file_exists(JPATH_BASE . '/media/com_html5flippingbook' . '/thumbs/thimb_' . $bookObj->c_thumb)) {
                        $bookObj->thumbnailUrl = JURI::root() . "media/com_html5flippingbook/thumbs/thimb_" . $bookObj->c_thumb;
                    } else {
                        $bookObj->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
                    }
                }

                $myFinishedBooksByDate[$dateDiff->y][] = $bookObj;
            }
        }

        return $myFinishedBooksByDate;
    }


}