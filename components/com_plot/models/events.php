<?php

defined('_JEXEC') or die('Restricted access');

class PlotModelEvents extends JModelList
{

    public $tags = array();
    public $ages = array();

    public function __construct($config = array())
    {


        parent::__construct($config);
    }

    protected function populateState($ordering = null, $direction = null)
    {
        $status = $this->getUserStateFromRequest($this->context . '.filter.status', 'filter_status', '');
        $this->setState('filter.status', $status);

        parent::populateState('a.start_date', 'desc');
    }

    protected function getListQuery()
    {
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('a.*, ue.uid ')
            ->from('`#__plot_events` AS `a`')
            ->leftJoin('#__plot_user_event_map AS ue ON `a`.id=`ue`.event_id');

        // Filter by search in name.
        $status = $this->getState('filter.status');

        if ($status) {

            switch ($status) {
                case "current":
                    $query->where('a.end_date>=NOW()');
                    break;
                case "old":
                    $query->where('a.end_date<NOW()');
                    break;
            }
        }

        $query->order($db->escape($this->state->get('list.ordering') . ' ' . $this->state->get('list.direction')));
        return $query;
    }

    public function getUserEvents($id)
    {
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('e.*')
            ->from('`#__plot_events` AS `e`')
            ->innerJoin('`#__plot_user_event_map` AS `em` ON em.event_id=e.id')
            ->where('`e`.user_id=' . (int)$user->id . ' OR `em`.uid=' . (int)$user->id)
            ->where('e.end_date>=NOW()')
            ->group('`e`.`id`')
            ->order('`e`.`create_date` DESC');

        $db->setQuery($query);

        return $db->loadObjectList();
    }

    public function rejectEvent($event_id)
    {
        $db = $this->_db;
        $query = $db->getQuery(true);
        $query->delete('#__plot_user_event_map')
            ->where('event_id=' . (int)$event_id)
            ->where('uid=' . (int)Foundry::user()->id);
        $db->SetQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

        $stream = Foundry::stream();
        $template = $stream->getTemplate();
        $template->setActor(Foundry::user()->id, 'user');
        $template->setVerb('unsubscribe');
        $template->setContext(Foundry::user()->id, 'unsubscribe-event');
        $template->setParams(array('id' => $event_id));
        $template->setDate(Foundry::date()->toMySQL());
        $stream->add($template);

        return true;
    }

    public function sudscribeEvent($event_id)
    {
        $db = $this->_db;
        $event = new stdClass();
        $event->uid = (int)Foundry::user()->id;
        $event->event_id = (int)$event_id;
        $db->insertObject('#__plot_user_event_map', $event);
//add stream
        $stream = Foundry::stream();
        $template = $stream->getTemplate();
        $template->setActor(Foundry::user()->id, 'user');
        $template->setVerb('subscribe');
        $template->setContext(Foundry::user()->id, 'subscribe-event');
        $template->setParams(array('id' => $event_id));
        $template->setDate(Foundry::date()->toMySQL());
        $stream->add($template);
        return true;
    }

    public function deleteEvent($event_id)
    {

        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('e.*')
            ->from('`#__plot_events` AS `e`')
            ->innerJoin('`#__plot_user_event_map` AS `em` ON em.event_id=e.id')
            ->where('`e`.id=' . (int)$event_id);
        $db->setQuery($query);
        $event = $db->loadObject();

        $query = $db->getQuery(true)
            ->clear()
            ->select('em.uid')
            ->from('`#__plot_events` AS `e`')
            ->innerJoin('`#__plot_user_event_map` AS `em` ON em.event_id=e.id')
            ->where('`e`.id=' . (int)$event_id)
            ->where('`em`.uid!=' . (int)Foundry::user()->id);
        $db->setQuery($query);
        $users = $db->loadObjectList();
        if ($users) {
            foreach ($users AS $user) {
                $this->sendMessage($user->uid, $event);
                plotPoints::assign('unsubscribe.meeting', 'com_plot', $user->uid, $event_id);
            }
        }

        $query = $db->getQuery(true);
        $query->delete('#__plot_user_event_map')
            ->where('event_id=' . (int)$event_id)
            ->where('uid=' . (int)Foundry::user()->id);
        $db->SetQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

        $query = $db->getQuery(true);
        $query->delete('#__plot_events')
            ->where('id=' . (int)$event_id);
        $db->SetQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

        $query = $db->getQuery(true);
        $query->delete('#__plot_age_event_map')
            ->where('event_id=' . (int)$event_id);
        $db->SetQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

        $query = $db->getQuery(true);
        $query->delete('#__plot_tags')
            ->where('entity="meeting"')
            ->where('entityId=' . (int)$event_id);
        $db->SetQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

        $this->deleteStream('add.meeting', $event_id);
        //delete event img
        if ($event) {
            $path = $_SERVER["DOCUMENT_ROOT"] . '/images/com_plot/events/' . $event->user_id . '/' . $event->img;
            if (file_exists($path)) {
                unlink($path); // Delete now
            }
        }


        $stream = Foundry::stream();
        $template = $stream->getTemplate();
        $template->setActor(Foundry::user()->id, 'user');
        $template->setVerb('delete');
        $template->setContext(Foundry::user()->id, 'delete-event');

        $template->setParams(array('title' => $event->title));
        $template->setDate(Foundry::date()->toMySQL());
        $stream->add($template);
        return true;
    }

    public function deleteStream($command, $id)
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`pph`.`sph_id`')
            ->from('`#__social_points_history` AS `sph`')
            ->innerJoin('`#__social_points` AS `sp` ON (`sp`.`id` = `sph`.`points_id`)')
            ->innerJoin('`#__plot_points_history` AS `pph` ON (`pph`.`sph_id` = `sph`.`id`)')
            ->where('`sph`.`user_id` = ' . (int)plotUser::factory()->id)
            ->where('`sp`.`command` = "' . $command . '"')
            ->where('pph.entity_id = ' . (int)$id);
        $db->setQuery($query);
        $sph_id = (int)$db->loadResult();

        $query = $db->getQuery(true);
        $query->delete('#__plot_points_history')
            ->where('sph_id=' . (int)$sph_id);
        $db->SetQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

        $query = $db->getQuery(true);
        $query->delete('#__social_points_history')
            ->where('id=' . (int)$sph_id);
        $db->SetQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

        return true;
    }

    public function getEvent($event_id)
    {
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('e.*')
            ->from('`#__plot_events` AS `e`')
            ->leftJoin('`#__plot_user_event_map` AS `em` ON em.event_id=e.id')
            ->where('`e`.id=' . (int)$event_id);
        $db->setQuery($query);


        return $db->loadObject();
    }

    public function sendMessage($users, $notification)
    {
        JModelLegacy::addIncludePath(JPATH_ROOT . DS . 'components' . DS . 'com_plot' . DS . 'models', 'Plot');
        $eventsModel = JModelLegacy::getInstance('events', 'plotModel');
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();

        // Get the current logged in user.
        $my = Foundry::user(859);

        // Get list of recipients.
        $recipients = $users;

        // Ensure that the recipients is an array.
        $recipients = Foundry::makeArray($recipients);

        // The user might be writing to a friend list.
        $lists = JRequest::getVar('list_id');


        // Get configuration
        $config = Foundry::config();

        // Filter recipients and ensure all the user id's are proper!
        $total = count($recipients);


        // Go through all the recipient and make sure that they are valid.
        for ($i = 0; $i < $total; $i++) {
            $userId = $recipients[$i];
            $user = Foundry::user($userId);

            if (!$user || empty($userId)) {
                unset($recipients[$i]);
            }
        }

        // After processing the recipients list, and no longer has any recipients, stop the user.
        if (empty($recipients)) {

            $app->enqueueMessage(JText::_('COM_EASYSOCIAL_CONVERSATIONS_ERROR_EMPTY_RECIPIENTS'), SOCIAL_MSG_ERROR);
            return;
        }

        // Get the conversation table.
        $conversation = Foundry::table('Conversation');

        // Determine the type of message this is by the number of recipients.
        $type = count($recipients) > 1 ? SOCIAL_CONVERSATION_MULTIPLE : SOCIAL_CONVERSATION_SINGLE;

        // For single recipients, we try to reuse back previous conversations
        // so that it will be like a long chat of history.
        if ($type == SOCIAL_CONVERSATION_SINGLE) {
            // We know that the $recipients[0] is always the target user.
            $state = $conversation->loadByRelation($my->id, $recipients[0], SOCIAL_CONVERSATION_SINGLE);
        }

        // Set the conversation creator.
        $conversation->created_by = $my->id;

        // Set the last replied date.
        $conversation->lastreplied = Foundry::date()->toMySQL();

        // Set the conversation type.
        $conversation->type = $type;

        // Let's try to create the conversation now.
        $state = $conversation->store();

        // If there's an error storing the conversation, break.
        if (!$state) {
            return;
        }

        // @rule: Store conversation message
        $message = Foundry::table('ConversationMessage');
        $message_text = 'Удалена втреча ' . $notification->title;


        $post = array('message' => $message_text, 'uid' => $users);

        // Bind the message data.
        $message->bind($post);

        // Set the conversation id since we have the conversation id now.
        $message->conversation_id = $conversation->id;

        // Sets the message type.
        $message->type = SOCIAL_CONVERSATION_TYPE_MESSAGE;

        // Set the creation date.
        $message->created = Foundry::date()->toMySQL();

        // Set the creator.
        $message->created_by = $my->id;

        // Try to store the message now.
        $state = $message->store();

        if (!$state) {


            return;
        }

        // Add users to the message maps.
        array_unshift($recipients, $my->id);

        $model = Foundry::model('Conversations');

        // Add the recipient as a participant of this conversation.
        $model->addParticipants($conversation->id, $recipients);

        // Add the message maps so that the recipient can view the message
        $model->addMessageMaps($conversation->id, $message->id, $recipients, $my->id);

        // Process attachments here.
        if ($config->get('conversations.attachments.enabled')) {
            $attachments = JRequest::getVar('upload-id');

            // If there are attachments, store them appropriately.
            if ($attachments) {
                $message->bindTemporaryFiles($attachments);
            }
        }

        // Bind message location if necessary.
        if ($config->get('conversations.location')) {
            $address = JRequest::getVar('address', '');
            $latitude = JRequest::getVar('latitude', '');
            $longitude = JRequest::getVar('longitude', '');

            if (!empty($address) && !empty($latitude) && !empty($longitude)) {
                $location = Foundry::table('Location');
                $location->loadByType($message->id, SOCIAL_TYPE_CONVERSATIONS, $my->id);

                $location->address = $address;
                $location->latitude = $latitude;
                $location->longitude = $longitude;
                $location->user_id = $this->created_by;
                $location->type = SOCIAL_TYPE_CONVERSATIONS;
                $location->uid = $message->id;

                $state = $location->store();
            }
        }

        // Send notification email to recipients
        foreach ($recipients as $recipientId) {
            // We should not send a notification to ourself.
            if ($recipientId != $my->id) {
                $recipient = Foundry::user($recipientId);

                // Add new notification item
                $mailParams = Foundry::registry();
                $mailParams->set('name', $recipient->getName());
                $mailParams->set('authorName', $my->getName());
                $mailParams->set('authorAvatar', $my->plotGetAvatar());
                $mailParams->set('authorLink', $my->getPermalink(true, true));
                $mailParams->set('message', $message->message);
                $mailParams->set('messageDate', $message->created);
                $mailParams->set('conversationLink', $conversation->getPermalink(true, true));

                // Send a notification for all participants in this thread.
                $state = Foundry::notify('conversations.new', array($recipientId), array('title' => JText::sprintf('COM_EASYSOCIAL_CONVERSATIONS_NEW_EMAIL_TITLE', $my->getName()), 'params' => $mailParams), false);
            }
        }

        return;
    }

    public function getEventStatus()
    {
        $statuses = array();

        $statuses[0] = array(
            'value' => '',
            'text' => JText::_('COM_PLOT_ALL_EVENTS')
        );
        $statuses[1] = array('value' => 'current',
            'text' => JText::_('COM_PLOT_CURRENT_EVENTS')
        );
        $statuses[2] = array('value' => 'old',
            'text' => JText::_('COM_PLOT_OLD_EVENTS')
        );

        return $statuses;
    }

    public function searchByDate($search, $user_id)
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('e.*')
            ->from('`#__plot_events` AS `e`')
            ->innerJoin('`#__plot_user_event_map` AS `em` ON em.event_id=e.id')
            ->where('`e`.user_id=' . (int)$user_id . ' OR `em`.uid=' . (int)$user_id)
            ->where('e.start_date="' . $db->escape($search) . '"');
        $db->setQuery($query);
        $events = $db->loadObjectList();

        if ($events) {

            foreach ($events AS $event) {
                $event->link = JRoute::_('index.php?option=com_plot&view=event&id=' . $event->id);
                $event->img = JURI::root() . 'images/com_plot/events/' . $event->user_id . '/' . $event->img;

                if ((int)PlotHelper::chechEventSubscription($event->id)) {
                    if ((int)PlotHelper::isOwnerEvent($event->id)) {
                        $event->button = '<button onclick="App.Event.DeleteEvent(this); return false;" data-id="' . $event->id . '">' . JText::_("COM_PLOT_REMOVE_EVENT") . '</button>';
                    } else {
                        $event->button = '<button onclick="App.Event.RemoveSubscribe(this); return false;" data-id="' . $event->id . '">' . JText::_("COM_PLOT_REMOVE_SUBSCRIBE_EVENT") . '</button>';
                    }
                } else {
                    $event->button = '<button onclick="App.Event.Subscribe(this);return false;" data-id="' . $event->id . '">' . JText::_("COM_PLOT_SUBSCRIBE_ON_EVENT") . '</button>';
                }

                $event->start = JHtml::date($event->start_date, 'Y-m-d H:m');
                $event->end = JHtml::date($event->end_date, 'Y-m-d H:m');
            }
        }
        return $events;
    }

    public function getEvents($categoryId = 'now', $sortTable = '`e`.create_date ASC', $ages = array(), $tags = array(0), $limit = array(), $my = 0)
    {
        $query = $this->_db->getQuery(true)
            ->clear()
            ->select('`e`.*')
            ->select('COUNT(ue.event_id) AS sum')
            ->from('`#__plot_events` AS `e`')
            ->leftJoin('`#__plot_user_event_map` AS `ue` ON `e`.`id` = `ue`.`event_id`')
            ->leftJoin('`#__plot_tags` AS `t` ON (`t`.`entityId` = `e`.`id` AND `t`.`entity`="meeting")')
            ->leftJoin('`#__plot_age_event_map` AS `a` ON `a`.`event_id` = `e`.`id`');

        if ($tags) {
            $this->tags = $tags;
            $this->setState('tags', $tags);
            $query->where('`t`.`tagId` IN (' . implode(",", $tags) . ')');
        }

        if ($ages) {
            $this->ages = $ages;
            $this->setState('ages', $ages);
            $str_a = implode(',', $ages);
            if ($str_a != '') {
                $query->where('`a`.`age_id` IN (' . $str_a . ')');
            }
        }

        if ($my) {
            $query->where('(`ue`.`uid` = ' . (int)plotUser::factory()->id . ' OR `e`.`user_id`=' . (int)plotUser::factory()->id . ')');
        }

        if ($categoryId == 'now') {
            $query->where('DATE(`e`.`start_date`)<= DATE(NOW()) AND DATE(`e`.`end_date`)>= DATE(NOW())');
        }
        if ($categoryId == 'new') {
            $query->where('DATE(`e`.`start_date`) > DATE(NOW())');
        }
        if ($categoryId == 'old') {
            $query->where('DATE(`e`.`end_date`) < DATE(NOW())');
        }

        if ($sortTable) {
            $query->order($this->_db->escape($sortTable));
        }

        if ($sortTable == 'sum DESC') {
            $query->group('`ue`.event_id');
        } else {
            $query->group('`e`.id');
        }

        if (isset($limit['offset']) && isset($limit['limit'])) {
            $books = $this->_db->setQuery($query, $limit['offset'], $limit['limit'])->loadObjectList();
            // die(var_dump($query->__toString()));
        } else {
            $books = $this->_db->setQuery($query)->loadObjectList();
        }

        $data['items'] = $books;
        $data['countItems'] = $this->getCountEvents($categoryId, $sortTable, $ages, $tags, $limit, $my);

        return $data;
    }

    public function getCountEvents($categoryId = 'now', $sortTable = '`e`.create_date ASC', $ages = array(), $tags = array(0), $limit = array(), $my = 0)
    {
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('`e`.*')
            ->select('COUNT(ue.event_id) AS sum')
            ->from('`#__plot_events` AS `e`')
            ->leftJoin('`#__plot_user_event_map` AS `ue` ON `e`.`id` = `ue`.`event_id`')
            ->leftJoin('`#__plot_tags` AS `t` ON `t`.`entityId` = `e`.`id` AND `t`.`entity`="meeting"')
            ->leftJoin('`#__plot_age_event_map` AS `a` ON `a`.`event_id` = `e`.`id`');

        if ($tags) {

            $this->tags = $tags;
            $this->setState('tags', $tags);
            $query->where('`t`.`tagId` IN (' . implode(",", $tags) . ')');
        }

        if ($ages) {
            $this->ages = $ages;
            $this->setState('ages', $ages);
            $str_a = implode(',', $ages);
            if ($str_a != '') {
                $query->where('`a`.`age_id` IN (' . $str_a . ')');
            }
        }

        if ($my) {
            $query->where('(`ue`.`uid` = ' . (int)plotUser::factory()->id . ' OR `e`.`user_id`=' . (int)plotUser::factory()->id . ')');
        }


        if ($categoryId == 'now') {
            $query->where('DATE(`e`.`start_date`)<= DATE(NOW()) AND DATE(`e`.`end_date`)>= DATE(NOW())');
        }
        if ($categoryId == 'new') {
            $query->where('DATE(`e`.`start_date`) > DATE(NOW())');
        }
        if ($categoryId == 'old') {
            $query->where('DATE(`e`.`end_date`) < DATE(NOW())');
        }


        if ($sortTable) {
            $query->order($db->escape($sortTable));
        }

        if ($sortTable == 'sum DESC') {
            $query->group('`ue`.event_id');
        } else {
            $query->group('`e`.id');
        }

        $books = $this->_db->setQuery($query)->loadObjectList();
        return count($books);
    }

    public function getCurrentEvents($sort = 'now')
    {
        $data = array();
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('`e`.*')
            ->from('`#__plot_events` AS `e`')
            ->leftJoin('`#__plot_user_event_map` AS `ue` ON `e`.`id` = `ue`.`event_id`')
            ->where('(`ue`.`uid` = ' . (int)plotUser::factory()->id . ' OR `e`.`user_id`=' . (int)plotUser::factory()->id . ')');


        if ($sort == 'now') {
            $query->where('DATE(`e`.`start_date`)<= DATE(NOW()) AND DATE(`e`.`end_date`)>= DATE(NOW())');
        } else {
            $query->where('DATE(`e`.`start_date`) > DATE(NOW())');
        }


        $events = $this->_db->setQuery($query)->loadObjectList();


        $data['items'] = $events;
        return $data;
    }

    public function getEventTags($id)
    {

        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('k.id, k.title')
            ->from('`#__plot_events` AS `e`')
            ->innerJoin('`#__plot_tags` AS `t` ON `t`.`entityId` = `e`.`id` AND `t`.`entity`="meeting"')
            ->innerJoin('`#__k2_items` AS `k` ON `k`.`id` = `t`.`tagId`')
            ->where('`e`.`id`=' . (int)$id);
//die(var_dump($this->_db->setQuery($query)->loadObjectList()));
        return $this->_db->setQuery($query)->loadObjectList();
    }

    public function getEventAges($id)
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('pa.id, pa.title')
            ->from('`#__plot_age_event_map` AS `a`')
            ->innerJoin('`#__plot_ages` AS `pa` ON `pa`.`id` = `a`.`age_id`')
            ->where('`a`.`event_id`=' . (int)$id);

        return $this->_db->setQuery($query)->loadObjectList();
    }

    public function getCurentAndFutureEvents()
    {

        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('`e`.*')
            ->from('`#__plot_events` AS `e`')
            ->where('e.start_date > NOW()')
            ->order('e.start_date ASC');
        return $this->_db->setQuery($query, 0, 6)->loadObjectList();
    }


    /**
     * returns count all events
     *
     * @access    public
     * @return int
     */
    public function getCountAllEvents()
    {
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('COUNT(*)')
            ->from('`#__plot_events`');

        return (int)$this->_db->setQuery($query)->loadResult();
    }

}
