<?php
defined('_JEXEC') or die('Restricted access');


class PlotModelEssay extends JModelLegacy
{
    public function save($text, $bookId)
    {
        $exist = $this->isExist($bookId);
        //die(var_dump($exist));
        if ($exist) {
        return    $this->update($text, $exist, $bookId);
        } else {
            return     $this->insert($text, $bookId);
        }
    }

    public function isExist($bookId)
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('id')
            ->from('#__plot_essay')
            ->where('book_id=' . $db->quote($bookId))
            ->where('user_id=' . $db->quote(JFactory::getUser()->id));

        $result = $db->setQuery($query)->loadResult();
        return $result;
    }

    private function insert($text, $bookId)
    {
        $db = JFactory::getDbo();
        $my = plotUser::factory();
        $book = new plotBook($bookId);
        $buyer = $book->whoBoughtBook($bookId, $my->id);
        $essay = new stdClass();
        $essay->user_id = JFactory::getUser()->id;
        $essay->book_id = $bookId;
        $essay->status = 0;
        $essay->text = $db->escape($text);
        $essay->reviewer = $buyer;
        $essay->date=Foundry::date()->toMySQL();
        $db->insertObject('#__plot_essay', $essay);
        $essayId = $db->insertid();
        $img = $this->createImage($text, $essayId);
        $this->ubdateEssayImage($essayId, $img);


        plotPoints::assign('add.essay', 'com_plot', plotUser::factory()->id, $essayId);
        if(plotUser::factory($buyer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($buyer)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
            $conversationsModel = JModelLegacy::getInstance('conversations', 'PlotModel');
            $conversationsModel->sendMessageSystem($buyer, 'plot-essay-add-' . $essayId);
            $mailer = JFactory::getMailer();
            $config = JFactory::getConfig();
            $sender = array(
                $config->get('mailfrom'),
                $config->get('fromname')
            );
            $bookObj = new plotBook($bookId);
            $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $my->id) . '">' . $my->name . '</a>';
            $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $bookId) . '">' . $bookObj->c_title . '</a>';
            $mailer->setSender($sender);
            $mailer->addRecipient(plotUser::factory($buyer)->email);
            $mailer->setSubject(JText::sprintf('COM_PLOT_ESSAY_BOOK_TITLE', $bookObj->c_title));
            $mailer->isHTML(true);
            $mailer->setBody(JText::sprintf("COM_PLOT_ESSAY_WAS_ADDED", '<a href="javascript:void(0);" onclick="SqueezeBox.open(\'' . JRoute::_("index.php?option=com_plot&task=essay.ajaxEssay") . '&id=' . $essayId . '\', {size:{x:744, y:500}, handler:\'iframe\'})">Эссе</a>',  $target, $actor));
            $mailer->Send();
        }
        $this->insertEssayInSocial($img, $essayId);
        $arr=array();
        $arr['img']=$img;
        $arr['essay']=$essayId;
        return $arr;
    }

    private function update($text, $exist, $bookId)
    {
        $db = JFactory::getDbo();
        $essay = new stdClass();
        $my=plotUser::factory();
        $essay->id = (int)$exist;
        $essay->text = $db->escape($text);
        $essay->date=Foundry::date()->toMySQL();
        $db->updateObject('#__plot_essay', $essay, 'id');
        $essayObj = $this->getEssayById($exist);
        $img = $this->createImage($text, $exist);
        $essay->img = $img;
        $db->updateObject('#__plot_essay', $essay, 'id');
        $book = new plotBook($bookId);
        $buyer = $book->whoBoughtBook($bookId, plotUser::factory()->id);

        if(plotUser::factory($buyer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($buyer)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
            $conversationsModel = JModelLegacy::getInstance('conversations', 'PlotModel');
            $conversationsModel->sendMessageSystem($buyer, 'plot-essay-add-' . $essay->id);
            $mailer = JFactory::getMailer();
            $config = JFactory::getConfig();
            $sender = array(
                $config->get('mailfrom'),
                $config->get('fromname')
            );
            $bookObj = new plotBook($bookId);
            $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $my->id) . '">' . $my->name . '</a>';
            $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $bookId) . '">' . $bookObj->c_title . '</a>';
            $mailer->setSender($sender);
            $mailer->addRecipient(plotUser::factory($buyer)->email);
            $mailer->setSubject(JText::sprintf('COM_PLOT_ESSAY_BOOK_TITLE', $bookObj->c_title));
            $mailer->isHTML(true);
            $mailer->setBody(JText::sprintf("COM_PLOT_ESSAY_WAS_ADDED",'<a href="javascript:void(0);" onclick="SqueezeBox.open(\'' . JRoute::_("index.php?option=com_plot&task=essay.ajaxEssay") . '&id=' . $exist . '\', {size:{x:744, y:500}, handler:\'iframe\'})">Эссе</a>', $target,$actor));
            $mailer->Send();
        }
        $this->updateEssayInSocial($essayObj->img, $exist, $img);
        $arr=array();
        $arr['img']=$img;
        $arr['essay']=$exist;
        return $arr;

    }

    public function getEssay($limit = array(), $book_id)
    {
        $data = array();
        $db = $this->_db;
        $user = Foundry::user();
        $query = $db->getQuery(true)
            ->clear()
            ->select('`e`.*')
            ->from('`#__plot_essay` AS `e`')
            ->where('`e`.`book_id`=' . (int)$book_id)
            ->where('(`e`.`status`=1 OR (`e`.`status`=0 AND `e`.`reviewer`=' . $user->id . ') OR (`e`.`status`=0 AND `e`.`user_id`=' . $user->id . '))');
        $query->order($db->escape('e.id DESC'));

        if (isset($limit['offset']) && isset($limit['limit'])) {
            $essaies = $this->_db->setQuery($query, $limit['offset'], $limit['limit'])->loadObjectList();
        } else {
            $essaies = $this->_db->setQuery($query)->loadObjectList();
        }


        foreach ($essaies AS $e) {
            $e->original = JURI::root() . 'media/com_plot/essay/' . $e->id . '/' . $e->img;
            $e->thumb = JFile::exists(JPATH_BASE . '/media/com_plot/essay/' . $e->id . '/' . $e->img) ? JURI::root() . 'media/com_plot/essay/' . $e->id . '/' . $e->img : JUri::root() . 'templates/plot/img/blank150x150.jpg';

        }
        $data['countItems'] = $this->_db->setQuery('SELECT FOUND_ROWS()')->loadResult();

        $data['items'] = $essaies;
        return $data;
    }

    public function getEssayById($id)
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('`e`.*')
            ->from('`#__plot_essay` AS `e`')
            ->where('`e`.`id`=' . abs((int)$id));

        return $this->_db->setQuery($query)->loadObject();
    }

    public function essayUpdateStatus($id)
    {
        $db = JFactory::getDbo();
        $essay = new stdClass();
        $essay->id = (int)$id;
        $essay->status = 1;
        $db->updateObject('#__plot_essay', $essay, 'id');
    }

    public function essayUpdateReason($id, $text)
    {
        $db = JFactory::getDbo();
        $essay = new stdClass();
        $essay->id = (int)$id;
        $essay->reason = $db->escape($text);
        $db->updateObject('#__plot_essay', $essay, 'id');
    }


    public function createImage($text, $id)
    {


        if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_plot/essay')) {

            mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_plot/essay');

        }

        if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_plot/essay/' . $id)) {

            mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_plot/essay/' . $id);
        }else {
            PlotHelper::deleteDir(JPATH_BASE . DIRECTORY_SEPARATOR . 'media/com_plot/essay/' . $id);

        }
        if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_plot/essay/' . $id)) {
            mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_plot/essay/' . $id);
        }
        $essay=$this->getEssayById($id);
        $bookObj=new plotBook($essay->book_id);
        $my=plotUser::factory();
        $city=$my->getSocialFieldData('city');
        $user_name=$my->name;
        if($city){
            $user_name=$user_name.' ,';
        }
        $new_img_name = md5(time() . plotUser::factory()->id);
        $im = imagecreatefromjpeg(plotGlobalConfig::getVar('essayPicture'));
        // Allocate A Color For The Text
        $green=imagecolorallocate($im, 7, 178, 76);
        $black=imagecolorallocate($im, 0, 0, 0);
        $brown=imagecolorallocate($im, 169, 79, 9);
        // Set Path to Font File
        $font_path = 'media/arial.ttf';

        $title_book=JText::sprintf('COM_PLOT_ESSAY_BOOK_TITLE', $bookObj->c_title);
        $sentArray = explode(plotGlobalConfig::getVar("essayTextDelimiter"), preg_replace('/\r?\n|\r/',' ', $text));
        $cert_date=date("d.m.Y");
        $i = 0;
        $bigPiecesArray[0] = '';
        for ($k = 0; $k < count($sentArray); $k++) {
            $bigPiecesArray[$i] .= $sentArray[$k].plotGlobalConfig::getVar("essayTextDelimiter");
            if (strlen($bigPiecesArray[$i]) > (int)plotGlobalConfig::getVar("essayTextMaxSymbols")){
                $i++;
                $bigPiecesArray[$i] = '';
            }
        }
        $ii=15;

        // Print Text On Image
        imagettftext($im, 14, 0, 30, 120,  $brown, $font_path, $title_book);
        foreach($bigPiecesArray AS $val){
            imagettftext($im, 12, 0, 30, (140+$ii), $black, $font_path, $val);
            $ii=$ii+20;
        }
        imagettftext($im, 12, 0, 30, 650,  $brown, $font_path, $cert_date);
        imagettftext($im, 12, 0, 500, 650,  $brown, $font_path,$user_name);
        if($city){
            imagettftext($im, 12, 0, 500, 665,  $brown, $font_path,$city);
        }

        // imagettftext($im, 25, 0, 180, 720, $white, $font_path, $user_score);
        imagepng($im, 'media/com_plot/essay/' . $id . '/' . $new_img_name . '.png');
        imagedestroy($im);
        return $new_img_name . '.png';
    }

    private function ubdateEssayImage($id, $img)
    {
        $db = JFactory::getDbo();
        $essay = new stdClass();
        $essay->id = (int)$id;
        $essay->img = $db->escape($img);
        $db->updateObject('#__plot_essay', $essay, 'id');
        return;
    }

    private function insertEssayInSocial($img, $exist)
    {


        $db = Foundry::db();
        $my = plotUser::factory();
        $query = $db->getQuery(true)
            ->select('`a`.*')
            ->from('`#__social_albums` AS `a`')
            ->where('`a`.`uid` = ' . (int)$my->id)
            ->where('`a`.`type` = "plot-essay"');
        $db->setQuery($query);
        $album = $db->loadObject();

        if (!$album) {
            $album = Foundry::table('Album');
            $album->uid = $my->id;
            $album->type = 'plot-essay';
            $album->created = Foundry::date()->toMySQL();
            $album->ordering = 0;
            $album->assigned_date = Foundry::date()->toMySQL();
            $db->insertObject('#__social_albums', $album);
            $albumId = $db->insertid();
        } else {
            $albumId = $album->id;
        }

        $photo = new stdClass();
        $photo->uid = $my->id;
        $photo->type = 'plot-essay';
        $photo->album_id = $albumId;
        $photo->title = JText::_('COM_PLOT_ESSAY_WAS_CREATED');
        $photo->caption = JText::_('COM_PLOT_ESSAY_WAS_CREATED');
        $photo->created = Foundry::date()->toMySQL();
        $photo->assigned_date = Foundry::date()->toMySQL();
        $photo->featured = 0;
        $photo->state = 0;
        $photo->storage = 'joomla';
        $photo->ordering = 0;
        $db->insertObject('#__social_photos', $photo);
        $photoId = $db->insertid();

        $meta = new stdClass();
        $meta->photo_id = $photoId;
        $meta->group = 'path';
        $meta->property = 'thumbnail';
        $meta->value = JPATH_SITE . DIRECTORY_SEPARATOR . 'media/com_plot/essay/' . $exist . '/' . $img;

        $db->insertObject('#__social_photos_meta', $meta);
        $metaid = $db->insertid();

    }

    private function updateEssayInSocial($img, $exist, $new_name)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->update('`#__social_photos_meta`')
            ->set("`value` = '" . JPATH_SITE . '\/media\/com_plot\/essay\/' . $exist . '\/' . $new_name . "'")
            ->where('`property` ="thumbnail"')
            ->where('`value` LIKE "%' . $img . '"')
            ->where('`group`="path"');

        $db->setQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

    }

    public function updateEssaySocialImgStatus($id)
    {
        $essay = $this->getEssayById($id);
        $photoId = $this->getEssaySocialPhotoId($essay->img);
        //die(var_dump($photoId));
        $this->updateEssaySocialStatus($photoId);
    }

    private function getEssaySocialPhotoId($img)
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('photo_id')
            ->from('#__social_photos_meta')
            ->where('`value` LIKE "%' . $img . '"')
            ->where('`property` ="thumbnail"')
            ->where('`group`="path"');

        $result = $db->setQuery($query)->loadResult();
        return $result;
    }

    private function updateEssaySocialStatus($id)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->update('`#__social_photos`')
            ->set("`state` = 1")
            ->where('`id`=' . (int)$id);

        $db->setQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }
    }

    public function replaceForConversation($ids)
    {

        if ($ids) {
            $my_arr = array();
            foreach ($ids AS $id) {
                $my_arr[] = abs(filter_var($id, FILTER_SANITIZE_NUMBER_INT));
            }
            if ($my_arr) {
                $data = array();
                $db = $this->_db;

                foreach ($my_arr AS $id) {
                    $query = $db->getQuery(true)
                        ->clear()
                        ->select('`e`.*')
                        ->from('`#__plot_essay` AS `e`')
                        ->where('`e`.`id` =' . (int)$id);
                    $data[] = $this->_db->setQuery($query)->loadObject();

                }

                foreach ($data AS $essay) {

                    if($essay){
                        $book_name = '<a href="' . JRoute::_("index.php?option=com_plot&view=publication&id=$essay->book_id") . '">' . PlotHelper::getBookById($essay->book_id)->c_title . '</a>';
                        $user = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $essay->user_id) . '">' . plotUser::factory($essay->user_id)->name . '</a>';
                        $essay->msg = JText::sprintf("COM_PLOT_ESSAY_WAS_ADDED", '<a href="javascript:void(0);" onclick="SqueezeBox.open(\'' . JRoute::_("index.php?option=com_plot&task=essay.ajaxEssay") . '&id=' . $essay->id . '\', {size:{x:744, y:500}, handler:\'iframe\'})">Эссе</a>', $book_name, $user);

                    }else{
                        $essay=new stdClass();
                        $essay->msg = JText::sprintf("COM_PLOT_ESSAY_WAS_ADDED", 'Эссе', '', '');
                    }


                }
                return $data;
            }

        }

    }

    public function booksReplaceText($ids, $text_const)
    {
        $data = array();
        if ($ids) {
            $my_arr = array();
            foreach ($ids AS $id) {
                $my_arr[] = abs(filter_var($id, FILTER_SANITIZE_NUMBER_INT));
            }
            if ($my_arr) {


                foreach ($my_arr AS $id) {
                    $obj = new stdClass();
                    $essayModel = JModelLegacy::getInstance('Essay', 'PlotModel');
                    $essayObj = $essayModel->getEssayById($id);

                    if($essayObj){
                        $user = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $essayObj->user_id) . '">' . plotUser::factory($essayObj->user_id)->name . '</a>';
                        $book_name = '<a href="' . JRoute::_("index.php?option=com_plot&view=publication&id=$essayObj->book_id") . '">' . PlotHelper::getBookById($essayObj->book_id)->c_title . '</a>';
                        $obj->msg = JText::sprintf($text_const, $user, $book_name);
                    }else{
                        $obj->msg = JText::sprintf($text_const, '', '');
                    }


                    $data[] = $obj;
                }
                return $data;
            }

        }

    }

    public function essayApprovedReplaceText($ids)
    {
        $data = array();
        if ($ids) {
            $my_arr = array();
            foreach ($ids AS $id) {
                $my_arr[] = abs(filter_var($id, FILTER_SANITIZE_NUMBER_INT));
            }
            if ($my_arr) {


                foreach ($my_arr AS $id) {
                    $obj = new stdClass();
                    $essayModel = JModelLegacy::getInstance('Essay', 'PlotModel');
                    $essayObj = $essayModel->getEssayById($id);
                     if($essayObj){
                         $user = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $essayObj->reviewer) . '">' . plotUser::factory($essayObj->reviewer)->name . '</a>';
                         $book_name = '<a href="' . JRoute::_("index.php?option=com_plot&view=publication&id=$essayObj->book_id") . '">' . PlotHelper::getBookById($essayObj->book_id)->c_title . '</a>';
                         $obj->msg = JText::sprintf(JText::_("COM_PLOT_MSG_APPROVE_ESSAY"), $user, $book_name);
                     }else{
                         $obj->msg = JText::sprintf(JText::_("COM_PLOT_MSG_APPROVE_ESSAY"), '','');
                     }


                    $data[] = $obj;
                }
                return $data;
            }

        }

    }

    public function essayRejectedReplaceText($ids)
    {
        $data = array();
        if ($ids) {
            $my_arr = array();
            foreach ($ids AS $id) {
                $my_arr[] = abs(filter_var($id, FILTER_SANITIZE_NUMBER_INT));
            }
            if ($my_arr) {


                foreach ($my_arr AS $id) {
                    $obj = new stdClass();
                    $essayModel = JModelLegacy::getInstance('Essay', 'PlotModel');
                    $essayObj = $essayModel->getEssayById($id);

                    if($essayObj) {
                        $user = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $essayObj->reviewer) . '">' . plotUser::factory($essayObj->reviewer)->name . '</a>';
                        $book_name = '<a href="' . JRoute::_("index.php?option=com_plot&view=publication&id=$essayObj->book_id") . '">' . PlotHelper::getBookById($essayObj->book_id)->c_title . '</a>';
                        $reason = $essayObj->reason;
                        $obj->msg = JText::sprintf(JText::_("COM_PLOT_MSG_REJECT_ESSAY"), $user, $book_name, $reason);
                    }else{
                        $obj->msg = JText::sprintf(JText::_("COM_PLOT_MSG_REJECT_ESSAY"), '', '', '');
                    }
                    $data[] = $obj;
                }
                return $data;
            }

        }

    }

}
