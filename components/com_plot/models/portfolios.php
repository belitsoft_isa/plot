<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');

class PlotModelPortfolios extends JModelList
{

    //----------------------------------------------------------------------------------------------------
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'a.id',
                'a.title',
                'a.link'

            );
        }

        parent::__construct($config);
    }

    //----------------------------------------------------------------------------------------------------
    protected function populateState($ordering = null, $direction = null)
    {
        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        parent::populateState('a.id', 'asc');
    }

    //----------------------------------------------------------------------------------------------------
    protected function getListQuery()
    {
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('a.*')
            ->from('`#__plot_portfolio` AS `a`')
            ->where('`a`.user_id=' . (int)plotUser::factory()->id);

        // Filter by search in name.

        $search = $this->getState('filter.search');

        if (!empty($search)) {
            $search = $db->Quote('%' . $db->escape($search, true) . '%');
            $query->where('`a`.`title` LIKE ' . $search);
        }
        $query->order($db->escape($this->state->get('list.ordering') . ' ' . $this->state->get('list.direction')));
        return $query;
    }

    public function getLastPortfolios($id)
    {
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('a.*')
            ->from('`#__plot_portfolio` AS `a`')
            ->where('`a`.user_id=' . (int)$id)
            ->order('`a`.`date` desc');
        $db->setQuery($query, 0, plotGlobalConfig::getVar('countShowLastPortfolios'));
        $porfolios = $db->loadObjectList();
        foreach ($porfolios AS $item) {
            if (!file_exists(JPATH_SITE . '/images/com_plot/portfolio/' . (int)$item->user_id . '/thumb/' . $item->img)) {
                $item->img = JUri::root() . 'images/com_plot/def_portfolio.jpg';
            }
        }
        return $porfolios;
    }

}
