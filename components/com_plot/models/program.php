<?php
defined('_JEXEC') or die('Restricted access');


class PlotModelProgram extends JModelLegacy
{

    public function getCourses($id)
    {
        $db = JFactory::getDbo();
        $data = array();
        $query = $db->getQuery(true);
        $query->select('`lc`.`course_name`, `lc`.`course_description`,`lc`.owner_id, pc.*')
            ->from('#__plot_course_program_map AS c')
            ->leftJoin('`#__lms_courses` AS `lc` ON lc.id=c.course_id')
            ->leftJoin("#__plot_courses AS `pc` ON (`pc`.`id` = `lc`.`id`)")
            ->where('c.program_id=' . $db->quote((int)$id));
        $data['countItems'] = $this->getTotal($query);
        $result = $db->setQuery($query)->loadObjectList();
        $my=plotUser::factory();
        if ($result) {
            foreach ($result AS $course) {
                $course->link = JRoute::_("index.php?option=com_plot&view=course&id=" . $course->id);
                $course->image = JUri::root() . $course->image;
                $course->finished=$my->isCourseFinished($course->id);
                $course->video_approved = (int)$my->courseVideoApproved($course->id);
            }
        }

        $data['items'] = $result;

        return $data;

    }

    public function getBooks($id)
    {
        $db = JFactory::getDbo();
        $data = array();
        $query = $db->getQuery(true);
        $query->select('p.*, b.author AS book_author')
            ->from('#__plot_books_program_map AS c')
            ->leftJoin('`#__html5fb_publication` AS `p` ON p.c_id=c.book_id')
            ->leftJoin('`#__plot_book_cost` AS `b` ON b.id=p.c_id')
            ->where('c.program_id=' . $db->quote((int)$id));
        $data['countItems'] = $this->getTotal($query);
        $result = $db->setQuery($query)->loadObjectList();
        $my = plotUser::factory();
        if ($result) {
            foreach ($result AS $book) {
                $thumbnailPath = JPATH_SITE . '/media/com_html5flippingbook' . '/thumbs/' . $book->c_thumb;
                if ($book->c_thumb == "" || !is_file($thumbnailPath)) {
                    $book->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
                } else {
                    if (file_exists(JPATH_BASE . '/media/com_html5flippingbook' . '/thumbs/thimb_' . $book->c_thumb)) {
                        $book->thumbnailUrl = JURI::root() . "media/com_html5flippingbook/thumbs/thimb_" . $book->c_thumb;
                    } else {
                        $book->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
                    }
                }
                $book->readed = (int)$my->bookIsReaded($book->c_id);
                $book->essay_approved = (int)$my->essayIsWritten($book->c_id);
                $book->link = JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $book->c_id);

            }
        }

        $data['items'] = $result;

        return $data;

    }

    public function getPrograms($id, $limit = array())
    {
        $program = new plotProgram($id);
        $data = array();
        $db = JFactory::getDbo();
        $my = plotUser::factory();
        $query = $db->getQuery(true);
        $query->select('p.*, m.`value` AS img, ages.title AS title_age, l.title AS level_title')
            ->from('#__plot_programs_program_map AS c')
            ->leftJoin('`#__social_clusters` AS `p` ON p.id=c.item_id')
            ->leftJoin('`#__plot_program` AS `pr` ON pr.id=c.item_id')
            ->leftJoin('`#__plot_age_program_map` AS age ON age.program_id=p.id')
            ->leftJoin('`#__plot_ages` AS ages ON ages.id=age.age_id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON l.id=pr.level')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`p`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`p`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")');
        if (!$my->isAvaliableProgram()) {
            $query->where('pr.level!=' . plotGlobalConfig::getVar("programMageLevelId"));
        }
        $query->where('c.program_id=' . $db->quote((int)$id))
            //->where('p.category_id=' . (int)$program->category_id)
            ->group('c.item_id');

        $data['countItems'] = $this->getTotal($query);


        $this->_db->setQuery($query);


        $result = $this->_db->loadObjectList();

        foreach ($result AS $row) {
            $group = FD::group($row->id);

            if ($group->isOpen()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_OPEN_GROUP');
            }
            if ($group->isClosed()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_CLOSED_GROUP');
            }
            if ($group->isInviteOnly()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_INVITE_GROUP');
            }

            $row->link = FRoute::groups(array('layout' => 'item', 'id' => $group->getAlias()), false) . '?plot=1';

            if( $group->avatars["medium"]){
                $row->img = JURI::root() . "/media/com_easysocial/avatars/group/".$row->id."/".$group->avatars["medium"];
            }else{
                $row->img = JURI::root() . "/media/com_easysocial/defaults/avatars/group/square.png";
            }

            if ($my) {
                $row->percent = $my->programProgress($row->id);
            }

            $progObj=new plotProgram($row->id);
            $row->ages=implode(', ',$progObj->getProgramAgesTitle());

        }

        $data['items'] = $result;

        return $data;


    }

    public function getAllPrograms($limit = array(),$level=0, $age=0)
    {
        $data = array();
        $db = JFactory::getDbo();
        $my = plotUser::factory();
        $query = $db->getQuery(true);

        $query->select('p.*, m.`value` AS img, ages.title AS title_age, l.title AS level_title, cats.title AS cats_title')
            ->from('#__plot_program AS c')
            ->leftJoin('`#__social_clusters` AS `p` ON c.id=p.id')
            ->leftJoin('`#__social_clusters_categories` AS `cats` ON cats.id=p.category_id')
            ->leftJoin('`#__plot_age_program_map` AS age ON age.program_id=p.id')
            ->leftJoin('`#__plot_ages` AS ages ON ages.id=age.age_id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON l.id=c.level')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`p`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`p`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")');
        if (!$my->isAvaliableProgram()) {
            $query->where('c.level!=' . plotGlobalConfig::getVar("programMageLevelId"));
        }
        if ($level) {
            $query->where('`c`.level=' . (int)$level);
        }
        if ($age) {
            $query->where('`age`.age_id IN(' . $db->quote($age) . ')');
        }
        $query->group('c.id');

        $data['countItems'] = $this->getTotal($query);

        if (isset($limit['limit']) && isset($limit['offset'])) {
            $this->_db->setQuery($query, $limit['offset'], $limit['limit']);
        } else {
            $this->_db->setQuery($query);
        }

        $result = $this->_db->loadObjectList();

        foreach ($result AS $row) {
            $group = FD::group($row->id);

            if ($group->isOpen()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_OPEN_GROUP');
            }
            if ($group->isClosed()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_CLOSED_GROUP');
            }
            if ($group->isInviteOnly()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_INVITE_GROUP');
            }

            $row->link = FRoute::groups(array('layout' => 'item', 'id' => $group->getAlias()), false) . '?plot=1';
            if( $group->avatars["medium"]){
                $row->img = JURI::root() . "/media/com_easysocial/avatars/group/".$row->id."/".$group->avatars["medium"];
            }else{
                $row->img = JURI::root() . "/media/com_easysocial/defaults/avatars/group/square.png";
            }

            if ($my) {
                $row->percent = $my->programProgress($row->id);
            }

            $progObj=new plotProgram($row->id);
            $row->ages=implode(', ',$progObj->getProgramAgesTitle());
            $row->members=$group->getTotalMembers();


        }


        $data['items'] = $result;

        return $data;
    }

    private function getTotal($sql)
    {
        $db = JFactory::getDbo();
        $data = $this->_db->setQuery($sql)->loadColumn();
        return count($data);
    }

    public function getMyPrograms($limit = array(), $level=0, $age=0)
    {
        $data = array();
        $db = JFactory::getDbo();
        $my = plotUser::factory();
        $query = $db->getQuery(true);
        $query->select('p.*, m.`value` AS img, ages.title AS title_age, l.title AS level_title, cats.title AS cats_title')
            ->from('#__plot_program AS c')
            ->leftJoin('`#__social_clusters` AS `p` ON c.id=p.id')
            ->leftJoin('`#__social_clusters_categories` AS `cats` ON cats.id=p.category_id')
            ->leftJoin('`#__social_clusters_nodes` AS cn ON cn.cluster_id=c.id')
            ->leftJoin('`#__plot_age_program_map` AS age ON age.program_id=p.id')
            ->leftJoin('`#__plot_ages` AS ages ON ages.id=age.age_id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON l.id=c.level')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`p`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`p`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")');
        if (!$my->isAvaliableProgram()) {
            $query->where('c.level!=' . plotGlobalConfig::getVar("programMageLevelId"));
        }
        if ($level) {
            $query->where('`c`.level=' . (int)$level);
        }
        if ($age) {
            $query->where('`age`.age_id IN(' . $db->quote($age) . ')');
        }
        $query->where('`cn`.uid=' . (int)plotUser::factory()->id)
            ->group('c.id');
        $data['countItems'] = $this->getTotal($query);
        if (isset($limit['limit']) && isset($limit['offset'])) {
            $this->_db->setQuery($query, $limit['offset'], $limit['limit']);
        } else {
            $this->_db->setQuery($query);
        }
        $result = $this->_db->loadObjectList();

        foreach ($result AS $row) {
            $group = FD::group($row->id);
            if ($group->isOpen()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_OPEN_GROUP');
            }
            if ($group->isClosed()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_CLOSED_GROUP');
            }
            if ($group->isInviteOnly()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_INVITE_GROUP');
            }

            $row->link = FRoute::groups(array('layout' => 'item', 'id' => $group->getAlias()), false) . '?plot=1';
            if( $group->avatars["medium"]){
                $row->img = JURI::root() . "/media/com_easysocial/avatars/group/".$row->id."/".$group->avatars["medium"];
            }else{
                $row->img = JURI::root() . "/media/com_easysocial/defaults/avatars/group/square.png";
            }

            if ($my) {
                $row->percent = $my->programProgress($row->id);
            }

            $progObj=new plotProgram($row->id);
            $row->ages=implode(', ',$progObj->getProgramAgesTitle());
            $row->members=$group->getTotalMembers();

        }
        $data['items'] = $result;

        return $data;
    }

    public function getGroupsByCategory($id, $limit = array(), $level=0, $age=0)
    {
        $db = JFactory::getDbo();
        $data = array();
        $my = plotUser::factory();
        $query = $db->getQuery(true);
        $query->select('p.*, m.`value` AS img, ages.title AS title_age, l.title AS level_title, cats.title AS cats_title')
            ->from('#__plot_program AS c')
            ->leftJoin('`#__social_clusters` AS `p` ON c.id=p.id')
            ->leftJoin('`#__social_clusters_categories` AS `cats` ON cats.id=p.category_id')
            ->leftJoin('`#__plot_age_program_map` AS age ON age.program_id=p.id')
            ->leftJoin('`#__plot_ages` AS ages ON ages.id=age.age_id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON l.id=c.level')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`p`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`p`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")');
        if (!$my->isAvaliableProgram()) {
            $query->where('c.level!=' . plotGlobalConfig::getVar("programMageLevelId"));
        }
        if ($level) {
            $query->where('`c`.level=' . (int)$level);
        }
        if ($age) {
            $query->where('`age`.age_id IN(' . $db->quote($age) . ')');
        }
        $query->where('p.category_id=' . (int)$id)
            ->group('c.id');
        $data['countItems'] = $this->getTotal($query);
        if (isset($limit['limit']) && isset($limit['offset'])) {
            $this->_db->setQuery($query, $limit['offset'], $limit['limit']);
        } else {
            $this->_db->setQuery($query);
        }
        $result = $this->_db->loadObjectList();

        foreach ($result AS $row) {
            $group = FD::group($row->id);
            if ($group->isOpen()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_OPEN_GROUP');
            }
            if ($group->isClosed()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_CLOSED_GROUP');
            }
            if ($group->isInviteOnly()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_INVITE_GROUP');
            }

            $row->link = FRoute::groups(array('layout' => 'item', 'id' => $group->getAlias()), false) . '?plot=1';
            if( $group->avatars["medium"]){
                $row->img = JURI::root() . "/media/com_easysocial/avatars/group/".$row->id."/".$group->avatars["medium"];
            }else{
                $row->img = JURI::root() . "/media/com_easysocial/defaults/avatars/group/square.png";
            }

            if ($my) {
                $row->percent = $my->programProgress($row->id);
            }

            $progObj=new plotProgram($row->id);
            $row->ages=implode(', ',$progObj->getProgramAgesTitle());
            $row->members=$group->getTotalMembers();

        }

        $data['items'] = $result;

        return $data;
    }

    public function getGroupsBylevel($id, $limit = array())
    {
        $data = array();
        $db = JFactory::getDbo();
        $my = plotUser::factory();
        $query = $db->getQuery(true);
        $query->select('p.*, m.`value` AS img, ages.title AS title_age, l.title AS level_title, cats.title AS cats_title')
            ->from('#__plot_program AS pr')
            ->leftJoin('`#__social_clusters` AS `p` ON p.id=pr.id')
            ->leftJoin('`#__social_clusters_categories` AS `cats` ON cats.id=p.category_id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON l.id=pr.level')
            ->leftJoin('`#__plot_age_program_map` AS age ON age.program_id=p.id')
            ->leftJoin('`#__plot_ages` AS ages ON ages.id=age.age_id')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`p`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`p`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")');
        if ($id) {
            $query->where('`pr`.level=' . (int)$id);
        }
        if (!$my->isAvaliableProgram()) {
            $query->where('pr.level!=' . plotGlobalConfig::getVar("programMageLevelId"));
        }
        $query->group('p.id');
        $data['countItems'] = $this->getTotal($query);
        if (isset($limit['limit']) && isset($limit['offset'])) {
            $this->_db->setQuery($query, $limit['offset'], $limit['limit']);
        } else {
            $this->_db->setQuery($query);
        }
        $result = $this->_db->loadObjectList();

        foreach ($result AS $row) {
            $group = FD::group($row->id);
            if ($group->isOpen()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_OPEN_GROUP');
            }
            if ($group->isClosed()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_CLOSED_GROUP');
            }
            if ($group->isInviteOnly()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_INVITE_GROUP');
            }

            $row->link = FRoute::groups(array('layout' => 'item', 'id' => $group->getAlias()), false) . '?plot=1';
            if( $group->avatars["medium"]){
                $row->img = JURI::root() . "/media/com_easysocial/avatars/group/".$row->id."/".$group->avatars["medium"];
            }else{
                $row->img = JURI::root() . "/media/com_easysocial/defaults/avatars/group/square.png";
            }

            if ($my) {
                $row->percent = $my->programProgress($row->id);
            }

            $progObj=new plotProgram($row->id);
            $row->ages=implode(', ',$progObj->getProgramAgesTitle());
            $row->members=$group->getTotalMembers();

        }
        $data['items'] = $result;

        return $data;
    }

    public function getGroupsByAge($id, $limit = array())
    {
        $db = JFactory::getDbo();
        $data = array();
        $my = plotUser::factory();
        $query = $db->getQuery(true);
        $query->select('p.*, m.`value` AS img, ages.title AS title_age, l.title AS level_title, cats.title AS cats_title')
            ->from('#__plot_program AS pr')
            ->leftJoin('`#__social_clusters` AS `p` ON p.id=pr.id')
            ->leftJoin('`#__social_clusters_categories` AS `cats` ON cats.id=p.category_id')
            ->leftJoin('`#__plot_age_program_map` AS age ON age.program_id=p.id')
            ->leftJoin('`#__plot_ages` AS ages ON ages.id=age.age_id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON l.id=pr.level')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`p`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`p`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")');
        if ($id) {
            $query->where('`age`.age_id IN(' . $db->quote($id) . ')');
        }
        if (!$my->isAvaliableProgram()) {
            $query->where('pr.level!=' . plotGlobalConfig::getVar("programMageLevelId"));
        }
        $query->group('p.id');
        $data['countItems'] = $this->getTotal($query);
        if (isset($limit['limit']) && isset($limit['offset'])) {
            $this->_db->setQuery($query, $limit['offset'], $limit['limit']);
        } else {
            $this->_db->setQuery($query);
        }
        $result = $this->_db->loadObjectList();

        foreach ($result AS $row) {
            $group = FD::group($row->id);
            if ($group->isOpen()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_OPEN_GROUP');
            }
            if ($group->isClosed()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_CLOSED_GROUP');
            }
            if ($group->isInviteOnly()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_INVITE_GROUP');
            }

            $row->link = FRoute::groups(array('layout' => 'item', 'id' => $group->getAlias()), false) . '?plot=1';
            if( $group->avatars["medium"]){
                $row->img = JURI::root() . "/media/com_easysocial/avatars/group/".$row->id."/".$group->avatars["medium"];
            }else{
                $row->img = JURI::root() . "/media/com_easysocial/defaults/avatars/group/square.png";
            }

            if ($my) {
                $row->percent = $my->programProgress($row->id);
            }

            $progObj=new plotProgram($row->id);
            $row->ages=implode(', ',$progObj->getProgramAgesTitle());
            $row->members=$group->getTotalMembers();

        }

        $data['items'] = $result;

        return $data;
    }


    public function getFeaturedPrograms($limit = array(), $level=0, $age=0)
    {
        $data = array();
        $db = JFactory::getDbo();
        $my = plotUser::factory();
        $query = $db->getQuery(true);

        $query->select('p.*, m.`value` AS img, ages.title AS title_age, l.title AS level_title, cats.title AS cats_title')
            ->from('#__plot_program AS c')
            ->leftJoin('`#__social_clusters` AS `p` ON c.id=p.id')
            ->leftJoin('`#__social_clusters_categories` AS `cats` ON cats.id=p.category_id')
            ->leftJoin('`#__plot_age_program_map` AS age ON age.program_id=p.id')
            ->leftJoin('`#__plot_ages` AS ages ON ages.id=age.age_id')
            ->leftJoin('`#__plot_program_levels` AS `l` ON l.id=c.level')
            ->leftJoin('`#__social_albums` AS a ON (a.uid=`p`.id AND a.type="group" AND a.core=2)')
            ->leftJoin('`#__social_photos` AS ph ON (ph.uid=`p`.id AND ph.type="group" AND a.id=ph.album_id)')
            ->leftJoin('`#__social_photos_meta` AS m ON (m.photo_id=`ph`.id AND m.group="path" AND m.property="thumbnail")')
            ->where('p.featured=1');
        if (!$my->isAvaliableProgram()) {
            $query->where('c.level!=' . plotGlobalConfig::getVar("programMageLevelId"));
        }
        if ($level) {
            $query->where('`c`.level=' . (int)$level);
        }
        if ($age) {
            $query->where('`age`.age_id IN(' . $db->quote($age) . ')');
        }

        $query->group('c.id');
        $data['countItems'] = $this->getTotal($query);
        if (isset($limit['limit']) && isset($limit['offset'])) {
            $this->_db->setQuery($query, $limit['offset'], $limit['limit']);
        } else {
            $this->_db->setQuery($query);
        }

        $result = $this->_db->loadObjectList();

        foreach ($result AS $row) {
            $group = FD::group($row->id);
            if ($group->isOpen()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_OPEN_GROUP');
            }
            if ($group->isClosed()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_CLOSED_GROUP');
            }
            if ($group->isInviteOnly()) {
                $row->group_status = JText::_('COM_EASYSOCIAL_GROUPS_INVITE_GROUP');
            }

            $row->link = FRoute::groups(array('layout' => 'item', 'id' => $group->getAlias()), false) . '?plot=1';
            if( $group->avatars["medium"]){
                $row->img = JURI::root() . "/media/com_easysocial/avatars/group/".$row->id."/".$group->avatars["medium"];
            }else{
                $row->img = JURI::root() . "/media/com_easysocial/defaults/avatars/group/square.png";
            }

            if ($my) {
                $row->percent = $my->programProgress($row->id);
            }

            $progObj=new plotProgram($row->id);
            $row->ages=implode(', ',$progObj->getProgramAgesTitle());
            $row->members=$group->getTotalMembers();

        }

        $data['items'] = $result;

        return $data;
    }

    public function programFinishedReplaceText($ids)
    {
        if ($ids) {
            $my_arr = array();
            $count_ids = count($ids);
            for ($i = 0; $i < $count_ids; $i++) {
                $str = explode('-', $ids[$i]);

                $my_arr[$i]['book'] = abs(filter_var($str[0], FILTER_SANITIZE_NUMBER_INT));
                $my_arr[$i]['user'] = abs(filter_var($str[1], FILTER_SANITIZE_NUMBER_INT));
            }

            if ($my_arr) {


                foreach ($my_arr AS $key => $item) {
                    $group = FD::group($item['book']);
                    $book = '<a href="' . FRoute::groups(array('layout' => 'item', 'id' => $group->getAlias()), false) . '?plot=1">' . $group->title . '</a>';
                    $user = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $item['user']) . '">' . plotUser::factory($item['user'])->name . '</a>';
                    $data[$key] = new stdClass();
                    $data[$key]->msg = JText::sprintf(JText::_("COM_PLOT_MSG_PROGRAM_FINISHED"), $user, $book);


                }

                return $data;
            }

        }


    }


}
