<?php
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_SITE . '/components/com_plot/helpers/html5fbfront.php');

class PlotModelPublication extends JModelLegacy
{
    public function getBook()
    {
        $user_id = plotUser::factory()->id;

        $id = JRequest::getInt('bookId');;
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('`pub`.*')
            ->select(Foundry::user()->id . ' AS `uid`')
            ->select('`res`.`width`, `res`.`height`')
            ->select('`cp`.`count_points`')
            ->select('`c`.`c_category`')
            ->select('`q`.`id_quiz`')
            ->select('`bc`.`author` AS pay_author, bc.text')
            ->select('`bp`.`finished`')
            ->select('`up`.`params`')
            ->from('`#__html5fb_publication` AS `pub`')
            ->leftJoin('`#__html5fb_resolutions` AS `res` ON `pub`.`c_resolution_id` = `res`.`id`')
            ->leftJoin('`#__plot_count_points` AS `cp` ON `pub`.`c_id` = `cp`.`entity_id` AND `cp`.`entity`="book"')
            ->leftJoin('`#__html5fb_category` AS `c` ON `pub`.`c_category_id` = `c`.`c_id`')
            ->leftJoin('`#__plot_book_quiz_map` AS `q` ON `q`.id_pub=`pub`.`c_id`')
            ->innerJoin('`#__plot_book_cost` AS `bc` ON `bc`.book_id=`pub`.`c_id`')
            ->leftJoin('`#__plot_user_pages` AS `up` ON `up`.`book_id` = `pub`.`c_id` AND `up`.`user_id`=' . (int)$user_id)
            ->leftJoin('`#__plot_books_paid` AS `bp` ON `bp`.`book_id` = `pub`.`c_id` AND `bp`.`child_id`=' . (int)$user_id)
            ->where('`pub`.`c_id` = ' . (int)$id)
            ->where('`pub`.published=1');
        $db->setQuery($query);

        $row = $db->loadObject();

        if ($row) {
            $row->popupWidth = $row->width * 2 + 66;
            $row->popupHeight = $row->height + 100;
            $thumbnailPath = JPATH_SITE . '/media/com_html5flippingbook' . '/thumbs/' . $row->c_thumb;
            if ($row->c_thumb == "" || !is_file($thumbnailPath)) {
                $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
            } else {
                if (file_exists(JPATH_BASE . '/media/com_html5flippingbook' . '/thumbs/thimb_' . $row->c_thumb)) {
                    $row->thumbnailUrl = JURI::root() . "media/com_html5flippingbook/thumbs/thimb_" . $row->c_thumb;
                } else {
                    $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
                }
            }
            $row->price = $this->bookPrice($id, $user_id);
            $row->publicationLink = 'index.php?option=com_html5flippingbook&view=publication&id=' . $row->c_id;
            $row->new = 0;
            if ((int)$row->finished == 1) {
                $row->percent = 100;
            } else {
                if ($row->params) {
                    $params = json_decode($row->params, true);
                    $count_pages = PlotHelper::getCountPages($row->c_id);
                    $row->percent = round(PlotHelper::getPersent($count_pages, count($params)));

                    if ($row->percent > 100) {
                        $row->percent = 90;
                    }
                } else {
                    $row->percent = 0;
                }
            }
        }

        return $row;
    }

    public function buyBook($bookId, $children_id)
    {
        $count_children = count($children_id);

        for ($i = 0; $i < $count_children; $i++) {
            $bookToDb = new stdClass();

            $bookToDb->parent_id = Foundry::user()->id;

            $bookToDb->child_id = (int)$children_id[$i];

            $bookToDb->book_id = (int)$bookId;

            $bookToDb->read = -1;

            $this->_db->insertObject('#__plot_books', $bookToDb);
            Foundry::points()->assign('buy.book', 'com_plot', Foundry::user()->id);
        }

        return true;

    }

    public function bookPrice($book_id, $child_id)
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('finished_price')
            ->from('`#__plot_books_paid`')
            ->where('book_id = ' . (int)$book_id)
            ->where('child_id = ' . (int)$child_id)
            ->where('finished = 0');
        $db->setQuery($query);
        $row = $db->loadResult();
        return $row;
    }

    public function ratingBay($book_id)
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('COUNT(*)')
            ->from('`#__plot_books_paid`')
            ->where('book_id = ' . (int)$book_id);
        $db->setQuery($query);
        $row = $db->loadResult();
        return $row;

    }

    public function ratingRead($book_id)
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('COUNT(*)')
            ->from('`#__plot_books_paid`')
            ->where('book_id = ' . (int)$book_id)
            ->where('finished = 1');
        $db->setQuery($query);
        $row = $db->loadResult();
        return $row;
    }

    public function getBookAuthorId($bookId)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`p`.author')
            ->from('`#__plot_book_cost` AS `p`')
            ->where('`p`.`book_id` = ' . (int)$bookId);
        $author = $db->setQuery($query)->loadResult();
        return $author;
    }

    public function addPercentOfBookCostToAuthor($bookId, $amountForComplete)
    {

        $bookAuthor = (int)$this->getBookAuthorId($bookId);
        if ($bookAuthor) {
            $courseOwner = plotUser::factory($bookAuthor);
            $book = new plotBook($bookId);
            $buyerCosts = $book->getBuyerCosts($amountForComplete);
            $courseOwner->addMoney($buyerCosts['author']);
        }

        return true;
    }

    public function getBookFiles()
    {
        $data = array();
        $db = $this->_db;
        $book_id = JRequest::getInt('bookId');
        $query = $db->getQuery(true)
            ->clear()
            ->select('f.*')
            ->from('`#__plot_books_files` AS `f`')
            ->where('`f`.`book_id` = ' . (int)$book_id)
            ->order('f.type ASC');
        $db->setQuery($query);
        $files = $db->loadObjectList();

        foreach ($files AS $item) {
            if ($item->type != 'audio' && $item->type != 'url') {
                $data[] = $item;
            }
        }
        foreach ($files AS $item) {
            if ($item->type == 'audio' ) {
                $data[] = $item;
            }elseif($item->type == 'url'){
                $data[] = $item;
            }

        }

        return  $data;

    }

    public function bookReadedReplaceText($ids, $msg)
    {
        if ($ids) {
            $my_arr = array();
            $count_ids = count($ids);
            for ($i = 0; $i < $count_ids; $i++) {
                $str = explode('-', $ids[$i]);

                $my_arr[$i]['book'] = abs(filter_var($str[0], FILTER_SANITIZE_NUMBER_INT));
                $my_arr[$i]['user'] = abs(filter_var($str[1], FILTER_SANITIZE_NUMBER_INT));
            }

            if ($my_arr) {


                foreach ($my_arr AS $key => $item) {
                    $bookObk = new plotBook((int)$item['book']);
                    $book = '<a href="' . JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $item['book']) . '">' . $bookObk->c_title . '</a>';
                    $user = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $item['user']) . '">' . plotUser::factory($item['user'])->name . '</a>';
                    $data[$key] = new stdClass();
                    $data[$key]->msg = JText::sprintf($msg, $user, $book);


                }

                return $data;
            }

        }


    }

    public function bookBuyReplaceText($ids, $msg)
    {
        $my=plotUser::factory();
        if ($ids) {
            $my_arr = array();
            $count_ids = count($ids);
            for ($i = 0; $i < $count_ids; $i++) {
                $str = explode('-', $ids[$i]);

                $my_arr[$i]['book'] = abs(filter_var($str[0], FILTER_SANITIZE_NUMBER_INT));
                $my_arr[$i]['user'] = abs(filter_var($str[1], FILTER_SANITIZE_NUMBER_INT));
            }

            if ($my_arr) {


                foreach ($my_arr AS $key => $item) {
                    $bookObk = new plotBook((int)$item['book']);
                    $book = '<a href="' . JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $item['book']) . '">' . $bookObk->c_title . '</a>';
                    $user = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $item['user']) . '">' . plotUser::factory($item['user'])->name . '</a>';
                    $data[$key] = new stdClass();
                    if((int)$bookObk->book_author==$my->id){
                        $data[$key]->msg = JText::sprintf(JText::_('COM_PLOT_BUY_BOOK_EMAIL_AUTHOR'), $user, $book);
                    }else{
                        $data[$key]->msg = JText::sprintf($msg, $user, $book);
                    }



                }

                return $data;
            }

        }


    }

}
