<?php defined('_JEXEC') or die('Restricted access');


class PlotModelPortfolio extends JModelAdmin
{
    protected $text_prefix = 'com_plot';

    //----------------------------------------------------------------------------------------------------
    public function getTable($type = 'portfolios', $prefix = 'PlotTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    //----------------------------------------------------------------------------------------------------
    public function getItem($pk = null)
    {
        return parent::getItem($pk);
    }

    //----------------------------------------------------------------------------------------------------
    protected function loadFormData()
    {
        $data = JFactory::getApplication()->getUserState('com_plot.edit.portfolio.data', array());

        if (empty($data)) {
            $data = $this->getItem();
        }

        return $data;
    }

    //----------------------------------------------------------------------------------------------------
    public function getForm($data = array(), $loadData = true)
    {
        $app = JFactory::getApplication();

        $form = $this->loadForm('com_plot.portfolios', 'portfolio', array('control' => 'jform', 'load_data' => $loadData));

        return (empty($form) ? false : $form);
    }

    //----------------------------------------------------------------------------------------------------
    public function delete(&$pks)
    {
        $dispatcher = JEventDispatcher::getInstance();
        $pks = (array)$pks;
        $table = $this->getTable();

        // Include the content plugins for the on delete events.
        JPluginHelper::importPlugin('content');

        // Iterate the items to delete each one.
        foreach ($pks as $i => $pk) {

            if ($table->load($pk)) {

                $context = $this->option . '.' . $this->name;

                // Trigger the onContentBeforeDelete event.
                $result = $dispatcher->trigger($this->event_before_delete, array($context, $table));

                if (in_array(false, $result, true)) {
                    $this->setError($table->getError());
                    return false;
                }

                if (!$table->delete($pk)) {
                    $this->setError($table->getError());
                    return false;
                }

                // Trigger the onContentAfterDelete event.
                $dispatcher->trigger($this->event_after_delete, array($context, $table));


            } else {
                $this->setError($table->getError());
                return false;
            }
        }

        // Clear the component's cache
        $this->cleanCache();

        return true;
    }

    public function save($data)
    {
        $table = $this->getTable();
        if ($data['id'] == 0) {
            $data['date'] = Foundry::date()->toMySQL();
        }
        if (!$table->bind($data)) {
            $this->setError($table->getError());
            return false;
        }

        // Bind the data.
        if (!$table->bind($data)) {
            $this->setError($table->getError());
            return false;
        }

        // Check the data.
        if (!$table->check()) {
            $this->setError($table->getError());
            return false;
        }

        // Store the data.
        if (!$table->store()) {
            $this->setError($table->getError());
            return false;
        } else {
            $app = JFactory::getApplication();
            $app->setUserState('com_plot.default.portfolio.data', $data);
        }

        $this->saveImages($table->id);

        return true;
    }

    public function saveImages($id)
    {

        $db = $this->_db;
        $rEFileTypes = "/^\.(jpg|jpeg|gif|png|bmp|xcf|odg){1}$/i";

        if (!JFolder::exists(JPATH_SITE . "/images/com_plot")) {

            JFolder::create(JPATH_SITE . "/images/com_plot");

        }
        if (!JFolder::exists(JPATH_SITE . "/images/com_plot/portfolio")) {

            JFolder::create(JPATH_SITE . "/images/com_plot/portfolio");

        }

        if (!JFolder::exists(JPATH_SITE . "/images/com_plot/portfolio/" . (int)plotUser::factory()->id)) {

            JFolder::create(JPATH_SITE . "/images/com_plot/portfolio/" . (int)plotUser::factory()->id);

        }


        if (isset($_FILES['img']) && $_FILES['img']["tmp_name"]) {
            $beaverfiles = JRequest::getVar('img', array(), 'files');
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot')) {

                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot');

            }
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio')) {

                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio');

            }
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio' . DIRECTORY_SEPARATOR . (int)plotUser::factory()->id)) {

                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio' . DIRECTORY_SEPARATOR . (int)plotUser::factory()->id);

            }


            $ext = JFile::getExt($beaverfiles['name']);

            $new_name = md5(time() . $beaverfiles['name']) . '.' . $ext;

            if (preg_match($rEFileTypes, strrchr($new_name, '.'))) {

                if (JFile::upload($beaverfiles['tmp_name'], JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio' . DIRECTORY_SEPARATOR . (int)plotUser::factory()->id . DIRECTORY_SEPARATOR . $new_name)) {

                    if (JFile::exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio' . DIRECTORY_SEPARATOR . (int)plotUser::factory()->id . DIRECTORY_SEPARATOR . $new_name)) {

                        JFile::move($beaverfiles["tmp_name"], JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio' . DIRECTORY_SEPARATOR . (int)plotUser::factory()->id . DIRECTORY_SEPARATOR . $new_name);

                        chmod(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'portfolio' . DIRECTORY_SEPARATOR . (int)plotUser::factory()->id . DIRECTORY_SEPARATOR . $new_name, 0644);
                        $query = $db->getQuery(true)
                            ->update('`#__plot_portfolio`')
                            ->set('`img` = "' . $new_name . '"')
                            ->where('`id` = ' . (int)$id);
                        $db->setQuery($query);
                        try {
                            $db->execute();
                        } catch (RuntimeException $e) {
                            $this->setError($e->getMessage());
                            return false;
                        }
                    }
                }

            }

        }
    }

    public function deleteImg($id)
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('img')
            ->from('`#__plot_portfolio`')
            ->where('id = ' . (int)$id);
        $db->setQuery($query);
        $src = $db->loadResult();
        if (file_exists(JURI::root() . 'images/com_plot/portfolio/' . (int)plotUser::factory()->id . '/' . $src)) {
            unlink(JURI::root() . 'images/com_plot/portfolio/' . (int)plotUser::factory()->id . '/' . $src); // Delete now
        }
        $query = $db->getQuery(true)
            ->update('`#__plot_portfolio`')
            ->set('`img` =""')
            ->where('`id` = ' . (int)$id);

        $db->setQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }
        return true;
    }
}