<?php

defined('_JEXEC') or die('Restricted access');

class PlotModelReadbook extends JModelLegacy
{
    public function getBook()
    {
        $id = JRequest::getInt('bookId');
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('`pub`.*')
            ->select('`up`.`params`')
            ->select('`b`.`read`')
            ->select(Foundry::user()->id . ' AS `uid`')
            ->select('`res`.`width`, `res`.`height`')
            ->from('`#__html5fb_publication` AS `pub`')
            ->leftJoin('`#__html5fb_resolutions` AS `res` ON `pub`.`c_resolution_id` = `res`.`id`')
            ->leftJoin('`#__plot_user_pages` AS `up` ON `up`.`book_id` = `pub`.`c_id` AND `up`.`user_id`=' . (int)Foundry::user()->id)
            ->leftJoin('`#__plot_books` AS `b` ON `b`.`book_id` = `pub`.`c_id` AND `b`.`child_id`=' . (int)Foundry::user()->id)
            ->where('`pub`.`c_id` = ' . (int)$id);
        $db->setQuery($query);
        return $db->loadObject();
    }

    public function getCheckBook()
    {
        $bookId = JRequest::getInt('bookId');
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('`b`.book_id')
            ->from('`#__plot_books` AS `b`')
            ->where('`b`.`book_id` = ' . (int)$bookId)
            ->where('`b`.`child_id` = ' . (int)Foundry::user()->id)
            ->where('`b`.`read` !=1');
        $db->setQuery($query);

        if ($db->loadObject()) {
            return true;
        }
        return false;
    }

    public function getQuizId()
    {
        $bookId = JRequest::getInt('bookId');
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('`b`.id_quiz')
            ->from('`#__plot_book_quiz_map` AS `b`')
            ->where('`b`.`id_pub` = ' . (int)$bookId);

        $db->setQuery($query);

        return $db->loadResult();
    }

    public function getBookPoints()
    {
        $bookId = JRequest::getInt('bookId');
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('count_points')
            ->from('`#__plot_count_points`')
            ->where('entity_id = ' . (int)$bookId)
            ->where('entity = "book"');
        $db->setQuery($query);
        return (int)$db->loadResult();
    }

    public function getBookPrice()
    {
        $bookId = JRequest::getInt('bookId');
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('finished_price')
            ->from('`#__plot_books_paid`')
            ->where('book_id = ' . (int)$bookId)
            ->where('child_id = ' . (int)Foundry::user()->id)
            ->where('finished = 0');
        $db->setQuery($query);
        return (int)$db->loadResult();
    }

}
