<?php
defined('_JEXEC') or die('Restricted access');

class PlotModelCourse extends JModelLegacy
{

    public function getCourse($id)
    {
        $course = new plotCourse($id);
        return $course;
    }

    public function insertPaidCourseIfNotExist(stdClass $paidCourse)
    {
        if (!$this->isPaidCourseExist($paidCourse->parent_id, $paidCourse->child_id, $paidCourse->course_id)) {
            $this->_db->insertObject('#__plot_courses_paid', $paidCourse, 'id');
            $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
            fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__  ." insertPaidCourseIfNotExist true");
            fclose($fp);
            return true;
        }
        $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
        fwrite($fp,  "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__  ." insertPaidCourseIfNotExistfalse");
        fclose($fp);
        return false;
    }

    public function isPaidCourseExist($parentId, $childId, $courseId)
    {
        $query = "SELECT `id` FROM `#__plot_courses_paid` WHERE `parent_id` = " . (int)$parentId . " AND `child_id` = " . (int)$childId . " AND `course_id` = " . (int)$courseId;
        $result = $this->_db->setQuery($query)->loadResult();
        $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
        fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__  ." isPaidCourseExist " .(int)$result);
        fclose($fp);
        return $result;
    }

    public function addUsersToJoomlaLMSCourse($userIds, $courseId)
    {
        $this->requireJoomlaLMSFiles();

        $_JLMS_PLUGINS = &JLMSFactory::getPlugins();
        $JLMS_CONFIG = JLMSFactory::getConfig();
        $db = JFactory::getDBO();

        $msg = '';
        $group_id = 0;

        $role_id = plotGlobalConfig::getVar('joomlaLMSRoleStudentId');

        $p_s = 0;
        $d_s = '0000-00-00';
        $p_e = 0;
        $d_e = '0000-00-00';

        $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
        fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__." userIds " .json_encode($userIds)." courseId ".$courseId);
        fclose($fp);

        if ($userIds && $courseId) {
            global $license_lms_users;

            foreach ($userIds as $user_id) {
                $do_add = false;

                $query = "SELECT count(*) FROM #__lms_users_in_groups WHERE course_id = '" . $courseId . "' AND user_id = '" . $user_id . "'";
                $db->SetQuery($query);
                $c = $db->LoadResult();
                $query = "SELECT count(*) FROM #__lms_user_courses WHERE course_id = '" . $courseId . "' AND user_id = '" . $user_id . "'";
                $db->SetQuery($query)->loadResult();
                $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
                fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__." c " .(int)$c);
                fclose($fp);
                if ((int)$c == 0) {
                    $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
                    fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__." license_lms_users ".(int)$license_lms_users);
                    fclose($fp);
                    if ($license_lms_users) {
                        $query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
                        $db->SetQuery($query);
                        $total_students = $db->LoadResult();
                        $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
                        fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__."  total_students ".intval($total_students)."  license_lms_users".intval($license_lms_users));
                        fclose($fp);

                        if (intval($total_students) < intval($license_lms_users)) {
                            $do_add = true;
                        }
                        if (!$do_add) {
                            $query = "SELECT count(*) FROM #__lms_users_in_groups WHERE user_id = '" . $user_id . "'";
                            $db->SetQuery($query);
                            $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
                            fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__ ." SELECT count(*) FROM #__lms_users_in_groups ".$db->LoadResult());
                            fclose($fp);
                            if ($db->LoadResult()) {
                                $do_add = true;
                            }
                        }
                    } else {
                        $do_add = true;
                    }
                    $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
                    fwrite($fp, "\r\n ".date('Y-m-d H:i:s')."do_add line 103 ".$do_add);
                    fclose($fp);
                    if ($do_add) {
                        $query = "INSERT INTO #__lms_users_in_groups"
                            . "\n (course_id, group_id, user_id, role_id, teacher_comment, publish_start, start_date, publish_end, end_date, enrol_time)"
                            . "\n VALUES"
                            . "\n ('" . $courseId . "', '" . $group_id . "', '" . $user_id . "', '" . $role_id . "', '', $p_s, '" . $d_s . "', $p_e, '" . $d_e . "', '" . JLMS_gmdate() . "')";
                        $db->SetQuery($query);

                        if ($db->query()) {

                            $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
                            fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__ ." insert ".true);
                            fclose($fp);
                            //send email to import user						
                            $course = new stdClass();
                            $course->course_alias = '';
                            $course->course_name = '';

                            $query = "SELECT course_name, name_alias FROM #__lms_courses WHERE id = '" . $courseId . "'";
                            $db->setQuery($query);
                            $course = $db->loadObject();

                            $user_group = new stdClass();
                            $user_group->ug_name = '';

                            $query = "SELECT ug_name  FROM #__lms_usergroups WHERE id = '" . $group_id . "'";
                            $db->setQuery($query);
                            $user_group = $db->loadObject();

                            $params['user_id'] = $user_id;
                            $params['course_id'] = $courseId;

                            $user = new stdClass();
                            $user->email = '';
                            $user->name = '';
                            $user->username = '';

                            $query = "SELECT email, name, username FROM #__users WHERE id = '" . $user_id . "'";
                            $db->setQuery($query);
                            $user = $db->loadObject();

                            $params['markers']['{email}'] = $user->email;
                            $params['markers']['{name}'] = $user->name;
                            $params['markers']['{username}'] = $user->username;
                            $params['markers']['{coursename}'] = $course->course_name;//( $course->course_alias )?$course->course_alias:$course->course_name;

                            if ($user_group)
                                $params['markers']['{groupname}'] = $user_group->ug_name;

                            $Itemid = $JLMS_CONFIG->getItemid();
                            $params['markers']['{courselink}'] = JURI::root() . "index.php?option=com_joomla_lms&Itemid=$Itemid&task=details_course&id=$courseId";

                            $params['action_name'] = 'OnEnrolmentInCourseByJoomlaAdmin';

                            $_JLMS_PLUGINS->loadBotGroup('emails');
                            $_JLMS_PLUGINS->loadBotGroup('system');
                            $params['course_ids'] = array($courseId);
                            $plugin_result_array = $_JLMS_PLUGINS->trigger('OnEnrolmentInCourseByJoomlaAdmin', array(& $params));

                        }

                        $user_info = new stdClass();
                        $user_info->user_id = $user_id;
                        $user_info->group_id = $group_id;
                        $user_info->course_id = $courseId;
                        $_JLMS_PLUGINS->loadBotGroup('user');
                        $_JLMS_PLUGINS->trigger('onCourseJoin', array($user_info));
                    } else {
                        $msg = _JLMS_USERS_MSG_USR_L_EXCEEDED;
                    }
                }
            }
        }

        return true;
    }

    private function requireJoomlaLMSFiles()
    {
        if (!defined('_JOOMLMS_FRONT_HOME')) {
            define('_JOOMLMS_FRONT_HOME', JPATH_BASE . '/components/com_joomla_lms');
        }

        // no direct access
        if (!defined('_VALID_MOS') && !defined('_JEXEC')) {
            die('Restricted access');
        }
        if (!defined('_JLMS_EXEC')) {
            define('_JLMS_EXEC', 1);
        }
        if (!defined('_JOOMLMS_COMP_NAME')) {
            define('_JOOMLMS_COMP_NAME', 'JoomlaLMS');
        }
        $doc = JFactory::getDocument();//for 'addStyleSheet'
        global $option;
        $option = 'com_joomla_lms';
        if (!defined('DS')) {
            define('DS', DIRECTORY_SEPARATOR);
        }
        if (!defined('_JOOMLMS_FRONT_HOME')) {
            define('_JOOMLMS_FRONT_HOME', JPATH_SITE . DS . 'components' . DS . 'com_joomla_lms');
        }
        require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "component.legacy.php");
        require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.factory.php");

        require_once(JPATH_ROOT . "/components/com_joomla_lms/joomla_lms.main.php");
        require_once(_JOOMLMS_FRONT_HOME . "/includes/component.legacy.php");
        require_once(_JOOMLMS_FRONT_HOME . "/includes/classes/lms.factory.php");

    }

    public function markCoursesForUsersAsPaid($choosedUsersIds, $courseId, $price, $payerId)
    {

        if (!$choosedUsersIds) {
            return false;
        }
        foreach ($choosedUsersIds AS $userId) {
            $paidCourse = new stdClass();
            $paidCourse->parent_id = $payerId;
            $paidCourse->child_id = $userId;
            $paidCourse->course_id = $courseId;
            $paidCourse->paid_date = JFactory::getDate()->toSql();
            $paidCourse->finished_price = $price;
            $paidCourse->finished = 0;


            $this->insertPaidCourseIfNotExist($paidCourse);
            $conversationModel = JModelLegacy::getInstance('conversations', 'PlotModel');


            $courseObj=new plotCourse($paidCourse->course_id);


            $config = JFactory::getConfig();
            $sender = array(
                $config->get('mailfrom'),
                $config->get('fromname')
            );

            $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
            fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__." parent id ".$payerId."\n");
            fclose($fp);



            $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' .  $payerId) . '">' . plotUser::factory($payerId)->name . '</a>';
            $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=course&id=' . $paidCourse->course_id) . '">' . $courseObj->course_name . '</a>';


            //send email child
            if(plotUser::factory($paidCourse->child_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($paidCourse->child_id)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                $mailer = JFactory::getMailer();
                $mailer->setSender($sender);
                $mailer->addRecipient(plotUser::factory($paidCourse->child_id)->email);
                $mailer->setSubject(JText::_('COM_PLOT_BUY_COURSE_EMAIL_SUBJECT'));
                $mailer->isHTML(true);
                $mailer->setBody(JText::sprintf("COM_PLOT_BUY_COURSE_EMAIL", $actor, $target));
                $mailer->Send();
            }


            //send email author
            if($courseObj->owner_id && (plotUser::factory($courseObj->owner_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($courseObj->owner_id)->getSocialFieldData('UNSUBSCRIBE')==NULL)){
                $mailer = JFactory::getMailer();
                $mailer->setSender($sender);
                $mailer->addRecipient(plotUser::factory($courseObj->owner_id)->email);
                $mailer->setSubject(JText::_('COM_PLOT_BUY_COURSE_EMAIL_SUBJECT'));
                $mailer->isHTML(true);
                $mailer->setBody(JText::sprintf("COM_PLOT_BUY_COURSE_EMAIL_AUTHOR", $actor, $target));
                $mailer->Send();
            }


            //send notification child
            if(plotUser::factory($paidCourse->child_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($paidCourse->child_id)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                $conversationModel->sendMessageSystem($paidCourse->child_id, 'plot-course-buy-' . $paidCourse->course_id . '-' . $payerId);
            }

            //send notification owner
            if($courseObj->owner_id && (plotUser::factory($courseObj->owner_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($courseObj->owner_id)->getSocialFieldData('UNSUBSCRIBE')==NULL)){
               $conversationModel->sendMessageSystem($courseObj->owner_id, 'plot-course-buy-' . $paidCourse->course_id . '-' . $paidCourse->parent_id);
            }


        }
        return true;
    }

    public function addPercentOfCourseCostToAuthor($courseId, $amountForComplete)
    {
        $course = new plotCourse($courseId);
        $courseOwner = plotUser::factory($course->owner_id);

        $buyerCosts = $course->getBuyerCosts($amountForComplete);
        $courseOwner->addMoney($buyerCosts['author']);
        $courseOwner->addAuthorMoneyInformation('course', $courseId, $buyerCosts['author']);
        return true;
    }


    public function saveCourseVideo($courseId, $data)
    {
        $exist = $this->isExist($courseId);

        if ($exist) {
            $this->update($courseId, $data, $exist);
        } else {
            $this->insert($courseId, $data);
        }
    }

    public function isExist($courseId)
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('id')
            ->from('#__plot_course_video')
            ->where('course_id=' . $db->quote($courseId))
            ->where('user_id=' . $db->quote(plotUser::factory()->id));

        $result = $db->setQuery($query)->loadResult();
        return $result;
    }

    private function insert($courseId, $data)
    {
        $db = JFactory::getDbo();
        $my = plotUser::factory();
        $buyer = $this->whoBoughtBook($courseId, $my->id);
        $course = new stdClass();
        $course->user_id = $my->id;
        $course->course_id = $courseId;
        $course->type= $data['type'] ? $db->escape($data['type']) : 'link';
        $course->title = $data['title'] ? $db->escape($data['title']) : '';
        $course->text = $data['desc'] ? $db->escape($data['desc']) : '';
        $course->status = 0;

        $course->reviewer = $buyer;
        $course->link = $data['link'];

        $db->insertObject('#__plot_course_video', $course);
        $course_video_id = $db->insertid();
        $video = new stdClass();
        $video->path = $data['link'];
        $video->type = 'course';
        $video->uid = $my->id;
        $video->title = $data['title'] ? $db->escape($data['title']) : '';
        $video->description = $data['desc'] ? $db->escape($data['desc']) : '';
        $video->date = Foundry::date()->toMySQL();
        $video->status = 0;
        $video->type = $data['type'];
        $db->insertObject('#__plot_video', $video);
        plotPoints::assign('add.coursevideo', 'com_plot', plotUser::factory()->id, $course_video_id);

        if(plotUser::factory($buyer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($buyer)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
            $conversationsModel = JModelLegacy::getInstance('conversations', 'PlotModel');
            $conversationsModel->sendMessageSystem($buyer, 'plot-coursevideo-' . $course_video_id);
            $mailer = JFactory::getMailer();
            $config = JFactory::getConfig();
            $sender = array(
                $config->get('mailfrom'),
                $config->get('fromname')
            );
            $courseObj = new plotCourse($courseId);
            $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $my->id) . '">' . $my->name . '</a>';
            $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=course&id=' . $courseId) . '">' . $courseObj->name . '</a>';
            $mailer->setSender($sender);
            $mailer->addRecipient(plotUser::factory($buyer)->email);
            $mailer->setSubject(JText::_('COM_PLOT_SEND_COURSE_SUBJECT'));
            $mailer->isHTML(true);

            $mailer->setBody(JText::sprintf("COM_PLOT_COURSE_VIDEO_WAS_ADDED", '<a href="javascript:void(0);" onclick="SqueezeBox.open(\'' . JRoute::_("index.php?option=com_plot&task=course.ajaxCourse") . '&id=' . $course_video_id . '\', {size:{x:744, y:500}, handler:\'iframe\'})">Видео</a>', $target, $actor));
            $mailer->Send();
        }
    }

    private function whoBoughtBook($courseId, $userId)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`p`.parent_id')
            ->from('`#__plot_courses_paid` AS `p`')
            ->where('`p`.`course_id` = ' . (int)$courseId)
            ->where('`p`.`child_id`=' . (int)$userId);

        $buyer = $db->setQuery($query)->loadResult();
        return $buyer;
    }

    private function update($courseId, $data, $course_video_id)
    {
        $db = JFactory::getDbo();
        $courseVideo = $this->getCourseVideoById($course_video_id);

        $this->updateVideoLink($courseVideo->link, $data);
        $course = new stdClass();
        $course->id = (int)$course_video_id;
        $course->course_id = $courseId;
        $course->title = $data['title'] ? $db->escape($data['title']) : '';
        $course->text = $data['desc'] ? $db->escape($data['desc']) : '';
        $course->link = $data['link'];
        $course->type = $data['type'];
        $course->date = Foundry::date()->toMySQL();

        $db->updateObject('#__plot_course_video', $course, 'id');

        $bauer = $courseVideo->reviewer;

        if(plotUser::factory($bauer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($bauer)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
            $conversationsModel = JModelLegacy::getInstance('conversations', 'PlotModel');
            $conversationsModel->sendMessageSystem($bauer, 'plot-coursevideo-' . $course_video_id);
            $mailer = JFactory::getMailer();
            $config = JFactory::getConfig();
            $sender = array(
                $config->get('mailfrom'),
                $config->get('fromname')
            );
            $courseObj = new plotCourse($courseId);
            $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . plotUser::factory()->id) . '">' . plotUser::factory()->name . '</a>';
            $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=course&id=' . $courseId) . '">' . $courseObj->name . '</a>';
            $mailer->setSender($sender);
            $mailer->addRecipient(plotUser::factory($bauer)->email);
            $mailer->setSubject(JText::_('COM_PLOT_SEND_COURSE_SUBJECT'));
            $mailer->isHTML(true);
            $mailer->setBody(JText::sprintf("COM_PLOT_COURSE_VIDEO_WAS_ADDED", '<a href="javascript:void(0);" onclick="SqueezeBox.open(\'' . JRoute::_("index.php?option=com_plot&task=course.ajaxCourse") . '&id=' . $course_video_id . '\', {size:{x:744, y:500}, handler:\'iframe\'})">Видео</a>', $target, $actor));
            $mailer->Send();
        }
    }

    public function getCourseVideoById($course_video_id)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select('`p`.*')
            ->from('`#__plot_course_video` AS `p`')
            ->where('`p`.`id`=' . (int)$course_video_id);

        $course = $db->setQuery($query)->loadObject();
        return $course;
    }

    public function getCourses($limit = array(), $course_id)
    {
        $data = array();
        $db = $this->_db;
        $user = Foundry::user();
        $query = $db->getQuery(true)
            ->clear()
            ->select('`e`.*')
            ->from('`#__plot_course_video` AS `e`')
            ->where('`e`.`course_id`=' . (int)$course_id)
            ->where('(`e`.`status`=1 OR ((`e`.`status`=0 AND `e`.`reviewer`=' . $user->id . ') OR (`e`.`status`=0 AND `e`.`user_id`=' . $user->id . ')))');
        $query->order($db->escape('e.id'));

        if (isset($limit['offset']) && isset($limit['limit'])) {
            $courses = $this->_db->setQuery($query, $limit['offset'], $limit['limit'])->loadObjectList();
        } else {
            $courses = $this->_db->setQuery($query)->loadObjectList();
        }

        foreach ($courses AS $c) {
            if($c->type=='link'){
                if (strripos($c->link, 'youtube.com/') !== FALSE) {
                    $youtubeNumber = substr($c->link, strripos($c->link, '?v=') + 3);
                    if (($pos = strpos($youtubeNumber, '&')) !== FALSE) {
                        $youtubeNumber = substr($youtubeNumber, 0, $pos);
                        $c->img = JUri::root() . 'media/com_plot/coursevideo/' . $c->user_id . '/thumb/' . $youtubeNumber . '.jpg';
                    } else {
                        $c->img = JUri::root() . 'media/com_plot/coursevideo/' . $c->user_id . '/thumb/' . $youtubeNumber . '.jpg';

                    }
                } else {

                    $c->img = JUri::root() . 'images/com_plot/def_video.jpg';
                }
            }else{
                $fileNameNoExtension = preg_replace("/\.[^.]+$/", "", $c->link);
                if (file_exists(JPATH_BASE . '/media/com_plot/coursevideo/' .$c->user_id. '/thumb/'.$fileNameNoExtension.'.jpg')) {
                    $c->img = JURI::root() . 'media/com_plot/coursevideo/' .$c->user_id. '/thumb/'.$fileNameNoExtension.'.jpg';
                } else {
                    $c->img = JURI::root() . "images/com_plot/def_video.jpg";
                }


            }

        }

        $data['items'] = $courses;
        $data['countItems'] = $this->_db->setQuery('SELECT FOUND_ROWS()')->loadResult();

        return $data;
    }

    public function courseVideoUpdateStatus($id)
    {
        $db = JFactory::getDbo();
        $essay = new stdClass();
        $essay->id = (int)$id;
        $essay->status = 1;
        $db->updateObject('#__plot_course_video', $essay, 'id');
    }

    public function generateCertificate($uid, $course_id)
    {

        if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images/com_plot/photos')) {

            mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images/com_plot/photos');

        }

        if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images/com_plot/photos/' . $uid)) {

            mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images/com_plot/photos/' . $uid);
        }

        if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images/com_plot/photos/' . $uid . '/original')) {

            mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images/com_plot/photos/' . $uid . '/original');
        }


        $new_img_name = md5(time() . $uid);
        $user = plotUser::factory($uid);
        $course = new plotCourse($course_id);
        $text = '"' . $course->course_name . '"';
        $im = imagecreatefromjpeg(plotGlobalConfig::getVar('coursePicture'));
        // Allocate A Color For The Text

        $green = imagecolorallocate($im, 7, 178, 76);
        $light_green = imagecolorallocate($im, 113, 172, 152);
        $brown = imagecolorallocate($im, 114, 80, 45);
        $cert_date = date("d.m.Y");
        // Set Path to Font File
        $font_path = 'media/tahoma.ttf';
        // Print Text On Image
        $course_lenth = strlen($text);

        imagettftext($im, 55, 0, 450, 1530, $green, $font_path, $user->name);
        if ($course_lenth <= 50) {
            imagettftext($im, 45, 0, 850, 2200, $light_green, $font_path, $text);
        } else {
            imagettftext($im, 45, 0, 650, 2200, $light_green, $font_path, $text);
        }
        imagettftext($im, 45, 0, 300, 2700, $brown, $font_path, $cert_date);

        // imagettftext($im, 25, 0, 180, 720, $white, $font_path, $user_score);
        imagepng($im, 'images/com_plot/photos/' . $uid . '/original/' . $new_img_name . '.png');
        imagedestroy($im);

        $this->insertEssayInSocial($new_img_name . '.png', $uid);

    }

    private function insertEssayInSocial($img_src, $uid)
    {

        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->select('`a`.*')
            ->from('`#__social_albums` AS `a`')
            ->where('`a`.`uid` = ' . (int)$uid)
            ->where('`a`.`type` = "plot-certificate" ');
        $db->setQuery($query);
        $album = $db->loadObject();

        if (!$album) {
            $album = Foundry::table('Album');
            $album->uid = $uid;
            $album->type = 'plot-certificate';
            $album->created = Foundry::date()->toMySQL();
            $album->ordering = 0;
            $album->assigned_date = Foundry::date()->toMySQL();
            $db->insertObject('#__social_albums', $album);
            $albumId = $db->insertid();
        } else {
            $albumId = $album->id;
        }

        $album = Foundry::table('Album');
        $album->load($albumId);
        $photo = Foundry::table('Photo');
        // Set the creation date alias
        $photo->assigned_date = Foundry::date()->toMySQL();

        $date = Foundry::date()->toMySQL();
        $photo->created = $date;
        $image = Foundry::image();
        $image->load(JPATH_SITE . '/' . 'images' . '/' . 'com_plot' . '/' . 'photos' . '/' . $uid . '/' . 'original' . '/' . $img_src);

        $photo->uid = $uid;
        $photo->type = SOCIAL_TYPE_USER;
        $photo->album_id = $albumId;
        $photo->ordering = 0;
        $photo->state = SOCIAL_STATE_PUBLISHED;
        $photo->title = '';
        $photo->caption = '';

        // Set the creation date alias
        $photo->assigned_date = Foundry::date()->toMySQL();

        // Let's test if exif exists
        $exif = Foundry::get('Exif');
        $photo->cleanupTitle();

        // Detect the photo caption and title if exif is available.
        if ($exif->isAvailable() && $image->hasExifSupport()) {
            // Load the image
            $exif->load(JPATH_SITE . '/' . 'images' . '/' . 'com_plot' . '/' . 'photos' . '/' . $uid . '/' . 'original' . '/' . $img_src);
            $createdAlias = $exif->getCreationDate();
            if ($createdAlias) {
                $photo->assigned_date = $createdAlias;
            }

        }
        $state = $photo->store();

        // Push all the ordering of the photo down
        $photosModel = Foundry::model('photos');
        $photosModel->pushPhotosOrdering($albumId, $photo->id);

        // Detect location for the photo
        if ($exif->isAvailable() && $image->hasExifSupport()) {
            $exif->load(JPATH_SITE . '/' . 'images' . '/' . 'com_plot' . '/' . 'photos' . '/' . $uid . '/' . 'original' . '/' . $img_src);
            $locationCoordinates = $exif->getLocation();
            // Once we have the coordinates, we need to reverse geocode it to get the address.
            if ($locationCoordinates) {
                $geocode = Foundry::get('GeoCode');
                $address = $geocode->reverse($locationCoordinates->latitude, $locationCoordinates->longitude);
                $location = Foundry::table('Location');
                $location->loadByType($photo->id, SOCIAL_TYPE_PHOTO, $uid);
                $location->address = $address;
                $location->latitude = $locationCoordinates->latitude;
                $location->longitude = $locationCoordinates->longitude;
                $location->user_id = $uid;
                $location->type = SOCIAL_TYPE_PHOTO;
                $location->uid = $photo->id;
                $state = $location->store();
            }
            $photosModel->storeCustomMeta($photo, $exif);
        }

        // If album doesn't have a cover, set the current photo as the cover.
        if (!$album->hasCover()) {
            $album->cover_id = $photo->id;
            $album->store();
        }

        $photoLib = Foundry::get('Photos', $image);

        $storage = $photoLib->getStoragePath($album->id, $photo->id);
        $paths = $photoLib->create($storage);

        // Create metadata about the photos
        foreach ($paths as $type => $fileName) {
            $meta = Foundry::table('PhotoMeta');
            $meta->photo_id = $photo->id;
            $meta->group = SOCIAL_PHOTOS_META_PATH;
            $meta->property = $type;
            $meta->value = $storage . '/' . $fileName;
            $meta->store();
        }

        @unlink(JPATH_SITE . '/' . 'images' . '/' . 'com_plot' . '/' . 'photos' . '/' . $uid . '/' . 'thumb' . '/' . $img_src);
        @unlink(JPATH_SITE . '/' . 'images' . '/' . 'com_plot' . '/' . 'photos' . '/' . $uid . '/' . 'original' . '/' . $img_src);

        return;

    }


    private function  updateVideoLink($old_link, $data)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->update('`#__plot_video`')
            ->set("`path` = " . $db->quote($db->escape($data['link'])))
            ->set("`title` = " . $db->quote($db->escape($data['title'])))
            ->set("`description` = " . $db->quote($db->escape($data['desc'])))
            ->where('`uid` =' . plotUser::factory()->id)
            ->where('`path` ="' . $old_link . '"')
            ->where('`type`="course"');

        $db->setQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

    }

    public function replaceForConversation($ids)
    {

        if ($ids) {
            $my_arr = array();
            foreach ($ids AS $id) {
                $my_arr[] = abs(filter_var($id, FILTER_SANITIZE_NUMBER_INT));
            }
            if ($my_arr) {
                $data = array();
                $db = $this->_db;

                foreach ($my_arr AS $id) {
                    $query = $db->getQuery(true)
                        ->clear()
                        ->select('`e`.*')
                        ->from('`#__plot_course_video` AS `e`')
                        ->where('`e`.`id` =' . (int)$id);
                    $data[] = $this->_db->setQuery($query)->loadObject();

                }

                foreach ($data AS $course) {
                    if ($course) {
                        $courseObj = new plotCourse($course->course_id);
                        $course_name = '<a href="' . JRoute::_("index.php?option=com_plot&view=course&id=$course->id") . '">' . $courseObj->course_name . '</a>';
                        $user = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $course->user_id) . '">' . plotUser::factory($course->user_id)->name . '</a>';
                        $course->msg = JText::sprintf("COM_PLOT_COURSE_VIDEO_WAS_ADDED", '<a href="javascript:void(0);" onclick="SqueezeBox.open(\'' . JRoute::_("index.php?option=com_plot&task=course.ajaxCourse") . '&id=' . $course->id . '\', {size:{x:744, y:500}, handler:\'iframe\'})">Видео</a>', $course_name, $user);
                    }


                }
                return $data;
            }

        }

    }

    public function getBeforeHashtagsCourses($limit = array(), $bookId)
    {
        $data = array();
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('hashtag_id')
            ->from('`#__plot_before_hashtag_course_map`')
            ->where('course_id=' . (int)$bookId);

        $tags = $this->_db->setQuery($query)->loadColumn();
        $tags_str = implode(',', $tags);


        $tableName = '#__lms_courses';

        $query = "SELECT SQL_CALC_FOUND_ROWS DISTINCT `c`.*, `cats`.`c_category`, `pc`.*, (`pc`.`admin_min_cost` + `pc`.`author_min_cost`) AS `total_min_cost` FROM `$tableName` AS `c` "
            . "LEFT JOIN `#__lms_course_cats` AS `cats` ON (`c`.`cat_id`=`cats`.`id`) "
            . "LEFT JOIN `#__plot_courses` AS `pc` ON (`c`.`id` = `pc`.`id`) "
            . "LEFT JOIN `#__plot_age_course_map` AS `ages` ON (`ages`.`course_id` = `c`.`id` ) "
            . "LEFT JOIN `#__plot_tags` AS `tags` ON (`tags`.`entity` = 'course' AND `tags`.`entityId` = `c`.`id` ) "
            . "LEFT JOIN `#__plot_hashtag_course_map` AS `t` ON `t`.`course_id` = `c`.`id` "
            . "WHERE t.course_id!=" . (int)$bookId . " AND t.hashtag_id IN('" . $tags_str . "') "
            . "GROUP BY `c`.`id`";


        if (isset($limit['offset']) && isset($limit['limit'])) {
            $items = $this->_db->setQuery($query, $limit['offset'], $limit['limit'])->loadObjectList();
        } else {
            $items = $this->_db->setQuery($query)->loadObjectList();
        }

        foreach ($items AS $i => $course) {
            if (!$course->image) {
                $items[$i]->image = JUri::root() . 'templates/plot/img/blank300x200.jpg';
            }
        }

        $data = array();
        $data['items'] = $items;
        $data['countItems'] = $this->_db->setQuery('SELECT FOUND_ROWS()')->loadResult();

        return $data;


    }

    public function getAfterHashtagsCourses($limit = array(), $courseId)
    {
        $data = array();
        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('hashtag_id')
            ->from('`#__plot_after_hashtag_course_map`')
            ->where('course_id=' . (int)$courseId);

        $tags = $this->_db->setQuery($query)->loadColumn();
        $tags_str = implode(',', $tags);


        $tableName = '#__lms_courses';

        $query = "SELECT SQL_CALC_FOUND_ROWS DISTINCT `c`.*, `cats`.`c_category`, `pc`.*, (`pc`.`admin_min_cost` + `pc`.`author_min_cost`) AS `total_min_cost` FROM `$tableName` AS `c` "
            . "LEFT JOIN `#__lms_course_cats` AS `cats` ON (`c`.`cat_id`=`cats`.`id`) "
            . "LEFT JOIN `#__plot_courses` AS `pc` ON (`c`.`id` = `pc`.`id`) "
            . "LEFT JOIN `#__plot_age_course_map` AS `ages` ON (`ages`.`course_id` = `c`.`id` ) "
            . "LEFT JOIN `#__plot_tags` AS `tags` ON (`tags`.`entity` = 'course' AND `tags`.`entityId` = `c`.`id` ) "
            . "LEFT JOIN `#__plot_hashtag_course_map` AS `t` ON `t`.`course_id` = `c`.`id` "
            . "WHERE t.course_id!=" . (int)$courseId . " AND t.hashtag_id IN('" . $tags_str . "') "
            . "GROUP BY `c`.`id`";


        if (isset($limit['offset']) && isset($limit['limit'])) {
            $items = $this->_db->setQuery($query, $limit['offset'], $limit['limit'])->loadObjectList();
        } else {
            $items = $this->_db->setQuery($query)->loadObjectList();
        }
        //die(var_dump($limit['offset'].'-'.$limit['limit'].'<br>'));
        foreach ($items AS $i => $course) {
            if (!$course->image) {
                $items[$i]->image = JUri::root() . 'templates/plot/img/blank300x200.jpg';
            }
        }

        $data = array();
        $data['items'] = $items;
        $data['countItems'] = $this->_db->setQuery('SELECT FOUND_ROWS()')->loadResult();
        return $data;
    }


    public function courseReplaceText($ids, $text_const)
    {
        $my=plotUser::factory();
        if ($ids) {
            $my_arr = array();
            $count_ids = count($ids);
            for ($i = 0; $i < $count_ids; $i++) {
                $str = explode('-', $ids[$i]);

                $my_arr[$i]['course'] = abs(filter_var($str[0], FILTER_SANITIZE_NUMBER_INT));
                $my_arr[$i]['user'] = abs(filter_var($str[1], FILTER_SANITIZE_NUMBER_INT));
            }

            if ($my_arr) {


                foreach ($my_arr AS $key => $item) {
                    $courseObj = new plotCourse($item['course']);

                    $book = '<a href="' . JRoute::_('index.php?option=com_plot&view=course&id=' . $item['course']) . '">' . $courseObj->course_name . '</a>';
                    $user = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $item['user']) . '">' . plotUser::factory($item['user'])->name . '</a>';
                    $data[$key] = new stdClass();
                    if($courseObj->owner_id==(int)$my->id){
                       $data[$key]->msg = JText::sprintf(JText::_('COM_PLOT_BUY_COURSE_EMAIL_AUTHOR'), $user, $book);
                    }else{
                        $data[$key]->msg = JText::sprintf($text_const, $user, $book);
                    }



                }

                return $data;
            }



        }

    }

}
