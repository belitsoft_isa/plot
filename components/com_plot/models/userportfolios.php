<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');

class PlotModelUserportfolios extends JModelList
{

    //----------------------------------------------------------------------------------------------------
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'a.id',
                'a.title',
                'a.link'

            );
        }

        parent::__construct($config);
    }

    //----------------------------------------------------------------------------------------------------
    protected function populateState($ordering = null, $direction = null)
    {
        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        parent::populateState('a.id', 'asc');
    }

    //----------------------------------------------------------------------------------------------------
    protected function getListQuery()
    {
        $db = $this->_db;
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }

        $query = $db->getQuery(true)
            ->clear()
            ->select('a.*')
            ->from('`#__plot_portfolio` AS `a`')
            ->where('`a`.user_id=' . (int)$user->id);

        // Filter by search in name.

        $search = $this->getState('filter.search');

        if (!empty($search)) {
            $search = $db->Quote('%' . $db->escape($search, true) . '%');
            $query->where('`a`.`title` LIKE ' . $search);
        }
        $query->order($db->escape($this->state->get('list.ordering') . ' ' . $this->state->get('list.direction')));
        return $query;
    }


}
