<?php

defined('_JEXEC') or die('Restricted access');

class PlotModelProfileEdit extends JModelList
{

    public function getAllChildrenProfilesIds()
    {
        $query = "SELECT `user_id` FROM `#__social_profiles_maps` WHERE `profile_id` = " . plotGlobalConfig::getVar('childProfileId');
        $childrenIds = $this->_db->setQuery($query)->loadColumn();
        return $childrenIds;
    }

    public function addTag($tagId, $tagSmile = null, $tagTitle = '')
    {
        $db = Foundry::db();
        $tag = new stdClass();
        $tag->tagId = $tagId;
        $tag->entity = 'user';
        $tag->entityId = plotUser::factory()->id;
        $tag->smiley = $tagSmile;
        $tag->title = $tagTitle;
        $db->insertObject('#__plot_tags', $tag);
        $stream = Foundry::stream();
        $template = $stream->getTemplate();
        $template->setActor(plotUser::factory()->id, 'user');
        $template->setVerb('update');
        $template->setContext(plotUser::factory()->id, 'profiles');
        $template->setDate(Foundry::date()->toMySQL());
        $stream->add($template);


        plotPoints::assign('add.tag', 'com_plot', plotUser::factory()->id, $tagId);
        if ($tagTitle) {
            plotPoints::assign('describe.tag', 'com_plot', plotUser::factory()->id, $tagId);
        }
        if ($tagSmile) {
            plotPoints::assign('assess.tag', 'com_plot', plotUser::factory()->id, $tagId);
        }

        return true;
    }

    public function deleteTag($tag_id)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->clear()
            ->select('`i`.id, `i`.title')
            ->from('`#__k2_items` AS `i`')
            ->where('`i`.id=' . (int)$tag_id)
            ->group('`i`.id');
        $db->setQuery($query);
        $tag = $db->loadObject();

        $this->deletePointsForTitle($tag_id);
        $this->deletePointsForSmiley($tag_id);
        $this->deletePointsForTag();

        $query = $db->getQuery(true);
        $query->delete('#__plot_tags')
            ->where('tagId=' . (int)$tag_id)
            ->where('entity="user"')
            ->where('entityId=' . plotUser::factory()->id);
        $db->SetQuery($query);

        try {
            $db->execute();
        } catch (RuntimeException $e) {

            return false;
        }

        return $tag;
    }

    public function saveTagTitle($tag_id, $tag_title)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->update('`#__plot_tags`')
            ->set('`title` = "' . $db->escape($tag_title) . '"')
            ->where('`tagId` = ' . (int)$tag_id)
            ->where('entity="user"')
            ->where('entityId=' . plotUser::factory()->id);

        $db->setQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {

            return false;
        }
        plotPoints::assign('describe.tag', 'com_plot', plotUser::factory()->id, $tag_id);

        $stream = Foundry::stream();
        $template = $stream->getTemplate();
        $template->setActor(plotUser::factory()->id, 'user');
        $template->setVerb('update');
        $template->setContext(plotUser::factory()->id, 'profiles');
        $template->setDate(Foundry::date()->toMySQL());
        $stream->add($template);
        return true;
    }

    public function saveTagSmiley($tag_id, $tag_smiley)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->clear()
            ->select('smiley')
            ->from('`#__plot_tags`')
            ->where('tagId=' . (int)$tag_id)
            ->where('entity="user"')
            ->where('entityId=' . plotUser::factory()->id)
            ->group('id');
        $db->setQuery($query);
        $smiley = (int)$db->loadResult();

        $query = $db->getQuery(true)
            ->update('`#__plot_tags`')
            ->set('`smiley` = ' . (int)$tag_smiley)
            ->where('`tagId` = ' . (int)$tag_id)
            ->where('entity="user"')
            ->where('entityId=' . plotUser::factory()->id);

        $db->setQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {

            return false;
        }

        if ($smiley == 0) {

            plotPoints::assign('assess.tag', 'com_plot', plotUser::factory()->id, $tag_id);
        }
        $stream = Foundry::stream();
        $template = $stream->getTemplate();
        $template->setActor(plotUser::factory()->id, 'user');
        $template->setVerb('update');
        $template->setContext(plotUser::factory()->id, 'profiles');
        $template->setDate(Foundry::date()->toMySQL());
        $stream->add($template);
        return true;
    }

    public function deletePointsForTitle($tag_id)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->clear()
            ->select('title')
            ->from('`#__plot_tags`')
            ->where('tagId=' . (int)$tag_id)
            ->where('entity="user"')
            ->where('entityId=' . (int)plotUser::factory()->id);
        $db->setQuery($query);
        $title = $db->loadResult();

        if ($title != '') {
            $query = $db->getQuery(true)
                ->clear()
                ->select('id')
                ->from('`#__social_points_history`')
                ->where('user_id=' . (int)plotUser::factory()->id)
                ->where("points_id=86")
                ->order('created');
            $db->setQuery($query);
            $tag = $db->loadResult();
            $query = $db->getQuery(true);
            $query->delete('#__social_points_history')
                ->where('id=' . (int)$tag);
            $db->SetQuery($query);
            try {
                $db->execute();
            } catch (RuntimeException $e) {

                return false;
            }
        }

    }

    public function deletePointsForSmiley($tag_id)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->clear()
            ->select('smiley')
            ->from('`#__plot_tags`')
            ->where('tagId=' . (int)$tag_id)
            ->where('entity="user"')
            ->where('entityId=' . (int)plotUser::factory()->id);
        $db->setQuery($query);
        $smiley = (int)$db->loadResult();

        if ($smiley != 0) {
            $query = $db->getQuery(true)
                ->clear()
                ->select('id')
                ->from('`#__social_points_history`')
                ->where('user_id=' . (int)plotUser::factory()->id)
                ->where("points_id=85")
                ->order('created');
            $db->setQuery($query);
            $tag = $db->loadResult();
            $query = $db->getQuery(true);
            $query->delete('#__social_points_history')
                ->where('id=' . (int)$tag);
            $db->SetQuery($query);

            try {
                $db->execute();
            } catch (RuntimeException $e) {

                return false;
            }
        }

    }

    public function deletePointsForTag()
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true)
            ->clear()
            ->select('id')
            ->from('`#__social_points_history`')
            ->where('user_id=' . (int)plotUser::factory()->id)
            ->where("points_id=84")
            ->order('created DESC');
        $db->setQuery($query);
        $tag = $db->loadResult();

        $query = $db->getQuery(true);
        $query->delete('#__social_points_history')
            ->where('id=' . (int)$tag);
        $db->SetQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {

            return false;
        }

    }

    public function checkFrendsForTag($tag_id)
    {
        $db = JFactory::getDbo();
        $my = plotUser::factory();

        $query = $db->getQuery(true)
            ->clear()
            ->select('sf.*')
            ->from('`#__social_friends` AS sf')
            ->innerJoin('`#__plot_friends` AS pf ON sf.id=pf.social_id')
            ->where('pf.tag_id=' . (int)$tag_id)
            ->where('(sf.actor_id=' . (int)$my->id . ' OR sf.target_id=' . (int)$my->id . ')');
        $db->setQuery($query);
        $friends = $db->loadObjectList();
        return $friends;
    }

}
