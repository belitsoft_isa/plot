<?php defined('_JEXEC') or die('Restricted access');


class PlotModelOlduserevents extends JModelList
{
    public function __construct($config = array())
    {


        parent::__construct($config);
    }

    //----------------------------------------------------------------------------------------------------
    protected function populateState($ordering = null, $direction = null)
    {
        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);
        parent::populateState('a.start_date', 'desc');
    }

    //----------------------------------------------------------------------------------------------------
    protected function getListQuery()
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('a.*, ue.uid ')
            ->from('`#__plot_events` AS `a`')
            ->innerJoin('`#__plot_user_event_map` AS `ue` ON ue.event_id=a.id')
            ->where('a.end_date<NOW()')
            ->where('ue.uid=' . (int)Foundry::user()->id);
        $search = $this->getState('filter.search');

        if (!empty($search)) {
            $search = $db->Quote('%' . $db->escape($search, true) . '%');
            $query->where('`a`.`title` LIKE ' . $search);
        }
        $query->order($db->escape($this->state->get('list.ordering') . ' ' . $this->state->get('list.direction')));
        return $query;
    }


}