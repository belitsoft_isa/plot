<?php

defined('_JEXEC') or die('Restricted access');

class PlotModelPhotos extends JModelLegacy
{

    public function getPhotos($params, $id)
    {
        $user = plotUser::factory($id);
        $query = $this->_db->getQuery(true)
            ->select('SQL_CALC_FOUND_ROWS `p`.*')
            ->select('`m`.`value`')
            ->select('`a`.`type` AS album_type ')
            ->from('`#__social_photos` AS `p`')
            ->leftJoin('`#__social_albums` AS `a` ON `a`.`id` = `p`.`album_id`')
            ->innerJoin('`#__social_photos_meta` AS `m` ON `m`.`photo_id` = `p`.`id`')
            ->where('`p`.`uid` = ' . (int)$user->id)
            ->where('`p`.`state` = 1')
            ->where('(`a`.`type` = "plot-photos" OR `a`.`type` = "plot-essay")')
            ->where('`m`.`property` = "thumbnail" AND `m`.`group`="path"')
            ->order('`p`.`created` DESC');

        if (isset($params['limit']) && isset($params['offset'])) {
            $this->_db->setQuery($query, $params['offset'], $params['limit']);
        } else {
            $this->_db->setQuery($query);
        }

        $photos = $this->_db->loadObjectList();

        $data = array('countItems' => $this->_db->setQuery('SELECT FOUND_ROWS()')->loadResult());
        $path = array();

        foreach ($photos AS $photo) {
            $path = explode("/", $photo->value);
            $photo->value = end($path);
            $photo->original = JUri::root() . 'templates/plot/img/blank150x150.jpg';
            $photo->thumb = JUri::root() . 'templates/plot/img/blank150x150.jpg';
            if ($photo->album_type != 'plot-essay') {
                $photo->value = end($path);
                $photoImageRelativeUrl = 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . PlotHelper::getImageOriginalById($photo->id)->value;
                $photoThumb = 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . $photo->value;
            } else {
                $essay = PlotHelper::getEssayByImg(end($path));
                if ($essay) {
                    $photoImageRelativeUrl = 'media/com_plot/essay/' . $essay->id . '/' . end($path);
                    $photoThumb = $photoImageRelativeUrl;
                } else {
                    $photoImageRelativeUrl = 'templates/plot/img/blank150x150.jpg';
                    $photoThumb = 'templates/plot/img/blank150x150.jpg';
                }

            }
            if (JFile::exists(JPATH_BASE . '/' . $photoImageRelativeUrl)) {
                $photo->original = JUri::root() . $photoImageRelativeUrl;
            }
            if (JFile::exists(JPATH_BASE . '/' . $photoImageRelativeUrl)) {
                $photo->thumb = JUri::root() . $photoThumb;
            }

        }

        $data['items'] = $photos;
        return $data;
    }

    public function getCertificates($params, $id)
    {
        $user = Foundry::user($id);
        $query = $this->_db->getQuery(true)
            ->select('SQL_CALC_FOUND_ROWS `p`.*')
            ->select('`m`.`value`, a.type AS album_type')
            ->from('`#__social_photos` AS `p`')
            ->leftJoin('`#__social_albums` AS `a` ON `a`.`id` = `p`.`album_id`')
            ->innerJoin('`#__social_photos_meta` AS `m` ON `m`.`photo_id` = `p`.`id`')
            ->where('`p`.`uid` = ' . (int)$user->id)
            ->where('(`a`.`type` = "plot-certificate"  OR `a`.`type` = "plot-programs")')
            ->where('`m`.`property` = "thumbnail" AND `m`.`group`="path"')
            ->order('`p`.`created` DESC');

        if (isset($params['limit']) && isset($params['offset'])) {
            $this->_db->setQuery($query, $params['offset'], $params['limit']);
        } else {
            $this->_db->setQuery($query);
        }

        $photos = $this->_db->loadObjectList();
        $countPhotos = $this->_db->setQuery("SELECT FOUND_ROWS();")->loadResult();

        $path = array();
        foreach ($photos AS $photo) {
            $path = explode("/", $photo->value);
            $photo->value = end($path);


            $photo->original = JUri::root() . 'templates/plot/img/blank150x150.jpg';
            $photo->thumb = JUri::root() . 'templates/plot/img/blank150x150.jpg';
            if ($photo->album_type == "plot-programs") {

                $photoImageRelativeUrl = 'media/com_plot/programs/' . $photo->uid . '/' . $photo->value;
                $photoThumb = 'media/com_plot/programs/' . $photo->uid . '/' . $photo->value;

            } else {
                $photoImageRelativeUrl = 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . PlotHelper::getImageOriginalById($photo->id)->value;
                $photoThumb = 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . $photo->value;
            }

            if (JFile::exists(JPATH_BASE . '/' . $photoImageRelativeUrl)) {
                $photo->original = JUri::root() . $photoImageRelativeUrl;
            }
            if (JFile::exists(JPATH_BASE . '/' . $photoImageRelativeUrl)) {
                $photo->thumb = JUri::root() . $photoThumb;
            }

        }

        $data = array('countItems' => $countPhotos, 'items' => $photos);
        return $data;
    }

    public function getVideos($params, $id)
    {
        $user = Foundry::user((int)$id);
        $query = $this->_db->getQuery(true)
            ->select('SQL_CALC_FOUND_ROWS `v`.*')
            ->from('`#__plot_video` AS `v`')
            ->where('`v`.`uid` = ' . (int)$user->id)
            ->where('`v`.`status`=1')
            ->order('`v`.`date` DESC');
        if (isset($params['limit']) && isset($params['offset'])) {
            $this->_db->setQuery($query, $params['offset'], $params['limit']);
        } else {
            $this->_db->setQuery($query);
        }
        $items = $this->_db->loadObjectList();
        $countItems = $this->_db->setQuery('SELECT FOUND_ROWS()')->loadResult();
        $data = array('countItems' => $countItems, 'items' => $items);
        return $data;
    }

    public function deletePhoto($photoId, $command)
    {
        $photo = PlotHelper::getImageById($photoId);
        $db = $this->_db;
        if (!$this->deleteStream($command, $photoId)) {
            return false;
        }
        PlotHelper::deleteDir(JPATH_BASE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_easysocial' . DIRECTORY_SEPARATOR . 'photos' . DIRECTORY_SEPARATOR . $photo->album_id . DIRECTORY_SEPARATOR . $photoId);

        $query = $db->getQuery(true);
        $query->delete('#__social_photos')
            ->where('id=' . (int)$photoId);
        $db->SetQuery($query);

        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }
        $query = $db->getQuery(true);
        $query->delete('#__social_photos_meta')
            ->where('photo_id=' . (int)$photoId);
        $db->SetQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }
        return true;
    }

    public function deleteVideo($videoId)
    {
        $video = PlotHelper::getVideoById($videoId);
        $db = $this->_db;

        if (!$this->deleteStream('add.video', $videoId)) {
            return false;
        }

        if ($video == 'file') {
            $img = substr($video->path, 0, -3) . 'jpg';
            @unlink(JPATH_BASE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR . plotUser::factory()->id . DIRECTORY_SEPARATOR . 'original' . DIRECTORY_SEPARATOR . $img);
            @unlink(JPATH_BASE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR . plotUser::factory()->id . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $img);
            @unlink(JPATH_BASE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR . plotUser::factory()->id . DIRECTORY_SEPARATOR . $video->path);
        } else {
            if (strripos($video->path, 'youtube.com/') !== FALSE) {
                $youtubeNumber = substr($video->path, strripos($video->path, '?v=') + 3);
                if (($pos = strpos($youtubeNumber, '&')) !== FALSE)
                    $youtubeNumber = substr($youtubeNumber, 0, $pos);
                @unlink(JPATH_BASE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR . plotUser::factory()->id . DIRECTORY_SEPARATOR . 'original' . DIRECTORY_SEPARATOR . $youtubeNumber . '.jpg');
                @unlink(JPATH_BASE . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR . plotUser::factory()->id . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $youtubeNumber . '.jpg');
            }
        }

        $query = $db->getQuery(true);
        $query->delete('#__plot_video')
            ->where('id=' . (int)$videoId);
        $db->SetQuery($query);

        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

        return true;
    }

    public function deleteStream($command, $id)
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`pph`.`sph_id`')
            ->from('`#__social_points_history` AS `sph`')
            ->innerJoin('`#__social_points` AS `sp` ON (`sp`.`id` = `sph`.`points_id`)')
            ->innerJoin('`#__plot_points_history` AS `pph` ON (`pph`.`sph_id` = `sph`.`id`)')
            ->where('`sph`.`user_id` = ' . (int)plotUser::factory()->id)
            ->where('`sp`.`command` = "' . $command . '"')
            ->where('pph.entity_id = ' . (int)$id);
        $db->setQuery($query);
        $sph_id = (int)$db->loadResult();

        if ($command == 'add.certificate' && !$sph_id) {
            $query = $db->getQuery(true)
                ->select('`pph`.`sph_id`')
                ->from('`#__social_points_history` AS `sph`')
                ->innerJoin('`#__social_points` AS `sp` ON (`sp`.`id` = `sph`.`points_id`)')
                ->innerJoin('`#__plot_points_history` AS `pph` ON (`pph`.`sph_id` = `sph`.`id`)')
                ->where('`sph`.`user_id` = ' . (int)plotUser::factory()->id)
                ->where('`sp`.`command` = "add.old.certificate"')
                ->where('pph.entity_id = ' . (int)$id);
            $db->setQuery($query);
            $sph_id = (int)$db->loadResult();
        }

        $query = $db->getQuery(true);
        $query->delete('#__plot_points_history')
            ->where('sph_id=' . (int)$sph_id);
        $db->SetQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

        $query = $db->getQuery(true);
        $query->delete('#__social_points_history')
            ->where('id=' . (int)$sph_id);
        $db->SetQuery($query);
        try {
            $db->execute();
        } catch (RuntimeException $e) {
            $this->setError($e->getMessage());
            return false;
        }

        return true;
    }

}
