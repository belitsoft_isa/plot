<?php
defined('_JEXEC') or die('Restricted access');

class PlotModelBook extends JModelLegacy
{

    public function getBook()
    {
        $id = JRequest::getInt('bookId');
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('`pub`.*')
            ->select(Foundry::user()->id . ' AS `uid`')
            ->select('`res`.`width`, `res`.`height`')
            ->select('`bs`.*')
            ->from('`#__html5fb_publication` AS `pub`')
            ->leftJoin('`#__html5fb_resolutions` AS `res` ON `pub`.`c_resolution_id` = `res`.`id`')
            ->leftJoin('`#__plot_book_cost` AS `bs` ON `pub`.`c_id` = `bs`.`book_id`')
            ->where('`pub`.`c_id` = ' . (int)$id);
        $db->setQuery($query);
        return $db->loadObject();
    }


    public function insertBookIfNotExist(stdClass $paidCourse){

         if (!$this->isBookExist($paidCourse->parent_id, $paidCourse->child_id, $paidCourse->book_id)) {
             $this->_db->insertObject('#__plot_books', $paidCourse, 'id');

             return true;
         }

        return false;
    }

    public function insertPaidCourseIfNotExist(stdClass $paidCourse)
    {
        if (!$this->isPaidBookExist($paidCourse->parent_id, $paidCourse->child_id, $paidCourse->book_id)) {
            $this->_db->insertObject('#__plot_books_paid', $paidCourse, 'id');
            $conversationModel = JModelLegacy::getInstance('conversations', 'PlotModel');
            $bookObj=new plotBook($paidCourse->book_id);


            $config = JFactory::getConfig();
            $sender = array(
                $config->get('mailfrom'),
                $config->get('fromname')
            );
            if(plotUser::factory($paidCourse->child_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($paidCourse->child_id)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                $data=$conversationModel->sendMessageSystem($paidCourse->child_id, 'plot-book-buy-' . $paidCourse->book_id . '-' . $paidCourse->parent_id);
                $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
                fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__." conversation1  ".$data['status'] );
                fclose($fp);
            }
            if(plotUser::factory($bookObj->book_author)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($bookObj->book_author)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                $data=$conversationModel->sendMessageSystem($bookObj->book_author, 'plot-book-buy-' . $paidCourse->book_id . '-' . $paidCourse->parent_id);
                $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
                fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__." conversation2  ".$data['status'] );
                fclose($fp);
            }
            $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $paidCourse->parent_id) . '">' . plotUser::factory($paidCourse->parent_id)->name . '</a>';
            $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=publication&bookId=' . $paidCourse->book_id) . '">' . $bookObj->c_title . '</a>';




            if(plotUser::factory($bookObj->book_author)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($bookObj->book_author)->getSocialFieldData('UNSUBSCRIBE')==NULL ) {
                $mailer = JFactory::getMailer();
                $mailer->setSender($sender);
                $mailer->addRecipient(plotUser::factory($bookObj->book_author)->email);
                $mailer->setSubject(JText::_('COM_PLOT_BUY_BOOK_EMAIL_SUBJECT'));
                $mailer->isHTML(true);
                $mailer->setBody(JText::sprintf("COM_PLOT_BUY_BOOK_EMAIL_AUTHOR", $actor, $target));
                $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
                fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__ ." send ".$mailer->Send() );
                fclose($fp);

            }

            if(plotUser::factory($paidCourse->child_id)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory($paidCourse->child_id)->getSocialFieldData('UNSUBSCRIBE')==NULL){
                $mailer = JFactory::getMailer();
                $mailer->setSender($sender);
                $mailer->addRecipient(plotUser::factory($paidCourse->child_id)->email);
                $mailer->setSubject(JText::_('COM_PLOT_BUY_BOOK_EMAIL_SUBJECT'));
                $mailer->isHTML(true);
                $mailer->setBody(JText::sprintf("COM_PLOT_BUY_BOOK_EMAIL", $actor, $target));
                $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
                fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__ ." send ".$mailer->Send() );
                fclose($fp);
            }


            $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
            fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__ ." ".true );
            fclose($fp);
            return true;
        }
        $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
        fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__ ." ".false );
        fclose($fp);
        return false;
    }

    public function isPaidBookExist($parentId, $childId, $courseId)
    {
        $query = "SELECT `id` FROM `#__plot_books_paid` WHERE `parent_id` = " . (int)$parentId . " AND `child_id` = " . (int)$childId . " AND `book_id` = " . (int)$courseId;
        $result = $this->_db->setQuery($query)->loadResult();

        $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
        fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__." isPaidBookExist  ".$result );
        fclose($fp);

        return $result;
    }


    public function isBookExist($parentId, $childId, $courseId){
        $query = "SELECT `id` FROM `#__plot_books` WHERE `parent_id` = " . (int)$parentId . " AND `child_id` = " . (int)$childId . " AND `book_id` = " . (int)$courseId;
        $result = $this->_db->setQuery($query)->loadResult();

        return $result;
    }

    public function buyBook($bookId, $children_id)
    {
        $count_children = count($children_id);
        $child_pay = JRequest::getInt('amount-for-each-child');
        for ($i = 0; $i < $count_children; $i++) {
            $bookToDb = new stdClass();
            $bookToDb->parent_id = Foundry::user()->id;
            $bookToDb->child_id = (int)$children_id[$i];
            $bookToDb->book_id = (int)$bookId;
            $bookToDb->read = -1;
            $this->_db->insertObject('#__plot_books', $bookToDb);

            $stream = Foundry::stream();
            $template = $stream->getTemplate();
            $template->setActor(Foundry::user()->id, 'user');
            $template->setVerb('buy');
            $template->setTitle('buy book');
            $template->setContext(Foundry::user()->id, 'buy-book-for-child');
            $template->setTarget((int)$children_id[$i]);
            $template->setDate(Foundry::date()->toMySQL());
            $stream->add($template);

            $payToDb = new stdClass();
            $payToDb->parent_id = Foundry::user()->id;
            $payToDb->child_id = (int)$children_id[$i];
            $payToDb->book_id = (int)$bookId;
            $payToDb->finished_price = (int)$child_pay;
            $payToDb->finished = 0;
            $this->_db->insertObject('#__plot_books_paid', $payToDb);

            if (!$this->payMoney()) {
                return false;
            }
        }

        return true;
    }

    public function payMoney()
    {
        $cost = JRequest::getInt('all-cost');
        $user = plotUser::factory();
        $user->addMoney(-$cost);
        return true;
    }

    public function getBookSettings($id)
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('`pub`.*')
            ->from('`#__plot_book_cost` AS `pub`')
            ->where('`pub`.`book_id` = ' . (int)$id);
        $db->setQuery($query);
        return $db->loadObject();
    }

    public function getBoughtBook()
    {
        $db = $this->_db;
        $id = JRequest::getInt('bookId');
        $query = $db->getQuery(true)
            ->clear()
            ->select('COUNT(`pub`.id)')
            ->from('`#__plot_books` AS `pub`')
            ->where('`pub`.`parent_id` = ' . (int)Foundry::user()->id)
            ->where('`pub`.`book_id` = ' . $id);
        $db->setQuery($query);
        $bought = (int)$db->loadResult();
        if ($bought == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function getCheckChildren()
    {
        $db = $this->_db;
        $book_id = JRequest::getInt('bookId');
        $my = plotUser::factory();
        $children = $my->getChildrenIds();
        $count_children = count($children);
        if ($children) {
            for ($i = 0; $i < $count_children; $i++) {
                $query = $db->getQuery(true)
                    ->clear()
                    ->select('`pub`.child_id')
                    ->from('`#__plot_books` AS `pub`')
                    ->where('`pub`.`child_id` = ' . (int)$children[$i])
                    ->where('`pub`.`book_id` = ' . $book_id);
                $db->setQuery($query);
                $child_id = (int)$db->loadResult();
                if ($child_id) {
                    unset($children[$i]);
                }
            }
        }
        return $children;
    }

    public function getCheckFriends()
    {

        $db = $this->_db;
        $book_id = JRequest::getInt('bookId');
        $my = plotUser::factory();
        $friends = $my->getTotalFriendsList();

        $count_friends = count($friends);
        if ($friends) {
            for ($i = 0; $i < $count_friends; $i++) {
                $query = $db->getQuery(true)
                    ->clear()
                    ->select('`pub`.child_id')
                    ->from('`#__plot_books` AS `pub`')
                    ->where('`pub`.`child_id` = ' . (int)$friends[$i]->id)
                    ->where('`pub`.`book_id` = ' . $book_id);
                $db->setQuery($query);
                $friend_id = (int)$db->loadResult();
                if ($friend_id) {
                    unset($friends[$i]->id);
                }
            }
        }
        return $friends;
    }

    public function markBooksForUsersAsPaid($choosedUsersIds, $courseId, $price, $payerId)
    {
        if (!$choosedUsersIds) {
            return false;
        }
        foreach ($choosedUsersIds AS $userId) {
            $paidCourse = new stdClass();
            $paidCourse->parent_id = $payerId;
            $paidCourse->child_id = $userId;
            $paidCourse->book_id = $courseId;
            $paidCourse->paid_date = JFactory::getDate()->toSql();
            $paidCourse->finished_price = $price;
            $paidCourse->finished = 0;
            $this->insertPaidCourseIfNotExist($paidCourse);
                $bookToDb = new stdClass();
                $bookToDb->parent_id = $payerId;
                $bookToDb->child_id = (int)$userId;
                $bookToDb->book_id = (int)$courseId;
                $bookToDb->read = -1;

            $this->insertBookIfNotExist($bookToDb);
        }

        return true;
    }

    public function addPercentOfBookCostToAuthor($bookId, $amountForComplete)
    {
        $book = new plotBook($bookId);
        $bookOwner = plotUser::factory($book->book_author);

        $buyerCosts = $book->getBuyerCosts($amountForComplete);
        $bookOwner->addMoney($buyerCosts['author']);
        $bookOwner->addAuthorMoneyInformation('book', $bookId, $buyerCosts['author']);
        return true;
    }

    public function isBookInUsersExist($payerId,$userId,$boojId){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->clear()
            ->select('id')
            ->from('`#__plot_books`')
            ->where('parent_id = ' . (int)$payerId)
            ->where('child_id = ' . (int)$userId)
            ->where('book_id = ' . (int)$boojId);
        $db->setQuery($query);
        return $db->loadResult();
    }


}
