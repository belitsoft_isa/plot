<?php
defined('_JEXEC') or die('Restricted access');

class PlotModelAboutme extends JModelList
{

    public function getAllChildrenIds()
    {
        $db = JFactory::getDbo();
        $query = "SELECT `ju`.`id` FROM `#__users` AS `ju` "
            . "LEFT JOIN `#__social_profiles_maps` AS `p` ON (`ju`.`id` = `p`.`user_id`) "
            . "WHERE `p`.`profile_id` = " . $db->quote(plotGlobalConfig::getVar('childProfileId')) . " "
            . "ORDER BY `p`.`created` DESC";
        $children = $db->setQuery($query)->loadObjectList();
        return $children;
    }

}