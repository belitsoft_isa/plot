<?php defined('_JEXEC') or die('Restricted access');


class PlotModelConversations extends JModelList
{

    public $filter = 'all';

    //----------------------------------------------------------------------------------------------------
    public function __construct($config = array())
    {

        $this->getUserStateFromRequest('filter', 'all');
        parent::__construct($config);
    }

    //----------------------------------------------------------------------------------------------------
    protected function populateState($ordering = null, $direction = null)
    {
        $filter = $this->getUserStateFromRequest($this->context . '.filter.filter', 'filter_filter');
        $this->setState('filter.filter', $filter);
        parent::populateState('b.created', 'desc');
    }

    //----------------------------------------------------------------------------------------------------
    protected function getListQuery()
    {
        $jinput = JFactory::getApplication()->input;

        $db = $this->_db;

        $query = $db->getQuery(true)
            ->clear()
            ->select('a.*, b.message')
            ->from('`#__social_conversations` AS `a`')
            ->innerJoin('`#__social_conversations_message` AS `b` ON a.id=b.conversation_id')
            ->innerJoin('#__social_conversations_message_maps AS c ON c.message_id=b.id')
            ->where('c.user_id=' . (int)plotUser::factory()->id);


        $filter = $this->getState('filter.filter');
        if (!$filter) {
            $filter = $jinput->get('filter', '');
        }
        if (isset($filter)) {
            switch ($filter) {
                case 'unread':
                    $query->where('c.isread=0');
                    break;
                case 'read':
                    $query->where('c.isread=1');
                    break;
            }
        }

        $query->order($db->escape($this->state->get('list.ordering') . ' ' . $this->state->get('list.direction')));

        // die(var_dump($query->__toString()));
        return $query;
    }

    public function getStatuses()
    {
        $statuses = array();


        $statuses[0] = array(
            'value' => 'all',
            'text' => JText::_('COM_PLOT_CONVERSATIONS_FILTER_ALL')
        );
        $statuses[1] = array('value' => 'unread',
            'text' => JText::_('COM_PLOT_CONVERSATIONS_FILTER_UNREAD')

        );
        $statuses[2] = array('value' => 'read',
            'text' => JText::_('COM_PLOT_CONVERSATIONS_FILTER_READ')

        );

        return $statuses;
    }

    public function getCountNewMessages()
    {
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('COUNT(a.id)')
            ->from('`#__social_conversations` AS `a`')
            ->innerJoin('`#__social_conversations_message` AS `b` ON a.id=b.conversation_id')
            ->innerJoin('#__social_conversations_message_maps AS c ON c.message_id=b.id')
            ->where('c.user_id=' . (int)plotUser::factory()->id)
            ->where('c.isread=0');
        $db->setQuery($query);

        return $db->loadResult();
    }

    public function sendMessage($recipients, $msg)
    {
        $app = JFactory::getApplication();
        $doc = JFactory::getDocument();

        // Ensure that the user is logged in.
        Foundry::requireLogin();

        // Get the current logged in user.
        $my = plotUser::factory();

        // Ensure that the recipients is an array.
        $recipients = Foundry::makeArray($recipients);


        // Get configuration
        $config = Foundry::config();


        // If recipients is not provided, we need to throw an error.
        if (empty($recipients)) {
            $data['message'] = JText::_('COM_EASYSOCIAL_CONVERSATIONS_ERROR_EMPTY_RECIPIENTS');
            $data['status'] = 0;
            return $data;
        }
        // Ensure that the recipients is not only itself.
        foreach ($recipients as $recipient) {
            // When user tries to enter it's own id, we should just break out of this function.
            if ($recipient == $my->id) {
                $data['message'] = JText::_('COM_PLOT_CONVERSATIONS_ERROR_CANNOT_SEND_TO_SELF');
                $data['status'] = 0;
                return $data;
            }


        }


        // Message should not be empty.
        if (empty($msg)) {
            $data['message'] = JText::_('COM_PLOT_CONVERSATIONS_ERROR_EMPTY_MESSAGE');
            $data['status'] = 0;
            return $data;

        }

        // Filter recipients and ensure all the user id's are proper!
        $total = count($recipients);

        // If there is more than 1 recipient and group conversations is disabled, throw some errors
        if ($total > 1 && !$config->get('conversations.multiple')) {
            $data['message'] = JText::_('COM_EASYSOCIAL_CONVERSATIONS_GROUP_CONVERSATIONS_DISABLED');
            $data['status'] = 0;
            return $data;

        }

        // Go through all the recipient and make sure that they are valid.
        for ($i = 0; $i < $total; $i++) {
            $userId = $recipients[$i];
            $user = Foundry::user($userId);

            if (!$user || empty($userId)) {
                unset($recipients[$i]);
            }
        }

        // After processing the recipients list, and no longer has any recipients, stop the user.
        if (empty($recipients)) {
            $data['message'] = JText::_('COM_EASYSOCIAL_CONVERSATIONS_ERROR_EMPTY_RECIPIENTS');
            $data['status'] = 0;
            return $data;

        }

        // Get the conversation table.
        $conversation = Foundry::table('Conversation');

        // Determine the type of message this is by the number of recipients.
        $type = count($recipients) > 1 ? SOCIAL_CONVERSATION_MULTIPLE : SOCIAL_CONVERSATION_SINGLE;

        // Set the conversation creator.
        $conversation->created_by = $my->id;

        // Set the last replied date.
        $conversation->lastreplied = Foundry::date()->toMySQL();

        // Set the conversation type.
        $conversation->type = $type;

        // Let's try to create the conversation now.
        $state = $conversation->store();

        // If there's an error storing the conversation, break.
        if (!$state) {
            $data['message'] = JText::_($conversation->getError());
            $data['status'] = 0;
            return $data;

        }

        // @rule: Store conversation message
        $message = Foundry::table('ConversationMessage');


        $post = JRequest::get('POST');

        // Bind the message data.
        //$message->bind($post);

        // Set the conversation id since we have the conversation id now.
        $message->conversation_id = $conversation->id;

        // Sets the message type.
        $message->type = SOCIAL_CONVERSATION_TYPE_MESSAGE;

        // Set the creation date.
        $message->created = Foundry::date()->toMySQL();

        // Set the creator.
        $message->created_by = $my->id;
        $message->message = $msg;

        // Try to store the message now.
        $state = $message->store();

        if (!$state) {
            $data['message'] = JText::_($message->getError());
            $data['status'] = 0;
            return $data;

        }

        // Add users to the message maps.
        array_unshift($recipients, $my->id);

        $model = Foundry::model('Conversations');

        // Add the recipient as a participant of this conversation.
        $model->addParticipants($conversation->id, $recipients);

        // Add the message maps so that the recipient can view the message
        $model->addMessageMaps($conversation->id, $message->id, $recipients, $my->id);


        // Send notification email to recipients
        foreach ($recipients as $recipientId) {
            // We should not send a notification to ourself.
            if ($recipientId != $my->id) {
                $recipient = Foundry::user($recipientId);

                // Add new notification item
                $mailParams = Foundry::registry();
                $mailParams->set('name', $recipient->getName());
                $mailParams->set('authorName', $my->getName());
                $mailParams->set('authorAvatar', $my->plotGetAvatar());
                $mailParams->set('authorLink', $my->getPermalink(true, true));
                $mailParams->set('message', $message->message);
                $mailParams->set('messageDate', $message->created);
                $mailParams->set('conversationLink', $conversation->getPermalink(true, true));

                // Send a notification for all participants in this thread.
                $state = Foundry::notify('conversations.new', array($recipientId), array('title' => JText::sprintf('COM_EASYSOCIAL_CONVERSATIONS_NEW_EMAIL_TITLE', $my->getName()), 'params' => $mailParams), false);
            }
        }
        $data['message'] = JText::_('COM_PLOT_CONVERSATIONS_MESSAGE_SENT');
        $data['status'] = 1;
        return $data;

    }

    public function sendMessageSystem($recipients, $msg)
    {
        //$app = JFactory::getApplication();
        //$doc = JFactory::getDocument();
        $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
        fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__." go to  sendMessageSystem");
        fclose($fp);
        // Ensure that the user is logged in.
        //Foundry::requireLogin();

        // Get the current logged in user.
        $my = Foundry::user(plotGlobalConfig::getVar('adminUserId'));


        $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
        fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__." admin id " .$my->id );
        fclose($fp);

        // Ensure that the recipients is an array.
        $recipients = Foundry::makeArray($recipients);

        $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
        fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__." recipients " .serialize($recipients) );
        fclose($fp);
        // Get configuration
        $config = Foundry::config();


        // If recipients is not provided, we need to throw an error.
        if (empty($recipients)) {
            $data['message'] = JText::_('COM_EASYSOCIAL_CONVERSATIONS_ERROR_EMPTY_RECIPIENTS');
            $data['status'] = 0;
            return $data;
        }
        // Ensure that the recipients is not only itself.
        foreach ($recipients as $recipient) {
            // When user tries to enter it's own id, we should just break out of this function.
            if ($recipient == $my->id) {
                $data['message'] = JText::_('COM_PLOT_CONVERSATIONS_ERROR_CANNOT_SEND_TO_SELF');
                $data['status'] = 0;
                return $data;
            }


        }


        // Message should not be empty.
        if (empty($msg)) {
            $data['message'] = JText::_('COM_PLOT_CONVERSATIONS_ERROR_EMPTY_MESSAGE');
            $data['status'] = 0;
            return $data;

        }

        // Filter recipients and ensure all the user id's are proper!
        $total = count($recipients);

        // If there is more than 1 recipient and group conversations is disabled, throw some errors
        if ($total > 1 && !$config->get('conversations.multiple')) {
            $data['message'] = JText::_('COM_EASYSOCIAL_CONVERSATIONS_GROUP_CONVERSATIONS_DISABLED');
            $data['status'] = 0;
            return $data;

        }

        // Go through all the recipient and make sure that they are valid.
        for ($i = 0; $i < $total; $i++) {
            $userId = $recipients[$i];
            $user = Foundry::user($userId);

            if (!$user || empty($userId)) {
                unset($recipients[$i]);
            }
        }

        $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
        fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__." recipients2 " .serialize($recipients) );
        fclose($fp);

        // After processing the recipients list, and no longer has any recipients, stop the user.
        if (empty($recipients)) {
            $data['message'] = JText::_('COM_EASYSOCIAL_CONVERSATIONS_ERROR_EMPTY_RECIPIENTS');
            $data['status'] = 0;
            return $data;

        }

        // Get the conversation table.
        $conversation = Foundry::table('Conversation');

        // Determine the type of message this is by the number of recipients.
        $type = count($recipients) > 1 ? SOCIAL_CONVERSATION_MULTIPLE : SOCIAL_CONVERSATION_SINGLE;

        // Set the conversation creator.
        $conversation->created_by = $my->id;

        // Set the last replied date.
        $conversation->lastreplied = Foundry::date()->toMySQL();

        // Set the conversation type.
        $conversation->type = $type;

        // Let's try to create the conversation now.
        $state = $conversation->store();
        $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
        fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__." state " .$state );
        fclose($fp);
        // If there's an error storing the conversation, break.
        if (!$state) {
            $data['message'] = JText::_($conversation->getError());
            $data['status'] = 0;
            return $data;

        }

        // @rule: Store conversation message
        $message = Foundry::table('ConversationMessage');


        //$post = JRequest::get('POST');

        // Bind the message data.
        //$message->bind($post);

        // Set the conversation id since we have the conversation id now.
        $message->conversation_id = $conversation->id;

        // Sets the message type.
        $message->type = SOCIAL_CONVERSATION_TYPE_MESSAGE;

        // Set the creation date.
        $message->created = Foundry::date()->toMySQL();

        // Set the creator.
        $message->created_by = $my->id;
        $message->message = $msg;

        // Try to store the message now.
        $state = $message->store();

        $fp = fopen(JPATH_ROOT.'/logs/1.txt', 'a');
        fwrite($fp, "\r\n  ".date('Y-m-d H:i:s')."  ".__FILE__." line ".__LINE__." message state " .$state );
        fclose($fp);

        if (!$state) {
            $data['message'] = JText::_($message->getError());
            $data['status'] = 0;
            return $data;

        }

        // Add users to the message maps.
        array_unshift($recipients, $my->id);

        $model = Foundry::model('Conversations');

        // Add the recipient as a participant of this conversation.
        $model->addParticipants($conversation->id, $recipients);

        // Add the message maps so that the recipient can view the message
        $model->addMessageMaps($conversation->id, $message->id, $recipients, $my->id);


        // Send notification email to recipients
        foreach ($recipients as $recipientId) {
            // We should not send a notification to ourself.
            if ($recipientId != $my->id) {
                $recipient = Foundry::user($recipientId);

                // Add new notification item
                $mailParams = Foundry::registry();
                $mailParams->set('name', $recipient->getName());
                $mailParams->set('authorName', $my->getName());
                $mailParams->set('authorAvatar', $my->getAvatar());
                $mailParams->set('authorLink', $my->getPermalink(true, true));
                $mailParams->set('message', $message->message);
                $mailParams->set('messageDate', $message->created);
                $mailParams->set('conversationLink', $conversation->getPermalink(true, true));

                // Send a notification for all participants in this thread.
                $state = Foundry::notify('conversations.new', array($recipientId), array('title' => JText::sprintf('COM_EASYSOCIAL_CONVERSATIONS_NEW_EMAIL_TITLE', $my->getName()), 'params' => $mailParams), false);
            }
        }
        $data['message'] = JText::_('COM_PLOT_CONVERSATIONS_MESSAGE_SENT');
        $data['status'] = 1;
        return $data;

    }

}