<?php

defined('_JEXEC') or die('Restricted access');

class PlotModelCourses extends JModelLegacy
{

    public function getCourses($filter = array(), $order = '`total_min_cost` ASC', $agesIds = array(), $tagsIds = array(0), $limit = array())
    {
        $tableName = '#__lms_courses';

        if (isset($filter['field'])) {
            # only one field filter
            $filter = array($filter);
        }

        foreach ($filter as $fieldArr) {
            # if table - change field name
            if (isset($fieldArr['table']))
                $fieldName = $this->_db->quoteName($fieldArr['table']) . '.' . $this->_db->quoteName($fieldArr['field']);
            else
                $fieldName = $this->_db->quoteName($fieldArr['field']);

            if (!isset($fieldArr['type']) || !$fieldArr['type'])
                $fieldArr['type'] = '=';
            switch ($fieldArr['type']) {
                case 'like':
                    $whereArr[] = $fieldName . " LIKE '%" . $this->_db->escape($fieldArr['value']) . "%'";
                    break;
                case 'start':
                    $whereArr[] = "SUBSTRING(LTRIM(" . $fieldName . "), 1, 1)=" . $this->_db->quote($fieldArr['value']);
                    break;
                case 'or':
                    $whereArr[] = '(' . $fieldName . '=' . $this->_db->quote($fieldArr['value']) . ' OR ' . $this->_db->quoteName($fieldArr['field2']) . '=' . $this->_db->quote($fieldArr['value2']) . ')';
                    break;
                case 'IN':
                    $whereArr[] = $fieldName . ' IN (' . implode(',', ($fieldArr['value'])) . ')';
                    break;
                case '=':
                case '!=':
                case '>':
                case '>=':
                case '<':
                case '<=':
                default:
                    $whereArr[] = $fieldName . $fieldArr['type'] . $this->_db->quote($fieldArr['value']);
                    break;
            }
        }
        $where = (isset($whereArr) && $whereArr) ? "WHERE " . implode(" AND ", $whereArr) . " " : "WHERE 1 ";

        if ($agesIds) {
            $where .= "AND `ages`.`age_id` IN (" . implode(',', $agesIds) . ") ";
        }
        $where .= "AND `tags`.`tagId` IN (" . implode(',', $tagsIds) . ") ";

        $query = "SELECT SQL_CALC_FOUND_ROWS DISTINCT `c`.*, `cats`.`c_category`, `pc`.*, (`pc`.`admin_min_cost` + `pc`.`author_min_cost`) AS `total_min_cost` FROM `$tableName` AS `c` "
            . "LEFT JOIN `#__lms_course_cats` AS `cats` ON (`c`.`cat_id`=`cats`.`id`) "
            . "LEFT JOIN `#__plot_courses` AS `pc` ON (`c`.`id` = `pc`.`id`) "
            . "LEFT JOIN `#__plot_age_course_map` AS `ages` ON (`ages`.`course_id` = `c`.`id` )"
            . "LEFT JOIN `#__plot_tags` AS `tags` ON (`tags`.`entity` = 'course' AND `tags`.`entityId` = `c`.`id` )"
            . "$where "
            . "ORDER BY $order";

        if (isset($limit['offset']) && isset($limit['limit'])) {
            $items = $this->_db->setQuery($query, $limit['offset'], $limit['limit'])->loadObjectList();
        } else {
            $items = $this->_db->setQuery($query)->loadObjectList();
        }

        foreach ($items AS $i => $course) {
            if (!$course->image) {
                $items[$i]->image = JUri::root() . 'templates/plot/img/blank300x200.jpg';
            }
        }

        $data = array();
        $data['items'] = $items;
        $data['countItems'] = $this->_db->setQuery('SELECT FOUND_ROWS()')->loadResult();

        return $data;
    }

    public function getCoursesCategoriesWhichHavePublishedCourses()
    {
        $db = JFactory::getDbo();

        $query = "SELECT DISTINCT `cats`.* FROM `#__lms_courses` AS `c` "
            . "LEFT JOIN `#__lms_course_cats` AS `cats` ON (`cats`.`id` = `c`.`cat_id`) "
            . "WHERE `c`.`published` = 1 ";

        $categories = $db->setQUery($query)->loadObjectList();
        return $categories;
    }

    public function getChildStudiedCoursesByYears($childId)
    {
        $db = JFactory::getDbo();
        $child = plotUser::factory($childId);

        $childBirthday = $child->getSocialFieldData('BIRTHDAY');
        if (!isset($childBirthday->day) || !isset($childBirthday->month)) {
            $childBirthday = new stdClass();
            $childBirthday->day = '01';
            $childBirthday->month = '01';
            $childBirthday->year = '1900';
        }
        $childBirthdayDate = JFactory::getDate($childBirthday->year . '-' . $childBirthday->month . '-' . $childBirthday->day);

        $query = "SELECT `course_id`, `finished_date` "
            . "FROM `#__plot_courses_paid` "
            . "WHERE `child_id`=" . (int)$childId . " "
            . "AND `finished`=1 "
            . "ORDER BY `finished_date` DESC";
        $myFinishedCourses = $db->setQuery($query)->loadObjectList();

        # sort by my yearsOld
        $myFinishedCoursesByDate = array();
        foreach ($myFinishedCourses AS $course) {
            $courseFinishedDate = JFactory::getDate($course->finished_date);
            $dateDiff = $courseFinishedDate->diff($childBirthdayDate);
            if ($dateDiff->invert) {
                $courseObj = new plotCourse($course->course_id);
                $courseObj->finished_date = $course->finished_date;
                $myFinishedCoursesByDate[$dateDiff->y][] = $courseObj;
            }
        }

        return $myFinishedCoursesByDate;
    }

    public function getCoursesPaidNewTop($params)
    {
        $db = JFactory::getDbo();

        $queryCoursesPaidNew =
            "SELECT `courses_paid`.`id` AS `courses_paid_id`, `courses_paid`.`parent_id`, `courses_paid`.`child_id`, `courses_paid`.`course_id`, `courses_paid`.`paid_date`, "
            . "`courses_paid`.`finished_price`, `courses_paid`.`finished`, `courses_paid`.`finished_date`, `courses_paid`.`is_new`, `courses_full`.* FROM `#__plot_courses_paid` AS `courses_paid` "
            . "LEFT JOIN "
            . "(SELECT DISTINCT `c`.*, `cats`.`c_category`, "
            . "`pc`.`admin_min_cost`, `pc`.`admin_cost`, `pc`.`admin_max_cost`, `pc`.`author_min_cost`, `pc`.`author_cost`, `pc`.`author_max_cost`, "
            . "`pc`.`min_cost`, `pc`.`image`, (`pc`.`admin_min_cost` + `pc`.`author_min_cost`) AS `total_min_cost` FROM `#__lms_courses` AS `c` "
            . "LEFT JOIN `#__lms_course_cats` AS `cats` ON (`c`.`cat_id`=`cats`.`id`) "
            . "LEFT JOIN `#__plot_courses` AS `pc` ON (`c`.`id` = `pc`.`id`) "
            . "LEFT JOIN `#__plot_age_course_map` AS `ages` ON (`ages`.`course_id` = `c`.`id` ) "
            . "LEFT JOIN `#__plot_tags` AS `tags` ON (`tags`.`entity` = 'course' AND `tags`.`entityId` = `c`.`id` )) AS `courses_full` ON (`courses_full`.`id` = `courses_paid`.`course_id`) "
            . "WHERE `courses_paid`.`child_id` = " . $db->quote($params['userId']) . " AND `courses_paid`.`is_new` = 1 AND `courses_full`.`published` = 1 "
            . "ORDER BY `courses_paid`.`paid_date` DESC";
        $coursesPaidNew = $db->setQuery($queryCoursesPaidNew)->loadObjectList();

        $queryCoursesPaidNotNew =
            "SELECT `courses_paid`.`id` AS `courses_paid_id`, `courses_paid`.`parent_id`, `courses_paid`.`child_id`, `courses_paid`.`course_id`, `courses_paid`.`paid_date`, "
            . "`courses_paid`.`finished_price`, `courses_paid`.`finished`, `courses_paid`.`finished_date`, `courses_paid`.`is_new`, `courses_full`.* FROM `#__plot_courses_paid` AS `courses_paid` "
            . "LEFT JOIN "
            . "(SELECT DISTINCT `c`.*, `cats`.`c_category`, "
            . "`pc`.`admin_min_cost`, `pc`.`admin_cost`, `pc`.`admin_max_cost`, `pc`.`author_min_cost`, `pc`.`author_cost`, `pc`.`author_max_cost`, "
            . "`pc`.`min_cost`, `pc`.`image`, (`pc`.`admin_min_cost` + `pc`.`author_min_cost`) AS `total_min_cost` FROM `#__lms_courses` AS `c` "
            . "LEFT JOIN `#__lms_course_cats` AS `cats` ON (`c`.`cat_id`=`cats`.`id`) "
            . "LEFT JOIN `#__plot_courses` AS `pc` ON (`c`.`id` = `pc`.`id`) "
            . "LEFT JOIN `#__plot_age_course_map` AS `ages` ON (`ages`.`course_id` = `c`.`id` ) "
            . "LEFT JOIN `#__plot_tags` AS `tags` ON (`tags`.`entity` = 'course' AND `tags`.`entityId` = `c`.`id` )) AS `courses_full` ON (`courses_full`.`id` = `courses_paid`.`course_id`) "
            . "WHERE `courses_paid`.`child_id` = " . $db->quote($params['userId']) . " AND `courses_paid`.`is_new` = 0 AND `courses_full`.`published` = 1 "
            . "ORDER BY `courses_paid`.`paid_date` DESC";
        $coursesPaidNotNew = $db->setQuery($queryCoursesPaidNotNew)->loadObjectList();

        if (isset($params['limit'])) {
            $coursesPaidNewTop = array_slice(array_merge($coursesPaidNew, $coursesPaidNotNew), 0, $params['limit']);
        } else {
            $coursesPaidNewTop = array_merge($coursesPaidNew, $coursesPaidNotNew);
        }


        foreach ($coursesPaidNewTop AS $i => $coursePaidNewTop) {
            $percent = 0;
            $query = "SELECT `lps`.* FROM `#__lms_learn_path_steps` as `lps` "
                . "WHERE `lps`.`course_id` = '" . $coursePaidNewTop->id . "'";
            $db->setQuery($query);
            $all_steps = $db->loadObjectList();

            $query = "SELECT lpsr.* FROM `#__lms_learn_path_results` as lpr, `#__lms_learn_path_step_results` as lpsr "
                . "WHERE lpr.id = lpsr.result_id "
                . "AND lpr.course_id = '" . $coursePaidNewTop->id . "' "
                . "AND lpr.user_id = '" . $params['userId'] . "'";
            $db->setQuery($query);
            $all_result_steps = $db->loadObjectList();

            $tmp_all_steps = array();
            foreach ($all_steps as $n => $step) {
                $tmp_all_steps[$n] = $step;
                $tmp_all_steps[$n]->step_status = 0;
                foreach ($all_result_steps as $result_step) {
                    if ($step->id == $result_step->step_id) {
                        $tmp_all_steps[$n]->step_status = $result_step->step_status;
                    }
                }
            }
            $all_steps = $tmp_all_steps;

            if (isset($all_steps) && count($all_steps)) {
                $completed_step = 0;
                foreach ($all_steps as $step) {
                    if (isset($step->step_status) && $step->step_status == 1) {
                        $completed_step++;
                    }
                }
                if ($completed_step) {
                    $percent = round(($completed_step / count($all_steps)) * 100);
                }
            }

            $coursesPaidNewTop[$i]->percentCompleted = $percent;
            $coursesPaidNewTop[$i]->routedLinkToLearningPath = JRoute::_("index.php?option=com_joomla_lms&Itemid=0&task=show_lpath&course_id=" . $coursePaidNewTop->id . "&id=" . PlotHelperJLMS::getLearningPathId($coursePaidNewTop->id));
        }

        return $coursesPaidNewTop;
    }

    public function getNewCoursesCount($userId)
    {
        $query = 'SELECT COUNT(`cp`.`id`) FROM `#__plot_courses_paid` AS `cp` '
            . 'LEFT JOIN `#__lms_courses` AS `c` ON (`c`.`id` = `cp`.`course_id` ) '
            . 'WHERE `cp`.`child_id` = ' . $this->_db->quote($userId) . ' AND `cp`.`is_new` = 1 '
            . 'AND `c`.`published` = 1';
        $countNewCourses = $this->_db->setQuery($query)->loadResult();
        return $countNewCourses;
    }




}
