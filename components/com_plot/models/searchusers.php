<?php

defined('_JEXEC') or die('Restricted access');

class PlotModelSearchUsers extends JModelLegacy
{

    public $totalUsers = 0;

    public function getUsers($filter = array(), $order = '`u`.`name` ASC', $limit = array())
    {
        $tableName = '#__users';

        if (isset($filter['field'])) {
            $filter = array($filter);
        }

        foreach ($filter as $fieldArr) {
            # if table - change field name
            if (isset($fieldArr['table']))
                $fieldName = $this->_db->quoteName($fieldArr['table']) . '.' . $this->_db->quoteName($fieldArr['field']);
            else
                $fieldName = $this->_db->quoteName($fieldArr['field']);

            if (!isset($fieldArr['type']) || !$fieldArr['type'])
                $fieldArr['type'] = '=';
            switch ($fieldArr['type']) {
                case 'like':
                    $whereArr[] = $fieldName . " LIKE '%" . $this->_db->escape($fieldArr['value']) . "%'";
                    break;
                case 'start':
                    $whereArr[] = "SUBSTRING(LTRIM(" . $fieldName . "), 1, 1)=" . $this->_db->quote($fieldArr['value']);
                    break;
                case 'or':
                    $whereArr[] = '(' . $fieldName . '=' . $this->_db->quote($fieldArr['value']) . ' OR ' . $this->_db->quoteName($fieldArr['field2']) . '=' . $this->_db->quote($fieldArr['value2']) . ')';
                    break;
                case '=':
                case '!=':
                case '>':
                case '>=':
                case '<':
                case '<=':
                default:
                    $whereArr[] = $fieldName . $fieldArr['type'] . $this->_db->quote($fieldArr['value']);
                    break;
            }
        }
        $where = (isset($whereArr) && $whereArr) ? "WHERE " . implode(" AND ", $whereArr) : "WHERE 1";

        $query = "SELECT SQL_CALC_FOUND_ROWS * FROM `$tableName` AS `u` "
            . "$where "
            . "ORDER BY $order";

        if ($limit) {
            $users = $this->_db->setQuery($query, $limit['limitstart'], $limit['limit'])->loadObjectList();
        } else {
            $users = $this->_db->setQuery($query)->loadObjectList();
        }

        $this->totalUsers = $this->_db->setQuery('SELECT FOUND_ROWS();')->loadResult();

        return $users;
    }

}
