<?php
defined('_JEXEC') or die('Restricted access');


class PlotModelNotifications extends JModelLegacy
{


    public function replaceForConversation($ids)
    {

        if ($ids) {
            $my_arr = array();
            foreach ($ids AS $id) {
                $my_arr[] = abs(filter_var($id, FILTER_SANITIZE_NUMBER_INT));
            }
            if ($my_arr) {
                $data = array();
                $db = $this->_db;

                foreach ($my_arr AS $id) {
                    $query = $db->getQuery(true)
                        ->clear()
                        ->select('`e`.*')
                        ->select('"" AS msg')
                        ->from('`#__plot_events` AS e')
                        ->where('`e`.`id` =' . (int)$id);
                    $data[] = $this->_db->setQuery($query)->loadObject();

                }

                foreach ($data AS $event) {

                    @$event->msg = 'Добавлено событие ' . $event->title . '. Подробную информацию можно просмотреть по <a href="' . JRoute::_("index.php?option=com_plot&view=event&id=") . $event->id . '">ссылке</a>';


                }
                return $data;
            }

        }

    }

}
