<?php defined('_JEXEC') or die('Restricted access');

class PlotModelProfile extends JModelList
{

    protected $user_id;
    protected $_read_list = FALSE;
    protected $_fav_list = FALSE;
    protected $_lastOpen = FALSE;
    public $_startF = 0;
    public $_startR = 0;
    public $_limitF = 3;
    public $_limitR = 3;
    public $_bookshelf = TRUE;

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->user_id = JFactory::getUser()->get('id');
    }

    protected function populateState($ordering = null, $direction = null)
    {
        // List state information
        $this->setState('list.reading.limit', $this->_limitR);
        $this->setState('list.reading.start', $this->_startR);

        $this->setState('list.favorite.limit', $this->_limitF);
        $this->setState('list.favorite.start', $this->_startF);

        $categoryId = $this->getUserStateFromRequest($this->context . '.filter.category_id', 'filter_category_id', '');
        $this->setState('filter.category_id', $categoryId);
        if (JFactory::getApplication()->input->get('sortTable', '', 'STRING')) {
            $this->setState('sort', JFactory::getApplication()->input->get('sortTable', '', 'STRING'));
        }
        $new = $this->getUserStateFromRequest($this->context . '.filter.new', 'filter_new', '');
        $this->setState('filter.new', $new);
        // parent::populateState('upub.read', 'asc');
    }

    public function getReadList()
    {
        $this->_read_list = TRUE;
        $this->_fav_list = FALSE;
        $this->_lastOpen = FALSE;

        $query = $this->getListQuery();
        if ($this->_bookshelf) {
            $this->_db->setQuery($query);
//			$this->_bookshelf = FALSE;
        } else {
            $this->_db->setQuery($query, (int)$this->getState('list.reading.start'), (int)$this->getState('list.reading.limit'));
        }

        return $this->_db->loadObjectList();
    }

    public function getFavoriteList()
    {
        $this->_fav_list = TRUE;
        $this->_read_list = FALSE;
        $this->_lastOpen = FALSE;

        $query = $this->getListQuery();
        if ($this->_bookshelf) {
            $this->_db->setQuery($query);
            $this->_bookshelf = FALSE;
        } else {
            $this->_db->setQuery($query, (int)$this->getState('list.favorite.start'), (int)$this->getState('list.favorite.limit'));
        }

        return $this->_db->loadObjectList();
    }

    protected function getListQuery()
    {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('`pub`.*');
        if (!plotUser::factory()->isParent()) {
            $query->select('`upub`.`child_id` AS `uid`, `upub`.`read`');
        } elseif (plotUser::factory()->isParent()) {
            $query->select(Foundry::user()->id . ' AS  `uid`, `upub`.`read`');
        }
        $query->from('`#__html5fb_publication` AS `pub`')
            ->innerJoin('`#__plot_books` AS `upub` ON `upub`.`book_id` = `pub`.`c_id`')
            ->innerJoin('`#__plot_books_paid` AS `pbp` ON `pub`.`c_id` = `pbp`.`book_id`')
            ->where('`upub`.`read` = 0')
            ->where('`pbp`.`finished` = 0')
            ->where('`pub`.published=1')
            ->where('`pbp`.`child_id` = ' . (int)$user->id);
        $query->group('`pub`.`c_id`');

        return $query;
    }

    protected function getTotalRows($list)
    {
        if ($list == 'reading') {
            $this->_read_list = TRUE;
            $this->_fav_list = FALSE;
            $this->_lastOpen = FALSE;
        } elseif ($list == 'favorite') {
            $this->_fav_list = TRUE;
            $this->_read_list = FALSE;
            $this->_lastOpen = FALSE;
        }

        $query = $this->getListQuery();
        $this->_db->setQuery($query);

        return count($this->_db->loadObjectList());
    }

    /**
     * Method to get the starting number of items for the data set.
     *
     * @param   string   Tab name where publication should display
     * @return  integer  The starting number of items available in the data set.
     *
     * @since   12.2
     */
    public function getPageStart($list)
    {
        $start = $this->getState('list.' . $list . '.start');
        $limit = $this->getState('list.' . $list . '.limit');
        $total = $this->getTotalRows($list);

        if ($start > $total - $limit) {
            $start = max(0, (int)(ceil($total / $limit) - 1) * $limit);
        }

        return $start;
    }

    /**
     * Method to get a JPagination object for the data set.
     *
     * @return  JPagination  A JPagination object for the data set.
     *
     * @since   12.2
     */
    public function getReadingPagination()
    {
        // Create the pagination object.
        $limit = (int)$this->getState('list.reading.limit') - (int)$this->getState('list.reading.links');
        $page = new JPagination($this->getTotalRows('reading'), $this->getPageStart('reading'), $limit, 'reading');

        return $page;
    }

    /**
     * Method to get a JPagination object for the data set.
     *
     * @return  JPagination  A JPagination object for the data set.
     *
     * @since   12.2
     */
    public function getFavoritePagination()
    {
        // Create the pagination object.
        $limit = (int)$this->getState('list.favorite.limit') - (int)$this->getState('list.favorite.links');
        $page = new JPagination($this->getTotalRows('favorite'), $this->getPageStart('favorite'), $limit, 'favorite');

        return $page;
    }

    public function getLastOpenPublication()
    {
        $this->_fav_list = FALSE;
        $this->_read_list = FALSE;
        $this->_lastOpen = TRUE;

        $query = $this->getListQuery();
        $this->_db->setQuery($query, 0, 1);

        return $this->_db->loadObject();
    }

    public function getUserJSFriends()
    {
        include_once JPATH_ROOT . '/components/com_community/libraries/core.php';
        $model = CFactory::getModel('Friends');

        return $model->getFriends($this->user_id, 'name', FALSE);
    }

    public function getVideoLinks()
    {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`v`.*')
            ->from('`#__plot_video` AS `v`')
            ->where('`v`.`uid` = ' . (int)$user->id)
            ->where('`v`.`status`=1')
            ->where('(`v`.`type`="link" OR `v`.`type`="course")')
            ->order('`v`.`date` DESC');

        $db->setQuery($query);
        $videos = $db->loadObjectList();
        return $videos;
    }

    public function getVideoFiles()
    {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`v`.*')
            ->from('`#__plot_video` AS `v`')
            ->where('`v`.`uid` = ' . (int)$user->id)
            ->where('`v`.`status`=1')
            ->where('`v`.`type` = "file"');
        $db->setQuery($query);

        return $db->loadObjectList();

    }

    public function getListPhotos($params = array())
    {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`p`.*')
            ->select('`m`.`value`')
            ->select('`a`.`type` AS img_type')
            ->from('`#__social_photos` AS `p`')
            ->leftJoin('`#__social_albums` AS `a` ON `a`.`id` = `p`.`album_id`')
            ->innerJoin('`#__social_photos_meta` AS `m` ON `m`.`photo_id` = `p`.`id`')
            ->where('`p`.`uid` = ' . (int)$user->id)
            ->where('(`a`.`type` = "plot-photos" OR `a`.`type` = "plot-essay")')
            ->where('`m`.`property` = "thumbnail" AND `m`.`group`="path"')
            ->where('`p`.`state` = 1')
            ->order('`p`.`created` DESC');
        if (isset($params['limit']) && $params['limit']) {
            $db->setQuery($query, 0, $params['limit']);
        } else {
            $db->setQuery($query);
        }

        $photos = $db->loadObjectList();
        $path = array();
        foreach ($photos AS $photo) {
            $path = explode("/", $photo->value);
            if ($photo->img_type != 'plot-essay') {
                $photo->value = end($path);

            } else {

                $essay = PlotHelper::getEssayByImg(end($path));
                if ($essay) {
                    $photo->value = $essay->id . '/' . end($path);
                } else {
                    $photo->value = end($path);
                }


            }
        }

        return $photos;
    }

    public function getListCertificates($params = array())
    {


        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        if ($id) {
            $user = plotUser::factory($id);
        } else {
            $user = plotUser::factory();
        }
        $birthday = new stdClass();
        $birthday->year = $user->getSocialFieldData('year');
        $birthday->month = $user->getSocialFieldData('month');
        $birthday->day = $user->getSocialFieldData('day');

        if (!$birthday || !$birthday->day) {
            $defaultChildBirthday = plotGlobalConfig::getVar('defaultChildBirthday');
            $date1 = strtotime((date('Y') - 1) . '-' . $defaultChildBirthday['month'] . '-' . $defaultChildBirthday['day']);
        } else {

            $date1 = strtotime((date('Y') - 1) . '-' . $birthday->month . '-' . $birthday->day);
        }

        $db = $this->_db;
        $query = $db->getQuery(true)
            ->select('`p`.*')
            ->select('`m`.`value`, a.type AS album_type')
            ->from('`#__social_photos` AS `p`')
            ->leftJoin('`#__social_albums` AS `a` ON `a`.`id` = `p`.`album_id`')
            ->innerJoin('`#__social_photos_meta` AS `m` ON `m`.`photo_id` = `p`.`id`')
            ->where('`p`.`uid` = ' . $user->id)
            ->where('(`a`.`type` = "plot-certificate" OR `a`.`type` = "plot-programs" )')
            ->where('`m`.`property` = "thumbnail" AND `m`.`group`="path"')
            ->order('`p`.`created` DESC');

        if (!isset($params['anyDates']) || !$params['anyDates']) {
            $query->where('`p`.`created`>="' . date('Y-m-d', $date1) . '"');
        }

        if (isset($params['limit'])) {
            $db->setQuery($query, 0, $params['limit']);
        } else {
            $db->setQuery($query);
        }

        $photos = $db->loadObjectList();
        $path = array();
        foreach ($photos AS $photo) {
            $path = explode("/", $photo->value);
            $photo->value = end($path);

            $photo->imageUrl = JUri::root() . 'templates/plot/img/blank150x150.jpg';
            $photo->thumb = JUri::root() . 'templates/plot/img/blank150x150.jpg';
            if ($photo->album_type == "plot-programs") {
                $photoImageRelativeUrl = 'media/com_plot/programs/' . $photo->uid . '/' . $photo->value;
                $photoThumb = 'media/com_plot/programs/' . $photo->uid . '/thumbnail_' . $photo->value;
                if(!JFile::exists(JPATH_BASE . '/' . $photoThumb)){
                    $photoThumb ='templates/plot/img/blank150x150.jpg';
                }


            } else {
                $photoImageRelativeUrl = 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . PlotHelper::getImageOriginalById($photo->id)->value;
                $photoThumb = 'media/com_easysocial/photos/' . $photo->album_id . '/' . $photo->id . '/' . $photo->value;
            }

            if (JFile::exists(JPATH_BASE . '/' . $photoImageRelativeUrl)) {
                $photo->imageUrl = JUri::root() . $photoImageRelativeUrl;
            }
            if (JFile::exists(JPATH_BASE . '/' . $photoThumb)) {
                $photo->thumb = JUri::root() . $photoThumb;
            }
        }

        return $photos;
    }

    public function getNewBooks()
    {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }

        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('`pub`.c_id')
            ->from('`#__html5fb_publication` AS `pub`')
            ->innerJoin('`#__plot_books_paid` AS `pbp` ON `pub`.`c_id` = `pbp`.`book_id`')
            ->innerJoin('`#__plot_books` AS `upub` ON `upub`.`book_id` = `pub`.`c_id`')
            ->where('`pbp`.`child_id` = ' . (int)$user->id)
            ->where('`upub`.`read` = -1')
            ->where('`pbp`.`finished` = 0');
        $query->where('`pub`.published=1');
        $query->group('`pub`.`c_id`');
        $db->setQuery($query);

        $new_books = $db->loadObjectList();

        if ($new_books) {
            foreach ($new_books AS $book) {
                $query = $db->getQuery(true)
                    ->clear();
                $query->select('`pub`.`page`')
                    ->from('`#__html5fb_users_publ` AS `pub`')
                    ->where('`pub`.`uid` = ' . (int)$user->id)
                    ->where('`pub`.`publ_id` = ' . (int)$book->c_id);
                $db->setQuery($query);
                $read = (int)$db->loadResult();

                if ($read) {
                    $query = $db->getQuery(true)
                        ->update('`#__plot_books`')
                        ->set('`read` = 0')
                        ->where('`book_id` = ' . (int)$book->c_id);
                    $query->where('`child_id` = ' . (int)$user->id);
                    $db->setQuery($query);
                    $db->execute();
                }
            }
        }
        $query = $db->getQuery(true)
            ->clear()
            ->select('`pub`.*');
        $query->select('`upub`.`child_id` AS `uid`, `upub`.`read`')
            ->from('`#__html5fb_publication` AS `pub`')
            ->innerJoin('`#__plot_books` AS `upub` ON `upub`.`book_id` = `pub`.`c_id`')
            ->where('`upub`.`child_id` = ' . (int)$user->id)
            ->where('`upub`.`read` = -1');
        $query->where('`pub`.published=1');
        $query->group('`pub`.`c_id`');

        $db->setQuery($query);
        $books = $db->loadObjectList();
        foreach ($books AS $row) {
            $thumbnailPath = JPATH_SITE . '/media/com_html5flippingbook' . '/thumbs/' . $row->c_thumb;
            if ($row->c_thumb == "" || !is_file($thumbnailPath)) {
                $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
            } else {
                if (file_exists(JPATH_BASE . '/media/com_html5flippingbook' . '/thumbs/thimb_' . $row->c_thumb)) {
                    $row->thumbnailUrl = JURI::root() . "media/com_html5flippingbook/thumbs/thimb_" . $row->c_thumb;
                } else {
                    $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
                }

            }
            $row->new = 1;
            $row->publicationLink = 'index.php?option=com_html5flippingbook&view=publication&id=' . $row->c_id;
        }


        return $books;


    }

    public function getReadBooks()
    {

        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('`pub`.*');
        $query->select('`res`.`width`, `res`.`height`')
            ->select($user->id . ' AS `uid`, `upub`.`read`, `pbp`.`paid_date`, `pbp`.`finished_date`,`bc`.`author` AS `book_author`')
            ->from('`#__html5fb_publication` AS `pub`')
            ->leftJoin('`#__html5fb_resolutions` AS `res` ON `pub`.`c_resolution_id` = `res`.`id`')
            ->innerJoin('`#__plot_books` AS `upub` ON `upub`.`book_id` = `pub`.`c_id`')
            ->innerJoin('`#__plot_book_cost` AS `bc` ON `bc`.`book_id` = `pub`.`c_id`')
            ->innerJoin('`#__plot_essay` AS `e` ON `e`.`book_id` = `pub`.`c_id`')
            ->innerJoin('`#__plot_books_paid` AS `pbp` ON `pbp`.`book_id` = `pub`.`c_id`');
        if ($user->id == Foundry::user()->id) {

            $query->where('`upub`.`child_id` = ' . (int)Foundry::user()->id);

        } else {
            $query->where('`upub`.`child_id` = ' . (int)$user->id);
        }
        $query->where('`pbp`.`finished` = 1');
        $query->where('`pub`.published=1');
        $query->where('`e`.status=1');
        $query->group('`pub`.`c_id`');

        $db->setQuery($query);
        $books = $db->loadObjectList();
        foreach ($books AS $row) {
            $thumbnailPath = JPATH_SITE . '/media/com_html5flippingbook' . '/thumbs/' . $row->c_thumb;
            if ($row->c_thumb == "" || !is_file($thumbnailPath)) {
                $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
            } else {
                if (file_exists(JPATH_BASE . '/media/com_html5flippingbook' . '/thumbs/thimb_' . $row->c_thumb)) {
                    $row->thumbnailUrl = JURI::root() . "media/com_html5flippingbook/thumbs/thimb_" . $row->c_thumb;
                } else {
                    $row->thumbnailUrl = JURI::root() . "images/com_plot/def_book.jpg";
                }

            }

        }

        return $books;

    }

    public function getPoints()
    {
        $jinput = JFactory::getApplication()->input;
        $id = $jinput->get('id', 0, 'INT');
        if ($id) {
            $user = Foundry::user($id);
        } else {
            $user = Foundry::user();
        }
        $db = Foundry::db();

        $query = $db->sql();

        $query->select('#__social_points_history');
        $query->column('points', '', 'sum');
        $query->where('user_id', $user->id);
        $query->where('state', SOCIAL_STATE_PUBLISHED);

        $db->setQuery($query);

        $points = $db->loadResult();

        if (!$points) {
            return 0;
        }

        return $points;
    }

    public function getMoney()
    {
        $user = Foundry::user();
        $db = $this->_db;
        $query = $db->getQuery(true)
            ->clear()
            ->select('`m`.money')
            ->from('`#__plot_money` AS `m`')
            ->where('`m`.`user_id` =' . (int)$user->id);

        $db->setQuery($query);
        return $db->loadObjectList();
    }

    /* exist 3 status. 0- unread, 1-read, 2- reading*/
    public function getStatuses()
    {
        $statuses = array();


        $statuses[0] = array(
            'value' => -1,
            'text' => JText::_('COM_PLOT_UNREAD_BOOKS')
        );
        $statuses[1] = array('value' => 1,
            'text' => JText::_('COM_PLOT_UNREAD_READ')

        );
        $statuses[2] = array('value' => 0,
            'text' => JText::_('COM_PLOT_READING_BOOKS')

        );

        return $statuses;
    }

    public function updateEvent($id)
    {
        $db = $this->_db;
        $jinput = JFactory::getApplication()->input;
        $title = $jinput->get('title', '', 'STRING');
        $description = $jinput->get('description', '', 'HTML');
        $place = $jinput->get('place', '', 'STRING');
        $start_date = $jinput->get('start_date', '', 'STRING');
        $start_date_time = $jinput->get('start_date_time', '', 'STRING');
        $start_date .= ' ' . $start_date_time;
        $end_date = $jinput->get('end_date', '', 'STRING');
        $end_date_time = $jinput->get('end_date_time', '', 'STRING');
        $event_age = $jinput->get('event_age', '', 'ARRAY');
        $event_tags = $jinput->get('tags', '', 'ARRAY');

        $end_date .= ' ' . $end_date_time;
        $event = new stdClass();
        $event->title = $title;
        $event->id = $id;
        $event->user_id = Foundry::user()->id;
        $event->description = $description;
        $event->place = $place;
        $event->start_date = Foundry::date($start_date)->toMySQL();
        $event->end_date = Foundry::date($end_date)->toMySQL();
        $event->create_date = Foundry::date()->toMySQL();

        $db->updateObject('#__plot_events', $event, 'id');
        $event_map = new stdClass();
        $event_map->uid = Foundry::user()->id;
        $event_map->event_id = $id;
        $db->insertObject('#__plot_user_event_map', $event_map);
        //age
        $event_age_map = new plotAges();
        if ($event_age) {
            foreach ($event_age AS $tag) {
                $event_age_map->add('event', $id, $tag);
            }
        }
        //tags
        $tagobj = new plotTags();
        if ($event_tags) {
            foreach ($event_tags AS $tag) {
                $tagobj->add($tag, 'meeting', $id);
            }
        }

        plotPoints::assign('add.meeting', 'com_plot', plotUser::factory()->id, $id);
        $stream = Foundry::stream();
        $template = $stream->getTemplate();
        $template->setActor(Foundry::user()->id, 'user');
        $template->setVerb('add');
        $template->setContext(Foundry::user()->id, 'add.meeting');
        $template->setParams(array('id' => $id));
        $template->setDate(Foundry::date()->toMySQL());
        $stream->add($template);

    }

    public function saveImages($id)
    {

        $db = $this->_db;
        $rEFileTypes = "/^\.(jpg|jpeg|gif|png|bmp|xcf|odg){1}$/i";

        if (!JFolder::exists(JPATH_SITE . "/images/com_plot/events/" . (int)plotUser::factory()->id)) {
            JFolder::create(JPATH_SITE . "/images/com_plot/events/" . (int)plotUser::factory()->id);
        }

        if (isset($_FILES['img']) && $_FILES['img']["tmp_name"]) {
            $beaverfiles = JRequest::getVar('img', array(), 'files');
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot')) {
                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot');
            }
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events')) {
                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events');
            }
            if (!file_exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events' . DIRECTORY_SEPARATOR . (int)plotUser::factory()->id)) {
                mkdir(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events' . DIRECTORY_SEPARATOR . (int)plotUser::factory()->id);
            }

            $ext = JFile::getExt($beaverfiles['name']);
            $new_name = md5(time() . $beaverfiles['name']) . '.' . $ext;
            if (preg_match($rEFileTypes, strrchr($new_name, '.'))) {
                if (JFile::upload($beaverfiles['tmp_name'], JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events' . DIRECTORY_SEPARATOR . (int)plotUser::factory()->id . DIRECTORY_SEPARATOR . $new_name)) {
                    if (JFile::exists(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events' . DIRECTORY_SEPARATOR . (int)plotUser::factory()->id . DIRECTORY_SEPARATOR . $new_name)) {
                        JFile::move($beaverfiles["tmp_name"], JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events' . DIRECTORY_SEPARATOR . (int)plotUser::factory()->id . DIRECTORY_SEPARATOR . $new_name);
                        chmod(JPATH_SITE . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'com_plot' . DIRECTORY_SEPARATOR . 'events' . DIRECTORY_SEPARATOR . (int)plotUser::factory()->id . DIRECTORY_SEPARATOR . $new_name, 0644);
                        $query = $db->getQuery(true)
                            ->update('`#__plot_events`')
                            ->set('`img` = "' . $new_name . '"')
                            ->where('`id` = ' . (int)$id);
                        $db->setQuery($query);
                        try {
                            $db->execute();
                        } catch (RuntimeException $e) {
                            $this->setError($e->getMessage());
                            return false;
                        }
                    }
                }
            }
        }
    }

    public function friendRequestReplaceText($ids, $text_const)
    {
        if ($ids) {
            $my_arr = array();
            $count_ids = count($ids);
            for ($i = 0; $i < $count_ids; $i++) {
                $str = explode('-', $ids[$i]);

                $my_arr[$i]['actor_id'] = abs(filter_var($str[0], FILTER_SANITIZE_NUMBER_INT));
                $my_arr[$i]['target_id'] = abs(filter_var($str[1], FILTER_SANITIZE_NUMBER_INT));
            }

            if ($my_arr) {


                foreach ($my_arr AS $key => $item) {

                    $actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $item['actor_id']) . '">' . plotUser::factory($item['actor_id'])->name . '</a>';
                    $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $item['target_id']) . '">' . plotUser::factory($item['target_id'])->name . '</a>';
                    $data[$key] = new stdClass();
                    $data[$key]->msg = JText::sprintf($text_const, $actor, $target);


                }

                return $data;
            }

        }
    }







}