<?php
/**
* joomla_lms.course_users.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );
global $my, $Itemid, $option;;
$task 	= mosGetParam( $_REQUEST, 'task', '' );
if ($task != 'export_usergroup') {
	global $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id',0);
	$pathway = array();
	$pathway[] = array('name' => _JLMS_PATHWAY_HOME, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"), 'is_home' => true);
	$pathway[] = array('name' => $JLMS_CONFIG->get('course_name'), 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$course_id"), 'is_course' => true);
	$pathway[] = array('name' => _JLMS_TOOLBAR_USERS, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=course_users&amp;id=$course_id"));
	JLMSAppendPathWay($pathway);
	JLMS_ShowHeading();
}

require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.course_users.html.php");

$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) );

switch ($task) {
	case 'course_users':
		if ($JLMS_CONFIG->get('use_global_groups', 1)) JLMS_showUsersGlobal( $option );
		else JLMS_showUserGroups( $id, $option );							break;

	case 'new_usergroup':		JLMS_editUserGroup( 0, $id, $option );	break;
	case 'edit_usergroup':
	$cid = mosGetParam( $_POST, 'cid', array(0) );
	if (!is_array( $cid )) { $cid = array(0); }
	JLMS_editUserGroup( $cid[0], $id, $option );						break;
	case 'save_usergroup':		JLMS_saveUserGroup( $option );			break;
	case 'cancel_usergroup':	JLMS_cancelUserGroup( $option );		break;
	case 'view_users':
		if ($JLMS_CONFIG->get('use_global_groups', 1)) {
			$id = mosGetParam( $_REQUEST, 'course_id', 0 );
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$id") );
		} else {
			JLMS_showUsers( $id, $option );
		}
		break;
	case 'add_user':			JLMS_showAddUser( $id, $option );		break;

	case 'edit_user':
		if ($JLMS_CONFIG->get('use_global_groups', 1)) JLMS_showEditUserGlobal( $option );
		else JLMS_showEditUser( $id, $option );							break;

	case 'add_user_save':
						$cid = mosGetParam( $_POST, 'cid', array(0) );
						if (!is_array( $cid )) { $cid = array(0); }
						JLMS_addUserToGroup( $cid, $option );			break;
	case 'edit_user_save':		JLMS_editUserSave( $option );			break;
	case 'usergroup_delete':	JLMS_deleteUserGroup( $id, $option );	break;
	case 'delete_user':
		if ($JLMS_CONFIG->get('use_global_groups', 1)) JLMS_deleteUsersGlobal( $option );
		else JLMS_deleteUsersFromGroup( $id, $option ); 				break;

	case 'user_delete_yes':		JLMS_deleteUsers_yes( $id, $option );	break;
	case 'cancel_user':			JLMS_cancelUser( $option );				break;
	case 'import_users_csv':	JLMS_importUsersCSV($id, $option);		break;
	case 'import_users':		JLMS_importUsers( $option );			break;
	case 'export_usergroup':	JLMS_exportUserGroup( $id, $option );	break;
	case 'view_assistants':		JLMS_showAssistants( $id, $option );	break;
	case 'user_csv_delete':		JLMS_deleteUsersCSV( $id, $option );	break;
	case 'import_csv_delete':	JLMS_deleteUsersFromCSV( $option );		break;
	case 'cancel_csv_delete':	JLMS_cancelUserGroup( $option ); 		break;
	case 'user_delete_yes2':	JLMS_deleteUsers_yes2( $option );		break;
}

function JLMS_deleteUsersCSV( $course_id, $option ) {
	global $my, $JLMS_DB, $Itemid;
	$JLMS_ACL = JLMSFactory::getACL();
	if ( $course_id && $JLMS_ACL->CheckPermissions('users', 'manage') ) {
		JLMS_course_users_html::showDeleteUsersCSV( $course_id, $option );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id") );
	}
}

function JLMS_showUserGroups( $id, $option) {
	global $my, $JLMS_DB, $Itemid;
	$JLMS_ACL = JLMSFactory::getACL();
	if ($id && $JLMS_ACL->CheckPermissions('users', 'view')) {
		$query = "SELECT a.*"
		. "\n FROM #__lms_usergroups as a"
		. "\n WHERE a.course_id = '".$id."'"
		. "\n ORDER BY a.ordering, a.ug_name";
		$JLMS_DB->SetQuery( $query );
		$rows = $JLMS_DB->LoadObjectList();
		$is_no_group = 1;
		$lists = array();
		$lists['no_group'] = $is_no_group;
		JLMS_course_users_html::showUserGroups( $id, $option, $rows, $lists );
	} else {
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$id"));
	}
}
function JLMS_editUserGroup( $id, $course_id, $option ) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	if ( $course_id && $JLMS_ACL->CheckPermissions('users', 'manage') && ( ($id && (JLMS_GetGroupCourse($id) == $course_id)) || (!$id) ) ) {
		$row = new mos_Joomla_LMS_UserGroup( $JLMS_DB );
		$is_forum_enabled = false;
		if ($JLMS_CONFIG->get('plugin_forum')) {
			$query = "SELECT add_forum FROM #__lms_courses WHERE id = $course_id";
			$JLMS_DB->SetQuery( $query );
			$course_forum = $JLMS_DB->LoadResult();
			if ($course_forum) {
				$is_forum_enabled = true;
			}
		}
		$row->load( $id );
		if (!$is_forum_enabled) {
			$row->group_forum = 0;
		}
		if (!$id) {
			$row->group_forum = 0;
			$row->group_chat = 0;
		}
		$lists = array();
		$lists['group_forum'] = mosHTML::yesnoRadioList( 'group_forum', 'class="inputbox" '.((!$is_forum_enabled)?'disabled="disabled" ':''), $row->group_forum);
		$lists['group_chat'] = mosHTML::yesnoRadioList( 'group_chat', 'class="inputbox" ', $row->group_chat);
		JLMS_course_users_html::editUserGroup( $row, $lists, $option, $course_id );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
	}
}
function JLMS_saveUserGroup( $option ) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));

	// 31 January 2008 - get course name for fix (by TPETb)
	$query = "SELECT `course_name` FROM #__lms_courses WHERE id = $course_id";
	$JLMS_DB->setQuery($query);
	$course_name = $JLMS_DB->loadResult();
	//course name's got
	$JLMS_ACL = JLMSFactory::getACL();
	$id = intval(mosGetParam($_REQUEST, 'id', 0));
	if ( $course_id && $JLMS_ACL->CheckPermissions('users', 'manage') && ( ($id && (JLMS_GetGroupCourse($id) == $course_id)) || (!$id) ) )
	{
		$row = new mos_Joomla_LMS_UserGroup( $JLMS_DB );
		if (!$row->bind( $_POST )) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		$row->owner_id = $my->id;

		$row->ug_name = strval(JLMS_getParam_LowFilter($_POST, 'ug_name', ''));
		$row->ug_name = JLMS_Process_ContentNames($row->ug_name);

		$row->ug_description = strval(JLMS_getParam_LowFilter($_POST, 'ug_description', ''));
		$row->ug_description = JLMS_ProcessText_LowFilter($row->ug_description);

		if (!$row->check()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if ($row->group_forum) {
			$row->group_forum = 0;
			if ($JLMS_CONFIG->get('plugin_forum') == 1){
				$query = "SELECT add_forum FROM #__lms_courses WHERE id = $course_id";
				$JLMS_DB->SetQuery( $query );
				$is_course_forum = $JLMS_DB->LoadResult();
				if ($is_course_forum) {
					$row->group_forum = 1;
				}
			}
		}
		if (!$row->store()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}

		// 31 January 2008 - get group name fix (replace)(by TPETb)
		$forum_group_name = JLMSDatabaseHelper::GetEscaped($row->ug_name);
		//joomla group name's got

		$query = "UPDATE `#__lms_forum_details` AS fd SET fd.is_active = ".(int)$row->group_forum.", fd.need_update = 1
					WHERE fd.board_type IN (SELECT f.id FROM `#__lms_forums` AS f WHERE f.forum_level != 1 AND f.user_level = 1)
						AND fd.course_id = '".$row->course_id."' AND fd.group_id = '".$row->id."'";
		$JLMS_DB->setQuery($query);

		if( $JLMS_DB->query() )
		{
			$forum = & JLMS_SMF::getInstance();
			if( $forum )
				$forum->applyChanges( $row->course_id );
		}
	}

	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id") );
}
function JLMS_deleteUserGroup( $course_id, $option ) {
	global $my, $JLMS_DB, $Itemid;
	$JLMS_ACL = JLMSFactory::getACL();
	if ( $course_id && $JLMS_ACL->CheckPermissions('users', 'manage') ) {
		$cid = mosGetParam( $_POST, 'cid', array(0) );
		if (!is_array( $cid )) { $cid = array(0); }
		$i = 0;
		while ($i < count($cid)) {
			$cid[$i] = intval($cid[$i]);
			$i ++;
		}
		if (!empty($cid)) {
			require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_del_operations.php");
			$user_ids = & JLMS_DelOp_deleteUserGroup( $course_id, $cid );
			JLMS_deleteUsersFromGroup( 0, $option, true, 1, $course_id, $user_ids, true );
		}
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id") );
	}
}
function JLMS_cancelUserGroup( $option ) {
	global $Itemid;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id") );
}
function JLMS_showUsers( $id, $option) {
	global $my, $JLMS_DB, $Itemid, $JLMS_SESSION, $JLMS_CONFIG;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$JLMS_ACL = JLMSFactory::getACL();
	
	if ( $course_id && $JLMS_ACL->CheckPermissions('users', 'view') && (($id && (JLMS_GetGroupCourse($id) == $course_id)) || !$id) ) {

		$limit = intval( mosGetParam( $_GET, 'limit', $JLMS_SESSION->get('list_limit', $JLMS_CONFIG->getCfg('list_limit')) ) );
		$JLMS_SESSION->set('list_limit', $limit);
		$limitstart = intval( mosGetParam( $_GET, 'limitstart', 0 ) );
		$u_search = mosGetParam( $_REQUEST, 'u_search', $JLMS_SESSION->get('JLMS_UM_UG_u_search', '') );
		$JLMS_SESSION->set('JLMS_UM_UG_u_search', $u_search);

		$query = "SELECT count(a.id)"
		. "\n FROM #__users as a, #__lms_users_in_groups as b, #__lms_usertypes as d"
		. "\n WHERE a.id = b.user_id AND b.group_id = '".$id."' AND b.course_id = '".$course_id."' AND b.role_id = d.id"
		.(($u_search)?"\n AND (a.username LIKE '%".$u_search."%' OR a.name LIKE '%".$u_search."%' OR a.email LIKE '%".$u_search."%')":"")
		;
		$JLMS_DB->SetQuery( $query );
		$total = $JLMS_DB->LoadResult();

		require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
		$pageNav = new JLMSPageNav( $total, $limitstart, $limit );

		$query = "SELECT a.*, b.teacher_comment as user_add_comment, b.publish_start, b.start_date, b.publish_end, b.end_date, c.ug_name, d.roletype_id, d.lms_usertype"
		. "\n FROM #__users as a, #__lms_users_in_groups as b LEFT JOIN #__lms_usergroups as c ON b.group_id = c.id AND c.course_id = '".$course_id."'"
		. "\n , #__lms_usertypes as d"
		. "\n WHERE a.id = b.user_id AND b.group_id = '".$id."' AND b.course_id = '".$course_id."' AND b.role_id = d.id"
		.(($u_search)?"\n AND (a.username LIKE '%".$u_search."%' OR a.name LIKE '%".$u_search."%' OR a.email LIKE '%".$u_search."%')":"")
		. "\n ORDER BY a.username, a.name"
		. "\n LIMIT $pageNav->limitstart, $pageNav->limit";
		$JLMS_DB->SetQuery( $query );
		$rows = $JLMS_DB->LoadObjectList();
		$rows_groups = & JLMS_USERS_getCourseUserGroups( $course_id );
		$lists = array();

		$vars = array();
		foreach ($rows_groups as $grouprow) {
			$vars[] = $grouprow->id;
		}
		$non_sef_link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=view_users&amp;course_id=$course_id&amp;id={var}";
		$def_link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=view_users&amp;course_id=$course_id&amp;id=0";
		$jscode = JLMS_SEFHelper::buildJSRedirectfunction($non_sef_link, $def_link, $vars, 'jlms_usergroup_redirect');
		$JLMS_CONFIG->set('jlms_aditional_js_code', $JLMS_CONFIG->get('jlms_aditional_js_code','').$jscode);

		$lists['groups'] = mosHTML::selectList( $rows_groups, 'filt_group', 'class="inputbox" style="width:200px" size="1" onchange="jlms_usergroup_redirect(this);"', 'id', 'ug_name', $id );
		JLMS_course_users_html::showUsers( $course_id, $id, $option, $rows, $lists, $pageNav, $u_search );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
	}
}

function JLMS_showUsersGlobal ( $option ) {
	$user = JLMSFactory::getUser();
	$my_id = $user->get('id');
	$db = & JLMSFactory::getDB();
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$JLMS_SESSION = JLMSFactory::getSession();
	$Itemid = $JLMS_CONFIG->get('Itemid');

	$course_id = mosGetParam($_REQUEST, 'id', 0);
	$group_id = mosGetParam($_REQUEST, 'group_id', 0);
	$u_search = mosGetParam( $_REQUEST, 'u_search', $JLMS_SESSION->get('JLMS_UM_UGG_u_search', '') );
	$JLMS_SESSION->set('JLMS_UM_UGG_u_search', $u_search);

	$uids = array();

	$JLMS_ACL = JLMSFactory::getACL();
	if ($course_id && $JLMS_ACL->CheckPermissions('users', 'view')) {

		$limit = intval( mosGetParam( $_GET, 'limit', $JLMS_SESSION->get('list_limit', $JLMS_CONFIG->getCfg('list_limit')) ) );
		$JLMS_SESSION->set('list_limit', $limit);
		$limitstart = intval( mosGetParam( $_GET, 'limitstart', 0 ) );

//		$role_ceo = 1;
//		$query = "SELECT all_users FROM #__lms_user_parent_store WHERE parent_id = '".$my_id."'";
//		$db->setQuery($query);
//		$role_ceo = $db->loadResult();

		//$staff_learners = (in_array($JLMS_ACL->_role_type, array(2,3,4)) && isset($JLMS_ACL->_staff_learners))?$JLMS_ACL->_staff_learners:array();

//		echo '<pre>';
//		print_r($staff_learners);
//		echo '</pre>';

		//if(isset($JLMS_ACL->_roles_full[$JLMS_ACL->_role]['ceo']->view_all_users) && !$JLMS_ACL->_roles_full[$JLMS_ACL->_role]['ceo']->view_all_users){

		if ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only')) {
//		if(isset($role_ceo) && !$role_ceo){
			//$uids = $staff_learners;

			$course_members_array = array();
			$query = "SELECT a.group_id FROM `#__lms_user_assign_groups` as a WHERE a.user_id = '".$my_id."'"
			;
			$db->setQuery($query);
			$groups_where_admin_manager = JLMSDatabaseHelper::loadResultArray();

			if(count($groups_where_admin_manager) == 1) {
				$group_id = $groups_where_admin_manager[0];
			}

			$groups_where_admin_manager = implode(',', $groups_where_admin_manager);

			if($groups_where_admin_manager != '') {
				$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE (group_id IN ($groups_where_admin_manager) OR subgroup1_id IN ($groups_where_admin_manager))"
					. ($group_id ? ("\n AND group_id = '".$group_id."'") : '')
//						. ($filt_subgroup ? ("\n AND subgroup1_id = '".$filt_subgroup."'") : '')
				;
				$db->setQuery($query);
				$members = JLMSDatabaseHelper::loadResultArray();
				$course_members_array = $members;
			}

			$users_where_ceo_parent = array();
			if($JLMS_ACL->_role_type == 3) {
				$query = "SELECT user_id FROM `#__lms_user_parents` WHERE parent_id = '".$my_id."'"
				;
				$db->setQuery($query);
				$users_where_ceo_parent = JLMSDatabaseHelper::loadResultArray();

				//$members = array_merge($members, $users_where_ceo_parent);
			}

			if($members != "'0'" && count($users_where_ceo_parent)) {
				$members = array_merge($members, $users_where_ceo_parent);
				$course_members_array = array_merge($course_members_array, $users_where_ceo_parent);
			}
			elseif(count($users_where_ceo_parent)) {
				$members = $users_where_ceo_parent;
				$course_members_array = $users_where_ceo_parent;
			}
			if(is_array($members) && count($members)){
				$members = implode(',', $members);
			}
			if($members == '') {
				$members = "'0'";
			}

			/*if ($group_id) {
				$query = "SELECT ugg.user_id"
				. "\n FROM #__lms_users_in_global_groups as ugg, #__lms_user_assign_groups as upsi"
				. "\n WHERE ugg.group_id = $group_id AND ugg.group_id = upsi.group_id AND upsi.user_id = $my_id"
				//.(count($staff_learners) ? "\n AND ugg.user_id IN (".implode(",", $staff_learners).")" : '')
				;
				$db->setQuery($query);
				$uids = JLMSDatabaseHelper::loadResultArray();
				$uids = array_merge($staff_learners, $uids);
				$uids = array_unique($uids);
			}*/
			$query = "SELECT distinct a.*, b.teacher_comment as user_add_comment, b.publish_start, b.start_date, b.publish_end, b.end_date, b.user_id, c.lms_usertype"
			. "\n FROM #__users as a,"
			. "\n #__lms_users_in_groups as b, #__lms_usertypes as c, #__lms_users_in_global_groups as ugg"
			. "\n WHERE a.id = b.user_id AND b.role_id = c.id AND b.course_id = '".$course_id."'"
			. "\n AND ugg.user_id = b.user_id"
//			.(($group_id) ? " AND b.user_id IN (".implode(',', $uids).")" : '')
			. "\n AND b.user_id IN (".$members.")"
			.(($u_search)?"\n AND (a.username LIKE '%".JLMSDatabaseHelper::GetEscaped($u_search)."%' OR a.name LIKE '%".JLMSDatabaseHelper::GetEscaped($u_search)."%' OR a.email LIKE '%".JLMSDatabaseHelper::GetEscaped($u_search)."%')":"")
			. "\n ORDER BY a.username, a.name";
			$db->setQuery($query);
			$rows_tmp = $db->loadObjectList();

			$uids = array();
			foreach ($rows_tmp as $object) {
				$uids[] = $object->user_id;
			}

			$total = count($rows_tmp);
			
			require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
			$pageNav = new JLMSPageNav( $total, $limitstart, $limit );

			$db->setQuery($query, $pageNav->limitstart, $pageNav->limit);
			$rows = $db->LoadObjectList();

			$groups_array = array();
			if (count($uids)) {
				$query = "SELECT ug.user_id, g.ug_name, g.id AS group_id"
				."\n FROM #__lms_users_in_global_groups AS ug, #__lms_usergroups AS g"
				."\n WHERE ug.group_id=g.id AND ug.user_id IN (".(implode(',',$uids) ? implode(',',$uids) : '0').")"
				."\n AND g.id IN (".($groups_where_admin_manager ? $groups_where_admin_manager : '0').")";
				$db->setQuery($query);
				$groups_array_raw = $db->loadObjectList();
				$groups_array = array();
				foreach ($groups_array_raw as $object) {
					isset($groups_array[$object->user_id]) ? $groups_array[$object->user_id] .= '<br />'.$object->ug_name : $groups_array[$object->user_id] = $object->ug_name;
				}
				foreach ($rows as $key => $object) {
					isset($groups_array[$object->user_id]) ? $rows[$key]->ug_name = $groups_array[$object->user_id] : $rows[$key]->ug_name = '';
				}
			}
			$lists = array();

			$link = "index.php?option=$option&amp;Itemid=$Itemid&task=course_users&id=$course_id";
			$link = $link ."&amp;group_id=".JLMS_SELECTED_INDEX_MARKER;
			$link = processSelectedIndexMarker( $link );

			$groups_array_raw = array();
			if (count($uids)) {
				$query = "SELECT distinct g.ug_name, g.id AS group_id"
				."\n FROM #__lms_users_in_global_groups AS ug, #__lms_usergroups AS g"
				."\n WHERE ug.group_id=g.id AND ug.user_id IN (".(implode(',',$uids) ? implode(',',$uids) : '0').")"
				."\n AND g.id IN (".($groups_where_admin_manager ? $groups_where_admin_manager : '0').")"
				."\n ORDER BY g.ug_name"
				;
				$db->setQuery($query);
				$groups_array_raw = $db->loadObjectList();
			}
			$obj = array();
			$obj[] = mosHTML::makeOption(0, _JLMS_ATT_FILTER_ALL_GROUPS, 'group_id', 'ug_name');
			$groups_array_raw = array_merge($obj, $groups_array_raw);
			$lists['groups'] = mosHTML::selectList( $groups_array_raw, 'filt_group', 'class="inputbox" style="width:200px" size="1" onchange="document.location.href=\''. $link .'\';"', 'group_id', 'ug_name', $group_id );
		} else {
			if ($group_id) {
				$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE group_id = $group_id"
				.(isset($staff_learners) && count($staff_learners) ? "\n AND user_id IN (".implode(",", $staff_learners).")" : '')
				;
				$db->setQuery($query);
				$uids = JLMSDatabaseHelper::loadResultArray();
			}
			$query = "SELECT a.*, b.teacher_comment as user_add_comment, b.publish_start, b.start_date, b.publish_end, b.end_date, b.user_id, c.lms_usertype"
			. "\n FROM #__users as a, "
			. "\n #__lms_users_in_groups as b, #__lms_usertypes as c "
			. "\n WHERE a.id = b.user_id AND b.role_id = c.id AND b.course_id = '".$course_id."'"
			.(($group_id) ? "\n AND b.user_id IN (".implode(',', $uids).")" : '')
			.(isset($staff_learners) && count($staff_learners) ? "\n AND b.user_id IN (".implode(",", $staff_learners).")" : '')
			.(($u_search)?"\n AND (a.username LIKE '%".$u_search."%' OR a.name LIKE '%".$u_search."%' OR a.email LIKE '%".$u_search."%')":"")
			. "\n ORDER BY a.username, a.name";
			$db->setQuery($query);
			$rows_tmp = $db->loadObjectList();

			$uids = array();
			foreach ($rows_tmp as $object) {
				$uids[] = $object->user_id;
			}

			$total = count($rows_tmp);
			
			require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
			$pageNav = new JLMSPageNav( $total, $limitstart, $limit );

			$db->setQuery($query, $pageNav->limitstart, $pageNav->limit);
			
			$rows = $db->LoadObjectList();

			$groups_array = array();
			if (count($uids)) {
				$query = "SELECT ug.user_id, g.ug_name, g.id AS group_id"
				."\n FROM #__lms_users_in_global_groups AS ug, #__lms_usergroups AS g"
				."\n WHERE ug.group_id=g.id AND ug.user_id IN (".implode(',',$uids).")";
				$db->setQuery($query);
				$groups_array_raw = $db->loadObjectList();
				$groups_array = array();
				foreach ($groups_array_raw as $object) {
					isset($groups_array[$object->user_id]) ? $groups_array[$object->user_id] .= '<br />'.$object->ug_name : $groups_array[$object->user_id] = $object->ug_name;
				}
				foreach ($rows as $key => $object) {
					isset($groups_array[$object->user_id]) ? $rows[$key]->ug_name = $groups_array[$object->user_id] : $rows[$key]->ug_name = '';
				}
			}
			$lists = array();

			$groups_array_raw = array();
			if (count($uids)) {
				$query = "SELECT distinct g.ug_name, g.id AS group_id"
				."\n FROM #__lms_users_in_global_groups AS ug, #__lms_usergroups AS g"
				."\n WHERE ug.group_id=g.id AND ug.user_id IN (".implode(',',$uids).")"
				."\n ORDER BY g.ug_name"
				;
				$db->setQuery($query);
				$groups_array_raw = $db->loadObjectList();
			}

			$lms_titles_cache = & JLMSFactory::getTitles();
			$lms_titles_cache->setArray('usergroups', $groups_array_raw, 'group_id', 'ug_name');

			$vars = array();
			foreach ($groups_array_raw as $grouprow) {
				$vars[] = $grouprow->group_id;
			}
			$non_sef_link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=course_users&amp;id=$course_id&amp;group_id={var}";
			$def_link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=course_users&amp;id=$course_id";
			$jscode = JLMS_SEFHelper::buildJSRedirectfunction($non_sef_link, $def_link, $vars, 'jlms_usergroup_redirect');
			$JLMS_CONFIG->set('jlms_aditional_js_code', $JLMS_CONFIG->get('jlms_aditional_js_code','').$jscode);

			$obj = array();
			$obj[] = mosHTML::makeOption(0, _JLMS_ATT_FILTER_ALL_GROUPS, 'group_id', 'ug_name');
			$groups_array_raw = array_merge($obj, $groups_array_raw);
			$lists['groups'] = mosHTML::selectList( $groups_array_raw, 'filt_group', 'class="inputbox" style="width:200px" size="1" onchange="jlms_usergroup_redirect(this);"', 'group_id', 'ug_name', $group_id );
		}

		JLMS_course_users_html::showUsersGlobal( $course_id, $group_id, $option, $rows, $lists, $pageNav, $u_search, 1);
	} else {
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$course_id"));
	}
}

function JLMS_showAssistants( $course_id, $option) {
	global $my, $JLMS_DB, $Itemid, $JLMS_SESSION, $JLMS_CONFIG;
	$course_id = (isset($course_id) && $course_id)?$course_id:mosGetParam($_REQUEST, 'course_id', 0);
	$u_search = mosGetParam($_REQUEST, 'u_search', '');
	$JLMS_ACL = JLMSFactory::getACL();
	if ( $course_id && $JLMS_ACL->CheckPermissions('users', 'manage_teachers') ) {

		$limit = intval( mosGetParam( $_GET, 'limit', $JLMS_SESSION->get('list_limit',$JLMS_CONFIG->getCfg('list_limit')) ) );
		$JLMS_SESSION->set('list_limit', $limit);
		$limitstart = intval( mosGetParam( $_GET, 'limitstart', 0 ) );

		$query = "SELECT count(a.id) FROM #__users as a, #__lms_user_courses as b"
		. "\n WHERE a.id = b.user_id AND b.course_id = '".$course_id."'"
		;
		$JLMS_DB->SetQuery( $query );
		$total = $JLMS_DB->LoadResult();

		require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
		$pageNav = new JLMSPageNav( $total, $limitstart, $limit );

		$query = "SELECT a.*, c.lms_usertype, b.role_id, c.roletype_id"
		. "\n FROM #__users as a, #__lms_user_courses as b, #__lms_usertypes as c"
		. "\n WHERE a.id = b.user_id AND b.course_id = '".$course_id."' AND b.role_id = c.id"
		.(($u_search)?"\n AND (a.username LIKE '%".$u_search."%' OR a.name LIKE '%".$u_search."%' OR a.email LIKE '%".$u_search."%')":"")
		. "\n ORDER BY a.username, a.name"
		. "\n LIMIT $pageNav->limitstart, $pageNav->limit";
		$JLMS_DB->SetQuery( $query );
		$rows = $JLMS_DB->LoadObjectList();

		$id = 0;
		$lists = array();
		JLMS_course_users_html::showUsers( $course_id, $id, $option, $rows, $lists, $pageNav, $u_search, 2 );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id") );
	}
}
/**
 * JLMS_showAddUser()
 *
 * @param mixed $group_id
 * @param mixed $option
 * @return void
 */
function JLMS_showAddUser( $group_id, $option ) {
	$db = & JLMSFactory::getDB();
	$user = JLMSFactory::getUser();
	$my_id = $user->get('id');
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$JLMS_SESSION = JLMSFactory::getSession();
	$Itemid = $JLMS_CONFIG->get('Itemid');
	$JLMS_ACL = JLMSFactory::getACL();

	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$group_id2 = intval(mosGetParam($_REQUEST, 'group_id', 0));
	if ($group_id2) {
		$group_id = $group_id2;
	}
	$JLMS_ACL = JLMSFactory::getACL();
	$utype = intval(mosGetParam($_REQUEST, 'utype', 1));
	$u_search = mosGetParam( $_REQUEST, 'u_search', $JLMS_SESSION->get('JLMS_UM_AU_u_search', '') );
	$JLMS_SESSION->set('JLMS_UM_AU_u_search', $u_search);
	$limit = intval( mosGetParam( $_REQUEST, 'limit', $JLMS_SESSION->get('list_limit', $JLMS_CONFIG->get('list_limit')) ) );
	$JLMS_SESSION->set('list_limit', $limit);
	$limitstart = intval( mosGetParam( $_REQUEST, 'limitstart', 0 ) );
	
	if ($utype != 1 && $utype != 2) { $utype = 1; }
	if ( $course_id && $JLMS_ACL->CheckPermissions('users', 'manage') && ( ( $utype == 1 && ( ($group_id && JLMS_GetGroupCourse($group_id) == $course_id) || !$group_id ) ) || ($utype == 2 && ($JLMS_ACL->CheckPermissions('users', 'manage_teachers'))) ) ) {
		$lists = array();
		$ex_users = array();
		if ($utype == 1) {
			$query = "SELECT user_id FROM #__lms_users_in_groups WHERE course_id = $course_id";
			$db->SetQuery($query);
			$ex_users = JLMSDatabaseHelper::LoadResultArray();
		} else {
			$ex_users[] = $my_id;
			$ass_roles = $JLMS_ACL->GetSystemRoles(5, true);
			if(count($ass_roles) > 1){
				$lists['user_role'] = mosHTML::selectList( $ass_roles, 'role_id', 'class="inputbox" size="1" ', 'id', 'lms_usertype', 4 );
			} elseif(count($ass_roles)) {
				$lists['user_role_id'] = $ass_roles[0]->id;
			} else {
				JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_assistants&id=$course_id") );
			}
		}

		$lists['disabled_import'] = 0;
		if(isset($JLMS_ACL->_role_type) && $JLMS_ACL->_role_type == 3){
			$lists['disabled_import'] = 1;
		}

//		$role_ceo = 1;
//		$query = "SELECT all_users FROM #__lms_user_parent_store WHERE parent_id = '".$my_id."'";
//		$db->setQuery($query);
//		$role_ceo = $db->loadResult();

		$staff_learners = ($JLMS_ACL->_role_type == 3 && isset($JLMS_ACL->_staff_learners))?$JLMS_ACL->_staff_learners:array();
		//if(isset($JLMS_ACL->_roles_full[$JLMS_ACL->_role]['ceo']->view_all_users) && !$JLMS_ACL->_roles_full[$JLMS_ACL->_role]['ceo']->view_all_users){
		if ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only')) {
//		if(isset($role_ceo) && !$role_ceo){
			//$uids = $staff_learners;

			$course_members_array = array();
			$query = "SELECT a.group_id FROM `#__lms_user_assign_groups` as a WHERE a.user_id = '".$my_id."'"
			;
			$db->setQuery($query);
			$groups_where_admin_manager = JLMSDatabaseHelper::loadResultArray();

			if(count($groups_where_admin_manager) == 1) {
				//$group_id = $groups_where_admin_manager[0];
			}

			if(count($groups_where_admin_manager)) {
				$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE (group_id IN (".implode(',', $groups_where_admin_manager).") OR subgroup1_id IN (".implode(',', $groups_where_admin_manager)."))"
					//. ($group_id ? ("\n AND group_id = '".$group_id."'") : '')
//						. ($filt_subgroup ? ("\n AND subgroup1_id = '".$filt_subgroup."'") : '')
				;
				$db->setQuery($query);
				$members = JLMSDatabaseHelper::loadResultArray();
				$course_members_array = $members;
			}

			$users_where_ceo_parent = array();
			if($JLMS_ACL->_role_type == 3) {
				$query = "SELECT user_id FROM `#__lms_user_parents` WHERE parent_id = '".$my_id."'"
				;
				$db->setQuery($query);
				$users_where_ceo_parent = JLMSDatabaseHelper::loadResultArray();
				
				//$members = array_merge($members, $users_where_ceo_parent);
			}

			if($members != "'0'" && count($users_where_ceo_parent)) {
				$members = array_merge($members, $users_where_ceo_parent);
				$course_members_array = array_merge($course_members_array, $users_where_ceo_parent);
			}
			elseif(count($users_where_ceo_parent)) {
				$members = $users_where_ceo_parent;
				$course_members_array = $users_where_ceo_parent;
			}
			if(is_array($members) && count($members)){
				$members = implode(',', $members);
			}
			if($members == '') {
				$members = "'0'";
			}
//		if(isset($role_ceo) && !$role_ceo){
			$query = "SELECT user_id FROM #__lms_user_courses WHERE course_id = $course_id";
			$db->SetQuery($query);
			$ex_users = array_merge($ex_users, JLMSDatabaseHelper::LoadResultArray());
			if (empty($ex_users)) {
				$ex_users[] = $my_id;
			}
			$e_str = implode(',',$ex_users);

			$query = "SELECT COUNT(*)"
			. "\n FROM #__users as u"
			. "\n WHERE 1"
			. "\n AND u.block = 0"
			. "\n AND u.id NOT IN ($e_str)"
			. "\n AND u.id IN ($members)"
			.(($u_search)?"\n AND (u.username LIKE '%".$u_search."%' OR u.name LIKE '%".$u_search."%' OR u.email LIKE '%".$u_search."%')":"")
			."\n ORDER BY u.username";
			$db->SetQuery($query);
			$total = $db->LoadResult();

			require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
			$pageNav = new JLMSPageNav( $total, $limitstart, $limit );
			$query = "SELECT distinct u.id, u.username, u.name, u.email"
			. "\n FROM #__users as u"
			. "\n WHERE 1"
			. "\n AND u.block = 0"
			. "\n AND u.id NOT IN ($e_str)"
			. "\n AND u.id IN ($members)"
			.(($u_search)?"\n AND (u.username LIKE '%".$u_search."%' OR u.name LIKE '%".$u_search."%' OR u.email LIKE '%".$u_search."%')":"")
			."\n ORDER BY u.username";
			$db->SetQuery($query,$limitstart,$limit);
			$usrs = $db->LoadObjectList();
		} else {
			$query = "SELECT user_id FROM #__lms_user_courses WHERE course_id = $course_id";
			$db->SetQuery($query);
			$ex_users = array_merge($ex_users, JLMSDatabaseHelper::LoadResultArray());
			if (empty($ex_users)) {
				$ex_users[] = $my_id;
			}
			$e_str = implode(',',$ex_users);

			$query = "SELECT COUNT(*) FROM #__users WHERE id NOT IN ($e_str) AND block = 0 "
			.(count($staff_learners) ? "\n AND id IN (".implode(",", $staff_learners).")" : '')
			.(($u_search)?"\n AND (username LIKE '%".$u_search."%' OR name LIKE '%".$u_search."%' OR email LIKE '%".$u_search."%')":"")
			."\n ORDER BY username";
			$db->SetQuery($query);
			$total = $db->LoadResult();

			require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
			$pageNav = new JLMSPageNav( $total, $limitstart, $limit );
			$query = "SELECT id , username, name, email FROM #__users WHERE id NOT IN ($e_str) AND block = 0 "
			.(count($staff_learners) ? "\n AND id IN (".implode(",", $staff_learners).")" : '')
			.(($u_search)?"\n AND (username LIKE '%".$u_search."%' OR name LIKE '%".$u_search."%' OR email LIKE '%".$u_search."%')":"")
			."\n ORDER BY username";
			
			$db->SetQuery($query,$pageNav->limitstart, $pageNav->limit);
			$usrs = $db->LoadObjectList();
		}

		//Roles
		$role_id = mosGetParam($_REQUEST, 'role_id', $JLMS_ACL->defaultRole(1));//TODO: What is this for? default role_id to create?
		//$query = "SELECT id as value, lms_usertype as text FROM #__lms_usertypes WHERE roletype_id = '1' ORDER BY roletype_id";
		//$db->setQuery($query);
		$assis_roles = $JLMS_ACL->GetSystemRoles(1, true);//$db->loadObjectList();
		for ($i = 0, $n = count($assis_roles); $i < $n; $i ++) {
			$assis_roles[$i]->value = $assis_roles[$i]->id;
			$assis_roles[$i]->text = $assis_roles[$i]->lms_usertype;
		}

		if(count($assis_roles) > 1){
			$roles = array();
			$roles = array_merge($roles, $assis_roles);
			$lists['role'] = mosHTML::selectList( $roles, 'role_id', 'class="text_area" size="1"', 'value', 'text', $role_id );
		} else if(count($assis_roles)){
			$lists['role_id'] = $assis_roles[0]->value;
		} else {
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id"), _JLMS_USERS_NOT_ASSIGN_TYPE_USERS );
		}

		JLMS_course_users_html::addUser( $usrs, $option, $course_id, $group_id, $utype, $pageNav, $u_search, $lists );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id") );
	}
}
function JLMS_showEditUser( $group_id, $option ) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$cid = mosGetParam( $_POST, 'cid', array(0) );
	if (!is_array( $cid )) { $cid = array(0); }
	$user_id = intval($cid[0]);
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	if (($course_id && $user_id) && $JLMS_ACL->CheckPermissions('users', 'manage') && ( ($group_id && JLMS_GetGroupCourse($group_id) == $course_id)) || !$group_id ) {
		$lists = array();
		$query = "SELECT a.id as value, a.username, a.name, a.email, b.teacher_comment, b.publish_start, b.start_date, b.publish_end, b.end_date, b.role_id, b.enrol_time"
		. "\n FROM #__users as a, #__lms_users_in_groups as b"
		. "\n WHERE a.id = '".$user_id."' AND b.course_id = '".$course_id."' AND b.group_id = '".$group_id."' AND b.user_id = a.id";
		$JLMS_DB->SetQuery($query);
		$user_data = $JLMS_DB->loadObject();
		if ( is_object( $user_data ) ) {
			$rows_groups = & JLMS_USERS_getCourseUserGroups( $course_id );
			$lists['groups'] = mosHTML::selectList( $rows_groups, 'group_id', 'class="inputbox" style="width:300px" size="1" ', 'id', 'ug_name', $group_id );
			$user_data->spec_reg = 0;
			$user_data->spec_quest = '';
			$user_data->spec_answer = '';
			if ( $JLMS_CONFIG->get('course_spec_reg') ) {
				$user_data->spec_reg = 1;

				$JLMS_ACL = JLMSFactory::getACL();
				$sr_role = intval($JLMS_ACL->GetRole(1));
				$query = "SELECT role_id, id, is_optional FROM #__lms_spec_reg_questions WHERE course_id = $course_id AND (role_id = 0 OR role_id = $sr_role) ORDER BY role_id DESC, ordering";
				$JLMS_DB->SetQuery( $query );
				$sr_quests = $JLMS_DB->LoadObjectList();
				$sr_answs = array();
				if (!empty($sr_quests)) {
					$sr_role = $sr_quests[0]->role_id;
					$sr_ids = array();
					$sr_qq = array();
					foreach ($sr_quests as $srq) {
						if ($srq->role_id == $sr_role) {
							$sr_ids[] = $srq->id;
							$sr_qq[] = $srq;
						}
					}
					if (!empty($sr_ids)) {
						$sr_idss = implode(',',$sr_ids);
						$query = "SELECT a.*, b.course_question FROM #__lms_spec_reg_answers as a, #__lms_spec_reg_questions as b WHERE a.course_id = $course_id AND a.user_id = $my->id AND a.role_id = $sr_role AND a.quest_id IN ($sr_idss) AND a.quest_id = b.id ORDER BY b.ordering";
						$JLMS_DB->SetQuery( $query );
						$sr_answs = $JLMS_DB->LoadObjectList();
					}
				}
				$user_data->spec_answers = $sr_answs;
			}

			//Roles
			$default_role = $JLMS_ACL->defaultRole(1);
			$role_id = isset($user_data->role_id) ? $user_data->role_id : $default_role;
			//$query = "SELECT id as value, lms_usertype as text FROM #__lms_usertypes WHERE roletype_id = '1' ORDER BY roletype_id";
			//$JLMS_DB->setQuery($query);
			$roles = array();
			$stu_roles = $JLMS_ACL->GetSystemRoles(1, true);//$db->loadObjectList();

			$can_edit_user = true;
			if (isset($user_data->role_id) && $user_data->role_id) {
				$can_edit_user = false;
				for ($i = 0, $n = count($stu_roles); $i < $n; $i ++) {
					if ($user_data->role_id == $stu_roles[$i]->id) {
						$can_edit_user = true; break;
					}
				}
			}
			if (!$can_edit_user) {
				JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_users&course_id=$course_id&id=$group_id") );
			}
			for ($i = 0, $n = count($stu_roles); $i < $n; $i ++) {
				$stu_roles[$i]->value = $stu_roles[$i]->id;
				$stu_roles[$i]->text = $stu_roles[$i]->lms_usertype;
			}
			$roles[] = mosHTML::makeOption(0, '- Select Role -');
			$roles = array_merge($roles, $stu_roles);
			$lists['role'] = mosHTML::selectList( $roles, 'role_id', 'class="inputbox" size="1"', 'value', 'text', $role_id );
			$user_data->enrol_time = JLMS_addJoomlaLocale( 'Y-m-d H:i' , $user_data->enrol_time );

			JLMS_course_users_html::editUser( $user_data, $lists, $option, $course_id, $group_id);
		} else {
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_users&course_id=$course_id&id=$group_id") );
		}
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_users&course_id=$course_id&id=$group_id") );
	}
}

function JLMS_showEditUserGlobal( $option ) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$cid = mosGetParam( $_POST, 'cid', array(0) );
	if (!is_array( $cid )) { $cid = array(0); }
	$user_id = intval($cid[0]);
	$course_id = intval(mosGetParam($_REQUEST, 'id', 0));
	$JLMS_ACL = JLMSFactory::getACL();
	if (($course_id && $user_id) && $JLMS_ACL->CheckPermissions('users', 'manage')) {
		$lists = array();
		$query = "SELECT a.id as value, a.username, a.name, a.email, b.teacher_comment, b.publish_start, b.start_date, b.publish_end, b.end_date, b.group_id, b.role_id, b.enrol_time"
		. "\n FROM #__users as a, #__lms_users_in_groups as b"
		. "\n WHERE a.id = '".$user_id."' AND b.course_id = '".$course_id."' AND b.user_id = a.id";
		$JLMS_DB->SetQuery($query);
		$user_data = $JLMS_DB->loadObject();
		if ( is_object( $user_data ) ) {
			$user_data->spec_reg = 0;
			$user_data->spec_quest = '';
			$user_data->spec_answer = '';
			if ( $JLMS_CONFIG->get('course_spec_reg') ) {
				$user_data->spec_reg = 1;
				$JLMS_ACL = JLMSFactory::getACL();
				$sr_role = intval($JLMS_ACL->GetRole(1));
				$query = "SELECT role_id, id, is_optional FROM #__lms_spec_reg_questions WHERE course_id = $course_id AND (role_id = 0 OR role_id = $sr_role) ORDER BY role_id DESC, ordering";
				$JLMS_DB->SetQuery( $query );
				$sr_quests = $JLMS_DB->LoadObjectList();
				$sr_answs = array();
				if (!empty($sr_quests)) {
					$sr_role = $sr_quests[0]->role_id;
					$sr_ids = array();
					$sr_qq = array();
					foreach ($sr_quests as $srq) {
						if ($srq->role_id == $sr_role) {
							$sr_ids[] = $srq->id;
							$sr_qq[] = $srq;
						}
					}
					if (!empty($sr_ids)) {
						$sr_idss = implode(',',$sr_ids);
						$query = "SELECT a.*, b.course_question FROM #__lms_spec_reg_answers as a, #__lms_spec_reg_questions as b WHERE a.course_id = $course_id AND a.user_id = $my->id AND a.role_id = $sr_role AND a.quest_id IN ($sr_idss) AND a.quest_id = b.id ORDER BY b.ordering";
						$JLMS_DB->SetQuery( $query );
						$sr_answs = $JLMS_DB->LoadObjectList();
					}
				}
				$user_data->spec_answers = $sr_answs;
			}
			$lists = array();
			$group_id = $user_data->group_id;

			//Roles
			$default_role = $JLMS_ACL->defaultRole(1);
			$role_id = isset($user_data->role_id) ? $user_data->role_id : $default_role;
			//$query = "SELECT id as value, lms_usertype as text FROM #__lms_usertypes WHERE roletype_id = '1' ORDER BY roletype_id";
			//$JLMS_DB->setQuery($query);
			$roles = array();
			$stu_roles = $JLMS_ACL->GetSystemRoles(1, true);//$db->loadObjectList();

			$can_edit_user = true;
			if (isset($user_data->role_id) && $user_data->role_id) {
				$can_edit_user = false;
				for ($i = 0, $n = count($stu_roles); $i < $n; $i ++) {
					if ($user_data->role_id == $stu_roles[$i]->id) {
						$can_edit_user = true; break;
					}
				}
			}
			if (!$can_edit_user) {
				JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_users&course_id=$course_id&id=$group_id") );
			}
			for ($i = 0, $n = count($stu_roles); $i < $n; $i ++) {
				$stu_roles[$i]->value = $stu_roles[$i]->id;
				$stu_roles[$i]->text = $stu_roles[$i]->lms_usertype;
			}

			$roles[] = mosHTML::makeOption(0, '- Select Role -');
			$roles = array_merge($roles, $stu_roles);
			$lists['role'] = mosHTML::selectList( $roles, 'role_id', 'class="inputbox" size="1"', 'value', 'text', $role_id );

			$user_data->enrol_time = JLMS_addJoomlaLocale( 'Y-m-d H:i' , $user_data->enrol_time );

			JLMS_course_users_html::editUser( $user_data, $lists, $option, $course_id, $group_id);
		} else {
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_users&course_id=$course_id&id=$group_id") );
		}
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_users&course_id=$course_id&id=$group_id") );
	}
}

/**
 * IMPORTANT: the same function is declarated in the cart.php
 * Implement your changes to the both functions!
**/
function FLMS_access_global($cid, $course_id){
	global $JLMS_DB;
	$obj = new stdClass();

	$cids = implode(",", $cid);
	$query = "SELECT group_id FROM #__lms_users_in_global_groups WHERE user_id IN (".$cids.")";
	$JLMS_DB->setQuery($query);
	$in_group = JLMSDatabaseHelper::loadResultArray();

	if (count($in_group)) {
		$in_groups = implode(",", $in_group);
		$query = "SELECT * FROM #__lms_usergroups WHERE id IN (".$in_groups.") AND course_id = '0'";
		$JLMS_DB->setQuery($query);
		$detail_group = $JLMS_DB->loadObjectList();

		$x_start = 0;
		$x_end = 0;
		$out_start = '';
		$out_end = '';
		$out_start_publish = 0;
		$out_end_publish = 0;
		foreach($detail_group as $group) {
			if (isset($group->publish_start_date)) { // check if fiedl is present in the database (for compatibility with old DBs - e.g. if there was an error during updating)
				if(!$group->publish_start_date){
					$out_start = '';
					$out_start_publish = 0;
					$x_start = strtotime($group->start_date);
					break;
				} else if($x_start == 0 && $group->publish_start_date && strtotime($group->start_date) > $x_start){
					$out_start = $group->start_date;
					$out_start_publish = 1;
					$x_start = strtotime($group->start_date);
				} else if($group->publish_start_date && strtotime($group->start_date) < $x_start) {
					$out_start = $group->start_date;
					$out_start_publish = 1;
					$x_start = strtotime($group->start_date);
				}
			}
		}
		foreach($detail_group as $group){
			if (isset($group->publish_end_date)) { // if fields is exists in the DB (temporary) - for compatibility without ant DB changes
				if(!$group->publish_end_date){
					$out_end = '';
					$out_end_publish = 0;
					$x_end = strtotime($group->end_date);
					break;
				} else if($x_end == 0 && $group->publish_end_date && strtotime($group->end_date) > $x_end){
					$out_end = $group->end_date;
					$out_end_publish = 1;
					$x_end = strtotime($group->end_date);
				} else if($group->publish_end_date && strtotime($group->end_date) > $x_end){
					$out_end = $group->end_date;
					$out_end_publish = 1;
					$x_end = strtotime($group->end_date);
				}
			}
		}
		$obj->start_date = $out_start;
		$obj->end_date = $out_end;
		$obj->publish_start = $out_start_publish;
		$obj->publish_end = $out_end_publish;
	} else {
		$obj->start_date = '';
		$obj->end_date = '';
		$obj->publish_start = 0;
		$obj->publish_end = 0;
	}

	return $obj;
}

function JLMS_addUserToGroup( $cid, $option ) {
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	global $Itemid, $my, $JLMS_DB, $JLMS_CONFIG;

	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$group_id = intval(mosGetParam($_REQUEST, 'group_id', 0));
	$user_id = intval(mosGetParam($_REQUEST, 'user_id', 0));
	$utype = intval(mosGetParam($_REQUEST, 'utype', 1));
	$JLMS_ACL = JLMSFactory::getACL();
	$default_role = $JLMS_ACL->defaultRole(1);
	$role_id = intval(mosGetParam($_REQUEST, 'role_id', $default_role));
	if ($utype != 1 && $utype != 2) { $utype = 1; }
	$JLMS_ACL = JLMSFactory::getACL();
	$teacher_comment = strval(JLMS_getParam_LowFilter($_REQUEST, 'teacher_comment', ''));
	$iFilter = new JLMS_InputFilter(null,null,1,1);
	$teacher_comment = $iFilter->process( $teacher_comment );
	$teacher_comment = JLMSDatabaseHelper::GetEscaped( $teacher_comment );
	if (($course_id && count($cid)) && $JLMS_ACL->CheckPermissions('users', 'manage') && ( ( $utype == 1 && ( ($group_id && JLMS_GetGroupCourse($group_id) == $course_id)) || !$group_id ) || ($utype == 2 && ($JLMS_ACL->CheckPermissions('users', 'manage_teachers'))) ) ) {
		$cids = implode(',',$cid);
		$query = "SELECT count(*) FROM #__lms_users_in_groups WHERE course_id = '".$course_id."' AND user_id IN (".$cids.")";
		$JLMS_DB->SetQuery( $query );
		$c = $JLMS_DB->LoadResult();
		$query = "SELECT count(*) FROM #__lms_user_courses WHERE course_id = '".$course_id."' AND user_id IN (".$cids.")";
		$JLMS_DB->SetQuery( $query );
		$c2 = $JLMS_DB->LoadResult();
		if ($utype == 1) {
			if (intval($c) == 0) {
				$do_add = false;
				if ($JLMS_CONFIG->get('license_lms_users')) {
					$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
					$JLMS_DB->SetQuery( $query );
					$total_students = $JLMS_DB->LoadResult();
					if (intval($total_students) < intval($JLMS_CONFIG->get('license_lms_users'))) {
						$do_add = true;
					}
					if (!$do_add) {
						$query = "SELECT count(*) FROM #__lms_users_in_groups WHERE user_id IN (".$cids.")";
						$JLMS_DB->SetQuery( $query );
						if ($JLMS_DB->LoadResult()) {
							$do_add = true;
						}
					}
				} else {
					$do_add = true;
				}
				if ($do_add) {
					$access_period_type = intval(mosGetParam($_REQUEST, 'access_period_type', 1));
					if ($access_period_type == 2) {
						$publish_start = intval(mosGetParam($_REQUEST, 'publish_start', 0));
						$publish_end = intval(mosGetParam($_REQUEST, 'publish_end', 0));
						if ($publish_start) {
							$start_date = mosGetParam($_REQUEST, 'start_date', '');
							$start_date = JLMS_dateToDB($start_date);
						} else { $start_date = ''; }
						if ($publish_end) {
							$end_date = mosGetParam($_REQUEST, 'end_date', '');
							$end_date = JLMS_dateToDB($end_date);
						} else { $end_date = ''; }
					} elseif ($access_period_type == 3) {
						$publish_start = 1;
						$publish_end = 1;
						$start_date = date('Y-m-d');
						$days = intval(mosGetParam($_REQUEST, 'days_number', 0));
						$end_date = date('Y-m-d', time() + $days*24*60*60);
					} else { //($access_period_type == 1)
						if($JLMS_CONFIG->get('flms_integration', 0)){ //naverno nugno dobavit
							if($JLMS_CONFIG->get('use_global_groups', 1)){
								//FLMS mod
								$access_pediod_global = FLMS_access_global($cid, $course_id);
								$publish_start = $access_pediod_global->publish_start;
								$publish_end = $access_pediod_global->publish_end;
								$start_date = $access_pediod_global->start_date;
								$end_date = $access_pediod_global->end_date;
							} else {
								$publish_start = 0;
								$publish_end = 0;
								$start_date = '';
								$end_date = '';
							}
						} else {
							$publish_start = 0;
							$publish_end = 0;
							$start_date = '';
							$end_date = '';
						}
					}

					for($i=0;$i<count($cid);$i++){
						$query = "INSERT INTO #__lms_users_in_groups (course_id, group_id, user_id, role_id, teacher_comment, publish_start, start_date, publish_end, end_date, enrol_time ) VALUES('".$course_id."', '".$group_id."', '".$cid[$i]."', '".$role_id."', '".$teacher_comment."', ".$publish_start.", '".$start_date."', ".$publish_end.", '".$end_date."', '". JLMS_gmdate()."')";
						$JLMS_DB->SetQuery( $query );
						if( $JLMS_DB->query() )
						{
							$e_course = new stdClass();
							$e_course->course_alias = '';
							$e_course->course_name = '';

							$query = "SELECT course_name, name_alias FROM #__lms_courses WHERE id = '".$course_id."'";
							$JLMS_DB->setQuery( $query );
							$e_course = $JLMS_DB->loadObject();

							$e_user = new stdClass();
							$e_user->name = '';
							$e_user->email = '';
							$e_user->username = '';

							$query = "SELECT email, name, username FROM #__users WHERE id = '".$cid[$i]."'";
							$JLMS_DB->setQuery( $query );
							$e_user = $JLMS_DB->loadObject();

							$e_params['user_id'] = $cid[$i];
							$e_params['course_id'] = $course_id;
							$e_params['markers']['{email}'] = $e_user->email;
							$e_params['markers']['{name}'] = $e_user->name;
							$e_params['markers']['{username}'] = $e_user->username;
							$e_params['markers']['{coursename}'] = $e_course->course_name;
							$e_params['markers']['{courselink}'] = JLMSEmailRoute("index.php?option=com_joomla_lms&Itemid=$Itemid&task=details_course&id=$course_id");
							$e_params['markers_nohtml']['{courselink}'] = $e_params['markers']['{courselink}'];
							$e_params['markers']['{courselink}'] = '<a href="'.$e_params['markers']['{courselink}'].'">'.$e_params['markers']['{courselink}'].'</a>';
							$e_params['markers']['{lmslink}'] = JLMSEmailRoute("index.php?option=com_joomla_lms&Itemid=$Itemid");
							$e_params['markers_nohtml']['{lmslink}'] = $e_params['markers']['{lmslink}'];
							$e_params['markers']['{lmslink}'] = '<a href="'.$e_params['markers']['{lmslink}'].'">'.$e_params['markers']['{lmslink}'].'</a>';

							$e_params['action_name'] = 'OnEnrolmentInCourseByTeacher';

							$_JLMS_PLUGINS->loadBotGroup('emails');
							$_JLMS_PLUGINS->loadBotGroup('system');
							$e_params['user_id'] = $cid[$i];
							$e_params['course_ids'] = array($course_id);
							$plugin_result_array = $_JLMS_PLUGINS->trigger('OnEnrolmentInCourseByTeacher', array (& $e_params));
						}

						$user_info = new stdClass();
						$user_info->user_id = $cid[$i];
						$user_info->group_id = $group_id;
						$user_info->course_id = $course_id;
						$_JLMS_PLUGINS->loadBotGroup('user');
						$_JLMS_PLUGINS->trigger('onCourseJoin', array($user_info));
					}
				}
			}
		} elseif ($utype == 2 && $role_id) {
			$JLMS_ACL = JLMSFactory::getACL();
			if (intval($c2) == 0 && $JLMS_ACL->GetTypeofRole($role_id) == 5) {
				for($i=0;$i<count($cid);$i++){
					$query = "INSERT INTO #__lms_user_courses (user_id, course_id, role_id) VALUES('".$cid[$i]."', '".$course_id."', '".$role_id."')";
					$JLMS_DB->SetQuery( $query );
					$JLMS_DB->query();
				}
			}
		}
	}
	$link = "index.php?option=$option&Itemid=$Itemid&task=view_users&course_id=$course_id&id=$group_id";
	if ($JLMS_CONFIG->get('use_global_groups', 1)) $link = "index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id";
	if ($utype == 2) { $link = "index.php?option=$option&Itemid=$Itemid&task=view_assistants&id=$course_id"; }
	JLMSRedirect( sefRelToAbs( $link ) );
}
function JLMS_editUserSave( $option ) {
	global $Itemid, $my, $JLMS_DB, $JLMS_CONFIG;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$group_id = intval(mosGetParam($_REQUEST, 'group_id', 0));
	$user_id = intval(mosGetParam($_REQUEST, 'user_id', 0));

	$JLMS_ACL = JLMSFactory::getACL();
	$default_role = $JLMS_ACL->defaultRole(1);
	$role_id = intval(mosGetParam($_REQUEST, 'role_id', $default_role));
	$JLMS_ACL = JLMSFactory::getACL();
	$teacher_comment = strval(JLMS_getParam_LowFilter($_REQUEST, 'teacher_comment', ''));
	$iFilter = new JLMS_InputFilter(null,null,1,1);
	$teacher_comment = $iFilter->process( $teacher_comment );
	$teacher_comment = JLMSDatabaseHelper::GetEscaped( $teacher_comment );

	if (($course_id && $user_id) && $JLMS_ACL->CheckPermissions('users', 'manage') && ( ($group_id && JLMS_GetGroupCourse($group_id) == $course_id) || !$group_id) ) {
		$query = "SELECT * FROM #__lms_users_in_groups WHERE course_id = '".$course_id."' AND user_id = '".$user_id."'";
		$JLMS_DB->SetQuery( $query );
		$old_user_info = $JLMS_DB->LoadObjectList();
		if (count($old_user_info)) {
			$access_period_type = intval(mosGetParam($_REQUEST, 'access_period_type', 1));
			if ($access_period_type == 2) {
				$publish_start = intval(mosGetParam($_REQUEST, 'publish_start', 0));
				$publish_end = intval(mosGetParam($_REQUEST, 'publish_end', 0));
				if ($publish_start) {
					$start_date = mosGetParam($_REQUEST, 'start_date', '');
					$start_date = JLMS_dateToDB($start_date);
				} else { $start_date = ''; }
				if ($publish_end) {
					$end_date = mosGetParam($_REQUEST, 'end_date', '');
					$end_date = JLMS_dateToDB($end_date);
				} else { $end_date = ''; }
			} elseif ($access_period_type == 3) {
				$publish_start = 1;
				$publish_end = 1;
				$start_date = date('Y-m-d');
				$days = intval(mosGetParam($_REQUEST, 'days_number', 0));
				$end_date = date('Y-m-d', time() + $days*24*60*60);
			} else { //($access_period_type == 1)
				$publish_start = 0;
				$publish_end = 0;
				$start_date = '';
				$end_date = '';
			}

			$enrol_time = JLMS_addJoomlaLocale( 'Y-m-d H:i' , JLMS_dateToDB(mosGetParam($_REQUEST, 'enrol_time_date', '')), 'todb' );


			$query = "UPDATE #__lms_users_in_groups SET role_id = '".$role_id."', teacher_comment = '".$teacher_comment."', group_id = '".$group_id."', publish_start = $publish_start, start_date = '".$start_date."', publish_end = $publish_end, end_date = '".$end_date."', enrol_time = '".$enrol_time."'"
			. "\n WHERE course_id = '".$course_id."' AND user_id = '".$user_id."'";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
		}
	}
	if ($JLMS_CONFIG->get('use_global_groups', 1)) JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id") );
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_users&course_id=$course_id&id=$group_id") );
}
function JLMS_deleteUsersFromGroup( $group_id, $option, $manual = false, $utype = 1, $course_id = 0, $cid = array(0), $del_group = false ) {
	global $Itemid, $my, $JLMS_DB, $JLMS_CONFIG;
	if (!$manual) {
		$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
		$utype = intval(mosGetParam($_REQUEST, 'utype', 1));
	}
	$JLMS_ACL = JLMSFactory::getACL();
	if ($utype != 1 && $utype != 2) { $utype = 1; }
	if ( ($course_id) && $JLMS_ACL->CheckPermissions('users', 'manage') && ( ($utype == 1 && (($group_id &&  JLMS_GetGroupCourse($group_id) == $course_id) || !$group_id )) || ($utype == 2 && ($JLMS_ACL->CheckPermissions('users', 'manage_teachers'))) ) ) {
		if (!$manual) {
			$cid = mosGetParam( $_POST, 'cid', array(0) );
			if (!is_array( $cid )) { $cid = array(0); }
			$i = 0;
			while ($i < count($cid)) {
				$cid[$i] = intval($cid[$i]);
				$i ++;
			}
		}
		$cids = implode(',',$cid);
		if ($utype == 1) {
			$query = "SELECT b.*, c.ug_name, a.group_id "
			."\n FROM #__users as b, "
			."\n #__lms_users_in_groups as a "
			."\n LEFT JOIN #__lms_usergroups as c ON a.group_id = c.id AND c.course_id = '".$course_id."' "
			."\n WHERE b.id = a.user_id AND a.course_id = '".$course_id."' AND a.group_id = '".$group_id."' AND a.user_id IN ($cids)";
			$JLMS_DB->SetQuery( $query );
			$del_users = $JLMS_DB->LoadObjectList();
			if (count($del_users)) {
				$lists = array();
				JLMS_course_users_html::confirm_delUsers( $del_users, $lists, $option, $course_id, $group_id, $utype, $del_group);
			} else {
				if ($del_group) {
					JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id") );
				} else {
					JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_users&course_id=$course_id&id=$group_id") );
				}
			}
		} elseif ($utype == 2) {
			$JLMS_ACL = JLMSFactory::getACL();
			$ass_roles = $JLMS_ACL->GetSystemRolesIds(5, true);
			$del_users = array();
			if (count($ass_roles)) {
				$ass_roles_str = implode(',',$ass_roles);
				$query = "SELECT b.* "
				."\n FROM #__users as b, "
				."\n #__lms_user_courses as a "
				."\n WHERE b.id = a.user_id AND a.course_id = '".$course_id."' AND a.role_id IN ($ass_roles_str) AND a.user_id IN ($cids)";
				$JLMS_DB->SetQuery( $query );
				$del_users = $JLMS_DB->LoadObjectList();
			}
			if (count($del_users)) {
				$lists = array();
				JLMS_course_users_html::confirm_delUsers( $del_users, $lists, $option, $course_id, $group_id, $utype, false);
			} else {
				JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_assistants&id=$course_id") );
			}
		}
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
	}
}

function JLMS_deleteUsersGlobal( $option ) {
	global $Itemid, $my, $JLMS_DB, $JLMS_CONFIG;

	//$course_id = intval(mosGetParam($_REQUEST, 'id', 0));
	$course_id = $JLMS_CONFIG->get('course_id',0);
	$utype = intval(mosGetParam($_REQUEST, 'utype', 1));
	$group_id = 0;
	$JLMS_ACL = JLMSFactory::getACL();

	if ($utype != 1 && $utype != 2) { $utype = 1; }
	if ( ($course_id) && $JLMS_ACL->CheckPermissions('users', 'manage') && ($utype == 1 || ($utype == 2 && ($JLMS_ACL->CheckPermissions('users', 'manage_teachers'))))) {
		$cid = mosGetParam( $_POST, 'cid', array(0) );
		if (!is_array( $cid )) { $cid = array(0); }
		for ($i=0; $i<count($cid); $i++) $cid[$i] = intval($cid[$i]);
		$cids = implode(',',$cid);
		if ($utype == 1) {
			$query = "SELECT *, id AS user_id FROM #__users WHERE id IN ($cids)";
			$JLMS_DB->SetQuery( $query );
			$del_users = $JLMS_DB->LoadObjectList();

			$query = "SELECT ug.user_id, g.ug_name, g.id AS group_id"
			."\n FROM #__lms_users_in_global_groups AS ug, "
			."\n #__lms_usergroups AS g"
			."\n WHERE ug.group_id=g.id AND ug.user_id IN ($cids)";
			$JLMS_DB->setQuery($query);
			$groups_array_raw = $JLMS_DB->loadObjectList();
			$groups_array = array();
			foreach ($groups_array_raw as $object) {
				isset($groups_array[$object->user_id]) ? $groups_array[$object->user_id] .= '<br />'.$object->ug_name : $groups_array[$object->user_id] = $object->ug_name;
			}
			foreach ($del_users as $key => $object) {
				isset($groups_array[$object->user_id]) ? $del_users[$key]->ug_name = $groups_array[$object->user_id] : $del_users[$key]->ug_name = '';
			}

			if (count($del_users)) {
				$lists = array();
				JLMS_course_users_html::confirm_delUsers( $del_users, $lists, $option, $course_id, 0, $utype, 0);
			} else {
				if ($del_group || $JLMS_CONFIG->get('use_global_groups', 1)) {
					JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id") );
				} else {
					JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_users&course_id=$course_id&id=$group_id") );
				}
			}
		} elseif ($utype == 2) {
			$JLMS_ACL = JLMSFactory::getACL();
			$ass_roles = $JLMS_ACL->GetSystemRolesIds(5, true);
			$del_users = array();
			if (count($ass_roles)) {
				$ass_roles_str = implode(',',$ass_roles);
				$query = "SELECT b.* FROM #__users as b, #__lms_user_courses as a WHERE b.id = a.user_id AND a.course_id = '".$course_id."' AND a.role_id IN ($ass_roles_str) AND a.user_id IN ($cids)";
				$JLMS_DB->SetQuery( $query );
				$del_users = $JLMS_DB->LoadObjectList();
			}
			if (count($del_users)) {
				$lists = array();
				JLMS_course_users_html::confirm_delUsers( $del_users, $lists, $option, $course_id, $group_id, $utype, false);
			} else {
				JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_assistants&id=$course_id") );
			}
		}
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
	}
}

function JLMS_deleteUsersFromCSV( $option ) {
	global $Itemid, $my, $JLMS_DB;

	$utype = 1;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$JLMS_ACL = JLMSFactory::getACL();
	if ( ($course_id) && $JLMS_ACL->CheckPermissions('users', 'manage') ) {
		$usernames = array();
		if ( isset($_FILES['csv_file']) ) {
			require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_csv_import.php");
			// make arrays of valid fields for error checking
			$fieldDescriptors = new DeImportFieldDescriptors();
			$fieldDescriptors->addRequired('username');
			$fieldDescriptors->addRequired('name');
			$fieldDescriptors->addRequired('email');

			$userfile			= mosGetParam($_FILES, 'csv_file', null);
			$userfileTempName	= $userfile['tmp_name'];
			$userfileName 		= $userfile['name'];

			$loader		= new DeCsvLoader();
			$loader->setFileName($userfileTempName);
			if (!$loader->load()) {
				echo "<script> alert('import failed: ".$loader->getErrorMessage()."'); window.history.go(-1); </script>\n"; exit();
			}
			if (!JLMS_prepareImport($loader, $fieldDescriptors, true)) {
				echo "<script> alert('import failed'); window.history.go(-1); </script>\n"; exit();
			}

			$requiredFieldNames	= $fieldDescriptors->getRequiredFieldNames();
			$allFieldNames		= $fieldDescriptors->getFieldNames();

			// Prepare the data to be imported but first validate all entries before eventually importing
			// (a bit like a software transaction)
			$usernames	= array();
			$emails	= array();
			$ii = 0;
			while(!$loader->isEof()) {
				$values		= $loader->getNextValues();
				if (!JLMS_prepareImportRow($loader, $fieldDescriptors, $values, $requiredFieldNames, $allFieldNames, true)) {
					//echo "<script> alert('".$ii." row import failed'); window.history.go(-1); </script>\n"; exit();
				} else {
					$usernames[] = $values['username'];
					$emails[] = $values['email'];
				}
				$ii++;
			}
		}
		else {
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
		}

		$del_users = array();
		if (!empty($usernames)) {
			$query = "SELECT a.*, c.ug_name, b.group_id FROM #__users AS a, #__lms_users_in_groups AS b "
			."LEFT JOIN #__lms_usergroups as c ON b.group_id = c.id AND c.course_id = '".$course_id."' "
			."WHERE b.course_id = '".$course_id."' AND a.id = b.user_id AND (a.username IN ( '".implode("','",$usernames)."' ) OR a.email IN ( '".implode("','",$emails)."' )) ";

			$JLMS_DB->SetQuery( $query );
			$del_users = $JLMS_DB->LoadObjectList();
		}
		if (count($del_users)) {
			$lists = array();
			JLMS_course_users_html::confirm_delUsers( $del_users, $lists, $option, $course_id, -1, $utype, false);
		} else {
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id") );
		}
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
	}
}

function JLMS_deleteUsers_yes2( $option ) {
	global $Itemid, $my, $JLMS_DB;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$utype = 1;
	$JLMS_ACL = JLMSFactory::getACL();
	if (($course_id) && $JLMS_ACL->CheckPermissions('users', 'manage') ) {
		$cid = mosGetParam( $_POST, 'cid', array(0) );
		if (!is_array( $cid )) { $cid = array(0); }
		$i = 0;
		while ($i < count($cid)) {
			$cid[$i] = intval($cid[$i]);
			$i ++;
		}
		$cids = implode(',',$cid);
		if ($utype == 1) {
			@set_time_limit('3000');
			$query = "SELECT a.user_id FROM #__lms_users_in_groups as a WHERE a.course_id = '".$course_id."' AND a.user_id IN ($cids)";
			$JLMS_DB->SetQuery( $query );
			$del_ids = JLMSDatabaseHelper::LoadResultArray();
			if (count($del_ids)) {
				$group_ids = array();
				foreach($del_ids as $del_id) {
					$query = "SELECT a.group_id FROM #__lms_users_in_groups as a WHERE a.course_id = '".$course_id."' AND a.user_id = $del_id";
					$JLMS_DB->SetQuery( $query );
					$group_ids[] = $JLMS_DB->loadResult();
				}
				require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_del_operations.php");
				foreach($group_ids as $group_id) {
					JLMS_DelOp_deleteCourseStudents( $course_id, $group_id, $del_ids );
				}

				//section of plugin actions
				$_JLMS_PLUGINS->loadBotGroup('course');
				$users_joined = $_JLMS_PLUGINS->trigger('onCourseExpulsion', array($course_id));
				$users_joined = $users_joined[0];
				$users_info = array();
				foreach ($users_joined as $user) {
					$tmp = new stdClass();
					$tmp->user_id = $user->user_id;
					$tmp->course_id = $user->course_id;
					$tmp->teacher_id = $my->id;
					$users_info[] = $tmp;
				}
				//print_r($users_info);
				$_JLMS_PLUGINS->loadBotGroup('user');
				$_JLMS_PLUGINS->loadBotGroup('notifications');
				$_JLMS_PLUGINS->trigger('onCourseJoin', array($users_info));
			}
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id") );
		}
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
	}
}

function JLMS_deleteUsers_yes( $group_id, $option ) {
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	global $Itemid, $my, $JLMS_DB, $JLMS_SESSION, $JLMS_CONFIG;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$utype = intval(mosGetParam($_REQUEST, 'utype', 1));

	if ($utype != 1 && $utype != 2) { $utype = 1; }
	$JLMS_ACL = JLMSFactory::getACL();
	if (($course_id) && $JLMS_ACL->CheckPermissions('users', 'manage') && ( ( $utype == 1 && (($group_id &&  JLMS_GetGroupCourse($group_id) == $course_id) || !$group_id )) || ($utype == 2 && ($JLMS_ACL->CheckPermissions('users', 'manage_teachers'))) ) ) {
		$cid = mosGetParam( $_POST, 'cid', array(0) );
		if (!is_array( $cid )) { $cid = array(0); }
		$i = 0;
		while ($i < count($cid)) {
			$cid[$i] = intval($cid[$i]);
			$i ++;
		}
		$cids = implode(',',$cid);
		if ($utype == 1) {
			@set_time_limit('3000');
			$del_ids = $cid;
			if (count($del_ids)) {
				require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_del_operations.php");
				JLMS_DelOp_deleteCourseStudents( $course_id, $group_id, $del_ids );

				//section of plugin actions
				$_JLMS_PLUGINS->loadBotGroup('course');
				$users_joined = $_JLMS_PLUGINS->trigger('onCourseExpulsion', array($course_id));

				if (isset($users_joined[0]) && count($users_joined[0])) {
					$users_joined = $users_joined[0];
					$users_info = array();
					foreach ($users_joined as $user) {
						$tmp = new stdClass();
						$tmp->user_id = $user->user_id;
						$tmp->course_id = $user->course_id;
						$tmp->teacher_id = $my->id;
						$users_info[] = $tmp;
					}

					if (count($users_info)) {
						$_JLMS_PLUGINS->loadBotGroup('user');
						$_JLMS_PLUGINS->loadBotGroup('notifications');
						$_JLMS_PLUGINS->trigger('onCourseJoin', array($users_info));
					}
				}

				//b Events JLMS Plugins
				$_JLMS_PLUGINS->loadBotGroup('system');
				$plg_params = array();
				$plg_params['user_ids'] = $cid;
				$plg_params['course_ids'] = array($course_id);
				$_JLMS_PLUGINS->trigger('onRemoveUserCourse', array(&$plg_params));
				//e Events JLMS Plugins
			}
			$del_group = intval(mosGetParam($_REQUEST, 'del_group', 0));
			if ($del_group) {
				if (is_array($users_joined) && count($users_joined)) {
					$JLMS_SESSION->set('redirect_after_mail', sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id"));
					JLMSRedirect( sefRelToAbs("index.php?tmpl=component&option=com_joomla_lms&task=mail_main&assigned=$my->id") );
				} else {
					JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id") );
				}
			} else {
				if (is_array($users_joined) && count($users_joined)) {
					if ($JLMS_CONFIG->get('use_global_groups', 1)) $JLMS_SESSION->set('redirect_after_mail', sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id"));
					else $JLMS_SESSION->set('redirect_after_mail', sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_users&course_id=$course_id&id=$group_id"));
					JLMSRedirect( sefRelToAbs("index.php?tmpl=component&option=com_joomla_lms&task=mail_main&assigned=$my->id") );
				} else {
					if ($JLMS_CONFIG->get('use_global_groups', 1)) JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id") );
					else JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_users&course_id=$course_id&id=$group_id") );
				}
			}
		} elseif ($utype == 2) {
			require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_del_operations.php");
			// [TODO] the possibility to remove teacher is still exists..... fix it !!!
			JLMS_DelOp_deleteCourseAssistants( $course_id, $cid );
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_assistants&id=$course_id") );
		}
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
	}
}
function JLMS_cancelUser( $option ) {
	global $Itemid;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$group_id = intval(mosGetParam($_REQUEST, 'group_id', 0));
	$utype = intval(mosGetParam($_REQUEST, 'utype', 1));
	if ($utype != 1 && $utype != 2) { $utype = 1; }
	if ($utype == 1) {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_users&course_id=$course_id&id=$group_id") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_assistants&id=$course_id") );
	}
}

function JLMS_importUsersCSV($group_id, $option){
	global $JLMS_DB;

	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$group_id2 = intval(mosGetParam($_REQUEST, 'group_id', 0));
	if ($group_id2) {
		$group_id = $group_id2;
	}

	$utype = intval(mosGetParam($_REQUEST, 'utype', 1));
	if ($utype != 1 && $utype != 2) { $utype = 1; }

	$lists['disabled_import'] = 0;
	if(isset($JLMS_ACL->_role_type) && $JLMS_ACL->_role_type == 3){
		$lists['disabled_import'] = 1;
	}

	//Roles
	$role_id = mosGetParam($_REQUEST, 'role_id', $JLMS_ACL->defaultRole(1));

	$assis_roles = $JLMS_ACL->GetSystemRoles(1, true);//$db->loadObjectList();
	for ($i = 0, $n = count($assis_roles); $i < $n; $i ++) {
		$assis_roles[$i]->value = $assis_roles[$i]->id;
		$assis_roles[$i]->text = $assis_roles[$i]->lms_usertype;
	}

	if(count($assis_roles) > 1){
		$roles = array();
		$roles = array_merge($roles, $assis_roles);
		$lists['role'] = mosHTML::selectList( $roles, 'role_id', 'class="text_area" size="1"', 'value', 'text', $role_id );
	} else if(count($assis_roles)){
		$lists['role_id'] = $assis_roles[0]->value;
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=view_users&course_id=$course_id&id=$group_id") );
	}
	/* Following code allow import users to global groups 
	$user = JFactory::getUser();
	$JLMS_ACL = JLMSFactory::getACL();
	
	if( $JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ) 
	{
		$query = "SELECT a.id as value, a.ug_name as `text` FROM #__lms_usergroups as a, #__lms_user_assign_groups AS b"
		. "\n WHERE a.course_id = 0 AND (b.group_id = a.id OR b.group_id = a.parent_id) AND b.user_id = '".$user->get('id')."'"
		. "\n ORDER BY a.ug_name";
	} else {
		$query = "SELECT a.id as value, a.ug_name as `text` FROM #__lms_usergroups as a"
		. "\n WHERE a.course_id = 0"
		. "\n ORDER BY a.ug_name";
	}
	$JLMS_DB->setQuery($query);
		
	$impGroups[] = mosHTML::makeOption( '', '' );
	$impGroups = array_merge( $impGroups, $JLMS_DB->loadObjectList() );	
	
	$query = "SELECT group_id FROM #__lms_user_assign_groups WHERE user_id = ".$user->get('id');			
	$JLMS_DB->setQuery($query);
	$managerGroups = $JLMS_DB->loadColumn();
		
	$lists['jlms_groups_import'] = mosHTML::selectList( $impGroups, 'class_id_import[]', 'multiple="multiple" style="width:322px;"', 'value', 'text', $managerGroups );
	*/
	JLMS_course_users_html::showImportUsersCSV($option, $course_id, $group_id, $utype, $lists);
}

//import users from CSV-file to Joomla!
function JLMS_importUsers( $option ) {
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$Itemid = $JLMS_CONFIG->get('Itemid');
	global $my, $JLMS_DB, $Itemid;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$group_id = intval(mosGetParam($_REQUEST, 'group_id', -1));

	$acl = & JLMSFactory::getJoomlaACL();
/*
	$can_reg_users = true;

	$usersConfig = & JComponentHelper :: getParams('com_users');
	if (!$usersConfig->get('allowUserRegistration')) {
		$can_reg_users = false;
	}
	if (!$can_reg_users) {
		$link = "index.php?option=$option&Itemid=$Itemid&task=view_users&course_id=$course_id&id=$group_id";
		if ($JLMS_CONFIG->get('use_global_groups', 1)) $link = "index.php?option=$option&Itemid=$Itemid&task=course_users&id=$course_id";
		JLMSRedirect( sefRelToAbs($link), "User registration is disabled");
	}
*/
	$JLMS_ACL = JLMSFactory::getACL();
	$import_log = array();
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$role_id = intval(mosGetParam($_REQUEST, 'role_id', $JLMS_ACL->defaultRole(1)));
	$group_id = intval(mosGetParam($_REQUEST, 'group_id', -1));
	$impGroups = mosGetParam( $_POST, 'class_id_import', array(0) );	
	$teacher_comment = strval(JLMS_getParam_LowFilter($_REQUEST, 'teacher_comment', ''));
	$iFilter = new JLMS_InputFilter(null,null,1,1);
	$teacher_comment = $iFilter->process( $teacher_comment );
	$teacher_comment = JLMSDatabaseHelper::GetEscaped( $teacher_comment );
	
	if ( $course_id && ($group_id != -1) && $JLMS_ACL->CheckPermissions('users', 'manage') && $JLMS_ACL->CheckPermissions('users', 'import_users') && ($group_id == 0 || JLMS_GetGroupCourse($group_id) == $course_id) ) {
		if ( ($JLMS_CONFIG->get('allow_import_users') == 1) && isset($_FILES['csv_file']) ) {
			require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_csv_import.php");
			// make arrays of valid fields for error checking
			$fieldDescriptors = new DeImportFieldDescriptors();
			$fieldDescriptors->addRequired('username');
			$fieldDescriptors->addRequired('name');
			$fieldDescriptors->addRequired('email');
			$fieldDescriptors->addOptional('password', $JLMS_CONFIG->get('new_user_password'));
			$fieldDescriptors->addOptional('ceo', 0);
			$fieldDescriptors->addOptional('usergroup', '');

			$userfile			= mosGetParam($_FILES, 'csv_file', null);
			$userfileTempName	= $userfile['tmp_name'];
			$userfileName 		= $userfile['name'];

			$loader		= new DeCsvLoader();
			$loader->setFileName($userfileTempName);

			if (!$loader->load()) {
				echo "<script> alert('import failed: ".$loader->getErrorMessage()."'); window.history.go(-1); </script>\n"; exit();
			}

			//echo '<pre>';
			//print_r($loader);
			//echo '</pre>';
			//die;

			if (!JLMS_prepareImport($loader, $fieldDescriptors, true)) {
				echo "<script> alert('import failed'); window.history.go(-1); </script>\n"; exit();
			}

			$requiredFieldNames	= $fieldDescriptors->getRequiredFieldNames();
			$allFieldNames		= $fieldDescriptors->getFieldNames();

			// Prepare the data to be imported but first validate all entries before eventually importing
			// (a bit like a software transaction)
			$rows	= array();
			$ceos	= array();
			$usergroup_field_present = false;
			$ii = 0;
			while(!$loader->isEof()) {
				$values		= $loader->getNextValues();

				//echo '<pre>';
				//print_r($values);
				//echo '</pre>';

				if (!JLMS_prepareImportRow($loader, $fieldDescriptors, $values, $requiredFieldNames, $allFieldNames, true)) {
					//echo "<script> alert('".$ii." row import failed'); window.history.go(-1); </script>\n"; exit();
				} else {
					if (isset($row)) {
						unset($row);
					}
					$row = new mosUser( $JLMS_DB );

					if (!$row->bind( $values )) { mosErrorAlert( $row->getError() ); }

					mosMakeHtmlSafe($row);

					$row->id 		= 0;
					if( !JLMS_J30version() ) 
					{
						$row->usertype 	= 'Registered';
					}
					if( JLMS_J16version() ) {
						$row->groups = $acl->get_group_id( 'Registered', 'ARO' );
					} else {
						$row->gid = $acl->get_group_id( 'Registered', 'ARO' );
					}	
					

					if (!$row->check()) {
						$row->is_checked = 0;
						$row->checked_msg = $row->getError();
					} else {
						$row->is_checked = 1;
						$row->checked_msg = '';
					}

					$pwd 				= $row->password;
					$row->pwd			= $pwd;
					$row->password 		= JLMS_HashPassword( $row->password );
					$row->registerDate 	= gmdate( 'Y-m-d H:i:s' );
					if (isset($values['ceo']) && $values['ceo']) {
						$ceos[] = & $row;
					} else {
						if (isset($values['usergroup']) && $values['usergroup']) {
							$usergroup_field_present = true;
							$row->usergroup = $values['usergroup'];
						}
						$rows[] = & $row;
					}
				}
				$ii ++;
			}

			$ceo_ids = array();
			$is_cb_installed = $JLMS_CONFIG->get('is_cb_installed');
			if (count($ceos)) {
				$do_add = false;
				if ($JLMS_CONFIG->get('license_lms_users')) {
					$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
					$JLMS_DB->SetQuery( $query );
					$total_students = $JLMS_DB->LoadResult();
					if (intval($total_students) < intval($JLMS_CONFIG->get('license_lms_users'))) {
						$do_add = true;
					}
				} else {
					$do_add = true;
				}
				if ($do_add) {
					foreach(array_keys($ceos) as $ck) {
						$row_ceo = & $ceos[$ck];

						$i_log = new StdClass();
						$i_log->userinfo = '<b>'.$row_ceo->username.'</b>, '.$row_ceo->name.' ('.$row_ceo->email.') '._JLMS_USER_PASS.' <b>'.$row_ceo->pwd.'</b>';
						unset($row_ceo->pwd);
						if ($row_ceo->is_checked == 1) {
							unset($row_ceo->is_checked);
							unset($row_ceo->checked_msg);
							if (!$row_ceo->store()) {
								$i_log->result = '<b>'.$row_ceo->getError().'</b>';
							} else {
								$i_log->result = _JLMS_RESULT_OK;
								$row_ceo->checkin();
								$ceo_id = $row_ceo->id;
								$ceo_ids[] = $ceo_id;
								if ($is_cb_installed) {
									$query = "INSERT INTO `#__comprofiler` (`id`,`user_id`) VALUES ('".$user_id."', '".$user_id."')";
									$JLMS_DB->SetQuery($query);
									$JLMS_DB->query();
								}
								$i_log->result .= ' - <b>CEO</b>';
							}
						} else {
							// TODO: fix this code for J1.5 - check the same code below.
							if ( defined('_REGWARN_INUSE') && ($row_ceo->checked_msg == _REGWARN_INUSE || $row_ceo->checked_msg == _REGWARN_EMAIL_INUSE)) {
								$ceo_id = 0;
								// change userinfo (we don't know anything about his password)
								$i_log->userinfo = '<b>'.$row_ceo->username.'</b>, '.$row_ceo->name.' ('.$row_ceo->email.')';
								if (defined('_REGWARN_INUSE') && $row_ceo->checked_msg == _REGWARN_INUSE) {
									$query = "SELECT id FROM #__users WHERE username = '".$row_ceo->username."'";
									$JLMS_DB->SetQuery( $query );
									$ceo_id = $JLMS_DB->LoadResult();
									$i_log->result = _JLMS_USERS_CSV_ER_NAME;
								}
								if ($ceo_id > 0 ) {
									$ceo_ids[] = $ceo_id;
									$i_log->result .= ' - <b>CEO</b>';
								} else {
									$i_log->result = _JLMS_USERS_CSV_ER_LEARNER;
								}
							} else {
								$i_log->result = $row_ceo->checked_msg;
							}
						}
						$import_log[] = $i_log;
					}
				}
			}

			// Finally import the data
			$is_cb_installed = $JLMS_CONFIG->get('is_cb_installed');
			$usergroups_arr = array();
			if ($usergroup_field_present) {
				if ($JLMS_CONFIG->get('use_global_groups', 1)) {
					$query = "SELECT * FROM #__lms_usergroups WHERE course_id = 0";
					$JLMS_DB->SetQuery($query);
					$usergroups_arr = $JLMS_DB->LoadObjectList();
				}
			}
			foreach(array_keys($rows) as $k) {
				$do_add = false;
				if ($JLMS_CONFIG->get('license_lms_users')) {
					$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
					$JLMS_DB->SetQuery( $query );
					$total_students = $JLMS_DB->LoadResult();
					if (intval($total_students) < intval($JLMS_CONFIG->get('license_lms_users'))) {
						$do_add = true;
					}
				} else {
					$do_add = true;
				}
				
				$row_user	=& $rows[$k];
				$i_log = new StdClass();
				$i_log->userinfo = '<b>'.$row_user->username.'</b>, '.$row_user->name.' ('.$row_user->email.') '._JLMS_USER_PASS.' <b>'.$row_user->pwd.'</b>';

				$email_params['user_was_added'] = false;
				$email_params['group_id'] = 0;
				$email_params['course_id'] = 0;
				
				$pwd = $row_user->pwd;
				unset($row_user->pwd);
				
				//fix for exist users, email notification
				$query = "SELECT id FROM #__users WHERE username = '".$row_user->username."'";
				$JLMS_DB->setQuery($query);
				$db_user_id = $JLMS_DB->loadResult();
				if(isset($db_user_id) && $db_user_id){
					$pwd = _JLMS_CURRENT_PASSWORD;
				}
				//fix for exist users, email notification				
				if ($row_user->is_checked == 1) {
					if ($do_add) {					
						$user_usergroup_str = isset($row_user->usergroup) ? trim($row_user->usergroup) : '';					
						unset($row_user->is_checked);
						unset($row_user->usergroup);
						unset($row_user->checked_msg);
						if (!$row_user->store()) {
							$i_log->result = '<b>'.$row_user->getError().'</b>';
						} else {

							$email_params['user_was_added'] = true;

							if ($JLMS_CONFIG->get('use_global_groups', 1)) {
								$i_log->result = '';
							} else {
								$i_log->result = _JLMS_RESULT_OK;
							}
							$row_user->checkin();
							$user_id = $row_user->id;
							if ($is_cb_installed) {
								$query = "INSERT INTO `#__comprofiler` (`id`,`user_id`) VALUES ('".$user_id."', '".$user_id."')";
								$JLMS_DB->SetQuery($query);
								$JLMS_DB->query();
							}
							$query = "INSERT INTO #__lms_users_in_groups (course_id, group_id, user_id, role_id, teacher_comment, enrol_time) VALUES('".$course_id."', '".$group_id."', '".$user_id."', '".$role_id."', '".$teacher_comment."', '".JLMS_gmdate()."')";
							$JLMS_DB->SetQuery( $query );
							if ($JLMS_DB->query()) {

								$email_params['group_id'] = $group_id;
								$email_params['course_id'] = $course_id;

								if ($JLMS_CONFIG->get('use_global_groups', 1)) {
									$i_log->result .= _JLMS_RESULT_OK;
								} else {
									$i_log->result .= ' ('._JLMS_USER_ADD_TO_GROUP.' - '._JLMS_RESULT_OK.')';
								}
							} else {
								if ($JLMS_CONFIG->get('use_global_groups', 1)) {
									$i_log->result .= _JLMS_RESULT_FAIL;
								} else {
									$i_log->result .= ' ('._JLMS_USER_ADD_TO_GROUP.' - '._JLMS_RESULT_FAIL.')';
								}
							}
							if ($JLMS_CONFIG->get('use_global_groups', 1)) {
								if ($usergroup_field_present && $user_usergroup_str) {
									$global_group_id = 0;
									foreach ($usergroups_arr as $usergroups_item) {
										if ($usergroups_item->ug_name && $user_usergroup_str == $usergroups_item->ug_name ) {
											$global_group_id = $usergroups_item->id;
											break;
										}
									}
									if ($global_group_id) {
										$query = "INSERT INTO #__lms_users_in_global_groups (group_id, user_id) VALUES('".$global_group_id."', '".$user_id."')";
										$JLMS_DB->SetQuery( $query );
										if ($JLMS_DB->query()) {

											$email_params['group_id'] = $global_group_id;

											$i_log->result .= ' ('._JLMS_USER_ADD_TO_GROUP." '$user_usergroup_str' - "._JLMS_RESULT_OK.')';
										} else {
											$i_log->result .= ' ('._JLMS_USER_ADD_TO_GROUP." '$user_usergroup_str' - "._JLMS_RESULT_FAIL.')';
										}
									}
								} 							
							}
							
							if (count($ceo_ids)) {
								foreach ($ceo_ids as $ceo_id) {
									$query = "INSERT INTO #__lms_user_parents (user_id, parent_id) VALUES('".$user_id."', '".$ceo_id."')";
									$JLMS_DB->SetQuery( $query );
									$JLMS_DB->query();
								}
							}
						}
					} else {
						$i_log->result = _JLMS_USERS_CSV_ER_USER_LIMIT;
					}
				} else {
					// 12.10.2008 - issue with Joomla 1.5 is fixed !!!					
					if (( defined('_REGWARN_INUSE') && ($row_user->checked_msg == _REGWARN_INUSE || $row_user->checked_msg == _REGWARN_EMAIL_INUSE))
						|| (( class_exists('JText') && ($row_user->checked_msg == JText::_('WARNREG_INUSE') || $row_user->checked_msg == 'WARNREG_INUSE' || $row_user->checked_msg == JText::_( 'WARNREG_EMAIL_INUSE' )) || $row_user->checked_msg == JText::_('JLIB_DATABASE_ERROR_USERNAME_INUSE')) || $row_user->checked_msg == JText::_('JLIB_DATABASE_ERROR_EMAIL_INUSE') )) {						
						if ( $do_add ) 
						{	
							$user_id = 0;
							// change userinfo (we don't know anything about his password)
							$i_log->userinfo = '<b>'.$row_user->username.'</b>, '.$row_user->name.' ('.$row_user->email.')';
							if ( (defined('_REGWARN_INUSE') && $row_user->checked_msg == _REGWARN_EMAIL_INUSE) 
							|| (( class_exists('JText') && ($row_user->checked_msg == JText::_('WARNREG_INUSE') || $row_user->checked_msg == 'WARNREG_INUSE' || $row_user->checked_msg == JText::_( 'WARNREG_EMAIL_INUSE' )) || $row_user->checked_msg == JText::_('JLIB_DATABASE_ERROR_USERNAME_INUSE')) || $row_user->checked_msg == JText::_('JLIB_DATABASE_ERROR_EMAIL_INUSE') )) {
								$query = "SELECT id FROM #__users WHERE email = '".$row_user->email."'";
								$JLMS_DB->SetQuery( $query );
								$user_id = $JLMS_DB->LoadResult();
								$i_log->result = _JLMS_USERS_CSV_ER_EMAIL;
							}
							if ( (defined('_REGWARN_INUSE') && $row_user->checked_msg == _REGWARN_INUSE) 
							|| (( class_exists('JText') && ($row_user->checked_msg == JText::_('WARNREG_INUSE') || $row_user->checked_msg == 'WARNREG_INUSE' || $row_user->checked_msg == JText::_( 'WARNREG_EMAIL_INUSE' )) || $row_user->checked_msg == JText::_('JLIB_DATABASE_ERROR_USERNAME_INUSE')) )) {
								$query = "SELECT id FROM #__users WHERE username = '".$row_user->username."'";
								$JLMS_DB->SetQuery( $query );
								$user_id = $JLMS_DB->LoadResult();
								$i_log->result = _JLMS_USERS_CSV_ER_NAME;
							}
							$xid = 0;
							if ($user_id) {
								$query = "SELECT user_id FROM #__lms_users_in_groups WHERE course_id = $course_id AND user_id = $user_id";
								$JLMS_DB->SetQuery( $query );
								$xid = intval( $JLMS_DB->loadResult() );
							}
							
							if (!$xid && $user_id > 0 ) {
								$query = "INSERT INTO #__lms_users_in_groups (course_id, group_id, user_id, role_id, teacher_comment, enrol_time) VALUES('".$course_id."', '".$group_id."', '".$user_id."', '".$role_id."', '".$teacher_comment."', '".JLMS_gmdate()."')";
								$JLMS_DB->SetQuery( $query );
								if ($JLMS_DB->query()) {

									$email_params['group_id'] = $group_id;
									$email_params['course_id'] = $course_id;

									$i_log->result .= ' ('._JLMS_USER_ADD_TO_GROUP.' - '._JLMS_RESULT_OK.')';
								} else {
									$i_log->result .= ' ('._JLMS_USER_ADD_TO_GROUP.' - '._JLMS_RESULT_FAIL.')';
								}
								if (count($ceo_ids)) {
									foreach ($ceo_ids as $ceo_id) {
										$query = "SELECT count(*) FROm #__lms_user_parents WHERE user_id = $user_id AND parent_id = $ceo_id";
										$JLMS_DB->SetQuery( $query );
										$is_ex = $JLMS_DB->LoadResult();
										if (!$is_ex) {
											$query = "INSERT INTO #__lms_user_parents (user_id, parent_id) VALUES('".$user_id."', '".$ceo_id."')";
											$JLMS_DB->SetQuery( $query );
											$JLMS_DB->query();
										}
									}
								}
							} else {
								$i_log->result = _JLMS_USERS_CSV_ER_LEARNER;
							}						
						} else {
							$i_log->result = _JLMS_USERS_CSV_ER_USER_LIMIT;
						}
					} else {
						$i_log->result = $row_user->checked_msg;
					}
				}
				
				/* Following code allow import users to global groups 
				if( $do_add ) {					
					if( !empty($impGroups) ) {
						$impGroupStr = implode(',', $impGroups);
						$query = "
								SELECT group_id 
									FROM #__lms_users_in_global_groups 
									WHERE user_id = '".$user_id."' AND group_id IN(".$impGroupStr.") AND subgroup1_id = 0
								UNION ALL
								SELECT subgroup1_id 
									FROM #__lms_users_in_global_groups 
									WHERE user_id = '".$user_id."' AND subgroup1_id IN(".$impGroupStr.")									 
								";
						$JLMS_DB->SetQuery( $query );
						$eGroups = $JLMS_DB->loadResultArray();						
						$iGroups = array_diff($impGroups, $eGroups);							
						
						if( !empty($iGroups) ) 
						{
							$query = "SELECT id, ug_name, parent_id FROM #__lms_usergroups WHERE id IN (".implode(',', $iGroups).")";
							$JLMS_DB->SetQuery( $query );
							$groupsList = $JLMS_DB->loadObjectList('id');
						}
																						
						foreach( $iGroups AS $iGroup ) 
						{
							if( !isset($groupsList[$iGroup]->parent_id) ) 
							{
								continue;
							}
							
							if($groupsList[$iGroup]->parent_id) 
							{
								$query = "INSERT INTO #__lms_users_in_global_groups (group_id, user_id, subgroup1_id) VALUES('".$groupsList[$iGroup]->parent_id."', '".$user_id."', '".$iGroup."')";								
							} else {
								$query = "INSERT INTO #__lms_users_in_global_groups (group_id, user_id) VALUES('".$iGroup."', '".$user_id."')";
							}
							$JLMS_DB->SetQuery( $query );
							if ($JLMS_DB->query()) {

								$email_params['group_id'] = $iGroup;

								$i_log->result .= ' ('._JLMS_USER_ADD_TO_GROUP." '".$groupsList[$iGroup]->ug_name."' - "._JLMS_RESULT_OK.')';
							} else {
								$i_log->result .= ' ('._JLMS_USER_ADD_TO_GROUP." '".$groupsList[$iGroup]->ug_name."' - "._JLMS_RESULT_FAIL.')';
							}
						}
					}					
				}
				*/
				$import_log[] = $i_log;

				if( $email_params['user_was_added'] || $email_params['course_id'] || $email_params['group_id'] )
				{
					//send email to import user
					$course = new stdClass();
					$course->course_alias = '';
					$course->course_name = '';

					$query = "SELECT course_name, name_alias FROM #__lms_courses WHERE id = '".$email_params['course_id']."'";
					$JLMS_DB->setQuery( $query );
					$course = $JLMS_DB->loadObject();

					$user_group = new stdClass();
					$user_group->ug_name = '';

					$query = "SELECT ug_name  FROM #__lms_usergroups WHERE id = '".$email_params['group_id']."'";
					$JLMS_DB->setQuery( $query );
					$user_group = $JLMS_DB->loadObject();

					$params['user_id'] = $user_id;
					$params['course_id'] = $email_params['course_id'];

					$params['markers']['{password}'] = $pwd;
					$params['markers']['{email}'] = $row_user->email;
					$params['markers']['{name}'] = $row_user->name;
					$params['markers']['{username}'] = $row_user->username;

					$params['wrappers']['{ifcreated}'] = 'hide';
					$params['wrappers']['{ifcourse}'] = 'hide';
					$params['wrappers']['{ifgroup}'] = 'hide';

					if( $email_params['user_was_added'] )
						$params['wrappers']['{ifcreated}'] = 'show';
					if( $email_params['course_id'] )
						$params['wrappers']['{ifcourse}'] = 'show';
					if( $email_params['group_id'] )
						$params['wrappers']['{ifgroup}'] = 'show';

					if( isset($course) )
						$params['markers']['{coursename}'] = $course->course_name;//( $course->course_alias )?$course->course_alias:$course->course_name;
					else
						$params['markers']['{coursename}'] = '';

					if( isset($user_group) )
						$params['markers']['{groupname}'] = $user_group->ug_name;
					else
						$params['markers']['{groupname}'] = '';


					$params['markers']['{courselink}'] = JRoute::_("index.php?option=com_joomla_lms&Itemid=$Itemid&task=details_course&id=$course_id");;
					$pos = strpos($params['markers']['{courselink}'], 'http');
					if ($pos !== 0) {
						$params['markers']['{courselink}'] = $JLMS_CONFIG->get('live_site').$params['markers']['{courselink}'];
					}
					$params['markers']['{courselink}'] = '<a href="'.$params['markers']['{courselink}'].'">'.$params['markers']['{courselink}'].'</a>';


					$params['markers']['{lmslink}'] = JRoute::_("index.php?option=com_joomla_lms&Itemid=$Itemid");;
					$pos = strpos($params['markers']['{lmslink}'], 'http');
					if ($pos !== 0) {
						$params['markers']['{lmslink}'] = $JLMS_CONFIG->get('live_site').$params['markers']['{lmslink}'];
					}

					$params['markers_nohtml']['{lmslink}'] = $params['markers']['{lmslink}'];
					$params['markers']['{lmslink}'] = '<a href="'.$params['markers']['{lmslink}'].'">'.$params['markers']['{lmslink}'].'</a>';

					$params['action_name'] = 'OnCSVImportUser';
					
					//echo '<pre>';
					//print_r($params);
					//echo '</pre>';
					//die;

					$_JLMS_PLUGINS->loadBotGroup('emails');
					$plugin_result_array = $_JLMS_PLUGINS->trigger('OnCSVImportUser', array (& $params));
					
					if( $params['course_id'] ) 
                    {
						$_JLMS_PLUGINS->loadBotGroup('system');
						$_JLMS_PLUGINS->trigger('OnCSVImportUserToCourse', array (& $params));						
                    }                  
				}
			}
		} // end if "($JLMS_CONFIG->get('allow_import_users') == 1)"
		JLMS_course_users_html::showImportLog( $import_log, $option, $course_id, $group_id );
	} else { // end if (checks)
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
	}

}
function JLMS_exportUserGroup( $course_id, $option ) {
	global $my, $JLMS_DB, $Itemid;
	$cid = mosGetParam( $_POST, 'cid', array(0) );
	if (!is_array( $cid )) { $cid = array(0); }
	$group_ids = $cid;//$cid[0];
	$error = false;
	foreach($group_ids as $group_id){
		if ( !($group_id == 0 || JLMS_GetGroupCourse($group_id) == $course_id) ) {
			$error = true;
			break;
		}
	}
	$JLMS_ACL = JLMSFactory::getACL();
	if ( $course_id && $group_ids && $JLMS_ACL->CheckPermissions('users', 'manage') && !$error ) {
		/*
		$query = "SELECT a.username, a.name, a.email, c.ug_name, b.teacher_comment"
		. "\n FROM #__users as a, #__lms_users_in_groups as b, #__lms_usergroups as c"
		. "\n WHERE a.id = b.user_id AND b.group_id = c.id AND c.id = '".$group_id."'";
		*/
		$query = "SELECT a.username, a.name, a.email, b.teacher_comment"
		. "\n FROM #__users as a, #__lms_users_in_groups as b "
		. "\n WHERE a.id = b.user_id AND b.course_id = ".$course_id." AND b.group_id IN ( ".implode(',', $group_ids)." ) ";
		$JLMS_DB->SetQuery($query);
		$exp_users = $JLMS_DB->LoadObjectList();
		if (count($exp_users)) {
			$ug_name = 'course_users_export';//$exp_users[0]->ug_name;
			$text_to_csv = 'username,name,email,teacher comments'."\n";
			foreach ($exp_users as $exp_user) {
				$text_to_csv .= $exp_user->username.",".$exp_user->name.",".$exp_user->email.",".JLMS_processCSVField(strip_tags($exp_user->teacher_comment))."\n";
			}
			if (preg_match('/Opera(\/| )([0-9].[0-9]{1,2})/', $_SERVER['HTTP_USER_AGENT'])) {
				$UserBrowser = "Opera";
			}
			elseif (preg_match('/MSIE ([0-9].[0-9]{1,2})/', $_SERVER['HTTP_USER_AGENT'])) {
				$UserBrowser = "IE";
			} else {
				$UserBrowser = '';
			}
			header("Content-type: application/csv");
			header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
			header("Content-Length: ".strlen(trim($text_to_csv)));
			header('Content-Disposition: attachment; filename="'.$ug_name.'.csv"');
			if ($UserBrowser == 'IE') {
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Pragma: public');
			} else {
				header('Pragma: no-cache');
			}
			echo "\xEF\xBB\xBF"; //UTF-8 BOM
			echo $text_to_csv;
			exit();
		}
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
	}
}

function & JLMS_USERS_getCourseUserGroups( $course_id ) {
	global $JLMS_DB;
	$query = "SELECT id, ug_name FROM #__lms_usergroups WHERE course_id = '".$course_id."' ORDER BY ug_name";
	$JLMS_DB->SetQuery( $query );
	$rows_groups = $JLMS_DB->LoadObjectList();
	$n_group = new stdClass();
	$n_group->id = 0;
	$n_group->ug_name = _JLMS_USER_NO_GROUP_NAME;
	$r_gr = array();
	$r_gr[] = $n_group;
	$rows_groups = array_merge($r_gr, $rows_groups);
	return $rows_groups;
}
?>