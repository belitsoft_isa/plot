<?php
/**
* joomla_lms.gradebook.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if(!function_exists('JLMS_GB_getUsersGrades')){
	require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_grades.lib.php");
}
require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.gradebook.html.php");

$task 	= mosGetParam( $_REQUEST, 'task', '' );
if ($task != 'crt_preview' && $task != 'gb_get_cert') {
	$JLMS_CONFIG = JLMSFactory::getConfig();
	
	$course_id = $JLMS_CONFIG->get('course_id',0);
	$pathway = array();
	$pathway[] = array('name' => _JLMS_PATHWAY_HOME, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"), 'is_home' => true);
	$pathway[] = array('name' => $JLMS_CONFIG->get('course_name'), 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$course_id"), 'is_course' => true);
	$pathway[] = array('name' => _JLMS_TOOLBAR_GRADEBOOK, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=gradebook&amp;id=$course_id"));
	JLMSAppendPathWay($pathway);
	$is_full = intval(mosGetParam( $_REQUEST, 'is_full', 0 ));
	if ($task == 'gradebook' && $is_full && (mosGetParam($_REQUEST,'view') != 'csv' && mosGetParam($_REQUEST,'view') != 'xls' && !mosGetParam($_REQUEST,'is_full'))){
		JLMS_ShowHeading($JLMS_CONFIG->get('course_name').' - '._JLMS_GB_TITLE, false);
	} else if($task == 'gradebook' && (mosGetParam($_REQUEST,'view') == 'csv' || mosGetParam($_REQUEST,'view') == 'xls') || mosGetParam($_REQUEST,'is_full')){
		if (mosGetParam($_REQUEST,'is_full') && mosGetParam($_REQUEST,'view') != 'csv' && mosGetParam($_REQUEST,'view') != 'xls') {
			/*echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'."\n";
			echo '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb">'."\n";
			echo '<head>'."\n";
			echo '<title>'.$JLMS_CONFIG->get('course_name').' - '._JLMS_GB_TITLE.'</title>'."\n";
			echo '<style type="text/css">'."\n";
			echo 'table.jlms_gradebook_fullview_table td {'."\n";
			echo '	border-top:1px solid grey;'."\n";
			echo '	border-right:1px solid grey;'."\n";
			echo '}'."\n";
			echo 'table.jlms_gradebook_fullview_table {'."\n";
			echo '	border-bottom:1px solid grey;'."\n";
			echo '	border-left:1px solid grey;'."\n";
			echo '}'."\n";
			echo '</style>'."\n";
			echo '</head>'."\n";
			echo '<body>'."\n";*/
			echo '<link rel="stylesheet" href="'.JLMSCSS::link().'" type="text/css" />';
		}
	} else {
		JLMS_ShowHeading();
	}
}
$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) );

switch ($task) {	
	case 'gradebook':
							global $JLMS_CONFIG;
							$mem_lim = $JLMS_CONFIG->get('memory_limit_gradebook', 0);
							if ($mem_lim) {
								JLMS_adjust_memory_limit($mem_lim);
							}
							JLMS_showGradebook( $id, $option );
	break;
	case 'gb_items':			JLMS_showGBItems( $id, $option );		break;
	case 'gbi_new':				JLMS_editGBItem( 0, $id, $option );		break;
	case 'gbi_edit':
					$cid = mosGetParam( $_POST, 'cid', array(0) );
					if (!is_array( $cid )) { $cid = array(0); }
					JLMS_editGBItem( $cid[0], $id, $option );			break;
	case 'gbi_editA':
					$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
					JLMS_editGBItem( $id, $course_id, $option );		break;
	case 'gbi_save':			JLMS_saveGBItem( $option );				break;
	case 'gbi_delete':			JLMS_deleteGBItems( $id, $option );		break;
	case 'gbi_orderup':
	case 'gbi_orderdown':		JLMS_orderGBItems( $id, $option );		break;
	case 'gbi_cancel':			JLMS_cancelGBItem( $option );			break;
	case 'gb_crtA':
	case 'gb_crt':				JLMS_markCertificate( $id, $option );	break;
	case 'gb_user_pdf':
	case 'gb_usergrade':	
							global $JLMS_CONFIG;
							$mem_lim = $JLMS_CONFIG->get('memory_limit_gradebook', 0);
							if ($mem_lim) {
								JLMS_adjust_memory_limit($mem_lim);
							}
							JLMS_showUserGrade( $id, $option );
	break;
	case 'gb_certificates':		JLMS_GB_certificate( $id, $option );	break;
	case 'crt_save':			JLMS_saveCertificateGB( $id, $option );	break;
	case 'crt_preview':			JLMS_previewCertificate( $id, $option);	break;
	case 'gb_get_cert':			JLMS_getCertificate( $id, $option );	break;
	case 'save_grades':			JLMS_saveUserGrades( $id, $option );	break;

	case 'gb_scale':			JLMS_showGBScale( $option );			break;
	
	case 'gbs_new':				JLMS_editGBScale( 0, $option );			break;
	case 'gbs_edit':
					$cid = mosGetParam( $_POST, 'cid', array(0) );
					if (!is_array( $cid )) { $cid = array(0); }
					JLMS_editGBScale( $cid[0], $option );				break;
	case 'gbs_editA':			JLMS_editGBScale( $id, $option );		break;
	case 'gbs_save':			JLMS_saveGBScale( $option );			break;
	case 'gbs_delete':			JLMS_deleteGBScale( $option );			break;
	case 'gbs_orderup':
	case 'gbs_orderdown':		JLMS_orderGBScale( $option );			break;
	case 'gbs_cancel':			JLMS_cancelGBScale( $option );			break;
	
	case 'gb_learning_path':	JLMS_showLearningPath( $id, $option );	break;
	
	case 'gb_setup_path':		JLMS_ListPath($id, $option, $course_id, $Itemid);	break;
	case 'gb_apply_path':
	case 'gb_save_path':		JLMS_savePath( $option );					break;
	case 'gb_del_path':			JLMS_removePath( $cid, $option );			break; 		
}
if ($task == 'gradebook' && mosGetParam($_REQUEST,'is_full') && mosGetParam($_REQUEST,'view') != 'csv' && mosGetParam($_REQUEST,'view') != 'xls') {
	//$JLMS_CONFIG = JLMSFactory::getConfig();
	//$JLMS_CONFIG->set('add_html_at_the_end', '</body></html>');
}

function JLMS_exportXLS($rows, $lists, $reporting_header){
	global $JLMS_DB, $JLMS_CONFIG;
	
	require_once(_JOOMLMS_FRONT_HOME . "/includes/libraries/lms.lib.reporting.php");
	
	$results = array();
	
	$course_info = array();
	$course_info_hits = array();
	$title_headers = array();
	$data_grade = array();
	
	if(isset($rows) && count($rows) && isset($lists) && count($lists)){
		
		$data_info = array();
		$data_grade = array();
		
		$data_info[0][] = _JLMS_GB_TBL_HEAD_STU;
		$data_info[0][] = _JLMS_GB_TBL_HEAD_GROUP;
		
		//Mod EOGN-1203
		if($JLMS_CONFIG->get('show_gradebook_cb_columns', false)){
			if(isset($lists['cb_fields']) && count('cb_fields')){
				foreach($lists['cb_fields'] as $cb_field_title){
					$data_grade[0][] = $cb_field_title;
				}
			}
		}
		if($JLMS_CONFIG->get('show_gradebook_enrollment_date', false)){
			$data_grade[0][] = _JLMS_ENROLL_TIME;
		}
		if($JLMS_CONFIG->get('show_gradebook_course_questions', false)){
			if(isset($lists['course_questions']) && count('course_questions')){
				foreach($lists['course_questions'] as $course_question){
					$data_grade[0][] = $course_question;
				}
			}
		}
		//Mod EOGN-1203
		
		$sc_num = 0;
		foreach ($lists['sc_rows'] as $sc_row){
			if ($sc_row->show_in_gradebook){
				$sc_num++;
				$data_grade[0][] = $sc_row->lpath_name;
			}
		}
		foreach ($lists['quiz_rows'] as $quiz_row){
			$data_grade[0][] = $quiz_row->c_title;
		}
		foreach ($lists['gb_rows'] as $gb_row) {
			$data_grade[0][] = $gb_row->gbi_name;
		}
		
		$data_grade[0][] = _JLMS_COURSE_COMPLETION_TABLE;
		$data_grade[0][] = _JLMS_COMPLETION_DATE;
		if($JLMS_CONFIG->get('enable_timetracking')){
			$data_grade[0][] = _JLMS_TIME_SPENT_TABLE;
		}
		
		$ii=1;
		for($i=0;$i<count($rows);$i++){
			$row = $rows[$i];
			
			$data_info[$ii][] = $row->username;
			$data_info[$ii][] = str_replace('<br />', ', ', $row->ug_name);
			
			//Mod EOGN-1203
			if($JLMS_CONFIG->get('show_gradebook_cb_columns', false)){
				if(isset($row->cb_fields) && count($row->cb_fields)){
					foreach($row->cb_fields as $cb_field){
						$data_grade[$ii][] = $cb_field;
					}
				}
			}
			if($JLMS_CONFIG->get('show_gradebook_enrollment_date', false)){
				$data_grade[$ii][] = JLMS_dateToDisplay($row->enrol_time);
			}
			if($JLMS_CONFIG->get('show_gradebook_course_questions', false)){
				if(isset($row->course_questions) && count($row->course_questions)){
					foreach($row->course_questions as $course_question){
						$data_grade[$ii][] = $course_question;
					}
				}
			}
			//Mod EOGN-1203
			
			$sc_num = 0;
			$sc_num2 = 0;
			foreach($lists['sc_rows'] as $sc_row){
				if($sc_row->show_in_gradebook){
					$sc_num++;
					$j = 0;
					while ($j < count($row->scorm_info)){
						if ($row->scorm_info[$j]->gbi_id == $sc_row->item_id) {
							if ($sc_num2 < $sc_num) {
								if ($row->scorm_info[$j]->user_status == -1) {
									$data_grade[$ii][] = '-';
								} else {
									$user_status = '';
									$user_status .= $row->scorm_info[$j]->user_status ? _CMN_YES : _CMN_NO;
									$user_status .= isset($row->scorm_info[$j]->user_grade) ? ' '.$row->scorm_info[$j]->user_grade : '';
									$user_status .= isset($row->scorm_info[$j]->user_pts) ? ' ('.$row->scorm_info[$j]->user_pts . _JLMS_GB_POINTS.')' : '';
									
									$data_grade[$ii][] = $user_status;
								}
								$sc_num2++;
							}
						}
						$j++;
					}
				}
			}
			foreach($lists['quiz_rows'] as $quiz_row){
				$j = 0;
				while ($j < count($row->quiz_info)) {
					if ($row->quiz_info[$j]->gbi_id == $quiz_row->c_id) {
						if ($row->quiz_info[$j]->user_status == -1) {
							$data_grade[$ii][] = '-';
						} else {
							$user_status = '';
//							$user_status .= $row->quiz_info[$j]->user_status ? _CMN_YES : _CMN_NO;
//							$user_status .= ' '.$row->quiz_info[$j]->user_grade.' ';
//							$user_status .= '('.$row->quiz_info[$j]->user_pts_full .')';

							$user_status = JLMS_showQuizStatus($row->quiz_info[$j], '', 1);
							
							$data_grade[$ii][] = $user_status;
						}
					}
					$j ++;
				}	
			}
			$j = 0;
			while ($j < count($row->grade_info)) {
				$data_grade[$ii][] = $row->grade_info[$j]->user_grade;
				$j ++;
			}

			$data_grade[$ii][] = $row->user_certificate ? _CMN_YES : _CMN_NO;
			$data_grade[$ii][] = $row->user_certificate_date ? JLMS_offsetDateToDisplay($row->user_certificate_date) : '-';
			if($JLMS_CONFIG->get('enable_timetracking')){
				$data_grade[$ii][] = isset($row->time_spent) ? $row->time_spent : '';
			}

			$ii++;
		}	
	}

	$results['data_info'] = $data_info;
	$results['data_grade'] = $data_grade;

	$data_tr = array();
	if(count($data_info)){
		$k = 1;
		foreach($data_info as $n=>$d){
			$data_tr[] = $k;
			$k = 3 - $k;
		}
	}
	$results['data_tr'] = $data_tr;

	global $task;
	if($task == 'gradebook'){
		$tmpl_name = 'gradebook_report';
		$prefix_title = str_replace("_", " ", $tmpl_name);
	}

	JLMS_reporting::exportXLS($results, $reporting_header, $tmpl_name, $prefix_title);
}

function JLMS_exportCSV($rows, $lists, $reporting_header){
	global $JLMS_DB, $JLMS_CONFIG;
	
	require_once(_JOOMLMS_FRONT_HOME . "/includes/libraries/lms.lib.reporting.php");
	
	$is_full = intval( mosGetParam( $_REQUEST, 'is_full', 0 ) );
	
	$data = array();
	if(isset($rows) && count($rows) && isset($lists) && count($lists)){
		
		if($is_full){
			$data[0][] = _JLMS_COURSES_TBL_HEAD_NAME;//'Course Title';
		}
		$data[0][] = str_replace(':', '', _JLMS_UI_NAME);
		$data[0][] = str_replace(':', '', _JLMS_UI_USERNAME);
		$data[0][] = str_replace(':', '', _JLMS_UI_EMAIL);
		$data[0][] = _JLMS_GB_TBL_HEAD_GROUP;
		
		if(isset($lists['regenroll_fields']) && count($lists['regenroll_fields'])){
			$regenroll_fields = $lists['regenroll_fields'];
			foreach($regenroll_fields as $field){
				$data[0][] = $field->title;	
			}
		}
		
		$data[0][] = _JLMS_COURSE_COMPLETION_TABLE;
		$data[0][] = _JLMS_COMPLETION_DATE;
		if($JLMS_CONFIG->get('enable_timetracking')){
			$data[0][] = _JLMS_TIME_SPENT_TABLE;
		}
		$data[0][] = str_replace(':', '', _JLMS_DATE);
	//	$data[0][] = 'Access#';//commented
		
		$sc_num = 0;
		foreach ($lists['sc_rows'] as $sc_row){
			if ($sc_row->show_in_gradebook){
				$sc_num++;
				$data[0][] = $sc_row->lpath_name;
			}
		}
		foreach ($lists['quiz_rows'] as $quiz_row){
			$data[0][] = $quiz_row->c_title;
		}
		foreach ($lists['gb_rows'] as $gb_row) {
			$data[0][] = $gb_row->gbi_name;
		}
		
		$ii=1;
		for($i=0;$i<count($rows);$i++){
			$row = $rows[$i];
			
			if($is_full){
				$data[$ii][] = $lists['course_name'];
			}
			
			$data[$ii][] = $row->name;
			$data[$ii][] = $row->username;
			$data[$ii][] = $row->email;
			$data[$ii][] = JLMS_processCSVField(str_replace('<br />', ', ', $row->ug_name));
			
			if(isset($lists['regenroll_fields']) && count($lists['regenroll_fields'])){
				$regenroll_fields = $lists['regenroll_fields'];
				$rows_regenroll = $lists['rows_regenroll'];
				foreach($regenroll_fields as $field){
					foreach($rows_regenroll as $row_regenroll){
						if($row->user_id == $row_regenroll->user_id){
							$field_name = $field->name;
							$data[$ii][] = $row->$field_name;
						}
					}
				}
			}
			
			$data[$ii][] = $row->user_certificate ? _CMN_YES : _CMN_NO;
			$data[$ii][] = $row->user_certificate_date ? JLMS_processCSVField(JLMS_offsetDateToDisplay($row->user_certificate_date)) : JLMS_processCSVField('-');
			if($JLMS_CONFIG->get('enable_timetracking')){ 
				$data[$ii][] = $row->time_spent; 
			}
			$data[$ii][] = JLMS_dateToDisplay($row->date_completed);
			//$data[$ii][] = $row->access;//commented
			
			$sc_num = 0;
			$sc_num2 = 0;
			foreach($lists['sc_rows'] as $sc_row){
				if($sc_row->show_in_gradebook){
					$sc_num++;
					$j = 0;
					while ($j < count($row->scorm_info)){
						if ($row->scorm_info[$j]->gbi_id == $sc_row->item_id) {
							if ($sc_num2 < $sc_num) {
								if ($row->scorm_info[$j]->user_status == -1) {
									$data[$ii][] = '-';
								} else {
									$user_status = '';
									$user_status .= $row->scorm_info[$j]->user_status ? _CMN_YES : _CMN_NO;
									$user_status .= isset($row->scorm_info[$j]->user_grade) ? ' '.$row->scorm_info[$j]->user_grade : '';
									$user_status .= isset($row->scorm_info[$j]->user_pts) ? ' ('.$row->scorm_info[$j]->user_pts . _JLMS_GB_POINTS.')' : '';
									
									$data[$ii][] = $user_status;
								}
								$sc_num2++;
							}
						}
						$j++;
					}
				}
			}
			foreach($lists['quiz_rows'] as $quiz_row){
				$j = 0;
				while ($j < count($row->quiz_info)) {
					if ($row->quiz_info[$j]->gbi_id == $quiz_row->c_id) {
						if ($row->quiz_info[$j]->user_status == -1) {
							$data[$ii][] = '-';
						} else {
							$user_status = '';
//							$user_status .= $row->quiz_info[$j]->user_status ? _CMN_YES : _CMN_NO;
//							$user_status .= ' '.$row->quiz_info[$j]->user_grade.' ';
//							$user_status .= '('.$row->quiz_info[$j]->user_pts_full .')';
							
							$user_status = JLMS_showQuizStatus($row->quiz_info[$j], '', 1);
							
							$data[$ii][] = $user_status;
						}
					}
					$j ++;
				}	
			}
			$j = 0;
			while ($j < count($row->grade_info)) {
				$data[$ii][] = $row->grade_info[$j]->user_grade;
				$j ++;
			}
			
			$ii++;
		}	
	}
	
	global $task;
	if($task == 'gradebook'){
		$tmpl_name = 'gradebook_report';
		$prefix_title = str_replace("_", " ", $tmpl_name);
	}
	JLMS_reporting::outputCSV($data, $tmpl_name);
}

function JLMS_ListPath($id, $option, $course_id, $Itemid)	{
	global $JLMS_DB;
	$JLMS_ACL = JLMSFactory::getACL();
	
	$JLMS_CONFIG = & JLMSFactory::getConfig();
	
	if ($course_id && $JLMS_ACL->CheckPermissions('gradebook', 'configure')) {
		$query = "SELECT b.lpath_name AS text, b.id AS value"
		. "\n FROM #__lms_gradebook_lpaths AS a, #__lms_learn_paths AS b"
		. "\n WHERE a.course_id = '".$course_id."'"
		. "\n AND a.learn_path_id = b.id"
		;
		$JLMS_DB->SetQuery( $query );
		$rows = $JLMS_DB->LoadObjectList();
		
		$lists = array();
		$lists['added_lpath'] = mosHTML::selectList( $rows, 'added_ids[]', 'class="inputbox chzn-done" size="7" multiple="multiple" style="width:100%; height: auto;"', 'value', 'text' ); 

		$query = "SELECT id as value, lpath_name as text, lp_type"
		. "\n FROM #__lms_learn_paths"
		. "\n WHERE course_id = '".$course_id."'"
		//. "\n AND item_id = 0" //(Max) Contractor-Licensing SCORM complete course
		. "\n AND published = 1"
		;
		$JLMS_DB->SetQuery( $query );		
		$path = $JLMS_DB->LoadObjectList();
		
		if($JLMS_CONFIG->get('enabled_distinction_lp_sc', false)){
			if(isset($path) && count($path)){
				for($i=0;$i<count($path);$i++){
					if($path[$i]->lp_type){
						$path[$i]->text = '['._JLMS_GB_CONFIG_COMPLETION_SCORM_TYPE.']'. ' ' . $path[$i]->text;
					} else {
						//$path[$i]->text = '(lpath)'. ' ' . $path[$i]->text;
					}
				}
			}
		}
		
		$lists['select_lpath'] = mosHTML::selectList( $path, 'learn_path_ids[]', 'class="inputbox chzn-done" size="7" multiple="multiple" style="width:100%; height: auto;"', 'value', 'text' ); 
		 
		JLMS_gradebook_html::showListPath( $rows, $lists, $option, $course_id, $Itemid, $id);
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gradebook&id=$course_id") );
	}
}

function JLMS_savePath( $option ) {
	global $JLMS_DB, $Itemid;

	$course_id = intval(mosGetParam($_REQUEST,'course_id',0));
	$lpaths = JRequest::getVar('added_ids', array());
					
	if ( !empty($lpaths) ) 
	{
		$query = "SELECT learn_path_id FROM #__lms_gradebook_lpaths WHERE course_id = '$course_id'";
		$JLMS_DB->SetQuery($query);
		$addedLpaths = JLMSDatabaseHelper::loadResultArray();
				
		$addLpaths = array_diff($lpaths, $addedLpaths);
		$remLpaths = array_diff($addedLpaths, $lpaths);			
			
		if( !empty($remLpaths) ) 
		{
			$query = "DELETE FROM #__lms_gradebook_lpaths WHERE course_id = '$course_id' AND learn_path_id IN (".implode(',', $remLpaths).")";
			$JLMS_DB->SetQuery($query);
			$JLMS_DB->query();
		}
		
		$values = array();
		foreach($addLpaths as $addLpath){
			if( $addLpath ) 
			{
				$values[] = "($course_id, $addLpath)";
			}
		}
		
		if( !empty($values) ) 
		{
			$query = "INSERT INTO #__lms_gradebook_lpaths (course_id,learn_path_id) VALUES ".implode(',', $values);
			$JLMS_DB->SetQuery($query);
			$JLMS_DB->query();
		}
	} else {
		$query = "DELETE FROM #__lms_gradebook_lpaths WHERE course_id = '$course_id'";
		$JLMS_DB->SetQuery($query);
		$JLMS_DB->query();			
	}
		
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gb_setup_path&id=$course_id") );
}
function JLMS_removePath( &$cid, $option ) {
	global $JLMS_DB, $Itemid;
	
	$course_id = mosGetParam($_REQUEST,'course_id');
	
	if (count( $cid )) {
		$cids = implode( ',', $cid );
		$query = "DELETE FROM #__lms_gradebook_lpaths"
		. "\n WHERE id IN ( $cids )";
		$JLMS_DB->setQuery( $query );
		if (!$JLMS_DB->query()) {
			echo "<script> alert('".$JLMS_DB->getErrorMsg()."'); window.history.go(-1); </script>\n";
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gb_setup_path&id=$course_id") );
}

function JLMS_showGBScale( $option ) {
	global $JLMS_DB, $Itemid;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	if ($course_id && $JLMS_ACL->CheckPermissions('gradebook', 'configure')) {
		$query = "SELECT a.*"
		. "\n FROM #__lms_gradebook_scale as a"
		. "\n WHERE a.course_id = '".$course_id."'"
		. "\n ORDER BY a.ordering, a.scale_name";
		$JLMS_DB->SetQuery( $query );
		$rows = $JLMS_DB->LoadObjectList();
		JLMS_gradebook_html::showGBScale( $course_id, $option, $rows );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gradebook&id=$course_id") );
	}
}
function JLMS_editGBScale( $id, $option ) {
	global $JLMS_DB, $Itemid;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$JLMS_ACL = JLMSFactory::getACL();
	if ( $course_id && $JLMS_ACL->CheckPermissions('gradebook', 'configure') && ( ($id && (JLMS_GetGBScaleCourse($id) == $course_id)) || !$id ) ) {
		$row = new mos_Joomla_LMS_GBScale( $JLMS_DB );
		$row->load( $id );
		$lists = array();

		JLMS_gradebook_html::showEditGBScale( $row, $lists, $option, $course_id );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gb_scale&course_id=$course_id") );
	}
}
function JLMS_cancelGBScale( $option ) {
	global $Itemid;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gb_scale&course_id=$course_id"));
}
function JLMS_saveGBScale( $option ) {
	global $JLMS_DB, $Itemid;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$id = intval(mosGetParam($_REQUEST, 'id', 0));
	$JLMS_ACL = JLMSFactory::getACL();
	if ( $JLMS_ACL->CheckPermissions('gradebook', 'configure') && ( ($id && (JLMS_GetGBScaleCourse($id) == $course_id)) || !$id ) ) {
		$row = new mos_Joomla_LMS_GBScale( $JLMS_DB );
		if (!$row->bind( $_POST )) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if (!$id) {
			$query = "SELECT max(ordering) FROM #__lms_gradebook_scale WHERE course_id = $course_id";
			$JLMS_DB->SetQuery( $query );
			$max_order = $JLMS_DB->LoadResult();
			$row->ordering = intval($max_order + 1);
		}
		$gbs_name = isset($_REQUEST['scale_name'])?strval($_REQUEST['scale_name']):'00';
		$gbs_name = (get_magic_quotes_gpc()) ? stripslashes( $gbs_name ) : $gbs_name; 
		$row->scale_name	= ampReplace(strip_tags($gbs_name));
		if (!$row->check()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if (!$row->store()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gb_scale&course_id=$course_id") );
}
function JLMS_deleteGBScale( $option ) {
	global $JLMS_DB, $Itemid;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$JLMS_ACL = JLMSFactory::getACL();
	if ( $course_id && $JLMS_ACL->CheckPermissions('gradebook', 'configure') ) {
		$cid = mosGetParam( $_POST, 'cid', array(0) );
		if (!is_array( $cid )) {
			$cid = array(0);
		}
		$i = 0;
		while ($i < count($cid)) {
			$cid[$i] = intval($cid[$i]);
			$i ++;
		}
		$cids = implode(',',$cid);
		$query = "DELETE FROM #__lms_gradebook_scale WHERE id IN ($cids) AND course_id = '".$course_id."'";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
	}
	JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gb_scale&course_id=$course_id"));
}
function JLMS_orderGBScale( $option ) {
	global $JLMS_DB, $task, $Itemid;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$JLMS_ACL = JLMSFactory::getACL();
	if ( $course_id && $JLMS_ACL->CheckPermissions('gradebook', 'configure') ) {
		$order_id = intval(mosGetParam($_REQUEST, 'row_id', 0));
		$query = "SELECT id FROM #__lms_gradebook_scale WHERE course_id = '".$course_id."' ORDER BY ordering, scale_name";
		$JLMS_DB->SetQuery( $query );
$id_array = JLMSDatabaseHelper::LoadResultArray();
		if (count($id_array)) {
			$i = 0;$j = 0;
			while ($i < count($id_array)) {
				if ($id_array[$i] == $order_id) { $j = $i;}
				$i++;
			}
			$do_update = true;
			if (($task == 'gbs_orderup') && ($j) ) {
				$tmp = $id_array[$j-1];
				$id_array[$j-1] = $id_array[$j];
				$id_array[$j] = $tmp;
			} elseif (($task == 'gbs_orderdown') && ($j < (count($id_array)-1)) ) {
				$tmp = $id_array[$j+1];
				$id_array[$j+1] = $id_array[$j];
				$id_array[$j] = $tmp;
			}
			$i = 0;
			foreach ($id_array as $gbi_id) {
				$query = "UPDATE #__lms_gradebook_scale SET ordering = '".$i."' WHERE id = '".$gbi_id."' and course_id = '".$course_id."'";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$i ++;
			}
		}
	}
	JLMSRedirect("index.php?option=$option&Itemid=$Itemid&task=gb_scale&course_id=$course_id");
}
function JLMS_GetGBScaleCourse($gbs_id) {
	global $JLMS_DB;
	$query = "SELECT course_id FROM #__lms_gradebook_scale WHERE id = '".$gbs_id."'";
	$JLMS_DB->SetQuery( $query );
	return $JLMS_DB->LoadResult();
}
function JLMS_showGBItems( $id, $option) {
	global $JLMS_DB;
	$JLMS_ACL = JLMSFactory::getACL();
	if ($id && $JLMS_ACL->CheckPermissions('gradebook', 'configure')) {
		$query = "SELECT a.*, b.gb_category"
		. "\n FROM #__lms_gradebook_items as a LEFT JOIN #__lms_gradebook_cats as b ON a.gbc_id = b.id"
		. "\n WHERE a.course_id = '".$id."'"
		. "\n ORDER BY a.ordering, a.gbi_name";
		$JLMS_DB->SetQuery( $query );
		$rows = $JLMS_DB->LoadObjectList();
		JLMS_gradebook_html::showGBItems( $id, $option, $rows );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gradebook&id=$course_id") );
	}
}
function JLMS_editGBItem( $id, $course_id, $option ) {
	global $JLMS_DB, $Itemid;
	$JLMS_ACL = JLMSFactory::getACL();
	if ( $course_id && $JLMS_ACL->CheckPermissions('gradebook', 'configure') && ( ($id && (JLMS_GetGBItemCourse($id) == $course_id)) || !$id ) ) {
		$row = new mos_Joomla_LMS_GBItem( $JLMS_DB );
		$row->load( $id );
		$lists = array();
		$query = "SELECT * FROM #__lms_gradebook_cats ORDER BY gb_category";
		$JLMS_DB->setQuery( $query );
		$cats = $JLMS_DB->LoadObjectList();
		$cats_tun = array();
		foreach($cats as $st) {
			$sto = new stdClass();
			$sto->value = $st->id;
			$sto->text = $st->gb_category;
			$cats_tun[] = $sto;
		}
		$lists['gb_cats'] = mosHTML::SelectList($cats_tun, 'gbc_id', 'class="inputbox" size="1" ', 'value', 'text', $row->gbc_id);

		JLMS_gradebook_html::showEditGBItem( $row, $lists, $option, $course_id );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gb_items&id=$course_id") );
	}
}
function JLMS_cancelGBItem( $option ) {
	global $Itemid;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gb_items&id=$course_id"));
}
function JLMS_saveGBItem( $option ) {
	global $JLMS_DB, $Itemid;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$id = intval(mosGetParam($_REQUEST, 'id', 0));
	$JLMS_ACL = JLMSFactory::getACL();
	if ( $JLMS_ACL->CheckPermissions('gradebook', 'configure') && ( ($id && (JLMS_GetGBItemCourse($id) == $course_id)) || !$id ) ) {
		$row = new mos_Joomla_LMS_GBItem( $JLMS_DB );
		if (!$row->bind( $_POST )) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		#$row->gbi_name = strval(mosGetParam($_POST, 'gbi_name', ''));
		$gbi_name = isset($_REQUEST['gbi_name'])?strval($_REQUEST['gbi_name']):'grade item';
		$gbi_name = (get_magic_quotes_gpc()) ? stripslashes( $gbi_name ) : $gbi_name; 
		$row->gbi_name	= ampReplace(strip_tags($gbi_name));

		$row->gbi_description = strval(JLMS_getParam_LowFilter($_POST, 'gbi_description', ''));
		$row->gbi_description = JLMS_ProcessText_LowFilter($row->gbi_description);

		$row->gbi_option = 0;
		if (!$row->check()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if (!$row->store()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gb_items&id=$course_id") );
}
function JLMS_deleteGBItems( $course_id, $option ) {
	global $JLMS_DB, $Itemid;
	$JLMS_ACL = JLMSFactory::getACL();
	if ( $course_id && $JLMS_ACL->CheckPermissions('gradebook', 'configure') ) {
		$cid = mosGetParam( $_POST, 'cid', array(0) );
		if (!is_array( $cid )) {
			$cid = array(0);
		}
		$i = 0;
		while ($i < count($cid)) {
			$cid[$i] = intval($cid[$i]);
			$i ++;
		}
		$cids = implode(',',$cid);
		$query = "SELECT id FROM #__lms_gradebook_items WHERE id IN ($cids) AND course_id = '".$course_id."'";
		$JLMS_DB->SetQuery( $query );
		$cid = JLMSDatabaseHelper::LoadResultArray();
		if (count($cid)) {
			$cids = implode(',',$cid);
			$query = "DELETE FROM #__lms_gradebook_items WHERE id IN ($cids) AND course_id = '".$course_id."'";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			$query = "DELETE FROM #__lms_gradebook WHERE gbi_id IN ($cids) AND course_id = '".$course_id."'";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
		}
	}
	JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gb_items&id=$course_id"));
}
function JLMS_orderGBItems( $course_id, $option ) {
	global $JLMS_DB, $task, $Itemid;
	$JLMS_ACL = JLMSFactory::getACL();
	if ( $course_id && $JLMS_ACL->CheckPermissions('gradebook', 'configure') ) {
		$order_id = intval(mosGetParam($_REQUEST, 'row_id', 0));
		$query = "SELECT id FROM #__lms_gradebook_items WHERE course_id = '".$course_id."' ORDER BY ordering";
		$JLMS_DB->SetQuery( $query );
		$id_array = JLMSDatabaseHelper::LoadResultArray();
		if (count($id_array)) {
			$i = 0;$j = 0;
			while ($i < count($id_array)) {
				if ($id_array[$i] == $order_id) { $j = $i;}
				$i++;
			}
			$do_update = true;
			if (($task == 'gbi_orderup') && ($j) ) {
				$tmp = $id_array[$j-1];
				$id_array[$j-1] = $id_array[$j];
				$id_array[$j] = $tmp;
			} elseif (($task == 'gbi_orderdown') && ($j < (count($id_array)-1)) ) {
				$tmp = $id_array[$j+1];
				$id_array[$j+1] = $id_array[$j];
				$id_array[$j] = $tmp;
			}
			$i = 0;
			foreach ($id_array as $gbi_id) {
				$query = "UPDATE #__lms_gradebook_items SET ordering = '".$i."' WHERE id = '".$gbi_id."' and course_id = '".$course_id."'";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$i ++;
			}
		}
	}
	JLMSRedirect("index.php?option=$option&Itemid=$Itemid&task=gb_items&id=$course_id");
}
function JLMS_GetGBItemCourse($gbi_id) {
	global $JLMS_DB;
	$query = "SELECT course_id FROM #__lms_gradebook_items WHERE id = '".$gbi_id."'";
	$JLMS_DB->SetQuery( $query );
	return $JLMS_DB->LoadResult();
}
function JLMS_markCertificate( $course_id, $option ) {
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$Itemid = $JLMS_CONFIG->get('Itemid');
	global $JLMS_DB, $task;
	$JLMS_ACL = JLMSFactory::getACL();
	if ($course_id && $JLMS_ACL->CheckPermissions('gradebook', 'manage')) {
		$state = intval(mosGetParam($_REQUEST, 'state', 0));
		if ($state != 1) { $state = 0; }
		$cid = mosGetParam( $_REQUEST, 'cid', array(0) );
		$cid2 = intval(mosGetParam( $_REQUEST, 'cid2', 0 ));
		if ($cid2) {
			$cid = array();
			$cid[] = $cid2;
		}
		if (!is_array( $cid ) || count( $cid ) < 1) {
			$action = 1 ? 'Mark' : 'Unmark';
			echo "<script> alert('Select an item to $action'); window.history.go(-1);</script>\n";
			exit();
		}
		$cids = implode( ',', $cid );
		//check rights
		$query = "SELECT user_id FROM #__lms_users_in_groups WHERE course_id = '".$course_id."' AND user_id IN ( $cids )";
		$JLMS_DB->SetQuery( $query );
		$cid = JLMSDatabaseHelper::LoadResultArray();
		if (!is_array( $cid )) {
			$cid = array(0);
		}
		if (is_array( $cid ) && count( $cid ) > 0) {
			$cids = implode( ',', $cid );
			$first_user = 0;
			if (isset($cid[0]) && $cid[0]) {
				$first_user = intval($cid[0]);
			}
			$is_first_user = false;
			if ($first_user) {
				$query = "SELECT user_id FROM #__lms_certificate_users WHERE course_id = '".$course_id."' AND user_id = $first_user";
				$JLMS_DB->SetQuery( $query );
				$is_first_user = $JLMS_DB->LoadResult();
				$query = "SELECT crt_option FROM #__lms_certificate_users WHERE course_id = '".$course_id."' AND user_id = $first_user";
				$JLMS_DB->SetQuery( $query );
				$is_first_user_state = $JLMS_DB->LoadResult();
			}
			$query = "DELETE FROM #__lms_certificate_users WHERE course_id = '".$course_id."' AND user_id IN ( $cids )";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			if ($state == 1) {
				$query = "INSERT INTO #__lms_certificate_users (course_id, user_id, crt_option, crt_date) VALUES ";
				$t = 0;
				foreach($cid as $user_id) {
					$query .= "\n ($course_id, ".$user_id.", $state, '".gmdate('Y-m-d H:i:s')."')".(($t < (count($cid) - 1))?',':'');
					$t ++;
				}
				$JLMS_DB->setQuery( $query );
				if (!$JLMS_DB->query()) {
					echo "<script> alert('".$JLMS_DB->getErrorMsg()."'); window.history.go(-1); </script>\n";
					exit();
				}
			}// end if ($state)
			
			if ($first_user && (($is_first_user && !$is_first_user_state) || !$is_first_user) && $state) {
					$e_course = new stdClass();
					$e_course->course_alias = '';
					$e_course->course_name = '';			

					$query = "SELECT course_name, name_alias FROM #__lms_courses WHERE id = '".$course_id."'";
					$JLMS_DB->setQuery( $query );
					$e_course = $JLMS_DB->loadObject();

					$e_user = new stdClass();
					$e_user->name = '';
					$e_user->email = '';
					$e_user->username = '';

					$query = "SELECT email, name, username FROM #__users WHERE id = '".$first_user."'";
					$JLMS_DB->setQuery( $query );
					$e_user = $JLMS_DB->loadObject();

					$e_params['user_id'] = $first_user;
					$e_params['course_id'] = $course_id;					
					$e_params['markers']['{email}'] = $e_user->email;	
					$e_params['markers']['{name}'] = $e_user->name;										
					$e_params['markers']['{username}'] = $e_user->username;
					$e_params['markers']['{coursename}'] = $e_course->course_name;//( $e_course->course_alias )?$e_course->course_alias:$e_course->course_name;

					$e_params['markers']['{courselink}'] = JLMSEmailRoute("index.php?option=com_joomla_lms&Itemid=$Itemid&task=details_course&id=$course_id");

					$e_params['markers_nohtml']['{courselink}'] = $e_params['markers']['{courselink}'];
					$e_params['markers']['{courselink}'] = '<a href="'.$e_params['markers']['{courselink}'].'">'.$e_params['markers']['{courselink}'].'</a>';
											
					$e_params['markers']['{lmslink}'] = JLMSEmailRoute("index.php?option=com_joomla_lms&Itemid=$Itemid");

					$e_params['markers_nohtml']['{lmslink}'] = $e_params['markers']['{lmslink}'];					
					$e_params['markers']['{lmslink}'] = '<a href="'.$e_params['markers']['{lmslink}'].'">'.$e_params['markers']['{lmslink}'].'</a>';

					$e_params['action_name'] = 'OnTeacherMarksCourseCompletion';

					$_JLMS_PLUGINS->loadBotGroup('emails');
					$plugin_result_array = $_JLMS_PLUGINS->trigger('OnTeacherMarksCourseCompletion', array (& $e_params));							
			}
		}//if (array(...))
	}
	if ($task == 'gb_crtA') {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gb_usergrade&course_id=$course_id&id=".$cid[0]) );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gradebook&id=$course_id") );
	}	
}
function JLMS_saveUserGrades( $id, $option ) {
	global $JLMS_DB, $Itemid;
	$JLMS_ACL = JLMSFactory::getACL();
	if ($id && $JLMS_ACL->CheckPermissions('gradebook', 'configure')) {
		$gb_items = mosGetParam($_REQUEST, 'gb_item', array(0));
		$gb_vals = mosGetParam($_REQUEST, 'gbi_val', array(0));
		$user_id = mosGetParam($_REQUEST, 'user_id', 0);
		
		$query = "SELECT count(1) FROM #__lms_users_in_groups WHERE course_id = '".$id."' AND user_id = '".$user_id."'";
		$JLMS_DB->setQuery( $query );
		$inCourse = $JLMS_DB->loadResult();	
				
		if ( $inCourse && !empty($gb_items) && !empty($gb_vals) && (count($gb_items) == count($gb_vals)) ) {
			$gbi_str = implode(',',$gb_items);
			$query = "DELETE FROM #__lms_gradebook WHERE course_id = '".$id."' AND user_id = '".$user_id."' AND gbi_id IN ($gbi_str)";
			$JLMS_DB->setQuery( $query );
			$JLMS_DB->query();
			$query = "INSERT INTO #__lms_gradebook (course_id, user_id, gbi_id, gb_points) VALUES ";
			$t = 0;
			while ($t < count($gb_items)) {
				$query .= "\n ($id, ".$user_id.", ".$gb_items[$t].", ".$gb_vals[$t].")".(($t < (count($gb_items) - 1))?',':'');
				$t ++;
			}
			$JLMS_DB->setQuery( $query );
			$JLMS_DB->query();
		}
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gradebook&id=$id"));
	} else {
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid"));
	}
}
function JLMS_showGradebook( $id, $option ) {
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$JLMS_SESSION = JLMSFactory::getSession();
	$Itemid = $JLMS_CONFIG->get('Itemid');
	$db = & JLMSFactory::getDB();
	$j_my_user = JLMSFactory::getUser();
	$my_id = $j_my_user->get('id');

    $is_exporting = false;
	$view = mosGetParam($_REQUEST, 'view', '');
	if($view == 'csv' || $view == 'xls') {
		$_REQUEST['is_full'] = 1;
        $is_exporting = true;
	}
	$reporting_header = array();
	
	$usertype = JLMS_GetUserType($my_id, $id); // for CEO !!!
	$JLMS_ACL = JLMSFactory::getACL();

	$manage = $JLMS_ACL->CheckPermissions('gradebook', 'manage');
	$assigned_groups_only = $JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only'); //TODO: net takogo permission!!!!!!!
	
	$limit		= intval( mosGetParam( $_REQUEST, 'limit', $JLMS_SESSION->get('list_limit', $JLMS_CONFIG->get('list_limit')) ) );
	$JLMS_SESSION->set('list_limit', $limit);
	$limitstart = intval( mosGetParam( $_REQUEST, 'limitstart', 0 ) );
	$is_full = intval( mosGetParam( $_REQUEST, 'is_full', 0 ) );
	$cid = mosGetParam( $_REQUEST, 'cid', array() );
	
	$lists = array();
	$lists['is_full'] = $is_full;
	
	$groups_where_admin_manager = array();
	$members = array();
	if ($id && $JLMS_ACL->CheckPermissions('gradebook', 'configure')) {
		
		$filt_group = intval( mosGetParam( $_REQUEST, 'filt_group', $JLMS_SESSION->get('filt_group', 0) ) );
		$filt_user = strval( mosGetParam( $_REQUEST, 'username_filter', $JLMS_SESSION->get('username_filter', '') ) );
		$filt_user = JLMSDatabaseHelper::GetEscaped( trim( strtolower( $filt_user ) ) );
		$JLMS_SESSION->set('filt_group', $filt_group);
		$JLMS_SESSION->set('username_filter', $filt_user);
		
		//new group permission
		
		if($assigned_groups_only) {
			$course_members_array = array();
			$query = "SELECT a.group_id FROM `#__lms_user_assign_groups` as a WHERE a.user_id = '".$my_id."'"
			;
			$db->setQuery($query);
			$groups_where_admin_manager = JLMSDatabaseHelper::loadResultArray();
			
			if(count($groups_where_admin_manager) == 1) {
				$filt_group = $groups_where_admin_manager[0];
			}
			
			$groups_where_admin_manager = implode(',', $groups_where_admin_manager);
			
			if($groups_where_admin_manager != '') {
				$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE (group_id IN ($groups_where_admin_manager) OR subgroup1_id IN ($groups_where_admin_manager))"
					. ($filt_group ? ("\n AND group_id = '".$filt_group."'") : '')
//						. ($filt_subgroup ? ("\n AND subgroup1_id = '".$filt_subgroup."'") : '')
				;
				$db->setQuery($query);
				$members = JLMSDatabaseHelper::loadResultArray();
				$course_members_array = $members;
			}
				
			$users_where_ceo_parent = array();
			if($JLMS_ACL->_role_type == 3) {
				$query = "SELECT user_id FROM `#__lms_user_parents` WHERE parent_id = '".$my_id."'"
				;
				$db->setQuery($query);
				$users_where_ceo_parent = JLMSDatabaseHelper::loadResultArray();
				
				//$members = array_merge($members, $users_where_ceo_parent);
			}
			
			if($members != "'0'" && count($users_where_ceo_parent)) {
				$members = array_merge($members, $users_where_ceo_parent);
				$course_members_array = array_merge($course_members_array, $users_where_ceo_parent);
			}
			elseif(count($users_where_ceo_parent)) {
				$members = $users_where_ceo_parent;
				$course_members_array = $users_where_ceo_parent;
			}
		} else {
			$course_members_array = array();
			$query = "SELECT user_id FROM #__lms_users_in_groups WHERE course_id = $id";
			$db->setQuery($query);
			$course_members_array = JLMSDatabaseHelper::loadResultArray();
		}	
		//             ---
		
		$cids_where = '';
		if(in_array($view, array('csv', 'xls'))){
			if(count($cid)){
				$cids_where .= "\n AND u.id IN (".implode(',', $cid).")";
			}
		}
		$members[] = 0;
		$members_str = implode(',', $members);
		if ($JLMS_CONFIG->get('use_global_groups', 1)){
			if($filt_group){
				$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE group_id = '".$filt_group."'";
				$db->setQuery($query);
				$uids = JLMSDatabaseHelper::loadResultArray();
			}
			$query = "SELECT count(*)"
			. "\n FROM #__users as u, #__lms_users_in_groups as b"
			. "\n LEFT JOIN #__lms_usergroups as c ON b.group_id = c.id"
			. "\n WHERE u.id = b.user_id AND b.course_id = '".$id."'"
			. ($filt_group ? "\n AND b.user_id IN (".implode(',', $uids).")" : '')
			. ($filt_user ? ("\n AND (u.username LIKE '%$filt_user%' OR u.email LIKE '%$filt_user%' OR u.name LIKE '%$filt_user%')") : '')
			. ($assigned_groups_only ? ("\n AND u.id IN ($members_str)") :'')
			. $cids_where
			;
		} else {
			$query = "SELECT count(*) FROM #__lms_users_in_groups as b"
			. "\n, #__users as u"
			. "\n WHERE b.course_id = '".$id."' AND b.user_id = u.id"
			. ($filt_group ? ("\n AND b.group_id = '".$filt_group."'") : '')
			. ($filt_user ? ("\n AND b.user_id = u.id AND (u.username LIKE '%$filt_user%' OR u.email LIKE '%$filt_user%' OR u.name LIKE '%$filt_user%')") : '')
			. ($assigned_groups_only ? ("\n AND b.user_id IN ($members_str)") :'')
			. $cids_where
			;
		}
		$db->SetQuery( $query );
		$total = $db->LoadResult();
		require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
		$pageNav = new JLMSPageNav( $total, $limitstart, $limit );
		
		if ($JLMS_CONFIG->get('use_global_groups', 1)){
			$query = "SELECT u.username, u.name, u.email, b.*, c.ug_name"
			. "\n FROM #__users as u, #__lms_users_in_groups as b"
			. "\n LEFT JOIN #__lms_usergroups as c ON b.group_id = c.id"
			. "\n WHERE u.id = b.user_id AND b.course_id = '".$id."'"
			. ($filt_group ? "\n AND b.user_id IN (".implode(',', $uids).")" : '')
			. ($filt_user ? ("\n AND (u.username LIKE '%$filt_user%' OR u.email LIKE '%$filt_user%' OR u.name LIKE '%$filt_user%')") : '')
			. ($assigned_groups_only ? ("\n AND u.id IN ($members_str)") :'')
			. $cids_where
			. "\n ORDER BY u.username, u.name"
			. (!$is_full ? "\n LIMIT $pageNav->limitstart, $pageNav->limit" : '')
			;
		} else {
			$query = "SELECT b.*, u.username, u.name, u.email, c.ug_name"
			. "\n FROM #__lms_users_in_groups as b"
			. "\n LEFT JOIN #__lms_usergroups as c ON b.group_id = c.id AND c.course_id = '".$id."',"
			. "\n #__users as u"
			. "\n WHERE b.course_id = '".$id."' AND b.user_id = u.id"
			. ($filt_group ? ("\n AND b.group_id = '".$filt_group."'") : '')
			. ($filt_user ? ("\n AND (u.username LIKE '%$filt_user%' OR u.email LIKE '%$filt_user%' OR u.name LIKE '%$filt_user%')") : '')
			. ($assigned_groups_only ? ("\n AND b.user_id IN ($members_str)") :'')
			. $cids_where
			. "\n ORDER BY u.username, u.name"
			. (!$is_full ? "\n LIMIT $pageNav->limitstart, $pageNav->limit" : '')
			;
		}
		$db->SetQuery( $query );
		$rows = $db->LoadObjectList();
		
		/*Fix - Display All Global Groups User*/
		if ($JLMS_CONFIG->get('use_global_groups', 1)){
			$tmp = array();
			foreach($rows as $n=>$row){
				$tmp[$n] = $row;
				
				$query = "SELECT b.ug_name"
				. "\n FROM #__lms_users_in_global_groups as a, #__lms_usergroups as b"
				. "\n WHERE 1"
				. "\n AND a.group_id = b.id"
				. "\n AND b.course_id = 0"
				. "\n AND a.user_id = '".$row->user_id."'"
				. "\n ORDER BY b.ug_name"
				;
				$db->setQuery($query);
				$groups = JLMSDatabaseHelper::loadResultArray();
				
				$str_groups = '';
				if(count($groups)){
					$str_groups = implode("<br />", $groups);
				}
				
				$tmp[$n]->ug_name = $str_groups;
			}
			if(isset($tmp) && count($tmp)){
				$rows = array();	
				$rows = $tmp;
			}
		}
		/*Fix - Display All Global Groups User*/
		
		$uids = array();
		foreach ($rows as $row) {
			$uids[] = $row->user_id;
		}
		
		if($JLMS_CONFIG->get('enable_timetracking')){
			for($i=0;$i<count($rows);$i++){
				$query = "SELECT time_spent"
				. "\n FROM #__lms_time_tracking"
				. "\n WHERE 1"
				. "\n AND course_id = '".$id."'"
				. "\n AND user_id = '".$rows[$i]->user_id."'"
				;
				$db->setQuery($query);
				$time_spent = $db->loadResult() ? $db->loadResult() : 0;
				$rows[$i]->time_spent = JLMS_getTimeNormal($time_spent);
			}
		}
		
		/*Registartion Custom + part 2*/
		/* customization temporary commented by DEN - NOT FOR THIS RELEASE
		$query = "SELECT COUNT(*)"
		. "\n FROM `jos_components`"
		. "\n WHERE `option` LIKE CONVERT( _utf8 'com_jlms_regenroll'"
		. "\n USING utf8 )"
		. "\n COLLATE utf8_general_ci"
		;
		$db->setQuery($query);
		$exist_regenroll = $db->loadResult();
		*/
		$lists['regenroll_fields'] = array();
		$lists['rows_regenroll'] = array();
		/* customization temporary commented by DEN - NOT FOR THIS RELEASE
		if(isset($exist_regenroll) && $exist_regenroll){
			$query = "SELECT *"
			. "\n FROM #__regenroll_fields"
			. "\n WHERE published = 1"
			. "\n ORDER BY ordering"
			;	
			$db->setQuery($query);
			$regenroll_fields = $db->loadObjectList();
			if(isset($regenroll_fields) && count($regenroll_fields)){
				$lists['regenroll_fields'] = $regenroll_fields;
				
				$select_fields = array();
				foreach($regenroll_fields as $field){
					$select_fields[] = $field->name;	
				}
				$str_select_fields = "";
				if(count($select_fields)){
					$str_select_fields .= ", ";	
				}
				$str_select_fields .= implode(", ", $select_fields);
				$rows_regenroll = array();
				$query = "SELECT id, user_id".$str_select_fields
				. "\n FROM #__regenroll"
				. "\n WHERE 1";
				$db->setQuery($query);
				$rows_regenroll = $db->loadObjectList();
				$lists['rows_regenroll'] = $rows_regenroll;
				
				if(isset($rows_regenroll) && count($rows_regenroll)){
					foreach($rows as $n=>$row){
						foreach($regenroll_fields as $field){
							$field_name = $field->name;
							$rows[$n]->$field_name = '';	
							foreach($rows_regenroll as $row_regenroll){
								if($row->user_id == $row_regenroll->user_id){
									$rows[$n]->$field_name	= $row_regenroll->$field_name;
								}	
							}
						}
					}
				}
			}
		}
		*/
		
		$query = "SELECT *"
		. "\n FROM #__lms_certificate_users"
		. "\n WHERE course_id = $id";
		$db->setQuery($query);
		$user_certificates = $db->loadObjectList();

		/* customization temporary commented by DEN - NOT FOR THIS RELEASE
		$query = "SELECT *"
		. "\n FROM #__lms_track_hits"
		. "\n WHERE 1";
		$db->setQuery($query);
		$all_track_hits = $db->loadObjectList();
		*/

		foreach($rows as $n=>$row){
			$rows[$n]->date_completed = '';
			$rows[$n]->access = 0;
			if(isset($user_certificates) && count($user_certificates)){
				foreach($user_certificates as $user_certificate){
					if($id == $user_certificate->course_id && $row->user_id == $user_certificate->user_id){
						$rows[$n]->date_completed = $user_certificate->crt_date;	
					}	
				}
			}
			/* customization temporary commented by DEN - NOT FOR THIS RELEASE
			$tmp_access = 0;
			if(isset($all_track_hits) && count($all_track_hits)){
				foreach($all_track_hits as $track_hit){
					if($id == $track_hit->course_id && $row->user_id == $track_hit->user_id){
						$tmp_access = $tmp_access + 1;	
					}
				}
				$rows[$n]->access = $tmp_access;	
			}
			*/
		}

		//TODO: replace this part of code with LMSTitles object
		$teacher_in_courses = $JLMS_CONFIG->get('teacher_in_courses', array());
		if(isset($teacher_in_courses) && count($teacher_in_courses)){
			$str_teacher_in_courses = implode(",", $teacher_in_courses);
			$query = "SELECT id as value, course_name as text"
			. "\n FROM #__lms_courses"
			. "\n WHERE id IN (".$str_teacher_in_courses.")"
			;
			$db->setQuery($query);
			$courses = $db->loadObjectList();
			
			$list_courses = array();
			$list_courses = array_merge($list_courses, $courses);
			$lists['courses'] = mosHTML::selectList($list_courses, 'course_id', 'class="inputbox" onchange="javascript:submutFormChangeCourse(this);"', 'value', 'text', $id );
		}
		
		$lists['course_name'] = '';
		$query = "SELECT course_name FROM #__lms_courses WHERE id = '".$id."'";
		$db->setQuery($query);
		$course_name = $db->loadResult();
		if(isset($course_name) && $course_name){
			$lists['course_name'] = $course_name;
		}
		/*Registartion Custom + part 2*/
		
			if (true) {

			//if (count($uids)) {
				$lists['filt_user'] = $filt_user;
                if ($is_exporting)
                {
                   $arr_get_res = JLMS_getCertDataByParts($rows, $id, $lists);
                    $rows = $arr_get_res[0];
                    $lists = $arr_get_res[1];
                } else
                {
                    JLMS_GB_getUsersGrades($id, $uids, $rows, $lists);
                }
		
				$g_items = array();
				$g_items[] = mosHTML::makeOption(0, _JLMS_GB_FILTER_ALL_GROUPS);
				
			if ($JLMS_CONFIG->get('use_global_groups', 1)){
				if($JLMS_ACL->_role_type == 2 || $JLMS_ACL->_role_type == 4) {
					$query = "SELECT distinct a.id as value, a.ug_name as text"
					. "\n FROM #__lms_usergroups as a"
					. "\n WHERE a.course_id = 0"
					. "\n AND a.parent_id = 0"
					. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND a.id IN ($groups_where_admin_manager)") :'')
					. "\n ORDER BY a.ug_name"
					;
				}
				else {
					//TODO: bug! CEO cann't see all the groups available in the system
					$query = "SELECT distinct a.id as value, a.ug_name as text"
					. "\n FROM #__lms_usergroups as a"
					. "\n WHERE a.course_id = 0"
					. "\n AND a.parent_id = 0"
					. "\n ORDER BY a.ug_name"
					;
				}
			} else {
				$query = "SELECT distinct a.id as value, a.ug_name as text FROM #__lms_usergroups as a, #__lms_users_in_groups as b"
				. "\n WHERE a.course_id = '".$course_id."' AND b.group_id = a.id ORDER BY a.ug_name";
			}
			$db->SetQuery( $query );
			$groups = $db->LoadObjectList();
			$g_items = array_merge($g_items, $groups);
			/*
			$link = "index.php?option=$option&amp;Itemid=$Itemid&task=gradebook&id=$id";
			$link = $link ."&amp;filt_group='+this.options[selectedIndex].value+'";
			$link = sefRelToAbs( $link );
			$link = str_replace('%5C%27',"'", $link);$link = str_replace('%5B',"[", $link);$link = str_replace('%5D',"]", $link);$link = str_replace('%20',"+", $link);$link = str_replace("\\\\\\","", $link);$link = str_replace('%27',"'", $link);
			$lists['filter'] = mosHTML::selectList($g_items, 'filt_group', 'class="inputbox" style="width:150px" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filt_group );
			*/
			$lists['filter'] = mosHTML::selectList($g_items, 'filt_group', 'class="inputbox" style="width:150px" size="1" onchange="javascript:submitFormView();"', 'value', 'text', $filt_group );
			
			$groups_reporting = array();
			foreach($g_items as $grp){
				if($filt_group && $grp->value == $filt_group){
					$groups_reporting[] = $grp->text;
				}
			}
			$name_groups_reporting[] = _JLMS_REPORTING_USERGROUP;
			$reporting_header['name_groups'] = $name_groups_reporting;	
			$reporting_header['groups'] = $groups_reporting;
			
			//Mod EOGN-1203
			$sgb_cb_columns = $JLMS_CONFIG->get('show_gradebook_cb_columns', false);
			if($sgb_cb_columns){
				
				JLMS_require_lang( $JLMS_LANGUAGE, 'admin.main.lang', $JLMS_CONFIG->get('default_language'), 'backend' );	 
				JLMS_processLanguage( $JLMS_LANGUAGE, false, 'backend' );
				
				require(_JOOMLMS_FRONT_HOME . '/includes/classes/lms.cb_join.php');
				
				$jlms_cb_fields = array();

                for($i=0; $i<=12; $i++){
                    $jlms_cb_fields[$i] = new stdClass();
                }

				$jlms_cb_fields[0]->var = 'cb_address';
				$jlms_cb_fields[0]->lang_var = _JLMS_ADDRESS;
				
				$jlms_cb_fields[1]->var = 'cb_country';
				$jlms_cb_fields[1]->lang_var = _JLMS_COUNTRY;
				
				$jlms_cb_fields[2]->var = 'cb_city';
				$jlms_cb_fields[2]->lang_var = _JLMS_CITY;
				
				$jlms_cb_fields[3]->var = 'cb_state';
				$jlms_cb_fields[3]->lang_var = _JLMS_STATE_PROVINCE;
				
				$jlms_cb_fields[4]->var = 'cb_postal_code';
				$jlms_cb_fields[4]->lang_var = _JLMS_POSTAL_CODE;
				
				$jlms_cb_fields[5]->var = 'cb_phone';
				$jlms_cb_fields[5]->lang_var = _JLMS_PHONE;
				
				$jlms_cb_fields[6]->var = 'cb_location';
				$jlms_cb_fields[6]->lang_var = _JLMS_LOCATION;
				
				$jlms_cb_fields[7]->var = 'cb_website';
				$jlms_cb_fields[7]->lang_var = _JLMS_WEBSITE;
			
				$jlms_cb_fields[8]->var = 'cb_icq';
				$jlms_cb_fields[8]->lang_var = 'ICQ';
				
				$jlms_cb_fields[9]->var = 'cb_aim';
				$jlms_cb_fields[9]->lang_var = 'AIM';
				
				$jlms_cb_fields[10]->var = 'cb_yim';
				$jlms_cb_fields[10]->lang_var = 'YIM';
				
				$jlms_cb_fields[11]->var = 'cb_msn';
				$jlms_cb_fields[11]->lang_var = 'MSN';
				
				$jlms_cb_fields[12]->var = 'cb_company';
				$jlms_cb_fields[12]->lang_var = _JLMS_COMPANY;
				
				$query = "SELECT cb_assoc as var, field_name as lang_var, 1 as custom FROM #__lms_cb_assoc WHERE 1 AND gb = '1'";
				$db->setQuery($query);
				$extra_jlms_cb_fields = $db->loadObjectList();
				
				$jlms_cb_fields = array_merge($jlms_cb_fields, $extra_jlms_cb_fields);
				
				$cb_fields = array();
				
				$row = new jlms_adm_config();
				$row->bind($_POST);
				$row->loadFromDb( $db );
				
				$gb = array();
				foreach($jlms_cb_fields as $d){
					if(isset($row->{'gb_'.$d->var})){
						$gb[$d->var] = $row->{'gb_'.$d->var};
					}
				}
				
				if(isset($extra_jlms_cb_fields) && count($extra_jlms_cb_fields)){
					foreach($extra_jlms_cb_fields as $e_cb_field){
						$gb[$e_cb_field->var] = 1;
					}
				}
				
				foreach($jlms_cb_fields as $jlms_cb_field){
					if(isset($gb[$jlms_cb_field->var]) && $gb[$jlms_cb_field->var]){
						$cb_fields[] = $jlms_cb_field;
					}
				}
				
				$lists['cb_fields'] = array();
				foreach($cb_fields as $indx=>$cb_field){
					$lists['cb_fields'][] = $cb_field->lang_var;
				}
				
				for($i=0;$i<count($rows);$i++){
					$rows[$i]->cb_fields = array();
					
					$ii=0;
					foreach($cb_fields as $cb_field){
						if(isset($cb_field->custom) && $cb_field->custom){
							$tmp_cb_field = JLMSCBJoin::getASSOC($cb_field->var, $rows[$i]->user_id);
						} else {
							$tmp_cb_field = JLMSCBJoin::getASSOC('lms_'.$cb_field->var, $rows[$i]->user_id);
						}
						$rows[$i]->cb_fields[$ii] = isset($tmp_cb_field) ? $tmp_cb_field : '';
						$ii++;
					}
				}
			}
			
			$course_params = $JLMS_CONFIG->get('course_params');
			$params = new JLMSParameters($course_params);
		
			if( $params->get('spec_reg', false ) ) {
				$sgb_course_qustions = $JLMS_CONFIG->get('show_gradebook_course_questions', false);
			} else {
				$sgb_course_qustions = false;
			}
			
			if($sgb_course_qustions){
				$query = "SELECT * FROM #__lms_spec_reg_questions WHERE course_id = '".$id."' ORDER BY ordering";
				$db->setQuery($query);
				$course_questions = $db->loadObjectList();
				
				$lists['course_questions'] = array();
				if(isset($course_questions) && count($course_questions)){
					foreach($course_questions as $course_question){
						$lists['course_questions'][] = ucfirst($course_question->course_question);
					}
				}
				
				for($i=0;$i<count($rows);$i++){
					$rows[$i]->course_questions = array();
					
					$query = "SELECT user_answer"
					. "\n FROM #__lms_spec_reg_questions as a, #__lms_spec_reg_answers as b"
					. "\n WHERE 1"
					. "\n AND a.course_id = '".$id."'"
					. "\n AND a.id = b.quest_id"
					. "\n AND b.user_id = '".$rows[$i]->user_id."'"
					. "\n ORDER BY a.ordering"
					;
					$db->setQuery($query);
					$course_questions_value = JLMSDatabaseHelper::loadResultArray();
					
					if(isset($course_questions_value) && count($course_questions_value)){
						$rows[$i]->course_questions = $course_questions_value;
					} else {
						foreach($course_questions as $course_question){
							$rows[$i]->course_questions[] = '';
						}
					}
				}
			}
			//Mod EOGN-1203
			
			if($view == 'csv'){
				JLMS_exportCSV($rows, $lists, $reporting_header);
			} else if($view == 'xls'){
				JLMS_exportXLS($rows, $lists, $reporting_header);
			} else {
				$lms_titles_cache = & JLMSFactory::getTitles();
				$lms_titles_cache->setArray('users', $rows, 'user_id', 'username');
				JLMS_gradebook_html::showGradebook( $id, $option, $rows, $pageNav, $lists, $manage );
			}
		} else { JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid"));} //else for  if(count($uids)).....
	} elseif ($id && !$JLMS_ACL->isStaff() && $JLMS_ACL->CheckPermissions('gradebook', 'view')) {		
		JLMS_showUserGrade($my_id, $option, $id, true);
//	} elseif ($id && ($usertype == 6)) {
	} elseif ($id && $JLMS_ACL->isStaff() && $JLMS_ACL->CheckPermissions('gradebook', 'view')) {
		
		//new group permission
		$members = "'0'";
		if($assigned_groups_only) {
			$query = "SELECT a.group_id FROM `#__lms_user_assign_groups` as a WHERE a.user_id = '".$my_id."'"
			;
			$db->setQuery($query);
			$groups_where_admin_manager = JLMSDatabaseHelper::loadResultArray();
			
			if(count($groups_where_admin_manager) == 1) {
				$filt_group = $groups_where_admin_manager[0];
			}
			
			$groups_where_admin_manager = implode(',', $groups_where_admin_manager);
			
				if($groups_where_admin_manager != '') {
					$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE (group_id IN ($groups_where_admin_manager) OR subgroup1_id IN ($groups_where_admin_manager))"
//						. ($filt_group ? ("\n AND group_id = '".$filt_group."'") : '')
//						. ($filt_subgroup ? ("\n AND subgroup1_id = '".$filt_subgroup."'") : '')
					;
					
					$db->setQuery($query);
					$members = JLMSDatabaseHelper::loadResultArray();
				}
					
				$users_where_ceo_parent = array();
				if($JLMS_ACL->_role_type == 3) {
					$query = "SELECT user_id FROM `#__lms_user_parents` WHERE parent_id = '".$my_id."'"
					;
					$db->setQuery($query);
					$users_where_ceo_parent = JLMSDatabaseHelper::loadResultArray();
					
					//$members = array_merge($members, $users_where_ceo_parent);
				}
				
				if($members != "'0'" && count($users_where_ceo_parent)) {
					$members = array_merge($members, $users_where_ceo_parent);
				}
				elseif(count($users_where_ceo_parent)) {
					$members = $users_where_ceo_parent;
				}
					
				$members = implode(',', $members);
				if($members == '') {
					$members = "'0'";
				}	
		}	
		//             ---
		
		$query = "SELECT b.*, u.username, u.name, u.email, c.ug_name"
		. "\n FROM #__lms_user_parents as a, #__lms_users_in_groups as b"
		. "\n LEFT JOIN #__lms_usergroups as c ON b.group_id = c.id AND c.course_id = '".$id."',"
		. "\n #__users as u"
		. "\n WHERE a.user_id = b.user_id"
		. "\n AND a.parent_id = '".$my_id."'"
		."\n AND b.course_id = '".$id."' AND b.user_id = u.id"
		. ($assigned_groups_only ? ("\n AND b.user_id IN ($members)") :'')
		. "\n ORDER BY u.username, u.name"
		;
		$db->SetQuery( $query );
		$rows = $db->LoadObjectList();
		
		$exist_user_ids = array();
		if(count($rows)){
			foreach($rows as $key=>$row){
				$exist_user_ids[] = $row->user_id;	
			}	
		}
		
		$staff_learners = ($JLMS_ACL->_role_type == 3 && isset($JLMS_ACL->_staff_learners))?$JLMS_ACL->_staff_learners:array();
		if(count($staff_learners)){
			$str_staff_learners = implode(",", $staff_learners);
			$str_exist_user_ids = implode(",", $exist_user_ids);
			$query = "SELECT distinct u.id as user_id, u.username, u.name, u.email, uig.course_id" //, xxx as ug_name"
			. "\n FROM #__users as u, #__lms_users_in_groups as uig"
			. "\n WHERE 1"
			. "\n AND u.id = uig.user_id"
			. "\n AND uig.course_id = '".$id."'"
			. "\n AND u.id IN (".$str_staff_learners.")"
			. ($assigned_groups_only ? ("\n AND u.id IN ($members)") :'')
			.(count($exist_user_ids) ? "\n AND u.id NOT IN (".$str_exist_user_ids.")" : '')
			;
			$db->setQuery($query);
			$rows_ceo = $db->loadObjectList();
			
			$uids = array();
			foreach ($rows_ceo as $object) {
				$uids[] = $object->user_id;
			}
			$groups_array = array();
			if (count($uids)) {
				$query = "SELECT ug.user_id, g.ug_name, g.id AS group_id"
				."\n FROM #__lms_users_in_global_groups AS ug, #__lms_usergroups AS g"
				."\n WHERE ug.group_id=g.id AND ug.user_id IN (".implode(',',$uids).")"
				;
				$db->setQuery($query);
				$groups_array_raw = $db->loadObjectList();
				$groups_array = array();
				foreach ($groups_array_raw as $object) {
					isset($groups_array[$object->user_id]) ? $groups_array[$object->user_id] .= '<br />'.$object->ug_name : $groups_array[$object->user_id] = $object->ug_name;
				}
				foreach ($rows_ceo as $key => $object) {
					isset($groups_array[$object->user_id]) ? $rows_ceo[$key]->ug_name = $groups_array[$object->user_id] : $rows_ceo[$key]->ug_name = '';
				}
			}
			$rows = array_merge($rows, $rows_ceo);
		}
		
		$uids = array();
		foreach ($rows as $row) {
			$uids[] = $row->user_id;
		}
		if (true) {//if (count($uids)) {
			$lists = array();
			$lists['is_full'] = $is_full;
			JLMS_GB_getUsersGrades($id, $uids, $rows, $lists);

			$lists = JLMS_hideScorms($lists);
			
			if($view == 'csv'){
				JLMS_exportCSV($rows, $lists, $reporting_header);
			} else if($view == 'xls'){
				JLMS_exportXLS($rows, $lists, $reporting_header);
			} else {
				$lms_titles_cache = & JLMSFactory::getTitles();
				$lms_titles_cache->setArray('users', $rows, 'user_id', 'username');
				JLMS_gradebook_html::showGradebook_CEO( $id, $option, $rows, $lists );
			}
		} else { JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid"));} //else for  if(count($uids)).....
	} elseif ($id) {	
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$id") );
	} else {
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid"));
	}
}

function JLMS_hideScorms($lists) {
	global $my, $JLMS_DB;

	$JLMS_ACL = JLMSFactory::getACL();
	$is_teacher = $JLMS_ACL->isTeacher();
	for($i=0;$i<count($lists['sc_rows']);$i++) {
		if($lists['sc_rows'][$i]->lp_type == 2 ) {
			$query = "SELECT scorm_package FROM #__lms_n_scorm WHERE id = '".$lists['sc_rows'][$i]->item_id."'";
			$JLMS_DB->SetQuery( $query );
			$scorm_package = $JLMS_DB->LoadResult();	
			
			$query = "SELECT id FROM #__lms_n_scorm WHERE scorm_package = '".$scorm_package."' AND course_id = 0";
			$JLMS_DB->SetQuery( $query );
			$scorm_lib_id = $JLMS_DB->LoadResult();	
			
			$query = "SELECT outdoc_share, owner_id, allow_link FROM #__lms_outer_documents WHERE file_id = '".$scorm_lib_id."' AND folder_flag = 3";
			$JLMS_DB->SetQuery( $query );
			$outer_doc = $JLMS_DB->LoadObject();	

			if(is_object($outer_doc) && isset($outer_doc->allow_link) && $outer_doc->allow_link == 1 ) {
			} else {
				unset($lists['sc_rows'][$i]);
			}
		}
	}

	$mas = array();
	foreach ($lists['sc_rows'] as $k=>$v) {
		$mas[] = $lists['sc_rows'][$k];	
	}
	unset($lists['sc_rows']);
	$lists['sc_rows'] = $mas;

	return $lists;
}

function JLMS_showUserGrade( $user_id, $option, $course_id = 0, $checked_rights = false ) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
			
	$task = strval(mosGetParam($_REQUEST, 'task', ''));	
	
	if (!$course_id) {
		$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	}
	$usertype = JLMS_GetUserType($my->id, $course_id);
	$JLMS_ACL = JLMSFactory::getACL();
	$usertype2 = JLMS_GetUserType($user_id, $course_id, false, true, true, true);
	//TODO: - check permissions here
	$we_can_proceed = false;
	if ($checked_rights ) {
		//learner view own gradebook?
		$we_can_proceed = true;
	} elseif ($course_id && $user_id) {
		if (($JLMS_ACL->CheckPermissions('gradebook', 'configure') && $usertype2)) {
			// teacher ?
			$we_can_proceed = true;
		} elseif (($JLMS_CONFIG->get('main_usertype') == 1)) {
			// learner????
			$we_can_proceed = true;
		} elseif ($JLMS_ACL->isStaff() && isset($JLMS_ACL->_staff_learners) && is_array($JLMS_ACL->_staff_learners) && in_array($user_id, $JLMS_ACL->_staff_learners)) {
			// CEO
			$we_can_proceed = true;
		}else if ( $JLMS_ACL->CheckPermissions('gradebook', 'view') && $my->id == $user_id ) 
		{
			// user
			$we_can_proceed = true;
		}
		
	} 

	if ($we_can_proceed) {
		$query = "SELECT b.*, u.username, u.name, u.email, c.ug_name"
		. "\n FROM #__lms_users_in_groups as b"
		. "\n LEFT JOIN #__lms_usergroups as c ON b.group_id = c.id AND c.course_id = '".$course_id."',"
		. "\n #__users as u"
		. "\n WHERE b.course_id = '".$course_id."' AND b.user_id = u.id"
		. "\n AND b.user_id = '".$user_id."'";
		$JLMS_DB->SetQuery( $query );
		$rows = $JLMS_DB->LoadObjectList();
		$uids = array();
		foreach ($rows as $row) {
			$uids[] = $row->user_id;
		}

		if (empty($uids)) {
			if ($JLMS_CONFIG->get('main_usertype') == 1) {
				$uids[] = $my->id;
				$user_info = new stdClass();
				$user_info->username = $my->username;
				$user_info->name = $my->name;
				$user_info->email = $my->email;
				$user_info->ug_name = '-';
				$user_info->user_id = $my->id;
				$user_info->course_id = $course_id;
				$rows[] = $user_info;
			}
		}
		
		/*Fix - Display All Global Groups User*/
		if ($JLMS_CONFIG->get('use_global_groups', 1)){
			$tmp = array();
			foreach($rows as $n=>$row){
				$tmp[$n] = $row;
				
				$query = "SELECT b.ug_name"
				. "\n FROM #__lms_users_in_global_groups as a, #__lms_usergroups as b"
				. "\n WHERE 1"
				. "\n AND a.group_id = b.id"
				. "\n AND b.course_id = 0"
				. "\n AND a.user_id = '".$row->user_id."'"
				. "\n ORDER BY b.ug_name"
				;
				$JLMS_DB->setQuery($query);
				$groups = JLMSDatabaseHelper::LoadResultArray();
				
				$str_groups = '';
				if(count($groups)){
					$str_groups = implode("<br />", $groups);
				}
				
				$tmp[$n]->ug_name = $str_groups;
			}
			if(isset($tmp) && count($tmp)){
				$rows = array();	
				$rows = $tmp;
			}
		}
		/*Fix - Display All Global Groups User*/

		if (count($uids)) {
			$lists = array();
			$lists['usertype'] = $usertype;
			$lists['user_id'] = $user_id;
			//JLMS_LP_populate_results($course_id, $rows, $uids);
			JLMS_GB_getUsersGrades($course_id, $uids, $rows, $lists);
			JLMS_GB_getUserCertificates($course_id, $user_id, $lists);

			for($i=0; $i<count($rows); $i++){
				for($j=0; $j<count($rows[$i]->quiz_info); $j++){
					$quiz_info = $rows[$i]->quiz_info[$j];
					$stud_quiz_uniq_dates = JLMS_GB_getStudentQuizUniqueDates($rows[$i]->user_id,$quiz_info->gbi_id, $quiz_info->user_pts);
					if (isset($stud_quiz_uniq_dates->user_unique_id)) {
						$rows[$i]->quiz_info[$j]->user_unique_id = $stud_quiz_uniq_dates->user_unique_id;
						$rows[$i]->quiz_info[$j]->stu_quiz_id = $stud_quiz_uniq_dates->stu_quiz_id;
					}
				}
			}	

			if ($JLMS_ACL->CheckPermissions('gradebook', 'configure')) {
				if ( $JLMS_CONFIG->get('course_spec_reg') ) {
					$user_data->spec_reg = 1;
	
					$JLMS_ACL = JLMSFactory::getACL();
					$sr_role = intval($JLMS_ACL->UserRole($JLMS_DB, $user_id, 1));
					$query = "SELECT role_id, id, is_optional FROM #__lms_spec_reg_questions WHERE course_id = $course_id AND (role_id = 0 OR role_id = $sr_role) ORDER BY role_id DESC, ordering";
					$JLMS_DB->SetQuery( $query );
					$sr_quests = $JLMS_DB->LoadObjectList();
					$sr_answs = array();
					if (!empty($sr_quests)) {
						$sr_role = $sr_quests[0]->role_id;
						$sr_ids = array();
						$sr_qq = array();
						foreach ($sr_quests as $srq) {
							if ($srq->role_id == $sr_role) {
								$sr_ids[] = $srq->id;
								$sr_qq[] = $srq;
							}
						}
						if (!empty($sr_ids)) {
							$sr_idss = implode(',',$sr_ids);
							$query = "SELECT a.*, b.course_question FROM #__lms_spec_reg_answers as a, #__lms_spec_reg_questions as b WHERE a.course_id = $course_id AND a.user_id = $user_id AND a.role_id = $sr_role AND a.quest_id IN ($sr_idss) AND a.quest_id = b.id ORDER BY b.ordering";
							$JLMS_DB->SetQuery( $query );
							$sr_answs = $JLMS_DB->LoadObjectList();
						}
					}
					$lists['enrollment_answers'] = $sr_answs;
				}
			}
			
			/*Integration Plugin Percentiles*/
			$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
			$_JLMS_PLUGINS->loadBotGroup('system');
			
			$data_plugin = new stdClass();
			$data_plugin->course_id = $course_id;
			$data_plugin->user_id = $user_id;
			
			if($out_plugin = $_JLMS_PLUGINS->trigger('onShowUserGrade', array($data_plugin))){
				if(count($out_plugin)){
					$percentiles = $out_plugin[0];
					
					$out = new stdClass();
					$out->show = 0;
					if(isset($percentiles) && $percentiles){
						$out->show = 1;
						$out->chart	= $percentiles->chart;
					}
					
					$lists['chart_percentiles'] = $out;
				}	
			}
			
			/*Integration Plugin Percentiles*/
			if( $task == 'gb_user_pdf')
				JLMS_gradebook_html::gbUserPDF( $course_id, $option, $rows, $lists );				
			else			
				JLMS_gradebook_html::showUserGradebook( $course_id, $option, $rows, $lists );
		} else {
			//JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid"));
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$course_id") );
		}
	} else {		
		if ($course_id) {
			$task = strval(mosGetParam($_REQUEST, 'task', ''));
			if ($task == 'gb_usergrade' || $task == 'gb_user_pdf' ) {
				JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gradebook&id=$course_id") );
			} else {
				JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$course_id") );
			}
		} else {
			JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid"));
		}
	}
}

function JLMS_GB_getStudentQuizUniqueDates($student_id, $quiz_id, $user_pts){
	global $JLMS_DB;
	$query = "SELECT `c_id` as stu_quiz_id, `unique_id` as user_unique_id" 
	." FROM `#__lms_quiz_r_student_quiz`"	
	." WHERE `c_student_id` = ".intval($student_id)
	." AND `c_quiz_id` = ".intval($quiz_id)
	." AND `c_total_score` = ".$user_pts
    ." ORDER BY c_id DESC"
	." LIMIT 0,1";

	$JLMS_DB->setQuery($query);
	$result = $JLMS_DB->loadObject(); 
	return $result;	
}

function JLMS_GB_getUserCertificates($id, $user_id, &$lists) {
	/**
	 * Certificates MOD - 04.10.2007 (DEN)
	 * We will show the list of all achieved certificates in the User Gradebook
	 */
	global $JLMS_DB;
	$JLMS_ACL = JLMSFactory::getACL();
	$query = "SELECT * FROM #__lms_quiz_t_quiz WHERE course_id = '".$id."' AND c_certificate <> 0 ORDER BY c_title";
	$JLMS_DB->SetQuery( $query );
	$quiz_rows = $JLMS_DB->LoadObjectList();

	$p = array();
	foreach ($quiz_rows as $qrow) {
		$pp = new stdClass();
		$pp->gbi_id = $qrow->c_id;
		$pp->user_pts = 0;
		$pp->user_status = -1;
		$pp->quiz_name = '';
		$pp->crtf_id = '';
		$p[] = $pp;
	}

	$certificates = array();
	$quiz_ans = array();
	if (count($quiz_rows)) {
		$query = "SELECT a.*, b.c_full_score, b.c_title, b.c_certificate FROM #__lms_quiz_results as a, #__lms_quiz_t_quiz as b WHERE a.course_id = '".$id."'"
		. "\n AND a.quiz_id = b.c_id AND a.user_id = $user_id ORDER BY a.user_id, a.quiz_id";
		$JLMS_DB->SetQuery( $query );
		$quiz_ans = $JLMS_DB->LoadObjectList();
		
		$j = 0;
		while ($j < count($quiz_ans)) {
			if ($quiz_ans[$j]->user_id == $user_id) {
				$k = 0;
				while ($k < count($p)) {
					if ($p[$k]->gbi_id == $quiz_ans[$j]->quiz_id) {
						$p[$k]->user_pts = $quiz_ans[$j]->user_score;
						$p[$k]->user_status = $quiz_ans[$j]->user_passed;
						$p[$k]->quiz_name = $quiz_ans[$j]->c_title;
						$p[$k]->crtf_id = $quiz_ans[$j]->c_certificate;
						$p[$k]->crtf_date = $quiz_ans[$j]->quiz_date;
					}
					$k ++;
				}
			}
			$j ++;
		}

		$certificates = array();
		foreach ($p as $pp) {
			if ($pp->user_status == 1) {
				$query = "SELECT * FROM #__lms_quiz_r_student_quiz WHERE c_quiz_id = $pp->gbi_id AND c_student_id = $user_id AND c_total_score = $pp->user_pts AND c_passed = 1 LIMIT 0,1";
				$JLMS_DB->SetQuery( $query );
				$u_res = $JLMS_DB->LoadObject();
				if (is_object($u_res)) {
					$role = $JLMS_ACL->UserRole($JLMS_DB, $user_id, 1);
					$query = "SELECT crtf_date FROM #__lms_certificate_prints WHERE user_id = $user_id AND (role_id = $role OR role_id = 0)  AND course_id = $id AND quiz_id = $pp->gbi_id AND crtf_id = $pp->crtf_id"
					. "\n ORDER BY role_id DESC LIMIT 0,1";
					/* !!!!!!!! Bring from DB date of printing by user role or by default role (only if userrole not found) - imenno dlya etogo tut sidit ORDER i LIMIT*/
					$JLMS_DB->SetQuery( $query );
					$crtf_date = $JLMS_DB->LoadResult();
					if (!$crtf_date) {
						$crtf_date = date("Y-m-d H:i:s", (strtotime($u_res->c_date_time) + $u_res->c_total_time));
					}
					$ppp = new stdClass();
					$ppp->user_id = $user_id;
					$ppp->stu_quiz_id = $u_res->c_id;
					$ppp->user_unique_id = $u_res->unique_id;
					$ppp->quiz_name = $pp->quiz_name;
					//$ppp->crtf_date = $crtf_date;
					$ppp->crtf_date = $pp->crtf_date;
					$certificates[] = $ppp;
				}
			}
		}
	}
	$lists['user_quiz_certificates'] = & $certificates;
	/* END of Certificates MOD */
}

function JLMS_GB_certificate( $course_id, $option ) {
	global $my, $JLMS_DB, $Itemid;
	//$usertype = JLMS_GetUserType($my->id, $course_id);
	$JLMS_ACL = JLMSFactory::getACL();
	if ($course_id && $JLMS_ACL->CheckPermissions('course', 'manage_certificates')) {
		/*require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_certificates.php");
		JLMS_editCertificate_Page($row);*/
		$row = new mos_Joomla_LMS_Certificate( $JLMS_DB );
		$query = "SELECT * FROM #__lms_certificates WHERE course_id = '".$course_id."' AND crtf_type = 1 AND parent_id = 0";
		$JLMS_DB->SetQuery( $query );
		$row1 = $JLMS_DB->loadObject();
		$crtf_id = (isset($row1->id) && $row1->id) ? $row1->id : 0;
		if (is_object($row1)) {
			$row->id = isset($row1->id) ? $row1->id : 0;
			$row->course_id = isset($row1->course_id) ? $row1->course_id : 0;
			$row->crtf_type = isset($row1->crtf_type) ? $row1->crtf_type : 0;
			$row->parent_id = isset($row1->parent_id) ? $row1->parent_id : 0;
			$row->published = isset($row1->published) ? $row1->published : 0;
			$row->file_id = isset($row1->file_id) ? $row1->file_id : 0;
			$row->crtf_text = isset($row1->crtf_text) ? $row1->crtf_text : 0;
			$row->crtf_name = isset($row1->crtf_name) ? $row1->crtf_name : '';
			$row->text_x = isset($row1->text_x) ? $row1->text_x : 0;
			$row->text_y = isset($row1->text_y) ? $row1->text_y : 0;
			$row->text_size = isset($row1->text_size) ? $row1->text_size : 0;
			$row->crtf_align = isset($row1->crtf_align) ? $row1->crtf_align : 0;
			$row->crtf_shadow = isset($row1->crtf_shadow) ? $row1->crtf_shadow : 0;
			$row->crtf_font = isset($row1->crtf_font) ? $row1->crtf_font : 0;
		}
		if ($crtf_id) {
			$query = "SELECT * FROM #__lms_certificates WHERE course_id = '".$course_id."' AND parent_id = $crtf_id";
			$JLMS_DB->SetQuery( $query );
			$row->add_certificates = $JLMS_DB->loadObjectList();
		} else {
			$row->add_certificates = array();
		}
		$lists = array();
		JLMS_gradebook_html::showCourseCertificate( $course_id, $option, $row, $lists );
	} else {
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid"));
	}
}
function JLMS_saveCertificateGB( $course_id, $option ) {
	global $Itemid;
	require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_certificates.php");
	JLMS_Certificates::JLMS_saveCertificate( $course_id, $option, 1, "index.php?option=$option&Itemid=$Itemid&task=gb_certificates&id=$course_id" );
}
function JLMS_previewCertificate( $course_id, $option ) {
	global $my, $JLMS_DB, $Itemid;
	//$usertype = JLMS_GetUserType($my->id, $course_id);
	$JLMS_ACL = JLMSFactory::getACL();
	if ($course_id && $JLMS_ACL->CheckPermissions('gradebook', 'configure')) {
		$query = "SELECT id FROM #__lms_certificates WHERE course_id = '".$course_id."' AND crtf_type = 1";
		$JLMS_DB->SetQuery( $query );
		$crtf_id = $JLMS_DB->LoadResult();
		require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_certificates.php");
		$query = "SELECT * FROM #__users WHERE id = '".$my->id."'";
		$JLMS_DB->SetQuery( $query );
		$u_data = $JLMS_DB->LoadObjectList();
		$tm_obj->username = isset($u_data[0]->username)?$u_data[0]->username:'';
		$tm_obj->name = isset($u_data[0]->name)?$u_data[0]->name:'';
		$tm_obj->crtf_date = strtotime(gmdate("Y-m-d H:i:s"));
		$tm_obj->crtf_spec_answer = '';
		/*$query = "SELECT b.user_answer FROM #__lms_courses as a, #__lms_spec_reg_answers as b WHERE a.id = $course_id AND a.spec_reg = 1 AND a.id = b.course_id AND b.user_id = ".$my->id;
		$JLMS_DB->SetQuery( $query );
		$user_answer = $JLMS_DB->LoadResult();
		if ($user_answer) {
			$tm_obj->crtf_spec_answer = $user_answer;
		}*/
		$tm_obj->is_preview = true;
		JLMS_Certificates::JLMS_outputCertificate( $crtf_id, $course_id, $tm_obj );
	} else {
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gradebook&id=$course_id"));
	}
}
function JLMS_getCertificate( $course_id, $option ) {
	global $my, $JLMS_DB, $Itemid;
	//$usertype = JLMS_GetUserType($my->id, $course_id);
	
	$JLMS_CONFIG = JLMSFactory::getConfig();
	
	$JLMS_ACL = JLMSFactory::getACL();
	$user_id_request = intval(mosGetParam( $_REQUEST, 'user_id', 0));
	$user_id = $my->id;
	if ($user_id_request && $user_id_request != $user_id) {
		if ($JLMS_ACL->isCourseTeacher()) {
			$user_id = $user_id_request;
		} elseif ($JLMS_ACL->isStaff() && isset($JLMS_ACL->_staff_learners) && is_array($JLMS_ACL->_staff_learners) && in_array($user_id_request, $JLMS_ACL->_staff_learners)) {
			$user_id = $user_id_request;
		}
	}

	$query = "SELECT id, crt_date FROM #__lms_certificate_users WHERE course_id = '".$course_id."' AND user_id = '".$user_id."' AND crt_option = 1";
	$JLMS_DB->SetQuery( $query );
	$row = $JLMS_DB->LoadObject();
	
	//proeducate certificate
	if($JLMS_CONFIG->get('enabled_clear_course_user_data', false)){
		$query = "SELECT * FROM #__lms_certificate_prints_arhive WHERE course_id = '".$course_id."' AND user_id = '".$user_id."'";
		$JLMS_DB->setQuery($query);
		$certificate_prints_arhive = $JLMS_DB->loadObjectList();
	}
	//proeducate certificate

	//if ($course_id && $JLMS_ACL->CheckPermissions('gradebook', 'view') && isset($row->id) && $row->id) {
	if ($user_id && $course_id && isset($row->id) && $row->id) {
		$tm_obj = new stdClass();
		if ($row->crt_date == '0000-00-00 00:00:00' || !$row->crt_date) {
			$row->crt_date = gmdate('Y-m-d H:i:s');
			$query = "UPDATE #__lms_certificate_users SET crt_date = '".$row->crt_date."' WHERE id = ".$row->id." AND course_id = '".$course_id."' AND user_id = '".$user_id."' AND crt_option = 1";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			$tm_obj->force_update_print_date = true;//to fix bug with 'null' date of print.
		}
		$query = "SELECT id FROM #__lms_certificates WHERE course_id = '".$course_id."' AND crtf_type = 1 AND published = 1";
		$JLMS_DB->SetQuery( $query );
		$crtf_id = $JLMS_DB->LoadResult();
		require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_certificates.php");
		$query = "SELECT * FROM #__users WHERE id = '".$user_id."'";
		$JLMS_DB->SetQuery( $query );
		$u_data = $JLMS_DB->LoadObjectList();
		
		$query = "SELECT * FROM #__lms_courses WHERE id = '".$course_id."'";
		$JLMS_DB->setQuery( $query );
		$course_data = $JLMS_DB->LoadObjectList();
		
		$tm_obj->username = isset($u_data[0]->username)?$u_data[0]->username:'';
		$tm_obj->name = isset($u_data[0]->name)?$u_data[0]->name:'';
		$tm_obj->course_name = isset($course_data[0]->course_name)?$course_data[0]->course_name:'';
		$tm_obj->crtf_date = strtotime($row->crt_date); //time();
		//$tm_obj->crtf_date = $row->crt_date; //time();
		$tm_obj->crtf_spec_answer = '';
		$tm_obj->is_preview = false;
		$tm_obj->force_update_print_date = true;
		
		$user = new stdClass();
		$user->id = isset($u_data[0]->id) ? $u_data[0]->id : 0;
		$user->username = isset($u_data[0]->username) ? $u_data[0]->username : '';
		$user->name = isset($u_data[0]->name) ? $u_data[0]->name : '';
		$user->email = isset($u_data[0]->email) ? $u_data[0]->email : '';
		JLMS_Certificates::JLMS_outputCertificate( $crtf_id, $course_id, $tm_obj, $user );
	} else
	if(isset($certificate_prints_arhive) && count($certificate_prints_arhive)){
		//proeducate certificate
		getArhiveCertificate($course_id, $user_id);
		//proeducate certificate
	} else {
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=gradebook&id=$course_id"));
	}
}

/*
 * 15 May 2014
 * Method for reduction of high load
 * + 20 May 14 add return updated $list for additional results
 */
function JLMS_getCertDataByParts($rows, $id, $lists)
    {
       $start_point = $elem_counter = 0;
       $res_main_data = $result_rows = $uids = array();
       $rows_count = count($rows);
       $step_width = 50;

       while ($elem_counter <= $rows_count)
       {
            $result_rows = $uids = array();
                for($i = $start_point; $i < ($start_point + $step_width); $i++)
                 {
                    if (isset($rows[$i]))
                     {
                        $uids[] = $rows[$i]->user_id;
                        $result_rows[] = $rows[$i];
                     }
                     $elem_counter++;
                 }
            $start_point += $step_width;
            JLMS_GB_getUsersGrades($id, $uids, $result_rows, $lists);
            $res_main_data[] = $result_rows;

            if ($start_point > $rows_count) break; // On the safe side
       }

       if (sizeof($res_main_data))
        {
            $rows = array();
               foreach($res_main_data as $res_main_item)
               {
                       if (sizeof($res_main_item))
                       {
                           foreach($res_main_item as $row)
                                { $rows[] = $row; }
                       }
               }
        }
        $arr_ret = array($rows,$lists);
        return $arr_ret;
    }