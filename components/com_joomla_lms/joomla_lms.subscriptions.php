<?php
/**
* joomla_lms.subscriptions.php
* JoomaLMS eLearning Software http://www.joomlalms.com/
* * * (c) ElearningForce Inc - http://www.elearningforce.biz/
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );
require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.subscriptions.html.php");
require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "libraries" . DS . "lms.lib.user_sessions.php");
$task 	= mosGetParam( $_REQUEST, 'task', '' );
if ($task != 'callback') {
	global $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id',0);
	$pathway = array();
	$pathway[] = array('name' => _JLMS_PATHWAY_HOME, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"), 'is_home' => true);
	$pathway[] = array('name' => _JLMS_SUBSCRIPTION, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=subscription&amp;id=$course_id"));
	JLMSAppendPathWay($pathway);
}
if ($task == 'subscription' ){
	JLMS_ShowHeading();
}
$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) );
$cid = josGetArrayInts('cid', $_POST);//mosGetParam( $_POST, 'cid', array(0) );
if (!is_array( $cid )) { $cid = array(0); }

switch ( $task ) {
	//susbcription for one course
	case 'subscription':
	$after_login = 0;
	if (!empty($cid) && isset($cid[0]) && $cid[0]){
		$id = $cid[0];
		$from_courses = true;
	} else {
		$after_reg = intval( mosGetParam( $_REQUEST, 'after_reg', 0 ) ); //after login/registration during subscription to the !FREE! course
		$after_login = intval( mosGetParam( $_REQUEST, 'after_login', 0 ) );
		if ($after_reg && $id) {
			$from_courses = true;
		} else {
			$from_courses = false;
		}
	}
	JLMS_list_subscriptions( $id, $cid, $option, $from_courses, $after_login);	break;
	case 'course_subscribe':	JLMS_course_subscribe( $id, $option ); 					break;
	case 'callback':			JLMS_callBack($option); 								break;
	case 'subscribe':			JLMS_subscription( $option, $id); 						break;
	case 'subs_login':			JLMS_log_in_joomla($option);							break;
	case 'subs_register':
		JLMS_SUBS_register($option);
	break;

	case 'subs_register_cb':
		JLMS_SUBS_register($option, 1);
	break;
}

function JLMS_SUBS_register( $option, $from_cb = 0 ) 
{	
	global $Itemid;
	
	$freeCourse = true;
	
	$msg = JLMS_UserSessions::register( $from_cb, $freeCourse );
	
	JLMSredirect(sefRelToAbs("index.php?option=".$option."&Itemid=".$Itemid."&task=subscription&after_reg=1"), $msg, false);
}

function JLMS_log_in_joomla($option){
	global $JLMS_DB,$JLMS_CONFIG,$JLMS_SESSION, $Itemid, $version;
	$username = stripslashes( strval( mosGetParam( $_POST, 'username', '' ) ) );
	$password = stripslashes( strval( mosGetParam( $_POST, 'passwd', '' ) ) );
	$is_error = false;
	$from_course_enrollment = intval( mosGetParam( $_POST, 'from_course_enrollment', 0 ) );
	
	if ( !$username || !$password ) {
		$is_error = true;
	} else {
		$remember = intval(mosGetParam($_REQUEST,'remember',0));
		$sub_id = intval( mosGetParam( $_POST, 'jlms_sub', 0) );
		$sub_proc = intval( mosGetParam( $_POST, 'proc_id', 0) );
		if ($sub_proc){	$JLMS_SESSION->set('sub_proc' , $sub_proc); }
		if ($sub_id){	$JLMS_SESSION->set('sub_id' , $sub_id); }

		$login_success = JLMS_UserSessions::processLogin($username, $password);
		
		if ($login_success) {
			if ($from_course_enrollment) {
				JLMSredirect(sefRelToAbs("index.php?option=".$option."&amp;Itemid=".$Itemid."&amp;task=subscription&amp;id=$from_course_enrollment&amp;after_reg=1"), _JLMS_LOGIN_SUCCESS."<br />"._JLMS_SUBSCRIBE_CONTINUE, false);
			} else {
				JLMSredirect(sefRelToAbs("index.php?option=".$option."&amp;Itemid=".$Itemid."&amp;task=subscription&amp;after_login=1"), _JLMS_LOGIN_SUCCESS."<br />"._JLMS_SUBSCRIBE_CONTINUE, false);
			}
		} else {
			$is_error = true;
		}
	}
	if ($is_error) {		
		if ($from_course_enrollment) {
			JLMSredirect(sefRelToAbs("index.php?option=".$option."&amp;Itemid=".$Itemid."&amp;task=subscription&amp;id=$from_course_enrollment&amp;after_reg=1"), _JLMS_LOGIN_INCORRECT, false);
		} else {
			JLMSredirect(sefRelToAbs("index.php?option=".$option."&amp;Itemid=".$Itemid."&amp;task=subscription"), _JLMS_LOGIN_INCORRECT, false);
		}
	}
}

function JLMS_callBack( $option ) {
	global $JLMS_DB, $my, $Itemid;
	$proc_id = intval(mosGetParam($_REQUEST, 'proc', 0));
	$item_number = intval(mosGetParam($_POST, 'item_number', 0));//$_POST['item_number'];

	$query = "SELECT * FROM #__lms_subscriptions_procs WHERE id=".$proc_id;
	$JLMS_DB->setQuery( $query );
	$proc = $JLMS_DB->loadObject();
	if (is_object($proc) && isset($proc->id)) {
		if ($proc->id) {
			if (file_exists(_JOOMLMS_FRONT_HOME.'/includes/processors/'.$proc->filename.'.php')){
				require_once(_JOOMLMS_FRONT_HOME.'/includes/processors/'.$proc->filename.'.php');
				$newClass = "JLMS_".$proc->filename;
				$newProcObj = new $newClass();
				$newProcObj->validate_callback($proc);

				// ------ invoice email ------ -//

				$rowz = new jlms_adm_config();
				$rowz->loadFromDb( $JLMS_DB );

				$params_proc = new mosParameters( $proc->params );
				//var_dump($_REQUEST);die();

				//$item_number = $_POST['item_number']; - find 'getparam' above
				$query = "SELECT p.* FROM #__lms_payments as p WHERE p.id=". (int) $item_number;
				$JLMS_DB->setQuery($query);
				$paym_res = $JLMS_DB->loadObjectList();
				if (count($paym_res)) {
					$payment_type = $paym_res[0]->payment_type;
					if (!$payment_type) {
						$query = "SELECT s.sub_name as description, s.price as price, s.discount as discount FROM #__lms_subscriptions as s WHERE s.id = ".$paym_res[0]->sub_id;
						$JLMS_DB->setQuery($query);
						$payment_sub_info = $JLMS_DB->loadObject();
						if (is_object($payment_sub_info)) {
							$paym_res[0]->description = $payment_sub_info->description;
							$paym_res[0]->price = $payment_sub_info->price;
							$paym_res[0]->discount = $payment_sub_info->discount;
						} else {
							$paym_res = null;
						}
					} elseif ($payment_type == 1) {
						$query = "SELECT s.price as price FROM #__lms_subscriptions_custom as s WHERE s.id = ".$paym_res[0]->sub_id;
						$JLMS_DB->setQuery($query);
						$payment_sub_info = $JLMS_DB->loadObject();
						if (is_object($payment_sub_info)) {
							$paym_res[0]->description = $JLMS_CONFIG->get('custom_subscr_name', 'Custom subscription');
							$paym_res[0]->price = $payment_sub_info->price;
							$paym_res[0]->discount = 0;
						} else {
							$paym_res = null;
						}
					} else {
						$paym_res = null;
					}
				}

				/*$query = "SELECT p.*,s.sub_name as description,s.price as price, s.discount as discount FROM #__lms_payments as p, #__lms_subscriptions as s WHERE p.sub_id = s.id AND p.id=". (int) $item_number;
				$JLMS_DB->setQuery($query);
				$paym_res = $JLMS_DB->loadObjectList();*/

				if( count($paym_res) )
				if($params_proc->get('subscr_status') == 2 && $rowz->jlms_subscr_status_email && $my->id && $paym_res[0]->status = 'Completed')
				{
					//--- if completed send mail



					$print_row = new stdClass();
					$query = "SELECT * FROM #__lms_subscriptions_config";
					$JLMS_DB->setQuery($query);
					$rowz = $JLMS_DB->loadObjectList();
					if(count($rowz))
					$print_row = $rowz[0];
					require(_JOOMLMS_FRONT_HOME . '/includes/classes/lms.cb_join.php');
					$def_cb_templ = 'lms_cb_company';
					$print_row->company = JLMSCBJoin::getASSOC($def_cb_templ);
					$def_cb_templ = 'lms_cb_address';
					$print_row->address = JLMSCBJoin::getASSOC($def_cb_templ);
					$def_cb_templ = 'lms_cb_city';
					$print_row->city = JLMSCBJoin::getASSOC($def_cb_templ);
					$def_cb_templ = 'lms_cb_state';
					$print_row->state = JLMSCBJoin::getASSOC($def_cb_templ);
					$def_cb_templ = 'lms_cb_pcode';
					$print_row->pcode = JLMSCBJoin::getASSOC($def_cb_templ);
					$def_cb_templ = 'lms_cb_phone';
					$print_row->phone = JLMSCBJoin::getASSOC($def_cb_templ);
					$print_row->tax_amount = $paym_res[0]->tax_amount;
					$print_row->quantity = 1;
					$print_row->name = $my->name;
					$print_row->price = $paym_res[0]->amount;
					$print_row->shipping = 0;
					$print_row->description = $paym_res[0]->description;
					include(_JOOMLMS_FRONT_HOME.'/includes/classes/lms.pdf_invoice.php');
					$query = "SELECT lms_config_value FROM #__lms_config  WHERE lms_config_var = 'jlms_subscr_invoice_path'";
					$JLMS_DB->setQuery( $query );
					$file_path = $JLMS_DB->loadResult();
					$filenamez = md5(time().$my->id).".pdf";
					$file_path .= "/".$filenamez;
					//echo  $file_path;
					$my_pdf = new JLMS_PDF_invoice();
					$my_pdf->makeInvoicePDF($print_row, $file_path);
					$query = "UPDATE #__lms_subscriptions_config SET invoice_number = ".($print_row->invoice_number+1);
					$JLMS_DB->setQuery( $query );
					$JLMS_DB->query();
					$query = "INSERT INTO #__lms_subs_invoice(subid,filename) VALUES('".$item_number."','".$filenamez."')";
					$JLMS_DB->setQuery( $query );
					$JLMS_DB->query();

					include(_JOOMLMS_FRONT_HOME.'/joomla_lms.mailbox.php');
					if(!$print_row->mail_subj){
						$print_row->mail_subj = 'Invoice fot your order';
					}
					if(!$print_row->mail_body){
						$print_row->mail_body = "Hello. Check the attached file to see detailed information about your purchase.";
					}
					$subject = stripslashes($print_row->mail_subj);
					$body = stripslashes($print_row->mail_body);
					JLMS_mosMail( '', '', $my->email, $subject, $body, 0, NULL, NULL, $file_path, 'invoice.pdf' );
				}

			}else{
				JLMSRedirect(sefRelToAbs("index.php?option=".$option."&amp;Itemid=".$Itemid."&amp;task=subscription"));
			}
		}
	}
}

function JLMS_subscription( $option, $sub_id ){
	global $my,$Itemid, $JLMS_DB, $JLMS_CONFIG;

	$proceed_to_process = false;
	if ($sub_id == -1) {
		$new_cids = strval(mosGetParam($_REQUEST,'custom_courses',''));
		if ($new_cids) {
			$new_cid = explode(',',$new_cids);
			$new_cid2 = array();
			foreach ($new_cid as $nc) {
				if (intval($nc)) {
					$new_cid2[] = intval($nc);
				}
			}
			if (!empty($new_cid2)) {
				$new_cids = implode(',',$new_cid2);
				$subscription = new stdClass();
				$subscription->id = -1;
				$subscription->price = 0;
				$subscription->discount = 0;
				$subscription->access_days = 0;
				$subscription->account_type = 1;
				$subscription->sub_name = $JLMS_CONFIG->get('custom_subscr_name', 'Custom subscription');
				$subscription->start_date = null;
				$subscription->end_date = null;
				$subscription->courses = array();
				$subscription->course_names = array();
				$query = "SELECT * FROM #__lms_courses WHERE id IN ($new_cids) ORDER BY course_name";
				$JLMS_DB->setQuery($query);
				$ssscourses = $JLMS_DB->loadObjectList();
				foreach ($ssscourses as $ssscourse) {
					$subscription->courses[] = $ssscourse->id;
					$subscription->course_names[] = $ssscourse->course_name;
					$subscription->price = $subscription->price + $ssscourse->course_price;
				}
				if (!empty($subscription->courses)) {
					$proceed_to_process = true;
				}
			}
		}
	} else {
		$query = "SELECT * FROM `#__lms_subscriptions` WHERE id = $sub_id ";
		$JLMS_DB->setQuery($query);
		$subscription = $JLMS_DB->loadObject();
		if (is_object($subscription)) {
			$proceed_to_process = true;
		}
	}

	if ($proceed_to_process) {
		$jlms_tax_counting = $JLMS_CONFIG->get('enabletax');
		$tax_amount = 0;
		$rows2 = array();
		//22 June 2007 (DEN) Estimate $tax_amount
		// counting taxes
		if ($jlms_tax_counting) {
			$is_cb_installed = $JLMS_CONFIG->get('is_cb_installed', 0);
			$get_country_info = $JLMS_CONFIG->get('get_country_info', 0);
			$cb_country_filed_id = intval($JLMS_CONFIG->get('jlms_cb_country'));
			
			$isset_country = false;
			if($is_cb_installed && $get_country_info && $cb_country_filed_id){ //by Max (get country info)
				$query = "SELECT cf.name"
				. "\n FROM #__comprofiler_fields as cf"
				. "\n WHERE 1"
				. "\n AND cf.fieldid = '".$cb_country_filed_id."'"
				;
				$JLMS_DB->setQuery($query);
				$cb_country_field_name = $JLMS_DB->loadResult();
				
				$query = "SELECT ".$cb_country_field_name.""
				. "\n FROM #__comprofiler"
				. "\n WHERE 1"
				. "\n AND user_id = '".$my->id."'"
				;
				$JLMS_DB->setQuery($query);
				$country_name = $JLMS_DB->loadResult();
				
				require_once('components'. DS .$option. DS .'includes'. DS .'libraries'. DS .'lms.lib.countries.php');
				$CodeCountry = new CodeCountries();
				$code = $CodeCountry->code($country_name);
				if($code){
					$user_country = $code;	
				}
				$user_country_name = '';
				$us_state = '';
			} else {
				$ip_address = $_SERVER['REMOTE_ADDR'];
				$ip_address = '86.57.158.98';
				$isset_country = false;
				if(@ini_get('allow_url_fopen')){
					$fn = @file('http://api.hostip.info/get_html.php?ip='.$ip_address);
					// country ip identified
					if ($fn != false) {
						$ip_info = implode('',$fn);
						preg_match_all("(\(..\))", $ip_info, $dop);
						$user_country = str_replace('(','',str_replace(")",'',$dop[0][0]));
		
						preg_match_all("(\:.*\()", $ip_info, $dop2);
						$user_country_name = str_replace(': ','',str_replace(" (",'',$dop2[0][0]));
						preg_match_all("(\, ..)", $ip_info, $dop3);
						$us_state = @str_replace(', ','',$dop3[0][0]);
						//echo $us_state;
					}
				}
			}
			if(isset($user_country)){
				$query = "SELECT * FROM #__lms_subscriptions_countries WHERE published = 1 AND code='".$user_country."' ";
				$JLMS_DB->setQuery( $query );
				$rows2 = $JLMS_DB->loadObjectList();
				// if no country found
				if (!count($rows2)) {
					// check if in EU
					$query = "SELECT * FROM #__lms_subscriptions_countries WHERE published = 1 AND code='EU' AND list REGEXP '".$user_country."' ";
					$JLMS_DB->setQuery( $query );
					$rows_eu = $JLMS_DB->loadObjectList();
	
					if (count($rows_eu)) {
						$isset_country = true;
						$rows2[0]->tax_type = $rows_eu[0]->tax_type;
						$rows2[0]->tax = $rows_eu[0]->tax;
						$user_country_name = $rows_eu[0]->name.' ('.$user_country_name.')';
					}
				}
				// additional check for US
				if ($user_country == 'US') {
					$query = "SELECT * FROM #__lms_subscriptions_countries WHERE published = 1 AND code = 'US-".$us_state."' ";
					$JLMS_DB->setQuery( $query );
					$rows_states = $JLMS_DB->loadObjectList();
					if (count($rows_states)){
						$isset_country = true;
						$rows2 = array();
						$rows2[0]->tax_type = $rows_states[0]->tax_type;
						$rows2[0]->tax = $rows_states[0]->tax;
						$user_country_name = 'United states ('.$rows_states[0]->name.' )';
					}
				}
				
				//10.01.09 (Max) default tax option
				if(!$isset_country){
					$rows2[0]->tax_type = $JLMS_CONFIG->get('default_tax_type', 1);
					$rows2[0]->tax = $JLMS_CONFIG->get('default_tax', 0);	
				}
			}
			$tax_amount = 0;
			$disc = 0;
			$total = 0;
			if (count($rows2) > 0) {
				$sub_total = round($subscription->price,2);
				$sub_total1 = $sub_total;
				if ($subscription->account_type=='5'){
					$disc = $subscription->price*($subscription->discount/100);
					$sub_total1 = $sub_total-$disc;
				}
				if ($rows2[0]->tax_type == 1) $tax_amount = round( $sub_total1 / 100 * $rows2[0]->tax, 2);
				if ($rows2[0]->tax_type == 2) $tax_amount = $rows2[0]->tax;
			}
		}

		$subscription->tax_amount = $tax_amount;

		if ($subscription->account_type == 5 ){
			$subscription->price = round($subscription->price-($subscription->price*$subscription->discount/100),2);
		}
		$proc_id = intval(mosGetParam($_REQUEST, 'proc_id', 0));

		$query = "SELECT * FROM `#__lms_subscriptions_procs` WHERE id = $proc_id ";
		$JLMS_DB->setQuery($query);
		$processor = $JLMS_DB->loadObject();
		if (is_object($processor) && isset($processor->id) && $processor->id) {
		} else {
			$query = "SELECT * FROM `#__lms_subscriptions_procs` WHERE default_p = 1 ";
			$JLMS_DB->setQuery( $query );
			$processor = $JLMS_DB->loadObject();
		}
		if (is_object($processor) && isset($processor->id) && $processor->id) {
			$payment_type = 0;
			if ($subscription->id == -1) {
				$query = "INSERT INTO `#__lms_subscriptions_custom` ( price ) "
				."\n VALUES ('".$subscription->price."')";
				$JLMS_DB->setQuery($query);
				$JLMS_DB->query();
				$payment_type = 1;
				$sub_id = $JLMS_DB->insertid();
				foreach ($subscription->courses as $c_id) {
					$query = "INSERT INTO `#__lms_subscriptions_custom_courses` ( sub_id, course_id ) "
					."\n VALUES ($sub_id, $c_id)";
					$JLMS_DB->setQuery($query);
					$JLMS_DB->query();
				}
			} else {
				$payment_type = 0;
			}
			$subscription->payment_type = $payment_type;
			$query = "INSERT INTO `#__lms_payments` ( payment_type, sub_id, txn_id, processor, status, amount, tax_amount, date, user_id ) "
			."\n VALUES ($payment_type, '".$sub_id."','','".$processor->name."','pending','".$subscription->price."','".$tax_amount."', '".gmdate('Y-m-d H:i:s')."','".$my->id."' )";
			$JLMS_DB->setQuery($query);
			$JLMS_DB->query();
			$item_id = $JLMS_DB->insertid();
			$query = "SELECT * "
			. "\n FROM #__lms_subscriptions_procs"
			. "\n WHERE id = '$processor->id'"
			;
			$rowz = new jlms_adm_config();
			$rowz->loadFromDb( $JLMS_DB );
			$JLMS_DB->setQuery( $query );
			$proc_param = $JLMS_DB->loadObjectList();
			$params_proc = new mosParameters( $proc_param[0]->params );
			//var_dump($_REQUEST);die();

			if($params_proc->get('subscr_status') == 1 && $rowz->jlms_subscr_status_email && $my->id)
			{
				//--- if pending send mail
				$print_row = new stdClass();
				$query = "SELECT * FROM #__lms_subscriptions_config";
				$JLMS_DB->setQuery($query);
				$rowz = $JLMS_DB->loadObjectList();
				if(count($rowz))
				$print_row = $rowz[0];
				require(_JOOMLMS_FRONT_HOME . '/includes/classes/lms.cb_join.php');
				$def_cb_templ = 'lms_cb_company';
				$print_row->company = JLMSCBJoin::getASSOC($def_cb_templ);
				$def_cb_templ = 'lms_cb_address';
				$print_row->address = JLMSCBJoin::getASSOC($def_cb_templ);
				$def_cb_templ = 'lms_cb_city';
				$print_row->city = JLMSCBJoin::getASSOC($def_cb_templ);
				$def_cb_templ = 'lms_cb_state';
				$print_row->state = JLMSCBJoin::getASSOC($def_cb_templ);
				$def_cb_templ = 'lms_cb_pcode';
				$print_row->pcode = JLMSCBJoin::getASSOC($def_cb_templ);
				$def_cb_templ = 'lms_cb_phone';
				$print_row->phone = JLMSCBJoin::getASSOC($def_cb_templ);
				$print_row->tax_amount = $tax_amount;
				$print_row->quantity = 1;
				$print_row->name = $my->name;
				$print_row->price = $subscription->price;
				$print_row->shipping = 0;
				$print_row->description = $subscription->sub_name;
				include(_JOOMLMS_FRONT_HOME.'/includes/classes/lms.pdf_invoice.php');

				$query = "SELECT lms_config_value FROM #__lms_config  WHERE lms_config_var = 'jlms_subscr_invoice_path'";
				$JLMS_DB->setQuery( $query );
				$file_path = $JLMS_DB->loadResult();
				$filenamez = md5(time().$my->id).".pdf";
				$file_path .= "/".$filenamez;
				//echo  $file_path;
				$my_pdf = new JLMS_PDF_invoice();
				$my_pdf->makeInvoicePDF($print_row, $file_path);
				$query = "UPDATE #__lms_subscriptions_config SET invoice_number = ".($print_row->invoice_number+1);
				$JLMS_DB->setQuery( $query );
				$JLMS_DB->query();
				$query = "INSERT INTO #__lms_subs_invoice(subid,filename) VALUES('".$item_id."','".$filenamez."')";
				$JLMS_DB->setQuery( $query );
				$JLMS_DB->query();

				include(_JOOMLMS_FRONT_HOME.'/joomla_lms.mailbox.php');
				if(!$print_row->mail_subj){
						$print_row->mail_subj = 'Invoice fot your order';
					}
					if(!$print_row->mail_body){
						$print_row->mail_body = "Hello. Check the attached file to see detailed information about your purchase.";
					}
				$subject = stripslashes($print_row->mail_subj);
				$body = stripslashes($print_row->mail_body);
				JLMS_mosMail( '', '', $my->email, $subject, $body, 0, NULL, NULL, $file_path, 'invoice.pdf' );


			}
			if (file_exists(_JOOMLMS_FRONT_HOME.'/includes/processors/'.$processor->filename.'.php')){
				require_once(_JOOMLMS_FRONT_HOME.'/includes/processors/'.$processor->filename.'.php');
				$newClass = "JLMS_".$processor->filename;
				$newProcObj = new $newClass();
				$newProcObj->show_checkout($option ,$subscription, $item_id, $processor);

			}else{
				JLMSRedirect(sefRelToAbs("index.php?option=".$option."&amp;Itemid=".$Itemid."&amp;task=subscription"));
			}
		}else{
			JLMSRedirect(sefRelToAbs("index.php?option=".$option."&amp;Itemid=".$Itemid."&amp;task=subscription"));
		}
	} else {
		JLMSRedirect(sefRelToAbs("index.php?option=".$option."&amp;Itemid=".$Itemid."&amp;task=subscription"));
	}
}

function JLMS_list_subscriptions($id, $cid, $option,$from_courses, $after_login = false){
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG, $JLMS_SESSION;
	$very_global_sub_id = 0;
	$enable_custom_subscriptions = $JLMS_CONFIG->get('use_custom_subscr', false);
	if ($enable_custom_subscriptions) {
		$sub_courses_ids = strval(mosGetParam($_GET, 'sub_courses_ids',''));
		if ($after_login && isset($_COOKIE['sub_courses_ids']) && $_COOKIE['sub_courses_ids']) {
			if (isset($_COOKIE['sub_id']) && $_COOKIE['sub_id'] == -1) {
				$new_cids = urldecode($_COOKIE['sub_courses_ids']);
				$new_cid = explode(',',$new_cids);
				$i = 0;
				while ($i < count($new_cid)) {
					$new_cid[$i] = intval($new_cid[$i]);
					$i ++;
				}
				if (!empty($new_cid)) {
					$cid = $new_cid;
				}
			}
		} elseif ($sub_courses_ids) {
			$new_cid = explode(',',$sub_courses_ids);
			$new_cid_check = array();
			foreach ($new_cid as $nc) {
				if (intval($nc)) {
					$new_cid_check[] = intval($nc);
				}
			}
			if (!empty($new_cid_check)) {
				$cid = $new_cid_check;
			}
		} elseif (isset($_GET['course_filter'])) {
			if (!$_GET['course_filter']) {
				setcookie('sub_courses_ids', '', time()+3600, '/');
				unset($_COOKIE['sub_courses_ids']);
			}
		} elseif (!empty($cid) && count($cid) == 1) {
			if ($cid[0] == 0) {
			} else {
				setcookie('sub_courses_ids', '', time()+3600, '/');
				unset($_COOKIE['sub_courses_ids']);
			}
		} elseif (!empty($cid) && count($cid) > 1) {
			setcookie('sub_id', '-1', time()+3600, '/');
			$JLMS_SESSION->set('sub_id' , -1);
			$very_global_sub_id = -1;
		}
		
	}
	//print_r($cid);
	//include(_JOOMLMS_FRONT_HOME.'/includes/classes/jlms_pdf_invoice.php');
	//$my_pdf = new JLMS_PDF_invoice();
	//$my_pdf->makeInvoicePDF($my);
	
	$do_add = false;
	if ($JLMS_CONFIG->get('license_lms_users')) {
		/*$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
		$JLMS_DB->SetQuery( $query );*/
		$total_students = $JLMS_CONFIG->get('license_cur_users');/*$JLMS_DB->LoadResult();*/
		if (intval($total_students) < intval($JLMS_CONFIG->get('license_lms_users'))) {
			$do_add = true;
		}
		if (!$do_add && !$my->id) {
			$do_add = true;
		} elseif (!$do_add) {
			$query = "SELECT count(*) FROM #__lms_users_in_groups WHERE user_id = '".$my->id."'";
			$JLMS_DB->SetQuery( $query );
			if ($JLMS_DB->LoadResult()) {
				$do_add = true;
			}
		}
	} else {
		$do_add = true;
	}

	$course_id = intval(mosGetParam($_REQUEST, 'course_filter', $JLMS_SESSION->get('course_filter')));
	$JLMS_SESSION->set('course_filter', $course_id);
	if ($id){
		$course_id = $id;
	}
	$category_id = intval(mosGetParam($_REQUEST, 'category_filter', 0));
	//	$JLMS_SESSION->set('category_filter', $category_id);

	$new_cid = array();
	if ($enable_custom_subscriptions && (count($cid) > 1)) {
		$cids = implode(',',$cid);
		$query = "SELECT id, gid FROM `#__lms_courses` WHERE id IN ($cids)";
		$JLMS_DB->setQuery($query);
		$course_gids = $JLMS_DB->LoadObjectList();
		$new_cid = array();
		foreach ($course_gids as $course_gid) {
			if (JLMS_checkCourseGID($my->id, $course_gid->gid)) {
				$new_cid[] = $course_gid->id;
			}
		}
		if (empty($new_cid)) {
			JLMSRedirect(sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"));
		} else {
			$course_id = $new_cid[0]; // if only one course left
		}
		$new_cids = implode(',',$new_cid);
		setcookie('sub_courses_ids', $new_cids, time()+3600, '/');
		setcookie('sub_id', '-1', time()+3600, '/');
		$_COOKIE['sub_courses_ids'] = $new_cids;
		$_COOKIE['sub_id'] = -1;
	} elseif ($course_id) {
		$query = "SELECT gid FROM `#__lms_courses` WHERE id = $course_id ";
		$JLMS_DB->setQuery($query);
		$course_gid = $JLMS_DB->loadResult();

		$can_enroll = JLMS_checkCourseGID($my->id, $course_gid);
		if (!$can_enroll){
			JLMSRedirect(sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"));
		}
		setcookie('sub_course_id', $id, time()+3600, '/');
	} 
//	elseif ($category_id) {
//		$query = "SELECT gid FROM `#__lms_courses` WHERE cat_id=$category_id OR sec_cat LIKE '%|$category_id|%'";
//		$JLMS_DB->setQuery($query);
//		$course_gids = JLMSDatabaseHelper::loadResultArray();
//
//		foreach ($course_gids as $course_gid) {
//			$can_enroll = JLMS_checkCourseGID($my->id, $course_gid);
//			if (!$can_enroll){
//				JLMSRedirect(sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"));
//			}
//		}
//		setcookie('sub_course_id', $id);
//	} 
	else {
		if ($after_login && isset($_COOKIE['sub_course_id']) && $_COOKIE['sub_course_id']) {
			$course_id = $_COOKIE['sub_course_id'];
			$id = $_COOKIE['sub_course_id'];
		} elseif (isset($_COOKIE['sub_courses_ids']) && $_COOKIE['sub_courses_ids'] && $enable_custom_subscriptions) {
			$new_cids = urldecode($_COOKIE['sub_courses_ids']);
			$new_cid = explode(',',$new_cids);
			$i = 0;
			while ($i < count($new_cid)) {
				$new_cid[$i] = intval($new_cid[$i]);
				$i ++;
			}
		} else {
			setcookie('sub_course_id', '',time()+3600, '/');
			setcookie('sub_courses_ids', '', time()+3600, '/');
		}
	}
	if ($do_add) {
		$return = true;
		if ($from_courses && (count($new_cid) > 1) ) {
			$new_cids = implode(',',$new_cid);
			$query = "SELECT * FROM `#__lms_courses` WHERE id IN ($new_cids)";
			$JLMS_DB->setQuery($query);
			$courses_infos = $JLMS_DB->loadObjectList();
			$is_paid = false;
			$free = array();
			foreach ($courses_infos as $course_info) {
				if ($course_info->paid){
					$is_paid = true;
				} else {
					$free[] = $course_info;
				}
			}

			// temporary
			//$is_paid = false;
			//$courses_infos = $free;

			if (!$is_paid) {
				$course_id = $new_cid[0];
				$new_cid = array();
				foreach ($courses_infos as $course_info) {
					$course_usertype = 0;
					if ( in_array($course_info->id, $JLMS_CONFIG->get('teacher_in_courses',array(0))) ) {
						$course_usertype = 1;
					} elseif ( in_array($course_info->id, $JLMS_CONFIG->get('student_in_courses',array(0))) ) {
						$course_usertype = 2;
					}
					if (!$course_usertype) {
						$new_cid[] = $course_info->id;
					}
				}
				if (empty($new_cid)) {
					JLMSRedirect(sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$course_id"), '_clear_');
				}

				JLMS_subscription_html::JLMS_free_subscriptions($option, $courses_infos, $course_id, true);
				$return = false;
			}
		} elseif ($from_courses && $course_id){
			$query = "SELECT * FROM `#__lms_courses` WHERE id = $course_id ";
			$JLMS_DB->setQuery($query);
			$course_info = $JLMS_DB->loadObject();
			if (!$course_info->paid){
				if ($course_info->id){
					$course_usertype = 0;
					if ( in_array($course_id, $JLMS_CONFIG->get('teacher_in_courses',array(0))) ) {
						$course_usertype = 1;
					} elseif ( in_array($course_id, $JLMS_CONFIG->get('student_in_courses',array(0))) ) {
						$course_usertype = 2;
					}
					if ($course_usertype) {
						JLMSRedirect(sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$id"), '_clear_');
					}
					JLMS_subscription_html::JLMS_free_subscriptions($option , $course_info, $course_id);
				}else{
					JLMSRedirect(sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"));
				}
				$return = false;
			}
		}
		/*
		elseif ($from_courses && $category_id) {
			$query = "SELECT * FROM `#__lms_courses` WHERE cat_id=$category_id OR sec_cat LIKE '%|$category_id|%'";
			$JLMS_DB->setQuery($query);
			$courses_info = $JLMS_DB->loadObjectList();
			foreach ($courses_info as $course_info) {
				if (!$course_info->paid){
					if ($course_info->id){
						$course_usertype = 0;
						if ( in_array($course_id, $JLMS_CONFIG->get('teacher_in_courses',array(0))) ) {
							$course_usertype = 1;
						} elseif ( in_array($course_id, $JLMS_CONFIG->get('student_in_courses',array(0))) ) {
							$course_usertype = 2;
						}
						if ($course_usertype) {
							JLMSRedirect(sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$id"), '_clear_');
						}
						JLMS_subscription_html::JLMS_free_subscriptions($option , $course_info, $course_id);
					}else{
						JLMSRedirect(sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"));
					}
					$return = false;
				}
			}
		}
		*/
		if ($return){

			if ($my->id) {
				if ($JLMS_CONFIG->get('use_secure_checkout', false) && $JLMS_CONFIG->get('secure_url') && !$JLMS_CONFIG->get('under_ssl', false)) {
					//var_dump($after_login);var_dump($id);var_dump($_COOKIE);var_dump($new_cids);
					//die;
					if ($after_login || $id) {
						$do_redirect = true;
						if ( $enable_custom_subscriptions && isset($_COOKIE['sub_id']) && ($_COOKIE['sub_id'] == -1)) {
							if ($after_login && isset($_COOKIE['sub_courses_ids']) && $_COOKIE['sub_courses_ids']) {
								$new_cids = urldecode($_COOKIE['sub_courses_ids']);
								$new_cid = explode(',',$new_cids);
								$i = 0;
								while ($i < count($new_cid)) {
									$new_cid[$i] = intval($new_cid[$i]);
									$i ++;
								}
								if (count($new_cid)) {
									$custom_courses = implode(',',$new_cid);
									$do_redirect = false;
									JLMSRedirect($JLMS_CONFIG->get('secure_url')."/index.php?option=com_joomla_lms&Itemid=$Itemid&task=subscription".($id?"&id=$id":'')."&sub_courses_ids=".$custom_courses.($after_login?"&after_login=1":''));
								}
							} elseif ($new_cids) {
								JLMSRedirect($JLMS_CONFIG->get('secure_url')."/index.php?option=com_joomla_lms&Itemid=$Itemid&task=subscription&sub_courses_ids=".$new_cids);
							}
						}
						if ($do_redirect) {
							JLMSRedirect($JLMS_CONFIG->get('secure_url')."/index.php?option=com_joomla_lms&Itemid=$Itemid&task=subscription".($id?"&id=$id":'').($after_login?"&after_login=1":''));
						}
					} else {
						JLMSRedirect($JLMS_CONFIG->get('secure_url')."/index.php?option=com_joomla_lms&Itemid=$Itemid&task=subscription");
					}
				}
			}

			$type[] = mosHTML::makeOption( 0, _JLMS_SUBSCR_ALL_COURSES );

			$query = "SELECT gid, paid, id AS value, course_name AS text, cat_id, sec_cat FROM `#__lms_courses` WHERE published = 1";
			$JLMS_DB->setQuery($query);
			$courses = $JLMS_DB->loadObjectList();
			$courseDis = array();
			$avail_courses = array();
			foreach($courses as $course){
				$can_enroll = JLMS_checkCourseGID($my->id, $course->gid);
				if ($category_id && false) { // ????????????? - what is it?
					$in_cat = ($course->cat_id == $category_id);
					if ($JLMS_CONFIG->get('sec_cat_use', 0)) $in_sec_cat = (strstr($course->sec_cat, "|$category_id|") != false);
					else $in_sec_cat = false;
				} else {
					$in_cat = true;
					$in_sec_cat = true;
				}
				if ($can_enroll){
					if ($course->paid){
						$courseDis[] = $course;
					}
					if ($in_cat || $in_sec_cat)	$avail_courses[] = $course->value;
				}
			}
			//$avail_courses = implode(",", $ids);
			$courses = array_merge($type, $courseDis);

			//creating category filter
			$cat_string = -518768; //there is surely no category with such id))
			foreach ($courses as $row) {
				if (!isset($row->cat_id)) continue; //skip first element...
				if ($JLMS_CONFIG->get('sec_cat_use', 0)){
					$tmp = explode('|', $row->sec_cat);
					array_shift($tmp); array_pop($tmp);
					$tmp = (count($tmp)) ? ','.implode(',', $tmp) : '';
				} else {
					$tmp = '';
				}
				$cat_string .= ','.$row->cat_id.$tmp;
			}
			$query = "SELECT id, c_category FROM #__lms_course_cats WHERE id IN ($cat_string)";
			$JLMS_DB->setQuery($query);
			$categories = $JLMS_DB->loadObjectList();
			$tmp->c_category = _JLMS_SUBSCR_CATEGORY_ALL;
			$tmp->id = 0;
			array_unshift($categories, $tmp);

			$categorycourses 			= array();
			$categorycourses[0] 		= array();
			foreach ($courses as $row) {
				$categorycourses[0][] = mosHTML::makeOption( $row->value, $row->text, 'value', 'text' );
			}
			//print_r($courses);die;
			foreach ($categories as $category) {
				foreach ($courses as $row) {
					if (!isset($row->cat_id)) continue; //skip first element...
					if ($category->id == $row->cat_id || (strpos($row->sec_cat, $category->id) != false)) {
						if ($category->id) {
							$do_add_val = true;
							$do_add_null = true;
							if (isset($categorycourses[$category->id]) && is_array($categorycourses[$category->id])) {
								foreach ($categorycourses[$category->id] as $cccc) {
									if (isset($cccc->value)) {
										if ($cccc->value == 0) {
											$do_add_null = false;
										} elseif ($cccc->value == $row->value) {
											$do_add_val = false;
										}
									}
								}
							}
							if ($do_add_null) {
								$categorycourses[$category->id][] = mosHTML::makeOption( 0, _JLMS_SUBSCR_ALL_COURSES, 'value', 'text' );
							}
							if ($do_add_val) {
								$categorycourses[$category->id][] = mosHTML::makeOption( $row->value, $row->text, 'value', 'text' );
							}
						}
					}
				}
			}
			foreach ($categories as $category) {
				unset($tmp);
				$tmp->value = $category->id;
				$tmp->text = $category->c_category;
				$f_categories[] = $tmp;
			}
			//---->generate $category_course_ids - ids of courses in chosen category
			$category_course_ids = array();
			foreach ($categorycourses[$category_id] as $row) {
				$category_course_ids[] = $row->value;
			}
			//<----

			$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=subscription";
			$link = $link ."&amp;category_filter=".JLMS_SELECTED_INDEX_MARKER;			
			$link = $link ."&amp;course_filter=0";
			$link = processSelectedIndexMarker( $link );
			$lists['category_filter'] = mosHTML::selectList($f_categories, 'category_filter', 'class="inputbox" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $category_id);

			//category filter created

			$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=subscription";
			$link = $link ."&amp;category_filter=' + getCategoryValue() + '";
			$link = $link ."&amp;course_filter=".JLMS_SELECTED_INDEX_MARKER;			
			$link = processSelectedIndexMarker( $link );
			$lists['course_filter'] = mosHTML::selectList($categorycourses[$category_id], 'course_filter', 'class="inputbox" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $course_id );
			$lists['course_filter_big'] = (count($courseDis) ? true : false);
			$lists['course_filter_cur'] = $course_id;

			$limit		= intval( mosGetParam( $_REQUEST, 'limit', $JLMS_SESSION->get('list_limit', $JLMS_CONFIG->get('list_limit')) ) );
			$JLMS_SESSION->set('list_limit', $limit);
			$limitstart = intval( mosGetParam( $_REQUEST, 'limitstart', 0 ) );

			$query = "SELECT a.*, b.*, c.course_name "
			."FROM `#__lms_subscriptions` as a, `#__lms_subscriptions_courses` as b LEFT JOIN #__lms_courses as c ON b.course_id = c.id "
			."\n WHERE a.id = b.sub_id AND a.published = 1"
			."\n ORDER BY a.id, b.course_id ";
			$JLMS_DB->setQuery($query);
			$subscriptions = $JLMS_DB->loadObjectList();
			$new_subs = array();
			$ids = array(0);
			
/*			deb($course_id);
			deb($category_id);
			deb($category_course_ids);*/
			
			for ($j = 0, $m = count($subscriptions); $j < $m; $j ++) {
				$subscription = $subscriptions[$j];
				$do_new = true;
				for ($i = 0, $n = count($new_subs); $i < $n; $i ++) {
					if ($new_subs[$i]->id == $subscription->id) {
						$new_subs[$i]->courses[] = $subscription->course_id;
						$new_subs[$i]->course_names[] = $subscription->course_name;
						$do_new = false;
					}
				}
				if ($do_new) {
					$if_mogno = true;
					for ($k = 0, $f = count($subscriptions); $k < $f; $k ++) {
						if ($subscriptions[$k]->id == $subscription->id) {
							if (!in_array($subscriptions[$k]->course_id, $avail_courses)) {
								$if_mogno = false; break;
							}
						}
					}
					if ($if_mogno && ($course_id || $category_id || !empty($new_cid))) {
//					if ($if_mogno && $course_id) {
						$if_mogno = false;						
						for ($k = 0, $f = count($subscriptions); $k < $f; $k ++) {
							if ($subscriptions[$k]->id == $subscription->id) {
								if ($subscriptions[$k]->course_id == $course_id ||
									($category_id && in_array($subscriptions[$k]->course_id, $category_course_ids)) || 
									(!empty($new_cid) && in_array($subscriptions[$k]->course_id, $new_cid)) ) {
										$if_mogno = true; break;
								}
							}
						}
					}
					if ($if_mogno) {
						$new_sub = new stdClass();
						$new_sub->id = $subscription->id;
						$new_sub->price = $subscription->price;
						$new_sub->discount = $subscription->discount;
						$new_sub->access_days = $subscription->access_days;
						$new_sub->account_type = $subscription->account_type;
						$new_sub->sub_name = $subscription->sub_name;
						$new_sub->start_date = $subscription->start_date;
						$new_sub->end_date = $subscription->end_date;
						$new_sub->courses = array($subscription->course_id);
						$new_sub->course_names = array($subscription->course_name);
						$new_subs[] = $new_sub;
						$ids[] = $subscription->id;
					}
				}
			}

			$isfound_sub = false;
			$custom_sub = new stdClass();
			if ($enable_custom_subscriptions) {
				// try to find subscription, which includes all selected courses
				foreach ($new_subs as $nsub) {
					$good_courses = empty($nsub->courses) ? false : true;
					foreach ($nsub->courses as $nsub_course) {
						if (in_array($nsub_course, $new_cid)) {
							
						} else {
							$good_courses = false;
							break;
						}
					}
					if ($good_courses) {
						foreach ($new_cid as $newc) {
							if (in_array($newc, $nsub->courses)) {
							
							} else {
								$good_courses = false;
								break;
							}
						}
					}
					if ($good_courses) {
						$isfound_sub = true;
						break;
					}
				}
				if ($isfound_sub) {
					
				} else {
					$custom_sub->id = -1;
					$custom_sub->price = 0;
					$custom_sub->discount = 0;
					$custom_sub->access_days = 0;
					$custom_sub->account_type = 1;
					$custom_sub->sub_name = $JLMS_CONFIG->get('custom_subscr_name', 'Custom subscription');
					$custom_sub->start_date = null;
					$custom_sub->end_date = null;
					$custom_sub->courses = array();
					$custom_sub->course_names = array();
					$new_cids = implode(',', $new_cid);
					$query = "SELECT * FROM #__lms_courses WHERE id IN ($new_cids) ORDER BY course_name";
					$JLMS_DB->setQuery($query);
					$ssscourses = $JLMS_DB->loadObjectList();
					foreach ($ssscourses as $ssscourse) {
						if ($category_id && in_array($ssscourse->id, $category_course_ids) || !$category_id) {
							$custom_sub->courses[] = $ssscourse->id;
							$custom_sub->course_names[] = $ssscourse->course_name;
							$custom_sub->price = $custom_sub->price + $ssscourse->course_price;
						}
						//$custom_sub->courses[] = $ssscourse->id;
						//$custom_sub->course_names[] = $ssscourse->course_name;
						//$custom_sub->price = $custom_sub->price + $ssscourse->course_price;
					}
				}
			}

			$total = count($new_subs);
			$ids_string = implode(",", $ids);
			require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
			$pageNav = new JLMSPageNav( $total, $limitstart, $limit );

			$query = "SELECT id "
			."FROM `#__lms_subscriptions` "
			."\n WHERE id IN ($ids_string) "
			. "\n ORDER BY sub_name "
			. "\n LIMIT $pageNav->limitstart, $pageNav->limit";
			$JLMS_DB->setQuery($query);
			$subscriptions = $JLMS_DB->loadObjectList();

			if (count($subscriptions) && (count($subscriptions) != $total) && isset($_COOKIE['sub_id']) && $_COOKIE['sub_id'] && $after_login) {
				$sub_id = $_COOKIE['sub_id'];
				$is_found = false;
				foreach ($subscriptions as $sub_tmp) {
					if ($sub_tmp->id == $sub_id) {
						$is_found = true;
						break;
					}
				}
				if (!$is_found) {
					$do_search = true;
					$lim_s = $pageNav->limitstart;
					$lim = $pageNav->limit;
					if (($lim_s + $lim) > $total) {
						$do_search = false;
					}
					while ($do_search) {
						$lim_s = $lim_s + $lim;
						$query = "SELECT id "
						."FROM `#__lms_subscriptions` "
						."\n WHERE id IN ($ids_string) "
						. "\n ORDER BY sub_name "
						. "\n LIMIT $lim_s, $lim";
						$JLMS_DB->setQuery($query);
						$subscriptions1 = $JLMS_DB->loadObjectList();
						if (($lim_s + $lim) > $total) {
							$do_search = false;
						}
						foreach ($subscriptions1 as $sub_tmp) {
							if ($sub_tmp->id == $sub_id) {
								$is_found = true;
								$do_search = false;
								break;
							}
						}
					}
					if ($is_found) {
						$subscriptions = $subscriptions1;
						$pageNav->limitstart = $lim_s;
						$pageNav->limit = $lim;
					}
				}
			}

			$sub_limited = array();

			foreach($subscriptions as $subscription){
				if (in_array($subscription->id, $ids) ){
					foreach($new_subs as $sub){
						if ($sub->id == $subscription->id){
							$sub_limited[] = $sub;
						}
					}
				}
			}
			if ($enable_custom_subscriptions && !$isfound_sub && !empty($custom_sub->courses)) {
				array_unshift($sub_limited, $custom_sub);
				$JLMS_SESSION->set('sub_id',-1);
			}

			$query = "SELECT * FROM `#__lms_subscriptions_procs` WHERE published=1 ORDER BY ordering";
			$JLMS_DB->setQuery($query);
			$procs = $JLMS_DB->loadObjectList();
			$user = null;
			$cb = $JLMS_CONFIG->get('is_cb_installed',0);
			if ($my->id){
				if ($cb){
					$cb_config = $JLMS_CONFIG->getByPrefix('jlms_cb_', array());
					if (!empty($cb_config)) {
						$cb_select = implode(',', $cb_config);
						$query = "SELECT fieldid, name FROM `#__comprofiler_fields` WHERE fieldid IN (".$cb_select.")";
						$JLMS_DB->setQuery($query);
						$cb_fs = $JLMS_DB->loadObjectList();
					} else {
						$cb_fs = array();
					}

					$cb_fields_a = array();
					$cb_fields_assoc = array();
					foreach ($cb_fs as $cbf) {
						$cb_fields_a[] = $cbf->name;
						$cb_fields_assoc[$cbf->fieldid] = $cbf->name;
					}
					$user_info = null;
					if (!empty($cb_fields_a)) {
						$cb_fields = implode(',',$cb_fields_a);

						$query = "SELECT firstname, lastname, ".$cb_fields." FROM `#__comprofiler` WHERE user_id = ".$my->id ." ";
						$JLMS_DB->setQuery($query);
						$user_info = $JLMS_DB->loadObject();
					}
					$user = new stdClass();
					$user->first_name = isset($user_info->firstname)?$user_info->firstname:'';
					$user->last_name = isset($user_info->lastname)?$user_info->lastname:'';
					$user->address = ($JLMS_CONFIG->get('jlms_cb_address', 0) && isset($cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_address', 0)])) ? $user_info->{$cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_address', 0)]} : '';
					$user->city = ($JLMS_CONFIG->get('jlms_cb_city', 0) && isset($cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_city', 0)])) ? $user_info->{$cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_city', 0)]} : '';
					$user->state = ($JLMS_CONFIG->get('jlms_cb_state', 0) && isset($cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_state', 0)])) ? $user_info->{$cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_state', 0)]} : '';
					$user->postal_code = ($JLMS_CONFIG->get('jlms_cb_postal_code', 0) && isset($cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_postal_code', 0)])) ? $user_info->{$cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_postal_code', 0)]} : '';
					$user->country = ($JLMS_CONFIG->get('jlms_cb_country', 0) && isset($cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_country', 0)])) ? $user_info->{$cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_country', 0)]} : '';
					$user->phone = ($JLMS_CONFIG->get('jlms_cb_phone', 0) && isset($cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_phone', 0)])) ? $user_info->{$cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_phone', 0)]} : '';
				}
			}			
			//				$user->address = $cb_config['jlms_cb_address'] ? $user_info->address : "";
			JLMS_subscription_html::JLMS_list_paid_subscriptions($option , $sub_limited, $pageNav, $procs, $lists, $avail_courses, $user, $very_global_sub_id );
		}

	} else {
		$msg = 'Sorry user limit is exceeded, please contact your LMS administrator.';
		$JLMS_SESSION->set('joomlalms_sys_message',$msg);

		JLMSRedirect (sefRelToAbs("index.php?option=$option&amp;task=courses&amp;Itemid=$Itemid"));
	}
}

function JLMS_course_subscribe( $id, $option ){
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG, $JLMS_SESSION;
	$query = "SELECT * FROM `#__lms_courses` WHERE id = '$id' AND self_reg != '0'  ";
	$JLMS_DB->setQuery($query);
	$course = $JLMS_DB->loadObject();
	if (is_object($course) && isset($course->gid)) {
		$can_enroll = JLMS_checkCourseGID($my->id, $course->gid);
	} else {
		$can_enroll = false;
	}

	if ($can_enroll && $my->id){ /* 03.Sept.2007 - (DEN) - $my->id - na vsyakii sluchai ;) */
		if ($course->paid == 0 && $course->self_reg == 1){
			$do_add = false;
			if ($JLMS_CONFIG->get('license_lms_users')) {
				$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
				$JLMS_DB->SetQuery( $query );
				$total_students = $JLMS_DB->LoadResult();
				if (intval($total_students) < intval($JLMS_CONFIG->get('license_lms_users'))) {
					$do_add = true;
				}
				if (!$do_add) {
					$query = "SELECT count(*) FROM `#__lms_users_in_groups` WHERE user_id = '".$my->id."'";
					$JLMS_DB->SetQuery( $query );
					if ($JLMS_DB->LoadResult()) {
						$do_add = true;
					}
				}
			} else {
				$do_add = true;
			}
			if ($do_add) {
				$user_info = new stdClass();
				$user_info->user_id = intval($my->id);
				$user_info->group_id = 0; //??? or what?
				$user_info->course_id = intval($id);
				
				$course_info = new stdClass();
				$course_info->course_id = $id;				
				$params = new JLMSParameters($course->params);
				$course_info->max_attendees = $params->get('max_attendees', 0);
				
				$_JLMS_PLUGINS->loadBotGroup('course');
				$plugin_result_array = $_JLMS_PLUGINS->trigger('onCourseJoinAttempt', array($user_info, $course_info));

				$allow_join = true;
				foreach ($plugin_result_array as $result) {
					if (!$result) $allow_join = false;
				}

				if ($allow_join) {
					$query = "INSERT INTO `#__lms_users_in_groups` ( course_id , user_id, enrol_time ) VALUES (".intval($id).",".intval($my->id).", '".JLMS_gmdate()."')";
					$JLMS_DB->setQuery($query);					
					if( $JLMS_DB->query() ) 
					{
						$course = new stdClass();
						$course->course_alias = '';
						$course->course_name = '';			

						$query = "SELECT course_name, name_alias FROM #__lms_courses WHERE id = '".$id."'";
						$JLMS_DB->setQuery( $query );
						$course = $JLMS_DB->loadObject();			

						$e_params['user_id'] = $my->id;
						$e_params['course_id'] = $id;					
						$e_params['markers']['{email}'] = $my->email;	
						$e_params['markers']['{name}'] = $my->name;										
						$e_params['markers']['{username}'] = $my->username;
						$e_params['markers']['{coursename}'] = $course->course_name;
						$e_params['markers']['{courselink}'] = JLMSEmailRoute("index.php?option=com_joomla_lms&Itemid=$Itemid&task=details_course&id=$id");
						$e_params['markers_nohtml']['{courselink}'] = $e_params['markers']['{courselink}'];
						$e_params['markers']['{courselink}'] = '<a href="'.$e_params['markers']['{courselink}'].'">'.$e_params['markers']['{courselink}'].'</a>';
						$e_params['action_name'] = 'OnSelfEnrolmentIntoFreeCourse';

						$_JLMS_PLUGINS->loadBotGroup('emails');
						$plugin_result_array = $_JLMS_PLUGINS->trigger('OnSelfEnrolmentIntoFreeCourse', array (& $e_params));
					}
					
					$_JLMS_PLUGINS->loadBotGroup('user');
					$_JLMS_PLUGINS->trigger('onCourseJoin', array($user_info));
	
					$msg = str_replace('#COURSENAME#', $course->course_name, _JLMS_CONGRATULATIONS);
				} else {
//					$msg = str_replace('#COURSENAME#', $course->course_name, _JLMS_CONGRATULATIONS);
					$msg = _JLMS_ADDED_TO_QUEUE;
				}

				$JLMS_SESSION->set('joomlalms_sys_message',$msg);
				$JLMS_SESSION->set('joomlalms_just_joined',1);
				JLMSRedirect (sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$id"));
			} else {

				$msg = 'Sorry user limit is exceeded, please contact your LMS administrator.';
				$JLMS_SESSION->set('joomlalms_sys_message',$msg);

				JLMSRedirect (sefRelToAbs("index.php?option=$option&amp;task=courses&amp;Itemid=$Itemid"));
			}
		}
		else{
			JLMSRedirect (sefRelToAbs("index.php?option=$option&amp;task=courses&amp;Itemid=$Itemid"));
		}
	}else{
		JLMSRedirect (sefRelToAbs("index.php?option=$option&amp;task=courses&amp;Itemid=$Itemid"));
	}
}
?>