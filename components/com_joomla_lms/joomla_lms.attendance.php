﻿<?php
/**
* joomla_lms.attendance.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );
require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.attendance.html.php");

	global $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id',0);
	$pathway = array();
	$pathway[] = array('name' => _JLMS_PATHWAY_HOME, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"), 'is_home' => true);
	$pathway[] = array('name' => $JLMS_CONFIG->get('course_name'), 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$course_id"), 'is_course' => true);
	$pathway[] = array('name' => _JLMS_TOOLBAR_ATTEND, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=attendance&amp;id=$course_id"), 'is_home' => false);
	JLMSAppendPathWay($pathway);

$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) ); 
$task 	= mosGetParam( $_REQUEST, 'task', '' );

if( $task != 'at_export' ) 
{
	JLMS_ShowHeading();
}

switch ($task) {
	case 'attendance':			JLMS_showAttendance( $id, $option );	break;
	case 'at_uchange':
	case 'at_dateschange':
	case 'at_periodchange':
	case 'at_change':			JLMS_changeAT( $id, $option );			break;
	case 'at_userattend':		JLMS_showUserAT( $id, $option );		break;
	case 'at_pre_export':		JLMS_showExportAT( $id, $option );		break;
	case 'at_export':			JLMS_ExportAT( $id, $option );			break;
}
function JLMS_ExportAT( $id, $option ) {
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$JLMS_SESSION = JLMSFactory::getSession();
	$Itemid = $JLMS_CONFIG->get('Itemid');
	$db = & JLMSFactory::getDB();
	$joomla_user_my = JLMSFactory::getUser();
	$my_id = $joomla_user_my->get('id');
	$JLMS_ACL = JLMSFactory::getACL();
	
	if ($id && ($JLMS_ACL->CheckPermissions('attendance', 'manage'))) {
		$at_date = mosGetParam( $_REQUEST, 'at_date', $JLMS_SESSION->get('at_date', date('Y-m-d')) );
		$at1_date = mosGetParam( $_REQUEST, 'at1_date', $JLMS_SESSION->get('at1_date', date('Y-m-d')) );
		$filt_group = intval( mosGetParam( $_GET, 'filt_group', $JLMS_SESSION->get('filt_group', 0) ) );
		
		$JLMS_SESSION->set('at_date', $at_date);
		$JLMS_SESSION->set('at1_date', $at1_date);
		$JLMS_SESSION->set('filt_group', $filt_group);
		
		$at_date = JLMS_dateToDB($at_date);
		$at1_date = JLMS_dateToDB($at1_date);
		
		if (strtotime($at1_date) > strtotime($at_date)) { $at_date = $at1_date; }

		$groups_mode = $JLMS_CONFIG->get('use_global_groups', 1);

		$at_user_ids = array();
		$at_users = mosGetParam( $_REQUEST, 'at_users', array(0) );
		if ( is_array($at_users) && count($at_users) ) {

			/// checks for assigned groups (begin)
			$assigned_groups_only = $JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only');
			$assigned_only_users = array();
			if($assigned_groups_only) {
				$groups_where_admin_manager = JLMS_ACL_HELPER::GetAssignedGroups($my_id, $id);
				if(count($groups_where_admin_manager)) {
					if(JLMS_ACL_HELPER::GetCountAssignedGroups($my_id, $id) == 1) {	
						$filt_group = $groups_where_admin_manager[0];
					}
				}
				if(count($groups_where_admin_manager)) {
					$groups_where_admin_manager = implode(',', $groups_where_admin_manager);
					if($groups_where_admin_manager != '') {
						$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE (group_id IN ($groups_where_admin_manager) OR subgroup1_id IN ($groups_where_admin_manager))"
							. ($filt_group ? ("\n AND group_id = '".$filt_group."'") : '')
						;
						$db->setQuery($query);
						$members = JLMSDatabaseHelper::loadResultArray();
					}
	
					$users_where_ceo_parent = array();
					if($JLMS_ACL->_role_type == 3) {
						$query = "SELECT user_id FROM `#__lms_user_parents` WHERE parent_id = '".$my_id."'"
						;
						$db->setQuery($query);
						$users_where_ceo_parent = JLMSDatabaseHelper::loadResultArray();
						//$members = array_merge($members, $users_where_ceo_parent);
					}
	
					if($members != "'0'" && count($users_where_ceo_parent)) {
						$members = array_merge($members, $users_where_ceo_parent);
					}
					elseif(count($users_where_ceo_parent)) {
						$members = $users_where_ceo_parent;
					}
					if (is_array($members)) {
						$members_array = $members;
						$members = implode(',', $members);
					}
					if($members == '') {
						$members = "'0'";
					}
				}
				else {
					$groups_where_admin_manager = "'0'";
				}
				$users = JLMS_getCourseStudentsListLimited( $id, $members_array, $filt_group );
				$new_at_users = array();
				foreach ($at_users as $at_user) {
					if (!$at_user) {
						$new_at_users[] = $at_user;
					} elseif (in_array($at_user, $users)) {
						$new_at_users[] = $at_user;
					}
				}
				unset($at_users);
				$at_users = & $new_at_users;
				if (!(count($users))) {
					$assigned_only_users = array();
					$assigned_only_users[] = 0;
				} else {
					foreach($users as $users_item) {
						$assigned_only_users[] = $users_item->id;
					}
				}
			}
			/// checks for assigned groups (end)

			if (isset($at_users[0]) && ($at_users[0] == 0) ) {
				$query = "SELECT a.user_id FROM #__lms_users_in_groups as a".( ($groups_mode && $filt_group) ? ", #__lms_users_in_global_groups as d" : '')." WHERE a.course_id = '".$id."'"
				. ($filt_group ? ( $groups_mode ? ("\n AND a.user_id = d.user_id AND d.group_id = '".$filt_group."'") : ("\n AND a.group_id = '".$filt_group."'")) : '')
				. ($assigned_groups_only ? ("\n AND a.user_id IN ( ".implode(',', $assigned_only_users)." )") : '')
				;
			} else {
				$ids = implode(',',$at_users);
				$query = "SELECT a.user_id FROM #__lms_users_in_groups as a".( ($groups_mode && $filt_group) ? ", #__lms_users_in_global_groups as d" : '')." WHERE a.course_id = '".$id."'"
				. ($filt_group ? ( $groups_mode ? ("\n AND a.user_id = d.user_id AND d.group_id = '".$filt_group."'") : ("\n AND a.group_id = '".$filt_group."'")) : '')
				. "\n AND a.user_id IN ( ".$ids." )"
				;
			}
			$db->SetQuery( $query );
			$at_user_ids = JLMSDatabaseHelper::LoadResultArray();

			if (count($at_user_ids)) {

				$uids_str = implode(',',$at_user_ids);
				$query = "SELECT * FROM #__lms_attendance WHERE course_id = '".$id."' AND at_date >= '".$at1_date."' AND at_date <= '".$at_date."' AND user_id IN ($uids_str)"
				. "\n ORDER BY at_date";
				$db->SetQuery( $query );
				$at_rows = $db->LoadObjectList();

				$query = "SELECT * FROM #__lms_attendance_periods";
				$db->SetQuery( $query );
				$per_rows = $db->LoadObjectList();

				$query = "SELECT b.*, u.username, u.name, u.email, c.ug_name"
				. "\n FROM #__lms_users_in_groups as b"
				. "\n LEFT JOIN #__lms_usergroups as c ON b.group_id = c.id AND c.course_id = '".$id."',"
				. "\n #__users as u"
				. "\n WHERE b.course_id = '".$id."' AND b.user_id = u.id"
				. "\n AND b.user_id IN ($uids_str)"
				. "\n ORDER BY u.username, u.name";
				$db->SetQuery( $query );
				$rows = $db->LoadObjectList();

				$at_dates = array();
				foreach ($at_rows as $atrow) {
					if (!in_array($atrow->at_date,$at_dates)) {
						$at_dates[] = $atrow->at_date;
					}
				}
				$rows_g = array();
				$t = 0;
				while ($t < count($at_dates)) {
					$i = 0;
					while ($i < count($rows)) {
						$j = 0;
						$p = array();
						foreach ($per_rows as $per_row) {
							$pp = new stdClass();
							$pp->period_id = $per_row->id;
							$pp->period_begin = $per_row->period_begin;
							$pp->period_end = $per_row->period_end;
							$pp->at_status = 0;
							$p[] = $pp;
						}
						while ($j < count($at_rows)) {
							if ($at_rows[$j]->user_id == $rows[$i]->user_id && $at_rows[$j]->at_date == $at_dates[$t]) {
								$k = 0;
								while ($k < count($p)) {
									if ($p[$k]->period_id == $at_rows[$j]->at_period) {
										$p[$k]->at_status = $at_rows[$j]->at_status;
									}
									$k ++;
								}
							}
							$j ++;
						}
						$rows[$i]->attendance = $p;
						$i ++;
					}
					$rrr = new stdClass();
					$rrr->rows = $rows;
					$rrr->at_date = $at_dates[$t];
					$rows_g[] = $rrr;
					$t ++;
				}
				
				$file_name = 'attendance.csv';
				$v_date = date("Y-m-d H:i:s");
				if (preg_match('/Opera(\/| )([0-9].[0-9]{1,2})/', $_SERVER['HTTP_USER_AGENT'])) {
					$UserBrowser = "Opera";
				}
				elseif (preg_match('/MSIE ([0-9].[0-9]{1,2})/', $_SERVER['HTTP_USER_AGENT'])) {
					$UserBrowser = "IE";
				} else {
					$UserBrowser = '';
				}
				$mime_type = ($UserBrowser == 'IE' || $UserBrowser == 'Opera') ? 'application/octetstream' : 'application/octet-stream';
				header('Content-Type: ' . $mime_type );
				header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
				if ($UserBrowser == 'IE') {
					header('Content-Disposition: attachment; filename="' . $file_name . '";');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Pragma: public');
				} else {
					header('Content-Disposition: attachment; filename="' . $file_name . '";');
					header('Pragma: no-cache');
				}
				
				echo "\xEF\xBB\xBF"; //UTF-8 BOM
				echo _JLMS_GROUP.','._JLMS_ROLE_STU.',,';
				foreach ($per_rows as $per_row) {
					echo substr($per_row->period_begin,0,5).' - '.substr($per_row->period_end,0,5).',';
				}
				echo "\n";
				$l_at = '';
				foreach ($rows_g as $rowg) {
					echo ',,'.$rowg->at_date."\n";
					foreach ($rowg->rows as $row) {
						echo $row->ug_name.','.$row->username.",,";
						foreach ($row->attendance as $row2) {
							echo ($row2->at_status?_JLMS_YES_ALT_TITLE:_JLMS_NO_ALT_TITLE).',';
						}
						echo "\n";
					}
				}
				exit();
			}
		}
	}
	JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=at_pre_export&id=$id"));
}
function JLMS_showExportAT( $id, $option ) {
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$JLMS_SESSION = JLMSFactory::getSession();
	$user = JLMSFactory::getUser();
	$db = & JLMSFactory::getDB();
	$my_id = $user->get('id');
	$Itemid = $JLMS_CONFIG->get('Itemid');
	$JLMS_ACL = JLMSFactory::getACL();

	if ($id && ($JLMS_ACL->CheckPermissions('attendance', 'manage'))) {
		$at_date = mosGetParam( $_REQUEST, 'at_date', $JLMS_SESSION->get('at_date', date('Y-m-d')) );
		$at1_date = mosGetParam( $_REQUEST, 'at1_date', $JLMS_SESSION->get('at1_date', date('Y-m-d')) );
		
		$filt_group = intval( mosGetParam( $_GET, 'filt_group', $JLMS_SESSION->get('filt_group', 0) ) );
		$filt_subgroup = 0;//TODO: fix this
		
		$JLMS_SESSION->set('at_date', $at_date);
		$JLMS_SESSION->set('at1_date', $at1_date);
		$JLMS_SESSION->set('filt_group', $filt_group);
		
		$at_date = JLMS_dateToDB($at_date);
		$at1_date = JLMS_dateToDB($at1_date);
		
		if (strtotime($at1_date) > strtotime($at_date)) { $at_date = $at1_date; }
		
		$members = '0';
		$members_array = array(0);
		$assigned_groups_only = $JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only');
		if($assigned_groups_only) {
			$groups_where_admin_manager = JLMS_ACL_HELPER::GetAssignedGroups($my_id, $id);
			if(count($groups_where_admin_manager)) {
				if(JLMS_ACL_HELPER::GetCountAssignedGroups($my_id, $id) == 1) {	
					$filt_group = $groups_where_admin_manager[0];
				}
			}
			if(count($groups_where_admin_manager)) {
				$groups_where_admin_manager = implode(',', $groups_where_admin_manager);
				if($groups_where_admin_manager != '') {
					$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE (group_id IN ($groups_where_admin_manager) OR subgroup1_id IN ($groups_where_admin_manager))"
						. ($filt_group ? ("\n AND group_id = '".$filt_group."'") : '')
						. ($filt_subgroup ? ("\n AND subgroup1_id = '".$filt_subgroup."'") : '')
					;
					$db->setQuery($query);
					$members = JLMSDatabaseHelper::loadResultArray();
				}

				$users_where_ceo_parent = array();
				if($JLMS_ACL->_role_type == 3) {
					$query = "SELECT user_id FROM `#__lms_user_parents` WHERE parent_id = '".$my_id."'"
					;
					$db->setQuery($query);
					$users_where_ceo_parent = JLMSDatabaseHelper::loadResultArray();
					//$members = array_merge($members, $users_where_ceo_parent);
				}

				if($members != "'0'" && count($users_where_ceo_parent)) {
					$members = array_merge($members, $users_where_ceo_parent);
				}
				elseif(count($users_where_ceo_parent)) {
					$members = $users_where_ceo_parent;
				}
				if (is_array($members)) {
					$members_array = $members;
					$members = implode(',', $members);
				}
				if($members == '') {
					$members = "'0'";
				}
			}
			else {
				$groups_where_admin_manager = "'0'";
			}
		}

		$lists = array();
		$lists['at1_date'] = $at1_date;
		$lists['at_date'] = $at_date;
		if ($assigned_groups_only) {
			$users = JLMS_getCourseStudentsListLimited( $id, $members_array, $filt_group );
		} else {
			$users = JLMS_getCourseStudentsList( $id, $filt_group );
		}
		$uu = array();
		$uuu = new stdClass(); $uuu->id = 0; $uuu->username = _JLMS_ATT_FILTER_ALL_USERS;
		$uu[] = $uuu;
		$users = array_merge($uu, $users);
		/*18 June 2007 - Fix of `id` attribute. (xHTML compliance)
		We wouldn't pass to 'SelectList()' array with numeric `id` attribute;*/
		$users_fixed = array();
		$user_ids = array();
		foreach ($users as $usertmp) {
			$uf = new stdClass();
			$uf->value = $usertmp->id;
			$uf->text = $usertmp->username;
			$users_fixed[] = $uf;
			$user_ids[] = $usertmp->id;
		}
		$lists['at_users'] = mosHTML::selectList($users_fixed, 'at_users[]', 'class="inputbox" style=\'width:340px\' size="7" multiple="multiple" ', 'value', 'text', null );

		if ($JLMS_CONFIG->get('use_global_groups', 1)) {
			$gl_group_ids = array();
			if (count($user_ids)) {
				$query = "SELECT distinct group_id FROM #__lms_users_in_global_groups WHERE user_id IN (".implode(',', $user_ids).")";
				$db->SetQuery($query);
				$gl_group_ids = JLMSDatabaseHelper::LoadResultArray();
			}
			if (!count($gl_group_ids)) {
				$gl_group_ids[] = 0;
			}
			$gl_group_ids_str = implode(',', $gl_group_ids);
			$query = "SELECT * FROM #__lms_usergroups WHERE course_id = 0 AND id IN(".$gl_group_ids_str.")";
		} else {
			$query = "SELECT * FROM #__lms_usergroups WHERE course_id = '".$id."'";
		}
		$db->SetQuery( $query );
		$ugs = array();
		$ug = new stdClass(); $ug->id = 0; $ug->ug_name = _JLMS_ATT_FILTER_ALL_GROUPS; $ugs[] = $ug;
		$ugs = array_merge( $ugs, $db->LoadObjectList() );
		$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=at_pre_export&amp;id=$id";
		$link = $link ."&amp;filt_group=".JLMS_SELECTED_INDEX_MARKER;
		$link = processSelectedIndexMarker( $link );
		$lists['at_groups'] = mosHTML::selectList($ugs, 'at_groups', 'class="inputbox" style=\'width:340px\' size="1" onchange="document.location.href=\''. $link .'\';"', 'id', 'ug_name', $filt_group );

		JLMS_attendance_html::showExportAT( $id, $option, $lists );
	} else {
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=attendance&id=$id"));
	}
}
function JLMS_showAttendance( $id, $option) {
	global $my, $JLMS_DB, $Itemid, $JLMS_SESSION, $JLMS_CONFIG;
	
	$usertype = JLMS_GetUserType($my->id, $id);
	$limit = intval( mosGetParam( $_REQUEST, 'limit', $JLMS_SESSION->get('list_limit', $JLMS_CONFIG->get('list_limit')) ) );
	$JLMS_SESSION->set('list_limit', $limit);
	$limitstart = intval( mosGetParam( $_REQUEST, 'limitstart', 0 ) );
	$at_date = mosGetParam( $_REQUEST, 'at_date', $JLMS_SESSION->get('at_date', date('Y-m-d')) );
	
	$JLMS_SESSION->set('at_date', $at_date);
	
	$at_date = JLMS_dateToDB($at_date);
	
	$dateArr = getdate(strtotime($at_date));				
	$y=$dateArr['year']; $m=$dateArr['mon']; $d=$dateArr['mday'];
	$isValid = checkdate($m, $d, $y);		
	
	if (!$isValid) {		
		$at_date = date('Y-m-d');
	}
	
	$JLMS_ACL = JLMSFactory::getACL();
	
//	$view_all_course_categories = $JLMS_ACL->CheckPermissions('advanced', 'view_all_course_categories');
	$assigned_groups_only = $JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only');
	
	//echo $view_all_users;
	
	$groups_mode = $JLMS_CONFIG->get('use_global_groups', 1);

	if ($id && ($JLMS_ACL->CheckPermissions('attendance', 'manage'))) {
	
		$filt_group = intval( mosGetParam( $_GET, 'filt_group', $JLMS_SESSION->get('filt_group', 0) ) );
		$filt_subgroup = intval( mosGetParam( $_GET, 'filt_subgroup', $JLMS_SESSION->get('filt_subgroup', 0) ) );
		
		if(!$filt_group) {
			$filt_subgroup = 0;
		}
		
		$JLMS_SESSION->set('filt_group', $filt_group);
		$JLMS_SESSION->set('filt_subgroup', $filt_subgroup);
		
		$members = "'0'";
		if($assigned_groups_only) {
			
			$groups_where_admin_manager = JLMS_ACL_HELPER::GetAssignedGroups($my->id, $id);
		
			if(count($groups_where_admin_manager)) {
				if(JLMS_ACL_HELPER::GetCountAssignedGroups($my->id, $id) == 1) {	
					$filt_group = $groups_where_admin_manager[0];
				}
			}
			
			if(count($groups_where_admin_manager)) {
			$groups_where_admin_manager = implode(',', $groups_where_admin_manager);
				if($groups_where_admin_manager != '') {
					$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE (group_id IN ($groups_where_admin_manager) OR subgroup1_id IN ($groups_where_admin_manager))"
						. ($filt_group ? ("\n AND group_id = '".$filt_group."'") : '')
						. ($filt_subgroup ? ("\n AND subgroup1_id = '".$filt_subgroup."'") : '')
					;
					
					$JLMS_DB->setQuery($query);
					$members = JLMSDatabaseHelper::loadResultArray();
				}
					
				$users_where_ceo_parent = array();
				if($JLMS_ACL->_role_type == 3) {
					$query = "SELECT user_id FROM `#__lms_user_parents` WHERE parent_id = '".$my->id."'"
					;
					$JLMS_DB->setQuery($query);
					$users_where_ceo_parent = JLMSDatabaseHelper::loadResultArray();
					
					//$members = array_merge($members, $users_where_ceo_parent);
				}
					
				if($members != "'0'" && count($users_where_ceo_parent)) {
					$members = array_merge($members, $users_where_ceo_parent);
				}
				elseif(count($users_where_ceo_parent)) {
					$members = $users_where_ceo_parent;
				}
					
				$members = implode(',', $members);
				if($members == '') {
					$members = "'0'";
				}
			}
			else {
				$groups_where_admin_manager = "'0'";
			}
		}	
		
		if ($groups_mode && $filt_group) {
//			$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE group_id = $filt_group";
//			$JLMS_DB->setQuery($query);
//			$uid = JLMSDatabaseHelper::loadResultArray();
//			if (!$uid) $uid = array(-1);
			$query = "SELECT count(*) FROM #__lms_users_in_groups as b, #__lms_users_in_global_groups as d"
			. "\n WHERE b.course_id = '".$id."'"
			. "\n AND d.user_id = b.user_id"
			. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
			. ($filt_group ? ("\n AND d.group_id = '".$filt_group."'") : '')
			. ($filt_subgroup ? ("\n AND d.subgroup1_id = '".$filt_subgroup."'") : '')
//			. "\n GROUP BY d.user_id"
			;
		} 
		else {
			$query = "SELECT count(*) FROM #__lms_users_in_groups as b, #__users as a"
			. "\n WHERE b.course_id = '".$id."' AND a.id = b.user_id"
			. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
			;
		}
		$JLMS_DB->SetQuery( $query );
		$total = $JLMS_DB->LoadResult();
		require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
		$pageNav = new JLMSPageNav( $total, $limitstart, $limit );
		
		if ($groups_mode && $filt_group) {
			$query = "SELECT b.*, u.username, u.name, u.email, c.ug_name"
			. "\n FROM #__lms_users_in_groups as b"
			. "\n LEFT JOIN #__lms_usergroups as c ON b.group_id = c.id,"
			. "\n #__users as u, #__lms_users_in_global_groups as d"
			. "\n WHERE b.course_id = ".$id." AND b.user_id = u.id"
			. "\n AND d.user_id = b.user_id"
//			. ($filt_group ? ($JLMS_CONFIG->get('use_global_groups', 1) ? "\n AND b.user_id IN (".implode(',', $uid).")" : ("\n AND b.group_id = '".$filt_group."'")) : '')
			. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND d.user_id IN ($members)") :'')		
			. ($filt_group ? ("\n AND d.group_id = '".$filt_group."'") : '')
			. ($filt_subgroup ? ("\n AND d.subgroup1_id = '".$filt_subgroup."'") : '')
			. "\n LIMIT $pageNav->limitstart, $pageNav->limit";
		}
		else {
			$query = "SELECT b.*, u.username, u.name, u.email, c.ug_name"
			. "\n FROM #__lms_users_in_groups as b"
			. "\n LEFT JOIN #__lms_usergroups as c ON b.group_id = c.id AND c.course_id = ".$id.","
			. "\n #__users as u"
			. "\n WHERE b.course_id = ".$id." AND b.user_id = u.id"
//			. ($filt_group ? ($JLMS_CONFIG->get('use_global_groups', 1) ? "\n AND b.user_id IN (".implode(',', $uid).")" : ("\n AND b.group_id = '".$filt_group."'")) : '')
			. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')			
			. "\n ORDER BY u.username, u.name"
			. "\n LIMIT $pageNav->limitstart, $pageNav->limit";
		}
		
		$JLMS_DB->SetQuery( $query );
		$rows = $JLMS_DB->LoadObjectList();
		
		$uids = array();
		foreach ($rows as $row) {
			$uids[] = $row->user_id;
		}
		if ($JLMS_CONFIG->get('use_global_groups', 1)) {
			$groups_array = array();
			if (count($uids)) {
				$query = "SELECT ug.user_id, g.ug_name"
				."\n FROM #__lms_users_in_global_groups AS ug, #__lms_usergroups AS g"
				."\n WHERE ug.group_id=g.id AND ug.user_id IN (".implode(',',$uids).")"
//				. (!$view_all_users ? ("\n AND ug.user_id IN ($members)") :'')			
				;
				$JLMS_DB->setQuery($query);
				$groups_array_raw = $JLMS_DB->loadObjectList();
				
				$groups_array = array();
				foreach ($groups_array_raw as $object) {
					isset($groups_array[$object->user_id]) ? $groups_array[$object->user_id] .= '<br />'.$object->ug_name : $groups_array[$object->user_id] = $object->ug_name;				
				}
				foreach ($rows as $key => $object) {
					isset($groups_array[$object->user_id]) ? $rows[$key]->ug_name = $groups_array[$object->user_id] : $rows[$key]->ug_name = '';
				}
			}
		}		
		$at_rows = array();
		if (count($uids)) {
			$uids_str = implode(',',$uids);
			$query = "SELECT * FROM #__lms_attendance WHERE course_id = ".$id." AND at_date = '".$at_date."' AND user_id IN ($uids_str)"
//			. (!$view_all_users ? ("\n AND user_id IN ($members)") :'')			
			;
			$JLMS_DB->SetQuery( $query );
			$at_rows = $JLMS_DB->LoadObjectList();
		}
		$query = "SELECT * FROM #__lms_attendance_periods";
		$JLMS_DB->SetQuery( $query );
		$per_rows = $JLMS_DB->LoadObjectList();
		$i = 0;
		while ($i < count($rows)) {
			$j = 0;
			$p = array();
			foreach ($per_rows as $per_row) {
				$pp = new stdClass();
				$pp->period_id = $per_row->id;
				$pp->at_status = 0;
				$p[] = $pp;
			}
			while ($j < count($at_rows)) {
				if ($at_rows[$j]->user_id == $rows[$i]->user_id) {
					$k = 0;
					while ($k < count($p)) {
						if ($p[$k]->period_id == $at_rows[$j]->at_period) {
							$p[$k]->at_status = $at_rows[$j]->at_status;
						}
						$k ++;
					}
				}
				$j ++;
			}
			$rows[$i]->attendance = $p;
			$rows[$i]->is_selected = 0;
			$i ++;
		}
		$box = 0;
		if ($JLMS_SESSION->has('at_cid')) {
			$cid = $JLMS_SESSION->get('at_cid');
			if (is_array($cid) && !empty($cid)) {
				$i = 0;
				while ($i < count($cid)) {
					$j = 0;
					while ($j < count($rows)) {
						if ($cid[$i] == $rows[$j]->user_id) {
							$rows[$j]->is_selected = 1;
							$box ++;
							break;
						}
						$j ++;
					}
					$i ++;
				}
			}
			$JLMS_SESSION->clear('at_cid');
		}
		$lists = array();
//		$g_items = array();
//		$g_items[] = mosHTML::makeOption(0, _JLMS_ATT_FILTER_ALL_GROUPS);
//		if ($JLMS_CONFIG->get('use_global_groups', 1)) {
//			$query = "SELECT user_id FROM #__lms_users_in_groups WHERE course_id = $id"
//			. (!$view_all_users ? ("\n AND user_id IN ($members)") :'')
//			;
//			$JLMS_DB->setQuery($query);
//			$cid = JLMSDatabaseHelper::loadResultArray();
//			if (!$cid) $cid = array(-1);
//			$query = "SELECT group_id FROM #__lms_users_in_global_groups WHERE user_id IN (".implode(',', $cid).")"
//			. (!$view_all_users ? ("\n AND user_id IN ($members)") :'')
//			;
//			$JLMS_DB->setQuery($query);
//			$gid = JLMSDatabaseHelper::loadResultArray();
//			if (!$gid) $gid = array(-1);
//			$query = "SELECT id AS value, ug_name AS text FROM #__lms_usergroups WHERE id IN (".implode(',', $gid).") AND course_id = 0";//course id check just in case))
//			$JLMS_DB->setQuery($query);
//			$groups = $JLMS_DB->loadObjectList();
//		} else {
//			$query = "SELECT distinct a.id as value, a.ug_name as text FROM #__lms_usergroups as a, #__lms_users_in_groups as b"
//			. "\n WHERE a.course_id = '".$id."' AND b.group_id = a.id ORDER BY a.ug_name";
//			$JLMS_DB->SetQuery( $query );
//			$groups = $JLMS_DB->LoadObjectList();
//		}
//		$g_items = array_merge($g_items, $groups);
//		$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=attendance&amp;id=$id";
//		$link = $link ."&amp;filt_group=' + this.options[selectedIndex].value + '";
//		$lists['filter'] = mosHTML::selectList($g_items, 'filt_group', 'class="inputbox" style="width:250px;" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filt_group );
//
//		if($filt_group) {
//			$g_items = array();
//			$g_items[] = mosHTML::makeOption(0, _JLMS_FILTER_ALL_SUBGROUPS);
//
//			if( ($JLMS_ACL->_role_type == 2 || $JLMS_ACL->_role_type == 4) && $JLMS_CONFIG->get('use_global_groups', 1)) {
//				$query = "SELECT distinct a.id as value, a.ug_name as text"
//				. "\n FROM #__lms_usergroups as a"
//				. "\n WHERE a.owner_id = 0 AND a.course_id = 0"
//				. "\n AND a.parent_id = $filt_group"
//				. (!$view_all_users ? ("\n AND a.parent_id IN ($groups_where_admin_manager)") :'')
//				. "\n ORDER BY a.ug_name"
//				;
//				$JLMS_DB->SetQuery( $query );
//				$sbugroups = $JLMS_DB->LoadObjectList();
//				
//				if(count($sbugroups)) {
//					$g_items = array_merge($g_items, $sbugroups);
//					$link = "index.php?option=$option&amp;Itemid=$Itemid&task=attendance&amp;id=$id";
//					$link = $link ."&amp;filt_group=".$filt_group."&amp;filt_subgroup='+this.options[selectedIndex].value+'";
//					$link = sefRelToAbs( $link );
//					$link = str_replace('%5C%27',"'", $link);$link = str_replace('%5B',"[", $link);$link = str_replace('%5D',"]", $link);$link = str_replace('%20',"+", $link);$link = str_replace("\\\\\\","", $link);$link = str_replace('%27',"'", $link);
//					$lists['filter3'] = mosHTML::selectList($g_items, 'filt_subgroup', 'class="inputbox" style="width:250px;" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filt_subgroup );
//				}
//			}
//		}
		
		//filer groups
		if ($JLMS_CONFIG->get('use_global_groups', 1)) {
			
			$g_items = array();
			$g_items[] = mosHTML::makeOption(0, _JLMS_ATT_FILTER_ALL_GROUPS);
			
			$query = "SELECT distinct a.id as value, a.ug_name as text"
			. "\n FROM #__lms_usergroups as a"
			. "\n WHERE a.course_id = 0"
			. "\n AND a.parent_id = 0"
			. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND a.id IN ($groups_where_admin_manager)") :'')			
			. "\n ORDER BY a.ug_name"
			;
		
			$JLMS_DB->SetQuery( $query );
			$groups = $JLMS_DB->LoadObjectList();
			
			$g_items = array_merge($g_items, $groups);
			$link = "index.php?option=$option&amp;Itemid=$Itemid&task=attendance&amp;id=$id";
			$link = $link ."&amp;filt_group=".JLMS_SELECTED_INDEX_MARKER;
			$link = processSelectedIndexMarker( $link );
			$lists['filter'] = mosHTML::selectList($g_items, 'filt_group', 'class="inputbox" style="width:250px;" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filt_group );
			
			if($filt_group) {
				$g_items = array();
				$g_items[] = mosHTML::makeOption(0, _JLMS_FILTER_ALL_SUBGROUPS);

				$query = "SELECT distinct a.id as value, a.ug_name as text"
				. "\n FROM #__lms_usergroups as a"
				. "\n WHERE a.course_id = 0"
				. "\n AND a.parent_id = $filt_group"
				. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND a.parent_id IN ($groups_where_admin_manager)") :'')				
				. "\n ORDER BY a.ug_name"
				;
				$JLMS_DB->SetQuery( $query );
				$sbugroups = $JLMS_DB->LoadObjectList();
				
				if(count($sbugroups)) {
					$g_items = array_merge($g_items, $sbugroups);
					$link = "index.php?option=$option&amp;Itemid=$Itemid&task=attendance&id=$id";
					$link = $link ."&amp;filt_group=".$filt_group."&amp;filt_subgroup=".JLMS_SELECTED_INDEX_MARKER;
					$link = processSelectedIndexMarker( $link );
					$lists['filter3'] = mosHTML::selectList($g_items, 'filt_subgroup', 'class="inputbox" style="width:250px;" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filt_subgroup );
				}
			}
		}

		$lms_titles_cache = & JLMSFactory::getTitles();
		$lms_titles_cache->setArray('users', $rows, 'user_id', 'username');

		JLMS_attendance_html::showAttendance( $id, $option, $rows, $per_rows, $pageNav, $at_date, $box, $lists, true );
//	} elseif ($id && ($usertype == 6)) {
	} elseif ($id && $JLMS_ACL->_role_type == 3 && $JLMS_ACL->CheckPermissions('attendance', 'view')) {
		if(!$assigned_groups_only) {
			$query = "SELECT b.*, u.username, u.name, u.email, c.ug_name"
				. "\n FROM #__lms_users_in_groups as b"
				. "\n LEFT JOIN #__lms_usergroups as c ON b.group_id = c.id AND c.course_id = '".$id."',"
				. "\n #__users as u"
				. "\n WHERE b.course_id = '".$id."' AND b.user_id = u.id"
				. "\n ORDER BY u.username, u.name"
				;
		}
		else {
			$query = "SELECT b.*, u.username, u.name, u.email, c.ug_name"
			. "\n FROM #__lms_user_parents as p, #__lms_users_in_groups as b"
			. "\n LEFT JOIN #__lms_usergroups as c ON b.group_id = c.id AND c.course_id = '".$id."',"
			. "\n #__users as u"
			. "\n WHERE b.course_id = '".$id."' AND b.user_id = u.id AND b.user_id = p.user_id AND p.parent_id = '".$my->id."'"
			. "\n ORDER BY u.username, u.name"
			;
		}
		$JLMS_DB->SetQuery( $query );
		$rows = $JLMS_DB->LoadObjectList();

		$exist_user_ids = array();
		if(count($rows)){
			foreach($rows as $key=>$row){
				$exist_user_ids[] = $row->user_id;	
			}	
		}
		
		$staff_learners = ($JLMS_ACL->_role_type == 3 && isset($JLMS_ACL->_staff_learners))?$JLMS_ACL->_staff_learners:array();
		if(count($staff_learners)){
			$str_staff_learners = implode(",", $staff_learners);
			$str_exist_user_ids = implode(",", $exist_user_ids);
			$query = "SELECT distinct u.id as user_id, u.username, u.name, u.email" //, xxx as ug_name"
			. "\n FROM #__users as u, #__lms_users_in_groups as uig"
			. "\n WHERE 1"
			. "\n AND u.id = uig.user_id"
			. "\n AND uig.course_id = '".$id."'"
			. "\n AND u.id IN (".$str_staff_learners.")"
			.(count($exist_user_ids) ? "\n AND u.id NOT IN (".$str_exist_user_ids.")" : '')
			;
			$JLMS_DB->setQuery($query);
			$rows_ceo = $JLMS_DB->loadObjectList();
			
			$uids = array();
			foreach ($rows_ceo as $object) {
				$uids[] = $object->user_id;
			}
			$groups_array = array();
			if (count($uids)) {
				$query = "SELECT ug.user_id, g.ug_name, g.id AS group_id"
				."\n FROM #__lms_users_in_global_groups AS ug, #__lms_usergroups AS g"
				."\n WHERE ug.group_id=g.id AND ug.user_id IN (".implode(',',$uids).")"
				;
				$JLMS_DB->setQuery($query);
				$groups_array_raw = $JLMS_DB->loadObjectList();
				$groups_array = array();
				foreach ($groups_array_raw as $object) {
					isset($groups_array[$object->user_id]) ? $groups_array[$object->user_id] .= '<br />'.$object->ug_name : $groups_array[$object->user_id] = $object->ug_name;
				}
				foreach ($rows_ceo as $key => $object) {
					isset($groups_array[$object->user_id]) ? $rows_ceo[$key]->ug_name = $groups_array[$object->user_id] : $rows_ceo[$key]->ug_name = '';
				}
			}
			$rows = array_merge($rows, $rows_ceo);
		}
		
		$total = count($rows);
		require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
		$pageNav = new JLMSPageNav( $total, $limitstart, $limit );
		
		if(count($rows)){
			$tmp_rows = array();
			foreach($rows as $key=>$row){
				if(	$key >= $pageNav->limitstart && $key <= $pageNav->limit ){
					$tmp_rows[] = $row;	
				}
			}
			$rows = $tmp_rows;
		}
		
		
		$uids = array();
		foreach ($rows as $row) {
			$uids[] = $row->user_id;
		}
		$at_rows = array();
		if (count($uids)) {
			$uids_str = implode(',',$uids);
			$query = "SELECT * FROM #__lms_attendance WHERE course_id = '".$id."' AND at_date = '".$at_date."' AND user_id IN ($uids_str)";
			$JLMS_DB->SetQuery( $query );
			$at_rows = $JLMS_DB->LoadObjectList();
		}
		$query = "SELECT * FROM #__lms_attendance_periods";
		$JLMS_DB->SetQuery( $query );
		$per_rows = $JLMS_DB->LoadObjectList();
		$i = 0;
		while ($i < count($rows)) {
			$j = 0;
			$p = array();
			foreach ($per_rows as $per_row) {
				$pp = new stdClass();
				$pp->period_id = $per_row->id;
				$pp->at_status = 0;
				$p[] = $pp;
			}
			while ($j < count($at_rows)) {
				if ($at_rows[$j]->user_id == $rows[$i]->user_id) {
					$k = 0;
					while ($k < count($p)) {
						if ($p[$k]->period_id == $at_rows[$j]->at_period) {
							$p[$k]->at_status = $at_rows[$j]->at_status;
						}
						$k ++;
					}
				}
				$j ++;
			}
			$rows[$i]->attendance = $p;
			$rows[$i]->is_selected = 0;
			$i ++;
		}
		$box = 0;
		if ($JLMS_SESSION->has('at_cid')) {
			$cid = $JLMS_SESSION->get('at_cid');
			if (is_array($cid) && !empty($cid)) {
				$i = 0;
				while ($i < count($cid)) {
					$j = 0;
					while ($j < count($rows)) {
						if ($cid[$i] == $rows[$j]->user_id) {
							$rows[$j]->is_selected = 1;
							$box ++;
							break;
						}
						$j ++;
					}
					$i ++;
				}
			}
			$JLMS_SESSION->clear('at_cid');
		}
		$lists = array();
		$lists['filter'] = '';

		$lms_titles_cache = & JLMSFactory::getTitles();
		$lms_titles_cache->setArray('users', $rows, 'user_id', 'username');

		JLMS_attendance_html::showAttendance( $id, $option, $rows, $per_rows, $pageNav, $at_date, $box, $lists, false );
	} elseif ($id && $JLMS_ACL->CheckPermissions('attendance', 'view')) {
		JLMS_showUserAT_my( $my->id, $option, $id );
	} else {
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid"));
	}
}
function JLMS_showUserAT( $user_id, $option) {
	global $JLMS_DB, $my, $Itemid, $JLMS_SESSION, $JLMS_CONFIG;
	$course_id = intval( mosGetParam( $_REQUEST, 'course_id', 0 ) );
	$JLMS_ACL = JLMSFactory::getACL();

	$members = "'0'";
	$flag = 0;
	if($JLMS_ACL->_role_type == 2 || $JLMS_ACL->_role_type == 4) {
		if($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only')) {
			
			$groups_where_admin_manager = JLMS_ACL_HELPER::GetAssignedGroups($my->id, $course_id);					
			
			if(count($groups_where_admin_manager)) {
				if(JLMS_ACL_HELPER::GetCountAssignedGroups($my->id, $course_id) == 1) {	
					$filt_group = $groups_where_admin_manager[0];
				}
			}

			if(count($groups_where_admin_manager)) {
				$groups_where_admin_manager = implode(',', $groups_where_admin_manager);
					if($groups_where_admin_manager != '') {
						$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE (group_id IN ($groups_where_admin_manager) OR subgroup1_id IN ($groups_where_admin_manager))"
						;
						$JLMS_DB->setQuery($query);
						$members = JLMSDatabaseHelper::loadResultArray();
	
						if(in_array($user_id,$members)) {
							$flag = 1;
						}
					}
			}
			else {
				$groups_where_admin_manager = "'0'";
			}
		}
	}
	
	if($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') && !$flag) {
		$proverka_na_menedjera_gruppi = false;
	}
	else {
		$proverka_na_menedjera_gruppi = true;
	}
	
	if ($course_id && $user_id && ($JLMS_ACL->CheckPermissions('attendance', 'manage')) && $proverka_na_menedjera_gruppi) {
	//if ($course_id && $user_id && (JLMS_GetUserType($my->id, $course_id) == 1) && JLMS_isUserCourse($user_id, $course_id)) {
		$at_date = mosGetParam( $_REQUEST, 'at_date', $JLMS_SESSION->get('at_date', date('Y-m-d')) );
		$at_date = JLMS_dateToDB($at_date);
		$m = substr($at_date,5,2);
		$d = substr($at_date,8,2);
		$y = substr($at_date,0,4);
		if (!checkdate($m,$d,$y) || ($y.'-'.$m.'-'.$d != $at_date)) {
			$at_date = date('Y-m-d');
			$m = substr($at_date,5,2);
			$d = substr($at_date,8,2);
			$y = substr($at_date,0,4);
		}
		$JLMS_SESSION->set('at_date', $at_date);
		$wday = JDDayOfWeek(GregorianToJD($m, $d,$y), 0);
		$tdate = strtotime($at_date);
		
		$allow_dates = array();
		$r = $JLMS_CONFIG->get('attendance_days');
		$ar = unserialize($r);
		if ($JLMS_CONFIG->get('date_format_fdow', 1) == 1) {// Monday first
		  if ($wday == 0){$wday = 7;}
		  $mon_day_num = strtotime("-".($wday-1)." day", $tdate);
		  $sun_day_num = strtotime("+".(7-$wday)." day", $tdate);
		} else {// Sunday first
			$mon_day_num = strtotime("-".($wday)." day", $tdate);
			$sun_day_num = strtotime("+".(6-$wday)." day", $tdate);
		}
		$i = 0;
		while ($i < 7) {
			$mon_day_num2 = strtotime("+".($i)." day", $mon_day_num);
			if (in_array(($i+1),$ar)) {
				$allow_dates[] = date('Y-m-d', $mon_day_num2);
			}
			$i ++;
		}
		$at_rows = array();
		if (count($allow_dates)) {
			$ad_str = implode("','", $allow_dates);
			$query = "SELECT * FROM #__lms_attendance WHERE course_id = '".$course_id."' AND user_id = '".$user_id."' AND at_date IN ('$ad_str') ORDER BY at_date, at_period";
			$JLMS_DB->SetQuery( $query );
			$at_rows = $JLMS_DB->LoadObjectList();
		}
		$query = "SELECT * FROM #__lms_attendance_periods";
		$JLMS_DB->SetQuery( $query );
		$per_rows = $JLMS_DB->LoadObjectList();
		$attendance = array();
		foreach ($allow_dates as $ald) {
			$newrow = new stdClass();
			$newrow->at_date = $ald;
			$newrow->is_selected = 0;
			$at_stats = array();
			foreach ($per_rows as $perrow) {
				$nr = new stdClass();
				$nr->period_id = $perrow->id;
				$nr->at_status = 0;
				$i = 0;
				while ($i < count($at_rows)) {
					if ($at_rows[$i]->at_period == $perrow->id && $at_rows[$i]->at_date == $ald) {
						$nr->at_status = $at_rows[$i]->at_status;
						break;
					}
					$i ++;
				}
				$at_stats[] = $nr;
			}
			$newrow->at_stats = $at_stats;
			$attendance[] = $newrow;
		}
		$box = 0;
		if ($JLMS_SESSION->has('at_cid')) {
			$cid = $JLMS_SESSION->get('at_cid');
			if (is_array($cid) && !empty($cid)) {
				$i = 0;
				while ($i < count($cid)) {
					$j = 0;
					while ($j < count($attendance)) {
						if ($cid[$i] == $attendance[$j]->at_date) {
							$attendance[$j]->is_selected = 1;
							$box ++;
							break;
						}
						$j ++;
					}
					$i ++;
				}
			}
			$JLMS_SESSION->clear('at_cid');
		}
		$lists = array();
		$query = "SELECT name, username, email FROM #__users WHERE id = '".$user_id."'";
		$JLMS_DB->SetQuery( $query );
		$user_info = $JLMS_DB->LoadRow();
		$lists['username'] = isset($user_info[0])?$user_info[0]:'';
		$lists['name'] = isset($user_info[1])?$user_info[1]:'';
		$lists['email'] = isset($user_info[2])?$user_info[2]:'';
		$lists['user_id'] = &$user_id;
		$lists['box'] = &$box;
		$lists['course_id'] = &$course_id;
		$lists['at_date'] = &$at_date;
		$lists['filter'] = '';
		JLMS_attendance_html::showUserAttendance( $option, $attendance, $per_rows, $allow_dates, $lists );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=attendance&id=$course_id") );
	}
}
function JLMS_showUserAT_my( $user_id, $option, $course_id) {
	global $JLMS_DB, $my, $Itemid, $JLMS_SESSION, $JLMS_CONFIG;
	$no_script = intval( mosGetParam( $_REQUEST, 'no_script', 0 ) );
	if ($no_script) {
		$at_date = mosGetParam( $_REQUEST, 'date', $JLMS_SESSION->get('at_date', date('Y-m-d')) );
	} else {
		$at_date = mosGetParam( $_REQUEST, 'at_date', $JLMS_SESSION->get('at_date', date('Y-m-d')) );
	}
	$at_date = JLMS_dateToDB($at_date);
	$m = substr($at_date,5,2);
	$d = substr($at_date,8,2);
	$y = substr($at_date,0,4);
	if (!checkdate($m,$d,$y) || ($y.'-'.$m.'-'.$d != $at_date)) {
		$at_date = date('Y-m-d');
		$m = substr($at_date,5,2);
		$d = substr($at_date,8,2);
		$y = substr($at_date,0,4);
	}
	$JLMS_SESSION->set('at_date', $at_date);
	$wday = JDDayOfWeek(GregorianToJD($m, $d,$y), 0);
	$tdate = strtotime($at_date);
	if ($wday == 0){$wday = 7;}
	$allow_dates = array();
	$r = $JLMS_CONFIG->get('attendance_days');
	$ar = unserialize($r);
	if (!is_array($ar)) {
		$ar = array();
	}
	$mon_day_num = strtotime("-".($wday-1)." day", $tdate);
	$sun_day_num = strtotime("+".(7-$wday)." day", $tdate);
	$i = 0;
	while ($i < 7) {
		$mon_day_num2 = strtotime("+".($i)." day", $mon_day_num);
		if (in_array(($i+1),$ar)) {
			$allow_dates[] = date('Y-m-d', $mon_day_num2);
		}
		$i ++;
	}
	$at_rows = array();
	if (count($allow_dates)) {
		$ad_str = implode("','", $allow_dates);
		$query = "SELECT * FROM #__lms_attendance WHERE course_id = '".$course_id."' AND user_id = '".$user_id."' AND at_date IN ('$ad_str') ORDER BY at_date, at_period";
		$JLMS_DB->SetQuery( $query );
		$at_rows = $JLMS_DB->LoadObjectList();
	}
	$query = "SELECT * FROM #__lms_attendance_periods";
	$JLMS_DB->SetQuery( $query );
	$per_rows = $JLMS_DB->LoadObjectList();
	$attendance = array();
	foreach ($allow_dates as $ald) {
		$newrow = new stdClass();
		$newrow->at_date = $ald;
		$newrow->is_selected = 0;
		$at_stats = array();
		foreach ($per_rows as $perrow) {
			$nr = new stdClass();
			$nr->period_id = $perrow->id;
			$nr->at_status = 0;
			$i = 0;
			while ($i < count($at_rows)) {
				if ($at_rows[$i]->at_period == $perrow->id && $at_rows[$i]->at_date == $ald) {
					$nr->at_status = $at_rows[$i]->at_status;
					break;
				}
				$i ++;
			}
			$at_stats[] = $nr;
		}
		$newrow->at_stats = $at_stats;
		$attendance[] = $newrow;
	}
	$box = 0;
	if ($JLMS_SESSION->has('at_cid')) {
		$cid = $JLMS_SESSION->get('at_cid');
		if (is_array($cid) && !empty($cid)) {
			$i = 0;
			while ($i < count($cid)) {
				$j = 0;
				while ($j < count($attendance)) {
					if ($cid[$i] == $attendance[$j]->at_date) {
						$attendance[$j]->is_selected = 1;
						$box ++;
						break;
					}
					$j ++;
				}
				$i ++;
			}
		}
		$JLMS_SESSION->clear('at_cid');
	}
	$lists = array();
	$query = "SELECT username, name, email FROM #__users WHERE id = '".$user_id."'";
	$JLMS_DB->SetQuery( $query );
	$user_info = $JLMS_DB->LoadRow();
	$lists['username'] = isset($user_info[0])?$user_info[0]:'';
	$lists['name'] = isset($user_info[1])?$user_info[1]:'';
	$lists['email'] = isset($user_info[2])?$user_info[2]:'';
	$lists['user_id'] = &$user_id;
	$lists['box'] = &$box;
	$lists['course_id'] = &$course_id;
	$lists['at_date'] = &$at_date;
	$lists['filter'] = '';
	JLMS_attendance_html::showUserAttendance( $option, $attendance, $per_rows, $allow_dates, $lists );
}
function JLMS_changeAT( $course_id, $option ) {
	global $JLMS_DB, $my, $Itemid, $JLMS_SESSION, $JLMS_CONFIG, $task;
	
	//$user_id = $course_id;
	$user_id = intval( mosGetParam($_REQUEST, 'id', 0));
	$course_id = $JLMS_CONFIG->get('course_id');//( mosGetParam($_REQUEST, 'course_id', 0));
	if ($task == 'at_dateschange') {
		$course_id = intval( mosGetParam($_REQUEST, 'course_id', 0));

	}
	$JLMS_ACL = JLMSFactory::getACL();
	if ($course_id && ($JLMS_ACL->CheckPermissions('attendance', 'manage'))) {
	//if ($course_id && JLMS_GetUserType($my->id, $course_id) == 1) {
		$state = intval(mosGetParam($_REQUEST, 'state', 0));
		if ($state != 1) { $state = 0; }
		$cid = mosGetParam( $_REQUEST, 'cid', array(0) );
		$cid2 = intval(mosGetParam( $_REQUEST, 'cid2', 0 ));
		//var_dump($cid2);var_dump($course_id);var_dump($user_id);die;
		/*if (!is_array( $cid )) {
			$cid = array(0);
		} */
		if ($cid2) {
			$cid = array();
			$cid[] = $cid2;
		}
		if (!is_array( $cid ) || count( $cid ) < 1) {
			$action = 1 ? 'Publish' : 'Unpublish';
			echo "<script> alert('Select an item to $action'); window.history.go(-1);</script>\n";
			exit();
		}
		$ar_dates = $cid;
		$ar_dates_str = implode("','", $ar_dates);
		if ($task == 'at_dateschange') {
			$cid = array($user_id);
		}
		$cids = implode( ',', $cid );
		$period_id = intval(mosGetParam($_REQUEST, 'period_id', 0));
		$query = "SELECT id FROM #__lms_attendance_periods WHERE id = '".$period_id."'";
		$JLMS_DB->SetQuery( $query );
		$period_id = $JLMS_DB->LoadResult();
		if ($period_id) {
			if ($task == 'at_dateschange') {
				$r = $JLMS_CONFIG->get('attendance_days');
				$ar = unserialize($r);
				$ard = array();
				foreach ($ar_dates as $ad) {
					$m = substr($ad,5,2);
					$d = substr($ad,8,2);
					$y = substr($ad,0,4);
					$wday = JDDayOfWeek(GregorianToJD($m, $d,$y), 0);
					if ($wday == 0){$wday = 7;}
					if (in_array($wday,$ar) && checkdate($m,$d,$y) && ($y.'-'.$m.'-'.$d == $ad)) {
						$ard[] = $ad;
					}
				}
				if (!count($ard)) {
					JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=at_userattend&course_id=$course_id&at_date={$ar_dates[0]}&id=$user_id") );
				}
				$ar_dates = $ard;
			}
			$at_date = mosGetParam($_REQUEST, 'at_date', date('Y-m-d'));
			$at_date = date("Y-m-d", strtotime($at_date));
			
			$cc = false;
			if ($task != 'at_dateschange') {
				$r = $JLMS_CONFIG->get('attendance_days');
				$ar = unserialize($r);
				$m = substr($at_date,5,2);
				$d = substr($at_date,8,2);
				$y = substr($at_date,0,4);
				$wday = JDDayOfWeek(GregorianToJD($m, $d,$y), 0);
				if ($wday == 0){$wday = 7;}
				if (in_array($wday,$ar)) {
					$cc = true;
				}
				if (!checkdate($m,$d,$y) || ($y.'-'.$m.'-'.$d != $at_date)) {
					$cc = false;
				}
			}
			
			if ( ($cc && ($task != 'at_dateschange')) || ($task == 'at_dateschange') ) {
				//check rights
				$query = "SELECT user_id FROM #__lms_users_in_groups WHERE course_id = '".$course_id."' AND user_id IN ( $cids )";
				$JLMS_DB->SetQuery( $query );
				$cid = JLMSDatabaseHelper::LoadResultArray();
				
				if (!is_array( $cid )) {
					$cid = array(0);
				}
				$cids = implode( ',', $cid );
				if ($task == 'at_periodchange') {
					$JLMS_SESSION->set('at_cid', $cid);
				} elseif ($task == 'at_dateschange') {
					$JLMS_SESSION->set('at_cid', $ar_dates);
				} else {
					$JLMS_SESSION->clear('at_cid');
				}
				$query = "DELETE FROM #__lms_attendance WHERE course_id = '".$course_id."' AND at_period = '".$period_id."' AND user_id IN ( $cids ) AND at_date = '".$at_date."'";
				if ($task == 'at_dateschange') {
					$query = "DELETE FROM #__lms_attendance WHERE course_id = '".$course_id."' AND at_period = '".$period_id."' AND user_id = '".$user_id."' AND at_date IN('$ar_dates_str')";
				}
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				
				if ($state == 1) {
					$query = "INSERT INTO #__lms_attendance (course_id, user_id, at_period, at_date, at_status) VALUES ";
					if ($task == 'at_dateschange') {
						$t = 0;
						foreach($ar_dates as $adate) {
							$query .= "\n ($course_id, ".$user_id.", ".$period_id.", '".$adate."', $state)".(($t < (count($ar_dates) - 1))?',':'');
							$t ++;
						}
					} else {
						$t = 0;
						foreach($cid as $user_id) {
							$query .= "\n ($course_id, ".$user_id.", ".$period_id.", '".$at_date."', $state)".(($t < (count($cid) - 1))?',':'');
							$t ++;
						}
					}
					if ($t > 0) {
						$JLMS_DB->setQuery( $query );
						if (!$JLMS_DB->query()) {
							echo "<script> alert('".$JLMS_DB->getErrorMsg()."'); window.history.go(-1); </script>\n";
							exit();
						}
					}
				}// end if ($state)
			} else {
				$msg = _JLMS_ATT_CANNOT_CHANGE_RECORDS;
			}// end if ($cc.....
		}// end if ($period_id)
	}
	if ($task == 'at_dateschange') {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=at_userattend&course_id=$course_id&at_date=".JLMS_dateToDisplay($ar_dates[0])."&id=$user_id") );
	} elseif ($task == 'at_uchange') {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=at_userattend&course_id=$course_id&at_date=".JLMS_dateToDisplay($at_date)."&id={$cid[0]}") );
	} elseif ($task == 'at_change' || $task == 'at_periodchange') {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=attendance&id=$course_id&at_date=".JLMS_dateToDisplay($at_date)), $msg );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=attendance&id=$course_id") );
	}
}
?>