<?php
/**
* joomla_lms.docs.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) );
$cid2 	= intval( mosGetParam( $_REQUEST, 'cid2', 0 ) );
$task 	= mosGetParam( $_REQUEST, 'task', '' );
$task2 	= mosGetParam( $_REQUEST, 'task2', '' );
require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.docs.hlpr.php");
require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.docs.html.php");
	global $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id',0);
	$pathway = array();
	$pathway[] = array('name' => _JLMS_PATHWAY_HOME, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"), 'is_home' => true);
	$pathway[] = array('name' => $JLMS_CONFIG->get('course_name'), 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$course_id"), 'is_course' => true);
	$pathway[] = array('name' => _JLMS_TOOLBAR_DOCS, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=documents&amp;id=$course_id"), 'is_home' => false);

	JLMSAppendPathWay($pathway);

if ($task != 'save_doc') {
	JLMS_ShowHeading();
}

//echo $task;die;

switch ($task) {
################################		DOCUMENTS		##########################
	case 'documents':			JLMS_showDocuments( $option );			break;
	case 'new_document':		JLMS_editDocument( 0 , $option );		break;
	case 'new_folder':			JLMS_editDocument(0 , $option, 1);		break;
	case 'cancel_doc':			JLMS_cancelDocument( $option );			break;
	case 'edit_doc':
			$cid = mosGetParam( $_POST, 'cid', array(0) );
			if (!is_array( $cid )) { $cid = array(0); }
			if ($cid2) {
				$did = $cid2;
			} else {
				$did = $cid[0];
			}
			JLMS_editDocument( $did, $option );
	break;
	case 'save_doc':
			if ($task2 == 'add_perms') {
				JLMS_addDocPerms( $option );
			} elseif ($task2 == 'del_perms') {
				JLMS_delDocPerms( $option );
			} else {
				JLMS_saveDocument( $option );
			}
	break;
	case 'docs_choose_startup':	JLMS_chooseStartup( $option );			break;
	case 'docs_save_startup':	JLMS_saveStartup( $option );			break;
	case 'change_doc':			JLMS_changeDoc( $option );				break;
	case 'get_document':		JLMS_downloadDocument( $id, $option );	break;
	case 'doc_delete':			JLMS_doDeleteDocuments( $option );		break;
	case 'doc_orderdown':
	case 'doc_orderup':			JLMS_OrderDocuments( $option );			break;
	case 'doc_saveorder':		JLMS_saveorderDocuments($option);		break;
	case 'docs_view_zip':		JLMS_previewZipPack( $id, $option );	break;
	case 'docs_view_content':	JLMS_previewContent( $id, $option );	break;
	case 'documents_view_save':	JLMS_DocumentsSaveView( $id, $option ); break;
	case 'add_doclink':			JLMS_NewDocLink($option);				break;
	case 'save_doclink':		JLMS_SaveDocLink($option);				break;
}

/*
Custom permissions notes.
Ustanavlivayutsa na papku i deistvuyut tol'ko na vse vlogennye elementy
Prichem eta papka otobragaetsa useram tol'ko esli est' hotyaby odin permission = 1
Esli vnutri est' eshe papki s custom permissions to v nih permissions dlya rolei kotorye byli customised ranee mogut izmenyatsa,
	 permissions dlya nekotoryh roleu mogut ischeznut', no permissions dlya novyh rolei poyavitsa ne moget.
*/


function JLMS_delDocPerms($option) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$id = intval(mosGetParam($_REQUEST, 'id', 0));
	$course_id = $JLMS_CONFIG->get('course_id');
	$JLMS_ACL = JLMSFactory::getACL();
	if ($JLMS_ACL->CheckPermissions('docs', 'set_permissions')) {

		$role_id = intval(mosGetParam($_REQUEST, 'role_id2', 0));

		$query = "DELETE FROM #__lms_documents_perms WHERE doc_id = ".intval($id)." AND role_id = ".$role_id;
		$JLMS_DB->SetQuery($query);
		$JLMS_DB->query();

	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=edit_doc&course_id=$course_id&cid2=$id#perms") );
}
function JLMS_addDocPerms($option) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$id = intval(mosGetParam($_REQUEST, 'id', 0));
	$course_id = $JLMS_CONFIG->get('course_id');
	$JLMS_ACL = JLMSFactory::getACL();
	if ($JLMS_ACL->CheckPermissions('docs', 'set_permissions')) {

		$role_id = intval(mosGetParam($_REQUEST, 'role_id', 0));

		$query = "SELECT roletype_id FROM #__lms_usertypes WHERE id = ".$role_id;
		$JLMS_DB->SetQuery($query);
		$roletype_id = $JLMS_DB->LoadResult();
		if ($roletype_id) {
			$query = "DELETE FROM #__lms_documents_perms WHERE doc_id = ".intval($id)." AND role_id = ".$role_id;
			$JLMS_DB->SetQuery($query);
			$JLMS_DB->query();
	
			$p_view = intval(mosGetParam($_REQUEST, 'p_view', 0));
			$p_viewall = intval(mosGetParam($_REQUEST, 'p_viewall', 0));
			$p_order = intval(mosGetParam($_REQUEST, 'p_order', 0));
			$p_publish = intval(mosGetParam($_REQUEST, 'p_publish', 0));
			$p_manage = intval(mosGetParam($_REQUEST, 'p_manage', 0));
			if ($roletype_id == 1) {
				$p_viewall = 0;$p_order = 0;$p_publish = 0;$p_manage = 0;
			}
			$query = "INSERT INTO #__lms_documents_perms (doc_id, role_id, p_view, p_viewall, p_order, p_publish, p_manage) "
			. "\n VALUES ($id, $role_id, $p_view, $p_viewall, $p_order, $p_publish, $p_manage)";
			$JLMS_DB->SetQuery($query);
			$JLMS_DB->query();
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=edit_doc&course_id=$course_id&cid2=$id#perms") );
}
function JLMS_SaveDocLink($option){
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');
	$id = intval(mosGetParam($_REQUEST, 'id', 0));
//	$out_id = intval(mosGetParam($_REQUEST, 'out_id', 0));
	$folder_flag = intval(mosGetParam($_REQUEST, 'folder_flag', 0));

	$cid = mosGetParam( $_REQUEST, 'cid', array(0) );
	$if_zip_pack = 0;

	$is_teacher = $JLMS_ACL->isTeacher();
	if ($course_id && $JLMS_ACL->CheckPermissions('docs', 'manage')) {
		$use_any_library_resource = $JLMS_ACL->CheckPermissions('library', 'only_own_items') ? false : ( $JLMS_ACL->CheckPermissions('library', 'manage') ? true : false);
		
		for($i=0;$i<count($cid);$i++) {
			$out_id = $cid[$i];

			$query = "SELECT COUNT(*)"
				. "\n FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0"
				. "\n WHERE a.id = ".$out_id." AND a.allow_link = 1 AND (((a.published = 1) "
				. "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )"
				. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d')."')) OR (a.publish_end = 0) )"
				. "\n AND ( a.allow_link = 1 ) AND (a.outdoc_share != 0) )"
				. "\n OR (a.owner_id = ".$my->id.")".($use_any_library_resource ? ' OR 1=1' : '').")"
				. "\n ORDER BY a.parent_id, a.ordering, a.doc_name";
				$JLMS_DB->SetQuery( $query );

			if ( $JLMS_DB->LoadResult() && $out_id)  {
				$row = new mos_Joomla_LMS_Document( $JLMS_DB );
				$row->publish_start = 0;
				$row->publish_end = 0;
				if (!$row->bind( $_POST )) {
					echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
					exit();
				}
				$query = "SELECT * FROM #__lms_outer_documents WHERE id=".$out_id;
				$JLMS_DB->SetQuery( $query );
				$out_row = $JLMS_DB->LoadObjectList();
				if(!count($out_row))
				{
					JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
					exit;
				}
				$row->folder_flag = 3;
				$row->file_id = $out_id;

				if (!$id) {
					$row->owner_id = $my->id;
				} else {
					unset($row->owner_id);
					$query = "SELECT owner_id FROM #__lms_documents WHERE id = $id";
					$JLMS_DB->SetQuery( $query );
					$old_owner = $JLMS_DB->LoadResult();
					if ($JLMS_ACL->CheckPermissions('docs', 'only_own_items') && $old_owner != $my->id) {
						JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
					} elseif ($JLMS_ACL->CheckPermissions('docs', 'only_own_role') && $JLMS_ACL->GetRole() != $JLMS_ACL->UserSystemRole($JLMS_DB, $old_owner)) {
						JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
					} 
				}
		
				// start-end publishing
				$publish_start = intval(mosGetParam($_REQUEST, 'is_start', 0));
				$publish_end = intval(mosGetParam($_REQUEST, 'is_end', 0));
				$row->publish_start = $publish_start;
				$row->publish_end = $publish_end;
				if ($row->publish_start) {
					$row->start_date = mosGetParam($_REQUEST, 'start_date', '');
					$row->start_date = JLMS_dateToDB($row->start_date);
				} else { $row->start_date = ''; }
				if ($row->publish_end) {
					$row->end_date = mosGetParam($_REQUEST, 'end_date', '');
					$row->end_date = JLMS_dateToDB($row->end_date);
				} else { $row->end_date = ''; }
		
				$parent_id = intval(mosGetParam( $_REQUEST, 'course_folder', 0 ));
				if (JLMS_GetDocumentCourse($parent_id) != $course_id) { $parent_id = 0; }
				$row->parent_id = $parent_id;
				if (!$id) {
					$query = "SELECT max(ordering) FROM #__lms_documents WHERE course_id = '".$course_id."' AND parent_id = '".$parent_id."'";
					$JLMS_DB->SetQuery( $query );
					$new_ordering = $JLMS_DB->LoadResult();
					$row->ordering = intval($new_ordering) + 1;
				}
		
				if (!$row->check()) {
					echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
					exit();
				}
				
				if (!$row->store()) {
					echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
					exit();
				}
				
			}
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
}

function JLMS_NewDocLink($option){
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id');
	$id = intval(mosgetparam($_REQUEST,'e_id',0));
	$JLMS_ACL = JLMSFactory::getACL();
	$lists = array();
	

	if ($course_id) {
		$rows = array();
		$possibilities = new stdClass();
		$only_folders = true;
		if ($id) { $only_folders = false; }
		JLMSDocs::FillList($course_id, $rows, $possibilities, $only_folders);// also 'rows' would be used for 'course_folders' selectbox

		if ($id) { // existent item
			$permissions = JLMSDocs::GetItemPermissions($rows, $id);
		} else { // new item - we can create it only (if we have 'manage' permission or in the folder with custom 'manage' permission)
			$permissions = new stdClass();
			$permissions->manage = 0;
			if (isset($possibilities->create) && $possibilities->create) {
				$permissions->manage = 1;
			}
		}
	}

	if (isset($permissions->manage) && $permissions->manage) {
		$cur_id = 0;
		$use_any_library_resource = $JLMS_ACL->CheckPermissions('library', 'only_own_items') ? false : ( $JLMS_ACL->CheckPermissions('library', 'manage') ? true : false);
		$query = "SELECT a.*, b.file_name"
		. "\n FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0"
		. "\n WHERE (((a.published = 1) "
		. "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )"
		. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d')."')) OR (a.publish_end = 0) )"
		. "\n AND ( a.allow_link = 1 ) AND (a.outdoc_share != 0) )"
		. "\n OR (a.owner_id = ".$my->id.") OR (a.folder_flag = 1)".($use_any_library_resource ? ' OR (a.folder_flag = 0)': '').")"
		. "\n AND a.folder_flag <> 3"
		. "\n ORDER BY a.parent_id, a.ordering, a.doc_name";
		$JLMS_DB->SetQuery( $query );
		$out_files = $JLMS_DB->LoadObjectList();

		//---------
		$tmp_ass = array();
		for($z=0;$z<count($out_files);$z++){
			
			if($out_files[$z]->folder_flag == 1){
			$not_null = 0;	
				for($b=0;$b<count($out_files);$b++){
					if($out_files[$z]->id == $out_files[$b]->parent_id){
						$not_null = 1;
					}
				}
				if(!$not_null) $tmp_ass[] = $z;
			}
		}
		$temp_arr = array();
		for($z=0;$z<count($out_files);$z++){
			if(!in_array($z, $tmp_ass)){
				$temp_arr[] = $out_files[$z];
			}
		}
		$out_files = $temp_arr;
		////////---

		$out_files = JLMS_GetTreeStructure( $out_files );

		$out_files = AppendFileIcons_toList( $out_files );

		foreach ($out_files as $i=>$v) {
			if($out_files[$i]->folder_flag == 1) {
				$flag = 0;

				$temp[0] = $out_files[$i]->id;

				while ($flag < 1 ) {
					if(JLMS_children($temp[0], $out_files) && count(JLMS_children($temp[0], $out_files))) {
						$temp = JLMS_children($temp[0], $out_files);
						
						for($m=0;$m<count($temp);$m++) {					
							if($out_files[JLMS_children_i($temp[$m],$out_files)]->folder_flag == 0) {
								$flag = $out_files[JLMS_children_i($temp[$m],$out_files)]->id;
								break(2);
							}
						}	
						
					}
					else {
						break;
					}	
				}
				if($flag == 0) {
					unset($out_files[$i]);
				}
			}	
		}

		$mas = array();
		foreach ($out_files as $k=>$v) {
			$mas[] = $out_files[$k];	
		}
		unset($out_files);
		$out_files = $mas;

		$lists['out_files'] = $out_files;
		$row = new mos_Joomla_LMS_Document( $JLMS_DB );
		$row->load( $id );
		if ($id) {
			//$row->checkout($my->id);
		} else {
			$row->published = 0;
			$row->folder_flag = 0;
		}
		$ex_id = null;
		//$lists['course_folders'] = Create_CourseFoldersList($course_id, $row->parent_id, $ex_id);
		$rows1 = array();
		if ($JLMS_ACL->CheckPermissions('docs', 'manage')) {
			$blank_element = new stdClass();$blank_element->id = 0; $blank_element->doc_name_tree = _JLMS_SB_COURSE_FOLDER;
			$rows1[] = $blank_element;
			//$rows1 = array_merge($rows1, $rows);
		}
		$rows1 = array_merge($rows1, JLMSDocs::GetItemsbyPermission($rows, 'create'));
		/*if ($id) {
			$ex_items = array();$ex_items[] = $id;
			$rows2 = & JLMSDocs::ExcludeItems($rows1, $ex_items);
		} else {
			$rows2 = &$rows1;
		}*/
		$lists['course_folders'] = mosHTML::selectList( $rows1, 'course_folder', 'class="inputbox" size="1" style="width:275px;" ', 'id', 'doc_name_tree', $ex_id );

		$lists['publishing'] = mosHTML::yesnoRadioList( 'published', 'class="inputbox" ', $row->published);

		$query = "SELECT id FROM #__lms_outer_documents";
		$JLMS_DB->SetQuery($query);
		$rows_coll = JLMSDatabaseHelper::LoadResultArray();
		$lists['collapsed_folders'] = $rows_coll;

		JLMS_docs_html::NewDocLink($row, $lists, $course_id, $option, $cur_id);	
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
	}	
}

function JLMS_children_i ($id, $rows) {

	foreach ($rows as $i=>$v) {
		if($rows[$i]->id == $id) {
			return $i;		
		}
	}
}

function JLMS_children ($id,$rows) {

	$flag=array();
	foreach ($rows as $i=>$v) {
		if($rows[$i]->parent_id == $id) {
			$flag[] = $rows[$i]->id;
		}
	}
	
	if(count($flag))
		return $flag;
	else 
		return false;	
}

function JLMS_DocumentsSaveView( $id, $option) {
	global $JLMS_DB, $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id');
	$JLMS_ACL = JLMSFactory::getACL();
	if ($course_id && $course_id == $id && $JLMS_ACL->CheckPermissions('docs', 'manage') ) {
		$folders = strval(mosGetParam($_REQUEST, 'folders', ''));
		$query = "DELETE FROM #__lms_documents_view WHERE course_id = $id";
		$JLMS_DB->SetQuery($query);
		$JLMS_DB->query();
		$success_message = _JLMS_DOCS_SAVE_VIEW_SUCCESS;
		if ($folders) {
			$fold_array = explode('-', $folders);
			if (!empty($fold_array)) {
				$fold_array_int = array();
				foreach ($fold_array as $fa) {
					$fold_array_int[] = intval($fa);
				}
				$fold_array_int_str = implode(',', $fold_array_int);
				$query = "SELECT id FROM #__lms_documents WHERE id IN ($fold_array_int_str) AND course_id = $id AND file_id = 0 AND folder_flag = 1";
				$JLMS_DB->SetQuery($query);
				$fs = JLMSDatabaseHelper::LoadResultArray();
				if (!empty($fs)) {
					$query = "INSERT INTO #__lms_documents_view (course_id, doc_id) VALUES";
					$pref = '';
					foreach ($fs as $fs_one) {
						$query .= $pref."\n ($id, $fs_one)";
						$pref = ',';
					}
					$JLMS_DB->SetQuery($query);
					$JLMS_DB->query();
				}
			}
		}
	} else {
		$success_message = 'ERROR';
	}
	if (defined(_ISO)) {
		$iso = explode( '=', _ISO );
	} else {
		$iso = array();
		$iso[0] = 'encoding';
		$iso[1] = 'iso-8859-1';
	}
	echo "\n"."some notices :)";
	$debug_str = @ob_get_contents();
	@ob_end_clean();
	header ('Expires: Thu, 12 May 1983 11:00:00 GMT');
	header ('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
	header ('Cache-Control: no-cache, must-revalidate');
	header ('Pragma: no-cache');
	if (class_exists('JFactory')) {
		$document=JFactory::getDocument();
		$charset_xml = $document->getCharset();
		header ('Content-Type: text/xml; charset='.$charset_xml);
	} else {
		header ('Content-Type: text/xml');
	}
	echo '<?xml version="1.0" encoding="'.$iso[1].'" standalone="yes"?>' . "\n";
	echo '<response>' . "\n";
		echo "\t" . '<message><![CDATA['.$success_message.']]></message>' . "\n";
	echo "\t" . '<debug><![CDATA['.$debug_str.']]></debug>' . "\n";
	echo '</response>' . "\n";
	die;
}

function JLMS_previewContent( $doc_id, $option ) {
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$do_redirect = true;
	$course_id = $JLMS_CONFIG->get('course_id');
	if ( $course_id && $JLMS_ACL->CheckPermissions('docs', 'view') ) {
		$query = "SELECT * FROM #__lms_documents"
		. "\n WHERE id = '".$doc_id."' AND course_id = '".$course_id."' AND ((folder_flag = 0 AND file_id = 0) OR folder_flag = 3)"
		. (($JLMS_ACL->CheckPermissions('docs', 'view_all')) ? '' : "\n AND published = 1")
		. (($JLMS_ACL->CheckPermissions('docs', 'view_all')) ? '' : "\n AND ( ((publish_start = 1) AND (start_date <= '".date('Y-m-d')."')) OR (publish_start = 0) )")
		. (($JLMS_ACL->CheckPermissions('docs', 'view_all')) ? '' : "\n AND ( ((publish_end = 1) AND (end_date >= '".date('Y-m-d')."')) OR (publish_end = 0) )")
		;
		$JLMS_DB->SetQuery( $query );
		$row_zip = $JLMS_DB->LoadObject();
		if ( is_object($row_zip) ) {
			if($row_zip->folder_flag == 3){
				$query = "SELECT a.*, b.file_name"
				. "\n FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0"
				. "\n WHERE a.folder_flag = 0 AND a.id = ".$row_zip->file_id." AND a.allow_link = 1";
				$JLMS_DB->SetQuery( $query );
				$out_row = $JLMS_DB->LoadObjectList();

				if(count($out_row)){
					$row_zip->doc_name = $out_row[0]->doc_name;
					$row_zip->doc_description = $out_row[0]->doc_description;
					$row_zip->file_id = $out_row[0]->file_id;
					
				}else{
					$row_zip->doc_name = _JLMS_LP_RESOURSE_ISUNAV;
				}
			}

			if ($row_zip->doc_name) {
				$do_redirect = false;
				$lists = array();
				//tracking
				global $Track_Object;
				$Track_Object->UserDownloadFile( $my->id, $doc_id );
				JLMS_docs_html::show_ZipPack( $course_id, $option, $row_zip, $lists, 'content' );
			}
		}
	}
	if ($do_redirect) {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
	}
}
function JLMS_previewZipPack( $doc_id, $option ) {
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$do_redirect = true;
	$course_id = $JLMS_CONFIG->get('course_id');
	if ( $course_id && $JLMS_ACL->CheckPermissions('docs', 'view') ) {
		$query = "SELECT a.file_id, b.startup_file, b.zip_folder, a.doc_name FROM #__lms_documents as a, #__lms_documents_zip as b"
		. "\n WHERE a.id = '".$doc_id."' AND a.course_id = '".$course_id."' AND a.folder_flag = 2 AND a.file_id = b.id"
		. (($JLMS_ACL->CheckPermissions('docs', 'view_all')) ? '' : "\n AND a.published = 1")
		. (($JLMS_ACL->CheckPermissions('docs', 'view_all')) ? '' : "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )")
		. (($JLMS_ACL->CheckPermissions('docs', 'view_all')) ? '' : "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d')."')) OR (a.publish_end = 0) )")
		;
		$JLMS_DB->SetQuery( $query );
		$row_zip = $JLMS_DB->LoadObject();
		if ( is_object($row_zip) ) {
			if ($row_zip->startup_file) {
				$do_redirect = false;
				$lists = array();
				//tracking
				global $Track_Object;
				$Track_Object->UserDownloadFile( $my->id, $doc_id );
				JLMS_docs_html::show_ZipPack( $course_id, $option, $row_zip, $lists );
			}
		}
	}
	if ($do_redirect) {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
	}
}
function JLMS_chooseStartup( $option ) {
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;
	$doc_id = intval(mosGetParam($_REQUEST, 'doc_id', 0));
	$course_id = $JLMS_CONFIG->get('course_id');
	$can_prooceed = false;
	if ($course_id && $doc_id) {
		$rows = array();
		$possibilities = new stdClass();
		JLMSDocs::FillList($course_id, $rows, $possibilities);
		if ($possibilities->manage && JLMSDocs::CheckCourseID($rows, $doc_id, $course_id)) {
			$permissions = JLMSDocs::GetItemPermissions($rows, $doc_id);
			if (isset($permissions->manage) && $permissions->manage) {
				$query = "SELECT a.file_id, b.zip_folder, b.zip_srv_name, b.startup_file FROM #__lms_documents as a, #__lms_documents_zip as b"
				. "\n WHERE a.id = '".$doc_id."' AND a.course_id = '".$course_id."' AND a.folder_flag = 2 AND a.file_id = b.id";
				$JLMS_DB->SetQuery( $query );
				$zip_row = $JLMS_DB->loadObject();
				if (is_object( $zip_row )) {
					$can_prooceed = true;
				}
			}
		}
	}
	if ( $can_prooceed ) {
		$full_dir = _JOOMLMS_SCORM_FOLDER_PATH . "/" . $zip_row->zip_folder;
		$zippackFiles = JLMS_ReadDirectory( $full_dir );
		$zp_files = array(  mosHTML::makeOption( '', _JLMS_DOCS_SEL_STARTUP_FILE ) );
		foreach ( $zippackFiles as $file ) {
			$zp_files[] = mosHTML::makeOption( $file );
		}
		$zp_files = mosHTML::selectList( $zp_files, 'zip_contents', 'class="inputbox" size="1" ', 'value', 'text', $zip_row->startup_file );
		$lists = array();
		$lists['doc_id'] = $doc_id;
		JLMS_docs_html::show_PageChooseStartup( $course_id, $option, $zp_files, $lists );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
	}
}
function JLMS_saveStartup( $option ) {
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;
	$doc_id = intval(mosGetParam($_REQUEST, 'doc_id', 0));
	$course_id = $JLMS_CONFIG->get('course_id');
	$can_prooceed = false;
	if ($course_id && $doc_id) {
		$rows = array();
		$possibilities = new stdClass();
		JLMSDocs::FillList($course_id, $rows, $possibilities);
		if ($possibilities->manage && JLMSDocs::CheckCourseID($rows, $doc_id, $course_id)) {
			$permissions = JLMSDocs::GetItemPermissions($rows, $doc_id);
			if (isset($permissions->manage) && $permissions->manage) {
				$can_prooceed = true;
			}
		}
	}
	if ( $can_prooceed ) {
		$query = "SELECT a.file_id FROM #__lms_documents as a, #__lms_documents_zip as b"
		. "\n WHERE a.id = '".$doc_id."' AND a.course_id = $course_id AND a.folder_flag = 2 AND a.file_id = b.id";
		$JLMS_DB->SetQuery( $query );
		$zippack_id = $JLMS_DB->LoadResult();
		if ($zippack_id) {
			$startup_file = strval(mosGetParam($_REQUEST, 'zip_contents', ''));
			if ( $startup_file && ( strpos($startup_file, '../') === false ) && ( strpos($startup_file, '..\\') === false ) && ( strpos($startup_file, ':') === false ) ) {
				$query = "UPDATE #__lms_documents_zip SET startup_file = '".JLMSDatabaseHelper::GetEscaped($startup_file)."' WHERE id = $zippack_id AND course_id = $course_id";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
			}
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
}
function JLMS_OrderDocuments( $option ) {
	global $JLMS_DB, $my, $Itemid, $task, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');
	$order_id = intval(mosGetParam($_REQUEST, 'row_id', 0));
	if ( $order_id && $course_id && $JLMS_ACL->CheckPermissions('docs', 'order') ) {
		$query = "SELECT parent_id FROM #__lms_documents WHERE id = '".$order_id."'";
		$JLMS_DB->SetQuery( $query );
		$item_parent = $JLMS_DB->LoadResult();
		$query = "SELECT id FROM #__lms_documents WHERE course_id = '".$course_id."' AND parent_id = '".$item_parent."' ORDER BY ordering, doc_name";
		$JLMS_DB->SetQuery( $query );
		$id_array = JLMSDatabaseHelper::LoadResultArray();
		if (count($id_array)) {
			$i = 0;$j = 0;
			while ($i < count($id_array)) {
				if ($id_array[$i] == $order_id) { $j = $i;}
				$i ++;
			}
			$do_update = true;
			if (($task == 'doc_orderup') && ($j) ) {
				$tmp = $id_array[$j-1];
				$id_array[$j-1] = $id_array[$j];
				$id_array[$j] = $tmp;
			} elseif (($task == 'doc_orderdown') && ($j < (count($id_array)-1)) ) {
				$tmp = $id_array[$j+1];
				$id_array[$j+1] = $id_array[$j];
				$id_array[$j] = $tmp;
			}
			$i = 1; //!!!
			foreach ($id_array as $document_id) {
				$query = "UPDATE #__lms_documents SET ordering = '".$i."' WHERE id = '".$document_id."' and course_id = '".$course_id."'";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$i ++;
			}
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
}

//todo: remake $JLMS_DB->LoadObjectList ==> $JLMS_DB->LoadObject();
function JLMS_downloadDocument( $id, $option ) {
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');
	$force_download = strval( mosGetParam( $_REQUEST, 'force', '' ) ); 
	$file_contents = false;
	
	$sessionid = JRequest::getString('sessionid', '');
	if(!$my->id){
		$query = "SELECT *"
		. "\n FROM #__session"
		. "\n WHERE 1"
		. "\n AND session_id = '".$sessionid."'"
		;
		$JLMS_DB->setQuery($query);
		$session_row = $JLMS_DB->loadObject();
		
		$userid = 0;
		if(isset($session_row->userid) && $session_row->userid){
			$userid = $session_row->userid;
		}

		$JLMS_ACL = & JLMSFactory::getACL($userid);
		$course_id = $course_id ? $course_id : JRequest::getInt('course_id', 0);
		$JLMS_ACL->PrepareCourse($course_id);
	}
	
	if ($course_id && $JLMS_ACL->CheckPermissions('docs', 'view') && (JLMS_GetDocumentCourse($id) == $course_id) ) {
		$query = "SELECT file_id, doc_name, folder_flag,owner_id FROM #__lms_documents"
		. "\n WHERE id = '".$id."' AND course_id = '".$course_id."'"
		. (($JLMS_ACL->CheckPermissions('docs', 'view_all')) ? '' : "\n AND published = 1")
		. (($JLMS_ACL->CheckPermissions('docs', 'view_all')) ? '' : "\n AND ( ((publish_start = 1) AND (start_date <= '".date('Y-m-d')."')) OR (publish_start = 0) )")
		. (($JLMS_ACL->CheckPermissions('docs', 'view_all')) ? '' : "\n AND ( ((publish_end = 1) AND (end_date >= '".date('Y-m-d')."')) OR (publish_end = 0) )")
		;
		$JLMS_DB->SetQuery( $query );
		$file_data = $JLMS_DB->LoadObjectList();
		if (count($file_data) == 1) {
			$file_contents = false;
			$do_tracking = false;
			///-----------
			if($file_data[0]->folder_flag == 3){
				$query = "SELECT a.*, b.file_name"
				. "\n FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0"
				. "\n WHERE a.folder_flag = 0 AND a.id = ".$file_data[0]->file_id." AND a.allow_link = 1";
				$JLMS_DB->SetQuery( $query );
				$out_row = $JLMS_DB->LoadObjectList();
				
				if(count($out_row)){
					$file_data[0]->doc_name = $out_row[0]->doc_name;
					
					$file_data[0]->file_id = $out_row[0]->file_id;
					
				}
				else{
					JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
				}
			}
			
			if ($force_download != 'force' && $force_download != 'player') {
				require_once(_JOOMLMS_FRONT_HOME . "/includes/jlms_docs_process.php");
				$file_contents = JLMS_showMediaDocument($file_data[0]->file_id, $id, $file_data[0]->doc_name, $do_tracking );
			}
			if ( ($file_contents && $do_tracking) || !$file_contents) {
				//tracking
				global $Track_Object;
				$Track_Object->UserDownloadFile( $my->id, $id );
			}
			
			if ($file_contents) {
				$lists = array();
				$row_zip = new stdClass();
				$row_zip->doc_description = $file_contents;
				$row_zip->doc_name = $file_data[0]->doc_name;
				$row_zip->doc_id = $id;
				$lpath_id = intval( mosGetParam( $_REQUEST, 'lpath_id', 0 ) ); 
				$lists['lpath_id'] = $lpath_id;
				JLMS_docs_html::show_ZipPack($course_id, $option, $row_zip, $lists, 'document_contents');
			} else {
				$headers = array();
				if ($force_download == 'player') {
					$headers['Content-Disposition'] = 'inline';
				}
				JLMS_downloadFile( $file_data[0]->file_id, $option, $file_data[0]->doc_name, true, $headers);
			}
			
			
		}
	}
	if (!$file_contents) {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
	}
}
//to do: proverku na teacher, student,. ...
//+ otobragat' daty zakachki faila..
// + add v 'SelectListe' otobragat' dlinnyi put' k folderam (t.e. s nazvaniyami vsex parentov)
// new: ubrat' lists - teper' eta peremennaya ne nugna t.k. dobavlenie failov/folderov vyneseno v dr. procedury
function JLMS_showDocuments( $option) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
	
//	echo intval(ini_get('post_max_size'));
//	echo '<br />';
//	ini_set("post_max_size", "8M");
//	echo '<br />';
//	echo intval(ini_get('post_max_size'));
//	echo '<br />';
//	die;
	
	$JLMS_ACL = JLMSFactory::getACL();
	
	$id = $JLMS_CONFIG->get('course_id');
	if ( $id && $JLMS_ACL->CheckPermissions('docs', 'view') ) {

		$rows = array();
		$possibilities = new stdClass();
		JLMSDocs::FillList($id, $rows, $possibilities);

		$query = "SELECT doc_id FROM #__lms_documents_view WHERE course_id = $id";
		$JLMS_DB->SetQuery($query);
		$rows2 = JLMSDatabaseHelper::LoadResultArray();

		$lists = array();
		$lists['collapsed_folders'] = $rows2;

		$lms_titles_cache = & JLMSFactory::getTitles();
		$lms_titles_cache->setArray('documents', $rows, 'id', 'doc_name');
		JLMS_docs_html::showCourseDocuments( $id, $option, $rows, $lists, $possibilities );
	} else {
		if ($id) {
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$id") );
		} else {
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
		}
	}
}

function JLMS_editDocument( $id, $option, $is_folder = 0 ) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');
	$i_can_manage_it = false;
	if ($course_id) {
		$rows = array();
		$possibilities = new stdClass();
		$only_folders = true;
		if (!$is_folder && $id) { $only_folders = false; }
		JLMSDocs::FillList($course_id, $rows, $possibilities, $only_folders);// also 'rows' would be used for 'course_folders' selectbox

		if ($id) { // existent item
			$permissions = JLMSDocs::GetItemPermissions($rows, $id);
		} else { // new item - we can create it only (if we have 'manage' permission or in the folder with custom 'manage' permission)
			$permissions = new stdClass();
			$permissions->manage = 0;
			if (isset($possibilities->create) && $possibilities->create) {
				$permissions->manage = 1;
			}
		}
	}
	if (isset($permissions->manage) && $permissions->manage) {
		$AND_ST = "";
		if( false !== ( $enroll_period = JLMS_getEnrolPeriod( $my->id, $course_id )) ) 
		{
			$AND_ST = " AND IF(is_time_related, (show_period < '".$enroll_period."' ), 1) ";	
		}
		
		$row = new mos_Joomla_LMS_Document( $JLMS_DB );
		$row->addCond( $AND_ST );
		$row->load( $id );
		if ($id) {
			if($row->folder_flag == 3){
				JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=add_doclink&id=$course_id&e_id=".$id) );
				exit();
			}
		} else {
			$row->published = 0;
			$row->folder_flag = $is_folder;
		}
		$lists = array();
		$ex_id = null;
		if ($id) {
			$ex_id = $row->parent_id;
		}
		//$lists['course_folders'] = Create_CourseFoldersList($course_id, $row->parent_id, $ex_id);
		$rows1 = array();
		if ($JLMS_ACL->CheckPermissions('docs', 'manage')) {
			$blank_element = new stdClass();$blank_element->id = 0; $blank_element->doc_name_tree = _JLMS_SB_COURSE_FOLDER;
			$rows1[] = $blank_element;
			//$rows1 = array_merge($rows1, $rows);
		}
		$rows1 = array_merge($rows1, JLMSDocs::GetItemsbyPermission($rows, 'create'));
		if ($id) {
			$ex_items = array();$ex_items[] = $id;
			$rows2 = & JLMSDocs::ExcludeItems($rows1, $ex_items);
		} else {
			$rows2 = &$rows1;
		}
		$lists['course_folders'] = mosHTML::selectList( $rows2, 'course_folder', 'class="inputbox" size="1" style="width:275px;" ', 'id', 'doc_name_tree', $ex_id );

		$lists['publishing'] = mosHTML::yesnoRadioList( 'published', 'class="inputbox" ', $row->published);
		$lists['upload_zip'] = mosHTML::yesnoRadioList( 'upload_zip', ' onchange="jlms_changeZips_values(this);"', 1 );/*batch uploads */
		$lists['content_zip_pack'] = mosHTML::yesnoRadioList( 'zip_package', ' onchange="jlms_changeZips_values(this);"', 0 );/*batch uploads */
		
		JLMS_docs_html::showEditDocument( $row, $lists, $option, $course_id );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
	}
}
function JLMS_cancelDocument( $option ) {
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$Itemid = $JLMS_CONFIG->get('Itemid');
	$course_id = $JLMS_CONFIG->get('course_id');
	JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id"));
}
function JLMSDocs_get_dir_elements($curpath) {
	$lsoutput = "";
	$result = array();
	$dir = dir($curpath);
   
	while ($file = $dir->read()) {
	   if($file != "." && $file != "..") {
	       if (is_dir($curpath.$file)) {
	             $result[$file] = JLMSDocs_get_dir_elements($curpath.$file."/");
	       } else {
	         	$result [$file] = $file;
	       }
	   }
	}
	$dir->close();
	return $result;
}
/* batch uploads */
function JLMSDocs_save_file_from_zip_array ( $elem , $parent, $path, &$ordering, &$first_saved_id, &$default_row, $is_zip_pack, $elemname = array()) {
	
	foreach ($elem as $k=>$v) {
		if(is_array($v)) {
			$path1 = $path."/".$k;
			$parent1 = JLMSDocs_save_single_file($k, 1, $parent, '', $ordering, $default_row);
			if (!$first_saved_id) {
				$first_saved_id = $parent1;
			}
			JLMSDocs_save_file_from_zip_array ( $v , $parent1, $path1, $ordering, $first_saved_id, $default_row, $new_elem_name);
		} else {
			$new_elem_name = '';
			if (is_array($elemname) && count($elemname)) {
				$new_elem_name = isset($elemname[$k]) ? $elemname[$k] : '';
			}
			$out_fileext = strtolower(substr($k, - 4));
			$new_ff = ($is_zip_pack && $out_fileext == '.zip') ? 2 : 0;
			$saved_elem = JLMSDocs_save_single_file($k, $new_ff, $parent, $path, $ordering, $default_row, $new_elem_name);
			if (!$first_saved_id) {
				$first_saved_id = $saved_elem;
			}
		}
	}
}
function JLMSDocs_uploadZIPPack_fromFile( $course_id, $file_path, $real_filename ) {
	$db = & JLMSFactory::getDb();
	$user = JLMSFactory::getUser();

	$base_Dir = _JOOMLMS_SCORM_FOLDER_PATH;

	if (!$file_path) {
		mosErrorAlert(_JLMS_EM_SELECT_FILE);
	}
	if (!file_exists($file_path)) {
		mosErrorAlert (_JLMS_EM_UPLOAD_SIZE_ERROR);
	}

	if (strcmp(substr($file_path,-4,1),".")) {
		mosErrorAlert(_JLMS_EM_BAD_FILEEXT);
	}
	if (strcasecmp(substr($file_path,-4),".zip")) {
		mosErrorAlert(_JLMS_EM_BAD_FILEEXT);
	}
	$fold_unique_name = str_pad($user->get('id'),4,'0',STR_PAD_LEFT) . '_zip_' . md5(uniqid(rand(), true));
	$file_unique_name = $fold_unique_name . '.' . substr($real_filename,-3);

	$uploadPath = $base_Dir."/".$fold_unique_name;

	$realFileSize = 0;
	$realFilesCount = 0;

	if (preg_match("/.zip$/", strtolower($file_path))) {

		require_once(_JOOMLMS_FRONT_HOME . "/includes/libraries/lms.lib.zip.php");

		$zipFile = new pclZip($file_path);
		$zipContentArray = $zipFile->listContent();
		$realFileSize = 0;
		$realFilesCount = 0;
		foreach($zipContentArray as $thisContent) {
			if ( preg_match('~\.(php.*|phtml)$~i', $thisContent['filename']) ) {
				mosErrorAlert(_JLMS_EM_READ_PACKAGE_ERROR);
			}
			$realFileSize += $thisContent['size'];
			if (!$thisContent['folder']) {
				$realFilesCount ++;
			}
		}
		if(!mkdir($uploadPath, 0755)) {
			mosErrorAlert(_JLMS_EM_SCORM_FOLDER);
		}
		if($uploadPath[strlen($uploadPath)-1] == '/') {
			$uploadPath = substr($uploadPath,0,-1);
		}
		/*
		--------------------------------------
			Uncompressing phase
		--------------------------------------
		*/
		chdir($uploadPath);
		$unzippingState = $zipFile->extract(PCLZIP_OPT_PATH, $uploadPath."/");
		if ($dir = @opendir($uploadPath)) {
			while ($file = readdir($dir)) {
				if ($file != '.' && $file != '..') {
					$filetype = "file";
					if (is_dir($uploadPath.'/'.$file)) {
						$filetype = "folder";
					}
					$safe_file = replace_dangerous_char($file,'strict');
					@rename($uploadPath.'/'.$file, $uploadPath.'/'.$safe_file);
					if ($filetype == "file") {
						@chmod($uploadPath.'/'.$safe_file, 0644);
					} elseif ($filetype = "folder") {
						@chmod($uploadPath.'/'.$safe_file, 0755);
					}
				}
			}
			closedir($dir);
		}
	}

	$zipfile_size = filesize($file_path);
	if (!rename($file_path,$base_Dir."/".$file_unique_name) || !mosChmod($base_Dir."/".$file_unique_name)) {
		mosErrorAlert("Upload of ".$real_filename." failed");
	}

	$userfile_name = JLMSDatabaseHelper::GetEscaped($real_filename);
	$query = "INSERT INTO #__lms_documents_zip (course_id, owner_id, zip_name, zip_srv_name, zip_folder, startup_file, count_files, zip_size, zipfile_size, upload_time)"
	. "\n VALUES "
	. "\n ('".$course_id."', '".$user->get('id')."', '".$userfile_name."', '".$file_unique_name."', '".$fold_unique_name."', '', '".$realFilesCount."', '".$realFileSize."', "
	. "\n '".$zipfile_size."', '".gmdate('Y-m-d H:i:s')."')";
	$db->SetQuery( $query );
	$db->query();
	$zippack_id = $db->insertid();
	return $zippack_id;
}
/* batch uploads */
function JLMSDocs_save_single_file ($doc_name_post, $folder_flag, $parent_id, $path, &$ordering, &$default_row, $elemname = '') {
	$db = JFactory::getDbo();
	$user = JLMSFactory::getUser();
	$JLMS_CONFIG = JLMSFactory::getConfig();

	$row = new mos_Joomla_LMS_Document( $db );
	$row->publish_start = 0;
	$row->publish_end = 0;
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}

	$row->folder_flag = $folder_flag; 

	$row->id = intval($row->id);
	if($row->id){
		$query = "SELECT file_id FROM #__lms_documents WHERE id=".$row->id;
		$db->setQuery($query);
		$row->file_id = $db->loadResult();
	}

	$doc_name_post = (get_magic_quotes_gpc()) ? stripslashes( $doc_name_post ) : $doc_name_post; 
	$doc_name_post = strip_tags($doc_name_post);

	if ($folder_flag == 1) {
		$row->doc_name = $elemname ? $elemname : $doc_name_post;
	} else {
		
		
		
		$row->doc_name = $elemname ? $elemname : $doc_name_post;
		if ($folder_flag == 2) {
			$file_id = JLMSDocs_uploadZIPPack_fromFile( $row->course_id, $path . DS . $doc_name_post, $doc_name_post );
			if ($file_id) {
				$JLMS_CONFIG->set('zippack_was_uploaded', 1);
			}
		} else {
			if ($path) {
				$file_id = JLMSDocs_uploadFile_from_zip($path, $doc_name_post);
				if ($file_id === false) {
					$JLMS_CONFIG->set('files_skipped_by_extension', true);
					return false;
				}
			} else {
				$file_id = 0;
			}
		}
		if ($file_id) {
			if ($row->file_id) {
				if ($row->id) {
					$query = "SELECT folder_flag FROM #__lms_documents WHERE id = ".$row->id;
					$db->SetQuery($query);
					$old_ff = $db->LoadResult();
					if ($old_ff == 2) {
						$zip_ids = array();
						$zip_ids[] = $row->file_id;
						JLMS_deleteZipPacks($row->course_id,$zip_ids);
					} else {
						JLMS_deleteFiles($row->file_id);
					}
				}
			}
			@unlink($path."/".$doc_name_post);
			$row->file_id = $file_id;
		}
	}

	$row->start_date = '';
	$row->end_date = '';
	if (isset($default_row->published)) {
		$row->published = $default_row->published;
		$row->is_time_related = $default_row->is_time_related;
		$row->show_period = $default_row->show_period;
	} else {
		unset($row->published);
		unset($row->is_time_related);
		unset($row->show_period);
	}
	if (isset($default_row->publish_start)) {
		$row->publish_start = $default_row->publish_start;
		$row->start_date = $default_row->start_date;
	} else {
		unset($row->publish_start);
	}
	if (isset($default_row->publish_end)) {
		$row->publish_end = $default_row->publish_end;
		$row->end_date = $default_row->end_date;
	} else {
		unset($row->publish_end);
	}

	$row->parent_id = $parent_id;

	if ($row->id) {
		unset($row->owner_id);
		unset($row->ordering);
	} else {
		$row->owner_id = $user->get('id');
		$doc_order = 0;
		if (isset($ordering[$parent_id])) {
			$doc_order = $ordering[$parent_id];
			$ordering[$parent_id] ++;
		} else {
			$ordering[$parent_id] = 1;//ordering for next element of this parent will be = 1 (the current element ordering = 0 )
		}
		$row->ordering = $doc_order;
	}
	$row->doc_description = strval(JLMS_getParam_LowFilter($_POST, 'doc_description', ''));
	$row->doc_description = JLMS_ProcessText_LowFilter($row->doc_description);

	if (!$row->check()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if ($JLMS_CONFIG->get('zippack_was_uploaded', 0)) {
		$JLMS_CONFIG->set('zippack_was_uploaded', $row->id);
	}

	if($folder_flag || $file_id) {
		return $row->id;
	}
	return false;
}
function JLMSDocs_uploadFile_from_zip($path, $name) {
	global $JLMS_DB;
	
	$is_supported_type = 1;
	$base_Dir = _JOOMLMS_DOC_FOLDER;
	$userfile_name = $name;

	$good_ext = false;
	$file_ext = '';
	
	if(preg_match('/\S+\.(\S+)$/', $userfile_name, $out)){
		if(isset($out[0]) && isset($out[1]) && $out[1]){
			$file_ext = $out[1];
			if(strlen($file_ext) <= 6){
				$good_ext = true;
			}
		}
	}
	
	if (!$good_ext) {
		mosErrorAlert(_JLMS_EM_BAD_FILEEXT);
	}
	$query = "SELECT LOWER(filetype) as filetype FROM #__lms_file_types";
	$JLMS_DB->SetQuery( $query );
	$ftypes = JLMSDatabaseHelper::LoadResultArray();
	
	if (empty($ftypes) || !in_array(strtolower($file_ext), $ftypes)) {
		//mosErrorAlert(_JLMS_EM_BAD_FILEEXT);
		$is_supported_type = 0;
	}
	
	if($is_supported_type) {
		$file_unique_name = str_pad('0',4,'0',STR_PAD_LEFT) . '_' . md5(uniqid(rand(), true)) . '.' . $file_ext;
	
		if (!copy ($path."/".$name,$base_Dir.$file_unique_name) || !mosChmod($base_Dir.$file_unique_name)) {
			mosErrorAlert("Upload of ".$userfile_name." failed");
		} else {
			global $JLMS_DB, $my;
			$userfile_name = JLMSDatabaseHelper::GetEscaped($userfile_name);
			$query = "INSERT INTO #__lms_files (file_name, file_srv_name, owner_id)"
			. "\n VALUES ('".$userfile_name."', '".$file_unique_name."', '".$my->id."')";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			return $JLMS_DB->insertid();
		}
	}
	else {
		return false;
	}
}
function JLMS_saveDocument( $option ) {
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$Itemid = $JLMS_CONFIG->get('Itemid');
	$db = JFactory::getDbo();
	$user = JLMSFactory::getUser();
	$my_id = $user->get('id');
	$JLMS_ACL = JLMSFactory::getACL();
	$JLMS_CONFIG->set('files_skipped_by_extension', false);

	$course_id = $JLMS_CONFIG->get('course_id');
	$id = intval(mosGetParam($_REQUEST, 'id', 0));
	$folder_flag = intval(mosGetParam($_REQUEST, 'folder_flag', 0));
	$if_zip_pack = 0;
	if ( $course_id) {//} && ( $id || (!$id && ((isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) || (($folder_flag == 1) || !empty($_REQUEST['doc_name'])) ) ) ) ) {

		$stop_processing = false;

		$parent_id = intval(mosGetParam( $_REQUEST, 'course_folder', 0 ));$rows = array();
		
		$rows = array();
		$possibilities = new stdClass();
		JLMSDocs::FillList($course_id, $rows, $possibilities);
		
		if (!JLMSDocs::CheckCourseID($rows, $parent_id, $course_id)) { $parent_id = 0; }
		if ($id && $parent_id == $id) { $parent_id = 0; }

		if (!JLMSDocs::CheckCourseID($rows, $id, $course_id)) { $stop_processing = true; }
		$item_permissions = & JLMSDocs::GetItemPermissions($rows, $id);
		$parent_permissions = & JLMSDocs::GetItemPermissions($rows, $parent_id);
		if ($id && $item_permissions->manage) {
			if (JLMSDocs::GetItemParentID($rows, $id) == $parent_id) {
				//parent is not changed - we don't need to check its permissions
			} else {
				if (!$parent_permissions->create) {
					$parent_id = JLMSDocs::GetItemParentID($rows, $id);
				}
			}
		} elseif(!$id && $parent_permissions->create) {
		} else {
			$stop_processing = true;
		}

		if ($stop_processing) {
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
			exit();
		}

		$upload_zip = intval(mosGetParam($_REQUEST, 'upload_zip', 0));
		$is_zip_pack = intval(mosGetParam($_REQUEST, 'zip_package', 0));
		if ($folder_flag) {
			$upload_zip = false;
			$is_zip_pack = false;
		}
		if ($id) {
			$upload_zip = false;
		}
		if ($upload_zip) {
			$is_zip_pack = false;
		}

		$query = "SELECT max(ordering) FROM #__lms_documents WHERE course_id = $course_id AND parent_id = $parent_id";
		$db->SetQuery( $query );
		$max_ordering = $db->LoadResult() + 1;
		$ordering = array();
		$ordering[$parent_id] = $max_ordering;

		require_once(JPATH_SITE . DS . "components" . DS . "com_joomla_lms" . DS . "includes" . DS . "libraries" . DS . "lms.lib.files.php");
		require_once(JPATH_SITE . DS . "components" . DS . "com_joomla_lms" . DS . "includes" . DS . "libraries" . DS . "lms.lib.zip.php");

		$available_app_ext = array();
		$available_app_ext[] = 'application/zip';
		$available_app_ext[] = 'application/x-zip';
		$available_app_ext[] = 'application/zip-compressed';
		$available_app_ext[] = 'application/x-zip-compressed';

		$zip_files_array = array();
		$zip_files_names = array();
		$no_files_array = array();
		$temporary_files = array();

		if (count($_FILES)) {
			for($i=0;$i<count($_FILES);$i++) {
				if(isset($_FILES['userfile'.$i]) && isset($_FILES['userfile'.$i]['name']) && $_FILES['userfile'.$i]['name']){
					//get 4 last letters from file name and check if it is zip file
					$out_fileext = '';
					if($upload_zip) {
						$out_fileext = '';
						if(preg_match('/\S+\.(\S+)$/', $_FILES['userfile'.$i]['name'], $out)){
							if(isset($out[0]) && isset($out[1]) && $out[1]){
								$out_fileext = $out[1];
							}
						}
					}
					
					if($upload_zip && (in_array($_FILES['userfile'.$i]['type'], $available_app_ext) || ($out_fileext == '.zip')) ) {
						//upload and extract zip file
						$msg = '';
						if(JLMS_Files::uploadFile( $_FILES['userfile'.$i]['tmp_name'], $_FILES['userfile'.$i]['name'], $msg )) {
							$temp_dir = uniqid(rand());
							$extract_dir = JPATH_SITE."/tmp/file_".$temp_dir."/";
							$temporary_files[] = $extract_dir;
							$archive = JPATH_SITE."/tmp/".$_FILES['userfile'.$i]['name'];
							if(JLMS_Zip::extractFile( $archive, $extract_dir)) {
								$zip_files_array[$extract_dir] = JLMSDocs_get_dir_elements($extract_dir);
								//check if zip contains only one file - then replace it's name with the doc_nameX field
								$elems_count = 0;
								$folders_count = 0;
								$item_name = '';
								foreach ($zip_files_array[$extract_dir] as $k => $v) {
									$item_name = $k;
									$elems_count++;
									if (is_array($v)) {
										$folders_count ++;
									}
								}
								if (!$folders_count) {
									if ($elems_count == 1 && $item_name) {
										if (isset($_REQUEST['doc_name'.$i]) && $_REQUEST['doc_name'.$i]) {
											$zip_files_names[$extract_dir][$item_name] = $_REQUEST['doc_name'.$i];
										}
									}
								}
							}
						}
					} else {
						//upload usual file
						$msg = '';
						if(JLMS_Files::uploadFile( $_FILES['userfile'.$i]['tmp_name'], $_FILES['userfile'.$i]['name'], $msg )) {
							$tmp_folder = JPATH_SITE . "/tmp/";
							$zip_files_array[$tmp_folder][$_FILES['userfile'.$i]['name']] = $_FILES['userfile'.$i]['name'];
							$temporary_files[] = $tmp_folder.$_FILES['userfile'.$i]['name'];
							if (isset($_REQUEST['doc_name'.$i]) && $_REQUEST['doc_name'.$i]) {
								$zip_files_names[$tmp_folder][$_FILES['userfile'.$i]['name']] = $_REQUEST['doc_name'.$i];
							}
						} else {
							$JLMS_CONFIG->set('files_skipped_by_extension', true);
						}
					}
				} else {
					if(isset($_REQUEST['doc_name'.$i]) && $_REQUEST['doc_name'.$i]){
						$no_files_array[] = $_REQUEST['doc_name'.$i];
					}
				}
			}
		} else {
			//new folder, without $_FILES array
			$i = 0;
			if(isset($_REQUEST['doc_name'.$i]) && $_REQUEST['doc_name'.$i]){
				$no_files_array[] = $_REQUEST['doc_name'.$i];
			}
		}

		/* populate default properties */
		$default_row = new stdClass();
		$default_row->published = intval(mosGetParam($_REQUEST, 'published', 0));
		$default_row->publish_start = 0;
		$default_row->publish_end = 0;
		$default_row->start_date = '';
		$default_row->end_date = '';
		$default_row->days = intval(mosGetParam($_POST, 'days', ''));
		$default_row->hours = intval(mosGetParam($_POST, 'hours', ''));
		$default_row->mins = intval(mosGetParam($_POST, 'mins', ''));
		$default_row->is_time_related = intval(mosGetParam($_REQUEST, 'is_time_related', 0));
		$default_row->show_period = 0;

		if ($default_row->is_time_related) {
			$default_row->show_period = JLMS_HTML::_('showperiod.getminsvalue', $default_row->days, $default_row->hours, $default_row->mins );
		}
		if ($id && $item_permissions->publish && $parent_permissions->publish_childs) {
			$i_can_publish_it = true;
		} elseif(!$id && $parent_permissions->publish_childs) {
			$i_can_publish_it = true;
		} else {
			$i_can_publish_it = false;
		}
		// start-end publishing
		if ($i_can_publish_it) {
			$publish_start = intval(mosGetParam($_REQUEST, 'is_start', 0));
			$publish_end = intval(mosGetParam($_REQUEST, 'is_end', 0));
			$default_row->publish_start = $publish_start;
			$default_row->publish_end = $publish_end;
			if ($default_row->publish_start) {
				$default_row->start_date = mosGetParam($_REQUEST, 'start_date', '');
				$default_row->start_date = JLMS_dateToDB($default_row->start_date);
			} else { $default_row->start_date = ''; }
			if ($default_row->publish_end) {
				$default_row->end_date = mosGetParam($_REQUEST, 'end_date', '');
				$default_row->end_date = JLMS_dateToDB($default_row->end_date);
			} else { $default_row->end_date = ''; }
		} else {
			unset($default_row->published);
			unset($default_row->publish_start);
			unset($default_row->publish_end);
		}
		$default_row->parent_id = $parent_id;
		
		if( count ($zip_files_array)) {
			$first_saved_id = 0;
			foreach ($zip_files_array as $k=>$v) {
				$docname = (isset($zip_files_names[$k]) && $zip_files_names[$k]) ? $zip_files_names[$k] : array();
				$saved_id = 0;
				JLMSDocs_save_file_from_zip_array($v, $parent_id, substr($k,0,strlen($k)-1), $ordering, $saved_id, $default_row, $is_zip_pack, $docname);
				if (!$first_saved_id && $saved_id) {
					$first_saved_id = $saved_id;
				}
			}
			
			if ($first_saved_id) {
				$open_element = $first_saved_id;
				if ($parent_id) {
					$open_folder = $parent_id;
				}
			}
			foreach ($temporary_files as $temporary_file) {
				if (file_exists($temporary_file)) {
					if (is_dir($temporary_file)) {
						JLMS_Files::delFolder($temporary_file);
					} elseif (is_file($temporary_file)) {
						JLMS_Files::delFile($temporary_file);
					}
				}
			}
		} else {
			if ($id) {
				foreach ($no_files_array as $no_files_elem) { // only one item should be in this array
					$new_folder_flag = $folder_flag;
					if ($is_zip_pack) {
						$new_folder_flag = 2;
					}
					JLMSDocs_save_single_file ($no_files_elem, $new_folder_flag, $parent_id, '', $ordering, $default_row);
				}
				$open_element = $id;
				if ($parent_id) {
					$open_folder = $parent_id;
				}
			} else {
				$first_elem_id = 0;
				foreach ($no_files_array as $no_files_elem) {
					$new_folder_flag = $folder_flag;
					if ($is_zip_pack) {
						$new_folder_flag = 2;
					}
					$new_elem = JLMSDocs_save_single_file ($no_files_elem, $new_folder_flag, $parent_id, '', $ordering, $default_row);
					if (!$first_elem_id) {
						$first_elem_id = $new_elem;
					}
				}
				if ($first_elem_id) {
					$open_element = $id;
					if ($parent_id) {
						$open_folder = $parent_id;
					}
				}
			}
		}
	}
	if ($JLMS_CONFIG->get('zippack_was_uploaded', 0)) {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=docs_choose_startup&id=$course_id&doc_id=".$JLMS_CONFIG->get('zippack_was_uploaded', 0)) );
	} else {
		$msg = '';
		if ($JLMS_CONFIG->get('files_skipped_by_extension', false)) {
			$msg = _JLMS_DOCS_NOT_UPLOADED_DUE_TO_TYPE_RESTRICTION;
		}
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id"), $msg );
	}
}

function JLMS_saveorderDocuments($option){
	global $JLMS_DB, $JLMS_CONFIG, $Itemid;
	
	$course_id = $JLMS_CONFIG->get('course_id');
	
	$cid = josGetArrayInts( 'cid' );
	
	$total		= count( $cid );

	$order 		= josGetArrayInts( 'order' );

	$row 		= new mos_Joomla_LMS_Document( $JLMS_DB );
	$conditions = array();
	
//	echo '<pre>';
//	print_r($_SERVER); 
//	print_r($_SERVER['CONTENT_LENGTH']); 
//	print_r($cid); 
//	print_r($order); 
//	print_r($rrr); 
//	echo '</pre>';
	
	
	$query = "SELECT ordering FROM #__lms_documents WHERE course_id = 1"; // ORDER BY parent_id, ordering
	$JLMS_DB->setQuery($query);
	$results = JLMSDatabaseHelper::loadResultArray();
	/*
	?>
	<table border="1">
		<tr>
			<td>
			<?php
			echo '<pre>';
			print_r($cid);
			echo '</pre>';
			?>
			</td>
			<td>
			<?php
			echo '<pre>';
			print_r($results);
			echo '</pre>';
			?>
			</td>
			<td>
			<?php
			echo '<pre>';
			print_r($order);
			echo '</pre>';
			?>
			</td>
		</tr>
	</table>
	<?php
	die;
	*/
	
	$n = 0;
	
	// update ordering values
	for( $i=0; $i < $total; $i++ ) {
		if($results[$i] != $order[$i]){
			$n++;
			$row->load( (int) $cid[$i] );
			if ($row->ordering != $order[$i]) {
				$row->ordering = $order[$i];
				if (!$row->store()) {
					echo "<script> alert('".$JLMS_DB->getErrorMsg()."'); window.history.go(-1); </script>\n";
					exit();
				} // if
				// remember to updateOrder this group
				$condition = "parent_id = " . (int) $row->parent_id . " AND course_id = " . (int) $row->course_id . "";
				$found = false;
				foreach ( $conditions as $cond )
					if ($cond[1]==$condition) {
						$found = true;
						break;
					} // if
				if (!$found) $conditions[] = array($row->id, $condition);
			} // if
		}
	} // for
	
	// execute updateOrder for each group
	foreach ( $conditions as $cond ) {
		$row->load( $cond[0] );
		$row->updateOrder( $cond[1] );
	} // foreach
	
	// clean any existing cache files
	mosCache::cleanCache( 'com_joomla_lms' );
	
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
}

// (27.10) new algorithm to publish/unpublish documents
// How it's work:
//		1. if documents publishing - all parent folders publish
//		2. if folder publishing - p.1 + all child documents publish
//		3. if folder unpublishing - all child documents unpublish
function JLMS_changeDoc( $option) {
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');
	if ( $course_id ) {
		$state = intval(mosGetParam($_REQUEST, 'state', 0));
		if ($state != 1) { $state = 0; }
		$cid = mosGetParam( $_REQUEST, 'cid', array(0) );
		$cid2 = intval(mosGetParam( $_REQUEST, 'cid2', 0 ));
		if ($cid2) {
			$cid = array();
			$cid[] = $cid2;
		}
		if (!is_array( $cid )) {
			$cid = array(0);
		}
		if ( !is_array( $cid ) || count( $cid ) < 1 || empty( $cid ) ) {
			$action = 1 ? 'Publish' : 'Unpublish';
			echo "<script> alert('Select an item to $action'); window.history.go(-1);</script>\n";
			exit();
		}
		$rows = array();
		$possibilities = new stdClass();
		JLMSDocs::FillList($course_id, $rows, $possibilities);
		if ($possibilities->publish) {
			$change_state_items = array();
			$new_cids = array();
			if ($state == 1) {
				// HERE: process 'parent' itmes
				$all_parents = array();
				foreach ($cid as $cid_one) {
					$item = JLMSDocs::GetItembyID($rows,$cid_one);
					if (!is_null($item)) {
						if (!$item->published) {
							if (isset($item->p_publish) && $item->p_publish) {
								$first_parent = JLMSDocs::GetItemParentID($rows, $cid_one);
								if ($first_parent) {
									$parent_id = $cid_one;
									$can_publish = true;
									$do_find_parents = true;
									$temp_parents = array();
									$anti_loop = 100;
									while ($do_find_parents) {
										$parent_id = JLMSDocs::GetItemParentID($rows, $parent_id);
										if ($parent_id) {
											$item = JLMSDocs::GetItembyID($rows,$cid_one);
											if (!is_null($item)) {
												if (!$item->published) {
													if (isset($item->p_publish) && $item->p_publish) {
														$temp_parents[] = $parent_id;
													} else {
														$can_publish = false;break;
													}
												}
											}
										} else {
											$do_find_parents = false;
										}
										$anti_loop --;
										if ($anti_loop < 1) {
											$do_find_parents = false;
										}
									}
									if ($can_publish) {
										$all_parents = array_merge($all_parents, $temp_parents);
										$new_cids[] = $cid_one;
									}
								} else {
									$new_cids[] = $cid_one;
								}
							}
						}
					}
				}
				if (count($all_parents)) {
					$change_state_items = array_merge($change_state_items, $all_parents);
				}
			} else {
				foreach ($cid as $cid_one) {
					$item = JLMSDocs::GetItembyID($rows,$cid_one);
					if (!is_null($item)) {
						if (isset($item->p_publish) && $item->p_publish) {
							$new_cids[] = $cid_one;
						}
					}
				}
			}
			if (count($new_cids)) {
				$change_state_items = array_merge($change_state_items, $new_cids);
			}
			// HERE: process all childs
			$unstate = abs($state - 1);
			if (count($new_cids)) {
				$all_change_items = & JLMSDocs::GetChildsToPublish($rows, $new_cids, true);
				if (count($all_change_items)) {
					$change_state_items = array_merge($change_state_items, $all_change_items);
				}
			}
			if (count($change_state_items)) {
				$change_state_items_str = implode( ',', $change_state_items );
				$query = "UPDATE #__lms_documents"
				. "\n SET published = $state"
				. "\n WHERE id IN ( $change_state_items_str ) AND course_id = $course_id"
				;
				$JLMS_DB->setQuery( $query );
				$JLMS_DB->query();
			}
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
}


// to do: udalenie failov i papok (HARD)
// fail udalyat' tol'ko togda kogda on ne ispol'zuetsa v 'learn_path' (esli budut 'public' files to proveryat' i shoby on drugimi ne ispol'zovalsya
// papki udalyat' tol'ko kogda ne ostaetsa ni odnogo vlogennogo elementa na vsex urovnyax vlogennosti
function JLMS_doDeleteDocuments( $option ) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');
	if ( $course_id && $JLMS_ACL->CheckPermissions('docs', 'manage') ) {
		$cid = mosGetParam( $_POST, 'cid', array(0) );
		if (!is_array( $cid )) {
			//$cid = array(0);
		} else {
			$cids = implode( ',', $cid );
			$all_childs = array();
			$do_find_childs = true;
			$parents = $cid;
			while($do_find_childs) {
				$parents_str = implode( ',', $parents );
				$query = "SELECT id FROM #__lms_documents WHERE course_id = '".$course_id."' AND parent_id IN ( $parents_str )";
				$JLMS_DB->SetQuery( $query );
				$childs = JLMSDatabaseHelper::LoadResultArray();
				if (count($childs)) {
					$all_childs = array_merge($all_childs, $childs);
					$parents = $childs;
					$do_find_childs = true;
				} else {
					$do_find_childs = false;
				}
			}
			$all_docs = array_merge($all_childs, $cid);
			$all_docs_ids = implode( ',', $all_docs );
			$query = "SELECT item_id FROM #__lms_learn_path_steps WHERE course_id = '".$course_id."' AND step_type = 2 AND item_id IN ( $all_docs_ids )";
			$JLMS_DB->SetQuery( $query );
			$cid_used = JLMSDatabaseHelper::LoadResultArray();
			$cid_new = array_diff($all_docs, $cid_used); // id's for deleting
			$cids_new = implode( ',', $cid_new );
			// deleting
			// 1. delete all files (folder_flag == 0)
			// folder_flag == 3 - from library
			$query = "SELECT id, file_id, folder_flag FROM #__lms_documents WHERE (folder_flag = 0 OR folder_flag = 3) AND course_id = '".$course_id."' AND id IN ( $cids_new )";
			$JLMS_DB->SetQuery( $query );
			$files_deleted = $JLMS_DB->LoadObjectList();
			$lms_docs_ids = array();
			if (count($files_deleted)) { //now delete files
				$lms_files_ids = array();
				foreach ($files_deleted as $file_del) {
					if($file_del->folder_flag !=3 )
					$lms_files_ids[] = $file_del->file_id;
					$lms_docs_ids[] = $file_del->id;
				}
				JLMS_deleteFiles($lms_files_ids);
				
				$lms_docs_ids_str = implode( ',', $lms_docs_ids );
				$query = "DELETE FROM #__lms_documents WHERE id IN ( $lms_docs_ids_str ) AND course_id = '".$course_id."'";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				
				//topics
				$query = "DELETE FROM #__lms_topic_items WHERE item_id IN ($lms_docs_ids_str) AND item_type = 2";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				//-------------
			}
			// 1b. delete all ZIP-packages (folder_flag == 2)
			$query = "SELECT id, file_id FROM #__lms_documents WHERE folder_flag = 2 AND course_id = '".$course_id."' AND id IN ( $cids_new )";
			$JLMS_DB->SetQuery( $query );
			$zips_deleted = $JLMS_DB->LoadObjectList();
			$lms_docs_ids = array();
			if (count($zips_deleted)) { //now delete zips
				$lms_zips_ids = array();
				foreach ($zips_deleted as $zip_del) {
					$lms_zips_ids[] = $zip_del->file_id;
					$lms_docs_ids[] = $zip_del->id;
				}
				$real_del_zip_ids = & JLMS_deleteZipPacks( $course_id, $lms_zips_ids);
				$lms_docs_ids = array();
				foreach ($real_del_zip_ids as $rdzi) {
					foreach($zips_deleted as $zd) {
						if ($zd->file_id == $rdzi) {
							$lms_docs_ids[] = $zd->id;
						}
					}
				}
				if (count($lms_docs_ids)) {
					$lms_docs_ids_str = implode( ',', $lms_docs_ids );
					$query = "DELETE FROM #__lms_documents WHERE id IN ( $lms_docs_ids_str ) AND course_id = '".$course_id."'";
					$JLMS_DB->SetQuery( $query );
					$JLMS_DB->query();
				}
			}
			// files and documents deleted
			// 2. delete folders (only if empty)
			$lms_folders_ids = array_diff($cid_new, $lms_docs_ids);
			if (count($lms_folders_ids)) {
				$query = "SELECT id, parent_id, folder_flag FROM #__lms_documents WHERE course_id= '".$course_id."'";
				$JLMS_DB->SetQuery( $query );
				$course_docs_data = $JLMS_DB->LoadObjectList();
				if (count($course_docs_data)) {
					$folders_for_del = array();
					foreach($lms_folders_ids as $lms_folder_id) {
						$i = 0;
						while ($i < count($course_docs_data)) {
							if ($course_docs_data[$i]->id == $lms_folder_id) {
								$for_delete = JLMS_isfolderempty($course_docs_data, $lms_folder_id);
								if ($for_delete) {
									$folders_for_del[] = $lms_folder_id;
								}
							}
							$i ++;
						}
					}
					if (count($folders_for_del)) {
						$folders_for_del_str = implode( ',', $folders_for_del );
						$query = "DELETE FROM #__lms_documents WHERE id IN ( $folders_for_del_str ) AND course_id = '".$course_id."' AND folder_flag = 1";
						$JLMS_DB->SetQuery( $query );
						$JLMS_DB->query();
					}
				}
			}
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=documents&id=$course_id") );
}
// (27.10) only for private use ( used in "JLMS_doDeleteDocuments()")
// return 'true' if folder is EMPTY (or if folder contains only EMPTY subfolders, subsubfolders..)
// (recurse function)
function JLMS_isfolderempty(&$elements, $id) {
	$is_empty = true;
	$j = 0;
	while ($j < count($elements)) {
		if ($elements[$j]->parent_id == $id) {//est' child
			if ($elements[$j]->folder_flag == 1) { //child - folder (nugno uznat' shto eta folder - pustaya
				$is_empty = JLMS_isfolderempty($elements, $elements[$j]->id);
			} else {// child - document
				$is_empty = false;
			}
		}
		$j ++;
	}
	return $is_empty;
}
?>