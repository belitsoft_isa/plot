<?php 
/**
* joomla_lms.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/
/* Set flag that this is a parent file */
if ( !defined( '_JLMS_EXEC' ) ) { define( '_JLMS_EXEC', 1 ); }

/* some definitions */
if (!defined('_JOOMLMS_FRONT_HOME')) { define('_JOOMLMS_FRONT_HOME', dirname(__FILE__)); }
if (!defined('DS')) { define( 'DS', DIRECTORY_SEPARATOR ); }

require_once(_JOOMLMS_FRONT_HOME . DS . 'includes' . DS . 'component.legacy.php');
require_once(_JOOMLMS_FRONT_HOME . DS . 'includes' . DS . 'lms_legacy.php');
require_once(_JOOMLMS_FRONT_HOME . DS . 'includes' . DS . 'forums' . DS . 'smf' . DS .'smf.php');

/* JLMSFactory class (need for Database definition) */
require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.factory.php");

require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "libraries" . DS . "lms.lib.sef.php");

/* Database class ( TODO: do not use global varibale and use class store for global vars - as in J1.5 ) */
$GLOBALS['JLMS_DB'] = & JLMSFactory::getDB();
global $JLMS_DB, $Itemid, $option; /* needed for quesries in this file (i.e. license check); */

$Itemid = JRequest::getInt('Itemid', 0);
$option = 'com_joomla_lms';

//smt tmp
/*smt*/ if (JLMS_J30version()){JHtml::_('behavior.framework', true);}

/* require some files */
require_once(_JOOMLMS_FRONT_HOME . DS . "joomla_lms.func.php");
require_once(_JOOMLMS_FRONT_HOME . DS . "joomla_lms.html.php");
require_once(_JOOMLMS_FRONT_HOME . DS . "joomla_lms.class.php");

/* 'JLMSObject' - 'php 4.x.x class __construct hack' (need for SESSION class) */
require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.class.php");
require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.css.php");
/* TABS class - temporary it requires here */
//TODO: move this require to the functions where tabs are used
require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.tabs.php");

/* PARAMS class - temporary it requires here */
//TODO: move this require to the functions where params are used.... + jparameter will be no longer supported in Joomla 1.6 / 1.7
if (JLMS_Jversion() == 2) {
	require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.params.new.php");
} else {
	require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.params.php");
}

// User Agent
require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.user_agent.php");
$GLOBALS['JLMS_UA'] = new JLMS_UserAgent();
global $JLMS_UA;

/* some global variables */
global $my, $Itemid;
//var_dump($task); die();
/* GET security code from the CB captcha plugin */
$code = '';
if ($task == 'saveregisters'){ if (!session_id()){ session_start(); } if (isset($_SESSION['security_code'])) { $code = $_SESSION['security_code']; } }
$GLOBALS['JLMS_SESSION'] = JLMSFactory::getSession();
global $JLMS_SESSION;
if ($code) { $JLMS_SESSION->set('security_code', $code); }

/* CONFIG class */
$GLOBALS['JLMS_CONFIG'] = JLMSFactory::getConfig();
global $JLMS_CONFIG;

if ($JLMS_CONFIG->get('use_secure_checkout', false) || $JLMS_CONFIG->get('use_secure_enrollment', false)) {
	if( $_SERVER['SERVER_PORT'] == 443 || @$_SERVER['HTTPS'] == 'on') {

		$JLMS_CONFIG->set('under_ssl', true);
		$task = mosGetParam( $_REQUEST, 'task', '' );
		//$ssl_redirect = mosGetParam( $_REQUEST, 'ssl_redirect', 0 );
		/*if ($ssl_redirect == 2) {
			$JLMS_CONFIG->set('under_lms_ssl', true);
		}*/
		$JLMS_CONFIG->set('real_live_site','');
		if ( !$JLMS_CONFIG->get('always_under_ssl', false) ) { // if site doesn't work under SSL everytime - then do hack fixes ;)
			if (substr( JURI::base(), 0, 8 ) == 'https://') {
				$dom_name = JURI::base();
				if (substr($dom_name, -1) == '/') {
					$dom_name = substr($dom_name, 0, -1);
				}
				$JLMS_CONFIG->set('real_live_site', 'http://'.substr( $dom_name, 8 ));
				if ($JLMS_CONFIG->get('use_secure_checkout', false) && (!$my->id || (($task != 'subscription' || $JLMS_CONFIG->get('use_cart', false)) && $task != 'subscribe' && $task != 'show_cart' && $task != 'apply_coupon_code' && $task != 'update_cart' && $task != 'remove_from_cart' && $task != 'checkout_cart'))) {
					if ($JLMS_CONFIG->get('use_secure_enrollment', false) && (!$my->id || ($task != 'spec_reg' && $task != 'spec_reg_answer'))) {
						//here in Joomla 1.0.x we were changed https live_site to http ... don't know how to do this in Joomla 1.5.x / 1.6.x
						//$JLMS_CONFIG->set('live_site', 'http://'.substr( $JLMS_CONFIG->get('live_site'), 8 )); - do not do thsi if we can't change Joomla live_site
						//TODO: maybe made redirect to non-ssl page???
					}
				}
			} else {
				// 06.12.2007 - if we are under https, but live_site (SEF bug?) is 'http' - change it!
				if ($JLMS_CONFIG->get('use_secure_checkout', false) && ((($task == 'subscription' && !$JLMS_CONFIG->get('use_cart', false)) || $task == 'subscribe' || $task == 'show_cart' || $task == 'apply_coupon_code' || $task == 'update_cart' || $task == 'remove_from_cart' || $task == 'checkout_cart'))) {					
					$JLMS_CONFIG->set('real_live_site', 'http://'.substr( substr_replace(JURI::root(), '', -1, 1), 7 ) );
					//here in Joomla 1.0.x we were changed http live_site to https ... don't know how to do this in Joomla 1.5.x / 1.6.x
				// 05.06.2008 - if we are under https, but live_site (SEF bug?) is 'http' - change it!
				} elseif ($JLMS_CONFIG->get('use_secure_enrollment', false) && ($task == 'spec_reg' || $task == 'spec_reg_answer')) {
					$JLMS_CONFIG->set('real_live_site', 'http://'.substr( substr_replace(JURI::root(), '', -1, 1), 7 ));
					//here in Joomla 1.0.x we were changed http live_site to https ... don't know how to do this in Joomla 1.5.x / 1.6.x
				}
			}
		}
	}
}

global $my;
if (!$my->id && class_exists('JFactory')) {
	//do not remove thsi code upon we fully Joomla 1.5.x native...if we still use $my - do not remove this code
	// fixes the $my global if the user was restored by the 'remember me' plugin (if 'remember me' ordering is lower than 'legacy')
	$user = JLMSFactory::getUser();
	if ($user->id) {
		if ($GLOBALS['my']->id === 0) {
			$GLOBALS['my']	= (object)$user->getProperties();
			$GLOBALS['my']->gid = $user->get('aid', 0);
		}
	}
}

/* some definitions */
if (!defined('_JOOMLMS_DOC_FOLDER')) { define('_JOOMLMS_DOC_FOLDER', $JLMS_CONFIG->get('jlms_doc_folder') . DS); }
if (!defined('_JOOMLMS_SCORM_FOLDER')) { define('_JOOMLMS_SCORM_FOLDER',  $JLMS_CONFIG->get('scorm_folder')); }
if (!defined('_JOOMLMS_SCORM_FOLDER_PATH')) { define('_JOOMLMS_SCORM_FOLDER_PATH',  JPATH_SITE . DS . $JLMS_CONFIG->get('scorm_folder')); }
if (!defined('_JOOMLMS_SCORM_PLAYER')) {
	if (file_exists(_JOOMLMS_FRONT_HOME . "/../../jlms_scorm_play.php")) {
		 define('_JOOMLMS_SCORM_PLAYER', "jlms_scorm_play.php"); 
	} else {
		 define('_JOOMLMS_SCORM_PLAYER', $JLMS_CONFIG->get('scorm_folder')); 
	}
}
/* detect Joomla 1.5 legacy mode (TODO: FIX THIS!!! (now legacy mode is detected always) */
if (class_exists('JFactory')) {	$JLMS_CONFIG->set('joomla15_legacy', true); } else { $JLMS_CONFIG->set('joomla15_legacy', false); }

/* set LMS site title */
if ($JLMS_CONFIG->get('jlms_title')) {
	$doc = JFactory::getDocument();
	$doc->SetTitle( $JLMS_CONFIG->get('jlms_title') );
}

/* ***		***		***		***		LICENSE SECTION		***		***		***		***		***/
/* LICENSE check (we are not debug this query (to prevent listing of this query in debug window) */
$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
$JLMS_DB->SetQuery( $query );
$total_students = $JLMS_DB->LoadResult();
$is_trial = false;
$users_limit = 100;
$lms_license_gqp = true;
$lms_license_reports = true;
$lms_license_roles = true;
if (function_exists("ioncube_file_is_encoded") && ioncube_file_is_encoded()) {
	$license_properties = array();
	if (function_exists('ioncube_license_properties')) {
		$license_properties = ioncube_license_properties();
		if (isset($license_properties['users']['value'])) {
			$users_limit = $license_properties['users']['value'];
		}
		if (isset($license_properties['reports']['value']) && $license_properties['reports']['value'] == 1) {
			$lms_license_reports = true;
		} else {
			$lms_license_reports = false;
		}
		if (isset($license_properties['gqp']['value']) && $license_properties['gqp']['value'] == 1) {
			$lms_license_gqp = true;
		} else {
			$lms_license_gqp = false;
		}
		if (isset($license_properties['roles']['value']) && $license_properties['roles']['value'] == 1) {
			$lms_license_roles = true;
		} else {
			$lms_license_roles = false;
		}
		if (isset($license_properties['type']['value']) && $license_properties['type']['value'] == 1) {
			$is_trial = false;
		} elseif (isset($license_properties['type']['value']) && $license_properties['type']['value'] == 2) {
			$is_trial = false;
		} else {
			$is_trial = true;
		}
	}
}
//$is_trial = true;//temporary
$JLMS_CONFIG->set('license_lms_gqp', $lms_license_gqp);
$JLMS_CONFIG->set('license_lms_reports', $lms_license_reports);
$JLMS_CONFIG->set('license_lms_roles', $lms_license_roles);
$JLMS_CONFIG->set('license_lms_users', $users_limit);

$JLMS_CONFIG->set('roles_management', $lms_license_roles);
$JLMS_CONFIG->set('show_statistics_reports', $lms_license_reports);
$JLMS_CONFIG->set('global_quest_pool', $lms_license_gqp);
$JLMS_CONFIG->set('lmspro_roles', $lms_license_roles);

$JLMS_CONFIG->set('is_trial', $is_trial);
$JLMS_CONFIG->set('license_cur_users', intval($total_students));
if ($users_limit) {
	if (intval($total_students) > $users_limit) {
		$JLMS_CONFIG->set('lms_isonline', 0);
		$JLMS_CONFIG->set('offline_message', '<div class="joomlalms_sys_message">Sorry user limit is exceeded, please contact your LMS administrator.</div>');
	}
}
// SET component heading, additional text notes etc. for trial version
if ($is_trial) {
	$JLMS_CONFIG->set('trial_reports_module_text', 'Note: this feature is available only in Trial and Professional edition of JoomlaLMS.<br /><a target="_blank" title="JoomlaLMS editions comparison" rel=nofollow href="http://www.joomlalms.com/lms/joomlalms-professional-edition-features.html">[view full editions comparison]</a>');
	$JLMS_CONFIG->set('trial_reports_module_heading', ' <font style="font-weight:bold" color="red">(PRO only)</font>');
	$JLMS_CONFIG->set('trial_reports_heading_text', ' <font color="red">(PRO only)</font>');

	$JLMS_CONFIG->set('trial_gqp_page_text', 'Note: this feature is available only in Trial and Professional edition of JoomlaLMS.<br /><a target="_blank" title="JoomlaLMS editions comparison" rel=nofollow  href="http://www.joomlalms.com/lms/joomlalms-professional-edition-features.html">[view full editions comparison]</a>');
	$JLMS_CONFIG->set('trial_gqp_heading_text', ' <font color="red">(PRO only)</font>');

	$JLMS_CONFIG->set('trial_custom_perm_page_text', 'Note: this feature is available only in Trial and Professional edition of JoomlaLMS.<br /><a target="_blank" title="JoomlaLMS editions comparison" rel=nofollow href="http://www.joomlalms.com/lms/joomlalms-professional-edition-features.html">[view full editions comparison]</a>');
	$JLMS_CONFIG->set('trial_custom_perm_heading_text', ' <font color="red">(PRO only)</font>');

	$JLMS_CONFIG->set('jlms_heading', 'JoomlaLMS Trial');

	$JLMS_CONFIG->set('license_lms_gqp', true);
	$JLMS_CONFIG->set('license_lms_reports', true);
	$JLMS_CONFIG->set('license_lms_roles', true);

	$JLMS_CONFIG->set('roles_management', true);
	$JLMS_CONFIG->set('show_statistics_reports', true);
	$JLMS_CONFIG->set('global_quest_pool', true);
	$JLMS_CONFIG->set('lmspro_roles', true);

	$lms_license_gqp = true;
	$lms_license_reports = true;
	$lms_license_roles = true;
}


/* ***		***		***		***		ONLINE		***		***		***		***		***/
if ($JLMS_CONFIG->get('lms_isonline')) {

/* TRACKING object */
require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.tracking.php");
$GLOBALS['Track_Object'] = new JLMS_Tracking($JLMS_CONFIG->get('tracking_enable'), $JLMS_DB);
global $Track_Object;

/* LANGUAGE array */
$GLOBALS['JLMS_LANGUAGE'] = array();
global $JLMS_LANGUAGE;

$JLMS_CONFIG->set('pre_default_language', $JLMS_CONFIG->get('default_language') );

/* JoomlaLMS main functions */
require_once(_JOOMLMS_FRONT_HOME . DS . "joomla_lms.main.php");

$mem_lim_global = $JLMS_CONFIG->get('memory_limit_global', 0);
if ($mem_lim_global) {
	JLMS_adjust_memory_limit($mem_lim_global);
}

//25.08.2007 (DEN) - set debug level for the database object
if ( method_exists($JLMS_DB, "jlms_debug")) {
	$JLMS_DB->jlms_debug($JLMS_CONFIG->get('debug_mode', false));
}

$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
$_JLMS_PLUGINS->loadBotGroup('system');//TODO: 'system' group or 'user' group?
$plugin_args = array();
$_JLMS_PLUGINS->trigger('onBeforeJoomlaLMSStart', $plugin_args );

JLMS_require_lang($JLMS_LANGUAGE, 'main.lang', $JLMS_CONFIG->get('default_language'));

####### (3.03.2007) menu functions
require_once(_JOOMLMS_FRONT_HOME . "/includes/jlms_menu.php");

require_once(_JOOMLMS_FRONT_HOME . "/includes/jlms_acl.php");
require_once(_JOOMLMS_FRONT_HOME . "/includes/jlms_text_process.php");

require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "libraries" . DS . "lms.lib.discounts.php");

$JLMS_ACL = JLMSFactory::getACL();

$doc = JFactory::getDocument();
$doc->addScriptDeclaration("var JLMS_JURI = '".JURI::root()."';");

if($my->id){
	
	if( JLMS_J30version() ) 
	{
		JHtml::_('formbehavior.chosen', 'select:not(.chzn-done)');
	}
	
	setcookie('juser_id', $my->id);
	
	$plugin_args = array();
	$_JLMS_PLUGINS->trigger('onJoomlaLMSStart', $plugin_args );

################################################################

	$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) );
	$cid 	= mosGetParam( $_POST, 'cid', array(0) );
	if (!is_array( $cid )) {
		$cid = array(0);
	}	
	$task = mosGetParam( $_REQUEST, 'task', '' );	
	$course_id = intval( mosGetParam( $_REQUEST, 'course_id', 0 ) );
        
	if ( !$course_id  ) 
	{
		if( !in_array($task, array('api_scorm', 'playerSCORMFiles', 'loadsco_scorm', 'datamodel_scorm' ) ) ) 
		{
			$course_id = $id; 
		}
	}
			
	$option = 'com_joomla_lms';		
		
		if( JRequest::getVar('demo_users', 0) ){
			require_once(_JOOMLMS_FRONT_HOME . DS . 'create_users_sql.php'); //create demo users
		}
		
		if($course_id && $JLMS_CONFIG->get('enable_timetracking') && $task != 'timetracking' /*&& function_exists('TimeTracking')*/ && $Track_Object->TimeTrakingRequireJS()){
			$Track_Object->TimeTrakingOnJS($course_id, $my->id);
			$Track_Object->TimeTrackingActivate($course_id, $my->id);
		}                  
	//var_dump($task); die();
		switch ($task) {
	###############		MANUAL MAIL 		###############
		case 'manual_email':
			if(file_exists(JPATH_SITE . DS . 'jlms_email' . DS . 'library.jlms_email.php')){
				require_once(JPATH_SITE . DS . 'jlms_email' . DS . 'library.jlms_email.php');
				ManualMail::showManualMail();
			}
		break;		
	###############		PLUGIN ACTION 		###############
		case 'plugin_action':
			require_once(_JOOMLMS_FRONT_HOME . DS . 'joomla_lms.plugins.php');
		break;
	###############		TIME TRACKING		###############		
		case 'timetracking':
			$Track_Object->TimeTracking();
		break;	
	###############		ONLINE USERS		###############
		case 'online_users':			
			JLMS_processSettings( $course_id, true, false, false );
			$JLMS_SESSION->set('jlms_task', 'online_users');
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_ONLINE);
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.online.php");
		break;
	###############		SUBSCRIBE		###############
		case 'course_subscribe':
		case 'callback':
		case 'subscription':case 'subscribe': case 'subscriptions': 
		case 'add_to_cart': case 'checkout_cart': case 'update_cart': case 'remove_from_cart': case 'show_cart': case 'cart_login': case 'cart_register': case 'checkout_cart': case 'apply_coupon_code':
		case 'get_payment_invoice':
			JLMS_processSettings( $course_id, true, true, false );
			JLMS_require_lang($JLMS_LANGUAGE, array('subscription.lang', 'courses_main.lang'), $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_task', 'subscription');
			if ($JLMS_CONFIG->get('use_cart', false)) {				
				require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.cart.php");
			} else {				
				require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.subscriptions.php");
			}
		break;
	###############		QUIZZES		###############
		case 'quizzes':case 'show_quiz':case 'quiz_ajax_action':/*MAX noscript mod*/case 'quiz_action':case 'print_quiz_result':case 'print_quiz_cert':
		if ($JLMS_CONFIG->get('plugin_quiz')) {
			$page_quizzes = mosGetParam( $_REQUEST, 'page', '' );
			if ($lms_license_gqp) {
				if($JLMS_CONFIG->get('license_lms_gqp', 0)) {
				} else {
					JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid"));
				}
			}
			JLMS_processSettings( $course_id, true, true, false );
			
			if ($task == 'show_quiz' || $task == 'quiz_ajax_action' || $task == 'quiz_action') {
				$Track_Object->UserHit( $my->id, $course_id, JLMS_PAGE_TYPE_QUIZ );
				//$Track_Object->UserViewedItems( $my->id, $id, JLMS_PAGE_TYPE_QUIZ );
				//commented by DEN, ask me why
			}
			$JLMS_SESSION->set('jlms_task', 'quizzes');

			JLMS_require_lang($JLMS_LANGUAGE, array('quiz.lang', 'gradebook.lang', 'courses_main.lang'), $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_QUIZZES);
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.quiz.php");
		} else {
			JLMS_processLanguage( $JLMS_LANGUAGE );
			JLMS_disabledFunction($course_id);
		}
		break;
	###############		MAILBOX		###############
		case 'mailbox_reply':case 'mailbox':case 'letter_compose':case 'mail_send':case 'mail_sendbox':case 'mail_delete':case 'mail_view':case 'mailbox_new':case 'mfile_load':case 'mk_read':case 'mk_unread':
			if($task == 'mail_view') {
				//$Track_Object->UserViewedItems($my->id, JRequest::getInt('view_id'), JLMS_PAGE_TYPE_MAILBOX);
				//commented by DEN, ask me why
			}
			JLMS_processSettings( $course_id, true, true, false );
			$JLMS_SESSION->set('jlms_task', 'mailbox');
			JLMS_require_lang($JLMS_LANGUAGE, 'mailbox.lang', $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_MAILBOX);
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.mailbox.php");
		break;
	###############		LINKS		###############
		case 'links':case 'pre_link_edit':case 'pre_create_link'://case 'create_link':case 'update_link':
		case 'link_delete':case 'save_link':
		case 'link_orderup':case 'link_orderdown':case 'cancel_link':case 'change_link':case 'changeA_link':
		case 'view_inline_link':
			JLMS_processSettings( $course_id, true, true, false );
			$Track_Object->UserHit( $my->id, $course_id, JLMS_PAGE_TYPE_LINK );
			$JLMS_SESSION->set('jlms_task', 'links');
			JLMS_require_lang($JLMS_LANGUAGE, 'course_links', $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_LINKS);
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.course_links.php");
		break;
	###############		GRADEBOOK		###############
		case 'gradebook':case 'gb_items':case 'gb_user_pdf':
		case 'gbi_new':case 'gbi_edit':case 'gbi_editA':case 'gbi_save':case 'gbi_delete':case 'gbi_cancel':
		case 'gbi_orderup':case 'gbi_orderdown':case 'gb_crt':case 'gb_crtA':case 'gb_usergrade':
		case 'gb_certificates':case 'crt_save':case 'crt_preview':case 'gb_get_cert':case 'save_grades':
		case 'gb_scale':
		case 'gbs_new':case 'gbs_edit':case 'gbs_editA':case 'gbs_save':case 'gbs_delete':case 'gbs_cancel':
		case 'gbs_orderup':case 'gbs_orderdown':case 'gb_setup_path':case 'gb_save_path':case 'gb_del_path':
			if($task == 'gb_certificates') {
				//$Track_Object->UserViewedItems($my->id, $course_id, JLMS_PAGE_TYPE_CERT);
				//commented by DEN, ask me why
			}
			JLMS_processSettings( $course_id, true, true, true );
			$JLMS_SESSION->set('jlms_task', 'gradebook');
			JLMS_require_lang($JLMS_LANGUAGE, array('gradebook.lang', 'courses_main.lang'), $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_GRADEBOOK);			
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.gradebook.php");
		break;
	###############		VIEW TRACKING STATS		###############
		case 'tracking':case 'track_clear':case 'track_do_clear':
		case 'get_lp_stats':case 'get_docs_stats':case 'get_quiz_stats':
		if ($JLMS_CONFIG->get('tracking_enable')) {
			JLMS_processSettings( $course_id, true, true, false );
			$JLMS_SESSION->set('jlms_task', 'tracking');
			JLMS_require_lang($JLMS_LANGUAGE, array('tracking.lang', 'course_lpath', 'gradebook.lang', 'homework.lang', 'courses_main.lang'), $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_TRACK);
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.tracking.php");
		} else {
			JLMS_processLanguage( $JLMS_LANGUAGE );
			JLMS_disabledFunction($course_id);
		}
		break;
	###############		REGISTER OF ATTENDANCE		###############
		case 'attendance':case 'at_change':case 'at_periodchange':case 'at_userattend':case 'at_uchange':case 'at_dateschange':
		case 'at_pre_export': case 'at_export':
			JLMS_processSettings( $course_id, true, true, true );
			if ($JLMS_CONFIG->get('course_attendance')) {
				$JLMS_SESSION->set('jlms_task', 'attendance');
				JLMS_require_lang($JLMS_LANGUAGE, array('attendance.lang','agenda.lang'), $JLMS_CONFIG->get('default_language'));
				JLMS_processLanguage( $JLMS_LANGUAGE );
				$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_ATTEND);
				require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.attendance.php");
			} else {
				JLMS_processLanguage( $JLMS_LANGUAGE );
				JLMS_disabledFunction($course_id);
			}
		break;
	###############		HOMEWORK	###############
		case 'homework':
		case 'hw_publish': case 'hw_create':case 'hw_edit':case 'hw_delete':case 'hw_save':case 'hw_cancel':case 'hw_change':case 'hw_view':case 'hw_stats': case 'hw_tchange': case 'hw_uploadfile': case 'hw_view_result': case 'hw_save_result': case 'hw_downloadfile':
			JLMS_processSettings( $course_id, true, true, true );			
			if ($JLMS_CONFIG->get('course_homework')) {				
				$Track_Object->UserHit( $my->id, $course_id, JLMS_PAGE_TYPE_HW );				
				if($task == 'hw_view') {
					//$Track_Object->userViewedItems($my->id, JRequest::getInt('id'), JLMS_PAGE_TYPE_HW);
					//commented by DEN, ask me why
				}
				$JLMS_SESSION->set('jlms_task', 'homework');
				JLMS_require_lang($JLMS_LANGUAGE, array('homework.lang','course_users.lang'), $JLMS_CONFIG->get('default_language'));
				JLMS_processLanguage( $JLMS_LANGUAGE );
				$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_HOMEWORK);
				require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.homework.php");
			} else {
				JLMS_processLanguage( $JLMS_LANGUAGE );
				JLMS_disabledFunction($course_id);
			}
		break;
	###############		LEARNING PATHS's	###############
		case 'learnpaths':case 'learnpath':case 'add_lpath_step':
		case 'lpath_orderup':case 'lpath_orderdown':case 'lpath_delete':
		case 'new_lpath':case 'new_lpath_scorm':
		case 'edit_lpath':case 'edit_lpathA':case 'save_lpath':case 'change_lpath':case 'changeA_lpath':case 'cancel_lpath':case 'cancel_scormlink':
		case 'compose_lpath':case 'cancel_lpath_step':
		case 'lpath_item_orderup':case 'lpath_item_orderdown':case 'lpath_saveorederall':
		case 'lpath_item_saveorder': /* Save Orders Items Steps (Max) */	
		case 'lpath_add_doc':case 'lpath_add_link':case 'lpath_add_chapter':
		case 'new_lpath_chapter':case 'lpath_save_chapter':
		case 'lpath_save_doc':case 'lpath_save_link':
		case 'lpath_save_quiz': /* 1.0.1 */
		case 'lpath_save_scorm': /* 1.0.4 */
		case 'lpath_item_edit':
		case 'lpath_add_content': case 'lpath_save_content': /* (new 04.11) */
		case 'lpath_item_delete':
		case 'lpath_step_cond': case 'lpath_save_cond': case 'lpath_cond_delete':

		case 'download_scorm':
		case 'show_lp_content':
		case 'lpath_add_quiz':
		case 'lpath_add_scorm': /* 1.0.4 */

		case 'lpath_add_prereq': case 'lpath_del_prereq':/* 13 August 2007 (DEN) */

		case 'new_scorm_from_library':
		case 'save_scormlink':	
		
			JLMS_processSettings( $course_id, true, true, false );
			$Track_Object->UserHit( $my->id, $course_id, JLMS_PAGE_TYPE_LPATH );
			$JLMS_SESSION->set('jlms_task', 'learnpaths');
			JLMS_require_lang($JLMS_LANGUAGE, array('course_lpath','courses_main.lang'), $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_LPATH);			
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.course_lpath.php");
		break;

	###############		SCORM		###############
        case 'aicc_task':
            JLMS_processSettings( $course_id, true, true, false );
            $Track_Object->UserHit( $my->id, $course_id, JLMS_PAGE_TYPE_LPATH );
            $JLMS_SESSION->set('jlms_task', 'learnpaths');
            JLMS_require_lang($JLMS_LANGUAGE, 'course_lpath', $JLMS_CONFIG->get('default_language'));
            JLMS_processLanguage( $JLMS_LANGUAGE );
            $JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_LPATH);
            require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.aicc.php");
        break;
		case 'player_scorm':
		case 'api_scorm':
		case 'loadsco_scorm':
		case 'datamodel_scorm':
		case 'ajaxgetcistatus':		
			JLMS_processSettings( $course_id, true, true, false );
			$Track_Object->UserHit( $my->id, $course_id, JLMS_PAGE_TYPE_LPATH );
			$JLMS_SESSION->set('jlms_task', 'learnpaths');
			JLMS_require_lang($JLMS_LANGUAGE, 'course_lpath', $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_LPATH);
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.scorm.php");
		break;

	###############		LEARNING PATHS's	STU		###############
		case 'show_lpath':
		case 'show_lpath_nojs':
                        # isHack start
                        $user = plotUser::factory();
                        if ($user->id) {
                            $user->courseMarkAsNotNew($course_id);
                        }
                        # isHack end
			JLMS_processSettings( $course_id, true, true, false );
			$Track_Object->UserHit( $my->id, $course_id, JLMS_PAGE_TYPE_LPATH_STU );
			$JLMS_SESSION->set('jlms_task', 'learnpaths');
			JLMS_require_lang($JLMS_LANGUAGE, array('quiz.lang', 'course_lpath'), $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_LPATH);
			
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.course_lpathstu.php");
		break;
	###############		USERS	###############
		case 'course_users':
		case 'user_csv_delete':case 'import_csv_delete':case 'cancel_csv_delete':case 'user_delete_yes2':
		case 'new_usergroup':case 'edit_usergroup':case 'save_usergroup':
		case 'cancel_usergroup':case 'cancel_user':case 'edit_user':
		case 'view_users':case 'edit_user':case 'edit_user_save':
		case 'add_user':case 'add_user_save':case 'delete_user':
		case 'import_users':case 'export_usergroup':
		case 'import_users_csv':
		case 'view_assistants':case 'user_delete_yes':case 'usergroup_delete':
			JLMS_processSettings( $course_id, true, false, true );
			$JLMS_SESSION->set('jlms_task', 'course_users');
			JLMS_require_lang($JLMS_LANGUAGE, 'course_users.lang', $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_USERS);
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.course_users.php");
		break;
	###############		AGENDA		###############
		case 'my_agenda':case 'agenda':
			JLMS_processSettings( $course_id, true, true, true );
			$Track_Object->UserHit( $my->id, $course_id, JLMS_PAGE_TYPE_AGENDA );
			$JLMS_SESSION->set('jlms_task', 'agenda');
			JLMS_require_lang($JLMS_LANGUAGE, 'agenda.lang', $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_AGENDA);
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.agenda.php");
		break;
	###############		DROPBOX		###############
		case 'new_dropbox':case 'edit_dropbox': case 'del_dropbox':case 'drp_view_descr':
		case 'save_dropbox':case 'change_dropbox':
		case 'dropbox':case 'get_frombox':case 'cancel_dropbox':			
			JLMS_processSettings( $course_id, true, true, false );
			$Track_Object->UserHit( $my->id, $course_id, JLMS_PAGE_TYPE_DROPBOX );
			$JLMS_SESSION->set('jlms_task', 'dropbox');
			JLMS_require_lang($JLMS_LANGUAGE, 'course_dropbox', $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_DROP);
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.course_dropbox.php");
		break;
	###############		PAGE NOTICES		###############
		case 'new_notice':
		case 'new_notice_no_ajax':	
		case 'view_notice':
		case 'save_notice':
		case 'save_notice_no_ajax':
		case 'edit_notice':
		case 'edit_notice_no_ajax':
		case 'delete_notice':
		case 'delete_notice_no_ajax':
		case 'get_notice_count':
		case 'view_all_notices':
			JLMS_processSettings( 0, true, true, false );
			
			//$JLMS_SESSION->set('jlms_task', 'new_notice');
			JLMS_require_lang($JLMS_LANGUAGE, array('main.lang', 'courses_main.lang'), $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			//$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_DROP);
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.user_notices.php");
		break;	
	###############		COURSES		###############
		case 'courses': $course_id = 0; case 'add_course': case 'cancel_course': case 'edit_course': case 'save_course': case 'details_course':

		case 'export_course': case 'export_course_pre': case 'import_tpl':

		case 'course_import':
		case 'import': case 'import_succesfull':
		case 'change_course': case 'delete_course':case 'course_delete_yes':
		case 'settings': case 'settings_save':

		case 'topic': //view topic decription and all it's related info (e.g. jomcomments integration)	

		case 'add_topic': case 'edit_topic': case 'save_topic': case 'delete_topic':
		case 'save_allorder_topic': case 'orderup_topic': case 'orderdown_topic': 
		case 'publish_topic':
		
		case 'add_topic_element': case 'add_submit_topic_element': case 'delete_topic_element':
		case 'orderup_topic_element': case 'orderdown_topic_element': 
		case 'change_element': case 'orderup_element': case 'orderdown_element':
		
		case 'cancel_topic':
		case 'fcourse_orderup': case 'fcourse_orderdown': case 'fcourse_save_order':
		case 'course_guest': case 'add_prereq': case 'del_prereq':

			if ($task == 'course_guest') {
				JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$id") );
				exit();
			}

			JLMS_processSettings( $course_id, true, true, true );
			$JLMS_SESSION->set('jlms_section', '&nbsp;');//_JLMS_TOOLBAR_COURSES);
			if ($task == 'details_course') {
                                # isHack start
                                $user = plotUser::factory();
                                if ($user->id) {
                                    $user->courseMarkAsNotNew($course_id);
                                }
                                # isHack end
				$JLMS_SESSION->set('jlms_task', 'details_course');
				$Track_Object->UserHit( $my->id, $course_id, JLMS_PAGE_TYPE_COURSE );
			} else {
				$JLMS_SESSION->clear('switch_usertype');
				$JLMS_SESSION->set('jlms_task', 'courses');
			}
			JLMS_require_lang($JLMS_LANGUAGE, array('topics.lang', 'courses_main.lang', 'subscription.lang'), $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.courses.php");
		break;
	###############		DOCUMENTS		###############
		case 'documents':
		case 'pre_upload_file':case 'pre_create_folder':case 'pre_doc_edit':
		case 'upload_document':case 'create_folder':case 'doc_delete':case 'update_document':
		#case 'get_file':
		case 'new_document': case 'add_doclink':case 'save_doclink':
		case 'cancel_doc':case 'edit_doc':case 'save_doc':case 'new_folder':case 'change_doc':case 'changeA_doc': case 'get_document':
		case 'doc_orderup': case 'doc_orderdown': case 'doc_saveorder': 
		case 'docs_choose_startup': case 'docs_save_startup': case 'docs_view_zip': case 'docs_view_content':
		case 'documents_view_save':
			JLMS_processSettings( $course_id, true, true, false );
			$Track_Object->UserHit( $my->id, $course_id, JLMS_PAGE_TYPE_DOC );
			if($task == 'get_document') {				
				//$Track_Object->UserViewedItems($my->id, JRequest::getInt('id'), JLMS_PAGE_TYPE_DOC);
				//commented by DEN, ask me why
			}			
			$JLMS_SESSION->set('jlms_task', 'documents');
			JLMS_require_lang($JLMS_LANGUAGE, 'course_docs.lang', $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_DOCS);
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.docs.php");
		break;
	###############		DOCUMENTS OUT		###############
		case 'outdocs':
		case 'outpre_upload_file':case 'pre_create_outfolder':case 'pre_outdoc_edit':
		case 'upload_outdocument':case 'create_outfolder':case 'outdoc_delete':case 'update_outdocument':
		case 'new_outdocs':
		case 'new_scorm':	
		case 'save_scorm':
		case 'edit_scorm':
		case 'cancel_scorm':
		case 'playerSCORMFiles':
		case 'cancel_outdoc':case 'edit_outdoc':case 'save_outdoc':case 'new_outfolder':case 'change_outdoc':case 'changeA_outdoc':case 'get_outdoc':
		case 'outdoc_orderup': case 'outdoc_orderdown':
		case 'outdocs_choose_startup': case 'outdocs_save_startup': case 'outdocs_view_zip': case 'outdocs_view_content':
		case 'outdocs_view_save':			
			if($task == 'get_outdoc') {				
				//$Track_Object->UserViewedItems($my->id, JRequest::getInt('id'), JLMS_PAGE_TYPE_OUTDOC);
				//commented by DEN, ask me why
			}			
			JLMS_processSettings( 0, true, true, true );			
			$JLMS_SESSION->set('jlms_task', 'outdocs');
			JLMS_require_lang($JLMS_LANGUAGE, array('course_docs.lang', 'course_lpath', 'courses_main.lang'), $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_DOCS);
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.outdocs.php");
		break;	
	###############		CHAT		###################
		case 'chat':case 'get_chat_xml':case 'chat_post':
		if ($JLMS_CONFIG->get('chat_enable')) {
			JLMS_processSettings( $course_id, true, true, false );
			if ($JLMS_CONFIG->get('course_chat')) {
				JLMS_processSettings( $course_id );
				if ($task != 'get_chat_xml') {
					$Track_Object->UserHit( $my->id, $course_id, JLMS_PAGE_TYPE_CHAT );
				}
				$JLMS_SESSION->set('jlms_task', 'chat');
				JLMS_require_lang($JLMS_LANGUAGE, 'chat.lang', $JLMS_CONFIG->get('default_language'));
				JLMS_processLanguage( $JLMS_LANGUAGE );
				$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_CHAT);
				require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.chat.php");
			} else {
				JLMS_processLanguage( $JLMS_LANGUAGE );
				JLMS_disabledFunction($course_id);
			}
		} else {
			JLMS_processLanguage( $JLMS_LANGUAGE );
			JLMS_disabledFunction($course_id);
		}
		break;
	###############		CONFERENCE	 ##################
		case 'conference':
			if ($JLMS_CONFIG->get('conference_enable')) {
				JLMS_processSettings( $course_id, true, true, false );
				$Track_Object->UserHit( $my->id, $course_id, JLMS_PAGE_TYPE_CONFERENCE );
				$JLMS_SESSION->set('jlms_task', 'conference');
				JLMS_require_lang($JLMS_LANGUAGE, 'conference.lang', $JLMS_CONFIG->get('default_language'));
				$mode = strval(mosGetParam( $_REQUEST, 'mode', ''));
				if ($mode == 'params') {
					JLMS_processLanguage( $JLMS_LANGUAGE, true );
				} else {
					JLMS_processLanguage( $JLMS_LANGUAGE );
				}
				$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_CONF);
				require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.conference.php");
			} else {
				JLMS_processLanguage( $JLMS_LANGUAGE );
				JLMS_disabledFunction($course_id);
			}
		break;
	###############  SWITCH USER TYPE (Teacher/Student)  ##################
		case 'to_teacher':
			JLMS_processSettings( $course_id, true, true, true );
			JLMS_processLanguage( $JLMS_LANGUAGE );
			JLMS_SwitchUserType( 1, $option );
		break;
		case 'to_student':
			JLMS_processSettings( $course_id, true, true, true );
			JLMS_processLanguage( $JLMS_LANGUAGE );
			JLMS_SwitchUserType( 2, $option );
		break;
		case 'to_ceo':
			JLMS_processSettings( $course_id, true, true, true );
			JLMS_processLanguage( $JLMS_LANGUAGE );
			JLMS_SwitchUserType( 6, $option );
		break;
	###############  Courses forum  ##################
		case 'course_forum':
		case 'login_to_forum':
			JLMS_processSettings( $course_id, true, true, false );
			JLMS_processLanguage( $JLMS_LANGUAGE );
			if ($JLMS_CONFIG->get('plugin_forum') && $JLMS_CONFIG->get('course_forum')) {
				$Track_Object->UserHit( $my->id, $course_id, JLMS_PAGE_TYPE_FORUM );
				$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_FORUM);
				$JLMS_SESSION->set('jlms_task', 'course_forum');
				require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.course_forum.php");
			} else {
				JLMS_disabledFunction($course_id);
			}
		break;
		case 'ceo_page':
			JLMS_processSettings( 0 );
			JLMS_processLanguage( $JLMS_LANGUAGE );
			if ($JLMS_CONFIG->get('is_user_parent')) {
				$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_CEO_PARENT);
				$JLMS_SESSION->set('jlms_task', 'ceo_page');
				JLMS_showCEOPage( $option );
			} else {
				JLMS_disabledFunction($course_id);
			}
		break;
		case 'ceo_page_learner_courses':
			JLMS_processSettings( 0 );
			JLMS_processLanguage( $JLMS_LANGUAGE );
			if ($JLMS_CONFIG->get('is_user_parent')) {
				$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_CEO_PARENT);
				$JLMS_SESSION->set('jlms_task', 'ceo_page');
				JLMS_showCEOPage_learner_courses( $option );
			} else {
				JLMS_disabledFunction($course_id);
			}
		break;
		case 'enter_ceo':
			JLMS_processSettings( $course_id );
			JLMS_processLanguage( $JLMS_LANGUAGE );
			if ($JLMS_CONFIG->get('is_user_parent')) {
				$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_CEO_PARENT);
				$JLMS_SESSION->set('jlms_task', 'ceo_page');
				$JLMS_SESSION->set('switch_usertype', 6);
				JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$course_id"));
			} else {
				JLMS_disabledFunction($course_id);
			}
		break;
		case 'user_lang':
			$lang = strval(mosGetParam($_REQUEST, 'lang', ''));
			if ($lang) {
				$query = "SELECT lang_name FROM #__lms_languages WHERE lang_name = ".$JLMS_DB->Quote($lang)." AND published = 1";
				$JLMS_DB->SetQuery( $query );
				$lang = $JLMS_DB->LoadResult();
				if ($lang) {
					$JLMS_SESSION->set('lms_user_language', $lang);
				}
			}
			JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=".$JLMS_SESSION->get('jlms_task')."&id=".$course_id));
	##################   SPEC. REGISTRATION  ######################
		case 'spec_reg':
			JLMS_processSettings( $course_id );
			JLMS_processLanguage( $JLMS_LANGUAGE );
			JLMS_showSpecRegPage( $option, $course_id );
		break;
		case 'spec_reg_answer':
			JLMS_processSettings( $course_id );
			JLMS_processLanguage( $JLMS_LANGUAGE );
			JLMS_SpecRegAnswer( $option, $course_id );
		break;

		case 'keep_a_live':
			echo '1';JLMS_die();
		break;
		
	##################   NOTIFICATIONS SENDING   ##########
		case 'mail_main':	case 'mail_iframe':
			require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_mail_sender.php");
			break;
	##################   REPORTS   ##########
		case 'report_access':	case 'report_certif': case 'report_grade': case 'report_scorm':
			if ($lms_license_reports) {
				JLMS_processSettings( 0 );
				JLMS_require_lang($JLMS_LANGUAGE, 'courses_main.lang', $JLMS_CONFIG->get('default_language'));
				JLMS_require_lang($JLMS_LANGUAGE, 'course_users.lang', $JLMS_CONFIG->get('default_language'));
				JLMS_processLanguage( $JLMS_LANGUAGE );
				require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.reports.php");
			} else {
				JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid"));
			}
			break;
	
	# ---    Helper Users List    --- #
		case 'users_list':			Helper_UsersList::getUsers();						break;	
		
	##################   DEFAULT  ######################
		default:
			$JLMS_SESSION->clear('switch_usertype');
			JLMS_processSettings( 0 );
			JLMS_require_lang($JLMS_LANGUAGE, 'courses_main.lang', $JLMS_CONFIG->get('default_language'));
			JLMS_processLanguage( $JLMS_LANGUAGE );
			$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_HOME);
			JLMS_showTopPage( $option );
		break;
	}
} else { //not logged
//Show here Courses list for Guests/Spiders
	$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) );
	$task 	= mosGetParam( $_REQUEST, 'task', '' );
	$action = mosGetParam( $_REQUEST, 'action', '' );
	$atask = mosGetParam( $_REQUEST, 'atask', '' );
	if ($task =='keep_a_live') {
		echo '0';JLMS_die();
	}
	JLMS_process_menu_for_guests($option);
	if ($task == 'details_course') {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_guest&id=$id") );
		exit();
	}
	$do_scorm_stop = false;
	if ($task == 'playerSCORMFiles' ) {
		$id = intval($id);
		$query = "SELECT * FROM #__lms_n_scorm WHERE id = $id AND course_id = 0";
		$JLMS_DB->SetQuery($query);
		$tmp_scorm_check = $JLMS_DB->LoadObject();
		if (is_object($tmp_scorm_check) && isset($tmp_scorm_check->params)) {
			$tmp_scorm_params = new JLMSParameters($tmp_scorm_check->params);
			if ($tmp_scorm_params->get('scorm_layout', 0) && $tmp_scorm_params->get('scorm_layout', 0) == 1) {
				$do_scorm_stop = true;
			}
		}
	}

	if ($task == 'course_guest') {
		$JLMS_SESSION->set('jlms_section', '&nbsp;');//_JLMS_TOOLBAR_COURSES);
		$JLMS_SESSION->clear('switch_usertype');
		$JLMS_SESSION->set('jlms_task', 'courses');
		JLMS_require_lang($JLMS_LANGUAGE, array('subscription.lang', 'courses_main.lang'), $JLMS_CONFIG->get('default_language'));
		JLMS_processLanguage( $JLMS_LANGUAGE );
		if ($JLMS_UA->isRobot()) {
			JLMS_showCourseGuest( $id, $option );
		} elseif ($JLMS_CONFIG->get('guest_access_subscriptions', 1)) {
			JLMS_showCourseGuest( $id, $option, true );
		} else {
			JLMS_showCourseGuest( $id, $option );
		}	
	} elseif ( ($JLMS_CONFIG->get('guest_access_subscriptions', 1)) && ($task == 'course_subscribe' || $task == 'subscription' || $task == 'subscribe' || $task == 'subscriptions' || $task == 'subs_login' || $task == 'subs_register' || $task == 'subs_register_cb' || $task == 'add_to_cart' || $task == 'checkout_cart' || $task == 'update_cart' || $task == 'remove_from_cart' || $task == 'show_cart' || $task == 'apply_coupon_code' || $task == 'cart_login' || $task == 'cart_register' || $task == 'cart_register_cb') ){
	//SUBSCRIBE	FOR GUEST
		JLMS_require_lang($JLMS_LANGUAGE, array('subscription.lang', 'courses_main.lang'), $JLMS_CONFIG->get('default_language'));
		JLMS_processLanguage( $JLMS_LANGUAGE );
		if ($task == 'subs_register' || $task == 'subs_register_cb' || $task == 'subs_login' ) {
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.subscriptions.php");
		} elseif ($JLMS_CONFIG->get('use_cart', false)) {
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.cart.php");
		} else {
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.subscriptions.php");
		}
	} elseif ($task == 'callback') {
		if ($JLMS_CONFIG->get('use_cart', false)) {
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.cart.php");
		} else {
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.subscriptions.php");
		}
	} elseif ($task == 'playerSCORMFiles' && $do_scorm_stop ) {
		JLMS_require_lang($JLMS_LANGUAGE, 'main.lang', $JLMS_CONFIG->get('default_language'));
		JLMS_processLanguage( $JLMS_LANGUAGE );
		echo '<link href="'.JLMSCSS::link().'" rel="stylesheet" type="text/css" />';
		echo "<div class='joomlalms_user_message'>"._JLMS_NOT_AUTH_SESSION_EXPIRED."</div>";
		die;
	} elseif($task == 'manual_email'){
		//TODO: remoe this
		if(file_exists(JPATH_SITE . DS . 'jlms_email' . DS . 'library.jlms_email.php')){
			require_once(JPATH_SITE . DS . 'jlms_email' . DS . 'library.jlms_email.php');
			ManualMail::showManualMail();
		}
	} elseif($task == 'datamodel_scorm'){
		require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.scorm.php");
	} elseif($task == 'get_document'){
		require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.docs.php");
		JLMS_downloadDocument($id, $option);
	} else if($task == 'quiz_ajax_action' && in_array($atask, array('start', 'next', 'finish_stop', 'review_start', 'review_next', 'preview_quest', 'next_preview', 'goto_quest', 'resume_quiz', 'email_results'))){
		
		JLMS_require_lang($JLMS_LANGUAGE, array('main.lang', 'quiz.lang'), $JLMS_CONFIG->get('default_language'));
		JLMS_processLanguage( $JLMS_LANGUAGE );
		
		$_REQUEST['atask_org'] = $atask;
		$atask = 'message';
		
		require_once(_JOOMLMS_FRONT_HOME . "/includes/quiz/ajax_quiz.class.php");
		JLMS_quiz_ajax_class::JQ_ajax_main($atask);
		
		exit;
	} else if($task == 'show_lpath' && in_array($action, array('start_lpath', 'restart_lpath', 'next_lpathstep', 'prev_lpathstep', 'seek_lpathstep', 'get_lpath_doc'))){
		JLMS_require_lang($JLMS_LANGUAGE, array('main.lang', 'quiz.lang', 'course_lpath'), $JLMS_CONFIG->get('default_language'));
		JLMS_processLanguage( $JLMS_LANGUAGE );
		
		$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) );
		$_REQUEST['action_org'] = $action;
		$_REQUEST['action'] = 'message';
		
		require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.course_lpathstu.php");
		
		exit;
	} else {
		$JLMS_SESSION->set('jlms_section', '&nbsp;');//_JLMS_TOOLBAR_COURSES);
		$JLMS_SESSION->clear('switch_usertype');
		$JLMS_SESSION->set('jlms_task', 'courses');
		JLMS_require_lang($JLMS_LANGUAGE, 'courses_main.lang', $JLMS_CONFIG->get('default_language'));
		JLMS_processLanguage( $JLMS_LANGUAGE );
		if ($JLMS_UA->isRobot()) {
			JLMS_showCoursesForGuest($option);
		} elseif ($JLMS_CONFIG->get('guest_access_subscriptions', 1)) {
			JLMS_showCoursesForGuest($option, true);
		} else {
			JLMS_showCoursesForGuest($option);
		}
	}
	//echo _NOT_AUTH;
}
} else {//end if (lms_isonline)
	$head_tag = '<link href="'.JLMSCSS::link().'" rel="stylesheet" type="text/css" />';
	echo $head_tag;
	echo $JLMS_CONFIG->get('offline_message','');
}
$show_branding = true;
if (function_exists("ioncube_file_is_encoded") && ioncube_file_is_encoded()) {
	$license_properties = array();
	if (function_exists('ioncube_license_properties')) {
		$license_properties = ioncube_license_properties();
		if (isset($license_properties['branding']['value']) && $license_properties['branding']['value'] == 1) {
			if ($JLMS_CONFIG->get('branding_free_configured', 0)) {
				$show_branding = false;
			}
		}
	}
}
if ($show_branding) {
	require_once(_JOOMLMS_FRONT_HOME . DS . "joomla_lms.branding.php");
	$secret_hash_key = JLMS_showPoweredBy();
	if ($secret_hash_key == '08r5328arlji15889s6401pbd9roeiqe') {
	} else {
		$head_tag = '<link href="'.JLMSCSS::link().'" rel="stylesheet" type="text/css" />';
		echo '<div class="joomlalms_sys_message">JoomlaLMS files are corrupted</div>';
		die;
	}
}

// 16.08.2007 (DEN) - output all 'domready' events at the one place
if (function_exists('addDomReadyScript')) {
	addDomReadyScript();
}

if ( isset($_JLMS_PLUGINS) && is_object($_JLMS_PLUGINS) && method_exists($_JLMS_PLUGINS, 'loadBotGroup') ) {
	//this check is to avoid PHP fatal error in offline mode
	$_JLMS_PLUGINS->loadBotGroup('system');
	$plugin_args = array();
	$_JLMS_PLUGINS->trigger('onJoomlaLMSend', $plugin_args);
}

if ($JLMS_CONFIG->get('debug_mode', false)) {
	$do_debug = false;
	if ($JLMS_CONFIG->get('debug_user', -1) == -1) {
		$do_debug = true;
	} elseif ($JLMS_CONFIG->get('debug_user', -1) == $my->id) {
		$do_debug = true;
	}
	if ($do_debug) {
	if ($JLMS_CONFIG->get('debug_database', true)) {
	echo '<div class="joomlalms_sys_message">';
	if ( method_exists($JLMS_DB, "get_jlms_debug")) {

		global $JLMS_SESSION;
		$jlms_db_prev_log = $JLMS_SESSION->get('joomlalms_prev_debug_log', array() );
		if (!empty($jlms_db_prev_log)) {
			$i = 0;
			foreach ($jlms_db_prev_log as $jlms_log) {
				echo ($i? " $i ":'') . $jlms_log . "<br /><br /><br />";
				$i ++;
			}
		}
		$JLMS_SESSION->clear('joomlalms_prev_debug_log');

		echo '<pre>' . 'Total queries executed: ' . $JLMS_DB->get_jlms_ticker() . '</pre>';
		$i = 1;
		$jlms_db_log = $JLMS_DB->get_jlms_debug();
		foreach ($jlms_db_log as $jlms_log) {
			echo " $i " . $jlms_log . "<br /><br /><br />";
			$i ++;
		}
	}
	echo '</div>';
	}
	echo '<div class="joomlalms_sys_message" style="text-align:left">';
		$JLMS_ACL = JLMSFactory::getACL();
		echo '<pre>';
		print_r($JLMS_ACL);
		echo '</pre>';
	echo '</div>';
	}
}
if (isset($JLMS_CONFIG) && is_object($JLMS_CONFIG) && method_exists($JLMS_CONFIG, 'get')) {
	$html_at_the_end = $JLMS_CONFIG->get('add_html_at_the_end', '');
	if ($html_at_the_end) {
		echo $html_at_the_end;
	}
}
?>