<?php
/**
* joomla_lms.course_lpathstu.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.course_lpathstu.html.php");
$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) ); 
$task 	= mosGetParam( $_REQUEST, 'task', '' );
$action = mosGetParam( $_REQUEST, 'action', '' );
if ((!$action && $task == 'show_lpath') || $task == 'show_lpath_nojs') {
	global $JLMS_CONFIG, $Itemid, $option;
	$course_id = $JLMS_CONFIG->get('course_id',0);
	$pathway = array();
	$pathway[] = array('name' => _JLMS_PATHWAY_HOME, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"), 'is_home' => true);
	$pathway[] = array('name' => $JLMS_CONFIG->get('course_name'), 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$course_id"), 'is_course' => true);
	$pathway[] = array('name' => _JLMS_TOOLBAR_LPATH, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=learnpaths&amp;id=$course_id"));
	JLMSAppendPathWay($pathway);
	JLMS_ShowHeading();
}

switch ($task) {
	case 'show_lpath':			JLMS_show_LPath_Pre( $id, $option, $action );		break;
	case 'show_lpath_nojs':		JLMS_show_LPath_noJS_Pre( $id, $option, $action );	break;
}
function JLMS_show_LPath_Pre( $id, $option, $action ) {
	global $JLMS_CONFIG;

	$JLMS_CONFIG->set('process_without_js', false);
	
	//if($action && $action != 'start_lpath'){
	//	sleep(35);
	//}
	
	switch ($action) {
		case 'message':			JLMS_message_LPath( $id, $option );		break;
		
		case 'start_lpath':		JLMS_start_LPath( $id, $option );		break;
		case 'restart_lpath':	JLMS_restart_LPath( $id, $option );		break;
		case 'next_lpathstep':	JLMS_next_LPathStep( $id, $option );	break;
		case 'prev_lpathstep':	JLMS_seek_LPathStep($id, $option, -1);	break;//JLMS_prev_LPathStep( $id, $option );	break;
		case 'seek_lpathstep':	JLMS_seek_LPathStep( $id, $option );	break;
		case 'get_lpath_doc':	JLMS_get_LPathDoc( $id, $option );		break;
		case '':				JLMS_show_LPath( $id, $option );		break;
	}
}

function CurrentStep($step_item){
	global $JLMS_DB, $my, $JLMS_CONFIG;
	
	$query = "UPDATE #__lms_learn_path_results SET last_step_id = '".$step_item->id."' WHERE lpath_id = '".$step_item->lpath_id."' AND course_id = '".$step_item->course_id."' AND user_id = '".$my->id."'";
	$JLMS_DB->setQuery($query);
	$JLMS_DB->query();
}

function JLMS_show_LPath_noJS_Pre( $id, $option, $action ) {
	global $JLMS_CONFIG;

	$JLMS_CONFIG->set('process_without_js', true); // 20.03.2008 - DEN - noJS functionality

	switch ($action) {
		case 'start_lpath':		JLMS_start_LPath( $id, $option );		break;
		
		case 'next_lpathstep':	JLMS_next_LPathStep( $id, $option );	break;
		case 'prev_lpathstep':	JLMS_seek_LPathStep($id, $option, -1);	break;
		case 'restart_lpath':	JLMS_restart_LPath( $id, $option );		break;

		case 'contents_lpath':	JLMS_contents_LPath( $id, $option );	break; // 21.03.2008 - (DEN) - this function is only for noJS functionality !!
		case 'seek_lpathstep':	JLMS_seek_LPathStep( $id, $option );	break;
		case 'get_lpath_doc':	JLMS_get_LPathDoc( $id, $option );		break;
		case '':				JLMS_show_LPath( $id, $option );		break;
	}
}

// TODO: sm. inside (DONE)
function JLMS_get_LPathDoc( $lpath_id, $option ){
	global $JLMS_DB, $my, $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id');
	$usertype = $JLMS_CONFIG->get('current_usertype', 0);
	$send_doc = false;
	$ret_str = array();
	if ($my->id && $course_id && $usertype && JLMS_isValidLPath_ID($lpath_id, $course_id) ) {
		$user_unique_id = strval(mosGetParam($_REQUEST, 'user_unique_id', ''));
		$user_start_id = intval(mosGetParam($_REQUEST, 'user_start_id', 0));
		if (JLMS_isValidStartUnique($user_start_id, $user_unique_id, $course_id, $lpath_id)) {
			$doc_id = intval(mosGetParam($_REQUEST, 'doc_id', 0));
			$step_id = intval(mosGetParam($_REQUEST, 'step_id', 0));
			$query = "SELECT * FROM #__lms_learn_path_steps WHERE id = '".$step_id."' AND lpath_id = '".$lpath_id."'"
			. "\n AND course_id = '".$course_id."' AND step_type = 2 AND item_id = '".$doc_id."'";
			$JLMS_DB->SetQuery( $query );
			$st_data = $JLMS_DB->LoadObjectList();
			if (count($st_data) == 1) {
				$query = "SELECT file_id, doc_name, folder_flag FROM #__lms_documents WHERE id = '".$doc_id."' AND course_id = '".$course_id."'";// AND published = 1 ";
				$JLMS_DB->SetQuery( $query );
				$doc_data = $JLMS_DB->LoadObjectList();
				if (count($doc_data) == 1) {
					if($doc_data[0]->folder_flag == 3){
							$query = "SELECT a.*, b.file_name FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0  WHERE a.id=".$doc_data[0]->file_id;
							$JLMS_DB->SetQuery( $query );
							$out_row = $JLMS_DB->LoadObjectList();

							if(count($out_row)){
								if (isset($out_row[0]->allow_link) && $out_row[0]->allow_link) {
									$doc_data[0]->doc_name = $out_row[0]->doc_name;
									$doc_data[0]->file_id = $out_row[0]->file_id;
								} else {
									$doc_data[0]->doc_name = $st_data[0]->step_name;
									$doc_data[0]->file_id = 0;
								}
							} else {
								$doc_data[0]->doc_name = $st_data[0]->step_name;
								$doc_data[0]->file_id = 0;
							}
						}

					// TODO: allow download if this step pending or completed (DONE)
					$query = "SELECT a.* FROM #__lms_learn_path_step_results as a, #__lms_learn_path_results as b"
					. "\n WHERE a.result_id = b.id AND b.user_id = '".$my->id."' AND b.course_id = '".$course_id."'"
					. "\n AND b.lpath_id = '".$lpath_id."' AND a.step_id = '".$step_id."' AND a.step_status <> 0";
					$JLMS_DB->SetQuery( $query );
					$res_data = $JLMS_DB->LoadObjectList();
					if (count($res_data) == 1) {
						if ($doc_data[0]->file_id) {

							//tracking
							global $Track_Object;
							$Track_Object->UserDownloadFile( $my->id, $doc_id );

							// 30.11.2007 (DEN) - do not know where to place this 'markstepstatus' before or after file donwload
							// seems like after it doesn't work! (e.g. the file is too big and connection to db got lostduring preparing of the download)
							//TODO: (May 2010) - review this comment... possibly reconect to db will help
							$fake_element = new stdClass();
							JLMS_Track_MarkStepStatus($user_start_id, $course_id, $lpath_id, $step_id, $fake_element, 3);

							// headers-fix for AdobeFlashPlayer 10
							$headers = array();
							$force_download = strval(mosGetParam($_REQUEST, 'force', ''));
							if ($force_download == 'player') {
								$headers['Content-Disposition'] = 'inline';
							}

							JLMS_downloadFile($doc_data[0]->file_id, $option, $doc_data[0]->doc_name, false, $headers);
							//JLMS_Track_MarkStepStatus($user_start_id, $course_id, $lpath_id, $step_id, 3);
							exit();
						}
					}
				}
			}
		}
	}

	JLMS_downloadFakeFile();
}
function JLMS_show_LPath( $lpath_id, $option ) {
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG, $JLMS_SESSION;
	
	$course_id = $JLMS_CONFIG->get('course_id');
	$usertype = $JLMS_CONFIG->get('current_usertype', 0);
	$r = new stdClass();
	$r = JLMS_Check_LPath_ID_pre($lpath_id, $course_id, $usertype);

	if ($my->id && $course_id && $usertype && $r->result ) {
		if ($r->item_id) {
			$height = JRequest::getInt('height');
			$redirect_link = sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=player_scorm&id=$r->item_id&course_id=$course_id&lpath_id=$lpath_id".($height ? "&height=$height" : ''));
			JLMSRedirect( $redirect_link );
			exit();
		} elseif (!$r->course_id) {
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
		}
		// 01.03.2007 (parameter 'redirect_to_learnpath' used to avoid redirect cycles from course home to LP and back
		$JLMS_SESSION->clear('redirect_to_learnpath');

		$query = "SELECT * FROM #__lms_learn_paths WHERE id = '".$lpath_id."' AND course_id = '".$course_id."' AND published = 1";
		$JLMS_DB->SetQuery( $query );
		$lpath_data = $JLMS_DB->LoadObjectList();
		if (count($lpath_data) == 1) {
			$pathway = array();
			$pathway[] = array('name' => $lpath_data[0]->lpath_name, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=show_lpath&amp;course_id=$course_id&amp;id=".$lpath_data[0]->id));
			JLMSAppendPathWay($pathway);
			
			$lpath_contents = JLMS_GetLPath_Data($lpath_id, $course_id);
			$count_quizzes = 0;
			$quiz_ids = array();
			$quiz_steps = array();
			$quizzes_data = array();
			foreach ($lpath_contents as $lc) {
				if ($lc->step_type == 5) {
					$count_quizzes ++;
					$quiz_ids[] = $lc->item_id;
					$quiz_steps[] = $lc->id;
				}
			}
			if ($count_quizzes) {
				$qqi = 0;
				foreach ($quiz_ids as $qi) {
					$fquiz = new stdClass();
					$fquiz->c_id = $qi;
					$query = "SELECT a.id as result_id, b.stu_quiz_id FROM #__lms_learn_path_results as a, #__lms_learn_path_step_quiz_results as b WHERE a.user_id = '".$my->id."'"
					. "\n AND a.course_id = '".$course_id."' AND a.lpath_id = '".$lpath_id."' AND b.result_id = a.id AND b.step_id = ".$quiz_steps[$qqi];
					$JLMS_DB->SetQuery( $query );
					$stu_quiz_id = 0;
					$res_id = 0;
					$step_status = 0;
					$trtrtrt = $JLMS_DB->LoadObject();
					if (is_object($trtrtrt)) {
						$stu_quiz_id = $trtrtrt->stu_quiz_id;
						$res_id = $trtrtrt->result_id;
						if ($res_id) {
							$query = "SELECT step_status FROM #__lms_learn_path_step_results WHERE result_id = $res_id AND step_id = $quiz_steps[$qqi]";
							$JLMS_DB->SetQuery( $query );
							$rrtt = $JLMS_DB->loadResult();
							if ($rrtt == 1) {
								$step_status = 1;
							}
						}
					}

					// 27 April 2007 (DEN) - Question Pool support
					if ($stu_quiz_id && $step_status) {
						$query = "SELECT a.*, b.c_id as stu_quest, b.c_score as stu_score FROM #__lms_quiz_r_student_quiz_pool as qp, #__lms_quiz_t_question as a"
						. "\n LEFT JOIN #__lms_quiz_r_student_question as b ON b.c_stu_quiz_id = $stu_quiz_id AND b.c_question_id = a.c_id"
						. "\n WHERE qp.start_id = $stu_quiz_id AND qp.quest_id = a.c_id AND a.course_id = ".$course_id." ORDER BY qp.ordering";// (a.c_quiz_id = ".$this->quiz_id." OR (a.c_quiz_id = 0 AND )) ORDER BY b.ordering";
						$JLMS_DB->SetQuery( $query );
						$fquiz->panel_data = $JLMS_DB->LoadObjectList();
					} else {
						// old way
						/*$query = "SELECT a.*, b.c_id as stu_quest, b.c_score as stu_score FROM #__lms_quiz_t_question as a"
						. "\n LEFT JOIN #__lms_quiz_r_student_question as b ON b.c_stu_quiz_id = $stu_quiz_id AND b.c_question_id = a.c_id"
						. "\n WHERE a.c_quiz_id = '".$qi."' ORDER BY ordering, c_id";*/
						$fquiz->panel_data = array();
					}

					$fquiz->stu_quiz_id = $stu_quiz_id;

					$q_from_pool = array();
					$q_from_pool_gqp = array();
					foreach ($fquiz->panel_data as $row) {
						if ($row->c_type == 20) {
							$q_from_pool[] = $row->c_pool;
						}
						if ($row->c_type == 21) {
							$q_from_pool_gqp[] = $row->c_pool_gqp;
						}
					}
					if (count($q_from_pool) || count ($q_from_pool_gqp)) {
						$rows2 = array();
						$rows3 = array();
						if ($q_from_pool) {
							$qp_ids =implode(',',$q_from_pool);
							$query = "SELECT a.* FROM #__lms_quiz_t_question as a"
							. "\n WHERE a.course_id = ".$course_id." AND a.c_id IN ($qp_ids)";
							$JLMS_DB->setQuery( $query );
							$rows2 = $JLMS_DB->loadObjectList();
						}
						if ($q_from_pool_gqp) {
							$gqp_ids =implode(',',$q_from_pool_gqp);
							$query = "SELECT a.* FROM #__lms_quiz_t_question as a"
							. "\n WHERE a.course_id = 0 AND a.c_id IN ($gqp_ids)"
							//. "\n AND a.published = '1'"
							;
							$JLMS_DB->setQuery( $query );
							$rows3 = $JLMS_DB->loadObjectList();
						}
						for ($i=0, $n=count( $fquiz->panel_data ); $i < $n; $i++) {
							if ($fquiz->panel_data[$i]->c_type == 20) {
								for ($j=0, $m=count( $rows2 ); $j < $m; $j++) {
									if ($fquiz->panel_data[$i]->c_pool == $rows2[$j]->c_id) {
										$fquiz->panel_data[$i]->c_question = $rows2[$j]->c_question;
										$fquiz->panel_data[$i]->c_point = $rows2[$j]->c_point;
										$fquiz->panel_data[$i]->c_attempts = $rows2[$j]->c_attempts;
										$fquiz->panel_data[$i]->c_type = $rows2[$j]->c_type;
										break;
									}
								}
							}
							if ($fquiz->panel_data[$i]->c_type == 21) {
								for ($j=0, $m=count( $rows3 ); $j < $m; $j++) {
									if ($fquiz->panel_data[$i]->c_pool_gqp == $rows3[$j]->c_id) {
										$fquiz->panel_data[$i]->c_question = $rows3[$j]->c_question;
										$fquiz->panel_data[$i]->c_point = $rows3[$j]->c_point;
										$fquiz->panel_data[$i]->c_attempts = $rows3[$j]->c_attempts;
										$fquiz->panel_data[$i]->c_type = $rows3[$j]->c_type;
										break;
									}
								}
							}
						}
					}

					$ar_element = 'quiz_'.$qi;
					
					$query = "SELECT c_max_numb_attempts FROM #__lms_quiz_t_quiz"
					. "\n WHERE c_id = ".$qi;
					$JLMS_DB->setQuery( $query );
					$fquiz->c_max_numb_attempts = $JLMS_DB->loadResult();
					
					$query = "SELECT count(c_id) FROM #__lms_quiz_r_student_quiz WHERE c_quiz_id = ".$qi." ORDER BY c_date_time desc LIMIT 1";
					$JLMS_DB->SetQuery($query);
					$count = $JLMS_DB->LoadResult();
					
					$fquiz->attempts_of_this_quiz = $count;
					
					$quizzes_data[$ar_element] = $fquiz;
					$qqi ++;
				}
/*				$panel_str = "\t" . '<quiz_panel_data><![CDATA[';
				$query = "SELECT * FROM #__lms_quiz_t_question WHERE c_quiz_id = '".$quiz_id."' ORDER BY ordering, c_id";
				$JLMS_DB->SetQuery( $query );
				$panel_data = $JLMS_DB->LoadObjectList();
				$panel_str .= '<table id="jq_results_panel_table" width="100%" style="padding: 0px 20px 0px 20px" class="">';
				$k = 1;
				foreach ($panel_data as $panel_row) {
					$panel_str .= '<tr class="sectiontableentry'.$k.'"><td><a href="javascript:void(0)" onclick="javascript:JQ_gotoQuestionOn('.$panel_row->c_id.')">'.substr(strip_tags($panel_row->c_question),0,50).'</a></td><td width="40px" align="center">'.$panel_row->c_point.'</td><td width="25px" align="center"><div id="quest_result_'.$panel_row->c_id.'">-</div></td></tr>';
					$k = 3 - $k;
				}
				$panel_str .= '</table>]]></quiz_panel_data>' . "\n";
				return $panel_str;*/
			}
			JLMS_initialize_SqueezeBox(); //for scorm squeezebox within lpath
			JLMS_initialize_SqueezeBox(false);; //for links squeezebox within lpath
	
			global $Track_Object;
			if($JLMS_CONFIG->get('enable_timetracking', false) && method_exists($Track_Object, 'TimeTrakingOnJS_Resource')){
				$Track_Object->TimeTrakingOnJS_Resource($course_id, $my->id, 9, $lpath_id); //initialize timetracking
				
				if(isset($lpath_contents) && count($lpath_contents)){
					foreach($lpath_contents as $lpath_content){
						if($lpath_content->step_type == 5){
							$quiz_id = $lpath_content->item_id;
							$Track_Object->TimeTrakingOnJS_Resource($course_id, $my->id, 11, $quiz_id); //initialize timetracking
						}
					}
				}
			}
			
			JLMS_course_lpathstu_html::showLPath_MainPage( $course_id, $lpath_id, $option, $lpath_data[0], $lpath_contents, $quizzes_data );
		} else {
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
		}
	} else {

		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}

function JLMS_message_LPath($lpath_id, $option){
	
	$user = JFactory::getUser();
	
	if(!isset($user->id) || !$user->id){
		$user_id = isset($_COOKIE['juser_id']) ? $_COOKIE['juser_id'] : 0;
	}
	
	$course_id = JRequest::getVar('course_id', 0);
	
	$JLMS_CONFIG = JLMSFactory::getConfig();
	
	$action_org = mosGetParam( $_REQUEST, 'action_org', '' );
	
	$toolbar = array();
	if($action_org == 'start_lpath' || $action_org == 'restart_lpath'){
		$toolbar[] = array('btn_type' => 'quiz_ok', 'btn_js' => "javascript:ajax_action('start_lpath');");
	} else
	if($action_org == 'prev_lpathstep'){
		$toolbar[] = array('btn_type' => 'quiz_ok', 'btn_js' => "javascript:ajax_action('prev_lpathstep');");
	} else {
		$toolbar[] = array('btn_type' => 'quiz_ok', 'btn_js' => "javascript:ajax_action('next_lpathstep');");
	}
	
	$tool_txt = JLMS_ShowToolbar($toolbar);
	$tool_txt = str_replace('"components/com_joomla_lms', '"'.$JLMS_CONFIG->getCfg('live_site').'/components/com_joomla_lms', $tool_txt);
	
	/**/$ret_obj = array();
	/**/$ret_obj['task'] = new stdClass();
	/**/$ret_obj['task']->value = 'message';
	/**/$ret_obj['menu_contents'] = new stdClass();
	/**/$ret_obj['menu_contents']->value = $tool_txt;
	/**/$ret_obj['menu_contents']->is_cdata = true;
	
	$step_script = '';
	if($JLMS_CONFIG->get('enable_timetracking', false)){
		$step_script .= "TTracker_".$course_id."_".$user_id."_9_".$lpath_id.".stop_time();";
		
		$step_script .= "if(getObj('time_remaining')){ getObj('time_remaining').style.display = 'none'; }";
		$step_script .= "if(getObj('time_remaining_quiz')){ getObj('time_remaining_quiz').style.display = 'none'; }";
	}
	
	$ret_obj['step_exec_script']->value = '1';
	$ret_obj['step_exec_script_contents']->value = $step_script;
	
	JLMS_outputXML($ret_obj);
}

// TODO:	1. zanesti zapis' v lms_learn_path_results esli ee tam netu. (inache update status = 0) (DONE)
//			2. contents peresmatrivat' polnost'yu (pri starte) (pending, completed, incompleted)
function JLMS_start_LPath( $lpath_id, $option, $is_restart = false ) {
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;
	
	$course_id = $JLMS_CONFIG->get('course_id');
	$usertype = $JLMS_CONFIG->get('current_usertype', 0);
	$ret_str = array();
	$i = 0;
	$mark_complete = array();
	$mark_pending = array();
		
	if ($my->id && $course_id && $usertype && JLMS_isValidLPath_ID($lpath_id, $course_id) ) {
		$lpath_contents = JLMS_GetLPath_Data($lpath_id, $course_id);
		
		$query = "SELECT lp_params FROM #__lms_learn_paths WHERE id = $lpath_id";
		$JLMS_DB->SetQuery($query);
		$lp_params_str = $JLMS_DB->LoadResult();
		$lp_params = new JLMSParameters($lp_params_str);

		if (count($lpath_contents)) {
						
			//Max 2.04.08		
			if(!$lp_params->get('resume_last_attempt', 1)){
				$query = "SELECT id FROM #__lms_learn_path_results WHERE user_id = '".$my->id."'"
				. "\n AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
				$JLMS_DB->SetQuery( $query );
				$res_id = $JLMS_DB->LoadResult();
				if ($res_id) {
					$query = "DELETE FROM #__lms_learn_path_results WHERE id = '".$res_id."'";
					$JLMS_DB->SetQuery( $query );
					$JLMS_DB->query();
					$query = "DELETE FROM #__lms_learn_path_step_results WHERE result_id = '".$res_id."'";
					$JLMS_DB->SetQuery( $query );
					$JLMS_DB->query();
					$query = "DELETE FROM #__lms_learn_path_step_quiz_results WHERE result_id = '".$res_id."'";
					$JLMS_DB->SetQuery( $query );
					$JLMS_DB->query();
				}	
			}
			//end 2.04.08
			
			$new_unique_id = md5(uniqid(rand(), true));
			$start_time = gmdate( 'Y-m-d H:i:s');
			$query = "INSERT INTO #__lms_track_learnpath_stats (unique_id, user_id, course_id, lpath_id, start_time, user_status)"
			. "\n VALUES ('".$new_unique_id."', '".$my->id."', '".$course_id."', '".$lpath_id."', '".$start_time."', 0)";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			$new_start_id = $JLMS_DB->insertid();

			// insert record to '__lms_learn_path_results' if not exist
			$query = "SELECT count(*) FROM #__lms_learn_path_results WHERE user_id = '".$my->id."'"
			. "\n AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
			$JLMS_DB->SetQuery( $query );
			$c = $JLMS_DB->LoadResult();
			if (!$c) {
				$query = "INSERT INTO #__lms_learn_path_results (course_id, lpath_id, user_id, user_points, user_time, user_status, start_time, end_time)"
				. "\n VALUES ('".$course_id."', '".$lpath_id."', '".$my->id."', '0', '0', '0', '".$start_time."', '".$start_time."')";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
			}
			
			#$new_next_step = jlms_LPathPlay_findNextStep($lpath_contents, 0);
			$i = 0;
			JLMS_Track_MarkStepStatus($new_start_id, $course_id, $lpath_id, $lpath_contents[$i]->id, $lpath_contents[$i], 2);

			$mark_complete = JLMS_Track_processSteps($new_start_id, $course_id, $lpath_id, $lpath_contents, $i);
			$mark_incomplete = array();
			
			/*Navigation model + last attempt to return to the same step*/
			$navigation_model_switch = $lp_params->get('navigation_model', 0);
			$resume_last_attempt = $lp_params->get('resume_last_attempt', 1);

			$query = "SELECT last_step_id FROM #__lms_learn_path_results WHERE lpath_id = '".$lpath_contents[$i]->lpath_id."' AND course_id = '".$lpath_contents[$i]->course_id."' AND user_id = '".$my->id."'";
			$JLMS_DB->setQuery($query);
			$last_step_id = $JLMS_DB->loadResult();
			if($navigation_model_switch || $resume_last_attempt){
				if(isset($last_step_id) && count($last_step_id)){
					$new_next_step_index = 0;
					for($k=0;$k<count($lpath_contents);$k++){
						if($lpath_contents[$k]->id == $last_step_id && $last_step_id){
							$new_next_step_index = $k;
							break;
						}
					}
				}
			}
			CurrentStep($lpath_contents[$i]);
			/*Navigation model + last attempt to return to the same step*/
			
			if (isset($lpath_contents[$i]->new_step_status) && ($lpath_contents[$i]->new_step_status == 1)){
				//next step (now this) already completed
				$new_next_step = jlms_LPathPlay_findNextStep($lpath_contents, $i);
				//$new_next_step != -1				
				if($new_next_step != -1 && ($navigation_model_switch || $resume_last_attempt)){
					$new_next_step = $new_next_step_index;
				} 			
				
				echo '$navigation_model_switch='.$navigation_model_switch;
				echo "\n";
				echo '$resume_last_attempt='.$resume_last_attempt;
				echo "\n";
				echo '$new_next_step='.$new_next_step;
				
				if ( isset($lpath_contents[$new_next_step]) ) {					
					JLMS_Track_MarkStepStatus($new_start_id, $course_id, $lpath_id, $lpath_contents[$new_next_step]->id, $lpath_contents[$new_next_step], 2);
					$mark_pending = JLMS_getPendingSteps($lpath_contents);

					// if Learning Path was edited by teacher (teacher is added new steps) - then mark LPath result as uncompleted.
					$query = "UPDATE #__lms_learn_path_results SET user_status = 0 WHERE user_id = '".$my->id."'"
					. "\n AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
					$JLMS_DB->SetQuery( $query );
					$JLMS_DB->query();

					$ret_str = jlms_getXML_start($lpath_contents, $new_next_step, $mark_complete, $new_unique_id, $new_start_id, $mark_pending, $is_restart, $lp_params);
				} else {
					if (count($mark_complete)) {
						$c_steps = implode(',',$mark_complete);
					} else { $c_steps = 0; }
					$t_ar = JLMS_writeCompleteStats($new_start_id, $new_unique_id, $course_id, $lpath_id);
					$ret_str = JLMS_finishLPath($lpath_id, $option, $c_steps, $t_ar, true, $new_unique_id, $new_start_id);
				}
			} else {
				$mark_pending = JLMS_getPendingSteps($lpath_contents);

				// if Learning Path was edited by teacher (teacher is added new steps) - then mark LPath result as uncompleted.
				$query = "UPDATE #__lms_learn_path_results SET user_status = 0 WHERE user_id = '".$my->id."'"
				. "\n AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();

				$ret_str = jlms_getXML_start($lpath_contents, $i, $mark_complete, $new_unique_id, $new_start_id, $mark_pending, $is_restart, $lp_params);
			}
		}
	}
	if ($JLMS_CONFIG->get('process_without_js', false)) {
		 // 20.03.2008 - DEN - noJS functionality
		if(empty($ret_str)) {
			JLMS_course_lpathstu_html::show_ErrorMessage(_JLMS_LPATH_LOAD_DATA_ERROR);
		} else {
			$ret_str['mark_complete'] = new stdClass(); $ret_str['mark_complete']->value = $mark_complete;
			$ret_str['mark_pending'] = new stdClass(); $ret_str['mark_pending']->value = $mark_pending;
			$query = "SELECT lpath_name FROM #__lms_learn_paths WHERE id = $lpath_id";
			$JLMS_DB->SetQuery( $query );
			$lpath_name = $JLMS_DB->LoadResult();
			JLMS_course_lpathstu_html::show_NoJs_Page2($ret_str,$lpath_name, $lpath_contents, $i);
		}
	} elseif (!$JLMS_CONFIG->get('process_without_js', false)) {
		JLMS_outputXML($ret_str);
	}
}
// TODO: zateret' vse zapisi v lms_learn_path_step_results + v lms_learn_path_results zapisat' status = 0 (updatom ili insertom) (DONE)
function JLMS_restart_LPath( $lpath_id, $option ) {
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id');
	$usertype = $JLMS_CONFIG->get('current_usertype', 0);
	$ret_str = '';
	
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$_JLMS_PLUGINS->loadBotGroup('system');
	$plg_params = array();
	$plg_params['course_id'] = $JLMS_CONFIG->get('course_id');
//	$plg_params['lpath_id'] = $lpath_contents[$i]->lpath_id;
	$plg_params['lpath_id'] = $lpath_id;
	$plugin_results = $_JLMS_PLUGINS->trigger('onCheckRestartLpath', array(&$plg_params));
	$allow_lpath_restart = 1;
	if(isset($plugin_results) && count($plugin_results)){
		foreach($plugin_results as $plg_result){
			if(!$plg_result){
				$allow_lpath_restart = 0;
				break;
			}
		}
	}
	
	if ($allow_lpath_restart && $my->id && $course_id && $usertype && JLMS_isValidLPath_ID($lpath_id, $course_id) ) {
		$user_unique_id = strval(mosGetParam($_REQUEST, 'user_unique_id', ''));
		$user_start_id = intval(mosGetParam($_REQUEST, 'user_start_id', 0));
		if (JLMS_isValidStartUnique($user_start_id, $user_unique_id, $course_id, $lpath_id)) {
		
			// delete records from '__lms_learn_path_results' and from '__lms_learn_path_step_results'
			$query = "SELECT id FROM #__lms_learn_path_results WHERE user_id = '".$my->id."'"
			. "\n AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
			$JLMS_DB->SetQuery( $query );
			$res_id = $JLMS_DB->LoadResult();
			if ($res_id) {
				$query = "DELETE FROM #__lms_learn_path_results WHERE id = '".$res_id."'";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$query = "DELETE FROM #__lms_learn_path_step_results WHERE result_id = '".$res_id."'";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$query = "DELETE FROM #__lms_learn_path_step_quiz_results WHERE result_id = '".$res_id."'";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
			}

			JLMS_start_LPath($lpath_id, $option, true);
			if ($JLMS_CONFIG->get('process_without_js', false)) {
			} else {
				exit();
			}
		}
	}
	if (!$JLMS_CONFIG->get('process_without_js', false)) {
		JLMS_outputXML($ret_str);
	}
}
function JLMS_contents_LPath( $lpath_id, $option ) {
	// 21.03.2008 - (DEN) - this function is only for noJS functionality !!
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id');
	$usertype = $JLMS_CONFIG->get('current_usertype', 0);
	$step_id = intval(mosGetParam($_REQUEST, 'step_id', 0));
	// Max 25.03.08
	$quiz_id = intval(mosGetParam($_REQUEST, 'quiz_id', 0));
	$stu_quiz_id = intval(mosGetParam($_REQUEST, 'stu_quiz_id', 0));
	$quest_id = intval(mosGetParam($_REQUEST, 'quest_id', 0));
	
	$ret_str = '';
	$show_error = true;
	if ($my->id && $course_id && $usertype && JLMS_isValidLPath_ID($lpath_id, $course_id) ) {
		$user_unique_id = strval(mosGetParam($_REQUEST, 'user_unique_id', ''));
		$user_start_id = intval(mosGetParam($_REQUEST, 'user_start_id', 0));
		if (JLMS_isValidStartUnique($user_start_id, $user_unique_id, $course_id, $lpath_id)) {
			$lpath_contents = JLMS_GetLPath_Data($lpath_id, $course_id);

			$i = 0;
			while ($i < count($lpath_contents)) {
				if ($lpath_contents[$i]->id == $step_id) {
					break;
				}
				$i++;
			}
			//var_dump($i);var_dump($step_id);
			if (isset($lpath_contents[$i])) {
				$mark_complete = array();
				$mark_pending = array();
				$mark_complete = JLMS_Track_processSteps($user_start_id, $course_id, $lpath_id, $lpath_contents, $i);
				$mark_pending = JLMS_getPendingSteps($lpath_contents);
				$show_error = false;
				$query = "SELECT lpath_name FROM #__lms_learn_paths WHERE id = $lpath_id";
				$JLMS_DB->SetQuery( $query );
				$lpath_name = $JLMS_DB->LoadResult();
				$user_unique_id_quiz = 0;
				if($quiz_id && $quest_id && $stu_quiz_id){
					$query = "SELECT a.unique_id FROM #__lms_quiz_r_student_quiz as a, #__lms_learn_path_step_quiz_results as b WHERE b.stu_quiz_id = '".$stu_quiz_id."' AND b.stu_quiz_id = a.c_id";	
					$JLMS_DB->setQuery($query);
					$user_unique_id_quiz = $JLMS_DB->LoadResult();
				}
				JLMS_course_lpathstu_html::show_NoJs_Page($lpath_name, $lpath_contents, $i, $mark_complete, $user_unique_id, $user_start_id, $user_unique_id_quiz, $quiz_id, $quest_id, $stu_quiz_id, $mark_pending, '***JLMS***SHOW***CONTENTS***');
			}
		}
	}
	if ($JLMS_CONFIG->get('process_without_js', false) && $show_error) {
		 // 21.03.2008 - DEN - noJS functionality
		JLMS_course_lpathstu_html::show_ErrorMessage(_JLMS_LPATH_LOAD_DATA_ERROR);
	}
}
function JLMS_next_LPathStep( $lpath_id, $option ) {
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;
	
	$course_id = $JLMS_CONFIG->get('course_id');
	$usertype = $JLMS_CONFIG->get('current_usertype', 0);
	$step_id = intval(mosGetParam($_REQUEST, 'step_id', 0));
	$ret_str = array();
	$mark_complete = array();
	$mark_pending = array();
	
	$i = 0;
	if ($my->id && $course_id && $usertype && JLMS_isValidLPath_ID($lpath_id, $course_id) ) {
		
		$query = "SELECT lp_params FROM #__lms_learn_paths WHERE id = $lpath_id";
		$JLMS_DB->SetQuery($query);
		$lp_params_str = $JLMS_DB->LoadResult();
		$lp_params = new JLMSParameters($lp_params_str);
		
		$user_unique_id = strval(mosGetParam($_REQUEST, 'user_unique_id', ''));
		$user_start_id = intval(mosGetParam($_REQUEST, 'user_start_id', 0));
		if (JLMS_isValidStartUnique($user_start_id, $user_unique_id, $course_id, $lpath_id)) {
			$lpath_contents = JLMS_GetLPath_Data($lpath_id, $course_id);

			$i = 0;
			$do_search = true;
			while ($do_search && $i < count($lpath_contents)) {
				if ($lpath_contents[$i]->id == $step_id) $do_search = false;
				$i++;
			}

			if (isset($lpath_contents[$i])) {
				JLMS_Track_MarkStepStatus($user_start_id, $course_id, $lpath_id, $lpath_contents[$i]->id, $lpath_contents[$i], 2);
				$mark_complete = array();
				$mark_complete = JLMS_Track_processSteps($user_start_id, $course_id, $lpath_id, $lpath_contents, $i);
				
				/*Navigation model + last attempt to return to the same step*/
				$navigation_model_switch = $lp_params->get('navigation_model', 0);				
				CurrentStep($lpath_contents[$i]);
				/*Navigation model + last attempt to return to the same step*/
				
				if (isset($lpath_contents[$i]->new_step_status) && ($lpath_contents[$i]->new_step_status == 1) && !$navigation_model_switch){
					//next step (now this) already completed
					$new_next_step = jlms_LPathPlay_findNextStep($lpath_contents, $i);
					if ( isset($lpath_contents[$new_next_step]) ) {
						JLMS_Track_MarkStepStatus($user_start_id, $course_id, $lpath_id, $lpath_contents[$new_next_step]->id, $lpath_contents[$new_next_step], 2);
						$ret_str = jlms_getXML_next($lpath_contents, $new_next_step, $mark_complete, $user_unique_id, $user_start_id, $lp_params);
						$i = $new_next_step;
					} else {
						if (count($mark_complete)) {
							$c_steps = implode(',',$mark_complete);
						} else { $c_steps = 0; }
						$t_ar = JLMS_writeCompleteStats($user_start_id, $user_unique_id, $course_id, $lpath_id);
						$ret_str = JLMS_finishLPath($lpath_id, $option, $c_steps, $t_ar, false, $user_unique_id, $user_start_id);
					}
				} else {
					$ret_str = jlms_getXML_next($lpath_contents, $i, $mark_complete, $user_unique_id, $user_start_id, $lp_params);
				}
			} else {
				if($i == count($lpath_contents)) {
					
					$cond_ar = jlms_LPathPlay_checkConditions($lpath_contents, $i);
					
					if (isset($cond_ar['is_continue']) && $cond_ar['is_continue']){
						$mark_complete = array();
						$mark_complete = JLMS_Track_processSteps($user_start_id, $course_id, $lpath_id, $lpath_contents, $i); //vmesto $i mogno i 0 vstavit' (vse ravno lpath_contents[$i] not set)
						$new_next_step = jlms_LPathPlay_findNextStep($lpath_contents, $i);
						if ( isset($lpath_contents[$new_next_step]) ) {
							JLMS_Track_MarkStepStatus($user_start_id, $course_id, $lpath_id, $lpath_contents[$new_next_step]->id, $lpath_contents[$new_next_step], 2);
							$ret_str = jlms_getXML_next($lpath_contents, $new_next_step, $mark_complete, $user_unique_id, $user_start_id, $lp_params);
							$i = $new_next_step;
						} else {
							if (count($mark_complete)) {
								$c_steps = implode(',',$mark_complete);
							} else { $c_steps = 0; }
							$t_ar = JLMS_writeCompleteStats($user_start_id, $user_unique_id, $course_id, $lpath_id);
							$ret_str = JLMS_finishLPath($lpath_id, $option, $c_steps, $t_ar, false, $user_unique_id, $user_start_id);
							$i = $i - 1;
						}
					} else {
						$n = $i - 1;
						JLMS_Track_REM_StepStatus( $user_start_id, $lpath_contents[$n]->course_id, $lpath_contents[$n]->lpath_id, $lpath_contents[$n]->id);
						$ret_str = jlms_getXML_cond( $cond_ar, $mark_complete, $lpath_contents[$n]->step_name, $lp_params );
						/**/$ret_str['user_unique_id'] = new stdClass(); $ret_str['user_unique_id']->value = $user_unique_id;
						/**/$ret_str['user_start_id'] = new stdClass(); $ret_str['user_start_id']->value = $user_start_id;
						/**/$ret_str['course_id'] = new stdClass(); $ret_str['course_id']->value = isset($lpath_contents[$i]->course_id) ? $lpath_contents[$i]->course_id : $JLMS_CONFIG->get('course_id', 0);
						/**/$ret_str['lpath_id'] = new stdClass(); $ret_str['lpath_id']->value = isset($lpath_contents[$i]->lpath_id) ? $lpath_contents[$i]->lpath_id : 0;
					}
				} 
			}
		}
	}
	if ($JLMS_CONFIG->get('process_without_js', false)) {
		 // 20.03.2008 - DEN - noJS functionality
		if(empty($ret_str)) {
			JLMS_course_lpathstu_html::show_ErrorMessage(_JLMS_LPATH_LOAD_DATA_ERROR);
		} else {
			$query = "SELECT lpath_name FROM #__lms_learn_paths WHERE id = $lpath_id";
			$JLMS_DB->SetQuery( $query );
			$lpath_name = $JLMS_DB->LoadResult();
			$ret_str['mark_complete'] = new stdClass(); $ret_str['mark_complete']->value = $mark_complete;
			$ret_str['mark_pending'] = new stdClass(); $ret_str['mark_pending']->value = $mark_pending;
			JLMS_course_lpathstu_html::show_NoJs_Page2($ret_str, $lpath_name, $lpath_contents, $i);
		}
	} elseif (!$JLMS_CONFIG->get('process_without_js', false)) {
		JLMS_outputXML($ret_str);
	}
}
function JLMS_seek_LPathStep( $lpath_id, $option, $sm = 0 ) {
	global $JLMS_DB, $my, $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id');
	$usertype = $JLMS_CONFIG->get('current_usertype', 0);
	$step_id = intval(mosGetParam($_REQUEST, 'step_id', 0));
	$seek_step_id = intval(mosGetParam($_REQUEST, 'seek_step_id', 0));
	if ($seek_step_id) { $step_id = $seek_step_id; }
	$ret_str = '';
	$mark_complete = array();
	$mark_pending = array();
	if ($my->id && $course_id && $usertype && JLMS_isValidLPath_ID($lpath_id, $course_id) ) {
		$query = "SELECT lp_params FROM #__lms_learn_paths WHERE id = $lpath_id";
		$JLMS_DB->SetQuery($query);
		$lp_params_str = $JLMS_DB->LoadResult();
		$lp_params = new JLMSParameters($lp_params_str);
		
		$user_unique_id = strval(mosGetParam($_REQUEST, 'user_unique_id', ''));
		$user_start_id = intval(mosGetParam($_REQUEST, 'user_start_id', 0));
		if (JLMS_isValidStartUnique($user_start_id, $user_unique_id, $course_id, $lpath_id)) {
			$lpath_contents = JLMS_GetLPath_Data($lpath_id, $course_id);
			$i = 0;
			$do_search = true;
			while ($do_search && $i < count($lpath_contents)) {
				if ($lpath_contents[$i]->id == $step_id) $do_search = false;
				$i++;
			}
			if ($sm) {$i = $i + $sm;}
			if (isset($lpath_contents[$i-1])){
				
				/*Navigation model + last attempt to return to the same step*/
				CurrentStep($lpath_contents[$i-1]);
				/*Navigation model + last attempt to return to the same step*/
				
				JLMS_Track_MarkStepStatus($user_start_id, $course_id, $lpath_id, $lpath_contents[$i-1]->id, $lpath_contents[$i-1], 2);
				$mark_complete = array();
				$mark_complete = JLMS_Track_processSteps($user_start_id, $course_id, $lpath_id, $lpath_contents, ($i - 1));
				$ret_str = jlms_getXML_next($lpath_contents, ($i - 1), $mark_complete, $user_unique_id, $user_start_id, $lp_params);
				$i = $i - 1;
			}
		}
	}
	if ($JLMS_CONFIG->get('process_without_js', false)) {
		 // 20.03.2008 - DEN - noJS functionality
		if(empty($ret_str)) {
			JLMS_course_lpathstu_html::show_ErrorMessage(_JLMS_LPATH_LOAD_DATA_ERROR);
		} else {
			$query = "SELECT lpath_name FROM #__lms_learn_paths WHERE id = $lpath_id";
			$JLMS_DB->SetQuery( $query );
			$lpath_name = $JLMS_DB->LoadResult();
			$ret_str['mark_complete'] = new stdClass(); $ret_str['mark_complete']->value = $mark_complete;
			$ret_str['mark_pending'] = new stdClass(); $ret_str['mark_pending']->value = $mark_pending;
			JLMS_course_lpathstu_html::show_NoJs_Page2($ret_str, $lpath_name, $lpath_contents, $i);
		}
	} elseif (!$JLMS_CONFIG->get('process_without_js', false)) {
		JLMS_outputXML($ret_str);
	}
}
function jlms_LPathPlay_searchConds(&$lpath_contents, $lid, &$conds_data) {
	global $JLMS_DB;
	if(isset($lpath_contents[$lid]->id)){
		$query = "SELECT * FROM #__lms_learn_path_conds WHERE step_id = '".$lpath_contents[$lid]->id."' ORDER BY id";
		$JLMS_DB->SetQuery( $query );
		$conds_data2 = $JLMS_DB->LoadObjectList();
		if (!count($conds_data2)) {
	//	$cond = $lpath_contents[$lid]->cond_id;
	//	if (!$cond) {
			$parent = $lpath_contents[$lid]->parent_id;
			if ($parent) {
				$t = 0;
				$e = -1;
				while ($t < count($lpath_contents)) {
					if ($lpath_contents[$t]->id == $parent) {
						$e = $t;
					}
					$t ++;
				}
				if ($e >= 0) {
					jlms_LPathPlay_searchConds($lpath_contents, $e, $conds_data);
				}
			}
		} else {
			$i = 0;
			while ($i < count($conds_data2)) {
				$conds_data[] = $conds_data2[$i];
				$i ++;
			}
		}
	}
}
function jlms_LPathPlay_checkConditions(&$lpath_contents, $lid) {
	global $JLMS_DB;
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$ret_ar = array();
	$ret_ar['is_continue'] = true;
	$conds_data = array();
	
	if($JLMS_CONFIG->get('enable_timetracking', false)){
		for($j=0;$j<count($lpath_contents);$j++){
			global $my;
			$query = "SELECT *"
			. "\n FROM #__lms_time_tracking_resources"
			. "\n WHERE 1"
			. "\n AND course_id = ".$lpath_contents[$j]->course_id
			. "\n AND user_id = ".$my->id
			. "\n AND resource_type = 9"
			. "\n AND resource_id = ".$lpath_contents[$j]->lpath_id
			. "\n AND item_id = ".$lpath_contents[$j]->id
			;
			$JLMS_DB->setQuery($query);
			$timetrack = $JLMS_DB->loadObject();
			if(isset($timetrack) && $timetrack->id){
				$lpath_contents[$j]->time_spent = $timetrack->time_spent;
			} else {
				$lpath_contents[$j]->time_spent = 0;
			}
		}
	}
	
	jlms_LPathPlay_searchConds($lpath_contents, $lid, $conds_data);
	
	$cond_array = array();
	
	if (count($conds_data)) {
		foreach ($conds_data as $condition){
			if($condition->ref_step){
				$ref_step = $condition->ref_step;
				$rs_num = -1;
				$t = 0;
				while ($t < count($lpath_contents)) {
					if ($lpath_contents[$t]->id == $ref_step) {
						$rs_num = $t;
					}
					$t++;
				}
				
				if ($rs_num >= 0){
					
					if ($condition->cond_type == 1){
						if ($condition->ref_step == $lpath_contents[$rs_num]->id && $lpath_contents[$rs_num]->new_step_status != 1){
							$ret_ar['is_continue'] = false;
							$cc = new stdClass();
							$cc->seek_id = $lpath_contents[$rs_num]->id;
							$cc->seek_name = $lpath_contents[$rs_num]->step_name;
							$cc->lpath_id = $lpath_contents[$rs_num]->lpath_id;
						}
						
						$cond_array[] = $cc;
					}
				}
			}
		}
	}
	
	if($lid && $JLMS_CONFIG->get('enable_timetracking')){
		jlms_LPathPlay_searchConds($lpath_contents, ($lid - 1), $conds_data);

		if(count($conds_data)){
			foreach ($conds_data as $condition){
				if(!$condition->ref_step){
					//if(($lid + 1) == count($lpath_contents)){
					//	$ref_step = $lpath_contents[$lid]->id;
					//} else {
						$ref_step = $lpath_contents[$lid - 1]->id;	
					//}
					
					$rs_num = -1;
					$t = 0;
					while ($t < count($lpath_contents)) {
						if ($lpath_contents[$t]->id == $ref_step) {
							$rs_num = $t;
						}
						$t++;
					}
					
					if ($rs_num >= 0){
						
						global $Track_Object;
						$my = JFactory::getUser();
						$Track_Object->TimeTrackingActivate($lpath_contents[$rs_num]->course_id, $my->id, 9, $lpath_contents[$rs_num]->lpath_id, $lpath_contents[$rs_num]->id);
						
						$flag_status = 1;
						//if(isset($lpath_contents[$rs_num]->new_step_status) && $lpath_contents[$rs_num]->new_step_status){
						//	$flag_status = 0;
						//}
						
						if ($condition->cond_type == 1){
							//if($flag_status && $ref_step == $condition->step_id && isset($condition->cond_time) && $condition->cond_time && $lpath_contents[$rs_num]->time_spent < ($condition->cond_time * 60 + $condition->cond_time_sec)){
							if($flag_status && $ref_step == $condition->step_id && isset($condition->cond_time) && $condition->cond_time && $lpath_contents[$rs_num]->time_spent < ($condition->cond_time)){
								$ret_ar['is_continue'] = false;
								$cc = new stdClass();
								$cc->seek_id = $lpath_contents[$rs_num]->id;
								$cc->seek_name = $lpath_contents[$rs_num]->step_name;
								$cc->lpath_id = $lpath_contents[$rs_num]->lpath_id;
								
								//if(isset($condition->cond_time) && $condition->cond_time && $lpath_contents[$rs_num]->time_spent < ($condition->cond_time * 60 + $condition->cond_time_sec)){
								if(isset($condition->cond_time) && $condition->cond_time && $lpath_contents[$rs_num]->time_spent < ($condition->cond_time)){
									$ret_ar['is_continue'] = false;
									$cc = new stdClass();
									$cc->seek_id = $lpath_contents[$rs_num]->id;
									$cc->seek_name = $lpath_contents[$rs_num]->step_name;
									$cc->lpath_id = $lpath_contents[$rs_num]->lpath_id;
									
									if($JLMS_CONFIG->get('enable_timetracking', false)){
										global $my;
										$query = "SELECT time_spent"
										. "\n FROM #__lms_time_tracking_resources"
										. "\n WHERE 1"
										. "\n AND course_id = '".$lpath_contents[$rs_num]->course_id."'"
										. "\n AND user_id = '".$my->id."'"
										. "\n AND resource_type = '9'"
										. "\n AND resource_id = '".$lpath_contents[$rs_num]->lpath_id."'"
										. "\n AND item_id = '".$lpath_contents[$rs_num]->id."'"
										;
										$JLMS_DB->setQuery($query);
										$time_spent = $JLMS_DB->loadResult();
										$need_to_time_spent = 0;
										//$need_to_time_spent = (($condition->cond_time * 60) + $condition->cond_time_sec) - $time_spent;
										$need_to_time_spent = ($condition->cond_time) - $time_spent;
										if($need_to_time_spent < 0){
											$need_to_time_spent = 0;
										}
										
										if($need_to_time_spent){
											$cc->need_to_time_spent = helper_need_to_time_spent($need_to_time_spent);
										}
									}
								}
								$cond_array[] = $cc;
							}
						}
					}
				}
			}
		}
	}
	
	$ret_ar['conditions'] = $cond_array;
	
	return $ret_ar;
}

function helper_need_to_time_spent($sec){
	
	$m = $sec >= 60 ? floor($sec/60) : 0;
	$sec = $sec - $m * 60;
	$s = $sec;
	
	$time = '';
	$time .= str_pad($m, 2, '0', STR_PAD_LEFT).':';
	$time .= str_pad($s, 2, '0', STR_PAD_LEFT);
	
	return $time;
}

function jlms_getXML_cond( &$conds, &$mark_complete, $pname, & $lp_params ) {
	global $JLMS_CONFIG, $my, $Itemid;	

	/**/$ret_obj = array();
	/**/$ret_obj['task'] = new stdClass();$ret_obj['task']->value = 'check_cond';

	$ret_str = '<task>check_cond</task>'."\n";
	//$ret_str .= '<step_id>'.$pid.'</step_id>'."\n";

	/**/$ret_obj['step_name'] = new stdClass();$ret_obj['step_name']->value = $pname;$ret_obj['step_name']->is_cdata = true;

	$ret_str .= '<step_name><![CDATA['.$pname.']]></step_name>'."\n";
	
	$cid = isset($conds['conditions'][0]->seek_id)?$conds['conditions'][0]->seek_id:0;

	/**/$ret_obj['seek_id'] = new stdClass(); $ret_obj['seek_id']->value = $cid; // noJS object only !!!!!!
	/**/   // Not necessary ????  $ret_obj['conditions'] = $conds['conditions']; // noJS object only !!!!!!
	
	$descr_htm = '';
	$cs = false;
	foreach ($conds['conditions'] as $cond){
		if(!isset($cond->need_to_time_spent)){
			$cs = true;
		}
	}
	if($cs){
		$descr_htm .= _JLMS_LPATH_COMPLETE_STEPS_TXT. '<br />';
	}
	$nts = false;
	$tmp = array();
	foreach ($conds['conditions'] as $cond){
		if(!isset($cond->need_to_time_spent)){
			$tmp[] = $cond;
		}
	}
	$i=0;
	foreach ($conds['conditions'] as $cond){
		if(!isset($cond->need_to_time_spent)){
			$descr_htm .= '<a href="javascript:seek_step_id='.$cond->seek_id.';ajax_action(\'lpath_seek\');" title=\''._JLMS_LPATH_COMPLETE_STEP_TITLE.'\'>'.$cond->seek_name.'</a>';
			if($i < count($tmp) - 1){
				$descr_htm .= ', ';
			}
			$i++;
		} else {
			$nts = true;
		}
	}
	if($nts){
		if($cs){
			$descr_htm .= '<br /><br />';
		}
		$descr_htm .= _JLMS_LPATH_COMPLETE_NEED_TIME_STEPS_TXT. '<br />';
	}
	foreach ($conds['conditions'] as $cond){
		if(isset($cond->need_to_time_spent)){
			$descr_htm .= '<a href="javascript:seek_step_id='.$cond->seek_id.';ajax_action(\'lpath_seek\');" title=\''._JLMS_LPATH_COMPLETE_STEP_TITLE.'\'>'.$cond->seek_name.'</a>';
			$descr_htm .= ' '.str_replace('{XX:XX}', $cond->need_to_time_spent, _JLMS_LPATH_CONDTYPE_STILL_NEED_TO_SPENT) . '<br />';
		}
	}

	/**/$ret_obj['step_descr'] = new stdClass();$ret_obj['step_descr']->value = '<div class="'.$JLMS_CONFIG->get('system_message_css_class', 'joomlalms_sys_message').'">'.$descr_htm.'</div>';$ret_obj['step_descr']->is_cdata = true;

	$ret_str .= '<step_descr><![CDATA[<div class="'.$JLMS_CONFIG->get('system_message_css_class', 'joomlalms_sys_message').'">'.$descr_htm.'</div>]]></step_descr>'."\n";
	$toolbar = array();
	$toolbar[] = array('btn_type' => 'continue', 'btn_js' => "javascript:seek_step_id=".$cid.";ajax_action('lpath_seek');");
	
	if ( $lp_params->get('navigation_type', 0 ) != 2 ) {
		$toolbar[] = array('btn_type' => 'contents', 'btn_js' => "javascript:ajax_action('contents_lpath');");
	}
	
	$tool_txt = JLMS_ShowToolbar($toolbar);
	$tool_txt = str_replace('"components/com_joomla_lms','"'.$JLMS_CONFIG->getCfg('live_site').'/components/com_joomla_lms',$tool_txt);

	/**/$ret_obj['menu_contents'] = new stdClass();$ret_obj['menu_contents']->value = $tool_txt;$ret_obj['menu_contents']->is_cdata = true;

	$ret_str .= '<menu_contents><![CDATA['.$tool_txt.']]></menu_contents>'."\n";
	//$ret_str .= '<pending_steps>'.$pid.'</pending_steps>'."\n";
	if (count($mark_complete)) {
		$c_steps = implode(',',$mark_complete);
	} else { $c_steps = 0; }

	/**/$ret_obj['completed_steps'] = new stdClass();$ret_obj['completed_steps']->value = $c_steps;

	$ret_str .= '<completed_steps>'.$c_steps.'</completed_steps>'."\n";

	//global $Track_Object;
	//$Track_Object->TimeTrackingActivate($JLMS_CONFIG->get('course_id'), $my->id, 9, $conds['conditions'][0]->lpath_id, $elem->id);
	
	$step_script = '';
	if($JLMS_CONFIG->get('enable_timetracking', false)){
		$step_script .= "TTracker_".$JLMS_CONFIG->get('course_id')."_".$my->id."_9_".$conds['conditions'][0]->lpath_id.".getitemid(0, 0);";
		$step_script .= "TTracker_".$JLMS_CONFIG->get('course_id')."_".$my->id."_9_".$conds['conditions'][0]->lpath_id.".hide_online();";
		$step_script .= "TTracker_".$JLMS_CONFIG->get('course_id')."_".$my->id."_9_".$conds['conditions'][0]->lpath_id.".stop();";
	}
	
	
	/**/$ret_obj['step_exec_script'] = new stdClass();
		$ret_obj['step_exec_script']->value = '1';
		$ret_obj['step_exec_script_contents']->value = $step_script;
		
	$ret_str .= '<step_exec_script>1</step_exec_script>'."\n";
	$ret_str .= '<step_exec_script_contents>'.$step_script.'</step_exec_script_contents>'."\n";
	//return $ret_str;
	return $ret_obj;
}

function jlms_getXML_CustomCond( &$ret_obj, &$mark_complete, &$lpath_contents, $i, $message, $task_xml = 'next_step' ) {
	global $JLMS_CONFIG, $Itemid, $JLMS_DB;
	
	$elem = $lpath_contents[$i];	
	
	$lp_params_str = '';
	if( isset($ret_obj['lpath_id']) && $ret_obj['lpath_id'] ) {
		$query = "SELECT lp_params FROM #__lms_learn_paths WHERE id = ".$JLMS_DB->quote($ret_obj['lpath_id']->value);
		$JLMS_DB->SetQuery($query);
		$lp_params_str = $JLMS_DB->LoadResult();
	}		
	$lp_params = new JLMSParameters($lp_params_str);
	
	/**/$ret_obj['task'] = new stdClass();$ret_obj['task']->value = $task_xml;
	/**/$ret_obj['step_id'] = new stdClass(); $ret_obj['step_id']->value = $elem->id;
	/**/$ret_obj['step_item_id'] = new stdClass(); $ret_obj['step_item_id']->value = $elem->item_id;
	/**/$ret_obj['step_type'] = new stdClass();$ret_obj['step_type']->value = $elem->step_type;$ret_obj['step_type']->is_cdata = true;
	/**/$ret_obj['step_name'] = new stdClass();$ret_obj['step_name']->value = $elem->step_name;$ret_obj['step_name']->is_cdata = true;
		
	$descr_htm = $message. '<br />';
	/**/$ret_obj['step_descr'] = new stdClass();$ret_obj['step_descr']->value = '<div class="'.$JLMS_CONFIG->get('system_message_css_class', 'joomlalms_sys_message').'">'.$descr_htm.'</div>';$ret_obj['step_descr']->is_cdata = true;
		
	$toolbar = array();
	
	if ($i > 0) {
		$toolbar[] = array('btn_type' => 'prev', 'btn_js' => "javascript:ajax_action('prev_lpathstep');");
	}
		
	$toolbar[] = array('btn_type' => 'next', 'btn_js' => "javascript:ajax_action('next_lpathstep');");
	
	if ( $lp_params->get('navigation_type', 0 ) != 2 ) {
		$toolbar[] = array('btn_type' => 'contents', 'btn_js' => "javascript:ajax_action('contents_lpath');");
	}
	
	$toolbar = showPrevButtonLP($toolbar, $lp_params);
	
	$tool_txt = JLMS_ShowToolbar($toolbar);
	$tool_txt = str_replace('"components/com_joomla_lms','"'.$JLMS_CONFIG->getCfg('live_site').'/components/com_joomla_lms',$tool_txt);

	/**/$ret_obj['menu_contents'] = new stdClass();$ret_obj['menu_contents']->value = $tool_txt;$ret_obj['menu_contents']->is_cdata = true;
	
	if (count($mark_complete)) {
		$c_steps = implode(',',$mark_complete);
	} else { $c_steps = 0; }

	/**/$ret_obj['menu_contents'] = new stdClass();$ret_obj['menu_contents']->value = $tool_txt;$ret_obj['menu_contents']->is_cdata = true;
	/**/$ret_obj['pending_steps'] = new stdClass();$ret_obj['pending_steps']->value = $elem->id;
	/**/$ret_obj['completed_steps'] = new stdClass();$ret_obj['completed_steps']->value = $c_steps;
	/**/$ret_obj['step_exec_script'] = new stdClass();$ret_obj['step_exec_script']->value = '0';	

	return $ret_obj;
} 

function jlms_timeToShow( $elem ) {
	$db = JFactory::getDbo();
	$user = JLMSFactory::getUser();

	$res = new stdClass();
	$res->result = true;
	$res->time_left = 0;

	$AND_ST = "";
	$do_check_time = false;
	if( false !== ( $enroll_period = JLMS_getEnrolPeriod( $user->get('id'), $elem->course_id )) ) 
	{
		$AND_ST = " AND IF(is_time_related, (show_period < '".$enroll_period."' ), 1) ";
		$do_check_time = true;	
	}
	$AND_ST = "";
	$obj = null;

	switch ( $elem->step_type ) {
		case 2: 
			$query = "SELECT * FROM #__lms_documents WHERE id = '".intval($elem->item_id)."' ".$AND_ST." LIMIT 1";
			$db->setQuery( $query );
			$obj = $db->loadObject();
			break;
		case 3:
			$query = "SELECT * FROM #__lms_links WHERE id = '".intval($elem->item_id)."' ".$AND_ST." LIMIT 1";
			$db->setQuery( $query );
			$obj = $db->loadObject();
			break;
		case 5:			 
			$query = "SELECT * FROM #__lms_quiz_t_quiz WHERE c_id = ".intval($elem->item_id).$AND_ST." LIMIT 1";			
			$db->setQuery( $query );
			$obj = $db->loadObject();
			break;
		case 6:			
			$query = "SELECT * FROM #__lms_learn_paths WHERE id = ".intval($elem->item_id)." ".$AND_ST." LIMIT 1";
			$db->setQuery( $query );
			$obj = $db->loadObject();
			break;
	}
	if ($elem->step_type == 2 || $elem->step_type == 3 || $elem->step_type == 5 || $elem->step_type == 6 ) {
		if (is_object($obj) && ( (isset($obj->id) && $obj->id) || (isset($obj->c_id) && $obj->c_id) )) {
			if ($do_check_time) {
				if ($obj->is_time_related) {
					$time_difference = $obj->show_period - $enroll_period;
					if ($time_difference > 0) {
						$res->result = false;
						$res->time_left = intval(ceil($time_difference));
						$ost1 = $res->time_left%(24*60);		
						$res->time_left_days = ($res->time_left - $ost1)/(24*60);
						$ost2 = $res->time_left%60;						
						$res->time_left_hours = ($ost1 - $ost2)/60;
						$res->time_left_minutes = $ost2;
					} else {
						$res->result = true;
					}
				} else {
					$res->result = true;
				}
			} else {
				$res->result = true;
			}
		} else {
			$res->result = false;
		}
	} else {
		$res->result = true;
	}

	return $res;	
}

function jlms_getXML_next(&$lpath_contents, $i, &$mark_complete, $user_unique_id, $user_start_id, &$lp_params){
	global $my, $JLMS_DB, $JLMS_CONFIG;
	
	$quiz_params = new stdClass();
	
	$cond_ar = jlms_LPathPlay_checkConditions($lpath_contents, $i);
	/**/$ret_obj = array();
	if (isset($cond_ar['is_continue']) && $cond_ar['is_continue']){
		$time_res = jlms_timeToShow( $lpath_contents[$i] );
		if (!$time_res->result) {
			JLMS_Track_REM_StepStatus( $user_start_id, $lpath_contents[$i]->course_id, $lpath_contents[$i]->lpath_id, $lpath_contents[$i]->id);	
			$mess = '';							 												
			$ret_obj['user_unique_id'] = new stdClass(); $ret_obj['user_unique_id']->value = $user_unique_id;
			$ret_obj['user_start_id'] = new stdClass(); $ret_obj['user_start_id']->value = $user_start_id;
			$ret_obj['task'] = new stdClass();$ret_obj['task']->value = 'next_step';
			$ret_obj['course_id'] = new stdClass(); $ret_obj['course_id']->value = isset($lpath_contents[$i]->course_id) ? $lpath_contents[$i]->course_id : $JLMS_CONFIG->get('course_id', 0);
			$ret_obj['lpath_id'] = new stdClass(); $ret_obj['lpath_id']->value = isset($lpath_contents[$i]->lpath_id) ? $lpath_contents[$i]->lpath_id : 0;
			if ($time_res->time_left) {
				$msg_time_released = _JLMS_REQUESTED_RESOURCE_WILL_BE_RELEASED;
				if (isset($time_res->time_left_days) && $time_res->time_left_days) {
					$msg_time_released .= ' '.$time_res->time_left_days.' '._JLMS_RELEASED_IN_DAYS;
				}
				if (isset($time_res->time_left_hours) && $time_res->time_left_hours) {
					$msg_time_released .= ' '.$time_res->time_left_hours.' '._JLMS_RELEASED_IN_HOURS;
				}
				if (isset($time_res->time_left_minutes) && $time_res->time_left_minutes) {
					$msg_time_released .= ' '.$time_res->time_left_minutes.' '._JLMS_RELEASED_IN_MINUTES;
				}
			} else {
				$msg_time_released = _JLMS_LPATH_RESOURCE_UNAVAILABLE;
			}
			jlms_getXML_CustomCond( $ret_obj, $mark_complete, $lpath_contents, $i, $msg_time_released );						
			
			return $ret_obj;
		}

		/**/$ret_obj['task'] = new stdClass();$ret_obj['task']->value = 'next_step';

		/**/$ret_obj['user_unique_id'] = new stdClass(); $ret_obj['user_unique_id']->value = $user_unique_id;
		/**/$ret_obj['user_start_id'] = new stdClass(); $ret_obj['user_start_id']->value = $user_start_id;

		$ret_str = '<task>next_step</task>';
		$ret_str .= JLMS_prepareLPath_ElemObj($ret_obj, $lpath_contents[$i], $user_unique_id, $user_start_id);
		$toolbar = array();	
			
		if ($i > 0) {
			$toolbar[] = array('btn_type' => 'prev', 'btn_js' => "javascript:ajax_action('prev_lpathstep');");
		}
		
		if ($lpath_contents[$i]->step_type == 5) {
			$stu_quiz_id = 0;
			$stu_quiz_data = NULL;
			$resume_quiz = NULL;
			
//			if($lp_params->get('resume_last_attempt', 1) || ){
			if(true){
				/** do all this staff only if 'resume' is enabled for LearningPath **/
				$query = "SELECT b.stu_quiz_id FROM #__lms_learn_path_results as a, #__lms_learn_path_step_quiz_results as b WHERE a.user_id = '".$my->id."'"
				. "\n AND a.course_id = '".$lpath_contents[$i]->course_id."' AND a.lpath_id = '".$lpath_contents[$i]->lpath_id."' AND b.result_id = a.id AND b.step_id = ".$lpath_contents[$i]->id;
				$JLMS_DB->SetQuery( $query );
				$stu_quiz_id = $JLMS_DB->LoadResult();
				if (!$stu_quiz_id) { $stu_quiz_id = 0; }
				if ($stu_quiz_id) {
					$query = "SELECT c_passed, c_total_time, c_total_score FROM #__lms_quiz_r_student_quiz WHERE c_id = $stu_quiz_id";
					$JLMS_DB->SetQuery( $query );
					$stu_quiz_data = $JLMS_DB->LoadObject();
				} else {
					// find any not-completed quiz attempt started from the 'quizzes' tool
					$query = "SELECT c_id, unique_id, c_quiz_id FROM #__lms_quiz_r_student_quiz WHERE c_quiz_id = ".$lpath_contents[$i]->item_id." AND c_student_id = ".$my->id." and c_passed = 0 and c_total_time = 0 ORDER BY c_id desc LIMIT 1";
					$JLMS_DB->SetQuery($query);
					$tmp_resume_quiz = $JLMS_DB->LoadObject();
					if (isset($tmp_resume_quiz->c_id)) {
						$stu_quiz_id = $tmp_resume_quiz->c_id;
						$query = "SELECT id FROM #__lms_learn_path_results WHERE user_id = '".$my->id."'"
						. "\n AND course_id = '".$lpath_contents[$i]->course_id."' AND lpath_id = '".$lpath_contents[$i]->lpath_id."'";
						$JLMS_DB->SetQuery($query);
						$tmp_result_id = $JLMS_DB->LoadResult();
						$query = "UPDATE #__lms_learn_path_step_quiz_results SET stu_quiz_id = $stu_quiz_id WHERE result_id = $tmp_result_id AND step_id = ".$lpath_contents[$i]->id;
						$JLMS_DB->SetQuery($query);
						$JLMS_DB->query();
					}
				}
				/** end **/
			}

			if ($stu_quiz_id) {
				$query = "SELECT c_id, unique_id, c_quiz_id FROM #__lms_quiz_r_student_quiz WHERE c_id = $stu_quiz_id AND c_quiz_id = ".$lpath_contents[$i]->item_id." AND c_student_id = ".$my->id." and c_passed = 0 and c_total_time = 0 ORDER BY c_id desc LIMIT 1";
				$JLMS_DB->SetQuery($query);
				$resume_quiz = $JLMS_DB->LoadObject();
			}

			if(isset($resume_quiz->c_id)) {
				$query = "SELECT c_question_id FROM #__lms_quiz_r_student_question WHERE c_stu_quiz_id = ".$resume_quiz->c_id." ORDER BY c_id desc LIMIT 1";
				$JLMS_DB->SetQuery($query);
				$last_question = $JLMS_DB->LoadResult();

				if (!$last_question) {
					$last_question = -1;
				}

				$quiz_params->resume_quiz = $resume_quiz->c_id;
				$quiz_params->unique_id = $resume_quiz->unique_id;
				$quiz_params->last_question = $last_question;
			}			
			
			if(isset($lpath_contents[$i]->item_id) && $lpath_contents[$i]->item_id) {
				$query = "SELECT count(c_id) FROM #__lms_quiz_r_student_quiz WHERE c_quiz_id = ".$lpath_contents[$i]->item_id." AND c_student_id = '".$my->id."'";
				$JLMS_DB->SetQuery($query);
				$count = $JLMS_DB->LoadResult();
	
				$quiz_params->attempts_of_this_quiz = $count;
			}
			else {
				$quiz_params->attempts_of_this_quiz = 0;
			}

			$query = "SELECT c_max_numb_attempts, c_min_after FROM #__lms_quiz_t_quiz WHERE c_id = ".$lpath_contents[$i]->item_id."";
			$JLMS_DB->SetQuery($query);
			$some_quiz_params = $JLMS_DB->LoadObject();
			if (is_object($some_quiz_params)) {
				$quiz_params->c_max_numb_attempts = $some_quiz_params->c_max_numb_attempts;
				$quiz_params->c_min_after = $some_quiz_params->c_min_after;
			} else {
				$quiz_params->c_max_numb_attempts = 0;
				$quiz_params->c_min_after = 0;
			}
			
			//-----------
			$do_resume = isset($quiz_params->resume_quiz) && $quiz_params->resume_quiz && $quiz_params->last_question;
			if ((isset($stu_quiz_data->c_passed) && $stu_quiz_data->c_passed) || (($quiz_params->attempts_of_this_quiz >= $quiz_params->c_max_numb_attempts) &&  $quiz_params->c_max_numb_attempts > 0 && !$do_resume)) {
				$toolbar[] = array('btn_type' => 'next', 'btn_js' => "javascript:ajax_action('next_lpathstep');");
			} else {
				if($do_resume) {
					$toolbar[] = array('btn_type' => 'next', 'btn_js' => "javascript:jq_ResumeQuizOn(".$quiz_params->resume_quiz.",'".$quiz_params->unique_id."', ".$quiz_params->last_question.");");
				} elseif( ($quiz_params->attempts_of_this_quiz < $quiz_params->c_max_numb_attempts) || $quiz_params->c_max_numb_attempts == 0) {
					$do_start_no_time_limit = true;
					$q_time_limit = ($quiz_params->c_min_after)*60;
					if ($q_time_limit) {
//						$query = "SELECT max(c_date_time) as time_last_access FROM #__lms_quiz_r_student_quiz WHERE c_quiz_id = '".$lpath_contents[$i]->item_id."' and c_student_id = '".$my->id."'";
						$query = "SELECT MAX(FROM_UNIXTIME(UNIX_TIMESTAMP(c_date_time) + c_total_time)) as time_last_access"
						. "\n FROM #__lms_quiz_r_student_quiz"
						. "\n WHERE 1"
						. "\n AND c_quiz_id = '".$lpath_contents[$i]->item_id."'"
						. "\n AND c_student_id = '".$my->id."'"
						. "\n ORDER BY (UNIX_TIMESTAMP(c_date_time) + c_total_time)"
						;
						$JLMS_DB->SetQuery( $query );
						$q_last_access = $JLMS_DB->LoadResult();
						if ($q_last_access) { //'STRTOTIME with an empty parameter' bug fixed (02.10.2006)
							$q_last_access_t = strtotime($q_last_access);
							$time_goes = time() - date('Z') - $q_last_access_t;
							if ($time_goes > 1) { //esli proshla 1 minuta (changed to 10 secs)
								if ($time_goes < $q_time_limit) {
									$message = str_replace("{text}", (($q_time_limit - $time_goes) >60)?(floor(($q_time_limit - $time_goes)/60).' minute(s)'):(($q_time_limit - $time_goes). ' seconds'), _JLMS_q_quiz_comeback_later);
									#$message .= ' '.$time_goes;
									$message = '<div class="joomlalms_sys_message">'.$message.'</div>';
									$ret_obj['step_descr'] = new stdClass(); $ret_obj['step_descr']->value = $message; $ret_obj['step_descr']->is_cdata = true;
									$do_start_no_time_limit = false;
								}
							}
						}
					}
					if ($do_start_no_time_limit) {
						$toolbar[] = array('btn_type' => 'next', 'btn_js' => "javascript:jq_StartQuizOn();");
					} else {
						$toolbar[] = array('btn_type' => 'next', 'btn_js' => "javascript:ajax_action('next_lpathstep');");
					}
				}
			}
		} else {
			$toolbar[] = array('btn_type' => 'next', 'btn_js' => "javascript:ajax_action('next_lpathstep');");
		}
		
		if($lp_params->get('navigation_type', 0 ) != 2 ) {
			$toolbar[] = array('btn_type' => 'contents', 'btn_js' => "javascript:ajax_action('contents_lpath');");
		}
		
		$toolbar = showPrevButtonLP($toolbar, $lp_params);
		
		$tool_txt = JLMS_ShowToolbar($toolbar);
		$tool_txt = str_replace('"components/com_joomla_lms','"'.$JLMS_CONFIG->getCfg('live_site').'/components/com_joomla_lms',$tool_txt);

		/**/$ret_obj['menu_contents'] = new stdClass();$ret_obj['menu_contents']->value = $tool_txt;$ret_obj['menu_contents']->is_cdata = true;

		$ret_str .= '<menu_contents><![CDATA['.$tool_txt.']]></menu_contents>'."\n";

		/**/$ret_obj['pending_steps'] = new stdClass();$ret_obj['pending_steps']->value = $lpath_contents[$i]->id;

		$ret_str .= '<pending_steps>'.$lpath_contents[$i]->id.'</pending_steps>'."\n";
		if (count($mark_complete)) {
			$c_steps = implode(',',$mark_complete);
		} else { $c_steps = 0; }

		/**/$ret_obj['completed_steps'] = new stdClass();$ret_obj['completed_steps']->value = $c_steps;

		$ret_str .= '<completed_steps>'.$c_steps.'</completed_steps>'."\n";
	} else {
		JLMS_Track_REM_StepStatus( $user_start_id, $lpath_contents[$i]->course_id, $lpath_contents[$i]->lpath_id, $lpath_contents[$i]->id);
		$ret_obj = jlms_getXML_cond( $cond_ar, $mark_complete, $lpath_contents[$i]->step_name, $lp_params );
		/**/$ret_obj['user_unique_id'] = new stdClass(); $ret_obj['user_unique_id']->value = $user_unique_id;
		/**/$ret_obj['user_start_id'] = new stdClass(); $ret_obj['user_start_id']->value = $user_start_id;
		/**/$ret_obj['course_id'] = new stdClass(); $ret_obj['course_id']->value = isset($lpath_contents[$i]->course_id) ? $lpath_contents[$i]->course_id : $JLMS_CONFIG->get('course_id', 0);
		/**/$ret_obj['lpath_id'] = new stdClass(); $ret_obj['lpath_id']->value = isset($lpath_contents[$i]->lpath_id) ? $lpath_contents[$i]->lpath_id : 0;
		return $ret_obj;
	}
	//return $ret_str;
	return $ret_obj;
}
function jlms_getXML_start(&$lpath_contents, $i, &$mark_complete, $new_unique_id, $new_start_id, &$mark_pending, $is_restart, &$lp_params) {
	global $JLMS_DB, $JLMS_CONFIG, $my;
	
	/**/$ret_obj = array();
		
	$cond_ar = jlms_LPathPlay_checkConditions($lpath_contents, $i);
	if (isset($cond_ar['is_continue']) && $cond_ar['is_continue']){
	
		/**/$ret_obj['task'] = new stdClass();
		if ($is_restart) {
			/**/$ret_obj['task']->value = 'start_restart';
			$ret_str = '<task>start_restart</task>';
		} else {
			/**/$ret_obj['task']->value = 'start';
			$ret_str = '<task>start</task>';
		}
		/**/$ret_obj['user_unique_id'] = new stdClass(); $ret_obj['user_unique_id']->value = $new_unique_id;
		/**/$ret_obj['user_start_id'] = new stdClass(); $ret_obj['user_start_id']->value = $new_start_id;
	
		$time_res = jlms_timeToShow( $lpath_contents[$i] );
		if (!$time_res->result) {
			JLMS_Track_REM_StepStatus( $ret_obj['user_start_id'], $lpath_contents[$i]->course_id, $lpath_contents[$i]->lpath_id, $lpath_contents[$i]->id);	
			$mess = '';							 												
			$ret_obj['course_id'] = new stdClass(); $ret_obj['course_id']->value = isset($lpath_contents[$i]->course_id) ? $lpath_contents[$i]->course_id : $JLMS_CONFIG->get('course_id', 0);
			$ret_obj['lpath_id'] = new stdClass(); $ret_obj['lpath_id']->value = isset($lpath_contents[$i]->lpath_id) ? $lpath_contents[$i]->lpath_id : 0;
			if ($time_res->time_left) {
				$msg_time_released = _JLMS_REQUESTED_RESOURCE_WILL_BE_RELEASED;
				if (isset($time_res->time_left_days) && $time_res->time_left_days) {
					$msg_time_released .= ' '.$time_res->time_left_days.' '._JLMS_RELEASED_IN_DAYS;
				}
				if (isset($time_res->time_left_hours) && $time_res->time_left_hours) {
					$msg_time_released .= ' '.$time_res->time_left_hours.' '._JLMS_RELEASED_IN_HOURS;
				}
				if (isset($time_res->time_left_minutes) && $time_res->time_left_minutes) {
					$msg_time_released .= ' '.$time_res->time_left_minutes.' '._JLMS_RELEASED_IN_MINUTES;
				}
			} else {
				$msg_time_released = _JLMS_LPATH_RESOURCE_UNAVAILABLE;
			}
			jlms_getXML_CustomCond( $ret_obj, $mark_complete, $lpath_contents, $i, $msg_time_released, 'start' );						
			return $ret_obj;
		}
		$ret_str .= '<user_unique_id>'.$new_unique_id.'</user_unique_id>';
		$ret_str .= '<user_start_id>'.$new_start_id.'</user_start_id>';
		
		$ret_str .= JLMS_prepareLPath_ElemObj($ret_obj,$lpath_contents[$i], $new_unique_id, $new_start_id);
		$toolbar = array();		
		
		if ($i > 0) {
			$toolbar[] = array('btn_type' => 'prev', 'btn_js' => "javascript:ajax_action('prev_lpathstep');");
		}
		
		/*Fix if first quiz*/
		if(isset($lpath_contents[$i]->step_type) && $lpath_contents[$i]->step_type == 5){
			$query = "SELECT b.stu_quiz_id FROM #__lms_learn_path_results as a, #__lms_learn_path_step_quiz_results as b WHERE a.user_id = '".$my->id."'"
			. "\n AND a.course_id = '".$lpath_contents[$i]->course_id."' AND a.lpath_id = '".$lpath_contents[$i]->lpath_id."' AND b.result_id = a.id AND b.step_id = ".$lpath_contents[$i]->id;
			$JLMS_DB->SetQuery( $query );
			$stu_quiz_id = $JLMS_DB->LoadResult();
			if (!$stu_quiz_id) { $stu_quiz_id = 0; }
	
			$stu_quiz_data = NULL;
			$resume_quiz = NULL;
			if ($stu_quiz_id) {
				$query = "SELECT c_passed, c_total_time, c_total_score FROM #__lms_quiz_r_student_quiz WHERE c_id = $stu_quiz_id";
				$JLMS_DB->SetQuery( $query );
				$stu_quiz_data = $JLMS_DB->LoadObject();
			} else {
				// find any not-completed quiz attempt started from the 'quizzes' tool
				$query = "SELECT c_id, unique_id, c_quiz_id FROM #__lms_quiz_r_student_quiz WHERE c_quiz_id = ".$lpath_contents[$i]->item_id." AND c_student_id = ".$my->id." and c_passed = 0 and c_total_time = 0 ORDER BY c_id desc LIMIT 1";
				$JLMS_DB->SetQuery($query);
				$tmp_resume_quiz = $JLMS_DB->LoadObject();
				if (is_object($tmp_resume_quiz)) {
					$stu_quiz_id = $tmp_resume_quiz->c_id;
					$query = "SELECT id FROM #__lms_learn_path_results as a WHERE a.user_id = '".$my->id."'"
					. "\n AND a.course_id = '".$lpath_contents[$i]->course_id."' AND a.lpath_id = '".$lpath_contents[$i]->lpath_id."'";
					$JLMS_DB->SetQuery($query);
					$tmp_result_id = $JLMS_DB->LoadResult();
					$query = "UPDATE #__lms_learn_path_step_quiz_results SET stu_quiz_id = $stu_quiz_id WHERE result_id = $tmp_result_id AND step_id = ".$lpath_contents[$i]->id;
					$JLMS_DB->SetQuery($query);
					$JLMS_DB->query();
				}
			}
	
			if ($stu_quiz_id) {
				$query = "SELECT c_id, unique_id, c_quiz_id FROM #__lms_quiz_r_student_quiz WHERE c_id = $stu_quiz_id AND c_quiz_id = ".$lpath_contents[$i]->item_id." AND c_student_id = ".$my->id." and c_passed = 0 and c_total_time = 0 ORDER BY c_id desc LIMIT 1";
				$JLMS_DB->SetQuery($query);
				$resume_quiz = $JLMS_DB->LoadObject();
			}
	
			if(isset($resume_quiz->c_id)) {
				$query = "SELECT c_question_id FROM #__lms_quiz_r_student_question WHERE c_stu_quiz_id = ".$resume_quiz->c_id." ORDER BY c_id desc LIMIT 1";
				$JLMS_DB->SetQuery($query);
				$last_question = $JLMS_DB->LoadResult();
	
				if (!$last_question) {
					$last_question = -1;
				}
	
				$quiz_params->resume_quiz = $resume_quiz->c_id;
				$quiz_params->unique_id = $resume_quiz->unique_id;
				$quiz_params->last_question = $last_question;
			}			
			
			if(isset($lpath_contents[$i]->item_id) && $lpath_contents[$i]->item_id) {
				$query = "SELECT count(c_id) FROM #__lms_quiz_r_student_quiz WHERE c_quiz_id = ".$lpath_contents[$i]->item_id." AND c_student_id = '".$my->id."'";
				$JLMS_DB->SetQuery($query);
				$count = $JLMS_DB->LoadResult();
	
				$quiz_params->attempts_of_this_quiz = $count;
			}
			else {
				$quiz_params->attempts_of_this_quiz = 0;
			}
	
			$query = "SELECT c_max_numb_attempts, c_min_after FROM #__lms_quiz_t_quiz WHERE c_id = ".$lpath_contents[$i]->item_id."";
			$JLMS_DB->SetQuery($query);
			$some_quiz_params = $JLMS_DB->LoadObject();
			if (is_object($some_quiz_params)) {
				$quiz_params->c_max_numb_attempts = $some_quiz_params->c_max_numb_attempts;
				$quiz_params->c_min_after = $some_quiz_params->c_min_after;
			} else {
				$quiz_params->c_max_numb_attempts = 0;
				$quiz_params->c_min_after = 0;
			}
	
			//-----------
			$do_resume = isset($quiz_params->resume_quiz) && $quiz_params->resume_quiz && $quiz_params->last_question;
			if (isset($stu_quiz_data->c_passed) && $stu_quiz_data->c_passed || (($quiz_params->attempts_of_this_quiz >= $quiz_params->c_max_numb_attempts) &&  $quiz_params->c_max_numb_attempts > 0 && !$do_resume)) {
				$toolbar[] = array('btn_type' => 'next', 'btn_js' => "javascript:ajax_action('next_lpathstep');");
			} else {
				if($do_resume) {
					$toolbar[] = array('btn_type' => 'next', 'btn_js' => "javascript:jq_ResumeQuizOn(".$quiz_params->resume_quiz.",'".$quiz_params->unique_id."', ".$quiz_params->last_question.");");
				} elseif( ($quiz_params->attempts_of_this_quiz < $quiz_params->c_max_numb_attempts) || $quiz_params->c_max_numb_attempts == 0) {
					$do_start_no_time_limit = true;
					$q_time_limit = ($quiz_params->c_min_after)*60;
					if ($q_time_limit) {
//						$query = "SELECT max(c_date_time) as time_last_access FROM #__lms_quiz_r_student_quiz WHERE c_quiz_id = '".$lpath_contents[$i]->item_id."' and c_student_id = '".$my->id."'";
						$query = "SELECT MAX(FROM_UNIXTIME(UNIX_TIMESTAMP(c_date_time) + c_total_time)) as time_last_access"
						. "\n FROM #__lms_quiz_r_student_quiz"
						. "\n WHERE 1"
						. "\n AND c_quiz_id = '".$lpath_contents[$i]->item_id."'"
						. "\n AND c_student_id = '".$my->id."'"
						. "\n ORDER BY (UNIX_TIMESTAMP(c_date_time) + c_total_time)"
						;
						$JLMS_DB->SetQuery( $query );
						$q_last_access = $JLMS_DB->LoadResult();
						if ($q_last_access) { //'STRTOTIME with an empty parameter' bug fixed (02.10.2006)
							$q_last_access_t = strtotime($q_last_access);
							$time_goes = time() - date('Z') - $q_last_access_t;
							
							if ($time_goes > 1) { //esli proshla 1 minuta (changed to 10 secs)
								if ($time_goes < $q_time_limit) {
									$message = str_replace("{text}", (($q_time_limit - $time_goes) >60)?(floor(($q_time_limit - $time_goes)/60).' minute(s)'):(($q_time_limit - $time_goes). ' seconds'), _JLMS_q_quiz_comeback_later);
									$message .= ' '.$time_goes;
									$message = '<div class="joomlalms_sys_message">'.$message.'</div>';
									$ret_obj['step_descr'] = new stdClass(); $ret_obj['step_descr']->value = $message; $ret_obj['step_descr']->is_cdata = true;
									$do_start_no_time_limit = false;
								}
							}
						}
					}
					if ($do_start_no_time_limit) {
						$toolbar[] = array('btn_type' => 'next', 'btn_js' => "javascript:jq_StartQuizOn();");
					} else {
						$toolbar[] = array('btn_type' => 'next', 'btn_js' => "javascript:ajax_action('next_lpathstep');");
					}
				}
			}
		} else {
			$toolbar[] = array('btn_type' => 'next', 'btn_js' => "javascript:ajax_action('next_lpathstep');");
		}
				
		if( $lp_params->get('navigation_type', 0 ) != 2 ) {
			$toolbar[] = array('btn_type' => 'contents', 'btn_js' => "javascript:ajax_action('contents_lpath');");
		}
		
		$toolbar = showPrevButtonLP($toolbar, $lp_params);
		
		$tool_txt = JLMS_ShowToolbar($toolbar);
		$tool_txt = str_replace('"components/com_joomla_lms','"'.$JLMS_CONFIG->getCfg('live_site').'/components/com_joomla_lms',$tool_txt);
	
		/**/$ret_obj['menu_contents'] = new stdClass();$ret_obj['menu_contents']->value = $tool_txt;$ret_obj['menu_contents']->is_cdata = true;
	
		$ret_str .= '<menu_contents><![CDATA['.$tool_txt.']]></menu_contents>'."\n";
		$p_steps = implode(',',$mark_pending);
		if ($is_restart) {
			$i_steps = array();
			$r = 0;
			while ($r < count($lpath_contents)) {
				$i_steps[] = $lpath_contents[$r]->id;
				$r ++;
			}
			if (empty($i_steps)) { $i_steps = array(0); }
			$i_steps = implode(',',$i_steps);
	
			/**/$ret_obj['incompleted_steps'] = new stdClass();$ret_obj['incompleted_steps']->value = $i_steps;
	
			$ret_str .= '<incompleted_steps>'.$i_steps.'</incompleted_steps>'."\n";
	
			$quiz_ids = array();
			$count_quizzes = 0;
			foreach ($lpath_contents as $lc) {
				if ($lc->step_type == 5) {
					$count_quizzes ++;
					$quiz_ids[] = $lc->item_id;
				}
			}
			$inc_quests = array();
			if ($count_quizzes) {
				$qq = implode(',',$quiz_ids);
				$query = "SELECT c_id FROM #__lms_quiz_t_question"
				. "\n WHERE c_quiz_id IN ($qq)";
				$JLMS_DB->SetQuery( $query );
				$inc_quests = JLMSDatabaseHelper::LoadResultArray();
			}
			if (empty($inc_quests)) { $inc_quests = array(0); }
			$inc_quests = implode(',',$inc_quests);
	
			/**/$ret_obj['incompleted_quests'] = new stdClass();$ret_obj['incompleted_quests']->value = $inc_quests;
	
			$ret_str .= '<incompleted_quests>'.$inc_quests.'</incompleted_quests>'."\n";
		}
	
		/**/$ret_obj['pending_steps'] = new stdClass();$ret_obj['pending_steps']->value = $p_steps;
	
		$ret_str .= '<pending_steps>'.$p_steps.'</pending_steps>'."\n";
		if (count($mark_complete)) {
			$c_steps = implode(',',$mark_complete);
		} else { $c_steps = 0; }
	
		/**/$ret_obj['completed_steps'] = new stdClass();$ret_obj['completed_steps']->value = $c_steps;
	
		$ret_str .= '<completed_steps>'.$c_steps.'</completed_steps>'."\n";
	
	} else {
		JLMS_Track_REM_StepStatus( $new_start_id, $lpath_contents[$i]->course_id, $lpath_contents[$i]->lpath_id, $lpath_contents[$i]->id);
		$ret_obj = jlms_getXML_cond( $cond_ar, $mark_complete, $lpath_contents[$i]->step_name, $lp_params );
		
		/**/$ret_obj['user_unique_id'] = new stdClass(); $ret_obj['user_unique_id']->value = $new_unique_id;
		/**/$ret_obj['user_start_id'] = new stdClass(); $ret_obj['user_start_id']->value = $new_start_id;
		/**/$ret_obj['course_id'] = new stdClass(); $ret_obj['course_id']->value = isset($lpath_contents[$i]->course_id) ? $lpath_contents[$i]->course_id : $JLMS_CONFIG->get('course_id', 0);
		/**/$ret_obj['lpath_id'] = new stdClass(); $ret_obj['lpath_id']->value = isset($lpath_contents[$i]->lpath_id) ? $lpath_contents[$i]->lpath_id : 0;
		return $ret_obj;
	}
	
	//return $ret_str;
	return $ret_obj;
}
function JLMS_finishLPath( $lpath_id, $option, $step_id, &$t_ar, $is_quick = false, $new_unique_id = '', $new_start_id = 0 ) {
	global $JLMS_CONFIG, $JLMS_DB, $my;
	
	$query = "SELECT lp_params FROM #__lms_learn_paths WHERE id = $lpath_id";
	$JLMS_DB->SetQuery($query);
	$lp_params_str = $JLMS_DB->LoadResult();
	$lp_params = new JLMSParameters($lp_params_str);
	
	//b Events JLMS Plugins
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$_JLMS_PLUGINS->loadBotGroup('system');
	$plg_params = array();
	$plg_params['course_id'] = $JLMS_CONFIG->get('course_id');
	$plg_params['lpath_id'] = $lpath_id;
	$plg_params['is_quick'] = $is_quick;
	$_JLMS_PLUGINS->trigger('onFinishLpath', array(&$plg_params));
	//e Events JLMS Plugins
	
	/**/$ret_obj = array();
	/**/$ret_obj['task'] = new stdClass(); $ret_obj['task']->value = ($is_quick?'finish_lpath_quick':'finish_lpath');

	$ret_str = '<task>'.($is_quick?'finish_lpath_quick':'finish_lpath').'</task>';
	if ($is_quick) {
		$ret_str .= '<user_unique_id>'.$new_unique_id.'</user_unique_id>';
		$ret_str .= '<user_start_id>'.$new_start_id.'</user_start_id>';
	}

	/**/$ret_obj['user_unique_id'] = new stdClass();$ret_obj['user_unique_id']->value = $new_unique_id;
	/**/$ret_obj['user_start_id'] = new stdClass();$ret_obj['user_start_id']->value = $new_start_id;

	$toolbar = array();
	$toolbar[] = array('btn_type' => 'restart', 'btn_js' => "javascript:ajax_action('lpath_restart');");
	#$toolbar[] = array('btn_type' => 'contents', 'btn_js' => "javascript:ajax_action('contents_lpath');");
	// contents i tak otobragayutsa na finish-strani4ke
	$tool_txt = JLMS_ShowToolbar($toolbar);
	$tool_txt = str_replace('"components/com_joomla_lms','"'.$JLMS_CONFIG->getCfg('live_site').'/components/com_joomla_lms',$tool_txt);

	/**/$ret_obj['menu_contents'] = new stdClass(); $ret_obj['menu_contents']->value = $tool_txt; $ret_obj['menu_contents']->is_cdata = true;

	$ret_str .= '<menu_contents><![CDATA['.$tool_txt.']]></menu_contents>'."\n";
	
	/**/$ret_obj['step_name'] = new stdClass(); $ret_obj['step_name']->value = _JLMS_LPATH_COMPLETE_TITLE; $ret_obj['step_name']->is_cdata = true;

	$ret_str .= '<step_name><![CDATA['._JLMS_LPATH_COMPLETE_TITLE.']]></step_name>'."\n";
	$htm_txt = '<table class="jlms_table_no_borders"><tr><td align=\'left\'>'._JLMS_LPATH_START_TIME_TEXT. '&nbsp;</td><td align=\'left\'><strong>'.JLMS_dateToDisplay($t_ar['start_time'], false, $JLMS_CONFIG->get('offset')*60*60, ($JLMS_CONFIG->get('hide_lpath_results_time', false) ? ' ' : ' H:i:s')).'</strong></td></tr>';
	$htm_txt .= '<tr><td align=\'left\'>'._JLMS_LPATH_END_TIME_TEXT. '&nbsp;</td><td align=\'left\'><strong>'.JLMS_dateToDisplay($t_ar['end_time'], false, $JLMS_CONFIG->get('offset')*60*60, ($JLMS_CONFIG->get('hide_lpath_results_time', false) ? ' ' : ' H:i:s')).'</strong></td></tr></table>';
	
	$ret_str .= '<step_descr><![CDATA['.$htm_txt.']]></step_descr>'."\n";

	/**/$ret_obj['step_descr'] = new stdClass(); $ret_obj['step_descr']->value = $htm_txt; $ret_obj['step_descr']->is_cdata = true;
	/**/$ret_obj['completed_steps'] = new stdClass(); $ret_obj['completed_steps']->value = $step_id;

	$ret_str .= '<completed_steps>'.$step_id.'</completed_steps>'."\n";

	// 16 August 2007 (DEN) - new feaure - learning path completion message
	global $JLMS_DB;
	$query = "SELECT lp_params FROM #__lms_learn_paths WHERE id = $lpath_id";
	$JLMS_DB->SetQuery($query);
	$lp_params_str = $JLMS_DB->LoadResult();
	$lp_params = new JLMSParameters($lp_params_str);
	$compl_msg = $lp_params->get('lpath_completion_msg','');
	
	$step_script = '';
	
	if($JLMS_CONFIG->get('enable_timetracking', false)){
		$step_script .= "TTracker_".$JLMS_CONFIG->get('course_id')."_".$my->id."_9_".$lpath_id.".stop();"; //for timetracking
		$step_script .= "TTracker_".$JLMS_CONFIG->get('course_id')."_".$my->id."_9_".$lpath_id.".hide_online();"; //for timetracking
		$step_script .= "TTracker_".$JLMS_CONFIG->get('course_id')."_".$my->id."_9_".$lpath_id.".loop(1);"; //for timetracking
	}
	
	if ($compl_msg) {
		$compl_msg = base64_decode($compl_msg);
		$ar = JLMS_ShowText_WithFeatures($compl_msg, true, true);
		$compl_msg = $ar['new_text'];//JLMS_ShowText_WithFeatures($elem->step_description, true);
		
		$step_script .= $ar['js'];
		$ret_str .= '<show_completion_msg>1</show_completion_msg>'."\n";
		$ret_str .= '<lpath_completion_msg><![CDATA['.$compl_msg.']]></lpath_completion_msg>'."\n";
		/**/$ret_obj['show_completion_msg'] = new stdClass(); $ret_obj['show_completion_msg']->value = '1';
		/**/$ret_obj['lpath_completion_msg'] = new stdClass(); $ret_obj['lpath_completion_msg']->value = $compl_msg; $ret_obj['lpath_completion_msg']->is_cdata = true;
	} else {
		$ret_str .= '<show_completion_msg>0</show_completion_msg>'."\n";
		/**/$ret_obj['show_completion_msg'] = new stdClass(); $ret_obj['show_completion_msg']->value = '0';
	}
	if ($step_script) {
		$ret_str .= '<step_exec_script>1</step_exec_script>'."\n";
		/**/$ret_obj['step_exec_script'] = new stdClass(); $ret_obj['step_exec_script']->value = '1';
		$ret_str .= '<step_exec_script_contents><![CDATA['.($step_script ? $step_script : ' ').']]></step_exec_script_contents>'."\n";
		/**/$ret_obj['step_exec_script_contents'] = new stdClass(); $ret_obj['step_exec_script_contents']->value = ($step_script ? $step_script : ' '); $ret_obj['step_exec_script_contents']->is_cdata = true;
	} else {
		$ret_str .= '<step_exec_script>0</step_exec_script>'."\n";
		/**/$ret_obj['step_exec_script'] = new stdClass(); $ret_obj['step_exec_script']->value = '0';
	}

        // isHack start - course finished - pay to child and mark as finished
        $my = plotUser::factory();
        if (!$my->isCourseFinished($plg_params['course_id'])) {
			require_once JPATH_ROOT.'/components/com_plot/models/conversations.php';
			$courseObj=new plotCourse($plg_params['course_id']);
			$conversationModel = JModelLegacy::getInstance('conversations', 'PlotModel');

			$config = JFactory::getConfig();
			$sender = array(
				$config->get('mailfrom'),
				$config->get('fromname')
			);

			if($courseObj->isSmall()){
				$my->finishCourse($plg_params['course_id']);
				$my->setCourseFinished($plg_params['course_id']);
				plotPoints::assign( 'course.finish' , 'com_plot' , $my->id, $plg_params['course_id']);
				$finishedPrice = $my->getFinishedPriceForMyCourse($plg_params['course_id']);
				$my->addMoney($finishedPrice, $my->id);
				$bauer=(int)$courseObj->whoBoughtCourse($my->id);

                if(plotUser::factory((int)$bauer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory((int)$bauer)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                    $conversationModel->sendMessageSystem((int)$bauer, 'plot-money-course-get-' . $plg_params['course_id'] . '-' . (int)$my->id);
                }
                if($my->getSocialFieldData('UNSUBSCRIBE')==1 || $my->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                    $conversationModel->sendMessageSystem($my->id, 'plot-money-course-get-' . $plg_params['course_id'] . '-' . $my->id);
                }
				$actor = '<a href="' . JRoute::_('index.php?option=com_plot&view=profile&id=' . $my->id) . '">' . $my->name . '</a>';
				$target = '<a href="' . JRoute::_('index.php?option=com_plot&view=course&id=' . $plg_params['course_id']) . '">' . $courseObj->course_name . '</a>';

                if(plotUser::factory((int)$bauer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory((int)$bauer)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                    $mailer = JFactory::getMailer();
                    $mailer->setSender($sender);
                    $mailer->addRecipient(plotUser::factory($bauer)->email);
                    $mailer->setSubject(JText::_('COM_PLOT_SEND_MONEY_GET_SUBJECT'));
                    $mailer->isHTML(true);
                    $mailer->setBody(JText::sprintf("COM_PLOT_SEND_MONEY_COURSE_GET", $actor, $target));
                    $mailer->Send();
                }
                if($my->getSocialFieldData('UNSUBSCRIBE')==1 || $my->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                    $mailer = JFactory::getMailer();
                    $mailer->setSender($sender);
                    $mailer->addRecipient($my->email);
                    $mailer->setSubject(JText::_('COM_PLOT_SEND_MONEY_GET_SUBJECT'));
                    $mailer->isHTML(true);

                    $mailer->setBody(JText::sprintf("COM_PLOT_SEND_MONEY_COURSE_GET", $actor, $target));
                    $mailer->Send();
                }


			}else{

                $bauer=(int)$courseObj->whoBoughtCourse($my->id);
                $target = '<a href="' . JRoute::_('index.php?option=com_plot&view=course&id=' . $plg_params['course_id']) . '">' . $courseObj->course_name . '</a>';
                if($my->getSocialFieldData('UNSUBSCRIBE')==1 || $my->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                    $conversationModel->sendMessageSystem($my->id, 'plot-course-passed-' . $plg_params['course_id'] . '-' . $my->id);
                }
                if(plotUser::factory((int)$bauer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory((int)$bauer)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                    $conversationModel->sendMessageSystem((int)$bauer, 'plot-course-passed-' . $plg_params['course_id'] . '-' . (int)$my->id);
                }
                if($my->getSocialFieldData('UNSUBSCRIBE')==1 || $my->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                    $mailer = JFactory::getMailer();
                    $mailer->setSender($sender);
                    $mailer->addRecipient($my->email);
                    $mailer->setSubject(JText::_('COM_PLOT_SEND_COURSE_SUBJECT'));
                    $mailer->isHTML(true);
                    $mailer->setBody(JText::sprintf("COM_PLOT_SEND_COURSE_PASSED", $my->name, $target));
                    $mailer->Send();
                }

                if(plotUser::factory((int)$bauer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory((int)$bauer)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
                    $mailer = JFactory::getMailer();
                    $mailer->setSender($sender);
                    $mailer->addRecipient(plotUser::factory((int)$bauer)->email);
                    $mailer->setSubject(JText::_('COM_PLOT_SEND_COURSE_SUBJECT'));
                    $mailer->isHTML(true);
                    $mailer->setBody(JText::sprintf("COM_PLOT_SEND_COURSE_PASSED", $my->name, $target));
                    $mailer->Send();
                }


			}


        }
        // isHack end
        
	return $ret_obj;
}
function jlms_LPathPlay_findNextStep(&$lpath_contents, $i) {
	$t = $i;
	$next_item = -1;
	while ($t < count($lpath_contents)) {
		if ( (isset($lpath_contents[$t]->new_step_status) && ($lpath_contents[$t]->new_step_status != 1)) || !isset($lpath_contents[$t]->new_step_status) ) {
			$next_item = $t;
			break;
		}
		$t ++;
	}
	if ($next_item == -1) {
		$t = 0;
		while ($t <= $i) {
			if ( (isset($lpath_contents[$t]->new_step_status) && ($lpath_contents[$t]->new_step_status != 1)) || !isset($lpath_contents[$t]->new_step_status) ) {
				$next_item = $t;
				break;
			}
			$t ++;
		}
	}
	return $next_item;
}
// chapter - pending, poka vse vnutrennie ne proshli.
// document - pending, poka ne ska4al document
// documents (page without file) - first step - pending, after pending - complete (!only if conditions pozvolyayut !!! BUG !!!).
// link - first step - pending, after pending - complete (!only if conditions pozvolyayut !!! BUG !!!).
function JLMS_checkthisparent(&$lpath_contents, $t, &$ids_for_complete) {
	$i = $t;
	$pid = $lpath_contents[$i]->id;
	$j = 0;
	$inside_items = 0;
	$completed = true;
	while ($j < count($lpath_contents)) {
		if ( ($lpath_contents[$j]->parent_id == $pid) && ($lpath_contents[$j]->step_type == 1) ) {
			if ($lpath_contents[$j]->new_step_status != 1) {
				$sc = JLMS_checkthisparent($lpath_contents, $j, $ids_for_complete);
				if (!$sc) { $completed = false; }
			}
			$inside_items++;//calculating of nested items
		} elseif ( ($lpath_contents[$j]->parent_id == $pid) && ($lpath_contents[$j]->step_type != 1) ) {
			if ($lpath_contents[$j]->new_step_status != 1) {
				$completed = false;
			}
			$inside_items++;//calculating of nested items
		}
		$j++;
	}
	if (!$inside_items && $completed && ($lpath_contents[$i]->new_step_status == 0)) { // if no nested items and this parent not 'pending'
		$completed = false;
	}
	if ($completed) {
		$ids_for_complete[] = $pid;
	}
	return $completed;
}
function JLMS_getPendingSteps(&$lpath_contents) {
	$ra = array();
	$t = 0;
	while ($t < count($lpath_contents)) {
		if (isset($lpath_contents[$t]->new_step_status) && ($lpath_contents[$t]->new_step_status == 2)) { $ra[] = $lpath_contents[$t]->id; }
		$t++;
	}
	if (empty($ra)) {
		$ra = array(0);
	}
	return $ra;
}

/**
 * BUGS with completing status and conditions!!
 */
function JLMS_Track_processSteps($start_id, $course_id, $lpath_id, &$lpath_contents, $i) {
	global $JLMS_DB, $my;
	
	$JLMS_CONFIG = JLMSFactory::getConfig();
	
	//1 - chapter, 2 - doc, 3 - link;
	// if chapter without (not completed) items - mark complete;
	// if chapter with not completed items - mark pending
	// (07 August 2007) - BUG. If chapter without items and it is not pending - it automatically marked as completed. ;)
	// each step check items for all chapters and try to mark chapter completed AND MARK pending items (links only? + some docs) completed.
	$query = "SELECT a.*, b.id as res_id FROM #__lms_learn_path_step_results as a, #__lms_learn_path_results as b"
	. "\n WHERE a.result_id = b.id AND b.user_id = '".$my->id."' AND b.course_id = '".$course_id."' AND b.lpath_id = '".$lpath_id."'";
	#$query = "SELECT * FROM #__lms_track_learnpath_steps"
	#. "\n WHERE user_id = '".$my->id."' AND start_id = '".$start_id."' AND lpath_id = '".$lpath_id."'";
	$JLMS_DB->SetQuery( $query );
	$view_items = $JLMS_DB->LoadObjectList();
	
	$res_id = 0;
	if (isset($view_items[0]->res_id)) { $res_id = $view_items[0]->res_id; }
	$lc = 0;
	while ( $lc < count($lpath_contents)) {
		$lpath_contents[$lc]->new_step_status = 0;
		foreach( $view_items as $view_item) {
			if ($view_item->step_id == $lpath_contents[$lc]->id) {
				$lpath_contents[$lc]->new_step_status = $view_item->step_status;
				$lpath_contents[$lc]->step_track_id = $view_item->id; //step_result_id
			}
		}
		$lc ++;
	}
	$ids_for_complete = array();
	$ids_completed = array();
	$t = 0;
	while ($t < count($lpath_contents)) {
		
		$completed = 1;
		if($JLMS_CONFIG->get('enable_timetracking')){
			$cond_data = array();
			jlms_LPathPlay_searchConds($lpath_contents, $t, $cond_data);
			//if(isset($cond_data[0])){ //old
			if(isset($cond_data[0]) && !$lpath_contents[$t]->new_step_status){
				//if($lpath_contents[$t]->time_spent < $cond_data[0]->cond_time * 60 + $cond_data[0]->cond_time_sec){
				if($lpath_contents[$t]->time_spent < $cond_data[0]->cond_time){
					$completed = 0;
				}
			}
		}
		
		if($completed){
			//mark 'pending' link as 'completed'
			if (($t != $i) && ( ($lpath_contents[$t]->step_type == 3) || ($lpath_contents[$t]->step_type == 4) ) && ($lpath_contents[$t]->new_step_status == 2) ) {
				$cond_ar = jlms_LPathPlay_checkConditions($lpath_contents, $t);
				if (isset($cond_ar['is_continue']) && $cond_ar['is_continue']) {
					$ids_for_complete[] = $lpath_contents[$t]->id;
					$lpath_contents[$t]->new_step_status = 1;
				}
			}
			//mark downloaded documents as 'completed'
			if (($t != $i) && ($lpath_contents[$t]->step_type == 2) && ($lpath_contents[$t]->new_step_status == 3) ) {
				$ids_for_complete[] = $lpath_contents[$t]->id;
				$lpath_contents[$t]->new_step_status = 1;
			}
			//mark 'pending' document (only if this document is content page without file) as 'completed'
			if (($t != $i) && ($lpath_contents[$t]->step_type == 2) && ($lpath_contents[$t]->new_step_status == 2) ) {
				if ( isset($lpath_contents[$t]->file_id) && !$lpath_contents[$t]->file_id ) {
					$cond_ar = jlms_LPathPlay_checkConditions($lpath_contents, $t);
					if (isset($cond_ar['is_continue']) && $cond_ar['is_continue']) {
						$ids_for_complete[] = $lpath_contents[$t]->id;
						$lpath_contents[$t]->new_step_status = 1;
					}
				}
			}
			//mark 'pending' document (only if this document is ZIP-CONTENT package) as 'completed'
			if (($t != $i) && ($lpath_contents[$t]->step_type == 2) && ($lpath_contents[$t]->new_step_status == 2) ) {
				if ( isset($lpath_contents[$t]->file_id) && $lpath_contents[$t]->file_id && isset($lpath_contents[$t]->folder_flag) && ($lpath_contents[$t]->folder_flag == 2) ) {
					$cond_ar = jlms_LPathPlay_checkConditions($lpath_contents, $t);
					if (isset($cond_ar['is_continue']) && $cond_ar['is_continue']) {
						$ids_for_complete[] = $lpath_contents[$t]->id;
						$lpath_contents[$t]->new_step_status = 1;
					}
				}
			}
			//mark 'pending' quiz (only if this quiz passed by student) as 'completed'
			if (($t != $i) && ($lpath_contents[$t]->step_type == 5) && ($lpath_contents[$t]->new_step_status == 2) ) {
				$query = "SELECT a.* FROM #__lms_quiz_r_student_quiz as a, #__lms_learn_path_step_quiz_results as b"
				. "\n WHERE b.result_id = $res_id AND b.step_id = ".$lpath_contents[$t]->id." AND b.stu_quiz_id = a.c_id";
				$JLMS_DB->SetQuery( $query );
				$quiz_res = $JLMS_DB->LoadObject();
				if (is_object($quiz_res) && isset($quiz_res->c_passed)) {
					if ($quiz_res->c_passed) {
						$ids_for_complete[] = $lpath_contents[$t]->id;
						$lpath_contents[$t]->new_step_status = 1;
					}
				}
			}
			//mark 'pending' scorm, if it is completed (checks by 'tracking type' - last/best attempt) as 'completed'
			if (($t != $i) && ($lpath_contents[$t]->step_type == 6) && ($lpath_contents[$t]->new_step_status == 2) ) {
				$scn_id = 0;
				$query = "SELECT item_id FROM #__lms_learn_paths WHERE id = ".$lpath_contents[$t]->item_id." AND course_id = $course_id AND (lp_type = 1 OR lp_type = 2)";
				$JLMS_DB->SetQuery($query);
				$scn_id = $JLMS_DB->loadResult();
				/*echo $JLMS_DB->geterrormsg();
				echo $scn_id;die;*/
				if ($scn_id) {
					require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_scorm.lib.php");
					$user_ids = array();
					$scn_ids = array();
					$scn_ids[] = $scn_id;
					$user_ids[] = $my->id;
	
	
					// Why so many comments? ;)
					//if tracking_type == by the last attempt - all is ok
					//if tracking_type == 'by the best attempt' - all time pending scorms will be marked as 'completed' - in this case we are emulate 'by the last attempt'
	
					/*global $JLMS_CONFIG;
					$course_params = $JLMS_CONFIG->get('course_params');
					$params = new JLMSParameters($course_params);*/
					$scorm_n_ans = & JLMS_Get_N_SCORM_userResults($user_ids, $scn_ids, 0);//$params->get('track_type', 0));
	
					if (isset($scorm_n_ans[0]->status) && $scorm_n_ans[0]->status == 1) {
						$ids_for_complete[] = $lpath_contents[$t]->id;
						$lpath_contents[$t]->new_step_status = 1;
					}
				}
			}
	
			if ($lpath_contents[$t]->new_step_status == 1) {
				$ids_completed[] = $lpath_contents[$t]->id;
			}
		}
		$t ++;
	}
	//check all parents for mark theys as 'completed'
	$t = 0;
	while ($t < count($lpath_contents)) {
		if ( ($lpath_contents[$t]->parent_id == 0) && ($lpath_contents[$t]->step_type == 1) && ($lpath_contents[$t]->new_step_status != 1) && $t != $i ) {
			JLMS_checkthisparent($lpath_contents, $t, $ids_for_complete);
			
		}
		$t ++;
	}
	if (count($ids_for_complete)) {
		$ids = array();
		$t = 0;
		while ($t < count($lpath_contents)) {
			if (in_array($lpath_contents[$t]->id, $ids_for_complete) && isset($lpath_contents[$t]->step_track_id) && $lpath_contents[$t]->step_track_id) {
				$ids[] = $lpath_contents[$t]->step_track_id;
				if ( ($lpath_contents[$t]->new_step_status == 2) || ($lpath_contents[$t]->new_step_status == 3) ) {
					$lpath_contents[$t]->new_step_status = 1;
				}
			}
			$t ++;
		}
		$ids = implode(',',$ids);
		$query = "UPDATE #__lms_learn_path_step_results SET step_status = 1 WHERE id IN ( $ids ) AND (step_status = 2 OR step_status = 3)";
		#$query = "UPDATE #__lms_track_learnpath_steps SET step_status = 1 WHERE id IN ( $ids ) AND (step_status = 2 OR step_status = 3)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
	}
	$ret_comp_ids = array_merge($ids_for_complete, $ids_completed);
	return $ret_comp_ids;
}
// (04.08.2007) - remove 'pending' status of step (i.e. when step is closed for viewving - with conditions)
function JLMS_Track_REM_StepStatus($start_id, $course_id, $lpath_id, $step_id) {
	global $JLMS_DB, $my;
	$query = "SELECT id FROM #__lms_learn_path_results WHERE user_id = '".$my->id."' AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
	$JLMS_DB->SetQuery( $query );
	$res_id = $JLMS_DB->LoadResult();
	if ($res_id) {
		$query = "DELETE FROM #__lms_learn_path_step_results "
			. "\n WHERE result_id = '".$res_id."' AND step_id = '".$step_id."'";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
	}
}

function JLMS_Track_MarkStepStatus($start_id, $course_id, $lpath_id, $step_id, &$element, $step_status = 2) {
	global $JLMS_DB, $my;
	
	$query = "SELECT id FROM #__lms_learn_path_results WHERE user_id = '".$my->id."' AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
	$JLMS_DB->SetQuery( $query );
	$res_id = $JLMS_DB->LoadResult();
	$query = "SELECT * FROM #__lms_learn_path_step_results"
	. "\n WHERE result_id = '".$res_id."' AND step_id = '".$step_id."'";
	#$query = "SELECT * FROM #__lms_track_learnpath_steps"
	#. "\n WHERE user_id = '".$my->id."' AND start_id = '".$start_id."'"
	#. "\n AND lpath_id = '".$lpath_id."' AND step_id = '".$step_id."'";
	$JLMS_DB->SetQuery( $query );
	$tls = $JLMS_DB->LoadObjectList();
	if (count($tls) == 1) {
		if ( (($step_status == 2) && ($tls[0]->step_status != 1) && ($tls[0]->step_status != 3)) || ($step_status != 2) ) {
			$query = "UPDATE #__lms_learn_path_step_results SET step_status = '".$step_status."'"
			. "\n WHERE result_id = '".$res_id."' AND step_id = '".$step_id."'";
			
			#$query = "UPDATE #__lms_track_learnpath_steps SET step_status = '".$step_status."'"
			#. "\n WHERE user_id = '".$my->id."' AND start_id = '".$start_id."'"
			#. "\n AND lpath_id = '".$lpath_id."' AND step_id = '".$step_id."'";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
		}
	} else {
		if ($res_id) {
			$query = "INSERT INTO #__lms_learn_path_step_results (result_id, step_id, step_status)"
			. "\n VALUES ('".$res_id."', '".$step_id."', '".$step_status."')";
			# uncomment $st_date
			#$query = "INSERT INTO #__lms_track_learnpath_steps (user_id, start_id, lpath_id, step_id, step_status, tr_time)"
			#. "\n VALUES ('".$my->id."', '".$start_id."', '".$lpath_id."', '".$step_id."', '".$step_status."', '".$st_date."')";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			$element->step_accessed_first_time = true;
		}
	}
}

function JLMS_writeCompleteStats($user_start_id, $user_unique_id, $course_id, $lpath_id){
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$Itemid = $JLMS_CONFIG->get('Itemid');
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$db = JFactory::getDbo();
	$user = JLMSFactory::getUser();
	$end_time = gmdate( 'Y-m-d H:i:s');
	$query = "SELECT * FROM #__lms_track_learnpath_stats WHERE id = '".$user_start_id."'";
	$db->SetQuery( $query );
	$st = $db->LoadObjectList();
	$ret_ar = array();
	$ret_ar['start_time'] = '';
	$ret_ar['end_time'] = '';
	if (count($st) == 1) {
		$st_time = $st[0]->start_time;
		$spent_time = strtotime($end_time) - strtotime($st_time);
		$user_points = 0;
		$user_status = 1; // 1 - completed
		$query = "UPDATE #__lms_track_learnpath_stats SET end_time = '".$end_time."', user_status = '".$user_status."' WHERE id = '".$user_start_id."'";
		$db->SetQuery( $query );
		$db->query();
		
		$query = "SELECT * FROM #__lms_learn_path_results WHERE user_id = '".$user->get('id')."'"
		. "\n AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
		$db->SetQuery( $query );
		$c = $db->LoadObjectList();
		$try_to_update_grades = false;
		if (count($c) == 1) {
			$ret_ar['start_time'] = $c[0]->start_time;
			$ret_ar['end_time'] = $c[0]->end_time;
			if ($c[0]->user_status != 1) {
				$ret_ar['end_time'] = $end_time;
				#$spent_time = $spent_time + $c[0]->user_time;
				$query = "UPDATE #__lms_learn_path_results SET user_points = '".$user_points."', user_time = '".$spent_time."', user_status = '".$user_status."', end_time = '".$end_time."'"
				. "\n WHERE user_id = '".$user->get('id')."' AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
				$db->SetQuery( $query );
				$db->query();
				$try_to_update_grades = true;
			}
		} else {
			$ret_ar['start_time'] = $st_time;
			$ret_ar['end_time'] = $end_time;
			//we never enter this section
			$query = "INSERT INTO #__lms_learn_path_results (course_id, lpath_id, user_id, user_points, user_time, user_status, start_time, end_time)"
			. "\n VALUES ('".$course_id."', '".$lpath_id."', '".$user->get('id')."', '".$user_points."', '".$spent_time."', '".$user_status."', '".$st_time."', '".$end_time."')";
			$db->SetQuery( $query );
			$db->query();
			$try_to_update_grades = true;
		}
		if ($user_status == 1) {
			//*** send email notification about lpath completion.
			$e_course = new stdClass();
			$e_course->course_alias = '';
			$e_course->course_name = '';			

			$query = "SELECT course_name, name_alias FROM #__lms_courses WHERE id = '".$course_id."'";
			$db->setQuery( $query );
			$e_course = $db->loadObject();

			$query = "SELECT lpath_name FROM #__lms_learn_paths WHERE id = '".$lpath_id."'";
			$db->setQuery( $query );
			$lpathname = $db->loadResult();

			$e_user = new stdClass();
			$e_user->name = '';
			$e_user->email = '';
			$e_user->username = '';

			$query = "SELECT email, name, username FROM #__users WHERE id = '".$user->get('id')."'";
			$db->setQuery( $query );
			$e_user = $db->loadObject();

			$e_params['user_id'] = $user->get('id');
			$e_params['course_id'] = $course_id;					
			$e_params['markers']['{email}'] = $e_user->email;	
			$e_params['markers']['{name}'] = $e_user->name;										
			$e_params['markers']['{username}'] = $e_user->username;
			$e_params['markers']['{coursename}'] = $e_course->course_name;//( $e_course->course_alias )?$e_course->course_alias:$e_course->course_name;
			$e_params['markers']['{lpathname}'] = $lpathname ? $lpathname : '';

			$e_params['markers']['{courselink}'] = JLMSEmailRoute("index.php?option=com_joomla_lms&Itemid=$Itemid&task=details_course&id=$course_id");

			$e_params['markers_nohtml']['{courselink}'] = $e_params['markers']['{courselink}'];
			$e_params['markers']['{courselink}'] = '<a href="'.$e_params['markers']['{courselink}'].'">'.$e_params['markers']['{courselink}'].'</a>';

			$e_params['markers']['{lmslink}'] = JLMSEmailRoute("index.php?option=com_joomla_lms&Itemid=$Itemid");

			$e_params['markers_nohtml']['{lmslink}'] = $e_params['markers']['{lmslink}'];
			$e_params['markers']['{lmslink}'] = '<a href="'.$e_params['markers']['{lmslink}'].'">'.$e_params['markers']['{lmslink}'].'</a>';

			$e_params['action_name'] = 'OnLPathCompletion';

			$_JLMS_PLUGINS->loadBotGroup('emails');
			$plugin_result_array = $_JLMS_PLUGINS->trigger('OnLPathCompletion', array (& $e_params));
			//*** end of email notifications	
		}

		if ($try_to_update_grades) {
			$do_update_grades = false;
			$query = "SELECT * FROM #__lms_learn_path_grades WHERE user_id = '".$user->get('id')."'"
			. "\n AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
			$db->SetQuery( $query );
			$lp_grade = $db->LoadObject();
			if (is_object($lp_grade)) {
				global $JLMS_CONFIG;
				$course_params = $JLMS_CONFIG->get('course_params');
				$params = new JLMSParameters($course_params);

				if ($params->get('track_type', 0) == 1) { // by the best attempt
					if ($user_status > $lp_grade->user_status) {
						$do_update_grades = true;
					} elseif($user_status == $lp_grade->user_status && $lp_grade->end_time != '0000-00-00 00:00:00' && $ret_ar['end_time'] != '0000-00-00 00:00:00') {
						$f_time = strtotime($lp_grade->end_time) - strtotime($lp_grade->start_time);
						$s_time = strtotime($ret_ar['end_time']) - strtotime($ret_ar['start_time']);
						if ($s_time < $f_time) {
							$do_update_grades = true;
						}
					}
				} else {
					$do_update_grades = true;
				}

			} else {
				$do_update_grades = true;
			}
			if ($do_update_grades) {
				$query = "DELETE FROM #__lms_learn_path_grades WHERE user_id = '".$user->get('id')."'"
				. "\n AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
				$db->SetQuery( $query );
				$db->query();
				$query = "INSERT INTO #__lms_learn_path_grades (course_id, lpath_id, user_id, user_points, user_time, user_status, start_time, end_time)"
				. "\n VALUES ('".$course_id."', '".$lpath_id."', '".$user->get('id')."', '".$user_points."', '".$spent_time."', '".$user_status."', '".$ret_ar['start_time']."', '".$ret_ar['end_time']."')";
				$db->SetQuery( $query );
				$db->query();
			}
		}
	}

	/* [BEGIN] auto course completion */
	
	$SCC = new JLMS_SCORMCourseComplete();
	$SCC->getInit($user->get('id'), $course_id);

	/*
	$query = "SELECT * FROM #__lms_gradebook_lpaths WHERE course_id = '".$course_id."'";
	$db->SetQuery( $query );
	$all_learn = $db->LoadObjectList();

	$do_set_course_completion = true;
	if (count($all_learn)) {
		for($i=0;$i<count($all_learn);$i++) {
			//TODO: replace queries in a loop with one big query
			$query = "SELECT user_status FROM #__lms_learn_path_grades WHERE course_id = '".$course_id."' AND lpath_id = '".$all_learn[$i]->learn_path_id."' AND user_id = '".$user->get('id')."'";
			$db->SetQuery( $query );
			$status_learn = $db->LoadResult();		
			if(!$status_learn) {
				$do_set_course_completion = false;
				break;
			}
		}
	} else {
		$do_set_course_completion = false;
	}

	if($do_set_course_completion) {
		$query = "SELECT id FROM #__lms_certificate_users WHERE course_id = $course_id AND user_id = ".$user->get('id')." AND crt_option = 1";
		$db->SetQuery( $query );
		if ($db->LoadResult()) {
			//course is already completed by the user
		} else {
			$query = "INSERT INTO #__lms_certificate_users (course_id, user_id, crt_option, crt_date)"
			. "\n VALUES ('".$course_id."', '".$user->get('id')."', '1', '".gmdate('Y-m-d H:i:s')."')";
			$db->SetQuery( $query );
			$db->query();

			$e_course = new stdClass();
			$e_course->course_alias = '';
			$e_course->course_name = '';			

			$query = "SELECT course_name, name_alias FROM #__lms_courses WHERE id = '".$course_id."'";
			$db->setQuery( $query );
			$e_course = $db->loadObject();

			$e_user = new stdClass();
			$e_user->name = '';
			$e_user->email = '';
			$e_user->username = '';

			$query = "SELECT email, name, username FROM #__users WHERE id = '".$user->get('id')."'";
			$db->setQuery( $query );
			$e_user = $db->loadObject();

			$e_params['user_id'] = $user->get('id');
			$e_params['course_id'] = $course_id;					
			$e_params['markers']['{email}'] = $e_user->email;	
			$e_params['markers']['{name}'] = $e_user->name;										
			$e_params['markers']['{username}'] = $e_user->username;
			$e_params['markers']['{coursename}'] = $e_course->course_name;//( $e_course->course_alias )?$e_course->course_alias:$e_course->course_name;

			$e_params['markers']['{courselink}'] = JLMSEmailRoute("index.php?option=com_joomla_lms&Itemid=$Itemid&task=details_course&id=$course_id");

			$e_params['markers_nohtml']['{courselink}'] = $e_params['markers']['{courselink}'];
			$e_params['markers']['{courselink}'] = '<a href="'.$e_params['markers']['{courselink}'].'">'.$e_params['markers']['{courselink}'].'</a>';
									
			$e_params['markers']['{lmslink}'] = JLMSEmailRoute("index.php?option=com_joomla_lms&Itemid=$Itemid");

			$e_params['markers_nohtml']['{lmslink}'] = $e_params['markers']['{lmslink}'];					
			$e_params['markers']['{lmslink}'] = '<a href="'.$e_params['markers']['{lmslink}'].'">'.$e_params['markers']['{lmslink}'].'</a>';

			$e_params['action_name'] = 'OnUserCompletesTheCourse';

			$_JLMS_PLUGINS->loadBotGroup('emails');
			$plugin_result_array = $_JLMS_PLUGINS->trigger('OnUserCompletesTheCourse', array (& $e_params));							
		}
	}
	*/
	/* [END] auto course completion */
	
	return $ret_ar;
}

function JLMS_get_RestartMenu() {
	global $JLMS_CONFIG;
	$toolbar = array();
	$toolbar[] = array('btn_type' => 'restart', 'btn_js' => "javascript:ajax_action('lpath_restart');");
	$tool_txt = JLMS_ShowToolbar($toolbar);
	$tool_txt = str_replace('"components/com_joomla_lms','"'.$JLMS_CONFIG->getCfg('live_site').'/components/com_joomla_lms',$tool_txt);
	return $tool_txt;
}

function JLMS_outputXML($rstr){
	$iso = explode( '=', _ISO );
	echo "\n"."some notices :)";
	$debug_str = ob_get_contents();
	while( @ob_end_clean() );
	header ('Expires: Thu, 12 May 1983 11:00:00 GMT');
	header ('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
	header ('Cache-Control: no-cache, must-revalidate');
	header ('Pragma: no-cache');

	if (class_exists('JFactory')) {
		$document=JFactory::getDocument();
		$charset_xml = $document->getCharset();
		header ('Content-Type: text/xml; charset='.$charset_xml);
	} else {
		header ('Content-Type: text/xml');
	}
	echo '<?xml version="1.0" encoding="'.$iso[1].'" standalone="yes"?>';
	echo '<response>' . "\n";
	if (!empty($rstr)) {
		foreach ($rstr as $rkey => $rval) {

		if($rkey == 'result_info') {
				echo "\t" . $rval->value . "\n";
			}
			else {

			$rvalue = ' ';
			if ($rval->value || $rval->value === 0 || $rval->value === '0') {
				$rvalue = $rval->value;
			}
			//08Aug2008 - fix by DEN. In J1.5 WYSIWYG editors inserted ' border="0" ' with errors. Maybe not an editor - byt MySql codepage issue.
			$rvalue = preg_replace("/border=\"([\D^\"]+)\"/", 'border="0" ', $rvalue);
			if (isset($rval->is_cdata) && $rval->is_cdata) {
				echo "\t" . '<'.$rkey.'><![CDATA['.$rvalue.']]></'.$rkey.'>' . "\n";
			} else {
				echo "\t" . '<'.$rkey.'>'.$rvalue.'</'.$rkey.'>' . "\n";
			}
		}
		}
	} else {
		echo "\t" . '<task>failed</task>' . "\n";
		echo "\t" . '<info>boom</info>' . "\n";
		//echo "\t" . '<menu_contents><![CDATA['.JLMS_get_RestartMenu().']]></menu_contents>'."\n";
	}
	echo "\t" . '<debug><![CDATA['.$debug_str.']]></debug>' . "\n";
	echo '</response>' . "\n";
	die;
}

function JLMS_prepareLPath_Elem_DATA($elem, $user_unique_id, $user_start_id, $status=0) {
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG, $option;
	$add_descr = '';
	$step_script = '';
	$step_type = $elem->step_type;
	$is_change_descrs = false;
	$is_show_step_descr = true;
	$end_quiz = 0;
	
	$result_flag = 0;
	$result_info = '';
	
	$quiz_params = new stdClass();
	
	switch ($elem->step_type) {
		case 2:
			$is_if = 1;
			$query = "SELECT * FROM #__lms_documents WHERE id = '".$elem->item_id."' AND course_id = '".$elem->course_id."'";// AND published = 1";
			$JLMS_DB->SetQuery( $query );
			$doc_data = $JLMS_DB->LoadObjectList();
			if (count($doc_data) == 1) {
				if($doc_data[0]->folder_flag == 3){
					$query = "SELECT a.*, b.file_name, c.name, c.username FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0 LEFT JOIN #__users as c ON a.owner_id = c.id WHERE a.id=".$doc_data[0]->file_id;
					$JLMS_DB->SetQuery( $query );
					$out_row = $JLMS_DB->LoadObjectList();
					
					if(count($out_row) && isset($out_row[0]->allow_link) && $out_row[0]->allow_link){
						$doc_data[0]->doc_name = $out_row[0]->doc_name;
						$doc_data[0]->doc_description = $out_row[0]->doc_description;
						$doc_data[0]->file_id = $out_row[0]->file_id;
						
					}else{
						$doc_data[0]->doc_name = _JLMS_LP_RESOURSE_ISUNAV;
						$doc_data[0]->doc_description = '';
						$is_if = 0;
						$is_show_step_descr = false;
						$add_descr = '<div class="'.$JLMS_CONFIG->get('system_message_css_class', 'joomlalms_sys_message').'">'._JLMS_LPATH_RESOURCE_UNAVAILABLE.'<div>';
					}
				}
				if($is_if)
				if ( (!$doc_data[0]->folder_flag || $doc_data[0]->folder_flag == 3) && $doc_data[0]->file_id) {
					$do_tracking = false;
					$md_params = array();
					$md_params['disable_activate_content'] = true;
					$md_params['enable_suto_start'] = true;
					require_once(_JOOMLMS_FRONT_HOME . "/includes/jlms_docs_process.php");
					//$user_unique_id = strval(mosGetParam($_REQUEST, 'user_unique_id', ''));
					//$user_start_id = intval(mosGetParam($_REQUEST, 'user_start_id', 0));
					$md_params['doc_get_link'] = $JLMS_CONFIG->getCfg('live_site')."/index.php?tmpl=component&no_html=1&option=com_joomla_lms&Itemid=".$Itemid."&task=show_lpath&action=get_lpath_doc&user_unique_id=".$user_unique_id."&user_start_id=".$user_start_id."&id=".$elem->lpath_id."&course_id=".$elem->course_id."&doc_id=".$doc_data[0]->id."&step_id=".$elem->id."&force=force";
					$md_params['doc_get_link_url_enc'] = $JLMS_CONFIG->getCfg('live_site')."/index.php%3Ftmpl%26component%3Dno_html%3D1%26option%3Dcom_joomla_lms%26Itemid%3D".$Itemid."%26task%3Dshow_lpath%26action%3Dget_lpath_doc%26user_unique_id%3D".$user_unique_id."%26user_start_id%3D".$user_start_id."%26id%3D".$elem->lpath_id."%26course_id%3D".$elem->course_id."%26doc_id%3D".$doc_data[0]->id."%26step_id%3D".$elem->id."%26force%3Dforce";
					$add_descr = JLMS_showMediaDocument($doc_data[0]->file_id, $doc_data[0]->id, $doc_data[0]->doc_name, $do_tracking, $md_params );
					
					if ($add_descr) {
						if ($do_tracking) {
							$fake_element = new stdClass();
							JLMS_Track_MarkStepStatus($user_start_id, $elem->course_id, $elem->lpath_id, $elem->id, $fake_element, 3);
						}
					} else {
						$add_descr = "<center>";
						if ($JLMS_CONFIG->get('process_without_js', false)) {
							$link = $JLMS_CONFIG->getCfg('live_site')."/index.php?tmpl=component&no_html=1&amp;option=com_joomla_lms&amp;Itemid=".$Itemid."&amp;task=show_lpath&amp;action=get_lpath_doc&amp;user_unique_id=".$user_unique_id."&amp;user_start_id=".$user_start_id."&amp;id=".$elem->lpath_id."&amp;course_id=".$elem->course_id."&amp;doc_id=".$elem->item_id."&amp;step_id=".$elem->id;
							$add_descr .= "<a href=\"$link\"";
						} else {
							$add_descr .= "<a href=\"javascript:get_doc_id = ".$elem->item_id.";ajax_action('get_document');\"";
						}
						$add_descr .= " title='".$elem->step_name."'>".$elem->step_name."</a></center><br />";
					}
				} elseif ( ($doc_data[0]->folder_flag == 2) && $doc_data[0]->file_id) {
					$query = "SELECT * FROM #__lms_documents_zip WHERE id = '".$doc_data[0]->file_id."' AND course_id = '".$elem->course_id."'";
					$JLMS_DB->SetQuery( $query );
					$row_zip = $JLMS_DB->LoadObject();
					if ( is_object($row_zip) ) {
						if ($row_zip->startup_file) {
							$add_descr = '<iframe id="zip_contents" name="zip_contents" height="600px" width="100%" frameborder="0" scrolling="auto" src="'.$JLMS_CONFIG->getCfg('live_site') . "/" . _JOOMLMS_SCORM_PLAYER . "/".$row_zip->zip_folder."/".$row_zip->startup_file.'">'._JLMS_IFRAMES_REQUIRES.'</iframe>';
							$is_change_descrs = true;
						}
					}
				} elseif ((!$doc_data[0]->folder_flag || $doc_data[0]->folder_flag == 3) && !$doc_data[0]->file_id) {
					$elem->step_description = $doc_data[0]->doc_description;
				}
				//index.php?tmpl=component&option=com_joomla_lms&task=get_document&course_id=".$elem->course_id."&id=".$elem->item_id."' title='".$elem->step_name."'>".$elem->step_name."</a><center><br />";
			}
		break;
		case 3:			
			$query = "SELECT * FROM #__lms_links WHERE id = '".$elem->item_id."' AND course_id = '".$elem->course_id."'";// AND published = 1";
			$JLMS_DB->SetQuery( $query );
			$link_data = $JLMS_DB->LoadObject();
			if ( is_object( $link_data ) ) {
				$link_href = $link_data->link_href;
				$link_name = $link_data->link_name;
				//TODO: review
				/*
				$_JLMS_PLUGINS->loadBotGroup('content');
				$plugin_result_array = $_JLMS_PLUGINS->trigger('onContentProcess', array(&$link_href));
				$plugin_result_array = $_JLMS_PLUGINS->trigger('onContentProcess', array(&$link_name));
				*/
				if ($link_data->link_type == 2) {					
					if ($link_data->params) {
						$params = new JLMSParameters($link_data->params);
					} else {
						$params = new JLMSParameters('display_width=720'."\n".'display_height=540');
					}
					$add_descr = '<iframe id="jlms_inline_link" name="jlms_inline_link" height="'.$params->get('display_height', 540).'px" width="100%" frameborder="0" scrolling="auto" src="'.$link_href.'">'._JLMS_IFRAMES_REQUIRES.'</iframe>';
				} elseif ($link_data->link_type == 3) {					
					if ($link_data->params) {
						$tmp_params = new JLMSParameters($link_data->params);
					} else {
						$tmp_params = new JLMSParameters('display_width=0'."\n".'display_height=0');
					}
					$x_size = 0;
					$y_size = 0;
					if ( is_object($tmp_params) && method_exists($tmp_params, 'get') && $tmp_params->get('display_width') ) {
						$x_size = $tmp_params->get('display_width');
					}
					if ( is_object($tmp_params) && method_exists($tmp_params, 'get') && $tmp_params->get('display_height') ) {
						$y_size = $tmp_params->get('display_height');
					}
					$add_descr = '<center><a id="jlms_modal_link'.$link_data->id.'" class="jlms_modal" rel="{handler:\'iframe\', size:{x:'.$x_size.',y:'.$y_size.'}}" href="'.$link_href.'" title="'.str_replace('"','&quot;',$link_name).'">'.$link_name.'</a></center><br />';
					$step_script = JLMS_SqueezeBox_getDomreadyFire(false);
					$is_show_step_descr = false;
				} else {
					$add_descr = "<center><a target='_blank' href='".$link_href."' title='".$link_name."'>".$link_name."</a></center><br />";
				}
			}
		break;
		case 6:		
			global $option, $Itemid;
			$failed = false;
			$lpath_id_scorm = intval($elem->item_id);
			$query = "SELECT * FROM #__lms_learn_paths WHERE id = $lpath_id_scorm AND course_id = ".$elem->course_id." AND item_id <> 0";//AND lp_type = 1";
			$JLMS_DB->SetQuery( $query );
			$scorm_info_data = $JLMS_DB->LoadObject();
			if (is_object($scorm_info_data)) {
				$id_scorm = $scorm_info_data->item_id;
				$scorm = null;
				$scoid = 0;
				require_once(_JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.class.php");
				require_once(_JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.lib.php");
				$usertype = $JLMS_CONFIG->get('current_usertype', 0);
				if (JLMS_CheckSCORM($id_scorm, $lpath_id_scorm, $elem->course_id, $scorm, $usertype)) {
					
					if (!empty($scorm)) {
						$step_params = new JLMSParameters($elem->params);
						
						$scorm_layout_type = 0;
						$scparams = new JLMSParameters($scorm->params);
						if ( is_object($scparams) && method_exists($scparams, 'get') && $scparams->get('scorm_layout') == 1 ) {
							$scorm_layout_type = 1;
						}
						if ($scorm_layout_type == 1) {
							$int_skip_resume = 0;
							if (isset($elem->step_accessed_first_time) && $elem->step_accessed_first_time) {
								$int_skip_resume = 1;
							}
							$scorm_box_link = sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=player_scorm&id=".$id_scorm."&course_id=".$elem->course_id."&lpath_id=$lpath_id_scorm&int_skip_resume=$int_skip_resume&step_id=".$elem->id);
							$x_size = $scorm->width > 100 ? $scorm->width : 0;
							$y_size = $scorm->height > 100 ? $scorm->height : 0;
							$add_descr = '';
							if($step_params->get('description_position', 0) == 1 && strlen($elem->step_description)){
								$add_descr .= $elem->step_description;
							}
							$add_descr .= '<center>';
							$add_descr .= '<a id="jlms_modal_scorm'.$id_scorm.'" class="scorm_modal" rel="{handler:\'iframe\', size:{x:'.$x_size.',y:'.$y_size.'}}" href="'.$scorm_box_link.'" title="'.str_replace('"','&quot;',$elem->step_name).'">'.$elem->step_name.'</a>';
							$add_descr .= '</center>';
							if($step_params->get('description_position', 0) == 2 && strlen($elem->step_description)){
								$add_descr .= $elem->step_description;
							}
							$add_descr .= '<br />';
							$step_script = JLMS_SqueezeBox_getDomreadyFire(true);
							//$step_script = 'var showscormsquezzeebox = true;';
							$is_show_step_descr = false;
						} else {
							require_once( _JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.play.php");
							$skip_resume = false;
							if (isset($elem->step_accessed_first_time) && $elem->step_accessed_first_time) {
								$skip_resume = true;
							}
							$ret = JLMS_SCORM_PLAY_MAIN( $scorm, $option, $elem->course_id, $lpath_id_scorm, $scoid, 'normal', '', 'on', true, false, false, $skip_resume );
							$is_show_step_descr = false;
							$add_descr = '';
							if($step_params->get('description_position', 0) == 1 && strlen($elem->step_description)){
								$add_descr .= $elem->step_description;
							}
							$add_descr .= $ret['response'];
							if($step_params->get('description_position', 0) == 2 && strlen($elem->step_description)){
								$add_descr .= $elem->step_description;
							}
							$step_script = $ret['script'];
						}
					} else {
						$failed = true;
					}
				} else {
					$failed = true;
				}
			} else {
				$failed = true;
			}
			if ($failed) {
				$add_descr = '<div class="'.$JLMS_CONFIG->get('system_message_css_class', 'joomlalms_sys_message').'">'._JLMS_LPATH_RESOURCE_UNAVAILABLE.'<div>';
			}
		break;
		case 5:
			//----------------------resume quiz------------------------------- 
			$query = "SELECT count(c_id) FROM #__lms_quiz_r_student_quiz WHERE c_quiz_id = ".intval($elem->item_id)." AND c_student_id = '".$my->id."' ORDER BY c_date_time desc LIMIT 1";
			$JLMS_DB->SetQuery($query);
			$count = $JLMS_DB->LoadResult();			
			$quiz_params->attempts_of_this_quiz = $count;
			
			$query = "SELECT c_max_numb_attempts FROM #__lms_quiz_t_quiz WHERE c_id = ".intval($elem->item_id)."";
			$JLMS_DB->SetQuery($query);
			$quiz_params->c_max_numb_attempts = $JLMS_DB->LoadResult();
			
			$query = "SELECT c_id,unique_id FROM #__lms_quiz_r_student_quiz WHERE c_quiz_id = ".intval($elem->item_id)." AND c_student_id = '".$my->id."' ORDER BY c_date_time desc LIMIT 1";
			$JLMS_DB->SetQuery($query);
			$quiz_info = $JLMS_DB->LoadObject();

			if($quiz_params->c_max_numb_attempts > 0 && ($quiz_params->attempts_of_this_quiz >= $quiz_params->c_max_numb_attempts)) {
				
				$result_flag = 1;
				$lpath_id = intval( mosGetParam( $_REQUEST, 'id', 0 ) );
				$step_id = $elem->id;
				$result_id = isset($quiz_info->c_id)?($quiz_info->c_id):'';//intval( mosGetParam( $_REQUEST, 'user_start_id', 0 ) );
				$result_uniq = isset($quiz_info->unique_id)?($quiz_info->unique_id):'';
				$course_id = intval( mosGetParam( $_REQUEST, 'course_id', ($elem->course_id ? $elem->course_id : 0) ) );
				
				//TODO: ! check validity of these vars
				if ($lpath_id && $result_id && $result_uniq) {
					$query = "SELECT id FROM #__lms_learn_path_results WHERE user_id = '".$my->id."'"
					. "\n AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
					$JLMS_DB->SetQuery( $query );
					$res_id = $JLMS_DB->LoadResult();
					
					$query = "DELETE FROM #__lms_learn_path_step_quiz_results"
					. "\n WHERE result_id = $res_id AND step_id = $step_id";// AND stu_quiz_id = ".$this->stu_quiz_id;
					$JLMS_DB->SetQuery($query);
					$JLMS_DB->query();
	
					$query = "INSERT INTO #__lms_learn_path_step_quiz_results (result_id, step_id, stu_quiz_id, start_id, unique_id)"
					. "\n VALUES('".$res_id."', '".$step_id."', '".$quiz_info->c_id."', '".$result_id."', '".$result_uniq."')";
					$JLMS_DB->SetQuery($query);
					$JLMS_DB->query();
				}
				
				require_once(dirname(__FILE__) .'/includes/quiz/ajax_quiz.class.php');
					
				$QA = new JLMS_quiz_API(intval($elem->item_id), 1);
				if (!$QA->quiz_valid()) {
					return '';
				}
				$q_data = $QA->quiz_Get_QuestionList();	
				$result_info = JQ_GetPanelData_LP_(intval($elem->item_id), $course_id, $q_data, $elem->id, $quiz_info->c_id);
			}
			//--------------------------------
			
			$is_show_step_descr = false;
			
			$query = "SELECT b.stu_quiz_id FROM #__lms_learn_path_results as a, #__lms_learn_path_step_quiz_results as b WHERE a.user_id = '".$my->id."'"
			. "\n AND a.course_id = '".$elem->course_id."' AND a.lpath_id = '".$elem->lpath_id."' AND b.result_id = a.id AND b.step_id = ".$elem->id;
			$JLMS_DB->SetQuery( $query );
			$stu_quiz_id = $JLMS_DB->LoadResult();
			if (!$stu_quiz_id) { $stu_quiz_id = 0; }
			$query = "SELECT c_passed, c_total_time, c_total_score, unique_id, c_id as start_id FROM #__lms_quiz_r_student_quiz WHERE c_id = $stu_quiz_id";
			$JLMS_DB->SetQuery( $query );
			$stu_quiz_data = $JLMS_DB->LoadObject();


			/********************************/
			$resume_quiz = NULL;
			if ($stu_quiz_id) {
				$query = "SELECT c_id, unique_id, c_quiz_id FROM #__lms_quiz_r_student_quiz WHERE c_id = $stu_quiz_id AND c_quiz_id = ".$elem->item_id." AND c_student_id = ".$my->id." and c_passed = 0 and c_total_time = 0 ORDER BY c_id desc LIMIT 1";
				$JLMS_DB->SetQuery($query);
				$resume_quiz = $JLMS_DB->LoadObject();
			}
			if(isset($resume_quiz->c_id)) {
				$query = "SELECT c_question_id FROM #__lms_quiz_r_student_question WHERE c_stu_quiz_id = ".$resume_quiz->c_id." ORDER BY c_id desc LIMIT 1";
				$JLMS_DB->SetQuery($query);
				$last_question = $JLMS_DB->LoadResult();

				if (!$last_question) {
					$last_question = -1;
				}

				$quiz_params->resume_quiz = $resume_quiz->c_id;
				$quiz_params->unique_id = $resume_quiz->unique_id;
				$quiz_params->last_question = $last_question;
			}			

			//-----------
			$do_resume = isset($quiz_params->resume_quiz) && $quiz_params->resume_quiz && $quiz_params->last_question;
			/********************************/

			if ((!empty($stu_quiz_data) && isset($stu_quiz_data->c_passed) && $stu_quiz_data->c_passed) || (($quiz_params->c_max_numb_attempts > 0 && $quiz_params->attempts_of_this_quiz >= $quiz_params->c_max_numb_attempts && !$do_resume)) ) {
				$user_passed = 0;
				if(isset($stu_quiz_data->c_passed) && $stu_quiz_data->c_passed) {
					$user_passed = 1;
				}
				$user_score = isset($stu_quiz_data->c_total_score) ? $stu_quiz_data->c_total_score : 0;
				$user_time = isset($stu_quiz_data->c_total_time) ? $stu_quiz_data->c_total_time : 0;
				$query = "SELECT SUM(c_point) FROM #__lms_quiz_t_question WHERE published = 1 AND c_quiz_id = ".intval($elem->item_id);
				$JLMS_DB->setQuery($query);
				$max_score = $JLMS_DB->LoadResult();
					
					//Max 27.03.08 Coreckcia kolichestva ochkov
					$query = "SELECT a.* FROM #__lms_quiz_t_question as a, #__lms_quiz_r_student_quiz_pool as b"
					. "\n WHERE b.start_id = $stu_quiz_id AND b.quest_id = a.c_id AND a.course_id = ".$elem->course_id." ORDER BY b.ordering";// (a.c_quiz_id = ".$this->quiz_id." OR (a.c_quiz_id = 0 AND )) ORDER BY b.ordering";
					$JLMS_DB->SetQuery($query);
					$question_list = $JLMS_DB->LoadObjectList();
					
					if(isset($question_list) && !count($question_list)){
						$query = "SELECT a.* FROM #__lms_quiz_t_question as a, #__lms_quiz_r_student_question as b"
						. "\n WHERE 1"
						. "\n AND b.c_stu_quiz_id = $stu_quiz_id"
						. "\n AND b.c_question_id = a.c_id"
						//. "\n AND a.course_id = ".$elem->course_id.""
						. "\n ORDER BY a.ordering"
						;
						// (a.c_quiz_id = ".$this->quiz_id." OR (a.c_quiz_id = 0 AND )) ORDER BY b.ordering";
						$JLMS_DB->SetQuery($query);
						$question_list = $JLMS_DB->LoadObjectList();
						
					}
					
					$q_from_pool = array();
					$q_from_pool_gqp = array();
					foreach ($question_list as $row) {
						if ($row->c_type == 20) {
							$q_from_pool[] = $row->c_pool;
						}
						if ($row->c_type == 21) {
							$q_from_pool_gqp[] = $row->c_pool_gqp;
						}
					}
					if (count($q_from_pool) || count($q_from_pool_gqp)) {
						if ($q_from_pool) {
							$qp_ids = implode(',',$q_from_pool);
							$query = "SELECT a.* FROM #__lms_quiz_t_question as a"
							. "\n WHERE a.course_id = ".$elem->course_id." AND a.c_id IN ($qp_ids)";
							$JLMS_DB->setQuery( $query );
							$rows2 = $JLMS_DB->loadObjectList();
						}
						if ($q_from_pool_gqp) {
							$gqp_ids = implode(',',$q_from_pool_gqp);
							$query = "SELECT a.* FROM #__lms_quiz_t_question as a"
							. "\n WHERE a.course_id = 0 AND a.c_id IN ($gqp_ids)"
							//. "\n AND a.published = '1'"
							;
							$JLMS_DB->setQuery( $query );
							$rows3 = $JLMS_DB->loadObjectList();
						}
						for ($i=0, $n=count( $question_list ); $i < $n; $i++) {
							if ($question_list[$i]->c_type == 20) {
								for ($j=0, $m=count( $rows2 ); $j < $m; $j++) {
									if ($question_list[$i]->c_pool == $rows2[$j]->c_id) {
										$question_list[$i]->c_question = $rows2[$j]->c_question;
										$question_list[$i]->c_point = $rows2[$j]->c_point;
										$question_list[$i]->c_attempts = $rows2[$j]->c_attempts;
										$question_list[$i]->c_type = $rows2[$j]->c_type;
										// added 18.05.2007 - params of Pool question. (get 'Disable feedback' option of pool question).
										$question_list[$i]->params = $rows2[$j]->params;
										break;
									}
								}
							}
							if ($question_list[$i]->c_type == 21) {
								for ($j=0, $m=count( $rows3 ); $j < $m; $j++) {
									if ($question_list[$i]->c_pool_gqp == $rows3[$j]->c_id) {
										$question_list[$i]->c_question = $rows3[$j]->c_question;
										$question_list[$i]->c_point = $rows3[$j]->c_point;
										$question_list[$i]->c_attempts = $rows3[$j]->c_attempts;
										$question_list[$i]->c_type = $rows3[$j]->c_type;
										$question_list[$i]->params = $rows3[$j]->params;
										break;
									}
								}
							}
						}
					}
					$max_score = 0;
					foreach ($question_list as $tcl) {
						$max_score = $max_score + intval($tcl->c_point);
					}
					//end
					
				$query = "SELECT * FROM #__lms_quiz_t_quiz WHERE c_id = '".$elem->item_id."' AND course_id = '".$elem->course_id."'";
				$JLMS_DB->SetQuery( $query );
				$quiz = $JLMS_DB->LoadObject();
				$nugno_score = ($quiz->c_passing_score * $max_score) / 100;
				require_once(dirname(__FILE__) . '/includes/quiz/templates/joomlaquiz_lms_template/jq_template.php');
				global $JLMS_LANGUAGE;
				JLMS_require_lang($JLMS_LANGUAGE, 'quiz.lang', $JLMS_CONFIG->get('default_language'));
				require(_JOOMLMS_FRONT_HOME . '/includes/quiz/quiz_language.php');
				global $jq_language;
				if ($user_passed) {
					if ($quiz->c_pass_message) {
						$jq_language['quiz_user_passes'] = nl2br($quiz->c_pass_message);
					}
				} else {
					if ($quiz->c_unpass_message) {
						$jq_language['quiz_user_fails'] = nl2br($quiz->c_unpass_message);
					}
				}
				$eee = $jq_language['quiz_header_fin_message'];
				
				$t_ar = array();
				$t_ar[] = mosHTML::makeOption($user_score." of ".$max_score, $jq_language['quiz_res_mes_score']);
				$t_ar[] = mosHTML::makeOption(($nugno_score?($nugno_score." (".$quiz->c_passing_score."%)"):''), $jq_language['quiz_res_mes_pas_score']);
				$tot_min = floor($user_time / 60);
				$tot_sec = $user_time - $tot_min*60;
				$tot_time = str_pad($tot_min, 2, "0", STR_PAD_LEFT).":".str_pad($tot_sec, 2, "0", STR_PAD_LEFT);
				$t_ar[] = mosHTML::makeOption($tot_time, $jq_language['quiz_res_mes_time']);
				
				$quiz_params = new JLMSParameters($quiz->params);
				
				if($quiz_params->get('sh_final_page_text', 1) == 1){
					$results_txt = JoomlaQuiz_template_class::JQ_show_results($jq_language['quiz_header_fin_results'], $t_ar);
				} else {
					$results_txt = '';	
				}
				if($quiz_params->get('sh_final_page_fdbck', 1) == 1){
					$ret_str = JoomlaQuiz_template_class::JQ_show_results_msg($eee, ($user_passed?$jq_language['quiz_user_passes']:$jq_language['quiz_user_fails']), $user_passed);
					$ret_str .= '<br />';
				} else {
					$ret_str .= '<br />';
				}

				$footer_ar = array();
				$footer_ar[] = mosHTML::makeOption(0,$jq_language['quiz_fin_btn_review']);
				$footer_ar[] = mosHTML::makeOption(1,$jq_language['quiz_fin_btn_print']);
				$footer_ar[] = mosHTML::makeOption(2,$jq_language['quiz_fin_btn_certificate']);
				$footer_ar[] = mosHTML::makeOption(3,$jq_language['quiz_fin_btn_email']);
				
				$query = "SELECT c_quiz_id, unique_id FROM #__lms_quiz_r_student_quiz WHERE c_id = '".$stu_quiz_id."'";
				$JLMS_DB->setQuery($query);
				$quiz_unique = $JLMS_DB->loadObjectList();
				
				$quiz_id = isset($quiz_unique[0]->c_quiz_id) ? $quiz_unique[0]->c_quiz_id : 0;
				$user_unique_id_quiz = isset($quiz_unique[0]->unique_id) ? $quiz_unique[0]->unique_id : '';
				
				$toolbar_footer = array();
				if ($quiz->c_certificate && $user_passed) {
					if($JLMS_CONFIG->get('process_without_js', false)){
						$link_certificate = sefRelToAbs("index.php?option=$option&Itemid=".$Itemid."&no_html=1&task=print_quiz_cert&course_id=".$elem->course_id."&stu_quiz_id=".$stu_quiz_id."&user_unique_id=".$user_unique_id_quiz."");
					} elseif(!$JLMS_CONFIG->get('process_without_js', false)){
						$link_certificate = 'window.open(\''.$JLMS_CONFIG->getCfg('live_site').'/index.php?tmpl=component&option=com_joomla_lms&Itemid='.$Itemid.'&no_html=1&task=print_quiz_cert&course_id='.$elem->course_id.'&stu_quiz_id='.$stu_quiz_id.'&user_unique_id=\'+user_unique_id,\'blank\');';
					}
					$footer_ar[2]->text = "<div class='back_button'><a href='javascript:void(0)' onclick=\"window.open('".$JLMS_CONFIG->getCfg('live_site')."/index.php?tmpl=component&option=com_joomla_lms&Itemid=".$Itemid."&no_html=1&task=print_quiz_cert&course_id=".$elem->course_id."&stu_quiz_id=".$stu_quiz_id."&user_unique_id='+user_unique_id,'blank');\">".$jq_language['quiz_fin_btn_certificate']."</a></div>";
					$toolbar_footer[2] = array('btn_type'=>'certificate_fbar', 'btn_js'=>$link_certificate);
				}
				if ($quiz->c_enable_print) {
					if($JLMS_CONFIG->get('process_without_js', false)){
						$link_print = sefRelToAbs("index.php?option=$option&Itemid=".$Itemid."&no_html=1&task=print_quiz_result&course_id=".$elem->course_id."&stu_quiz_id=".$stu_quiz_id."&user_unique_id=".$user_unique_id_quiz."");
					} elseif(!$JLMS_CONFIG->get('process_without_js', false)){
						$link_print = 'window.open(\''.$JLMS_CONFIG->getCfg('live_site').'/index.php?tmpl=component&option=com_joomla_lms&Itemid='.$Itemid.'&no_html=1&task=print_quiz_result&course_id='.$elem->course_id.'&stu_quiz_id='.$stu_quiz_id.'&user_unique_id=\'+user_unique_id,\'blank\');';
					}
					$footer_ar[1]->text = "<div class='back_button'><a href='javascript:void(0)' onclick=\"window.open('".$JLMS_CONFIG->getCfg('live_site')."/index.php?tmpl=component&option=com_joomla_lms&Itemid=".$Itemid."&no_html=1&task=print_quiz_result&course_id=".$elem->course_id."&stu_quiz_id=".$stu_quiz_id."&user_unique_id='+user_unique_id,'blank');\">".$jq_language['quiz_fin_btn_print']."</a></div>";
					$toolbar_footer[1] = array('btn_type'=>'print_fbar', 'btn_js'=>$link_print);
				}
				if ($quiz->c_email_to) {
					if($JLMS_CONFIG->get('process_without_js', false)){
						$link_email_to = $JLMS_CONFIG->getCfg('live_site')."/index.php?tmpl=component&option=$option&Itemid=$Itemid&task=quiz_action&id=".$elem->course_id."&user_unique_id=$user_unique_id_quiz&atask=email_results&quiz=$quiz_id&stu_quiz_id=$stu_quiz_id";
					} elseif(!$JLMS_CONFIG->get('process_without_js', false)){
						$link_email_to = 'jq_emailResults();';
					}
					$footer_ar[3]->text = "<div class='back_button'><a href='javascript:void(0)' onclick=\"jq_emailResults();\">".$jq_language['quiz_fin_btn_email']."</a></div>";
					$toolbar_footer[3] = array('btn_type'=>'email_to_fbar', 'btn_js'=>$link_email_to);
				}
				if ($quiz->c_enable_review) {
					if($JLMS_CONFIG->get('process_without_js', false)){
						$link_review = sefRelToAbs("index.php?option=$option&Itemid=$Itemid&id=".$elem->course_id."&task=quiz_action&quiz=$quiz_id&stu_quiz_id=$stu_quiz_id&user_unique_id=$user_unique_id_quiz&atask=review_start");
					} elseif(!$JLMS_CONFIG->get('process_without_js', false)){
						$link_review = 'jq_startReview();';
					}
					/*$query = "UPDATE #__lms_quiz_r_student_quiz SET allow_review = 1 WHERE c_id = '".$stu_quiz_id."' and c_quiz_id = '".$quiz_id."' and c_student_id = '".$my->id."'";
					$JLMS_DB->SetQuery( $query );
					$JLMS_DB->query();*/
					$footer_ar[0]->text = "<div class='back_button'><a href='javascript:void(0)' onclick=\"jq_startReview();\">".$jq_language['quiz_fin_btn_review']."</a></div>";
					$toolbar_footer[0] = array('btn_type'=>'review_fbar', 'btn_js'=>$link_review);
				}
				ksort($toolbar_footer);
//				$footer_html = JoomlaQuiz_template_class::JQ_show_results_footer($footer_ar);
				if($JLMS_CONFIG->get('process_without_js', false)){
					$tf_param = true;
				} elseif(!$JLMS_CONFIG->get('process_without_js', false)){
					$tf_param = false;
				}
				if(count($toolbar_footer) > 0){
					$footer_html = JLMS_ShowToolbar($toolbar_footer, $tf_param, 'center');
				} else {
					$footer_html = '';	
				}
				
				$footer_html_graf = '';
				
				if($quiz_params->get('sh_final_page_grafic', 0) == 1)
				{
					////----barss----////
					$is_pool = 0;
					//$showtype_id = intval( mosGetParam( $_POST, 'showtype_id', 0 ) );
					//if(!$quiz_id)
					//$quiz_id = intval(mosGetParam($_POST,'quiz_id',-1));
					if($quiz_id == -1 || $quiz_id == 0) {$is_pool = 1; $quiz_id = 0;}
				
					$query = "SELECT a.*, b.c_qtype as qtype_full, c.c_title as quiz_name, qc.c_category"
					. "\n FROM #__lms_quiz_t_question a LEFT JOIN #__lms_quiz_t_qtypes b ON b.c_id = a.c_type LEFT JOIN #__lms_quiz_t_quiz c ON a.c_quiz_id = c.c_id AND c.course_id = '".$elem->course_id."'"
					. "\n LEFT JOIN #__lms_quiz_t_category as qc ON a.c_qcat = qc.c_id AND qc.course_id = '".$elem->course_id."' AND qc.is_quiz_cat = 0"
					. "\n WHERE a.course_id = '".$elem->course_id."'"
					. "\n AND c_quiz_id = '".$quiz_id."'" 
					. "\n ORDER BY a.ordering, a.c_id"
					;
					$JLMS_DB->setQuery( $query );
					$rows = $JLMS_DB->loadObjectList();
				
					$q_from_pool = array();
					$q_from_pool_gqp = array();
					foreach ($rows as $row) {
						if ($row->c_type == 20) {
							$q_from_pool[] = $row->c_pool;
						}
						if ($row->c_type == 21) {
							$q_from_pool_gqp[] = $row->c_pool_gqp;
						}
					}
					if (count($q_from_pool) || count($q_from_pool_gqp)) {
						$rows2 = array();
						$rows3 = array();
						if ($q_from_pool) {
							$qp_ids =implode(',',$q_from_pool);
							$query = "SELECT a.* FROM #__lms_quiz_t_question as a"
							. "\n WHERE a.course_id = ".$course_id." AND a.c_id IN ($qp_ids)";
							$JLMS_DB->setQuery( $query );
							$rows2 = $JLMS_DB->loadObjectList();
						}
						if ($q_from_pool_gqp) {
							$gqp_ids =implode(',',$q_from_pool_gqp);
							$query = "SELECT a.* FROM #__lms_quiz_t_question as a"
							. "\n WHERE a.course_id = 0 AND a.c_id IN ($gqp_ids)"
							//. "\n AND a.published = '1'"
							;
							$JLMS_DB->setQuery( $query );
							$rows3 = $JLMS_DB->loadObjectList();
						}
						for ($i=0, $n=count( $rows ); $i < $n; $i++) {
							if ($rows[$i]->c_type == 20) {
								for ($j=0, $m=count( $rows2 ); $j < $m; $j++) {
									if ($rows[$i]->c_pool == $rows2[$j]->c_id) {
										$rows[$i]->c_question = $rows2[$j]->c_question;
										$rows[$i]->c_type = $rows2[$j]->c_type;
										break;
									}
								}
							}
							if ($rows[$i]->c_type == 21) {
								for ($j=0, $m=count( $rows3 ); $j < $m; $j++) {
									if ($rows[$i]->c_pool_gqp == $rows3[$j]->c_id) {
										$rows[$i]->c_question = $rows3[$j]->c_question;
										$rows[$i]->c_type = $rows3[$j]->c_type;
										break;
									}
								}
							}
						}
					}
					//var_dump($rows);
					// 18 August 2007 - changes (DEN) - added check for GD and FreeType support
						$generate_images = true;
						$msg = '';
						if (!function_exists('imageftbbox') || !function_exists('imagecreatetruecolor')) {
							$generate_images = false;
							$sec = false;
							if (!function_exists('imagecreatetruecolor')) {
								$msg = 'This function requires GD 2.0.1 or later (2.0.28 or later is recommended).';
								$sec = true;
							}
							if (!function_exists('imageftbbox')) {
								$msg .= ($sec?'<br />':'').'This function is only available if PHP is compiled with freetype support.';
							}
						} // end of GD and FreeType support check
					if ($JLMS_CONFIG->get('temp_folder', '') && $generate_images) { // temp folder setup is ready.
				
				//--------- array of bar-images
					$img_arr = array();
					$title_arr = array();
					$count_graph =array();
					for($i=0,$n=count($rows);$i<$n;$i++){
						$row = $rows[$i];
						if (isset($row->c_pool) && $row->c_pool) {
							$row->c_pool_id = $row->c_pool;
						} else {
							$row->c_pool_id = $row->c_id;
						}
						$quest_params = new JLMSParameters($row->params);
						$z = 1;
						$show_case = true;
	//					if($showtype_id && !$quest_params->get('survey_question'))
						if(false && !$quest_params->get('survey_question')) {
							$show_case = false;
						}
						if($show_case){
							require_once(_JOOMLMS_FRONT_HOME . "/includes/libraries/lms.lib.graph.php");
							$c_question_id = $row->c_pool_id;
							$group_id = 0;
							$str_user_in_groups = '';
							$obj_GraphStat = JLMS_GraphStatistics($option, $course_id, $quiz_id, $i, $z, $row, $c_question_id, $group_id, $str_user_in_groups);
							
							foreach($obj_GraphStat as $key=>$item){
								if(preg_match_all('#([a-z]+)_(\w+)#', $key, $out, PREG_PATTERN_ORDER)){
									if($out[1][0] == 'img'){
										$img_arr[$i]->$out[2][0] = $item;	
									} else 
									if($out[1][0] == 'title'){
										$title_arr[$i]->$out[2][0] = $item;	
									} else 
									if($out[1][0] == 'count'){
										$count_graph[$i]->$out[2][0] = $item;	
									}
								}	
							}
						}
					}
					
					}
					$footer_html_graf = JoomlaQuiz_template_class::JQ_show_results_footer_content_bars($img_arr, $title_arr, $count_graph, $elem->course_id);
				}

				$doc = & JFactory::GetDocument();
				if (method_exists($doc,'addStyleSheet')) {
					$doc->addStyleSheet($JLMS_CONFIG->getCfg('live_site').'/components/com_joomla_lms/includes/quiz/templates/joomlaquiz_lms_template/jq_template.css');
				}
				$mes_span = '<div style="width:100%; text-align:center;"><span style="visibility:hidden; display:none" id="jq_quest_num_container">0</span><span style="visibility:hidden; display:none" id="jq_points_container">0</span><span id="error_messagebox" style="visibility:hidden;">&nbsp;</span></div>
				<div id="jq_quiz_result_reviews" style="text-align:center;font-size:14px;visibility:hidden; display:none; overflow:hidden;"></div>';
				$rc2 = '<div id="jq_quiz_explanation" style="padding:15px 20px 0px 10px; text-align:center; visibility:hidden; display:none; clear:both "></div>';
				$add_descr1 = $mes_span . '<div id="jq_quiz_container">' . $results_txt . $ret_str . $footer_html_graf . $footer_html.'</div>'.$rc2;
				$add_descr = $add_descr1;
				
				$step_script = "var user_unique_id = '".(isset($stu_quiz_data->unique_id) ? $stu_quiz_data->unique_id : '')."'; var stu_quiz_id = '".(isset($stu_quiz_data->start_id) ? $stu_quiz_data->start_id : '')."'; var quiz_blocked = 0; var saved_prev_res_data = jlms_RFE(response,'step_descr');";
				
				$end_quiz = 1;
				
			} else {
				$query = "SELECT * FROM #__lms_quiz_t_quiz WHERE c_id = ".$elem->item_id." AND course_id = ".$elem->course_id;// AND published = 1";
				$JLMS_DB->SetQuery( $query );
				$quiz_data = $JLMS_DB->LoadObject();
				if (is_object($quiz_data)) {
					require_once(_JOOMLMS_FRONT_HOME . '/includes/quiz/templates/joomlaquiz_lms_template/jq_template.php');
					//$quiz_description = JLMS_ShowText_WithFeatures($quiz_data->c_description, true);

					// 30.11.2007 (DEN) - fix. 'addslashes' operation removed. It inserts unnecessary slahses at the quiz page
					//$d_str = addslashes(str_replace( "\n", '<br />',str_replace( "\r", '',$quiz_description)));
					$tmp = JLMS_ShowText_WithFeatures($quiz_data->c_description, true, true);
					
					//$d_str = str_replace( "\n", '',str_replace( "\r", '', $quiz_description));
					$d_str = str_replace( "\n", '',str_replace( "\r", '', $tmp['new_text']));
					
					$step_script = '';
					$step_script .= $tmp['js'];
					
					$progress_bar_js = true;
					
					$add_descr = JoomlaQuiz_template_class::JQ_MainScreen($d_str, '<br />', '', $progress_bar_js);
					
					$progress_bar_js = false;
					
					if($progress_bar_js && $JLMS_CONFIG->get('quiz_progressbar', 0) == 1){
						$step_script .= 'if(document.getElementById(\'progress_bar\') != null){';
						$step_script .= 'var progressbar = new ProgressBar({id:\'progress_bar\',width:'.$JLMS_CONFIG->get('quiz_progressbar_width', 300).',highlight:'.$JLMS_CONFIG->get('quiz_progressbar_highlight', '0').',smooth:'.$JLMS_CONFIG->get('quiz_progressbar_smooth', '1').'});';
						$step_script .= '}';
					}
				} else {
					$add_descr = '<div class="'.$JLMS_CONFIG->get('system_message_css_class', 'joomlalms_sys_message').'">'._JLMS_LPATH_RESOURCE_UNAVAILABLE.'<div>';
				}
			}
		break;
	}
	
	$step_descr = '';
	$elem_descr = '';
	
	if ( $is_show_step_descr ) {
		$ar = JLMS_ShowText_WithFeatures($elem->step_description, true, true);
		$elem_descr = $ar['new_text'];
		$step_script = $ar['js'];
	}
	if ($is_change_descrs && $is_show_step_descr) {
		$step_descr = $elem_descr;
	}
	$step_descr .= $add_descr;
	if (!$is_change_descrs && $is_show_step_descr) {
		$step_descr .= $elem_descr;
	}
	
	//echo "\n";
	//print_r('$status='.$status);
	//echo "\n";
	
	global $Track_Object;
	if($JLMS_CONFIG->get('enable_timetracking', false)){
		$step_script .= "TTracker_".$elem->course_id."_".$my->id."_9_".$elem->lpath_id.".getitemid(".$elem->id.", ".$elem->step_type.");";
		if(in_array($status, array(1,2))){
			if($status == 2){
				$Track_Object->TimeTrackingClear($elem->course_id, $my->id, 9, $elem->lpath_id);
			}
			$Track_Object->TimeTrackingActivate($elem->course_id, $my->id, 9, $elem->lpath_id, $elem->id);
			
			$step_script .= "TTracker_".$elem->course_id."_".$my->id."_9_".$elem->lpath_id.".start();";
		} else 
		if(in_array($status, array(3))){
			//$Track_Object->TimeTrackingActivate($elem->course_id, $my->id, 9, $elem->lpath_id, $elem->id);
			
			$step_script .= "TTracker_".$elem->course_id."_".$my->id."_9_".$elem->lpath_id.".start();";
		}
	}

	$elem_info = array();
	$elem_info['step_descr'] = $step_descr;
	$elem_info['step_script'] = $step_script;
	$elem_info['end_quiz'] = $end_quiz;
	$elem_info['result_flag'] = $result_flag;
	$elem_info['result_info'] = $result_info;
	
	
	
	return $elem_info;
}

function JLMS_prepareLPath_Elem($elem, $user_unique_id, $user_start_id) {

	$elem_info = array();
	$step_type = $elem->step_type;
	$elem_info = JLMS_prepareLPath_Elem_DATA($elem, $user_unique_id, $user_start_id);

	$step_descr = isset($elem_info['step_descr']) ? $elem_info['step_descr'] : '';
	$step_script = isset($elem_info['step_script']) ? $elem_info['step_script'] : '';

	$result_flag = isset($elem_info['result_flag']) ? $elem_info['result_flag'] : ''; 
	
	$ret_str = '<step_id>'.$elem->id.'</step_id>'."\n";
	$ret_str .= '<step_item_id>'.$elem->item_id.'</step_item_id>'."\n";
	$ret_str .= '<step_type><![CDATA['.$step_type.']]></step_type>'."\n";
	$ret_str .= '<step_name><![CDATA['.($elem->step_name?$elem->step_name:' ').']]></step_name>'."\n";
	$ret_str .= '<step_descr><![CDATA['.($step_descr?$step_descr:' ').']]></step_descr>'."\n";
	$ret_str .= '<result_flag>'.$result_flag.'</result_flag>';

	if ($step_script) {
		$ret_str .= '<step_exec_script>1</step_exec_script>'."\n";
	} else {
		$ret_str .= '<step_exec_script>0</step_exec_script>'."\n";
	}
	$ret_str .= '<step_exec_script_contents><![CDATA['.($step_script ? $step_script : ' ').']]></step_exec_script_contents>'."\n";
	return $ret_str;
}
function JLMS_prepareLPath_ElemObj( &$ret_obj, $elem, $user_unique_id, $user_start_id) {
	$status = 0;
	if(isset($ret_obj['task']->value)){
		if($ret_obj['task']->value == 'start'){
			$status = 1;
		} else 
		if($ret_obj['task']->value == 'start_restart'){
			$status = 2;
		} else 
		if($ret_obj['task']->value == 'next_step'){
			$status = 3;
		}
	}
	
	$elem_info = array();
	$step_type = $elem->step_type;
	$elem_info = JLMS_prepareLPath_Elem_DATA($elem, $user_unique_id, $user_start_id, $status);
	
	$step_descr = isset($elem_info['step_descr']) ? $elem_info['step_descr'] : '';
	$step_script = isset($elem_info['step_script']) ? $elem_info['step_script'] : '';
	$end_quiz = isset($elem_info['end_quiz']) ? $elem_info['end_quiz'] : '';
	$result_flag = isset($elem_info['result_flag']) ? $elem_info['result_flag'] : ''; 
	$result_info = isset($elem_info['result_info']) ? $elem_info['result_info'] : ''; 
	
	$ret_str = '<step_id>'.$elem->id.'</step_id>'."\n";
	$ret_str .= '<step_item_id>'.$elem->item_id.'</step_item_id>'."\n";
	$ret_str .= '<step_type><![CDATA['.$step_type.']]></step_type>'."\n";
	$ret_str .= '<step_name><![CDATA['.($elem->step_name?$elem->step_name:' ').']]></step_name>'."\n";
	$ret_str .= '<step_descr><![CDATA['.($step_descr?$step_descr:' ').']]></step_descr>'."\n";
	$ret_str .= '<result_flag>'.$result_flag.'</result_flag>';

	
	/**/$ret_obj['step_id'] = new stdClass(); $ret_obj['step_id']->value = $elem->id;
	/**/$ret_obj['step_item_id'] = new stdClass(); $ret_obj['step_item_id']->value = $elem->item_id;
	/**/$ret_obj['step_type'] = new stdClass(); $ret_obj['step_type']->value = $step_type; $ret_obj['step_type']->is_cdata = true;
	/**/$ret_obj['step_name'] = new stdClass(); $ret_obj['step_name']->value = $elem->step_name; $ret_obj['step_name']->is_cdata = true;
	/**/$ret_obj['step_descr'] = new stdClass(); $ret_obj['step_descr']->value = $step_descr; $ret_obj['step_descr']->is_cdata = true;
	/**/$ret_obj['end_quiz'] = new stdClass(); $ret_obj['end_quiz']->value = $end_quiz;

	$ret_obj['result_flag'] = new stdClass(); $ret_obj['result_flag']->value = $result_flag;
	$ret_obj['result_info'] = new stdClass(); $ret_obj['result_info']->value = $result_info;
	
	if ($step_script) {
		$ret_str .= '<step_exec_script>1</step_exec_script>'."\n";
		/**/$ret_obj['step_exec_script'] = new stdClass(); $ret_obj['step_exec_script']->value = '1';
	} else {
		$ret_str .= '<step_exec_script>0</step_exec_script>'."\n";
		/**/$ret_obj['step_exec_script'] = new stdClass(); $ret_obj['step_exec_script']->value = '0';
	}
	$ret_str .= '<step_exec_script_contents><![CDATA['.($step_script ? $step_script : ' ').']]></step_exec_script_contents>'."\n";

	/**/$ret_obj['step_exec_script_contents'] = new stdClass(); $ret_obj['step_exec_script_contents']->value = $step_script ? $step_script : ' '; $ret_obj['step_exec_script_contents']->is_cdata = true;

	return $ret_str;	
}
function JLMS_Check_LPath_ID_pre($lpath_id, $course_id, $usertype) {
	global $JLMS_DB, $my;
	$ret= new stdClass();
	$ret->course_id = 0;
	$ret->item_id = 0;
	$ret->lp_type = 0;
	$ret->result = false;
	$query_add = " AND published = 1";
	if ($usertype == 1) {
		$query_add = '';
	}
	$query = "SELECT * FROM #__lms_learn_paths WHERE id = '".$lpath_id."' AND course_id = '".$course_id."'".$query_add;
	$JLMS_DB->SetQuery( $query );
	$lp = $JLMS_DB->LoadObject();
	if (is_object($lp)) {
		// 13 August 2007 (DEN) check for global prerequisites
		if ($usertype != 1) {
			$query = "SELECT b.*, a.time_minutes, '' as r_status, '' as r_start, '' as r_end"
				. "\n FROM #__lms_learn_path_prerequisites as a, #__lms_learn_paths as b"
				. "\n WHERE a.lpath_id = $lpath_id  AND a.req_id = b.id AND b.course_id = $course_id";
			$JLMS_DB->SetQuery($query);
			$prereqs = $JLMS_DB->LoadObjectList();
			
			if (!empty($prereqs)) {
				require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_grades.lib.php");
				$user_ids = array();
				$user_ids[] = $my->id;
				JLMS_LP_populate_results($course_id, $prereqs, $user_ids);
				$j = 0;
				$not_available = false;
				while ($j < count($prereqs)) {
					if (!$prereqs[$j]->item_id) {
						if (empty($prereqs[$j]->r_status)) {
							$not_available = true;
							break;
						} else {
							$end_time = strtotime($prereqs[$j]->r_end);
							$current_time = strtotime(JHTML::_('date', null, "Y-m-d H:i:s"));
							if($current_time > $end_time && (($current_time - $end_time) < ($prereqs[$j]->time_minutes*60))){
								$not_available = true;
								break;	
							}
						}
					} else {
						if (empty($prereqs[$j]->s_status)) {
							$not_available = true;
							break;
						} else {
							$end_time = strtotime($prereqs[$j]->r_end);
							$current_time = strtotime(JHTML::_('date', null, "Y-m-d H:i:s"));
							if($current_time > $end_time && (($current_time - $end_time) < ($prereqs[$j]->time_minutes*60))){
								$not_available = true;
								break;	
							}
						}
					}
					$j ++;
				}
				if (!$not_available) {
					$ret->result = true;
					$ret->course_id = $course_id;
					$ret->item_id = $lp->item_id;
					$ret->lp_type = $lp->lp_type;
				}
				return $ret;// !!!!!!!!!!
			}
		}
		$ret->result = true;
		$ret->course_id = $course_id;
		$ret->item_id = $lp->item_id;
		$ret->lp_type = $lp->lp_type;
	}
	return $ret;
}
function JLMS_isValidLPath_ID($lpath_id, $course_id) {
	global $JLMS_DB;
	$query = "SELECT count(*) FROM #__lms_learn_paths WHERE id = '".$lpath_id."' AND course_id = '".$course_id."' AND published = 1";
	$JLMS_DB->SetQuery( $query );
	$r = $JLMS_DB->LoadResult();
	return $r;
}
function JLMS_isValidStartUnique($start_id, $unique_id, $course_id, $lpath_id) {
	global $my, $JLMS_DB;
	$ret_val = false;
	if ($start_id && $unique_id) {
		$query = "SELECT * FROM #__lms_track_learnpath_stats WHERE id = '".$start_id."'";
		$JLMS_DB->SetQuery( $query );
		$st_data = $JLMS_DB->LoadObjectList();
		if (count($st_data) == 1) {
			if ( ($st_data[0]->unique_id == $unique_id) && ($st_data[0]->user_id == $my->id) && ($st_data[0]->course_id == $course_id) && ($st_data[0]->lpath_id == $lpath_id) ) {
				$ret_val = true;
			}
		}
	}
	return $ret_val;
}


function JQ_GetPanelData_LP_($quiz_id, $course_id, $panel_data = array(), $step_id, $stu_quiz_id) {
	global $JLMS_DB, $JLMS_CONFIG;
	$lpath_id = intval( mosGetParam( $_REQUEST, 'id', 0 ) );
	//$step_id = intval( mosGetParam( $_REQUEST, 'step_id', 0 ) );
	$result_id = intval( mosGetParam( $_REQUEST, 'user_start_id', 0 ) );
	$result_uniq = strval( mosGetParam( $_REQUEST, 'lp_user_unique_id', '' ) );
	// return id of this step, of next step and return quiz contents...... (to make 'contentents' at the F.E.)
	$lpath_contents = JLMS_GetLPath_Data($lpath_id, $course_id);
	$panel_str = '';
	
	$tree_modes = array();
	$prev_tds = array();
	$colspan = 0;
	for ($i=0, $n=count($lpath_contents); $i < $n; $i++) {
		$row_path = $lpath_contents[$i];
		$max_tree_width = $row_path->tree_max_width;
		
		if ($row_path->tree_mode_num) {
			$g = 0;
			$tree_modes[$row_path->tree_mode_num - 1] = $row_path->tree_mode;
			while ($g < ($row_path->tree_mode_num - 1)) {
				$pref = '';
				if (isset($tree_modes[$g]) && ($tree_modes[$g] == 2) ) { $pref = 'empty_'; }
				if ($row_path->id == $step_id) {
					$prev_tds[] = "<img src='".$JLMS_CONFIG->getCfg('live_site')."/components/com_joomla_lms/lms_images/treeview/".$pref."line.png' width='16' height='16' alt='line' border='0' />";
				}
				$g ++;
			}
			if ($row_path->id == $step_id) {
				$pref = '';
				if ($row_path->tree_mode == 2) {
					$pref = 'empty_';
				}
				$prev_tds[] = "<img src='".$JLMS_CONFIG->getCfg('live_site')."/components/com_joomla_lms/lms_images/treeview/".$pref."line.png' width='16' height='16' border='0' alt='line' />";
			}
			$max_tree_width = $max_tree_width - $g - 1;
		}
		
		if ($row_path->id == $step_id) {
			$colspan = $max_tree_width + 1;
		}
		
	}

	
	//$panel_str = "\t" . '<quiz_panel_data_gen>' . "\n";
	$panel_str .= "\t" . '<prev_tds_count>' . count($prev_tds) . '</prev_tds_count>' . "\n";
	for ($i = 0, $n = count($prev_tds); $i < $n; $i ++) {
		$panel_str .= "\t" . '<prev_td_'.($i+1).'><![CDATA[' . $prev_tds[$i] . ']]></prev_td_'.($i+1).'>' . "\n";
	}
	$panel_str .= "\t" . '<quest_colspan>' . $colspan . '</quest_colspan>' . "\n";


	if (empty($panel_data)) {
		$query = "SELECT * FROM #__lms_quiz_t_question WHERE c_quiz_id = '".$quiz_id."' ORDER BY ordering, c_id";
		$JLMS_DB->SetQuery( $query );
		$panel_data = $JLMS_DB->LoadObjectList();
	}
	$panel_str .= "\t" . '<quest_count_c_gen>' . count($panel_data) . '</quest_count_c_gen>' . "\n";
	
	//--------------------
	$query = "SELECT a.*, b.c_id as stu_quest, b.c_score as stu_score, b.c_correct FROM #__lms_quiz_r_student_quiz_pool as qp, #__lms_quiz_t_question as a"
	. "\n LEFT JOIN #__lms_quiz_r_student_question as b ON b.c_stu_quiz_id = $stu_quiz_id AND b.c_question_id = a.c_id"
	. "\n WHERE qp.start_id = $stu_quiz_id AND qp.quest_id = a.c_id AND a.course_id = ".mosGetParam($_REQUEST,'course_id',0)." ORDER BY qp.ordering";// (a.c_quiz_id = ".$this->quiz_id." OR (a.c_quiz_id = 0 AND )) ORDER BY b.ordering";
	$JLMS_DB->SetQuery( $query );
	$panel_data = $JLMS_DB->LoadObjectList();
	//--------------------
	//TODO: where is check for question type = 20/21 ??? Is it necessary here? 
	if (!empty($panel_data)) {
		#$panel_str .= "\t" . '<questions>' . "\n";
		$i = 0;
		foreach ($panel_data as $panel_row) {
			
			if(!$panel_row->stu_score) {
				$panel_row->stu_score = 0;
			}
			
			$panel_str .= "\t" . '<question_'.($i+1).'_id>' . $panel_row->c_id . '</question_'.($i+1).'_id>' . "\n";
			$panel_str .= "\t" . '<question_'.($i+1).'_points>' . $panel_row->c_point . '</question_'.($i+1).'_points>' . "\n";
			$panel_str .= "\t" . '<question_'.($i+1).'_text><![CDATA[' . jlms_string_substr(strip_tags($panel_row->c_question),0,50) . ']]></question_'.($i+1).'_text>' . "\n";
			$panel_str .= "\t" . '<question_'.($i+1).'_correct>' . $panel_row->c_correct . '</question_'.($i+1).'_correct>' . "\n";
			$panel_str .= "\t" . '<question_'.($i+1).'_score>' . $panel_row->stu_score . '</question_'.($i+1).'_score>' . "\n";
			$i ++;
		}
		#$panel_str .= "\t" . '</questions>' . "\n";
	}

	//$panel_str .= "\t" . '</quiz_panel_data_gen>' . "\n";
	return $panel_str;	
}
?>