<?php
/**
* joomla_lms.outdocs.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) );
$task 	= mosGetParam( $_REQUEST, 'task', '' );
require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.outdocs.hlpr.php");
require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.outdocs.html.php");
require_once(_JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.lib.php");


	$pathway = array();
	$pathway[] = array('name' => _JLMS_PATHWAY_HOME, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"), 'is_home' => true);
	$pathway[] = array('name' => _JLMS_TOOLBAR_LIBRARY, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=outdocs"), 'is_home' => false);

	JLMSAppendPathWay($pathway);

if ($task != 'save_outdoc' && $task != 'playerSCORMFiles') {
	JLMS_ShowHeading();
	$doc = JFactory::getDocument();
	$doc->setTitle($doc->getTitle().' - '._JLMS_TOOLBAR_LIBRARY);
}

switch ($task) {
################################		DOCUMENTS		##########################
	case 'outdocs':					JLMSO_showDocuments( $option );			break;
	case 'new_outdocs':				JLMSO_editDocument( 0 , $option );		break;
	case 'new_outfolder':			JLMSO_editDocument(0 , $option, 1);		break;
	case 'cancel_scorm':
	case 'cancel_outdoc':			JLMSO_cancelDocument( $option );			break;
	case 'edit_outdoc':			$cid = mosGetParam( $_POST, 'cid', array(0) );
				if (!is_array( $cid )) { $cid = array(0); }
				JLMSO_editDocument( $cid[0], $option );					break;
	case 'save_outdoc':				JLMSO_saveDocument( $option );			break;

	case 'change_outdoc':			JLMSO_changeDoc( $option );				break;
	case 'get_outdoc':				JLMSO_downloadDocument( $id, $option );	break;
	case 'outdoc_delete':			JLMSO_doDeleteDocuments( $option );		break;
	case 'outdoc_orderdown':
	case 'outdoc_orderup':			JLMSO_OrderDocuments( $option );			break;
	//case 'outdocs_view_zip':		JLMS_previewZipPack( $id, $option );	break;
	case 'outdocs_view_content':	JLMSO_previewContent( $id, $option );	break;
	//case 'outdocs_view_save':		JLMS_DocumentsSaveView( $id, $option ); break;
	case 'new_scorm':				JLMSO_editScorm( 0 , $option, 1 );		break;
	case 'edit_scorm':				JLMSO_editScorm( $id , $option );		break;
	case 'save_scorm':				JLMSO_saveScorm( $option );				break;
	case 'playerSCORMFiles':		JLMSO_playerSCORM( $id , $option );		break;
}

function JLMSO_playerSCORM( $id, $option ) {
	global $JLMS_DB, $JLMS_CONFIG, $Itemid, $JLMS_SESSION;

	$course_id = $JLMS_CONFIG->get('course_id');
	$usertype = $JLMS_CONFIG->get('current_usertype', 0);
	$lpath_id = intval(mosGetParam($_REQUEST, 'lpath_id',0));
	$scorm = null;//new stdClass();

	//kosmos_edit
	$scorm = JLMS_CheckSCORMFile($id, $lpath_id, $course_id, $scorm, $usertype);

	if (isset($scorm->scorm_name) && $scorm->scorm_name)  {	
		if (!empty($scorm)) {
			// 11.04.2007 (parameter 'redirect_to_learnpath' used to avoid redirect cycles from course home to LP and back
			$JLMS_SESSION->clear('redirect_to_learnpath');

			// sleep one second. SCORM could send some requests on onCloasPage event. We need wait for these requests to be processed.
			#sleep(2);

			$scoid = intval(mosGetParam($_REQUEST, 'scoid',0));
    		$mode = 'normal';//strval(mosGetParam($_REQUEST, 'mode','normal'));//optional_param('mode', 'normal', PARAM_ALPHA);
    		$currentorg = strval(mosGetParam($_REQUEST, 'currentorg', ''));//, PARAM_RAW);
    		$newattempt = strval(mosGetParam($_REQUEST, 'newattempt', 'on'));

			$box_view = false;
			require_once( _JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.play.php");
			if (isset($scorm->params)) {
				$params = new JLMSParameters($scorm->params);
				if ($params->get('scorm_layout', 0) && $params->get('scorm_layout', 0) == 1) {
					$box_view = true;
				}
			}

			if ($box_view) {
				// SqueezeBox view - don't show Heading.
				echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
				echo '<html xmlns="http://www.w3.org/1999/xhtml" style="margin:0;padding:0"><head>';
				echoScormBoxHeaders();
				echo '</head><body style="margin:0;padding:0">';
			} else {
				JLMS_ShowHeading();
			}
			$scorm_file = 1;

			JLMS_SCORM_PLAY_MAIN( $scorm, $option, $course_id, $lpath_id, $scoid, $mode, $currentorg, $newattempt , false, $scorm_file, $box_view);
			if ($box_view) {
				echo '</body></html>';
				JLMS_die();
			}
			//JLMS_TMPL::CloseMT();
		} else {
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=outdocs") );
		}
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=outdocs") );
	}
}

function JLMS_CheckSCORMFile( &$id, $lpath_id, $course_id, &$scorm, $usertype ) {
	global $JLMS_DB;

	$query = "SELECT a.*, b.doc_name FROM #__lms_n_scorm AS a, #__lms_outer_documents AS b WHERE b.file_id = '".$id."' AND a.id = '".$id."'";
	$JLMS_DB->SetQuery($query);
	$scorm = $JLMS_DB->LoadObject();
	if (is_object($scorm)) {
		$scorm->scorm_name = $scorm->doc_name;
		$query = "SELECT * FROM #__lms_scorm_packages WHERE id = $scorm->scorm_package";
		$JLMS_DB->SetQuery($query);
		$scorm_ref = $JLMS_DB->LoadObject();
		if (is_object($scorm_ref)) {
			$scorm->reference = _JOOMLMS_SCORM_FOLDER_PATH . "/" . $scorm_ref->package_srv_name;
			$scorm->reference_folder = _JOOMLMS_SCORM_FOLDER_PATH . "/" . $scorm_ref->folder_srv_name;
		} else {
			return false;
		}
	} 

	unset($scorm->doc_name);
	unset($scorm->reference);
	unset($scorm->reference_folder);

	return $scorm;
}

function JLMSO_saveScorm( $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$user = JLMSFactory::getUser();

	$open_folder = 0;
	$open_element = 0;
	$course_id = 0;
    
    $changeHeightSCROM = new JLMS_changeHeightSCORM( JRequest::getVar('id', 0) );
    if($changeHeightSCROM->update_all_recent_sources){
    
        $row = new mos_Joomla_LMS_Outer_Document( $JLMS_DB );
    
        $row->publish_start = 0;
        $row->publish_end = 0;
        if (!$row->bind( $_POST )) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }
    
        $scorm_params = '';
        $params = mosGetParam( $_POST, 'params', '' );
        if (is_array( $params )) {
            $txt = array();
            foreach ( $params as $k=>$v) {
                $txt[] = "$k=$v";
            }
            $scorm_params = implode( "\n", $txt );
        }
    
        $row->id = intval($row->id);
        if($row->id){
            $query = "SELECT file_id FROM #__lms_outer_documents WHERE id=".$row->id;
            $JLMS_DB->setQuery($query);
            $row->file_id = $JLMS_DB->loadResult();
        }
    
        $scorm_result = false;
        $scorm_upl_type = intval(mosGetParam($_REQUEST, 'scorm_upl_type', 1));
        $from_ftp = false;
        $req = 'scorm_file';
        
        if ($scorm_upl_type == 2) {
            $from_ftp = true;
            $req = strval(mosGetParam($_REQUEST, 'scorm_ftp_file', ''));
        }
    
        $scorm_pack_id = '';
    
        if (!$row->id && !isset($_FILES['scorm_file']['name']) && !strval(mosGetParam($_REQUEST, 'scorm_ftp_file', ''))) {
            echo "<script> alert('"._JLMS_EM_SELECT_FILE."'); window.history.go(-1); </script>\n";
            exit();
        }
        elseif($row->id && (isset($_FILES['scorm_file']['name']) || strval(mosGetParam($_REQUEST, 'scorm_ftp_file', '')))) {
            
            //Max (05.06.2013) - fix delete old folder and file scorm when edit
            $lms_n_scorm_id = 0;
            if(isset($row->file_id) && $row->file_id){
                $lms_n_scorm_id = $row->file_id;
            }
            //Max (05.06.2013) - fix delete old folder and file scorm when edit
            
            if(!$_FILES['scorm_file']['name'] && !strval(mosGetParam($_REQUEST, 'scorm_ftp_file', ''))) {
                $query = "SELECT scorm_package FROM #__lms_n_scorm WHERE id = $row->file_id";
                $JLMS_DB->setQuery($query);
                $scorm_pack_id = $JLMS_DB->loadResult();
            }
            else {
                $scorm_pack_id = JLMS_uploadSCORM($req, $from_ftp, $lms_n_scorm_id); //Max (05.06.2013) - fix delete old folder and file scorm when edit
            }
    
            $query = "SELECT package_srv_name FROM #__lms_scorm_packages WHERE id = $scorm_pack_id";
            $JLMS_DB->setQuery($query);
            $package_srv_name = $JLMS_DB->loadResult();
    
            $reference = _JOOMLMS_SCORM_FOLDER_PATH . "/" . $package_srv_name;
            $md5hash = md5_file($reference);
    
            $query = "SELECT package_srv_name FROM #__lms_scorm_packages WHERE id = $scorm_pack_id";
            $JLMS_DB->setQuery($query);
            $package_srv_name = $JLMS_DB->loadResult();
    
            $reference = _JOOMLMS_SCORM_FOLDER_PATH . "/" . $package_srv_name;
            $md5hash = md5_file($reference);
    
            $query = "SELECT scorm_package FROM #__lms_n_scorm WHERE id = $row->file_id";
            $JLMS_DB->setQuery($query);
            $scorm_package = $JLMS_DB->loadResult();
    
            $query = "UPDATE #__lms_n_scorm SET scorm_package = $scorm_pack_id , md5hash = '".$md5hash."' WHERE scorm_package = $scorm_package";
            $JLMS_DB->SetQuery($query);
            $JLMS_DB->query();
    
            $query = "UPDATE #__lms_n_scorm_lib SET scorm_package = $scorm_pack_id WHERE lib_id = $row->id AND lib_n_scorm_id = $row->file_id";
            $JLMS_DB->SetQuery($query);
            $JLMS_DB->query();
        }
        elseif(isset($_FILES['scorm_file']['name']) || strval(mosGetParam($_REQUEST, 'scorm_ftp_file', ''))) {
            $scorm_pack_id = JLMS_uploadSCORM($req, $from_ftp);
        }
        else {
            $query = "SELECT c.id FROM #__lms_outer_documents AS a, #__lms_n_scorm AS b, #__lms_scorm_packages AS c WHERE a.id='".$row->id."' AND b.id=a.file_id AND c.id=b.scorm_package";
            $JLMS_DB->setQuery($query);
            $scorm_pack_id = $JLMS_DB->loadResult();
        }
        $msg_db = $JLMS_DB->geterrormsg();
    
        if ($msg_db && !$row->id) {
            echo "<script> alert('".$msg_db."');</script>\n";
        }
    
        if ($scorm_pack_id) {
            $scorm_height = intval(mosGetParam($_REQUEST, 'scorm_height', 0));
            $scorm_width = intval(mosGetParam($_REQUEST, 'scorm_width', 0));
    
            $query = "UPDATE #__lms_scorm_packages SET window_height = $scorm_height WHERE id = $scorm_pack_id";
            $JLMS_DB->SetQuery($query);
            $JLMS_DB->query();
    
            $query = "SELECT * FROM #__lms_n_scorm WHERE scorm_package = $scorm_pack_id"
			. "\n AND course_id = 0" //fix for related resources
			;
            $JLMS_DB->SetQuery($query);
            $scorm_n = $JLMS_DB->LoadObject();
            if (is_object($scorm_n)) {
                $query = "UPDATE #__lms_scorm_packages SET window_height = $scorm_height WHERE id = $scorm_n->scorm_package";
                $JLMS_DB->SetQuery($query);
                $JLMS_DB->query();
                $query = "UPDATE #__lms_n_scorm SET height = ".$scorm_height.", width = ".$scorm_width.", params = ".$JLMS_DB->Quote($scorm_params)." WHERE id = $scorm_n->id";
                $JLMS_DB->SetQuery($query);
                $JLMS_DB->query();
            }					
    
            // HERE we need to update this package (insert records into `#__lms_n_scorm`, `#__lms_n_scorm_scoes` tables
            require_once(_JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.class.php");
            require_once(_JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.lib.php");
            require_once(_JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.operate.php");
            
            $scorm = new stdClass();
    
            if(!$row->id) {
                $scorm = JLMS_SCORM_ADD_INSTANCE($course_id, $scorm_pack_id);
            }
            else {
                $scorm->id = $row->file_id;
            }
    
            if (isset($scorm->id) && $scorm->id) {
                $scorm_result = true;
                //$row->item_id = $scorm->id;
                $scorm_id = $scorm->id;
                //$row->lp_type = 1;
                if ($scorm_params) {
                    $query = "UPDATE #__lms_n_scorm SET params = ".$JLMS_DB->Quote($scorm_params)." WHERE id = $scorm_id";
                    $JLMS_DB->SetQuery($query);
                    $JLMS_DB->query();
                }
            } else {
                if(!$row->id) {
                    $query = "SELECT * FROM #__lms_scorm_packages WHERE id = $scorm_pack_id";
                    $JLMS_DB->SetQuery($query);
                    $del_scorm = $JLMS_DB->LoadObject();
                    if (is_object($del_scorm)) {
                        require_once(_JOOMLMS_FRONT_HOME . "/includes/jlms_dir_operation.php");
                        $scorm_folder = JPATH_SITE . "/" . _JOOMLMS_SCORM_FOLDER . "/";
                        deldir( $scorm_folder . $del_scorm->folder_srv_name . "/" );
                        unlink( $scorm_folder . $del_scorm->package_srv_name );
                        $query = "DELETE FROM #__lms_scorm_packages WHERE id = $scorm_pack_id";
                        $JLMS_DB->SetQuery( $query );
                        $JLMS_DB->query();
                    }
                    $lp_save_error_tmp = str_replace('_line_', __LINE__, $lp_save_error) . 'failed to add SCORM instance.';
                    echo "<script> alert('".$lp_save_error_tmp."'); window.history.go(-1); </script>\n";
                    exit();
                }
            }
        } else {
            if(!$row->id) {
                $lp_save_error_tmp = str_replace('_line_', __LINE__, $lp_save_error) . 'failed to upload SCORM instance.';
                echo "<script> alert('".$lp_save_error_tmp."'); window.history.go(-1); </script>\n";
                exit();
            }
        }
    
        $row->folder_flag = 3;
        if (!$row->id) {
            $row->owner_id = $user->get('id');
        } else {
            unset($row->owner_id);
        }
        $row->published = 1;
    
        if(!$row->file_id && isset($scorm_id) && $scorm_id) {
            $row->file_id = $scorm_id;
        }
    
        $parent_id = intval(mosGetParam( $_REQUEST, 'course_folder', 0 ));
        $row->parent_id = $parent_id;
    
        if($row->doc_name == '') {
            if($scorm_upl_type == 1)
                $row->doc_name = $_FILES['scorm_file']['name'];
            if($scorm_upl_type == 2)
                $row->doc_name = mosGetParam($_REQUEST, 'scorm_ftp_file', '');
        }		
    
        if (!$row->check()) {
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }
        if (!$row->store()) {
            echo $row->getError(); die;
            echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
            exit();
        }
    
        $open_folder = isset($row->parent_id) ? $row->parent_id : 0;
        $open_element = isset($row->id) ? $row->id : 0;
        $not_sef_link = "index.php?option=$option&Itemid=$Itemid&task=outdocs";
        if ($open_folder) {
            $not_sef_link .= "&folder=$open_folder";	
        }
        if ($open_element) {
            $not_sef_link .= "&element=$open_element";	
        }
        JLMSRedirect( sefRelToAbs($not_sef_link) );
    }
}


function JLMSO_editScorm( $id, $option, $is_folder = 0 ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$is_teacher = $JLMS_ACL->isTeacher(); 

	if($is_teacher){
		$row = new mos_Joomla_LMS_Outer_Document( $JLMS_DB );
		$row->load( $id );
		if ($id) {
		} else {
			$row->published = 0;
			$row->folder_flag = $is_folder;
		}
		$lists = array();
		$ex_id = null;
		if ($id && ($row->folder_flag == 1)) {
			$ex_id = $id;
		}
		$lists['course_folders'] = Create_OutFoldersList($row->parent_id, $ex_id);
		$lists['publishing'] = mosHTML::yesnoRadioList( 'published', 'class="inputbox" ', $row->published);
		if ($row->allow_link) { $row->allow_link = 1; } else { $row->allow_link = 0;}
		$lists['share_to_courses'] = mosHTML::yesnoRadioList( 'allow_link', '', $row->allow_link);

		$params = new JLMSParameters('scorm_nav_bar=0'."\n".'scorm_layout=0');
		$lp_params = new JLMSParameters('show_in_gradebook=0'."\n".'hide_in_list=0'."\n".'add_forum=0'); //added add_forum parameter (by TPETb)
		$row->lpath_completion_msg = '';
		$row->scorm_height = 0;
		$row->scorm_width = 0;

		$query = "SELECT height, width, params FROM #__lms_n_scorm WHERE id = '".$row->file_id."'";
		$JLMS_DB->SetQuery( $query );
		$f_p = $JLMS_DB->LoadObject();
		if (is_object($f_p)) {
			if ($f_p->height) {
				$row->scorm_height = $f_p->height;
			}
			if ($f_p->width) {
				$row->scorm_width = $f_p->width;
			}
			$params = new JLMSParameters($f_p->params);
		} else {
			$params = new JLMSParameters('scorm_nav_bar=0'."\n".'scorm_layout=0');
		}
		
		if ($row->allow_link) { $row->allow_link = 1; } else { $row->allow_link = 0;}
		$lists['share_to_courses'] = mosHTML::yesnoRadioList( 'allow_link', '', $row->allow_link);
		
		JLMS_outdocs_html::showEditScorm( $row, $lists, $option, $params, $lp_params );
	}	
}

function JLMSO_previewContent( $doc_id, $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$user = JLMSFactory::getUser();
	$do_redirect = true;
	$is_teacher = $JLMS_ACL->isTeacher();
	$can_do_everything = $JLMS_ACL->CheckPermissions('library', 'only_own_items') ? false : ( $is_teacher ? true : false);
	$query = "SELECT * FROM #__lms_outer_documents"
	. "\n WHERE id = '".$doc_id."' AND folder_flag = 0 AND file_id = 0"
	. "\n AND ( ((published = 1) "
	. "\n AND ( ((publish_start = 1) AND (start_date <= '".date('Y-m-d')."')) OR (publish_start = 0) )"
	. "\n AND ( ((publish_end = 1) AND (end_date >= '".date('Y-m-d')."')) OR (publish_end = 0) )"
	. (($is_teacher) ? "\n AND ( outdoc_share!=0 ))" : "\n AND ( outdoc_share=2 ))")
	. "\n OR (owner_id = ".$user->get('id').")".($can_do_everything ? ' OR 1=1' : '').")"
	;
	$JLMS_DB->SetQuery( $query );
	$row_zip = $JLMS_DB->LoadObject();
	if ( is_object($row_zip) ) {
		if ($row_zip->doc_name) {
			$do_redirect = false;
			$lists = array();

			JLMS_outdocs_html::show_ZipPack(  $option, $row_zip, $lists, 'content' );
		}
	}
	if ($do_redirect) {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=outdocs") );
	}
}

function JLMSO_OrderDocuments( $option ) {
	global $JLMS_DB, $Itemid, $task, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$user = JLMSFactory::getUser();

	$order_id = intval(mosGetParam($_REQUEST, 'row_id', 0));
	$query = "SELECT owner_id FROM #__lms_outer_documents WHERE id = '".$order_id."'";
	$JLMS_DB->SetQuery( $query );
	$u_id = $JLMS_DB->LoadResult();
	$is_teacher = $JLMS_ACL->isTeacher(); 
	$can_do_everything = $JLMS_ACL->CheckPermissions('library', 'only_own_items') ? false : ( $is_teacher ? true : false);
	$open_folder = 0;
	if ( $order_id && ($u_id == $user->get('id') || $can_do_everything) ) {
		$query = "SELECT parent_id FROM #__lms_outer_documents WHERE id = '".$order_id."'";
		$JLMS_DB->SetQuery( $query );
		$item_parent = $JLMS_DB->LoadResult();
		if ($item_parent) {
			$open_folder = $item_parent;
		}
		$query = "SELECT id FROM #__lms_outer_documents WHERE parent_id = '".$item_parent."' ORDER BY ordering, doc_name";
		$JLMS_DB->SetQuery( $query );
		$id_array = JLMSDatabaseHelper::LoadResultArray();
		if (count($id_array)) {
			$i = 0;$j = 0;
			while ($i < count($id_array)) {
				if ($id_array[$i] == $order_id) { $j = $i;}
				$i ++;
			}
			$do_update = true;
			if (($task == 'outdoc_orderup') && ($j) ) {
				$tmp = $id_array[$j-1];
				$id_array[$j-1] = $id_array[$j];
				$id_array[$j] = $tmp;
			} elseif (($task == 'outdoc_orderdown') && ($j < (count($id_array)-1)) ) {
				$tmp = $id_array[$j+1];
				$id_array[$j+1] = $id_array[$j];
				$id_array[$j] = $tmp;
			}
			$i = 0;
			foreach ($id_array as $document_id) {
				$query = "UPDATE #__lms_outer_documents SET ordering = '".$i."' WHERE id = '".$document_id."'";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$i ++;
			}
		}
	}
	if ($open_folder) {
		$link = sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=outdocs&folder=$open_folder");
	} else {
		$link = sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=outdocs");
	} 
	JLMSRedirect( $link );
}

function JLMSO_downloadDocument( $id, $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$user = JLMSFactory::getUser();

	$is_teacher = $JLMS_ACL->isTeacher();
	$force_download = strval( mosGetParam( $_REQUEST, 'force', '' ) ); 
	$file_contents = false;
	$can_do_everything = $JLMS_ACL->CheckPermissions('library', 'only_own_items') ? false : ( $is_teacher ? true : false);
	$query = "SELECT * FROM #__lms_outer_documents"
	. "\n WHERE id = '".$id."'"
	. "\n AND ( ((published = 1) "
	. "\n AND ( ((publish_start = 1) AND (start_date <= '".date('Y-m-d')."')) OR (publish_start = 0) )"
	. "\n AND ( ((publish_end = 1) AND (end_date >= '".date('Y-m-d')."')) OR (publish_end = 0) )"
	. (($is_teacher) ? "\n AND ( outdoc_share!=0 ))" : "\n AND ( outdoc_share=2 ))")
	. "\n OR (owner_id = ".$user->get('id').")".($can_do_everything ? ' OR 1=1' : '').")"
	;
	$JLMS_DB->SetQuery( $query );
	$file_data = $JLMS_DB->LoadObjectList();
	if (count($file_data) == 1) {
		$file_contents = false;
		$do_tracking = false;
		if ($force_download != 'force' && $force_download != 'player') {
			require_once(_JOOMLMS_FRONT_HOME . "/includes/jlms_docs_process.php");
			$params['doc_get_link'] = $JLMS_CONFIG->getCfg('live_site')."/index.php?tmpl=component&option=com_joomla_lms&Itemid=$Itemid&task=get_outdoc&id=$id&force=force";
			$params['doc_get_link_url_enc'] = $JLMS_CONFIG->getCfg('live_site')."/index.php%3Ftmpl%3Dcomponent%26option%3Dcom_joomla_lms%26Itemid%3D$Itemid%26task%3Dget_outdoc%26id%3D$id%26force%3Dforce";
			$file_contents = JLMS_showMediaDocument($file_data[0]->file_id, $id, $file_data[0]->doc_name, $do_tracking, $params );
		}
		if ( ($file_contents && $do_tracking) || !$file_contents) {
			//tracking
		}
		if ($file_contents) {
			$lists = array();
			$row_zip = new stdClass();
			$row_zip->doc_description = $file_contents;
			$row_zip->doc_name = $file_data[0]->doc_name;
			$row_zip->doc_id = $id;
			$row_zip->id = $id;
			$row_zip->parent_id = $file_data[0]->parent_id;
			$lpath_id = intval( mosGetParam( $_REQUEST, 'lpath_id', 0 ) ); 
			$lists['lpath_id'] = $lpath_id;
			JLMS_outdocs_html::show_ZipPack($option, $row_zip, $lists, 'document_contents');
		} else {
			$headers = array();
			if ($force_download == 'player') {
				$headers['Content-Disposition'] = 'inline';
			}
			JLMS_downloadFile( $file_data[0]->file_id, $option, $file_data[0]->doc_name, true, $headers);
		}
	}
	if (!$file_contents) {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=outdocs&id=$course_id") );
	}
}
//to do: proverku na teacher, student,. ...
//+ otobragat' daty zakachki faila..
// + add v 'SelectListe' otobragat' dlinnyi put' k folderam (t.e. s nazvaniyami vsex parentov)
// new: ubrat' lists - teper' eta peremennaya ne nugna t.k. dobavlenie failov/folderov vyneseno v dr. procedury
function JLMSO_get_itemparent(&$rows, $item) {
	$parent = 0;
	foreach ($rows as $row) {
		if ($row->id == $item) {
			$parent = $row->parent_id;break;
		}
	}
	return $parent;
}

function JLMSO_showDocuments( $option) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$user = JLMSFactory::getUser();

	$is_teacher = $JLMS_ACL->isTeacher();
	$open_folder = intval( mosGetParam( $_REQUEST, 'folder', 0 ) );
	$open_element = intval( mosGetParam( $_REQUEST, 'element', 0 ) );  
	$can_do_everything = $JLMS_ACL->CheckPermissions('library', 'only_own_items') ? false : ( $is_teacher ? true : false);

	$query = "SELECT a.*, b.file_name, c.name, c.username, c.name as author_name"
	. "\n FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0 LEFT JOIN #__users as c ON a.owner_id = c.id"
	. "\n WHERE ((a.published = 1) "
	. "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )"
	. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d')."')) OR (a.publish_end = 0) )"
	. (($is_teacher) ? "\n AND ( a.outdoc_share!=0 ))" : "\n AND ( a.outdoc_share=2 ))")
	. "\n OR (a.owner_id = ".$user->get('id').")".($can_do_everything ? ' OR 1=1' : '')
	. "\n ORDER BY a.parent_id, a.ordering, a.doc_name, c.username";
	$JLMS_DB->SetQuery( $query );
	$rows = $JLMS_DB->LoadObjectList();
		
	$rows = JLMS_GetTreeStructure( $rows );
	$rows = AppendFileIcons_toList( $rows );

	if ($open_folder || $open_element) {
		$do_scroll = $open_folder;
		if ($open_element) {
			$do_scroll = $open_element;
		}
		$domready = '
			var winScroller_lib = new Fx.Scroll(window);
			winScroller_lib.toElement("tree_row_'.$do_scroll.'");
			';
		$JLMS_CONFIG->set('web20_domready_code', $JLMS_CONFIG->get('web20_domready_code','').$domready);
	}
	if ($open_folder) {
		$first_openfolder = $open_folder;
		$limit_number = 20;
		$opened_folders = array();
		$opened_folders[] = $open_folder;
		$jj = 0;
		while ($jj < $limit_number && $open_folder) {
			$new_open_folder = JLMSO_get_itemparent($rows,$open_folder);
			if ($new_open_folder) {
				$opened_folders[] = $new_open_folder;
				$open_folder = $new_open_folder;
			}
			$jj++;
		}
		if (count($opened_folders)) {
			$opened_folders_str = implode(',',$opened_folders);
			$query = "SELECT id FROM #__lms_outer_documents WHERE folder_flag = 1 AND id NOT IN ($opened_folders_str)";
		} else {
			$query = "SELECT id FROM #__lms_outer_documents WHERE folder_flag = 1";
		}
	} else {
		$query = "SELECT id FROM #__lms_outer_documents WHERE folder_flag = 1";
	}
	$JLMS_DB->SetQuery($query);
	$rows2 = JLMSDatabaseHelper::LoadResultArray();

	$lists = array();
	$lists['collapsed_folders'] = $rows2;

	$scorm_ids = array();
	for($i=0;$i<count($rows);$i++) {
		if($rows[$i]->folder_flag == 3) {
			$rows[$i]->file_icon = 'tlb_scorm';
			$scorm_ids[] = $rows[$i]->file_id;
		}
	}
	if (count($scorm_ids)) {
		$scorm_ids_str = implode(',',$scorm_ids);
		$query = "SELECT id, params, width, height FROM #__lms_n_scorm WHERE id IN ($scorm_ids_str)";
		$JLMS_DB->SetQuery($query);
		$scorms_params = $JLMS_DB->LoadObjectList();
		foreach ($scorms_params as $scorm_params) {
			for($i=0;$i<count($rows);$i++) {
				if($rows[$i]->folder_flag == 3) {
					if ($rows[$i]->file_id == $scorm_params->id) {
						$rows[$i]->scorm_params = $scorm_params->params;
						$rows[$i]->scorm_width = $scorm_params->width;
						$rows[$i]->scorm_height = $scorm_params->height;
						break;
					}
				}
			}
		}
	}

	JLMS_outdocs_html::showCourseDocuments( $option, $rows, $lists, $is_teacher );
}

function JLMSO_editDocument( $id, $option, $is_folder = 0 ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$is_teacher = $JLMS_ACL->isTeacher(); 
	//$course_id = $JLMS_CONFIG->get('course_id');
	//if ( $course_id && $JLMS_ACL->CheckPermissions('docs', 'manage') && ( ($id && (JLMS_GetDocumentCourse($id) == $course_id)) || !$id ) ) {
	
	$query = "SELECT folder_flag FROM #__lms_outer_documents WHERE id = '".$id."'";
	$JLMS_DB->SetQuery( $query );
	$folder_flag = $JLMS_DB->LoadResult();

	
	if($is_teacher){
		
		if($folder_flag == 3) {
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=edit_scorm&id=".$id) );	
		}
		else {
		
		
		$row = new mos_Joomla_LMS_Outer_Document( $JLMS_DB );
		$row->load( $id );
		if ($id) {
		} else {
			$row->published = 0;
			$row->folder_flag = $is_folder;
		}
		$lists = array();
		$ex_id = null;
		if ($id && ($row->folder_flag == 1)) {
			$ex_id = $id;
		}
		$lists['course_folders'] = Create_OutFoldersList($row->parent_id, $ex_id);
		$lists['publishing'] = mosHTML::yesnoRadioList( 'published', 'class="inputbox" ', $row->published);
		if ($row->allow_link) { $row->allow_link = 1; } else { $row->allow_link = 0;}

		$lists['share_to_courses'] = mosHTML::yesnoRadioList( 'allow_link', '', $row->allow_link);
		$lists['upload_zip'] = mosHTML::yesnoRadioList( 'upload_zip', '', 1 );/*batch uploads */
		JLMS_outdocs_html::showEditDocument( $row, $lists, $option );
		}
	}
	else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=outdocs") );
	}
}
function JLMSO_cancelDocument( $option ) {
	global $Itemid, $JLMS_CONFIG;
	JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=outdocs"));
}

function JLMSO_get_dir_elements($curpath) {
	$lsoutput = "";
	$result = array();
	$dir = dir($curpath);
   
	while ($file = $dir->read()) {
	   if($file != "." && $file != "..") {
	       if (is_dir($curpath.$file)) {
	             $result[$file] = JLMSO_get_dir_elements($curpath.$file."/");
	       } else {
	         	$result [$file] = $file;
	       }
	   }
	}
	$dir->close();
	return $result;
}

function JLMSO_saveDocument( $option ) {
	
	$db = JFactory::getDbo();
	$JLMS_CONFIG =  JLMSFactory::getConfig();
	$Itemid = $JLMS_CONFIG->get('Itemid');
	$JLMS_ACL = JLMSFactory::getACL();
	$JLMS_CONFIG->set('files_skipped_by_extension', false);

	$id = intval(mosGetParam($_REQUEST, 'id', 0));
	$doc_order = intval(mosGetParam($_REQUEST, 'doc_order', 0));
	$folder_flag = intval(mosGetParam($_REQUEST, 'folder_flag', 0));
	$parent_id = intval(mosGetParam( $_REQUEST, 'course_folder', 0 ));
	$upload_zip = mosGetParam($_REQUEST,'upload_zip');	
    
    //==================================================
	// Saving order fix.
	
	// $query = "SELECT max(ordering) FROM #__lms_outer_documents WHERE parent_id = $parent_id";
	// $db->SetQuery( $query );
	// $max_ordering = $db->LoadResult() + 1;
	// $ordering = array();
	// $ordering[$parent_id] = $max_ordering;
	
	$ordering = array(
		$parent_id => (int) JRequest::getVar("doc_order", 0),
		);
	
	//==================================================

	$open_folder = 0;
	$open_element = 0;

	$zip_files_array = array();
	$zip_files_names = array();
	$no_files_array = array();
	$temporary_files = array();

	if ($folder_flag) {
		$upload_zip = false;
	}
	if ($id) {
		$upload_zip = false;
	}

	if(true) {

		require_once(JPATH_SITE . DS . "components" . DS . "com_joomla_lms" . DS . "includes" . DS . "libraries" . DS . "lms.lib.files.php");
		require_once(JPATH_SITE . DS . "components" . DS . "com_joomla_lms" . DS . "includes" . DS . "libraries" . DS . "lms.lib.zip.php");

		$available_app_ext = array();
		$available_app_ext[] = 'application/zip';
		$available_app_ext[] = 'application/x-zip';
		$available_app_ext[] = 'application/zip-compressed';
		$available_app_ext[] = 'application/x-zip-compressed';

		if (count($_FILES)) {
		for($i=0;$i<count($_FILES);$i++) {
			if(isset($_FILES['userfile'.$i]) && isset($_FILES['userfile'.$i]['name']) && $_FILES['userfile'.$i]['name']){
				//get 4 last letters from file name and check if it is zip file
				$out_fileext = '';
				if($upload_zip) {
					$out_fileext = strtolower(substr($_FILES['userfile'.$i]['name'], - 4));
					//preg_match('/.*\.(\S+)/', $_FILES['userfile'.$i]['name'], $out); //if MAC
				}
				if($upload_zip && (in_array($_FILES['userfile'.$i]['type'], $available_app_ext) || ($out_fileext == '.zip')) ) {
					//upload and extract zip file
					$msg = '';
					if(JLMS_Files::uploadFile( $_FILES['userfile'.$i]['tmp_name'], $_FILES['userfile'.$i]['name'], $msg )) {
						$temp_dir = uniqid(rand());
						$extract_dir = JPATH_SITE."/tmp/file_".$temp_dir."/";
						$temporary_files[] = $extract_dir;
						$archive = JPATH_SITE."/tmp/".$_FILES['userfile'.$i]['name'];
						if(JLMS_Zip::extractFile( $archive, $extract_dir)) {
							$zip_files_array[$extract_dir] = JLMSO_get_dir_elements($extract_dir);
							//check if zip contains only one file - then replace it's name with the doc_nameX field
							$elems_count = 0;
							$folders_count = 0;
							$item_name = '';
							foreach ($zip_files_array[$extract_dir] as $k => $v) {
								$item_name = $k;
								$elems_count++;
								if (is_array($v)) {
									$folders_count ++;
								}
							}
							if (!$folders_count) {
								if ($elems_count == 1 && $item_name) {
									if (isset($_REQUEST['doc_name'.$i]) && $_REQUEST['doc_name'.$i]) {
										$zip_files_names[$extract_dir][$item_name] = $_REQUEST['doc_name'.$i];
									}
								}
							}
						}
					}
				} else {
					//upload usual file
					$msg = '';
					if(JLMS_Files::uploadFile( $_FILES['userfile'.$i]['tmp_name'], $_FILES['userfile'.$i]['name'], $msg )) {
						$tmp_folder = JPATH_SITE . "/tmp/";
						$zip_files_array[$tmp_folder][$_FILES['userfile'.$i]['name']] = $_FILES['userfile'.$i]['name'];
						$temporary_files[] = $tmp_folder.$_FILES['userfile'.$i]['name'];
						if (isset($_REQUEST['doc_name'.$i]) && $_REQUEST['doc_name'.$i]) {
							$zip_files_names[$tmp_folder][$_FILES['userfile'.$i]['name']] = $_REQUEST['doc_name'.$i];
						}
					} else {
						$JLMS_CONFIG->set('files_skipped_by_extension', true);
					}
				}
			} else {
				if(isset($_REQUEST['doc_name'.$i]) && $_REQUEST['doc_name'.$i]){
					$no_files_array[] = $_REQUEST['doc_name'.$i];
				}
			}
		}
		} else {
			//new folder, without $_FILES array
			$i = 0;
			if(isset($_REQUEST['doc_name'.$i]) && $_REQUEST['doc_name'.$i]){
				$no_files_array[] = $_REQUEST['doc_name'.$i];
			}
		}

		if( count ($zip_files_array)) {
			$first_saved_id = 0;
			foreach ($zip_files_array as $k=>$v) {
				$docname = (isset($zip_files_names[$k]) && $zip_files_names[$k]) ? $zip_files_names[$k] : array();
				$saved_id = 0;
				JLMS_save_file_from_zip_array($v, $parent_id, substr($k,0,strlen($k)-1), $ordering, $saved_id, $docname);
				if (!$first_saved_id && $saved_id) {
					$first_saved_id = $saved_id;
				}
			}
			if ($first_saved_id) {
				$open_element = $first_saved_id;
				if ($parent_id) {
					$open_folder = $parent_id;
				}
			}
			foreach ($temporary_files as $temporary_file) {
				if (file_exists($temporary_file)) {
					if (is_dir($temporary_file)) {
						JLMS_Files::delFolder($temporary_file);
					} elseif (is_file($temporary_file)) {
						JLMS_Files::delFile($temporary_file);
					}
				}
			}
		} else {
			if ($id) {
				foreach ($no_files_array as $no_files_elem) { // only one item should be in this array
					JLMS_save_single_file ($no_files_elem, $folder_flag, $parent_id, '', $ordering);
				}
				$open_element = $id;
				if ($parent_id) {
					$open_folder = $parent_id;
				}
			} else {
				$first_elem_id = 0;
				foreach ($no_files_array as $no_files_elem) {
					$new_elem = JLMS_save_single_file ($no_files_elem, $folder_flag, $parent_id, '', $ordering);
					if (!$first_elem_id) {
						$first_elem_id = $new_elem;
					}
				}
				if ($first_elem_id) {
					$open_element = $id;
					if ($parent_id) {
						$open_folder = $parent_id;
					}
				}
			}
		}
	}

	$not_sef_link = "index.php?option=$option&Itemid=$Itemid&task=outdocs";
	if ($open_folder) {
		$not_sef_link .= "&folder=$open_folder";	
	}
	if ($open_element) {
		$not_sef_link .= "&element=$open_element";	
	}
	$msg = '';
	if ($JLMS_CONFIG->get('files_skipped_by_extension', false)) {
		$msg = _JLMS_DOCS_NOT_UPLOADED_DUE_TO_TYPE_RESTRICTION;
	}
	JLMSRedirect( sefRelToAbs($not_sef_link), $msg );
}

/* batch uploads */
function JLMS_save_file_from_zip_array ( $elem , $parent, $path, &$ordering, &$first_saved_id, $elemname = array()) {
	foreach ($elem as $k=>$v) {
		if(is_array($v)) {
			$path1 = $path."/".$k;
			$parent1 = JLMS_save_single_file($k, 1, $parent, '', $ordering);
			if (!$first_saved_id) {
				$first_saved_id = $parent1;
			}
			JLMS_save_file_from_zip_array ( $v , $parent1, $path1, $ordering, $first_saved_id, $new_elem_name);
		} else {
			$new_elem_name = '';
			if (is_array($elemname) && count($elemname)) {
				$new_elem_name = isset($elemname[$k]) ? $elemname[$k] : '';
			}
			$saved_elem = JLMS_save_single_file($k, 0, $parent, $path, $ordering, $new_elem_name);
			if (!$first_saved_id) {
				$first_saved_id = $saved_elem;
			}
		}
	}
}

/* batch uploads */
function JLMS_save_single_file ($doc_name_post, $folder_flag, $parent_id, $path, &$ordering, $elemname = '') {
	$db = JFactory::getDbo();
	$user = JLMSFactory::getUser();
	$JLMS_CONFIG = JLMSFactory::getConfig();

	$row = new mos_Joomla_LMS_Outer_Document( $db );
	$row->publish_start = 0;
	$row->publish_end = 0;
	if (!$row->bind( $_POST )) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	
	$row->folder_flag = $folder_flag; 
	
	$row->id = intval($row->id);
	if($row->id){
		$query = "SELECT file_id FROM #__lms_outer_documents WHERE id=".$row->id;
		$db->setQuery($query);
		$row->file_id = $db->loadResult();
	}

	$doc_name_post = (get_magic_quotes_gpc()) ? stripslashes( $doc_name_post ) : $doc_name_post; 
	$doc_name_post = strip_tags($doc_name_post);

	if ($folder_flag == 1) {
		$row->doc_name = $elemname ? $elemname : $doc_name_post;
	} else {
		$row->doc_name = $elemname ? $elemname : $doc_name_post;
		if ($path) {
			$file_id = JLMS_uploadFile_from_zip($path, $doc_name_post);
			if ($file_id === false) {
				$JLMS_CONFIG->set('files_skipped_by_extension', true);
				return false;
			}
		} else {
			$file_id = 0;
		}
		if ($file_id) {
			if ($row->file_id) {
				JLMS_deleteFiles($row->file_id);
			}
			@unlink($path."/".$doc_name_post);
			$row->file_id = $file_id;
		}
	}
	// start-end publishing
	$publish_start = intval(mosGetParam($_REQUEST, 'is_start', 0));
	$publish_end = intval(mosGetParam($_REQUEST, 'is_end', 0));
	$row->publish_start = $publish_start;
	$row->publish_end = $publish_end;
	if ($row->publish_start) {
		$row->start_date = mosGetParam($_REQUEST, 'start_date', '');
		$row->start_date = JLMS_dateToDB($row->start_date);
	} else { $row->start_date = ''; }
	if ($row->publish_end) {
		$row->end_date = mosGetParam($_REQUEST, 'end_date', '');
		$row->end_date = JLMS_dateToDB($row->end_date);
	} else { $row->end_date = ''; }

	$row->parent_id = $parent_id;

	//$row->ordering = $doc_order;
	if ($row->id) {
		unset($row->owner_id);
		unset($row->ordering);
	} else {
		$row->owner_id = $user->get('id');
		$doc_order = 0;
		if (isset($ordering[$parent_id])) {
			$doc_order = $ordering[$parent_id];
			$ordering[$parent_id] ++;
		} else {
			$ordering[$parent_id] = 1;//ordering for next element of this parent will be = 1 (the current element ordering = 0 )
		}
		$row->ordering = $doc_order;
	}
	$row->doc_description = stripslashes($row->doc_description);
	$row->doc_description = JLMS_ProcessText_LowFilter($row->doc_description);
	
	if (!$row->check()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	if (!$row->store()) {
		echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
		exit();
	}
	
	if($folder_flag || $file_id) {
		// start-end publishing
		$publish_start = intval(mosGetParam($_REQUEST, 'is_start', 0));
		$publish_end = intval(mosGetParam($_REQUEST, 'is_end', 0));
		$row->publish_start = $publish_start;
		$row->publish_end = $publish_end;
		if ($row->publish_start) {
			$row->start_date = mosGetParam($_REQUEST, 'start_date', '');
			$row->start_date = JLMS_dateToDB($row->start_date);
		} else { $row->start_date = ''; }
		if ($row->publish_end) {
			$row->end_date = mosGetParam($_REQUEST, 'end_date', '');
			$row->end_date = JLMS_dateToDB($row->end_date);
		} else { $row->end_date = ''; }
	
		$row->parent_id = $parent_id;
	
		//$row->ordering = $doc_order;
		if ($row->id) {
			unset($row->owner_id);
		} else {
			$row->owner_id = $my->id;
		}
	
		// 26.02.2007 (Media content integration)
		$row->doc_description = JLMS_ProcessText_LowFilter($row->doc_description);
		/*$iFilter = new JLMS_InputFilter(null,null,1,1);
		$row->doc_description = $iFilter->process( $row->doc_description );*/
		$row->folder_flag = $folder_flag; 
		
		if (!$row->check()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if (!$row->store()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
	
		return $row->id;
	}
	return false;
}

/* batch uploads */
function JLMS_uploadFile_from_zip($path, $name) {
	global $JLMS_DB;
	
	$is_supported_type = 1;
	$base_Dir = _JOOMLMS_DOC_FOLDER;
	$userfile_name = $name;

	$good_ext = false;
	$file_ext = '';
	if (strcmp(substr($userfile_name,-4,1),".") === 0) {
		$good_ext = true;
		$file_ext = substr($userfile_name,-3);
	}
	
	if (!$good_ext) {
		if (strcasecmp(substr($userfile_name,-5,1),".") === 0) {
			$good_ext = true;
			$file_ext = substr($userfile_name,-4);
		}
	}
	
	if (!$good_ext) {
		if (strcasecmp(substr($userfile_name,-6,1),".") === 0) {
			$good_ext = true;
			$file_ext = substr($userfile_name,-5);
		}
	}		
	
	if (!$good_ext) {
		if (strcasecmp(substr($userfile_name,-3,1),".") === 0) {
			$good_ext = true;
			$file_ext = substr($userfile_name,-2);
		}
	}
	if (!$good_ext) {
		mosErrorAlert(_JLMS_EM_BAD_FILEEXT);
	}
	$query = "SELECT LOWER(filetype) as filetype FROM #__lms_file_types";
	$JLMS_DB->SetQuery( $query );
	$ftypes = JLMSDatabaseHelper::LoadResultArray();
	
	if (empty($ftypes) || !in_array(strtolower($file_ext), $ftypes)) {
		//mosErrorAlert(_JLMS_EM_BAD_FILEEXT);
		$is_supported_type = 0;
	}
	
	if($is_supported_type) {
		$file_unique_name = str_pad('0',4,'0',STR_PAD_LEFT) . '_' . md5(uniqid(rand(), true)) . '.' . $file_ext;
	
		if (!copy ($path."/".$name,$base_Dir.$file_unique_name) || !mosChmod($base_Dir.$file_unique_name)) {
			mosErrorAlert("Upload of ".$userfile_name." failed");
		} else {
			global $JLMS_DB, $my;
			$userfile_name = JLMSDatabaseHelper::GetEscaped($userfile_name);
			$query = "INSERT INTO #__lms_files (file_name, file_srv_name, owner_id)"
			. "\n VALUES ('".$userfile_name."', '".$file_unique_name."', '".$my->id."')";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			return $JLMS_DB->insertid();
		}
	}
	else {
		return false;
	}
}

// (27.10) new algorithm to publish/unpublish documents
// How it's work:
//		1. if documents publishing - all parent folders publish
//		2. if folder publishing - p.1 + all child documents publish
//		3. if folder unpublishing - all child documents unpublish
function JLMSO_changeDoc( $option) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$open_folder = 0;
	$open_element = 0;
	//if ( $course_id && $JLMS_ACL->CheckPermissions('docs', 'publish') ) {
		$state = intval(mosGetParam($_REQUEST, 'state', 0));
		if ($state != 1) { $state = 0; }
		$cid = mosGetParam( $_REQUEST, 'cid', array(0) );
		$cid2 = intval(mosGetParam( $_REQUEST, 'cid2', 0 ));
		if ($cid2) {
			$cid = array();
			$cid[] = $cid2;
		}
		if (!is_array( $cid )) {
			$cid = array(0);
		} 
		if (!is_array( $cid ) || count( $cid ) < 1) {
			$action = 1 ? 'Publish' : 'Unpublish';
			echo "<script> alert('Select an item to $action'); window.history.go(-1);</script>\n";
			exit();
		}
		if ($state == 1) {
			$cids1 = implode( ',', $cid );
			$query = "SELECT parent_id FROM #__lms_outer_documents WHERE  id IN ( $cids1 ) AND parent_id <> '0'";
			$JLMS_DB->SetQuery( $query );
			$parent_ids = JLMSDatabaseHelper::LoadResultArray();
			if (count($cid) == 1 && isset($cid[0])) {
				$open_element = $cid[0];
				if (count($parent_ids) && isset($parent_ids[0])) {
					$open_folder = $parent_ids[0];
				}
			}
			if (count($parent_ids)) {
				$do_change_state = true;
				while ($do_change_state) {
					$parentids = implode( ',', $parent_ids );
					$query = "SELECT parent_id FROM #__lms_outer_documents WHERE id IN ( $parentids ) AND parent_id <> '0' AND published = '0'";
					$JLMS_DB->SetQuery( $query );
					$parent_ids2 = JLMSDatabaseHelper::LoadResultArray();
					$query = "UPDATE #__lms_outer_documents"
					. "\n SET published = $state"
					. "\n WHERE id IN ( $parentids )"
					;
					$JLMS_DB->setQuery( $query );
					if (!$JLMS_DB->query()) { echo "<script> alert('".$JLMS_DB->getErrorMsg()."'); window.history.go(-1); </script>\n"; exit(); }
					if (count($parent_ids2)) {
						$do_change_state = true;
						$parent_ids = $parent_ids2;
					} else {
						$do_change_state = false;
					}
				}
			}
		} else {
			if (count($cid) == 1 && isset($cid[0])) {
				$open_element = intval($cid[0]);
				$query = "SELECT parent_id FROM #__lms_outer_documents WHERE id = $open_element";
				$JLMS_DB->SetQuery( $query );
				$parent_id = $JLMS_DB->LoadResult();
				if ($parent_id) {
					$open_folder = $parent_id;
				}
			}
		}
		$do_change_state = true;
		$unstate = abs($state - 1);
		while ($do_change_state) {
			$cids = implode( ',', $cid );
			$query = "SELECT id FROM #__lms_outer_documents WHERE parent_id IN ( $cids ) AND ( ((folder_flag = 0 OR folder_flag = 2 OR folder_flag = 3) AND published = $unstate) OR (folder_flag = 1) )";
			$JLMS_DB->SetQuery( $query );
			$cid2 = JLMSDatabaseHelper::LoadResultArray();
			$query = "UPDATE #__lms_outer_documents"
			. "\n SET published = $state"
			. "\n WHERE id IN ( $cids ) "
			;
			$JLMS_DB->setQuery( $query );
			if (!$JLMS_DB->query()) {
				echo "<script> alert('".$JLMS_DB->getErrorMsg()."'); window.history.go(-1); </script>\n";
				exit();
			}
			if (count($cid2)) {
				$do_change_state = true;
				$cid = $cid2;
			} else {
				$do_change_state = false;
			}
		}
	//}
	$not_sef_link = "index.php?option=$option&Itemid=$Itemid&task=outdocs";
	if ($open_folder) {
		$not_sef_link .= "&folder=$open_folder";	
	}
	if ($open_element) {
		$not_sef_link .= "&element=$open_element";	
	}
	JLMSRedirect( sefRelToAbs($not_sef_link) );
}


// to do: udalenie failov i papok (HARD)
// fail udalyat' tol'ko togda kogda on ne ispol'zuetsa v 'learn_path' (esli budut 'public' files to proveryat' i shoby on drugimi ne ispol'zovalsya
// papki udalyat' tol'ko kogda ne ostaetsa ni odnogo vlogennogo elementa na vsex urovnyax vlogennosti
function JLMSO_doDeleteDocuments( $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$is_teacher = $JLMS_ACL->isTeacher();
	$open_element = 0;
	$open_folder = 0;
	if ($is_teacher) {
	//if ( $course_id && $JLMS_ACL->CheckPermissions('docs', 'manage') ) {
		$cid1 = mosGetParam( $_POST, 'cid', array(0) );
		$force_delete = intval(mosGetParam( $_POST, 'force_delete', 0 ));
		$proceed_to_force_delete = false;
		$do_proceed = true;
		if (!is_array( $cid1 )) {
			$do_proceed = false;
		}
		$cid = array();
		foreach ($cid1 as $cid1_one) {
			$cid_tmp = intval($cid1_one);
			if ($cid_tmp) {
				$cid[] = $cid_tmp;
			}
		}
		if (empty($cid)) {
			$do_proceed = false;
		}
		if ($do_proceed) {
			if (!$proceed_to_force_delete) {
				if (count($cid) == 1 && isset($cid[0])) {
					$open_element = $cid[0];
					$query = "SELECT parent_id FROM #__lms_outer_documents WHERE id = $open_element";
					$JLMS_DB->SetQuery( $query );
					$parent_id = $JLMS_DB->LoadResult();
					if ($parent_id) {
						$open_folder = $parent_id;
					}
				}
			}
			$cids = implode( ',', $cid );
			$all_childs = array();
			$do_find_childs = true;
			$parents = $cid;
			while($do_find_childs) {
				$parents_str = implode( ',', $parents );
				if ($JLMS_ACL->CheckPermissions('library', 'only_own_items')) {
					$user = JLMSFactory::getUser();
					$query = "SELECT id FROM #__lms_outer_documents WHERE parent_id IN ( $parents_str ) AND owner_id=".$user->get('id');
				} else {
					$query = "SELECT id FROM #__lms_outer_documents WHERE parent_id IN ( $parents_str )";
				}
				$JLMS_DB->SetQuery( $query );
				$childs = JLMSDatabaseHelper::LoadResultArray();
				if (count($childs)) {
					$all_childs = array_merge($all_childs, $childs);
					$parents = $childs;
					$do_find_childs = true;
				} else {
					$do_find_childs = false;
				}
			}
			$all_docs = array_merge($all_childs, $cid);
			$all_docs_ids = implode( ',', $all_docs );

			$cid_used_docs = array();
			$cid_used_lpaths = array();
			$cid_used = array();
			$linked_rows_del = array();
			$course_names = array();
			if ($force_delete) {
				//remove all docs and all linked resources
				JLMSO_delete_linked_docs($all_docs);
				JLMSO_delete_linked_scorms($all_docs);
			} else {
				$query = "SELECT course_id, file_id as lib_id FROM #__lms_documents WHERE file_id IN ( $all_docs_ids ) AND folder_flag = 3";
				$JLMS_DB->SetQuery( $query );
				$cid_used_docs = $JLMS_DB->LoadObjectList();
	
				$query = "SELECT course_id, lib_id FROM #__lms_n_scorm_lib WHERE lib_id IN ( $all_docs_ids )";
				$JLMS_DB->SetQuery( $query );
				$cid_used_lpaths = $JLMS_DB->LoadObjectList();
				if (count($cid_used_docs)) {
					foreach ($cid_used_docs as $cid_used_doc) {
						$cid_used[] = $cid_used_doc->lib_id;
					}
				}
				if (count($cid_used_lpaths)) {
					foreach ($cid_used_lpaths as $cid_used_lpath) {
						$cid_used[] = $cid_used_lpath->lib_id;
					}
				}
				if (count($cid_used)) {
					$proceed_to_force_delete = true;
					$cid_used_str = implode(',', $cid_used);
					$query = "SELECT a.*, b.file_name FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0 WHERE a.id IN ($cid_used_str) ORDER BY a.doc_name";
					$JLMS_DB->SetQuery( $query );
					$linked_rows_del = $JLMS_DB->LoadObjectList();
                    
					$linked_rows_del = AppendFileIcons_toList( $linked_rows_del );
					for($i=0;$i<count($linked_rows_del);$i++) {
						if($linked_rows_del[$i]->folder_flag == 3) {
							$linked_rows_del[$i]->file_icon = 'tlb_scorm';
						}
					}
					$course_ids = array();
					for ($i = 0, $n = count($linked_rows_del); $i < $n; $i++) {
						$linked_rows_del[$i]->course_ids = array();
						foreach ($cid_used_docs as $cid_used_doc) {
							if ($linked_rows_del[$i]->id == $cid_used_doc->lib_id) {
								$linked_rows_del[$i]->course_ids[] = $cid_used_doc->course_id;
								if (!in_array($cid_used_doc->course_id, $course_ids)) {
									$course_ids[] = $cid_used_doc->course_id;
								}
							}
						}
						foreach ($cid_used_lpaths as $cid_used_lpath) {
							if ($linked_rows_del[$i]->id == $cid_used_lpath->lib_id) {
								$linked_rows_del[$i]->course_ids[] = $cid_used_lpath->course_id;
								if (!in_array($cid_used_lpath->course_id, $course_ids)) {
									$course_ids[] = $cid_used_lpath->course_id;
								}
							}
						}
					}
					if (count($course_ids)) {
						$course_ids_str = implode(',', $course_ids);
						$query = "SELECT id, course_name FROM #__lms_courses WHERE id IN ($course_ids_str)";
						$JLMS_DB->SetQuery( $query );
						$course_names = $JLMS_DB->LoadObjectList('id');
					}
				}
			}

			$cid_new = array_diff($all_docs, $cid_used); // id's for deleting
			
			if (count($cid_new)) {
				//if there are 'not-used by courses' resources among selected for removal
			
			$cids_new = implode( ',', $cid_new );
			// deleting
			// 1. delete all files (folder_flag == 0)
			$query = "SELECT id, file_id FROM #__lms_outer_documents WHERE folder_flag = 0 AND id IN ( $cids_new )";
			$JLMS_DB->SetQuery( $query );
			$files_deleted = $JLMS_DB->LoadObjectList();
			$lms_docs_ids = array();
			if (count($files_deleted)) { //now delete files
				$lms_files_ids = array();
				foreach ($files_deleted as $file_del) {
					$lms_files_ids[] = $file_del->file_id;
					$lms_docs_ids[] = $file_del->id;
				}
				JLMS_deleteFiles($lms_files_ids);
				$lms_docs_ids_str = implode( ',', $lms_docs_ids );
				$query = "DELETE FROM #__lms_outer_documents WHERE id IN ( $lms_docs_ids_str )";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
			}
			// 1b. delete all ZIP-packages (folder_flag == 2)
			$query = "SELECT id, file_id FROM #__lms_outer_documents WHERE folder_flag = 2 AND id IN ( $cids_new )";
			$JLMS_DB->SetQuery( $query );
			$zips_deleted = $JLMS_DB->LoadObjectList();
			$lms_docs_ids = array();
			if (count($zips_deleted)) { //now delete zips
				$lms_zips_ids = array();
				foreach ($zips_deleted as $zip_del) {
					$lms_zips_ids[] = $zip_del->file_id;
					$lms_docs_ids[] = $zip_del->id;
				}
				$real_del_zip_ids = & JLMS_deleteZipPacks( $course_id, $lms_zips_ids);
				$lms_docs_ids = array();
				foreach ($real_del_zip_ids as $rdzi) {
					foreach($zips_deleted as $zd) {
						if ($zd->file_id == $rdzi) {
							$lms_docs_ids[] = $zd->id;
						}
					}
				}
				if (count($lms_docs_ids)) {
					$lms_docs_ids_str = implode( ',', $lms_docs_ids );
					$query = "DELETE FROM #__lms_outer_documents WHERE id IN ( $lms_docs_ids_str )";
					$JLMS_DB->SetQuery( $query );
					$JLMS_DB->query();
				}
			}

			// 1c. delete all SCORMS (folder_flag == 3)
			$query = "SELECT id, file_id FROM #__lms_outer_documents WHERE folder_flag = 3 AND id IN ( $cids_new )";
			$JLMS_DB->SetQuery( $query );
			$scn_lib_deleted = $JLMS_DB->LoadObjectList();
			$lms_docs_ids = array();
			if (count($scn_lib_deleted)) {
				$lms_scn_ids = array();
				foreach ($scn_lib_deleted as $scn_lib_del) {
					$lms_scn_ids[] = $scn_lib_del->file_id;
					$lms_docs_ids[] = $scn_lib_del->id;
				}

				if (count($lms_scn_ids)) {
					$scn_cids = implode(',',$lms_scn_ids);
					$query = "SELECT id, scorm_package FROM #__lms_n_scorm WHERE id IN ($scn_cids)";
					$JLMS_DB->SetQuery( $query );
					$scn_r_cid = $JLMS_DB->LoadObjectList();

					if (count($scn_r_cid)) {
						$scn_cid = array();
						$sc_cid = array();
						foreach ($scn_r_cid as $irc) {
							$scn_cid[] = $irc->id;
							$sc_cid[] = $irc->scorm_package;
						}
						if (count($scn_cid)) {
							$scn_cids = implode(',',$scn_cid);
							$query = "DELETE FROM #__lms_n_scorm WHERE id IN ($scn_cids)";
							$JLMS_DB->SetQuery( $query );
							$JLMS_DB->query();
							$query = "SELECT id FROM #__lms_n_scorm_scoes WHERE scorm IN ($scn_cids)";
							$JLMS_DB->SetQuery( $query );
							$scoes_ids = JLMSDatabaseHelper::LoadResultArray();
							if (count($scoes_ids)) {
								$query = "DELETE FROM #__lms_n_scorm_scoes WHERE scorm IN ($scn_cids)";
								$JLMS_DB->SetQuery( $query );
								$JLMS_DB->query();
								$sco_cids = implode(',',$scoes_ids);
								$query = "DELETE FROM #__lms_n_scorm_scoes_data WHERE scoid IN ($sco_cids)";
								$JLMS_DB->SetQuery( $query );
								$JLMS_DB->query();
								$query = "DELETE FROM #__lms_n_scorm_scoes_track WHERE scoid IN ($sco_cids)";
								$JLMS_DB->SetQuery( $query );
								$JLMS_DB->query();
								$query = "DELETE FROM #__lms_n_scorm_seq_mapinfo WHERE scoid IN ($sco_cids)";
								$JLMS_DB->SetQuery( $query );
								$JLMS_DB->query();
								$query = "DELETE FROM #__lms_n_scorm_seq_objective WHERE scoid IN ($sco_cids)";
								$JLMS_DB->SetQuery( $query );
								$JLMS_DB->query();
								$query = "DELETE FROM #__lms_n_scorm_seq_rolluprulecond WHERE scoid IN ($sco_cids)";
								$JLMS_DB->SetQuery( $query );
								$JLMS_DB->query();
								$query = "DELETE FROM #__lms_n_scorm_seq_rulecond WHERE scoid IN ($sco_cids)";
								$JLMS_DB->SetQuery( $query );
								$JLMS_DB->query();
								$query = "DELETE FROM #__lms_n_scorm_seq_ruleconds WHERE scoid IN ($sco_cids)";
								$JLMS_DB->SetQuery( $query );
								$JLMS_DB->query();
							}
						}
						if (count($sc_cid)) {
							$sc_cids = implode(',',$sc_cid);
							$query = "SELECT * FROM #__lms_scorm_packages WHERE id IN ($sc_cids)";
							$JLMS_DB->SetQuery( $query );
							$del_scorms = $JLMS_DB->LoadObjectList();
							if (count($del_scorms)) {
								$scorm_ids = array();
								require_once(_JOOMLMS_FRONT_HOME . "/includes/jlms_dir_operation.php");
								$scorm_folder = JPATH_SITE . "/" . _JOOMLMS_SCORM_FOLDER . "/";
								foreach ($del_scorms as $del_scorm) {
									deldir( $scorm_folder . $del_scorm->folder_srv_name . "/" );
									unlink( $scorm_folder . $del_scorm->package_srv_name );
									$scorm_ids[] = $del_scorm->id;
								}
								$sc_cids = implode(',',$scorm_ids);
								$query = "DELETE FROM #__lms_scorm_packages WHERE id IN ($sc_cids)";
								$JLMS_DB->SetQuery( $query );
								$JLMS_DB->query();
								$query = "DELETE FROM #__lms_scorm_sco WHERE content_id IN ($sc_cids)";
								$JLMS_DB->SetQuery( $query );
								$JLMS_DB->query();
							}
						}
					}
				}
				if (count($lms_docs_ids)) {
					$lms_docs_ids_str = implode( ',', $lms_docs_ids );
					$query = "DELETE FROM #__lms_outer_documents WHERE id IN ( $lms_docs_ids_str )";
					$JLMS_DB->SetQuery( $query );
					$JLMS_DB->query();
					$query = "DELETE FROM #__lms_n_scorm_lib WHERE lib_id IN ( $lms_docs_ids_str )";
					$JLMS_DB->SetQuery( $query );
					$JLMS_DB->query();
				}
			}

			// files and documents deleted
			// 2. delete folders (only if empty)
			$lms_folders_ids = array_diff($cid_new, $lms_docs_ids);
			if (count($lms_folders_ids)) {
				$query = "SELECT id, parent_id, folder_flag FROM #__lms_outer_documents";
				$JLMS_DB->SetQuery( $query );
				$course_docs_data = $JLMS_DB->LoadObjectList();
				if (count($course_docs_data)) {
					$folders_for_del = array();
					foreach($lms_folders_ids as $lms_folder_id) {
						$i = 0;
						while ($i < count($course_docs_data)) {
							if ($course_docs_data[$i]->id == $lms_folder_id) {
								$for_delete = JLMSO_isfolderempty($course_docs_data, $lms_folder_id);
								if ($for_delete) {
									$folders_for_del[] = $lms_folder_id;
								}
							}
							$i ++;
						}
					}
					if (count($folders_for_del)) {
						$folders_for_del_str = implode( ',', $folders_for_del );
						$query = "DELETE FROM #__lms_outer_documents WHERE id IN ( $folders_for_del_str ) AND folder_flag = 1";
						$JLMS_DB->SetQuery( $query );
						$JLMS_DB->query();
					}
				}
			}
			} // end of "if (count($cid_new)) {"
			if ($proceed_to_force_delete) {
				JLMS_outdocs_html::confirm_delLinkedResources( $linked_rows_del, $all_docs, $course_names, $option );
			}
		}
	}
	if (!$proceed_to_force_delete) {
		$not_sef_link = "index.php?option=$option&Itemid=$Itemid&task=outdocs";
		if ($open_folder) {
			$not_sef_link .= "&folder=$open_folder";	
		}
		if ($open_element) {
			$not_sef_link .= "&element=$open_element";	
		}
		JLMSRedirect( sefRelToAbs($not_sef_link) );
	}
}
// (27.10) only for private use ( used in "JLMS_doDeleteDocuments()")
// return 'true' if folder is EMPTY (or if folder contains only EMPTY subfolders, subsubfolders..)
// (recurse function)
function JLMSO_isfolderempty($elements, $id) {
	$is_empty = true;
	$j = 0;
	while ($j < count($elements)) {
		if ($elements[$j]->parent_id == $id) {//est' child
			if ($elements[$j]->folder_flag == 1) { //child - folder (nugno uznat' shto eta folder - pustaya
				$is_empty = JLMSO_isfolderempty($elements, $elements[$j]->id);
			} else {// child - document
				$is_empty = false;
			}
		}
		$j ++;
	}
	return $is_empty;
}

function JLMSO_delete_linked_docs($lib_ids) {
	global $JLMS_DB;

	$all_lib_ids = implode(',', $lib_ids);
	$query = "SELECT id FROM #__lms_documents WHERE file_id IN ( $all_lib_ids ) AND folder_flag = 3";
	$JLMS_DB->SetQuery( $query );
	$all_docs = JLMSDatabaseHelper::LoadResultArray();
	$all_docs_ids = implode( ',', $all_docs );

	$query = "SELECT id FROM #__lms_learn_path_steps WHERE item_id IN ( $all_docs_ids ) AND step_type = 2";
	$JLMS_DB->SetQuery( $query );
	$lpath_steps_with_docs = JLMSDatabaseHelper::LoadResultArray();
	if (count($lpath_steps_with_docs)) {
		$s_cids  = implode(',', $lpath_steps_with_docs);
		$query = "DELETE FROM #__lms_learn_path_steps WHERE id IN ($s_cids)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$query = "DELETE FROM #__lms_learn_path_step_results WHERE step_id IN ($s_cids)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$query = "DELETE FROM #__lms_learn_path_conds WHERE step_id IN ($s_cids)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
	}

	$query = "DELETE FROM #__lms_documents WHERE id IN ( $all_docs_ids )";
	$JLMS_DB->SetQuery( $query );
	$JLMS_DB->query();

	$query = "DELETE FROM #__lms_topic_items WHERE item_id IN ($all_docs_ids) AND item_type = 2";
	$JLMS_DB->SetQuery( $query );
	$JLMS_DB->query();
}

function JLMSO_delete_linked_scorms($lib_ids) {
	global $JLMS_DB;
	$all_lib_ids = implode(',', $lib_ids);
	$query = "SELECT lpath_id FROM #__lms_n_scorm_lib WHERE lib_id IN ( $all_lib_ids )";
	$JLMS_DB->SetQuery( $query );
	$all_lpaths = JLMSDatabaseHelper::LoadResultArray();
	if (count ($all_lpaths)) {
		$cids_pre = implode(',', $all_lpaths);
		$query = "SELECT id, item_id FROM #__lms_learn_paths WHERE id IN ($cids_pre) AND lp_type = 2 AND item_id <> 0";
		$JLMS_DB->SetQuery( $query );
		$i_cid = $JLMS_DB->LoadObjectList();
		if (count($i_cid)) {
			$lpath_ids = array();
			$lpath_items = array();
			foreach ($i_cid as $i_cid_one) {
				$lpath_ids[] = $i_cid_one->id;
				$lpath_items[] = $i_cid_one->item_id;
			}
			$lpath_ids_str = implode(',',$lpath_ids);
			$query = "DELETE FROM #__lms_topic_items WHERE item_id IN ($lpath_ids_str) AND item_type = 7";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();

			$query = "DELETE FROM #__lms_learn_paths WHERE id IN ($lpath_ids_str)";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();

			$query = "DELETE FROM #__lms_learn_path_prerequisites WHERE lpath_id IN ($lpath_ids_str) OR req_id IN ($lpath_ids_str)";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();

			$lpath_items_str = implode(',',$lpath_items);
			$query = "DELETE FROM #__lms_n_scorm WHERE id IN ($lpath_items_str)";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			$query = "SELECT id FROM #__lms_n_scorm_scoes WHERE scorm IN ($lpath_items_str)";
			$JLMS_DB->SetQuery( $query );
			$scoes_ids = JLMSDatabaseHelper::LoadResultArray();
			if (count($scoes_ids)) {
				$query = "DELETE FROM #__lms_n_scorm_scoes WHERE scorm IN ($lpath_items_str)";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$sco_cids = implode(',',$scoes_ids);
				$query = "DELETE FROM #__lms_n_scorm_scoes_data WHERE scoid IN ($sco_cids)";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$query = "DELETE FROM #__lms_n_scorm_scoes_track WHERE scoid IN ($sco_cids)";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$query = "DELETE FROM #__lms_n_scorm_seq_mapinfo WHERE scoid IN ($sco_cids)";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$query = "DELETE FROM #__lms_n_scorm_seq_objective WHERE scoid IN ($sco_cids)";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$query = "DELETE FROM #__lms_n_scorm_seq_rolluprulecond WHERE scoid IN ($sco_cids)";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$query = "DELETE FROM #__lms_n_scorm_seq_rulecond WHERE scoid IN ($sco_cids)";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$query = "DELETE FROM #__lms_n_scorm_seq_ruleconds WHERE scoid IN ($sco_cids)";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
			}
		}
	}
}

function Create_OutFoldersList($def_value, $exclude_id = null) {
	global $JLMS_DB;
	$user = JLMSFactory::getUser();
	$JLMS_ACL = JLMSFactory::getACL();

	$c_folders = array();
	//$c_folders[] = mosHTML::makeOption(0, _JLMS_SB_COURSE_FOLDER);
	$blank_element = new stdClass();$blank_element->value = 0; $blank_element->doc_name_tree = _JLMS_SB_COURSE_FOLDER;
	$c_folders[] = $blank_element;

	$is_teacher = $JLMS_ACL->isTeacher();
	$can_do_everything = $JLMS_ACL->CheckPermissions('library', 'only_own_items') ? false : ( $is_teacher ? true : false);
	//$id = $JLMS_CONFIG->get('course_id');
	//if ( $id && $JLMS_ACL->CheckPermissions('docs', 'view') ) {

		$query = "SELECT a.id as value, a.doc_name as text, a.parent_id, a.doc_name, a.id, a.folder_flag, a.ordering"
		. "\n FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 1 LEFT JOIN #__users as c ON a.owner_id = c.id"
		. "\n WHERE  a.folder_flag = 1 AND ((a.published = 1) "
		. "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )"
		. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d')."')) OR (a.publish_end = 0) )"
		. (($is_teacher) ? "\n AND ( a.outdoc_share!=0 ))" : "\n AND ( a.outdoc_share=2 ))")
		. "\n OR (a.owner_id = ".$user->get('id')." AND a.folder_flag = 1)".($can_do_everything ? ' OR a.folder_flag = 1' : '')
		. "\n ORDER BY a.parent_id, a.ordering, a.doc_name, c.username";
		
	$JLMS_DB->SetQuery( $query );
	$rows = $JLMS_DB->LoadObjectList();
	$rows = JLMS_GetTreeStructure( $rows );
	$ex_ids = array();
	if ($exclude_id) {
		$ex_ids[] = $exclude_id;
		$all_rows = array();
		$all_rows = jlms_tree_struct_main($rows, $all_rows,$exclude_id);
		foreach ($all_rows as $ei) {
			$ex_ids[] = $ei->id;
		}
	}
	$rows_new = array();
	$i = 0;
	while ($i < count($rows)) {
		if (!in_array($rows[$i]->id, $ex_ids)) {
			$rows_new[] = $rows[$i];
		}
		$i ++;
	}

	$i = 0;
	$c_folders = array_merge($c_folders, $rows_new);

	return mosHTML::selectList( $c_folders, 'course_folder', 'class="inputbox" size="1" style="width:275px;"', 'value', 'doc_name_tree', $def_value );
}
