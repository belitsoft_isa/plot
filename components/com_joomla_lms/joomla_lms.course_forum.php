<?php
/**
* joomla_lms.course_forum.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.course_forum.html.php");
$task 	= mosGetParam( $_REQUEST, 'task', '' );

if ($task != 'login_to_forum') {
	global $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id',0);
	$pathway = array();
	$pathway[] = array('name' => _JLMS_PATHWAY_HOME, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"), 'is_home' => true);
	$pathway[] = array('name' => $JLMS_CONFIG->get('course_name'), 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$course_id"), 'is_course' => true);
	$pathway[] = array('name' => _JLMS_TOOLBAR_FORUM, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=course_forum&amp;id=$course_id"));
	JLMSAppendPathWay($pathway);
	JLMS_ShowHeading();
}

$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) );

switch ($task) {
	case 'course_forum':	
		if( !JLMS_show_course_forum( $id, $option ) ) 
		{
			JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$id"));
		}
	break;			
	case 'login_to_forum':	JLMS_forum_SetSMFCookie( $id, $option );break;
}
function JLMS_forum_SetSMFCookie( $id, $option ) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
		
	if ($JLMS_CONFIG->get('plugin_forum')) {
		$query = "SELECT add_forum FROM #__lms_courses WHERE id = $id";
		$JLMS_DB->SetQuery( $query );
		$is_c_forum = $JLMS_DB->LoadResult();
		
		if ($is_c_forum) {			
			$username = strval(mosGetParam ($_REQUEST,'username',''));
			$password = strval(mosGetParam ($_REQUEST,'passwd',''));

			$query = "SELECT id, name, username, password, block"
			. "\n FROM #__users"
			. "\n WHERE username = ". $JLMS_DB->Quote( $username ) . " AND id = $my->id";
			;
			$JLMS_DB->setQuery( $query );
			$row = $JLMS_DB->loadObject();
			$is_loaded_user = false;

			if (is_object($row)) {
				if ( JLMS_J32version() ) {
					$match = JUserHelper::verifyPassword($password, $row->password, $row->id);
				} else {
					$parts	= explode( ':', $row->password );
					$crypt	= $parts[0];
					$salt	= @$parts[1];
					
					$testcrypt = JLMS_getCryptedPassword($password, $salt);
					$match = ($crypt == $testcrypt);
				}
				
				if ($match) {
					$query = "SELECT * FROM `#__users` WHERE id = $row->id";
					$JLMS_DB->setQuery($query);
					$user = $JLMS_DB->loadObject();
					$is_loaded_user = true;
				}
			}
							
			if ($is_loaded_user && isset($user->id)){												
				JLMS_createForumUser( $user, $password );				
			}

			JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=course_forum&id=$id"));
		} else {
			JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$id"));
		}
	} else {
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$id"));
	}
}

function JLMS_show_course_forum( $id, $option ) 
{
	/*
	board_type = 1 - Public/Group course board
	board_type = 2 - LearningPath public course board
	board_type = 3 - Board for users from 'Global group'
	board_type = 4 - Private (teachers-only) boards
	board_type = 5 - Private (teachers-only) LearningPath boards
	*/
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;

	$usertype = JLMS_GetUserType($my->id, $id);
	if (!$usertype) {
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$id"));
	}

	$JLMS_ACL = JLMSFactory::getACL();
	
	$forum = & JLMS_SMF::getInstance();
	
	if( !$forum )
		return false;
			
	if( ($res = $forum->applyChanges( $id )) === false ) 
	{		
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$id"));
	}
	
	JLMS_course_forum_html::wrapper_course_forum( $res['link'], $option, $id, $res['msg'] );
	
	return true; 
}

function JLMS_createForumUser( $user, $password ) 
{
	global $my, $JLMS_DB, $JLMS_CONFIG;
	
	$forum = & JLMS_SMF::getInstance();
	
	$error = '';
	
	$query = "SELECT distinct a.ID_GROUP AS id_group FROM `#__lms_forum_details` as a, `#__lms_users_in_groups` as b WHERE a.course_id = b.course_id AND (b.group_id = a.group_id OR a.group_id = '0') AND b.user_id='".$my->id."' AND a.is_active = 1";
	$JLMS_DB->setQuery($query);
	$forum_groups_stu = JLMSDatabaseHelper::loadResultArray();
	$error = $JLMS_DB->getErrorMsg()."\n";

	$query = "SELECT distinct a.ID_GROUP AS id_group FROM `#__lms_forum_details` as a, `#__lms_user_courses` as b WHERE a.course_id = b.course_id AND b.user_id='".$my->id."' AND a.is_active = 1";
	$JLMS_DB->setQuery($query);
	$forum_groups_tea = JLMSDatabaseHelper::loadResultArray();
	$error .= $JLMS_DB->getErrorMsg()."\n";

	$query = "SELECT distinct a.ID_BOARD AS id_board FROM `#__lms_forum_details` as a, `#__lms_user_courses` as b WHERE a.course_id = b.course_id AND b.user_id='".$my->id."' AND a.is_active = 1";
	$JLMS_DB->setQuery($query);
	$forum_boards_tea = JLMSDatabaseHelper::loadResultArray();
	$error .= $JLMS_DB->getErrorMsg()."\n";

	$forum_groups = array_unique(array_merge($forum_groups_stu, $forum_groups_tea));
	$cb_info = array();	
	if ($JLMS_CONFIG->get('is_cb_installed')) 
	{
		$fields = array ('website', 'ICQ', 'AIM', 'YIM','MSN', 'location');
		$fields_isset = array();
		foreach ($fields as $field) {
			if ($JLMS_CONFIG->get('jlms_cb_'.$field)) {
				$fields_isset[] = $JLMS_CONFIG->get('jlms_cb_'.$field);
			}
		}
		if (!empty($fields_isset)) {
			$fields_str = implode(',', $fields_isset );
			$query = "SELECT name FROM `#__comprofiler_fields` WHERE fieldid IN ($fields_str) ";
			$JLMS_DB->setQuery($query);
			$field_name = JLMSDatabaseHelper::loadResultArray();
			$error .= $JLMS_DB->getErrorMsg()."\n";
			$field_names = implode(',', $field_name);

			$query = "SELECT ".$field_names." FROM `#__comprofiler` WHERE user_id=".$user->id;
			$JLMS_DB->setQuery($query);
			$cb_user = JLMSDatabaseHelper::loadResultArray();
			if ( isset($cb_user[0]) ) {
				$cb_info = array_values( $cb_user );
			}
			$error .= $JLMS_DB->getErrorMsg()."\n";
		}
	}

	$forum_groups = array_unique(array_merge($forum_groups_stu, $forum_groups_tea));

	if (count($forum_groups)){
		$groups = implode(',', $forum_groups);
	} else {
		$groups = '';
		$forum_groups = array();
	}
	$query = "SELECT distinct ID_GROUP AS id_group FROM `#__lms_forum_details` ";
	$JLMS_DB->setQuery($query);
	$all_lms_groups = JLMSDatabaseHelper::loadResultArray();
	$error .= $JLMS_DB->getErrorMsg()."\n";
	
	$smf_user = $forum->loadMemberByName( $my->username );
		
	$primary_group = 0;	
		
	if (is_object( $smf_user )){
						
		$mem_id = $smf_user->id_member;
		$primary_group = $smf_user->id_group;
		$old_groups = explode(',', $smf_user->additional_groups);
		$old_group_s = array();
		foreach($old_groups as $group){
			if (!in_array($group, $all_lms_groups)){
				$old_group_s[] = $group;
			}
		}
		$forum_groups = array_unique(array_merge($forum_groups, $old_group_s));
		$groups = implode(',', $forum_groups);				
		
		$smf_password = sha1(strtolower($my->username) .$password);
		$storeData = array();
		$storeData['id_member'] = $mem_id;
		$storeData['passwd'] = $smf_password;
		$storeData['id_group'] = $primary_group;
		$storeData['additional_groups'] = $groups;
		$storeData = array_merge( $storeData, $cb_info );
				 		
		$forum->storeMember( $storeData );				 	
				
	} else {						
		$mem_id = $forum->registerOnForum( $user, $password, $groups, $cb_info);
	}

	$forum->setLoginCookie15( $mem_id, $password );
}
?>