<?php
/** 
* joomla_lms.courses.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) );
$task 	= mosGetParam( $_REQUEST, 'task', '' );
require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.courses.html.php");
if (isset($JLMS_CONFIG) && is_object($JLMS_CONFIG) && method_exists($JLMS_CONFIG, 'get')) {
	//ok
} else {
	$JLMS_CONFIG = JLMSFactory::getConfig();
}
if ($task != 'import' && $task != 'save_course' && $task != 'export_course' && $task != 'change_course' && $task !='course_delete_yes') {
	$pathway = array();
	$pathway[] = array('name' => _JLMS_PATHWAY_HOME, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"), 'is_home' => true);
	if ($task == 'details_course' || $task == 'add_topic_element' || $task == 'add_submit_topic_element' || $task == 'add_topic' || $task == 'edit_topic') {
		$id = $JLMS_CONFIG->get('course_id',0);
		//$id = intval( mosGetParam( $_REQUEST, 'id', 0 ) );
		$pathway[] = array('name' => $JLMS_CONFIG->get('course_name'), 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$id"), 'is_course' => true);
		//$pathway[] = array('name' => _JLMS_PATHWAY_COURSE_HOME, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$id"), 'is_home' => false);
	} else {
		$pathway[] = array('name' => _JLMS_TOOLBAR_COURSES, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=courses"));
	}
	JLMSAppendPathWay($pathway);

	if ($task == 'details_course') {
		JLMS_ShowHeading($JLMS_CONFIG->get('jlms_heading'), true);
	} else {//if ($task != 'add_topic_element' && $task != 'add_submit_topic_element' && $task != 'add_topic' && $task != 'edit_topic') {
		JLMS_ShowHeading($JLMS_CONFIG->get('jlms_heading'), false);
	}
}

require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.topics.php");  //this includes module of topics in course

	require_once(_JOOMLMS_FRONT_HOME . "/includes/flms/flms.courses.php"); // DEN: don't edit this line!!!!
if($JLMS_CONFIG->get('flms_integration', 0)){
	require_once(_JOOMLMS_FRONT_HOME . "/includes/flms/flms.courses.html.php");
	require_once(_JOOMLMS_FRONT_HOME . "/includes/flms/flms.class.php");
}

$rowid = mosGetParam( $_REQUEST, 'row_id', 0 );
$cid = mosGetParam( $_REQUEST, 't_chk', array(0) );
mosArrayToInts( $cid );

switch ($task) {
	case 'courses':				JLMS_showCourses( $option );						break;
	case 'add_course':			JLMS_editCourse( $option );							break;
	case 'edit_course':			JLMS_editCourse( $option );							break;
	case 'cancel_course':		JLMS_cancelCourse( $option );						break;
	case 'save_course':			JLMS_saveCourse( $option );							break;
	case 'details_course':		JLMS_showCourseDetails( $option );					break;
	case 'settings':			JLMS_showCourseSettings( $option );					break;
	case 'settings_save':		JLMS_saveCourseSettings( $option );					break;

	case 'export_course_pre': 	JLMS_Course_Export_Pre( $id, $option );				break;
	case 'export_course':   	JLMS_Course_Export( $id, $option );					break;

	case 'course_import':		JLMS_Course_Import_Pre( $option );					break;
	case 'import':   			JLMS_Course_Import( $option );						break;
	case 'import_succesfull':	JLMS_course_html::JLMS_Import_succesfull($option);	break;
	case 'change_course':	 	JLMS_courseChange( $id, $option );					break;
	case 'delete_course':		JLMS_pre_DeleteCourse( $id, $option );				break;
	case 'course_delete_yes': 	JLMS_deleteCourse( $id, $option );					break;
	case 'import_tpl':			JLMS_Import_Template($option);						break;
	case 'fcourse_orderup':		JLMS_orderCourse( intval( $rowid ), -1, $option );	break;
	case 'fcourse_orderdown':	JLMS_orderCourse( intval( $rowid ), 1, $option );	break;
	case 'fcourse_save_order':	JLMS_course_saveOrder( $cid, $option );				break;	
}

function JLMS_showCourseSettings( $option ) {
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$Itemid = $JLMS_CONFIG->get('Itemid');
	global $my, $JLMS_DB, $JLMS_ACL;
	$id = $JLMS_CONFIG->get('course_id',0);
	$JLMS_ACL = JLMSFactory::getACL();


	if ($id && $JLMS_ACL->CheckPermissions('course', 'manage_settings')) {

		$course = new mos_Joomla_LMS_Course( $JLMS_DB );
		$course->load( $id );
		$options = $lists = array();
		$params = new JLMSParameters($JLMS_CONFIG->get('course_params'));
		$params->def('lpath_redirect', 0);
		$params->def('track_type', 0);
		$params->def('agenda_view', 0);
		$params->def('dropbox_view', 0);
		$params->def('homework_view', 0);
		$params->def('mailbox_view', 0);
		$params->def('max_attendees', 0);
        $params->def('certificates_view', 0);
        $params->def('latest_forum_posts_view', 0);
        
		if($JLMS_CONFIG->get('show_status_scorm_as', false)){
			$params->def('scorm_course_status_as', -1); //contractorlicensing custom
		}
		if($JLMS_CONFIG->get('flms_integration', 0)){
			$params->def('show_in_report', 1);
		}

		$fp_lists = array();
		$fp_lists['homework_view'] = mosHTML::yesnoRadioList( 'params[homework_view]', 'class="inputbox" '.((($params->get('lpath_redirect') == 1) || !$JLMS_CONFIG->get('course_homework')) ? 'disabled ="disabled" ' : ''), $params->get('homework_view'), _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);
		$fp_lists['agenda_view'] = mosHTML::yesnoRadioList( 'params[agenda_view]', 'class="inputbox" '.($params->get('lpath_redirect') == 1 ? 'disabled ="disabled" ' : ''), $params->get('agenda_view'), _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);
		$fp_lists['dropbox_view'] = mosHTML::yesnoRadioList( 'params[dropbox_view]', 'class="inputbox" '.($params->get('lpath_redirect') == 1 ? 'disabled ="disabled" ' : ''), $params->get('dropbox_view'), _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);
		$fp_lists['mailbox_view'] = mosHTML::yesnoRadioList( 'params[mailbox_view]', 'class="inputbox" '.($params->get('lpath_redirect') == 1 ? 'disabled ="disabled" ' : ''), $params->get('mailbox_view'), _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);
		$fp_lists['certificates_view'] = mosHTML::yesnoRadioList( 'params[certificates_view]', 'class="inputbox" '.($params->get('lpath_redirect') == 1 ? 'disabled ="disabled" ' : ''), $params->get('certificates_view'), _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);
		$fp_lists['latest_forum_posts_view'] = mosHTML::yesnoRadioList( 'params[latest_forum_posts_view]', 'class="inputbox" '.($params->get('lpath_redirect') == 1 ? 'disabled ="disabled" ' : ''), $params->get('latest_forum_posts_view'), _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);
		$fp_lists['lpath_redirect'] = mosHTML::yesnoRadioList( 'params[lpath_redirect]', 'class="inputbox" onclick=\'jlms_disable_form_element()\' ', $params->get('lpath_redirect'), _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);

		$fp_lists['track_type'] = mosHTML::yesnoRadioList( 'params[track_type]', 'class="inputbox" ', $params->get('track_type'), _JLMS_COURSE_TRACK_TYPE_BEST, _JLMS_COURSE_TRACK_TYPE_LAST);

		$fp_lists['conf_book'] = mosHTML::yesnoRadioList( 'params[conf_book]', 'class="inputbox" ', $params->get('conf_book', 0), _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);

		$fp_lists['show_description'] = mosHTML::yesnoRadioList( 'params[show_description]', 'class="inputbox" ', $params->get('show_description', 1), _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);
		
		$fp_lists['max_attendees'] = "<input type='text' class='inputbox' value='".$params->get('max_attendees')."' name='params[max_attendees]' />";
		
		if($JLMS_CONFIG->get('flms_integration', 0)){
			$fp_lists['show_in_report'] = mosHTML::yesnoRadioList( 'params[show_in_report]', 'class="inputbox" ', $params->get('show_in_report'), _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);
		}
		
		$query_lp = "SELECT id as value, lpath_name as text "
		." FROM `#__lms_learn_paths` "
		." WHERE course_id = $id AND published = 1 ORDER BY ordering ";
		$JLMS_DB->setQuery($query_lp);
		$lpaths = $JLMS_DB->loadObjectList();
		$l_paths = array();
		//$l_paths[] = mosHTML::makeOption( 0, _JLMS_COURSEHOME_SELECT_LP );
		$l_paths[] = mosHTML::makeOption( 0, _JLMS_SETTINGS_LIST_LPATHS );
		$l_paths = array_merge($l_paths,$lpaths);
		$fp_lists['learn_path'] = mosHTML::selectList($l_paths, 'params[learn_path]', 'class="inputbox" size="1" '.($params->get('lpath_redirect') ? '' : 'disabled ="disabled" '), 'value', 'text', $params->get('learn_path') );

		$query_t = "SELECT a.*,b.menu_id as disabled "
		." FROM `#__lms_menu` as a LEFT JOIN `#__lms_local_menu` as b ON a.id = b.menu_id AND b.course_id = $id "
		." WHERE a.user_access = 1 ORDER BY a.ordering ";
		$JLMS_DB->setQuery($query_t);
		$teachers_menus = $JLMS_DB->loadObjectList();

		$query_s = "SELECT a.*,b.menu_id as disabled "
		." FROM `#__lms_menu` as a LEFT JOIN `#__lms_local_menu` as b ON a.id = b.menu_id AND b.course_id = $id "
		." WHERE a.user_access = 2 ORDER BY a.ordering ";
		$JLMS_DB->setQuery($query_s);
		$students_menus = $JLMS_DB->loadObjectList();
		
		//Course Properties Event//
		$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
		$_JLMS_PLUGINS->loadBotGroup('system');
		$plugin_args = array();
		$row = new mos_Joomla_LMS_Course( $JLMS_DB );
		$row->load( $id );
		$plugin_args[] = $row;
		$lists['plugin_return'] = $_JLMS_PLUGINS->trigger('onShowCourseSettings', $plugin_args);
		$all_fields_tmp = $lists['plugin_return'];
		$lists['plugin_return'] = array();
		foreach($all_fields_tmp as $fields_tmp){
			foreach($fields_tmp as $field_tmp){
				$lists['plugin_return'][] = $field_tmp;
			}
		}
		//Course Properties Event//

        
        $myTeachCourses = $JLMS_ACL->getMyTeachCourses();
        
        if( !empty($myTeachCourses) ) 
        {
    		$query = "SELECT * FROM #__lms_courses 
                        WHERE id <> ".intval($id)."
                        AND id IN (".implode(',', $myTeachCourses).")"                        
    			."\n ORDER BY course_name, id";
    			$JLMS_DB->SetQuery($query);
    		$courses1 = $JLMS_DB->loadObjectList();
		} else {
			$courses1 = array();         
		}
		
		$cOpts = array();
		//$cOpts[] =mosHTML::makeOption(0, _JLMS_COURSES_SELECT_ITEM);
		foreach ($courses1 as $c1) {
			$cOpt = new stdClass();
			$cOpt->value = $c1->id;
			$cOpt->text = $c1->course_name;
			$cOpts[] = $cOpt;
		}
		$lists['select_courses'] = mosHTML::selectList($cOpts, 'courses_prereqs[]', 'class="inputbox chzn-done" size="7" multiple="multiple" style="width: 100%; height: auto;" ', 'value', 'text', 0 );
				
		$query = "SELECT cp.course_id, cp.req_id, c.course_name"
			. "\n FROM #__lms_courses_prerequisites AS cp, #__lms_courses AS c"
			. "\n WHERE cp.course_id = '$id' AND cp.req_id = c.id"
			. "\n ORDER BY c.course_name, c.id";
		$JLMS_DB->SetQuery($query);
		$prereqs = $JLMS_DB->loadObjectList();
		
		$pOpts = array();
		//$cOpts[] =mosHTML::makeOption(0, _JLMS_COURSES_SELECT_ITEM);		
		foreach ($prereqs as $p1) {
			$pOpt = new stdClass();
			$pOpt->value = $p1->req_id;
			$pOpt->text = $p1->course_name;
			$pOpts[] = $pOpt;
		}		
		
		$lists['added_courses'] = mosHTML::selectList($pOpts, 'prereqs[]', 'class="inputbox chzn-done" size="7" multiple="multiple" style="width: 100%; height: auto;" ', 'value', 'text', 0 );		
        
        $prereqCourses[] = mosHTML::makeOption( 0, _JLMS_COURSES_DO_NOT_USE_PREREQ );
        $prereqCourses[] = mosHTML::makeOption( 1, _JLMS_COURSES_DISABLE_ENROLL );
        $prereqCourses[] = mosHTML::makeOption( 2, _JLMS_COURSES_HIDE_COURSE );
		
		$fp_lists['course_prereq_mode'] = mosHTML::selectList($prereqCourses, 'prereq_mode', 'class="inputbox" size="1" ', 'value', 'text', $course->prereq_mode );
		
		//CONTRACTOR
		if($JLMS_CONFIG->get('show_status_scorm_as', false)){
			$scorm_course_status_as = array();
			$scorm_course_status_as[] = mosHTML::makeOption( -1, 'global "Results display options"' );
			$scorm_course_status_as[] = mosHTML::makeOption( 1, 'text' );
			$scorm_course_status_as[] = mosHTML::makeOption( 2, 'icon' );
			$fp_lists['scorm_course_status_as'] = mosHTML::selectList( $scorm_course_status_as, 'params[scorm_course_status_as]', 'class="inputbox" size="1" ', 'value', 'text', $params->get('scorm_course_status_as', -1) );
		}
		//CONTRACTOR

		JLMS_course_html::showCourseSettings( $teachers_menus,	$students_menus, $option, $id, $fp_lists, $lists );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses") );
	}
}

function JLMS_saveCourseSettings( $option ){
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;
	$id = $JLMS_CONFIG->get('course_id',0);
	$JLMS_ACL = JLMSFactory::getACL();
	
	if ($id && $JLMS_ACL->CheckPermissions('course', 'manage_settings')) {
		// save params
		$params = mosGetParam( $_POST, 'params', '' );
		
		//(Max) fix for CoursePropeties plugin
		$row_temp = new mos_Joomla_LMS_Course( $JLMS_DB );
		$row_temp->load($id);
		
		$temp_params = explode("\n", $row_temp->params);
		$db_params = array();
		foreach($temp_params as $param){
			preg_match('#(\w+)=(\d+)#', $param, $out);
			if(isset($out[1]) && isset($out[2]) && $out[1]){
				$db_params[$out[1]] = $out[2];
			}
		}
		
		$temp = array();
		foreach($db_params as $db_param=>$db_value){
			$add = 1;
			foreach($params as $param=>$value){
				if($db_param == $param){
					$add = 0;
					break;
				} 
			}
			if($add){
				$params[$db_param] = $db_value;
			}
		}
		//(Max) fix for CoursePropeties plugin
        	
		$course_params = '';
		if (is_array( $params )) {
			$txt = array();
			$n = $i = $m = $tt = 0;
			foreach ( $params as $k=>$v) {
				$r = $v;
				if ($k == 'homework_view'){
					if (!$JLMS_CONFIG->get('course_homework')) {
						$r = 0;
					}
					$n++;
				}
				if ($k == 'agenda_view'){
					$i++;
				}
				if ($k == 'dropbox_view'){
					$m++;
				}
				if ($k == 'track_type'){
					$tt++;
				}
				$txt[] = "$k=$v";
			}
			if (!$n){ $txt[] = 'homework_view=0';}
			if (!$i){ $txt[] = 'agenda_view=0';}
			if (!$m){ $txt[] = 'dropbox_view=0';}
			if (!$tt){ $txt[] = 'track_type=0';}
			$course_params = implode( "\n", $txt );
		}
		
		//Course Properties Event//
		$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
		$_JLMS_PLUGINS->loadBotGroup('system');
		$lists['plugin_return'] = $_JLMS_PLUGINS->trigger('onSaveCourseSettings');
		
		if(isset($lists['plugin_return']) && count($lists['plugin_return'])){
			$fields = $lists['plugin_return'];
			
			if(isset($id) && $id){
				$row_p = new mos_Joomla_LMS_Course( $JLMS_DB );
				$row_p->load($id);
			}
			
			$params = strlen($row_p->params) ? explode("\n", $row_p->params) : array();
			
			$params_check = array();
			$i=0;
			foreach($params as $param){
				preg_match('#(.*)=(.*)#', $param, $out);
				
				if(isset($out[0]) && strlen($out[0]) && $out[1] && isset($out[2])){
					$params_check[$i]->name = $out[1];
					$params_check[$i]->value = $out[2];
					$i++;
				}
			}
			
			$txt_check = array();
			$i=0;
			foreach($txt as $param){
				preg_match('#(.*)=(.*)#', $param, $out);
				
				if(isset($out[0]) && strlen($out[0]) && $out[1] && isset($out[2])){
					$txt_check[$i]->name = $out[1];
					$txt_check[$i]->value = $out[2];
					$i++;
				}
			}
			
			for($i=0;$i<count($params_check);$i++){
				$param_ch = $params_check[$i];
				
				foreach($txt_check as $txt_ch){
					if(strtolower($param_ch->name) == strtolower($txt_ch->name)){
						$param_ch->value = $txt_ch->value;
					}
				}
			}
			
			$all_fields_tmp = $fields;
			$fields = array();
			foreach($all_fields_tmp as $fields_tmp){
				foreach($fields_tmp as $field_tmp){
					$fields[] = $field_tmp;
				}
			}
			
			for($i=0;$i<count($fields);$i++){
				$field = $fields[$i];
				
				$triger = true;
				foreach($params_check as $param_ch){
					if(strtolower($field->name) == $param_ch->name){
						if($field->value == $param_ch->value){
							$triger = false;
							break;
						} else {
							$triger = false;
							$param_ch->value = $field->value;
							break;
						}
					}
				}
				
				$params = array();
				foreach($params_check as $param_ch){
					$params[] = strtolower($param_ch->name).'='.$param_ch->value;
				}
				
				if($triger && strlen($field->name) && $field->value){
					$params[] = strtolower($field->name).'='.$field->value;
				}
				
			}
			
			$course_params = count($params) ? implode("\n", $params) : $course_params;
		}
		//Course Properties Event//
        $JLMS_DB->setQuery("SELECT COUNT(cp.course_id) FROM #__lms_courses_prerequisites AS cp WHERE cp.course_id = ".$id);
        $hasPrereq =  $JLMS_DB->loadResult();              
		$spec_reg = intval(mosGetParam( $_POST, 'spec_reg', 0)) ? 1 : 0;                
        $prereq_mode = ($hasPrereq?intval(mosGetParam( $_POST, 'prereq_mode', 0)):0);
        
		$query = "UPDATE `#__lms_courses` SET params = ".$JLMS_DB->Quote($course_params).", spec_reg = $spec_reg, prereq_mode = $prereq_mode WHERE id = ".$id ;
		$JLMS_DB->setQuery($query);
		$JLMS_DB->query();
		//save menu params
		$query = "DELETE FROM `#__lms_local_menu` WHERE course_id = ".$id;
		$JLMS_DB->setQuery($query);
		$JLMS_DB->query();
		$query = "INSERT INTO `#__lms_local_menu` ( `course_id` , `menu_id` , `user_access` ) VALUES ";
		$query_add = '';
		foreach ($_POST as $key=>$value){
			if (substr($key,0,7) == 'enabled'){
				$user_ac = intval(substr($key,8,1));
				$menu_id = intval(substr($key,10,3));
				if (!$value){
					$query_add .= " ( '".$id."', '".$menu_id."', '".$user_ac."' ),";
				}
			}
		}
		if ($query_add) {
			$query .= $query_add;
			$query = substr($query,0,-1);
			$JLMS_DB->setQuery($query);
			$JLMS_DB->query();
		}

		/**
		 * 13.10.2007 (DEN)
		 * Additional Registration questions MOD
		 */
		if ($JLMS_CONFIG->get('show_course_spec_property', 1) == 1) {
			$JLMS_ACL = JLMSFactory::getACL();
			$lroles = $JLMS_ACL->GetSystemRolesIds(1);

			$course_questions = mosGetParam( $_REQUEST, 'course_question', array() );
			$course_question_ids = mosGetParam( $_REQUEST, 'course_question_id', array() );
			$course_question_opts = mosGetParam( $_REQUEST, 'course_quest_optional_hid', array() );
			$course_question_defans = mosGetParam( $_REQUEST, 'course_quest_def_answer', array() );
			$proc_ids = array(0);
			if (!empty($course_questions) && !empty($course_question_ids) && is_array($course_questions) && is_array($course_question_ids) && (count($course_questions) == count($course_question_ids))) {
				$i = 0;
				while ($i < count($course_questions)) {
					$c_quest = $course_questions[$i];
					if ($c_quest) {
						$c_quest_id = intval($course_question_ids[$i]);
						$c_opt = (isset($course_question_opts[$i]) && $course_question_opts[$i]) ? 1 : 0;
						$c_defans = (isset($course_question_defans[$i]) && $course_question_defans[$i]) ? $course_question_defans[$i] : '';
						$is_updated = false;
						if ($c_quest_id) {
							$query = "SELECT id FROM #__lms_spec_reg_questions WHERE id = $c_quest_id AND course_id = $id AND role_id = 0";
							$JLMS_DB->SetQuery($query);
							$tid = $JLMS_DB->LoadResult();
							if ($tid) {
								$query = "UPDATE #__lms_spec_reg_questions SET course_question = ".$JLMS_DB->Quote($c_quest).", default_answer = ".$JLMS_DB->Quote($c_defans).", role_id = 0, is_optional = $c_opt, ordering = $i WHERE id = $c_quest_id AND course_id = $id AND role_id = 0";
								$JLMS_DB->SetQuery($query);
								$JLMS_DB->query();
								$is_updated = true;
								$proc_ids[] = $c_quest_id;
							}
						}
						if (!$is_updated) {
							$query = "INSERT INTO #__lms_spec_reg_questions (course_id, role_id, is_optional, ordering, default_answer, course_question) VALUES ($id, 0, $c_opt, $i, ".$JLMS_DB->Quote($c_defans).", ".$JLMS_DB->Quote($c_quest).")";
							$JLMS_DB->SetQuery($query);
							$JLMS_DB->query();
							$proc_ids[] = $JLMS_DB->insertid();
						}
					}
					$i ++;
				}
			}
			$proc_ids_s = implode(',',$proc_ids);
			$query = "DELETE FROM #__lms_spec_reg_questions WHERE course_id = $id AND role_id = 0 AND id NOT IN ($proc_ids_s)";
			$JLMS_DB->SetQuery($query);
			$JLMS_DB->query();
			$add_srs = josGetArrayInts('course_sr_types', $_REQUEST);
			$types = array();
			$proc_ids = array(0);
			if (!empty($add_srs)) {
				foreach($add_srs as $add_sr) {
					if ($add_sr && in_array($add_sr, $lroles)) {
						$sr_default = intval(mosGetParam($_REQUEST, 'course_quest_default_'.$add_sr, 0));
						if (!$sr_default) {
							$course_questions = mosGetParam( $_REQUEST, 'course_question_'.$add_sr, array() );
							$course_question_ids = mosGetParam( $_REQUEST, 'course_question_id_'.$add_sr, array() );
							$course_question_opts = mosGetParam( $_REQUEST, 'course_quest_optional_hid_'.$add_sr, array() );
							$course_question_defans = mosGetParam( $_REQUEST, 'course_quest_def_answer_'.$add_sr, array() );
							if (!empty($course_questions) && !empty($course_question_ids) && is_array($course_questions) && is_array($course_question_ids) && (count($course_questions) == count($course_question_ids))) {
								$i = 0;
								while ($i < count($course_questions)) {
									$c_quest = $course_questions[$i];
									if ($c_quest) {
										$c_quest_id = intval($course_question_ids[$i]);
										$c_opt = (isset($course_question_opts[$i]) && $course_question_opts[$i]) ? 1 : 0;
										$c_defans = (isset($course_question_defans[$i]) && $course_question_defans[$i]) ? $course_question_defans[$i] : '';
										$is_updated = false;
										if ($c_quest_id) {
											$query = "SELECT id FROM #__lms_spec_reg_questions WHERE id = $c_quest_id AND course_id = $id AND role_id = $add_sr";
											$JLMS_DB->SetQuery($query);
											$tid = $JLMS_DB->LoadResult();
											if ($tid) {
												$query = "UPDATE #__lms_spec_reg_questions SET course_question = ".$JLMS_DB->Quote($c_quest).", default_answer = ".$JLMS_DB->Quote($c_defans).", role_id = $add_sr, is_optional = $c_opt, ordering = $i WHERE id = $c_quest_id AND course_id = $id AND role_id = $add_sr";;
												$JLMS_DB->SetQuery($query);
												$JLMS_DB->query();
												$is_updated = true;
												$proc_ids[] = $c_quest_id;
											}
										}
										if (!$is_updated) {
											$query = "INSERT INTO #__lms_spec_reg_questions (course_id, role_id, is_optional, ordering, default_answer, course_question) VALUES ($id, $add_sr, $c_opt, $i, ".$JLMS_DB->Quote($c_defans).", ".$JLMS_DB->Quote($c_quest).")";
											$JLMS_DB->SetQuery($query);
											$JLMS_DB->query();
											$proc_ids[] = $JLMS_DB->insertid();
										}
									}
									$i ++;
								}
							}
						}
					}
				}
			}
			$proc_ids_s = implode(',',$proc_ids);
			$query = "DELETE FROM #__lms_spec_reg_questions WHERE course_id = $id AND role_id <> 0 AND id NOT IN ($proc_ids_s)";
			$JLMS_DB->SetQuery($query);
			$JLMS_DB->query();
		}
				
		/* END of 'Additional Registration questions' MOD */
		
		//save prerequisites			
		$prereqs = JRequest::getVar('prereqs', array());
						
		if ( !empty($prereqs) ) 
		{
			$query = "SELECT req_id FROM #__lms_courses_prerequisites WHERE course_id = '$id'";
			$JLMS_DB->SetQuery($query);
			$addedPrereqs = JLMSDatabaseHelper::loadResultArray();						
			
			$addPrereqs = array_diff($prereqs, $addedPrereqs);
			$remPrereqs = array_diff($addedPrereqs, $prereqs);			
							
			if( !empty($remPrereqs) ) 
			{
				$query = "DELETE FROM #__lms_courses_prerequisites WHERE course_id = '$id' AND req_id IN (".implode(',', $remPrereqs).")";
				$JLMS_DB->SetQuery($query);
				$JLMS_DB->query();
			}
			
			$values = array();
			foreach($addPrereqs as $addPrereq){
				if( $addPrereq ) 
				{
					$values[] = "($id, $addPrereq)";
				}
			}
			
			if( !empty($values) ) 
			{
				$query = "INSERT INTO #__lms_courses_prerequisites (course_id, req_id) VALUES ".implode(',', $values);
				$JLMS_DB->SetQuery($query);
				$JLMS_DB->query();
			}
		} else {
			$query = "DELETE FROM #__lms_courses_prerequisites WHERE course_id = '$id'";
			$JLMS_DB->SetQuery($query);
			$JLMS_DB->query();			
		}
		
		JLMSRedirect(sefRelToAbs("index.php?option=$option&amp;task=details_course&amp;Itemid=$Itemid&amp;id=".$id), _JLMS_COURSES_TITLE_SETTINGS_SUCCESS);
		//JLMSRedirect(sefRelToAbs("index.php?option=$option&amp;task=settings&amp;Itemid=$Itemid&amp;id=".$id), _JLMS_COURSES_TITLE_SETTINGS_SUCCESS);
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses") );
	}
}

function JLMS_pre_DeleteCourse( $id, $option ) {
	global $my, $JLMS_DB, $Itemid;
	if ( $id && JLMS_GetUserType($my->id, $id) == 1) {
		$query = "SELECT * FROM #__lms_courses WHERE id = $id";
		$JLMS_DB->SetQuery( $query );
		$rows = $JLMS_DB->LoadObjectList();
		JLMS_course_html::view_preDeletePage( $rows, $id, $option );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses") );
	}
}
function JLMS_courseChange( $id, $option ) {
	global $my, $JLMS_DB, $Itemid;
	if ( $id && JLMS_GetUserType($my->id, $id) == 1) {
		$state = intval(mosGetParam($_REQUEST, 'state', 0));
		if ($state != 1) { $state = 0; }
		$query = "UPDATE #__lms_courses SET published = $state WHERE id = $id";
		$JLMS_DB->setQuery( $query );
		$JLMS_DB->query();
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses") );
}

function JLMS_Course_Export_Pre($id , $option){
	global $Itemid, $JLMS_CONFIG;
	if ($id && $JLMS_CONFIG->get('is_teacher_user') && ($JLMS_CONFIG->get('main_usertype') == 1) ) {
		JLMS_course_html::view_preExportPage( $id, $option );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses") );
	}
}

function JLMS_Course_Export($id , $option){
	global $my, $JLMS_DB, $Itemid;
	if ( $id && JLMS_GetUserType($my->id, $id) == 1) {
		$cid = mosGetParam( $_REQUEST, 'cid', array(0) );
		mosArrayToInts( $cid );
		if (!is_array( $cid )) {
			$cid = array(0);
		} else {
			require_once (_JOOMLMS_FRONT_HOME.'/includes/jlms_course_export.php');
			JLMS_courseExport ($option, $id, 'exp', $cid );
		}
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses") );
	}
}
function JLMS_Import_Template($option) {
	global $my, $Itemid, $JLMS_CONFIG;
	require_once (_JOOMLMS_FRONT_HOME.'/includes/jlms_course_import.php');
	JLMS_templateImport ( $option );
	$msg = _JLMS_COURSE_IMPORT_SUCCESS;
	JLMSRedirect(sefRelToAbs("index.php?option=$option&task=import_succesfull&Itemid=$Itemid"), $msg);
}

function JLMS_Course_Import( $option ){
	global $my, $Itemid, $JLMS_CONFIG;
	$msg = '';

	/*		$rows = array();
	$rows[] = array('id' => 1, 'name' => _JLMS_TOOLBAR_DOCS);
	$rows[] = array('id' => 2, 'name' => _JLMS_TOOLBAR_LPATH);
	$rows[] = array('id' => 3, 'name' => 'SCORMs');
	$rows[] = array('id' => 4, 'name' => _JLMS_TOOLBAR_LINKS);
	$rows[] = array('id' => 5, 'name' => _JLMS_TOOLBAR_QUIZZES);
	$rows[] = array('id' => 6, 'name' => _JLMS_TOOLBAR_AGENDA);
	$rows[] = array('id' => 7, 'name' => _JLMS_TOOLBAR_HOMEWORK);
	$rows[] = array('id' => 8, 'name' => _JLMS_TOOLBAR_GRADEBOOK.' '._JLMS_COURSES_EXPORT_GB_SETTINGS_ONLY);

	$rows_bb = array();
	$rows_bb[] = array('id' => 1, 'name' => _JLMS_TOOLBAR_DOCS);
	$rows_bb[] = array('id' => 4, 'name' => _JLMS_TOOLBAR_LINKS);
	$rows_bb[] = array('id' => 5, 'name' => _JLMS_TOOLBAR_QUIZZES);
	$rows_bb[] = array('id' => 6, 'name' => _JLMS_TOOLBAR_AGENDA);
	$rows_bb[] = array('id' => 7, 'name' => _JLMS_TOOLBAR_HOMEWORK);
	$rows_bb[] = array('id' => 8, 'name' => _JLMS_TOOLBAR_GRADEBOOK);*/
	if ($JLMS_CONFIG->get('is_teacher_user')) {
		//if ( JLMS_GetUserType_simple($my->id) == 1) {
		$course_imported_id = intval(mosGetParam($_REQUEST, 'merge_course', 0));
		if ($course_imported_id) {
			if (in_array($course_imported_id, $JLMS_CONFIG->get('teacher_in_courses', array(0) ))) {
			} else {
				$course_imported_id = 0;
			}
		} else { $course_imported_id = 0; }
		$pack_type = strval(mosGetParam($_REQUEST, 'pack_type', 'joomlalms'));
		$cid = josGetArrayInts('cid', $_REQUEST);
		$bb_options = null;
		if ($pack_type != 'joomlalms') {
			if (!empty($cid)) {
				$bb_options = array();//'documents', 'links', 'quizzes', 'homework', 'gradebook_items', 'announcements');
				if (in_array(1, $cid)) { $bb_options[] = 'documents'; }
				if (in_array(4, $cid)) { $bb_options[] = 'links'; }
				if (in_array(5, $cid)) { $bb_options[] = 'quizzes'; }
				if (in_array(6, $cid)) { $bb_options[] = 'announcements'; }
				if (in_array(7, $cid)) { $bb_options[] = 'homework'; }
				if (in_array(8, $cid)) { $bb_options[] = 'gradebook_items'; }
			}
		}
		switch ($pack_type) {
			case 'joomlalms':
			require_once (_JOOMLMS_FRONT_HOME.'/includes/jlms_course_import.php');
			JLMS_courseImport ( $option, $course_imported_id, $cid );
			$msg = _JLMS_COURSE_IMPORT_SUCCESS;
			break;
			case 'blackboard':
			require_once (_JOOMLMS_FRONT_HOME.'/includes/import/lms_bb2lms_converter.php');
			$BB2LMS = new JLMS_bb2lms_converter($bb_options);
			$fail_msg = $BB2LMS->prepare('jlms_ifile');
			if (!$fail_msg) {
				$bb_name = strval(mosGetParam($_REQUEST, 'bb_course_name', ''));
				if ($bb_name) {
					$ret = $BB2LMS->processBBfile($course_imported_id, $bb_name );
				} else {
					$ret = $BB2LMS->processBBfile($course_imported_id);
				}
				$import_msg = isset($ret['msg'])?$ret['msg']:_JLMS_COURSE_IMPORT_SUCCESS;
				$msg = $import_msg;
			} else {
				$msg = $fail_msg;
			}
			break;
			case 'blackboard_media':
			require_once (_JOOMLMS_FRONT_HOME.'/includes/import/lms_bb2lms_converter.php');
			$BB2LMS = new JLMS_bb2lms_converter($bb_options);
			$jlms_ifile = strval(mosGetParam($_REQUEST, 'jlms_ifile', ''));
			if ($jlms_ifile) {
				$fail_msg = $BB2LMS->prepareFTP($jlms_ifile);
				if (!$fail_msg) {
					$bb_name = strval(mosGetParam($_REQUEST, 'bb_course_name', ''));
					if ($bb_name) {
						$ret = $BB2LMS->processBBfile($course_imported_id, $bb_name);
					} else {
						$ret = $BB2LMS->processBBfile($course_imported_id);
					}
					$import_msg = isset($ret['msg'])?$ret['msg']:_JLMS_COURSE_IMPORT_SUCCESS;
					$msg = $import_msg;
				} else {
					$msg = $fail_msg;
				}
			}
			break;
		}
		JLMSRedirect(sefRelToAbs("index.php?option=$option&task=import_succesfull&Itemid=$Itemid"), $msg);
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses") );
	}
}

function JLMS_showCourses($option) {
	$db = & JLMSFactory::getDB();
	$user = JLMSFactory::getUser();
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$JLMS_SESSION = JLMSFactory::getSession();
	$Itemid = $JLMS_CONFIG->get('Itemid');

	$limit		= intval( mosGetParam( $_REQUEST, 'limit', $JLMS_SESSION->get('list_limit',$JLMS_CONFIG->get('list_limit')) ) );
	$JLMS_SESSION->set('list_limit', $limit);
	$limitstart = intval( mosGetParam( $_REQUEST, 'limitstart', 0 ) );
	
	$lists = array();
	$menu_params = null;
	/*Courses Blog*/
	if($Itemid){
        $app = JFactory::getApplication();
		$menus	=  $app->getMenu();
		if (method_exists($menus, 'getParams')) {
			$menu_params = $menus->getParams($Itemid);
			$lists['menu_params'] = $menu_params;
		}
	}
	/*Courses Blog*/
	
	$JLMS_ACL = JLMSFactory::getACL();
	$usertype_simple = $JLMS_ACL->_role_type;
	
	$where = '';
	$filter = intval( mosGetParam( $_REQUEST, 'courses_type', $JLMS_SESSION->get('courses_type', 0) ) );
	$JLMS_SESSION->set('courses_type', $filter);
	$filter_groups = intval( mosGetParam( $_REQUEST, 'groups_course', $JLMS_SESSION->get('groups_course', 0) ) );
	$JLMS_SESSION->set('groups_course', $filter_groups);

	/*$query = "SELECT * FROM `#__lms_course_cats` ";
//	if ($usertype_simple == 2) {
//	if (!$JLMS_ACL->CheckPermissions('lms', 'create_course')) {
	if($JLMS_ACL->_role_type == 0 || $JLMS_ACL->_role_type == 1) {
		$query1 = "SELECT group_id FROM `#__lms_users_in_global_groups` WHERE user_id = '".$user->get('id')."'";
		$db->setQuery($query1);
		$user_group_ids = JLMSDatabaseHelper::loadResultArray();

		$query .= "\n WHERE `parent` = '0' AND (";
		if (count($user_group_ids)) {
			$query .= "( `restricted` = 1 AND ( `groups` LIKE '%|$user_group_ids[0]|%'";
			for($i=1;$i<count($user_group_ids);$i++) {
				$query .= "\n OR `groups` like '%|$user_group_ids[$i]|%'";
			}
			$query .=  "\n ) ) \n OR ";
		}
		$query .= "(`restricted` = 0 )) ";
	}
	$query .= "\n ORDER BY `c_category`";
	
	$db->setQuery($query);*/
	/*$groups = $JLMS_ACL->getMyCategories();//$db->loadObjectList();
	
	$type_g[] = mosHTML::makeOption( 0, _JLMS_COURSES_ALL_CATEGORIES );
	$i = 1;
	foreach ($groups as $group){
		$type_g[] = mosHTML::makeOption( $group->id, $group->c_category );
		$i ++;
	}

	$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=courses";
	$link = $link ."&amp;groups_course='+this.options[selectedIndex].value+'";
	$link = sefRelToAbs($link);
	$link = str_replace('%5C%27',"'", $link);
	$link = str_replace('%5B',"[", $link);
	$link = str_replace('%5D',"]", $link);$link = str_replace('%20',"+", $link);$link = str_replace("\\\\\\","", $link);
	$link = str_replace('%27',"'", $link);
	$lists['groups_course'] = mosHTML::selectList($type_g, 'groups_course', 'class="inputbox" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filter_groups );
	*/

	//DEN: above code commented, because it used only if multicat disabled - this can't be never true =) ... at teh time being we always use multicat mode
	
	//DEN: code below is commented, because now this part moved to ACL
	//FLMS multicat
	/*$query_1 = '';
	if($JLMS_ACL->_role_type == 2 || $JLMS_ACL->_role_type == 3 || $JLMS_ACL->_role_type == 4) {
		$view_all_course_categories = $JLMS_ACL->CheckPermissions('advanced', 'view_all_course_categories');
		if(!$view_all_course_categories) {
			$query = "SELECT a.group_id FROM `#__lms_user_assign_groups` as a WHERE a.user_id = '".$user->get('id')."'";
			$db->setQuery($query);
			$user_group_ids = JLMSDatabaseHelper::loadResultArray();
			if (count($user_group_ids)) {
				$query_1 = "  ( `restricted` = 0 OR (`restricted` = 1 AND (`groups` LIKE '%|$user_group_ids[0]|%'";
				for($i=1;$i<count($user_group_ids);$i++) {
					$query_1 .= "\n OR `groups` like '%|$user_group_ids[$i]|%'";
				}
				$query_1 .=  "\n ) ) ) \n ";
			}
		}
	}*/
	
	$levels = array();
	if ($JLMS_CONFIG->get('multicat_use', 0)){
		$query = "SELECT * FROM #__lms_course_cats_config ORDER BY id";
		$db->setQuery($query);
		$levels = $db->loadObjectList();
		if(count($levels) == 0){
			for($i=0;$i<5;$i++){
				$levels[$i] = new stdClass();
				$levels[$i]->cat_name = _JLMS_COURSES_COURSES_GROUPS;
			}
		}
		
		$level_id = array();
		for($i=0;$i<count($levels);$i++){
			if(isset($_REQUEST['category_filter']) && $_REQUEST['category_filter']){
				if($i == 0){
					$level_id[$i] = $_REQUEST['category_filter'];
					$parent_id[$i] = 0;
				} else {
					$level_id[$i] = 0;	
					$parent_id[$i] = $level_id[$i-1];
				}
			} else {
				if($i == 0){
					$level_id[$i] = intval( mosGetParam( $_REQUEST, 'filter_id_'.$i, $JLMS_SESSION->get('FLMS_filter_id_'.$i, 0) ) );
					$_REQUEST['filter_id_'.$i] = $level_id[$i];
					$JLMS_SESSION->set('FLMS_filter_id_'.$i.'', $level_id[$i]);
				} else {
					$level_id[$i] = intval( mosGetParam( $_REQUEST, 'filter_id_'.$i, $JLMS_SESSION->get('FLMS_filter_id_'.$i, 0) ) );
					$_REQUEST['filter_id_'.$i] = $level_id[$i];
					$JLMS_SESSION->set('FLMS_filter_id_'.$i.'', $level_id[$i]);
				}
				if($i == 0){
					$parent_id[$i] = 0;
				} else {
					$parent_id[$i] = $level_id[$i-1];
				}
				if($i == 0 || $parent_id[$i]){ //(Max): extra requests
					/*$query = "SELECT count(id) FROM `#__lms_course_cats` WHERE parent = '".$parent_id[$i]."'"
					. ( $query_1 ? " AND ".$query_1 : "" )
					."\n ORDER BY c_category";
					$db->setQuery($query);*/
					$groups = $JLMS_ACL->getMyCategories($i, $parent_id[$i]);// $db->loadResult();
					
					if($groups==0){
						$level_id[$i] = 0;	
						$JLMS_SESSION->set('FLMS_filter_id_'.$i.'', $level_id[$i]);
					}
				}
			}
		}
		
		for($i=0;$i<count($levels);$i++){
			if($i > 0 && $level_id[$i - 1] == 0){
				$level_id[$i] = 0;
				$_REQUEST['filter_id_'.$i] = $level_id[$i];
				$JLMS_SESSION->set('FLMS_filter_id_'.$i.'', $level_id[$i]);
				$parent_id[$i] = 0;
			} elseif($i == 0 && $level_id[$i] == 0) {
				$level_id[$i] = 0;
				$_REQUEST['filter_id_'.$i] = $level_id[$i];
				$JLMS_SESSION->set('FLMS_filter_id_'.$i.'', $level_id[$i]);
				$parent_id[$i] = 0;
			}
		}
		
		$javascript = 'onclick="javascript:read_filter();" onchange="javascript:write_filter();document.adminForm.task.value=\'courses\';document.adminForm.itemid.value=\''.$Itemid.'\';document.adminForm.submit();"';
		
		$query1 = "SELECT group_id FROM `#__lms_users_in_global_groups` WHERE user_id = '".$user->get('id')."'";
		$db->setQuery($query1);
		$user_group_ids = JLMSDatabaseHelper::loadResultArray();

		for($i=0;$i<count($levels);$i++) {
			if($i == 0 || $parent_id[$i]){ //(Max): extra requests
				$groups = array();
				//if( $parent_id[$i] == 0 && (!$JLMS_ACL->CheckPermissions('lms', 'create_course'))) {
				/*if( $parent_id[$i] == 0 && ($JLMS_ACL->_role_type == 0 || $JLMS_ACL->_role_type == 1)) {
					$query = "SELECT * FROM `#__lms_course_cats` WHERE `parent` = '0'"
					. ( $query_1 ? " AND ".$query_1 : "" )
					;
					$query .= "\n AND (";
					if (count($user_group_ids)) {
						$query .= "( `restricted` = 1 AND ( `groups` LIKE '%|$user_group_ids[0]|%'";
						for($i1=1;$i1<count($user_group_ids);$i1++) {
							$query .= "\n OR `groups` like '%|$user_group_ids[$i1]|%'";
						}
						$query .=  "\n ) ) \n OR ";
					}
					$query .= "(`restricted` = 0 )) ";
					$query .= "\n ORDER BY `c_category`";
				}
				else {
					$query = "SELECT * FROM `#__lms_course_cats` WHERE parent = '".$parent_id[$i]."'"
					. ( $query_1 ? " AND ".$query_1 : "" )
					."\n ORDER BY c_category";
				}
				
				$db->setQuery($query);
				$groups = $db->loadObjectList();*/
				$groups = $JLMS_ACL->getMyCategories($i, $parent_id[$i]);
				
				if($parent_id[$i] && $i > 0 && count($groups)) {
					$type_level[$i][] = mosHTML::makeOption( 0, ' &nbsp; ' );
					foreach ($groups as $group){
						$type_level[$i][] = mosHTML::makeOption( $group->id, $group->c_category );
					}
					$lists['filter_'.$i.''] = mosHTML::selectList($type_level[$i], 'filter_id_'.$i.'', 'class="inputbox" size="1" style="width: 266px;" '.$javascript, 'value', 'text', $level_id[$i] ); //onchange="document.location.href=\''. $link_multi .'\';"
				} elseif($i == 0) {
					$type_level[$i][] = mosHTML::makeOption( 0, ' &nbsp; ' );
					foreach ($groups as $group){
						$type_level[$i][] = mosHTML::makeOption( $group->id, $group->c_category );
					}
					$lists['filter_'.$i.''] = mosHTML::selectList($type_level[$i], 'filter_id_'.$i.'', 'class="inputbox" size="1" style="width: 266px;" '.$javascript, 'value', 'text', $level_id[$i] ); //onchange="document.location.href=\''. $link_multi .'\';"
				}
			}
		}	
	}
	//FLMS multicat

	//echo $usertype_simple;
//	if ($usertype_simple == 1 || $usertype_simple == 5) {



	if ($JLMS_ACL->CheckPermissions('lms', 'create_course')) {
		if ($filter){
			if ($filter == 1){
				$where = " AND a.owner_id = ".$user->get('id');
			}
			elseif($filter == 2){
				$where = " AND a.owner_id != ".$user->get('id');
			}
		}
		if ($JLMS_CONFIG->get('multicat_use', 0)){
			//NEW MUSLTICATS
//			$tmp_level = array();
			$last_catid = 0;
			$query_1 = 'xm ?';//
			$tmp_cats_filter = JLMS_getFilterMulticategories($last_catid, $query_1);
			/*
			if(isset($_REQUEST['category_filter']) && $_REQUEST['category_filter']){
				$last_catid = $_REQUEST['category_filter'];
			} else {
				$i=0;
				foreach($_REQUEST as $key=>$item){
					if(preg_match('#filter_id_(\d+)#', $key, $result)){
						if($item){
							$tmp_level[$i] = $result;
							$last_catid = $item;
							$i++;
						}	
					}	
				}
			}
			
			$query = "SELECT * FROM #__lms_course_cats"
			. ( $query_1 ? " WHERE ".$query_1 : "" )
			;
			$query .= " ORDER BY id";
			
			$db->setQuery($query);
			$all_cats = $db->loadObjectList();
			
			$tmp_cats_filter = array();
			$children = array();
			foreach($all_cats as $cat){
				$pt = $cat->parent;
				$list = @$children[$pt] ? $children[$pt] : array();
				array_push($list, $cat->id);
				$children[$pt] = $list;
			}
			$tmp_cats_filter[0] = $last_catid;
			$i=1;
			foreach($children as $key=>$childs){
				if($last_catid == $key){
					foreach($children[$key] as $v){
						if(!in_array($v, $tmp_cats_filter)){
							$tmp_cats_filter[$i] = $v;
							$i++;
						}
					}
				}
			}
			foreach($children as $key=>$childs){
				if(in_array($key, $tmp_cats_filter)){
					foreach($children[$key] as $v){
						if(!in_array($v, $tmp_cats_filter)){
							$tmp_cats_filter[$i] = $v;
							$i++;
						}
					}
				}
			}
			$tmp_cats_filter = array_unique($tmp_cats_filter);
			*/
			$catids = implode(",", $tmp_cats_filter);
			
			if($last_catid && count($tmp_cats_filter)){
				$where .= "\n AND ( a.cat_id IN (".$catids.")";
				if($JLMS_CONFIG->get('sec_cat_use', 0)){
					foreach ($tmp_cats_filter as $tmp_cats_filter_one) {
						$where .= "\n OR a.sec_cat LIKE '%|".$tmp_cats_filter_one."|%'";
					}
				}
				$where .= "\n )";
			}
		} else {
			if ($filter_groups){
				if ($JLMS_CONFIG->get('sec_cat_use', 0)) {
					$where .= " AND (a.cat_id = '$filter_groups' OR a.sec_cat LIKE '%|$filter_groups|%') ";
				} else {
					$where .= " AND a.cat_id = '$filter_groups' ";
				}
			}
		}

		//show paid courses (admin settings)
		//$show_paid_courses = $JLMS_CONFIG->get('show_paid_courses', 1);
		//$where .= $show_paid_courses ? '' : ' AND a.paid <> 1 ';

		$type[] = mosHTML::makeOption( 0, _JLMS_COURSES_ALL_COURSES );
		$type[] = mosHTML::makeOption( 1, _JLMS_COURSES_TITLE_MY_COURSES );
		$type[] = mosHTML::makeOption( 2, _JLMS_COURSES_NO_MY_COURSES );
		$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=courses";
		$link = $link ."&amp;courses_type=".JLMS_SELECTED_INDEX_MARKER;
		$link = processSelectedIndexMarker( $link );
		$lists['courses_type'] = mosHTML::selectList($type, 'courses_type', 'class="inputbox" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filter );

		$my_course_ids = JLMS_GetUserCourses_IDs( $user->get('id'), $usertype_simple, true );
//		if ($usertype_simple == 5) { $usertype_simple = 1; }
		if ($usertype_simple == 4) { $usertype_simple = 2; }
		if (!count($my_course_ids)) { $my_course_ids = array(0); }
		$mc_cid = implode(',', $my_course_ids);
		
		$user_group_ids = array();
		if($JLMS_ACL->_role_type == 2 || $JLMS_ACL->_role_type == 3 || $JLMS_ACL->_role_type == 4) {
			$view_all_course_categories = $JLMS_ACL->CheckPermissions('advanced', 'view_all_course_categories');
			if(!$view_all_course_categories) {
				$query = "SELECT a.group_id FROM `#__lms_user_assign_groups` as a WHERE a.user_id = '".$user->get('id')."'";
				$db->setQuery($query);
				$user_group_ids = JLMSDatabaseHelper::loadResultArray();
//				if (count($user_group_ids)) {
//					$query_2 = "  ( d.restricted = 0 OR (d.restricted = 1 AND (d.groups LIKE '%|$user_group_ids[0]|%'";
//					for($i=1;$i<count($user_group_ids);$i++) {
//						$query_2 .= "\n OR d.groups like '%|$user_group_ids[$i]|%'";
//					}
//					$query_2 .=  "\n ) ) )\n ";
//				}
			}
		}

		$restricted_courses = JLMS_illegal_courses($user_group_ids);
		$restricted_courses = array_diff($restricted_courses, $JLMS_ACL->getMyCourses());

		$restricted_courses = implode(',', $restricted_courses);
			
		if($restricted_courses == '')
			$restricted_courses = "''";
		
		//AND a.user_id = '".$user->get('id')."'
		
		$query = "SELECT a.*, b.username, b.email, b.name as user_fullname, a.id as course_id, d.c_category, wl.id AS in_wl"
		. "\n FROM `#__lms_courses` as a "
		. "\n LEFT JOIN `#__lms_course_cats` as d ON d.id = a.cat_id "
		. "\n LEFT JOIN `#__lms_waiting_lists` as wl ON wl.course_id = a.id AND wl.user_id=".$user->get('id')
		. "\n LEFT JOIN `#__users` as b ON a.owner_id = b.id"
		. "\n WHERE 1"
		. "\n AND ( ( a.published = 1"
		. ( $JLMS_CONFIG->get('show_future_courses', false) ? '' : "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )" )
		. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d')."')) OR (a.publish_end = 0) )"
		. "\n ) OR (a.id IN ($mc_cid)) ) AND a.id NOT IN ($restricted_courses)"
		. "\n $where "
		. "\n ORDER BY ".($JLMS_CONFIG->get('lms_courses_sortby',0)?"a.ordering, a.course_name, a.id":"a.course_name, a.ordering, a.id")
		;
		$db->setQuery( $query );
		$db->query();
		
		$total = $db->getNumRows();
		
		require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
		$pageNav = new JLMSPageNav( $total, $limitstart, $limit );
		
//		$db->SetQuery( $query, $pageNav->limitstart, $pageNav->limit );
		$db->SetQuery( $query );
		$rows = $db->LoadObjectList();
		
		//Leading Courses - Blog parametrs
		if(is_object($menu_params) && method_exists($menu_params, 'get') && strlen($menu_params->get('leading_courses', ''))){
			$leading_courses = array();
			
			$leading_courses = explode(',', $menu_params->get('leading_courses', ''));
			
			for($i=0;$i<count($rows);$i++){
				$rows[$i]->leading_course = 0;
				if(in_array($rows[$i]->id, $leading_courses)){
					$rows[$i]->leading_course = 1;
					$lists['leading_courses'] = 1;
				}
			}
			
			$ordering = 0;
			$i = 0;
			while ($i < count($rows)) {
				$j = $i + 1;
				while ($j < count($rows)) {
					if($rows[$j]->leading_course) {
						$rows[$i]->ordering = $j;
						$rows[$j]->ordering = $i;
						$temp = new stdClass();
						$temp = $rows[$j];
						$rows[$j] = $rows[$i];
						$rows[$i] = $temp;
						break;
					}
					$j ++;	
				}
				$i ++;	
			}
			
			$ordering = 0;
			$i = 0;
			while ($i < count($rows)) {
				if($rows[$i]->leading_course) {
					$j = $i + 1;
					while ($j < count($rows)) {
						if(isset($leading_courses[$i]) && $rows[$j]->id == $leading_courses[$i]) {
							$rows[$i]->ordering = $j;
							$rows[$j]->ordering = $i;
							$temp = new stdClass();
							$temp = $rows[$j];
							$rows[$j] = $rows[$i];
							$rows[$i] = $temp;
							break;
						}
						$j ++;	
					}
				}
				$i ++;	
			}
		}
		//Leading Courses - Blog parametrs
		
		$tmp_rows = $rows;
		$rows = array();
		for($i=$pageNav->limitstart;$i<($pageNav->limitstart + $pageNav->limit);$i++){
			if(isset($tmp_rows[$i]) && $tmp_rows[$i]->id){
				$rows[] = $tmp_rows[$i];
			}
		}
		
		
		$lms_titles_cache = & JLMSFactory::getTitles();
		foreach ($rows as $key => $row) {
			$tmp_arr = array();
			if ($JLMS_CONFIG->get('sec_cat_use', 0)) {
				$tmp = explode('|', $row->sec_cat);
				array_shift($tmp);
				array_pop($tmp);
				$tmp_arr = $tmp;
				$tmp = (count($tmp) && isset($tmp[0]) && intval($tmp[0])) ? ','.implode(',', $tmp) : '';
			} else $tmp = '';
			$cat_string = $row->cat_id.$tmp;
			$tmp_arr[] = $row->cat_id;
			$ccategories = array();
			foreach ($tmp_arr as $tmp_cat_id) {
				$ccategories[] = $lms_titles_cache->get('categories', $tmp_cat_id);
			}
			//$query = "SELECT c_category FROM #__lms_course_cats WHERE id IN ($cat_string)";
			//$db->setQuery($query);
			$row->c_category = implode('<br />', $ccategories);//JLMSDatabaseHelper::loadResultArray());
		}

		$lms_titles_cache = & JLMSFactory::getTitles();
		$lms_titles_cache->setArray('courses', $rows, 'id', 'course_name');
		
		//Course Properties Event//
		$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
		$_JLMS_PLUGINS->loadBotGroup('system');
		
		$plugin_args = array();
		$plugin_args[] = $rows;
		$columns = $_JLMS_PLUGINS->trigger('onShowCourseListColumns', $plugin_args);
		
		$all_columns_tmp = $columns;
		$columns = array();
		foreach($all_columns_tmp as $columns_tmp){
			foreach($columns_tmp as $column_tmp){
				$columns[] = $column_tmp;
			}
		}
		
//		echo '<pre>';
//		print_r($columns);
//		echo '</pre>';
		
		$lists['extra_columns'] = $columns;
		
		for($i=0;$i<count($rows);$i++){
			$row = $rows[$i];
			
			$plugin_args = array();
			$plugin_args[] = $row;
			$plugin_course_list_extra_info = $_JLMS_PLUGINS->trigger('onShowCourseListExtraInformation', $plugin_args);
			$plugin_args[] = $lists['extra_columns'];
			$plugin_course_list_extra_cols = $_JLMS_PLUGINS->trigger('onShowCourseListExtraColumn', $plugin_args);
			
			if(isset($plugin_course_list_extra_info) && count($plugin_course_list_extra_info)){
				
				$alls_items_tmp = $plugin_course_list_extra_info;
				$plugin_course_list_extra_info = array();
				foreach($alls_items_tmp as $alls_item_tmp){
					foreach($alls_item_tmp as $item_tmp){
						$plugin_course_list_extra_info[] = $item_tmp;
					}
				}
				
				$row->plugin_course_list_extra_information = $plugin_course_list_extra_info;
			}
			if(isset($plugin_course_list_extra_cols) && count($plugin_course_list_extra_cols)){
				
				$alls_items_tmp = $plugin_course_list_extra_cols;
				$plugin_course_list_extra_cols = array();
				foreach($alls_items_tmp as $alls_item_tmp){
					foreach($alls_item_tmp as $item_tmp){
						$plugin_course_list_extra_cols[] = $item_tmp;
					}
				}
				
				$row->plugin_course_list_extra_column = $plugin_course_list_extra_cols;
			}
		}
		
//		echo '<pre>';
//		print_r($rows);
//		print_r($menu_params);
//		echo '</pre>';
		
		//Course Properties Event//

		if(is_object($menu_params) && method_exists($menu_params, 'get') && $menu_params->get('blog', 0)){
			JLMS_course_html::viewCourses_blog( $rows, $pageNav, $option, $usertype_simple, $lists, $levels);
		} else {
			JLMS_course_html::viewCourses( $rows, $pageNav, $option, $usertype_simple, $lists, $levels);
		}

//	} elseif ($usertype_simple == 2) {
	} 
	//elseif (!$JLMS_ACL->CheckPermissions('lms', 'create_course')) {
	elseif ($JLMS_ACL->_role_type == 0 || $JLMS_ACL->_role_type == 1 || $JLMS_ACL->_role_type == 3) {
		
		$restricted_courses = JLMS_illegal_courses($user_group_ids);
		$restricted_courses = array_diff($restricted_courses, $JLMS_ACL->getMyCourses());

		$restricted_courses = implode(',', $restricted_courses);
			
		if($restricted_courses == '')
			$restricted_courses = "''";
			
		if ($filter){
			$query = "SELECT course_id FROM `#__lms_users_in_groups` WHERE user_id = '".$user->get('id')."'";
			$db->setQuery($query);
			$id = JLMSDatabaseHelper::loadResultArray();
			$id = implode(',', $id);
			if ($filter == 1){
				$where = " AND a.id IN (".$id.") ";
			}
			elseif($filter == 2){
				$where = " AND a.id NOT IN ( ".$id.") " ;
			}
		}
		if ($JLMS_CONFIG->get('multicat_use', 0)){
			//NEW MUSLTICATS
//			$tmp_level = array();
			$last_catid = 0;
			
			$tmp_cats_filter = JLMS_getFilterMulticategories($last_catid);
						
			$catids = implode(",", $tmp_cats_filter);
			
			if($last_catid && count($tmp_cats_filter)){
				$where .= "\n AND ( a.cat_id IN (".$catids.")";
				if($JLMS_CONFIG->get('sec_cat_use', 0)){
					foreach ($tmp_cats_filter as $tmp_cats_filter_one) {
						$where .= "\n OR a.sec_cat LIKE '%|".$tmp_cats_filter_one."|%'";
					}
				}
				$where .= "\n )";
			}
			//NEW MUSLTICATS
			
		} else {
			if ($filter_groups){
				if ($JLMS_CONFIG->get('sec_cat_use', 0)) {
					$where .= " AND (a.cat_id = '$filter_groups' OR a.sec_cat LIKE '%|$filter_groups|%') ";
				} else {
					$where .= " AND a.cat_id = '$filter_groups' ";
				}
			}
		}
		$type[] = mosHTML::makeOption( 0, _JLMS_COURSES_ALL_COURSES );
		$type[] = mosHTML::makeOption( 1, _JLMS_COURSES_SUBSCRIBED );
		$type[] = mosHTML::makeOption( 2, _JLMS_COURSES_NOT_SUBSCRIBED );
		$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=courses";
		$link = $link ."&amp;courses_type=".JLMS_SELECTED_INDEX_MARKER;
		$link = processSelectedIndexMarker( $link );
		$lists['courses_type'] = mosHTML::selectList($type, 'courses_type', 'class="inputbox" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filter );

		$my_course_ids = JLMS_GetUserCourses_IDs( $user->get('id'), $usertype_simple, true );
		if (!count($my_course_ids)) { $my_course_ids = array(0); }
		$mc_cid = implode(',', $my_course_ids);

		$query = "SELECT a.*, b.username, b.email, b.name as user_fullname, a.id as course_id, d.c_category, wl.id AS in_wl"
//		. "\n EXISTS(SELECT id FROM `#__lms_waiting_lists` AS `wl` WHERE wl.user_id=".$user->get('id')." AND wl.course_id=a.id) AS in_wl"
		. "\n FROM `#__lms_courses` as a "
		. "\n LEFT JOIN `#__lms_course_cats` as d ON d.id = a.cat_id "
		. "\n LEFT JOIN `#__lms_waiting_lists` as wl ON wl.course_id = a.id AND wl.user_id=".$user->get('id')
		. "\n LEFT JOIN `#__users` as b ON a.owner_id = b.id"
		. "\n WHERE 1 = 1"
		. "\n AND ( ( a.published = 1"
		. ( $JLMS_CONFIG->get('show_future_courses', false) ? '' : "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )" )
		. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d')."')) OR (a.publish_end = 0) )"
		. "\n AND a.id NOT IN ($restricted_courses)) OR (a.id IN ($mc_cid)) )"
		//. "\n AND d.id = a.cat_id $where "
		. "\n $where "
		. "\n ORDER BY ".($JLMS_CONFIG->get('lms_courses_sortby',0)?"a.ordering, a.course_name, a.id":"a.course_name, a.ordering, a.id")
		;
		$db->SetQuery( $query );
		$db->query();
		
		$total = $db->getNumRows();
		
		require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
		$pageNav = new JLMSPageNav( $total, $limitstart, $limit );
		
		$db->SetQuery( $query );
		$rows = $db->LoadObjectList();
		
		//Leading Courses - Blog parametrs
		if(is_object($menu_params) && method_exists($menu_params, 'get') && strlen($menu_params->get('leading_courses', ''))){
			$leading_courses = array();
			
			$leading_courses = explode(',', $menu_params->get('leading_courses', ''));
			
			for($i=0;$i<count($rows);$i++){
				$rows[$i]->leading_course = 0;
				if(in_array($rows[$i]->id, $leading_courses)){
					$rows[$i]->leading_course = 1;
					$lists['leading_courses'] = 1;
				}
			}
			
			
			
			$ordering = 0;
			$i = 0;
			while ($i < count($rows)) {
				$j = $i + 1;
				while ($j < count($rows)) {
					if($rows[$j]->leading_course) {
						$rows[$i]->ordering = $j;
						$rows[$j]->ordering = $i;
						$temp = new stdClass();
						$temp = $rows[$j];
						$rows[$j] = $rows[$i];
						$rows[$i] = $temp;
						break;
					}
					$j++;	
				}
				$i++;	
			}
			
			/*
			echo '<pre>';
			print_r($leading_courses);
			print_r($rows);
			echo '</pre>';
			
			$ordering = 0;
			$i = 0;
			while ($i < count($rows)) {
				if($rows[$i]->leading_course) {
					$j = $i + 1;
					foreach($leading_courses as $k=>$leadid){
						if($rows[$i]->id == $leadid){
							$rows[$i]->ordering = $k;
							if($rows[$i]->ordering > $rows[$j]->ordering){
								$temp = new stdClass();
								$temp = $rows[$j];
								$rows[$j] = $rows[$i];
								$rows[$i] = $temp;
							}
							break;
						}
					}
				}
				$i++;	
			}
			*/
			
		}
		//Leading Courses - Blog parametrs
		
		/*
		echo '<pre>';
		print_r($rows);
		echo '</pre>';
		*/
		
		$tmp_rows = $rows;
		$rows = array();
		for($i=$pageNav->limitstart;$i<($pageNav->limitstart + $pageNav->limit);$i++){
			if(isset($tmp_rows[$i]) && $tmp_rows[$i]->id){
				$rows[] = $tmp_rows[$i];
			}
		}
		
		
		$lms_titles_cache = & JLMSFactory::getTitles();
		foreach ($rows as $key => $row) {
			$tmp_arr = array();
			if ($JLMS_CONFIG->get('sec_cat_use', 0)) {
				$tmp = explode('|', $row->sec_cat);
				array_shift($tmp);
				array_pop($tmp);
				$tmp_arr = $tmp;
				$tmp = (count($tmp) && isset($tmp[0]) && intval($tmp[0])) ? ','.implode(',', $tmp) : '';
			} else $tmp = '';
			$cat_string = $row->cat_id.$tmp;
			$tmp_arr[] = $row->cat_id;
			$ccategories = array();
			foreach ($tmp_arr as $tmp_cat_id) {
				$ccategories[] = $lms_titles_cache->get('categories', $tmp_cat_id);
			}
			$row->c_category = implode('<br />', $ccategories);
		}

		$lms_titles_cache = & JLMSFactory::getTitles();
		$lms_titles_cache->setArray('courses', $rows, 'id', 'course_name');
		
		if(is_object($menu_params) && method_exists($menu_params, 'get') && $menu_params->get('blog', 0)){
			JLMS_course_html::viewCourses_blog( $rows, $pageNav, $option, $usertype_simple, $lists, $levels);
		} else {
			JLMS_course_html::viewCourses( $rows, $pageNav, $option, $usertype_simple, $lists, $levels);
		}
	} else {
		$query = "SELECT user_id FROM `#__lms_user_courses` WHERE user_id = " . $user->get('id');
		$db->setQuery( $query );
		$db->query();

		$total = $db->getNumRows();

		require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
		$pageNav = new JLMSPageNav( $total, $limitstart, $limit );

		$db->SetQuery( $query );
		$rows = $db->LoadObjectList();

		if(empty($rows)){
			$levels = array();
			$lists = array();
			JLMS_course_html::viewCourses($rows, $pageNav, $option, $usertype_simple, $lists, $levels);
		}else{
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
		}
	}
}

function JLMS_multicats($last_catid, $tmp, $i=0){
	global $JLMS_DB;
	
	$query = "SELECT parent FROM #__lms_course_cats WHERE id = '".$last_catid."'";
	$JLMS_DB->setQuery($query);
	$parent = $JLMS_DB->loadResult();
	$tmp[$i] = new stdClass();
	$tmp[$i]->catid = $last_catid;
	$tmp[$i]->parent = isset($parent)?$parent:0;
	if($parent){
		$last_catid = $parent;
		$i++;
		$tmp = JLMS_multicats($last_catid, $tmp, $i);
	}
	return $tmp;
}

function JLMS_editCourse( $option) {
	global $my, $Itemid, $JLMS_CONFIG, $JLMS_DB, $task;
	$id = $JLMS_CONFIG->get('course_id');
	$is_inside = intval(mosGetParam($_REQUEST, 'is_inside', 0));	


	$JLMS_ACL = JLMSFactory::getACL();
	
	if ( ($id && $JLMS_ACL->CheckPermissions('course', 'edit') ) || (!$id && $JLMS_ACL->CheckPermissions('lms', 'create_course')) ) {
		
		$query_1 = '';
		$view_all_course_categories = $JLMS_ACL->CheckPermissions('advanced', 'view_all_course_categories');
		if(!$view_all_course_categories) {
			$query = "SELECT a.group_id FROM `#__lms_user_assign_groups` as a WHERE a.user_id = '".$my->id."'";
			$JLMS_DB->setQuery($query);
			$user_group_ids = JLMSDatabaseHelper::loadResultArray();
			if (count($user_group_ids)) {
				$query_1 = "  ( `restricted` = 0 OR (`restricted` = 1 AND (`groups` LIKE '%|$user_group_ids[0]|%'";
				for($i=1;$i<count($user_group_ids);$i++) {
					$query_1 .= "\n OR `groups` like '%|$user_group_ids[$i]|%'";
				}
				$query_1 .=  "\n ) ) ) \n ";
			}
		}
		
		$row = new mos_Joomla_LMS_Course( $JLMS_DB );
		$row->load( $id );
		if (!$id){
			$row->add_forum = 0;
			$row->add_chat = 1;
			$row->self_reg = 1;
			$row->add_hw = 1;
			$row->add_attend = 1;
			$row->published = 0;
			$row->start_date = date('Y-m-d');
			$row->end_date = date('Y-m-d',strtotime("+6 month"));
			
			$query = "SELECT id FROM `#__lms_languages` WHERE lang_name = ".$JLMS_DB->Quote($JLMS_CONFIG->get('default_language'));
			$JLMS_DB->setQuery($query);
			$row->language = $JLMS_DB->loadResult();
		}
		if ($JLMS_CONFIG->get('multicat_show_admin_levels', 0)) {
			if($JLMS_CONFIG->get('flms_integration', 0)){
				$javascript = 'onclick="read_filter();" onchange="javascript:write_filter();document.adminForm.submit();"';
			} else {
				$query = "SELECT count(*) FROM `#__lms_course_cats` WHERE parent > '0'"
				. ( $query_1 ? " AND ".$query_1 : "" )
				;
				$JLMS_DB->setQuery($query);
				$count_subs = $JLMS_DB->loadResult();
				if($count_subs){
					$javascript = 'onclick="read_filter();" onchange="javascript:write_filter();document.adminForm.submit();"';
				} else {
					$javascript = '';	
				}
			}
		} else {
			$javascript = '';
		}


			
		$lists = array();
		
		$query = "SELECT * FROM `#__lms_course_cats` WHERE parent = '0'"
		. ( $query_1 ? " AND ".$query_1 : "" )
		."\n ORDER BY c_category";
		
		$JLMS_DB->setQuery($query);
		$groups = $JLMS_DB->loadObjectList();
		$type_g[] = mosHTML::makeOption( 0, _JLMS_COURSES_SELECT_GROUP );
		$i = 1;
		foreach ($groups as $group){
			$type_g[] = mosHTML::makeOption( $group->id, $group->c_category );
			$i++;
		}
		$lists['cat_id'] = mosHTML::selectList($type_g, 'cat_id', 'class="inputbox" size="1" style="width:266px;" ', 'value', 'text', $row->cat_id );

		if ($JLMS_CONFIG->get('sec_cat_use', 0) && $JLMS_CONFIG->get('sec_cat_show', 0)) {
			foreach ($type_g as $key => $l_option) {
				if ($l_option->value == $row->cat_id) {
					unset($type_g[$key]);
					break;
				}
			}
			$course_sec_cats = explode('|', $row->sec_cat);
			array_shift($course_sec_cats); array_pop($course_sec_cats);	array_shift($type_g);
			$selected_cats = array();
			foreach ($course_sec_cats as $course_sec_cat_id) {
				$selected_cats[]->value = $course_sec_cat_id;
			}
			$lists['sec_cat_id'] = mosHTML::selectList($type_g, 'sec_cat_ids[]', 'size="10" multiple="multiple" style="width:266px;" ', 'value', 'text', $selected_cats );
		}
		
		//FLMS multicategories
		$levels = array();
		if ($JLMS_CONFIG->get('multicat_use', 0)) {
			//NEW MULTICAT
			if($id){
				$tmp_level = array();
				$last_catid = 0;
				$i=0;
				foreach($_REQUEST as $key=>$item){
					if(preg_match('#level_id_(\d+)#', $key, $result)){
						if($item){
							$tmp_level[$i] = $result;
							$last_catid = $item;
							$i++;
						}	
					}	
				}
				if(!$i){
					$query = "SELECT cat_id FROM #__lms_courses WHERE id = '".$id."'";
					$JLMS_DB->setQuery($query);
					$last_catid = $JLMS_DB->loadResult();	
				}
				
				$tmp = array();
				$tmp = JLMS_multicats($last_catid, $tmp);
				$tmp = array_reverse($tmp);
				
				$tmp_pop = $tmp;
				$tmp_p = array_pop($tmp_pop);
				if(count($tmp) && $tmp_p->catid){
					$next = count($tmp);
					$tmp[$next] = new stdClass();
					$tmp[$next]->catid = 0;
					$tmp[$next]->parent = $tmp_p->catid;
				}
			} else {
				$tmp_level = array();
				$last_catid = 0;
				$i=0;
				foreach($_REQUEST as $key=>$item){
					if(preg_match('#level_id_(\d+)#', $key, $result)){
						if($item){
							$tmp_level[$i] = $result;
							$last_catid = $item;
							$i++;
						}
					}	
				}
				
				$tmp = array();
				$tmp = JLMS_multicats($last_catid, $tmp);
				$tmp = array_reverse($tmp);
				
				$tmp_pop = $tmp;
				$tmp_p = array_pop($tmp_pop);
				if(count($tmp) && $tmp_p->catid){
					$next = count($tmp);
					$tmp[$next] = new stdClass();
					$tmp[$next]->catid = 0;
					$tmp[$next]->parent = isset($tmp_p->catid)?$tmp_p->catid:0;
				}
			}
			$query = "SELECT * FROM #__lms_course_cats_config ORDER BY id";
			$JLMS_DB->setQuery($query);
			$levels = $JLMS_DB->loadObjectList();
			if(count($levels) == 0){
				for($i=0;$i<5;$i++){
					$levels[$i] = new stdClass();
					if($i > 0){
						$levels[$i]->cat_name = _JLMS_COURSES_COURSES_GROUPS;	
					} else {
						$levels[$i]->cat_name = _JLMS_COURSES_COURSES_GROUPS;
					}
				}
			}
			for($i=0;$i<count($levels);$i++){
				if(isset($tmp[$i])){
					$query = "SELECT * FROM `#__lms_course_cats` WHERE parent = '".$tmp[$i]->parent."'"
					. ( $query_1 ? " AND ".$query_1 : "" )
					."\n ORDER BY c_category";
					
					$JLMS_DB->setQuery($query);
					$groups = $JLMS_DB->loadObjectList();
					
					if($tmp[$i]->parent && $i > 0 && count($groups)){
						$type_level[$i][] = mosHTML::makeOption( 0, '&nbsp;' );
						
						foreach ($groups as $group){
							$type_level[$i][] = mosHTML::makeOption( $group->id, $group->c_category );
						}
						$lists['level_'.$i.''] = mosHTML::selectList($type_level[$i], 'level_id_'.$i, 'class="inputbox" size="1" style="width:266px;" '.$javascript, 'value', 'text', $tmp[$i]->catid );
					} elseif($i == 0) {
						$type_level[$i][] = mosHTML::makeOption( 0, ' - Select '.$levels[$i]->cat_name.' - ' );
					
						foreach ($groups as $group){
							$type_level[$i][] = mosHTML::makeOption( $group->id, $group->c_category );
						}
						$lists['level_'.$i.''] = mosHTML::selectList($type_level[$i], 'level_id_'.$i, 'class="inputbox" size="1" style="width:266px;" '.$javascript, 'value', 'text', $tmp[$i]->catid );
					}
				}
			}
			//NEW MULTICAT
		}

		if($JLMS_CONFIG->get('flms_integration', 0)){
			$query = "SELECT * FROM #__lmsf_conditions_air WHERE published = '1'";
			$JLMS_DB->setQuery($query);
			$frows = $JLMS_DB->loadObjectList();
			$query = "SELECT * FROM #__lmsf_course_conditions WHERE course_id = '".$id."'";
			$JLMS_DB->setQuery($query);
			$fvalues = $JLMS_DB->loadObjectList();
			$i=0;
			foreach($frows as $frow){
				foreach($fvalues as $fvalue){
					if($frow->id == $fvalue->condition_id){
						$frows[$i]->condition_value = $fvalue->condition_value;
						$i++;
					}	
				}	
			}
			$lists['conditions'] = $frows;
		}
		$lists['lesson_type'] = 0;
		if(!$id){
			$lists['lesson_type'] = 0;
			if(isset($tmp[0]) && $tmp[0]->catid){
				$query = "SELECT lesson_type FROM #__lms_course_cats WHERE id = '".$tmp[0]->catid."'"
				. ( $query_1 ? " AND ".$query_1 : "" )
								;
				$JLMS_DB->setQuery($query);
				$lists['lesson_type'] = $JLMS_DB->loadResult();
			}
		} else {
			if(isset($tmp[0]) && $tmp[0]->catid)
			{
				$query = "SELECT lesson_type FROM #__lms_course_cats WHERE id = '".$tmp[0]->catid."'"
				. ( $query_1 ? " AND ".$query_1 : "" )
				;
				$JLMS_DB->setQuery($query);
				$lists['lesson_type'] = $JLMS_DB->loadResult();
			} else {
				if( $JLMS_CONFIG->get('flms_integration', 0) ) 
				{
					$query = "SELECT type_lesson FROM #__lmsf_courses WHERE course_id = '".$id."'";
					$JLMS_DB->setQuery($query);
					$lists['lesson_type'] = $JLMS_DB->loadResult();
				}
			}
		}
		//FLMS multicategories

		$query = "SELECT * FROM `#__lms_languages` WHERE published = 1";
		$JLMS_DB->setQuery($query);
		$lang_all = $JLMS_DB->loadObjectList();
		$i = 1;
		foreach ($lang_all as $lang_one){
			$lang[] = mosHTML::makeOption( $lang_one->id, $lang_one->lang_name );
			$i++;
		}
		$lists['language'] = mosHTML::selectList($lang, 'course_language', 'class="inputbox jlms-input-midle" size="1" ', 'value', 'text', $row->language );
		$forum_enabled = false;
		if ($JLMS_CONFIG->get('plugin_forum')) {
			$forum_enabled = true;
		} else {
			$row->add_forum = 0;
		}
		$lists['add_forum'] = mosHTML::yesnoRadioList( 'add_forum', 'class="inputbox  input-mini" '.((!$forum_enabled)?'disabled="disabled" ':''), $row->add_forum, _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);
		$lists['add_chat'] = mosHTML::yesnoRadioList( 'add_chat', 'class="inputbox  input-mini" ', $row->add_chat, _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);
		$lists['add_hw'] = mosHTML::yesnoRadioList( 'add_hw', 'class="inputbox  input-mini" ', $row->add_hw, _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);
		$lists['add_attend'] = mosHTML::yesnoRadioList( 'add_attend', 'class="inputbox  input-mini" ', $row->add_attend, _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);
		$lists['self_reg'] = mosHTML::yesnoRadioList( 'self_reg', 'class="inputbox  input-mini" ', $row->self_reg, _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);
		$lists['published'] = mosHTML::yesnoRadioList( 'published', 'class="inputbox input-mini" ', $row->published, _JLMS_YES_ALT_TITLE, _JLMS_NO_ALT_TITLE);
		
		$lists['pay_type'] = mosHTML::yesnoRadioList( 'paid', 'class="inputbox jlms-input-midle" ', $row->paid, _JLMS_COURSES_PAID, _JLMS_COURSES_FREE);		

		/* 27.02.2007 + usergroup access (paid request) */
		// ensure user can't add group higher than themselves
		$acl = & JLMSFactory::getJoomlaACL();
		$gtree = array();
		$gtree[] = mosHTML::makeOption( 0, _JLMS_COURSES_DEFAULT_ACCESS );
		$gtree = array_merge($gtree, $acl->get_group_children_tree( null, 'USERS', false ) );

		$s_gid = array(0);
		if ($row->gid) {
			$s_gid = explode( ",", $row->gid );
		}
		$selected = array();
		foreach ($s_gid as $o) {
			$s = new stdClass();
			$s->value = $o;
			$selected[] = $s;
		}

		$lists['gid'] = mosHTML::selectList( $gtree, 'gid[]', 'size="10" multiple="multiple"', 'value', 'text', $selected );
		/* End of usergroups list*/
		
		//Course Properties Event//
		$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
		$_JLMS_PLUGINS->loadBotGroup('system');
		$plugin_args = array();
		$plugin_args[] = $row;
		$lists['plugin_return'] = $_JLMS_PLUGINS->trigger('onShowCourseProperties', $plugin_args);
		
		$all_fields_tmp = $lists['plugin_return'];
		$lists['plugin_return'] = array();
		foreach($all_fields_tmp as $fields_tmp){
			foreach($fields_tmp as $field_tmp){
				$lists['plugin_return'][] = $field_tmp;
			}
		}
		//Course Properties Event//
		
		JLMS_course_html::editCourse( $row, $lists, $option, $is_inside, $levels );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses") );
	}
}
function JLMS_saveCourse( $option ) {
	global $my, $Itemid, $JLMS_CONFIG, $JLMS_DB;
		
	$id = $JLMS_CONFIG->get('course_id', 0);
	
	$JLMS_ACL = JLMSFactory::getACL();
	
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$_JLMS_PLUGINS->loadBotGroup('system');
	
	if ( ($id && $JLMS_ACL->CheckPermissions('course', 'edit') ) || (!$id && $JLMS_ACL->CheckPermissions('lms', 'create_course')) ) {

		$pre_id = $id;

		if ($JLMS_CONFIG->get('multicat_use', 0)) {
			//OLD
//			$_POST['cat_id'] = $_POST['level_id_0'];	
			//OLD
					
			//NEW version multicat
			$tmp_level = array();
			$last_catid = 0;
			$i=0;
			foreach($_REQUEST as $key=>$item){
				if(preg_match('#level_id_(\d+)#', $key, $result)){
					if($item){
						$tmp_level[$i] = $result;
						$last_catid = $item;
						$i++;
					}	
				}	
			}
			$_POST['cat_id'] = $last_catid;
			//NEW version multicat
		}
		
		//(Max): Redirect course details new course
		$redirect_details = 0;
		if(isset($_POST['id']) && !$_POST['id']){
			$redirect_details = 1;	
		}

		$row = new mos_Joomla_LMS_Course( $JLMS_DB );
		$row->publish_start = 0;
		$row->publish_end = 0;
		if (!$row->bind( $_POST )) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		
		$row->language = JRequest::getInt('course_language', 'english');		
		
		if(!$JLMS_CONFIG->get('show_course_fee_property', 1)){
			unset($row->paid);
		}
		unset($row->spec_reg);
		/* 27.02.2007 + usergroup access (paid request) */
		$row->gid = array(0);
		$gid = mosGetParam($_POST, 'gid', array(0));
		$gid_real = array();
		foreach ($gid as $g) {
			$gid_real[] = $g;
		}
		if (in_array(0,$gid_real)) { $gid_real = array(0); }
		$row->gid = implode(",", $gid_real);
		if(!$JLMS_CONFIG->get('show_course_access_property', 1)){
			unset($row->gid);
		}
		/* End of usergroups access */
		$row->start_date = JLMS_dateToDB($row->start_date);
		$row->end_date = JLMS_dateToDB($row->end_date);
		unset($row->course_price);
		//$row->course_name = strval(mosGetParam($_REQUEST, 'course_name', ''));

		// 18 June 2007 - Apostrophe BUG fix
		$course_name_post = isset($_REQUEST['course_name'])?strval($_REQUEST['course_name']):'course_name';
		$course_name_post = (get_magic_quotes_gpc()) ? stripslashes( $course_name_post ) : $course_name_post;
		$course_name_post = ampReplace(strip_tags($course_name_post));
		$row->course_name = $course_name_post;

		$row->course_description = strval(JLMS_getParam_LowFilter($_POST, 'course_description', ''));
		$row->course_description = JLMS_ProcessText_LowFilter($row->course_description);

		$row->course_sh_description = strval(JLMS_getParam_LowFilter($_POST, 'course_sh_description', ''));
		$row->course_sh_description = JLMS_ProcessText_LowFilter($row->course_sh_description);

		$row->metadesc = JLMS_ProcessText_HardFilter( $row->metadesc );
		$row->metakeys = JLMS_ProcessText_HardFilter( $row->metakeys );

		if(!$JLMS_CONFIG->get('show_course_meta_property', 1)){
			unset($row->metakeys);
			unset($row->metadesc);
		}
		if (!$id) {
			$row->owner_id = $my->id;
		} else {
			if (isset($row->owner_id)) { unset($row->owner_id); }
		}

		if ($JLMS_CONFIG->get('sec_cat_use', 0) && $JLMS_CONFIG->get('sec_cat_show', 0)) {
			$row->sec_cat = '|'.implode('|', josGetArrayInts( 'sec_cat_ids' )).'|';
		}
		
		
		//Course Properties Event//
		$plugin_args = array();
		$plugin_args[] = $row;
		$lists['plugin_return'] = $_JLMS_PLUGINS->trigger('onSaveCourseProperties', $plugin_args);
		if(isset($lists['plugin_return']) && count($lists['plugin_return'])){
			$fields = $lists['plugin_return'];
			
			if(isset($row->id) && $row->id){
				$row_p = new mos_Joomla_LMS_Course( $JLMS_DB );
				$row_p->load($row->id);
			}
			
			$params = strlen($row_p->params) ? explode("\n", $row_p->params) : array();
			
			$params_check = array();
			$i=0;
			foreach($params as $param){
				preg_match('#(.*)=(.*)#', $param, $out);
				
				if(isset($out[0]) && strlen($out[0]) && $out[1] && isset($out[2])){
					$params_check[$i]->name = $out[1];
					$params_check[$i]->value = $out[2];
					$i++;
				}
			}
			
			$all_fields_tmp = $fields;
			$fields = array();
			foreach($all_fields_tmp as $fields_tmp){
				foreach($fields_tmp as $field_tmp){
					$fields[] = $field_tmp;
				}
			}
			
			for($i=0;$i<count($fields);$i++){
				$field = $fields[$i];
				
				$triger = true;
				$params_check = array();
				foreach($params_check as $param_ch){
					if(strtolower($field->name) == $param_ch->name){
						if($field->value == $param_ch->value){
							$triger = false;
							break;
						} else {
							$triger = false;
							$param_ch->value = $field->value;
							break;
						}
					}
				}
				
				$params = array();
				foreach($params_check as $param_ch){
					$params[] = strtolower($param_ch->name).'='.$param_ch->value;
				}
				
				if($triger && strlen($field->name)){
					$params[] = strtolower($field->name).'='.$field->value;
				}
				
			}
			
			$row->params = count($params) ? implode("\n", $params) : $row->params;
		}
		//Course Properties Event//

		if (!$row->check()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if ($JLMS_CONFIG->get('plugin_forum') == 1){
		} else {
			$row->add_forum = 0;
		}
		if (!$row->store()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		$new_course_id = $row->id;
		
		$plugin_args = array();
		$plugin_args[] = $row;
		$lists['plugin_return'] = $_JLMS_PLUGINS->trigger('onAfterSaveCourseProperties', $plugin_args);

		$course_id = $row->id;
		if (!$pre_id) {
			//Roles
			$JLMS_ACL = JLMSFactory::getACL();
			if ($JLMS_ACL->isAdmin()) {
				$default_role = $JLMS_ACL->defaultRole(2);
			} else {
				$default_role = $JLMS_ACL->GetRole(2);
			}
			if ($default_role) {
				$query = "INSERT INTO `#__lms_user_courses` (user_id, course_id, role_id) VALUES ('".$my->id."', '".$new_course_id."', '".$default_role."')";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
			} else {
				global $JLMS_SESSION;
				$JLMS_SESSION->set('joomlalms_sys_message', 'Failed to assign teacher role');
			}
		}
		
		// FLMS mod (Max)
		if($JLMS_CONFIG->get('flms_integration', 0)){
			FLMS_save_params_lesson($new_course_id);
			FLMS_cndts_aircraft_in_course_save($new_course_id);
			FLMS_r_inst_in_course_save($new_course_id);
			FLMS_r_stu_in_course_save($new_course_id);
		}
//		if ($JLMS_CONFIG->get('multicat_use', 0) && $JLMS_CONFIG->get('multicat_show_admin_levels', 0)) {
//			FLMS_save_multicat($new_course_id);	
//		}

		$add_forum = intval(mosGetParam($_REQUEST, 'add_forum', 0));			
		
		$query = "UPDATE #__lms_forum_details SET is_active = ".$add_forum.", need_update = 1 WHERE course_id = $course_id";
		$JLMS_DB->setQuery($query);
		if( $JLMS_DB->query() ) 
		{	
			$forum = & JLMS_SMF::getInstance();
			if( $forum ) 	
				$forum->applyChanges( $course_id );
		} 		
		
		$query = "UPDATE #__lms_usergroups SET group_forum = ".$add_forum." WHERE course_id = $course_id";
		$JLMS_DB->setQuery($query);
		$JLMS_DB->query();
		
		//(Max): Redirect course details new course
		if($redirect_details){
			JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$row->id"));
		}
		$is_inside = intval(mosGetParam($_REQUEST, 'is_inside', 0));
		if ($is_inside == 1) {
			JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$id"));
		} else {
			JLMSRedirect( sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=courses") );
		}
	}
}
function JLMS_cancelCourse( $option ) {
	global $Itemid;
	$is_inside = intval(mosGetParam($_REQUEST, 'is_inside'), 0);
	if ($is_inside == 1) {
		$id = intval(mosGetParam($_REQUEST, 'id'), 0);
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$id"));
	} else {
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses"));
	}
}
function JLMS_showCourseDetails( $option ) {
	$user = JLMSFactory::getUser();
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG, $JLMS_SESSION;
	
	//require('import_csv.php');
	
	$short = mosGetParam($_REQUEST, 'short', 0); //not display list topics
	
	$id = $JLMS_CONFIG->get('course_id',0);

	$course_params = $JLMS_CONFIG->get('course_params');
	$params = new JLMSParameters($course_params);

	$query = "SELECT * FROM `#__lms_courses` WHERE id = '".$id."'";
	$JLMS_DB->SetQuery( $query );
	$course_rows = $JLMS_DB->LoadObjectList();   
		 	
	if (count($course_rows)) {
		$row = $course_rows[0];

		$JLMS_ACL = JLMSFactory::getACL();

		$do_embed_sqbox_redirect = false;
		$embed_sqbox_link = "";
		$embed_sqbox_x_size = 800;
		$embed_sqbox_y_size = 600;
		$embed_sqbox_link_name = "";

		// Redirect to main Learning Path
		if ($id && $params->get('lpath_redirect') && $JLMS_ACL->GetRole(1) && $params->get('learn_path')){
			$lpath_id = intval($params->get('learn_path'));
			$query = "SELECT id, item_id, lp_type, lpath_name FROM #__lms_learn_paths WHERE id = $lpath_id AND course_id = $id AND published = 1";
			$JLMS_DB->SetQuery($query);
			$lpath_rows = $JLMS_DB->LoadObjectList();
			if (count($lpath_rows)) {
				$lpath = $lpath_rows[0];
				if (!$JLMS_SESSION->get('redirect_to_learnpath')) {
					$JLMS_SESSION->set('redirect_to_learnpath',1);
					$lp_task = 'show_lpath';
					$lp_id = $lpath->id;
					if ($lpath->item_id) {
						if ($lpath->lp_type == 1) {
							$query = "SELECT width, height, params FROM #__lms_n_scorm WHERE id = ".$lpath->item_id;
							$JLMS_DB->SetQuery($query);
							$scorm_params_data_obj_list = $JLMS_DB->LoadObjectList();
							if (count($scorm_params_data_obj_list)) {
								$scorm_params_data = $scorm_params_data_obj_list[0]->params;
								$scorm_params = new JLMSParameters($scorm_params_data);
								if ($scorm_params->get('scorm_layout') == 1) {
									$embed_sqbox_link_name = $lpath->lpath_name;
									//don't do redirect to the scorm with SQ-box enabled
									// -> open course homepage and automatically run SQ-box instead.
									if (isset($scorm_params_data_obj_list[0]->width)) {
										$embed_sqbox_x_size = $scorm_params_data_obj_list[0]->width;
									}
									if (isset($scorm_params_data_obj_list[0]->height)) {
										$embed_sqbox_y_size = $scorm_params_data_obj_list[0]->height;
									}
								 	$do_embed_sqbox_redirect = true;
								 	$embed_sqbox_link = sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=$lp_task&amp;course_id=$id&amp;id=$lp_id");
								 	JLMS_initialize_SqueezeBox_AutoLoad();
								 	$JLMS_SESSION->clear('redirect_to_learnpath');
								}
							}
						}
					}
					if (!$do_embed_sqbox_redirect) {
						JLMSRedirect(sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=$lp_task&amp;course_id=$id&amp;id=$lp_id"));
					}
				}
			}
		} elseif ($id && $params->get('lpath_redirect') && $JLMS_ACL->GetRole(1)) { // redirect to the list of learning paths
			if (!$JLMS_SESSION->get('redirect_to_learnpath')) {
				$JLMS_SESSION->set('redirect_to_learnpath',1);
				$lp_task = 'learnpaths';
				JLMSRedirect(sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=$lp_task&amp;id=$id"));
			}
		}

		$doc = JFactory::getDocument();
		$doc->setMetaData( 'description', $row->metadesc );
		$doc->setMetaData( 'keywords', $row->metakeys );
	
		$JLMS_ACL = JLMSFactory::getACL();
		$assigned_groups_only = $JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only');
		
		$lists = array();
		
		$lists['short'] = $short;
		
		if ( $id && $JLMS_ACL->GetRole()) {
			if (!$JLMS_ACL->isStaff()) {
				$hp_items = $JLMS_CONFIG->get('homepage_items');
				//dropbox
				$query = "SELECT count(*) FROM #__lms_dropbox WHERE course_id = $id AND recv_id = '".$my->id."'";
				$JLMS_DB->SetQuery( $query );
				$my_dropbox_total = $JLMS_DB->LoadResult();
				$query = "SELECT count(*) FROM #__lms_dropbox WHERE course_id = $id AND recv_id = '".$my->id."' AND drp_mark = 1";
				$JLMS_DB->SetQuery( $query );
				$my_dropbox_total_new = $JLMS_DB->LoadResult();
				$my_dropbox = array();
				if ($params->get('dropbox_view')) {
					$query = "SELECT a.* FROM #__lms_dropbox as a WHERE a.course_id = $id AND a.recv_id = '".$my->id."' AND a.drp_mark = 1 ORDER BY a.drp_time DESC LIMIT 0,$hp_items";
					$JLMS_DB->SetQuery( $query );
					$my_dropbox = $JLMS_DB->LoadObjectList();
				}
				$lists['dropbox_total'] = $my_dropbox_total?$my_dropbox_total:0;
				$lists['dropbox_total_new'] = $my_dropbox_total_new?$my_dropbox_total_new:0;
				$lists['my_dropbox'] = $my_dropbox;
				//homework
				$my_hw = array();
				if ($JLMS_CONFIG->get('course_homework') && $params->get('homework_view')) {
					$my_hw = my_homeworks ($id, $hp_items, $user->get('id'));
				}
				$lists['my_hw'] = $my_hw;
				//announcements
				$my_announcements = array();
				if ($params->get('agenda_view')) {
					$my_announcements = my_announcements ($id, $hp_items, $user->get('id'));
				}
				$lists['my_announcements'] = $my_announcements;
				
				$my_mailbox = array();
				if ($params->get('mailbox_view')) {
					$my_mailbox = my_mailbox($id, $hp_items, $user->get('id'));
				}
				$lists['my_mailbox'] = $my_mailbox;
				
				$my_certificates = array();
				if ($params->get('certificates_view')) {
					$my_certificates = my_certificates($id, $hp_items, $user->get('id'));
				}
				$lists['my_certificates'] = $my_certificates;
				
				$latest_forum_posts = array();
				if ($params->get('latest_forum_posts_view')) {
					$latest_forum_posts = latest_forum_posts($id, $hp_items, $user->get('id'));
				}
				$lists['latest_forum_posts'] = $latest_forum_posts;
				
			} else {
				$lists['dropbox_total'] = 0;
				$lists['dropbox_total_new'] = 0;
				$lists['my_dropbox'] = array();
				$lists['my_mailbox'] = array();
				$lists['my_certificates'] = array();
				$lists['latest_forum_posts'] = array();
			}
					
			require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.html.php");			
			if ($do_embed_sqbox_redirect) {
				$link_html = '<a href="'.$embed_sqbox_link.'" class="scorm_modal" id="scorm_sqbox_autoload" rel="{handler:\'iframe\', size:{x:'.$embed_sqbox_x_size.',y:'.$embed_sqbox_y_size.'}}" title="'.$embed_sqbox_link_name.'">'.$embed_sqbox_link_name.'</a>';
				$row->course_description = "<div style=\"visibility:hidden;width:100%\">".$link_html."</div>". $row->course_description ;
			}
			
			JLMS_course_html::showCourseDetails( $id, $row, $params, $option, $lists, 'enter' );
		} else {
			$query1 = "SELECT group_id FROM `#__lms_users_in_global_groups` WHERE user_id = '".$my->id."'";
			$JLMS_DB->setQuery($query1);
			$user_group_ids = JLMSDatabaseHelper::loadResultArray();

			$restricted_courses = JLMS_illegal_courses($user_group_ids);

			if(in_array($id,$restricted_courses)) {
				JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses"));
			}
			if (is_array($JLMS_CONFIG->get('student_in_future_courses')) && in_array($row->id, $JLMS_CONFIG->get('student_in_future_courses'))) {
				$vt = 'future_course';
				JLMS_course_html::showCourseDetails( $id, $row, $params, $option, $lists, $vt );
			}
			//TODO: check if course published
			elseif ($row->paid) {
				//course is paid so just make an offer to join
				$vt = 'view';
				$query = "SELECT COUNT(*) FROM #__lms_waiting_lists WHERE course_id = $id AND user_id = $my->id";
				$JLMS_DB->setQuery($query);
				if ($JLMS_DB->loadResult()) {
					$vt = 'inWL';
				}
				JLMS_course_html::showCourseDetails( $id, $row, $params, $option, $lists, $vt );
			} else {
				if (!$row->published) {
					JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses"));
				} elseif ($row->publish_end && strtotime($row->end_date) < strtotime(JLMS_dateToDB(JLMS_offsetDateToDisplay(gmdate("Y-m-d H:i:s"))))) {
					JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses"));
				}
				//count users in course
				$vt = 'view';
				$query = "SELECT COUNT(*) FROM #__lms_waiting_lists WHERE course_id = $id AND user_id = $my->id";
				$JLMS_DB->setQuery($query);
				if ($JLMS_DB->loadResult()) {
					$vt = 'inWL';
				} else {
					$max_attendees = $params->get('max_attendees', 0);
					if ($max_attendees) {
						$query = "SELECT COUNT(DISTINCT(user_id)) FROM `#__lms_users_in_groups` WHERE course_id = $id";// AND role_id = 2";
						$JLMS_DB->setQuery($query);
						$current_attendees = $JLMS_DB->loadResult();
						if ($current_attendees >= $max_attendees) {
							$vt = 'offerWL';
						}
					}
				}
				JLMS_course_html::showCourseDetails( $id, $row, $params, $option, $lists, $vt );
			}
		}		
	} else {		
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses"));
	}	
}
function JLMS_deleteCourse( $id, $option ) {
	global $my, $JLMS_DB, $JLMS_CONFIG, $Itemid;
	$cid = mosGetParam( $_POST, 'cid', array(0) );
	if (!is_array( $cid )) { $cid = array(0); }
	if ( isset($cid[0]) && $id && ($cid[0] == $id) && JLMS_GetUserType($my->id, $id, false, false) == 1) {
		require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_del_operations.php");
		JLMS_DelOp_deleteCourse( $id );

		if($JLMS_CONFIG->get('flms_integration', 0)){
			if (function_exists('FLMS_delete_params_lesson')) {
				FLMS_delete_params_lesson($id);
			}
		}
		if ($JLMS_CONFIG->get('multicat_use', 0)){
			if (function_exists('FLMS_delete_multicat')) {
				FLMS_delete_multicat($id);
			}	
		}

		if (JLMS_deleteFromFMS(NULL,$id, $option, false )){
			JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses"), _JLMS_COURSE_DELETED );
		}
	}else{
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses"), _JLMS_COURSE_DELETED );
	}
}

function JLMS_Course_Import_Pre( $option ) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();

	if ($JLMS_CONFIG->get('is_teacher_user')) {
		$my_courses = $JLMS_CONFIG->get('teacher_in_courses', array() );
		$do_merge = false;
		$lists = array();
		$lists['my_courses'] = '';
		if (!empty($my_courses)) {
			$mc_cid = implode(',', $my_courses);
			$query = "SELECT id as value, course_name as text FROM #__lms_courses WHERE id IN ($mc_cid) ORDER BY course_name";
			$JLMS_DB->SetQuery($query);
			$my_courses_names = $JLMS_DB->LoadObjectList();
			if (!empty($my_courses_names)) {
				$do_merge = true;
			}
			$mc = array();
			$mc[] = mosHTML::makeOption( 0, '&nbsp;' );
			$mc = array_merge($mc, $my_courses_names);
			$lists['my_courses'] = mosHTML::selectList($mc, 'merge_course', 'class="inputbox" size="1" style="width:200px"', 'value', 'text', 0 );
		}
		$lists['do_merge'] = $do_merge;
		$JLMS_DB->setQuery("SELECT tmpl.*, IFNULL(cat.restricted, 0) as restricted, cat.groups
		                        FROM #__lms_courses_template tmpl
                                    LEFT JOIN #__lms_courses c ON c.id = tmpl.course_id
                                    LEFT JOIN #__lms_course_cats cat ON cat.id = c.cat_id");

		$coursesTemplates = $JLMS_DB->loadObjectList();

        $cours = array();
        foreach($coursesTemplates as $template){
            if($template->restricted == 0){
                $tmpl = new stdClass();
                $tmpl->id = $template->id;
                $tmpl->templ = $template->templ;
                $cours[] = $tmpl;
                continue;
            }

            $templateUserGroups = explode('|', $template->groups);
            $query = "SELECT a.group_id FROM `#__lms_user_assign_groups` as a WHERE a.user_id = '".$my->id."'";
            $JLMS_DB->setQuery($query);
            $groups_where_admin_manager = JLMSDatabaseHelper::LoadResultArray();
            foreach($groups_where_admin_manager as $group){
                if(in_array($group, $templateUserGroups)){
                    $tmpl = new stdClass();
                    $tmpl->id = $template->id;
                    $tmpl->templ = $template->templ;
                    $cours[] = $tmpl;
                }
            }

        }

		$lists['template_list']  = $cours;
		JLMS_course_html::JLMS_Import($option, $lists);
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=courses") );
	}
}

function JLMS_orderCourse( $uid, $inc, $option ) {
	global $JLMS_DB, $JLMS_CONFIG, $Itemid, $my, $JLMS_SESSION;
	$JLMS_ACL = JLMSFactory::getACL();
	$usertype_simple = $JLMS_ACL->_role_type;
//	$usertype_simple = JLMS_GetUserType_simple($my->id, false, true);
	$where = '';
	$filter = intval( mosGetParam( $_REQUEST, 'courses_type', $JLMS_SESSION->get('courses_type', 0) ) );
	$JLMS_SESSION->set('courses_type', $filter);
	$filter_groups = intval( mosGetParam( $_REQUEST, 'groups_course', $JLMS_SESSION->get('groups_course', 0) ) );
	$JLMS_SESSION->set('groups_course', $filter_groups);


	$levels = array();
	if ($JLMS_CONFIG->get('multicat_use', 0)){
		$query = "SELECT * FROM #__lms_course_cats_config ORDER BY id";
		$JLMS_DB->setQuery($query);
		$levels = $JLMS_DB->loadObjectList();
		if(count($levels) == 0){
			for($i=0;$i<5;$i++){
				if($i>0){
					$levels[$i]->cat_name = _JLMS_COURSES_COURSES_GROUPS;	
				} else {
					$levels[$i]->cat_name = _JLMS_COURSES_COURSES_GROUPS;
				}
			}
		}
		
		$level_id = array();
		for($i=0;$i<count($levels);$i++){
			if(isset($_REQUEST['category_filter']) && $_REQUEST['category_filter']){
				if($i == 0){
					$level_id[$i] = $_REQUEST['category_filter'];
					$parent_id[$i] = 0;
				} else {
					$level_id[$i] = 0;	
					$parent_id[$i] = $level_id[$i-1];
				}
			} else {
				if($i == 0){
					$level_id[$i] = intval( mosGetParam( $_REQUEST, 'filter_id_'.$i.'', $JLMS_SESSION->get('FLMS_filter_id_'.$i.'', 0) ) );
					$_REQUEST['filter_id_'.$i] = $level_id[$i];
					$JLMS_SESSION->set('FLMS_filter_id_'.$i.'', $level_id[$i]);
				} else {
					$level_id[$i] = intval( mosGetParam( $_REQUEST, 'filter_id_'.$i.'', $JLMS_SESSION->get('FLMS_filter_id_'.$i.'', 0) ) );
					$_REQUEST['filter_id_'.$i] = $level_id[$i];
					$JLMS_SESSION->set('FLMS_filter_id_'.$i.'', $level_id[$i]);
				}
				if($i == 0){
					$parent_id[$i] = 0;
				} else {
					$parent_id[$i] = $level_id[$i-1];
				}
				$query = "SELECT count(id) FROM `#__lms_course_cats` WHERE parent = '".$parent_id[$i]."' ORDER BY c_category";
				$JLMS_DB->setQuery($query);
				$groups = $JLMS_DB->loadResult();
				if($groups==0){
					$level_id[$i] = 0;	
					$JLMS_SESSION->set('FLMS_filter_id_'.$i.'', $level_id[$i]);
				}
			}
		}
		
		for($i=0;$i<count($levels);$i++){
			if($i > 0 && $level_id[$i - 1] == 0){
				$level_id[$i] = 0;
				$_REQUEST['filter_id_'.$i] = $level_id[$i];
				$JLMS_SESSION->set('FLMS_filter_id_'.$i.'', $level_id[$i]);
				$parent_id[$i] = 0;
			} elseif($i == 0 && $level_id[$i] == 0) {
				$level_id[$i] = 0;
				$_REQUEST['filter_id_'.$i] = $level_id[$i];
				$JLMS_SESSION->set('FLMS_filter_id_'.$i.'', $level_id[$i]);
				$parent_id[$i] = 0;
			}
		}
	}

	$where = '';
//	if ($usertype_simple == 1 || $usertype_simple == 5) { //(Max): role_id
	if ($usertype_simple == 2 || $usertype_simple == 4) { //(Max): roletype_id
		if ($filter){
			if ($filter == 1){
				$where .= " owner_id = ".$my->id;
			}
			elseif($filter == 2){
				$where .= " owner_id != ".$my->id;
			}
		}
		if ($JLMS_CONFIG->get('multicat_use', 0)){
			//NEW MUSLTICATS
//			$tmp_level = array();
			$last_catid = 0;
			
			$tmp_cats_filter = JLMS_getFilterMulticategories($last_catid);
			/*
			if(isset($_REQUEST['category_filter']) && $_REQUEST['category_filter']){
				$last_catid = $_REQUEST['category_filter'];
			} else {
				$i=0;
				foreach($_REQUEST as $key=>$item){
					if(preg_match('#filter_id_(\d+)#', $key, $result)){
						if($item){
							$tmp_level[$i] = $result;
							$last_catid = $item;
							$i++;
						}	
					}	
				}
			}
			
			$query = "SELECT * FROM #__lms_course_cats ORDER BY id";
			$JLMS_DB->setQuery($query);
			$all_cats = $JLMS_DB->loadObjectList();
			
			$tmp_cats_filter = array();
			$children = array();
			foreach($all_cats as $cat){
				$pt = $cat->parent;
				$list = @$children[$pt] ? $children[$pt] : array();
				array_push($list, $cat->id);
				$children[$pt] = $list;
			}
			$tmp_cats_filter[0] = $last_catid;
			$i=1;
			foreach($children as $key=>$childs){
				if($last_catid == $key){
					foreach($children[$key] as $v){
						if(!in_array($v, $tmp_cats_filter)){
							$tmp_cats_filter[$i] = $v;
							$i++;
						}
					}
				}
			}
			foreach($children as $key=>$childs){
				if(in_array($key, $tmp_cats_filter)){
					foreach($children[$key] as $v){
						if(!in_array($v, $tmp_cats_filter)){
							$tmp_cats_filter[$i] = $v;
							$i++;
						}
					}
				}
			}
			$tmp_cats_filter = array_unique($tmp_cats_filter);
			*/
			$catids = implode(",", $tmp_cats_filter);
			
			if($last_catid && count($tmp_cats_filter)){
				$where .= "\n ( cat_id IN (".$catids.")";
				if($JLMS_CONFIG->get('sec_cat_use', 0)){
					foreach ($tmp_cats_filter as $tmp_cats_filter_one) {
						$where .= "\n OR sec_cat LIKE '%|".$tmp_cats_filter_one."|%'";
					}
				}
				$where .= "\n )";
			}
			//NEW MUSLTICATS
			
			//old
			/*
			if($level_id[0]){
				if($filter){
					$where .= " AND";	
				}
				$where .= " cat_id = '".$level_id[0]."' ";
			}
			$other_cat_ids = array();
			for($i=1;$i<count($level_id);$i++){
				if($level_id[$i]){
					$other_cat_ids[] = $level_id[$i];	
				}
			}
			$other_cat_id = '';
			if(count($other_cat_ids) > 0){
				$other_cat_id = implode(",", $other_cat_ids);	
			
				$query = "SELECT course_id, cat_id, level FROM #__lms_course_level WHERE cat_id IN (".$other_cat_id.")";
				$JLMS_DB->setQuery($query);
				$c_list = $JLMS_DB->loadObjectList();
				
				$c_result = array();
				foreach($c_list as $data){
					if($data->level == count($other_cat_ids)){
						$c_result[] = $data->course_id;
					}	
				}
				
				$course_subs = implode(",", $c_result);
				$where .= " AND id IN (".$course_subs.") ";
			}
			*/
		} else {
			if ($filter_groups){
				if ($JLMS_CONFIG->get('sec_cat_use', 0)) {
					$where .= " AND (cat_id = '$filter_groups' OR sec_cat LIKE '%|$filter_groups|%') ";
				} else {
					$where .= " AND cat_id = '$filter_groups' ";
				}
			}
		}
		
		$row = new mos_Joomla_LMS_Course( $JLMS_DB );
		$row->load( $uid );
		$row->move( $inc, $where);
		
		//show paid courses (admin settings)
		//$show_paid_courses = $JLMS_CONFIG->get('show_paid_courses', 1);
		//$where .= $show_paid_courses ? '' : ' AND a.paid <> 1 ';
//	} elseif ($usertype_simple == 2) { //(Max): role_id
	} elseif ($usertype_simple == 1) { //(Max): roletype_id
		
		$restricted_courses = JLMS_illegal_courses($user_group_ids);

		$restricted_courses = implode(',', $restricted_courses);
			
		if($restricted_courses == '')
			$restricted_courses = "'0'";
			
		if ($filter){
			$query = "SELECT course_id FROM `#__lms_users_in_groups` WHERE user_id = '".$my->id."'";
			$JLMS_DB->setQuery($query);
			$id = JLMSDatabaseHelper::loadResultArray();
			$id = implode(',', $id);
			if ($filter == 1){
				$where = " AND a.id IN (".$id.") ";
			}
			elseif($filter == 2){
				$where = " AND a.id NOT IN ( ".$id.") " ;
			}
		}
		if ($JLMS_CONFIG->get('multicat_use', 0)){
			//NEW MUSLTICATS
//			$tmp_level = array();
			$last_catid = 0;
			
			$tmp_cats_filter = JLMS_getFilterMulticategories($last_catid);
			/*
			if(isset($_REQUEST['category_filter']) && $_REQUEST['category_filter']){
				$last_catid = $_REQUEST['category_filter'];
			} else {
				$i=0;
				foreach($_REQUEST as $key=>$item){
					if(preg_match('#filter_id_(\d+)#', $key, $result)){
						if($item){
							$tmp_level[$i] = $result;
							$last_catid = $item;
							$i++;
						}	
					}	
				}
			}
			
			$query = "SELECT * FROM #__lms_course_cats ORDER BY id";
			$JLMS_DB->setQuery($query);
			$all_cats = $JLMS_DB->loadObjectList();
			
			$tmp_cats_filter = array();
			$children = array();
			foreach($all_cats as $cat){
				$pt = $cat->parent;
				$list = @$children[$pt] ? $children[$pt] : array();
				array_push($list, $cat->id);
				$children[$pt] = $list;
			}
			$tmp_cats_filter[0] = $last_catid;
			$i=1;
			foreach($children as $key=>$childs){
				if($last_catid == $key){
					foreach($children[$key] as $v){
						if(!in_array($v, $tmp_cats_filter)){
							$tmp_cats_filter[$i] = $v;
							$i++;
						}
					}
				}
			}
			foreach($children as $key=>$childs){
				if(in_array($key, $tmp_cats_filter)){
					foreach($children[$key] as $v){
						if(!in_array($v, $tmp_cats_filter)){
							$tmp_cats_filter[$i] = $v;
							$i++;
						}
					}
				}
			}
			$tmp_cats_filter = array_unique($tmp_cats_filter);
			*/
			$catids = implode(",", $tmp_cats_filter);
			
			if($last_catid && count($tmp_cats_filter)){
				$where .= "\n AND ( a.cat_id IN (".$catids.")";
				if($JLMS_CONFIG->get('sec_cat_use', 0)){
					foreach ($tmp_cats_filter as $tmp_cats_filter_one) {
						$where .= "\n OR a.sec_cat LIKE '%|".$tmp_cats_filter_one."|%'";
					}
				}
				$where .= "\n )";
			}
			//NEW MUSLTICATS
			
			//old
			/*
			if($level_id[0]){
				$where .= " AND a.cat_id = '".$level_id[0]."' ";
			}
			$other_cat_ids = array();
			for($i=1;$i<count($level_id);$i++){
				if($level_id[$i]){
					$other_cat_ids[] = $level_id[$i];	
				}
			}
			$other_cat_id = '';
			if(count($other_cat_ids) > 0){
				$other_cat_id = implode(",", $other_cat_ids);	
			
				$query = "SELECT course_id, cat_id, level FROM #__lms_course_level WHERE cat_id IN (".$other_cat_id.")";
				$JLMS_DB->setQuery($query);
				$c_list = $JLMS_DB->loadObjectList();
				
				$c_result = array();
				foreach($c_list as $data){
					if($data->level == count($other_cat_ids)){
						$c_result[] = $data->course_id;
					}	
				}
				
				$course_subs = implode(",", $c_result);
				$where .= " AND a.id IN (".$course_subs.") ";
			}
			*/
		} else {
			if ($filter_groups){
				if ($JLMS_CONFIG->get('sec_cat_use', 0)) {
					$where .= " AND (a.cat_id = '$filter_groups' OR a.sec_cat LIKE '%|$filter_groups|%') ";
				} else {
					$where .= " AND a.cat_id = '$filter_groups' ";
				}
			}
		}
		
		$my_course_ids = JLMS_GetUserCourses_IDs( $my->id, $usertype_simple, true );
		if (!count($my_course_ids)) { $my_course_ids = array(0); }
		$mc_cid = implode(',', $my_course_ids);

			$query = "SELECT a.*, b.username, b.email, b.name as user_fullname, a.id as course_id, d.c_category, wl.id AS in_wl"
//		. "\n EXISTS(SELECT id FROM `#__lms_waiting_lists` AS `wl` WHERE wl.user_id=$my->id AND wl.course_id=a.id) AS in_wl"
		. "\n FROM `#__lms_courses` as a "
		. "\n LEFT JOIN `#__lms_course_cats` as d ON d.id = a.cat_id "
		. "\n LEFT JOIN `#__lms_waiting_lists` as wl ON wl.course_id = a.id AND wl.user_id=$my->id,"
		. "\n LEFT JOIN `#__users` as b ON a.owner_id = b.id"		
		. "\n WHERE 1 = 1"
		. "\n AND ( ( a.published = 1"
		. "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )"
		. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d')."')) OR (a.publish_end = 0) )"
		. "\n AND a.id NOT IN ($restricted_courses)) OR (a.id IN ($mc_cid)) )"
		//. "\n AND d.id = a.cat_id $where "
		. "\n $where "
		. "\n ORDER BY ".($JLMS_CONFIG->get('lms_courses_sortby',0)?"a.ordering, a.course_name, a.id":"a.course_name, a.ordering, a.id")
		;

		$JLMS_DB->SetQuery( $query );
		$row = $JLMS_DB->LoadObjectList();
		$row->load( $uid );
	 	$row->move( $inc, $where);
	}	
	mosRedirect( 'index.php?option='. $option ."&Itemid=$Itemid&task=courses" );
}
function JLMS_course_saveOrder( &$cid, $option) {
	global $JLMS_DB, $Itemid;

	//josSpoofCheck();
	$order 		= josGetArrayInts( 'order' );

	$total		= count( $cid );

	$row		= new mos_Joomla_LMS_Course( $JLMS_DB );
	$conditions = array();

	// update ordering values
	for( $i=0; $i < $total; $i++ ) {
		$row->load( (int) $cid[$i] );
		if ($row->ordering != $order[$i]) {
			$row->ordering = $order[$i];
			if (!$row->store()) {
				echo "<script> alert('ERROR! CODE: COURSE_ORDER_001'); window.history.go(-1); </script>\n";
				exit();
			}
			// remember to updateOrder this group
			$condition = "";
			$found = false;
			
			if (!$found) $conditions[] = array($row->id, $condition);
		}
	}

	// execute updateOrder for each group
	foreach ( $conditions as $cond ) {
		$row->load( $cond[0] );
		$row->updateOrder( $cond[1] );
	}

	$msg 	= 'New ordering saved';//TODO: translation
	mosRedirect( 'index.php?option='. $option ."&Itemid=$Itemid&task=courses" , $msg );
}
?>