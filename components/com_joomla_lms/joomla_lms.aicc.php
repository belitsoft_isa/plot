<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

define('AICC_COMMANDS_PATH', _JOOMLMS_FRONT_HOME . '/includes/n_scorm/aicc/Commands/');

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Cache-Control: no-cache");
header("Pragma: no-cache");

require_once(_JOOMLMS_FRONT_HOME . "/includes/n_scorm/aicc/functions.php");
require_once(_JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.class.php");
require_once(_JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.lib.php");
require_once(_JOOMLMS_FRONT_HOME . "/includes/n_scorm/datamodels/aicclib.php");

foreach ($_POST as $key => $value) {
    $tempkey = strtolower($key);
    $_POST[$tempkey] = $value;
}

$command = mosGetParam($_REQUEST, 'command');
$sessionId = mosGetParam($_REQUEST, 'session_id', '');
$aiccData = mosGetParam($_REQUEST, 'aicc_data', '');

if (empty($command)) {
    echo "error=1\r\nerror_text=Invalid Command\r\n";
    die();
}

if(!checkSessionKey($sessionId, $my)){
    echo "error=3\r\nerror_text=Invalid Session ID\r\n";
    die();
}

$command = strtolower($command);

$scoid = $JLMS_SESSION->get('scorm_scoid');

if (is_null($scoid)) {
    error('Invalid script call');
}
$mode = $JLMS_SESSION->get('scorm_mode');
$mode = !is_null($mode)? $mode : 'normal';

$status = $JLMS_SESSION->get('scorm_status');
$status = !is_null($status)? $status : 'Not Initialized';

$attempt = $JLMS_SESSION->get('scorm_attempt');
$attempt = !is_null($attempt)? $attempt : 1;

$sco = scorm_get_sco($scoid, SCO_ONLY);
if(!$sco){
    error('Invalid script call');
}

if ($sco->launch == '') {
    // Search for the next launchable sco
    $query = "SELECT * FROM #__lms_n_scorm_scoes WHERE scorm = $sco->scorm AND launch <> '' AND id > $sco->id ORDER BY id ASC";
    $JLMS_DB->SetQuery($query);
    $scoes = $JLMS_DB->LoadObjectList();
    if (!empty($scoes)) {
        $sco = current($scoes);
    }
}

if (empty($sco)) {
    error('Invalid script call');
}
// load class with necessary command
if(!file_exists(AICC_COMMANDS_PATH . $command . "Command.php")){
    $command = "Default";
}
$classname = $command."Command";
require_once AICC_COMMANDS_PATH . $classname . ".php";

if(!class_exists($classname)){
    die("error=1\r\nerror_text=Invalid Command\r\n");
}

// creating object of selected command class
$class = new ReflectionClass($classname);
try{
    $commandObject = $class->newInstance(array(
        'status' => $status,
        'JLMS_SESSION' => $JLMS_SESSION,
        'JLMS_DB' => $JLMS_DB,
        'scoid' => $scoid,
        'user' => $my,
        'mode' => $mode,
        'aiccData'=> $aiccData,
        'sco' => $sco,
        'attempt' => $attempt,
        'enabled' => $JLMS_CONFIG->get('enable_aicc')
    ));
}
catch (Exception $e){
    error('Wrong command');
}

$commandObject->run();
die();