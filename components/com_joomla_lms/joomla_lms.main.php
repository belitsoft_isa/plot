<?php
/**
* joomla_lms.main.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

define('JLMS_SELECTED_INDEX_MARKER', '{selectedindex}');

//require_once(_JOOMLMS_FRONT_HOME. "/includes/config.inc.php");
require_once(_JOOMLMS_FRONT_HOME. "/includes/libraries/lms.lib.func.php");
require_once(_JOOMLMS_FRONT_HOME. "/includes/libraries/lms.lib.legacy.php");
require_once(_JOOMLMS_FRONT_HOME. "/includes/libraries/lms.lib.language.php");
require_once(_JOOMLMS_FRONT_HOME. "/includes/libraries/lms.lib.errorlog.php");
require_once(_JOOMLMS_FRONT_HOME. "/includes/libraries/lms.lib.userslist.php");
require_once(_JOOMLMS_FRONT_HOME. "/includes/lms_html_tmpl.php");
require_once(_JOOMLMS_FRONT_HOME.DS.'includes'.DS.'html'.DS.'lms.html.php');

if (JLMS_J16version()) {
	require_once(_JOOMLMS_FRONT_HOME.DS.'includes'.DS.'classes'.DS.'lms.editor.16.php');
} else {
	require_once(_JOOMLMS_FRONT_HOME.DS.'includes'.DS.'classes'.DS.'lms.editor.15.php');
}

$JLMS_CONFIG = JLMSFactory::getConfig();
if($JLMS_CONFIG->get('enabled_clear_course_user_data', false)){
	require_once(_JOOMLMS_FRONT_HOME.DS.'includes'.DS.'classes'.DS.'lms.scorm.clearcourseuserdata.php');
}
if($JLMS_CONFIG->get('enabled_current_step_scorm', false)){
	require_once(_JOOMLMS_FRONT_HOME.DS.'includes'.DS.'classes'.DS.'lms.scorm.currentstep.php');
}
require_once(_JOOMLMS_FRONT_HOME.DS.'includes'.DS.'classes'.DS.'lms.scorm.coursecompletion.php');

function JLMS_processSettings( $course_id, $teacher_rights = true, $stu_rights = true, $ceo_rights = true ) {
	global $my, $JLMS_DB, $JLMS_CONFIG, $JLMS_SESSION, $JLMS_LANGUAGE, $option, $Itemid;
	
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$JLMS_ACL = JLMSFactory::getACL();

	$JLMS_ACL->PrepareCourse($course_id);//, $my->id, $JLMS_DB);
	//Get user courses and type.
	//$check_time = time();
	// get courses where user teacher
	$JLMS_CONFIG->set('course_id', $course_id);

	//Get usertype
	$main_usertype = 0;
	if ($course_id) {
		if ($JLMS_CONFIG->get('is_super_user')) {
			$main_usertype = 1;
		} elseif (in_array($course_id, $JLMS_CONFIG->get('teacher_in_courses', array()))) {
			$main_usertype = 1;
		} elseif (in_array($course_id, $JLMS_CONFIG->get('student_in_courses', array()))) {
			$main_usertype = 2;
		} elseif (in_array($course_id, $JLMS_CONFIG->get('parent_in_courses', array()))) {
			$main_usertype = 6;
		}
	}

	$current_usertype = $main_usertype;
	if ($main_usertype) {
		if ($JLMS_SESSION->get('switch_usertype') == 2) {
			if ( in_array($course_id, $JLMS_CONFIG->get('student_in_courses', array())) || in_array($course_id, $JLMS_CONFIG->get('teacher_in_courses', array())) ) {
				$current_usertype = 2;
			}
		} elseif ($JLMS_SESSION->get('switch_usertype') == 6) {
			if ( in_array($course_id, $JLMS_CONFIG->get('parent_in_courses', array())) ) {
				$current_usertype = 6;
			}
		}
	}
	
	if ($current_usertype == 6 && !$ceo_rights) {
		$current_usertype = $main_usertype;
		if ($current_usertype == 6 && !$ceo_rights) {
			$current_usertype = 0;
			$main_usertype = 0;
		}
	}
	
	if ($current_usertype == 2 && !$stu_rights) {
		$current_usertype = $main_usertype;
		if ($current_usertype == 2 && !$stu_rights) {
			$current_usertype = 0;
			$main_usertype = 0;
		}
	}
	if ($course_id) {
		$query = "SELECT a.*, b.lang_name FROM #__lms_courses as a"
		. "\n LEFT JOIN #__lms_languages as b ON a.language = b.id"
		. "\n WHERE a.id = $course_id";
		$JLMS_DB->SetQuery( $query );
		$course_settings = $JLMS_DB->LoadObject();
		
		$plugin_args = array();
		$plugin_args[] = $course_settings->lang_name;
		$return_plugin = $_JLMS_PLUGINS->trigger('onAfterLoadCourseSettings', $plugin_args );
		if(isset($return_plugin)){
			if(isset($return_plugin[0])){
				if ($return_plugin[0]) 
				{
					$course_settings->lang_name = $return_plugin[0];
				}
			}
		}
	}
	if (($current_usertype == 2) && $course_id) {
		if (isset($course_settings->id) && $course_settings->id) {
		//if require spec. registration - redirect to spec. registration.
			if ($course_settings->spec_reg) {
				$sr_role = intval($JLMS_ACL->GetRole(1));
				$query = "SELECT role_id, id, is_optional FROM #__lms_spec_reg_questions WHERE course_id = $course_id AND (role_id = 0 OR role_id = $sr_role) ORDER BY role_id DESC, ordering";
				$JLMS_DB->SetQuery( $query );
				$sr_quests = $JLMS_DB->LoadObjectList();
				if (!empty($sr_quests)) {
					$sr_role = $sr_quests[0]->role_id;
					$sr_ids = array();
					$sr_qq = array();
					foreach ($sr_quests as $srq) {
						if ($srq->role_id == $sr_role) {
							$sr_ids[] = $srq->id;
							$sr_qq[] = $srq;
						}
					}
					if (!empty($sr_ids)) {
						$sr_idss = implode(',',$sr_ids);
						$query = "SELECT * FROM #__lms_spec_reg_answers WHERE course_id = $course_id AND user_id = $my->id AND role_id = $sr_role AND quest_id IN ($sr_idss)";
						$JLMS_DB->SetQuery( $query );
						$sr_answs = $JLMS_DB->LoadObjectList();
						$do_redirect = false;
						foreach ($sr_qq as $srqq) {
							$is_found = false;
							foreach ($sr_answs as $sra) {
								if ($sra->quest_id == $srqq->id) {
									if (!$sra->user_answer) {
										if ($srqq->is_optional) {
											$is_found = true;
										}
									} else {
										$is_found = true;
									}
									break;
								}
							}
							if (!$is_found) {
								$do_redirect = true;
							}
						}
						if (count($sr_answs) != count($sr_ids) || ($do_redirect)) {
							$task = strval(mosGetParam($_REQUEST, 'task', ''));
							if ($task != 'spec_reg' && $task != 'spec_reg_answer') {
								JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=spec_reg&id=$course_id"));
							}
						}
					}
				}
			}
		}
	}

	$JLMS_CONFIG->set('main_usertype', $main_usertype);
	$JLMS_CONFIG->set('current_usertype', $current_usertype);
	//set user language
	$is_user_language = false;
	if ($JLMS_SESSION->get('lms_user_language')) {
		$query = "SELECT lang_name FROM #__lms_languages WHERE lang_name = ".$JLMS_DB->Quote($JLMS_SESSION->get('lms_user_language'))." AND published = 1";
		$JLMS_DB->SetQuery( $query );
		$lang = $JLMS_DB->LoadResult();
		if ($lang) {
			$is_user_language = true;
			$old_def_language = $JLMS_CONFIG->get('default_language', 'english');
			$JLMS_CONFIG->set('default_language', $JLMS_SESSION->get('lms_user_language'));
			if ($old_def_language && $old_def_language =='russian' && $JLMS_SESSION->get('lms_user_language') != 'english' && $JLMS_SESSION->get('lms_user_language') != 'russian') {
				JLMS_require_lang($JLMS_LANGUAGE, 'main.lang', 'english');
			}
			JLMS_require_lang($JLMS_LANGUAGE, 'main.lang', $JLMS_CONFIG->get('default_language'));
		}
	}

	//process course settings
	$row = array();
	if ($course_id){
		if (isset($course_settings->id) && $course_settings->id) {
			// if course unpublished, disallow entering for students.
			if ($current_usertype == 2) {
				if ($main_usertype == 2) {
					//$now = time();
					$now = strtotime(JLMS_dateToDB(JLMS_offsetDateToDisplay(gmdate("Y-m-d H:i:s"))));
					$reset_rights = false;
					if ($course_settings->published) {
						if ($course_settings->publish_start) {
							$course_start = strtotime($course_settings->start_date);
							if ($course_start > $now) {
								$reset_rights = true;
							}
						}
						if ($course_settings->publish_end) {
							$course_end = strtotime($course_settings->end_date);
							if ($course_end < $now) {
								$reset_rights = true;
							}
						}
					} else {
						$reset_rights = true;
					}
					if ($reset_rights) {
						$current_usertype = 0;
						$main_usertype = 0;
						$JLMS_CONFIG->set('main_usertype', $main_usertype);
						$JLMS_CONFIG->set('current_usertype', $current_usertype);
					}
				}
			}
			$JLMS_CONFIG->set('course_homework', ($course_settings->add_hw?1:0) );
			$JLMS_CONFIG->set('course_spec_reg', ($course_settings->spec_reg?1:0) );
			//$JLMS_CONFIG->set('course_question', $course_settings->course_question );
			$JLMS_CONFIG->set('course_name', $course_settings->course_name);
			$JLMS_CONFIG->set('course_attendance', ($course_settings->add_attend?1:0) );
			$JLMS_CONFIG->set('course_chat', ($course_settings->add_chat?1:0) );
			$JLMS_CONFIG->set('course_forum', ($course_settings->add_forum?1:0) );
			$JLMS_CONFIG->set('course_params', $course_settings->params );
			if (!$is_user_language) {
				if (isset($course_settings->lang_name) && $course_settings->lang_name) {
					if ($JLMS_CONFIG->get('default_language') != $course_settings->lang_name) {
						if (file_exists(_JOOMLMS_FRONT_HOME . "/languages/".$course_settings->lang_name."/main.lang.php")) {
							$old_def_language = $JLMS_CONFIG->get('default_language', 'english');
							$JLMS_CONFIG->set('default_language', $course_settings->lang_name);
							if ($old_def_language && $old_def_language == 'russian' && $JLMS_SESSION->get('default_language') != 'english' && $JLMS_SESSION->get('default_language') != 'russian') {
								JLMS_require_lang($JLMS_LANGUAGE, 'main.lang', 'english');
							}
							JLMS_require_lang($JLMS_LANGUAGE, 'main.lang', $JLMS_CONFIG->get('default_language'));
						}
					}
				}
			}
		}
		//for JLMS_GetUserType support
		if ($JLMS_SESSION->get('switch_usertype') == 6) {
			$JLMS_SESSION->set('jlms_user_course', $course_id);
			$JLMS_SESSION->set('jlms_user_type', 6);
		}
		
		//menu processing
		$query	= "SELECT a.*,b.menu_id as disabled "
			." FROM `#__lms_menu` as a LEFT JOIN `#__lms_local_menu` as b ON a.id = b.menu_id AND b.course_id = $course_id "
			." WHERE a.user_access = ".$current_usertype." AND a.published = 1 ORDER BY a.ordering ";
		$JLMS_DB->setQuery($query);
		$menus = $JLMS_DB->loadObjectList();
		$row = array();
		$add_tracking	= $JLMS_CONFIG->get('tracking_enable');
		$add_quiz		= $JLMS_CONFIG->get('plugin_quiz');
		$add_chat		= ( $JLMS_CONFIG->get('chat_enable') && $JLMS_CONFIG->get('course_chat') );
		$add_forum 		= ( $JLMS_CONFIG->get('plugin_forum') && $JLMS_CONFIG->get('course_forum') );
		$add_conference	= $JLMS_CONFIG->get('conference_enable');
		$add_hw			= $JLMS_CONFIG->get('course_homework');
		$add_attend		= $JLMS_CONFIG->get('course_attendance');
		$user_parent = $JLMS_CONFIG->get('is_user_parent');
		foreach ($menus as $menu){
			$item = new stdClass();
			$item->id = $menu->id;
			$item->disabled = $menu->disabled;
			$item->display_mod = 0;
			$item->task = $menu->task;
			$item->target = '';
			$item->help_task = 0;
			$item->user_options = 0;
			if ($menu->lang_var == '_JLMS_TOOLBAR_HELP' ){
				$item->help_task = 1;
				$item->target = " target='_blank' ";
			}
			if($menu->lang_var == '_JLMS_TOOLBAR_USER_OPTIONS' ){
				$item->user_options = 1;
			}
			$item->is_separator = $menu->is_separator;
			$item->image = $menu->image;
			$item->lang_var = $menu->lang_var;
			if ( $menu->lang_var == '_JLMS_TOOLBAR_QUIZZES'){
				if (!$add_quiz) {
					$item->disabled = 1;
				}
			}elseif($menu->lang_var == '_JLMS_TOOLBAR_FORUM'){
				if  (!$add_forum ) {
					$item->disabled = 1;
				}
			}elseif($menu->lang_var == '_JLMS_TOOLBAR_CHAT' ){
				if (!$add_chat) {
					$item->disabled = 1;
				}
			}elseif($menu->lang_var == '_JLMS_TOOLBAR_CONF' ){
				if (!$add_conference) {
					$item->disabled = 1;
				}
			}elseif($menu->lang_var == '_JLMS_TOOLBAR_HOMEWORK'){
				if (!$add_hw) {
					$item->disabled = 1;
				}
			}elseif($menu->lang_var == '_JLMS_TOOLBAR_ATTEND'){
				if ( !$add_attend) {
					$item->disabled = 1;
				}
			}elseif($menu->lang_var == '_JLMS_TOOLBAR_TRACK'){
				if ( !$add_tracking) {
					$item->disabled = 1;
				}
			}

			$task = '';
			if ($menu->task){
				$task = "&amp;task=".$menu->task;
			}

			if ($menu->lang_var == '_JLMS_TOOLBAR_COURSES' || $menu->lang_var == '_JLMS_TOOLBAR_HOME' || $menu->lang_var == '_JLMS_TOOLBAR_SUBSCRIPTIONS' || $menu->lang_var == '_JLMS_TOOLBAR_LIBRARY'){
				$ids = '';
			} else {
				$ids = "&amp;id=".$course_id;
			}
			if ($menu->lang_var == '_JLMS_TOOLBAR_HELP'){
				$help_link = $JLMS_CONFIG->get('jlms_help_link', "http://www.joomlalms.com/index.php?option=com_lms_help&Itemid=40&task=view_by_task&key={toolname}");
				
				if (!isset($help_task)) { $help_task = $JLMS_SESSION->get('jlms_task'); }
				$user_access = $JLMS_CONFIG->get('current_usertype');
				if($user_access == 2){
					$help_task = "stu_".$help_task;
				}elseif($user_access == 6){
					$help_task = "ceo_".$help_task;
				}
				
				if (isset($item->help_task) && $item->help_task ) {
					$help_link = ampReplace(str_replace('{toolname}',$help_task,$help_link));
					$item->menulink = $help_link;
				}
			} else {	
				if ($JLMS_CONFIG->get('under_ssl') && $JLMS_CONFIG->get('real_live_site')) {
					$item->menulink = $JLMS_CONFIG->get('real_live_site') . "/index.php?option=$option&amp;Itemid=$Itemid".$task.$ids;
				} else {
					$item->menulink = sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid".$task.$ids);
				}
			}
			if ($menu->lang_var == '_JLMS_TOOLBAR_GQP_PARENT'){
				$item->menulink = sefRelToAbs("index.php?option=$option&amp;task=quizzes&amp;page=setup_gqp&amp;Itemid=$Itemid");
				$item->task = 'global_pool';
			}

			if ($menu->lang_var == '_JLMS_TOOLBAR_CEO_PARENT'){
				if ($user_parent) {
					$row[] = $item;
				}
			} else {
				$is_shown = true;
				if (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_GQP_PARENT') {
					$is_shown = $JLMS_CONFIG->get('license_lms_gqp', 0) ? true : false;
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_DOCS') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('docs');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_QUIZZES') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('quizzes');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_LINKS') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('links');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_LPATH') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('lpaths');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_AGENDA') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('announce');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_ATTEND') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('attendance');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_CHAT') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('chat');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_CONF') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('conference');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_GRADEBOOK') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('gradebook');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_TRACK') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('tracking');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_MAILBOX') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('mailbox');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_USERS') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('users');
				} elseif (isset($menu->user_options) && $menu->user_options) {
					$is_shown = $JLMS_ACL->CheckToolPermissions('user_settings');
				}
				if($is_shown){
					$row[] = $item;
				}
			}
		}
	} else {
		$query	= "SELECT * FROM `#__lms_menu` "
			." WHERE user_access = 0 AND published = 1 ORDER BY ordering ";
		$JLMS_DB->setQuery($query);
		$menus = $JLMS_DB->loadObjectList();
		$row = array();
		$user_parent = $JLMS_CONFIG->get('is_user_parent');
		foreach ($menus as $menu){

			$item = new stdClass();
			$item->id = $menu->id;
			$item->display_mod = 0;
			$item->task = $menu->task;
			$item->target = '';

			$item->is_separator = $menu->is_separator;
			$item->image = $menu->image;
			$item->lang_var = $menu->lang_var;

			$task = '';
			if ($menu->task){
				$task = "&amp;task=".$menu->task;
			}
			$ids = '';
			if ($JLMS_CONFIG->get('under_ssl') && $JLMS_CONFIG->get('real_live_site')) {
				$item->menulink = $JLMS_CONFIG->get('real_live_site') . "/index.php?option=$option&amp;Itemid=$Itemid".$task.$ids;
			} else {
				$item->menulink = sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid".$task.$ids);
			}

			if ($menu->lang_var == '_JLMS_TOOLBAR_GQP_PARENT'){
				$item->menulink = sefRelToAbs("index.php?option=$option&amp;task=quizzes&amp;page=setup_gqp&amp;Itemid=$Itemid");
			}
			
			if ($menu->lang_var == '_JLMS_TOOLBAR_CEO_PARENT'){
				if ($user_parent) {
					$row[] = $item;
				}
			} else {
				$is_shown = true;
				if (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_GQP_PARENT') {
					$is_shown = $JLMS_CONFIG->get('license_lms_gqp', 0) ? true : false;
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_DOCS') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('docs');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_QUIZZES') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('quizzes');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_LINKS') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('links');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_LPATH') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('lpaths');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_AGENDA') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('announce');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_ATTEND') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('attendance');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_CHAT') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('chat');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_CONF') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('conference');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_GRADEBOOK') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('gradebook');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_TRACK') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('tracking');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_MAILBOX') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('mailbox');
				} elseif (isset($menu->lang_var) && $menu->lang_var == '_JLMS_TOOLBAR_USERS') {
					$is_shown = $JLMS_ACL->CheckToolPermissions('users');
				} elseif (isset($menu->user_options) && $menu->user_options) {
					$is_shown = $JLMS_ACL->CheckToolPermissions('user_settings');
				}
				if($is_shown){
					$row[] = $item;
				}
			}
		}
	}
	
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$_JLMS_PLUGINS->loadBotGroup('system');
	$_JLMS_PLUGINS->trigger('onGenerateMenu', array(&$row));
	
	$JLMS_CONFIG->set('jlms_menu', $row);

	//get list of system languages
	$query = "SELECT lang_name as value, lang_name as text FROM #__lms_languages WHERE published = 1 ORDER BY lang_name";
	$JLMS_DB->SetQuery( $query );
	$lms_languages = $JLMS_DB->LoadObjectList();
	$JLMS_CONFIG->set('lms_languages', $lms_languages);
}

function showPrevButtonLP($toolbar, $lp_params){
	if(!$lp_params->get('show_prev_button', 1)){
		$tmp = array();
		foreach($toolbar as $toolbar_btn){
			if(isset($toolbar_btn['btn_type']) && !in_array($toolbar_btn['btn_type'], array('prev'))){
				$tmp[] = $toolbar_btn;
			}
		}
		if(isset($tmp) && count($tmp)){
			$toolbar = array();
			$toolbar = $tmp;
		}
	}
	return $toolbar;
}

function processSelectedIndexMarker( $link ) 
{
	$link = sefRelToAbs($link);
	$link = str_replace('%5C%27',"'", $link);
	$link = str_replace('%5B',"[", $link);
	$link = str_replace('%5D',"]", $link);$link = str_replace('%20',"+", $link);$link = str_replace("\\\\\\","", $link);
	$link = str_replace('%27',"'", $link);
	$link = str_replace( array( JLMS_SELECTED_INDEX_MARKER, urlencode(JLMS_SELECTED_INDEX_MARKER) ), "'+this.options[selectedIndex].value+'", $link );
	
	return $link;
}

if (!function_exists('JLMS_mosMail')) {

function JLMS_mosMail( $from, $fromname, $recipient, $subject, $body, $mode=0, $cc=NULL, $bcc=NULL, $attachment_file=NULL, $attachment_name=NULL, $replyto=NULL, $replytoname=NULL ) {
	$app = JFactory::getApplication();

	if ($from == '') {
		$from = $app->getCfg('mailfrom');
	}
	if ($fromname == '') {
		$fromname = $app->getCfg('fromname');
	}

	// Filter from, fromname and subject
	if (!JosIsValidEmail( $from ) || !JosIsValidName( $fromname ) || !JosIsValidName( $subject )) {
		return false;
	}

	$mail = mosCreateMail( $from, $fromname, $subject, $body );

	// activate HTML formatted emails
	if ( $mode ) {
		$mail->IsHTML(true);
	}

	if (is_array( $recipient )) {
		foreach ($recipient as $to) {
			if (!JosIsValidEmail( $to )) {
				return false;
			}
			$mail->AddAddress( $to );
		}
	} else {
		if (!JosIsValidEmail( $recipient )) {
			return false;
		}
		$mail->AddAddress( $recipient );
	}
	if (isset( $cc )) {
		if (is_array( $cc )) {
			foreach ($cc as $to) {
				if (!JosIsValidEmail( $to )) {
					return false;
				}
				$mail->AddCC($to);
			}
		} else {
			if (!JosIsValidEmail( $cc )) {
				return false;
			}
			$mail->AddCC($cc);
		}
	}
	if (isset( $bcc )) {
		if (is_array( $bcc )) {
			foreach ($bcc as $to) {
				if (!JosIsValidEmail( $to )) {
					return false;
				}
				$mail->AddBCC( $to );
			}
		} else {
			if (!JosIsValidEmail( $bcc )) {
				return false;
			}
			$mail->AddBCC( $bcc );
		}
	}
	if ($attachment_file) {
			$mail->AddAttachment( $attachment_file, $attachment_name );
	}
	//Important for being able to use mosMail without spoofing...
	if ($replyto) {
		if (is_array( $replyto )) {
			reset( $replytoname );
			foreach ($replyto as $to) {
				$toname = ((list( $key, $value ) = each( $replytoname )) ? $value : '');
				if (!JosIsValidEmail( $to ) || !JosIsValidName( $toname )) {
					return false;
				}
				$mail->AddReplyTo( $to, $toname );
			}
        } else {
			if (!JosIsValidEmail( $replyto ) || !JosIsValidName( $replytoname )) {
				return false;
			}
			$mail->AddReplyTo($replyto, $replytoname);
		}
    }

	$mailssend = $mail->Send();

	if(isset($mail->error_count) && $mail->error_count > 0 ) {
	}
	return $mailssend;
}

}

function JLMS_getOffset() 
{
		$config = JFactory::getConfig();
		//SMT j!3.0 update
		if (method_exists($config,'getValue'))
		{
		$configOffset = $config->getValue('config.offset');
		}else 
		{
			$configOffset = $config->get('offset');
		}	
		if( JLMS_J16version() ) 
		{						
			$tzObj = new DateTimeZone( $configOffset );
			$dt = new DateTime('now', $tzObj );			
			$offset = $dt->getOffset();				
		} else {
			$offset = $configOffset*3600; 
		}	
		
		return $offset; 
}

function JLMS_replDbNullDate( $date, $char = '' ) 
{
	$JLMS_DB = & JLMSFactory::getDB();             
	return (strpos( $JLMS_DB->getNullDate(), $date ) !== false )?$char:$date; 
}  

function JLMS_dateGetFormat($receiver = 1) {
// $receiver == 1; receiver JS calendar, we must return value like 'y-mm-dd' or 'dd-mm-y'
// $receiver == 2; receiver PHP function, we must return value like 'Y-m-d' or 'd-m-Y'
	$JLMS_CONFIG = & JLMSFactory::getConfig();
	$date_format = $JLMS_CONFIG->get('date_format', 'Y-m-d');
	if ($receiver == 1) {
		$rec_format = str_replace('d', 'dd', str_replace('m', 'mm', strtolower($date_format)));
	} else {
		$rec_format = $date_format;
	}
	return $rec_format;
}

function JLMS_offsetDateToDisplay($date, $is_time = false, $offset = 0, $format_suffix = '', $custom_date_format = '') 
{
	if( !$offset ) 
	{
		$offset = JLMS_getOffset();			
	}
	
	return JLMS_dateToDisplay( $date, $is_time, $offset, $format_suffix, $custom_date_format );	
}

function JLMS_dateToDisplay($date, $is_time = false, $offset = 0, $format_suffix = '', $custom_date_format = ''){
	$JLMS_CONFIG = & JLMSFactory::getConfig();
	
	if ($date) {
		if ($date == '0000-00-00 00:00:00' || $date == '0000-00-00') {
			$date_to_dis = '-';
		} else {
			if($custom_date_format && strlen($custom_date_format)){
				$date_format = $custom_date_format;
			} else {
				$date_format = $JLMS_CONFIG->get('date_format', 'Y-m-d');// 'Y-m-d' by default
			}
			
			//test development
			//if(!$format_suffix){ 
			//	$format_suffix = ' ' . $JLMS_CONFIG->get('time_format', 'H:i:s');
			//}
			//test development
			
			$date_suffix = '';
			if ($is_time) { // esli peredaetsa ne stroka a chislovoi variant
				$time = $date;
				$time = $time + $offset;
				
			} else {
				$time = strtotime($date);
				$time = $time + $offset;
				
				if (!$format_suffix) { // only 'date' part is inresting for us (all other info we can cut off)
					if (strlen($date) == 19) { // esli na konce eshe vremya visit
						$date_suffix = date(' H:i:s', $time);
					}
				}			
			}
			 
			if ($format_suffix) {
				$date_to_dis = date($date_format.$format_suffix, $time);// . $date_suffix;
			} else {
				$date_to_dis = date($date_format, $time) . $date_suffix;
			}
		}
	} else {
		$date_to_dis = '-'; // esli iznachal'no nikakoi daty netu
	}
	return $date_to_dis;
}

function JLMS_dateWithTimeZoneServerConvertToUTC($date){
	$date_offset_sec = date('Z', strtotime($date));
	$date_UTC_sec = strtotime($date) - $date_offset_sec;
	$date_UTC = date("Y-m-d H:i:s", $date_UTC_sec);
	
	return $date_UTC;
}

function JLMS_dateWithTimeZoneCMSConvertToUTC($date){
	$offset_cms = JLMS_getOffset();
	$date = date("Y-m-d H:i:s", (strtotime($date) - $offset_cms));
	return $date;
}

function JLMS_fixDateStoredOnServerTimeZone($date, $date_fix){
	$offset_cms = JLMS_getOffset();
	$date_server = date("Y-m-d H:i:s");
	$offset_php = date("Z", strtotime($date_server));
	
	$timezone_fix_on = 1;
	if($offset_cms == $offset_php){
		$timezone_fix_on = 0;
	}
	
	if(strtotime($date) < strtotime($date_fix)){
		if($timezone_fix_on){
			$date = JLMS_offsetDateToDisplay(JLMS_dateWithTimeZoneServerConvertToUTC($date));
		} else {
			$date = JLMS_dateToDisplay($date);
		}
	} else {
		$date = JLMS_offsetDateToDisplay($date);
	}
	
	return $date;
}

/**
 * v dal'neishem nado peredelat' function !!!!! (a vdrug format tipa 'm-d-Y', sputaetsa s 'd-m-Y')
 * This function is only for date! - NOT FOR datetime !!!
 * @param unknown_type $date
 * @return unknown
 */
function JLMS_dateToDB($date){
	//09.03.2007
	$time = time();
	
	$format = JLMS_dateGetFormat(2);
	
	$timeSuff = false;	
	
	if ( $format == 'Y-m-d') 
	{		
		$time = strtotime($date);
		
		if( $time !== false ) 
		{				
			$timeArr = getdate($time);
			if( $timeArr['hours'] > 0 || $timeArr['minutes'] > 0 ) 
			{				
				$timeSuff = true;
			}
		} else {
			$time = time();
		}			
	} elseif ( $format == 'd-m-Y' ) {
		if ( $date && preg_match( "/([0-9]{2})-([0-9]{2})-([0-9]{4})\s+([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})/", $date, $regs ) ) {
			$timeSuff = true;		 												
			$time = mktime( $regs[4], $regs[5], $regs[6], $regs[2], $regs[1], $regs[3] );
		} else	
		if ( $date && preg_match( "/([0-9]{2})-([0-9]{2})-([0-9]{4})\s+([0-9]{1,2}):([0-9]{1,2})/", $date, $regs ) ) {
			$timeSuff = true;		 												
			$time = mktime( $regs[4], $regs[5], 0, $regs[2], $regs[1], $regs[3] );						
		} else if ( $date && preg_match( "/([0-9]{2})-([0-9]{2})-([0-9]{4})\s+([0-9]{1,2})/", $date, $regs ) ) {			
			$time = mktime( $regs[4], 0, 0, $regs[2], $regs[1], $regs[3] );		
		} else if ( $date && preg_match( "/([0-9]{2})-([0-9]{2})-([0-9]{4})/", $date, $regs ) ) {
			$time = mktime( 0, 0, 0, $regs[2], $regs[1], $regs[3] );		
		}
		
		if ($time > -1) {
			// do nothing (all OK)
		} else {
			$time = time();
		}
	} elseif ( $format == 'm-d-Y' ) {
		if ( $date && preg_match( "/([0-9]{2})-([0-9]{2})-([0-9]{4})\s+([0-9]{1,2}):([0-9]{1,2})/", $date, $regs ) ) {			
			$timeSuff = true;		 												
			$time = mktime( $regs[4], $regs[5], 0, $regs[1], $regs[2], $regs[3] );						
		} else if ( $date && preg_match( "/([0-9]{2})-([0-9]{2})-([0-9]{4})\s+([0-9]{1,2})/", $date, $regs ) ) {			
			$time = mktime( $regs[4], 0, 0, $regs[1], $regs[2], $regs[3] );		
		} else if ( $date && preg_match( "/([0-9]{2})-([0-9]{2})-([0-9]{4})/", $date, $regs ) ) {
			$time = mktime( 0, 0, 0, $regs[1], $regs[2], $regs[3] );		
		}
		
		if ($time > -1) {
			// do nothing (all OK)
		} else {
			$time = time();
		}
	}
	
	if( $timeSuff ) {
		$date_to_db = date('Y-m-d H:i:s', $time);
	} else {
		$date_to_db = date('Y-m-d', $time);
	} 	
	
	return $date_to_db;
}
//RETURN Date SelectLists
function JLMS_dateGenerateSelectLists($name, $date, $dis = false){
	// $date must be in 'Y-m-d' format !!!
	//Generate SelectList of days
	$str_days = "<select name='".$name."day' id='".$name."dd' onchange=\"jlms_getDate('".$name."')\"".($dis?' disabled="disabled"':'').">";
	for ($i=1;$i<=31;$i++){
		$check1 = '';
		if ($date != ''){
			if ($i == intval(substr($date, 8, 2))){
				$check1 = " selected='selected'";
			}
		}
		elseif($i == date("j")){
			$check1 = " selected='selected'";
		}
		$str_days .=  "<option value='$i'".$check1.">$i</option>";
	}
	$str_days .= '</select>';

	//Generate SelectList of months
	$str_months = "<select name='".$name."month' id='".$name."mm' onchange=\"jlms_getDate('".$name."')\"".($dis?' disabled="disabled"':'').">";
	$first_day = mktime(1,1,1,1,1,06);
	for ($j=0;$j<12;$j++){
		$check2 = '';
		if ($date != ''){
			if (($j+1) == intval(substr($date, 5, 2))){
				$check2 = " selected='selected'";
			}
		}
		elseif(($j+1) == date("m")){
			$check2 = " selected='selected'";
		}
		$str_months .= "<option value='".($j)."'$check2>".month_lang(strftime("%m",strtotime("+".$j." month",$first_day)),0,2)."</option>";
	}
	$str_months .= '</select>';

	//generate SelectList of Years
	$str_years = "<select name='".$name."year' id='".$name."yy' onchange=\"jlms_getDate('".$name."')\"".($dis?' disabled="disabled"':'').">";
	for ($k=2006;$k<2021;$k++){
		$check3 = '';
		if ($date != ''){
			if ($k == intval(substr($date, 0, 4))){
				$check3 = " selected='selected'";
			}
		}
		elseif($k == date("Y")){
			$check3 = " selected='selected'";
		}
		$str_years .= "<option value='$k'$check3>".$k."</option>";
	}
	$str_years .= '</select>';
	$date_format = strtolower(JLMS_dateGetFormat(2));
	$trans = array("y" => $str_years, "m" => $str_months, "d" => $str_days, "-" => " ");
	$ret = strtr($date_format, $trans);
	return $ret;
}

//do not remove the function below! required by FlightSchool extensions;
function JLMS_loadCalendar() {
	$doc = & JFactory::GetDocument();
	$doc->addStyleSheet(JURI::base().'includes/js/calendar/calendar-mos.css')
	?>
	<!-- import the calendar script -->
	<script type="text/javascript" src="<?php echo JURI::base();?>includes/js/calendar/calendar_mini.js"></script>
	<!-- import the language module -->
	<?php if (class_exists('JHTML')) { ?>
	<script type="text/javascript" src="<?php echo JURI::base();?>includes/js/calendar/lang/calendar-en-GB.js"></script>
	<?php } else { ?>
	<script type="text/javascript" src="<?php echo JURI::base();?>includes/js/calendar/lang/calendar-en.js"></script>
	<?php
	}
}

function JLMS_dateLoadJSFrameWork($with_sellists = true) {
	$JLMS_CONFIG = & JLMSFactory::getConfig();
	$MondayFirst = ($JLMS_CONFIG->get('date_format_fdow', 1) == 1);?>
<script language="javascript" type="text/javascript">
<!--//--><![CDATA[//><!--
var jlms_calendar = null;
var sellist_name = '';
function jlms_selected(cal, date) {
	cal.sel.value = date;
	if (cal.sel.defaultValue != undefined) {
		cal.sel.defaultValue = date;
	}
<?php if ($with_sellists) { ?>
	var el1 = document.getElementById(sellist_name+'dd');
	el1.value = cal.date.getDate();
	var el2 = document.getElementById(sellist_name+'mm');
	el2.value = cal.date.getMonth();
	var el3 = document.getElementById(sellist_name+'yy');
	el3.value = cal.date.getFullYear();
<?php } ?>
}
function jlms_closeHandler(cal) {
	cal.hide();
	Calendar.removeEvent(document, "mousedown", jlms_checkCalendar);
}
function jlms_checkCalendar(ev) {
	var el = Calendar.is_ie ? Calendar.getElement(ev) : Calendar.getTargetElement(ev);
	for (; el != null; el = el.parentNode)
	if (el == jlms_calendar.element || el.tagName == "A") break;
	if (el == null) {
		jlms_calendar.callCloseHandler(); Calendar.stopEvent(ev);
	}
}
function jlms_showCalendar(id, sec_name) {
	sellist_name = sec_name;
	var el = document.getElementById(id);
	if (jlms_calendar != null) {
		jlms_calendar.hide();
		jlms_calendar.parseDate(el.value);
	} else {
		var cal = new Calendar(<?php echo $MondayFirst?"true":"false"; ?>, null, jlms_selected, jlms_closeHandler);
		jlms_calendar = cal;
		cal.setRange(1900, 2070);
		cal.setDateFormat('<?php echo JLMS_dateGetFormat(1); ?>');
		jlms_calendar.create();
		jlms_calendar.parseDate(el.value);
	}
	jlms_calendar.sel = el;
	jlms_calendar.showAtElement(el);
	Calendar.addEvent(document, "mousedown", jlms_checkCalendar);
	return false;
}
<?php if ($with_sellists) { ?>
function jlms_getDate(elem){
	jlms_day   = getObj(elem+'dd').value;
	if (jlms_day.length == 1){
		jlms_day = '0'+jlms_day;
	}
	jlms_month = getObj(elem+'mm').value;
	jlms_month = Number(jlms_month) + 1;
	jlms_month = jlms_month + '';
	if (jlms_month.length == 1){
		jlms_month = '0'+jlms_month;
	}
	jlms_year  = getObj(elem+'yy').value;
	var dt_fmt = '<?php echo strtolower(JLMS_dateGetFormat(2)); ?>';
	dt_fmt.replace('y',jlms_year).replace('m',jlms_month).replace('d', jlms_day);
	getObj(elem+'_date').value = dt_fmt.replace('y',jlms_year).replace('m',jlms_month).replace('d', jlms_day);
}
<?php } ?>
//--><!]]>
</script>
<?php
}
// Redeclare some functions from Calendar extension (if this extension not available)
if (!function_exists('get_jd_dmy')) {
   function get_jd_dmy($a_jd){
     $W = intval(($a_jd - 1867216.25)/36524.25) ;
     $X = intval($W/4) ;
     $A = $a_jd+1+$W-$X ;
     $B = $A+1524 ;
     $C = intval(($B-122.1)/365.25) ;
     $D = intval(365.25*$C) ;
     $E = intval(($B-$D)/30.6001) ;
     $F = intval(30.6001*$E) ;
     $a_day = $B-$D-$F ;
     if ( $E > 13 ) {
        $a_month=$E-13 ;
        $a_year = $C-4715 ;
     } else {
        $a_month=$E-1 ;
        $a_year=$C-4716 ;
     }
     return array($a_month, $a_day, $a_year) ;
   }
}

if (!function_exists('jddayofweek')) {
	function jddayofweek($a_jd,$a_mode){
       $tmp = get_jd_dmy($a_jd) ;
       $a_time = "$tmp[0]/$tmp[1]/$tmp[2]" ;
       switch($a_mode) {
           case 1:
               return day_month_lang(date("w",strtotime("$a_time")),0,1,0,0) ;
           case 2:
               return day_month_lang(date("w",strtotime("$a_time")),0,1,0,0,1) ;
           default:
               return strftime("%w",strtotime("$a_time")) ;
       }
	}
}
if(!function_exists('gregoriantojd')) {
	/*function gregoriantojd( $month, $day, $year) {
		if( $month < 3) {
			$month = $month + 12;
			$year = $year - 1;
		}
		$jd = $day + floor(( 153 * $month - 457) / 5) + 365 * $year
		+ floor ($year / 4) - floor($year / 100)
		+ floor($year / 400) + 1721118.5;
		return($jd );
	}*/
	function gregoriantojd($month,$day,$year) {
		if ($month > 2) {
			$month = $month - 3;
		} else {
			$month = $month + 9;
			$year = $year - 1;
		}
		$c = floor($year / 100);
		$ya = $year - (100 * $c);
		$j = floor((146097 * $c) / 4);
		$j += floor((1461 * $ya)/4);
		$j += floor(((153 * $month) + 2) / 5);
		$j += $day + 1721119;
		return $j;
	}
}

function JLMS_getOnlineUsers( $course_id ) {
	global $JLMS_DB;
	$query = "SELECT user_id FROM #__lms_users_in_groups WHERE course_id = $course_id";
	$JLMS_DB->SetQuery( $query );
	$uids = JLMSDatabaseHelper::LoadResultArray();
	$query = "SELECT user_id FROM #__lms_user_courses WHERE course_id = $course_id";
	$JLMS_DB->SetQuery( $query );
	$uids = array_merge($uids,JLMSDatabaseHelper::LoadResultArray());
	if (empty($uids)) { $uids = array(0); }
	$uidss = implode(',',$uids);
	$query = "SELECT DISTINCT a.username, a.userid, u.name"
	."\n FROM #__session AS a, #__users AS u"
	."\n WHERE (a.userid=u.id) AND (a.guest = 0) AND (NOT ( a.usertype is NULL OR a.usertype = '' )) AND u.id IN ($uidss)"
	."\n ORDER BY a.username";
	$JLMS_DB->setQuery($query);
	$rows = $JLMS_DB->loadObjectList();
	return $rows;
}

function JLMS_getOnlineUsers_TR( $course_id ) {
	global $JLMS_DB;
	$query = "SELECT user_id FROM #__lms_users_in_groups WHERE course_id = $course_id";
	$JLMS_DB->SetQuery( $query );
	$uids = JLMSDatabaseHelper::LoadResultArray();
	$query = "SELECT user_id FROM #__lms_user_courses WHERE course_id = $course_id";
	$JLMS_DB->SetQuery( $query );
	$uids = array_merge($uids,JLMSDatabaseHelper::LoadResultArray());
	if (empty($uids)) { $uids = array(0); }
	$uidss = implode(',',$uids);
	$query = "SELECT DISTINCT a.username, a.userid, u.name"
	."\n FROM #__session AS a, #__users AS u"
	."\n WHERE (a.userid=u.id) AND (a.guest = 0) AND (NOT ( a.usertype is NULL OR a.usertype = '' )) AND u.id IN ($uidss)"
	."\n ORDER BY a.username";
	$JLMS_DB->setQuery($query);
	$rows = $JLMS_DB->loadObjectList();
	$query = "SELECT max(track_time) as last_course_time, user_id FROM #__lms_track_hits WHERE user_id IN ($uidss) AND course_id = $course_id GROUP BY user_id";
	$JLMS_DB->setQuery($query);
	$rows2 = $JLMS_DB->loadObjectList();
	for ($i = 0, $n = count($rows); $i < $n; $i ++) {
		$rows[$i]->last_course_time = '';
	}

	foreach ($rows2 as $row2) {
		for ($i = 0, $n = count($rows); $i < $n; $i ++) {
			if ($rows[$i]->userid == $row2->user_id) {
				$rows[$i]->last_course_time = $row2->last_course_time;
				break;
			}
		}
	}
	return $rows;
}
function JLMS_uploadFile( $course_id, $file_fieldname = 'userfile') {
	global $JLMS_DB;
	$base_Dir = _JOOMLMS_DOC_FOLDER;
	// to do: ... ... ... mosGetParam ?
	if (!$file_fieldname) {
		mosErrorAlert(_JLMS_EM_SELECT_FILE);
	}
	$userfile2 = (isset($_FILES[$file_fieldname]['tmp_name']) ? $_FILES[$file_fieldname]['tmp_name'] : "");
	$userfile_name = (isset($_FILES[$file_fieldname]['name']) ? $_FILES[$file_fieldname]['name'] : "");
	// ... ... ... mosGetParam ?
	if (empty($userfile_name)) {
		mosErrorAlert(_JLMS_EM_SELECT_FILE);
	}
	if (!$userfile2) {
		mosErrorAlert(_JLMS_EM_SELECT_FILE);
	}
	if (!file_exists($userfile2)) {
		mosErrorAlert (_JLMS_EM_UPLOAD_SIZE_ERROR);
	}

	$good_ext = false;
	$file_ext = '';
	if (strcmp(substr($userfile_name,-4,1),".") === 0) {
		$good_ext = true;
		$file_ext = substr($userfile_name,-3);
	}
	if (!$good_ext) {
		if (strcasecmp(substr($userfile_name,-5,1),".") === 0) {
			$good_ext = true;
			$file_ext = substr($userfile_name,-4);
		}
	}
	if (!$good_ext) {
		if (strcasecmp(substr($userfile_name,-6,1),".") === 0) {
			$good_ext = true;
			$file_ext = substr($userfile_name,-5);
		}
	}		
	if (!$good_ext) {
		if (strcasecmp(substr($userfile_name,-3,1),".") === 0) {
			$good_ext = true;
			$file_ext = substr($userfile_name,-2);
		}
	}
	if (!$good_ext) {
		mosErrorAlert(_JLMS_EM_BAD_FILEEXT);
	}
	$query = "SELECT LOWER(filetype) as filetype FROM #__lms_file_types";
	$JLMS_DB->SetQuery( $query );
	$ftypes = JLMSDatabaseHelper::LoadResultArray();
	if (empty($ftypes) || !in_array(strtolower($file_ext), $ftypes)) {
		mosErrorAlert(_JLMS_EM_BAD_FILEEXT);
	}
	$file_unique_name = str_pad($course_id,4,'0',STR_PAD_LEFT) . '_' . md5(uniqid(rand(), true)) . '.' . $file_ext;

	if (!move_uploaded_file($_FILES[$file_fieldname]['tmp_name'], $base_Dir.$file_unique_name) || !mosChmod($base_Dir.$file_unique_name)) {
		mosErrorAlert("Upload of ".$userfile_name." failed");
	} else {
		global $JLMS_DB, $my;
		$userfile_name = JLMSDatabaseHelper::GetEscaped($userfile_name);
		$query = "INSERT INTO #__lms_files (file_name, file_srv_name, owner_id)"
		. "\n VALUES ('".$userfile_name."', '".$file_unique_name."', '".$my->id."')";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		return $JLMS_DB->insertid();
	}
	
	return false;
}
//26.12.2006
function JLMS_uploadZIPPack( $course_id, $request_str = 'userfile' ) {
	global $Itemid, $my, $JLMS_DB;
	$base_Dir = _JOOMLMS_SCORM_FOLDER_PATH;
	// to do: ... ... ... mosGetParam ?
	$userfile2 = (isset($_FILES[$request_str]['tmp_name']) ? $_FILES[$request_str]['tmp_name'] : "");
	$userfile_name = (isset($_FILES[$request_str]['name']) ? $_FILES[$request_str]['name'] : "");
	// ... ... ... mosGetParam ?
	if (empty($userfile_name)) {
		mosErrorAlert(_JLMS_EM_SELECT_FILE);
	}
	if (!$userfile2) {
		mosErrorAlert(_JLMS_EM_SELECT_FILE);
	}
	if (!file_exists($userfile2)) {
		mosErrorAlert (_JLMS_EM_UPLOAD_SIZE_ERROR);
	}

	if (strcmp(substr($userfile_name,-4,1),".")) {
		mosErrorAlert(_JLMS_EM_BAD_FILEEXT);
	}
	if (strcasecmp(substr($userfile_name,-4),".zip")) {
		mosErrorAlert(_JLMS_EM_BAD_FILEEXT);
	}
	$fold_unique_name = str_pad($my->id,4,'0',STR_PAD_LEFT) . '_zip_' . md5(uniqid(rand(), true));
	$file_unique_name = $fold_unique_name . '.' . substr($userfile_name,-3);

	$uploadPath = $base_Dir."/".$fold_unique_name;

	$realFileSize = 0;
	$realFilesCount = 0;

	if (preg_match("/.zip$/", strtolower($userfile_name))) {

		require_once(_JOOMLMS_FRONT_HOME . "/includes/libraries/lms.lib.zip.php");

		$zipFile = new pclZip($userfile2);
		$zipContentArray = $zipFile->listContent();
		$realFileSize = 0;
		$realFilesCount = 0;
		foreach($zipContentArray as $thisContent) {
			if ( preg_match('~\.(php.*|phtml)$~i', $thisContent['filename']) ) {
				mosErrorAlert(_JLMS_EM_READ_PACKAGE_ERROR);
			}
			$realFileSize += $thisContent['size'];
			if (!$thisContent['folder']) {
				$realFilesCount ++;
			}
		}
		if(!mkdir($uploadPath, 0755)) {
			mosErrorAlert(_JLMS_EM_SCORM_FOLDER);
		}
		if($uploadPath[strlen($uploadPath)-1] == '/') {
			$uploadPath = substr($uploadPath,0,-1);
		}
		/*
		--------------------------------------
			Uncompressing phase
		--------------------------------------
		*/
		chdir($uploadPath);
		//$unzippingState = $zipFile->extract(); // 09 Oct 2008 - FIX by DEN - nested folders wasn't be extracted on some servers. if extract path would be passed to the functiona - all is fine.
		$unzippingState = $zipFile->extract(PCLZIP_OPT_PATH, $uploadPath."/");
		/*for($j = 0; $j < count($unzippingState); $j++) {
			$state = $unzippingState[$j];
			$extension = strrchr($state["stored_filename"], ".");
		}*/
		if ($dir = @opendir($uploadPath)) {
			while ($file = readdir($dir)) {
				if ($file != '.' && $file != '..') {
					$filetype = "file";
					if (is_dir($uploadPath.'/'.$file)) {
						$filetype = "folder";
					}
					$safe_file = replace_dangerous_char($file,'strict');
					@rename($uploadPath.'/'.$file, $uploadPath.'/'.$safe_file);
					if ($filetype == "file") {
						@chmod($uploadPath.'/'.$safe_file, 0644);
					} elseif ($filetype = "folder") {
						@chmod($uploadPath.'/'.$safe_file, 0755);
					}
				}
			}
			closedir($dir);
		}
	}

	$zipfile_size = filesize($_FILES[$request_str]['tmp_name']);
	if (!move_uploaded_file ($_FILES[$request_str]['tmp_name'],$base_Dir."/".$file_unique_name) || !mosChmod($base_Dir."/".$file_unique_name)) {
		mosErrorAlert("Upload of ".$userfile_name." failed");
	}

	$userfile_name = JLMSDatabaseHelper::GetEscaped($userfile_name);
	$query = "INSERT INTO #__lms_documents_zip (course_id, owner_id, zip_name, zip_srv_name, zip_folder, startup_file, count_files, zip_size, zipfile_size, upload_time)"
	. "\n VALUES "
	. "\n ('".$course_id."', '".$my->id."', '".$userfile_name."', '".$file_unique_name."', '".$fold_unique_name."', '', '".$realFilesCount."', '".$realFileSize."', "
	. "\n '".$zipfile_size."', '".date('Y-m-d H:i:s')."')";
	$JLMS_DB->SetQuery( $query );
	$JLMS_DB->query();
	$zippack_id = $JLMS_DB->insertid();
	return $zippack_id;
}
function JLMS_ReadDirectory( $path, $include_dirs = false, $filter='.', $recurse=false, $fullpath=false  ) {
	$arr = array();
	if (!@is_dir( $path )) {
		return $arr;
	}
	$handle = opendir( $path );

	while ($file = readdir($handle)) {
		$dir = mosPathName( $path.'/'.$file, false );
		$isDir = is_dir( $dir );
		if (($file != ".") && ($file != "..")) {
			if ( ($isDir && $include_dirs) || (!$isDir) ) {
				if (preg_match( "/$filter/", $file )) {
					if ($fullpath) {
						$arr[] = trim( mosPathName( $path.'/'.$file, false ) );
					} else {
						$arr[] = trim( $file );
					}
				}
			}
			if ($recurse && $isDir) {
				$arr2 = JLMS_ReadDirectory( $dir,  $include_dirs, $filter, $recurse, $fullpath );
				$arr = array_merge( $arr, $arr2 );
			}
		}
	}
	closedir($handle);
	asort($arr);
	return $arr;
}
// 13.10.2006
// "if (strcmp(substr($userfile_name,-4,1),".")) {" - eto zna4it shto 4-aya s konca - "." (t.e. extension == 3 symbols)
// to do: 'set_time_limit()' ?
// (tip) 21.10.2006 (proverka na teachera ne nugna (t.k. ona est' v "saveLPath()" a eta function vyzyvaetsa tol'ko ottuda)
function JLMS_uploadSCORM( $request_str, $from_ftp = false, $lms_n_scrom_id=0 ) {
	global $Itemid, $my, $JLMS_DB, $JLMS_CONFIG, $JLMS_SESSION;
	$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	$base_Dir = _JOOMLMS_SCORM_FOLDER_PATH;	
	
	// to do: ... ... ... mosGetParam ?
	if ($from_ftp) {
		if (!preg_match("/^[a-zA-Z0-9\/\-\.\_\s]+\.zip$/",$request_str)) {
			mosErrorAlert(_JLMS_EM_BAD_FILEEXT);
		}
		$userfile2 = JPATH_SITE . "/media/" . $request_str;
		$userfile_name = $request_str;
	} else {
		$userfile2 = (isset($_FILES[$request_str]['tmp_name']) ? $_FILES[$request_str]['tmp_name'] : "");
		$userfile_name = (isset($_FILES[$request_str]['name']) ? $_FILES[$request_str]['name'] : "");
		// ... ... ... mosGetParam ?
	}

	if (empty($userfile_name)) {
		mosErrorAlert(_JLMS_EM_SELECT_FILE);
	}

	if (strcmp(substr($userfile_name,-4,1),".")) {
		mosErrorAlert(_JLMS_EM_BAD_FILEEXT);
	}
	if (strcasecmp(substr($userfile_name,-4),".zip")) {
		mosErrorAlert(_JLMS_EM_BAD_FILEEXT);
	}
	
	$fold_unique_name = str_pad($my->id,4,'0',STR_PAD_LEFT) . '_' . md5(uniqid(rand(), true));
	$file_unique_name = $fold_unique_name . '.' . substr($userfile_name,-3);

	$uploadPath = $base_Dir."/".$fold_unique_name;
	
	if (!$userfile2) {
		mosErrorAlert(_JLMS_EM_SELECT_FILE);
	}
	if (!file_exists($userfile2)) {
		mosErrorAlert (_JLMS_FILE_NOT_FOUND);		
	}
	
	if( $from_ftp ) {
		if (!file_exists($userfile2)) 
		{
			mosErrorAlert (_JLMS_FILE_NOT_FOUND);		
		}
	} else {	
		if (!file_exists($userfile2)) 
		{
			mosErrorAlert (_JLMS_EM_UPLOAD_SIZE_ERROR);		
		}
	}
	
	if (preg_match("/.zip$/", strtolower($userfile_name))) 
	{

		require_once(_JOOMLMS_FRONT_HOME . "/includes/libraries/lms.lib.zip.php");

		$zipFile = new pclZip($userfile2);
		$zipContentArray = $zipFile->listContent();
		$okScorm = false;
		$okPlantynScorm1 = false;
		$okPlantynScorm2 = false;
		$okPlantynScorm3 = false;
		$okAiccScorm = false;
		$realFileSize = 0;
        $rootDir = "";
		foreach($zipContentArray as $thisContent) {
			if ( preg_match('~\.(php.*|phtml)$~i', $thisContent['filename']) ) {
				mosErrorAlert(_JLMS_EM_READ_PACKAGE_ERROR);
			} elseif (stripos($thisContent['filename'], 'imsmanifest.xml') !== false) {
				$okScorm = true;
                $rootDir = str_replace('imsmanifest.xml', "", $thisContent['filename']);
			} elseif (stristr($thisContent['filename'], 'LMS')) {
				$okPlantynScorm1 = true;
			} elseif (stristr($thisContent['filename'], 'REF')) {
				$okPlantynScorm2 = true;
			} elseif (stristr($thisContent['filename'], 'SCO')) {
				$okPlantynScorm3 = true;
			} elseif (stripos($thisContent['filename'], '.cst')) {
				$okAiccScorm = true;
                $rootDir = preg_replace('#/[^/]*.cst#i', "", $thisContent['filename']);
			}
			$realFileSize += $thisContent['size'];
		}
		if ((($okPlantynScorm1 == true) && ($okPlantynScorm2 == true) && ($okPlantynScorm3 == true)) || ($okAiccScorm == true)) {
			$okScorm = true;
		}

        $enableAICC = $JLMS_CONFIG->get('enable_aicc');
        if($okAiccScorm && !$okScorm && !$enableAICC){
            $JLMS_SESSION->set('joomlalms_sys_message', 'AICC packages are not yet supported');
        }

		if (!$okScorm) {// && defined('CHECK_FOR_SCORM') && CHECK_FOR_SCORM) {
            $JLMS_SESSION->set('joomlalms_sys_message', 'Course is not recognized');
		}

		// it happens on Linux that $uploadPath sometimes doesn't start with '/'
		/*if($uploadPath[0] != '/') {
			$uploadPath = '/'.$uploadPath;
		}*/
		if(!mkdir($uploadPath, 0755)) {
			mosErrorAlert(_JLMS_EM_SCORM_FOLDER);
		}
		if($uploadPath[strlen($uploadPath)-1] == '/') {
			$uploadPath = substr($uploadPath,0,-1);
		}
		
		//echo '<pre>';
		//print_r($_REQUEST);
		//print_r($_FILES);
		//print_r($request_str);
		//var_dump($from_ftp);
		//var_dump($userfile_name);
		//var_dump($fold_unique_name);
		//var_dump($file_unique_name);
		//var_dump($uploadPath);
		//var_dump($row);
		//echo '</pre>';
		//die;
		
		/*
		--------------------------------------
			Uncompressing phase
		--------------------------------------
		*/
		/*
			The first version, using OS unzip, is not used anymore
			because it does not return enough information.
			We need to process each individual file in the zip archive to
			- add it to the da ta ba se
			- parse & change relative html links
		*/
		//13.10.2006 (DEN) naxren etot OS-unzip (mne kagetsa shto komanda s oshibkami napisana)
		/*if (PHP_OS == 'Linux' && ! get_cfg_var('safe_mode') && false) {	// *** UGent, changed by OC ***
			// Shell Method - if this is possible, it gains some speed
			exec("unzip -d \"".$uploadPath."/\"".$uploadedFile['name']." ".$uploadedFile['tmp_name']);
		} else {*/
		// PHP method - slower...
		chdir($uploadPath);
		//$unzippingState = $zipFile->extract(); // 09 Oct 2008 - FIX by DEN - nested folders wasn't be extracted on some servers. if extract path would be passed to the functiona - all is fine.
		$unzippingState = $zipFile->extract(PCLZIP_OPT_PATH, $uploadPath."/", PCLZIP_OPT_REMOVE_PATH, $rootDir);
		
		//echo '<pre>';
		//print_r($zipFile);
		//echo '<br />';
		//print_r(PCLZIP_OPT_PATH);
		//echo '<br />';
		//print_r($uploadPath);
		//echo '<br />';
		//print_r(PCLZIP_OPT_REMOVE_PATH);
		//echo '<br />';
		//print_r($rootDir);
		//echo '<br />';
		//print_r($unzippingState);
		//echo '</pre>';
		//die;
		
		for($j = 0; $j < count($unzippingState); $j++) {
			$state = $unzippingState[$j];

			//fix relative links in html files
			$extension = strrchr($state["stored_filename"], ".");
			// (13.10.2006 DEN) tupizm (nu i gde etot 'fix'?)
		}
		
		//echo '<pre>';
		//print_r($uploadPath);
		//echo '</pre>';
		
		if ($dir = @opendir($uploadPath)) {
			while ($file = readdir($dir)) {
				if ($file != '.' && $file != '..') {
					$filetype = "file";
					if (is_dir($uploadPath.'/'.$file)) {
						$filetype = "folder";
					}
					$safe_file = replace_dangerous_char($file,'strict');
					
					//echo '<pre>';
					//print_r($file);
					//echo '<br />';
					//print_r($safe_file);
					//echo '<br />';
					//print_r($filetype);
					//echo '</pre>';
					
					@rename($uploadPath.'/'.$file, $uploadPath.'/'.$safe_file);
					if ($filetype == "file") {
						@chmod($uploadPath.'/'.$safe_file, 0644);
					} elseif ($filetype == "folder") {
						@chmod($uploadPath.'/'.$safe_file, 0755);
					}
				}
			}
			closedir($dir);
			//die;
		}
	}

	if ($from_ftp) {
		if (!@copy($userfile2,$base_Dir."/".$file_unique_name)) {
			mosErrorAlert("Upload of ".$userfile_name." failed");
		}
		@unlink($userfile2);
	} else {
		if (!move_uploaded_file ($_FILES[$request_str]['tmp_name'],$base_Dir."/".$file_unique_name) || !mosChmod($base_Dir."/".$file_unique_name)) {
			mosErrorAlert("Upload of ".$userfile_name." failed");
		}
	}
	
	//Max (05.06.2013) - fix delete old folder and file scorm when edit save
	if($lms_n_scrom_id){
		$query = "SELECT c.*"
		. "\n FROM"
		. "\n #__lms_n_scorm as b"
		. "\n, #__lms_scorm_packages as c"
		. "\n WHERE 1"
		. "\n AND b.scorm_package = c.id"
		. "\n AND b.id = '".$lms_n_scrom_id."'"
		;
		$JLMS_DB->setQuery($query);
		$scorm_packages = $JLMS_DB->loadObjectList();
		
		if(isset($scorm_packages) && count($scorm_packages)){
			
			jimport('joomla.filesystem.folder');
			jimport('joomla.filesystem.file');
			
			$base_Dir = _JOOMLMS_SCORM_FOLDER_PATH;	
			
			foreach($scorm_packages as $scorm_pack){
				$file = $base_Dir . DS . $scorm_pack->package_srv_name;
				$folder = $base_Dir . DS . $scorm_pack->folder_srv_name;
				
				if(JFile::exists($file)){
					JFile::delete($file);
				}
				if(JFolder::exists($folder)){
					JFolder::delete($folder);
				}
				
				$query = "DELETE FROM #__lms_scorm_packages WHERE id = '".$scorm_pack->id."'";
				$JLMS_DB->setQuery($query);
				$JLMS_DB->query();
			}
		}
	}
	//Max (05.06.2013) - fix delete old folder and file scorm when edit save

	$userfile_name = JLMSDatabaseHelper::GetEscaped($userfile_name);
	$query = "INSERT INTO #__lms_scorm_packages (owner_id, course_id, folder_srv_name, package_srv_name, package_user_name, upload_time)"
	. "\n VALUES ('".$my->id."', '".$course_id."', '".$fold_unique_name."', '".$file_unique_name."', '".$userfile_name."', '".date('Y-m-d H:i:s')."')";
	$JLMS_DB->SetQuery( $query );
	$JLMS_DB->query();
	
	if ($JLMS_DB->geterrormsg()) { //strange? he-he (it did the trick on GoDaddy's servers after losing connection to the DB)
		sleep(1);
		$conf = JFactory::getConfig();
		$j_db_options = array();
		$j_db_options['host'] = $conf->get('host');
		$j_db_options['user'] = $conf->get('user');
		$j_db_options['password'] = $conf->get('password');
		$j_db_options['database'] = $conf->get('database');
		$j_db_options['prefix'] = $conf->get('dbprefix');
		$JLMS_DB->__construct($j_db_options);
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
	}
	$scorm_id = $JLMS_DB->insertid();
	return $scorm_id;
}

function replace_dangerous_char($filename, $strict = 'loose') {
	$filename = preg_replace("/\.+$/", "", substr(strtr(
		preg_replace("/[^!-~\x80-\xFF\x20]/", "_", trim($filename)),
		'\/:*?"<>|\'', /* Keep C1 controls for UTF-8 streams */'-----_---_'), 0, 250));
	if ($strict != 'strict') return $filename;
	return preg_replace("/[^!-~\x20]/", "x", $filename);
}

function JLMS_checkFiles($course_id, $file_ids) {
	global $JLMS_DB;
	if (count($file_ids)) {
		$file_ids_str = implode( ',', $file_ids );
		$query = "SELECT distinct file_id FROM #__lms_documents WHERE course_id = '".$course_id."' AND file_id IN ( $file_ids_str )";
		$JLMS_DB->SetQuery( $query );
		$files_docs = JLMSDatabaseHelper::LoadResultArray();
		$query = "SELECT distinct file_id FROM #__lms_dropbox WHERE course_id = '".$course_id."' AND file_id IN ( $file_ids_str )";
		$JLMS_DB->SetQuery( $query );
		$files_drop = JLMSDatabaseHelper::LoadResultArray();
		$files_used = array_merge($files_docs, $files_drop);
		$files_to_del = array_diff($file_ids, $files_used);
	} else {
		$files_to_del = array();
		
	}
	return $files_to_del;
}

function JLMS_deleteFiles($file_ids, $check_usage = false) {
	global $JLMS_DB;
	if (is_array($file_ids)) {
		$file_ids_str = implode( ',', $file_ids );
	} elseif (intval($file_ids)) {
		$file_ids_str = intval($file_ids);
	} else {
		return false;
	}
	$query = "SELECT * FROM #__lms_files WHERE id IN ( $file_ids_str )";
	$JLMS_DB->SetQuery( $query );
	$files_data = $JLMS_DB->LoadObjectList();
	if (count($files_data)) {
		$del_ids = array();
		foreach ($files_data as $del_file) {
			$srv_name = _JOOMLMS_DOC_FOLDER . $del_file->file_srv_name;
			if (@unlink($srv_name)) { $del_ids[] = $del_file->id;}
		}
		if (count($del_ids)) {
			$del_ids_str = implode( ',', $del_ids );
			$query = "DELETE FROM #__lms_files WHERE id IN ( $del_ids_str )";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
		}
	}
}
function JLMS_deleteZipPacks( $course_id, $zip_ids, $check_usage = false) {
	global $JLMS_DB;
	$zip_ids_str = implode( ',', $zip_ids );
	$query = "SELECT * FROM #__lms_documents_zip WHERE id IN ( $zip_ids_str ) AND course_id = '".$course_id."'";
	$JLMS_DB->SetQuery( $query );
	$zips_data = $JLMS_DB->LoadObjectList();
	$del_ids = array();
	if (count($zips_data)) {
		require_once(_JOOMLMS_FRONT_HOME . "/includes/jlms_dir_operation.php");
		$scorm_folder = JPATH_SITE . "/" . _JOOMLMS_SCORM_FOLDER . "/";
		foreach ($zips_data as $del_zip) {
			deldir( $scorm_folder . $del_zip->zip_folder . "/" );
			unlink( $scorm_folder . $del_zip->zip_srv_name );
			$del_ids[] = $del_zip->id;
		}
		if (count($del_ids)) {
			$del_ids_str = implode( ',', $del_ids );
			$query = "DELETE FROM #__lms_documents_zip WHERE id IN ( $del_ids_str )";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
		}
	}
	return $del_ids;
}
// return tree-list of course docs (if $exclude_id exists - this id and his childs will be excluded from the list)
function Create_CourseFoldersList($course_id, $def_value, $exclude_id = null) {
	global $JLMS_DB;
	$c_folders = array();
	$c_folders[] = mosHTML::makeOption(0, _JLMS_SB_COURSE_FOLDER);
	$query = "SELECT id as value, doc_name as text, parent_id, doc_name, id, folder_flag, ordering"
	. "\n FROM #__lms_documents WHERE folder_flag = 1 AND course_id = '".$course_id."'"
	. "\n ORDER BY parent_id, doc_name";
	$JLMS_DB->SetQuery( $query );
	$rows = $JLMS_DB->LoadObjectList();
	$rows = JLMS_GetTreeStructure( $rows );
	$ex_ids = array();
	if ($exclude_id) {
		$ex_ids[] = $exclude_id;
		$all_rows = array();
		$all_rows = jlms_tree_struct_main($rows, $all_rows,$exclude_id);
		foreach ($all_rows as $ei) {
			$ex_ids[] = $ei->id;
		}
	}
	$rows_new = array();
	$i = 0;
	while ($i < count($rows)) {
		if (!in_array($rows[$i]->id, $ex_ids)) {
			$rows_new[] = $rows[$i];
		}
		$i ++;
	}

	$i = 0;
	$c_folders = array_merge($c_folders, $rows_new);
	for ($i = 0; $i < count($c_folders); $i ++) {
		$k = (isset($c_folders[$i]->tree_mode_num) && $c_folders[$i]->tree_mode_num)?$c_folders[$i]->tree_mode_num:0;
		$c_folders[$i]->text = str_repeat('-&nbsp;', $k) . $c_folders[$i]->text;
		unset($c_folders[$i]->id);
	}
	return mosHTML::selectList( $c_folders, 'course_folder', 'class="inputbox" size="1" style="width:275px;"', 'value', 'text', $def_value );
}


//to do: eta function dolgna vozvraschat' tree-structure of all user documents + folder 'public repository' with all public documents
// + shtoby document stal 'for public' an dolgen proiti 'admin approving'.
// + podstavlyat' vnachalo nazvanii failov kotorye vlogennye - ' > ' dva vlogeniya - ' > > '
// + add 'ordering' field to #__lms_documents
// 11.12.2006 - izmenenie function (ubral owner_id)
function GetTeacherDocuments($user_id, $course_id, $published = 0) {
	global $JLMS_DB;
	$query = "SELECT a.*, b.file_name "
	. "\n FROM #__lms_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id"
	. "\n WHERE a.course_id = '".$course_id."'"
#	. "\n AND a.owner_id = '".$user_id."'" //(11.12.2006)
	. ($published ? "\n AND a.published = 1" : '' )
	. "\n ORDER BY a.parent_id, a.ordering, a.doc_name";
	// etot ordering vagen dlya JLMS_GetTreeStructure(); !!! (11.12.2006 - uge ne vagen)
	$JLMS_DB->SetQuery( $query );
	$rows = $JLMS_DB->LoadObjectList();
	$bad_in = array();
	$rows_n = array();
	for($j=0;$j<count($rows);$j++){
		if($rows[$j]->folder_flag == 3){
			$query = "SELECT a.*, b.file_name FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0 WHERE a.id=".$rows[$j]->file_id." AND a.folder_flag = 0";
			$JLMS_DB->SetQuery( $query );
			$out_row = $JLMS_DB->LoadObjectList();
			if(count($out_row) && isset($out_row[0]->allow_link) && ($out_row[0]->allow_link == 1)) {
				$rows[$j]->doc_name = $out_row[0]->doc_name;
				$rows[$j]->doc_description = $out_row[0]->doc_description;
				$rows[$j]->file_id = $out_row[0]->file_id;
				$rows[$j]->file_name = $out_row[0]->file_name;
			} else {
				$g = 0;
				$rows_n = array();
				for($z=0;$z<count($rows);$z++){
					if($z != $j && !in_array($z,$bad_in))
					{
						$rows_n[$g] = $rows[$z];
						$g++;
					}
					else{
						$bad_in[] = $j;
					}

				}

			}
		}
	}

	if(count($rows_n)){
		$rows = $rows_n;
	}
	return $rows;
}
//to do: sm. previous function
function GetTeacherLinks($user_id, $course_id, $published = 0) {
	global $JLMS_DB;
	$query = "SELECT a.* "
	. "\n FROM #__lms_links as a "
	. "\n WHERE a.course_id = '".$course_id."'"
	. ($published ? "\n AND a.published = 1" : '' )
	#. "\n AND a.owner_id = '".$user_id."'" //(11.12.2006)
	. "\n ORDER BY a.ordering, a.link_name";
	$JLMS_DB->SetQuery( $query );
	$rows = $JLMS_DB->LoadObjectList();
	return $rows;
}

function AppendFileIcons_toList($rows) {
     $keys = array_keys($rows);
     foreach ($keys as $key) {
		if ($rows[$key]->file_name) {
			$file_ext = strtolower(substr($rows[$key]->file_name,-4));
			switch ($file_ext) {
				case '.mp3': case '.wma':
								$rows[$key]->file_icon = 'file_audio';			break;
				case '.flv': case '.swf': case 'swfl':
								$rows[$key]->file_icon = 'file_flash';			break;
				case '.avi': case '.mp4': case 'mpeg':
				case '.mpe': case '.mpg': case '.wmv':
				case '.mov': case '3gp':
				case '.ram':
								$rows[$key]->file_icon = 'file_video';			break;

				case '.pdf':	$rows[$key]->file_icon = 'file_acrobat';		break;
				case 'xlsx':
				case '.xls':
				case '.csv':	$rows[$key]->file_icon = 'file_excel';			break;
				case '.rar':
				case '.zip':	$rows[$key]->file_icon = 'file_package';		break;
				case '.gif': case '.bmp': case '.jpg':
				case '.jpe': case 'jpeg': case '.pct':
				case '.pic': case 'pict': case '.png':
				case '.svg': case 'svgz': case '.tif': case 'tiff':
								$rows[$key]->file_icon = 'file_picture';		break;
				case 'pptx':
				case 'ppsx':
				case '.pps':
				case '.ppt':	$rows[$key]->file_icon = 'file_powerpoint';		break;
				case '.txt':	$rows[$key]->file_icon = 'file_text';			break;
				case '.rtf':
				case 'docx':
				case '.doc':	$rows[$key]->file_icon = 'file_word';			break;
				default:
					switch (strtolower(substr($rows[$key]->file_name,-3))) {
						case '.ra':	$rows[$key]->file_icon = 'file_audio';		break;
						case '.qt':	$rows[$key]->file_icon = 'file_video';		break;
						case '.rm':	$rows[$key]->file_icon = 'file_video';		break;
						default:	$rows[$key]->file_icon = 'file_none';		break;
					}
			}
          } else {
               if ( isset($rows[$key]->folder_flag) && ($rows[$key]->folder_flag == 2) ) {
                    $rows[$key]->file_icon = 'file_zippack';
               } elseif ( !$rows[$key]->file_id && (!$rows[$key]->folder_flag || $rows[$key]->folder_flag == 3)) {
                    $rows[$key]->file_icon = 'file_content';
               } else {
                    $rows[$key]->file_icon = 'file_none';
               }
          }
     }
     return $rows;
}

/// to do: !!PROTESTIROVAT' !!!!!!
function jlms_tree_get_childs( $rows, $id ) {
	$i = 0;
	$childs = array();
	while( $i < count($rows)) {
		if ($rows[$i]->parent_id == $id) {
			$childs[] = $rows[$i];
		}
		$i ++;
	}
	return $childs;
}
function jlms_tree_get_child_ids( $rows, $id ) {
	$i = 0;
	$childs = array();
	while( $i < count($rows)) {
		if ($rows[$i]->parent_id == $id) {
			$childs[] = $rows[$i]->id;
		}
		$i ++;
	}
	return $childs;
}
function jlms_tree_sort_childs( $new_rows ) {
	$i = 0;
	while ($i < (count($new_rows))) {
		$j = $i + 1;
		while ($j < count($new_rows)) {
			if ( ( ($new_rows[$j]->folder_flag == 1 && ( $new_rows[$i]->folder_flag == 2 || $new_rows[$i]->folder_flag == 0) ) ||
			(($new_rows[$j]->folder_flag == $new_rows[$i]->folder_flag) && strcasecmp($new_rows[$j]->doc_name,$new_rows[$i]->doc_name) < 0) ) &&
			($new_rows[$j]->parent_id == $new_rows[$i]->parent_id) ) {
				$tmp = new stdClass();
				$tmp = $new_rows[$i];
				$new_rows[$i] = $new_rows[$j];
				$new_rows[$j] = $tmp;
			}
			$j ++;
		}
		$i ++;
	}
	return $new_rows;
}
function jlms_tree_sort_childs_lpath( $new_rows ) {
	$i = 0;
	while ($i < (count($new_rows))) {
		$j = $i + 1;
		while ($j < count($new_rows)) {
			if ( ( ($new_rows[$j]->ordering < $new_rows[$i]->ordering) || (($new_rows[$j]->ordering == $new_rows[$i]->ordering) &&
			strcasecmp($new_rows[$j]->doc_name,$new_rows[$i]->doc_name) < 0) ) && ($new_rows[$j]->parent_id == $new_rows[$i]->parent_id) ) {
				$tmp = new stdClass();
				$tmp = $new_rows[$i];
				$new_rows[$i] = $new_rows[$j];
				$new_rows[$j] = $tmp;
			}
			$j ++;
		}
		$i ++;
	}
	return $new_rows;
}
function jlms_tree_append_childs( $rows, $new_rows ) {
	$ret_rows = array();
	$i = 0;
	if (count( $rows)) {
		if (count($new_rows)) {
			while ($i < (count($rows))) {
				if ($rows[$i]->id == $new_rows[0]->parent_id) {
					$ret_rows[] = $rows[$i];
					foreach ($new_rows as $new_row) {
						$ret_rows[] = $new_row;
					}
				} else {
					$ret_rows[] = $rows[$i];
				}
				$i ++;
			}
		} else { $ret_rows = $rows; }
	} else { $ret_rows = $new_rows; }
	return $ret_rows;
}
function jlms_tree_struct_main($rows, $all_rows, $id, $data_type = 1) {
	$new_rows = array();
	$new_rows_ids = array();
	$new_rows = jlms_tree_get_childs($rows, $id);
	$new_rows_ids = jlms_tree_get_child_ids($rows, $id);
	if ($data_type == 1) {
		$new_rows = jlms_tree_sort_childs($new_rows);
	} else {
		$new_rows = jlms_tree_sort_childs_lpath($new_rows);
	}
	$all_rows = jlms_tree_append_childs($all_rows, $new_rows);
	foreach ($new_rows_ids as $new_row_id) {
		$all_rows = jlms_tree_struct_main($rows, $all_rows, $new_row_id, $data_type);
	}
	return $all_rows;
}

function JLMS_GetTreeStructure( $rows ) {
	$all_rows = array();
	$new_rows = array();
	$new_rows_ids = array();
	$all_rows = jlms_tree_struct_main($rows, $all_rows, 0, 2);

	$rows = $all_rows;

	//poschitat' dlya kagdogo parenta kol-vo itemov
	$all_parents = array();
	$items_in_parent = array();
	$i = 0;
	while ($i < count($rows)) {
		$rows[$i]->tree_mode = 0;
		if (in_array($rows[$i]->parent_id, $all_parents)) {
			$items_in_parent[array_search($rows[$i]->parent_id, $all_parents)] ++;
		} else {
			$all_parents[] = $rows[$i]->parent_id;
			$items_in_parent[] = 1;
		}
		$i ++;
	}
	$i = 0;
	$str_add = '&nbsp;|&nbsp;';
	$prev_par_id = -1;
	$prev_id = -1;
	$str_add1 = $str_add;
	$kol_p = 0;
	$prev_parents = array(); // array of previous parents
	$c_all_parents = array();
	$c_items_in_parent = array();
	$max_tree_width = 0;
	while ($i < count($rows)) {
		if ($kol_p > $max_tree_width) { $max_tree_width = $kol_p; }
		if (in_array($rows[$i]->parent_id, $c_all_parents)) {
			$c_items_in_parent[array_search($rows[$i]->parent_id, $c_all_parents)] ++;
		} else {
			$c_all_parents[] = $rows[$i]->parent_id;
			$c_items_in_parent[] = 1;
		}
		$rows[$i]->tree_mode_num = 0;
		$rows[$i]->tree_mode = 0;
		$str_add1 = '';
		if ($rows[$i]->parent_id) {
			if ($rows[$i]->parent_id == $prev_par_id) {

			} else {
				if ($rows[$i]->parent_id == $prev_id) {
					$kol_p ++;
					$prev_par_id = $rows[$i]->parent_id;
					$prev_parents[$kol_p] = $prev_par_id;
				} else {
					while ( ($kol_p > 0) && ($prev_parents[$kol_p] != $rows[$i]->parent_id)) {
						$kol_p --;
					}
					$prev_par_id = $rows[$i]->parent_id;
				}
			}
			if ($kol_p > 0) {
				$str_add1 = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $kol_p - 1 ).'&nbsp;-&nbsp;';
				$rows[$i]->tree_mode = 2;
				$rows[$i]->tree_mode_num = $kol_p;
				if ($kol_p > $max_tree_width) { $max_tree_width = $kol_p; }
				$r = array_search($rows[$i]->parent_id, $c_all_parents);
				if ($c_items_in_parent[$r] < $items_in_parent[$r]) { $rows[$i]->tree_mode = 1; }
			}
		} else { $str_add1 = ''; $kol_p = 0;}
		$prev_id = $rows[$i]->id;
		//$str_add1 = '';//////////////////////////////////
		//$rows[$i]->doc_name = $str_add1 . $rows[$i]->doc_name;
		$rows[$i]->doc_name_tree = $str_add1 . $rows[$i]->doc_name;
		$r = array_search($rows[$i]->parent_id, $c_all_parents);
		if ($c_items_in_parent[$r] > 1) { $rows[$i]->allow_up = 1; } else { $rows[$i]->allow_up = 0; }
		if ($c_items_in_parent[$r] < $items_in_parent[$r]) { $rows[$i]->allow_down = 1; } else { $rows[$i]->allow_down = 0; }
		$i ++;
	}
	$i = 0;
	while ($i < count($rows)) {
		$rows[$i]->tree_max_width = $max_tree_width;
		$i ++;
	}
	return $rows;
}

function JLMS_ShowHeading( $heading = '_default_', $show_course_name = true) {
	$JLMS_CONFIG = & JLMSFactory::getConfig();
	if ($heading == '_default_')  {
		$heading = $JLMS_CONFIG->get('jlms_heading', '');
	}

	$heading = trim($heading);
	$main_heading_open = $JLMS_CONFIG->get('main_heading_open','<div class="componentheading" id="jlms_topdiv">');

	$background_class = '';
	if($JLMS_CONFIG->get('course_id')){
		$background_class = ' heading_course_'.$JLMS_CONFIG->get('course_id');	
	
		preg_match('#class="([a-z0-9]*)"#', $main_heading_open, $out_open);
		if(isset($out_open[1]) && $out_open[1]){
			$replace = $out_open[1].$background_class;
			$main_heading_open = str_replace($out_open[1], $replace, $main_heading_open);	
		}
	}
	if ($show_course_name && $JLMS_CONFIG->get('course_name')) {
		$doc = JFactory::getDocument();
		$doc->setTitle($doc->getTitle().' - '.$JLMS_CONFIG->get('course_name'));
	}

	$course_name = '';
	if($show_course_name && $JLMS_CONFIG->get('course_name') && strlen($heading)){
		$course_name .= ' - ';
	}

	if($show_course_name && $JLMS_CONFIG->get('course_name')){
		$course_name .= trim($JLMS_CONFIG->get('course_name'));
	}

	if (strlen($heading) || strlen($course_name)) {
		echo $main_heading_open.$JLMS_CONFIG->get('main_heading_tag_open','').$heading.$course_name.$JLMS_CONFIG->get('main_heading_tag_close','').$JLMS_CONFIG->get('main_heading_close','</div>');
	}


	JLMS_HTML::_('behavior.mootools');


	$doc = JFactory::getDocument();
	JLMS_addStyleSheet();
	$doc->addScript(JURI::base().'components/com_joomla_lms/includes/js/jlms_106.js');

/*	//commented for JoomlaLMS 1.0.7 ... there is no addCustomHeadTag in Joomla 1.6 and IE6 is out of date
	if ($JLMS_CONFIG->get('do_ie6_png_fix',true)) {
		$head_tag = '
			<!--[if lt IE 7]>
			<style type="text/css">
				img.JLMS_png {
					behavior: url("'.JURI::base().'components/com_joomla_lms/includes/pngbehavior.htc");
				}
			</style>
			<![endif]-->
		';
		$app = JFactory::getApplication();
		$app->addCustomHeadTag($head_tag);
	}
*/
}

function JLMS_addStyleSheet() 
{
	$doc = JFactory::getDocument();
	$doc->addStyleSheet(JLMSCSS::link());	
}

function JLMS_GetLPathElementType($element_id) {
	global $JLMS_DB;
	$query = "SELECT step_type FROM #__lms_learn_path_steps WHERE id = '".$element_id."'";
	$JLMS_DB->SetQuery( $query );
	return $JLMS_DB->LoadResult();
}
function JLMS_GetCourseOwner($course_id) {
	global $JLMS_DB;
	$query = "SELECT owner_id FROM #__lms_courses WHERE id = '".$course_id."'";
	$JLMS_DB->SetQuery( $query );
	return $JLMS_DB->LoadResult();
}
function JLMS_GetGroupCourse( $group_id ) {
	global $JLMS_DB;
	$query = "SELECT course_id FROM #__lms_usergroups WHERE id = '".$group_id."'";
	$JLMS_DB->SetQuery( $query );
	return $JLMS_DB->LoadResult();
}
function JLMS_GetDocumentCourse($doc_id) {
	global $JLMS_DB;
	$query = "SELECT course_id FROM #__lms_documents WHERE id = '".$doc_id."'";
	$JLMS_DB->SetQuery( $query );
	return $JLMS_DB->LoadResult();
}
function JLMS_GetDropItemCourse($drop_id) {
	global $JLMS_DB;
	$query = "SELECT course_id FROM #__lms_dropbox WHERE id = '".$drop_id."'";
	$JLMS_DB->SetQuery( $query );
	return $JLMS_DB->LoadResult();
}

function JLMS_GetDocumentOwner($file_id) {
	global $JLMS_DB;
	$query = "SELECT owner_id FROM #__lms_documents WHERE id = '".$file_id."'";
	$JLMS_DB->SetQuery( $query );
	return $JLMS_DB->LoadResult();
}

function JLMS_isUserCourse($user_id, $course_id) {
	global $JLMS_DB;
	$query = "SELECT count(*) FROM #__lms_users_in_groups WHERE course_id = '".$course_id."' AND user_id = '".$user_id."'";
	$JLMS_DB->SetQuery($query);
	$r = $JLMS_DB->LoadResult();
	if ($r) { return true; } else { return false; }
}
function JLMS_getCourseStudentsIds($course_id, $group_id = 0) {
	global $JLMS_DB;
	$query = "SELECT a.id"
	. "\n FROM #__users as a, #__lms_users_in_groups as c"
	. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."'"
	. ($group_id ? ("\n AND c.group_id = '".$group_id."'") : '' );
	$JLMS_DB->SetQuery( $query );
	$users = JLMSDatabaseHelper::LoadResultArray();
	return $users;
}
function JLMS_getCourseStudentsListLimited($course_id, &$members_only, $group_id = 0, $subgroup_id = 0) {
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$JLMS_DB = & JLMSFactory::getDB();             
	$user = JLMSFactory::getUser();
	$members_only_txt = implode(',', $members_only);
	if (!$members_only_txt) {
		$members_only_txt = '0';
	}
	if ($JLMS_CONFIG->get('use_global_groups', 1)){
		$query = "SELECT a.id, a.name, a.username, a.email "
//		. ($group_id ? "\n b.ug_name" : "\n '' as ug_name")
		. "\n FROM #__users as a, #__lms_users_in_groups as c"
		. ($group_id ? "\n , #__lms_users_in_global_groups as b" : '')
		. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."'"
		. "\n AND a.id IN ($members_only_txt)"
		. ($group_id ? "\n AND a.id = b.user_id AND b.group_id = '".$group_id."'" : '')
		. ($subgroup_id ? "\n AND b.subgroup1_id = '".$subgroup_id."'" : '')
		. "\n ORDER BY a.username"
		;
	} else {
		$query = "SELECT a.id, a.name, a.username, a.email, b.ug_name"
		. "\n FROM #__users as a, #__lms_users_in_groups as c LEFT JOIN #__lms_usergroups as b ON c.group_id = b.id AND b.course_id = '".$course_id."'"
		. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."'"
		. "\n AND a.id IN ($members_only_txt)"
		. ($group_id ? ("\n AND c.group_id = '".$group_id."'") : '' )
		. "\n ORDER BY b.ug_name, a.username";
	}
	$JLMS_DB->SetQuery( $query );
	$users = $JLMS_DB->LoadObjectList();
	
	$i = 0;
	while ($i < count($users)) {
		if ($JLMS_CONFIG->get('use_global_groups', 1)) $users[$i]->username = $users[$i]->name . ' ('.$users[$i]->username.')';
		else $users[$i]->username = ($users[$i]->ug_name?($users[$i]->ug_name.' - '):'').$users[$i]->name . ' ('.$users[$i]->username.')';
		$i ++;
	}
	return $users;
}
function JLMS_getCourseStudentsList($course_id, $group_id = 0, $subgroup_id = 0) {
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$JLMS_DB = & JLMSFactory::getDB();
	
	$JLMS_ACL = JLMSFactory::getACL(); //21.05.2011
	$user = JFactory::getUser();
	if ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only')){
		$course_members_array = array();
		$query = "SELECT a.group_id FROM `#__lms_user_assign_groups` as a WHERE a.user_id = '".$user->id."'"
		;
		$JLMS_DB->setQuery($query);
		$groups_where_admin_manager = JLMSDatabaseHelper::LoadResultArray();
		
		$group_id = 0;
		if(count($groups_where_admin_manager) == 1) {
			$group_id = $groups_where_admin_manager[0];
		}
		
		$course_members_array = array();
		if(count($groups_where_admin_manager)) {
			$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE (group_id IN (".implode(',', $groups_where_admin_manager).") OR subgroup1_id IN (".implode(',', $groups_where_admin_manager)."))"
			. ($group_id ? ("\n AND group_id = '".$group_id."'") : '')
			;
			$JLMS_DB->setQuery($query);
			$course_members_array = JLMSDatabaseHelper::LoadResultArray();
		}
		
		$users_where_ceo_parent = array();
		if($JLMS_ACL->_role_type == 3) {
			$query = "SELECT user_id FROM `#__lms_user_parents` WHERE parent_id = '".$user->id."'"
			;
			$JLMS_DB->setQuery($query);
			$users_where_ceo_parent = JLMSDatabaseHelper::LoadResultArray();
		}

		if(count($users_where_ceo_parent)) {
			$course_members_array = array_merge($course_members_array, $users_where_ceo_parent);
		} else
		if(count($users_where_ceo_parent)) {
			$course_members_array = $users_where_ceo_parent;
		}
	}
	
	//(Max): global groups
	if ($JLMS_CONFIG->get('use_global_groups', 1)){
		$query = "SELECT a.id, a.name, a.username, a.email "
//		. ($group_id ? "\n b.ug_name" : "\n '' as ug_name")
		. "\n FROM #__users as a, #__lms_users_in_groups as c"
		. ($group_id ? "\n , #__lms_users_in_global_groups as b" : '')
		. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."'"
		. ($group_id ? "\n AND a.id = b.user_id AND b.group_id = '".$group_id."'" : '')
		. ($subgroup_id ? "\n AND b.subgroup1_id = '".$subgroup_id."'" : '')
		.(isset($course_members_array) && count($course_members_array) ? "\n AND a.id IN (".implode(',', $course_members_array).")" : '') //21.05.2012
		. "\n ORDER BY a.username"
		;
	} else {
		$query = "SELECT a.id, a.name, a.username, a.email, b.ug_name"
		. "\n FROM #__users as a, #__lms_users_in_groups as c LEFT JOIN #__lms_usergroups as b ON c.group_id = b.id AND b.course_id = '".$course_id."'"
		. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."'"
		. ($group_id ? ("\n AND c.group_id = '".$group_id."'") : '' )
		. "\n ORDER BY b.ug_name, a.username";
	}
	$JLMS_DB->SetQuery( $query );
	$users = $JLMS_DB->LoadObjectList();
	
	$i = 0;
	while ($i < count($users)) {
		if ($JLMS_CONFIG->get('use_global_groups', 1)){
			$users[$i]->username = $users[$i]->name . ' ('.$users[$i]->username.')';
		} else {
			$users[$i]->username = ($users[$i]->ug_name?($users[$i]->ug_name.' - '):'').$users[$i]->name . ' ('.$users[$i]->username.')';
		}
		$i ++;
	}
	return $users;
}
// for 'Global' mode group filtering
function JLMS_getCourseStudentsIds2($course_id, $group_id = 0) {
	global $JLMS_DB;
	if ($group_id) { // course users who is associated with global group $group_id
		$query = "SELECT a.id"
		. "\n FROM #__users as a, #__lms_users_in_groups as c, #__lms_users_in_global_groups as d"
		. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."' AND a.id = d.user_id"
		. ($group_id ? ("\n AND d.group_id = '".$group_id."'") : '' );
	} else { // all course users
		$query = "SELECT a.id"
		. "\n FROM #__users as a, #__lms_users_in_groups as c"
		. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."'"
		. ($group_id ? ("\n AND c.group_id = '".$group_id."'") : '' );		
	}
	$JLMS_DB->SetQuery( $query );
	$users = JLMSDatabaseHelper::LoadResultArray();
	return $users;
}
function JLMS_getCourseStudentsList2($course_id, $group_id = 0) {
	global $JLMS_DB, $JLMS_CONFIG;
	if ($group_id) {
		$query = "SELECT a.id, a.name, a.username, a.email, '' as ug_name"
		. "\n FROM #__users as a, #__lms_users_in_groups as c, #__lms_users_in_global_groups as d"
		. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."' AND a.id = d.user_id"
		. ($group_id ? ("\n AND d.group_id = '".$group_id."'") : '' )
		. "\n ORDER BY a.username";
	} else {
		$query = "SELECT a.id, a.name, a.username, a.email, b.ug_name"
		. "\n FROM #__lms_users_in_groups as c, #__users as a"
		. "\n LEFT JOIN #__lms_users_in_global_groups as d ON a.id = d.user_id"
		. "\n LEFT JOIN #__lms_usergroups as b ON d.group_id = b.id"
		. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."'"
		. "\n ORDER BY b.ug_name, a.username";		
	}
	$JLMS_DB->SetQuery( $query );
	$users = $JLMS_DB->LoadObjectList();
		
	if (!$group_id) {
		$processed_users = array();
		$users2 = array();
		$i = 0;
		while ($i < count($users)) {
			if (!in_array($users[$i]->id,$processed_users)) {
				$users2[] = $users[$i];
				$processed_users[] = $users[$i]->id;
			}
			$i ++;	
		}
		$i = 0;
		while ($i < count($users2)) {
			$users2[$i]->username = ($users2[$i]->ug_name?($users2[$i]->ug_name.' - '):'').$users2[$i]->username . ' ('.$users2[$i]->name.')';
			$i ++;
		}
		return $users2;
	} else {
		$i = 0;
		while ($i < count($users)) {
			if ($JLMS_CONFIG->get('use_global_groups', 1)) $users[$i]->username = $users[$i]->username . ' ('.$users[$i]->name.')';
			else $users[$i]->username = ($users[$i]->ug_name?($users[$i]->ug_name.' - '):'').$users[$i]->username . ' ('.$users[$i]->name.')';
			$i ++;
		}
		return $users;
	}
}
function JLMS_downloadFakeFile() {
	$file_name = 'error.txt';
	$v_date = gmdate("Y-m-d H:i:s");
	if (preg_match('/Opera(\/| )([0-9].[0-9]{1,2})/', $_SERVER['HTTP_USER_AGENT'])) {
		$UserBrowser = "Opera";
	}
	elseif (preg_match('/MSIE ([0-9].[0-9]{1,2})/', $_SERVER['HTTP_USER_AGENT'])) {
		$UserBrowser = "IE";
	} else {
		$UserBrowser = '';
	}
	$mime_type = ($UserBrowser == 'IE' || $UserBrowser == 'Opera') ? 'application/octetstream' : 'application/octet-stream';
	header('Content-Type: ' . $mime_type );
	header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
	if ($UserBrowser == 'IE') {
		header('Content-Disposition: attachment; filename="' . $file_name . '";');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
	} else {
		header('Content-Disposition: attachment; filename="' . $file_name . '";');
		header('Pragma: no-cache');
	}
	@ob_end_clean();
	echo 'There was an error during download file.';
	exit();
}

//to do:
// + add tracking (OK)
// + add messages to user if problems
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// 26.10 eta function - vnutrennyaya (v nei ne dolgno byt' nikakix proverok i ona ne dolgna vyzyvat'sa napryamuyu!
function JLMS_downloadFile( $id, $option, $new_file_name = '', $do_exit = true, $headers = array() ) {
	global $JLMS_DB, $my;
	#if (JLMS_GetFileDownloadPermissions($id, $my->id)) {
	$query = "SELECT file_name, file_srv_name FROM #__lms_files WHERE id = '".$id."'";
	$JLMS_DB->SetQuery($query);
	$file_char = $JLMS_DB->LoadObjectList();
	if (count($file_char)) {
		$srv_name = _JOOMLMS_DOC_FOLDER . $file_char[0]->file_srv_name;
		$file_name = $file_char[0]->file_name;
		if ($new_file_name) {
			if (strcmp(substr($new_file_name,-4,1),".")) {
				//TODO: fix this code
				//bad extension (must be changed)
				$extension = substr($file_name,-4,4);
				//... = end(explode(".", $file_name));
				$test = explode(".", $file_name);
				if (count($test) > 1) {
					$extension = ".".end($test);
				}

				$file_name = $new_file_name.$extension;
			} else {
				$file_name = $new_file_name;
			}
		}
		if ( file_exists( $srv_name ) && is_readable( $srv_name ) ){
			#//tracking
			#global $Track_Object;
			#$Track_Object->UserDownloadFile( $my->id, $id );
			
			if(false){
				//(pending) New features for playing files on mobile devices 
				if (is_file($srv_name)){
					while (ob_get_level()) {
						ob_end_clean();
					}
					header("Content-type: video/mp4");
					if (isset($_SERVER['HTTP_RANGE'])){
						rangeDownload($srv_name);
					} else {
						header("Content-length: " . filesize($srv_name));
						readfile($srv_name);
					}
					die;
				}
				//(pending) New features for playing files on mobile devices 
			} else {
				@set_time_limit(0);
	
				$v_date = date("Y-m-d H:i:s");
				if (preg_match('/Opera(\/| )([0-9].[0-9]{1,2})/', $_SERVER['HTTP_USER_AGENT'])) {
					$UserBrowser = "Opera";
				}
				elseif (preg_match('/MSIE ([0-9].[0-9]{1,2})/', $_SERVER['HTTP_USER_AGENT'])) {
					$UserBrowser = "IE";
				} else {
					$UserBrowser = '';
				}
				
				$mime_type = ($UserBrowser == 'IE' || $UserBrowser == 'Opera') ? 'application/octetstream' : 'application/octet-stream';
				
				header('Content-Type: ' . $mime_type );
				header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
				if ($UserBrowser == 'IE') {
					header('Content-Disposition: '.(isset($headers['Content-Disposition']) ? $headers['Content-Disposition'] : 'attachment').'; filename="' . $file_name . '";');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Content-Length: '. filesize($srv_name)); 
					header('Pragma: public');
				} else {
					header('Content-Disposition: '.(isset($headers['Content-Disposition']) ? $headers['Content-Disposition'] : 'attachment').'; filename="' . $file_name . '";');
					header('Content-Length: '. filesize($srv_name)); 
					header('Pragma: no-cache');
				}
				
				while( @ob_end_clean() );
				
				readfile( $srv_name );
				if ($do_exit) {
					exit();
				}
			}
		}
	}
	#}
}

function rangeDownload($file){
	$fp = @fopen($file, 'rb');

	$size   = filesize($file); // File size
	$length = $size;           // Content length
	$start  = 0;               // Start byte
	$end    = $size - 1;       // End byte
	// Now that we've gotten so far without errors we send the accept range header
	/* At the moment we only support single ranges.
	 * Multiple ranges requires some more work to ensure it works correctly
	 * and comply with the spesifications: http://www.w3.org/Protocols/rfc2616/rfc2616-sec19.html#sec19.2
	 *
	 * Multirange support annouces itself with:
	 * header('Accept-Ranges: bytes');
	 *
	 * Multirange content must be sent with multipart/byteranges mediatype,
	 * (mediatype = mimetype)
	 * as well as a boundry header to indicate the various chunks of data.
	 */
	header("Accept-Ranges: 0-$length");
	// header('Accept-Ranges: bytes');
	// multipart/byteranges
	// http://www.w3.org/Protocols/rfc2616/rfc2616-sec19.html#sec19.2
	if (isset($_SERVER['HTTP_RANGE'])){
		$c_start = $start;
		$c_end   = $end;

		// Extract the range string
		list(, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);
		// Make sure the client hasn't sent us a multibyte range
		if (strpos($range, ',') !== false){
			// (?) Shoud this be issued here, or should the first
			// range be used? Or should the header be ignored and
			// we output the whole content?
			header('HTTP/1.1 416 Requested Range Not Satisfiable');
			header("Content-Range: bytes $start-$end/$size");
			// (?) Echo some info to the client?
			exit;
		} // fim do if
		// If the range starts with an '-' we start from the beginning
		// If not, we forward the file pointer
		// And make sure to get the end byte if spesified
		if ($range{0} == '-'){
			// The n-number of the last bytes is requested
			$c_start = $size - substr($range, 1);
		} else {
			$range  = explode('-', $range);
			$c_start = $range[0];
			$c_end   = (isset($range[1]) && is_numeric($range[1])) ? $range[1] : $size;
		} // fim do if
		/* Check the range and make sure it's treated according to the specs.
		 * http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
		 */
		// End bytes can not be larger than $end.
		$c_end = ($c_end > $end) ? $end : $c_end;
		// Validate the requested range and return an error if it's not correct.
		if ($c_start > $c_end || $c_start > $size - 1 || $c_end >= $size){
			header('HTTP/1.1 416 Requested Range Not Satisfiable');
			header("Content-Range: bytes $start-$end/$size");
			// (?) Echo some info to the client?
			exit;
		} // fim do if

		$start  = $c_start;
		$end    = $c_end;
		$length = $end - $start + 1; // Calculate new content length
		fseek($fp, $start);
		header('HTTP/1.1 206 Partial Content');
	} // fim do if

	// Notify the client the byte range we'll be outputting
	header("Content-Range: bytes $start-$end/$size");
	header("Content-Length: $length");

	// Start buffered download
	$buffer = 1024 * 8;
	while(!feof($fp) && ($p = ftell($fp)) <= $end){
		if ($p + $buffer > $end){
			// In case we're only outputtin a chunk, make sure we don't
			// read past the length
			$buffer = $end - $p + 1;
		} // fim do if

		@set_time_limit(0); // Reset time limit for big files
		echo fread($fp, $buffer);
		flush(); // Free up memory. Otherwise large files will trigger PHP's memory limit.
	} // fim do while

	fclose($fp);
} // fim do function

/* function return usertype
// 2 - student
// 1 - teacher
// 4 - assistant
if $transfrom_assist = true - return for assistant 1 - teacher type.
// (31.10) TODO:
// todo: insert into $_SESSION time poslednei proverki 4erez DB.
// a potom sravnivat' cur.time i etot time i smotret' esli proshlo kakoe-to vremya (30min), to opyat' sravnivat' 4erez DB.
*/
function JLMS_GetUserType($user_id, $course_id = 0, $ignore_switch = false, $transfrom_assist = true, $ignore_user_expiry = false, $ignore_course_expiry = false) {
	global $JLMS_DB, $JLMS_SESSION, $my;
	if (!$course_id) { echo '<script>alert("SYSTEM MESSAGE:\nYour course_id is undefined.");</script>'; }
	$use_session = true;
	if ($user_id != $my->id) { $use_session = false;}/*echo '<script>alert("SYSTEM MESSAGE:\nThere was an error during checking user rights");</script>';die; }*/
	$do_check = true;
	$usertype = 0;
	if ( $use_session ) {
		#if ($JLMS_SESSION->has('jlms_auth_user') && ($JLMS_SESSION->get('jlms_auth_user') == $user_id)) {
		if ($JLMS_SESSION->has('jlms_user_course') && ($JLMS_SESSION->get('jlms_user_course') == $course_id)) {
			if ($JLMS_SESSION->has('jlms_user_type')) {
				$usertype = $JLMS_SESSION->get('jlms_user_type', 0);
				$do_check = false;
				if ($usertype == 1 && !$transfrom_assist) {
					$do_check = true;
					$usertype = 0;
				}
				if ($usertype == 4 && $transfrom_assist) {
					$do_check = true;
					$usertype = 0;
				}
				if ($usertype == 6) {
					if ($JLMS_SESSION->get('switch_usertype') == 6) {
						if ($transfrom_assist) {
							$do_check = false;
							$usertype = 6;
						} else {
							$do_check = true;
							$usertype = 0;
						}
					} else {
						$do_check = true;
						$usertype = 0;
					}
				}
				if ($usertype == 0) {
					$do_check = true;
				}
			} else { $JLMS_SESSION->clear('jlms_user_course'); }
		} else { $JLMS_SESSION->clear('jlms_user_course'); $JLMS_SESSION->clear('jlms_user_type'); }
		#}
	}
	if ($do_check) {
		if ($course_id) {
			$query = "SELECT b.roletype_id FROM #__lms_users as a, #__lms_usertypes as b WHERE a.lms_usertype_id = b.id AND a.user_id = '".$user_id."'";
			$JLMS_DB->SetQuery( $query );
			$usertype = $JLMS_DB->LoadResult();

/** important to know:
Super: 		// NEW roletype == 4; OLD usertype == 5
teacher: 	// NEW roletype == 2; OLD usertype == 1
assistnat: 	// NEW roletype == 5; OLD usertype == 4
*********************/
			if ($usertype == 4) { // SUPER USER !!! // roletype == 4; usertype == 5
				$usertype = 1;
			} else {
				$usertype = 0;
			}
			#if ($usertype) {
			#	if ($usertype == 1) {
			if (!$usertype) {
				$query = "SELECT b.roletype_id FROM #__lms_user_courses as a, #__lms_usertypes as b WHERE a.role_id = b.id AND a.user_id = '".$user_id."' AND a.course_id = '".$course_id."'";
				$JLMS_DB->SetQuery( $query );
				$usertype_course = $JLMS_DB->LoadResult();
				if ($usertype_course) {
					if ($usertype_course == 2) {
						$usertype = 1;
					} elseif ($usertype_course == 5) {
						$usertype = 4;
					} else {
						$usertype = 0; // wtf ???
					}
				} else {
					$usertype = 0;
				}
			}
			#if ($usertype_course != 1) { $usertype = $usertype_course ? $usertype_course : 0; }
			#	}
			#}
			if (!$usertype) {
				$query = "SELECT a.user_id FROM #__lms_users_in_groups as a, #__lms_courses as b"
				. "\n WHERE a.user_id = '".$user_id."' AND a.course_id = '".$course_id."' AND a.course_id = b.id";
				if (!$ignore_user_expiry) {
					$query .= "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )"
					. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d')."')) OR (a.publish_end = 0) )";
				}
				if (!$ignore_course_expiry) {
					$query .= "\n AND b.published = 1"
					. "\n AND ( ((b.publish_start = 1) AND (b.start_date <= '".date('Y-m-d')."')) OR (b.publish_start = 0) )"
					. "\n AND ( ((b.publish_end = 1) AND (b.end_date >= '".date('Y-m-d')."')) OR (b.publish_end = 0) )";
				}
				$JLMS_DB->SetQuery( $query );
				$usertype2 = $JLMS_DB->LoadResult();
				/*$query = "SELECT a.user_id FROM #__lms_users_in_groups as a"//, #__lms_usergroups as b"
				. "\n WHERE a.user_id = '".$user_id."' AND a.course_id = '".$course_id."'";//a.group_id = b.id AND b.course_id = '".$course_id."'";
				$JLMS_DB->SetQuery( $query );
				$usertype2 = $JLMS_DB->LoadResult();*/
				if ($usertype2) { $usertype = 2; } else { $usertype = 0; }
			}
			if ( $use_session ) {
				$JLMS_SESSION->set('jlms_user_course', $course_id);
			}
		} else {
			$query = "SELECT b.roletype_id FROM #__lms_users as a, #__lms_usertypes as b WHERE a.lms_usertype_id = b.id AND a.user_id = '".$user_id."'";
			$JLMS_DB->SetQuery( $query );
			$usertype = $JLMS_DB->LoadResult();
			
			if ($usertype == 4) { // SUPER USER !!!
				$usertype = 1;
			} else {
				$usertype = 0;
			}
			/*$mode_select = strval(mosGetParam($_REQUEST, 'def_mode', ''));
			if ($mode_select == 'stu') { $usertype = 2; }*/
		}
		if ( $transfrom_assist && ($usertype == 4) ) { $usertype = 1; }
		if ( $use_session ) {
			$JLMS_SESSION->set('jlms_user_type', $usertype);
		}
	}
	//user switching allowed only if '$use_session' is set. ($use_session is set when $user_id == $my->id)
	if ( $use_session ) {
		if (!$ignore_switch) {
			if ( ($usertype == 1) && $JLMS_SESSION->has('switch_usertype') && ($JLMS_SESSION->get('switch_usertype') == 2) ) {
				$usertype = 2;
			}
		}
	}
	return $usertype;
}
function JLMS_GetUserType_simple($user_id, $ignore_switch = false, $ret_super = false) {
	global $JLMS_DB, $JLMS_SESSION;
	$query = "SELECT lms_usertype_id FROM #__lms_users WHERE user_id = '".$user_id."'";
	$JLMS_DB->SetQuery( $query );
	$usertype = $JLMS_DB->LoadResult();
/** important to know:
Super: 		// NEW roletype == 4; OLD usertype == 5
teacher: 	// NEW roletype == 2; OLD usertype == 1
assistnat: 	// NEW roletype == 5; OLD usertype == 4
learner:	// _________________; OLD usertype == 2
*********************/
	$query = "SELECT b.roletype_id FROM #__lms_users as a, #__lms_usertypes as b WHERE a.lms_usertype_id = b.id AND a.user_id = '".$user_id."'";
	$JLMS_DB->SetQuery( $query );
	$usertype = $JLMS_DB->LoadResult();
	if ($usertype == 4) {
		$usertype = 5;
	} elseif ($usertype == 2) {
		$usertype = 1;
	} else {
		$usertype = 0;
	}
	if (!$usertype) { $usertype = 2;}
	if ($usertype == 5 && !$ret_super) { $usertype = 1;}
	if (!$ignore_switch) {
		if ( ($usertype == 1) && $JLMS_SESSION->has('switch_usertype') && ($JLMS_SESSION->get('switch_usertype') == 2) ) {
			$usertype = 2;
		}
	}
	return $usertype;
}
function JLMS_orderUpIcon( $i, $id, $condition=true, $task='orderup', $alt=_JLMS_MOVEUP ) {
	$JLMS_CONFIG = & JLMSFactory::getConfig();
	$ret_str = '';
	if (($i > 0) && $condition) {
		$ret_str = '<a class="jlms_img_link" href="javascript:submitbutton_order(\''.$task.'\',\''.$id.'\');" title="'.$alt.'">';
		$ret_str .= '<img class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/btn_uparrow.png" width="16" height="16" border="0" alt="'.$alt.'" />';
		$ret_str .= '</a>';
	} else {
		$ret_str = '&nbsp;';
	}
	return $ret_str;
}
function JLMS_orderDownIcon( $i, $n, $id, $condition=true, $task='orderdown', $alt=_JLMS_MOVEDOWN ) {
	$JLMS_CONFIG = & JLMSFactory::getConfig();
	$ret_str = '';
	if (($i < $n-1) && $condition) {
		$ret_str = '<a class="jlms_img_link" href="javascript:submitbutton_order(\''.$task.'\',\''.$id.'\');" title="'.$alt.'">';
		$ret_str .= '<img class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/btn_downarrow.png" width="16" height="16" border="0" alt="'.$alt.'" />';
		$ret_str .= '</a>';
	} else {
		$ret_str = '&nbsp;';
	}
	return $ret_str;
}
function JLMS_publishIcon( $id, $course_id, $state, $task, $alt, $image, $option, $add_options = '' ) {
	global $Itemid, $JLMS_CONFIG;
	// !! NO sefRelToAbs !!! - glucki s cid[]
	$link = "index.php?option=".$option."&amp;Itemid=".$Itemid."&amp;task=".$task."&amp;state=".$state."&amp;id=".$course_id."&amp;cid[]=".$id;
	$ret_str = '<a class="jlms_img_link" '.$add_options.'href="'.$link.'" title="'.$alt.'">';
	$ret_str .= '<img class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/'.$image.'" width="16" height="16" border="0" alt="'.$alt.'" />';
	$ret_str .= '</a>';
	return $ret_str;
}
function JLMS_publishIcon2( $id, $course_id, $state, $task, $alt, $image, $option, $add_options = '' ) {
	global $Itemid, $JLMS_CONFIG;
	$link = "index.php?option=".$option."&amp;Itemid=".$Itemid."&amp;task=".$task."&amp;state=".$state."&amp;id=".$course_id."&amp;cid2=".$id;
	$ret_str = '<a class="jlms_img_link" '.$add_options.'href="'.$link.'" title="'.$alt.'">';
	$ret_str .= '<img class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/'.$image.'" width="16" height="16" border="0" alt="'.$alt.'" />';
	$ret_str .= '</a>';
	return $ret_str;
}


// 13.10.2006 (Get ID's of subscribed courses (for student), of own courses (for teacher); (Of all courses fo SuperUsers)
// 21.10.2006 (xm) "JLMS_GetUserType_simple" or "JLMS_GetUserType" ????????
// 14.12.2006 - function peredelana - dlya usera vozvrashayutsa id's vsex ego kursov (tam gde on u4itel', assistant, student)
function JLMS_GetUserCourses_IDs( $user_id, $usertype = 0, $only_teacher = false ) {
	global $JLMS_DB;
	
	$course_ids = array();
	$JLMS_ACL = JLMSFactory::getACL();
	$course_ids_t = array();
	$course_ids_t = $JLMS_ACL->_courses_teach;
	$course_ids_s = array();
	if (!$only_teacher) {
		$course_ids_s = $JLMS_ACL->_courses_learn;
	}
	$course_ids = array_merge($course_ids_t, $course_ids_s);
	$course_ids = array_unique($course_ids);
	return $course_ids;
	
	/*
	Old version
	*/
	/*
	$course_ids = array();
	if (!$usertype) {
		$usertype = JLMS_GetUserType_simple($user_id, true, true);
	}
	if ($usertype == 5) {
		$query = "SELECT distinct b.id FROM #__lms_courses as b";
		$JLMS_DB->SetQuery( $query );
		$course_ids = JLMSDatabaseHelper::LoadResultArray();
	} else {
		$query = "SELECT distinct b.id FROM #__lms_courses as b, #__lms_user_courses as a"
		. "\n WHERE a.course_id = b.id AND a.user_id = '".$user_id."'"
		. ( ($usertype == 1) ? "\n AND (a.role_id = 1 OR a.role_id = 4)": "\n AND a.role_id = 4" )
		;
		$JLMS_DB->SetQuery( $query );
		$course_ids_t = JLMSDatabaseHelper::LoadResultArray();
		$course_ids_s = array();
		if (!$only_teacher) {
			$query = "SELECT distinct a.id FROM #__lms_courses as a, #__lms_users_in_groups as c"
			. "\n WHERE a.id = c.course_id AND c.user_id = '".$user_id."'";
			$JLMS_DB->SetQuery( $query );
			$course_ids_s = JLMSDatabaseHelper::LoadResultArray();
		}
		$course_ids = array_merge($course_ids_t, $course_ids_s);
		$course_ids = array_unique($course_ids);
	}
	return $course_ids;
	*/
}
// 16.10.2006 (Get DATA of subscribed courses (for student), of own courses (for teacher);
// 14.12.2006 - function peredelana - dlya usera vozvrashayutsa id's vsex ego kursov (tam gde on u4itel', assistant, student)
function JLMS_CoursesNames( $cid ) {
	global $JLMS_DB, $JLMS_CONFIG;
	$cids = implode(',',$cid );
	$query = "SELECT distinct id, course_name FROM #__lms_courses WHERE id IN ($cids)"
	. "\n ORDER BY ".($JLMS_CONFIG->get('lms_courses_sortby',0)?"ordering, course_name, id":"course_name, ordering, id")
	;
	$JLMS_DB->SetQuery( $query );
	$course_data = $JLMS_DB->LoadObjectList();

	//TODO: move query to the int.functionality of LMSTitles object and leave only operations with LMSTitles object here

	$lms_titles_cache = & JLMSFactory::getTitles();
	$lms_titles_cache->setArray('courses', $course_data, 'id', 'course_name');

	return $course_data;
}

// function is deprecated
//do not use it !!!!!!!!!
function JLMS_GetUserCourses_Data( $user_id, $limit = 0 ) {
	global $JLMS_DB;
	$course_data = array();
	$JLMS_ACL = JLMSFactory::getACL();
	$usertype = $JLMS_ACL->_role_type;
//	$usertype = JLMS_GetUserType_simple($user_id, true, true);
//	if ($usertype == 5) { //(Max): role_id
	if ($usertype == 4) { //(Max): roletype_id
		$query = "SELECT b.*, '1' as user_course_role FROM #__lms_courses as b"
		. "\n ORDER BY b.course_name"
		. ($limit ? "\n LIMIT 0, $limit" : '')
		;
		$JLMS_DB->SetQuery( $query );
		$course_data = $JLMS_DB->LoadObjectList();
	} else {
		$query = "SELECT b.*, '1' as user_course_role FROM #__lms_courses as b, #__lms_user_courses as a"
		. "\n WHERE a.course_id = b.id AND a.user_id = '".$user_id."'"
//		. ( ($usertype == 1) ? "\n AND (a.role_id = 1 OR a.role_id = 4)": "\n AND a.role_id = 4" )
		. ( ($usertype == 2) ? "\n AND (a.role_id = 1 OR a.role_id = 4)": "\n AND a.role_id = 4" )
		. "\n ORDER BY b.course_name"
		. ($limit ? "\n LIMIT 0, $limit" : '')
		;
		$JLMS_DB->SetQuery( $query );
		$course_data_t = $JLMS_DB->LoadObjectList();
		$course_ids = array();

		foreach ($course_data_t as $ct) {
			$course_ids[] = $ct->id;
		}
		if (count($course_ids)) {
			$course_ids_str = implode(',', $course_ids);
		}
		$query = "SELECT a.*, '0' as user_course_role FROM #__lms_courses as a, #__lms_users_in_groups as c"
		. "\n WHERE a.id = c.course_id AND c.user_id = '".$user_id."'"
		. (count($course_ids) ? "\n AND a.id NOT IN ($course_ids_str)" : '')
		. "\n ORDER BY a.course_name"
		. ($limit ? "\n LIMIT 0, $limit" : '');
		$JLMS_DB->SetQuery( $query );
		$course_data_s = $JLMS_DB->LoadObjectList();
		$course_data = array_merge($course_data_t, $course_data_s);
		if ($limit) {
			if (count($course_data) > $limit) {
				$course_data_new = array();
				$i = 0;
				while ($i <= $limit) {
					$course_data_new[] = $course_data[$i];
					$i ++;
				}
				$course_data = $course_data_new;
			}
		}
	}
	return $course_data;
}
function JLMS_editorArea( $name, $content, $hiddenField, $width, $height, $col, $row, $buttons=true ) {
	$editor =JLMS07062010_JFactory::getEditor(); //jlms editor mod
	echo $editor->display( $hiddenField, $content, $width, $height, $col, $row, $buttons ) ;
}

function JLMS_getAsset(){
	global $JLMS_DB;
	
	$asset_id = null;
	
	if(JLMS_J16version()){
		$query = "SELECT id"
		. "\n FROM #__assets"
		. "\n WHERE name = 'com_joomla_lms'"
		;
		$JLMS_DB->setQuery($query);
		$asset_id = $JLMS_DB->loadResult();
	}
	return $asset_id;
}


function JLMS_GetLPathTreeStructure( $rows ) {
	$all_rows = array();
	$new_rows = array();
	$new_rows_ids = array();
	$all_rows = jlms_tree_struct_main($rows, $all_rows, 0, 2);

	$rows = $all_rows;

	//poschitat' dlya kagdogo parenta kol-vo itemov
	$all_parents = array();
	$items_in_parent = array();
	$i = 0;
	while ($i < count($rows)) {
		if (in_array($rows[$i]->parent_id, $all_parents)) {
			$items_in_parent[array_search($rows[$i]->parent_id, $all_parents)] ++;
		} else {
			$all_parents[] = $rows[$i]->parent_id;
			$items_in_parent[] = 1;
		}
		$i ++;
	}
	$i = 0;
	//$str_add = '&nbsp;|&nbsp;';
	$prev_par_id = -1;
	$prev_id = -1;
	//$str_add1 = $str_add;
	$kol_p = 0;
	$prev_parents = array(); // array of previous parents
	$c_all_parents = array();
	$c_items_in_parent = array();
	$max_tree_width = 0;
	while ($i < count($rows)) {
		if ($kol_p > $max_tree_width) { $max_tree_width = $kol_p; }
		if (in_array($rows[$i]->parent_id, $c_all_parents)) {
			$c_items_in_parent[array_search($rows[$i]->parent_id, $c_all_parents)] ++;
		} else {
			$c_all_parents[] = $rows[$i]->parent_id;
			$c_items_in_parent[] = 1;
		}
		$rows[$i]->tree_mode_num = 0;
		$rows[$i]->tree_mode = 0;
		$str_add1 = '';
		if ($rows[$i]->parent_id) {
			if ($rows[$i]->parent_id == $prev_par_id) {

			} else {
				if ($rows[$i]->parent_id == $prev_id) {
					$kol_p ++;
					$prev_par_id = $rows[$i]->parent_id;
					$prev_parents[$kol_p] = $prev_par_id;
				} else {
					while ( ($kol_p > 0) && ($prev_parents[$kol_p] != $rows[$i]->parent_id)) {
						$kol_p --;
					}
					$prev_par_id = $rows[$i]->parent_id;
				}
			}
			if ($kol_p > 0) {
				//$str_add1 = '';'&nbsp;' . str_repeat('>&nbsp;', $kol_p );
				$str_add1 = '';//'&nbsp;' . str_repeat('>&nbsp;', $kol_p );
				$rows[$i]->tree_mode = 2;
				$rows[$i]->tree_mode_num = $kol_p;
				$r = array_search($rows[$i]->parent_id, $c_all_parents);
				if ($c_items_in_parent[$r] < $items_in_parent[$r]) { $rows[$i]->tree_mode = 1; }
			}
		} else { $str_add1 = ''; $kol_p = 0;}
		$prev_id = $rows[$i]->id;
		//$str_add1 = '';
		$rows[$i]->doc_name = $str_add1 . $rows[$i]->doc_name;
		$r = array_search($rows[$i]->parent_id, $c_all_parents);
		if ($c_items_in_parent[$r] > 1) { $rows[$i]->allow_up = 1; } else { $rows[$i]->allow_up = 0; }
		if ($c_items_in_parent[$r] < $items_in_parent[$r]) { $rows[$i]->allow_down = 1; } else { $rows[$i]->allow_down = 0; }
		$i ++;
	}
	if ($kol_p > $max_tree_width) { $max_tree_width = $kol_p; }
	$i = 0;
	while ($i < count($rows)) {
		$rows[$i]->tree_max_width = $max_tree_width;
		$i ++;
	}
	return $rows;
}

function JLMS_GetLPath_Data($lpath_id, $course_id, $get_dname = 0) {
	global $JLMS_DB, $JLMS_CONFIG;
	
	$query = "SELECT a.*, a.step_name as doc_name, c.file_name, b.file_id, b.folder_flag FROM #__lms_learn_path_steps as a"
	. "\n LEFT JOIN #__lms_documents as b ON a.item_id = b.id AND a.step_type = 2"
	. "\n LEFT JOIN #__lms_files as c ON b.file_id = c.id WHERE a.lpath_id = '".$lpath_id."' AND a.course_id = '".$course_id."'"
	. "\n ORDER BY a.parent_id, a.ordering";
	$JLMS_DB->SetQuery( $query );
	$lpath_contents = $JLMS_DB->LoadObjectList();
	
	$bad_in = array();
	$rows_n = array();
	for($j=0;$j<count($lpath_contents);$j++){
		if($lpath_contents[$j]->folder_flag == 3){
			$query = "SELECT a.*, b.file_name FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0 WHERE a.id=".$lpath_contents[$j]->file_id." AND a.folder_flag = 0";
			$JLMS_DB->SetQuery( $query );
			$out_row = $JLMS_DB->LoadObjectList();
			if(count($out_row) && isset($out_row[0]->allow_link) && ($out_row[0]->allow_link == 1)){
				if ($get_dname)
				$lpath_contents[$j]->doc_name = $out_row[0]->doc_name;
				$lpath_contents[$j]->file_name = $out_row[0]->file_name;
				$lpath_contents[$j]->file_id = $out_row[0]->file_id;
			} else {
				if ($get_dname) {
					$lpath_contents[$j]->doc_name = _JLMS_LP_RESOURSE_ISUNAV;
				}
			}
		}
		
		//if($JLMS_CONFIG->get('enable_timetracking', false)){
		//	global $my;
		//	$query = "SELECT *"
		//	. "\n FROM #__lms_time_tracking_resources"
		//	. "\n WHERE 1"
		//	. "\n AND course_id = ".$lpath_contents[$j]->course_id
		//	. "\n AND user_id = ".$my->id
		//	. "\n AND resource_type = 9"
		//	. "\n AND resource_id = ".$lpath_contents[$j]->lpath_id
		//	. "\n AND item_id = ".$lpath_contents[$j]->id
		//	;
		//	$JLMS_DB->setQuery($query);
		//	$timetrack = $JLMS_DB->loadObject();
		//	if(isset($timetrack) && $timetrack->id){
		//		$lpath_contents[$j]->time_spent = $timetrack->time_spent;
		//	} else {
		//		$lpath_contents[$j]->time_spent = 0;
		//	}
		//}
	}
	
	$lpath_contents = JLMS_GetLPathTreeStructure($lpath_contents);
	$lpath_contents = AppendFileIcons_toList($lpath_contents);

	return $lpath_contents;
}
function JLMS_outputUserInfo($username, $name, $email, $group, $width = '15%', $show_tr_class = false) {
	$ret_str = '<table width="100%" style="margin:0;padding:0;" cellpadding="0" class="'.JLMSCSS::_('jlmslist').'" cellspacing = \'0\'>';
	$ret_str .= '<tr'.($show_tr_class ? (' class="'.JLMSCSS::_('sectiontableentry1').'"') : '').'><td width="'.$width.'" align="left">'._JLMS_UI_USERNAME.'</td><td align="left">'.$username.'</td></tr>';
	$ret_str .= '<tr'.($show_tr_class ? (' class="'.JLMSCSS::_('sectiontableentry2').'"') : '').'><td align="left">'._JLMS_UI_NAME.'</td><td align="left">'.$name.'</td></tr>';
	$ret_str .= '<tr'.($show_tr_class ? (' class="'.JLMSCSS::_('sectiontableentry1').'"') : '').'><td align="left">'._JLMS_UI_EMAIL.'</td><td align="left">'.$email.'</td></tr>';
	$ret_str .= '<tr'.($show_tr_class ? (' class="'.JLMSCSS::_('sectiontableentry2').'"') : '').'><td align="left">'._JLMS_UI_GROUP.'</td><td align="left">'.($group ? $group : '&nbsp;').'</td></tr>';
	$ret_str .= '</table>';
	return $ret_str;
}

function JLMS_toolTip($title='', $content='', $inside_tag='', $href='#', $link=1, $maxChars=36, $fixed='true', $classname='jlms_ttip jlms_img_link', $delay='500')
{
	static $mootoolsLoaded;
	
	$JLMS_CONFIG = JLMSFactory::getConfig();
	
	if( JLMS_J16version() && !isset($mootoolsLoaded) ) 
	{		
		JHtml::_('behavior.framework', true);
		$mootoolsLoaded = true;
	}
	
	$iso_enc = 'utf-8';
	if (defined('_ISO')) {
		$iso_enc = '';
		$iso = explode( '=', _ISO );
		if (isset($iso[1]) && $iso[1]) {
			$iso_enc = $iso[1];
		}
	}
	$classname_ar = explode(' ',$classname);
	$classname_js = (isset($classname_ar[0]) && $classname_ar[0]) ? $classname_ar[0] : $classname;
	static $exist_code;
	static $exist_classname;
	
	if($exist_classname != NULL && $exist_classname != $classname_js){
		$exist_code = NULL;
	}
	if(!isset($exist_code)){		
		if(strlen($title) || strlen($content)){					
			if( JLMS_mootools12() ) {
								
				if( JLMS_J16version() ) 
				{					
					$tip_wrap_js = '$$(\'.jlms_tool .tip\').setProperty( \'class\', \'tip_wrap\');';
				} else {
					$tip_wrap_js = '';
				}	
							
				$domready = '
				$$(\'.'.(string)$classname_js.'\').each(function(el) {
						var title = el.get(\'title\');
						if (title) {
							var parts = title.split(\'::\', 2);
							el.store(\'tip:title\', parts[0]);
							el.store(\'tip:text\', parts[1]);
						}
					});	 
			
			
					var JLMSTooltips = new Tips($$(\'.'.(string)$classname_js.'\'), {
							className: \'jlms_tool\',
							maxTitleChars: '.(int)$maxChars.',
							fixed: '.(string)$fixed.',						
							onShow: function() {																								
								JLMSTooltipsFx = new Fx.Tween(this.tip, \'opacity\', {duration: '.(int)$delay.', wait: false});
								JLMSTooltipsFx.start( \'opacity\', 0, 1 );
								'.$tip_wrap_js.'																
							},
							onHide: function() {								
								JLMSTooltipsFx = new Fx.Tween(this.tip, \'opacity\', {duration: '.(int)$delay.', wait: false});
								JLMSTooltipsFx.start( \'opacity\', 1, 0 );																
							}				
						});					
				';				
			} else {
				$domready = '
					new Tips($$(\'.'.(string)$classname_js.'\'), {
						className: \'jlms_tool\',
						maxTitleChars: '.(int)$maxChars.',
						fixed: '.(string)$fixed.',
						initialize:function(){					
							this.fx = new Fx.Style(this.toolTip, \'opacity\', {duration: '.(int)$delay.', wait: false}).set(0);					
						},
						onShow: function(toolTip) {					
							this.fx.start(1);
						},
						onHide: function(toolTip) {
							this.fx.start(0);
						}
					});
					';				
			}	 
	
			
			$JLMS_CONFIG->set('web20_domready_code', $JLMS_CONFIG->get('web20_domready_code','').$domready);
			$exist_code = true;
			$exist_classname = $classname_js;
		}
	}
	$tooltip = '';
	$properties = '';
	if(strlen($title) || strlen($content)){
		$title1 = $title;
		if( JLMS_mootools12() ) {
			if (strlen($title1) > $maxChars) {
				$title1 = jlms_UTF8string_substr($title1,0,$maxChars).' ...';
			}
		}
		$title = strlen($title) ? $title1 : '<div style="height: 1px;"></div>';
		$content = strlen($content) ? $content : '<div style="height: 1px;"></div>';
		/*if ( $link ) {
			if (strpos($classname, 'jlms_img_link') === false) {
				if ($classname) {
					$classname .= ' jlms_img_link';
				} else {
					$classname = 'jlms_img_link';
				}
			}
		}*/
		$properties .= ' class="'.$classname.'" title="'.htmlentities($title, ENT_QUOTES, $iso_enc).'::'.htmlentities($content, ENT_QUOTES, $iso_enc).'" ';
	} elseif ($classname == 'jlms_ttip jlms_img_link' && !strlen($title) && !strlen($content)) {
		//$properties .= ' class="jlms_ttip jlms_img_link" ';
		$properties .= ' class="jlms_img_link" ';
	}
	if($inside_tag == ''){
		$inside_tag = $title;	
	}
	
	if ( $link ) {
		$tooltip .= '<a '.$properties.' href="'. $href .'">'. $inside_tag .'</a>';
	} else {
		$tooltip .= '<span '.$properties.'>'. $inside_tag .'</span>';
	} 
	return $tooltip;
}

function month_lang($str, $start, $finish) {

	$month = substr($str,$start,$finish);

		switch ($month) {
			case '01': 	$str = substr_replace($str, _JLMS_JANUARY, $start,$finish); break;
			case '02': 	$str = substr_replace($str, _JLMS_FEBRUARY, $start,$finish); break;
			case '03': 	$str = substr_replace($str, _JLMS_MARCH, $start,$finish); break;
			case '04': 	$str = substr_replace($str, _JLMS_APRIL, $start,$finish); break;
			case '05': 	$str = substr_replace($str, _JLMS_MAY, $start,$finish); break;
			case '06': 	$str = substr_replace($str, _JLMS_JUNE, $start,$finish); break;
			case '07': 	$str = substr_replace($str, _JLMS_JULY, $start,$finish); break;
			case '08': 	$str = substr_replace($str, _JLMS_AUGUST, $start,$finish); break;
			case '09': 	$str = substr_replace($str, _JLMS_SEPTEMBER, $start,$finish); break;
			case '10': 	$str = substr_replace($str, _JLMS_OCTOBER, $start,$finish); break;
			case '11': 	$str = substr_replace($str, _JLMS_NOVEMBER, $start,$finish); break;
			case '12': 	$str = substr_replace($str, _JLMS_DECEMBER, $start,$finish); break;
		}
	return $str;
}

function day_month_lang($str,$start1,$finish1,$start2,$finish2, $short='') {

	$week_day = substr($str,$start1,$finish1);
	$month = substr($str,$start2,$finish2);

		switch ($month) {
			case '01': 	$str = substr_replace($str, _JLMS_JANUARY, $start2,$finish2); break;
			case '02': 	$str = substr_replace($str, _JLMS_FEBRUARY, $start2,$finish2); break;
			case '03': 	$str = substr_replace($str, _JLMS_MARCH, $start2,$finish2); break;
			case '04': 	$str = substr_replace($str, _JLMS_APRIL, $start2,$finish2); break;
			case '05': 	$str = substr_replace($str, _JLMS_MAY, $start2,$finish2); break;
			case '06': 	$str = substr_replace($str, _JLMS_JUNE, $start2,$finish2); break;
			case '07': 	$str = substr_replace($str, _JLMS_JULY, $start2,$finish2); break;
			case '08': 	$str = substr_replace($str, _JLMS_AUGUST, $start2,$finish2); break;
			case '09': 	$str = substr_replace($str, _JLMS_SEPTEMBER, $start2,$finish2); break;
			case '10': 	$str = substr_replace($str, _JLMS_OCTOBER, $start2,$finish2); break;
			case '11': 	$str = substr_replace($str, _JLMS_NOVEMBER, $start2,$finish2); break;
			case '12': 	$str = substr_replace($str, _JLMS_DECEMBER, $start2,$finish2); break;
		}

		if($short) {
			switch ($week_day) {
				case '1': 	$str = substr_replace($str, _JLMS_MON, $start1,$finish1); break;
				case '2': 	$str = substr_replace($str, _JLMS_TUE, $start1,$finish1); break;
				case '3': 	$str = substr_replace($str, _JLMS_WED, $start1,$finish1); break;
				case '4': 	$str = substr_replace($str, _JLMS_THU, $start1,$finish1); break;
				case '5': 	$str = substr_replace($str, _JLMS_FRI, $start1,$finish1); break;
				case '6': 	$str = substr_replace($str, _JLMS_SAT, $start1,$finish1); break;
				case '0': 	$str = substr_replace($str, _JLMS_SAN, $start1,$finish1); break;
			}
		}
		else {
			switch ($week_day) {
				case '1': 	$str = substr_replace($str, _JLMS_MONDAY, $start1,$finish1); break;
				case '2': 	$str = substr_replace($str, _JLMS_TUESDAY, $start1,$finish1); break;
				case '3': 	$str = substr_replace($str, _JLMS_WEDNESDAY, $start1,$finish1); break;
				case '4': 	$str = substr_replace($str, _JLMS_THURSDAY, $start1,$finish1); break;
				case '5': 	$str = substr_replace($str, _JLMS_FRIDAY, $start1,$finish1); break;
				case '6': 	$str = substr_replace($str, _JLMS_SATURDAY, $start1,$finish1); break;
				case '0': 	$str = substr_replace($str, _JLMS_SANDAY, $start1,$finish1); break;
			}
		}

	return $str;
}

function course_teachers($course_id){
	global $JLMS_DB;
	$query = "SELECT user_id FROM #__lms_user_courses WHERE course_id='".$course_id."'";
	$JLMS_DB->setQuery($query);
	$usrs = JLMSDatabaseHelper::LoadResultArray();
	$query = "SELECT user_id FROM #__lms_users WHERE lms_usertype_id = 5";
	$JLMS_DB->setQuery($query);
	$usrs = array_merge($usrs, JLMSDatabaseHelper::LoadResultArray());
	return $usrs;
}

function JLMS_gmdate( $format = 'Y-m-d H:i:s' ) 
{					
	return gmdate( $format );
}

function JLMS_addJoomlaLocale( $format = 'Y-m-d H:i:s', $date = false, $type = '' ) 
{		
	$offset = JLMS_getOffset();	
	
	$i = 1;
	if( $type == 'todb' ) $i = -1;
								
	$offset *= $i;	
				
	if( !intval($date) )
		$timestamp = time() + $offset;
	else 
		$timestamp = strtotime( $date ) + $offset;	
					
	return date( $format, $timestamp );
}

function JLMS_isDateValid( $date, $format = 'Y-m-d H:i:s' ) 
{							
	return ($date == date( $format, strtotime( $date ) ));
}

function JLMS_getEnrolPeriod( $user_id, $course_id ) 
{
	$JLMS_ACL = JLMSFactory::getACL();

	$enrol_time = $JLMS_ACL->getCoursesEnrolTime( $course_id );
			
	if ( !$enrol_time ) 
	{
		return false; 
	}
		 	
	$enrol_period_sec = (time() - date('Z') - strtotime( $enrol_time ));
	$enrol_period_min = $enrol_period_sec/60;
						
	return $enrol_period_min;	   	
}

function filterByShowPeriod( $rows ) 
{
	global $my;
	
	$enroll_periods = array();
	
	$res = array();
								 
	for( $i = 0; $i < count($rows); $i++ ) 
	{				
		$row = $rows[$i];
				
		if( !$row->is_time_related ) {	
			$res[] = $row; //fix or not =)		
			continue;
		}
			
		if( intval( $row->course_id ) ) 
		{								
			if( !isset($enroll_periods[$row->course_id]) ) 
			{
				if( false !== ( $enroll_period = JLMS_getEnrolPeriod( $my->id, $row->course_id )) ) 
				{
					$enroll_periods[$row->course_id] = $enroll_period;	
				} 
			} else {
				$enroll_period = $enroll_periods[$row->course_id];
			}
										
			if( $row->show_period <= $enroll_period ) 
			{						
				$res[] = $row;
			}
		}				
	}
	
	return $res;
}

//TODO: implement lmstitles usage here
function JLMS_getCourseName($id){
	$JLMS_DB = & JLMSFactory::getDB();
	$query = "SELECT course_name"
	. "\n FROM #__lms_courses"
	. "\n WHERE 1"
	. "\n AND id = '".intval($id)."'"
	;
	$JLMS_DB->setQuery($query);
	$name = $JLMS_DB->loadResult();
	if($name){
		return $name;
	} else {
		return false;
	}
}
//TODO: implement lmstitles usage here
function JLMS_getQuizName($id){
	$JLMS_DB = & JLMSFactory::getDB();
	$query = "SELECT c_title"
	. "\n FROM #__lms_quiz_t_quiz"
	. "\n WHERE 1"
	. "\n AND c_id = '".intval($id)."'"
	;
	$JLMS_DB->setQuery($query);
	$name = $JLMS_DB->loadResult();
	if($name){
		return $name;
	} else {
		return false;
	}
}

function JLMS_getTimeNormal($sec=0){
	$d = $sec >= (24 * 60 * 60) ? floor($sec / (24 * 60 * 60)) : 0;
	$sec = $sec - $d * 24 * 60 * 60;
	$h = $sec >= (60 * 60) ? floor($sec / (60 * 60)) : 0;
	$sec = $sec - $h * 60 * 60;
	$m = $sec >= 60 ? floor($sec/60) : 0;
	$sec = $sec - $m * 60;
	$s = $sec;
	
	$time = '';
	$time .= $d ? str_pad($d, strlen($d), '0', STR_PAD_LEFT).' day ' : '';
	$time .= (($d && $h) || ($d && !$h) || (!$d && $h)) ? str_pad($h, 2, '0', STR_PAD_LEFT).':' : '';
	$time .= str_pad($m, 2, '0', STR_PAD_LEFT).':';
	$time .= str_pad($s, 2, '0', STR_PAD_LEFT);
	
	return $time;
}

function JLMS_restricted_data(){
	$JLMS_DB = & JLMSFactory::getDB();
	
	$query = "SELECT id, parent, restricted, groups"
	. "\n FROM #__lms_course_cats"
	. "\n WHERE 1"
	;
	$JLMS_DB->setQuery($query);
	$cats = $JLMS_DB->loadObjectList();
	
	$query = "SELECT id, cat_id"
	. "\n FROM #__lms_courses"
	. "\n WHERE 1"
	;
	$JLMS_DB->setQuery($query);
	$courses = $JLMS_DB->loadObjectList();
	
	$tmp_cats = array();
	$n=0;
	for($i=0;$i<count($cats);$i++){
		foreach($courses as $course){
			if(isset($course->cat_id) && $course->cat_id && $course->cat_id == $cats[$i]->id){
				$cats[$i]->course_id = $course->id;
				$cats[$i]->category_id = $cats[$i]->id;
				if (!isset($tmp_cats[$n])) {
					$tmp_cats[$n] = new stdClass();
				}
				$tmp_cats[$n]->restricted = $cats[$i]->restricted;
				$tmp_cats[$n]->course_id = $course->id;
				$tmp_cats[$n]->category_id = $cats[$i]->id;
				$tmp_cats[$n]->groups = $cats[$i]->groups;
				
				$n++;
			}
		}
		$cat = $cats[$i];
		if(isset($cat->restricted) && $cat->restricted){
			JLMS_helper_child($cat, $cats);
		}
	}
	
	$tmp_info = array();
	$i=0;
//	foreach($cats as $cat){ //old bug (Max - 23.03.2011)
	foreach($tmp_cats as $cat){
		if($cat->restricted && isset($cat->category_id) && isset($cat->course_id)){
			$tmp_info[$i] = new stdClass();
			$tmp_info[$i]->course_id = $cat->course_id;
			$tmp_info[$i]->category_id = $cat->category_id;
			$tmp_info[$i]->restricted = $cat->restricted;
			$tmp_info[$i]->groups = $cat->groups;
			$i++;
		}
	}
	$info = array();
	if(count($tmp_info)){
		$info = $tmp_info;
	}
	
	return $info;
}
function JLMS_helper_child($cat, $cats, $i=0){
	foreach($cats as $c){
		if($cat->id == $c->parent){
			$c->restricted = $cat->restricted;
			$c->groups = $cat->groups;
			JLMS_helper_child($c, $cats);
		}
	}
}

function JLMS_illegal_courses($id_group=array())
{    
	$info = JLMS_restricted_data();

	$illegal_array = array();
	for($i=0;$i<count($info);$i++) {
		if($info[$i]->restricted) {
			$is_resticted = true;
			if (count($id_group)) {
				for($j=0;$j<count($id_group);$j++) {
					$flag = strpos($info[$i]->groups, '|'.$id_group[$j].'|');
					if ($flag === false) {
						//it is restricted already....
					} else {
						$is_resticted = false;
					}
				}
			}
			if ($is_resticted) {
				$illegal_array[] = $info[$i]->course_id;
			} 
		}
	}  
    
    $reqCoursesIds = JLMS_prereq_restricted();
       
    $illegal_array = array_merge( $illegal_array, $reqCoursesIds );       
    
	return $illegal_array;
}

function JLMS_prereq_restricted() 
{
    global $my;    

    $JLMS_DB = & JLMSFactory::getDB();             
        
    $query = "SELECT DISTINCT cp.course_id 
                FROM #__lms_courses_prerequisites AS cp, #__lms_courses AS c       
                WHERE cp.req_id NOT IN (SELECT cu.course_id FROM #__lms_certificate_users AS cu WHERE cu.user_id = ".$my->id.")
                        AND cp.course_id = c.id AND c.prereq_mode = 2
                ";
                ;
	$JLMS_DB->setQuery( $query );
	$reqCoursesIds = JLMSDatabaseHelper::LoadResultArray();
		      
    if( !empty($reqCoursesIds) )
        return $reqCoursesIds;
    else
        return array();         
}

function JLMS_prereq_courses( $course_id = 0 ) 
{
    global $my;
    
    $JLMS_CONFIG = JLMSFactory::getConfig();
    $JLMS_DB = & JLMSFactory::getDB();
    $JLMS_ACL = JLMSFactory::getACL();
    
    $c_id = $course_id;
    if( !$course_id )
        $c_id = $JLMS_CONFIG->get('course_id',0);       
    
    $query = "SELECT prereq_mode FROM #__lms_courses WHERE id = ".$c_id;
    $JLMS_DB->setQuery( $query );
    $prereqMode = $JLMS_DB->loadResult();
    
    $myCourses = $JLMS_ACL->getMyCourses();
    
    if( $my->id && !empty($myCourses) && in_array( $c_id, $myCourses ) ) 
    {
        return array();
    }
       
    if( $prereqMode < 1 )//0 - do not use prerequisites, 1 - disable enrollment, 2 - hide course 
    {
        return array();
    }
    
    $query = "SELECT DISTINCT cp.req_id 
                FROM #__lms_courses_prerequisites AS cp       
                WHERE cp.req_id NOT IN (SELECT cu.course_id FROM #__lms_certificate_users AS cu WHERE cu.user_id = ".$my->id.") 
                    AND cp.course_id = ".$c_id;
                ;
	$JLMS_DB->setQuery( $query );    
	$reqCoursesIds = JLMSDatabaseHelper::LoadResultArray();   
    
    if( !empty($reqCoursesIds) )
        return $reqCoursesIds;
    else
        return false;
}

function JLMS_all_children_categories($parent, $tmp, $i=0){
	global $JLMS_DB;
	
	$query = "SELECT id FROM #__lms_course_cats WHERE parent = '".$parent."'";
	$JLMS_DB->setQuery($query);
	$catid = $JLMS_DB->loadResult();
	
	if($catid){
		$tmp[$i] = $catid;
		
		$parent = $catid;
		$i++;
		$tmp = JLMS_all_children_categories($parent, $tmp, $i);
	}
	return $tmp;
}

function JLMS_getFilterMulticategories(&$last_catid, $query_where='', $preg_val = 'filter_id_'){
	$tmp_level = array();
	$last_catid = 0;
	if(isset($_REQUEST['category_filter']) && $_REQUEST['category_filter']){
		$last_catid = $_REQUEST['category_filter'];
	} else {
		$i=0;
		foreach($_REQUEST as $key=>$item){
			if(preg_match('#'.$preg_val.'(\d+)#', $key, $result)){
				if($item){
					$tmp_level[$i] = $result;
					$last_catid = $item;
					$i++;
				}	
			}	
		}
	}
	$JLMS_ACL = JLMSFactory::getACL();
	$all_cats = $JLMS_ACL->getMyCategories(-1, -1);

/*
	$query = "SELECT * FROM #__lms_course_cats"
	. "\n WHERE 1"
	.($query_where ? "\n AND ".$query_where : '')
	. "\n ORDER BY id"
	;
	//$JLMS_DB->setQuery($query);
	//$all_cats = $JLMS_DB->loadObjectList();
*/
	
	$tmp_cats_filter = array();
	$children = array();
	foreach($all_cats as $cat){
		$pt = $cat->parent;
		$list = @$children[$pt] ? $children[$pt] : array();
		array_push($list, $cat->id);
		$children[$pt] = $list;
	}
	$tmp_cats_filter[0] = $last_catid;
	$i=1;
	foreach($children as $key=>$childs){
		if($last_catid == $key){
			foreach($children[$key] as $v){
				if(!in_array($v, $tmp_cats_filter)){
					$tmp_cats_filter[$i] = $v;
					$i++;
				}
			}
		}
	}
	foreach($children as $key=>$childs){
		if(in_array($key, $tmp_cats_filter)){
			foreach($children[$key] as $v){
				if(!in_array($v, $tmp_cats_filter)){
					$tmp_cats_filter[$i] = $v;
					$i++;
				}
			}
		}
	}
	$tmp_cats_filter = array_unique($tmp_cats_filter);
	return $tmp_cats_filter;
}

function addDomReadyScript() 
{
	$JLMS_CONFIG = & JLMSFactory::getConfig();
	 
	if ($JLMS_CONFIG->get('web20_domready_code') || $JLMS_CONFIG->get('jlms_aditional_js_code')) 
	{
		$custom_head_tag = '
	<!--//--><![CDATA[//><!--
	
	';
		$custom_head_tag .= $JLMS_CONFIG->get('jlms_aditional_js_code','');
		if ($JLMS_CONFIG->get('web20_domready_code')) {
			$custom_head_tag .= '
	
				window.addEvent(\'domready\', function(){
	
				';
			$custom_head_tag .= $JLMS_CONFIG->get('web20_domready_code');
			$custom_head_tag .= '
	
			});
	
			';
		}
		$custom_head_tag .= '
	
	//--><!]]>
	';
		$doc = JFactory::getDocument();
		$doc->addScriptDeclaration($custom_head_tag);
		
		$JLMS_CONFIG->set('web20_domready_code', '');
	}
}

function JLMS_getAdminGroups() 
{
	if( JLMS_J16version() ) 
	{
		return array(7,8);
	} else {
		return array(24,25);
	}
}

/*
function JLMS_getAdminGroups( $string = false ) 
{
	if( JLMS_J16version() ) 
	{
		if( $string )
			return array('7','8');
		else
			return array(7,8);
	} else {
		if( $string )
			return array('24','25');
		else
			return array(24,25);
	}
}
*/

function JLMS_perctangle($numerator, $denominator, $round=0){
	$result = false;
	if($denominator && $numerator <= $denominator){
		$result = ($numerator / $denominator) * 100;
		$result = round($result, $round);
	} else {
		$result = 0;
	}
	
	if($result === false){
		$result = '';
	} else {
		$result = $result . '%';
	}
	return $result;
}

function JLMS_showQuizStatus($row, $width=150, $export_file=0){
	$JLMS_CONFIG = & JLMSFactory::getConfig();
	
	$quiz_status_as = $JLMS_CONFIG->get('quiz_status_as', 0);

	ob_start();
	
	if($export_file){
		switch ($quiz_status_as){
			case '1':
				if(isset($row->user_passed) && ($row->user_passed == 1)){
					echo _JLMS_QUIZ_STATUS_COMPLETED;
					echo ' - ';
				} else {
					echo _JLMS_QUIZ_STATUS_INCOMPLETE;
					echo ' - ';
				}
				
				if(isset($row->user_grade) && trim($row->user_grade) != '-'){
					echo $row->user_grade;
					echo ' - ';
				}
			break;
			case '2':
				if(isset($row->user_passed) && ($row->user_passed == 1)){
					echo _JLMS_QUIZ_STATUS_PASSED;
					echo ' - ';
				} else if(isset($row->user_passed) && ($row->user_passed == 0)){
					echo _JLMS_QUIZ_STATUS_FAILED;
					echo ' - ';
				}
				if(isset($row->user_grade) && trim($row->user_grade) != '-'){
					echo $row->user_grade;
					echo ' - ';
				}
			break;
			case '4':
			case '5':
				if(!isset($row->user_score)) { $row->user_score = 0; }
				if(!isset($row->quiz_max_score)) { $row->quiz_max_score = 0; }	
				
				if($quiz_status_as == 5){
					if(!isset($row->link_certificate)) {
					 	echo (isset($row->user_passed) && ($row->user_passed == 1)) ? _CMN_YES : _CMN_NO;
					 	echo ' - ';
					}
					if(isset($row->link_certificate) && $row->link_certificate){
						echo $row->link_certificate;
						echo ' - ';
					}
				}
				
				if(isset($row->user_grade) && trim($row->user_grade) != '-'){
					echo $row->user_grade;
					echo ' - ';
				}
				
				echo JLMS_perctangle($row->user_score, $row->quiz_max_score);
			break;
			case '3':
			case '0':
			default:	
				if(!isset($row->user_score)) { $row->user_score = 0; }
				if(!isset($row->quiz_max_score)) { $row->quiz_max_score = 0; }	
				
				if($quiz_status_as == 0){
					if(!isset($row->link_certificate)) {
					 	echo (isset($row->user_passed) && ($row->user_passed == 1)) ? _CMN_YES : _CMN_NO;
					 	echo ' - ';
					}
					if(isset($row->link_certificate) && $row->link_certificate){
						echo $row->link_certificate;
						echo ' - ';
					}
				}
				
				if(isset($row->user_grade) && trim($row->user_grade) != '-'){
					echo $row->user_grade;
					echo ' - ';
				}
				
				echo ($row->user_passed == -1) ? " - " : ($row->user_score." / ".$row->quiz_max_score);
			break;	
		}
	} else {
		$style = $width ? 'style="width: '.$width.'px;"' : '';
		$class_sufix = '';
		if($width <= 50){
			$class_sufix .= '_noflt';
		}
		
		?>
		<div class="jlms_quiz_status_as<?php echo $class_sufix;?>" <?php echo $style;?>>
		<?php
		switch ($quiz_status_as){
			case '1':
				if(isset($row->user_passed) && ($row->user_passed == 1)){
					echo "<b>"._JLMS_QUIZ_STATUS_COMPLETED."</b>";
				} else {
					echo "<b>"._JLMS_QUIZ_STATUS_INCOMPLETE."</b>";
				}
				
				if(isset($row->user_grade) && trim($row->user_grade) != '-'){
					echo '<div class="points'.$class_sufix.'">';
						echo '<b>'.$row->user_grade.'</b>';
						echo '<div class="clr"><!-- --></div>';
					echo '</div>';
				}
			break;
			case '2':
				if(isset($row->user_passed) && ($row->user_passed == 1)){
					echo "<b>"._JLMS_QUIZ_STATUS_PASSED."</b>";
				} else if(isset($row->user_passed) && ($row->user_passed == 0)){
					echo "<b>"._JLMS_QUIZ_STATUS_FAILED."</b>";
				}
				if(isset($row->user_grade) && trim($row->user_grade) != '-'){
					echo '<div class="points'.$class_sufix.'">';
						echo '<b>'.$row->user_grade.'</b>';
						echo '<div class="clr"><!-- --></div>';
					echo '</div>';
				}
			break;
			case '4':
			case '5':
				if(!isset($row->user_score)) { $row->user_score = 0; }
				if(!isset($row->quiz_max_score)) { $row->quiz_max_score = 0; }	
				
				if($quiz_status_as == 5){
					if(!isset($row->link_certificate)) {
						echo '<div class="icon'.$class_sufix.'">';
					 		echo (isset($row->user_passed) && ($row->user_passed == 1)) ? ('<img class="JLMS_png" width="16" height="16" border="0" alt="passed" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/btn_accept.png"/>') : ('<img class="JLMS_png" width="16" height="16" border="0" alt="failed" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/btn_cancel.png"/>');
					 		echo '<div class="clr"><!-- --></div>';
					 	echo '</div>';
					}
					if(isset($row->link_certificate)){
						echo '<div class="icon'.$class_sufix.'">';
							echo $row->link_certificate;
							echo '<div class="clr"><!-- --></div>';
						echo '</div>';
					}
				}
				
				if(isset($row->user_grade) && trim($row->user_grade) != '-'){
					echo '<div class="points'.$class_sufix.'">';
						echo '<b>'.$row->user_grade.'</b>';
						echo '<div class="clr"><!-- --></div>';
					echo '</div>';
				}
				
				echo '<div class="points'.$class_sufix.'">';
					echo JLMS_perctangle($row->user_score, $row->quiz_max_score);
					echo '<div class="clr"><!-- --></div>';
				echo '</div>';
			break;
			case '3':
			case '0':
			default:	
				if(!isset($row->user_score)) { $row->user_score = 0; }
				if(!isset($row->quiz_max_score)) { $row->quiz_max_score = 0; }	
				
				if($quiz_status_as == 0){
					if(!isset($row->link_certificate)) {
						echo '<div class="icon'.$class_sufix.'">';
					 		echo (isset($row->user_passed) && ($row->user_passed == 1)) ? ('<img class="JLMS_png" width="16" height="16" border="0" alt="passed" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/btn_accept.png"/>') : ('<img class="JLMS_png" width="16" height="16" border="0" alt="failed" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/btn_cancel.png"/>');
					 		echo '<div class="clr"><!-- --></div>';
					 	echo '</div>';
					}
					if(isset($row->link_certificate)){
						echo '<div class="icon'.$class_sufix.'">';
							echo $row->link_certificate;
							echo '<div class="clr"><!-- --></div>';
						echo '</div>';
					}
				}
				
				if(isset($row->user_grade) && trim($row->user_grade) != '-'){
					echo '<div class="points'.$class_sufix.'">';
						echo '<b>'.$row->user_grade.'</b>';
						echo '<div class="clr"><!-- --></div>';
					echo '</div>';
				}
				
				echo '<div class="points'.$class_sufix.'">';
					echo ($row->user_passed == -1) ? " - " : ($row->user_score." / ".$row->quiz_max_score);
					echo '<div class="clr"><!-- --></div>';
				echo '</div>';
			break;	
		}
		?>
		</div>
		<?php
	}	
	
	$html = ob_get_contents();
	ob_get_clean();
	
	return $html;
}

function getIconCourse($row){
	$JLMS_ACL = JLMSFactory::getACL();
	
	$icon_course = 'tlb_courses';
	if(in_array($row->c_id, array_merge($JLMS_ACL->_courses_teach, $JLMS_ACL->_courses_learn))){
		$icon_course = 'tlb_courses';
		if($row->certificate){
			$icon_course = 'btn_accept';	
		}
	} else
	if(in_array($row->c_id, $JLMS_ACL->_courses_staff)){
		$icon_course = 'tlb_ceo';
	}
	
	return $icon_course;
}

function utf_to_win($s){ 
    $in=array("%u0430","%u0431","%u0432","%u0433","%u0434","%u0435","%u0436","%u0437","%u0438","%u0439","%u043A","%u043B","%u043C","%u043D","%u043E","%u043F","%u0440","%u0441","%u0442","%u0443","%u0444","%u0445","%u0446","%u0447","%u0448","%u0449","%u044A","%u044B","%u044C","%u044D","%u044E","%u044F","%u0451","%u0410","%u0411","%u0412","%u0413","%u0414","%u0415","%u0416","%u0417","%u0418","%u0419","%u041A","%u041B","%u041C","%u041D","%u041E","%u041F","%u0420","%u0421","%u0422","%u0423","%u0424","%u0425","%u0426","%u0427","%u0428","%u0429","%u042A","%u042B","%u042C","%u042D","%u042E","%u042F","%u0401"); 
    $out=array("а","б","в","г","д","е","ж","з","и","й","к","л","м","н","о","п","р","с","т","у","ф","х","ц","ч","ш","щ","ъ","ы","ь","э","ю","я","ё","А","Б","В","Г","Д","Е","Ж","З","И","Й","К","Л","М","Н","О","П","Р","С","Т","У","Ф","Х","Ц","Ч","Ш","Щ","Ъ","Ы","Ь","Э","Ю","Я","Ё"); 
    $s=str_replace($in,$out,$s); 
    return $s; 
}

function getArhiveCertificate($course_id=0, $user_id=0){
	$JLMS_DB = & JLMSFactory::getDB();
	$JLMS_CONFIG = & JLMSFactory::getConfig();
	
	require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_certificates.php");
	
	if(!$course_id){
		$course_id = JRequest::getVar('course_id', 0);
	}
	if($user_id){
		$user_id = JRequest::getVar('user_id', 0);
	}
	$quiz_id = JRequest::getVar('quiz_id', 0);
	
	$juser = JFactory::getUser();
	
	if($JLMS_CONFIG->get('enabled_clear_course_user_data', false)){
		$query = "SELECT a.* FROM #__lms_certificate_prints_arhive as a, #__lms_certificates as b"
		. "\n WHERE 1"
		. "\n AND a.course_id = '".$course_id."'"
		. "\n AND a.user_id = '".$juser->id."'"
		.($quiz_id ? "\n AND a.quiz_id = '".$quiz_id."'" : '')
		. "\n AND a.crtf_id = b.id"
		.($quiz_id ? "\n AND b.crtf_type = 2" : "\n AND b.crtf_type = 1")
		. "\n AND b.published = 1"
		;
		$JLMS_DB->setQuery( $query );
		$certificate_arhive = $JLMS_DB->loadObject();
	}
	
	$tm_obj = new stdClass();
	$tm_obj->username = isset($juser->username)? $juser->username : '';
	$tm_obj->name = isset($juser->name) ? $juser->name : '';
	$tm_obj->crtf_date = isset($certificate_arhive->crtf_date) ? strtotime($certificate_arhive->crtf_date) : ''; //time();
	$tm_obj->crtf_spec_answer = '';
	$tm_obj->is_preview = false;
	
	JLMS_Certificates::JLMS_outputCertificate( $certificate_arhive->crtf_id, $course_id, $tm_obj, $juser );
}

function JLMS_pluralForm($n, $form1, $form2, $form5) {
    $n = abs($n) % 100;
    $n1 = $n % 10;
    if ($n > 10 && $n < 20) return $form5;
    if ($n1 > 1 && $n1 < 5) return $form2;
    if ($n1 == 1) return $form1;
    return $form5;
}

function JLMS_ShellSortAsc($elements, $length, $field) {
    $k=0;
    $gap[0] = (int) ($length / 2);
 
    while($gap[$k] > 1) {
        $k++;
        $gap[$k] = (int)($gap[$k-1] / 2);
    }//end while
 
    for($i = 0; $i <= $k; $i++){
        $step=$gap[$i];
 
        for($j = $step; $j < $length; $j++) {
            $temp = $elements[$j];
            $p = $j - $step;
            while($p >= 0 && $elements[$j]->{$field} < $elements[$p]->{$field}) {
                $elements[$p + $step] = $elements[$p];
                $p = $p - $step;
            }//end while
            $elements[$p + $step] = $temp;
        }//endfor j
    }//endfor i
 
    return $elements;
}

function JLMS_ShellSortDesc($elements, $length, $field) {
    $k=0;
    $gap[0] = (int) ($length / 2);
 
    while($gap[$k] > 1) {
        $k++;
        $gap[$k] = (int)($gap[$k-1] / 2);
    }//end while
 
    for($i = 0; $i <= $k; $i++){
        $step=$gap[$i];
 
        for($j = $step; $j < $length; $j++) {
            $temp = $elements[$j];
            $p = $j - $step;
            while($p >= 0 && $elements[$j]->{$field} > $elements[$p]->{$field}) {
                $elements[$p + $step] = $elements[$p];
                $p = $p - $step;
            }//end while
            $elements[$p + $step] = $temp;
        }//endfor j
    }//endfor i
 
    return $elements;
}