<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
$_JLMS_PLUGINS->registerFunction( 'onRequireLanguage', 'getLanguageJFish' );
$_JLMS_PLUGINS->registerFunction( 'onAfterLoadCourseSettings', 'switchJFishLanguage' );

function getLanguageJFish( & $language )
{
	$JLMS_CONFIG = JLMSFactory::getConfig();
	
	$lang = getCurrLang();	
		
	if( $lang ) 
	{		
		$JLMS_CONFIG->set('default_language', $lang);
		$language = $lang;
	}
}

function switchJFishLanguage($jlmslang)
{	
	return getCurrLang();
}

function getCurrLang() 
{
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$botParams = $_JLMS_PLUGINS->getPluginParams('languageswitcher');
	
	$langs_params = $botParams->get('langs', 'en=english, ru=russian, de=german1');				
	
	$langPlugin = JPluginHelper::getPlugin('system', 'languagefilter');	
	if( class_exists('JLanguageHelper') && !empty($langPlugin) ) 
	{
		$lang_codes = JLanguageHelper::getLanguages('lang_code');		
		$currLangCode = JRequest::getString(JApplication::getHash('language'), null , 'cookie');					
		if( isset($lang_codes[$currLangCode]) ) 
		{
			$currLang = $lang_codes[$currLangCode]->sef;
		}		
	} else {		
		if(isset($_COOKIE['jfcookie']))
		{
			if(isset($_COOKIE['jfcookie']['lang'])){
				$currLang = $_COOKIE['jfcookie']['lang'];
			}
		}		
	}
	
	if(JRequest::getVar('lang'))
	{
		$currLang = JRequest::getVar('lang');
	}
	
	if( empty($currLang) ) return '';
	
	$langs = explode(",", $langs_params);
		
	$correspondence_langs = array();
	
	foreach($langs as $k=>$lang)
	{		
		preg_match_all('#([-\w]+)\s*=\s*(\w+)#', trim($lang), $out, PREG_PATTERN_ORDER);
		if(isset($out[0][0]) && isset($out[1][0]) && isset($out[2][0])){
			$correspondence_langs[$k]['lang'] = $out[1][0];
			$correspondence_langs[$k]['jlmslang'] = $out[2][0];
		}
	}	
		
	foreach($correspondence_langs as $lang)
	{
		if( $lang['lang'] == $currLang ){
			return $lang['jlmslang'];
			break;
		}
	}
	
	return '';
}