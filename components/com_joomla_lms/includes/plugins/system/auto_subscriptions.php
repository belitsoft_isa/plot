<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$_JLMS_PLUGINS = & JLMSFactory::getPlugins();

$_JLMS_PLUGINS->registerFunction( 'onShowCourseProperties', 'ShowCourseAutoSubscriptions' );
$_JLMS_PLUGINS->registerFunction( 'onAfterSaveCourseProperties', 'SaveCourseAutoSubscriptions' );

class JLMS_Auto_Subscriptions{
	
	public static function getListFields(){
		$fields = array();
		$fields[0] = new stdClass();
		$fields[0]->name = 'course_price';
		$fields[0]->title = _JLMS_COURSES_PRICE;
		$fields[0]->value = number_format( 0, 2, '.', '' );
		$fields[0]->published = 1;
		
		$out_fields = array();
		foreach($fields as $field){
			if($field->published){
				$out_fields[] = $field;
			}
		}
		
		return $out_fields;
	}
	
	public static function getListFieldsData($fields, $row, $data_params=0){
		if($data_params){
			$params = explode("\n", $row->params);
			$prepare_params = array();
			if(isset($params) && count($params)){
				foreach($params as $param){
					preg_match('#(.*)=(.*)#', $param, $out);
					
					if(isset($out[0]) && isset($out[1]) && isset($out[2])){
						$obj = new stdClass();
						$obj->name = $out[1];
						$obj->value = $out[2];
					
						$prepare_params[] = $obj;
					}
				}
				if(count($prepare_params)){
					for($i=0;$i<count($fields);$i++){
						foreach($prepare_params as $param){
							if($fields[$i]->name == $param->name && $param->value){
								$fields[$i]->value = $param->value;
							}
						}
					}
				}
			}
		} else {
			$params = get_object_vars($row);
			$prepare_params = array();
			if(isset($params) && count($params)){
				foreach($params as $name=>$value){
					
					if(isset($name) && isset($value)){
						$obj = new stdClass();
						$obj->name = $name;
						$obj->value = $value;
					
						$prepare_params[] = $obj;
					}
				}
				if(count($prepare_params)){
					for($i=0;$i<count($fields);$i++){
						foreach($prepare_params as $param){
							if($fields[$i]->name == $param->name && $param->value){
								$fields[$i]->value = $param->value;
							}
						}
					}
				}
			}
		}
		return $fields;
	}
	
	public static function getPriceFormat($in_price){
		return number_format( $in_price, 2, '.', '' );
	}
	
}

function ShowCourseAutoSubscriptions($course){
	
	$obj_return = array();
	
	$list_fields = JLMS_Auto_Subscriptions::getListFields();
	if(isset($course->params)){
		$list_fields = JLMS_Auto_Subscriptions::getListFieldsData($list_fields, $course);
	}
	
	if(isset($list_fields) && count($list_fields)){
		foreach($list_fields as $field){
			$obj = new stdClass();
			$obj->name = $field->title;
			
			JLMS_Auto_Subscriptions::getPriceFormat($field->value);
			
			ob_start();
			?>
			<input type="text" name="<?php echo $field->name;?>" value="<?php echo JLMS_Auto_Subscriptions::getPriceFormat($field->value);?>" class="inputbox" />
			<?php
			$control = ob_get_contents();
			ob_end_clean();
			$obj->control = $control;
			
			$obj_return[] = $obj;
		}
	}

	return $obj_return;
}

function SaveCourseAutoSubscriptions($course){
	$obj_return = array();
	$list_fields = JLMS_Auto_Subscriptions::getListFields();
	
	$db = & JLMSFactory::getDB();
	
	if(isset($list_fields) && count($list_fields)){
		$query = "UPDATE #__lms_courses SET";
		$querys = array();
		foreach($list_fields as $field){
			$obj = new stdClass();
			$obj->name = $field->name;
			$obj->value = JRequest::getVar($field->name);
			
			$querys[] = $obj->name." = '".$obj->value."'";
			
			$course->{$obj->name} = $obj->value;
		}
		$query .= "\n ".implode(',', $querys);
		$query .= "\n WHERE id = '".$course->id."'";
		if(isset($querys) && count($querys)){
			$db->setQuery($query);
			$db->query();
		}
	}
	
	$query = "SELECT a.*"
	. "\n FROM #__lms_subscriptions_courses as a"
	. "\n WHERE 1"
	. "\n AND a.course_id = '".$course->id."'"
	;
	$db->setQuery($query);
	$subs = $db->loadObjectList();
	
	$query = "SELECT a.*"
	. "\n FROM #__lms_subscriptions_courses as a, #__lms_subscriptions as b"
	. "\n WHERE 1"
	. "\n AND a.sub_id = b.id"
	. "\n AND b.account_type = '1'"
	;
	$db->setQuery($query);
	$subs_courses = $db->loadObjectList();
	
	$result_subs = array();
	foreach($subs as $sub){
		$count_courses = 0;
		foreach($subs_courses as $subs_course){
			if($sub->sub_id == $subs_course->sub_id){
				$count_courses++;
			}
		}
		if($count_courses == 1){
			$result_subs[] = $sub;
		}
	}
	
	$temp_subs = array();
	foreach($result_subs as $result_sub){
		$temp_subs[] = $result_sub->sub_id;
	}
	
	if(isset($course->paid) && $course->paid){
		if(isset($result_subs) && !count($result_subs)){
			$query = "INSERT INTO #__lms_subscriptions"
			. "\n (id, sub_name, account_type, date, published, access_days, start_date, end_date, price, discount, sub_descr, restricted, restricted_groups, a1, a2, a3)"
			. "\n VALUES"
			. "\n ('', '".(get_magic_quotes_gpc() ? stripslashes( $course->course_name ) : $course->course_name)."', '1', '".date('Y-m-d H:i:s')."', 1, '10', '".date('Y-m-d')."', '".date('Y-m-d')."', '".$course->course_price."', '0', '', 0, '', 0, 0, 0)"
			;
			$db->setQuery($query);
			$db->query();
			
			if($db->insertid()){
				$query = "INSERT INTO #__lms_subscriptions_courses"
				. "\n (sub_id, course_id)"
				. "\n VALUES"
				. "\n ('".$db->insertid()."', '".$course->id."')"
				;
				$db->setQuery($query);
				$db->query();
			}
		} else
		if(isset($result_subs) && count($result_subs)){
			if(count($temp_subs)){
				$query = "UPDATE #__lms_subscriptions SET price = '".$course->course_price."' WHERE id IN (".implode(',', $temp_subs).")";
				$db->setQuery($query);
				$db->query();
			}
		}
	} else
	if(isset($course->paid) && !$course->paid){
		if(isset($result_subs) && count($result_subs)){
			if(count($temp_subs)){
				$query = "DELETE FROM #__lms_subscriptions_courses WHERE sub_id IN (".implode(',', $temp_subs).")";
				$db->setQuery($query);
				$db->query();
				
				$query = "DELETE FROM #__lms_subscriptions WHERE id IN (".implode(',', $temp_subs).")";
				$db->setQuery($query);
				$db->query();
			}
		}
	}
}