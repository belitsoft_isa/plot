<?php 

defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

/**
*	Provides CMS database independance and
*	Corrects bugs and backwards compatibility issues in mambo core class mosDBTable ! :
*	1) empty lists return empty arrays and not NULL values !
*	2) gives a coherent interface, independant of various CMS flavors
*/
class JLMSdatabase {
	/**  Host CMS database class
	* @var database */
	var $_db;
	var $_jlms_debug = 0;
	var $_jlms_ticker = 0;
	var $_jlms_log = array();

	/**
	* Database object constructor
	*/
	function JLMSdatabase( &$_JLMS_database ) {
		//$this->initDB($_JLMS_database);
		$this->_db = & $_JLMS_database;
	}

	function database( $host='localhost', $user, $pass, $db='', $table_prefix='', $goOffline=true ) {
		if (JLMS_Jversion() == 2) {
			//$options        = array ( 'host' => $host, 'user' => $user, 'password' => $pass, 'database' => $db, 'prefix' => $table_prefix );
			// for LEGACY mode only !!!!!!!!!!!!!!
			return $this->_db->__construct( $host, $user, $pass, $db, $table_prefix, $goOffline );
			//return $this->_db->database( $host, $user, $pass, $db, $table_prefix, $goOffline );
		}
		return $this->_db->database( $host, $user, $pass, $db, $table_prefix, $goOffline );
	}

	function initDB( &$_JLMS_database ) {
		$this->_db = & $_JLMS_database;
	}

	function jlms_debug( $level ) {
		$this->_jlms_debug = intval( $level );
	}

	function get_jlms_debug() {
		return $this->_jlms_log;
	}

	function get_jlms_ticker() {
		return $this->_jlms_ticker;
	}
	/**
	* Sets debug level
	* @param int
	*/
	function debug( $level ) {
		$this->_db->debug( $level );
	}
	/**
	* @return int The error number for the most recent query
	*/
	function getErrorNum( ) {
		return $this->_db->getErrorNum();
	}
	/**
	* @return string The error message for the most recent query
	*/
	function getErrorMsg( ) {
		return $this->_db->getErrorMsg();
	}
	/**
	* Get a database escaped string
	* @return string
	*/
	function getEscaped( $text ) {
		return $this->_db->getEscaped( $text );
	}
	/**
	* Get a quoted database escaped string
	* @return string
	*/
	function Quote( $text ) {
		return '\'' . $this->_db->getEscaped( $text ) . '\'';
	}
	/**
	* Quote an identifier name (field, table, etc)
	* @param  string  The name
	* @return string  The quoted name
	*/
	function NameQuote( $s ) {
		return '`' . $s . '`';
	}
	/**
	* @return string Quoted null/zero date string
	*/
	function getNullDate( ) {
		return '0000-00-00 00:00:00';
	}
	/**
	* Sets the SQL query string for later execution.
	*
	* This function replaces a string identifier <var>$prefix</var> with the
	* string held is the <var>_table_prefix</var> class variable.
	*
	* @param string $sql     The SQL query
	* @param int    $offset  The offset to start selection
	* @param int    $limit   The number of results to return
	* @param string $prefix  The common table prefix search for replacement string
	*/
	function setQuery( $sql, $offset = 0, $limit = 0, $prefix='#__' ) {
		global $_VERSION;

		if ( ( $_VERSION->PRODUCT == "Joomla!" || $_VERSION->PRODUCT == "Accessible Joomla!" ) ) {
			$this->_db->setQuery( $sql, $offset, $limit, $prefix );
		} else {
			// Mambo:
			if ( $offset || $limit ) {
				$sql		.=	" LIMIT ";
				if ( $offset ) {
					$sql	.=	( (int) $offset ) . ', ';
				}
				$sql		.=	( (int) $limit );
			}
			$this->_db->setQuery( $sql, $prefix );
		}
	}
	/**
	* @return string The current value of the internal SQL vairable
	*/
	function getQuery( ) {
		return $this->_db->getQuery();
	}
	/**
	* Execute the query
	* 
	* @param  string  the query (optional, it will use the setQuery one otherwise)
	* @return mixed A database resource if successful, FALSE if not.
	*/
	function query( $sql = null ) {
		if ( $sql ) {
			$this->setQuery( $sql );
		}
		if ($this->_jlms_debug) {
			$this->_jlms_ticker++;
			$rr = $this->_db->_sql;
	  		$this->_jlms_log[] = $rr;
		}

		if ($this->_jlms_debug) {
			$query_result = $this->_db->query();
			$msg = $this->_db->getErrorMsg();
			if ($msg) {
				$this->_jlms_log[] = $msg;
				if (function_exists( 'debug_backtrace' )) {
					foreach( debug_backtrace() as $back) {
						if (@$back['file']) {
							$this->_jlms_log[] = '<br />'.$back['file'].':'.$back['line'];
						}
					}
				}
			}
			return $query_result;
		} else {
			return $this->_db->query();
		}
	}
	/**
	 * Executes a series of SQL orders, optionally as a transaction
	 *
	 * @param  boolean $abort_on_error      Aborts on error (true by default)
	 * @param  boolean $p_transaction_safe  Encloses all in a single transaction (false by default)
	 * @return boolean true: success, false: error(s)
	 */
	function query_batch( $abort_on_error = true, $p_transaction_safe = false) {
		return $this->_db->query_batch( $abort_on_error, $p_transaction_safe );
	}
	/** for compatibility only: */
	function queryBatch( $abort_on_error = true, $p_transaction_safe = false) {
		return $this->query_batch( $abort_on_error, $p_transaction_safe );
	}
	/**
	* @return int The number of affected rows in the previous operation
	*/
	function getAffectedRows( ) {
		if ( is_callable( array( $this->_db, 'getAffectedRows' ) ) ) {
			$affected	=	$this->_db->getAffectedRows();
		} else {
			$affected	=	mysql_affected_rows( $this->_db->_resource );
		}
		return $affected;
	}
	/**
	* Returns the number of rows returned from the most recent query.
	* 
	* @return int
	*/
	function getNumRows( $cur = null ) {
		return $this->_db->getNumRows( $cur );
	}
	/**
	 * Explain of SQL
	 * 
	 * @return string
	 */
	function explain( ) {
		return $this->_db->explain();
	}
	/**
	* This method loads the first field of the first row returned by the query.
	*
	* @return The value returned in the query or null if the query failed.
	*/
	function loadResult( ) {
		if ($this->_jlms_debug) {
			$this->_jlms_ticker++;
	  		$this->_jlms_log[] = $this->_db->_sql;
		}

		if ($this->_jlms_debug) {
			$query_result = $this->_db->loadResult();
			$msg = $this->_db->getErrorMsg();
			if ($msg) {
				$this->_jlms_log[] = '<font color="red">'.stripslashes($msg).'</font>';
			}
			return $query_result;
		} else {
			return $this->_db->loadResult();
		}
	}
	function & _nullToArray( &$resultArray ) {
		if ( $resultArray === null ) {		// mambo strangeness
			$resultArray	=	array();
		}
		return $resultArray;
	}
	/**
	* Load an array of single field results into an array
	*/
	function loadResultArray( $numinarray = 0 ) {
		if ($this->_jlms_debug) {
			$this->_jlms_ticker++;
	  		$this->_jlms_log[] = $this->_db->_sql;
		}

		if ($this->_jlms_debug) {
			$query_result = $this->_db->loadResultArray( $numinarray );
			$msg = $this->_db->getErrorMsg();
			if ($msg) {
				$this->_jlms_log[] = '<font color="red">'.stripslashes($msg).'</font>';
			}
			return $this->_nullToArray( $query_result );
		} else {
			return $this->_nullToArray( $this->_db->loadResultArray( $numinarray ) );
		}
	}
	/**
	* Fetch a result row as an associative array
	*
	* @return array
	*/
	function loadAssoc( ) {
		if ( is_callable( array( $this->_db, 'loadAssoc' ) ) ) {
			return $this->_db->loadAssoc( );
		} else {
			// new independant efficient implementation:
			if ( ! ( $cur = $this->query() ) ) {
				$result	=	null;
			} else {
				$result		=	mysql_fetch_assoc( $cur );
				if ( ! $result ) {
					$result	=	null;
				}
				mysql_free_result( $cur );
				return $result;
			}
		}
	}
	/**
	* Load a assoc list of database rows
	* 
	* @param string The field name of a primary key
	* @return array If <var>key</var> is empty as sequential list of returned records.
	*/
	function loadAssocList( $key = null ) {
		global $_VERSION;
		if ( ( $key == '' ) || ( $_VERSION->PRODUCT == "Joomla!" || $_VERSION->PRODUCT == "Accessible Joomla!" ) ) {
			return $this->_nullToArray( $this->_db->loadAssocList( $key ) );
		} else {
			// mambo 4.5.2 - 4.6.2 has a bug in key:
			if ( ! ( $cur = $this->_db->query() ) ) {
				return null;
			}
			$array = array();
			while ( $row = mysql_fetch_assoc( $cur ) ) {
				if ( $key ) {
					$array[$row[$key]]	=	$row;		//  $row->key is not an object, but an array
				} else {
					$array[]			=	$row;
				}
			}
			mysql_free_result( $cur );
			return $array;
		}
	}
	/**
	* This global function loads the first row of a query into an object
	*
	* If an object is passed to this function, the returned row is bound to the existing elements of <var>object</var>.
	* If <var>object</var> has a value of null, then all of the returned query fields returned in the object.
	* @param string The SQL query
	* @param object The address of variable
	*/
	function loadObject( &$object ) {

		if ($this->_jlms_debug) {
			$this->_jlms_ticker++;
	  		$this->_jlms_log[] = $this->_db->_sql;
		}

		if ($this->_jlms_debug) {
			$query_result = $this->_db->loadObject( $object );
			$msg = $this->_db->getErrorMsg();
			if ($msg) {
				$this->_jlms_log[] = '<font color="red">'.stripslashes($msg).'</font>';
			}
			return $query_result;
		} else {
			return $this->_db->loadObject( $object );
		}
	}
	/**
	* Load a list of database objects
	* @param string The field name of a primary key
	* @return array If <var>key</var> is empty as sequential list of returned records.
	* If <var>key</var> is not empty then the returned array is indexed by the value
	* the database key.  Returns <var>null</var> if the query fails.
	*/
	function loadObjectList( $key = null ) {

		if ($this->_jlms_debug) {
			$this->_jlms_ticker++;
	  		$this->_jlms_log[] = $this->_db->_sql;
		}

		if ($this->_jlms_debug) {
			$query_result = $this->_db->loadObjectList( $key );
			$msg = $this->_db->getErrorMsg();
			if ($msg) {
				$this->_jlms_log[] = '<font color="red">'.stripslashes($msg).'</font>';
			}
			/*$qqq = $this->_db->_sql;
			$qqq = 'EXPLAIN '.$qqq;
			$this->_db->setQuery($qqq);
			$query_result2 = $this->_db->loadObjectList();
			//echo $this->_db->getquery();
			//echo $this->_db->getErrorMsg();
			$mss = var_export($query_result2, true);
			$this->_jlms_log[] = '<font color="red">'.stripslashes($mss).'</font>';*/
			return $this->_nullToArray( $query_result );
		} else {
			return $this->_nullToArray( $this->_db->loadObjectList( $key ) );
		}
	}
	/**
	* @return The first row of the query.
	*/
	function loadRow( ) {

		if ($this->_jlms_debug) {
			$this->_jlms_ticker++;
	  		$this->_jlms_log[] = $this->_db->_sql;
		}

		if ($this->_jlms_debug) {
			$query_result = $this->_db->loadRow();
			$msg = $this->_db->getErrorMsg();
			if ($msg) {
				$this->_jlms_log[] = '<font color="red">'.stripslashes($msg).'</font>';
			}
			return $query_result;
		} else {
			return $this->_db->loadRow();
		}
	}
	/**
	* Load a list of database rows (numeric column indexing)
	* @param string The field name of a primary key
	* @return array If <var>key</var> is empty as sequential list of returned records.
	* If <var>key</var> is not empty then the returned array is indexed by the value
	* the database key.  Returns <var>null</var> if the query fails.
	*/
	function loadRowList( $key = null ) {

		if ($this->_jlms_debug) {
			$this->_jlms_ticker++;
	  		$this->_jlms_log[] = $this->_db->_sql;
		}

		if ($this->_jlms_debug) {
			$query_result = $this->_db->loadRowList( $key );
			$msg = $this->_db->getErrorMsg();
			if ($msg) {
				$this->_jlms_log[] = '<font color="red">'.stripslashes($msg).'</font>';
			}
			return $this->_nullToArray( $query_result );
		} else {
			return $this->_nullToArray( $this->_db->loadRowList( $key ) );
		}

	}
	/**
	* Insert an object into database
	*
	* @param string   $table This is expected to be a valid (and safe!) table name
	* @param stdClass $object
	* @param string   $keyName
	* @param boolean  $verbose
	* @param boolean  TRUE if insert succeeded, FALSE when error
	*/
	function insertObject( $table, &$object, $keyName = NULL, $verbose=false ) {

		if ($this->_jlms_debug) {
			$this->_jlms_ticker++;
	  		$this->_jlms_log[] = $this->_db->_sql;
		}

		if ($this->_jlms_debug) {
			$query_result = $this->_db->insertObject( $table, $object, $keyName, $verbose );
			$msg = $this->_db->getErrorMsg();
			if ($msg) {
				$this->_jlms_log[] = '<font color="red">'.stripslashes($msg).'</font>';
			}
			return $query_result;
		} else {
			return $this->_db->insertObject( $table, $object, $keyName, $verbose );
		}

	}
	/**
	* Updates an object into a database
	*
	* @param  string   $table        This is expected to be a valid (and safe!) table name
	* @param  stdClass $object
	* @param  string   $keyName
	* @param  boolean  $updateNulls
	* @return mixed    A database resource if successful, FALSE if not.
	*/
	function updateObject( $table, &$object, $keyName, $updateNulls=true ) {
		// return $this->_db->updateObject( $table, $object, $keyName, $updateNulls );
		$fmtsql = "UPDATE $table SET %s WHERE %s";
		$tmp = array();
		foreach (get_object_vars( $object ) as $k => $v) {
			if( is_array($v) or is_object($v) or $k[0] == '_' ) { // internal or NA field
				continue;
			}
			if( $k == $keyName ) { // PK not to be updated
				$where = $keyName . '=' . $this->Quote( $v );
				continue;
			}
			if ($v === NULL && !$updateNulls) {
				continue;
			}
			if( $v === NULL ) {
				$val = "NULL";			// this case was missing in Mambo
			} elseif( $v == '' ) {
				$val = "''";
			} else {
				$val = $this->Quote( $v );
			}
			$tmp[] = $this->NameQuote( $k ) . '=' . $val;
		}
		$this->setQuery( sprintf( $fmtsql, implode( ",", $tmp ) , $where ) );
		return $this->query();
	}

	/**
	* Returns the formatted standard error message of SQL
	* @param  boolean $showSQL  If TRUE, displays the last SQL statement sent to the database
	* @return string  A standised error message
	*/
	function stderr( $showSQL = false ) {
		return $this->_db->stderr( $showSQL );
	}
	/**
	* Returns the insert_id() from Mysql
	*
	* @return int
	*/
	function insertid( ) {
		return $this->_db->insertid();
	}
	/**
	* Returns the version of MySQL
	*
	* @return string
	*/
	function getVersion( ) {
		return $this->_db->getVersion();
	}
	/**
	* @return array A list of all the tables in the database
	*/
	function getTableList( ) {
		return $this->_db->getTableList();
	}
	/**
	 * @param array A list of valid (and safe!) table names
	 * @return array A list the create SQL for the tables
	 */
	function getTableCreate( $tables ) {
		return $this->_db->getTableCreate( $tables );
	}
	/**
	 * @param array A list of valid (and safe!) table names
	 * @return array An array of fields by table
	 */
	function getTableFields( $tables ) {
		return $this->_db->getTableFields( $tables );
	}
	/**
	* Fudge method for ADOdb compatibility
	*/
	function GenID( $foo1=null, $foo2=null ) {
		return '0';
	}
	/**
	 * Checks if database's collation is case-INsensitive
	 * WARNING: individual table's fields might have a different collation
	 *
	 * @return boolean  TRUE if case INsensitive
	 */
	function isDbCollationCaseInsensitive( ) {
		static $result = null;

		if ( $result === null ) {
			$query = "SELECT IF('a'='A', 1, 0);";
			$this->setQuery( $query );
			$result		=	$this->loadResult();
		}
		return ( $result == 1 );
	}
}	// class JLMSdatabase

?>