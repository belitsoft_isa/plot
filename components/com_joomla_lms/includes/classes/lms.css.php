<?php 
/**
* includes/classes/lms.css.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

defined( 'JPATH_BASE' ) or die( 'Restricted access' );

class JLMSCSS
{	
	static function _( $classname, $addclass1 = '', $addclass2 = '', $addclass3 = '', $j30_hover = true )
	{
		$classes = array();
		if ($addclass1) $classes[] = $addclass1;
		if ($addclass2) $classes[] = $addclass2;
		if ($addclass3) $classes[] = $addclass3;
		
		if( JLMS_J16version() ) 
		{
			switch( $classname ) 
			{
				case 'sectiontableentry1':	{ $classes[] = 'sectiontableentry1'; $classes[] = 'odd'; }			break;
				case 'sectiontableentry2':	{ $classes[] = 'sectiontableentry2'; $classes[] = 'even'; }			break;
				case 'sectiontableheader':	{ $classes[] = 'sectiontableheader'; }								break;
				case 'jlmslist':			{ $classes[] = 'jlmslist'; $classes[] = 'category'; if (JLMS_J30version()) { $classes[] = 'table'; $classes[] = 'table-striped'; if ($j30_hover) {$classes[] = 'table-hover';} } }				break;
				case 'jlmslist-footer_td':	{ $classes[] = 'jlmslist-footer_td'; $classes[] = 'table_footer';}	break;
			} 
		} else {
			switch( $classname ) 
			{
				case 'sectiontableentry1':	{ $classes[] = 'sectiontableentry1'; }								break;
				case 'sectiontableentry2':	{ $classes[] = 'sectiontableentry2'; }								break;
				case 'sectiontableheader':	{ $classes[] = 'sectiontableheader'; }								break;
				case 'jlmslist':			{ $classes[] = 'jlmslist'; }										break;
				case 'jlmslist-footer_td':	{ $classes[] = 'jlmslist-footer_td'; }								break;
			} 
		}
		$classes = array_unique($classes);
		if (count($classes)) {
			$res = implode(' ', $classes);
		} else {
			$res = '';
		}
		return $res;
    }

    static function tableheadertag()
	{
		if( JLMS_J16version() ) {
			return 'th';
		} else {
			return 'td';
		}		
	}

    static function h2($text = '') {
		if (JLMS_J16version()) {
			return '<div class="contentheading"><h2>'.$text.'</h2></div>';
		} else {
			return '<div class="contentheading">'.$text.'</div>';
		}
	}

    static function h2_js($text = 'var_js'){
		ob_start();
		?>
		<script type="text/javascript">
		function JLMSCSS_h2_js(<?php echo $text;?>){
			var js = '<?php echo JLMSCSS::h2('{text}');?>';
			return js.replace('{text}', <?php echo $text;?>);
		}
		</script>
		<?php
		$js_fn = ob_get_contents();
		ob_get_clean();
		
		$js_fn = str_replace('<script type="text/javascript">', '', $js_fn);
		$js_fn = str_replace('</script>', '', $js_fn);
		
		return $js_fn;
	}

	static function file() {
		$JLMS_CONFIG = JLMSFactory::getConfig();
		$style_file = 'jlms_107.css';
		/*todo del
                if (JLMS_J30version()) 
		{
			$style_file = 'jlms_300.css';
		}*/
		return $style_file.'?rev='.$JLMS_CONFIG->getVersionToken();
	}
	
	static function link()
	{
		return 	JURI::root().'components/com_joomla_lms/lms_css/'.JLMSCSS::file();	
	}
}
?>