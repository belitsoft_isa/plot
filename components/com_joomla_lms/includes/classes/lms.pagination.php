<?php
/**
* includes/classes/lms.pagination.php
* * * ElearningForce Inc
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

/**
* Page navigation support class
*/
class JLMSPageNav {
	/** The record number to start dislpaying from 
	 *  @var int */
	var $limitstart 	= null;
	/** Number of rows to display per page
	 * @var int */
	var $limit 			= null;
	/** Total number of rows
	 * @var int */
	var $total 			= null;

	function JLMSPageNav( $total, $limitstart, $limit ) {
		$this->total 		= (int) $total;
		$this->limitstart 	= (int) max( $limitstart, 0 );
		$this->limit 		= (int) max( $limit, 1 );
		if(!in_array($this->limit,array(5,10,15,25,50,100))){
			$this->limit = 50;
		}
		if ($this->limit > $this->total) {
			$this->limitstart = 0;
		}
		if (($this->limit-1)*$this->limitstart > $this->total) {
			$this->limitstart -= $this->limitstart % $this->limit;
		}
	}
	/**
	* @return string The html for the limit # input box
	*/
	function getLimitBox ($link) {
		$limits = array();
		foreach ( array(5,10,15,25,50,100) as $i ) {
			$limits[] = mosHTML::makeOption( "$i" );
		}

		$link = $link ."&amp;limit=".JLMS_SELECTED_INDEX_MARKER."&amp;limitstart=". $this->limitstart;			
		$link = processSelectedIndexMarker( $link );		
		
		return mosHTML::selectList( $limits, 'limit', 'class="inputbox input-mini" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $this->limit ); 

	}
	/**
	* Writes the html limit # input box
	*/
	function writeLimitBox ($link) {
		echo $this->getLimitBox($link);
	}
	function writePagesCounter() {
		echo $this->getPagesCounter();
	}
	/**
	* @return string The html for the pages counter, eg, Results 1-10 of x
	*/
	function getPagesCounter() {
		$html = '';
		$from_result = $this->limitstart+1;
		if ($this->limitstart + $this->limit < $this->total) {
			$to_result = $this->limitstart + $this->limit;
		} else {
			$to_result = $this->total;
		}
		if ($this->total > 0) {
			$html .= "\n" . _JLMS_PN_RESULTS . " <strong>" . $from_result . " - " . $to_result . "</strong> " . _JLMS_PN_OF_TOTAL . " <strong>" . $this->total . "</strong>";

		} else {
			$html .= "\n" . _JLMS_PN_NO_RESULTS . ".";
		}
		return $html;
	}
	/**
	* Writes the html for the pages counter (pages counter is built using javascript and form fields), eg, Results 1-10 of x
	*/
	function writePagesLinksJS() {
		echo $this->getPagesLinksJS();
	}

	function writePageNavJS($form_name = 'adminForm') {
	echo '
<script language="javascript" type="text/javascript">
<!--
function jlmspagenav_goto(pagenum){
	var form = document.'.$form_name.';
	form.limitstart.value = pagenum;
	form.submit();
}
//-->
</script>
';
	}
	/**
	* @return string The html links for pages (built using javascript and form fields), eg, previous, next, 1 2 3 ... x
	*/
	function getPagesLinksJS() {
		$limitstart = max( (int) $this->limitstart, 0 );
		$limit		= max( (int) $this->limit, 1 );
		$total		= (int) $this->total;
		$html 				= '';
		$displayed_pages 	= 10;		// set how many pages you want displayed in the menu (not including first&last, and ev. ... repl by single page number.
		$total_pages = ceil( $total / $limit );
		$this_page = ceil( ($limitstart+1) / $limit );
//		$start_loop 		= (floor(($this_page-1)/$displayed_pages))*$displayed_pages+1;
		$start_loop = $this_page-floor($displayed_pages/2);
		if ($start_loop < 1) {
			$start_loop = 1;
		}
		if ($start_loop == 3) {
			$start_loop = 2;
		}
		if ( $start_loop + $displayed_pages - 1 < $total_pages - 2 ) {
			$stop_loop = $start_loop + $displayed_pages - 1;
		} else {
			$stop_loop = $total_pages;
		}

		if ($this_page > 1) {
			$page = ($this_page - 2) * $this->limit;
			$html .= "\n<a href=\"javascript:jlmspagenav_goto(0);\" class=\"pagenav\" title=\"" . _JLMS_PN_FIRST_PAGE . "\">&lt;&lt;&nbsp;" . _JLMS_PN_FIRST_PAGE . "</a>";
			$html .= "\n<a href=\"javascript:jlmspagenav_goto($page);\" class=\"pagenav\" title=\"" . _JLMS_PN_PREV_PAGE . "\">&lt;&nbsp;" . _JLMS_PN_PREV_PAGE . "</a>";
			if ($start_loop > 1) {
				$html .= "\n<a href=\"javascript:jlmspagenav_goto(0);\" class=\"pagenav\" title=\"" . _JLMS_PN_FIRST_PAGE . "\">&nbsp;1</a>";
			}
			if ($start_loop > 2) {
				$html .= "\n<span class=\"pagenav\"> <strong>...</strong> </span>";
			}
		} else {
			$html .= "\n<span class=\"pagenav\">&lt;&lt;&nbsp;" . _JLMS_PN_FIRST_PAGE . "</span>";
			$html .= "\n<span class=\"pagenav\">&lt;&nbsp;" . _JLMS_PN_PREV_PAGE . "</span>";
		}

		for ($i=$start_loop; $i <= $stop_loop; $i++) {
			$page = ($i - 1) * $this->limit;
			if ($i == $this_page) {
				$html .= "\n<span class=\"pagenav\"> $i </span>";
			} else {
				$html .= "\n<a href=\"javascript:jlmspagenav_goto($page);\" class=\"pagenav\"><strong>$i</strong></a>";
			}
		}

		if ($this_page < $total_pages) {
			$page = $this_page * $this->limit;
			$end_page = ($total_pages-1) * $this->limit;
			if ($stop_loop < $total_pages-1) {
				$html .= "\n<span class=\"pagenav\"> <strong>...</strong> </span>";
			}
			if ($stop_loop < $total_pages) {
				$html .= "\n<a href=\"javascript:jlmspagenav_goto($end_page);\" class=\"pagenav\" title=\"" . _JLMS_PN_END_PAGE . "\"> <strong>" . $total_pages."</strong></a>";
			}
			$html .= "\n<a href=\"javascript:jlmspagenav_goto($page);\" class=\"pagenav\" title=\"" . _JLMS_PN_NEXT_PAGE . "\"> " . _JLMS_PN_NEXT_PAGE . "&nbsp;&gt;</a>";
			$html .= "\n<a href=\"javascript:jlmspagenav_goto($end_page);\" class=\"pagenav\" title=\"" . _JLMS_PN_END_PAGE . "\"> " . _JLMS_PN_END_PAGE . "&nbsp;&gt;&gt;</a>";
		} else {
			$html .= "\n<span class=\"pagenav\">" . _JLMS_PN_NEXT_PAGE . "&nbsp;&gt;</span>";
			$html .= "\n<span class=\"pagenav\">" . _JLMS_PN_END_PAGE . "&nbsp;&gt;&gt;</span>";
		}
		return $html;
	}

	/**
	* Writes the html for the pages counter, eg, Results 1-10 of x
	*/
	function writePagesLinks($link) {
		echo $this->getPagesLinks($link);
	}

	/**
	* @return string The html links for pages, eg, previous, next, 1 2 3 ... x
	*/
	function getPagesLinks($link) {
		$limitstart = max( (int) $this->limitstart, 0 );
		$limit		= max( (int) $this->limit, 1 );
		$total		= (int) $this->total;
		$html 				= '';
		$displayed_pages 	= 10;		// set how many pages you want displayed in the menu (not including first&last, and ev. ... repl by single page number.
		$total_pages = ceil( $total / $limit );
		$this_page = ceil( ($limitstart+1) / $limit );
//		$start_loop 		= (floor(($this_page-1)/$displayed_pages))*$displayed_pages+1;
		$start_loop = $this_page-floor($displayed_pages/2);
		if ($start_loop < 1) {
			$start_loop = 1;
		}
		if ($start_loop == 3) {
			$start_loop = 2;
		}
		if ( $start_loop + $displayed_pages - 1 < $total_pages - 2 ) {
			$stop_loop = $start_loop + $displayed_pages - 1;
		} else {
			$stop_loop = $total_pages;
		}

		/*if ($this_page > 1) {
			$page = ($this_page - 2) * $this->limit;
			$txt .= '<a href="'. sefRelToAbs( "$link&amp;limitstart=0" ) .'" class="pagenav" title="'. _PN_START .'">'. _PN_LT . _PN_LT . $pnSpace . _PN_START .'</a> ';
			$txt .= '<a href="'. sefRelToAbs( "$link&amp;limitstart=$page" ) .'" class="pagenav" title="'. _PN_PREVIOUS .'">'. _PN_LT . $pnSpace . _PN_PREVIOUS .'</a> ';
		} else {
			$txt .= '<span class="pagenav">'. _PN_LT . _PN_LT . $pnSpace . _PN_START .'</span> ';
			$txt .= '<span class="pagenav">'. _PN_LT . $pnSpace . _PN_PREVIOUS .'</span> ';
		} */
		if ($this_page > 1) {
			$page = ($this_page - 2) * $this->limit;
			$html .= "\n<a href=\"". sefRelToAbs( "$link&amp;limitstart=0" ) ."\" class=\"pagenav\" title=\"" . _JLMS_PN_FIRST_PAGE . "\">&lt;&lt;&nbsp;" . _JLMS_PN_FIRST_PAGE . "</a>";
			$html .= "\n<a href=\"".sefRelToAbs( "$link&amp;limitstart=$page" )."\" class=\"pagenav\" title=\"" . _JLMS_PN_PREV_PAGE . "\">&lt;&nbsp;" . _JLMS_PN_PREV_PAGE . "</a>";
			if ($start_loop > 1) {
				$html .= "\n<a href=\"". sefRelToAbs( "$link&amp;limitstart=0" ) ."\" class=\"pagenav\" title=\"" . _JLMS_PN_FIRST_PAGE . "\">&nbsp;1</a>";
			}
			if ($start_loop > 2) {
				$html .= "\n<span class=\"pagenav\"> <strong>...</strong> </span>";
			}
		} else {
			$html .= "\n<span class=\"pagenav\">&lt;&lt;&nbsp;" . _JLMS_PN_FIRST_PAGE . "</span>";
			$html .= "\n<span class=\"pagenav\">&lt;&nbsp;" . _JLMS_PN_PREV_PAGE . "</span>";
		}

		for ($i=$start_loop; $i <= $stop_loop; $i++) {
			$page = ($i - 1) * $this->limit;
			if ($i == $this_page) {
				$html .= "\n<span class=\"pagenav\"> $i </span>";
			} else {
				$html .= "\n<a href=\"".sefRelToAbs( "$link&amp;limitstart=$page" )."\" class=\"pagenav\"><strong>$i</strong></a>";
			}
		}

		if ($this_page < $total_pages) {
			$page = $this_page * $this->limit;
			$end_page = ($total_pages-1) * $this->limit;
			if ($stop_loop < $total_pages-1) {
				$html .= "\n<span class=\"pagenav\"> <strong>...</strong> </span>";
			}
			if ($stop_loop < $total_pages) {
				$html .= "\n<a href=\"".sefRelToAbs( "$link&amp;limitstart=$end_page" )."\" class=\"pagenav\" title=\"" . _JLMS_PN_END_PAGE . "\"> <strong>" . $total_pages."</strong></a>";
			}
			$html .= "\n<a href=\"".sefRelToAbs( "$link&amp;limitstart=$page" )."\" class=\"pagenav\" title=\"" . _JLMS_PN_NEXT_PAGE . "\"> " . _JLMS_PN_NEXT_PAGE . "&nbsp;&gt;</a>";
			$html .= "\n<a href=\"".sefRelToAbs( "$link&amp;limitstart=$end_page" )."\" class=\"pagenav\" title=\"" . _JLMS_PN_END_PAGE . "\"> " . _JLMS_PN_END_PAGE . "&nbsp;&gt;&gt;</a>";
		} else {
			$html .= "\n<span class=\"pagenav\">" . _JLMS_PN_NEXT_PAGE . "&nbsp;&gt;</span>";
			$html .= "\n<span class=\"pagenav\">" . _JLMS_PN_END_PAGE . "&nbsp;&gt;&gt;</span>";
		}
		return $html;
	}
	function getListFooter($link) {
		$html = '<table class="adminlist"><tr><th colspan="3" style="text-align:center;">';
		$html .= $this->getPagesLinks($link);
		$html .= '</th></tr><tr>';
		$html .= '<td nowrap="nowrap" width="48%" align="right">'._JLMS_PN_DISPLAY_NUM.'</td>';
		$html .= '<td>' .$this->getLimitBox($link) . '</td>';
		$html .= '<td nowrap="nowrap" width="48%" align="left">' . $this->getPagesCounter() . '</td>';
		$html .= '</tr></table>';
  		return $html;
	}
	/**
	 * Sets the vars for the page navigation template
	 */
	function setTemplateVars( &$tmpl, $link='', $name = 'admin-list-footer' ) {
		$tmpl->addVar( $name, 'PAGE_LINKS', $this->getPagesLinks($link) );
		$tmpl->addVar( $name, 'PAGE_LIST_OPTIONS', $this->getLimitBox($link) );
		$tmpl->addVar( $name, 'PAGE_COUNTER', $this->getPagesCounter() );
	}

	function isMultiPages() {
		return ($this->total > $this->limit );
	}
}
?>