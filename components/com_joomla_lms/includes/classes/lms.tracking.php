<?php 
/**
* includes/lms_tracking.class.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

define('JLMS_PAGE_TYPE_COURSE',		0);
define('JLMS_PAGE_TYPE_DOC',		1);
define('JLMS_PAGE_TYPE_LINK',		2);
define('JLMS_PAGE_TYPE_DROPBOX',	3);
define('JLMS_PAGE_TYPE_LPATH',		4);
define('JLMS_PAGE_TYPE_HW',			5);
define('JLMS_PAGE_TYPE_AGENDA',		6);
define('JLMS_PAGE_TYPE_CONFERENCE',	7);
define('JLMS_PAGE_TYPE_CHAT',		8);
define('JLMS_PAGE_TYPE_LPATH_STU',	9);
define('JLMS_PAGE_TYPE_FORUM',		10);
define('JLMS_PAGE_TYPE_QUIZ',		11);
define('JLMS_PAGE_TYPE_CERT',		12);
define('JLMS_PAGE_TYPE_MAILBOX',	13);
define('JLMS_PAGE_TYPE_OUTDOC',		14);

require_once(JPATH_SITE.'/components/com_joomla_lms/includes/classes/lms.class.php');

class JLMS_Tracking extends JLMSObject {

	var $_trackEnabled = null;
	var $_db;
	
	var $_timeinterval;
	var $_show_online;
	var $_show_online_pulse;
	var $_show_status;

	/**	Class constructor	*/
	function __construct( $trackEnabled = 0, &$db ) {
		$this->_trackEnabled = $trackEnabled;
		$this->_db = & $db;
		$this->set( 'trackTime', date('Y-m-d H:i:s', time() - date('Z') ) ); // the same as gmdate('Y-m-d H:i:s'
	}
	function UserViewedItems($user_id, $item_id, $page_id) {		
		$query = "INSERT IGNORE INTO #__lms_track_viewed_items (user_id, page_id, item_id)"
		. "\n VALUES('$user_id', '$page_id', '$item_id')";
		$this->_db->SetQuery( $query );
		$this->_db->query();		
	}
	function UserDownloadFile( $user_id, $doc_id ) {
		if ($this->_trackEnabled) {
			$query = "INSERT INTO #__lms_track_downloads (doc_id, user_id, track_time)"
			. "\n VALUES('$doc_id', '$user_id', '".$this->trackTime."')";
			$this->_db->SetQuery( $query );
			$this->_db->query();
		}
	}
	function UserEnterChat( $user_id, $course_id ) {
		if ($this->_trackEnabled) {
			$query = "INSERT INTO #__lms_track_chat (user_id, course_id, track_time)"
			. "\n VALUES('$user_id', '$course_id', '".$this->trackTime."')";
			$this->_db->SetQuery( $query );
			$this->_db->query();
		}
	}
	function UserHit( $user_id, $course_id, $page_id ) {
		if ($this->_trackEnabled) {
			$query = "INSERT INTO #__lms_track_hits (user_id, page_id, course_id, track_time)"
			. "\n VALUES('$user_id', '$page_id', '$course_id', '".$this->trackTime."')";
			$this->_db->SetQuery( $query );
			if( $this->_db->query() ) 
			{		
				$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
				$_JLMS_PLUGINS->loadBotGroup('user');
				$_JLMS_PLUGINS->trigger('OnAfterUserHit', array($user_id, $course_id));
			}
		}
	}
	
	function TimeTrakingRequireJS(){
		$this->_timeinterval_course = 30; //sec
		$this->_timeinterval_resource = 60; //sec
		$this->_show_online = 0;
		$this->_show_online_pulse = 0;
		$this->_show_status = 0;
		$this->_limit_min = 1; //min
		
		if (JLMS_J30version())
		{
			$fileTimeTracker = 'timeTracker30.js';
		}else
		{
		if( JLMS_mootools12() ) {
			$fileTimeTracker = 'timeTracker16.js';
		} else {
			$fileTimeTracker = 'timeTracker.js';
		}
		}
		if(file_exists(_JOOMLMS_FRONT_HOME . DS . 'includes' . DS . 'js' . DS . $fileTimeTracker)){
			
			$fileTimeTracker .= '?rev='.date("YmdHms", $this->TimeTracking_currentTime());
			
			JLMS_HTML::_('behavior.mootools');
			
			$document = JFactory::getDocument();
			$document->addScript(JURI::base().'components/com_joomla_lms/includes/js/'.$fileTimeTracker);
			return true;
		} else {
			return false;
		}
	}
	
	function TimeTrakingOnJS(&$course_id, &$user_id){
		global $JLMS_CONFIG;
		$script = "
				var TTracker_".$course_id."_".$user_id." = new TimeTracker({
					interval: ".$this->_timeinterval_course.",
					url_handler: '".JURI::base()."index.php?option=com_joomla_lms&task=timetracking',
					method: 'post',
					course_id: ".$course_id.",
					user_id: ".$user_id.",
					show_online: ".$this->_show_online.",
					show_online_pulse: ".$this->_show_online_pulse.",
					show_status: ".$this->_show_status.",
					limit_min: ".$this->_limit_min."
				});
		";
		$document = JFactory::getDocument();
		$document->addScriptDeclaration($script);
		$script_domready = "TTracker_".$course_id."_".$user_id.".start();";
		if(!isset($_REQUEST['action'])){
			$JLMS_CONFIG->set('web20_domready_code', $JLMS_CONFIG->get('web20_domready_code','').$script_domready);
		}
	}
	
	function TimeTrakingOnJS_Resource(&$course_id, &$user_id, $resourse_type=9, $resourse_id){
		global $JLMS_CONFIG;
		$script = '';
		$script .= '
			var TTracker_'.$course_id.'_'.$user_id.'_'.$resourse_type.'_'.$resourse_id.' = new TimeTracker({
				interval: '.$this->_timeinterval_resource.',
				url_handler: "'.JURI::base().'index.php?option=com_joomla_lms&task=timetracking",
				method: "post",
				course_id: '.$course_id.',
				user_id: '.$user_id.',
				resource_type: '.$resourse_type.',
				resource_id: '.$resourse_id.',
				show_online: 1,
				show_online_pulse: '.$this->_show_online_pulse.',
				show_status: '.$this->_show_status.',
				limit_min: '.$this->_limit_min.'
			});
		';
		$document = JFactory::getDocument();
		$document->addScriptDeclaration($script);
		//$JLMS_CONFIG->set('web20_domready_code', $JLMS_CONFIG->get('web20_domready_code','').$script); //test
	}
	
	function TimeTracking_currentTime(){
		return time() - date('Z');
	}
	
	function TimeTrackingClear(&$course_id, &$user_id, $resource_type=0, &$resource_id=0){
		if($resource_id){
			$query = "DELETE FROM #__lms_time_tracking_resources"
			. "\n WHERE 1"
			. "\n AND course_id = '".$course_id."'"
			. "\n AND user_id = '".$user_id."'"
			. "\n AND resource_type = '".$resource_type."'"
			. "\n AND resource_id = '".$resource_id."'"
			;
		} else {
			$query = "DELETE FROM #__lms_time_tracking"
			. "\n WHERE 1"
			. "\n AND course_id = '".$course_id."'"
			. "\n AND user_id = '".$user_id."'"
			;
		}
		$this->_db->setQuery($query);
		$this->_db->query();
	}
	
	function TimeTrackingActivate(&$course_id, &$user_id, $resource_type=0, &$resource_id=0, &$item_id=0){
		if($course_id && $user_id){
			$current_time = $this->TimeTracking_currentTime();
			
			if($resource_id){
				$this->_timeinterval = $this->_timeinterval_resource;
			} else {
				$this->_timeinterval = $this->_timeinterval_course;
			}
			
			if($resource_id){
				$query = "SELECT *"
				. "\n FROM #__lms_time_tracking_resources"
				. "\n WHERE 1"
				. "\n AND course_id = '".$course_id."'"
				. "\n AND user_id = '".$user_id."'"
				. "\n AND resource_type = '".$resource_type."'"
				. "\n AND resource_id = '".$resource_id."'"
				. "\n AND item_id = '".$item_id."'"
				;
			} else {
				$query = "SELECT *"
				. "\n FROM #__lms_time_tracking"
				. "\n WHERE 1"
				. "\n AND course_id = '".$course_id."'"
				. "\n AND user_id = '".$user_id."'"
				;
			}
			$this->_db->setQuery($query);
			$time_tracking = $this->_db->loadObject();
			
			if(!isset($time_tracking->id) || !$time_tracking->id){
				if($resource_id){
					$query = "INSERT INTO #__lms_time_tracking_resources"
					. "\n (id, course_id, user_id, resource_type, resource_id, item_id, time_spent, time_last_activity)"
					. "\n VALUES"
					. "\n ('', ".$course_id.", ".$user_id.", ".$resource_type.", ".$resource_id.", ".$item_id.", 0, ".$current_time.")"
					;
				} else {
					$query = "INSERT INTO #__lms_time_tracking"
					. "\n (id, course_id, user_id, time_spent, time_last_activity)"
					. "\n VALUES"
					. "\n ('', ".$course_id.", ".$user_id.", 0, ".$current_time.")"
					;
				}
				$this->_db->setQuery($query);
				$this->_db->query();
			}
			
			if($resource_id){
				$query = "SELECT *"
				. "\n FROM #__lms_time_tracking_resources"
				. "\n WHERE 1"
				. "\n AND course_id = '".$course_id."'"
				. "\n AND user_id = '".$user_id."'"
				. "\n AND resource_type = '".$resource_type."'"
				. "\n AND resource_id = '".$resource_id."'"
				. "\n AND item_id = '".$item_id."'"
				;
			} else {
				$query = "SELECT *"
				. "\n FROM #__lms_time_tracking"
				. "\n WHERE 1"
				. "\n AND course_id = '".$course_id."'"
				. "\n AND user_id = '".$user_id."'"
				;
			}
			$this->_db->setQuery($query);
			$time_tracking = $this->_db->loadObject();
			
			$start_time = isset($time_tracking->time_last_activity) && $time_tracking->time_last_activity ? $time_tracking->time_last_activity : $current_time;
			$time_spent = isset($time_tracking->time_spent) && $time_tracking->time_spent ? $time_tracking->time_spent : 0;
			$time_spent_before = $time_spent;
			
			$delta_time_spent = $current_time - $start_time;
			
			if($resource_id){
				//echo "\n";
				//echo "\n";
				//print_r('$start_time='.$start_time);
				//echo "\n";
				//print_r('$time_spent='.$time_spent);
				//echo "\n";
				//print_r('$time_spent_before='.$time_spent_before);
				//echo "\n";
				//print_r('_timeinterval='.$this->_timeinterval);
				//echo "\n";
				//print_r('$delta_time_spent='.$delta_time_spent);
				//echo "\n";
				//print_r($time_tracking);
				//echo "\n";
			}
			
			if(isset($time_tracking->id) && $time_tracking->id){
				if($delta_time_spent < $this->_timeinterval){
					$time_spent = $time_spent + $delta_time_spent;
				}
			 	$time_spent_after = $time_spent;
				
				if($resource_id){
			 		$query = "UPDATE #__lms_time_tracking_resources"
				 	. "\n SET"
				 	. "\n time_last_activity = '".$current_time."'"
				 	.($time_spent_after && $time_spent_before <= $time_spent_after ? "\n, time_spent = '".$time_spent."'" : '')
				 	. "\n WHERE 1"
				 	. "\n AND course_id = '".$course_id."'"
					. "\n AND user_id = '".$user_id."'"
					. "\n AND resource_type = '".$resource_type."'"
					. "\n AND resource_id = '".$resource_id."'"
					. "\n AND item_id = '".$item_id."'"
					;
			 	} else {
				 	$query = "UPDATE #__lms_time_tracking"
				 	. "\n SET"
				 	. "\n time_last_activity = '".$current_time."'"
				 	.($time_spent_after && $time_spent_before <= $time_spent_after ? "\n, time_spent = '".$time_spent."'" : '')
				 	. "\n WHERE 1"
				 	. "\n AND course_id = '".$course_id."'"
					. "\n AND user_id = '".$user_id."'"
					;
			 	}
				
				if($resource_id){
					//echo "\n";
					//echo "\n";
					//print_r($query);
					//echo "\n";
				}
				
				$this->_db->setQuery($query);
				$this->_db->query();
			}
		}
	}
	
	function TimeTracking(){
		$course_id = JRequest::getVar('course_id', 0);
		$user_id = JRequest::getVar('user_id', 0);
		$resource_type = JRequest::getVar('resource_type', 0);
		$resource_id = JRequest::getVar('resource_id', 0);
		$item_id = JRequest::getVar('item_id', 0);
		
		$start = JRequest::getVar('start', 0);
		$is_active = JRequest::getVar('is_active', 0);
		
		if(isset($resource_id) && $resource_id){
			$this->TimeTrackingData($course_id, $user_id, $is_active, $start, $resource_type, $resource_id, $item_id);
		} else {
			$this->TimeTrackingData($course_id, $user_id, $is_active, $start);
		}
	}
	
	function TimeTrackingData(&$course_id, &$user_id, &$is_active, &$start, &$resource_type=0, &$resource_id=0, &$item_id=0){
		if($course_id && $user_id){
			$current_time = $this->TimeTracking_currentTime();
			
			if($resource_id){
				$query = "SELECT *"
				. "\n FROM #__lms_time_tracking_resources"
				. "\n WHERE 1"
				. "\n AND course_id = '".$course_id."'"
				. "\n AND user_id = '".$user_id."'"
				. "\n AND resource_type = '".$resource_type."'"
				. "\n AND resource_id = '".$resource_id."'"
				. "\n AND item_id = '".$item_id."'"
				;
			} else {
				$query = "SELECT *"
				. "\n FROM #__lms_time_tracking"
				. "\n WHERE 1"
				. "\n AND course_id = '".$course_id."'"
				. "\n AND user_id = '".$user_id."'"
				;
			}
			$this->_db->setQuery($query);
			$time_tracking = $this->_db->loadObject();
			
			//echo '<pre>';
			//print_r($_REQUEST);
			//print_r($query);
			//print_r($time_tracking);
			//echo '</pre>';
			
			if(isset($start) && $start && (!isset($time_tracking->id) || !$time_tracking->id)){
				if($resource_id){
					$query = "INSERT INTO #__lms_time_tracking_resources"
					. "\n (id, course_id, user_id, resource_type, resource_id, item_id, time_spent, time_last_activity)"
					. "\n VALUES"
					. "\n ('', ".$course_id.", ".$user_id.", ".$resource_type.", ".$resource_id.", ".$item_id.", 0, ".$current_time.")"
					;
				} else {
					$query = "INSERT INTO #__lms_time_tracking"
					. "\n (id, course_id, user_id, time_spent, time_last_activity)"
					. "\n VALUES"
					. "\n ('', ".$course_id.", ".$user_id.", 0, ".$current_time.")"
					;
				}
				$this->_db->setQuery($query);
				$this->_db->query();
			}
			
			if(isset($start) && $start && $resource_id && $resource_type == 11){
				$query = "UPDATE #__lms_time_tracking_resources"
				. "\n SET"
				. "\n time_quest_in = '".$current_time."'"
				. "\n WHERE 1"
				. "\n AND course_id = '".$course_id."'"
				. "\n AND user_id = '".$user_id."'"
				. "\n AND resource_type = '".$resource_type."'"
				. "\n AND resource_id = '".$resource_id."'"
				. "\n AND item_id = '".$item_id."'"
				;
				$this->_db->setQuery($query);
				$this->_db->query();
			}
			
			$start_time = isset($time_tracking->time_last_activity) && $time_tracking->time_last_activity ? $time_tracking->time_last_activity : $current_time;
			$time_spent = isset($time_tracking->time_spent) && $time_tracking->time_spent ? $time_tracking->time_spent : 0;
			$time_spent_before = $time_spent;
			
			$delta_time_spent = $current_time - $start_time;
			
			if(isset($is_active)){
				$time_spent_after = 0;
			 	if($is_active && !$start){
			 		$time_spent = $time_spent + $delta_time_spent;
			 		$time_spent_after = $time_spent;
			 	}
				
				$step_time_msec = JRequest::getVar('step_time', 0);
				if(!$is_active && $step_time_msec){
					$step_time = floor($step_time_msec / 1000);
					$time_spent = $time_spent + $step_time;
					$time_spent_after = $time_spent;
				}
				
			 	if($resource_id){
			 		$query = "UPDATE #__lms_time_tracking_resources"
				 	. "\n SET"
				 	. "\n time_last_activity = '".$current_time."'"
				 	.($time_spent_after && $time_spent_before <= $time_spent_after ? "\n, time_spent = '".$time_spent."'" : '')
				 	. "\n WHERE 1"
				 	. "\n AND course_id = '".$course_id."'"
					. "\n AND user_id = '".$user_id."'"
					. "\n AND resource_type = '".$resource_type."'"
					. "\n AND resource_id = '".$resource_id."'"
					. "\n AND item_id = '".$item_id."'"
					;
			 	} else {
				 	$query = "UPDATE #__lms_time_tracking"
				 	. "\n SET"
				 	. "\n time_last_activity = '".$current_time."'"
				 	.($time_spent_after && $time_spent_before <= $time_spent_after ? "\n, time_spent = '".$time_spent."'" : '')
				 	. "\n WHERE 1"
				 	. "\n AND course_id = '".$course_id."'"
					. "\n AND user_id = '".$user_id."'"
					;
			 	}
				$this->_db->setQuery($query);
				$this->_db->query();
			}
		}
		
		$this->TimeTrakingViewStatistics($course_id, $user_id, $resource_type, $resource_id, $item_id);
		die;
	}
	
	function TimeTrakingViewStatistics(&$course_id, &$user_id, &$resource_type=0, &$resource_id=0, &$item_id=0){
		if($resource_id){
			$query = "SELECT time_spent"
			. "\n FROM #__lms_time_tracking_resources"
			. "\n WHERE 1"
			. "\n AND course_id = '".$course_id."'"
			. "\n AND user_id = '".$user_id."'"
			. "\n AND resource_type = '".$resource_type."'"
			. "\n AND resource_id = '".$resource_id."'"
			. "\n AND item_id = '".$item_id."'"
			;
		} else {
			$query = "SELECT time_spent"
			. "\n FROM #__lms_time_tracking"
			. "\n WHERE 1"
			. "\n AND course_id = '".$course_id."'"
			. "\n AND user_id = '".$user_id."'"
			;
		}
		$this->_db->setQuery($query);
		$time_tracking = $this->_db->loadObject();
		
		if(!isset($time_tracking->time_spent) || $time_tracking->time_spent <= 0){
			$time_tracking->time_spent = 0;
		}
		
		$this->prepareShowTimeSpent($time_tracking, $course_id, $user_id, $resource_type, $resource_id, $item_id);
		
		echo $this->TT_getTimeNormal($time_tracking->time_spent); //Online time:
	}
	
	function prepareShowTimeSpent(&$time_tracking, &$course_id, &$user_id, &$resource_type=0, &$resource_id=0, &$item_id=0){
		switch($resource_type){
			case '9':
				if($item_id){ //moding for mycontractorexam.com
					//$query = "SELECT cond_time, cond_time_sec"
					$query = "SELECT cond_time"
					. "\n FROM #__lms_learn_path_conds"
					. "\n WHERE 1"
					. "\n AND course_id = '".$course_id."'"
					. "\n AND lpath_id = '".$resource_id."'"
					. "\n AND step_id = '".$item_id."'"
					. "\n AND ref_step = 0"
					;
					$this->_db->setQuery($query);
					$cond_time_obj = $this->_db->loadObject();
					
					//print_r($cond_time_obj);
					//print_r($time_tracking);
					
					$temp_time_spent = 0;
					if(isset($cond_time_obj->cond_time)){
						$cond_time_min = $cond_time_obj->cond_time;
						//$cond_time_sec = $cond_time_min * 60 + $cond_time_obj->cond_time_sec;
						$cond_time_sec = $cond_time_min;
						if($time_tracking->time_spent < $cond_time_sec){
							$temp_time_spent = $cond_time_sec - $time_tracking->time_spent;
						}
					}
					$time_tracking->time_spent = $temp_time_spent;
				} //end moding for mycontractorexam.com
			break;
		
			case '11':
				if($item_id){
					$query = "SELECT a.*"
					. "\n FROM #__lms_quiz_t_question as a, #__lms_quiz_r_student_quiz as b"
					. "\n WHERE 1"
					. "\n AND a.course_id = '".$course_id."'"
					. "\n AND a.c_quiz_id = b.c_quiz_id"
					. "\n AND b.c_id = '".$resource_id."'"
					. "\n AND a.c_id = '".$item_id."'"
					;
					$this->_db->setQuery($query);
					$quest_obj = $this->_db->loadObject();
					
					if(isset($quest_obj) && $quest_obj->c_type){
						$quest_time_remaining = 0;
						if($quest_obj->c_type == 20 && $quest_obj->c_pool){
							$query = "SELECT a.*"
							. "\n FROM #__lms_quiz_t_question as a"
							. "\n WHERE 1"
							. "\n AND a.course_id = '".$course_id."'"
							. "\n AND a.c_id = '".$quest_obj->c_pool."'"
							;
							$this->_db->setQuery($query);
							$qp_quest_obj = $this->_db->loadObject();
							$quest_params = $qp_quest_obj->params;
						} else
						if($quest_obj->c_type == 21 && $quest_obj->c_pool_gqp){
							$query = "SELECT a.*"
							. "\n FROM #__lms_quiz_t_question as a"
							. "\n WHERE 1"
							. "\n AND a.c_id = '".$quest_obj->c_pool_gqp."'"
							;
							$this->_db->setQuery($query);
							$gqp_quest_obj = $this->_db->loadObject();
							$quest_params = $gqp_quest_obj->params;
						} else {
							$quest_params = $quest_obj->params;
						}
						$params = new JParameter($quest_params);
						$quest_time_remaining = $params->get('time_remaining', 0);
						if($quest_time_remaining){
							$temp_time_spent = 0;
							if($time_tracking->time_spent < $quest_time_remaining){
								$temp_time_spent = $quest_time_remaining - $time_tracking->time_spent;
							}
							$time_tracking->time_spent = $temp_time_spent;
						}
					}
					
					if(isset($time_tracking->time_spent) && !$time_tracking->time_spent){
						$query = "SELECT *"
						. "\n FROM #__lms_quiz_r_student_question"
						. "\n WHERE 1"
						. "\n AND c_stu_quiz_id = '".$resource_id."'"
						. "\n AND c_question_id = '".$item_id."'"
						;
						$this->_db->setQuery($query);
						$row_question = $this->_db->loadObject();
						
						if(isset($row_question) && $row_question->c_id){
							$query = "UPDATE #__lms_quiz_r_student_question"
							. "\n SET"
							. "\n c_correct = '1'"
							. "\n WHERE 1"
							. "\n AND c_stu_quiz_id = '".$resource_id."'"
							. "\n AND c_question_id = '".$item_id."'"
							;
							$this->_db->setQuery($query);
							$this->_db->query();
						} else {
							$query = "INSERT INTO #__lms_quiz_r_student_question (c_stu_quiz_id, c_question_id, c_score, c_attempts, c_correct)"
							. "\n VALUES('".$resource_id."', '".$item_id."', '0', '0', '1')";
							$this->_db->SetQuery($query);
							$this->_db->query();
						}
					}
				}
				
			break;
		}
	}
	
	
	
	function TT_getTimeNormal($sec){
		$d = $sec >= (24 * 60 * 60) ? floor($sec / (24 * 60 * 60)) : 0;
		$sec = $sec - $d * 24 * 60 * 60;
		$h = $sec >= (60 * 60) ? floor($sec / (60 * 60)) : 0;
		$sec = $sec - $h * 60 * 60;
		$m = $sec >= 60 ? floor($sec/60) : 0;
		$sec = $sec - $m * 60;
		$s = $sec;
		
		$time = '';
		$time .= $d ? str_pad($d, strlen($d), '0', STR_PAD_LEFT).' day ' : '';
		$time .= (($d && $h) || ($d && !$h) || (!$d && $h)) ? str_pad($h, 2, '0', STR_PAD_LEFT).':' : '';
		$time .= str_pad($m, 2, '0', STR_PAD_LEFT).':';
		$time .= str_pad($s, 2, '0', STR_PAD_LEFT);
		
		return $time;
	}
}
?>