<?php 
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

class JLMS_ClearCourseUserData{
	
	var $course_ids = null;
	var $user_id = null;
	
	function __construct(){
		require_once(_JOOMLMS_FRONT_HOME. "/includes/quiz/joomlaquiz.class.php");
	}
	
	function setCourseID($course_ids=''){
		$this->course_ids = $course_ids;
	}
	
	function setUserID($user_id){
		$this->user_id = $user_id;
	}
	
	function getCertificateUsers($course_ids, $user_id){
		$JLMS_DB = & JLMSFactory::getDB();
		
		if(!isset($user_id) || !$user_id){
			return false;
		}
		
		$query = "SELECT *"
		. "\n FROM #__lms_certificate_users"
		. "\n WHERE 1"
		.($course_ids ? "\n AND course_id IN (".$course_ids.")" : '')
		. "\n AND user_id = '".$user_id."'"
		;
		$JLMS_DB->setQuery($query);
		return $JLMS_DB->loadObjectList();
	}
	
	function getQuizResults($course_ids, $user_id, $quiz_id=null){
		$JLMS_DB = & JLMSFactory::getDB();
		
		if(!isset($course_ids) || !$course_ids || !isset($user_id) || !$user_id){
			return false;
		}
		
		$query = "SELECT *"
		. "\n FROM #__lms_quiz_results"
		. "\n WHERE 1"
		. "\n AND course_id IN (".$course_ids.")"
		. "\n AND user_id = '".$user_id."'"
		.(isset($quiz_id) && $quiz_id ? "\n AND quiz_id = '".$quiz_id."'" : '')
		. "\n AND user_passed = '1'"
		;
		$JLMS_DB->setQuery($query);
		return $JLMS_DB->loadObjectList();
	}
	
	function getCertificatePrints($course_ids, $user_id){
		$JLMS_DB = & JLMSFactory::getDB();
		
		if(!isset($user_id) || !$user_id){
			return false;
		}
		
		$query = "SELECT *"
		. "\n FROM #__lms_certificate_prints"
		. "\n WHERE 1"
		.($course_ids ? "\n AND course_id IN (".$course_ids.")" : '')
		. "\n AND user_id = '".$user_id."'"
		;
		$JLMS_DB->setQuery($query);
		return $JLMS_DB->loadObjectList();
	}
	
	function getQuizWithCertificate(){
		$JLMS_DB = & JLMSFactory::getDB();
		
		$query = "SELECT * FROM #__lms_quiz_t_quiz"
		. "\n WHERE 1"
		. "\n AND c_certificate > 0"
		. "\n AND published = 1"
		. "\n AND course_id IN (".$this->course_ids.")"
		;
		$JLMS_DB->setQuery($query);
		return $JLMS_DB->loadObjectList();
	}
	
	function getArhiveCertificatePrints(){
		$JLMS_DB = & JLMSFactory::getDB();
		
		$query = "SELECT a.*"
		. "\n, a.username as cur_username"
		. "\n, a.user_id as cur_user"
		. "\n, a.name as cur_name"
		. "\n, a.course_name as cur_course_name"
		. "\n, a.quiz_name as cur_quiz_name"
		. "\n, a.crtf_date as crt_date"
		. "\n, 1 as arhive"
		. "\n FROM #__lms_certificate_prints_arhive as a"
		. "\n WHERE 1"
		.($this->course_ids ? "\n AND a.course_id IN (".$this->course_ids.")" : '')
		.($this->user_id ? "\n AND a.user_id = '".$this->user_id."'" : '')
		;
		$JLMS_DB->setQuery($query);
		$certificate_prints_arhive = $JLMS_DB->loadObjectList();
		
		return $certificate_prints_arhive;
	}
	
	function checkCourseCompleted(){
		$JLMS_DB = & JLMSFactory::getDB();
		
		$certificate_users = $this->getCertificateUsers($this->course_ids, $this->user_id);
		
		if(isset($certificate_users) && count($certificate_users)){
			return true;
		} else {
			return false;
		}
	}
	
	function initializeClearCourseUserData($course_ids, $user_id){
		
		$this->setCourseID($course_ids);
		$this->setUserID($user_id);
		
		if($this->checkCourseCompleted()){
			
			$certificate_users = $this->getCertificateUsers($this->course_ids, $this->user_id);
			$list_quizs = $this->getQuizWithCertificate();
			$quiz_results = $this->getQuizResults($this->course_ids, $this->user_id);
			$certificate_prints = $this->getCertificatePrints($this->course_ids, $this->user_id);
			
			if(isset($certificate_users) && count($certificate_users)){
				if(!isset($certificate_prints) || !count($certificate_prints)){
					if(isset($list_quizs) && count($list_quizs)){
						foreach($list_quizs as $quiz){
							$this->generationCertificateQuiz($quiz->c_id);
						}
					}
					$this->generationCertificateCourse();
				}
				
				$certificate_prints = $this->getCertificatePrints($this->course_ids, $this->user_id);
				
					//echo '<pre>';
					//print_r($certificate_prints);
					//print_r($list_quizs);
					//print_r($quiz_results);
					//echo '</pre>';
					//die;
				
				//if(isset($certificate_prints) && count($certificate_prints) == (count($list_quizs) + 1)){
				if(isset($certificate_prints) && count($certificate_prints) == (count($quiz_results) + 1)){
					
					foreach($certificate_prints as $certificate_print){
						$this->moveCertificatePrint($certificate_print);
					}
					
					//echo '<pre>';
					//print_r($this);
					//echo '</pre>';
					//die;
					
					$this->clearCourseUserData();
				}
			}
		} else {
			
		}
	}
	
	function clearCourseUserData(){
		$JLMS_DB = & JLMSFactory::getDB();
		
		$course_ids = $this->course_ids;
		$group_id = JRequest::getVar('group_id', 0);
		$del_ids = array();
		$del_ids[] = $this->user_id;
		
		$del_ids_str = implode(',',$del_ids);
		
		$query = "DELETE FROM #__lms_certificate_users WHERE course_id IN (".$course_ids.") AND user_id IN ($del_ids_str)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$query = "DELETE FROM #__lms_chat_history WHERE course_id IN (".$course_ids.") AND user_id IN ($del_ids_str)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$query = "DELETE FROM #__lms_chat_users WHERE course_id IN (".$course_ids.") AND user_id IN ($del_ids_str)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$query = "SELECT distinct file_id FROM #__lms_dropbox WHERE course_id IN (".$course_ids.") AND ( owner_id IN ($del_ids_str) OR recv_id IN ($del_ids_str) )";
		$JLMS_DB->SetQuery( $query );
		$del_files = JLMSDatabaseHelper::LoadResultArray();
		$query = "DELETE FROM #__lms_dropbox WHERE course_id IN (".$course_ids.") AND ( owner_id IN ($del_ids_str) OR recv_id IN ($del_ids_str) )";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$files_del = JLMS_checkFiles( $course_id, $del_files );
		if (count($files_del)) {
			JLMS_deleteFiles($files_del);
		}
		$query = "DELETE FROM #__lms_gradebook WHERE course_id IN (".$course_ids.") AND user_id IN ($del_ids_str)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$query = "DELETE FROM #__lms_homework_results WHERE course_id IN (".$course_ids.") AND user_id IN ($del_ids_str)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$query = "SELECT id FROM #__lms_learn_path_results WHERE course_id IN (".$course_ids.") AND user_id IN ($del_ids_str)";
		$JLMS_DB->SetQuery( $query );
		$lp_res_ids = JLMSDatabaseHelper::LoadResultArray();
	
		$query = "DELETE FROM #__lms_learn_path_grades WHERE course_id IN (".$course_ids.") AND user_id IN ($del_ids_str)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
	
		if (count($lp_res_ids)) {
			$lpr_str = implode(',',$lp_res_ids);
			$query = "DELETE FROM #__lms_learn_path_results WHERE id IN ($lpr_str)";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			$query = "DELETE FROM #__lms_learn_path_step_results WHERE result_id IN ($lpr_str)";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			// 18.08.2007 (deleting of lp quiz results)
			$query = "DELETE FROM #__lms_learn_path_step_quiz_results WHERE result_id IN ($lpr_str)";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
		}
	
		//delete QUIZ results
		$query = "DELETE FROM #__lms_quiz_results WHERE course_id IN (".$course_ids.") AND user_id IN ($del_ids_str)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$query = "SELECT c_id FROM #__lms_quiz_t_quiz WHERE course_id IN (".$course_ids.")";
		$JLMS_DB->SetQuery( $query );
		$q_ids = JLMSDatabaseHelper::LoadResultArray();
		if (count($q_ids)) {
			$q_str = implode(',',$q_ids);
			$query = "SELECT c_id FROM #__lms_quiz_r_student_quiz WHERE c_quiz_id IN ($q_str) AND c_student_id IN ($del_ids_str)";
			$JLMS_DB->SetQuery( $query );
			$rsq_ids = JLMSDatabaseHelper::LoadResultArray();
			if (count($rsq_ids)) {
				$rsq_str = implode(',',$rsq_ids);
				$query = "SELECT c_id FROM #__lms_quiz_r_student_question WHERE c_stu_quiz_id IN ( $rsq_str )";
				$JLMS_DB->SetQuery( $query );
				$rsqq_ids = JLMSDatabaseHelper::LoadResultArray();
				if (count($rsqq_ids)) {
					$stu_cids = implode(',',$rsqq_ids);
					$query = "DELETE FROM #__lms_quiz_r_student_blank WHERE c_sq_id IN ( $stu_cids )";
					$JLMS_DB->setQuery( $query );
					$JLMS_DB->query();
					$query = "DELETE FROM #__lms_quiz_r_student_choice WHERE c_sq_id IN ( $stu_cids )";
					$JLMS_DB->setQuery( $query );
					$JLMS_DB->query();
					$query = "DELETE FROM #__lms_quiz_r_student_hotspot WHERE c_sq_id IN ( $stu_cids )";
					$JLMS_DB->setQuery( $query );
					$JLMS_DB->query();
					$query = "DELETE FROM #__lms_quiz_r_student_matching WHERE c_sq_id IN ( $stu_cids )";
					$JLMS_DB->setQuery( $query );
					$JLMS_DB->query();
					$query = "DELETE FROM #__lms_quiz_r_student_survey WHERE c_sq_id IN ( $stu_cids )";
					$JLMS_DB->setQuery( $query );
					$JLMS_DB->query();
					$query = "DELETE FROM #__lms_quiz_r_student_question WHERE c_id IN ( $stu_cids )";
					$JLMS_DB->setQuery( $query );
					$JLMS_DB->query();
				}
				$query = "DELETE FROM #__lms_quiz_r_student_quiz WHERE c_id IN ( $rsq_str )";
				$JLMS_DB->setQuery( $query );
				$JLMS_DB->query();
			}
		}
		// end of QUIZ
	
		//delete SCORMs tracking
		$query = "SELECT id FROM #__lms_scorm_packages WHERE course_id IN (".$course_ids.")";
		$JLMS_DB->SetQuery( $query );
		$sc_ids = JLMSDatabaseHelper::LoadResultArray();
		//old scorms (before JoomlaLMS 1.0.5)
		if (count($sc_ids)) {
			$sc_str = implode(',',$sc_ids);
			$query = "DELETE FROM #__lms_scorm_sco WHERE content_id IN ($sc_str) AND user_id IN ($del_ids_str)";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
		}
		//new scorms
		$query = "SELECT id FROM #__lms_n_scorm WHERE course_id IN (".$course_ids.")";
		$JLMS_DB->SetQuery( $query );
		$scn_ids = JLMSDatabaseHelper::LoadResultArray();
		if (count($scn_ids)) {
			$scn_str = implode(',',$scn_ids);
			$query = "DELETE FROM #__lms_n_scorm_scoes_track WHERE scormid IN ($scn_str) AND userid IN ($del_ids_str)";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
		}
		//end of SCORMs part
	
		//delete TRACKING records
		$query = "DELETE FROM #__lms_track_chat WHERE course_id IN (".$course_ids.") AND user_id IN ($del_ids_str)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$query = "DELETE FROM #__lms_track_hits WHERE course_id IN (".$course_ids.") AND user_id IN ($del_ids_str)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$query = "DELETE FROM #__lms_track_learnpath_stats WHERE course_id IN (".$course_ids.") AND user_id IN ($del_ids_str)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$query = "SELECT id FROM #__lms_documents WHERE course_id IN (".$course_ids.")";
		$JLMS_DB->SetQuery( $query );
		$doc_ids = JLMSDatabaseHelper::LoadResultArray();
		if (count($doc_ids)) {
			$d_str = implode(',',$doc_ids);
			$query = "DELETE FROM #__lms_track_downloads WHERE doc_id IN ($d_str) AND user_id IN ($del_ids_str)";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
		}
	}
	
	function moveCertificatePrint($certificate_print){
		$JLMS_DB = & JLMSFactory::getDB();
		
		
		
		if(isset($certificate_print) && $certificate_print->id){
			$query = "INSERT INTO #__lms_certificate_prints_arhive"
			. "\n (id, uniq_id, user_id, role_id, crtf_date, crtf_id, crtf_text, last_printed, name, username, course_id, course_name, quiz_id, quiz_name )"
			. "\n VALUES"
			. "\n ("
			. "\n '',"
			. "\n ".$JLMS_DB->Quote($certificate_print->uniq_id).","
			. "\n ".$JLMS_DB->Quote($certificate_print->user_id).","
			. "\n ".$JLMS_DB->Quote($certificate_print->role_id).","
			. "\n ".$JLMS_DB->Quote($certificate_print->crtf_date).","
			. "\n ".$JLMS_DB->Quote($certificate_print->crtf_id).","
			. "\n ".$JLMS_DB->Quote($certificate_print->crtf_text).","
			. "\n ".$JLMS_DB->Quote($certificate_print->last_printed).","
			. "\n ".$JLMS_DB->Quote($certificate_print->name).","
			. "\n ".$JLMS_DB->Quote($certificate_print->username).","
			. "\n ".$JLMS_DB->Quote($certificate_print->course_id).","
			. "\n ".$JLMS_DB->Quote($certificate_print->course_name).","
			. "\n ".$JLMS_DB->Quote($certificate_print->quiz_id).","
			. "\n ".$JLMS_DB->Quote($certificate_print->quiz_name).""
			. "\n )"
			;
			$JLMS_DB->setQuery($query);
			if($JLMS_DB->query()){
				$query = "DELETE FROM #__lms_certificate_prints WHERE id = '".$certificate_print->id."'";
				$JLMS_DB->setQuery($query);
				$JLMS_DB->query();
			}
		}
	}
	
	function generationCertificateQuiz($quiz_id=0){
		
		$juser = & JFactory::getUser($this->user_id);
		
		$JLMS_DB = & JLMSFactory::getDB();
		$JLMS_ACL = JLMSFactory::getACL();
		
		//if($quiz_id && $JLMS_ACL->GetRole(1)){
			
			$query = "SELECT id FROM #__lms_certificates"
			. "\n WHERE 1"
			. "\n AND course_id = '".$this->course_id."'"
			//. "\n AND crtf_type = '".$JLMS_ACL->GetRole(1)."'"
			. "\n AND crtf_type = '".$JLMS_ACL->defaultRole(1)."'"
			. "\n AND published = 1"
			;
			$JLMS_DB->SetQuery( $query );
			$certificate_id = $JLMS_DB->LoadResult();
			
			return $this->generationCertificate($certificate_id, $this->course_id, $quiz_id);
		//}
	}
	
	function generationCertificateCourse(){
		$juser = & JFactory::getUser($this->user_id);
		
		$JLMS_DB = & JLMSFactory::getDB();
		$JLMS_ACL = JLMSFactory::getACL();
		
		//var_dump($JLMS_ACL->GetRole(1));
		
		//if($JLMS_ACL->GetRole(1)){
			
			$query = "SELECT id FROM #__lms_certificates"
			. "\n WHERE 1"
			. "\n AND course_id = '".$this->course_id."'"
			. "\n AND crtf_type = 1"
			. "\n AND published = 1"
			;
			$JLMS_DB->SetQuery( $query );
			$certificate_id = $JLMS_DB->LoadResult();
			
			return $this->generationCertificate($certificate_id, $this->course_id);
		//}
	}
	
	function generationCertificate($certificate_id, $course_id, $quiz_id=0){
		
		$juser = & JFactory::getUser($this->user_id);
		
		$JLMS_DB = & JLMSFactory::getDB();
		$JLMS_ACL = JLMSFactory::getACL();
		
		//var_dump($JLMS_ACL->UserRole($JLMS_DB, $this->user_id, 1));
		
		$do_s = true;
		//$certificate_role = $JLMS_ACL->GetRole(1);
		$certificate_role = $JLMS_ACL->defaultRole(1);
		
		//var_dump($certificate_role);
		//die;
		
		if ($certificate_role) {
			$query = "SELECT a.*, b.course_name"
			. "\n FROM #__lms_certificates as a, #__lms_courses as b"
			. "\n WHERE 1"
			. "\n AND a.course_id = '".$course_id."'"
			. "\n AND a.course_id = b.id"
			. "\n AND a.parent_id = '".$certificate_id."'"
			. "\n AND a.crtf_type = '".$certificate_role."'"
			;
			$JLMS_DB->SetQuery( $query );
			$certificates = $JLMS_DB->loadObjectList();
			if (count($certificates) == 1) {
				if ($certificates[0]->file_id) {
					$do_s = false;
				} else {
					$query = "SELECT file_id"
					. "\n FROM #__lms_certificates as a"
					. "\n WHERE 1"
					. "\n AND a.id = '".$certificate_id."'"
					. "\n AND a.course_id = '".$course_id."'"
					. "\n AND a.parent_id = 0"
					;
					$JLMS_DB->SetQuery( $query );
					$certificates[0]->file_id = $JLMS_DB->LoadResult();
					if ($certificates[0]->file_id) {
						$do_s = false;
					}
				}
			}
		}
		if ($do_s) {
			$query = "SELECT a.*, b.course_name"
			. "\n FROM #__lms_certificates as a, #__lms_courses as b"
			. "\n WHERE 1"
			. "\n AND a.id = '".$certificate_id."'"
			. "\n AND a.course_id = '".$course_id."'"
			. "\n AND a.course_id = b.id"
			. "\n AND a.parent_id = 0"
			;
			$JLMS_DB->SetQuery( $query );
			$certificates = $JLMS_DB->loadObjectList();
		}
		$certificate_text = $certificates[0]->crtf_text;
		
		
		$query = "SELECT * FROM #__lms_certificate_prints"
		. "\n WHERE 1"
		. "\n AND user_id = '".$this->user_id."'"
		. "\n AND role_id = '".$JLMS_ACL->GetRole(1)."'"
		. "\n AND course_id = '".$this->course_id."'"
		. "\n AND crtf_id = '".$certificate_id."'"
		.($quiz_id ? "\n AND quiz_id = $quiz_id" : '')
		;
		$JLMS_DB->SetQuery( $query );
		$certificate_print = $JLMS_DB->LoadObject();
		$is_exist = 0;
		if(isset($certificate_print) && $certificate_print->id){
			$is_exist = $certificate_print->id;
		}
		
		$course = new mos_Joomla_LMS_Course( $JLMS_DB );
		$course->load( $course_id );
		$course_name = $course->course_name;
		
		$query = "SELECT id, crt_date FROM #__lms_certificate_users"
		. "\n WHERE 1"
		. "\n AND course_id = '".$this->course_id."'"
		. "\n AND user_id = '".$this->user_id."'"
		. "\n AND crt_option = 1"
		;
		$JLMS_DB->SetQuery( $query );
		$row = $JLMS_DB->LoadObject();
		
		$certificate_date = strtotime($row->crt_date);
		
		$stu_quiz_id = 0;
		$quiz_name = '';
		if($quiz_id){
			$quiz = new mos_JoomQuiz_Quiz( $JLMS_DB );
			$quiz->load( $quiz_id );
			$quiz_name = $quiz->c_title;
			
			$query = "SELECT c_id"
			. "\n FROM #__lms_quiz_r_student_quiz"
			. "\n WHERE 1"
			. "\n AND c_quiz_id = '".$quiz_id."'"
			. "\n AND c_student_id = '".$this->user_id."'"
			. "\n ORDER BY c_id"
			;
			$JLMS_DB->setQuery($query);
			$stu_quiz_id = $JLMS_DB->loadResult();
			
			if($stu_quiz_id){
				$query = "SELECT sq.c_passed, sq.c_student_id, sq.c_total_score, sq.c_total_time, sq.unique_id, sq.c_date_time as completion_datetime,"
				. "\n qtq.c_full_score, qtq.c_title, qtq.c_certificate, qtq.course_id, qtq.c_id as quiz_id, qtq.course_id"
				. "\n FROM #__lms_quiz_r_student_quiz AS sq, #__lms_quiz_t_quiz AS qtq"
				. "\n WHERE sq.c_id = '".$stu_quiz_id."' and qtq.c_id = sq.c_quiz_id";
				$JLMS_DB->setQuery($query);
				$stu_quiz = $JLMS_DB->LoadObject();
				
				$certificate_date = strtotime($stu_quiz->completion_datetime) + $stu_quiz->c_total_time;
			}
		}
		
		$ucode = md5(uniqid(rand(), true));
		$ucode = substr($ucode, 0, 10);
		
		//echo '<pre>';
		//var_dump($is_exist);
		//echo '</pre>';
		//die;
		
		if (!$is_exist) {
			$query = "INSERT INTO #__lms_certificate_prints"
			. "\n (uniq_id, user_id, role_id, crtf_date, crtf_id, crtf_text, last_printed, name, username, course_id, course_name, quiz_id, quiz_name )"
			. "\n VALUES"
			. "\n ("
			. "\n ".$JLMS_DB->Quote($ucode).", ".$juser->id.", ".$certificate_role.",".$JLMS_DB->Quote(date('Y-m-d H:i:s', $certificate_date)).", $certificate_id, ".$JLMS_DB->Quote($certificate_text).","
			. "\n ".$JLMS_DB->Quote(gmdate('Y-m-d H:i:s')).", ".$JLMS_DB->Quote($juser->name).", ".$JLMS_DB->Quote($juser->username).","
			. "\n $course_id, ".$JLMS_DB->Quote($course_name).", $quiz_id, ".$JLMS_DB->Quote($quiz_name).")"
			;
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			$ex_crtf_id = $JLMS_DB->insertid();
		} else {
			$query = "UPDATE #__lms_certificate_prints SET last_printed = ".$JLMS_DB->Quote(gmdate('Y-m-d H:i:s')).","
			. "\n crtf_text = ".$JLMS_DB->Quote($certificate_text).", course_name = ".$JLMS_DB->Quote($course_name).","
			. "\n quiz_name = ".$JLMS_DB->Quote($quiz_name)
			. "\n WHERE id = $is_exist";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			$ex_crtf_id = $is_exist;
		}
		return $ex_crtf_id;
	}
	
}