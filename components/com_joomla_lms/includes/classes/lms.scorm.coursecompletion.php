<?php 
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

class JLMS_SCORMCourseComplete{
	
	function getInitFix($user_id=0, $course_id=0){ //not use
		$JLMS_CONFIG = JLMSFactory::getConfig();
		
		if(!$course_id){
			$course_id = $JLMS_CONFIG->get('course_id') ? $JLMS_CONFIG->get('course_id') : JRequest::getVar('course_id', 0);
		}
		
		if(!$user_id){
			$user_id = & JFactory::getUser()->id;
		}
		
		$check = $this->checkCompleteCourse($user_id, $course_id);
		$certificate_check = $this->getCertificateUserId($user_id, $course_id);
		if($check === true){
			//if(!$certificate_check){
			//	if($this->addCertificateUser($user_id, $course_id)){
			//		$this->notificationCompleteCourse($user_id, $course_id);
			//	}
			//}
		} else {
			if($certificate_check){
				$this->cleanCertificateUser($user_id, $course_id); //off //fix
			}
		}
	}
	
	function getInit($user_id=0, $course_id=0){
		$JLMS_CONFIG = JLMSFactory::getConfig();
		
		if(!$course_id){
			$course_id = $JLMS_CONFIG->get('course_id') ? $JLMS_CONFIG->get('course_id') : JRequest::getVar('course_id', 0);
		}
		
		if(!$user_id){
			$user_id = & JFactory::getUser()->id;
		}
		
		$check = $this->checkCompleteCourse($user_id, $course_id);
		
		//echo '<pre>';
		//print_r($check);
		//var_dump($check);
		//echo '</pre>';
		//die;
		
		$certificate_check = $this->getCertificateUserId($user_id, $course_id);
		if($check === true){			
			if(!$certificate_check){			
				if($this->addCertificateUser($user_id, $course_id)){
					$this->notificationCompleteCourse($user_id, $course_id);
				}
			}			
		} else {
			if($certificate_check){
				//$this->cleanCertificateUser($user_id, $course_id); //off
			}
		}
		
	}
	
	function notificationCompleteCourse($user_id=0, $course_id=0){	
		$JLMS_DB = JLMSFactory::getDB();
		$JLMS_CONFIG = JLMSFactory::getConfig();
		$Itemid = $JLMS_CONFIG->getItemid();
		
		$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
		
		$e_course = new stdClass();
		$e_course->course_alias = '';
		$e_course->course_name = '';			

		$query = "SELECT course_name, name_alias FROM #__lms_courses WHERE id = '".$course_id."'";
		$JLMS_DB->setQuery( $query );
		$e_course = $JLMS_DB->loadObject();

		$e_user = new stdClass();
		$e_user->name = '';
		$e_user->email = '';
		$e_user->username = '';

		$query = "SELECT email, name, username FROM #__users WHERE id = '".$user_id."'";
		$JLMS_DB->setQuery( $query );
		$e_user = $JLMS_DB->loadObject();

		$e_params['user_id'] = $user_id;
		$e_params['course_id'] = $course_id;					
		$e_params['markers']['{email}'] = $e_user->email;	
		$e_params['markers']['{name}'] = $e_user->name;										
		$e_params['markers']['{username}'] = $e_user->username;
		$e_params['markers']['{coursename}'] = $e_course->course_name;//( $e_course->course_alias )?$e_course->course_alias:$e_course->course_name;

		$e_params['markers']['{courselink}'] = JLMSEmailRoute("index.php?option=com_joomla_lms&Itemid=$Itemid&task=details_course&id=$course_id");

		$e_params['markers_nohtml']['{courselink}'] = $e_params['markers']['{courselink}'];
		$e_params['markers']['{courselink}'] = '<a href="'.$e_params['markers']['{courselink}'].'">'.$e_params['markers']['{courselink}'].'</a>';
								
		$e_params['markers']['{lmslink}'] = JLMSEmailRoute("index.php?option=com_joomla_lms&Itemid=$Itemid");

		$e_params['markers_nohtml']['{lmslink}'] = $e_params['markers']['{lmslink}'];					
		$e_params['markers']['{lmslink}'] = '<a href="'.$e_params['markers']['{lmslink}'].'">'.$e_params['markers']['{lmslink}'].'</a>';

		$e_params['action_name'] = 'OnUserCompletesTheCourse';

		$_JLMS_PLUGINS->loadBotGroup('emails');
		
		$plugin_result_array = $_JLMS_PLUGINS->trigger('OnUserCompletesTheCourse', array (& $e_params));
	}
	
	function getCertificateUserId($user_id=0, $course_id=0){
		$JLMS_DB = & JLMSFactory::getDB();
		
		$query = "SELECT id"
		. "\n FROM #__lms_certificate_users"
		. "\n WHERE 1"
		. "\n AND course_id = '".$course_id."'"
		. "\n AND user_id = '".$user_id."'"
		. "\n AND crt_option = 1"
		;
		$JLMS_DB->setQuery( $query );
		$certificate_user_id = $JLMS_DB->loadResult();
		
		return $certificate_user_id;
	}
	
	function addCertificateUser($user_id=0, $course_id=0){
		$JLMS_DB = & JLMSFactory::getDB();
		
		$query = "INSERT INTO #__lms_certificate_users (course_id, user_id, crt_option, crt_date)"
		. "\n VALUES ('".$course_id."', '".$user_id."', '1', '".gmdate('Y-m-d H:i:s')."')";
		$JLMS_DB->setQuery( $query );
		if($JLMS_DB->query()){
			return true;
		} else {			
			return false;
		}
	}
	
	function cleanCertificateUser($user_id=0, $course_id=0){
		$JLMS_DB = & JLMSFactory::getDB();
		
		$query = "DELETE FROM #__lms_certificate_users"
		. "\n WHERE 1"
		. "\n AND course_id = '".$course_id."'"
		. "\n AND user_id = '".$user_id."'"
		. "\n AND crt_option = 1"
		;
		$JLMS_DB->setQuery( $query );
		if($JLMS_DB->query()){
			return true;
		} else {
			return false;
		}
	}
	
	function getItemsNeedCompleteCourse($course_id=0){
		$JLMS_DB = & JLMSFactory::getDB();
		
		$rows = array();
		
		$query = "SELECT learn_path_id"
		. "\n FROM #__lms_gradebook_lpaths"
		. "\n WHERE 1"
		. "\n AND course_id = '".$course_id."'"
		;
		$JLMS_DB->setQuery($query);
		$rows = JLMSDatabaseHelper::LoadResultArray();
		
		return $rows;
	}
	
	function getItemsNeedCompleteCourseRaw($course_id=0){
		$JLMS_DB = & JLMSFactory::getDB();
		
		$itemsneedcompletecourse_ids = $this->getItemsNeedCompleteCourse($course_id);
		
		$rows = array();
		if(is_array($itemsneedcompletecourse_ids) && count($itemsneedcompletecourse_ids)){
			$query = "SELECT *"
			. "\n FROM #__lms_learn_paths"
			. "\n WHERE 1"
			. "\n AND id IN (".implode(',', $itemsneedcompletecourse_ids).")"
			. "\n AND course_id = '".$course_id."'"
			;
			$JLMS_DB->setQuery($query);
			$rows = $JLMS_DB->loadObjectList();
		}
		
		return $rows;
	}
	
	function getSCORMsDataRaw($user_id=0, $course_id=0){
		$JLMS_DB = & JLMSFactory::getDB();
		
		$rows = array();
		
		$query = "SELECT a.*"
		. "\n FROM #__lms_n_scorm_scoes_track as a"
		. "\n, #__lms_n_scorm as b"
		. "\n WHERE 1"
		. "\n AND a.scormid = b.id"
		. "\n AND a.userid = '".$user_id."'"
		. "\n AND b.course_id = '".$course_id."'"
		;
		$JLMS_DB->setQuery($query);
		$rows = $JLMS_DB->loadObjectList();
		
		return $rows;
	}
	
	function getLpathsCompeleteIds($user_id=0, $course_id=0){
		$JLMS_DB = & JLMSFactory::getDB();
		
		$query = "SELECT lpath_id"
		. "\n FROM #__lms_learn_path_grades"
		. "\n WHERE 1"
		. "\n AND course_id = '".$course_id."'"
		. "\n AND user_id = '".$user_id."'"
		. "\n AND user_status = '1'"
		;
		$JLMS_DB->setQuery( $query );
		$lpath_ids = JLMSDatabaseHelper::LoadResultArray();
		
		//echo '<pre>';
		//print_r($query);
		//echo '<br />';
		//print_r($lpath_ids);
		//echo '</pre>';
		//die;
		
		return $lpath_ids;
	}
	
	function getSCORMsCompleteIds($user_id=0, $course_id=0){
		
		$rows = $this->getSCORMsDataRaw($user_id, $course_id);
		
		$scorm_ids = array();
		if(isset($rows) && count($rows)){
			foreach($rows as $row){
				if(isset($row->element) && in_array($row->element, array('cmi.completion_status', 'cmi.core.completion_status', 'cmi.lesson_status', 'cmi.core.lesson_status')) && in_array($row->value, array('completed', 'passed'))){
					$scorm_ids[] = $row->scormid;
				}
			}
		}
		
		return $scorm_ids;
	}
	
	function getLpathsSCORMCompleteIds($user_id=0, $course_id=0){
		$JLMS_DB = & JLMSFactory::getDB();
		
		$scorm_ids = $this->getSCORMsCompleteIds($user_id, $course_id);
		
		$lpath_ids = array();
		
		if(isset($scorm_ids) && is_array($scorm_ids) && count($scorm_ids)){
			$query = "SELECT *"
			. "\n FROM #__lms_learn_paths"
			. "\n WHERE 1"
			. "\n AND (lp_type = 1 OR lp_type = 2)"
			. "\n AND course_id = '".$course_id."'"
			. "\n AND item_id IN (".implode(',', $scorm_ids).")"
			;
			$JLMS_DB->setQuery( $query );
			$lpath_ids = JLMSDatabaseHelper::LoadResultArray();
		}
		
		return $lpath_ids;
	}
	
	function getResultCompleteIds($user_id=0, $course_id=0){
		$getLpathsCompeleteIds = $this->getLpathsCompeleteIds($user_id, $course_id);
		$getLpathsSCORMCompleteIds = $this->getLpathsSCORMCompleteIds($user_id, $course_id);
		
		//echo '<pre>';
		//print_r($getLpathsCompeleteIds);
		//print_r($getLpathsSCORMCompleteIds);
		//echo '</pre>';
		//die;
		
		$results = array();
		if(is_array($getLpathsCompeleteIds) && is_array($getLpathsSCORMCompleteIds)){
			$results = array_merge($getLpathsCompeleteIds, $getLpathsSCORMCompleteIds);
		}
		
		return $results;
	}
	
	function checkCompleteCourse($user_id=0, $course_id=0){
		$getItemsNeedCompleteCourse = $this->getItemsNeedCompleteCourse($course_id);
		$getResultCompleteIds = $this->getResultCompleteIds($user_id, $course_id);
		
		//echo '<pre>';
		//print_r($getItemsNeedCompleteCourse);
		//print_r($getResultCompleteIds);
		//echo '</pre>';
		//die;
		
		$check = false;
		if(is_array($getItemsNeedCompleteCourse) && count($getItemsNeedCompleteCourse)){
			$check = true;
			foreach($getItemsNeedCompleteCourse as $itemneed){
				if(!in_array($itemneed, $getResultCompleteIds)){
					$check = false;
					break;
				}
			}
		}
		
		return $check;
	}
}