<?php 
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

class JLMS_SCORMCurrentStep{
	
	var $user_id = null;
	var $scorm_id = null;
	var $array_status_current = null;
	var $new_current_step = null;
	
	//function __construct() {
	//	
	//}
	
	function getInit($user_id, $scorm_id){
		
		$view_suspend_data = null;
		
		if($user_id && $scorm_id){
			
			$array_xml_suspend_data = $this->getXMLSuspendArray($scorm_id);
		
			$scorm_complete = $this->getSCORM_Complete($scorm_id);
			
			//if($_SERVER['REMOTE_ADDR'] == '86.57.158.98'  && ($scorm_id == 329 || $scorm_id == 330 || $scorm_id == 331) ) {
				//echo '<pre>';
				//echo '$scorm_complete';
				//echo '<br />';
				//var_dump($scorm_complete);
				//echo '</pre>';
			//}
			
			$string_complete_suspend_data = '';
			if(isset($scorm_complete->userid) && isset($scorm_complete->scormid)){
				$string_complete_suspend_data = $this->getSuspendData($scorm_complete->userid, $scorm_complete->scormid);
			}
			
			$obj_complete_scorm = $this->prepareStringSuspendData($string_complete_suspend_data);

			//den [begin]
			//if($_SERVER['REMOTE_ADDR'] == '86.57.158.98'  && ($scorm_id == 329 || $scorm_id == 330 || $scorm_id == 331) ) {
			//	echo '<pre>';
			//	print_r($string_complete_suspend_data);
			//	echo '</pre>';
			//	echo '<pre>';
			//	print_r($obj_complete_scorm);
			//	echo '</pre>';
			//}
			//den [end]
			
			//if($scorm_id == 3635){
			//	echo '<pre>';
			//	print_r($obj_complete_scorm);
			//	echo '</pre>';
			//	//die;
			//}
			
			$array_complete_suspend_data = array();
			$status_complete_current_step = null;
			if(isset($obj_complete_scorm->structure)){
				$array_complete_suspend_data = $obj_complete_scorm->structure;
				
				//echo '<pre>';
				//print_r($array_complete_suspend_data);
				//echo '</pre>';
				
				$status_complete_current_step = $this->getStatusCurrentStep($array_complete_suspend_data);
				
				//echo '<pre>';
				//print_r($status_complete_current_step);
				//echo '</pre>';
			}
			$this->array_status_current[] = $status_complete_current_step;
			
			$scorm_incomplete = $this->getSCORM_InComplete($scorm_id);
			
			//if($_SERVER['REMOTE_ADDR'] == '86.57.158.98'){
			//	echo '<pre>';
			//	print_r($scorm_incomplete);
			//	echo '</pre>';
			//}
			
			$string_incomplete_suspend_data = '';
			if(isset($scorm_incomplete->userid) && isset($scorm_incomplete->scormid)){
				$string_incomplete_suspend_data = $this->getSuspendData($scorm_incomplete->userid, $scorm_incomplete->scormid);
				
				//if($scorm_id == 3635){
				//	echo '<pre>';
				//	var_dump($string_incomplete_suspend_data);
				//	echo '</pre>';
				//}
			}
			
			$obj_incomplete_scorm = $this->prepareStringSuspendData($string_incomplete_suspend_data);
			
			//if($scorm_id == 3635){
			//	echo '<pre>';
			//	print_r($obj_incomplete_scorm);
			//	echo '</pre>';
			//}
			
			$array_incomplete_suspend_data = array();
			$status_incomplete_current_step = null;
			if(isset($obj_incomplete_scorm->structure)){
				$array_incomplete_suspend_data = $obj_incomplete_scorm->structure;
				
				//echo '<pre>';
				//print_r($array_incomplete_suspend_data);
				//echo '</pre>';
				
				$status_incomplete_current_step = $this->getStatusCurrentStep($array_incomplete_suspend_data);
				
				//echo '<pre>';
				//print_r($status_complete_current_step);
				//echo '</pre>';
			}
			//$this->array_status_current[] = $status_incomplete_current_step;
			
			$string_suspend_data = $this->getSuspendData($user_id, $scorm_id);
			
			$obj_scorm = $this->prepareStringSuspendData($string_suspend_data);
			
			$array_suspend_data = array();
			$status_current_step = null;
			if(isset($obj_scorm->structure)){
				$array_suspend_data = $obj_scorm->structure;
				
				//echo '<pre>';
				//print_r($array_suspend_data);
				//echo '</pre>';
				
				$status_current_step = $this->getStatusCurrentStep($array_suspend_data);
			}
			//$this->array_status_current[] = $status_current_step;
			
			//echo '<pre>';
			//print_r($obj_scorm);
			//echo '</pre>';
			
			//echo '<pre>';
			//print_r('$status_complete_current_step='.$status_complete_current_step);
			//echo '<br />';
			//print_r('$status_incomplete_current_step='.$status_incomplete_current_step);
			//echo '<br />';
			//echo '########################################################################';
			//echo '<br />';
			//echo '<br />';
			//echo '<br />';
			//echo '</pre>';
			
			$status_step = null;
			if($status_complete_current_step == $status_incomplete_current_step){
				$status_step = $status_complete_current_step;
			} else {
				$status_step = $status_complete_current_step;
			}
			
			
		//den [begin]
		//if($_SERVER['REMOTE_ADDR'] == '86.57.158.98' && ($scorm_id == 329 || $scorm_id == 330 || $scorm_id == 331) ) {
		//	echo '<pre>';
		//	print_r($user_id);echo '<br />';
		//	print_r($scorm_id);echo '<br />';
		//	print_r($array_xml_suspend_data);echo '<br />';
		//	print_r($array_complete_suspend_data);echo '<br />';
		//	print_r($status_step);
		//	echo '</pre>';
		//}
		//den [end]
			
			//echo '<pre>';
			//print_r('$status_step='.$status_step);
			//echo '</pre>';
			
			if(
			   isset($array_xml_suspend_data) && is_array($array_xml_suspend_data) &&
			   isset($array_complete_suspend_data) && is_array($array_complete_suspend_data) &&
			   isset($status_step) && $status_step
			){
				if(isset($obj_scorm) && isset($obj_scorm->structure) && is_array($obj_scorm->structure)){
					$view_suspend_data = $this->viewSuspendData($obj_scorm);
				} else {
					$obj = new stdClass();
					$obj->viewed[] = 1;
					$obj->lastviewedslide = 1;
					$obj->structure = array();
					foreach($array_xml_suspend_data as $num=>$item){
						if(!$num){
							$element = $status_step;
						} else {
							$element = $item;
						}
						$obj->structure[] = $element;
					}
					
					//echo '<pre>';
					//print_r($obj);
					//echo '</pre>';
					
					$view_suspend_data = $this->viewSuspendData($obj);
				}
			}
		}
		
		return $view_suspend_data;
	}
	
	function saveCurrentStep(){
		
		$JLMS_DB = & JLMSFactory::getDB();
		
		$scs_mod = JRequest::getVar('scs_mod', 0);
		$user_id = JRequest::getVar('filter_stu', 0);
		$scorm_id = JRequest::getVar('scs_control_id', 0);
		
		if($scs_mod){
			
			if($user_id && $scorm_id){
				
				$new_current_step = $this->getNewCurrentStep();
				
				$this->new_current_step = $new_current_step;
				
				$array_xml_suspend_data = $this->getXMLSuspendArray($scorm_id);
				
				$scorm_complete = $this->getSCORM_Complete($scorm_id);
				$string_complete_suspend_data = '';
				if(isset($scorm_complete->userid) && isset($scorm_complete->scormid)){
					$string_complete_suspend_data = $this->getSuspendData($scorm_complete->userid, $scorm_complete->scormid);
				}
				$obj_complete_scorm = $this->prepareStringSuspendData($string_complete_suspend_data);
				$array_complete_suspend_data = array();
				$status_complete_current_step = null;
				if(isset($obj_complete_scorm->structure)){
					$array_complete_suspend_data = $obj_complete_scorm->structure;
					$status_complete_current_step = $this->getStatusCurrentStep($array_complete_suspend_data);
				}
				
				$scorm_incomplete = $this->getSCORM_InComplete($scorm_id);
				$string_incomplete_suspend_data = '';
				if(isset($scorm_incomplete->userid) && isset($scorm_incomplete->scormid)){
					$string_incomplete_suspend_data = $this->getSuspendData($scorm_incomplete->userid, $scorm_incomplete->scormid);
				}
				$obj_incomplete_scorm = $this->prepareStringSuspendData($string_incomplete_suspend_data);
				$array_incomplete_suspend_data = array();
				$status_incomplete_current_step = null;
				if(isset($obj_incomplete_scorm->structure)){
					$array_incomplete_suspend_data = $obj_incomplete_scorm->structure;
					$status_incomplete_current_step = $this->getStatusCurrentStep($array_incomplete_suspend_data);
				}
				
				$string_suspend_data = $this->getSuspendData($user_id, $scorm_id);
				$obj_scorm = $this->prepareStringSuspendData($string_suspend_data);
				$array_suspend_data = array();
				$status_current_step = null;
				if(isset($obj_scorm->structure)){
					$array_suspend_data = $obj_scorm->structure;
					$status_current_step = $this->getStatusCurrentStep($array_suspend_data);
				}
				
				$status_step = null;
				//if($status_complete_current_step == $status_incomplete_current_step){
				//	$status_step = $status_complete_current_step;
				//} else {
				//	if(count($array_xml_suspend_data) == $new_current_step){
				//		$status_step = $status_complete_current_step;
				//	} elseif(count($array_xml_suspend_data) > $new_current_step) {
				//		$status_step = $status_complete_current_step;
				//	}
				//}
				
				if(!isset($status_incomplete_current_step) || $status_incomplete_current_step){
					$status_incomplete_current_step = $status_complete_current_step;
				}
				
				if($new_current_step == count($array_xml_suspend_data)){
					$status_step = $status_complete_current_step;
				} else {
					$status_step = $status_incomplete_current_step;
				}
				
				$obj = new stdClass();
				$obj->viewed = null;
				$obj->lastviewedslide = null;
				$obj->structure = null;
				
				$viewed = array();
				$lastviewedslide = $new_current_step;
				$structure = array();
				foreach($array_xml_suspend_data as $num=>$xml_item){
					$n = $num + 1;
					
					if($n <= $new_current_step){
						$viewed[] = $n;
					}
					
					if($n < $new_current_step){
						$structure[] = $array_complete_suspend_data[$num];
					}
					if($n == $new_current_step){
						$structure[] = $status_step;
					}
					if($n > $new_current_step){
						$structure[] = $array_xml_suspend_data[$num];
					}
				}
				
				$obj->viewed = $viewed;
				$obj->lastviewedslide = $lastviewedslide;
				$obj->structure = $structure;
				
				//echo '<pre>';
				//print_r('$new_current_step='.$new_current_step);
				//echo '<br />';
				//print_r('$status_complete_current_step='.$status_complete_current_step);
				//echo '<br />';
				//print_r('$status_incomplete_current_step='.$status_incomplete_current_step);
				//echo '<br />';
				//print_r($obj);
				//echo '<br />';
				//echo '###################################';
				//echo '</pre>';
				
				$new_suspend_data = $this->prepareNewStringSuspendData($string_complete_suspend_data, $obj, $new_current_step);
			
			}
			
			//echo '<pre>';
			//echo '#############################################################################################################';
			//print_r($new_suspend_data);
			//echo '<br />';
			//print_r($obj);
			//echo '<br />';
			//print_r($array_xml_suspend_data);
			//echo '<br />';
			//print_r($array_complete_suspend_data);
			//echo '<br />';
			//print_r($array_incomplete_suspend_data);
			//echo '<br />';
			//echo '#############################################################################################################';
			//echo '</pre>';
			//die;
			
			$query = "SELECT COUNT(*)"
			. "\n FROM #__lms_n_scorm_scoes_track"
			. "\n WHERE 1"
			. "\n AND userid = '".$user_id."'"
			. "\n AND scormid = '".$scorm_id."'"
			. "\n AND element = 'cmi.suspend_data'"
			;
			$JLMS_DB->setQuery($query);
			$exist = $JLMS_DB->loadResult();
			
			if($exist){
				$query = "UPDATE #__lms_n_scorm_scoes_track"
				. "\n SET value = '".$new_suspend_data."'"
				. "\n WHERE 1"
				. "\n AND userid = '".$user_id."'"
				. "\n AND scormid = '".$scorm_id."'"
				. "\n AND element = 'cmi.suspend_data'"
				;
				$JLMS_DB->setQuery($query);
				$JLMS_DB->query();
			} else {
				$query = "SELECT *"
				. "\n FROM #__lms_n_scorm_scoes_track"
				. "\n WHERE 1"
				. "\n AND userid = '".$scorm_complete->userid."'"
				. "\n AND scormid = '".$scorm_complete->scormid."'"
				//. "\n AND element = 'cmi.suspend_data'"
				;
				$JLMS_DB->setQuery($query);
				$rows = $JLMS_DB->loadObjectList();
				
				//echo '<pre>';
				//print_r($rows);
				//echo '</pre>';
				
				if(isset($rows) && count($rows)){
					foreach($rows as $row){
						$row->userid = $user_id;
						if(isset($row->element) && $row->element == 'cmi.suspend_data'){
							$row->value = $new_suspend_data;
						}
						
						if(isset($row->element) && in_array($row->element, array('cmi.completion_status', 'cmi.core.completion_status', 'cmi.lesson_status', 'cmi.core.lesson_status'))){
							$row->value = 'incomplete';
						}
						
						$query = "INSERT INTO #__lms_n_scorm_scoes_track"
						. "\n (id, userid, scormid, scoid, attempt, element, value, timemodified)"
						. "\n VALUES"
						. "\n ('', ".$row->userid.", ".$row->scormid.", ".$row->scoid.", ".$row->attempt.", '".$row->element."', '".$row->value."', ".$row->timemodified.")"
						;
						$JLMS_DB->setQuery($query);
						$JLMS_DB->query();
						
						//echo "<br />";
						//echo $query;
						//echo "<br />";
						
						
					}
				}
				
				
			}
			
			//echo $exist;
			//echo $query;
			//die;
			
			//echo '<pre>';
			//print_r($suspend_data);
			//print_r($obj_suspend_data);
			//print_r($new_suspend_data);
			//echo '</pre>';
			
			//die;
		
		}
		
	}
	
	function getStatusCurrentStep($array_suspend_data){
		
		$status_current_step = null;
		
		if(isset($array_suspend_data) && is_array($array_suspend_data)){
			
			//echo '<pre>#############';
			//print_r($this->new_current_step);
			//print_r($array_suspend_data);
			//echo '#############</pre>';
			
			//$max = 0;
			//$q_maxs = 0;
			//
			//foreach($array_suspend_data as $n=>$item){
			//	
			//	//echo '$item='.$item;
			//	//echo '<br />';
			//	
			//	if($item > $max){
			//		$max = $item;
			//		$q_maxs = 0;
			//	}
			//	if($max == $item){
			//		$q_maxs++;
			//	}
			//}
			
			//echo '$q_maxs='.$q_maxs;
			//echo '<br />';
			
			$status = null;
			
			foreach($array_suspend_data as $n=>$item){
				if(!in_array($item, array(0,1))){
					$status = $item;
				} else {
					break;
				}
			}
			$status_current_step = $status;
			
			
			//echo 'status='.$status;
			//echo '<br />';
			//
			//if($q_maxs > 1 && isset($array_suspend_data[(count($array_suspend_data) - 1)])){
			//	$status_current_step = !in_array($array_suspend_data[(count($array_suspend_data) - 1)], array(0,1)) ? $array_suspend_data[(count($array_suspend_data) - 1)] : $max;
			//	
			//	echo '1';
			//	echo '<br />';
			//} else {
			//	$status_current_step = $max;
			//	
			//	echo '2';
			//	echo '<br />';
			//}
			//
			//echo '$status_current_step='.$status_current_step;
			//echo '<br />';
			//echo '_________________________________________________________________________________________';
			
		}
		return $status_current_step;
	}
	
	function getDomObj($scorm_id){
		$xml = $this->getXML_Presentation($scorm_id);
		
		$domObj = new xmlToArrayParser($xml);
		
		//echo '<pre>';
		//print_r($xml);
		//echo '<br />';
		//print_r($domObj);
		//echo '</pre>';
		
		return $domObj;
	}
	
	function getTitleSlides($scorm_id){
		
		$title_slides = array();
		
		$domObj = $this->getDomObj($scorm_id);
		
		$slides = array();
		if(isset($domObj->array['Presentation']['Slides']['Slide'])){
			$slides = $domObj->array['Presentation']['Slides']['Slide'];
		}
		
		foreach($slides as $slide){
			if(isset($slide['Title']) && $slide['Title']){
				$title_slides[] = $slide['Title'];
			}
		}
		//den [begin]
		//if($_SERVER['REMOTE_ADDR'] == '86.57.158.98' && ($scorm_id == 329 || $scorm_id == 330 || $scorm_id == 331)) {
		//	echo '<pre>';
		//	print_r($title_slides);
		//	echo '</pre>';
		//}
		//den [end]
		
		return $title_slides;
	}
	
	function getXMLSuspendArray($scorm_id){
		
		$xml_suspend_data = array();
		
		$domObj = $this->getDomObj($scorm_id);
		
		//echo '<pre>';
		//print_r($domObj);
		//echo '</pre>';
		
		$slides = array();
		if(isset($domObj->array['Presentation']['Slides']['Slide'])){
			$slides = $domObj->array['Presentation']['Slides']['Slide'];
		}
		
		foreach($slides as $slide){
			
			if($slide['Hidden'] == 'false'){
				$xml_suspend_data[] = 1;
			} else
			if($slide['Hidden'] == 'true'){
				$xml_suspend_data[] = 0;
			}
		}
		
		return $xml_suspend_data;
	}
	
	function getXML_Presentation($scorm_id){
		
		$buffer = null;
		
		$JLMS_DB = & JLMSFactory::getDB();
		$JLMS_CONFIG = & JLMSFactory::getDB();
		
		$query = "SELECT folder_srv_name"
		. "\n FROM #__lms_n_scorm as a, #__lms_scorm_packages as b"
		. "\n WHERE 1"
		. "\n AND a.scorm_package = b.id"
		. "\n AND a.id = '".$scorm_id."'"
		;
		$JLMS_DB->setQuery($query);
		$folder_srv_name = $JLMS_DB->loadResult();
		
		//echo '<pre>';
		//print_r($folder_srv_name);
		//echo '</pre>';
		
		if(isset($folder_srv_name) && $folder_srv_name){
			$path_to_xml = _JOOMLMS_SCORM_FOLDER_PATH . DS . $folder_srv_name . DS . 'data' . DS . 'presentation.xml';
			
			//echo '<pre>';
			//print_r($path_to_xml);
			//echo '</pre>';
			
			if(isset($path_to_xml) && file_exists($path_to_xml)){
				$handle = fopen($path_to_xml, "r");
				$buffer = fread($handle, filesize($path_to_xml));
				fclose($handle);
			}
		}
		
		return $buffer;
	}
	
	function prepareNewStringSuspendData($suspend_data, $obj_suspend_data, $new_current_step){
		$new_suspend_data = false;
		
		if(preg_match('/viewed=([^|]*)\|lastviewedslide=(\d+)\|\d+\#\d+\#.*#\,([^#]*)\#/', $suspend_data, $out)){
			
			$new_suspend_data = $suspend_data;
			
			if(isset($out[1])){
				$new_suspend_data = str_replace($out[1], implode(',', $obj_suspend_data->viewed), $new_suspend_data);
			}
			if(isset($out[2])){
				$new_suspend_data = str_replace('lastviewedslide='.intval($out[2]), 'lastviewedslide='.$new_current_step, $new_suspend_data);
			}
			if(isset($out[3])){
				$new_suspend_data = str_replace($out[3], implode(',', $obj_suspend_data->structure), $new_suspend_data);
			}
		}
		
		//echo '<pre>';
		//var_dump($suspend_data);
		//var_dump($out);
		//var_dump($new_suspend_data);
		//var_dump($structure);
		//echo '</pre>';
		
		return $new_suspend_data;
	}
	
	function getNewCurrentStep(){
		$new_current_step = false;
		
		$scs_mod = JRequest::getVar('scs_mod', 0);
		if($scs_mod){
			$scs_control_id = JRequest::getVar('scs_control_id', 0);
			if($scs_control_id){
				$scs_control = JRequest::getVar('scorm_control_'.$scs_control_id, 0);
				if($scs_control){
					$new_current_step = $scs_control;
				}
			}
		}
		return $new_current_step;
	}
	
	function getSCORM_Complete($scorm_id, $exeption_user_ids=array()){
		$this->scorm_id = $scorm_id;
		$JLMS_DB = & JLMSFactory::getDB();
		
		//if($_SERVER['REMOTE_ADDR'] == '86.57.158.98'){
		//	echo '<pre>';
		//	var_dump($exeption_user_ids);
		//	echo '</pre>';
		//}
		
		$query = "SELECT *"
		. "\n FROM #__lms_n_scorm_scoes_track"
		. "\n WHERE 1"
		. "\n AND scormid = '".$scorm_id."'"
		.(is_array($exeption_user_ids) && count($exeption_user_ids) ? "\n AND userid NOT IN (".implode(',', $exeption_user_ids).")" : '')
		. "\n AND"
		. "\n ("
		. "\n element = 'cmi.completion_status'"
		. "\n OR"
		. "\n element = 'cmi.core.completion_status'"
		. "\n OR"
		. "\n element = 'cmi.lesson_status'"
		. "\n OR"
		. "\n element = 'cmi.core.lesson_status'"
		. "\n )"
		. "\n AND"
		. "\n ("
		. "\n value = 'completed'"
		. "\n OR"
		. "\n value = 'passed'"
		. "\n )"
		;
		$JLMS_DB->setQuery($query);
		$scorms = $JLMS_DB->loadObjectList();
		
		//if($_SERVER['REMOTE_ADDR'] == '86.57.158.98'  && ($scorm_id == 329 || $scorm_id == 330 || $scorm_id == 331) ) {
		//	echo '<pre>';
		//	echo $query;
		//	echo '<br />';
		//	print_r($scorms);
		//	echo '</pre>';
		//}
		
		if(isset($scorms) && count($scorms)){
			$for_stop = 0;
			for($i=0;$i<count($scorms);$i++){
				if(isset($scorms[$i]) && $scorms[$i] && $scorms[$i]->userid){
					$scorm = $scorms[$i];
					
					$string_suspend_data = $this->getSuspendData($scorm->userid, $scorm->scormid);
					
					//echo '<pre>';
					//print_r($string_suspend_data);
					//echo '</pre>';
					
					if($string_suspend_data && strlen($string_suspend_data)){ // && $this->getCheckStringSuspend($string_suspend_data)){
						$for_stop = 1;
						break;
					}
				}
			}
			if($for_stop){
				return $scorm;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	//function getSCORM_Complete($scorm_id, $exeption_user_ids=array()){
	//	$this->scorm_id = $scorm_id;
	//	$JLMS_DB = & JLMSFactory::getDB();
	//	
	//	//if($_SERVER['REMOTE_ADDR'] == '86.57.158.98'){
	//	//	echo '<pre>';
	//	//	var_dump($exeption_user_ids);
	//	//	echo '</pre>';
	//	//}
	//	
	//	$query = "SELECT *"
	//	. "\n FROM #__lms_n_scorm_scoes_track"
	//	. "\n WHERE 1"
	//	. "\n AND scormid = '".$scorm_id."'"
	//	.(is_array($exeption_user_ids) && count($exeption_user_ids) ? "\n AND userid NOT IN (".implode(',', $exeption_user_ids).")" : '')
	//	. "\n AND"
	//	. "\n ("
	//	. "\n element = 'cmi.completion_status'"
	//	. "\n OR"
	//	. "\n element = 'cmi.core.completion_status'"
	//	. "\n OR"
	//	. "\n element = 'cmi.lesson_status'"
	//	. "\n OR"
	//	. "\n element = 'cmi.core.lesson_status'"
	//	. "\n )"
	//	. "\n AND"
	//	. "\n ("
	//	. "\n value = 'completed'"
	//	. "\n OR"
	//	. "\n value = 'passed'"
	//	. "\n )"
	//	;
	//	
	//	//echo $query;
	//	
	//	$JLMS_DB->setQuery($query);
	//	$scorm = $JLMS_DB->loadObject();
	//	
	//	//if($_SERVER['REMOTE_ADDR'] == '86.57.158.98' && $scorm_id == 1233){
	//		echo '<pre>';
	//		echo $query;
	//		echo '<br />';
	//		print_r($scorm);
	//		echo '</pre>';
	//	//}
	//	
	//	if(isset($scorm) && $scorm && $scorm->userid){
	//		
	//		$string_suspend_data = $this->getSuspendData($scorm->userid, $scorm->scormid);
	//		
	//		//if($_SERVER['REMOTE_ADDR'] == '86.57.158.98' && $scorm_id == 1233){
	//			echo '<pre>';
	//			echo '<br />';
	//			var_dump($string_suspend_data);
	//			echo '</pre>';
	//		//}
	//		
	//		if(!$string_suspend_data){
	//			if(!in_array(intval($scorm->userid), $exeption_user_ids)){
	//				$exeption_user_ids[] = intval($scorm->userid);
	//				
	//				//if($_SERVER['REMOTE_ADDR'] == '86.57.158.98' && $scorm_id == 1233){
	//				//	echo 'restart';
	//				//}
	//				return $this->getSCORM_Complete($scorm_id, $exeption_user_ids);
	//			}
	//		} else {
	//			//if($_SERVER['REMOTE_ADDR'] == '86.57.158.98' && $scorm_id == 1233){
	//			//	echo '<br />';
	//			//	echo 'return';
	//			//	echo '<br />';
	//			//	echo '<pre>';
	//			//	print_r($scorm);
	//			//	echo '</pre>';
	//			//}
	//			
	//			return $scorm;
	//		}
	//	} else {
	//		return false;
	//	}
	//}
	
	function getSCORM_InComplete($scorm_id){
		$this->scorm_id = $scorm_id;
		$JLMS_DB = & JLMSFactory::getDB();
		
		$query = "SELECT *"
		. "\n FROM #__lms_n_scorm_scoes_track"
		. "\n WHERE 1"
		. "\n AND scormid = '".$scorm_id."'"
		//. "\n AND element = 'cmi.suspend_data'"
		. "\n AND"
		. "\n ("
		. "\n element = 'cmi.completion_status'"
		. "\n OR"
		. "\n element = 'cmi.core.completion_status'"
		. "\n OR"
		. "\n element = 'cmi.lesson_status'"
		. "\n OR"
		. "\n element = 'cmi.core.lesson_status'"
		. "\n )"
		. "\n AND"
		. "\n ("
		. "\n value = 'incomplete'"
		. "\n OR"
		. "\n value = 'failed'"
		. "\n )"
		;
		
		//echo $query;
		
		$JLMS_DB->setQuery($query);
		if($scorm = $JLMS_DB->loadObject()){
			return $scorm;
		} else {
			return false;
		}
	}
	
	function getSCORMComplete($scorms){
		
		$scorm = new stdClass();
		
		if(isset($scorms) && is_array($scorms) && count($scorms)){
			foreach($scorms as $item){
				if(isset($row->element) && in_array($row->element, array('cmi.completion_status', 'cmi.core.completion_status', 'cmi.lesson_status', 'cmi.core.lesson_status')) && in_array($row->value, array('completed', 'passed'))){
					$scorm_ids[] = $row->scormid;
				}
			}
		}
	}
	
	function getSuspendData($user_id, $scorm_id){
		
		if(isset($user_id) && isset($scorm_id)){
			$this->user_id = $user_id;
			$this->scorm_id = $scorm_id;
			
			$JLMS_DB = & JLMSFactory::getDB();
			
			$query = "SELECT value"
			. "\n FROM #__lms_n_scorm_scoes_track"
			. "\n WHERE 1"
			.($user_id ? "\n AND userid = '".$user_id."'" : '')
			. "\n AND scormid = '".$scorm_id."'"
			. "\n AND element = 'cmi.suspend_data'"
			;
			$JLMS_DB->setQuery($query);
			
			//if($_SERVER['REMOTE_ADDR'] == '86.57.158.98' && ($scorm_id == 329 || $scorm_id == 330 || $scorm_id == 331) ) {
				//echo '<pre>';
				//echo 'getSuspendData';
				//echo '<br />';
				//print_r($query);
				//echo '<br />';
				////print_r($JLMS_DB->loadObjectList());
				//echo '</pre>';
			//}
			
			if($suspend_data = $JLMS_DB->loadResult()){
				return $suspend_data;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	function prepareStringSuspendData($string_suspend_data){
		if(preg_match('/viewed=([^|]*)\|lastviewedslide=(\d+)\|\d+\#\d+\#.*#\,([^#]*)\#/', $string_suspend_data, $out)){
			if(isset($out) && isset($out[0]) && isset($out[3])){
				
				$viewed = explode(',', $out[1]);
				$lastviewedslide = $out[2];
				$structure = explode(',', $out[3]);
				
				$obj = new stdClass();
				$obj->viewed = $viewed;
				$obj->lastviewedslide = $lastviewedslide;
				$obj->structure = $structure;
				
				return $obj;
			}
		} else
		if(preg_match('/viewed=([^|]*)\|lastviewedslide=(\d+)\|\d+\#\d+\#.*#\,([^#]*)\#/', $string_suspend_data, $out)){
			
		}
		return false;
	}
	
	function viewSuspendData($obj){
		
		$array_xml_suspend_data = $this->getXMLSuspendArray($this->scorm_id);
		$title_slides = $this->getTitleSlides($this->scorm_id);
		
		$this->array_status_current = array_unique($this->array_status_current);
		
		//echo '<pre>#############';
		//print_r($_POST);
		//print_r($title_slides);
		//print_r($obj);
		//print_r($array_xml_suspend_data);
		//print_r($this->array_status_current);
		//echo '#############</pre>';
		
		$list = array();
		if(isset($obj) && $obj && is_array($obj->structure)){
			if(count($obj->structure)){
				$selected = null;
				$num = 1;
				
				foreach($obj->structure as $n=>$item){
					$value = $n + 1;
					if(isset($array_xml_suspend_data[$n]) && $array_xml_suspend_data[$n]){
						$list[$n]->value = $value;
						$list[$n]->text = $num . '. '.$title_slides[$n];
						if(in_array($item, $this->array_status_current)){
							$selected = $value;
						}
						$num++;
					}
				}
				if($selected == null){
					$i = $obj->lastviewedslide - 1;
					while($i >= 0 && $array_xml_suspend_data[$i] < 1){
						$i--;					
					}
					$selected = $i + 1;
				}
				
				$style = 'style="width: 95%;"';
				$javascript = 'onchange="document.adminForm.view.value=\'\';document.adminForm.scs_mod.value=1;document.adminForm.scs_control_id.value='.$this->scorm_id.';document.adminForm.submit();"';
				return JHTML::_('select.genericlist', $list, 'scorm_control_'.$this->scorm_id, ' '.$style.' '.$javascript, 'value', 'text', $selected);
			}
		}
		return false;
	}
	
	function convertingStatus($item){
		$status = 'n/a';
		$item = intval($item);
		if(in_array($item, $this->getState('current'))){
			$status = 'current';
		} else
		if(in_array($item, $this->getState('shown in full'))){
			$status = 'shown in full';
		} else
		if(in_array($item, $this->getState('scrolled'))){
			$status = 'scrolled';
		} else
		if(in_array($item, $this->getState('did not show'))){
			$status = 'did not show';
		}
		return $status;
	}
	
	function getState($type_state){
		$states = array();
		if(isset($type_state)){
			switch($type_state){
				case 'current':
					$states = array();
					for($i=10;$i<=20;$i++){
						$states[] = $i;
					}
					for($i=25;$i<=30;$i++){
						$states[] = $i;
					}
				break;
				case 'shown in full':
					$states = array();
					for($i=6;$i<=9;$i++){
						$states[] = $i;
					}
					for($i=20;$i<=25;$i++){
						$states[] = $i;
					}
				break;
				case 'scrolled':
					$states = array();
					for($i=2;$i<=5;$i++){
						$states[] = $i;
					}
				break;
				case 'did not show':
					$states = array();
					for($i=0;$i<=1;$i++){
						$states[] = $i;
					}
				break;
			}
		}
		return $states;
	}
	
	function getCheckStringSuspend($string_suspend_data){
		$result = false;
		if(preg_match('/viewed=([^|]*)\|lastviewedslide=(\d+)\|\d+\#\d+\#.*#\,([^#]*)\#/', $string_suspend_data, $out)){
			if(isset($out) && isset($out[0]) && isset($out[3])){
				$result = true;
			}
		}
		return $result;
	}
}

class xmlToArrayParser { 
  /** The array created by the parser can be assigned to any variable: $anyVarArr = $domObj->array.*/ 
  public  $array = array(); 
  public  $parse_error = false; 
  private $parser; 
  private $pointer; 
  
  /** Constructor: $domObj = new xmlToArrayParser($xml); */ 
  public function __construct($xml) { 
    $this->pointer =& $this->array; 
    $this->parser = xml_parser_create("UTF-8"); 
    xml_set_object($this->parser, $this); 
    xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, false); 
    xml_set_element_handler($this->parser, "tag_open", "tag_close"); 
    xml_set_character_data_handler($this->parser, "cdata"); 
    $this->parse_error = xml_parse($this->parser, ltrim($xml))? false : true; 
  } 
  
  /** Free the parser. */ 
  public function __destruct() { xml_parser_free($this->parser);} 

  /** Get the xml error if an an error in the xml file occured during parsing. */ 
  public function get_xml_error() { 
    if($this->parse_error) { 
      $errCode = xml_get_error_code ($this->parser); 
      $thisError =  "Error Code [". $errCode ."] \"<strong style='color:red;'>" . xml_error_string($errCode)."</strong>\", 
                            at char ".xml_get_current_column_number($this->parser) . " 
                            on line ".xml_get_current_line_number($this->parser).""; 
    }else $thisError = $this->parse_error; 
    return $thisError; 
  } 
  
  private function tag_open($parser, $tag, $attributes) { 
    $this->convert_to_array($tag, 'attrib'); 
    $idx=$this->convert_to_array($tag, 'cdata'); 
    if(isset($idx)) { 
      $this->pointer[$tag][$idx] = Array('@idx' => $idx,'@parent' => &$this->pointer); 
      $this->pointer =& $this->pointer[$tag][$idx]; 
    }else { 
      $this->pointer[$tag] = Array('@parent' => &$this->pointer); 
      $this->pointer =& $this->pointer[$tag]; 
    } 
    if (!empty($attributes)) { $this->pointer['attrib'] = $attributes; } 
  } 

  /** Adds the current elements content to the current pointer[cdata] array. */ 
  private function cdata($parser, $cdata) { $this->pointer['cdata'] = trim($cdata); } 

  private function tag_close($parser, $tag) { 
    $current = & $this->pointer; 
    if(isset($this->pointer['@idx'])) {unset($current['@idx']);} 
    
    $this->pointer = & $this->pointer['@parent']; 
    unset($current['@parent']); 
    
    if(isset($current['cdata']) && count($current) == 1) { $current = $current['cdata'];} 
    else if(empty($current['cdata'])) {unset($current['cdata']);} 
  } 
  
  /** Converts a single element item into array(element[0]) if a second element of the same name is encountered. */ 
  private function convert_to_array($tag, $item) { 
    if(isset($this->pointer[$tag][$item])) { 
      $content = $this->pointer[$tag]; 
      $this->pointer[$tag] = array((0) => $content); 
      $idx = 1; 
    }else if (isset($this->pointer[$tag])) { 
      $idx = count($this->pointer[$tag]); 
      if(!isset($this->pointer[$tag][0])) { 
        foreach ($this->pointer[$tag] as $key => $value) { 
            unset($this->pointer[$tag][$key]); 
            $this->pointer[$tag][0][$key] = $value; 
    }}}else $idx = null; 
    return $idx; 
  } 
} 