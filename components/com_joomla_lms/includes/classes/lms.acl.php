<?php 
/**
* includes/classes/lms.user_agent.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class JLMS_ACL extends JObject {

	var $_cache_course_names = array();

	var $_user_id = 0;
	var $_course_id = 0;
	var $_main_role = 0;
	var $_main_role_type = 0;
	var $_switch_role = 0;
	var $_switch_role_type = 0;
	var $_role = 0;
	var $_role_type = 0;

	var $_temp_role = 0;
	var $_temp_role_type = 0;

	var $_prepared = false;
	var $_cache = array();
	var $_roles = array();
	var $_roles_full = array();
	var $_system_roles = array();
	var $_system_user_roles_cache = array();
	var $_levels = array();
	var $_courses_teach = array();
	var $_courses_learn = array();
	var $_courses_future = array();
	var $_courses_staff = array();
	var $_courses_enrol_time = null;
	var $_staff_learners = array();
	
	var $_roles_assignments = array();
	
	var $_my_course_categories_full = array();
	var $_my_course_categories = array();
	var $_my_groups_assigned = array();
	var $_my_groups_user = array();
	
	var $_my_courses_roles = array();
	
	var $populated_systemroles = null;
	var $populated_temproletype = null;
	var $populated_mygroupsassigned = null;
	var $populated_mygroupuser = null;
	var $populated_mycategories = null;
	var $populated_mycourses = null;

	function JLMS_ACL($user_id) {
		$this->_user_id = $user_id;

		//Do we need to populate user ACL info???
		$this->PopulateSystemRoles();
		$temp_role_type = $this->getTempRoleType($user_id);
		$temp_role = $this->getTempRole($user_id);
		//if($temp_role_type == 4 || $temp_role_type == 2 ) { // lms admin role + teacher role
		$this->_main_role = $temp_role;
		$this->_main_role_type = $temp_role_type;
		$this->_role = $this->_main_role;
		$this->_role_type = $this->_main_role_type;
		if ($this->_role_type == 4) {
			$this->_levels[] = 4;
			$this->_levels[] = 2;
		} elseif ($this->_role_type == 2) {
			$this->_levels[] = 2;
		}
		$this->LoadRolePermissions(true);
		$this->LoadRolesAssignments();
		//$this->PopulateMyCategories();//not necessary here
		$this->PopulateMyCourses();/// populate object arrays with learner and teacher courses
		$this->_prepared = true;//TODO: temporary for compatibility with other functions of this object
		$this->CacheToTitles();
	}

	function GetRole($level = 0) {
		if (true){//}$this->_prepared) {
			if ($level) {
				if ($this->_role_type == $level) {
					return intval($this->_role);
				}
			} else {
				return intval($this->_role);
			}
		}
		return null;
	}

	function GetRoleType() {
		if (true) {//}$this->_prepared) {
			return intval($this->_role_type);
		}
		return null;
	}

	function UserRole( &$db, $user_id, $level = 0) {
		$role = 0;
		if ($level == 1) {
			$query= "SELECT role_id FROM #__lms_users_in_groups WHERE user_id = $user_id AND course_id = $this->_course_id";
			$db->SetQuery($query);
			$role = $db->LoadResult();
			if (!$role) {
				$role = 0;
			}
		}
		return $role;
	}

	function UserSystemRole( &$db, $user_id) {
		$role = 0;
		if ($user_id) {
			if (isset($this->_system_user_roles_cache[$user_id])) {
				return $this->_system_user_roles_cache[$user_id];
			}
			$query= "SELECT lms_usertype_id FROM #__lms_users WHERE user_id = $user_id";
			$db->SetQuery($query);
			$role = $db->LoadResult();
			if ($role) {
				$role_found = false;
				foreach ($this->_system_roles as $sys_role) {
					if ($sys_role->id == $role) {
						if ($sys_role->roletype_id != 4) {
							$role = 0;
						}
						$role_found = true;
						break;
					}
				}
				if (!$role_found) {
					$role = 0;
				}
			}
			if (!$role && $this->_course_id) {
				$query= "SELECT role_id FROM #__lms_user_courses WHERE user_id = $user_id AND course_id = $this->_course_id";
				$db->SetQuery($query);
				$role = $db->LoadResult();
			}
			$this->_system_user_roles_cache[$user_id] = $role;
		}
		return $role;
	}

	function isAdmin() {
		if (in_array(4, $this->_levels)) return true;
		return false;
	}

	function isTeacher() {
		if (in_array(2, $this->_levels)) return true;
		return false;
	}

	function isCourseTeacher() {
		if ($this->_course_id) {
			if ($this->_role_type == 4 || $this->_role_type == 2) {
				return true;
			}
		}
		return false;
	}

	function isStaff() {
//		if (in_array(3, $this->_levels)) return true;
		if (/*$this->_role == 6 && */$this->_role_type == 3) return true;
		return false;
	}

	function GetSwitchedRole() {
		$JLMS_SESSION = JLMSFactory::getSession();
		$this->_switch_role = $JLMS_SESSION->get('switch_usertype');
	}

	function ClearSwitchedRole() {
		$JLMS_SESSION = JLMSFactory::getSession();
		$this->_switch_role = 0;
		$this->_switch_role_type = 0;
		$JLMS_SESSION->clear('switch_usertype');

	
		$JLMS_SESSION->clear('jlms_user_course');
		$JLMS_SESSION->clear('jlms_user_type', 6);

	}

	function GetSystemRoles($level, $assigned_only = false) {
		$roles = array();
		foreach ($this->_system_roles as $sr) {
			if ($sr->roletype_id == $level) {
				if ($assigned_only) {
					if (isset($this->_roles_assignments) && is_array($this->_roles_assignments) && in_array($sr->id, $this->_roles_assignments)) {
						$roles[] = $sr;
					}
				} else {
					$roles[] = $sr;
				}
			}
		}
		return $roles;
	}

	function GetSystemRolesIds($level, $assigned_only = false) {
		$roles = array();
		foreach ($this->_system_roles as $sr) {
			if ($sr->roletype_id == $level) {
				if ($assigned_only) {
					if (isset($this->_roles_assignments) && is_array($this->_roles_assignments) && in_array($sr->id, $this->_roles_assignments)) {
						$roles[] = $sr->id;
					}
				} else {
					$roles[] = $sr->id;
				}
			}
		}
		return $roles;
	}

	function GetDefaultSystemRole($level) {
		$default_role_id = 0;
		$first_not_found = true;
		foreach ($this->_system_roles as $sr) {
			if ($sr->roletype_id == $level && $sr->default_role) {
				$default_role_id = $sr->id; break;
			} elseif ($sr->roletype_id == $level && $first_not_found) {
				$default_role_id = $sr->id;
				$first_not_found = false;
			}
		}
		return $default_role_id;
	}

	function GetTypeofRole($role) {
		$type = 0;
		foreach ($this->_system_roles as $sr) {
			if ($sr->id == $role) {
				$type = $sr->roletype_id;
				break;
			}
		}
		return $type;
	}
	
	function getCoursesEnrolTime( $course_id ) 
	{
		if( isset($this->_courses_enrol_time[$course_id])) 
			return $this->_courses_enrol_time[$course_id];
		else
			return false; 	
	}
	
	function PopulateSystemRoles() {
		if (is_null($this->populated_systemroles)) {
			$dbo = JFactory::getDbo();
			$query = "SELECT id, roletype_id, lms_usertype, default_role FROM #__lms_usertypes WHERE roletype_id <> 0 ORDER BY roletype_id, lms_usertype";
			$dbo->SetQuery( $query );
			$roles = $dbo->LoadObjectList();
			
			//Max - ordering roles
			$order = array(4,2,5,1,3);
			$tmp_roles = array();
			foreach($order as $x){
				foreach($roles as $role){
					if($role->roletype_id == $x){
						$tmp_roles[] = $role;
					}
				}
			}
			if(isset($tmp_roles) && count($tmp_roles)){
				$system_roles = array();
				$system_roles = $tmp_roles;
			}
			//Max - ordering roles
			
			$this->_system_roles = $system_roles;
			
			$this->populated_systemroles = true;
		}
	}

	function populateTempRoleType($user_id) {
		if (is_null($this->populated_temproletype)) {
			$dbo = JFactory::getDbo();
			$query = "SELECT u.lms_usertype_id, t.roletype_id FROM #__lms_users as u, #__lms_usertypes as t"
			 . "\n WHERE u.user_id = $user_id AND u.lms_usertype_id = t.id" // AND t.roletype_id <> 3"
			 . "\n ORDER BY t.roletype_id DESC LIMIT 0,1";
			$dbo->SetQuery( $query );
			$temp_role_type = 0;
			$temp_role = 0;
			$ur = $dbo->LoadObject();
			if (is_object($ur) && isset($ur->roletype_id)){
				$temp_role_type = $ur->roletype_id;
				$temp_role = $ur->lms_usertype_id;
			}
			$this->_temp_role = $temp_role;
			$this->_temp_role_type = $temp_role_type;
			$this->populated_temproletype = true;
		}
	}

	function getTempRole($user_id) {
		$this->populateTempRoleType($user_id);
		return $this->_temp_role;
	}

	function getTempRoleType($user_id) {
		$this->populateTempRoleType($user_id);
		return $this->_temp_role_type;
	}

	function CacheToTitles() {
		if (count($this->_cache_course_names)) {
			$rows = array();
			foreach ($this->_cache_course_names as $course_id => $course_name) {
				$row = new stdClass();
				$row->id = $course_id;
				$row->course_name = $course_name;
				$rows[] = $row;
			}
			$lms_titles_cache = & JLMSFactory::getTitles();
			$lms_titles_cache->setArray('courses', $rows, 'id', 'course_name');
		}
		if (count($this->_my_course_categories_full)) {
			$rows = array();
			foreach ($this->_my_course_categories_full as $level => $categories) {
				foreach ($categories as $category) {
					$row = new stdClass();
					$row->id = $category->id;
					$row->c_category = $category->c_category;
					$rows[] = $row;
				}
			}
			$lms_titles_cache = & JLMSFactory::getTitles();
			$lms_titles_cache->setArray('categories', $rows, 'id', 'c_category');
		}
	}

	function populateMyGroupsAssigned() {
		if (is_null($this->populated_mygroupsassigned)) {
			$user_id = $this->_user_id;
			$db = & JLMSFactory::getDB();
			$query = "SELECT group_id FROM #__lms_user_assign_groups WHERE user_id = $user_id";
			$db->SetQuery($query);

            $dbHelper = new JLMSDatabaseHelper();
			$this->_my_groups_assigned = $dbHelper->loadResultArray();;
			$this->populated_mygroupsassigned = true;
		}
	}

	function getMyGroupsAssigned() {
		$this->populateMyGroupsAssigned();
		return $this->_my_groups_assigned;
	}

	function populateMyGroupsUser() {
		if (is_null($this->populated_mygroupuser)) {
			$user_id = $this->_user_id;
			$db = JFactory::getDbo();
			$query = "SELECT group_id FROM #__lms_users_in_global_groups WHERE user_id = $user_id";
			$db->SetQuery($query);
			$this->_my_groups_user = JLMSDatabaseHelper::LoadResultArray();
			$this->populated_mygroupuser = true;
		}
	}

	function getMyGroupsUser() {
		$this->populateMyGroupsUser();
		return $this->_my_groups_user;
	}

	function PopulateMyCategories() {
		if (is_null($this->populated_mycategories)) {
			$db = JFactory::getDbo();
			$user_id = $this->_user_id;
			if ($this->CheckPermissions('advanced', 'view_all_course_categories')) {
				$query = "SELECT * FROM #__lms_course_cats ORDER BY c_category";
				$db->SetQuery($query);
				$all_cats = $db->LoadObjectList();
			} else {
				//TODO: after creating permission 'create_courses_in_limited_cats_only' - we will have 2 categories types: cars_view, cats_manage
				//1. get my groups and assigned groups
				$my_groups_assigned = $this->getMyGroupsAssigned();
				$my_groups_user = $this->getMyGroupsUser();
				//2. get limited list of categories
				$groups_subquery = '';
				if (count($my_groups_assigned) || count($my_groups_user)) {
					$groups_subqueries = array();
					foreach($my_groups_assigned as $mygroup) {
						$groups_subqueries[] = "`groups` like '%|".$mygroup."|%'";
					}
					foreach($my_groups_user as $mygroup) {
						$groups_subqueries[] = "`groups` like '%|".$mygroup."|%'";
					}
					$groups_subquery = implode("\n OR ",$groups_subqueries);
				}
				$query = "SELECT * FROM #__lms_course_cats WHERE (parent = '0' AND (`restricted` = 0".($groups_subquery ? (" OR (`restricted` = 1 AND ( ".$groups_subquery." ))") : '').") ) OR (parent <> '0') ORDER BY c_category";
				$db->SetQuery($query);
				$all_cats = $db->LoadObjectList();
			}
			$max_levels_limit = 10;
			$i = 0;
			$prooceed_with_cats = true;
			while ($i < $max_levels_limit && $prooceed_with_cats) {
				foreach ($all_cats as $cat) {
					if ($i) {
						if ($cat->parent && isset($this->_my_course_categories[$i - 1]) && in_array($cat->parent, $this->_my_course_categories[$i - 1])) {
							$ccat = new stdClass();
							$ccat->id = $cat->id;
							$ccat->c_category = $cat->c_category;
							$ccat->parent = $cat->parent;
							$ccat->restricted = $cat->restricted;
							$ccat->groups = $cat->groups;
							$this->_my_course_categories_full[$i][] = $ccat;
							$this->_my_course_categories[$i][] = $cat->id;
						}
					} else {
						//populate first level of categories - only parents
						if (!$cat->parent) {
							$ccat = new stdClass();
							$ccat->id = $cat->id;
							$ccat->c_category = $cat->c_category;
							$ccat->parent = $cat->parent;
							$ccat->restricted = $cat->restricted;
							$ccat->groups = $cat->groups;
							$this->_my_course_categories_full[0][] = $ccat;
							$this->_my_course_categories[0][] = $cat->id;
						}
					}
				}
				if (isset($this->_my_course_categories[$i]) && count($this->_my_course_categories[$i])) {
				} else {
					$prooceed_with_cats = false;
				}
				$i ++;
			}
			$this->populated_mycategories = true;
		}
	}

	function getMyCategories($level = 0, $parent_id = 0) {
		$this->PopulateMyCategories();
		$cats = array();
		if ($level == -1 && $parent_id == -1) {
			foreach ($this->_my_course_categories_full as $onecatlevel) {
				foreach ($onecatlevel as $onecat) {
					$cats[] = $onecat;
				}
			}
		} else {
			if (isset($this->_my_course_categories_full[$level])) {
				foreach ($this->_my_course_categories_full[$level] as $onecat) {
					if (isset($onecat->parent) && $onecat->parent == $parent_id) {
						$cats[] = $onecat;
					}
				}
			}
		}
		return $cats;
	}

	function getMyCategoriesIds($level = 0, $parent_id = 0) {
		$this->PopulateMyCategories();
		$cats = array();
		if ($level == -1 && $parent_id == -1) {
			for ($i = 0, $n = count($this->_my_course_categories); $i < $n; $i++) {
				for ($j = 0, $m = count($this->_my_course_categories[$i]); $j < $m; $j++) {
					$cats[] = $this->_my_course_categories[$i][$j];
				}
			}
			foreach ($this->_my_course_categories_full as $onecatlevel) {
				foreach ($onecatlevel as $onecat) {
					$cats[] = $onecat;
				}
			}
		} else {
			if (isset($this->_my_course_categories_full[$level])) {
				for ($j = 0, $m = count($this->_my_course_categories_full[$level]); $j < $m; $j++) {
					if (isset($this->_my_course_categories_full[$level][$j]->parent) && $this->_my_course_categories_full[$level][$j]->parent == $parent_id) {
						$cats[] = $this->_my_course_categories[$level][$j];
					}
				}
			}
		}
		return $cats;
	}

	function PopulateMyCourses() {
		if (is_null($this->populated_mycourses)) {
			$dbo = & JLMSFactory::getDB();
			$JLMS_CONFIG = JLMSFactory::getConfig();
			$user_id = $this->_user_id;
			$temp_role_type = $this->getTempRoleType($user_id);
			$temp_role = $this->getTempRole($user_id);
			$courses_teach_full_data = array();
			
			//1. lms admins...  ***  ***  ***
			$query = "SELECT id as course_id, course_name, cat_id FROM #__lms_courses";
			$dbo->SetQuery( $query );
			$all_lms_courses = $dbo->LoadObjectList();
			$courses_teach_full_data = array();
			if ($temp_role_type == 4) {
				if ($this->CheckPermissions('advanced', 'view_all_course_categories')) {
					//add all the courses into the list of 'teach' courses for 'lms administrator' roles
					foreach ($all_lms_courses as $lms_course) {
						$teachercourse = new stdClass();
						$teachercourse->course_id = $lms_course->course_id;
						$teachercourse->course_name = $lms_course->course_name;
						$teachercourse->role_id = $temp_role;
						$courses_teach_full_data[] = $teachercourse;
					}
				} else {
					$mycats = $this->getMyCategoriesIds(-1,-1);
					//add only allowed courses (allowed by categories) into the list of 'teach' courses for lms admins
					foreach ($all_lms_courses as $course_data_tmp ) {
						if ( !$course_data_tmp->cat_id || ($course_data_tmp->cat_id && in_array($course_data_tmp->cat_id, $mycats)) ) {
							$mycourse_data = new stdClass();
							$mycourse_data->course_id = $course_data_tmp->course_id;
							$mycourse_data->role_id = $temp_role;
							$mycourse_data->course_name = $course_data_tmp->course_name;
							$courses_teach_full_data[] = $mycourse_data;
						}
					}
					
				}
			}

			$JLMS_CONFIG->set('is_super_user', ($temp_role_type == 4)?1:0);
			$JLMS_CONFIG->set('is_teacher_user', ( ($temp_role_type == 4) || ($temp_role_type == 2) )?1:0);

			$this->_courses_teach = array();
			//populate _courses_teach and _my_courses_roles lists
			foreach ($courses_teach_full_data as $ct) {
				if (!in_array($ct->course_id, $this->_courses_teach)) {
					$courses_roles_obj = new stdClass();
					$courses_roles_obj->course_id = $ct->course_id;
					$courses_roles_obj->role_id = $ct->role_id;
					//$this->_my_courses_roles[] = $courses_roles_obj;
					$this->AddMyCourseRole($courses_roles_obj);
					$this->_courses_teach[] = $ct->course_id;
					$this->_cache_course_names[$ct->course_id] = $ct->course_name;
				}
			}
			
			//2. teacher's part... ***  ***  ***
			/**
			 *	why this code is executed for lms_admins if all the courses are already added for them?
			 *	...because course category can be restricted for admin and they can have different role for restricted courses
			 *	... (e.g. teacher role or student role for restricted courses and admin role for all the rest)
			 */
			if ($user_id) {
				$query = "SELECT distinct a.course_id, a.role_id, b.course_name FROM #__lms_user_courses as a, #__lms_courses as b WHERE a.user_id = $user_id AND b.id = a.course_id";
				$dbo->SetQuery( $query );
				$courses_teach_full_data = $dbo->LoadObjectList();
			} else {
				$courses_teach_full_data = array();
			}
			foreach ($courses_teach_full_data as $ct) {
				if (!in_array($ct->course_id, $this->_courses_teach)) {
					$courses_roles_obj = new stdClass();
					$courses_roles_obj->course_id = $ct->course_id;
					$courses_roles_obj->role_id = $ct->role_id;
					//$this->_my_courses_roles[] = $courses_roles_obj;
					$this->AddMyCourseRole($courses_roles_obj);
					$this->_courses_teach[] = $ct->course_id;
					$this->_cache_course_names[$ct->course_id] = $ct->course_name;
				}
			}

			// temporary for compatibility with old versions
			$JLMS_CONFIG->set('teacher_in_courses', ((!empty($this->_courses_teach))?$this->_courses_teach:array()) );

			//3. learner's part...  ***  ***  ***

			if ($JLMS_CONFIG->get('show_future_courses', false)) {
				$query = "SELECT a.course_id, a.role_id, b.course_name, a.enrol_time, "
				. "\n CASE WHEN (b.publish_start = 1 AND b.start_date > '".date('Y-m-d')."') THEN 1 "
				. "\n WHEN (a.publish_start = 1 AND a.start_date > '".date('Y-m-d', strtotime(JLMS_dateToDB(JLMS_offsetDateToDisplay(gmdate("Y-m-d H:i:s")))))."') THEN 1 ELSE 0 END as is_future_course"
				//IF(b.publish_start = 1, IF(b.start_date <= '".date('Y-m-d')."', 0, 1), 0) as is_future_course
				. "\n FROM #__lms_users_in_groups as a, #__lms_courses as b WHERE a.user_id = $user_id"
				. "\n AND a.course_id = b.id"
				. "\n AND ( b.published = 1"
				//. "\n AND ( ((b.publish_start = 1) AND (b.start_date <= '".date('Y-m-d')."')) OR (b.publish_start = 0) )"
				. "\n AND ( ((b.publish_end = 1) AND (b.end_date >= '".date('Y-m-d')."')) OR (b.publish_end = 0) ) )"
				//. "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )"
				. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d', strtotime(JLMS_dateToDB(JLMS_offsetDateToDisplay(gmdate("Y-m-d H:i:s")))))."')) OR (a.publish_end = 0) )"
				;
				$dbo->SetQuery( $query );
				$courses_learn_pre = $dbo->LoadObjectList();
	
				//TODO: future course eto eshe i kogda user access start_date in the future
	
				$this->_courses_learn = array();
				$this->_courses_future = array();
				foreach ($courses_learn_pre as $courses_learn_pre1) {
					if ($courses_learn_pre1->is_future_course) {
						if ($JLMS_CONFIG->get('show_future_courses', false)) {
							$this->_courses_future[] = $courses_learn_pre1->course_id;
						}
					} else {
						$this->_courses_learn[] = $courses_learn_pre1->course_id;
						$this->_courses_learn_full[$courses_learn_pre1->course_id] = $courses_learn_pre1->role_id;
						$course_role = new stdClass();
						$course_role->course_id = $courses_learn_pre1->course_id;
						$course_role->role_id = $courses_learn_pre1->role_id;
						$this->AddMyCourseRole($course_role);
					}
					$this->_cache_course_names[$courses_learn_pre1->course_id] = $courses_learn_pre1->course_name;
					$this->_courses_enrol_time[$courses_learn_pre1->course_id] = $courses_learn_pre1->enrol_time;
				}			
			} else {
				$query = "SELECT a.course_id, a.role_id, b.course_name, a.enrol_time"
				. "\n FROM #__lms_users_in_groups as a, #__lms_courses as b"
				. "\n WHERE 1"
				. "\n AND a.user_id = '".$user_id."'"
				. "\n AND a.course_id = b.id"
				. "\n AND ( b.published = 1"
				. "\n AND ( ((b.publish_start = 1) AND (b.start_date <= '".date('Y-m-d')."')) OR (b.publish_start = 0) )"
				. "\n AND ( ((b.publish_end = 1) AND (b.end_date >= '".date('Y-m-d')."')) OR (b.publish_end = 0) ) )"
	
				. "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d', strtotime(JLMS_dateToDB(JLMS_offsetDateToDisplay(gmdate("Y-m-d H:i:s")))))."')) OR (a.publish_start = 0) )"
				. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d', strtotime(JLMS_dateToDB(JLMS_offsetDateToDisplay(gmdate("Y-m-d H:i:s")))))."')) OR (a.publish_end = 0) )"
				;
				$dbo->SetQuery( $query );
				$courses_learn_full_info = $dbo->LoadObjectList();
				foreach ($courses_learn_full_info as $l_course) {
					$this->_courses_learn[] = $l_course->course_id;
					$this->_courses_learn_full[$l_course->course_id] = $l_course->role_id;
					$this->_cache_course_names[$l_course->course_id] = $l_course->course_name;
					$this->_courses_enrol_time[$l_course->course_id] = $l_course->enrol_time;
					$course_role = new stdClass();
					$course_role->course_id = $l_course->course_id;
					$course_role->role_id = $l_course->role_id;
					$this->AddMyCourseRole($course_role);
				}
				$this->_courses_future = array();
			}
			$this->CacheToTitles();
			$JLMS_CONFIG->set('student_in_courses', ((!empty($this->_courses_learn))?$this->_courses_learn:array()) );
			$JLMS_CONFIG->set('student_in_future_courses', ((!empty($this->_courses_future))?$this->_courses_future:array()) );
			$this->populated_mycourses = true;
			//TODO: 4. CEO part - implement them !!!
			if ($this->_main_role_type == 3) {
				$query = "SELECT user_id FROM #__lms_user_parents WHERE parent_id = $user_id";
				$dbo->SetQuery( $query );
				$this->_staff_learners = JLMSDatabaseHelper::LoadResultArray();
				$my_groups_assigned = $this->getMyGroupsAssigned();
				if (count($my_groups_assigned)) {
					$query = "SELECT distinct user_id FROM #__lms_users_in_global_groups WHERE group_id IN (".implode(',', $my_groups_assigned).")";
					$dbo->SetQuery( $query );
					$staff_learners2 = JLMSDatabaseHelper::LoadResultArray();
					if (count($staff_learners2)) {
						$this->_staff_learners = array_merge($this->_staff_learners, $staff_learners2);
						$this->_staff_learners = array_unique($this->_staff_learners);
					}
				}
				$JLMS_CONFIG->set('is_user_parent',(!empty($this->_staff_learners)?1:0));
				if (!empty($this->_staff_learners)) {
					$query = "SELECT distinct a.course_id FROM #__lms_users_in_groups as a WHERE a.user_id IN (".implode(',',$this->_staff_learners).")"
					. "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d', strtotime(JLMS_dateToDB(JLMS_offsetDateToDisplay(gmdate("Y-m-d H:i:s")))))."')) OR (a.publish_start = 0) )"
					. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d', strtotime(JLMS_dateToDB(JLMS_offsetDateToDisplay(gmdate("Y-m-d H:i:s")))))."')) OR (a.publish_end = 0) )"
					;
					$dbo->SetQuery( $query );
					$this->_courses_staff = JLMSDatabaseHelper::LoadResultArray();
					/*
					//TODO: move this code into prepareCourse !!!
					if ($course_id) {
						if (!in_array($course_id, $this->_courses_staff)) {
							if ($this->_switch_role == 6) {
								$this->ClearSwitchedRole();
							}
						}
					}*/
				} else {
					$this->_courses_staff = array();
				}
				$JLMS_CONFIG->set('parent_in_courses', ((!empty($this->_courses_staff))?$this->_courses_staff:array()) );
				
			}
			//TODO: 5. populate guest courses with role_id = 0... courses which user able to see in the list... - leave this one for JoomlaLMS 2.0
		}
	}

	function AddMyCourseRole($courserole) {
		$already_exists = false;
		foreach ($this->_my_courses_roles as $courseid => $roleid) {
			if ($courseid == $courserole->course_id) {
				$already_exists = true;break;
			}
		}
		if (!$already_exists) {
			$this->_my_courses_roles[$courserole->course_id] = $courserole->role_id;
		}
	}

	//important: for the time being only $my->id can be used with this function, do not use ID of any other user to avoid issues !
	//this function is deprecated, user preparecourse instead
	function Prepare($course_id, $user_id, &$db, $allow_teachers = true, $allow_learners = true, $allow_staffs = true) {
		$this->PrepareCourse($course_id);
	}

	function PrepareCourse($course_id) {
	if ($course_id) {//all the rest we did in object __construct function
		$JLMS_CONFIG = JLMSFactory::getConfig();
		$dbo = JFactory::getDbo();
		$user_id = $this->_user_id;

		$this->PopulateSystemRoles(); // preload all available JoomlaLMS roles into array

		if ($course_id) {
			$this->GetSwitchedRole();//works only within the course
		}
		$this->_course_id = $course_id;
		$temp_role_type = $this->getTempRoleType($user_id);
		$temp_role = $this->getTempRole($user_id);

		$this->_main_role = 0;
		$this->_main_role_type = 0;
		$this->_role = 0;
		$this->_role_type = 0;
		$this->_SetCourseRoleAndType($course_id);
		
//		if ($course_id && in_array($course_id, $this->_courses_teach)) {
//			$this->_SetCourseRoleAndType($course_id);
//		} elseif ($course_id && in_array($course_id, $this->_courses_learn)) {
//			$temp_role = $this->_courses_learn_full[$course_id];
//			$this->_main_role = $temp_role;
//			$this->_main_role_type = 1;
//		}

		if (!$this->_main_role) {
			 if ($course_id && in_array($course_id, $this->_courses_staff) && $this->_temp_role_type == 3) {
				$this->_main_role = $this->_temp_role;
				$this->_main_role_type = $this->_temp_role_type;
			}
		}
		$this->_role = $this->_main_role;
		$this->_role_type = $this->_main_role_type;

		if ($this->_main_role) {
			if ($this->_switch_role == 2) {
				// TODO: redevelop if we will have Student roles management
				if ( in_array($course_id, $this->_courses_learn) || in_array($course_id, $this->_courses_teach) ) {
					//assign default learner role here
					$def_learner_role = $this->GetDefaultSystemRole(1);
					// [TODO]: possible bug if there are no any learner roles - but in this case LMS will not work
					$this->_role = $def_learner_role;
					$this->_role_type = 1;
				}
			} elseif ($this->_switch_role == 6) {
				// TODO: redevelop if we will have Staff roles management
				if ( in_array($course_id, $this->_courses_staff) ) {
					//assign default staff role here
					$def_staff_role = $this->GetDefaultSystemRole(3);
					$this->_role = $def_staff_role;
					$this->_role_type = 3;
				}
			}
		}
		if ($this->_role) {
			$this->LoadRolePermissions();
			$this->LoadRolesAssignments();
		}

/*
		//
		//$JLMS_CONFIG->set('is_super_user', ($temp_role == 5)?1:0);
		$JLMS_CONFIG->set('is_super_user', ($temp_role_type == 4)?1:0);
		//$JLMS_CONFIG->set('is_teacher_user', ( ($temp_role == 5) || ($temp_role == 1) )?1:0);
		$JLMS_CONFIG->set('is_teacher_user', ( ($temp_role_type == 4) || ($temp_role_type == 2) )?1:0);
		//

		if ($temp_role_type == 4) { // administrator role
			$this->_levels[] = 4;
			$this->_levels[] = 2;
		} elseif ($temp_role_type == 2) {// teacher role
			$this->_levels[] = 2;
		} else {
		}
		
		if($temp_role_type == 4) {
			$this->_main_role = $temp_role;
			$this->_main_role_type = $temp_role_type;
			$this->_role = $this->_main_role;
			$this->_role_type = $this->_main_role_type;
			$this->LoadRolePermissions($course_id ? false : true);
			$this->LoadRolesAssignments();
			$this->_prepared = true;
			$this->PopulateMyCategories();
			$this->PopulateMyCourses();/// populate object arrays with learner and teacher courses
			//TODO: role switching didn't work for LMS_admins at the moment.. because we separate them here from other rol types.
		} else {
		
		$this->PopulateMyCategories();//TODO: check if this position is good for this function... maybe for teachers we need to move it below the role permissions load
		$this->PopulateMyCourses(); /// populate object arrays with learner and teacher courses

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		//get courses where user parent
		$query = "SELECT user_id FROM #__lms_user_parents WHERE parent_id = $user_id";
		$dbo->SetQuery( $query );
		$this->_staff_learners = JLMSDatabaseHelper::loadResultArray();
		$this->_staff_learners_bic = $this->_staff_learners;
		
			$query = "SELECT distinct(user_id) FROM #__lms_users_in_groups as uig, #__users as u WHERE uig.user_id = u.id";
			$dbo->setQuery($query);
			$all_users = JLMSDatabaseHelper::loadResultArray();
			
			$this->_staff_learners = array_merge($this->_staff_learners, $all_users);
		
		if (!empty($this->_staff_learners)) {
			$query = "SELECT distinct a.course_id FROM #__lms_users_in_groups as a WHERE a.user_id IN (".implode(',',$this->_staff_learners).")"
			. "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )"
			. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d')."')) OR (a.publish_end = 0) )"
			;
			$dbo->SetQuery( $query );
			$this->_courses_staff = JLMSDatabaseHelper::loadResultArray();
			if ($course_id) {
				if (!in_array($course_id, $this->_courses_staff)) {
					if ($this->_switch_role == 6) {
						$this->ClearSwitchedRole();
					}
				}
			}
		} else {
			$this->_courses_staff = array();
		}

			//TODO: made this normal way.. a bit later
			//query = "SELECT distinct a.id as course_id FROM #__lms_courses as a WHERE 1"
			//. "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )"
			//. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d')."')) OR (a.publish_end = 0) )"
			//;
			//$dbo->SetQuery( $query );
			//$this->_courses_staff = JLMSDatabaseHelper::loadResultArray();
			
			$JLMS_CONFIG->set('parent_in_courses', ((!empty($this->_courses_staff))?$this->_courses_staff:array()) );
		
		//$this->_courses_staff = array();
		//$this->_staff_learners = array();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		$this->_main_role = 0;
		if (true) {
			if ($this->isAdmin()) {
				$this->_main_role = 5;
				$this->_main_role_type = 4;
			} elseif (in_array($course_id, $this->_courses_teach)) {
				$this->_main_role = 1; // id !!!!!!!!!!!!!!
				$this->_main_role_type = 2;
			} elseif (in_array($course_id, $this->_courses_learn)) {
				$this->_main_role = 2;
				$this->_main_role_type = 1;
			} elseif (in_array($course_id, $this->_courses_staff)) {
				$this->_main_role = 6;
				$this->_main_role_type = 3;
			}
			if($temp_role_type == 4) {
				$this->_main_role = $temp_role;
				$this->_main_role_type = $temp_role_type;
			} elseif ($course_id && in_array($course_id, $this->_courses_teach)) {
				$this->_SetCourseRoleAndType($course_id);
			} elseif ($course_id && in_array($course_id, $this->_courses_learn)) {
				// TODO: redevelop if we will have Student roles management
				$temp_role = $this->_courses_learn_full[$course_id];
				$this->_main_role = $temp_role;
				$this->_main_role_type = 1;
			} elseif ($course_id && in_array($course_id, $this->_courses_staff)) {
				// TODO: redevelop if we will have Staff roles management
//				$this->_main_role = 6;
//				$this->_main_role_type = 3;
				
				$this->_main_role = $temp_role;
				$this->_main_role_type = $temp_role_type;
			}
//			
//			elseif (!$course_id && $temp_role_type == 2){
//				$this->_main_role = $temp_role;
//				$this->_main_role_type = 2;	
//			}
			elseif (!$course_id && in_array($temp_role_type, array(1,2,3,4,5))){
				$this->_main_role = $temp_role;
				$this->_main_role_type = $temp_role_type;	
			}
		}

		$this->_role = $this->_main_role;
		$this->_role_type = $this->_main_role_type;

		if ($this->_main_role) {
			if ($this->_switch_role == 2) {
				// TODO: redevelop if we will have Student roles management
				if ( in_array($course_id, $this->_courses_learn) || in_array($course_id, $this->_courses_teach) ) {
					//assign default learner role here
					$def_learner_role = $this->GetDefaultSystemRole(1);
					// [TODO]: possible bug if there are no any learner roles - but in this case LMS will not work
					$this->_role = $def_learner_role;
					$this->_role_type = 1;
				}
			} elseif ($this->_switch_role == 6) {
				// TODO: redevelop if we will have Staff roles management
				if ( in_array($course_id, $this->_courses_staff) ) {
					//assign default staff role here
					$def_staff_role = $this->GetDefaultSystemRole(3);
					$this->_role = $def_staff_role;
					$this->_role_type = 3;
				}
			}
		}
		if ($this->_role_type == 3 && !$allow_staffs) {
			// TODO: redevelop if we will have Staff roles management
			$this->_role = $this->_main_role;
			$this->_role_type = $this->_main_role_type;
			if ($this->_role_type == 3 && !$allow_staffs) {
				$this->_role = 0;
				$this->_role_type = 0;
				$this->_main_role = 0;
				$this->_main_role_type = 0;
			}
		}
		if ($this->_role_type == 1 && !$allow_learners) {
			// TODO: redevelop if we will have Student roles management
			$this->_role = $this->_main_role;
			$this->_role_type = $this->_main_role_type;
			if ($this->_role_type == 1 && !$allow_learners) {
				$this->_role = 0;
				$this->_role_type = 0;
				$this->_main_role = 0;
				$this->_main_role_type = 0;
			}
		}
		if ($this->_role) {
			$this->LoadRolePermissions($course_id ? false : true);
			$this->LoadRolesAssignments();
			$this->_prepared = true;
		}
		
		}//else for if temprole == 4
		//BrustresIce Cream
		//$this->_staff_learners = array();
//		if(isset($this->_roles_full[$this->_role]['ceo']->view_all_users)){
//			$all_users = array();
//			$this->_staff_learners = array();
//			if($this->_roles_full[$this->_role]['ceo']->view_all_users){
//				$query = "SELECT distinct(user_id) FROM #__lms_users_in_groups as uig, #__users as u WHERE uig.user_id = u.id";
//				$dbo->setQuery($query);
//				$all_users = JLMSDatabaseHelper::loadResultArray();
//			} else {
//				$query = "SELECT grp_id FROM #__lms_user_assigned_groups WHERE parent_id = $user_id";
//				$dbo->setQuery($query);
//				$grps = JLMSDatabaseHelper::loadResultArray();
//				
//				if(count($grps)){
//					$str_group_ids = implode(",", $grps);
//					$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE group_id IN (".$str_group_ids.")";
//					$dbo->setQuery($query);
//					$all_users = JLMSDatabaseHelper::loadResultArray();
//				}
//			}
//			$this->_staff_learners = array_merge($this->_staff_learners_bic, $all_users);
//		}
//		
////		echo '<pre>';
////		print_r($this->_staff_learners);
////		echo '</pre>';
//		
//		$JLMS_CONFIG->set('is_user_parent',(!empty($this->_staff_learners)?1:0));
//		
//		if(isset($this->_roles_full[$this->_role]['ceo']->view_all_users)){
//			if($this->_roles_full[$this->_role]['ceo']->view_all_users){
//				//TODO: made this normal way.. a bit later
//				$query = "SELECT distinct a.id as course_id FROM #__lms_courses as a WHERE 1"
//				. "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )"
//				. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d')."')) OR (a.publish_end = 0) )"
//				;
//				$db->SetQuery( $query );
//				$this->_courses_staff = JLMSDatabaseHelper::loadResultArray();
//			} else {
//				//TODO: made this normal way.. a bit later
//				$query = "SELECT distinct a.id as course_id FROM #__lms_courses as a, #__lms_users_in_groups as uig"
//				. "\n WHERE 1"
//				. "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )"
//				. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d')."')) OR (a.publish_end = 0) )"
//				. "\n AND uig.course_id = a.id"
//				. "\n AND uig.user_id IN (".implode(",", $this->_staff_learners).")"
//				;
//				$db->SetQuery( $query );
//				$this->_courses_staff = JLMSDatabaseHelper::loadResultArray();
//			}
//		}
		//TODO: made this normal way.. a bit later
//		$query = "SELECT distinct a.id as course_id FROM #__lms_courses as a WHERE 1"
//		. "\n AND ( ((a.publish_start = 1) AND (a.start_date <= '".date('Y-m-d')."')) OR (a.publish_start = 0) )"
//		. "\n AND ( ((a.publish_end = 1) AND (a.end_date >= '".date('Y-m-d')."')) OR (a.publish_end = 0) )"
//		;
//		$dbo->SetQuery( $query );
//		$this->_courses_staff = JLMSDatabaseHelper::loadResultArray();
//		
//		$JLMS_CONFIG->set('parent_in_courses', ((!empty($this->_courses_staff))?$this->_courses_staff:array()) );
//BrustresIce Cream
*/
	} //end for "if course_id"
	}

	function _SetCourseRoleAndType($course_id) {
		$role = 0;
		$role_type = 0;
		if ($course_id) {
			if (!empty($this->_my_courses_roles)) {
				foreach ($this->_my_courses_roles as $courseid => $roleid) {
					if ($courseid == $course_id) {
						if ($roleid) {
							foreach ($this->_system_roles as $sr) {
								if ($sr->id == $roleid) {
									$this->_main_role = $sr->id;
									$this->_main_role_type = $sr->roletype_id;
									break;
								}
							}
						}
						break;
					}
				}
			}
		}
	}

	function LoadRolePermissions($system_only = false, $role = null, $roletype = null) {
		if (is_null($role)) {
			$role = $this->_role;
			$roletype = $this->_role_type;
		}
		require_once( dirname(__FILE__) . "/acl/prototype.php");
		$text_permisions = & JLMS_role_prototype($roletype);
		
		if ($roletype == 4) {
			require_once( dirname(__FILE__) . "/acl/administrator.php");
			$this->_roles_full[$role] = & JLMS_get_administrator_role();
		} elseif ($roletype == 2) {
			require_once( dirname(__FILE__) . "/acl/teacher.php");
			$this->_roles_full[$role] = & JLMS_get_teacher_role();
		} elseif ($roletype == 5) {
			require_once( dirname(__FILE__) . "/acl/assistant.php");
			$this->_roles_full[$role] = & JLMS_get_assistant_role();
		} elseif ($roletype == 1) {
			require_once( dirname(__FILE__) . "/acl/learner.php");
			$this->_roles_full[$role] = & JLMS_get_learner_role();	
		} elseif ($roletype == 3) {
			require_once( dirname(__FILE__) . "/acl/ceo.php");
			$this->_roles_full[$role] = & JLMS_get_ceo_role();
		}
		/* additional roles handling goes here */
		/*if ($role == X) {
			require_once( dirname(__FILE__) . "/acl/custom_role1.php");
			$this->_roles_full[$role] = & JLMS_get_custom_role();
		}*/
		
		/*
		Extra permissions not permissions file role. permissions prototype.php (Max)
		*/
		foreach($text_permisions as $key=>$text_permision){
			$available_roletypes = isset($text_permision->role_types) ? $text_permision->role_types : array(0);
			foreach($text_permision as $key2=>$text_permision2){
				$available = 0;
				if($available_roletypes[0] == 0){
					$available = 1;
				} else {
					if(in_array($roletype, $available_roletypes)){
						$available = 1;	
					}
				}
				if($available && $key2 != 'role_types' && !isset($this->_roles_full[$role][$key]->$key2)){
					$this->_roles_full[$role][$key]->$key2 = 0;
				}	
			}
		}
		
		/* load permissions DB (Max) */
		if(isset($role) && $role){
			$db = JFactory::getDBO();
			$rows_permissions = array();
			if($role){
				$query = "SELECT * FROM #__lms_user_permissions WHERE role_id = '".$role."'";
				$db->setQuery($query);
				$rows_permissions = $db->loadObjectList();
			}
			
			$tmp_db_permissions = array();
			$p_category = '';
			foreach($rows_permissions as $row_permission){
				if($p_category == '' || $p_category != $row_permission->p_category){
					$p_category = $row_permission->p_category;
				}		
				
				$text_prms = $row_permission->p_permission;
				$value_prms = $row_permission->p_value;				
				
				if($text_prms != 'id' && $text_prms != 'role_id' && $text_prms != 'p_category')
				{					
					if(!isset($tmp_db_permissions[$p_category])) 
					{
						$tmp_db_permissions[$p_category] = new stdclass();
					}
					$tmp_db_permissions[$p_category]->$text_prms = $value_prms;					
				}
			}
			$tmp_permisions = $this->_roles_full[$role];
			if(count($tmp_permisions)){
				$permissions = array();
				foreach($tmp_permisions as $p_category=>$tmp_prms){
					$available_roles = isset($text_permisions[$p_category]->role_types)?$text_permisions[$p_category]->role_types:array(0);
					if(isset($text_permisions[$p_category]->role_types) && in_array($roletype, $available_roles)){
						foreach($tmp_prms as $permision=>$value){
							if(isset($text_permisions[$p_category]->$permision)){
								//smt for php 5.4+
								if (!isset($permissions[$p_category])) $permissions[$p_category] = new stdclass();
								$permissions[$p_category] = (object) $permissions[$p_category];
								//
								$permissions[$p_category]->$permision = $tmp_permisions[$p_category]->$permision;
								if(isset($tmp_permisions[$p_category]->$permision) && isset($tmp_db_permissions[$p_category]->$permision)){
									$permissions[$p_category]->$permision = $tmp_db_permissions[$p_category]->$permision;
								}
							}	
						}
					}	
				}
				$this->_roles_full[$role] = $permissions;
			}
		}
		
		//var_dump($system_only);
		//echo '<br />';
		
		if ($system_only && isset($this->_roles_full[$role]) && is_array($this->_roles_full[$role])) {
			foreach ($this->_roles_full[$role] as $tool => $permissions) {
				if ($tool == 'lms' || $tool == 'advanced' || $tool == 'library' || $tool == 'reports' || $tool == 'quizzes') {//'quizzes' is a temporary [permissions] fix for GQP
					
				} else {
					unset($this->_roles_full[$role][$tool]);
				}
			}
		}
	}
	
	function getRoles(&$db){
		$roles = array();
		
		$query = "SELECT *"
		. "\n FROM #__lms_usertypes"
		. "\n WHERE 1"
		. "\n AND roletype_id > 0"
		;
		$db->setQuery($query);
		$roles = $db->loadObjectList();
		
		$order = array(4,2,5,1,3);
		$tmp_roles = array();
		foreach($order as $x){
			foreach($roles as $role){
				if($role->roletype_id == $x){
					$tmp_roles[] = $role;
				}
			}
		}
		if(isset($tmp_roles) && count($tmp_roles)){
			$roles = array();
			$roles = $tmp_roles;
		}
		return $roles;
	}
	
	function TransformRolesAssignments(&$in_assignments, &$db){
		
		$db = JFactory::getDBO();
		
		$out_assignments = false;
	
//		$roles = $this->getRoles($db);
		$roles = $this->_system_roles;
		
		$xroles = array();
		$order = array(4,2,5);
		foreach($order as $x){
			foreach($roles as $role){
				if($role->roletype_id == $x){
					$xroles[] = $role;
				}
			}
		}
		
		$yroles = $roles;
		
		$i=0;
		foreach($xroles as $xrole){
			foreach($yroles as $yrole){
				foreach($in_assignments as $in_a){
					if($xrole->roletype_id == $in_a->roletype_id && in_array($yrole->roletype_id, $in_a->assignment)){
						
						$out_assignments[$i] = new stdClass();
						$out_assignments[$i]->role_id = $xrole->id;
						$out_assignments[$i]->role_assign = $yrole->id;
						$out_assignments[$i]->value = 1;
						
						$i++;
					}
				}
			}
		}
		
		$in_assignments = array();
		
		return $out_assignments;
	}
	
	function LoadRolesAssignments(){
		
		$db = JFactory::getDBO();
		
		require_once( dirname(__FILE__) . "/acl/assignments_roles.php");
		$assignments = & JLMS_get_allow_assignments_roles();
		$assignments = $this->TransformRolesAssignments($assignments, $db);
		
		$query = "SELECT *"
		. "\n FROM #__lms_user_roles_assignments"
		. "\n WHERE 1"
		;
		$db->setQuery($query);
		$db_tmp_assignments = $db->loadObjectList();

		if ($assignments && is_array($assignments)) {
			$tmp_asssignments = array();
			$i=0;
			foreach($assignments as $assign){
				$tmp_asssignments[$i] = $assign;
				foreach($db_tmp_assignments as $db_tmp_assign){
					if($assign->role_id == $db_tmp_assign->role_id && $assign->role_assign == $db_tmp_assign->role_assign){
						if($db_tmp_assign->value){
							$tmp_asssignments[$i]->value = $db_tmp_assign->value;
						} else 
						if(!$db_tmp_assign->value){
							$tmp_asssignments[$i]->value = $db_tmp_assign->value;
						}
					} 
					else {
						$tmp_asssignments[] = $db_tmp_assign;
					}
				}
				$i++;
			}
			if(isset($tmp_asssignments) && count($tmp_asssignments)){
				$assignments = array();
				$assignments = $tmp_asssignments;
			}
		}
	
		$roles_assignments = array();
		if ($assignments && is_array($assignments)) {
			foreach($assignments as $assign){
				if($assign->role_id == $this->_role && $assign->value && !in_array($assign->role_assign, $roles_assignments)){
					$roles_assignments[] = $assign->role_assign;
				}
			}
		}
		$this->_roles_assignments = $roles_assignments;
	}

	function CachePerm($tool, $action, $perm) {
		$this->_cache['role_'.$this->_role.'__'.$tool.'___'.$action] = $perm;
	}

	function CheckToolPermissions($tool) {
		$action = 'access_to_tool';
		if (true) {//$this->_prepared) {
			if (isset($this->_cache['role_'.$this->_role.'__'.$tool.'___'.$action])) {
				return $this->_cache['role_'.$this->_role.'__'.$tool.'___'.$action];
			}
			$perm = false;
			if ($tool == 'gradebook' && $this->_role_type == 3) { // DEN: temporary solution to fix CEO gradebook access bug
				$perm = true;
			} elseif (isset($this->_roles_full[$this->_role][$tool])) {
				foreach ($this->_roles_full[$this->_role][$tool] as $tool_action) {
					if ($tool_action == 1 || $tool_action == '1') {
						$perm = true;
					}
				}
			}
			$this->CachePerm($tool, $action, $perm);
			return $perm;
		} else {
			return false;
		}
	}

	function CheckPermissions($tool, $action) {
		if (true) {//if ($this->_prepared) {
			if (isset($this->_cache['role_'.$this->_role.'__'.$tool.'___'.$action])) {
				return $this->_cache['role_'.$this->_role.'__'.$tool.'___'.$action];
			}
			//$level = 3;
			//$check_by_level = true;
			$perm = false;
			if (isset($this->_roles_full[$this->_role][$tool]->$action) && $this->_roles_full[$this->_role][$tool]->$action) {
				$perm = true;
			}/*
			switch ($tool){
				case 'lms':
					switch($action) {
						case 'create_course':		$level = 2;		break;
						case 'home':				$level = -1;	break;
					}
				case 'docs':
					switch($action) {
						case 'view':				$level = 1;		break;
						case 'view_all':			$level = 2;		break;
						case 'order':				$level = 2;		break;
						case 'publish':				$level = 2;		break;
						case 'manage':				$level = 2;		break;
					}
				case 'course':
					switch($action) {
						case 'manage_certificates':	$level = 2;		break;
					}
			}
			$perm = $this->_CheckPermissionsInternal($level, $check_by_level);*/
			
			
			$this->CachePerm($tool, $action, $perm);
			return $perm;
		} else {
			return false;
		}
	}

	//TODO: get rid of this function... replace FileLibrary ACL checks with usual JoomlaLMS ACL checks
	function CheckPermissions_outdoc(&$db, $action) {
		if ($this->_prepared) {
			/*if (isset($this->_cache['outdoc___'.$action])) {
				return $this->_cache['outdoc___'.$action];
			}*/
			$level = 3;
			$check_by_level = true;

					switch($action) {
						case 'view':				$level = 1;		break;
						case 'view_all':			$level = 2;		break;
						case 'order':				$level = 2;		break;
						case 'publish':				$level = 2;		break;
						case 'manage':				$level = 2;		break;
					}

			$perm = $this->_CheckPermissionsInternal($level, $check_by_level);
			//$this->CachePerm($tool, $action, $perm);
			return $perm;
		} else {
			return false;
		}
	}
	/**
	 * _CheckPermissionsInternal($level) - function for checking user rights to access the resource with permissions level - $level
	 *
	 * List of access levels:
	 * -1 - guest
	 *  0 - registered
	 *  1 - learner role
	 *  2 - teacher role
	 *  3 - staff role
	 *  4 - administrator role
	 */
	function _CheckPermissionsInternal($level = -1, $check_by_level = true) {
		if ($check_by_level) {
			switch ($level){
				case 1: if (in_array($this->_role_type, array(1,2,4))) return true; break;
				case 2: if (in_array($this->_role_type, array(2,4))) return true; break;
				case 3: if ($this->_role_type == 3) return true; break;
				case 4: if ($this->_role_type == 4) return true; break;
				case 0: if ($this->_user_id) return true; break;
				case -1: return true; break;
			}
			return false;
		} else { // role permissions
			return false;
		}
	}

	//BUG: user can be assigned to more than 1 global group
	function CheckUserGroup(&$db, $user_id, $group_id, $course_id) {
		$JLMS_CONFIG = JLMSFactory::getConfig();
		if (isset($this->_cache['acl_chek_ug'.'___'.$user_id.'___'.$group_id.'___'.$course_id])) {
			$actual_group = $this->_cache['acl_chek_ug'.'___'.$user_id.'___'.$group_id.'___'.$course_id];
		} else {
			if ($JLMS_CONFIG->get('use_global_groups', 1)) {
				$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE group_id = $group_id AND user_id = $user_id";
			} else {
				$query = "SELECT group_id FROM #__lms_users_in_groups WHERE course_id = $course_id AND user_id = $user_id";
			}
			$db->SetQuery( $query );
			$actual_group = $db->LoadResult();
			$this->_cache['acl_chek_ug'.'___'.$user_id.'___'.$group_id.'___'.$course_id] = $actual_group;
		}
		if ($actual_group == $group_id) {
			return true;
		}
		return false;
	}

	function defaultRole($level){
		$db = JFactory::getDbo();
		$query = "SELECT id FROM #__lms_usertypes WHERE roletype_id = '".$level."' AND default_role = '1'";
		$db->setQuery($query);
		$default_role = $db->loadResult();
		return $default_role;
	}

	function getRolesAssignments(){
		return $this->_roles_assignments;
	}

	function getMyTeachCourses() {
		return $this->getMyCourses(4);
	}

	function getMyCourses($role_type = 0) {
		$this->PopulateMyCourses();
		$courses = array();
		if ($role_type) {
			if ($role_type == 1) {
				$courses = array_merge($courses, $this->_courses_learn);
			} elseif ($role_type == 2) {
				$courses = array_merge($courses, $this->_courses_teach);
			} elseif ($role_type == 3) {
				$courses = array_merge($courses, $this->_courses_staff);
			} elseif ($role_type == 4) {
				$courses = array_merge($courses, $this->_courses_teach);
			} elseif ($role_type == 5) {
				$courses = array_merge($courses, $this->_courses_teach);
			}
		} else {
			//show list of courses where user is admin/teacher/assistant/learner... all courses except of CEO/Staff courses
			$courses = array_merge($courses, $this->_courses_teach);
			$courses = array_merge($courses, $this->_courses_learn);
		}
		return $courses;
	}

	function getMyCourseRole($course_id) {
		if (isset($this->_my_courses_roles[$course_id])) {
			return $this->_my_courses_roles[$course_id];
		}
		return 0;
	}

	function getMyCoursesData($role_type = 0) {
		$courses_ids = $this->getMyCourses($role_type);
		$courses = array();
		foreach ($courses_ids as $course_id) {
			$new_course_data_obj = new stdClass();
			$new_course_data_obj->course_id = $course_id;
			$new_course_data_obj->course_name = $this->_cache_course_names[$course_id];
			$courses[] = $new_course_data_obj;
		}
		return $courses;
	}
}

class JLMS_ACL_HELPER {
	
	static function checkOtherTypeRole($roletype){
		$check = false;
		$JLMS_ACL = JLMSFactory::getACL();
		if (isset($JLMS_ACL->_roles_full) && count($JLMS_ACL->_roles_full)){
			foreach($JLMS_ACL->_roles_full as $role_full){
				
			}
		}
		return $check;
	}

    static function checkPermissionByToolAllRoles($tool, $perm){
		$check = false;
		$JLMS_ACL = JLMSFactory::getACL();
		if (isset($JLMS_ACL->_roles_full) && count($JLMS_ACL->_roles_full)){
			foreach($JLMS_ACL->_roles_full as $role_full){
				if(isset($role_full[$tool]->$perm) && $role_full[$tool]->$perm){
					$check = true;
					break;
				}
			}
		}
		return $check;
	}

    static function checkPermissionsByRole($role_id, $tool, $action) {
		if ($role_id) {
			$JLMS_ACL = JLMSFactory::getACL();
			if (!isset($JLMS_ACL->_roles_full[$role_id])) {
				$roletype = $JLMS_ACL->GetTypeofRole($role_id);
				$JLMS_ACL->LoadRolePermissions(true, $role_id, $roletype);
			}
			if (isset($JLMS_ACL->_roles_full[$role_id][$tool]->$action)) {
				if (isset($JLMS_ACL->_roles_full[$role_id][$tool]->$action) && $JLMS_ACL->_roles_full[$role_id]) {
					return true;
				}
			}
		}
		return false;
	}

    static function GetAssignedGroups($user_id, $course_id = 0) {
		//return array of assigned groups.
		
		$JLMS_DB = & JLMSFactory::getDB();
		
		$query = "SELECT a.group_id FROM `#__lms_user_assign_groups` as a WHERE a.user_id = '".$user_id."'"
		;
		$JLMS_DB->setQuery($query);
		$groups_where_admin_manager = JLMSDatabaseHelper::loadResultArray();
		
		$temp = $groups_where_admin_manager;
		
		if(count($temp)) {
			$ids = implode(',',$temp);
			$query = "select id FROM #__lms_usergroups WHERE parent_id IN ($ids)";
			$JLMS_DB->setQuery($query);
			$temp1 = JLMSDatabaseHelper::loadResultArray();

			$groups_where_admin_manager = array_merge($groups_where_admin_manager,$temp1);
		}
		
		return $groups_where_admin_manager;
	}

    static function GetUserGlobalGroup( $user_id, $course_id = 0 ) {
		$JLMS_DB = & JLMSFactory::getDB();
		
		$query = "SELECT a.group_id FROM `#__lms_users_in_global_groups` as a WHERE a.user_id = '".$user_id."'"
		;
		$JLMS_DB->setQuery($query);
		$res = JLMSDatabaseHelper::loadResultArray();

		return $res;
	}

    static function GetUserGroup($user_id, $course_id = 0) {
		$JLMS_DB = & JLMSFactory::getDB();
		
		$query = "SELECT group_id FROM `#__lms_users_in_groups` WHERE user_id = '".$user_id."' AND course_id=".$course_id;
		$JLMS_DB->setQuery($query);
		$res = JLMSDatabaseHelper::loadResultArray();

		return $res;
	}

    static function GetCountAssignedGroups($user_id, $course_id) {
		$JLMS_DB = & JLMSFactory::getDB();
		
		$query = "SELECT count(b.id) FROM #__lms_user_assign_groups as a, #__lms_usergroups as b WHERE a.user_id = '".$user_id."' AND a.group_id = b.id"
		;
		$JLMS_DB->setQuery($query);
		$count_groups_where_admin_manager = $JLMS_DB->loadResult();

		return $count_groups_where_admin_manager;	
	}

    static function getTeachersRoleIds()
	{
		$JLMS_DB = & JLMSFactory::getDB();
		
		$query = "SELECT id FROM #__lms_usertypes 
					WHERE roletype_id = 2"					
				;
		$JLMS_DB->setQuery($query);
		$teacher_roles = JLMSDatabaseHelper::loadResultArray();

		return $teacher_roles; 	
	}

    static function getAssistansRolesIds()
	{
		$JLMS_DB = & JLMSFactory::getDB();
		
		$query = "SELECT id FROM #__lms_usertypes 
					WHERE roletype_id = 5"					
				;
		$JLMS_DB->setQuery($query);
		$teacher_roles = JLMSDatabaseHelper::loadResultArray();

		return $teacher_roles; 	
	}

    static function getAdminsRoleIds()
	{
		$JLMS_DB = & JLMSFactory::getDB();
		
		$query = "SELECT id FROM #__lms_usertypes 
					WHERE roletype_id = 4"					
				;
		$JLMS_DB->setQuery($query);
		$admin_roles = JLMSDatabaseHelper::loadResultArray();

		return $admin_roles; 	
	}

    static function getCEORoleIds()
	{
		$JLMS_DB = & JLMSFactory::getDB();
		
		$query = "SELECT id FROM #__lms_usertypes 
					WHERE roletype_id = 3"					
				;
		$JLMS_DB->setQuery($query);
		$CEO_roles = JLMSDatabaseHelper::loadResultArray();

		return $CEO_roles;		
	}

    static function getCourseTeachers( $course_id, $roles )
	{
		$JLMS_DB = & JLMSFactory::getDB();
		
		$teachers = array();
				
		if( count($roles) ) {
			
			$roles = array_map( array( $JLMS_DB, 'quote' ), $roles );
			
			$query = "SELECT u.*  FROM #__lms_user_courses AS c, #__users AS u 
						WHERE c.course_id = '".$course_id."' 
						AND c.role_id IN (".implode(',', $roles).") 
						AND u.id = c.user_id 
						"
					;
			$JLMS_DB->setQuery($query);
			$teachers = $JLMS_DB->loadObjectList();
		}

		return $teachers; 	
	}

    static function getCourseAssistans( $course_id, $roles )
	{
		$JLMS_DB = & JLMSFactory::getDB();
		
		$assistans = array();
		
		if( count($roles) ) {	
					
			$roles = array_map( array( $JLMS_DB, 'quote' ), $roles );
					
			$query = "SELECT u.*  FROM #__lms_user_courses AS c, #__users AS u 
						WHERE c.course_id = '".$course_id."' 
						AND c.role_id IN (".implode(',', $roles).") 
						AND u.id = c.user_id 
						"
					;
			$JLMS_DB->setQuery($query);
			$assistans = $JLMS_DB->loadObjectList();
		}

		return $assistans;		
	}

    static function getUserCEO( $user_id, $roles )
	{
		$JLMS_DB = & JLMSFactory::getDB();
		$JLMS_CONFIG = JLMSFactory::getConfig();

		$ceo = array();
		
		if( count($roles) ) {
					
			$roles = array_map( array( $JLMS_DB, 'quote' ), $roles );	

			$query = "SELECT u.* FROM #__lms_user_parents AS p, #__users AS u, #__lms_users AS lu 
						WHERE p.user_id = '".$user_id."'
						AND lu.lms_usertype_id IN (".implode(',', $roles).")
						AND lu.user_id = p.parent_id 				 
						AND u.id = p.parent_id
						";
			$JLMS_DB->setQuery($query);
			$ceo = $JLMS_DB->loadObjectList();
			if($JLMS_CONFIG->get('use_global_groups', 1)){
				$query = "SELECT u.*"
					. "\n FROM #__lms_user_assign_groups as a, #__lms_users_in_global_groups as b, #__users AS u, #__lms_users AS lu"
					. "\n WHERE 1"
					. "\n AND b.user_id = '".$user_id."'"
					. "\n AND lu.lms_usertype_id IN (".implode(',', $roles).")"
					. "\n AND lu.user_id = a.user_id"
					. "\n AND a.group_id = b.group_id"
					. "\n AND u.id = a.user_id"
					;
				$JLMS_DB->setQuery($query);
				$ceo_group = $JLMS_DB->loadObjectList();

				$ceo = array_merge($ceo, $ceo_group);
			}
		}
		return $ceo;
	}

    static function getAdmins( $roles )
	{
		$JLMS_DB = & JLMSFactory::getDB();
		
		$admins = array();	
			
		if( count($roles) ) {
					
			$roles = array_map( array( $JLMS_DB, 'quote' ), $roles );
					
			$query = "SELECT u.*  FROM #__lms_users AS lu, #__users AS u 
						WHERE lu.lms_usertype_id IN (".implode(',', $roles).")									 
						AND u.id = lu.user_id
						"
					;
			$JLMS_DB->setQuery($query);
			$admins = $JLMS_DB->loadObjectList();
		}
		
		return $admins;	
	}
}
?>