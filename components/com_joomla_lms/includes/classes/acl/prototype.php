<?php 
/**
* includes/classes/acl/teacher.php
* Joomla LMS Component
* * * ElearningForce Biz
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

function & JLMS_role_prototype($roletype_id = '') {
	$role = array();
	
	if(isset($roletype_id) && ($roletype_id == 2 || $roletype_id == 3 || $roletype_id == 4 || $roletype_id == 5)) {
			$permissions_advanced = new stdClass();
			$permissions_advanced->assigned_groups_only = '_JLMS_PROTOTYPE_ADVANCED_ASSIGNED_GROUPS_ONLY_1';
			$permissions_advanced->view_all_course_categories = '_JLMS_PROTOTYPE_ADVANCED_VIEW_ALL_COURSE_CATEGORIES';
			
			$permissions_advanced->role_types = array(1,2,3,4); // teacher + admin + assistant
		$role['advanced'] = & $permissions_advanced;
	} elseif (isset($roletype_id) && $roletype_id == 1 ) {
			$permissions_advanced = new stdClass();
			$permissions_advanced->assigned_groups_only = '_JLMS_PROTOTYPE_ADVANCED_ASSIGNED_GROUPS_ONLY_2';
			$permissions_advanced->role_types = array(1); // teacher + admin + assistant
		$role['advanced'] = & $permissions_advanced;
	}

		$permissions_lms = new stdClass();
		$permissions_lms->create_course = '_JLMS_PROTOTYPE_LMS_CREATE_COURSE';
		$permissions_lms->order_courses = '_JLMS_PROTOTYPE_LMS_ORDER_COURSE';
		$permissions_lms->role_types = array(2,4); // teacher + admin
	$role['lms'] = & $permissions_lms;	

		$permissions_docs = new stdClass();
		$permissions_docs->view = '_JLMS_PROTOTYPE_DOCS_VIEW';
		$permissions_docs->view_all = '_JLMS_PROTOTYPE_DOCS_VIEW_ALL';
		$permissions_docs->order = '_JLMS_PROTOTYPE_DOCS_ORDER';
		$permissions_docs->publish = '_JLMS_PROTOTYPE_DOCS_PUBLISH';
		$permissions_docs->manage = '_JLMS_PROTOTYPE_DOCS_MANAGE';
		$permissions_docs->only_own_items = '_JLMS_PROTOTYPE_DOCS_ONLY_OWN_ITEMS';
		$permissions_docs->only_own_role = '_JLMS_PROTOTYPE_DOCS_ONLY_OWN_ROLES';
		$permissions_docs->set_permissions = '_JLMS_PROTOTYPE_DOCS_SET_PERMISSIONS';
		$permissions_docs->ignore_permissions = '_JLMS_PROTOTYPE_DOCS_IGNORE_PERMISSIONS';
		$permissions_docs->role_types = array(1,2,4,5); // learner + teacher + admin + assistant
	$role['docs'] = & $permissions_docs;

		$permissions_quizzes = new stdClass();
		$permissions_quizzes->view = '_JLMS_PROTOTYPE_QUIZZES_VIEW';
		$permissions_quizzes->view_all =  '_JLMS_PROTOTYPE_QUIZZES_VIEW_ALL';
		$permissions_quizzes->view_stats = '_JLMS_PROTOTYPE_QUIZZES_VIEW_STATS';
		$permissions_quizzes->publish = '_JLMS_PROTOTYPE_QUIZZES_PUBLISH';
		$permissions_quizzes->manage = '_JLMS_PROTOTYPE_QUIZZES_MANAGE';
		$permissions_quizzes->manage_pool = '_JLMS_PROTOTYPE_QUIZZES_MANAGE_POOL';
		$permissions_quizzes->role_types = array(1,2,4,5); // learner + teacher + admin + assistant
	$role['quizzes'] = & $permissions_quizzes;

		$permissions_links = new stdClass();
		$permissions_links->view = '_JLMS_PROTOTYPE_LINKS_VIEW';
		$permissions_links->view_all = '_JLMS_PROTOTYPE_LINKS_VIEW_ALL';
		$permissions_links->order = '_JLMS_PROTOTYPE_LINKS_ORDER';
		$permissions_links->publish = '_JLMS_PROTOTYPE_LINKS_PUBLISH';
		$permissions_links->manage = '_JLMS_PROTOTYPE_LINKS_MANAGE';
		$permissions_links->only_own_items = '_JLMS_PROTOTYPE_LINKS_ONLY_OWN_ITEMS';
		$permissions_links->only_own_role = '_JLMS_PROTOTYPE_LINKS_ONLY_OWN_ROLE';
		$permissions_links->role_types = array(1,2,4,5); // learner + teacher + admin + assistant
	$role['links'] = & $permissions_links;

		$permissions_lpaths = new stdClass();
		$permissions_lpaths->view = '_JLMS_PROTOTYPE_LPATHS_VIEW';
		$permissions_lpaths->view_all = '_JLMS_PROTOTYPE_LPATHS_VIEW_ALL';
		$permissions_lpaths->order = '_JLMS_PROTOTYPE_LPATHS_ORDER';
		$permissions_lpaths->publish = '_JLMS_PROTOTYPE_LPATHS_PUBLISH';
		$permissions_lpaths->manage = '_JLMS_PROTOTYPE_LPATHS_MANAGE';
		$permissions_lpaths->only_own_items = '_JLMS_PROTOTYPE_LPATHS_ONLY_OWN_ITEMS';
		$permissions_lpaths->only_own_role = '_JLMS_PROTOTYPE_LPATHS_ONLY_OWN_ROLE';
		$permissions_lpaths->role_types = array(1,2,4,5); // learner + teacher + admin + assistant
	$role['lpaths'] = & $permissions_lpaths;

		$permissions_announce = new stdClass();
		$permissions_announce->view = '_JLMS_PROTOTYPE_ANNOUNCE_VIEW';
		$permissions_announce->manage = '_JLMS_PROTOTYPE_ANNOUNCE_MANAGE';
		$permissions_announce->only_own_items = '_JLMS_PROTOTYPE_ANNOUNCE_ONLY_OWN_ITEMS';
		$permissions_announce->only_own_role = '_JLMS_PROTOTYPE_ANNOUNCE_ONLY_OWN_ROLE';
		$permissions_announce->role_types = array(1,2,3,4,5); // learner + teacher + ceo + admin + assistant
	$role['announce'] = & $permissions_announce;
	
		$permissions_dropbox = new stdClass();
		$permissions_dropbox->view = '_JLMS_PROTOTYPE_DROPBOX_VIEW';
		$permissions_dropbox->send_to_teachers = '_JLMS_PROTOTYPE_DROPBOX_SEND_TO_TEACHERS';
		$permissions_dropbox->send_to_learners = '_JLMS_PROTOTYPE_DROPBOX_SEND_TO_LEARNERS';
		$permissions_dropbox->mark_as_corrected = '_JLMS_PROTOTYPE_DROPBOX_MARK_AS_CORRECTED';
		$permissions_dropbox->role_types = array(1,2,4,5); // learner + teacher + admin + assistant
	$role['dropbox'] = & $permissions_dropbox;
	
		$permissions_homework = new stdClass();
		$permissions_homework->view = '_JLMS_PROTOTYPE_HOMEWORK_VIEW';
		$permissions_homework->view_stats = '_JLMS_PROTOTYPE_HOMEWORK_VIEW_STATS';
		$permissions_homework->view_all = '_JLMS_PROTOTYPE_HOMEWORK_VIEW_ALL';
		$permissions_homework->publish = '_JLMS_PROTOTYPE_HOMEWORK_PUBLISH';
		$permissions_homework->manage = '_JLMS_PROTOTYPE_HOMEWORK_MANAGE';		
		$permissions_homework->role_types = array(1,2,3,4,5); // learner + teacher + ceo + admin + assistant
	$role['homework'] = & $permissions_homework;
	
		$permissions_attendance = new stdClass();
		$permissions_attendance->view = '_JLMS_PROTOTYPE_ATTENDANCE_VIEW';
		$permissions_attendance->manage = '_JLMS_PROTOTYPE_ATTENDANCE_MANAGE';
		$permissions_attendance->role_types = array(1,2,3,4,5); // learner + teacher + ceo + admin + assistant
	$role['attendance'] = & $permissions_attendance;

		$permissions_chat = new stdClass();
		$permissions_chat->view = '_JLMS_PROTOTYPE_CHAT_VIEW';
		$permissions_chat->manage = '_JLMS_PROTOTYPE_CHAT_MANAGE';
		$permissions_chat->role_types = array(1,2,4,5); // learner + teacher + admin + assistant
	$role['chat'] = & $permissions_chat;

		$permissions_conf = new stdClass();
		$permissions_conf->view = '_JLMS_PROTOTYPE_CONFERENCE_VIEW';
		$permissions_conf->manage = '_JLMS_PROTOTYPE_CONFERENCE_MANAGE';
		$permissions_conf->role_types = array(1,2,4,5); // learner + teacher + admin + assistant
	$role['conference'] = & $permissions_conf;

		$permissions_gbook = new stdClass();
		$permissions_gbook->view = '_JLMS_PROTOTYPE_GRADEBOOK_VIEW';
		$permissions_gbook->manage = '_JLMS_PROTOTYPE_GRADEBOOK_MANAGE';
		if(isset($roletype_id) && ($roletype_id == 2|| $roletype_id == 4 || $roletype_id == 5)) {
			$permissions_gbook->configure = '_JLMS_PROTOTYPE_GRADEBOOK_CONFIGURE';
		}
		$permissions_gbook->role_types = array(1,2,3,4,5); // learner + teacher + ceo + admin + assistant
	$role['gradebook'] = & $permissions_gbook;

		$permissions_track = new stdClass();
		$permissions_track->manage = '_JLMS_PROTOTYPE_TRACKING_MANAGE';
		$permissions_track->clear_stats = '_JLMS_PROTOTYPE_TRACKING_CLEAR_STATS';
		//$permissions_track->view = 1;
		$permissions_track->role_types = array(2,4,5); // teacher + admin + assistant
	$role['tracking'] = & $permissions_track;

		$permissions_mail = new stdClass();
		$permissions_mail->view = '_JLMS_PROTOTYPE_MAILBOX_VIEW';
		$permissions_mail->manage = '_JLMS_PROTOTYPE_MAILBOX_MANAGE';
		$permissions_mail->role_types = array(1,2,4,5); // learner + teacher + admin + assistant
	$role['mailbox'] = & $permissions_mail;

		$permissions_users = new stdClass();
		$permissions_users->manage = '_JLMS_PROTOTYPE_USERS_MANAGE';
		$permissions_users->manage_teachers = '_JLMS_PROTOTYPE_USERS_MANAGE_TEACHERS';
		$permissions_users->view = '_JLMS_PROTOTYPE_USERS_VIEW';
		$permissions_users->import_users = '_JLMS_PROTOTYPE_USERS_IMPORTS_USERS';
		$permissions_users->role_types = array(2,3,4,5); // teacher + admin + assistant
	$role['users'] = & $permissions_users;

		$permissions_course = new stdClass();
		$permissions_course->edit = '_JLMS_PROTOTYPE_COURSE_EDIT';
		$permissions_course->manage_settings = '_JLMS_PROTOTYPE_COURSE_MANAGE_SETTINGS';
        $permissions_course->manage_topics = '_JLMS_PROTOTYPE_COURSE_MANAGE_TOPICS';
		$permissions_course->manage_certificates = '_JLMS_PROTOTYPE_COURSE_MANAGE_CERTIFICATES';
		$permissions_course->role_types = array(2,4,5); // teacher + admin + assistant
	$role['course'] = & $permissions_course;

		$permissions_library = new stdClass();
		$permissions_library->only_own_items = '_JLMS_PROTOTYPE_LIBRARY_ONLY_OWN_ITEMS';
		$permissions_library->role_types = array(2,4); // teacher + admin
	$role['library'] = & $permissions_library;
	
	$permissions_ceostore = new stdClass();
//		$permissions_ceostore->store_manager = "Store Manager";
//		$permissions_ceostore->corporate_admin = "Corporate Admin";
		$permissions_ceostore->view_all_users = '_JLMS_PROTOTYPE_CEO_VIEW_ALL_USERS';
		$permissions_ceostore->role_types = array(3); // ceo
	$role['ceo'] = & $permissions_ceostore;
	
	//$permissions_us = new stdClass();
	//	$permissions_us->switch_role = "_JLMS_PROTOTYPE_US_SWITCH_ROLE";
	//	$permissions_us->switch_language = "_JLMS_PROTOTYPE__US_SWITCH_LANGUAGE";
	//	$permissions_us->role_types = array(1,2,3,4,5); // learner + teacher + ceo + admin + assistant
	//$role['user_settings'] = & $permissions_us;

	return $role;
}
?>