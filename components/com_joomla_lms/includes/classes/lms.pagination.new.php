<?php
/**
* includes/classes/lms.pagination.php
* * * ElearningForce Inc
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

/**
* Page navigation support class
*/
jimport('joomla.html.pagination');
class JLMSPagination extends JPagination {
	function rowNumber($i) {
		return $this->getRowOffset($i);
	}
	
	public function getLimitBox($custom=null)
	{
		$app = JFactory::getApplication();
		
		if( JLMS_J16version() ) {
			// Initialise variables.
			$limits = array();

			// Make the option list.
			if($custom == 'cfu'){
				$limits[] = JHtml::_('select.option', '15');
			} else {
				for ($i = 5; $i <= 30; $i += 5)
				{
					$limits[] = JHtml::_('select.option', "$i");
				}
				$limits[] = JHtml::_('select.option', '50', JText::_('J50'));
				$limits[] = JHtml::_('select.option', '100', JText::_('J100'));
				$limits[] = JHtml::_('select.option', '0', JText::_('JALL'));
			}
			
			$selected = !empty($this->_viewall) ? 0 : $this->limit;

			// Build the select list.
			if ($app->isAdmin())
			{
				$html = JHtml::_(
					'select.genericlist',
					$limits,
					$this->prefix . 'limit',
					'class="inputbox" size="1" onchange="Joomla.submitform();"' . ($custom == 'cfu' ? ' '.'disabled="disabled"' : ''),
					'value',
					'text',
					$selected
				);
			}
			else
			{
				$html = JHtml::_(
					'select.genericlist',
					$limits,
					$this->prefix . 'limit',
					'class="inputbox" size="1" onchange="this.form.submit()"' . ($custom == 'cfu' ? ' '.'disabled="disabled"' : ''),
					'value',
					'text',
					$selected
				);
			}
		} else {
			$html = parent::getLimitBox();
		}
		return $html;
	}
	
	public function getListFooter($custom=null)
	{
		$app = JFactory::getApplication();

		$list = array();
		$list['prefix'] = $this->prefix;
		$list['limit'] = $this->limit;
		$list['limitstart'] = $this->limitstart;
		$list['total'] = $this->total;
		$list['limitfield'] = $this->getLimitBox($custom);
		$list['pagescounter'] = $this->getPagesCounter();
		$list['pageslinks'] = $this->getPagesLinks();

		$chromePath = JPATH_THEMES . '/' . $app->getTemplate() . '/html/pagination.php';
		if (file_exists($chromePath))
		{
			include_once $chromePath;
			if (function_exists('pagination_list_footer'))
			{
				return pagination_list_footer($list);
			}
		}
		return $this->_list_footer($list);
	}
}
?>