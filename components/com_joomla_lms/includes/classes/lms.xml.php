<?php 
/**
* includes/classes/lms.titles.php
* Joomla LMS Component
* * * ElearningForce DK
**/

defined( 'JPATH_BASE' ) or die( 'Restricted access' );

jimport('joomla.utilities.simplexml');
//jimport('joomla.utilities.xmlelement' );

//smt j3.0 update
if (!class_exists('JSimpleNewXML')&& JLMS_J30version())
{
if (!class_exists('JSimpleNewXMLElement'))
{
	class JSimpleNewXMLElement extends JXMLElement
	{

	public $_attributes = array();

	public $_name = '';

	public $_data = '';

	public $_children = array();

	public $_level = 0;


	public function name()
	{
		return $this->getname();
	}
/*
	public function attributes($attribute = null)
	{
		if (!isset($attribute))
		{
			return $this->_attributes;
		}
		return $this->attributes()->$attribute;
		//return isset($this->attributes([$attribute]) ? $this[$attribute] : null;
	}
*/
	public function data()
	{
		return (string)$this;
	}

	public function setData($data)
	{
		$this->_data = $data;
	}

	public function children($ns = NULL, $is_prefix = NULL)
	{
		$return = parent::children($ns, $is_prefix );
		return $return;
	}

	public function level()
	{
		
		return $this->_level;
	}

	public function getElementByPath($path)
	{
	
	//foreach ($this->$path->children() as $test) {
	//if ($test) $this->_children=$test->children();
	//echo 'SMT DEBUG: <pre>'; print_R($this->_children); echo '</pre>';
	//}
	//die;
		return $this->$path;
	}

	public function addAttribute($name, $value = NULL, $ns = NULL)
	{
		// Add the attribute to the element, override if it already exists
		$this->_attributes[$name] = $value;
	}

	public function removeAttribute($name)
	{
		unset($this->_attributes[$name]);
	}

	public function addChild($name, $attrs = array(), $level = null)
	{
		//If there is no array already set for the tag name being added,
		//create an empty array for it
		if (!isset($this->$name))
		{
			$this->$name = array();
		}

		// set the level if not already specified
		if ($level == null)
		{
			$level = ($this->_level + 1);
		}

		//Create the child object itself
		$classname = get_class($this);
		$child = new $classname($name, $attrs, $level);

		//Add the reference of it to the end of an array member named for the elements name
		$this->{$name}[] = &$child;

		//Add the reference to the children array member
		$this->_children[] = &$child;

		//return the new child
		return $child;
	}

	public function removeChild(&$child)
	{
		$name = $child->name();
		for ($i = 0, $n = count($this->_children); $i < $n; $i++)
		{
			if ($this->_children[$i] == $child)
			{
				unset($this->_children[$i]);
			}
		}
		for ($i = 0, $n = count($this->{$name}); $i < $n; $i++)
		{
			if ($this->{$name}[$i] == $child)
			{
				unset($this->{$name}[$i]);
			}
		}
		$this->_children = array_values($this->_children);
		$this->{$name} = array_values($this->{$name});
		unset($child);
	}


	public function map($callback, $args = array())
	{
		$callback($this, $args);
		// Map to all children
		if ($n = count($this->_children))
		{
			for ($i = 0; $i < $n; $i++)
			{
				$this->_children[$i]->map($callback, $args);
			}
		}
	}

	
	public function toString($whitespace = true)
	{

		// Start a new line, indent by the number indicated in $this->level, add a <, and add the name of the tag
		if ($whitespace)
		{
			$out = "\n" . str_repeat("\t", $this->_level) . '<' . $this->_name;
		}
		else
		{
			$out = '<' . $this->_name;
		}

		// For each attribute, add attr="value"
		foreach ($this->_attributes as $attr => $value)
		{
			$out .= ' ' . $attr . '="' . htmlspecialchars($value, ENT_COMPAT, 'UTF-8') . '"';
		}

		// If there are no children and it contains no data, end it off with a />
		if (empty($this->_children) && empty($this->_data))
		{
			$out .= " />";
		}
		// Otherwise...
		else
		{
			// If there are children
			if (!empty($this->_children))
			{
				// Close off the start tag
				$out .= '>';

				// For each child, call the asXML function (this will ensure that all children are added recursively)
				foreach ($this->_children as $child)
				{
					$out .= $child->toString($whitespace);
				}

				// Add the newline and indentation to go along with the close tag
				if ($whitespace)
				{
					$out .= "\n" . str_repeat("\t", $this->_level);
				}
			}
			// If there is data, close off the start tag and add the data
			elseif (!empty($this->_data))
				$out .= '>' . htmlspecialchars($this->_data, ENT_COMPAT, 'UTF-8');

			// Add the end tag
			$out .= '</' . $this->_name . '>';
		}

		//Return the final output
		return $out;
	}

	}
}
//

class JSimpleNewXML extends JObject
	{
	
		public function getXML($data, $isFile = true)
			{
				jimport('joomla.utilities.xmlelement');

				// Disable libxml errors and allow to fetch error information as needed
				libxml_use_internal_errors(true);

				if ($isFile)
				{
					// Try to load the XML file
					$xml = simplexml_load_file($data, 'JSimpleNewXMLElement', LIBXML_NOCDATA );
				}
				else
				{
					// Try to load the XML string
					$xml = simplexml_load_string($data, 'JXMLElement');
				}

				if (empty($xml))
				{
					// There was an error
					JError::raiseWarning(100, JText::_('JLIB_UTIL_ERROR_XML_LOAD'));

					if ($isFile)
					{
						JError::raiseWarning(100, $data);
					}

					foreach (libxml_get_errors() as $error)
					{
						JError::raiseWarning(100, 'XML: ' . $error->message);
					}
				}

				return $xml;
		}
		public function getElementByPath($el)
		{
			return $this->$el;
		}
	}
}
if (!class_exists('JSimpleXML'))
{
	class JSimpleXML extends JObject
	{}
}
//
class JLMSXML extends JSimpleXML
{
	function __construct($options = null)
	{
		parent::__construct( $options );
	}

}

//smt j3.0 update
if (!class_exists('JSimpleXMLElement'))
{
	class JSimpleXMLElement extends JSimpleXML
{
	function __construct($options = null)
	{
		parent::__construct( $options );
	}
	function name() {
		return $this->getname();
	}
}
}
//

class JLMSXMLElement extends JSimpleXMLElement
{
	function __construct($name, $attrs = array(), $level = 0)
	{
		parent::__construct($name, $attrs, $level );
	}

	function &getElementByPath($path)
	{	
		$ref = & parent::getElementByPath($path);
			
		if ( !$ref ) {					
			$classname = get_class( $this );
			$ref = new $classname('empty');
		}
				
		return $ref;
	}
}
?>