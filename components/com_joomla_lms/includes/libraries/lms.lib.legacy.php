<?php
/**
* lms_legacy.lib.php
* Joomla LMS Component
* * * ElearningForce DK
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

if (!defined('_PN_DISPLAY_NR')) {
	DEFINE('_PN_DISPLAY_NR','Display #');
}
if (!defined('_SEL_CATEGORY')) {
	DEFINE('_SEL_CATEGORY','- Select Category -');
}
if (!defined('_CMN_YES')) {
	DEFINE('_CMN_YES','Yes');
}
if (!defined('_CMN_NO')) {
	DEFINE('_CMN_NO','No');
}

function JLMS_adjust_memory_limit($new_limit = 16) {
	$mem_row = array(16, 32, 48, 64, 80, 96, 128, 160, 192, 224, 256);
	$AvailMem = trim( @ini_get( 'memory_limit' ) );
	if ($AvailMem) {
		$bytes_sign = strtolower( $AvailMem{strlen( $AvailMem ) - 1} );
		switch($bytes_sign) {
			case 'g':
				$AvailMem *= 1024;
			case 'm':
				$AvailMem *= 1024;
			case 'k':
				$AvailMem *= 1024;
		}

		foreach ($mem_row as $mem_val) {
			if ($mem_val <= $new_limit) {
				if ($AvailMem < $mem_val*1000000) {
					$new_limit_var = $mem_val.'M';
					@ini_set( 'memory_limit', $new_limit_var );
				}
			} else {
				break;
			}
		}
	}
}

// Joomla 1.5 Legacy mode compatibility
function JLMSRedirect($url, $msg='', $persistMsg = true) {
	if ($msg == '_clear_') {
		global $JLMS_SESSION;
		$JLMS_SESSION->clear('joomlalms_sys_message');
	} elseif ($msg) {
		global $JLMS_SESSION;
		$JLMS_SESSION->set('joomlalms_sys_message', $msg);
	}

	global $JLMS_CONFIG;
	if ($JLMS_CONFIG->get('debug_mode', false)) {
		global $JLMS_DB;
		if ( method_exists($JLMS_DB, "get_jlms_debug")) {
			global $JLMS_SESSION;
			$jlms_db_log = $JLMS_DB->get_jlms_debug();
			$jlms_db_logo = array();
			$jlms_db_logo[] = '<pre>Total queries at previous session: '. $JLMS_DB->get_jlms_ticker().'</pre>';
			if (!empty($jlms_db_log)) {
				$jlms_db_logo = array_merge($jlms_db_logo, $jlms_db_log);
			}
			$JLMS_SESSION->set('joomlalms_prev_debug_log', $jlms_db_logo );
		}
	}

	$url = str_replace( '&amp;', '&', $url );
	
	mosRedirect($url, '', $persistMsg);
	exit();
}
function JLMSAppendPathWay($pathway = array()) {
	global $JLMS_CONFIG;
	
	$pathway_show_lmshome = $JLMS_CONFIG->get('pathway_show_lmshome', false);
	$pathway_show_coursehome = $JLMS_CONFIG->get('pathway_show_coursehome', true);
	
	foreach ($pathway as $pitem) {
		if ( isset($pitem['is_home']) && $pitem['is_home']) {
			if( $pathway_show_lmshome)			
				JLMSAppendPathWayItems($pitem['name'], $pitem['link']);
		}
		elseif(isset($pitem['is_course']) && $pitem['is_course']) {
			if( $pathway_show_coursehome && $pitem['name'])			
				JLMSAppendPathWayItems($pitem['name'], $pitem['link']);
		}
		else {
			JLMSAppendPathWayItems($pitem['name'], $pitem['link']);
		}
	}
}
function JLMSAppendPathWayItems($name, $link) {
	if (class_exists('JFactory')) { //Joomla 1.5 +
		$app = JFactory::getApplication();
		$pathway = $app->getPathway();
		$pathway->addItem($name, $link);
	} else {
		
	}
}

// steal from CB (sorry guys)
// used in 'joomla_lms.course_users.php' at line 857
function JLMS_HashPassword( $passwd, $row = null ) {
	global $_VERSION, $JLMS_DB;
	$method	= 'md5';
	if (JLMS_Jversion() == 1) {
		$method	= 'md5salt';
		$saltLength	= 16;	
	} elseif (JLMS_Jversion() == 2) {
		$method = 'md5salt';
		$saltLength = 32;
	}
	switch ( $method ) {
		case 'md5salt':
			if ( $row ) {
				$parts			=	explode( ':', $row->password );
				if ( count( $parts ) > 1 ) {
					$salt		=	$parts[1];
				} else {
					// check password, if ok, auto-upgrade $JLMS_DB:
					$salt		=	JLMS_MakeRandomString( $saltLength );
					$crypt		=	md5( $passwd . $salt );
					$hashedPwd	=	$crypt. ':' . $salt;
					if ( md5( $passwd ) === $row->password ) {
						$query	= "UPDATE #__users SET password = '"
								. JLMSDatabaseHelper::GetEscaped( $hashedPwd ) . "'"
								. " WHERE id = " . (int) $row->id;
						$JLMS_DB->setQuery( $query );
						$JLMS_DB->query();
						$row->password	=	$hashedPwd;
					}
				}
			} else {
				$salt		=	JLMS_MakeRandomString( $saltLength );
			}
			$crypt			=	md5( $passwd . $salt );
			$hashedPwd		=	$crypt. ':' . $salt;
			break;
		case 'md5':
		default:
			if ( $row ) {
				$parts			=	explode( ':', $row->password );
				if ( count( $parts ) > 1 ) {
					// check password, if ok, auto-downgrade $JLMS_DB:
					$salt		=	$parts[2];
					$crypt		=	md5( $passwd . $salt );
					$hashedPwd	=	$crypt. ':' . $salt;
					if ( $hashedPwd === $row->password ) {
						$hashedPwd	=	md5( $passwd );
						$query	= "UPDATE #__users SET password = '"
								. JLMSDatabaseHelper::GetEscaped( $hashedPwd ) . "'"
								. " WHERE id = " . (int) $row->id;
						$JLMS_DB->setQuery( $query );
						$JLMS_DB->query();
						$row->password	=	$hashedPwd;
					}
				}
			}
			$hashedPwd		=	md5( $passwd );
			break;
	}
	if ( $row ) {
		return ( $hashedPwd === $row->password );
	} else {
		return $hashedPwd;
	}
}
// steal from CB (sorry guys)
function JLMS_MakeRandomString( $stringLength = 8, $noCaps = false ) {
	if ( $noCaps ) {
		$chars		=	'abchefghjkmnpqrstuvwxyz0123456789';
	} else {
		$chars		=	'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	}
	$len			=	strlen( $chars );
	$rndString		=	'';
	mt_srand( 10000000 * (double) microtime() );
	for ( $i = 0; $i < $stringLength; $i++ ) {
		$rndString	.=	$chars[mt_rand( 0, $len - 1 )];
	}
	return $rndString;
}

function JLMS_getCryptedPassword($plaintext, $salt = '', $encryption = 'md5-hex', $show_encrypt = false) {
	// Get the salt to use.
	$salt = JLMS_getSalt($encryption, $salt, $plaintext);

	// Encrypt the password.
	switch ($encryption)
	{
		case 'plain' :
			return $plaintext;

		case 'sha' :
			$encrypted = base64_encode(mhash(MHASH_SHA1, $plaintext));
			return ($show_encrypt) ? '{SHA}'.$encrypted : $encrypted;

		case 'crypt' :
		case 'crypt-des' :
		case 'crypt-md5' :
		case 'crypt-blowfish' :
			return ($show_encrypt ? '{crypt}' : '').crypt($plaintext, $salt);

		case 'md5-base64' :
			$encrypted = base64_encode(mhash(MHASH_MD5, $plaintext));
			return ($show_encrypt) ? '{MD5}'.$encrypted : $encrypted;

		case 'ssha' :
			$encrypted = base64_encode(mhash(MHASH_SHA1, $plaintext.$salt).$salt);
			return ($show_encrypt) ? '{SSHA}'.$encrypted : $encrypted;

		case 'smd5' :
			$encrypted = base64_encode(mhash(MHASH_MD5, $plaintext.$salt).$salt);
			return ($show_encrypt) ? '{SMD5}'.$encrypted : $encrypted;

		case 'aprmd5' :
			$length = strlen($plaintext);
			$context = $plaintext.'$apr1$'.$salt;
			$binary = JLMS__bin(md5($plaintext.$salt.$plaintext));

			for ($i = $length; $i > 0; $i -= 16) {
				$context .= substr($binary, 0, ($i > 16 ? 16 : $i));
			}
			for ($i = $length; $i > 0; $i >>= 1) {
				$context .= ($i & 1) ? chr(0) : $plaintext[0];
			}

			$binary = JLMS__bin(md5($context));

			for ($i = 0; $i < 1000; $i ++) {
				$new = ($i & 1) ? $plaintext : substr($binary, 0, 16);
				if ($i % 3) {
					$new .= $salt;
				}
				if ($i % 7) {
					$new .= $plaintext;
				}
				$new .= ($i & 1) ? substr($binary, 0, 16) : $plaintext;
				$binary = JLMS__bin(md5($new));
			}

			$p = array ();
			for ($i = 0; $i < 5; $i ++) {
				$k = $i +6;
				$j = $i +12;
				if ($j == 16) {
					$j = 5;
				}
				$p[] = JLMS__toAPRMD5((ord($binary[$i]) << 16) | (ord($binary[$k]) << 8) | (ord($binary[$j])), 5);
			}

			return '$apr1$'.$salt.'$'.implode('', $p).JLMS__toAPRMD5(ord($binary[11]), 3);

		case 'md5-hex' :
		default :
			$encrypted = ($salt) ? md5($plaintext.$salt) : md5($plaintext);
			return ($show_encrypt) ? '{MD5}'.$encrypted : $encrypted;
	}
}

function JLMS_getSalt($encryption = 'md5-hex', $seed = '', $plaintext = '')	{
	// Encrypt the password.
	switch ($encryption)
	{
		case 'crypt' :
		case 'crypt-des' :
			if ($seed) {
				return substr(preg_replace('|^{crypt}|i', '', $seed), 0, 2);
			} else {
				return substr(md5(mt_rand()), 0, 2);
			}
			break;

		case 'crypt-md5' :
			if ($seed) {
				return substr(preg_replace('|^{crypt}|i', '', $seed), 0, 12);
			} else {
				return '$1$'.substr(md5(mt_rand()), 0, 8).'$';
			}
			break;

		case 'crypt-blowfish' :
			if ($seed) {
				return substr(preg_replace('|^{crypt}|i', '', $seed), 0, 16);
			} else {
				return '$2$'.substr(md5(mt_rand()), 0, 12).'$';
			}
			break;

		case 'ssha' :
			if ($seed) {
				return substr(preg_replace('|^{SSHA}|', '', $seed), -20);
			} else {
				return mhash_keygen_s2k(MHASH_SHA1, $plaintext, substr(pack('h*', md5(mt_rand())), 0, 8), 4);
			}
			break;

		case 'smd5' :
			if ($seed) {
				return substr(preg_replace('|^{SMD5}|', '', $seed), -16);
			} else {
				return mhash_keygen_s2k(MHASH_MD5, $plaintext, substr(pack('h*', md5(mt_rand())), 0, 8), 4);
			}
			break;

		case 'aprmd5' :
			/* 64 characters that are valid for APRMD5 passwords. */
			$APRMD5 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

			if ($seed) {
				return substr(preg_replace('/^\$apr1\$(.{8}).*/', '\\1', $seed), 0, 8);
			} else {
				$salt = '';
				for ($i = 0; $i < 8; $i ++) {
					$salt .= $APRMD5 {
						rand(0, 63)
						};
				}
				return $salt;
			}
			break;

		default :
			//if ($seed) {
				$salt = $seed;
			//}
			return $salt;
			break;
	}
}
function JLMS__toAPRMD5($value, $count)	{
	/* 64 characters that are valid for APRMD5 passwords. */
	$APRMD5 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

	$aprmd5 = '';
	$count = abs($count);
	while (-- $count) {
		$aprmd5 .= $APRMD5[$value & 0x3f];
		$value >>= 6;
	}
	return $aprmd5;
}

function JLMS__bin($hex) {
	$bin = '';
	$length = strlen($hex);
	for ($i = 0; $i < $length; $i += 2) {
		$tmp = sscanf(substr($hex, $i, 2), '%x');
		$bin .= chr(array_shift($tmp));
	}
	return $bin;
}

function JLMS_fetch_user_ip() {
	return $_SERVER['REMOTE_ADDR'];
}

function JLMS_fetch_alt_user_ip() {
	$alt_ip = $_SERVER['REMOTE_ADDR'];
	if (isset($_SERVER['HTTP_CLIENT_IP'])) {
		$alt_ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && preg_match_all('#\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}#s', $_SERVER['HTTP_X_FORWARDED_FOR'], $matches)) {
		// make sure we dont pick up an internal IP defined by RFC1918
		foreach ($matches[0] AS $ip) {
			if (!preg_match("#^(10|172\.16|192\.168)\.#", $ip)) {
				$alt_ip = $ip;
				break;
			}
		}
	} elseif (isset($_SERVER['HTTP_FROM'])) {
		$alt_ip = $_SERVER['HTTP_FROM'];
	}
	return $alt_ip;
} 
?>