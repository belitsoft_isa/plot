<?php
// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

function JLMSEmailRoute($url) {
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$url = JRoute::_($url, false);
	$pos = strpos($url, 'http');
	if ($pos !== 0) {
		$del_part = JURI::root(true);
		$live_site_part = $JLMS_CONFIG->get('live_site');
		if (strlen($del_part)) {
			$live_site_part = str_replace(JURI::root(true).'/','',JURI::root(false));
		}
		$url = $live_site_part.$url;
	}
	return $url;
}

function php4_clone($object) {
	if (version_compare(phpversion(), '5.0') < 0) {
		return $object;
	} else {
		return @clone($object);
	}
}

function JLMS_removeBOM( $filename ) 
{
	//changed Max - 26.04.2012
	//fix BOM	
	$f = fopen($filename,'rb');
    $t = fread($f, filesize($filename));
	if (preg_match('|\xEF\xBB\xBF|', $t) !== 0){
		if (is_writable($filename) === true) {
            $data = preg_replace('|\xEF\xBB\xBF|', "", $t);
            $file = fopen($filename,'w+b');
            if (fwrite($file, $data) === FALSE) {
				return false;
            }
            fclose($file);
        }
	}
	//fix BOM
}

if(!function_exists('scandir')) {
	function scandir($dir) {
		$files = array(); // added to initialize variable
		if ($dh = opendir($dir)) {
			while(false !== ($filename = readdir($dh))) {
				if($filename == '.' || $filename == '..')
					continue;
				else
					$files[] = $filename;
			}
			closedir($dh);
		}
		return $files;
	}
}
?>