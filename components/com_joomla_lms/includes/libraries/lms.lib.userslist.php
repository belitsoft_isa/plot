<?php

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

class Helper_UsersList{
	
	static function getSelectUser_BECertificates(){
			
		$app = & JFactory::getApplication();	
		$doc = & JFactory::getDocument();
		
		JLMS_HTML::_('behavior.mootools');
		JHTML::_('behavior.modal');
		
		$js = '
			function jSelectUser(id, name){
				if($("user")){
					if($("userdata")){
						$("user").removeChild($("userdata"));
					}
					$("user").innerHTML = \'<div id="userdata">\'+name+\'<input type="hidden" name="filt_user" value="\'+id+\'" /></div>\';
					if(id && $("userdata")){
						$("userdata").setStyle("margin-right", "10px");
					}
				}
				if(document.adminForm){
					var form = document.adminForm;
					form.submit();
				}
				SqueezeBox.close();
			}
		';
		$doc->addScriptDeclaration($js);
		
		$course_id = JRequest::getVar('course_id', 0);
		$user_id = JRequest::getVar('user_id', 0);
		if(!$user_id){
			$user_id = intval($app->getUserStateFromRequest( "filt_user{$option}", 'filt_user', 0 ));
		}
		
		$db = & JFactory::getDBO();
		
		$query = "SELECT *"
		. "\n FROM #__users"
		. "\n WHERE 1"
		. "\n AND id = '".$user_id."'"
		;
		$db->setQuery($query);
		$user_data = $db->loadObject();
		
		ob_start();
		$link = 'index.php?option=com_joomla_lms&tmpl=component&task=users_list&custom=cfu';
		if($course_id){
			$link .= '&course_id='.$course_id;
		}
		if(JRequest::getVar('filt_course_id')){
			$link .= '&filt_course_id='.JRequest::getVar('filt_course_id');
		}
		if(JRequest::getVar('filt_group')){
			$link .= '&filt_group='.JRequest::getVar('filt_group');
		}
		?>
		<div style="float: left;">
			<div id="user" style="float: left;">
			<?php
			if(isset($user_data->id) && $user_data->id){
				?>
				<div id="userdata" style="float: left; margin-right: 10px;">
					<?php
					echo $user_data->name.', ('.$user_data->username.', '.$user_data->email.')';
					?>
					<input type="hidden" value="<?php echo $user_data->id;?>" name="filt_user">
				</div>
				<?php
			}
			?>
			</div>
			<div id="blank" style="float: left;">
				<a rel="{handler:'iframe', size:{x:975, y:625}}"  href="<?php echo $link;?>" class="modal" >[<?php echo _JLMS_CHOOSE;?>]</a>
				<a href="javascript: void(0);" onclick="if(document.adminForm.filt_user){document.adminForm.filt_user.value=0;document.adminForm.submit();}">[Clear]</a>
			</div>
		</div>
		<?php
		$html = ob_get_contents();
		ob_end_clean();
		
		return $html;
	}
	
	static function getSelectUser($multi=true){
		global $JLMS_CONFIG;
		
		JLMS_HTML::_('behavior.mootools');		
		JHTML::_('behavior.modal');
		
		$doc = JFactory::getDocument();
		/*old
		$js = '
			function jSelectUser(id, name){
				if($("user")){
					if($("userdata")){
						$("user").removeChild($("userdata"));
					}
					$("user").innerHTML = \'<div id="userdata">\'+name+\'<input type="hidden" name="user_id" value="\'+id+\'" /></div>\';
					if(id){
						$("blank").setStyle("margin-left", "10px");
					}
				}
				SqueezeBox.close();
			}
		';*/
		
		$img_del_user = '';
		if (!JLMS_J30version()) $img_del_user = '<img src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/btn_delete.png" />';
		if ($multi==false)
		{
			$js = '
			function jSelectUser(id, name){
				if($("user")){
					if($("userdata")){
						$("user").removeChild($("userdata"));
					}
					$("user").innerHTML = \'<div id="userdata" class="btn userinfo search-choice">\'+name+\'<input type="hidden" name="user_id" value="\'+id+\'" /></div>\';
					if(id){
						$("blank").setStyle("margin-left", "10px");
					}
				}
				SqueezeBox.close();
			}
			';
		} else
		{
		$js = '
			function deleteFromList(id)
			{
				if($("user")){
					$("user").removeChild($("userdata_"+id));
				}
				var uids_str = document.getElementById("user_ids").value;
				var uids = uids_str.split(",");
				var uids_to_hidden = [];	
				var uids_to_hidden_str = "";
				
				for (var i=0;i<uids.length;i++)
				{
					if (uids[i]!=id) 
					{
						uids_to_hidden.push(uids[i]);
					}
				}
				uids_to_hidden_str = uids_to_hidden.join (\',\');
				if (uids_to_hidden_str[0]==",") uids_to_hidden_str = uids_to_hidden_str.substr(1);
				document.getElementById("user_ids").value = uids_to_hidden_str;
			}
			
			function jSelectUser(id, name){
				if (!id) return false;
				var uids_str = document.getElementById("user_ids").value;
				var uids = uids_str.split(",");
				var uids_to_hidden = uids;	
				var uids_to_hidden_str = "";	
				for (var i=0;i<uids.length;i++)
				{
					if (uids[i]==id) 
					{
						alert(\'This user is already selected\');
						return false;
					}
				}
				uids_to_hidden.push(id);
				
				$("user").innerHTML = $("user").innerHTML+\'<li class="btn userinfo search-choice" id="userdata_\'+id+\'">\'+name+\'<a class="search-choice-close" href="javascript:void(0);" onclick="deleteFromList(\'+id+\');">'.$img_del_user.'</a></li>\';
				if(id){
					$("blank").setStyle("margin-left", "10px");
				}
				uids_to_hidden_str = uids_to_hidden.join (\',\');
				if (uids_to_hidden_str[0]==",") uids_to_hidden_str = uids_to_hidden_str.substr(1);
				document.getElementById("user_ids").value = uids_to_hidden_str;
				 
				SqueezeBox.close();
			}
		';
		}
		$doc->addScriptDeclaration($js);
		
		$course_id = JRequest::getVar('course_id', 0);
		
		$user_ids = JRequest::getVar('user_ids');
		
		ob_start();
		$link = 'index.php?option=com_joomla_lms&tmpl=component&task=users_list';
		if($course_id){
			$link .= '&course_id='.$course_id;
		}
		?>
		<div style="float: left;" class="chzn-container chzn-container-multi">
			<div id="user" style="float: left;" class="chzn-choices">
				<?php
				if (isset($user_ids))
				{
				$db = JFactory::getDBO();
				$uids = explode(',',$user_ids);
					if (sizeof($uids))
					{
						foreach ($uids as $user_id)
						{
							$user_id = (int)$user_id;
							if ($user_id)
							{
								$query = "SELECT name,username,email FROM #__users WHERE id=$user_id";
								$db->setQuery($query);
								$user_already_added = $db->loadObject(); 
								if (isset($user_already_added->username))
								{
									echo '<li class="btn userinfo search-choice" id="userdata_'.$user_id.'">'.$user_already_added->name.',('.$user_already_added->username.', '.$user_already_added->email.')<a class="search-choice-close" href="javascript:void(0);" onclick="deleteFromList('.$user_id.');">'.$img_del_user.'</a></li>';
								}
							}
						}
					}
				}
				?>
			</div>
			<input type="hidden" id="user_ids" name="user_ids" value="<?php echo $user_ids;?>" />
			<?php 
			if (JLMS_J30version())
			{
			?>
				<div id="blank" style="float: left;"><a rel="{handler:'iframe', size:{x:800, y:600}}"  href="<?php echo $link;?>" class="modal btn btn-primary modal_jform_created_by" >[<?php echo _JLMS_CHOOSE;?> <i class="icon-user"></i>]</a></div>
			<?php
			} else
			{
				?>
				<div id="blank" style="float: left;"><a rel="{handler:'iframe', size:{x:800, y:600}}"  href="<?php echo $link;?>" class="modal" >[<?php echo _JLMS_CHOOSE;?>]</a></div>
				<?php
			}?>
		</div>
		<?php
		$html = ob_get_contents();
		ob_end_clean();
		
		return $html;
	}
	static function getUsers(){
		global $option;
		
		$JVersion = new JVersion();
		$jver = str_replace('.', '', $JVersion->getHelpVersion());
		
		$app = JFactory::getApplication();
		$JLMS_DB = & JLMSFactory::getDB();
		
		$doc = JFactory::getDocument();
		$addstyle = 'body.component {overflow-y: auto !important; padding-top: 0 !important;}'; 
		$doc->addStyleDeclaration( $addstyle );

		$filter_order	= $app->getUserStateFromRequest( "view{$option}usersfilter_order",'filter_order','name','cmd' );
		$filter_order_Dir	= $app->getUserStateFromRequest( "view{$option}usersfilter_order_Dir",'filter_order_Dir','','word' ); 
		$limit = $app->getUserStateFromRequest('global.list.limit', 'limit',15,'int');
		$limitstart	= $app->getUserStateFromRequest("view{$option}userslimitstart",'limitstart',0,'int');
		
		//$default_group_id = $jver >= 16 ? 2 : 18;
		$default_group_id = 0;
		
		$course_id = JRequest::getVar('course_id', 0);
		$group_id = JRequest::getVar('search_group', $default_group_id);
		$search_username = JRequest::getVar('search_username', '');
		$search_name = JRequest::getVar('search_name', '');
		$search_email = JRequest::getVar('search_email', '');
		
		$custom = JRequest::getVar('custom', null);
		
		if($jver >= 16){
			$query = "SELECT id as value, title as text FROM #__usergroups";
		} else {
			$query = "SELECT id as value, name as text FROM #__core_acl_aro_groups WHERE id NOT IN (17, 28)";
		}
		$JLMS_DB->setQuery($query);
		$groups = $JLMS_DB->loadObjectList();
		
		$list_groups = array();
		$list_groups[] = mosHTML::makeOption(0, '&nbsp;');
		$list_groups = array_merge( $list_groups, $groups );
		$lists['search_group'] = JHTML::_('select.genericlist', $list_groups, 'search_group', 'class="text_area" style="width:100%" size="1" onchange="onSearch();"', 'value', 'text', $group_id );
		
		$lists['search_username'] = $search_username;
		$lists['search_name'] = $search_name;
		$lists['search_email'] = $search_email;
		
		$where = '';
		$group = '';
		$order = '';
		if($jver >= 16){
			$where .= ' AND a.id = b.user_id';
			$where .= ' AND b.group_id = c.id';
			$where .= $group_id ? ' AND c.id = '.$group_id : '';
			$where .= $search_username ? ' AND a.username LIKE "%'.$search_username.'%"' : '';
			$where .= $search_name ? ' AND a.name LIKE "%'.$search_name.'%"' : '';
			$where .= $search_email ? ' AND a.email LIKE "%'.$search_email.'%"' : '';
			//$where .= $course_id ? ' AND a.id NOT IN (SELECT user_id FROM #__lms_users_in_groups WHERE course_id = '.$course_id.' AND role_id > 0)' : '';
			
			if($custom == 'cfu'){ //certificates_filter_user
				$where .= ' AND a.id = luig.user_id';
				$where .= ' AND luig.course_id IN (SELECT lc.id FROM #__lms_courses as lc)';
			}
			
			$order .= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;
			
			$query = "SELECT"
				. " COUNT(*)"
			. " FROM"
				. " #__users as a,"
				. " #__user_usergroup_map as b,"
				. " #__usergroups as c"
				.($custom == 'cfu' ? "\n, #__lms_users_in_groups as luig" : '')
			. " WHERE 1"
			. $where
			. $group
			. $order
			;
		} else {
			$where .= ' AND a.id = b.value';
			$where .= ' AND b.id = d.aro_id';
			$where .= ' AND d.group_id = c.id';
			$where .= $group_id ? ' AND c.id = '.$group_id : '';
			$where .= $search_username ? ' AND a.username LIKE "%'.$search_username.'%"' : '';
			$where .= $search_name ? ' AND a.name LIKE "%'.$search_name.'%"' : '';
			$where .= $search_email ? ' AND a.email LIKE "%'.$search_email.'%"' : '';
			//$where .= $course_id ? ' AND a.id NOT IN (SELECT user_id FROM #__lms_users_in_groups WHERE course_id = '.$course_id.' AND role_id > 0)' : '';
			
			if($custom == 'cfu'){
				$where .= ' AND a.id = luig.user_id';
				$where .= ' AND luig.course_id IN (SELECT lc.id FROM #__lms_courses as lc)';
			}
			
			$order .= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;
			
			$query = "SELECT"
				. " COUNT(*)"
			. " FROM"
				. " #__users as a,"
				. " #__core_acl_aro as b,"
				. " #__core_acl_aro_groups as c,"
				. " #__core_acl_groups_aro_map as d"
				.($custom == 'cfu' ? "\n, #__lms_users_in_groups as luig" : '')
			. " WHERE 1"
			. $where
			. $group
			. $order
			;
		}
		$JLMS_DB->setQuery($query);
		$total = $JLMS_DB->loadResult();
		
		//jimport('joomla.html.pagination');
		//$pageNav = new JPagination( $total, $limitstart, $limit );
		
		require_once( JPATH_SITE . DS . 'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'classes'.DS.'lms.pagination.new.php');
		$pageNav = new JLMSPagination( $total, $limitstart, $limit  );
		
		$where = '';
		$group = '';
		$group .= ' GROUP BY a.id';
		$order = '';
		if($jver >= 16){
			$where .= ' AND a.id = b.user_id';
			$where .= ' AND b.group_id = c.id';
			$where .= $group_id ? ' AND c.id = '.$group_id : '';
			$where .= $search_username ? ' AND a.username LIKE "%'.$search_username.'%"' : '';
			$where .= $search_name ? ' AND a.name LIKE "%'.$search_name.'%"' : '';
			$where .= $search_email ? ' AND a.email LIKE "%'.$search_email.'%"' : '';
			//$where .= $course_id ? ' AND a.id NOT IN (SELECT user_id FROM #__lms_users_in_groups WHERE course_id = '.$course_id.' AND role_id > 0)' : '';
			
			if($custom == 'cfu'){
				$where .= ' AND a.id = luig.user_id';
				$where .= ' AND luig.course_id IN (SELECT lc.id FROM #__lms_courses as lc)';
			}
			
			$order .= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;
			
			$query = "SELECT"
				. " a.*,"
				. " c.title as groupname"
			. " FROM"
				. " #__users as a,"
				. " #__user_usergroup_map as b,"
				. " #__usergroups as c"
				.($custom == 'cfu' ? "\n, #__lms_users_in_groups as luig" : '')
			. " WHERE 1"
			. $where
			. $group
			. $order
			;
		} else {
			$where .= ' AND a.id = b.value';
			$where .= ' AND b.id = d.aro_id';
			$where .= ' AND d.group_id = c.id';
			$where .= $group_id ? ' AND c.id = '.$group_id : '';
			$where .= $search_username ? ' AND a.username LIKE "%'.$search_username.'%"' : '';
			$where .= $search_name ? ' AND a.name LIKE "%'.$search_name.'%"' : '';
			$where .= $search_email ? ' AND a.email LIKE "%'.$search_email.'%"' : '';
			//$where .= $course_id ? ' AND a.id NOT IN (SELECT user_id FROM #__lms_users_in_groups WHERE course_id = '.$course_id.' AND role_id > 0)' : '';
			
			if($custom == 'cfu'){
				$where .= ' AND a.id = luig.user_id';
				$where .= ' AND luig.course_id IN (SELECT lc.id FROM #__lms_courses as lc)';
			}
			
			$order .= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;
			
			$query = "SELECT"
				. " a.*,"
				. " c.name as groupname"
			. " FROM"
				. " #__users as a,"
				. " #__core_acl_aro as b,"
				. " #__core_acl_aro_groups as c,"
				. " #__core_acl_groups_aro_map as d"
				.($custom == 'cfu' ? "\n, #__lms_users_in_groups as luig" : '')
			. " WHERE 1"
			. $where
			. $group
			. $order
			;
		}
		$JLMS_DB->setQuery( $query, $pageNav->limitstart, $pageNav->limit);
		$rows = $JLMS_DB->loadObjectList();
		
		$lists['order_Dir'] = $filter_order_Dir;
		$lists['order']	= $filter_order;
		$lists['custom'] = $custom;
		
		$ClientId = $app->getClientId();
		if($ClientId){
			JLMS_BACKEND_USERS_LISTS_HTML::showUsersList($rows, $pageNav, $lists);
		} else {
			JLMS_FRONTEND_USERS_LISTS_HTML::showUsersList($rows, $pageNav, $lists);
		}
	}							
}