<?php
/**
* smf.php
* JoomlaLMS Component
* * * ElearningForce Inc.
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

class JLMS_SMF 
{
	var $smf_db = null;
		
	var $boardurl = null;
	
	var $parsedurl = null;
		
	var $cookiename = null;	
	
	var $mbname = null;
	
	var $modSettings = null;
	
	function JLMS_SMF() 
	{		
		$this->_createDBO();
		
		$query = "SELECT variable, value FROM #__settings";
		$this->smf_db->setQuery( $query );
		$this->modSettings = $this->smf_db->loadAssocList('variable', 'value');
			
		$this->parsedurl = parse_url($this->boardurl);		 
	}
	
	static function &getInstance()
	{
		$JLMS_CONFIG = & JLMSFactory::GetConfig();
				
		static $instance;

		if ( !is_object( $instance ) && file_exists( $JLMS_CONFIG->get('forum_path').'/Settings.php' ) )
		{			
			$smf_version = '1';
			
			if( file_exists( $JLMS_CONFIG->get('forum_path').'/Sources/ScheduledTasks.php' ) ) {					
				$smf_version = '2';
			}
			
			if( $smf_version == '1' ) 
			{
				require_once( JPATH_SITE.DS.'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'forums'.DS.'smf'.DS.'smf_v1.php');			
				$instance = new JLMS_SMF_V1();				
								
			} else if( $smf_version == '2' ) 
			{
				require_once( JPATH_SITE.DS.'components'.DS.'com_joomla_lms'.DS.'includes'.DS.'forums'.DS.'smf'.DS.'smf_v2.php');			
				$instance = new JLMS_SMF_V2();								
			}			
		}

		return $instance;
	}
	
	function getBoardURL() 
	{
		return $this->boardurl;		
	}
	
	function getCookieName() 
	{
		return $this->cookiename;		
	}
	
	function getMbname() 
	{
		return $this->mbname;
	}
	
	function _createDBO()
	{
		$JLMS_CONFIG = & JLMSFactory::GetConfig();
		
		jimport('joomla.database.database');
		//jimport( 'joomla.database.table' );
		
		$conf = JFactory::getConfig();
		
		if (method_exists($conf,'setValue'))
		{
			$driver 	= $conf->getValue('config.dbtype');
			$debug 	= $conf->getValue('config.debug');			
		} else {
			$driver 	= $conf->get('dbtype');
			$debug 	= $conf->get('debug');
		}
		
		require( $JLMS_CONFIG->get('forum_path').'/Settings.php' );	
		
		$this->boardurl = $boardurl;
		$this->cookiename = $cookiename;		
		
		$options	= array ( 'driver' => $driver, 'host' => $db_server, 'user' => $db_user, 'password' => $db_passwd, 'database' => $db_name, 'prefix' => $db_prefix );

		$this->smf_db  = JDatabase::getInstance( $options );
		
		if ( JError::isError( $this->smf_db ) ) {
			jexit('Database Error: ' . $this->smf_db->toString() );
		}

		if ($this->smf_db->getErrorNum() > 0) {
			JError::raiseError(500 , 'JDatabase::getInstance: Could not connect to database <br />' . 'joomla.library:'.$db->getErrorNum().' - '.$this->smf_db->getErrorMsg() );
		}

		$this->smf_db->debug( $debug );		
	} 
	
	function md5Hmac($data, $key)
	{
		$key = str_pad(strlen($key) <= 64 ? $key : pack('H*', md5($key)), 64, chr(0x00));
		return md5(($key ^ str_repeat(chr(0x5c), 64)) . pack('H*', md5(($key ^ str_repeat(chr(0x36), 64)) . $data)));
	}
	
	function setLoginCookieUN($id, $userdata, $password = '') {			
		$password = sha1(sha1(strtolower($userdata->member_name) . $password) . $userdata->password_salt );
			
		$data = serialize(empty($id) ? array(0, '', 0) : array($id, $password, time() , 0));
		$cookie_url = $this->urlParts();
						
		if( !$cookie_url[0] ) 
		{
			setcookie($this->cookiename, 0, time() - 3600, $cookie_url[1], $this->getDotHost( $this->parsedurl['host'] ));			
		} else {
			setcookie($this->cookiename, 0, time() - 3600, $cookie_url[1]);
		}	
				
		// Set the cookie, $_COOKIE, and session variable.
		if( $cookie_url[0]  == 'localhost' ) 
		{
			setcookie($this->cookiename, $data, time() + (60*60*24*365), $cookie_url[1]);
		} else {
			setcookie($this->cookiename, $data, time() + (60*60*24*365), $cookie_url[1], $cookie_url[0], 0);	
		}		
		// cookies are for 1 year... to force 'remember me' to avoid double-login	
					
		$_COOKIE[$this->cookiename] = $data;
		$_SESSION['login_' . $this->cookiename] = $data;
	}
	
	function urlParts()
	{
		// Parse the URL with PHP to make life easier.
		$parsed_url = $this->parsedurl;		
		
		if (isset($parsed_url['port']))
			$parsed_url['host'] .= ':' . $parsed_url['port'];
	
		// Is local cookies off?
		if (
				empty($parsed_url['path'])
			||	empty($this->modSettings['localCookies']) /* for joomla version > 1.5*/
			|| (!empty($this->modSettings['localCookies']) && isset($this->modSettings['localCookies']['value']) && !(int)$this->modSettings['localCookies']['value']) /* for joomla 1.5*/
			){
			$parsed_url['path'] = '';		
		}
			
		// Globalize cookies across domains (filter out IP-addresses)?
		if ((
				!empty($this->modSettings['globalCookies']['value']) /* for joomla 1.5*/
			||	!empty($this->modSettings['globalCookies']) /* for joomla version > 1.5*/
			)
			&& !preg_match('~^\d{1,3}(\.\d{1,3}){3}$~', $parsed_url['host']))
		{				
			// If we can't figure it out, just skip it.
			$parsed_url['host'] = $this->getDotHost( $parsed_url['host'] );
		}	
		// We shouldn't use a host at all if both options are off.
		elseif (empty($this->modSettings['localCookies'])) {
			$parsed_url['host'] = '';
		}
	
		return array($parsed_url['host'], $parsed_url['path'] . '/');
	}
	
	function getDotHost( $host ) 
	{
		$res = $host; 
		
		if ( preg_match('~(?:[^\.]+\.)?([^\.]{2,}\..+)\z~i', $host, $parts) == 1 )
				$res = '.' . $parts[1];
				
		return $res;
	} 
	
	function getSomething() {
		static $instance;
		static $is_already_populated;
		if ($is_already_populated === true) {
			return $instance;
		} else {
			//populate !!!!!!!!!!!!!!
			$is_already_populated = true;
			return $instance;
		}
	}
	
	function populateCourseForums( $course_id, &$user_forums, &$all_forums, &$type) {
		$is_ex = false;
		foreach ($all_forums as $af) {
			$af->item_title = '';
			if ($af->board_type == $type->id) {
				$is_ex = true;
				if ($af->is_active) {
					$user_forums[] = clone($af);
				}
			}
		}
		/* course forums are created in the course new/edit interface  - there is no need to create them here*/
		if (!$is_ex) { 
			$new_forum = clone($type);
			$new_forum->id = 0;
			$new_forum->item_title = '';			
			$new_forum->group_id = 0;
			$new_forum->course_id = $course_id;
			$new_forum->board_type = $type->id;
			$new_forum->id_cat = 0;
			$new_forum->id_group = 0;
			$new_forum->id_board = 0;
			$user_forums[] = $new_forum;
		}	
	}
	
	function populateLgroupForums( $course_id, &$user_forums, &$all_forums, &$type) 
	{
		global $my, $JLMS_DB;
			
		$JLMS_ACL = JLMSFactory::getACL();
		if ($JLMS_ACL->isTeacher()) 
		{
			$query = "SELECT distinct id, ug_name, group_forum FROM #__lms_usergroups WHERE course_id = '".$course_id."' ORDER BY ug_name";
		} else {
			$query = "SELECT distinct a.id, a.ug_name, a.group_forum FROM #__lms_usergroups as a, #__lms_users_in_groups as b WHERE a.course_id = '".$course_id."' AND a.course_id = b.course_id AND a.id = b.group_id AND b.user_id = ".$my->id." ORDER BY a.ug_name";
		}
		$JLMS_DB->SetQuery( $query );
		$user_groups = $JLMS_DB->LoadObjectList('id');
		$user_groups_ids = array();
								
		foreach ($user_groups as $ug) {
			$user_groups_ids[] = $ug->id;
		}	
		$is_ex = false;
						
		$groups_ex = array();
		$activeGroups = array();
				
						
		if (count($user_groups)) 
		{
			foreach ($all_forums as $af) 
			{
				$af->item_title = '';
				if ($af->board_type == $type->id) 
				{				
					if ( in_array($af->group_id, $user_groups_ids) ) 
					{
						$af->item_title = isset($user_groups[$af->group_id]->ug_name) ? $user_groups[$af->group_id]->ug_name : '';
						$user_forums[] = clone($af);
						$groups_ex[] = $af->group_id;
					}									 
				}			
			}
		}
				
		if (count($groups_ex) < count($user_groups)) 
		{			
			foreach ($user_groups as $ug) 
			{	
				if( !$ug->group_forum ) continue;
							
				if ( !in_array($ug->id, $groups_ex ) ) 
				{				
					$new_forum = clone($type);
					$new_forum->id = 0;
					$new_forum->group_id = $ug->id;
					$new_forum->board_type = $type->id;
					$new_forum->item_title = $ug->ug_name;
					$new_forum->id_cat = 0;
					$new_forum->id_group = 0;
					$new_forum->id_board = 0;
					$user_forums[] = $new_forum;
				}
			}
		}			
	}
	
	function populateGgroupForums( $course_id, &$user_forums, &$all_forums, &$type) 
	{
		global $my, $JLMS_DB;
		$JLMS_ACL = JLMSFactory::getACL();
		
		if ($JLMS_ACL->isTeacher()) 
		{
			
			$groups_where_admin_manager = "'0'";
			if($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only')) 
			{
				$query = "SELECT a.group_id FROM `#__lms_user_assign_groups` as a WHERE a.user_id = '".$my->id."' group by a.group_id"
				;
				$JLMS_DB->setQuery($query);
				$groups_where_admin_manager =JLMSDatabaseHelper::loadResultArray();
				
				if(count($groups_where_admin_manager) == 1) {
					$filt_group = $groups_where_admin_manager[0];
				}
				
				$groups_where_admin_manager = implode(',', $groups_where_admin_manager);
				
				if($groups_where_admin_manager == '') {
					$groups_where_admin_manager = "'0'";
				}
			}
			
			$query = "SELECT distinct ug.id, ug.ug_name, ug.group_forum"
			."\n FROM #__lms_users_in_groups AS uig, #__lms_users_in_global_groups AS uigg, #__lms_usergroups AS ug"
			."\n WHERE uig.course_id = $course_id"
			."\n AND ug.group_forum = 1"
			."\n AND uig.user_id = uigg.user_id"
			."\n AND ug.id = uigg.group_id AND ug.course_id = 0"
			. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND ug.id IN ($groups_where_admin_manager)") :'')
			;			
	
		} else {
			$query = "SELECT distinct ug.id, ug.ug_name, ug.group_forum FROM #__lms_usergroups AS ug, #__lms_users_in_global_groups as a WHERE ug.id = a.group_id AND a.user_id = $my->id AND ug.course_id = 0 AND ug.group_forum = 1";
		}
		$JLMS_DB->SetQuery( $query );
		$user_groups = $JLMS_DB->LoadObjectList('id');
					
		
		$user_groups_ids = array();
		foreach ($user_groups as $ug) {
			$user_groups_ids[] = $ug->id;
		}
		$groups_ex = array();
		$activeGroups = array();
		
		if (count($user_groups)) {
			foreach ($all_forums as $af) 
			{
				$af->item_title = '';
				if ($af->board_type == $type->id) 
				{
					if ( in_array($af->group_id, $user_groups_ids) ) 
					{
						$af->item_title = isset($user_groups[$af->group_id]->ug_name) ? $user_groups[$af->group_id]->ug_name : '';
						$user_forums[] = clone($af);
						$groups_ex[] = $af->group_id;
					}			
				}			
			}
		}
		// we need to create missing global groups forums
		if (count($groups_ex) < count($user_groups)) 
		{
			foreach ($user_groups as $ug) 
			{				
				if( !$ug->group_forum ) continue;
								
				if ( !in_array( $ug->id,$groups_ex ) ) 
				{
					$new_forum = clone($type);
					$new_forum->id = 0;
					$new_forum->group_id = $ug->id;
					$new_forum->board_type = $type->id;
					$new_forum->item_title = $ug->ug_name;
					$new_forum->id_cat = 0;
					$new_forum->id_group = 0;
					$new_forum->id_board = 0;
					$user_forums[] = $new_forum;
				}
			}
		}		
		
	}
	
	function populateLpathForums( $course_id, &$user_forums, &$all_forums, &$type)
	{
		global $my, $JLMS_DB;
		$JLMS_ACL = JLMSFactory::getACL();
		if ($JLMS_ACL->isTeacher()) {
			$query = "SELECT a.*"
			. "\n FROM #__lms_learn_paths as a"
			. "\n WHERE a.course_id = '".$course_id."'"
			. "\n ORDER BY a.ordering";
			$JLMS_DB->SetQuery( $query );
			$user_lpaths = $JLMS_DB->LoadObjectList();
			$user_lpaths_ids = array();
			foreach ($user_lpaths as $ul) {
				$user_lpaths_ids[] = $ul->id;
			}			
		} else {
			/* Get list of Published LPaths and check access rights to them (i.e. access is restricted by prerequisites) */
			$query = "SELECT a.*, '' as r_status, '' as r_start, '' as r_end"
			. "\n FROM #__lms_learn_paths as a"
			. "\n WHERE a.course_id = '".$course_id."'"
			//. "\n AND a.published = 1"
			. "\n ORDER BY a.ordering";
			$JLMS_DB->SetQuery( $query );
			$lpaths = $JLMS_DB->LoadObjectList();
	
			require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_grades.lib.php");
			$user_ids = array();
			$user_ids[] = $my->id;
			JLMS_LP_populate_results($course_id, $lpaths, $user_ids);
	
			// 13 August 2007 (DEN) Check for prerequisites.
			// 1. get the list of lpath_ids.
			$lpath_ids = array();
			foreach ($lpaths as $lpath) {
				$lpath_ids[] = $lpath->id;
			}
			
			if (!empty($lpath_ids)) 
			{
				$lpath_ids_str = implode(',', $lpath_ids);
				// 2. get the list of prerequisites
				// SELECT from two tables (+ #__lms_learn_paths) - because the prereq lpath could be deleted...
				$query = "SELECT a.* FROM #__lms_learn_path_prerequisites as a, #__lms_learn_paths as b"
				. "\n WHERE a.lpath_id IN ($lpath_ids_str) AND a.req_id = b.id";
				$JLMS_DB->SetQuery($query);
				$prereqs = $JLMS_DB->LoadObjectList();
				if (!empty($prereqs)) {
					// 3. compare lists of prereqs to the lists of lpaths.
					$i = 0;
					while ($i < count($lpaths)) {
						$is_hidden = false;
						$o = 0;
						while ($o < count($prereqs)) {
							if ($prereqs[$o]->lpath_id == $lpaths[$i]->id) {
								$j = 0;
								while ($j < count($lpaths)) {
									if ($lpaths[$j]->id == $prereqs[$o]->req_id) {
										if (!$lpaths[$j]->item_id) {
											if (empty($lpaths[$j]->r_status)) {
												$is_hidden = true;
												break;
											} else {
												$end_time = strtotime($lpaths[$j]->r_end);
												$current_time = strtotime(JHTML::_('date', null, "Y-m-d H:i:s"));
												if($current_time > $end_time && (($current_time - $end_time) < ($prereqs[$o]->time_minutes*60))){
													$is_hidden = true;
													break;	
												}
											}
										} else {
											if (empty($lpaths[$j]->s_status)) {
												$is_hidden = true;
												break;
											} else {
												$end_time = strtotime($lpaths[$j]->r_end);
												$current_time = strtotime(JHTML::_('date', null, "Y-m-d H:i:s"));
												if($current_time > $end_time && (($current_time - $end_time) < ($prereqs[$o]->time_minutes*60))){
													$is_hidden = true;
													break;	
												}
											}
										}
									}
									$j ++;
								}
							}
							$o ++;
						}
						if ($is_hidden) {
							$lpaths[$i]->published = 0;
						}
						$i ++;
					}
				}
			}
			
			$user_lpaths = array();
			$user_lpaths_ids = array();
			foreach ($lpaths as $lp) 
			{
				if ($lp->published) 
				{					
					$rrr = new stdClass();
					$rrr = clone($lp);
					$user_lpaths[] = $rrr;
					$user_lpaths_ids[] = $rrr->id;					
				}
			}
		}
			
		$groups_ex = array();
		if (count($user_lpaths)) {
			foreach ($all_forums as $af) {
				$af->item_title = '';
				if ($af->board_type == $type->id) {
					if ( in_array($af->group_id, $user_lpaths_ids) ) {
						foreach ($user_lpaths as $ulp_item) {
							if ($ulp_item->id == $af->group_id) {
								$af->item_title = $ulp_item->lpath_name;
								break;
							}
						}
						$user_forums[] = clone($af);
						$groups_ex[] = $af->group_id;
					}
				}
			}
		}
		
		// we need to create missing lpaths forums
		if (count($groups_ex) < count($user_lpaths)) {
			foreach ( $user_lpaths as $ul ) {				
				$pos = strpos($ul->lp_params, 'add_forum=1');
				if ($pos === false) continue;			
									
				if (!in_array($ul->id,$groups_ex)) {
					$new_forum = clone($type);
					$new_forum->id = 0;
					$new_forum->group_id = $ul->id;
					$new_forum->board_type = $type->id;
					$new_forum->item_title = $ul->lpath_name;
					$new_forum->id_cat = 0;
					$new_forum->id_group = 0;
					$new_forum->id_board = 0;
					$user_forums[] = $new_forum;
				}
			}
		}	
	}	
	
	function getDefBoardPermissions() 
	{
		return array( 'post_new', 'poll_view' ,'post_reply_own', 'post_reply_any', 'delete_own', 'modify_own', 'mark_any_notify', 'mark_notify','moderate_board', 'report_any', 'send_topic', 'poll_vote', 'poll_edit_own',	'poll_post', 'poll_add_own', 'post_attachment', 'lock_own', 'remove_own', 'view_attachments' );
	}
	
	function checkSMF_cookies( $reconnect = true ) 
	{
		global $JLMS_CONFIG, $JLMS_DB;
		$id_member = 0;
		$password = '';
				
		$cookiename = $this->getCookieName();
		
		if (isset($_COOKIE[$cookiename])) {
			
			$cookie_forum = stripslashes($_COOKIE[$cookiename]);
			if (preg_match('~^a:[34]:\{i:0;(i:\d{1,6}|s:[1-8]:"\d{1,8}");i:1;s:(0|40):"([a-fA-F0-9]{40})?";i:2;[id]:\d{1,14};(i:3;i:\d;)?\}$~', $cookie_forum) == 1) {
				list ($id_member, $password) = @unserialize($cookie_forum);
				$id_member = !empty($id_member) && strlen($password) > 0 ? (int) $id_member : 0;
			} else {
				$id_member = 0;
			}
		}
		if ( $id_member ) {	
			$user_data = $this->selectMembers( array($id_member) );
			
			$user_settings = array();
			if ( $user_data ) $user_settings = $user_data[0];		
			if (!empty($user_settings)) {
				// SHA-1 passwords should be 40 characters long.
				if (strlen($password) == 40) {
					$check = sha1($user_settings->passwd . $user_settings->password_salt) == $password;
				} else {
					$check = false;
				}
				// Wrong password or not activated - either way, you're going nowhere.
				//$id_member = $check && ($user_settings['is_activated'] == 1 || $user_settings['is_activated'] == 11) ? $user_settings['id_member'] : 0;
				$id_member = $check ? $user_settings->id_member : 0;
			} else {
				$id_member = 0;
			}
		}
		return $id_member;
	}

	
	function applyChanges( $course_id ) 
	{
		global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
		
		$JLMS_ACL = JLMSFactory::getACL();
		$doc = JFactory::getDocument();
		
		$id_cat = 0;
		$course_name = '';
		$owner_name = '';
		$private_forums_tobe_created = array(); // list of private forums to be created
		$msg = '';//error message - prompt to login if not blank
	
		$query = "SELECT * FROM #__lms_forums WHERE published = 1";
		$JLMS_DB->setQuery($query);
		$lms_forum_types = $JLMS_DB->loadObjectList('id');
		//[TODO] - check all types of the forums and create/remove necessary forum boards in the SMF - i.e. if they wasn't be created/removed previously
		// Note, autocreation is necessary only for 'global groups' forums and private forums (`access` is 1)
		$give_course_forums = array();
		$need_to_create = false;
		$need_to_update = false;
			
		if (count($lms_forum_types)) 
		{
			// check all forum types for access.
			$allowed_forum_types = array(); // allowed forum types
			$lms_forum_types_ids = array();
			foreach ($lms_forum_types as $lft) 
			{
				$do_proceeed = true;
	
				// firstly - check `access` and `permissions`
				switch(intval($lft->forum_access)) {
					case 0:
						// any user
					break;
					case 1:
						// check user role
						if ($lft->forum_permissions) 
						{
							$allowed_roles = explode(',', $lft->forum_permissions);
							if ( $JLMS_ACL->GetRole() && in_array($JLMS_ACL->GetRole(), $allowed_roles) ) {
								// allow access
							} else { // restrict access if this forum is not permissioned.							
								$do_proceeed = false;
							}
						} else { // if this is a restricted area but there is no roles configured
							$do_proceeed = false;
						}
					break;
				}
				if (!$do_proceeed) { continue; }
				// secondly - check `user_level` permissions
				switch(intval($lft->user_level)) {
					case 0: // any user
					break;
	
					case 1: // local usergroup forum
					case 2: // global usergroup forum
						if ($lft->user_level == 1 && $JLMS_CONFIG->get('use_global_groups', 1)) {
							$do_proceeed = false; // restrict access to the local-group forum, if we are in the global mode
						} elseif ($lft->user_level == 2 && !$JLMS_CONFIG->get('use_global_groups', 1)) {
							$do_proceeed = false; // restrict access to the global-group forum, if we are in the local mode
						}
					break;
				}
				if (!$do_proceeed) { continue; }
				$allowed_forum_types[$lft->id] = $lft; // we have a list of allowed forum types here.
				$lms_forum_types_ids[] = $lft->id;
			}
			
			$query = "SELECT ID_GROUP, ID_CAT, ID_BOARD FROM #__lms_forum_details WHERE course_id = $course_id";
			$JLMS_DB->setQuery($query);
			$smfDataList = $JLMS_DB->loadObjectList();
						
			
			if( isset($smfDataList[0]) ) 
			{
				if( !$this->getCategoryById( $smfDataList[0]->ID_CAT ) ) 
				{			
					$query = "DELETE FROM #__lms_forum_details WHERE ID_CAT = ".$JLMS_DB->quote( $smfDataList[0]->ID_CAT );
					$JLMS_DB->setQuery($query);
					$JLMS_DB->Query();
				}
			}
			
			$groupIds = array();
			$boardIds = array();
					
			foreach( $smfDataList AS $smfData  ) 
			{
				$groupIds[] = $smfData->ID_GROUP;
				$boardIds[] = $smfData->ID_BOARD;					
			}
							
			$brokenGroupIds = $this->getBrokenGroups( $groupIds );
			$brokenBoardIds = $this->getBrokenBoards( $boardIds );		
					
			if ( !empty($brokenGroupIds) ) 
			{					
				$query = "DELETE FROM #__lms_forum_details WHERE ID_GROUP IN (".implode(',', $brokenGroupIds).")";
				$JLMS_DB->setQuery($query);
				$JLMS_DB->Query();
			}
					
			if ( !empty($brokenBoardIds) ) 
			{
				$query = "DELETE FROM #__lms_forum_details WHERE ID_BOARD IN (".implode(',', $brokenBoardIds).")";
				$JLMS_DB->setQuery($query);
				$JLMS_DB->Query();
			}	
							
			
			if (count($lms_forum_types_ids)) {
				$query = "SELECT id, course_id, board_type, group_id, ID_GROUP AS id_group, ID_CAT AS id_cat, ID_BOARD AS id_board, is_active, need_update FROM #__lms_forum_details"
				 . "\n WHERE course_id = $course_id"
				 //. "\n  AND is_active = 1"
				 . "\n AND board_type IN (".implode(',',$lms_forum_types_ids).")";
				$JLMS_DB->setQuery($query);
				$active_course_forums = $JLMS_DB->loadObjectList();
				
						
				
				for ($i = 0; $i < count($active_course_forums); $i++) 
				{				
					$active_course_forums[$i]->forum_level = $lms_forum_types[$active_course_forums[$i]->board_type]->forum_level;
					$active_course_forums[$i]->user_level = $lms_forum_types[$active_course_forums[$i]->board_type]->user_level;
					$active_course_forums[$i]->forum_access = $lms_forum_types[$active_course_forums[$i]->board_type]->forum_access;
					$active_course_forums[$i]->forum_permissions = $lms_forum_types[$active_course_forums[$i]->board_type]->forum_permissions;
					$active_course_forums[$i]->parent_forum = $lms_forum_types[$active_course_forums[$i]->board_type]->parent_forum;
					$active_course_forums[$i]->forum_moderators = $lms_forum_types[$active_course_forums[$i]->board_type]->forum_moderators;
					$active_course_forums[$i]->forum_name = $lms_forum_types[$active_course_forums[$i]->board_type]->forum_name;
					$active_course_forums[$i]->forum_desc = $lms_forum_types[$active_course_forums[$i]->board_type]->forum_desc;
					$active_course_forums[$i]->moderated = $lms_forum_types[$active_course_forums[$i]->board_type]->moderated;
					// !!!! ATTENTION, `need_update` field was moved to the `lms_forum_details` table //$active_course_forums[$i]->need_update = $lms_forum_types[$active_course_forums[$i]->board_type]->need_update;
				}
				// [TODO] - inspect each forum type and populate list of necessary forums (to be created or already created)
				$give_course_forums = array();
																						
				foreach ($allowed_forum_types as $aft) 
				{
					switch(intval($aft->forum_level)) {
						case 0:
							switch(intval($aft->user_level)) {
								case 0:
									//one forum should be created for course.								
									$this->populateCourseForums( $course_id, $give_course_forums, $active_course_forums, $aft );		
								break;
								case 1:
									$this->populateLgroupForums( $course_id, $give_course_forums, $active_course_forums, $aft );
								break;
								case 2:							
									$this->populateGgroupForums( $course_id, $give_course_forums, $active_course_forums, $aft );
								break;
							}
						break;
						case 1:
							if (intval($aft->user_level) == 0) {
								$this->populateLpathForums( $course_id, $give_course_forums, $active_course_forums, $aft );			
							} else {
								//ignore any other forums - currently there is no posibility to have such difficult structure
							}
						break;
					}
				}
																			
				if (count($give_course_forums)) {
					$need_to_create = false;
					for ($i = 0; $i < count($give_course_forums); $i++) {
						if ($give_course_forums[$i]->id == 0) {
							$need_to_create = true;
						}
						if (isset($give_course_forums[$i]->need_update) && $give_course_forums[$i]->need_update) {
							$need_to_update = true;
						}
						if ($need_to_create && $need_to_update) {
							break;
						}
					}
					
				}
			}
		}
				
		$was_created = array();
		$was_updated = array();
		$newcat_wascreated = false;
        
        $msg = '';
        $link = '';        
        			
		if (count($give_course_forums)) 
		{
			//echo '<pre>';var_dump($give_course_forums);die;
			
            if( $need_to_create || $need_to_update ) 
            {
    			$query = "SELECT a.course_name, a.owner_id, b.username as owner_name FROM #__lms_courses as a LEFT JOIN #__users as b ON a.owner_id = b.id WHERE a.id = $course_id";
    			$JLMS_DB->setQuery($query);
    			$course_info = $JLMS_DB->loadObject();
    			if (is_object($course_info)) {
    				$course_name = $course_info->course_name;// - we need it to create boards
    				$owner_name = $course_info->owner_name;
    			}
            }           
						
			$query = "SELECT ID_CAT AS id_cat FROM #__lms_forum_details WHERE course_id = $course_id LIMIT 0,1";
			$JLMS_DB->setQuery($query);
			$id_cat = $JLMS_DB->loadResult();// ID of the Course CATEGORY (in which all course boards are placed) - we need it to create boards
			// TODO:
			// check if SMF boards was removed and create new ones if necessary!
			$query = "SELECT distinct ID_GROUP AS id_group FROM `#__lms_forum_details` WHERE course_id =".$course_id;
			$JLMS_DB->setQuery($query);
			$all_lms_groups = JLMSDatabaseHelper::loadResultArray();
			$all_moderator_ids = array();
			$all_moderators = array();
									
			for ($i = 0; $i < count($give_course_forums); $i++) 
			{			 
				if ((($need_to_create && $give_course_forums[$i]->id == 0) || $give_course_forums[$i]->need_update) && $give_course_forums[$i]->forum_moderators) 
				{
					$new_moderators = explode(',', $give_course_forums[$i]->forum_moderators);
					$all_moderator_ids = array_merge($all_moderator_ids, $new_moderators);
				}
			}
			
			if (count($all_moderator_ids)) {
				for ($i = 0; $i < count($all_moderator_ids); $i++) {
					$all_moderator_ids[$i] = intval($all_moderator_ids[$i]);
				}
				$all_moderator_ids = array_unique($all_moderator_ids);
				if (!empty($all_moderator_ids)) {
					$all_moderator_ids_str = implode(',',$all_moderator_ids);
					$query = "SELECT id, username FROM #__users WHERE id IN ($all_moderator_ids_str)";
					$JLMS_DB->setQuery($query);
					$all_moderators = $JLMS_DB->LoadObjectList('id');
				}
			}		
			
            if( ($need_to_create || $need_to_update) && $course_name ) 
            {			
    			$storeData = array();			
    			$storeData['name'] = $course_name;
    			$storeData['can_collapse'] = 1;
                $storeData['id_cat'] = $id_cat;
                 			
                if (!$id_cat) 
                {	 
                    $newcat_wascreated = true;
                }
                
                $id_cat = $this->storeCategory( $storeData );
            }  
												
					
			$user_exists = false;
			$is_forum_category = 0;
			$boardurl = $this->getBoardURL();
			$link = $boardurl.'/index.php#'.$id_cat;
			$smf_user_details = $this->loadMemberByName( $my->username );
			
			if ( $smf_user_details ) {
				$user_exists = true;
				$all_current_smf_groups = array();
										
				// create all parent forums			
				for ($i = 0; $i < count($give_course_forums); $i++) 
				{				
					if ($need_to_create && $give_course_forums[$i]->id == 0 && !$give_course_forums[$i]->parent_forum) 
					{	
						/*									
						if ( ($give_course_forums[$i]->forum_level == 1 && $give_course_forums[$i]->user_level == 0) ||
						($give_course_forums[$i]->forum_level == 0 && $give_course_forums[$i]->user_level == 2) || ($give_course_forums[$i]->forum_level == 0 && $give_course_forums[$i]->user_level == 0)	) 
						{	
							*/																		
							$tmp = $this->create( $give_course_forums, $i, $course_name, $owner_name, $all_moderators, $id_cat);
							$was_created[] = $tmp;
							$all_current_smf_groups[] = $tmp->id_group;
						/*}*/
					} elseif ($give_course_forums[$i]->id && $give_course_forums[$i]->need_update && !$give_course_forums[$i]->parent_forum) {
						$tmp = $this->update( $give_course_forums, $i, $course_name, $owner_name, $all_moderators, $id_cat);
						$was_updated[] = $tmp;
						$all_current_smf_groups[] = $give_course_forums[$i]->id_group;
					} elseif ($give_course_forums[$i]->id && !$give_course_forums[$i]->need_update) {
						$all_current_smf_groups[] = $give_course_forums[$i]->id_group;
					}
				}
									
				//crate all nested forums				
							
				for ($i = 0; $i < count($give_course_forums); $i++) 
				{
					if( $give_course_forums[$i]->parent_forum ) 
					{
						if ( $need_to_create && $give_course_forums[$i]->id == 0  ) 
						{
							
							if ( ($give_course_forums[$i]->forum_level == 1 && $give_course_forums[$i]->user_level == 0) ||
							($give_course_forums[$i]->forum_level == 0 && $give_course_forums[$i]->user_level == 2)	|| ($give_course_forums[$i]->forum_level == 0 && $give_course_forums[$i]->user_level == 0)) {
								
								
							$tmp = $this->create( $give_course_forums, $i, $course_name, $owner_name, $all_moderators, $id_cat);
							$was_created[] = $tmp;
							$all_current_smf_groups[] = $tmp->id_group;
							}
						} elseif ( $give_course_forums[$i]->id && $give_course_forums[$i]->need_update ) {							
							$tmp = $this->update( $give_course_forums, $i, $course_name, $owner_name, $all_moderators, $id_cat);
							$was_updated[] = $tmp;
							$all_current_smf_groups[] = $give_course_forums[$i]->id_group;
						}
					}
				}						
				
				$mem_id = $smf_user_details->id_member;
				$mem_real_name = $smf_user_details->real_name;
				$primary_group = $smf_user_details->id_group;
				$old_groups = explode(',', $smf_user_details->additional_groups);
				$old_groups_save = array();
				
				foreach($old_groups as $group)
				{
					if (!in_array($group, $all_lms_groups))
					{
						$old_groups_save[] = $group;
					}
				}
				
				//$this_groups = array_unique(array_merge($all_current_smf_groups, $old_groups_save));
				$this_groups = $all_current_smf_groups;
								
	
				$new_forum_groups = array();
				foreach ($this_groups as $fg) {
					if ($fg) {
						$new_forum_groups[] = $fg;
					}
				}
	
				$groups = implode(',', $new_forum_groups);				
				
								
				if (!$mem_real_name && isset($my->name) && $my->name) 
				{
					// update real_name of user, if it is missed
					$storeData = array();		
					$storeData['id_member'] = $mem_id;		
					$storeData['id_group'] = $primary_group;
					$storeData['additional_groups'] = $groups;
					$storeData['real_name'] = $my->name;				
				} else {
					$storeData = array();
					$storeData['id_member'] = $mem_id;
					$storeData['id_group'] = $primary_group;
					$storeData['additional_groups'] = $groups;
				}
				
				$this->storeMember( $storeData );
				
				if (count($give_course_forums) == 1) {
					$is_forum_category = 0;
					$link = $boardurl.'/index.php?board='.$give_course_forums[0]->id_board.'.0';
				} elseif (count($give_course_forums) > 1) {
					$is_forum_category = 1;
					$link = $boardurl.'/index.php#'.$id_cat;
				}
				
				$topic_id = JRequest::getVar('topic_id', 0);
				$message_id = JRequest::getVar('message_id', 0);
				if($topic_id && $message_id){
					$link = $boardurl.'/index.php';
					$link .= '?topic='.$topic_id;
					$link .= '.msg'.$message_id;
					$link .= '#msg'.$message_id;
				}
	
				if (true) {//($is_forum_category) {				
					$this_tree = $this->selectBoards();
					$user_cats = array();
					foreach ($this_tree as $ft) {
						$ar = $ft->member_groups;
						$ar = explode(',',$ar);
						$is_cat = false;
						foreach ($new_forum_groups as $gia) {
							if (in_array($gia, $ar)) { $is_cat = true; break; }
						}
						if ($is_cat) {
							$user_cats[] = $ft->id_cat;
						}
					}
					$user_cats = array_unique($user_cats);
					if (count($user_cats)) {									
							
						$this->deleteCollapsedCategories( $mem_id, $user_cats );
																
						$new_ar = array($id_cat);
						$rrr = array_diff($user_cats, $new_ar);
						$this->insertCollapsedCategories( $mem_id, $rrr );
					}
				}
			}
			$mem_id_cookies = 0;
			if ($user_exists) {
				$mem_id_cookies = $this->checkSMF_cookies(false);
			}
									
			if ($need_to_create) 
			{
				if (count($was_created)) 
				{				
					$query = "INSERT INTO #__lms_forum_details (course_id, board_type, group_id, ID_GROUP, ID_CAT, ID_BOARD, is_active) VALUES";
					$first = 1;
					
					$newCatIds = array();
					
					
					foreach ($was_created as $obj) 
					{			
						$query .= ($first) ? '' : ',';
						$query .= "\n($course_id, $obj->board_type, $obj->group_id, $obj->id_group, $obj->id_cat, $obj->id_board, 1)";
						$first = 0;
						$newCatIds[] = $obj->id_cat;
						$newGroupIds[] = $obj->id_group;
						$newBoardIds[] = $obj->id_board;
					}									
					
					if( !empty($newCatIds) ) 
					{
						$delQuery = "DELETE FROM #__lms_forum_details WHERE ID_CAT IN (".implode( ',', $newCatIds).") AND course_id != ".$course_id;
						$JLMS_DB->setQuery( $delQuery );										
						$JLMS_DB->query();					
					}				
					
					if( !empty($newGroupIds) ) 
					{
						$delQuery = "DELETE FROM #__lms_forum_details WHERE ID_GROUP IN (".implode( ',', $newGroupIds).") AND course_id != ".$course_id;
						$JLMS_DB->setQuery( $delQuery );										
						$JLMS_DB->query();					
					}				
					
					if( !empty($newBoardIds) ) 
					{
						$delQuery = "DELETE FROM #__lms_forum_details WHERE ID_BOARD IN (".implode( ',', $newBoardIds).") AND course_id != ".$course_id;
						$JLMS_DB->setQuery( $delQuery );										
						$JLMS_DB->query();					
					}						
					
					$JLMS_DB->setQuery( $query );
					$JLMS_DB->query();			
				}
			}
			
			if (isset($was_updated) && count($was_updated)) 
			{
				$was_updated = array_unique($was_updated);
				if (!empty($was_updated)) {
					$was_updated_str = implode(',',$was_updated);
					$query = "UPDATE #__lms_forum_details SET need_update = 0 WHERE id IN ($was_updated_str) AND need_update = 1";
					$JLMS_DB->setQuery($query);
					$JLMS_DB->query();
				}
			}
	
			if ($user_exists) {			
				if ($mem_id && $mem_id_cookies && $mem_id_cookies == $mem_id) {				
					$mbname = $this->getMbname();				
					$doc->setTitle( $mbname );
					//[TODO] - what is it - $mbname and $boardurl - from the Settings.php?
				} else { $msg = JLMS_FORUM_NOT_MEMBER; }
			} else {
				$msg = JLMS_FORUM_NOT_MEMBER;
			}
		} 
		
		return array( 'msg' => $msg, 'link' => $link );
	}

	function resetLocalCookiesOption() 
	{
		$query = 'UPDATE #__settings SET value = 0 WHERE variable = \'localCookies\'';
		$this->smf_db->setQuery( $query );										
		$this->smf_db->query();		
	}
}

class SMFTable extends JTable 
{		
	function storeAdapter( $fields, $markers ) 
	{			
		$keys = array_keys( $fields );
		$res = array();
				
		foreach( $keys AS $key ) 
		{
			if( isset($markers[$key])) 
			{
				$res[$markers[$key]] = $fields[$key];  
			}
		}	 	 	
		
		return $res; 
	}
	
	function loadAdapter( $objs, $markers )
	{					
		$markers_f = array_flip( $markers );
			
		if( empty( $objs ) ) return $objs;
						
		if( is_array($objs) ) 
		{
			$vars = get_object_vars($objs[0]);
			$keys = array_keys( $vars );		
						
			for( $i = 0; $i < count( $objs ); $i++ ) 
			{
				foreach( $keys AS $key ) 
				{
					if( isset($markers_f[$key]) ) 
					{
						$objs[$i]->{$markers_f[$key]} = $vars[$key];
					}	
				}				 
			}
			
		} else {			
			$vars = get_object_vars($objs);
			$keys = array_keys( $vars );
									
			foreach( $keys AS $key ) 
			{
				if( isset($markers_f[$key]) ) 
				{					
					$objs->{$markers_f[$key]} = $vars[$key];
				}	
			}						 
		}
		
		return $objs;		
	}
	
}
