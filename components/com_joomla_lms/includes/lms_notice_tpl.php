<?php
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );
global $JLMS_CONFIG, $JLMS_SESSION;


$task 		= mosGetParam( $_REQUEST, 'task', '' );
	$id 		= intval(mosGetParam( $_REQUEST, 'id', '' ));
	$course_id = $JLMS_CONFIG->get('course_id');
	
	$doc = JFactory::getDocument();
	
	if($task == $JLMS_SESSION->get('jlms_task'))
	{
		$id=0;
	}
	if($task == "quizzes")
	{
		if(isset($_REQUEST['page']))
		{
			
			$id = isset($_REQUEST['quiz_id'])?$_REQUEST['quiz_id']:(isset($_REQUEST['c_id'])?$_REQUEST['c_id']:$id);
		}
	}
	else
	if($task != "docs_view_content" && $task != "compose_lpath" && $task != "show_lp_content")
	{
		$task = $JLMS_SESSION->get('jlms_task');
	}
	/*	
	$doc->addStyleSheet($JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_css/moodalbox.css', 'text/css', 'screen' );	
	
	if( JLMS_mootools12() ) {
		$doc->addScript($JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/includes/js/moodalbox16.js' );
	} else {
		$doc->addScript($JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/includes/js/moodalbox.js' );
	}	
		
	$domready = 'setTimeout(\'MOOdalBox.init();\', 100);';
	$JLMS_CONFIG->set('web20_domready_code', $JLMS_CONFIG->get('web20_domready_code','').$domready);		
	*/

function get_notice_html($option)
{
	global $JLMS_DB,$my,$JLMS_CONFIG,$JLMS_SESSION;
	
	JLMS_HTML::_('behavior.mootools');		
	JLMS_SqueezeBox('joomla');	
	
	$task 		= strval(mosGetParam( $_REQUEST, 'task', '' ));
	$id 		= intval(mosGetParam( $_REQUEST, 'id', 0 ));
	if($task == $JLMS_SESSION->get('jlms_task'))
	{
		$id=0;
	}
	if($task == "quizzes")
	{
		if(isset($_REQUEST['page']))
		{
			
			$id = isset($_REQUEST['quiz_id'])?$_REQUEST['quiz_id']:(isset($_REQUEST['c_id'])?$_REQUEST['c_id']:$id);
		}
	}
	else
	if($task != "docs_view_content" && $task != "compose_lpath" && $task != "show_lp_content")
	{
		$task = $JLMS_SESSION->get('jlms_task');
	}
	
	$course_id = $JLMS_CONFIG->get('course_id');
	$query = "SELECT COUNT(*) FROM #__lms_page_notices WHERE usr_id=".$my->id." AND course_id=$course_id AND task=".$JLMS_DB->quote($task)." AND doc_id=$id";
	$JLMS_DB->setQuery($query);
	$count = $JLMS_DB->loadResult();
		
	echo ' <a class="modal" style="position: static;" rel="{ handler: \'iframe\', size: {x: 500, y: 400}, onClose: function() {}}" href="index.php?tmpl=component&option='.$option.'&amp;task=new_notice&amp;ntask='.$task.'&amp;course_id='.$course_id.'&amp;doc_id='.$id.'">'._JLMS_USER_OPTIONS_NOTES.' <span id="pn_count">'.$count.'</span></a>';
}
?>