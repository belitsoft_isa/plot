<?php
/**
* lms_legacy.lib.php
* Joomla LMS Component
* * * ElearningForce DK
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

if (!defined('_CMN_IFRAMES')) {
	DEFINE('_CMN_IFRAMES', 'This option will not work correctly.  Unfortunately, your browser does not support Inline Frames');
}

// 25.12.2007 (DEN) - bugfix (happens under Joomla1.5 only)
if (class_exists('moshtml')) {
	// do nothing
	// Ioncube bug? 'moshtml' unloads in encoded files if we comment this 'if' statement ???
	// problem happens only in Joomla 1.5 RC4
	// P.S. error "Fatal error: Cannot instantiate non-existent class: moshtml in ... joomla_lms.courses.php on line 0" occurs in encoded files
}
if (class_exists('mosuser')) {
}

//SMT j!3.0 update | Check: is it joomla 3.0?
function JLMS_getCheckAll($count=0) 
{
	if (JLMS_J30version()) return 'Joomla.checkAll(this)';
	else return 'checkAll('.(int)$count.');';
}
//

function JLMS_J32version() 
{
	$version = new JVersion();
	
	if( strnatcasecmp( $version->RELEASE, '3.2' ) >= 0 ) 
	{
		return true; 
	} else {
		return false;
	}
}

function JLMS_J31version() 
{
	$version = new JVersion();
	
	if( strnatcasecmp( $version->RELEASE, '3.1' ) >= 0 ) 
	{
		return true; 
	} else {
		return false;
	}
}

function JLMS_J30version() 
{
	$version = new JVersion();
	
	if( strnatcasecmp( $version->RELEASE, '3.0' ) >= 0 ) 
	{
		return true; 
	} else {
		return false;
	}
}

function JLMS_J25version() 
{
	$version = new JVersion();
	
	if( strnatcasecmp( $version->RELEASE, '2.5' ) >= 0 ) 
	{
		return true; 
	} else {
		return false;
	}
}

function JLMS_J16version() 
{
	$version = new JVersion();
	
	if( strnatcasecmp( $version->RELEASE, '1.6' ) >= 0 ) 
	{
		return true; 
	} else {
		return false;
	}
}

function JLMS_mootools12() 
{
	$app = JFactory::getApplication();
	return ( JLMS_J16version() || preg_match( '/^1.2.[0-9]+/', $app->get('MooToolsVersion') ) );	
}


/** 
 *  Return values:
 *  0 - Joomla 1.0.x  <  1.0.13 + mambo 4.5.0 - 4.5.3
 *  1 - Joomla 1.0.x  >= 1.0.13
 *  2 - Joomla 1.5 
 *  ------
 *  10 - Mambo 4.6
 */
function JLMS_Jversion() {
	global $_VERSION;

	static $version	= null;
	if ( $version !== null ) {
		return $version;
	}

	if (class_exists('JFactory')) {
		$version = 2;
		return $version;
	}

	if ( $_VERSION->PRODUCT == "Mambo" ) {
		if ( strncasecmp( $_VERSION->RELEASE, "4.6", 3 ) < 0 ) {
			$version = 0;
		} else { // MAMBO 4.6
			$version = 10;
		}
	} elseif ( ($_VERSION->PRODUCT == "Joomla!") || ($_VERSION->PRODUCT == "Accessible Joomla!") ) {
		if ( ( $_VERSION->RELEASE == '1.0' ) && ( $_VERSION->DEV_LEVEL < 13 ) ) { // Joomla 1.0.x  <  1.0.13
			$version = 0;
		} elseif ( ( $_VERSION->RELEASE == '1.0' ) && ( $_VERSION->DEV_LEVEL >= 13 ) ) { // Joomla 1.0.13 and above
			$version = 1;
		} elseif (strncasecmp($_VERSION->RELEASE, "1.0", 3)) { // Joomla 1.5 RC and above
			$version = 2;
		} else {
			$version = 0;
		}
	} else { //...... // i.e. $_VERSION->PRODUCT == "Elxis"
		$version = 0;
	}
	return $version;
}

function JLMS_die() {
	
	$app = JFactory::getApplication();
	$app->close();	
	die;
}

if (!function_exists('josGetArrayInts')) {
	function josGetArrayInts( $name, $type=NULL ) {
		if ( $type == NULL ) {
			$type = $_POST;
		}
		$array = mosGetParam( $type, $name, array(0) );
		mosArrayToInts( $array );
		if (!is_array( $array )) {
			$array = array(0);
		}
		return $array;
	}
}

if (!function_exists('JosIsValidEmail')) {
	function JosIsValidEmail( $email ) {
		$valid = preg_match( '/^[\w\.\-]+@\w+[\w\.\-]*?\.\w{1,4}$/', $email );
		return $valid;
	}
}

if (!function_exists('JosIsValidName')) {
	function JosIsValidName( $string ) {
		/*
		 * The following regular expression blocks all strings containing any low control characters:
		 * 0x00-0x1F, 0x7F
		 * These should be control characters in almost all used charsets.
		 * The high control chars in ISO-8859-n (0x80-0x9F) are unused (e.g. http://en.wikipedia.org/wiki/ISO_8859-1)
		 * Since they are valid UTF-8 bytes (e.g. used as the second byte of a two byte char),
		 * they must not be filtered.
		 */
		$invalid = preg_match( '/[\x00-\x1F\x7F]/', $string );
		if ($invalid) {
			return false;
		} else {
			return true;
		}
	}
}
?>