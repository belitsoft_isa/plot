<?php
defined('_JEXEC') or die( 'Restricted access' );

$notification_events = array();

/** *************************OnCourseEnrolment group events begin**********************************/

$notification_type = 1;

$event = new stdClass();
$event->id = 1;
$event->name = _JLMS_NOTS_SELFENROLMENT_INTO_FREE_COURSE;
$event->description = '';
$event->event_action = 'OnSelfEnrolmentIntoFreeCourse'; // unique_name � ������� ������� ����� �������������� ��������.
$event->learner_template = 1;
$event->manager_template = 1;
$event->use_learner_template = true;
$event->use_manager_template = true;
$event->learner_template_disabled = false;
$event->manager_template_disabled = false;
$event->skip_managers = false;
$event->notification_type = $notification_type;
$event->selected_manager_roles = array(1,4,5);
$event->disabled = true;

$notification_events[] = $event;

/** ***********************************************************/
$event = new stdClass();
$event->id = 2;
$event->name = _JLMS_NOTS_SELFENROLMENT_INTO_PAID_COURSE;
$event->description = '';
$event->event_action = 'OnSelfEnrolmentIntoPaidCourse'; // unique_name � ������� ������� ����� �������������� ��������.
$event->learner_template = 1;
$event->manager_template = 1;
$event->use_learner_template = true;
$event->use_manager_template = true;
$event->learner_template_disabled = false;
$event->manager_template_disabled = false;
$event->skip_managers = false;
$event->notification_type = $notification_type;
$event->selected_manager_roles = array(1,4,5);
$event->disabled = true;

$notification_events[] = $event;

/** ***********************************************************/
$event = new stdClass();
$event->id = 3;
$event->name = _JLMS_NOTS_ENROLMENT_INTO_COURSE_BY_COURSE_TEACHER;
$event->description = '';
$event->event_action = 'OnEnrolmentInCourseByTeacher'; // unique_name � ������� ������� ����� �������������� ��������.
$event->learner_template = 1;
$event->manager_template = 1;
$event->use_learner_template = true;
$event->use_manager_template = true;
$event->learner_template_disabled = false;
$event->manager_template_disabled = false;
$event->skip_managers = false;
$event->notification_type = $notification_type;
$event->selected_manager_roles = array(1,4,5);
$event->disabled = true;

$notification_events[] = $event;

/** ***********************************************************/
$event = new stdClass();
$event->id = 4;
$event->name = _JLMS_NOTS_ENROLMENT_INTO_COURSE_BY_JADMIN;
$event->description = '';
$event->event_action = 'OnEnrolmentInCourseByJoomlaAdmin'; // unique_name � ������� ������� ����� �������������� ��������.
$event->learner_template = 1;
$event->manager_template = 1;
$event->use_learner_template = true;
$event->use_manager_template = true;
$event->learner_template_disabled = false;
$event->manager_template_disabled = false;
$event->skip_managers = false;
$event->notification_type = $notification_type;
$event->selected_manager_roles = array(1,4,5);
$event->disabled = true;

$notification_events[] = $event;
/** *****************************OnCourseEnrolment group events end******************************/

/** *****************************OnCourseCompletion group events begin******************************/

$notification_type = 2;

$event = new stdClass();
$event->id = 5;
$event->name = _JLMS_NOTS_USER_COMPLETES_COURSE;
$event->description = '';
$event->event_action = 'OnUserCompletesTheCourse'; // unique_name � ������� ������� ����� �������������� ��������.
$event->learner_template = 1;
$event->manager_template = 1;
$event->use_learner_template = true;
$event->use_manager_template = true;
$event->learner_template_disabled = false;
$event->manager_template_disabled = false;
$event->skip_managers = false;
$event->notification_type = $notification_type;
$event->selected_manager_roles = array(1,4,5);
$event->disabled = true;

$notification_events[] = $event;

/** ***********************************************************/
$event = new stdClass();
$event->id = 6;
$event->name = _JLMS_NOTS_TEACHER_MARK_COURSE_COMPLETION;
$event->description = '';
$event->event_action = 'OnTeacherMarksCourseCompletion'; // unique_name � ������� ������� ����� �������������� ��������.
$event->learner_template = 3;
$event->manager_template = 3;
$event->use_learner_template = true;
$event->use_manager_template = true;
$event->learner_template_disabled = false;
$event->manager_template_disabled = false;
$event->skip_managers = false;
$event->notification_type = $notification_type;
$event->selected_manager_roles = array(1,4,5);
$event->disabled = true;

$notification_events[] = $event;
/** *****************************OnCourseCompletion group events end******************************/

/** *****************************OnCSVImportUser group events begin******************************/
$notification_type = 3;

$event = new stdClass();
$event->id = 7;
$event->name = _JLMS_NOTS_IMPORT_USER_INTO_LMS_FROM_CSV;
$event->description = '';
$event->event_action = 'OnCSVImportUser'; // unique_name � ������� ������� ����� �������������� ��������.
$event->learner_template = 4;
$event->manager_template = 4;
$event->use_learner_template = true;
$event->use_manager_template = true;
$event->learner_template_disabled = false;
$event->manager_template_disabled = false;
$event->skip_managers = false;
$event->notification_type = $notification_type;
$event->selected_manager_roles = array(1,4,5);
$event->disabled = true;

$notification_events[] = $event;
/** *****************************OnCSVImportUser group events end******************************/


$notification_type = 4; //on quiz completion

$event = new stdClass();
$event->id = 8;
$event->name = _JLMS_NOTS_USER_COMPLETES_QUIZ;
$event->description = '';
$event->event_action = 'OnQuizCompletion'; // unique_name � ������� ������� ����� �������������� ��������.
$event->learner_template = 5;
$event->manager_template = 13;
$event->use_learner_template = true;
$event->use_manager_template = true;
$event->learner_template_disabled = false;
$event->manager_template_disabled = false;
$event->skip_managers = false;
$event->notification_type = $notification_type;
$event->selected_manager_roles = array(1,4,5);
$event->disabled = true;

$notification_events[] = $event;




$notification_type = 5; //on homework review

$event = new stdClass();
$event->id = 9;
$event->name = _JLMS_NOTS_TEACHER_REVIEWS_HOMEWORK_SUBMISSION;
$event->description = '';
$event->event_action = 'OnHomeworkReview'; // unique_name � ������� ������� ����� �������������� ��������.
$event->learner_template = 6;
$event->manager_template = 12;
$event->use_learner_template = true;
$event->use_manager_template = true;
$event->learner_template_disabled = false;
$event->manager_template_disabled = false;
$event->skip_managers = false;
$event->notification_type = $notification_type;
$event->selected_manager_roles = array(1,4,5);
$event->disabled = true;

$notification_events[] = $event;




$notification_type = 6; //on new dropbox

$event = new stdClass();
$event->id = 10;
$event->name = _JLMS_NOTS_NEW_DROPBOX_FILE_RECEIVED;
$event->description = '';
$event->event_action = 'OnNewDropboxFile'; // unique_name � ������� ������� ����� �������������� ��������.
$event->learner_template = 7;
$event->manager_template = 7;
$event->use_learner_template = true;
$event->use_manager_template = true;
$event->learner_template_disabled = false;
$event->manager_template_disabled = false;
$event->skip_managers = true;
$event->notification_type = $notification_type;
$event->selected_manager_roles = array(1,4,5);
$event->disabled = true;

$notification_events[] = $event;


$notification_type = 7; //on lpath completion

$event = new stdClass();
$event->id = 11;
$event->name = _JLMS_NOTS_USER_COMPLETES_LEARNING_PATH;
$event->description = '';
$event->event_action = 'OnLPathCompletion'; // unique_name � ������� ������� ����� �������������� ��������.
$event->learner_template = 8;
$event->manager_template = 9;
$event->use_learner_template = true;
$event->use_manager_template = true;
$event->learner_template_disabled = false;
$event->manager_template_disabled = false;
$event->skip_managers = false;
$event->notification_type = $notification_type;
$event->selected_manager_roles = array(1,4,5); //roles IDs
$event->disabled = true;

$notification_events[] = $event;


$notification_type = 8; //on homework submission

$event = new stdClass();
$event->id = 12;
$event->name = _JLMS_NOTS_USER_SUBMITS_HOMEWORK;
$event->description = '';
$event->event_action = 'OnHomeworkSubmission';
$event->learner_template = 11;
$event->manager_template = 10;
$event->use_learner_template = true;
$event->use_manager_template = true;
$event->learner_template_disabled = false;
$event->manager_template_disabled = false;
$event->skip_managers = false;
$event->notification_type = $notification_type;
$event->selected_manager_roles = array(1,4,5); //roles IDs
$event->disabled = true;

$notification_events[] = $event;
?>