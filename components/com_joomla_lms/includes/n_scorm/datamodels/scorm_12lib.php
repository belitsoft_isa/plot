<?php

function scorm_eval_prerequisites($prerequisites,$usertracks) {
    $element = '';
    $stack = array();
    $statuses = array(
                'passed' => 'passed',
                'completed' => 'completed',
                'failed' => 'failed',
                'incomplete' => 'incomplete',
                'browsed' => 'browsed',
                'not attempted' => 'notattempted',
                'p' => 'passed',
                'c' => 'completed',
                'f' => 'failed',
                'i' => 'incomplete',
                'b' => 'browsed',
                'n' => 'notattempted'
                );
    $i=0;  
    while ($i<strlen($prerequisites)) {
        $symbol = $prerequisites[$i];
        switch ($symbol) {
            case '&':
            case '|':
                $symbol .= $symbol;
            case '~':
            case '(':
            case ')':
            case '*':
                $element = trim($element);
                
                if (!empty($element)) {
                    $element = trim($element);
                    if (isset($usertracks[$element])) {
                        $element = '((\''.$usertracks[$element]->status.'\' == \'completed\') || '.
                                  '(\''.$usertracks[$element]->status.'\' == \'passed\'))'; 
                    } else if (($operator = strpos($element,'=')) !== false) {
                        $item = trim(substr($element,0,$operator));
                        if (!isset($usertracks[$item])) {
                            return false;
                        }
                        
                        $value = trim(trim(substr($element,$operator+1)),'"');
                        if (isset($statuses[$value])) {
                            $status = $statuses[$value];
                        } else {
                            return false;
                        }
                                              
                        $element = '(\''.$usertracks[$item]->status.'\' == \''.$status.'\')';
                    } else if (($operator = strpos($element,'<>')) !== false) {
                        $item = trim(substr($element,0,$operator));
                        if (!isset($usertracks[$item])) {
                            return false;
                        }
                        
                        $value = trim(trim(substr($element,$operator+2)),'"');
                        if (isset($statuses[$value])) {
                            $status = $statuses[$value];
                        } else {
                            return false;
                        }
                        
                        $element = '(\''.$usertracks[$item]->status.'\' != \''.$status.'\')';
                    } else if (is_numeric($element)) {
                        if ($symbol == '*') {
                            $symbol = '';
                            $open = strpos($prerequisites,'{',$i);
                            $opened = 1;
                            $closed = 0;
                            for ($close=$open+1; (($opened > $closed) && ($close<strlen($prerequisites))); $close++) { 
                                 if ($prerequisites[$close] == '}') {
                                     $closed++;
                                 } else if ($prerequisites[$close] == '{') {
                                     $opened++;
                                 }
                            } 
                            $i = $close;
                            
                            $setelements = explode(',', substr($prerequisites, $open+1, $close-($open+1)-1));
                            $settrue = 0;
                            foreach ($setelements as $setelement) {
                                if (scorm_eval_prerequisites($setelement,$usertracks)) {
                                    $settrue++;
                                }
                            }
                            
                            if ($settrue >= $element) {
                                $element = 'true'; 
                            } else {
                                $element = 'false';
                            }
                        }
                    } else {
                        return false;
                    }
                    
                    array_push($stack,$element);
                    $element = '';
                }
                if ($symbol == '~') {
                    $symbol = '!';
                }
                if (!empty($symbol)) {
                    array_push($stack,$symbol);
                }
            break;
            default:
                $element .= $symbol;
            break;
        }
        $i++;
    }
    if (!empty($element)) {
        $element = trim($element);
        if (isset($usertracks[$element])) {
            $element = '((\''.$usertracks[$element]->status.'\' == \'completed\') || '.
                       '(\''.$usertracks[$element]->status.'\' == \'passed\'))'; 
        } else if (($operator = strpos($element,'=')) !== false) {
            $item = trim(substr($element,0,$operator));
            if (!isset($usertracks[$item])) {
                return false;
            }
            
            $value = trim(trim(substr($element,$operator+1)),'"');
            if (isset($statuses[$value])) {
                $status = $statuses[$value];
            } else {
                return false;
            }
            
            $element = '(\''.$usertracks[$item]->status.'\' == \''.$status.'\')';
        } else if (($operator = strpos($element,'<>')) !== false) {
            $item = trim(substr($element,0,$operator));
            if (!isset($usertracks[$item])) {
                return false;
            }
            
            $value = trim(trim(substr($element,$operator+1)),'"');
            if (isset($statuses[$value])) {
                $status = $statuses[$value];
            } else {
                return false;
            }
            
            $element = '(\''.$usertracks[$item]->status.'\' != \''.trim($status).'\')';
        } else {
            return false;
        }
        
        array_push($stack,$element);
    }
    return eval('return '.implode($stack).';');
}

function scorm_get_toc($user,$scorm,$liststyle,$currentorg='',$scoid='',$mode='normal',$attempt='',$play=false) {
	global $JLMS_DB;
	
	//echo $scoid.'-------------------<br />';
	//global $CFG;
	// (DEN) insert language constant here !
	$strexpand = 'expcoll';//get_string('expcoll','scorm');
	$modestr = '';
	if ($mode == 'browse') {
		$modestr = '&amp;mode='.$mode;
	}
	// (DEN) // check this path
	$scormpixdir = _JOOMLMS_SCORM_PICS;//$CFG->modpixpath.'/scorm/pix';
	
	$result = new stdClass();
	$result->toc = "<ul id='0' class='$liststyle'>\n";
	$tocmenus = array();
	$result->prerequisites = true;
	$incomplete = false;

	$joomla_toc = new stdClass();
	$joomla_toc->organizationtitle = '';
	$joomla_toc->items = array();

	//
	// Get the current organization infos
	//
	$organizationsql = '';
	if (!empty($currentorg)) {
		$query = "SELECT title FROM #__lms_n_scorm_scoes WHERE scorm = $scorm->id AND identifier = ".$JLMS_DB->Quote($currentorg);
		$JLMS_DB->SetQuery($query);
		$organizationtitle = $JLMS_DB->LoadResult();
		//if (($organizationtitle = get_field('scorm_scoes','title','scorm',$scorm->id,'identifier',$currentorg)) != '') {
		if ($organizationtitle) {
			$result->toc .= "\t<li>$organizationtitle</li>\n";
			$joomla_toc->organizationtitle = $organizationtitle;
			$tocmenus[] = $organizationtitle;
		}
		$organizationsql = "AND organization='$currentorg'";
	}
	//
	// If not specified retrieve the last attempt number
	//
	if (empty($attempt)) {
		$attempt = scorm_get_last_attempt($scorm->id, $user->id);
	}
	$result->attemptleft = $scorm->maxattempt - $attempt;
	$query = "SELECT * FROM #__lms_n_scorm_scoes WHERE scorm='$scorm->id' $organizationsql order by id ASC";
	$JLMS_DB->SetQuery($query);
	$scoes = $JLMS_DB->LoadObjectList();
	$statusicon = '';
	$statusicon_txt = '';
	//if ($scoes = get_records_select('scorm_scoes',"scorm='$scorm->id' $organizationsql order by id ASC")){
	if (!empty($scoes)) {
		//
		// Retrieve user tracking data for each learning object
		//
		
		$track_attempt = $attempt;
		if ($track_attempt > 1) {
			global $JLMS_CONFIG;
			$course_params = $JLMS_CONFIG->get('course_params');
			$params = new JLMSParameters($course_params);
			if ($params->get('track_type',0) == 1) { //by the best attaempt
				$track_attempt--;
			}
		}
		$usertracks = array();
		foreach ($scoes as $sco) {
			if (!empty($sco->launch)) {
				if ($usertrack=scorm_get_tracks($sco->id,$user->id,$track_attempt)) {
					if ($usertrack->status == '') {
						$usertrack->status = 'notattempted';
					}
					$usertracks[$sco->identifier] = $usertrack;
				}
			}
		}
		
		$level=0;
		$sublist=1;
		$previd = 0;
		$nextid = 0;
		$findnext = false;
		$parents[$level]='/';
$iiittt = 0;
		foreach ($scoes as $sco) {//echo '<pre>';print_r($sco);echo '</pre>';
			$isvisible = false;
				if ($optionaldatas = scorm_get_sco($sco->id, SCO_DATA)) {
				if (!isset($optionaldatas->isvisible) || (isset($optionaldatas->isvisible) && ($optionaldatas->isvisible == 'true'))) {
					$isvisible = true;
				}
			}
			if ($parents[$level]!=$sco->parent) {
				if ($newlevel = array_search($sco->parent,$parents)) {
					for ($i=0; $i<($level-$newlevel); $i++) {
						$result->toc .= "\t\t</ul></li>\n";
					}
					$level = $newlevel;
				} else {
					$i = $level;
					$closelist = '';
					while (($i > 0) && ($parents[$level] != $sco->parent)) {
						$closelist .= "\t\t</ul></li>\n";
						$i--;
					}
					if (($i == 0) && ($sco->parent != $currentorg)) {
						$style = '';
						if (isset($_COOKIE['hide:SCORMitem'.$sco->id])) {
							$style = ' style="display: none;"';
						}
						$result->toc .= "\t\t<li><ul id='$sublist' class='$liststyle'$style>\n";
						$level++;
					} else {
						$result->toc .= $closelist;
						$level = $i;
					}
					$parents[$level]=$sco->parent;
				}
			}
			if ($isvisible) {
				$result->toc .= "\t\t<li>";
			}
			/*echo '<pre>';
			print_r($scoes);
			echo '</pre>';*/
			if (isset($scoes[$iiittt + 1])) {
				$nextsco = $scoes[$iiittt + 1];
			} else {
				$nextsco = false;
			}
			//$nextsco = next($scoes);
			$nextisvisible = false;
			if (($nextsco !== false) && ($optionaldatas = scorm_get_sco($nextsco->id, SCO_DATA))) {
				if (!isset($optionaldatas->isvisible) || (isset($optionaldatas->isvisible) && ($optionaldatas->isvisible == 'true'))) {
					$nextisvisible = true;
				}
			}
			if ($nextisvisible && ($nextsco !== false) && ($sco->parent != $nextsco->parent) && (($level==0) || (($level>0) && ($nextsco->parent == $sco->identifier)))) {
				$sublist++;
				$icon = 'minus';
				if (isset($_COOKIE['hide:SCORMitem'.$nextsco->id])) {
					$icon = 'plus';
				}
				$result->toc .= '<a href="javascript:expandCollide(\'img'.$sublist.'\','.$sublist.','.$nextsco->id.');"><img id="img'.$sublist.'" src="'.$scormpixdir.'/'.$icon.'.gif" alt="'.$strexpand.'" title="'.$strexpand.'" border="0"/></a>';
			} else if ($isvisible) {
				$result->toc .= '<img src="'.$scormpixdir.'/spacer.gif" />';
			}
			if (empty($sco->title)) {
				$sco->title = $sco->identifier;
			}
			/*$rrrttt = !empty($sco->launch) ? true : false;
			echo $scoid . '---'.$sco->id.'---'.$sco->launch.'---';var_dump($rrrttt);echo '<br />';*/
			if (!empty($sco->launch)) {
				if ($isvisible) {
					$startbold = '';
					$endbold = '';
					$score = '';
					if (empty($scoid) && ($mode != 'normal')) {
						$scoid = $sco->id;
					}
					if (isset($usertracks[$sco->identifier])) {
						$usertrack = $usertracks[$sco->identifier];
						// (DEN) put language constant here !
						$strstatus = $usertrack->status;//get_string($usertrack->status,'scorm');
						if ($sco->scormtype == 'sco') {
							$statusicon = '<img src="'.$scormpixdir.'/'.$usertrack->status.'.gif" alt="'.$strstatus.'" title="'.$strstatus.'" />';
							$statusicon_txt = $usertrack->status;
						} else {
							//$tmp_alt = get_string('assetlaunched','scorm');
							// (DEN) put language constant here !
							$tmp_alt = 'assetlaunched';
							$statusicon = '<img src="'.$scormpixdir.'/assetc.gif" alt="'.$tmp_alt.'" title="'.$tmp_alt.'" />';
							$statusicon_txt = 'assetc';
						}

						if (($usertrack->status == 'notattempted') || ($usertrack->status == 'incomplete') || ($usertrack->status == 'browsed')) {
							$incomplete = true;
							if ($play && empty($scoid)) {
								$scoid = $sco->id;
							}
						}
						if ($usertrack->score_raw != '') {
							// (DEN) put language constant here !
							//$tmp_score_str = get_string('score','scorm');
							$tmp_score_str = 'score';
							$score = '('.$tmp_score_str.':&nbsp;'.$usertrack->score_raw.')';
						}
						// (DEN) put language constant here !
						//$strsuspended = get_string('suspended','scorm');
						$strsuspended = 'suspended';
						if (isset($usertrack->{'cmi.core.exit'}) && ($usertrack->{'cmi.core.exit'} == 'suspend')) {
							$statusicon = '<img src="'.$scormpixdir.'/suspend.gif" alt="'.$strstatus.' - '.$strsuspended.'" title="'.$strstatus.' - '.$strsuspended.'" />';
							$statusicon_txt = 'suspend';
						}
					} else {
						if ($play && empty($scoid)) {
							$scoid = $sco->id;
						}
						if ($sco->scormtype == 'sco') {
							//$tmp_alt = get_string('notattempted','scorm');
							// (DEN) put language constant here !
							$tmp_alt = 'notattempted';
							$statusicon = '<img src="'.$scormpixdir.'/notattempted.gif" alt="'.$tmp_alt.'" title="'.$tmp_alt.'" />';
							$statusicon_txt = 'notattempted';
							$incomplete = true;
						} else {
							//$tmp_alt = get_string('asset','scorm');
							// (DEN) put language constant here !
							$tmp_alt = 'asset';
							$statusicon = '<img src="'.$scormpixdir.'/asset.gif" alt="'.$tmp_alt.'" title="'.$tmp_alt.'" />';
							$statusicon_txt = 'asset';
						}
					}
					if ($sco->id == $scoid) {
						$scodata = scorm_get_sco($sco->id, SCO_DATA);
						$startbold = '<b>';
						$endbold = '</b>';
						$findnext = true;
						$shownext = isset($scodata->next) ? $scodata->next : 0;
						$showprev = isset($scodata->previous) ? $scodata->previous : 0;
					}

					if (($nextid == 0) && (scorm_count_launchable($scorm->id,$currentorg) > 1) && ($nextsco!==false) && (!$findnext)) {
						if (!empty($sco->launch)) {
							$previd = $sco->id;
						}
					}
					if (empty($sco->prerequisites) || scorm_eval_prerequisites($sco->prerequisites,$usertracks)) {
						if ($sco->id == $scoid) {
							$result->prerequisites = true;
						}
						// (DEN)
						global $option, $Itemid;
						$url = sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=player_scorm&amp;id=".$scorm->id.'&amp;scoid='.$sco->id.$modestr."&amp;currentorg=".$currentorg);
						//$url = $CFG->wwwroot.'/mod/scorm/player.php?a='.$scorm->id.'&amp;currentorg='.$currentorg.$modestr.'&amp;scoid='.$sco->id;
						$result->toc .= $statusicon.'&nbsp;'.$startbold.'<a href="'.$url.'">'.$sco->title.'</a>'.$score.$endbold."</li>\n";
						$is_bold = $startbold ? true : false;
						$joomla_toc->items[] = array( 'name' => $sco->title, 'img' => $statusicon, 'img_src' => $statusicon_txt, 'href' => true, 'bold' => $is_bold, 'level' => $level, 'scoid' => $sco->id, 'currentorg' => $currentorg);
						//$result->toc .= $statusicon.'&nbsp;'.$startbold.'<a href="'.$url.'">'.format_string($sco->title).'</a>'.$score.$endbold."</li>\n";
						$tocmenus[$sco->id] = scorm_repeater('&minus;',$level) . '&gt;' . $sco->title;
						//$tocmenus[$sco->id] = scorm_repeater('&minus;',$level) . '&gt;' . format_string($sco->title);
					} else {
						if ($sco->id == $scoid) {
							$result->prerequisites = false;
						}
						$result->toc .= $statusicon.'&nbsp;'.$sco->title."</li>\n";
						//$joomla_toc->items[] = array( 'name' => $statusicon.'&nbsp;'.$sco->title, 'level' => $level );
						$joomla_toc->items[] = array( 'name' => $sco->title, 'img' => $statusicon, 'img_src' => $statusicon_txt, 'href' => false, 'bold' => false, 'level' => $level, 'scoid' => '', 'currentorg' => '');
					}
				}
			} else {
				$result->toc .= '&nbsp;'.$sco->title."</li>\n";
				//$joomla_toc->items[] = array( 'name' => $sco->title, 'level' => $level );
				$joomla_toc->items[] = array( 'name' => $sco->title, 'img' => '', 'img_src' => '', 'href' => false, 'bold' => true, 'level' => $level, 'scoid' => '', 'currentorg' => '');
			}
			if (($nextsco !== false) && ($nextid == 0) && ($findnext)) {
				if (!empty($nextsco->launch)) {
					$nextid = $nextsco->id;
				}
			}
			$iiittt++;
		}
		//print_r($sco);
		for ($i=0;$i<$level;$i++) {
			$result->toc .= "\t\t</ul></li>\n";
		}

		if ($play) {
			/*echo '---'.$scoid.'---';*/
			$sco = scorm_get_sco($scoid);
			//echo '-!!!-';echo '<pre>';var_dump($scoid);var_dump($sco);echo '</pre>';echo '-!!!--';
			$sco->previd = $previd;
			$sco->nextid = $nextid;
			//print_r($sco);//die;
			$result->sco = $sco;
			$result->incomplete = $incomplete;
		} else {
			$result->incomplete = $incomplete;
		}
		//die('%%%');
	}
	$result->toc .= "\t</ul>\n";
	if ($scorm->hidetoc == 0) {
		$result->toc .= '
			<script type="text/javascript">
			//<![CDATA[
				function expandCollide(which,list,item) {
					var nn=document.ids?true:false
					var w3c=document.getElementById?true:false
					var beg=nn?"document.ids.":w3c?"document.getElementById(":"document.all.";
					var mid=w3c?").style":".style";

					if (eval(beg+list+mid+".display") != "none") {
						which.src = "'.$scormpixdir.'/plus.gif";
						eval(beg+list+mid+".display=\'none\';");
						new cookie("hide:SCORMitem" + item, 1, 356, "/").set();
					} else {
						which.src = "'.$scormpixdir.'/minus.gif";
						eval(beg+list+mid+".display=\'block\';");
						new cookie("hide:SCORMitem" + item, 1, -1, "/").set();
					}
				}
			//]]>
			</script>'."\n";
	}
	// (DEN)
	//global $option, $Itemid;
	//$url = sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=player_scorm&amp;id=".$scorm->id.'&amp;currentorg='.$currentorg.$modestr.'&amp;scoid=');
	// $url = $CFG->wwwroot.'/mod/scorm/player.php?a='.$scorm->id.'&amp;currentorg='.$currentorg.$modestr.'&amp;scoid=';
	$result->tocmenu = '';//"..........popup_form.......";//$result->tocmenu = popup_form($url,$tocmenus, "tocmenu", $sco->id, '', '', '', true);

	$result->joomla_toc = $joomla_toc;
	//print_r($result);die;
	return $result;
}

?>
