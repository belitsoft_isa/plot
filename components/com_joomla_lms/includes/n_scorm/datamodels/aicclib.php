<?php // $Id: aicclib.php,v 1.3 2007/01/03 14:45:01 moodler Exp $
function scorm_add_time($a, $b) {
	$aes = explode(':',$a);
	$bes = explode(':',$b);
	$aseconds = explode('.',$aes[2]);
	$bseconds = explode('.',$bes[2]);
	$change = 0;

	$acents = 0;  //Cents
	if (count($aseconds) > 1) {
		$acents = $aseconds[1];
	}
	$bcents = 0;
	if (count($bseconds) > 1) {
		$bcents = $bseconds[1];
	}
	$cents = $acents + $bcents;
	$change = floor($cents / 100);
	$cents = $cents - ($change * 100);
	if (floor($cents) < 10) {
		$cents = '0'. $cents;
	}

	$secs = $aseconds[0] + $bseconds[0] + $change;  //Seconds
	$change = floor($secs / 60);
	$secs = $secs - ($change * 60);
	if (floor($secs) < 10) {
		$secs = '0'. $secs;
	}

	$mins = $aes[1] + $bes[1] + $change;   //Minutes
	$change = floor($mins / 60);
	$mins = $mins - ($change * 60);
	if ($mins < 10) {
		$mins = '0' .  $mins;
	}

	$hours = $aes[0] + $bes[0] + $change;  //Hours
	if ($hours < 10) {
		$hours = '0' . $hours;
	}

	if ($cents != '0') {
		return $hours . ":" . $mins . ":" . $secs . '.' . $cents;
	} else {
		return $hours . ":" . $mins . ":" . $secs;
	}
}

/**
* Take the header row of an AICC definition file
* and returns sequence of columns and a pointer to
* the sco identifier column.
*
* @param string $row AICC header row
* @param string $mastername AICC sco identifier column
* @return mixed
*/
function scorm_get_aicc_columns($row,$mastername='system_id') {
	$tok = strtok(strtolower($row),"\",\n\r");
	$result->columns = array();
	$i=0;
	while ($tok) {
		if ($tok !='') {
			$result->columns[] = $tok;
			if ($tok == $mastername) {
				$result->mastercol = $i;
			}
			$i++;
		}
		$tok = strtok("\",\n\r");
	}
	return $result;
}

/**
* Given a colums array return a string containing the regular
* expression to match the columns in a text row.
*
* @param array $column The header columns
* @param string $remodule The regular expression module for a single column
* @return string
*/
function scorm_forge_cols_regexp($columns,$remodule='(".*")?,') {
	$regexp = '/^';
	foreach ($columns as $column) {
		$regexp .= $remodule;
	}
	$regexp = substr($regexp,0,-1) . '/';
	return $regexp;
}

function scorm_parse_aicc($pkgdir,$scormid) {
	global $JLMS_DB;
    $version = 'AICC';
    $ids = array();
    $courses = array();
    $extaiccfiles = array('crs','des','au','cst','ort','pre','cmp');
    if ($handle = opendir($pkgdir)) {
        while (($file = readdir($handle)) !== false) {
            if ($file[0] != '.') {
                $ext = substr($file,strrpos($file,'.'));
                $extension = strtolower(substr($ext,1));
                if (in_array($extension,$extaiccfiles)) {
                    $id = strtolower(basename($file,$ext));
                    $ids[$id]->$extension = $file;
                }
            }
        }
        closedir($handle);
    }
    foreach ($ids as $courseid => $id) {
        if (isset($id->crs)) {
            if (is_file($pkgdir.'/'.$id->crs)) {
                $rows = file($pkgdir.'/'.$id->crs);
                foreach ($rows as $row) {
                    if (preg_match("/^(.+)=(.+)$/",$row,$matches)) {
                        switch (strtolower(trim($matches[1]))) {
                            case 'course_id':
                                $courses[$courseid]->id = trim($matches[2]);
                            break;
                            case 'course_title':
                                $courses[$courseid]->title = trim($matches[2]);
                            break;
                            case 'version':
                                $courses[$courseid]->version = 'AICC_'.trim($matches[2]);
                            break;
                        }
                    }
                }
            }
        }
        if (isset($id->des)) {
            $rows = file($pkgdir.'/'.$id->des);
            $columns = scorm_get_aicc_columns($rows[0]);
            $regexp = scorm_forge_cols_regexp($columns->columns);
            for ($i=1;$i<count($rows);$i++) {
                if (preg_match($regexp,$rows[$i],$matches)) {
                    for ($j=0;$j<count($columns->columns);$j++) {
                        $column = $columns->columns[$j];
                        $courses[$courseid]->elements[substr(trim($matches[$columns->mastercol+1]),1,-1)]->$column = substr(trim($matches[$j+1]),1,-1);
                    }
                }
            }
        }
        if (isset($id->au)) {
            $rows = file($pkgdir.'/'.$id->au);
            $columns = scorm_get_aicc_columns($rows[0]);
            $regexp = scorm_forge_cols_regexp($columns->columns);
            for ($i=1;$i<count($rows);$i++) {
                if (preg_match($regexp,$rows[$i],$matches)) {
                    for ($j=0;$j<count($columns->columns);$j++) {
                        $column = $columns->columns[$j];
                        $courses[$courseid]->elements[substr(trim($matches[$columns->mastercol+1]),1,-1)]->$column = substr(trim($matches[$j+1]),1,-1);
                    }
                }
            }
        }
        if (isset($id->cst)) {
            $rows = file($pkgdir.'/'.$id->cst);
            $columns = scorm_get_aicc_columns($rows[0],'block');
            $regexp = scorm_forge_cols_regexp($columns->columns,'(.+)?,');
            for ($i=1;$i<count($rows);$i++) {
                if (preg_match($regexp,$rows[$i],$matches)) {
                    for ($j=0;$j<count($columns->columns);$j++) {
                        if ($j != $columns->mastercol) {
                            $courses[$courseid]->elements[substr(trim($matches[$j+1]),1,-1)]->parent = substr(trim($matches[$columns->mastercol+1]),1,-1);
                        }
                    }
                }
            }
        }
        if (isset($id->ort)) {
            $rows = file($pkgdir.'/'.$id->ort);
        }
        if (isset($id->pre)) {
            $rows = file($pkgdir.'/'.$id->pre);
            $columns = scorm_get_aicc_columns($rows[0],'structure_element');
            $regexp = scorm_forge_cols_regexp($columns->columns,'(.+),');
            for ($i=1;$i<count($rows);$i++) {
                if (preg_match($regexp,$rows[$i],$matches)) {
                    $courses[$courseid]->elements[$columns->mastercol+1]->prerequisites = substr(trim($matches[1-$columns->mastercol+1]),1,-1);
                }
            }
        }
        if (isset($id->cmp)) {
            $rows = file($pkgdir.'/'.$id->cmp);
        }
    }
    //print_r($courses);

    #$oldscoes = get_records('scorm_scoes','scorm',$scormid);
    $query = "SELECT * FROM #__lms_n_scorm_scoes WHERE scorm = $scormid";
    $JLMS_DB->SetQuery($query);
    $oldscoes = $JLMS_DB->LoadObjectList();
     #echo "1".$JLMS_DB->GetErrormsg();
    $launch = 0;
    if (isset($courses)) {
        foreach ($courses as $course) {
            unset($sco);
            $sco->identifier = $course->id;
            $sco->scorm = $scormid;
            $sco->organization = '';
            $sco->title = $course->title;
            $sco->parent = '/';
            $sco->launch = '';
            $sco->scormtype = '';

            //print_r($sco);

			/* (DEN) Check this code !!!! */
            $query = "SELECT * FROM #__lms_n_scorm_scoes WHERE scorm = $scormid AND identifier = " . $JLMS_DB->Quote($sco->identifier);
            $JLMS_DB->SetQuery( $query );
            $tmp_sco = $JLMS_DB->LoadObject();
            if (is_object($tmp_sco)) {
            	$query = "UPDATE #__lms_n_scorm_scoes SET ".JLMSDatabaseHelper::NameQuote('organization')." = ".$JLMS_DB->Quote($sco->organization).","
            	. "\n ".JLMSDatabaseHelper::NameQuote('organization')." = ".$JLMS_DB->Quote($sco->organization).","
            	. "\n ".JLMSDatabaseHelper::NameQuote('title')." = ".$JLMS_DB->Quote($sco->title).","
            	. "\n ".JLMSDatabaseHelper::NameQuote('parent')." = ".$JLMS_DB->Quote($sco->parent).","
            	. "\n ".JLMSDatabaseHelper::NameQuote('launch')." = ".$JLMS_DB->Quote($sco->launch).","
            	. "\n ".JLMSDatabaseHelper::NameQuote('scormtype')." = ".$JLMS_DB->Quote($sco->scormtype)
            	. "\n WHERE ".JLMSDatabaseHelper::NameQuote('scorm')." = $scormid AND ".JLMSDatabaseHelper::NameQuote('identifier')." = $sco->identifier";
            	$JLMS_DB->SetQuery( $query );
            	$JLMS_DB->query();
            	$id = $tmp_sco->id;
            	 #echo "2".$JLMS_DB->GetErrormsg();
            } else {
              	
            	$JLMS_DB->InsertObject('#__lms_n_scorm_scoes', $sco, 'id');
            	$id = $JLMS_DB->insertid();
            	 #echo "3".$JLMS_DB->GetErrormsg();
            }
            
            // Mistake in Moodle ? (in $sco object filed 'id' doesn't exists! )
            /*if (get_record('scorm_scoes','scorm',$scormid,'identifier',$sco->identifier)) {
                $id = update_record('scorm_scoes',$sco);
                unset($oldscoes[$id]);
            } else {
                $id = insert_record('scorm_scoes',$sco);
            }*/

            if ($launch == 0) {
                $launch = $id;
            }
            if (isset($course->elements)) {
                foreach($course->elements as $element) {
                    unset($sco);
                    $sco->identifier = $element->system_id;
                    $sco->scorm = $scormid;
                    $sco->organization = $course->id;
                    $sco->title = $element->title;
                    if (strtolower($element->parent) == 'root') {
                        $sco->parent = '/';
                    } else {
                        $sco->parent = $element->parent;
                    }
                    if (isset($element->file_name)) {
                        $sco->launch = $element->file_name;
                        $sco->scormtype = 'sco';
                    } else {
                        $element->file_name = '';
                        $sco->scormtype = '';
                    }
                    if (!isset($element->prerequisites)) {
                        $element->prerequisites = '';
                    }
                    $sco->prerequisites = $element->prerequisites;
                    if (!isset($element->max_time_allowed)) {
                        $element->max_time_allowed = '';
                    }
                    $sco->maxtimeallowed = $element->max_time_allowed;
                    if (!isset($element->time_limit_action)) {
                        $element->time_limit_action = '';
                    }
                    $sco->timelimitaction = $element->time_limit_action;
                    if (!isset($element->mastery_score)) {
                        $element->mastery_score = '';
                    }
                    $sco->masteryscore = $element->mastery_score;
                    $sco->previous = 0;
                    $sco->next = 0;
                    if ($oldscoid = scorm_array_search('identifier',$sco->identifier,$oldscoes)) {
                        $sco->id = $oldscoid;
                        $sco_tmp = new stdClass();
                    	$sco_tmp = $sco;
                    	unset($sco_tmp->prerequisites);
                    	unset($sco_tmp->max_time_allowed);
                    	unset($sco_tmp->time_limit_action);
                    	unset($sco_tmp->mastery_score);
                    	unset($sco_tmp->previous);
                    	unset($sco_tmp->next);
                        $JLMS_DB->UpdateObject('#__lms_n_scorm_scoes', $sco_tmp, 'id');
                        $id = $sco_tmp->id;
                        //$id = update_record('scorm_scoes',$sco);
                        unset($oldscoes[$oldscoid]);
                    } else {
                    	$sco_tmp = new stdClass();
                    	$sco_tmp = $sco;
                    	unset($sco_tmp->prerequisites);
                    	unset($sco_tmp->maxtimeallowed);
                    	unset($sco_tmp->timelimitaction);
                    	unset($sco_tmp->masteryscore);
                    	unset($sco_tmp->previous);
                    	unset($sco_tmp->next);
                    	$JLMS_DB->InsertObject('#__lms_n_scorm_scoes', $sco_tmp, 'id');
                    	$id = $JLMS_DB->insertid();
                        //$id = insert_record('scorm_scoes',$sco);
                    }
                    if ($launch==0) {
                        $launch = $id;
                    }
                }
            }
        }
    }
    if (!empty($oldscoes)) {
        foreach($oldscoes as $oldsco) {
        	$query = "DELETE FROM #__lms_n_scorm_scoes WHERE id = ".intval($oldsco->id);
        	$JLMS_DB->SetQuery($query);
        	$JLMS_DB->query();
        	$query = "DELETE FROM #__lms_n_scorm_scoes_track WHERE scoid = ".intval($oldsco->id);
        	$JLMS_DB->SetQuery($query);
        	$JLMS_DB->query();
            #delete_records('scorm_scoes','id',$oldsco->id);
            #delete_records('scorm_scoes_track','scoid',$oldsco->id);
        }
    }
    #echo "4".$JLMS_DB->GetErrormsg();
    $query = "UPDATE #__lms_n_scorm SET ".JLMSDatabaseHelper::NameQuote('version')." = ".$JLMS_DB->Quote('AICC')." WHERE id = ".intval($scormid);
    $JLMS_DB->SetQuery($query);
    $JLMS_DB->query();
    //set_field('scorm','version','AICC','id',$scormid);
    #die;
    return $launch;
}

function scorm_get_toc($user,$scorm,$liststyle,$currentorg='',$scoid='',$mode='normal',$attempt='',$play=false) {
	global $JLMS_DB;
    //global $CFG;
    // (DEN) // insert here language constant
    $strexpand = 'expcoll';//get_string('expcoll','scorm');
    $modestr = '';
    if ($mode == 'browse') {
        $modestr = '&amp;mode='.$mode;
    }
    // (DEN) // check this path
    $scormpixdir = _JOOMLMS_SCORM_PICS;//$CFG->modpixpath.'/scorm/pix';
    
    $result = new stdClass();
    $result->toc = "<ul id='0' class='$liststyle'>\n";
    $tocmenus = array();
    $result->prerequisites = true;
    $incomplete = false;
    
    //
    // Get the current organization infos
    //
    $organizationsql = '';
    if (!empty($currentorg)) {
    	$query = "SELECT title FROM #__lms_n_scorm_scoes WHERE scorm = $scorm->id AND identifier = ".$JLMS_DB->Quote($currentorg);
    	$JLMS_DB->SetQuery($query);
    	$organizationtitle = $JLMS_DB->LoadResult();
        //if (($organizationtitle = get_field('scorm_scoes','title','scorm',$scorm->id,'identifier',$currentorg)) != '') {
        if ($organizationtitle) {
            $result->toc .= "\t<li>$organizationtitle</li>\n";
            $tocmenus[] = $organizationtitle;
        }
        $organizationsql = "AND organization='$currentorg'";
    }
    //
    // If not specified retrieve the last attempt number
    //
    if (empty($attempt)) {
        $attempt = scorm_get_last_attempt($scorm->id, $user->id);
    }

    $result->attemptleft = $scorm->maxattempt - $attempt;
    $query = "SELECT * FROM #__lms_n_scorm_scoes WHERE scorm='$scorm->id' $organizationsql order by id ASC";
    $JLMS_DB->SetQuery($query);
    $scoes = $JLMS_DB->LoadObjectList();
    //if ($scoes = get_records_select('scorm_scoes',"scorm='$scorm->id' $organizationsql order by id ASC")){
    if (!empty($scoes)) {
        //
        // Retrieve user tracking data for each learning object
        // 
        $usertracks = array();
        foreach ($scoes as $sco) {
            if (!empty($sco->launch)) {
                if ($usertrack=scorm_get_tracks($sco->id,$user->id,$attempt)) {
                    if ($usertrack->status == '') {
                        $usertrack->status = 'notattempted';
                    }
                    $usertracks[$sco->identifier] = $usertrack;
                }
            }
        }

        $level=0;
        $sublist=1;
        $previd = 0;
        $nextid = 0;
        $findnext = false;
        $parents[$level]='/';
        
        foreach ($scoes as $sco) {
            if ($parents[$level]!=$sco->parent) {
                if ($newlevel = array_search($sco->parent,$parents)) {
                    for ($i=0; $i<($level-$newlevel); $i++) {
                        $result->toc .= "\t\t</ul></li>\n";
                    }
                    $level = $newlevel;
                } else {
                    $i = $level;
                    $closelist = '';
                    while (($i > 0) && ($parents[$level] != $sco->parent)) {
                        $closelist .= "\t\t</ul></li>\n";
                        $i--;
                    }
                    if (($i == 0) && ($sco->parent != $currentorg)) {
                        $style = '';
                        if (isset($_COOKIE['hide:SCORMitem'.$sco->id])) {
                            $style = ' style="display: none;"';
                        }
                        $result->toc .= "\t\t<li><ul id='$sublist' class='$liststyle'$style>\n";
                        $level++;
                    } else {
                        $result->toc .= $closelist;
                        $level = $i;
                    }
                    $parents[$level]=$sco->parent;
                }
            }
            $result->toc .= "\t\t<li>";
            $nextsco = next($scoes);
            if (($nextsco !== false) && ($sco->parent != $nextsco->parent) && (($level==0) || (($level>0) && ($nextsco->parent == $sco->identifier)))) {
                $sublist++;
                $icon = 'minus';
                if (isset($_COOKIE['hide:SCORMitem'.$nextsco->id])) {
                    $icon = 'plus';
                }
                $result->toc .= '<a href="javascript:expandCollide(img'.$sublist.','.$sublist.','.$nextsco->id.');"><img id="img'.$sublist.'" src="'.$scormpixdir.'/'.$icon.'.gif" alt="'.$strexpand.'" title="'.$strexpand.'"/></a>';
            } else {
                $result->toc .= '<img src="'.$scormpixdir.'/spacer.gif" />';
            }
            if (empty($sco->title)) {
                $sco->title = $sco->identifier;
            }
            if (!empty($sco->launch)) {
                $startbold = '';
                $endbold = '';
                $score = '';
                if (empty($scoid) && ($mode != 'normal')) {
                    $scoid = $sco->id;
                }
                if (isset($usertracks[$sco->identifier])) {
                    $usertrack = $usertracks[$sco->identifier];
                    // (DEN) put language constant here !
                    $strstatus = $usertrack->status;
                    //$strstatus = get_string($usertrack->status,'scorm');
                    if ($sco->scormtype == 'sco') {
                        $statusicon = '<img src="'.$scormpixdir.'/'.$usertrack->status.'.gif" alt="'.$strstatus.'" title="'.$strstatus.'" />';
                    } else {
                    	//$tmp_alt = get_string('assetlaunched','scorm');
                    	// (DEN) put language constant here !
                    	$tmp_alt = 'assetlaunched';
                        $statusicon = '<img src="'.$scormpixdir.'/assetc.gif" alt="'.$tmp_alt.'" title="'.$tmp_alt.'" />';
                    }
                    
                    if (($usertrack->status == 'notattempted') || ($usertrack->status == 'incomplete') || ($usertrack->status == 'browsed')) {
                        $incomplete = true;
                        if ($play && empty($scoid)) {
                            $scoid = $sco->id;
                        }
                    }
                    if ($usertrack->score_raw != '') {
                    	// (DEN) put language constant here !
                    	//$tmp_score_str = get_string('score','scorm');
                    	$tmp_score_str = 'score';
                        $score = '('.$tmp_score_str.':&nbsp;'.$usertrack->score_raw.')';
                    }
                    // (DEN) put language constant here !
                    //$strsuspended = get_string('suspended','scorm');
                    $strsuspended = 'suspended';
                    if ($usertrack->{'cmi.core.exit'} == 'suspend') {
                        $statusicon = '<img src="'.$scormpixdir.'/suspend.gif" alt="'.$strstatus.' - '.$strsuspended.'" title="'.$strstatus.' - '.$strsuspended.'" />';
                    }
                } else {
                    if ($play && empty($scoid)) {
                        $scoid = $sco->id;
                    }
                    if ($sco->scormtype == 'sco') {
                    	//$tmp_alt = get_string('notattempted','scorm');
                    	// (DEN) put language constant here !
                    	$tmp_alt = 'notattempted';
                        $statusicon = '<img src="'.$scormpixdir.'/notattempted.gif" alt="'.$tmp_alt.'" title="'.$tmp_alt.'" />';
                        $incomplete = true;
                    } else {
                    	//$tmp_alt = get_string('asset','scorm');
                    	// (DEN) put language constant here !
                    	$tmp_alt = 'asset';
                        $statusicon = '<img src="'.$scormpixdir.'/asset.gif" alt="'.$tmp_alt.'" title="'.$tmp_alt.'" />';
                    }
                }
                if ($sco->id == $scoid) {
                    $startbold = '<b>';
                    $endbold = '</b>';
                    $findnext = true;

                    if(isset($sco->next)){
                        $shownext = $sco->next;
                    }

                    if(isset($sco->previous)){
                        $showprev = $sco->previous;
                    }
                }
                
                if (($nextid == 0) && (scorm_count_launchable($scorm->id,$currentorg) > 1) && ($nextsco!==false) && (!$findnext)) {
                    if (!empty($sco->launch)) {
                        $previd = $sco->id;
                    }
                }
                // (DEN) i don't understand where function "scorm_eval_prerequisites()" is defined
                if (empty($sco->prerequisites) || scorm_eval_prerequisites($sco->prerequisites,$usertracks)) {
                    if ($sco->id == $scoid) {
                        $result->prerequisites = true;
                    }
                    global $option, $Itemid;
                    $url = sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=player_scorm&amp;id=".$scorm->id.'&amp;currentorg='.$currentorg.$modestr.'&amp;scoid='.$sco->id);
                    //$url = $CFG->wwwroot.'/mod/scorm/player.php?a='.$scorm->id.'&amp;currentorg='.$currentorg.$modestr.'&amp;scoid='.$sco->id;
                    $result->toc .= $statusicon.'&nbsp;'.$startbold.'<a href="'.$url.'">'.$sco->title.'</a>'.$score.$endbold."</li>\n";
                    //$result->toc .= $statusicon.'&nbsp;'.$startbold.'<a href="'.$url.'">'.format_string($sco->title).'</a>'.$score.$endbold."</li>\n";
                    $tocmenus[$sco->id] = scorm_repeater('&minus;',$level) . '&gt;' . $sco->title;
                    //$tocmenus[$sco->id] = scorm_repeater('&minus;',$level) . '&gt;' . format_string($sco->title);
                } else {
                    if ($sco->id == $scoid) {
                        $result->prerequisites = false;
                    }
                    $result->toc .= '&nbsp;'.$sco->title."</li>\n";
                }
            } else {
                $result->toc .= '&nbsp;'.$sco->title."</li>\n";
            }
            if (($nextsco !== false) && ($nextid == 0) && ($findnext)) {
                if (!empty($nextsco->launch)) {
                    $nextid = $nextsco->id;
                }
            }
        }
        for ($i=0;$i<$level;$i++) {
            $result->toc .= "\t\t</ul></li>\n";
        }
        
        if ($play) {
        	$query = "SELECT * FROM #__lms_n_scorm_scoes WHERE id = $scoid";
        	$JLMS_DB->SetQuery($query);
        	$sco = $JLMS_DB->LoadObject();
            //$sco = get_record('scorm_scoes','id',$scoid);
            $sco->previd = $previd;
            $sco->nextid = $nextid;
            $result->sco = $sco;
            $result->incomplete = $incomplete;
        } else {
            $result->incomplete = $incomplete;
        }
    }
    $result->toc .= "\t</ul>\n";
    if ($scorm->hidetoc == 0) {
        $result->toc .= '
          <script type="text/javascript">
          //<![CDATA[
              function expandCollide(which,list,item) {
                  var nn=document.ids?true:false
                  var w3c=document.getElementById?true:false
                  var beg=nn?"document.ids.":w3c?"document.getElementById(":"document.all.";
                  var mid=w3c?").style":".style";

                  if (eval(beg+list+mid+".display") != "none") {
                      which.src = "'.$scormpixdir.'/plus.gif";
                      eval(beg+list+mid+".display=\'none\';");
                      new cookie("hide:SCORMitem" + item, 1, 356, "/").set();
                  } else {
                      which.src = "'.$scormpixdir.'/minus.gif";
                      eval(beg+list+mid+".display=\'block\';");
                      new cookie("hide:SCORMitem" + item, 1, -1, "/").set();
                  }
              }
          //]]>
          </script>'."\n";
    }
    global $option, $Itemid;
    $url = sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=player_scorm&amp;id=".$scorm->id.'&amp;currentorg='.$currentorg.$modestr.'&amp;scoid=');
    //$url = $CFG->wwwroot.'/mod/scorm/player.php?a='.$scorm->id.'&amp;currentorg='.$currentorg.$modestr.'&amp;scoid=';
    //$result->tocmenu = popup_form($url,$tocmenus, "tocmenu", $sco->id, '', '', '', true);
    // (DEN) popup_form() i put in lms_scorm.weblib.php
    $result->tocmenu = "..........popup_form.......";//popup_form($url,$tocmenus, "tocmenu", $sco->id, '', '', '', true);

    return $result;
}

?>
