<?php
/**
* includes/n_scorm/lms_scorm.play.php
* Joomla LMS Component
* deN_SCORM library 20.03.2007
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/
// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

function echoScormBoxHeaders()
{
    global $JLMS_CONFIG;
	JLMS_HTML::_('behavior.mootools');
	JHTML::_('behavior.modal');
	$doc = JFactory::getDocument();
	$doc->addScript(JURI::base().'components/com_joomla_lms/includes/js/jlms_106.js');
    $doc->addStyleSheet($JLMS_CONFIG->getCfg('live_site').'/components/com_joomla_lms/lms_css/sbox_scorm.css');					
	header('Content-Type: text/html;charset=UTF-8');
	echo "\n".$doc->getBuffer('head');	
}

function JLMS_SCORM_PLAY_MAIN( $scorm, $option, $course_id, $lpath_id, $scoid, $mode, $currentorg, $newattempt, $do_for_lp = false, $scorm_file = false, $box_view = false, $skip_resume = false ) {
	global $JLMS_DB, $my, $JLMS_SESSION, $Itemid, $JLMS_CONFIG;
	
	$doc_id = JRequest::getInt('doc_id');
	$player_task = 'player_scorm';
	if ($scorm_file) {
		$player_task = 'playerSCORMFiles';
		$skip_resume = false;
	}
	if (!$box_view) {
		$scorm->width = 100;
	}
	//echo '----'.$scoid.'---'.'<br />';
	if (!$scoid) {
		// don't change this query without carefully tests !!!!!!!!
		$query = "SELECT min(id) FROM #__lms_n_scorm_scoes WHERE scorm = $scorm->id AND parent = '/'";
		$JLMS_DB->SetQuery($query);
		$scoid = $JLMS_DB->LoadResult();
		//echo $JLMS_DB->geterrormsg();
		if (!$scoid) { $scoid = 0; }
	}
	//echo '----'.$scoid.'---';
	$query = "SELECT * FROM #__lms_n_scorm_scoes WHERE id = $scoid AND parent = '/'";
	$JLMS_DB->SetQuery($query);
	$sco1 = $JLMS_DB->LoadObject();
	if (is_object($sco1)) {
	//if ($sco1 = get_record("scorm_scoes", "id", $scoid,"parent",'/')) {
		// if current $scoid is ID of the 1st unlaunchable SCO then we need to find next launchable SCO
		$query = "SELECT id, launch FROM #__lms_n_scorm_scoes WHERE scorm = $scorm->id ORDER BY id";
		$JLMS_DB->SetQuery($query);
		$found_scorm_scoes = $JLMS_DB->LoadObjectList();
		$new_scoid = $scoid;
		$do_generate = false;
		$iii = 0;
		$nnn =  count($found_scorm_scoes);
		while ($iii < $nnn) {
			$found_scorm_sco = $found_scorm_scoes[$iii];
			if ($do_generate) {
				if ($found_scorm_sco->launch) {
					$new_scoid = $found_scorm_sco->id;break;
				}
			} elseif ($found_scorm_sco->id == $scoid) {
				$do_generate = true;
			}
			$iii ++;
		}
		if ($new_scoid == $scoid) { // new SCO wasn't be found, seems like next SCO id is smaler then current one
			$do_generate = false;
			$iii = count($found_scorm_scoes) - 1;
			while ($iii >= 0) {
				$found_scorm_sco = $found_scorm_scoes[$iii];
				if ($do_generate) {
					if ($found_scorm_sco->launch) {
						$new_scoid = $found_scorm_sco->id;break;
					}
				} elseif ($found_scorm_sco->id == $scoid) {
					$do_generate = true;
				}
				$iii --;
			}	
		}
		$scoid = $new_scoid;
		/*$scoid++;
		$query = "SELECT launch FROM #__lms_n_scorm_scoes WHERE id = $scoid";
		$JLMS_DB->SetQuery($query);
		$xm_l = $JLMS_DB->LoadResult();
		if (!$xm_l) {
			$scoid++;
		}*/
	}

	$scorm->version = strtolower(preg_replace('/[^a-zA-Z0-9_-]/i', '', $scorm->version));
	// (DEN) check paths !!!!!
	//echo 'sdfsdfsfsdf'.$scorm->version;
	if (!file_exists(dirname(__FILE__).'/datamodels/'.$scorm->version.'lib.php')) {
		$scorm->version = 'scorm_12';
	}
	require_once(dirname(__FILE__).'/datamodels/'.$scorm->version.'lib.php');
	$attempt = scorm_get_last_attempt($scorm->id, $my->id);
	$track_attempt = $attempt;

	// 18 July 2007 (DEN)
	$scorm->maxattempt = 0;

	if (($newattempt=='on') && (($attempt < $scorm->maxattempt) || ($scorm->maxattempt == 0))) {

		// if `tracking_type` == 'by the last attempt' ==> then we souldn't increment $attempt value
		global $JLMS_CONFIG;
		$course_params = $JLMS_CONFIG->get('course_params');
		$params = new JLMSParameters($course_params);
		if ($params->get('track_type',0) == 1) { //by the best attaempt
			$attempt++;
		} elseif (!$attempt) {
			$attempt = 1;
		}
		//$attempt++;
		$mode = 'normal';
	}
	$track_attempt = $attempt;

	$params = new JLMSParameters($scorm->params);

	$attemptstr = '&amp;attempt=' . $attempt;
	
	$result = scorm_get_toc($my,$scorm,'structurelist',$currentorg,$scoid,$mode,$attempt,true);
	$sco = $result->sco;

	if (($mode == 'browse') && ($scorm->hidebrowse == 1)) {
		$mode = 'normal';
	}
	if ($mode != 'browse') {
		//if ($trackdata = scorm_get_tracks($sco->id,$my->id,$attempt)) {
		if ($trackdata = scorm_get_tracks($sco->id,$my->id,$track_attempt)) {
			if (($trackdata->status == 'completed') || ($trackdata->status == 'passed') || ($trackdata->status == 'failed')) {
				$mode = 'normal';
				//$mode = 'review';
				// no 'review' mode !!!
			} else {
				$mode = 'normal';
			}
		} else {
			$mode = 'normal';
		}
	}

	//add_to_log($course->id, 'scorm', 'view', "player.php?id=$cm->id&scoid=$sco->id", "$scorm->id");

	$scoidstr = '&amp;scoid='.$sco->id;
	$scoidpop = '&amp;scoid='.$sco->id;
	$modestr = '&amp;mode='.$mode;
	if ($mode == 'browse') {
		$modepop = '&amp;mode='.$mode;
	} else {
		$modepop = '';
	}
	$orgstr = '';
	if ($currentorg) { $orgstr = '&currentorg='.$currentorg; }

	$JLMS_SESSION->set('scorm_scoid', $sco->id);
	$JLMS_SESSION->set('scorm_status', 'Not Initialized');
	$JLMS_SESSION->set('scorm_mode', $mode);
	$JLMS_SESSION->set('scorm_attempt', $attempt);
	/*$SESSION->scorm_scoid = $sco->id;
	$SESSION->scorm_status = 'Not Initialized';
	$SESSION->scorm_mode = $mode;
	$SESSION->scorm_attempt = $attempt;*/

	// very DIRTY hack :)
	$cm = $scorm;
	
	// !!!!!! (DEN) check paths to .js files AND api !!!!
	
	/*
	tam eshe ne vse provereno !!!
	ex.: function "p()" doesn't exists
	*/
	if ($do_for_lp) {
		$script_descr = '
function KeepAL_AReq(hr_k) {
	if (hr_k.readyState == 4) {
		if ((hr_k.status == 200)) {
			//hr_k.responseText;
			timer_KeepAL = setTimeout("KeepAL_MReq()", 120000);
		}
	}
}

function KeepAL_MReq() {
	var hr_k = false;
	if (window.ActiveXObject) {
		try { hr_k = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try { hr_k = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	} else if (window.XMLHttpRequest) {
		hr_k = new XMLHttpRequest();
		if (hr_k.overrideMimeType) {
			hr_k.overrideMimeType(\'text/xml\');
		}
	}
	if (!hr_k) { return false; }
	hr_k.onreadystatechange = function() { KeepAL_AReq(hr_k); };
	hr_k.open(\'GET\', "'.$JLMS_CONFIG->getCfg('live_site') . "/index.php?tmpl=component&option=$option&Itemid=$Itemid&task=keep_a_live".'", true);
	hr_k.send(null);
}

function dhtmlLoadScript(url)
{
   var e = document.createElement("script");
   e.src = url;
   e.type="text/javascript";
   document.getElementsByTagName("head")[0].appendChild(e);
}

clearTimeout(timer_KeepAL);
timer_KeepAL = setTimeout("KeepAL_MReq()", 120000);
dhtmlLoadScript("'.$JLMS_CONFIG->getCfg('live_site') . "/components/" . $option.'/includes/js/cookies.js");
dhtmlLoadScript("'.$JLMS_CONFIG->getCfg('live_site') . "/components/" . $option.'/includes/js/request.js");
dhtmlLoadScript("'.$JLMS_CONFIG->getCfg('live_site').'/index.php?tmpl=component&option='.$option.'&Itemid='.$Itemid.'&task=api_scorm&course_id='.$course_id.($skip_resume ? '&skip_resume=1' : '').'&id='.$cm->id.str_replace('&amp;', '&',$scoidstr.$modestr.$attemptstr).'");
';
		$scorm->width = $scorm->width ? $scorm->width : '100';
		$scorm->height = $scorm->height ? $scorm->height : '600';
		$scorm_width = $scorm->width<=100 ? ($scorm->width.'%') : $scorm->width;
		$scorm_width_style = $scorm->width<=100 ? ($scorm->width.'%') : $scorm->width.'px';
		$scorm_height = $scorm->height<=100 ? ($scorm->height.'%') : $scorm->height;
		$scorm_height_style = $scorm->height<=100 ? ($scorm->height.'%') : $scorm->height.'px';
		$scorm_height_contents = $scorm->height<=100 ? ($scorm->height.'%') : ($scorm->height.'px');
	$add_descr = '

	<iframe id="main" name="main" frameborder="0" width="'.$scorm_width.'" height="'.$scorm_height.'" style="height: '.$scorm_height_style.' !important; width: '.$scorm_width_style.' !important;" scrolling="'.$JLMS_CONFIG->get('scorm_scrolling', 'no').'" src="'.sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=loadsco_scorm&course_id=$course_id&id=".$cm->id.$scoidstr.$modestr).'">'._JLMS_IFRAMES_REQUIRES.'</iframe>

';

	$sco_script_descr = '';
    if (($sco->previd != 0) && ((!isset($sco->previous)) || ($sco->previous == 0))) {
        $sco_script_descr .= "\n".'var prev="'.sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=$player_task&course_id=$course_id&lpath_id=$lpath_id&id=$cm->id".$orgstr.$modepop.$scostr)."\";\n";
    } else {
        $sco_script_descr .= "\n".'var prev="'."javascript:ajax_action(\'prev_lpathstep\');"."\";\n";
    }
    if (($sco->nextid != 0) && ((!isset($sco->next)) || ($sco->next == 0))) {
        $sco_script_descr .= "\n".'var next="'.sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=$player_task&course_id=$course_id&lpath_id=$lpath_id&id=$cm->id".$orgstr.$modepop.$scostr)."\";\n";
    } else {
        $sco_script_descr .= "\n".'var next="'."javascript:ajax_action(\'next_lpathstep\');"."\";\n";
    }

	$ret = array();
	$ret['script'] = $script_descr.$sco_script_descr;
	$ret['response'] = $add_descr;
	return $ret;
	} else {

		//JLMS_TMPL::OpenTS();

	$using_mode_str = '';
	$toolbar = array();


	if ($sco->scormtype == 'sco') {
		$req_js_file = $box_view ? 'request_iframe.js' : 'request.js';
	?>		
	<script type="text/javascript" src="<?php echo $JLMS_CONFIG->getCfg('live_site') . "/components/" . $option; ?>/includes/js/cookies.js"></script>
	<script type="text/javascript" src="<?php echo $JLMS_CONFIG->getCfg('live_site') . "/components/" . $option; ?>/includes/js/<?php echo $req_js_file;?>"></script>
	<script type="text/javascript" src="<?php echo $JLMS_CONFIG->getCfg('live_site') . "/index.php?tmpl=component&option=$option&amp;Itemid=$Itemid&amp;task=api_scorm&amp;course_id=$course_id&amp;id=$cm->id".$scoidstr.$modestr.$attemptstr;?>"></script>
<?php
    }
    if (($sco->previd != 0) && ((!isset($sco->previous)) || ($sco->previous == 0))) {
        $scostr = '&scoid='.$sco->previd;
        //echo '    <script type="text/javascript">'."\n//<![CDATA[\n".'var prev="'.$CFG->wwwroot.'/mod/scorm/player.php?id='.$cm->id.$orgstr.$modepop.$scostr."\";\n//]]>\n</script>\n";
        echo '    <script type="text/javascript">'."\n//<![CDATA[\n".'var prev="'.sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=$player_task&course_id=$course_id&lpath_id=$lpath_id&doc_id=$doc_id&id=$cm->id".$orgstr.$modepop.$scostr)."\";\n//]]>\n</script>\n";
    } else {
        //echo '    <script type="text/javascript">'."\n//<![CDATA[\n".'var prev="'.$CFG->wwwroot.'/mod/scorm/view.php?id='.$cm->id."\";\n//]]>\n</script>\n";
        //echo '    <script type="text/javascript">'."\n//<![CDATA[\n".'var prev="'.sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=preview_scorm&course_id=$course_id&lpath_id=$lpath_id&id=$cm->id")."\";\n//]]>\n</script>\n";
		if ($box_view) {
			echo '    <script type="text/javascript">'."\n//<![CDATA[\n".'var prev="'."javascript:top.jlmsSqueezeBox_scorm.close();void(0);"."\";\n//]]>\n</script>\n";
		} else {
			echo '    <script type="text/javascript">'."\n//<![CDATA[\n".'var prev="'.str_replace("&amp;", "&", sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id"))."\";\n//]]>\n</script>\n";
		}
	}
    if (($sco->nextid != 0) && ((!isset($sco->next)) || ($sco->next == 0))) {
        $scostr = '&scoid='.$sco->nextid;
        //echo '    <script type="text/javascript">'."\n//<![CDATA[\n".'var next="'.$CFG->wwwroot.'/mod/scorm/player.php?id='.$cm->id.$orgstr.$modepop.$scostr."\";\n//]]>\n</script>\n";
        echo '    <script type="text/javascript">'."\n//<![CDATA[\n".'var next="'.sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=$player_task&course_id=$course_id&lpath_id=$lpath_id&doc_id=$doc_id&id=$cm->id".$orgstr.$modepop.$scostr)."\";\n//]]>\n</script>\n";
    } else {
        //echo '    <script type="text/javascript">'."\n//<![CDATA[\n".'var next="'.$CFG->wwwroot.'/mod/scorm/view.php?id='.$cm->id."\";\n//]]>\n</script>\n";
        //echo '    <script type="text/javascript">'."\n//<![CDATA[\n".'var next="'.sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=preview_scorm&course_id=$course_id&lpath_id=$lpath_id&id=$cm->id")."\";\n//]]>\n</script>\n";
		if ($box_view) {
			echo '    <script type="text/javascript">'."\n//<![CDATA[\n".'var next="'."javascript:top.jlmsSqueezeBox_scorm.close();void(0);"."\";\n//]]>\n</script>\n";
		} else {
        	echo '    <script type="text/javascript">'."\n//<![CDATA[\n".'var next="'.str_replace("&amp;", "&", sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id"))."\";\n//]]>\n</script>\n";
 		}
    }
?>

    <!--div id="scormpage"-->
<?php 

	/* (DEN) kakoi-to header 4utb nige ( bylo: <div id="tochead" class="header"><?php print_string('coursestruct','scorm') ?></div>
	
	Also see code - 35 lines below (bylo: "get_string('browsemode','scorm')" and "get_string('reviewmode','scorm')" )
	*/
/*
    if ($scorm->hidetoc == 0) {
?>
        <div id="tocbox" class="generalbox">
            <div id="tochead" class="header"><?php echo 'coursestruct'; ?></div>
            <div id="toctree">
            <?php echo $result->toc; ?>
            </div>
        </div>
<?php
        $class = ' class="toc"';
    } else {
        $class = ' class="no-toc"';
    }
?>
        <div id="scormbox"<?php echo $class ?>>
<?php */
    // This very big test check if is necessary the "scormtop" div
	$toolbar = array();
	if (
			($mode != 'normal') ||											// We are not in normal mode so review or browse text will displayed
			(
				($scorm->hidenav == 0) &&									// Teacher want to display navigation links
				(
					(
						($sco->previd != 0) &&								// This is not the first learning object of the package
						((!isset($sco->previous)) || ($sco->previous == 0))	// M. must manage the previous link
					) ||
					(
						($sco->nextid != 0) &&								// This is not the last learning object of the package
						((!isset($sco->next)) || ($sco->next == 0))			// M. must manage the next link
					)
				)
			) || ($scorm->hidetoc == 2)										// Teacher want to display toc in a small dropdown menu 
		) { /*
?>
            <div id="scormtop">
        <?php echo $mode == 'browse' ? '<div id="scormmode" class="left">'.'browsemode'."</div>\n" : ''; ?>
        <?php echo $mode == 'review' ? '<div id="scormmode" class="left">'.'reviewmode'."</div>\n" : ''; ?>
<?php */
       	if ($mode == 'browse') {
       		$using_mode_str = 'browsemode';
       	} elseif ($mode == 'review') {
       		$using_mode_str = 'reviewmode';
       	}
       	
        if (($scorm->hidenav == 0) || ($scorm->hidetoc == 2)) { /*
?>
                <div id="scormnav" class="right">
        <?php */
        	$orgstr = '';
        	if ($currentorg) { $orgstr = '&amp;currentorg='.$currentorg; }
            if (($scorm->hidenav == 0) && ($sco->previd != 0) && ((!isset($sco->previous)) || ($sco->previous == 0))) {
                /// Print the prev LO link
                $scostr = '&amp;scoid='.$sco->previd;
                //$url = $CFG->wwwroot.'/mod/scorm/player.php?id='.$cm->id.$orgstr.$modestr.$scostr;
                $url = sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=$player_task&course_id=$course_id&lpath_id=$lpath_id&doc_id=$doc_id&id=$cm->id".$orgstr.$modestr.$scostr);
                // (DEN) nige bylo: get_string('prev','scorm')
                //echo '<a href="'.$url.'">&lt; '. 'prev' .'</a>';
                //if($params->get('scorm_nav_bar', 0)==2) {//scorm-left bar customisation
                //}
                $link_prev = $url;//scorm-left bar customisation
                /*if ($params->get('scorm_nav_bar', 0)==2) {
                	$toolbar[] = array('btn_type' => 'prev', 'btn_js' => "$url", 'btn_target' => 'main');
                } else {
                	
                }*/
                $toolbar[] = array('btn_type' => 'prev', 'btn_js' => "$url");
                
            }
            if ($scorm->hidetoc == 2) {
                //echo $result->tocmenu;
                // (DEN) it's a button ?
            }
            
            
            if (($scorm->hidenav == 0) && ($sco->nextid != 0) && ((!isset($sco->next)) || ($sco->next == 0))) {
                /// Print the next LO link
                $scostr = '&amp;scoid='.$sco->nextid;
                //$url = $CFG->wwwroot.'/mod/scorm/player.php?id='.$cm->id.$orgstr.$modestr.$scostr;
                $url = sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=$player_task&course_id=$course_id&lpath_id=$lpath_id&doc_id=$doc_id&id=$cm->id".$orgstr.$modestr.$scostr);
                
                 // (DEN) nige bylo: get_string('next','scorm')
                //echo '            &nbsp;<a href="'.$url.'">'. 'next' .' &gt;</a>';
                //if($params->get('scorm_nav_bar', 0)==2) {//scorm-left bar customisation
                //}
                $link_next = $url;//scorm-left bar customisation
                
                /*if ($params->get('scorm_nav_bar', 0)==2) {
                	$toolbar[] = array('btn_type' => 'next', 'btn_js' => "$url", 'btn_target' => 'main');
                } else {
					$toolbar[] = array('btn_type' => 'next', 'btn_js' => "$url");
				}*/
				$toolbar[] = array('btn_type' => 'next', 'btn_js' => "$url");
            } /*
        ?>

                </div>
<?php*/
        } /*
?>
            </div>
<?php*/
    } // The end of the very big test
	/*echo $using_mode_str;
	echo $mode;
    echo "<pre>";print_r($scorm);print_r($sco);print_r($toolbar);die;*/
	
	if($scorm_file) {
		$hparams = array('show_menu' => true,
		'simple_menu' => true,);
		//$toolbar = array();
		$title = '';
		if (isset($doc_details->folder_flag) && $doc_details->folder_flag == 1) {
			$title = $doc_details->id ? _JLMS_OUTDOCS_TITLE_EDIT_FOLDER : _JLMS_OUTDOCS_TITLE_NEW_FOLDER;
		} else {
			$title = (isset($doc_details->id)&& $doc_details->id)? _JLMS_OUTDOCS_TITLE_EDIT_DOC : _JLMS_OUTDOCS_TITLE_NEW_DOC;
		}
		
		
		/* ??? */
		$is_contents = false;		
		
		if ($params->get('scorm_nav_bar', 0) && $params->get('scorm_nav_bar', 0)!=2 ) {			
			$is_contents = true;			
			$toolbar[] = array('btn_type' => 'contents', 'btn_js' => "javascript:ajax_action('contents_scorm');");
		} else {
			//$toolbar = array();
		}
		
		JLMS_TMPL::OpenMT();
		
		$hparams = array();
		
		$hparams['navpanelonly'] = $box_view;
		
		if( !$course_id ) {
			$hparams['showCourseFilter'] = false;
		}
		
		JLMS_TMPL::ShowHeader('scorm', $scorm->scorm_name, $hparams, $toolbar);		
		/* end ??? */
		
	} else {
	
		$is_contents = false;		
		
		if ($params->get('scorm_nav_bar', 0) && $params->get('scorm_nav_bar', 0)!=2 ) {
			$is_contents = true;
			$toolbar[] = array('btn_type' => 'contents', 'btn_js' => "javascript:ajax_action('contents_scorm');");
		} else {
			//$toolbar = array();
		}
		
		JLMS_TMPL::OpenMT();
	
		$hparams = array();
		
		$hparams['navpanelonly'] = $box_view;
		
		JLMS_TMPL::ShowHeader('scorm', $scorm->scorm_name, $hparams, $toolbar);		
	}
?>
<?php /* To avoid session loose we should sent 'Keep_A_Live' headers */ ?>
<?php
$output_additional_tags = '';
?>

<?php 
if($box_view) { 
	$output_additional_tags .= '
	<script type="text/javascript">
	//<![CDATA[
	function goRunKeepAL_API(url) {
		if (parent == window) return;
		else parent.KeepAL_MReq_run(url);
	}
	goRunKeepAL_API("'.$JLMS_CONFIG->getCfg('live_site') . "/index.php?tmpl=component&option=$option&Itemid=$Itemid&task=keep_a_live".'");
	//]]>
	</script>
	';
} else {
	$output_additional_tags .= '
	<script type="text/javascript">
	//<![CDATA[
	function jlms_isdefined_jsvar(obj)
	{
		return (obj != undefined);
	}
	if (jlms_isdefined_jsvar(timer_KeepAL)) {
		//do nothing here...
	} else {
		function KeepAL_AReq(hr_k) {
			if (hr_k.readyState == 4) {
				if ((hr_k.status == 200)) {
					//hr_k.responseText;
					timer_KeepAL = setTimeout("KeepAL_MReq()", 20000);
				}
			}
		}
		function KeepAL_MReq() {
			var hr_k = false;
			if (window.ActiveXObject) {
				try { hr_k = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					try { hr_k = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {}
				}
			} else if (window.XMLHttpRequest) {
				hr_k = new XMLHttpRequest();
				if (hr_k.overrideMimeType) {
					hr_k.overrideMimeType(\'text/xml\');
				}
			}
			if (!hr_k) { return false; }
			hr_k.onreadystatechange = function() { KeepAL_AReq(hr_k); };
			hr_k.open(\'GET\', "'.$JLMS_CONFIG->getCfg('live_site') . "/index.php?tmpl=component&option=$option&Itemid=$Itemid&task=keep_a_live".'", true);
			hr_k.send(null);
		}
		var timer_KeepAL = setTimeout("KeepAL_MReq()", 20000);// 5 min
	}
	//]]>
	</script>
	';
}
/*    if (!empty($toolbar)) {
    	if ($is_contents) {
    			JLMS_TMPL::OpenTS(' id="jlms_scorm_toc_container" style="visibility:hidden; display:none;"');*/
    			
    $string_toolbar = '';
    
    $js_refreshContentsStatusFunction = '';
    
    if( $params->get('scorm_nav_bar', 0) ) 
	{				
		$js_refreshContentsStatusFunction = '
				<script type="text/javascript">
				function refreshContentsStatus(){
													var url = "'.sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=ajaxgetcistatus&amp;scorm_id=$cm->id").'";
													
													function updateContPanelImages( response, j15 ) 
													{
														var statuses_img = {
																\'passed\' : \'btn_accept\',
																\'completed\' : \'btn_accept\',
																\'failed\' : \'btn_cancel\',
																\'incomplete\' : \'btn_cancel\',
																\'browsed\' : \'btn_pending_cur\',
																\'suspend\' : \'btn_pending_cur\',
																\'notattempted\' : \'btn_cancel\',
																\'assetc\' : \'../spacer\',
																\'asset\' : \'../spacer\' };
														
														if( j15 ) 
														{
															var json = Json.evaluate(response) || {};
														} else {
															var json = JSON.decode(response) || {};
														}
														
														for (var key in json) 
														{									
															if( statuses_img[json[key]] != undefined && $(\'conts_img_\'+key) ) {										
																$(\'conts_img_\'+key).setProperty(\'src\', \''.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/\'+statuses_img[json[key]]+\'.png\');		
																$(\'conts_img_\'+key).setProperty(\'alt\', json[key]);		
															}
														};
													} 
			';							
				
			if( JLMS_J16version() ) { 
				$js_refreshContentsStatusFunction .= "new Request({
								url: url, 
								method: 'get',																							
								onSuccess: function( response ){
									updateContPanelImages( response, false );	
								},
								evalScripts: true
							}).send();
							";
			} else {
				$js_refreshContentsStatusFunction .=	"new Ajax(url, {
							method: 'get',																								
							onSuccess: function( response ){								
								updateContPanelImages( response, true );					 
							},
							evalScripts: true
						}).request();";
			}
				
			$js_refreshContentsStatusFunction .= '}
			</script>
			';
	}
            			
    if (!empty($toolbar) || $params->get('scorm_nav_bar', 0)==2) {//scorm-left bar customisation
    	if ($is_contents || $params->get('scorm_nav_bar', 0)==2) {
    		$scorm->width = $scorm->width ? $scorm->width : '100';
			$scorm->height = $scorm->height ? $scorm->height : '600';
			$scorm_height_contents = $scorm->height<=100 ? ($scorm->height.'%') : ($scorm->height.'px');

    		if ($params->get('scorm_nav_bar', 0)==2) {
    			$string_toolbar .= '<tr id="jlms_scorm_toc_container">
								<td><div style="height:'.$scorm_height_contents.'; overflow-y:auto;overflow-x:hidden;">';//.JLMS_ShowToolbar($toolbar, true);
			} else {
    			$string_toolbar .= '<tr id="jlms_scorm_toc_container" style="visibility:hidden; display:none;">
								<td>';
			}		
			
			$output_additional_tags .= '
				<script type="text/javascript">
					//<![CDATA[
						var jlms_contents_visible = 0;
						function ajax_action(pressbutton) {
							switch (pressbutton) {
								case \'contents_scorm\':
									jlms_blocked = 0;
									jlms_SwitchContents();
								break;
							}
						}
						function jlms_SwitchContents() {
							var vis_style1 = \'visible\';
							var disp_style1 = \'\';
							if (jlms_contents_visible == 1) {
								var vis_style1 = \'hidden\';
								var disp_style1 = \'none\';
							}
							var jlcc = getObj(\'jlms_scorm_toc_container\');
							jlcc.style.visibility = vis_style1;
							jlcc.style.display = disp_style1;
							if (jlms_contents_visible == 1) { jlms_contents_visible = 0;}
							else { jlms_contents_visible = 1; }
						}
					//]]>										
				</script>
			';
		
			//JLMS_TMPL::ShowToolbar($toolbar);
	    	//echo "</td></tr><tr><td height='8px' style='height:8px'></td></tr>";
			
	    	//echo "<tr id='jlms_scorm_toc_container' style=\"visibility:hidden; display:none;\"><td>";

		$cal_style = '
				<link rel="stylesheet" type="text/css" href="'.$JLMS_CONFIG->getCfg('live_site').'/components/com_joomla_lms/lms_css/scorm.css" />
		';
		$output_additional_tags .= $cal_style;
	
	
		if($params->get('scorm_nav_bar', 0) == 2) {
			$output_additional_tags .= '
			<style type="text/css">
				body {
					line-height: 125%;
					font-family: Tahoma, Helvetica, sans-serif;
					color: #666;
					font-size: 11px;
				}
				.sectiontableheader {
					font-family: Tahoma, Helvetica, sans-serif;
					font-weight: bold;
				}
				th.sectiontableheader,
				td.sectiontableheader {
				  	font-weight: bold;
				  	padding:2px 4px 2px;
				  	line-height: 20px;
					background: #eee;
					border-top: 1px solid #efefef;
					border-bottom: 1px solid #ddd;
				}
				
				tr.sectiontableentry1 td,
				tr.sectiontableentry2 td{
					padding: 2px 4px 2px;
					line-height: 20px;
				 	border-bottom:1px dotted #cccccc;
				 	font-size:10px;
				}
				tr.sectiontableentry2{
					background-color:#f8f8f8;
				}
				
				td.sectiontableentry1, 
				td.sectiontableentry2 {
					padding: 4px; 
				}
			</style>
			';
		}
		
//		$doc = JFactory::getDocument();
//		$output_additional_tags = str_replace('<style type="text/css">', '', $output_additional_tags);
//		$output_additional_tags = str_replace('</style>', '', $output_additional_tags);
//		$output_additional_tags = str_replace('<script type="text/javascript">', '', $output_additional_tags);
//		$output_additional_tags = str_replace('</script', '', $output_additional_tags);
//		$output_additional_tags = str_replace('//<![CDATA[', '', $output_additional_tags);
//		$output_additional_tags = str_replace('//]]>', '', $output_additional_tags);


		//process SCORM toc
		$joomla_toc = $result->joomla_toc;


		$do_close_table = false;
		if (isset($joomla_toc->organizationtitle) && $joomla_toc->organizationtitle ) {
			$string_toolbar .= "<table width='100%' cellpadding='0' cellspacing='0' border=\"0\"><tr><td colspan='3' class='contentheading'>".$joomla_toc->organizationtitle."</td></tr>";
			$do_close_table = true;
		} elseif (!empty($joomla_toc->items)) {
			
			if ($params->get('scorm_nav_bar', 0)==2)
				$class = "class='scorm_contents'";
			else 
				$class = '';	


			$string_toolbar .= "<table width='100%' ".$class." cellpadding='0' cellspacing='0' border=\"0\">";

			$string_toolbar .= "<tr><td width='16'></td><td width='16'></td><td width='100%'>";

//			if ($params->get('scorm_nav_bar', 0)==2) {
//			
//				if(isset($link_prev)) {
//					$string_toolbar .= "<a href='".$link_prev."'>";
//					$string_toolbar .= "<div class='left_active'></div>";
//				}	
//				else 
//					$string_toolbar .= "<div class='left_passive'></div>";
//
//				if(isset($link_prev)) {
//					$string_toolbar .= "</a>";
//				}	
//				
//				if(isset($link_next)) {
//					$string_toolbar .= "<a href='".$link_next."'>";
//					$string_toolbar .= "<div class='right_active'></div>";
//				}
//				else 	
//					$string_toolbar .= "<div class='right_passive'></div>";
//
//				if(isset($link_next)) {
//					$string_toolbar .= "</a>";
//				}
//	
//			}
	
			$string_toolbar .= "</td></tr>";

			$do_close_table = true;
			$i_c = 0;$k = 1;
			$statuses_img = array(
				'passed' => 'btn_accept',
				'completed' => 'btn_accept',
				'failed' => 'btn_cancel',
				'incomplete' => 'btn_cancel',
				'browsed' => 'btn_pending_cur',
				'suspend' => 'btn_pending_cur',
				'notattempted' => 'btn_cancel',
				'assetc' => '../spacer',
				'asset' => '../spacer' );
			for ($i_c=0, $n_c=count($joomla_toc->items); $i_c < $n_c; $i_c++) {
			//foreach ($joomla_toc->items as $toc_item) {
				$toc_item = $joomla_toc->items[$i_c];
				$string_toolbar .= "<tr class='sectiontableentry".$k."'><td>";
				$status_img = '&nbsp;';
				if ($toc_item['img_src']) {
					if (isset($statuses_img[$toc_item['img_src']])) {
						$status_img = "<img id=\"conts_img_".$toc_item['scoid']."\" class='JLMS_png' src='".$JLMS_CONFIG->get('live_site')."/components/com_joomla_lms/lms_images/toolbar/".$statuses_img[$toc_item['img_src']].".png' width='16' height='16' alt='".$toc_item['img_src']."' border='0' />";
					}
				}

				if (!$toc_item['level']) {
					$tree_img = "";
					$status_img = '&nbsp;';
				} else {
					if (isset($joomla_toc->items[$i_c+1])) {
						$r = $joomla_toc->items[$i_c+1];
						if ($r['level']) {
							$tree_img = "<img class='JLMS_png' src='".$JLMS_CONFIG->get('live_site')."/components/com_joomla_lms/lms_images/treeview/sub1.png' width='16' height='16' alt='sub' border='0' />";
						} else {
							$tree_img = "<img class='JLMS_png' src='".$JLMS_CONFIG->get('live_site')."/components/com_joomla_lms/lms_images/treeview/sub2.png' width='16' height='16' alt='sub' border='0' />";
						}
					} else {
						$tree_img = "<img class='JLMS_png' src='".$JLMS_CONFIG->get('live_site')."/components/com_joomla_lms/lms_images/treeview/sub2.png' width='16' height='16' alt='sub' border='0' />";
					}
				}
				$string_toolbar .= $status_img."</td>";
				if ($tree_img) {
					$string_toolbar .= "<td  valign='middle'>".$tree_img."</td>";
					$string_toolbar .= "<td>";
				} else {
					$string_toolbar .= "<td colspan='2'>";
				}
				$name = ($toc_item['bold']?'<strong>':'').$toc_item['name'].($toc_item['bold']?'</strong>':'');
				if ($toc_item['href']) {
					$href = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=$player_task&amp;course_id=$course_id&amp;id=$scorm->id&amp;lpath_id=$lpath_id&amp;doc_id=$doc_id";
					if ($toc_item['scoid']) {
						$href .= "&amp;scoid=".$toc_item['scoid'];
					}
					if ($toc_item['currentorg']) {
						$href .= "&amp;currentorg=".$toc_item['currentorg'];
					}
					$href = sefRelToAbs($href);
					/*if($params->get('scorm_nav_bar', 0) == 2 && $box_view) {
						$string_toolbar .= "<a href='$href' target='main'>".$name."</a>";
					} else {
						$string_toolbar .= "<a href='$href'>".$name."</a>";
					}*/
					$string_toolbar .= "<a href='$href'>".$name."</a>";
				} else {
					$string_toolbar .= $name;//."(".$toc_item['level'].")";
				}
				$string_toolbar .= "</td></tr>";
				$k = 3 - $k;
			}
		} else {
			$do_close_table = false;
		}
		if ($do_close_table) {
			$string_toolbar .= "<tr><td height='8' colspan='3' style='height:8px'></td></tr>";
			$string_toolbar .= "</table>";
		}
		if ($params->get('scorm_nav_bar', 0)==2) {
			$string_toolbar .= '</div>';
		}
	   $string_toolbar .= '</td>
		</tr>';
	}
    } // end of (!empty($toolbar) && $is_contents)
	
	//echo "</td></tr>";
	
	$string_toolbar1 = '';
	if($params->get('scorm_nav_bar', 0) == 1) {
		echo $string_toolbar;
	} elseif($params->get('scorm_nav_bar', 0) == 2) {
		/*if ($box_view) {
			$string_toolbar1 = str_replace($player_task, 'loadsco_scorm', $string_toolbar);
		} else {
			$string_toolbar1 = $string_toolbar;
		}*/
		$string_toolbar1 = $string_toolbar;
	} else { 
		$string_toolbar1 = '';
	}	

	$width = '';	
	if($params->get('scorm_nav_bar', 0) == 2)
		$width = 'width="200"';	
//	if($params->get('scorm_nav_bar', 0) == 1)
//		$width = '';			
		
	if($string_toolbar1) {
		$right_border = 'style="border-right:1px solid #666666; vertical-align: top;"';
	}
	else {
		$right_border = '';	
	}	

	echo '<tr>
			<td width="100%">'
			.$output_additional_tags
			.'<table border="0" width="100%" cellpadding="0" cellspacing="0" class="jlms_table_no_borders">
					<tr>
						<td valign="top" '.$right_border.' '.$width.'>'.($string_toolbar1 ? 
							('<table border="0" '.$width.' cellpadding="0" cellspacing="0">
								'.$string_toolbar1.'
							</table>'):'').'
						</td>
							<td style="vertical-align: top;" valign="top">
								<table class="jlms_table_no_borders" border="0" width="100%" cellpadding="0" cellspacing="0">
								
	';	
	
	JLMS_TMPL::OpenTS(" id='jlms_scorm_data_container'");
    
?>
            <!--div id="scormobject" class="right"> td opened above-->
                <noscript>
                    <div id="noscript">
                    <?php //(DEN) here was : "print_string('noscriptnoscorm','scorm')"
						?>
                        <?php echo 'noscriptnoscorm'; // No Martin(i), No Party ;-) ?>

                    </div>
                </noscript>
<?php
    if ($result->prerequisites) {
    	// (DEN) never open popup !!
    	$scorm->popup = 0;
        if ($scorm->popup == 0) {
        	$scorm->width = $scorm->width ? $scorm->width : '100';
        	$scorm->height = JRequest::getInt('height') ? JRequest::getInt('height') : ($scorm->height ? $scorm->height : '600');
        	$scorm_width = $scorm->width<=100 ? $scorm->width.'%' : $scorm->width;
        	$scorm_width_style = $scorm->width<=100 ? $scorm->width.'%' : $scorm->width.'px';
        	$scorm_height = $scorm->height<=100 ? $scorm->height.'%' : $scorm->height;
        	$scorm_height_style = $scorm->height<=100 ? ($scorm->height.'%') : $scorm->height.'px';
        	$scorm_width1 = $scorm_width;
        	$scorm_width_style1 = $scorm_width;
        	if ($scorm_width > 200) {
				if($params->get('scorm_nav_bar', 0) == 2) {
					$scorm_width1 = $scorm_width - 200;
					$scorm_width_style1 = $scorm_width - 200;
					$scorm_width_style1 = $scorm_width_style1 . 'px';
				}
			}
        /* (DEN) src of iframe was: loadSCO.php?id=<?php echo $cm->id.$scoidstr.$modestr ?> */
?>
			
				<?php echo $js_refreshContentsStatusFunction; ?>				
                <iframe <?php echo $params->get('scorm_nav_bar', 0)?"onload=\"refreshContentsStatus();\"":""; ?> id="main" name="main" frameborder="0" width="<?php echo $scorm_width1; ?>" height="<?php echo $scorm_height ?>" style="width: <?php echo $scorm_width_style1;?> !important; height: <?php echo $scorm_height_style;?> !important;" scrolling="<?php echo $JLMS_CONFIG->get('scorm_scrolling', 'no');?>" src="<?php echo sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=loadsco_scorm&course_id=$course_id&id=".$cm->id.$scoidstr.$modestr);?>"><?php echo _JLMS_IFRAMES_REQUIRES; ?></iframe>
<?php
        } else { // (DEN) we never open popup
        	/*
?>
                    <script type="text/javascript">
                    //<![CDATA[
                        function openpopup(url,name,options,width,height) {
                            fullurl = url;//"<?php //echo $CFG->wwwroot.'/mod/scorm/' ?>" + url;
                            windowobj = window.open(fullurl,name,options);
                            if ((width==100) && (height==100)) {
                                // Fullscreen
                                windowobj.moveTo(0,0);
                            } 
                            if (width<=100) {
                                width = Math.round(screen.availWidth * width / 100);
                            }
                            if (height<=100) {
                                height = Math.round(screen.availHeight * height / 100);
                            }
                            windowobj.resizeTo(width,height);
                            windowobj.focus();
                            return windowobj;
                        }

                        url = <?php echo sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=loadsco_scorm&course_id=$course_id&id=".$cm->id.$scoidpop);?>
                        <?php //(DEN)
                         //url = "loadSCO.php?id=<?php echo $cm->id.$scoidpop ?>"; 
                         ?>
                        width = <?php p($scorm->width) ?>;
                        height = <?php p($scorm->height) ?>;
                        var main = openpopup(url, "scormpopup", "<?php p($scorm->options) ?>", width, height);
                    //]]>
                    </script>
                    <noscript>
                    <iframe id="main" scrolling="no" frameborder="0"
                            width="<?php echo $scorm->width<=100 ? $scorm->width.'%' : $scorm->width ?>" 
                            height="<?php echo $scorm->height<=100 ? $scorm->height.'%' : $scorm->height ?>" 
                            src="<?php echo sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=loadsco_scorm&course_id=$course_id&id=".$cm->id.$scoidstr.$modestr);?>">
                    </iframe>
                    </noscript>
<?php   */         
        }
    } else {
    	// (DEN)
       //print_simple_box(get_string('noprerequisites','scorm'),'center');
       echo '<center>noprerequisites</center>';
    } /*
?>
            </div> <!-- SCORM object -->
        </div> <!-- SCORM box  -->
    </div> <!-- SCORM content -->
    </div> <!-- Content -->
    </div> <!-- Page -->
<?php */
	//echo "</td></tr>";
	JLMS_TMPL::CloseTS();
	echo '								</table>
							</td>
					</tr>
				</table>	
			</td>
		</tr>';
	
	JLMS_TMPL::CloseMT();
	}
}
?>