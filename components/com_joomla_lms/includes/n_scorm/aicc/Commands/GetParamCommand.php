<?php
/**
 * User: nester_a
 * Date: 29.01.13
 */

include_once "BaseCommand.php";

class GetParamCommand extends BaseCommand{

    public function __construct($params=array()){
        parent::_init($params);
    }

    public function run(){
        if ($this->status == 'Not Initialized') {
            $this->JLMS_SESSION->set('scorm_status', 'Running');
            $this->status = 'Running';
        }
        if ($this->status != 'Running') {
            echo "error=101\r\nerror_text=Terminated\r\n";
        } else {
            $userdata = new stdClass();
            if ($usertrack=scorm_get_tracks($this->scoid,$this->user->id,$this->attempt)) {
                $userdata = $usertrack;
            } else {
                $userdata->status = '';
                $userdata->score_raw = '';
            }
            $userdata->student_id = addslashes($this->user->username);;
            $userdata->student_name = addslashes($this->user->name);
            $userdata->mode = $this->mode;
            if ($userdata->mode == 'normal') {
                $userdata->credit = 'credit';
            } else {
                $userdata->credit = 'no-credit';
            }

            if ($sco = scorm_get_sco($this->scoid)) {
                $userdata->course_id = $sco->identifier;
                $userdata->datafromlms = isset($sco->datafromlms) ? $sco->datafromlms : '';
                $userdata->mastery_score = isset($sco->mastery_score) && is_numeric($sco->mastery_score) ? trim($sco->mastery_score) : '';
                $userdata->max_time_allowed = isset($sco->max_time_allowed) ? $sco->max_time_allowed : '';
                $userdata->time_limit_action = isset($sco->time_limit_action) ? $sco->time_limit_action : '';

                echo "error=0\r\nerror_text=Successful\r\naicc_data=";
                echo "[Core]\r\n";
                echo 'Student_ID='.$userdata->student_id."\r\n";
                echo 'Student_Name='.$userdata->student_name."\r\n";

                if (isset($userdata->{'cmi.core.lesson_location'})) {
                    echo 'Lesson_Location='.$userdata->{'cmi.core.lesson_location'}."\r\n";
                } else {
                    echo 'Lesson_Location='."\r\n";
                }

                echo 'Credit='.$userdata->credit."\r\n";

                if (isset($userdata->status)) {
                    if ($userdata->status == '') {
                        $userdata->entry = ', ab-initio';
                    } else {
                        if (isset($userdata->{'cmi.core.exit'}) && ($userdata->{'cmi.core.exit'} == 'suspend')) {
                            $userdata->entry = ', resume';
                        } else {
                            $userdata->entry = '';
                        }
                    }
                }

                if (isset($userdata->{'cmi.core.lesson_status'})) {
                    echo 'Lesson_Status='.$userdata->{'cmi.core.lesson_status'}.$userdata->entry."\r\n";
                    $this->JLMS_SESSION->set('scorm_lessonstatus', $userdata->{'cmi.core.lesson_status'});
                } else {
                    echo 'Lesson_Status=not attempted'.$userdata->entry."\r\n";
                    $this->JLMS_SESSION->set('scorm_lessonstatus', 'not attempted');
                }

                if (isset($userdata->{'cmi.core.score.raw'})) {
                    $max = '';
                    $min = '';
                    if (isset($userdata->{'cmi.core.score.max'}) && !empty($userdata->{'cmi.core.score.max'})) {
                        $max = ', '.$userdata->{'cmi.core.score.max'};
                        if (isset($userdata->{'cmi.core.score.min'}) && !empty($userdata->{'cmi.core.score.min'})) {
                            $min = ', '.$userdata->{'cmi.core.score.min'};
                        }
                    }
                    echo 'Score='.$userdata->{'cmi.core.score.raw'}.$max.$min."\r\n";
                } else {
                    echo 'Score='."\r\n";
                }
                if (isset($userdata->{'cmi.core.total_time'})) {
                    echo 'Time='.$userdata->{'cmi.core.total_time'}."\r\n";
                } else {
                    echo 'Time='.'00:00:00'."\r\n";
                }
                echo 'Lesson_Mode='.$userdata->mode."\r\n";
                if (isset($userdata->{'cmi.suspend_data'})) {
                    echo "[Core_Lesson]\r\n".rawurldecode($userdata->{'cmi.suspend_data'})."\r\n";
                } else {
                    echo "[Core_Lesson]\r\n";
                }
                echo "[Core_Vendor]\r\n".$userdata->datafromlms."\r\n";
                echo "[Evaluation]\r\nCourse_ID = {".$userdata->course_id."}\r\n";
                echo "[Student_Data]\r\n";
                echo 'Mastery_Score='.$userdata->mastery_score."\r\n";
                echo 'Max_Time_Allowed='.$userdata->max_time_allowed."\r\n";
                echo 'Time_Limit_Action='.$userdata->time_limit_action."\r\n";
            } else {
                error('Sco not found');
            }
        }
    }
}