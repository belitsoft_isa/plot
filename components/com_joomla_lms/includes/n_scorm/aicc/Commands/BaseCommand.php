<?php
/**
 * User: nester_a
 * Date: 29.01.13
 */

require_once(dirname(__FILE__) . "/../functions.php");
require_once(dirname(__FILE__) . "/../../lms_scorm.lib.php");

abstract class BaseCommand {

    private $_internalData = array();

    public function run(){
        switch($this->status){
            case 'Running':
                echo "error=0\r\nerror_text=Successful\r\n";
                break;
            case 'terminated':
                echo "error=1\r\nerror_text=Terminated\r\n";
                break;
            default:
                echo "error=1\r\nerror_text=Not Initialized\r\n";
                break;
        }
    }

    public function __get($name){
        return isset($this->_internalData[$name]) ? $this->_internalData[$name] : null;
    }

    public function __set($name, $value){
        $this->_internalData[$name] = $value;
    }

    protected function _init($params){
        foreach($params as $key=>$value){
            $this->$key = $value;
        }
    }


}