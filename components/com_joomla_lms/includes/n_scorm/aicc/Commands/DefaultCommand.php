<?php
/**
 * User: nester_a
 * Date: 29.01.13
 */

include_once "BaseCommand.php";

class DefaultCommand extends BaseCommand{

    public function __construct($params=array()){
        parent::_init($params);
    }

    public function run(){
        echo "error=1\r\nerror_text=Invalid Command\r\n";
    }
}