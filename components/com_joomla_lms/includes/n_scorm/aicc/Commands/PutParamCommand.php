<?php
/**
 * User: nester_a
 * Date: 29.01.13
 */

include_once "BaseCommand.php";
class PutParamCommand extends BaseCommand{

    protected $_methods = array(
       'cmi.core.lesson_location' => 'onLessonLocation',
       'cmi.core.lesson_status' => 'onLessonStatus',
       'cmi.core.score.raw' => 'onScoreRaw',
       'cmi.core.session_time' => 'onSessionTime',
       'cmi.suspend_data' => 'onSuspendData',
       'cmi.comments' => 'onComments',
    );

    public function __construct($params=array()){
        parent::_init($params);
    }

    public function run(){
        if($this->enabled){
            if ($this->status == 'Running') {
                $aiccData = $this->aiccData;
                if (!is_null($aiccData)) {
                    $initlessonStatus = 'not attempted';
                    $this->lessonStatus = 'not attempted';
                    if (isset($this->JLMS_SESSION->scorm_lessonStatus)) {
                        $initlessonStatus = $this->JLMS_SESSION->scorm_lessonStatus;
                    }
                    $score = '';

                    $datamodel['lesson_location'] = 'cmi.core.lesson_location';
                    $datamodel['lesson_status'] = 'cmi.core.lesson_status';
                    $datamodel['score'] = 'cmi.core.score.raw';
                    $datamodel['time'] = 'cmi.core.session_time';
                    $datamodel['[core_lesson]'] = 'cmi.suspend_data';
                    $datamodel['[comments]'] = 'cmi.comments';

                    $datarows = explode("\r\n",$this->aiccData);

                    reset($datarows);

                    $attempt = !is_null($this->attempt) ? $this->attempt : 1;

                    while ((list(,$datarow) = each($datarows)) !== false) {
                        if (($equal = strpos($datarow, '=')) !== false) {

                            $this->element = strtolower(trim(substr($datarow,0,$equal)));
                            $this->value = trim(substr($datarow,$equal+1));

                            if (isset($datamodel[$this->element])) {
                                $this->element = $datamodel[$this->element];

                                $method = isset($this->_methods[$this->element]) ? $this->_methods[$this->element] : "onDefault";
                                $id = $this->$method();
                            }
                        } else {
                            if (isset($datamodel[strtolower(trim($datarow))])) {
                                $this->element = $datamodel[strtolower(trim($datarow))];
                                $this->value = '';

                                while ((($datarow = current($datarows)) !== false) && (substr($datarow,0,1) != '[')) {
                                    $this->value .= $datarow."\r\n";
                                    next($datarows);
                                }
                                $this->value = (stripslashes($this->value));
                                $id = scorm_insert_track($this->user->id, $this->sco->scorm, $this->scoid, $attempt, $this->element, $this->value);
                            }
                        }
                    }


                    if (($this->mode == 'browse') && ($initlessonStatus == 'not attempted')){
                        $this->lessonStatus = 'browsed';
                        $id = scorm_insert_track($this->user->id, $this->sco->scorm, $this->scoid, $attempt, 'cmi.core.lesson_status', 'browsed');
                    }


                    if ($this->mode == 'normal') {
                        if ($sco = scorm_get_sco($this->scoid)) {
                            if (isset($sco->mastery_score) && is_numeric($sco->mastery_score)) {
                                if ($score != '') { // $score is correctly initialized w/ an empty string, see above
                                    if ($score >= trim($sco->mastery_score)) {
                                        $this->lessonStatus = 'passed';
                                    } else {
                                        $this->lessonStatus = 'failed';
                                    }
                                }
                            }
                            $id = scorm_insert_track($this->user->id, $this->sco->scorm, $sco->id, $attempt, 'cmi.core.lesson_status', $this->lessonStatus);
                        }
                    }
                }
                echo "error=0\r\nerror_text=Successful\r\n";
            } else if ($this->status == 'Terminated') {
                echo "error=1\r\nerror_text=Terminated\r\n";
            } else {
                echo "error=1\r\nerror_text=Not Initialized\r\n";
            }
        }
    }

    protected function onLessonLocation(){
        $attempt = !is_null($this->attempt) ? $this->attempt : 1;
        return scorm_insert_track($this->user->id, $this->sco->scorm, $this->scoid, $attempt, $this->element, $this->value);
    }

    protected function onLessonStatus(){
        $statuses = array(
            'passed' => 'passed',
            'completed' => 'completed',
            'failed' => 'failed',
            'incomplete' => 'incomplete',
            'browsed' => 'browsed',
            'not attempted' => 'not attempted',
            'p' => 'passed',
            'c' => 'completed',
            'f' => 'failed',
            'i' => 'incomplete',
            'b' => 'browsed',
            'n' => 'not attempted'
        );

        $exites = array(
            'logout' => 'logout',
            'time-out' => 'time-out',
            'suspend' => 'suspend',
            'l' => 'logout',
            't' => 'time-out',
            's' => 'suspend',
        );

        $values = explode(',',$this->value);
        $value = '';
        if (count($values) > 1) {
            $value = trim(strtolower($values[1]));
            $value = $value[0];
            if (isset($exites[$value])) {
                $value = $exites[$value];
            }
        }

        $attempt = !is_null($this->attempt) ? $this->attempt : 1;

        $id = "";
        if (empty($value) || isset($exites[$value])) {
            $subelement = 'cmi.core.exit';
            $id = scorm_insert_track($this->user->id, $this->sco->scorm, $this->scoid,  $attempt, $subelement, $value);
        }

        $value = trim(strtolower($values[0]));
        $value = $value[0];

        if (isset($statuses[$value]) && ($this->mode == 'normal')) {
            $value = $statuses[$value];

            $id = scorm_insert_track($this->user->id, $this->sco->scorm, $this->scoid,  $attempt, $this->element, $value);
        }

        $this->lessonStatus = $value;
        return $id;
    }

    protected function onScoreRaw(){
        $id = "";
        $values = explode(',',$this->value);

        $attempt = !is_null($this->attempt) ? $this->attempt : 1;

        if ((count($values) > 1) && ($values[1] >= $values[0]) && is_numeric($values[1])) {
            $subelement = 'cmi.core.score.max';
            $value = trim($values[1]);



                $id = scorm_insert_track($this->user->id, $this->sco->scorm, $this->scoid, $attempt, $subelement, $value);
            if ((count($values) == 3) && ($values[2] <= $values[0]) && is_numeric($values[2])) {
                $subelement = 'cmi.core.score.min';
                $value = trim($values[2]);
                $id = scorm_insert_track($this->user->id, $this->sco->scorm, $this->scoid, $attempt, $subelement, $value);
            }
        }

        $value = '';
        if (is_numeric($values[0])) {
            $value = trim($values[0]);
            $id = scorm_insert_track($this->user->id, $this->sco->scorm, $this->scoid,  $attempt, $this->element, $value);
        }
        $score = $value;
        return $id;
    }

    protected function onSessionTime(){
        if($this->value != "0000:00:00" || (is_null($this->JLMS_SESSION->get('scorm_session_time')))){
            $this->JLMS_SESSION->set('scorm_session_time', $this->value);
        }
        return false;
    }

    protected function onSuspendData(){

        return false;
    }

    protected function onComments(){

        return false;
    }

    protected function onDefault(){
        return false;
    }



}