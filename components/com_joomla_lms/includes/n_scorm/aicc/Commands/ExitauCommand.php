<?php
/**
 * User: nester_a
 * Date: 29.01.13
 */

include_once "Basecommand.php";
class ExitauCommand extends BaseCommand{

    public function __construct($param=array()){
        parent::_init($param);
    }

    public function run(){
        if ($this->status == 'Running') {
            if (!is_null($this->JLMS_SESSION->get('scorm_session_time'))) {

                $attempt = isset($this->attempt) && !is_null($this->attempt) ? $this->attempt : 1;
                // load track information for scorm
                $query = "SELECT * FROM #__lms_n_scorm_scoes_track WHERE userid='{$this->user->id}' AND scormid='{$this->sco->scorm}' AND scoid='{$this->scoid}' AND attempt='{$attempt}' AND element='cmi.core.total_time'";
                $this->JLMS_DB->SetQuery($query);
                $scoesTracks = $this->JLMS_DB->LoadObjectList();
                if (!empty($scoesTracks)) {
                    $track = current($scoesTracks);
                }

                // enable tracking only if AICC was enabled in config.inc.php
                if($this->enabled){
                    if (isset($track)) {
                        // Add session_time to total_time
                        $value = scorm_add_time($track->value, $this->JLMS_SESSION->get('scorm_session_time'));
                        $track->value = $value;
                        $track->timemodified = time();
                        $this->JLMS_DB->updateObject('#__lms_n_scorm_scoes_track',$track, 'id');
                        $id = $track->id;
                    } else {
                        $track = new stdClass();
                        $track->userid = $this->user->id;
                        $track->scormid = $this->sco->scorm;
                        $track->scoid = $this->scoid;
                        $track->element = 'cmi.core.total_time';
                        $track->value = $this->JLMS_SESSION->get('scorm_session_time');
                        $track->attempt = !is_null($this->attempt) ? $this->attempt : 1;
                        $track->timemodified = time();

                        $this->JLMS_DB->insertObject('#__lms_n_scorm_scoes_track',$track);
                    }
                }

            }
            $this->JLMS_SESSION->set('scorm_status', 'Terminated');

            $this->JLMS_SESSION->set('scorm_session_time', null);

            echo "error=0\r\nerror_text=Successful\r\n";
        } else if ($this->status == 'Terminated') {
            echo "error=1\r\nerror_text=Terminated\r\n";
        } else {
            echo "error=1\r\nerror_text=Not Initialized\r\n";
        }
    }
}