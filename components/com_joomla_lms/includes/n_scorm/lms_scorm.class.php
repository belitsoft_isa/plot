<?php
/**
* includes/n_scorm/lms_scorm.class.php
* JoomlaLMS Component
**/
// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

class mos_LMS_SCORM extends JLMSDBTable {
	var $id 				= null;
	var $course_id			= null;
	var $scorm_package		= null;
	var $summary			= null;
	var $version			= null;
	var $maxgrade			= null;
	var $grademethod		= null;
	var $maxattempt			= null;
	var $updatefreq			= null;
	var $md5hash			= null;
	var $launch				= null;
	var $skipview			= null;
	var $hidebrowse			= null;
	var $hidetoc			= null;
	var $hidenav			= null;
	var $auto				= null;
	var $popup				= null;
	var $options			= null;
	var $width				= null;
	var $height				= null;
	var $timemodified		= null;

	function mos_LMS_SCORM( &$db ) {
		$this->__construct( '#__lms_n_scorm', 'id', $db );
	}

	function check() {
		return true;
	}
}

function JLMS_CheckSCORM( &$id, $lpath_id, $course_id, &$scorm, $usertype ) {
	global $JLMS_DB, $my;
	
	$JLMS_ACL = JLMSFactory::getACL();
	$is_teacher = $JLMS_ACL->isTeacher(); 
	
	$query_add = " AND published = 1";
	if ($usertype == 1) {
		$query_add = '';
	}
	$query = "SELECT * FROM #__lms_learn_paths WHERE id = $lpath_id AND course_id = $course_id AND item_id = $id".$query_add;
	$JLMS_DB->SetQuery($query);
	$lp = $JLMS_DB->LoadObject();
	if (is_object($lp)) {
		if ($usertype != 1) {
			$query = "SELECT b.*, '' as r_status, '' as r_start, '' as r_end"
				. "\n FROM #__lms_learn_path_prerequisites as a, #__lms_learn_paths as b"
				. "\n WHERE a.lpath_id = $lpath_id  AND a.req_id = b.id AND b.course_id = $course_id";
			$JLMS_DB->SetQuery($query);
			$prereqs = $JLMS_DB->LoadObjectList();
			if (!empty($prereqs)) {
				require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_grades.lib.php");
				$user_ids = array();
				$user_ids[] = $my->id;
				JLMS_LP_populate_results($course_id, $prereqs, $user_ids);
				$j = 0;
				$not_available = false;
				while ($j < count($prereqs)) {
					if (!$prereqs[$j]->item_id) {
						if (empty($prereqs[$j]->r_status)) {
							$not_available = true;
							break;
						} else {
							$end_time = strtotime($prereqs[$j]->r_end);
							$current_time = strtotime(JHTML::_('date', null, "Y-m-d H:i:s"));
							if($current_time > $end_time && (($current_time - $end_time) < ($prereqs[$j]->time_minutes*60))){
								$not_available = true;
								break;	
							}
						}
					} else {
						if (empty($prereqs[$j]->s_status)) {
							$not_available = true;
							break;
						} else {
							$end_time = strtotime($prereqs[$j]->r_end);
							$current_time = strtotime(JHTML::_('date', null, "Y-m-d H:i:s"));
							if($current_time > $end_time && (($current_time - $end_time) < ($prereqs[$j]->time_minutes*60))){
								$not_available = true;
								break;	
							}
						}
					}
					$j ++;
				}
				if ($not_available) {
					return false;
				}
			}
		}
		switch ($lp->lp_type) {
			case 0:
				$query = "SELECT * FROM #__lms_n_scorm WHERE course_id = $course_id AND scorm_package = $id";
				$JLMS_DB->SetQuery($query);
				$scorm = $JLMS_DB->LoadObject();
				if (is_object($scorm)) {
					$scorm->scorm_name = $lp->lpath_name;
					$query = "SELECT * FROM #__lms_scorm_packages WHERE id = $scorm->scorm_package";
					$JLMS_DB->SetQuery($query);
					$scorm_ref = $JLMS_DB->LoadObject();
					if (is_object($scorm_ref)) {
						$scorm->reference = _JOOMLMS_SCORM_FOLDER_PATH . "/" . $scorm_ref->package_srv_name;
						$scorm->reference_folder = _JOOMLMS_SCORM_FOLDER_PATH . "/" . $scorm_ref->folder_srv_name;
					} else {
						return false;
					}
				} else {
					$query = "SELECT * FROM #__lms_scorm_packages WHERE id = $id AND course_id = $course_id";
					$JLMS_DB->SetQuery($query);
					$scorm_pack = $JLMS_DB->LoadObject();
					if (is_object($scorm_pack)) {
						// HERE we need to update this package (insert records into `#__lms_n_scorm`, `#__lms_n_scorm_scoes` tables
						require_once( _JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.operate.php");

						$scorm = JLMS_SCORM_ADD_INSTANCE($course_id, $id);
						$scorm->scorm_name = $lp->lpath_name;
						$scorm_params = 'scorm_nav_bar=1';
						$scorm->params = $scorm_params;
						$query = "UPDATE #__lms_n_scorm SET params = ".$JLMS_DB->Quote($scorm_params)." WHERE id = $scorm->id";
						$JLMS_DB->SetQuery($query);
						$JLMS_DB->query();
					} else {
						return false;
					}
				}
				if (isset($scorm->id) && $scorm->id) {
					$query = "UPDATE #__lms_learn_paths SET item_id = $scorm->id, lp_type = 1 WHERE course_id = $course_id AND item_id = $id AND lp_type = 0";
					$JLMS_DB->SetQuery($query);
					$JLMS_DB->query();
					$id = $scorm->id;
					return true;
				}
				return false;
			break;
			case 2:
				$query = "SELECT * FROM #__lms_n_scorm WHERE id = $id AND course_id = $course_id";
				$JLMS_DB->SetQuery($query);
				unset($outer_doc);
				$scorm = $JLMS_DB->LoadObject();
				if (is_object($scorm)) {
					$scorm_package = $scorm->scorm_package;	

					$query = "SELECT id FROM #__lms_n_scorm WHERE scorm_package = '".$scorm_package."' AND course_id = 0";
					$JLMS_DB->SetQuery( $query );
					$scorm_lib_id = $JLMS_DB->LoadResult();	

					$query = "SELECT outdoc_share, owner_id, allow_link FROM #__lms_outer_documents WHERE file_id = '".$scorm_lib_id."' AND folder_flag = 3";
					$JLMS_DB->SetQuery( $query );
					$outer_doc = $JLMS_DB->LoadObject();

					if(isset($outer_doc) && is_object($outer_doc) && isset($outer_doc->allow_link) && $outer_doc->allow_link == 1 ) {
						// 'share to courses' is still enabled
						$scorm->scorm_name = $lp->lpath_name;
						return true;
					} else {
						return false;
					}
				}
				return false;
			break;	
			case 1:
				$query = "SELECT * FROM #__lms_n_scorm WHERE id = $id AND course_id = $course_id";
				$JLMS_DB->SetQuery($query);
				$scorm = $JLMS_DB->LoadObject();
				if (is_object($scorm)) {
					$scorm->scorm_name = $lp->lpath_name;
					return true;
				}
				return false;
			break;
			default:
				return false;
			break;
		}
	}
	return false;
}
?>