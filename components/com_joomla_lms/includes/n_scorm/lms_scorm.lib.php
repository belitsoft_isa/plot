<?php
/**
* includes/n_scorm/lms_scorm.lib.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

/*function scorm_external_link($link) {
// check if a link is external
    $result = false;
    $link = strtolower($link);
    if (substr($link,0,7) == 'http://') {
        $result = true;
    } else if (substr($link,0,8) == 'https://') {
        $result = true;
    } else if (substr($link,0,4) == 'www.') {
        $result = true;
    }
    return $result;
}*/
if (!defined('SCO_ALL')) {
    define('SCO_ALL', 0);
}
if (!defined('SCO_DATA')) {
    define('SCO_DATA', 1);
}
if (!defined('SCO_ONLY')) {
    define('SCO_ONLY', 2);
}

if (!defined('_JOOMLMS_SCORM_API')) {
    define("_JOOMLMS_SCORM_API", _JOOMLMS_FRONT_HOME . "/includes/n_scorm");
}
if (!defined('_JOOMLMS_SCORM_PICS')) {
    global $JLMS_CONFIG;
    define("_JOOMLMS_SCORM_PICS", $JLMS_CONFIG->getCfg('live_site') . "/components/com_lms_scorm/scorm_pix");
}
/*function scorm_get_sco($id,$what=SCO_ALL) {
    if ($sco = get_record('scorm_scoes','id',$id)) {
        $sco = ($what == SCO_DATA) ? new stdClass() : $sco;
        if (($what != SCO_ONLY) && ($scodatas = get_records('scorm_scoes_data','scoid',$id))) {
            foreach ($scodatas as $scodata) {
                $sco->{$scodata->name} = $scodata->value;
            }
        }
        return $sco;
    } else {
        return false;
    }
}*/
function scorm_get_sco($id,$what=SCO_ALL) {
    global $JLMS_DB;
    $query = "SELECT * FROM #__lms_n_scorm_scoes WHERE id = $id";
    $JLMS_DB->SetQuery($query);
    $sco = $JLMS_DB->LoadObject();
    if (is_object($sco)) {//echo '----';echo '<pre>';print_r($sco);echo '</pre>';echo '----';
        $sco = ($what == SCO_DATA) ? new stdClass() : $sco;
        if ($what != SCO_ONLY) { 
            $query = "SELECT * FROM #__lms_n_scorm_scoes_data WHERE scoid = $id";
            $JLMS_DB->SetQuery($query);
            $scodatas = $JLMS_DB->LoadObjectList();
            foreach ($scodatas as $scodata) {
                $sco->{$scodata->name} = $scodata->value;
            }
        }
        return $sco;
    } else {
        return false;
    }
} 
function scorm_element_cmp($a, $b) {
    preg_match('/.*?(\d+)\./', $a, $matches);
    $left = intval($matches[1]);
    preg_match('/.?(\d+)\./', $b, $matches);
    $right = intval($matches[1]);
    if ($left < $right) {
        return -1; // smaller
    } elseif ($left > $right) {
        return 1;  // bigger
    } else {
        // look for a second level qualifier eg cmi.interactions_0.correct_responses_0.pattern
        if (preg_match('/.*?(\d+)\.(.*?)\.(\d+)\./', $a, $matches)) {
            $leftterm = intval($matches[2]);
            $left = intval($matches[3]);
            if (preg_match('/.*?(\d+)\.(.*?)\.(\d+)\./', $b, $matches)) {
                $rightterm = intval($matches[2]);
                $right = intval($matches[3]);
                if ($leftterm < $rightterm) {
                    return -1; // smaller
                } elseif ($leftterm > $rightterm) {
                    return 1;  // bigger
                } else {
                    if ($left < $right) {
                        return -1; // smaller
                    } elseif ($left > $right) {
                        return 1;  // bigger
                    }
                }
            }
        }
        // fall back for no second level matches or second level matches are equal
        return 0;  // equal to
    }
}
function scorm_insert_track($userid,$scormid,$scoid,$attempt,$element,$value) {
    global $JLMS_DB, $JLMS_CONFIG;
    $id = null;
    $query = "SELECT * FROM #__lms_n_scorm_scoes_track WHERE userid = '$userid' AND scormid = '$scormid' AND scoid = '$scoid' AND attempt = '$attempt' AND element = '$element'";
    $JLMS_DB->SetQuery($query);
    $track = $JLMS_DB->LoadObject();
    if (is_object($track)) {
    //if ($track = get_record_select('scorm_scoes_track',"userid='$userid' AND scormid='$scormid' AND scoid='$scoid' AND attempt='$attempt' AND element='$element'")) {
        $track->value = $value;
        $track->timemodified = time() - date('Z');
        $id = $track->id;
        $query = "UPDATE #__lms_n_scorm_scoes_track SET ".JLMSDatabaseHelper::NameQuote('value')." = ".$JLMS_DB->Quote($value).","
        . "\n ".JLMSDatabaseHelper::NameQuote('timemodified')." = ".$JLMS_DB->Quote(time() - date('Z'))
        . "\n WHERE userid = '$userid' AND scormid = '$scormid' AND scoid = '$scoid' AND attempt = '$attempt' AND element = '$element'";
        $JLMS_DB->SetQuery($query);
        $JLMS_DB->query();
        //$id = update_record('scorm_scoes_track',$track);
    } else {
        $track = new stdClass();
        $track->userid = $userid;
        $track->scormid = $scormid;
        $track->scoid = $scoid;
        $track->attempt = $attempt;
        $track->element = $element;
        $track->value = addslashes($value);
        $track->timemodified = time() - date('Z');
        $JLMS_DB->InsertObject('#__lms_n_scorm_scoes_track', $track, 'id');
        $id = $JLMS_DB->insertid();
        // $id = insert_record('scorm_scoes_track',$track);
    }
    
    JLMSErrorLog::writeSCORMLog($JLMS_DB->getErrorMsg(), $JLMS_CONFIG->get('course_id'), $scormid);
    
    $SCC = new JLMS_SCORMCourseComplete();
	$SCC->getInit($userid);

    return $id;
}


function scorm_simple_play($scorm,$user) {
    global $JLMS_DB;
    $result = false;
    $query = "SELECT * FROM #__lms_n_scorm_scoes WHERE scorm = $scorm->id AND launch<>''";
    $JLMS_DB->SetQuery($query);
    $scoes = $JLMS_DB->LoadObjectList();
    //if ($scoes = get_records_select('scorm_scoes','scorm='.$scorm->id.' AND launch<>""')) {
    if (!empty($scoes)) {
        if (count($scoes) == 1) {
            if ($scorm->skipview >= 1) {
                $sco = current($scoes);
                global $option, $Itemid;
                if (scorm_get_tracks($sco->id,$user->id) === false) {
                    JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=player_scorm&id=$scorm->id&scoid=$sco->id"));
                    //header('Location: player.php?a='.$scorm->id.'&scoid='.$sco->id);
                    $result = true;
                } else if ($scorm->skipview == 2) {
                    JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=player_scorm&id=$scorm->id&scoid=$sco->id"));
                    //header('Location: player.php?a='.$scorm->id.'&scoid='.$sco->id);
                    $result = true;
                }
            }
        }
    }
    return $result;
}

function scorm_repeater($what, $times) {
    if ($times <= 0) {
        return null;
    }
    $return = '';
    for ($i=0; $i<$times;$i++) {
        $return .= $what;
    }
    return $return;
}

/// Find the last attempt number for the given user id and scorm id
function scorm_get_last_attempt($scormid, $userid) {
    global $JLMS_DB;
    $query = "SELECT max(attempt) as a FROM #__lms_n_scorm_scoes_track WHERE userid = $userid AND scormid = $scormid";
    $JLMS_DB->SetQuery($query);
    $lastattempt = $JLMS_DB->LoadResult();
    if (!$lastattempt) {
        return '0';
    } else {
        return $lastattempt;
    }
    /*if ($lastattempt = get_record('scorm_scoes_track', 'userid', $userid, 'scormid', $scormid, '', '', 'max(attempt) as a')) {
        if (empty($lastattempt->a)) {
            return '1';
        } else {
            return $lastattempt->a;
        }
    }*/
}

function scorm_count_launchable($scormid,$organization='') {
    global $JLMS_DB;
    $strorganization = '';
    if (!empty($organization)) {
        $strorganization = " AND organization='$organization'";
    }
    $query = "SELECT count(*) FROM #__lms_n_scorm_scoes WHERE scorm=$scormid".$strorganization." AND launch<>''";
    $JLMS_DB->SetQuery($query);
    $record_count = $JLMS_DB->LoadResult();
    if (!$record_count) {
        $record_count = 0;
    }
    return $record_count;
    //return count_records_select('scorm_scoes',"scorm=$scormid$strorganization AND launch<>''");
}

/**
 * Gets all tracks of specified sco and user
 *
 * @param unknown_type $scoid
 * @param unknown_type $userid
 * @param unknown_type $attempt
 * @return unknown
 */
function scorm_get_tracks($scoid,$userid,$attempt='') {
    global $JLMS_DB;

    if (empty($attempt)) {
        $query = "SELECT scorm FROM #__lms_n_scorm_scoes WHERE id = $scoid";
        $JLMS_DB->SetQuery($query);
        $scormid = $JLMS_DB->LoadResult();
        //if ($scormid = get_field('scorm_scoes','scorm','id',$scoid)) {
        if ($scormid) {
            $attempt = scorm_get_last_attempt($scormid,$userid);
        } else {
            $attempt = 1;
        }
    }
    $attemptsql = ' AND attempt=' . $attempt;
    $query = "SELECT * FROM #__lms_n_scorm_scoes_track WHERE userid=$userid AND scoid=$scoid".$attemptsql." ORDER BY element ASC";
    $JLMS_DB->SetQuery($query);
    $tracks = $JLMS_DB->LoadObjectList();
    //if ($tracks = get_records_select('scorm_scoes_track',"userid=$userid AND scoid=$scoid".$attemptsql,'element ASC')) {
    if (!empty($tracks)) {
        $usertrack = new stdClass();
        $usertrack->userid = $userid;
        $usertrack->scoid = $scoid; 
        // Defined in order to unify scorm1.2 and scorm2004
        $usertrack->score_raw = '';
        $usertrack->status = '';
        $usertrack->total_time = '00:00:00';
        $usertrack->session_time = '00:00:00';
        $usertrack->timemodified = 0;
        foreach ($tracks as $track) {
            $element = $track->element;
            $usertrack->{$element} = $track->value;
            switch ($element) {
                case 'cmi.core.lesson_status':
                case 'cmi.completion_status':
                    if ($track->value == 'not attempted') {
                        $track->value = 'notattempted';
                    }       
                    $usertrack->status = $track->value;
                break;
                case 'cmi.core.score.raw':
                case 'cmi.score.raw':
                    $usertrack->score_raw = $track->value;
                break;
                case 'cmi.core.session_time':
                case 'cmi.session_time':
                    $usertrack->session_time = $track->value;
                break;
                case 'cmi.core.total_time':
                case 'cmi.total_time':
                    $usertrack->total_time = $track->value;
                break;
            }
            if (isset($track->timemodified) && ($track->timemodified > $usertrack->timemodified)) {
                $usertrack->timemodified = $track->timemodified;
            }
        }       
        return $usertrack;
    } else {
        return false;
    }
}

/*function scorm_optionals_data($item, $standarddata) {
    $result = array();
    $sequencingdata = array('sequencingrules','rolluprules','objectives');
    foreach ($item as $element => $value) {
        if (! in_array($element, $standarddata)) {
            if (! in_array($element, $sequencingdata)) {
                $result[] = $element;
            }
        }
    }
    return $result;
}*/


function scorm_array_search($item, $needle, $haystacks, $strict=false) {
    if (!empty($haystacks)) {
        foreach ($haystacks as $key => $element) {
            if ($strict) {
                if ($element->{$item} === $needle) {
                    return $key;
                }
            } else {
                if ($element->{$item} == $needle) {
                    return $key;
                }
            }
        }
    }
    return false;
}



function scorm_parse($scorm) {

    $reference = $scorm->reference_folder;
/*    if ($scorm->reference[0] == '#') {
        $reference = $CFG->repository.substr($scorm->reference,1);
    } else {
        $reference = $scorm->dir.'/'.$scorm->id;
    }*/
    // Parse scorm manifest
    if ($scorm->pkgtype == 'AICC') {
        require_once(_JOOMLMS_SCORM_API . '/datamodels/aicclib.php');
        $scorm->launch = scorm_parse_aicc($reference, $scorm->id);
    } else {
        require_once(_JOOMLMS_SCORM_API . '/datamodels/scormlib.php');
       /* if ($scorm->reference[0] == '#') {
            require_once($repositoryconfigfile);
        }*/

        $scorm->launch = scorm_parse_scorm($reference,$scorm->id);
    }
    return $scorm->launch;
}

/**
* Given a manifest path, this function will check if the manifest is valid
*
* @param string $manifest The manifest file
* @return object
*/
function scorm_validate_manifest($manifest) {
    $validation = new stdClass();
    if (is_file($manifest)) {
        $validation->result = true;
    } else {
        $validation->result = false;
        $validation->errors['reference'] = 'nomanifest';//get_string('nomanifest','scorm');
    }
    return $validation;
}

/**
* Given a aicc package directory, this function will check if the course structure is valid
*
* @param string $packagedir The aicc package directory path
* @return object
*/
function scorm_validate_aicc($packagedir) {
    $validation = new stdClass();
    $validation->result = false;
    if (is_dir($packagedir)) {
        if ($handle = opendir($packagedir)) {
            while (($file = readdir($handle)) !== false) {
                $ext = substr($file,strrpos($file,'.'));
                if (strtolower($ext) == '.cst') {
                    $validation->result = true;
                    break;
                }
            }
            closedir($handle);
        }
    }
    if ($validation->result == false) {
        $validation->errors['reference'] = 'nomanifest';//get_string('nomanifest','scorm');
    }
    return $validation;
}


function scorm_check_package($data) {
    global $JLMS_DB;
    $validation = new stdClass();

    //kosmos edition
    //if (!empty($data->course_id) && !empty($data->scorm_package)) {
    if ( !empty($data->scorm_package)) {
        #$externalpackage = scorm_external_link($reference);

        $validation->launch = 0;
        #$referencefield = $reference;
        #if (empty($reference)) {
        if (empty($data->scorm_package)) {
            $validation = null;
        }/* else if ($reference[0] == '#') {
            require_once($repositoryconfigfile);
            if ($CFG->repositoryactivate) {
                $referencefield = $reference.'/imsmanfest.xml';
                $reference = $CFG->repository.substr($reference,1).'/imsmanifest.xml';
            } else {
                $validation = null;
            }
        } else if (!$externalpackage) {*/
       // $query = "SELECT * FROM #__lms_scorm_packages WHERE id = $data->scorm_package";
        //$JLMS_DB->SetQuery($query);
        //$scorm_ref = $JLMS_DB->LoadObject();
        //$reference = $CFG->dataroot.'/'.$courseid.'/'.$reference;
        $reference = $data->reference;
        $reference_folder = $data->reference_folder;
        $externalpackage = false;
        #}g
        
        if (!empty($data->id)) {  
        //
        // SCORM Update
        //
            if ((!empty($validation)) && (is_file($reference) || $externalpackage)){
                
                if (!$externalpackage) {
                    $mdcheck = md5_file($reference);
                } else if ($externalpackage){
                    // temporary commented
                    /*if ($scormdir = make_upload_directory("$courseid/$CFG->moddata/scorm")) {
                        if ($tempdir = scorm_tempdir($scormdir)) {
                            copy ("$reference", $tempdir.'/'.basename($reference));
                            $mdcheck = md5_file($tempdir.'/'.basename($reference));
                            scorm_delete_files($tempdir);
                        }
                    }*/
                }
                $query = "SELECT a.*, b.package_srv_name, b.folder_srv_name FROM #__lms_n_scorm as a, #__lms_scorm_packages as b"
                . "\n WHERE a.id = $data->id AND a.scorm_package = b.id";
                $JLMS_DB->SetQuery($query);
                $scorm = $JLMS_DB->LoadObject();
                if (is_object($scorm)) {
                //if ($scorm = get_record('scorm','id',$scormid)) {
                    /*if ($scorm->reference[0] == '#') {
                        require_once($repositoryconfigfile);
                        if ($CFG->repositoryactivate) {
                            $oldreference = $CFG->repository.substr($scorm->reference,1).'/imsmanifest.xml';
                        } else {
                            $oldreference = $scorm->reference;
                        }
                    } else if (!scorm_external_link($scorm->reference)) {*/
                    $oldreference = _JOOMLMS_SCORM_FOLDER_PATH . "/" .$scorm->package_srv_name;
                    /* } else {
                        $oldreference = $scorm->reference;
                    }*/
                    $validation->launch = $scorm->launch;
                    if ((($oldreference == $reference) && ($mdcheck != $scorm->md5hash)) || ($oldreference != $reference)) {
                        // This is a new or a modified package
                        $validation->launch = 0;
                    } else {
                    // Old package already validated
                        if (strpos($scorm->version,'AICC') !== false) {
                            $validation->pkgtype = 'AICC';
                        } else {
                            $validation->pkgtype = 'SCORM';
                        }
                    }
                } else {
                    $validation = null;
                }
            } else {
                $validation = null;
            }
        }
        //$validation->launch = 0;
        if (($validation != null) && ($validation->launch == 0)) {
        //
        // Package must be validated
        //
        // (DEN) eto vse delo nugno vsunut' vmesto JLMS_UploadSCORM();
            $ext = strtolower(substr(basename($reference),strrpos(basename($reference),'.')));
            $tempdir = '';
            switch ($ext) {
                case '.pif':
                case '.zip':
                // Create a temporary directory to unzip package and validate package
                   /* $scormdir = '';
                    if ($scormdir = make_upload_directory("$courseid/$CFG->moddata/scorm")) {
                        if ($tempdir = scorm_tempdir($scormdir)) {
                            copy ("$reference", $tempdir.'/'.basename($reference));
                            unzip_file($tempdir.'/'.basename($reference), $tempdir, false);
                            if (!$externalpackage) {
                                unlink ($tempdir.'/'.basename($reference));
                            }
                            if (is_file($tempdir.'/imsmanifest.xml')) {
                                $validation = scorm_validate_manifest($tempdir.'/imsmanifest.xml');
                                $validation->pkgtype = 'SCORM';
                            } else {
                                $validation = scorm_validate_aicc($tempdir);
                                $validation->pkgtype = 'AICC';
                            }
                        } else {
                            $validation = null;
                        }
                    } else {
                        $validation = null;
                    }*/
                            if (is_file( $reference_folder . '/imsmanifest.xml')) {
                                $validation = scorm_validate_manifest($reference_folder.'/imsmanifest.xml');
                                $validation->pkgtype = 'SCORM';
                            } else {
                                $validation = scorm_validate_aicc($reference_folder);
                                $validation->pkgtype = 'AICC';
                            }
                  
                break;
                case '.xml':
                    if (basename($reference) == 'imsmanifest.xml') {
                        if ($externalpackage) {
                           /* if ($scormdir = make_upload_directory("$courseid/$CFG->moddata/scorm")) {
                                if ($tempdir = scorm_tempdir($scormdir)) {
                                    copy ("$reference", $tempdir.'/'.basename($reference));
                                    if (is_file($tempdir.'/'.basename($reference))) {
                                        $validation = scorm_validate_manifest($tempdir.'/'.basename($reference));
                                    } else {
                                        $validation = null;
                                    }
                                }
                            }*/
                        } else {
                            $validation = scorm_validate_manifest(/*$CFG->dataroot.'/'.$COURSE->id.'/'.*/$reference);
                        }
                        $validation->pkgtype = 'SCORM';
                    } else {
                        $validation = null;
                    }
                break;
                default: 
                    $validation = null;
                break;
            }
            if ($validation == null) {
                if (is_dir($tempdir)) {
                // Delete files and temporary directory
                //(DEN) temporary commented
                    //scorm_delete_files($tempdir);
                }
            } else {
                if (($ext == '.xml') && (!$externalpackage)) {
                    #$validation->datadir = dirname($referencefield);
                } else {
                    #$validation->datadir = substr($tempdir,strlen($scormdir));
                }
                $validation->launch = 0;
            }
        }
    } else {
        $validation = null;
    }
    return $validation;
}

/// Constants and settings for module scorm
define('UPDATE_NEVER', '0');
define('UPDATE_ONCHANGE', '1');
define('UPDATE_EVERYDAY', '2');
define('UPDATE_EVERYTIME', '3'); 

function scorm_view_display ($user, $scorm, $action, $cm, $boxwidth='') {
    //global $CFG;
    global $JLMS_DB;

    if ($scorm->updatefreq == UPDATE_EVERYTIME){
        $scorm->instance = $scorm->id;
        scorm_update_instance($scorm);
    }

    $organization = intval( mosGetParam($_REQUEST, 'organization', 0));
    //$organization = optional_param('organization', '', PARAM_INT);

    //print_simple_box_start('center',$boxwidth);
?>
        <div class="structurehead"><?php echo 'coursestruct';//print_string('coursestruct','scorm') ?></div>
<?php
    if (!$organization) {
        $organization = $scorm->launch;
    }
    $query = "SELECT id, title FROM #__lms_n_scorm_scoes WHERE scorm='$scorm->id' AND organization='' AND launch='' ORDER BY id";
    $JLMS_DB->SetQuery( $query );
    $orgs = $JLMS_DB->LoadObjectList();
   // if ($orgs = get_records_select_menu('scorm_scoes',"scorm='$scorm->id' AND organization='' AND launch=''",'id','id,title')) {
        if (count($orgs) > 1) {
 ?>
            <div class='center'>
                <?php echo 'organizations';//print_string('organizations','scorm') ?>
                <form id='changeorg' method='post' action='<?php echo $action ?>'>
                    <?php choose_from_menu($orgs, 'organization', "$organization", '','submit()') ?>
                </form>
            </div>
<?php
        }
    //}
    $orgidentifier = '';
    if ($sco = scorm_get_sco($organization, SCO_ONLY)) {
        if (($sco->organization == '') && ($sco->launch == '')) {
            $orgidentifier = $sco->identifier;
        } else {
            $orgidentifier = $sco->organization;
        }
    }
    $scorm->version = strtolower(preg_replace('/[^a-zA-Z0-9_-]/i', '', $scorm->version));   // Just to be safe
    if (!file_exists(dirname(__FILE__).'/datamodels/'.$scorm->version.'lib.php')) {
        $scorm->version = 'scorm_12';
    }
    require_once(dirname(__FILE__).'/datamodels/'.$scorm->version.'lib.php');

    $result = scorm_get_toc($user,$scorm,'structlist',$orgidentifier);
    $incomplete = $result->incomplete;
    echo $result->toc;
    //print_simple_box_end();
?>
            <div class="center">
                <form id="theform" method="post" action="<?php echo $CFG->wwwroot ?>/mod/scorm/player.php?id=<?php echo $cm->id ?>"<?php echo $scorm->popup == 1?' target="newwin"':'' ?>>
              <?php
                  if ($scorm->hidebrowse == 0) {
                      //print_string('mode','scorm');
                      // (DEN)
                      echo 'mode';//
                      // (DEN)
                      //echo ': <input type="radio" id="b" name="mode" value="browse" /><label for="b">'.get_string('browse','scorm').'</label>'."\n";
                      echo ': <input type="radio" id="b" name="mode" value="browse" /><label for="b">'.'browse'.'</label>'."\n";
                      //echo '<input type="radio" id="n" name="mode" value="normal" checked="checked" /><label for="n">'.get_string('normal','scorm')."</label>\n";
                      echo '<input type="radio" id="n" name="mode" value="normal" checked="checked" /><label for="n">'.'normal'."</label>\n";
                  } else {
                      echo '<input type="hidden" name="mode" value="normal" />'."\n";
                  }
                  if (($incomplete === false) && (($result->attemptleft > 0)||($scorm->maxattempt == 0))) {
?>
                  <br />
                  <input type="checkbox" id="a" name="newattempt" />
                  <label for="a"><?php echo 'newattempt';//print_string('newattempt','scorm') ?></label>
<?php
                  }
              ?>
              <br />
              <input type="hidden" name="scoid" />
              <input type="hidden" name="currentorg" value="<?php echo $orgidentifier ?>" />
              <input type="submit" value="<?php echo 'entercourse';//print_string('entercourse','scorm') ?>" />
              </form>
          </div>
<?php
}




?>