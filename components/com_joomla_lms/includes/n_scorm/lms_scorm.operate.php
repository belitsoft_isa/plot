<?php
/**
* includes/n_scorm/lms_scorm.operate.php
* Joomla LMS Component
* deN_SCORM library 20.03.2007
* * * ElearningForce DK
**/
// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

/**
 * Add new instance into table `#__lms_n_scorm` (record in `#__lms_scorm_packages` should be exists)
 *
 * @param unknown_type $course_id
 * @param unknown_type $scorm_package
 * 
 */
function JLMS_SCORM_ADD_INSTANCE( $course_id, $scorm_package ) {
	global $JLMS_DB, $JLMS_CONFIG;
	$mem_lim = $JLMS_CONFIG->get('memory_limit_import_export', 0);
	if ($mem_lim) {
		JLMS_adjust_memory_limit($mem_lim);
	}

	$row = new mos_LMS_SCORM( $JLMS_DB );

	$row->course_id = $course_id;
	$row->scorm_package = $scorm_package;
	$query = "SELECT * FROM #__lms_scorm_packages WHERE id = $row->scorm_package";
    $JLMS_DB->SetQuery($query);
    $scorm_ref = $JLMS_DB->LoadObject();

    //$reference = $CFG->dataroot.'/'.$courseid.'/'.$reference;
    $row->reference = _JOOMLMS_SCORM_FOLDER_PATH . "/" . $scorm_ref->package_srv_name;
    $row->reference_folder = _JOOMLMS_SCORM_FOLDER_PATH . "/" . $scorm_ref->folder_srv_name;
	#scorm_check_package($row);
	if (($packagedata = scorm_check_package($row)) != null) {
		$pkgtype = $packagedata->pkgtype;
		
		// (DEN) temporary i don't know what directory put here.
		$datadir = '';//$packagedata->datadir;
		$row->launch = $packagedata->launch;
		$parse = 1;
		
		$row->timemodified = time() - date('Z');

		$row->md5hash = md5_file($row->reference);

		$row->params = 'scorm_nav_bar=0'; // (DEN) syuda zanosit' vse nastroiki dlya 'Full screen' (old field is 'options')
		$row->width = intval(mosGetParam($_REQUEST, 'scorm_width', 100));// (%) str_replace('%','',$scorm->width);
		$row->height = intval(str_replace('px','',$scorm_ref->window_height));//'600';// (px) str_replace('%','',$scorm->height);
		if (!$row->height) {
			$row->height = '600';
		}
		
		if (!isset($row->whatgrade)) {
        	$row->whatgrade = 0;
    	}
    	$row->grademethod = ($row->whatgrade * 10) + $row->grademethod;
    	
    	$whatgrade = $row->whatgrade;
		unset($row->whatgrade);
    	unset($row->reference);
		unset($row->reference_folder);

		if (!$row->check()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if (!$row->store()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
        $row->datadir = $datadir;
        $row->pkgtype = $pkgtype;
        $row->parse = $parse;
        $row->whatgrade = $whatgrade;
        $row->reference = _JOOMLMS_SCORM_FOLDER_PATH . "/" . $scorm_ref->package_srv_name;
    	$row->reference_folder = _JOOMLMS_SCORM_FOLDER_PATH . "/" . $scorm_ref->folder_srv_name;
		// Parse scorm manifest
		if ($row->parse == 1) {
			//$row->id = $id;
			$row->launch = scorm_parse($row);
			$query = "UPDATE #__lms_n_scorm SET launch = ".$row->launch." WHERE id = ".$row->id;
			//set_field('scorm','launch',$scorm->launch,'id',$scorm->id);
		}

	}
	return $row;
}
?>