<?php
/**
* includes/joomla_lms.subscription.lib.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Direct Access to this location is not allowed.' );

require_once (_JOOMLMS_FRONT_HOME. '/joomla_lms.main.php');
require_once(_JOOMLMS_FRONT_HOME. '/includes/libraries/lms.lib.func.php');

function jlms_update_payment( $item_number, $txn_id, $status, $payment_date, $tax_amount = 0, $tax2_amount = 0, $isRecurring = false ){
	global $JLMS_DB;
	$payment_date = date('Y-m-d H:i:s', strtotime($payment_date));
	
	if( !$isRecurring ) 
	{
		if ($status == 'Pending') {
			$query = "SELECT status, payment_type, user_id FROM `#__lms_payments` WHERE id = $item_number ";
			$JLMS_DB->setQuery($query);
			$prev_payment = $JLMS_DB->LoadObject();
			if (is_object($prev_payment) && isset($prev_payment->status)) {
				if ($prev_payment->status == 'Completed' && $prev_payment->payment_type == 2) {
	
					$query = "SELECT item_id FROM `#__lms_payment_items` WHERE payment_id = ".intval($item_number)." AND item_type = 1";
					$JLMS_DB->setQuery($query);
					$conf_items = $JLMS_DB->LoadObjectList();
					if (!empty($conf_items)) {
						$conf_items_str = implode(',',$conf_items);
						$query = "SELECT credits FROM #__lmsce_conf_user_credits WHERE user_id = $prev_payment->user_id";
						$JLMS_DB->setQuery($query);
						$user_credits = intval($JLMS_DB->LoadResult());
						$query = "SELECT id, num_credits FROM #__lmsce_conf_subs WHERE id IN ($conf_items_str)";
						$JLMS_DB->setQuery($query);
						$credits_data = $JLMS_DB->LoadObjectList();
						$credits = 0;
						foreach ($conf_items as $subc) {
							foreach ($credits_data as $cd) {
								if ($cd->id == $subc) {
									$credits = $credits + $cd->num_credits; break;
								}
							}
						}
						$c_c = $user_credits - $credits;
						$query = "SELECT count(*) FROM #__lmsce_conf_user_credits WHERE user_id = $prev_payment->user_id";
						$JLMS_DB->setQuery($query);
						if ($JLMS_DB->LoadResult()) {
							$query = "UPDATE #__lmsce_conf_user_credits SET credits = $c_c WHERE user_id = $prev_payment->user_id";
							$JLMS_DB->setQuery($query);
							$JLMS_DB->query();
						} else {
							$query = "INSERT INTO #__lmsce_conf_user_credits (credits, user_id) VALUES ($c_c, $prev_payment->user_id)";
							$JLMS_DB->setQuery($query);
							$JLMS_DB->query();
						}
					}
				}
			}
		}
	}

	/*
	// commented by DEN - please run 'database check' after installation to execute this query
	$query = "ALTER TABLE `#__lms_payments` ADD `tax2_amount` VARCHAR( 255 ) NOT NULL AFTER `tax_amount`";
	$JLMS_DB->setQuery($query);
	$JLMS_DB->query();*/

	$query = "UPDATE `#__lms_payments` SET txn_id = '$txn_id', status = '$status', tax_amount = '$tax_amount', tax2_amount = '$tax2_amount', date ='$payment_date' WHERE id = $item_number ";
	$JLMS_DB->setQuery($query);
	$JLMS_DB->query();
	
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$_JLMS_PLUGINS->loadBotGroup('system');
	$args = array();
	$args[0] = $item_number;
	$args[1] = $status;
	$plugin_result_array = $_JLMS_PLUGINS->trigger('OnAfterUpdatePayment', $args);
}

function jlms_check_payment_transaction($sub_total, $item_number, $user_id = null) {
	global $JLMS_DB;

	$query = "SELECT * FROM `#__lms_payments` WHERE id = ".intval($item_number);
	$JLMS_DB->setQuery($query);
	$sub = $JLMS_DB->loadobject();
			
	if ( is_object($sub) && isset($sub->payment_type) && (float)$sub->amount == (float)$sub_total ) 
	{
		return true;	
	}
	
	/*
	if (is_object($sub) && isset($sub->payment_type)) {

		// TODO: check user_id
		if ($user_id === null) {
	
		} else {
			// TODO: check user_id
			// NOTE! this operation is called by payment gateway bot, so user_id would be zero!
		}
		if (!$sub->payment_type) {
			$query = "SELECT * FROM `#__lms_subscriptions` WHERE id = $sub->sub_id";
			$JLMS_DB->setQuery($query);
			$subscription = null;
			$subscription = $JLMS_DB->loadObject();
			if (is_object($subscription) && isset($subscription->account_type)) {
				$subscribe_total = $subscription->price;
				if ($subscription->account_type == 5 ){		
					$subscribe_total = round($subscription->price-($subscription->price*$subscription->discount/100),2);
				}
				if ($subscribe_total == $sub_total) {
					return true;
				}
			}
		} elseif ($sub->payment_type == 1) {
			$query = "SELECT * FROM `#__lms_subscriptions_custom` WHERE id = $sub->sub_id";
			$JLMS_DB->setQuery($query);
			$subscription = $JLMS_DB->loadObject();
			if (is_object($subscription) && isset($subscription->price)) {
				$subscribe_total = $subscription->price;
				if ($subscribe_total == $sub_total) {
					return true;
				}
			}
		} elseif ($sub->payment_type == 2) {
			$query = "SELECT * FROM `#__lms_payment_items` WHERE payment_id = ".intval($item_number);
			$JLMS_DB->setQuery($query);
			$payment_items = $JLMS_DB->LoadObjectList();

			$subscribe_total = 0;
			foreach ($payment_items as $pitem) {
				if ($pitem->item_type == 0) {
					$query = "SELECT * FROM `#__lms_subscriptions` WHERE id = $pitem->item_id";
					$JLMS_DB->setQuery($query);
					$subscription = $JLMS_DB->loadObject();
					if (is_object($subscription) && isset($subscription->account_type)) {
						if ($subscription->account_type == 5 ){		
							$subscribe_total = $subscribe_total + round($subscription->price-($subscription->price*$subscription->discount/100),2);
						} else {
							$subscribe_total = $subscribe_total + $subscription->price;
						}
					}
				} elseif ($pitem->item_type == 1) {
					$query = "SELECT * FROM `#__lmsce_conf_subs` WHERE id = $pitem->item_id";
					$JLMS_DB->setQuery($query);
					$subscription = $JLMS_DB->loadObject();
					if (is_object($subscription) && isset($subscription->price)) {
						$subscribe_total = $subscribe_total + $subscription->price;
					}
				}
			}

			if ($subscribe_total == $sub_total) {
				return true;
			}
		}
	}
	*/
	
	return false;
}

function jlms_register_new_user( $item_number, $if_subscr_end = 0 ) {
	global $JLMS_DB, $JLMS_CONFIG;

	$query = "SELECT id, payment_type, sub_id, user_id, `date`  FROM `#__lms_payments` WHERE id = $item_number";
	$JLMS_DB->setQuery($query);
	$payment_info = $JLMS_DB->loadObject();
		
	if ( is_object($payment_info) ) {		
		jlms_enroll_user_by_payment($payment_info, $JLMS_DB, $item_number, $if_subscr_end);
	}
}
function jlms_enroll_user_by_payment(&$payment_info, &$JLMS_DB, $item_number, $if_subscr_end) {
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	$JLMS_CONFIG = JLMSFactory::getConfig();
	
	if ($item_number) {// if item_number == 0 - yhis function is called from backend 'assign subscription' function, without payment_id
	/**
	* define whether subcriptions with account_types = 6 and 4 are in package
	*/
	$query = "SELECT s.id" 
			."\n FROM #__lms_subscriptions s,"
			."\n #__lms_payment_items pi,"
			."\n #__lms_payments p"
			."\n WHERE p.id=".$item_number." AND pi.payment_id=p.id AND pi.item_id=s.id AND s.account_type=6";
	$JLMS_DB->setQuery($query);
	$recurSubscr = $JLMS_DB->loadResult();

	$query = "SELECT s.id, s.access_days" 
			."\n FROM #__lms_subscriptions s,"
			."\n #__lms_payment_items pi,"
			."\n #__lms_payments p"
			."\n WHERE p.id=".$item_number." AND pi.payment_id=p.id AND pi.item_id=s.id AND s.account_type=4 LIMIT 0,1";
	$JLMS_DB->setQuery($query);
	$accessd = $JLMS_DB->loadObject();

	$accessdaysSubscr = isset($accessd->id) ? $accessd->id : 0;

	$query = "SELECT s.id" 
			."\n FROM #__lms_subscriptions s,"
			."\n #__lms_payment_items pi,"
			."\n #__lms_payments p"
			."\n WHERE p.id=".$item_number." AND pi.payment_id=p.id AND pi.item_id=s.id AND s.account_type=2 LIMIT 0,1";
	$JLMS_DB->setQuery($query);
	$daytodaySubscr = $JLMS_DB->loadResult();	
	//if 4 and 6 in one package
	$both = $accessdaysSubscr && $recurSubscr;
	//if 2 and 6 in one package
	$bothdtd = $daytodaySubscr && $recurSubscr;	
	} else {		
		$recurSubscr = 0;
		$accessd = 0;
		$accessdaysSubscr = 0;
		$daytodaySubscr = 0;
		$both = false;
		$bothdtd = false;
	}
	
	if (isset($payment_info->user_id)) {
		$user_id = $payment_info->user_id;
		$p_date  = $payment_info->date;
		
		//timezone fix
		$p_date = JLMS_offsetDateToDisplay($p_date);
		$p_date = JLMS_dateToDB($p_date);
		//timezone fix
		
		$courses_to_enroll = array(); // here would be placed items, which we should proceed
		$conferences_to_enroll = array();
		$sub_courses = array();
		$sub_conferences = array();

		// get list of subscriptions to proceed
		if ( ($payment_info->payment_type == 0) && $payment_info->sub_id) {			
			$new_sub = new stdClass();
			$new_sub->item_type = 0;
			$new_sub->id = $payment_info->sub_id;
			$sub_courses[] = $new_sub;
		} elseif ( ($payment_info->payment_type == 1) && $payment_info->sub_id) {			
			$new_sub = new stdClass();
			$new_sub->item_type = 1;
			$new_sub->id = $payment_info->sub_id;
			$sub_courses[] = $new_sub;
		} elseif ($payment_info->payment_type == 2 && isset($payment_info->id) && $payment_info->id) {			
			$query = "SELECT * FROM #__lms_payment_items WHERE payment_id = ".$item_number;
			$JLMS_DB->setQuery($query);
			$payment_subs = $JLMS_DB->loadObjectList();
			$sub_courses = array();
			$sub_conferences = array();
			
			foreach ($payment_subs as $ps) {
				if ($ps->item_type == 0) {
					$new_sub = new stdClass();
					$new_sub->item_type = 0;
					$new_sub->id = $ps->item_id;
					$sub_courses[] = $new_sub;
				} elseif ($ps->item_type == 1) {
					$sub_conferences[] = $ps->item_id;
				}
			}
		}
		
		// get list of courses/conferences to proceed		
		foreach ($sub_courses as $sub_c) {
			
			if ($sub_c->item_type == 0) {
				$query = "SELECT * FROM `#__lms_subscriptions` WHERE id = '$sub_c->id'";
			} elseif ($sub_c->item_type == 1) {
				$query = "SELECT * FROM `#__lms_subscriptions_custom` WHERE id = '$sub_c->id'";
			}
			$JLMS_DB->setQuery($query);
			$subscription = $JLMS_DB->loadObject();
			if (is_object($subscription)) {
				if ($sub_c->item_type == 1) {
					$subscription->account_type = 1;
				}
				if ($sub_c->item_type == 0) {
					$query = "SELECT course_id FROM `#__lms_subscriptions_courses` WHERE sub_id = '$sub_c->id'";
				} elseif ($sub_c->item_type == 1) {
					$query = "SELECT course_id FROM `#__lms_subscriptions_custom_courses` WHERE sub_id = '$sub_c->id'";
				}
				$JLMS_DB->setQuery($query);
				$courses = $JLMS_DB->loadObjectList();
				
				if($if_subscr_end && $bothdtd)
				{
					$sCourses = '(';
					for($courseCount = 0; $courseCount < count($courses); $courseCount++)
					{
						$sCourses .= $courses[$courseCount]->course_id.',';
					}
					$sCourses = substr($sCourses, 0, strlen($sCourses)-1);
					$sCourses = ')';
					$query = "UPDATE #__lms_users_in_groups SET start_date=0 WHERE course_id IN ".$sCourses." AND user_id=".$user_id;
					
					$JLMS_DB->setQuery($query);
					$JLMS_DB->query();
				}
				
				$i = 0;
				
				while ($i < count($courses)) {
					
					$j = 0;$create_new = true;
					foreach ($courses_to_enroll as $ce) {
						if ($ce->course_id == $courses[$i]->course_id) {
							$create_new = false; break;
						}
						$j ++;
					}

					$new_course_item = new stdClass();
					$new_course_item->course_id = $courses[$i]->course_id;
					$new_course_item->publish_start = 1;
					$new_course_item->start_date = '';
					$new_course_item->publish_end = 1;
					$new_course_item->end_date = '';
					$new_course_item->access_days = 0;
										
					if ($subscription->account_type == 2 && !$bothdtd) {
						$new_course_item->start_date = $subscription->start_date;
						$new_course_item->end_date = $subscription->end_date;
					}
					elseif($subscription->account_type == 2 && $bothdtd && $if_subscr_end)
					{
						$new_course_item->start_date = $subscription->start_date;
						$new_course_item->end_date = $subscription->end_date;					
					}
					elseif ($subscription->account_type == 3) {
						$new_course_item->start_date = $subscription->start_date;
						$new_course_item->publish_end = 0;
					}
					elseif ($subscription->account_type == 4 && !$if_subscr_end) {
						$new_course_item->access_days = $subscription->access_days;
					}					
					elseif ($subscription->account_type == 6 && !$if_subscr_end)

					{			
						
						$query = "SELECT SUM( s.access_days )"
								."\n FROM #__lms_subscriptions s"
								."\n INNER JOIN #__lms_payment_items pi ON pi.item_id=s.id"
								."\n INNER JOIN #__lms_payments p ON p.id=pi.payment_id"
								."\n WHERE s.account_type =4"
								."\n AND pi.payment_id =".$item_number
								."\n GROUP BY payment_id";
						$JLMS_DB->setQuery($query);
						$access_days = $JLMS_DB->loadResult();
						$currentPeriod = '';						
						//get all payments that where made from payments history
						
						$query = "SELECT COUNT(id)"
								."\n FROM #__lms_payments"
								."\n WHERE parent_id=$item_number";
						$JLMS_DB->setQuery($query);
						$numberPayments = $JLMS_DB->loadResult();						
						
						$query = "SELECT * FROM #__lms_payments_checksum WHERE payment_id = ".$item_number;	
						$JLMS_DB->setQuery( $query );
						$checksum = $JLMS_DB->loadObject();
																		
						$query = "SELECT p.id, p.name, p.description, p.published, p.p1, p.t1, p.p2, p.t2, p.p3, p.t3, s.a1, s.a2, s.a3, p.src, p.sra, p.srt"
							."\n FROM #__lms_subscriptions s,"
							."\n #__lms_plans_subscriptions ps,"
							."\n #__lms_plans p"
							."\n WHERE s.id=".$subscription->id
							."\n AND ps.subscr_id=s.id"
							."\n AND p.id=ps.plan_id";
						$JLMS_DB->setQuery($query);
						$plan = $JLMS_DB->loadObject();	
											
						$new_course_item->start_date = date('Y-m-d');
																
						//poluchaem kakoi seichas period
						if ((( $plan->t1 && $checksum->p1) || ($plan->t2 && $checksum->p2)) && $numberPayments < 2 )
						{
							$currentPeriod = '1trial';
							$daysToAdd = $checksum->p1;
						}
						elseif (($plan->t1 && $checksum->p1) && ($plan->t2 && $checksum->p2) && $numberPayments < 3)
						{
							$currentPeriod = '2trial';
							$daysToAdd = $checksum->p2;
						}
						else
						{
							$currentPeriod = 'recurring';
							$daysToAdd = $checksum->p3;
						}
						
						//vse ravno kakoi iz trialov
						//glavnoe chto perviy platezh i perviy trial	
												
						if ($currentPeriod == '1trial')
						{						
							$t1 = ($plan->t1)?$plan->t1:$plan->t2;
							$p1 = ($checksum->p1)?$checksum->p1:$checksum->p2;
							$p1 += ($access_days)?$access_days:0;
														
							switch($t1)
							{
								case 'D':																										
									$new_course_item->publish_end = 1;
									$new_course_item->end_date = gmdate('Y-m-d', mktime(0, 0, 0, gmdate("m"), gmdate("d")+$p1, gmdate("Y")));
									break;
								case 'W':									
									$new_course_item->publish_end = 1;									
									$new_course_item->end_date = gmdate('Y-m-d', mktime(0, 0, 0, gmdate("m"), gmdate("d")+7*$p1, gmdate("Y")));
									break;
								case 'Y':
									$new_course_item->publish_end = 1;									
									$new_course_item->end_date = gmdate('Y-m-d', mktime(0, 0, 0, gmdate("m"), gmdate("d"), gmdate("Y")+$p1));
									break;
								case 'M':
									$new_course_item->publish_end = 1;									
									$new_course_item->end_date = gmdate('Y-m-d', mktime(0, 0, 0, gmdate("m")+$p1, gmdate("d"), gmdate("Y")));
									break;
							}							
						}			
												
						//analogichno vtoroi platezh i 2trial
						if ($currentPeriod == '2trial')
						{	
							
							switch($plan->t2)
							{
								case 'D':																										
									$new_course_item->publish_end = 1;
									$new_course_item->end_date = gmdate('Y-m-d', mktime(0, 0, 0, gmdate("m"), gmdate("d")+$checksum->p2, gmdate("Y")));
									break;
								case 'W':									
									$new_course_item->publish_end = 1;
									$new_course_item->end_date = gmdate('Y-m-d', mktime(0, 0, 0, gmdate("m"), gmdate("d")+7*$checksum->p2, gmdate("Y")));
									break;
								case 'Y':
									$new_course_item->publish_end = 1;
									$new_course_item->end_date = gmdate('Y-m-d', mktime(0, 0, 0, gmdate("m"), gmdate("d"), gmdate("Y")+$checksum->p2));
									break;
								case 'M':
									$new_course_item->publish_end = 1;
									$new_course_item->end_date = gmdate('Y-m-d', mktime(0, 0, 0, gmdate("m")+$checksum->p2, gmdate("d"), gmdate("Y")));
									break;
							}											
						}
						
						if ($currentPeriod == 'recurring')
						{							
																	
								$planday = $checksum->p3;
								$planday += ($access_days)?$access_days:0;
							
								switch( $plan->t3 )
								{
									case 'D':																										
										$new_course_item->publish_end = 1;									
										$new_course_item->end_date = gmdate('Y-m-d', mktime(0, 0, 0, gmdate("m"), gmdate("d")+$planday, gmdate("Y")));
										break;
									case 'W':									
										$new_course_item->publish_end = 1;
										$new_course_item->end_date = gmdate('Y-m-d', mktime(0, 0, 0, gmdate("m"), gmdate("d")+7*$planday, gmdate("Y")));
										break;
									case 'Y':
										$new_course_item->publish_end = 1;									
										$new_course_item->end_date = gmdate('Y-m-d', mktime(0, 0, 0, gmdate("m"), gmdate("d"), gmdate("Y")+$planday));
										break;
									case 'M':
										$new_course_item->publish_end = 1;
										$new_course_item->end_date = gmdate('Y-m-d', mktime(0, 0, 0, gmdate("m")+$planday, gmdate("d"), gmdate("Y")));
										break;
								}
							
						}										
					}
					//zaglushki chtoby ne obnulilos' publish_start i publish_end
					elseif ($subscription->account_type == 6 && $if_subscr_end)
					{
					}
					elseif($subscription->account_type == 2 && $bothdtd && !$if_subscr_end)
					{					
					}
					elseif ($subscription->account_type == 4 && $if_subscr_end) {						
					}					
					else 
					{
						$new_course_item->publish_start = 0;
						$new_course_item->publish_end = 0;
					}
					

					if ($create_new) {												
						$courses_to_enroll[] = $new_course_item;
					} else {	
						
						// else we should work with $courses_to_enroll[$j] (if the course is exists in the list of courses to subscribe to)
						if ($subscription->account_type == 4 && !$if_subscr_end) {

							$courses_to_enroll[$j]->access_days = $courses_to_enroll[$j]->access_days + $subscription->access_days;
						} elseif($subscription->account_type == 6 && !$if_subscr_end)
						{
							if ($courses_to_enroll[$j]->publish_end)
							{
								if ($courses_to_enroll[$j]->publish_end && ( ($courses_to_enroll[$j]->end_date && (strtotime($courses_to_enroll[$j]->end_date) < strtotime($new_course_item->end_date))) || !$courses_to_enroll[$j]->end_date ) )
								{
									$courses_to_enroll[$j]->end_date = $new_course_item->end_date;
								}
							}
						} else {
							if (!$if_subscr_end)
							{
							if ($courses_to_enroll[$j]->publish_start){
								$courses_to_enroll[$j]->publish_start = $new_course_item->publish_start;
								if ($courses_to_enroll[$j]->publish_start && ( ($courses_to_enroll[$j]->start_date && (strtotime($courses_to_enroll[$j]->start_date) > strtotime($new_course_item->start_date))) || !$courses_to_enroll[$j]->start_date ) ){
									$courses_to_enroll[$j]->start_date = $new_course_item->start_date;
								}
							}
							if ($courses_to_enroll[$j]->publish_end){
								$courses_to_enroll[$j]->publish_end = $new_course_item->publish_end;
								if ($courses_to_enroll[$j]->publish_end && ( ($courses_to_enroll[$j]->end_date && (strtotime($courses_to_enroll[$j]->end_date) < strtotime($new_course_item->end_date))) || !$courses_to_enroll[$j]->end_date ) ){
									$courses_to_enroll[$j]->end_date = $new_course_item->end_date;
								}	
							}
							} else {
								if ($courses_to_enroll[$j]->publish_start){
									$courses_to_enroll[$j]->publish_start = $new_course_item->publish_start;
									if ($subscription->account_type!=6 && ($courses_to_enroll[$j]->publish_start && ( ($courses_to_enroll[$j]->start_date && (strtotime($courses_to_enroll[$j]->start_date) > strtotime($new_course_item->start_date))) || !$courses_to_enroll[$j]->start_date )) ){
										$courses_to_enroll[$j]->start_date = $new_course_item->start_date;
									}
								}
								if ($courses_to_enroll[$j]->publish_end){
									$courses_to_enroll[$j]->publish_end = $new_course_item->publish_end;
									if ($subscription->account_type!=6 && ($courses_to_enroll[$j]->publish_end && ( ($courses_to_enroll[$j]->end_date && (strtotime($courses_to_enroll[$j]->end_date) < strtotime($new_course_item->end_date))) || !$courses_to_enroll[$j]->end_date )) ){
										$courses_to_enroll[$j]->end_date = $new_course_item->end_date;
									}
								}
							}
						}

					}
					$i ++;
				}
			}
		}
		
		//echo '<pre>';
		//print_r($courses_to_enroll);
		//echo '</pre>';
		//die;

		// * * * * * enrollment is here
		foreach ($courses_to_enroll as $public_sub)
		{
			
			//proeducate certificate
			if($JLMS_CONFIG->get('enabled_clear_course_user_data', false)){
				if(isset($public_sub->course_id) && $public_sub->course_id && isset($user_id) && $user_id){
					$CCUD = new JLMS_ClearCourseUserData();
					$CCUD->initializeClearCourseUserData($public_sub->course_id, $user_id);
				}
			}
			//proeducate certificate

			$query = "SELECT * FROM `#__lms_users_in_groups` WHERE course_id = ".$public_sub->course_id." AND user_id = ".$user_id;
			$JLMS_DB->setQuery($query);
			$test = $JLMS_DB->loadObject();
			if (is_object($test)) {
				
				if ($test->publish_start){
					$test->publish_start = $public_sub->publish_start;
					if ($bothdtd && $if_subscr_end)
					{
						if ($test->publish_start)
							$test->start_date = $public_sub->start_date;
					}
					elseif ($test->publish_start && (strtotime($test->start_date) > strtotime($public_sub->start_date) ) && $public_sub->start_date)
					{
						$test->start_date = $public_sub->start_date;
					}

				}
				if ($test->publish_end){
					$test->publish_end = $public_sub->publish_end;
					if ($test->publish_end && (strtotime($test->end_date) < strtotime($public_sub->end_date) ) && $public_sub->end_date){
						$test->end_date = $public_sub->end_date;
					}	
				}

				if ($public_sub->access_days && !$both) {
					if ($test->publish_end) {
						if (strtotime($test->end_date) < strtotime(date('Y-m-d'))) {
							$test->end_date = date('Y-m-d',strtotime(date('Y-m-d')) + intval($public_sub->access_days)*24*60*60);
						} else {
							$test->end_date = date('Y-m-d',strtotime($test->end_date) + intval($public_sub->access_days)*24*60*60);
						}
					} elseif ($test->publish_start) {
						$test->start_date = date('Y-m-d',strtotime($test->start_date) - intval($public_sub->access_days)*24*60*60);
					}
				}

				$query = "UPDATE `#__lms_users_in_groups` SET publish_start = '".$test->publish_start."', start_date = '".$test->start_date."', publish_end = '".$test->publish_end."', end_date = '".$test->end_date."' WHERE course_id = ".$public_sub->course_id." AND user_id = ".$user_id;
				$JLMS_DB->setQuery($query);
				$JLMS_DB->query();			
			
			} else {		
				
				$do_add = false;
				if ($JLMS_CONFIG->get('license_lms_users')) {
					$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
					$JLMS_DB->SetQuery( $query );
					$total_students = $JLMS_DB->LoadResult();
								
					if (intval($total_students) < intval($JLMS_CONFIG->get('license_lms_users'))) {
						$do_add = true;
					}
					if (!$do_add) {
						$query = "SELECT count(*) FROM #__lms_users_in_groups WHERE user_id = '".$user_id."'";
						$JLMS_DB->SetQuery( $query );
						if ($JLMS_DB->LoadResult()) {
							$do_add = true;
						}
					}
				} else {
					$do_add = true;
				}		
				
				$course_info = new stdClass();
				$course_info->course_id = $public_sub->course_id;
			
				if ($do_add && !$if_subscr_end) {						
					if ($public_sub->access_days && !$both) {					
						$course_info->future_course = 0;
						$query = "SELECT params, IF(publish_start = 1, IF(start_date <= '".date('Y-m-d')."', 0, 1), 0) as is_future_course, start_date FROM #__lms_courses WHERE id = ".$course_info->course_id;
						$JLMS_DB->setQuery($query);
						$course_params = '';
						$course_getinfo = $JLMS_DB->LoadObject();
						if (is_object($course_getinfo)) {
							$course_params = $course_getinfo->params;
							$course_info->future_course = $course_getinfo->is_future_course;
							$course_info->start_date = $course_getinfo->start_date;
						}
						$params = new JLMSParameters($course_params);
						$course_info->max_attendees = $params->get('max_attendees', 0);
						if ($course_info->future_course && $public_sub->access_days && (!$public_sub->publish_end || ($public_sub->publish_end && !$public_sub->end_date)) && (!$public_sub->publish_start || ($public_sub->publish_start && !$public_sub->start_date) )) {
							// just an X days access to the future course
							$public_sub->publish_start = 1;
							$public_sub->start_date = $course_info->start_date;
							$public_sub->publish_end = 1;
							$public_sub->end_date = date('Y-m-d', strtotime($public_sub->start_date) + intval($public_sub->access_days)*24*60*60);
							$public_sub->access_days = 0;
						} elseif ($public_sub->access_days) {
							if ($public_sub->publish_end) {
								if ($public_sub->end_date) {
									$public_sub->end_date = date('Y-m-d', strtotime($public_sub->end_date) + intval($public_sub->access_days)*24*60*60);
								} else {
									$public_sub->end_date = date('Y-m-d', strtotime($p_date) + intval($public_sub->access_days)*24*60*60);
									if ($public_sub->publish_start && !$public_sub->start_date) {
										$public_sub->start_date = date('Y-m-d', strtotime($p_date));
									}
								}
							} elseif ($public_sub->publish_start) {
								if ($public_sub->start_date) {
									$public_sub->start_date = date('Y-m-d',strtotime($public_sub->start_date) - intval($public_sub->access_days)*24*60*60);
								} else {
									$public_sub->start_date = date('Y-m-d', strtotime($p_date) - intval($public_sub->start_date)*24*60*60); // hmmmm...... script will never comes here :)
								}
							}
							if ($public_sub->publish_start && !$public_sub->start_date) {
								$public_sub->publish_start = 0;
							}
						}
					}

					$user_info = new stdClass();
					$user_info->user_id = $user_id;
					$user_info->group_id = 0;
					$user_info->course_id = $public_sub->course_id;
					
					$plugin_result_array = array();
					if (is_object($_JLMS_PLUGINS) && method_exists($_JLMS_PLUGINS, 'loadBotGroup')) {
						$_JLMS_PLUGINS->loadBotGroup('course');
						$args = array();
						$args[] = $user_info;
						$args[] = $course_info;
																		
						$plugin_result_array = $_JLMS_PLUGINS->trigger('onCourseJoinAttempt', $args);
					}					
				 

					$allow_join = true;
					if (is_array($plugin_result_array)) {
						foreach ($plugin_result_array as $result) {
							if (!$result) $allow_join = false;
						}
					}

					if ($allow_join) {						
						$query = "INSERT INTO `#__lms_users_in_groups` (course_id, group_id, user_id, publish_start, start_date, publish_end, end_date, enrol_time ) VALUES ('".$public_sub->course_id."', '0', '".$user_id."' ,'".$public_sub->publish_start."' ,'".$public_sub->start_date."','".$public_sub->publish_end."','".$public_sub->end_date."', '".gmdate( 'Y-m-d H:i:s' )."') ";
						$JLMS_DB->setQuery($query);
						if( $JLMS_DB->query() ) 
						{
							$Itemid = $JLMS_CONFIG->get('Itemid');
							$e_course = new stdClass();
							$e_course->course_alias = '';
							$e_course->course_name = '';

							$query = "SELECT course_name, name_alias FROM #__lms_courses WHERE id = '".$public_sub->course_id."'";
							$JLMS_DB->setQuery( $query );
							$e_course = $JLMS_DB->loadObject();

							$query = "SELECT email, name, username FROM #__users WHERE id = '".$user_id."'";
							$JLMS_DB->setQuery( $query );
							$e_user = $JLMS_DB->loadObject();

							$e_params['user_id'] = $user_id;
							$e_params['course_id'] = $public_sub->course_id;
							$e_params['markers']['{email}'] = $e_user->email;
							$e_params['markers']['{name}'] = $e_user->name;
							$e_params['markers']['{username}'] = $e_user->username;
							$e_params['markers']['{coursename}'] = $e_course->course_name;
							$e_params['markers']['{courselink}'] = JLMSEmailRoute("index.php?option=com_joomla_lms&Itemid=$Itemid&task=details_course&id=".$public_sub->course_id);
							$e_params['markers']['{courselink}'] = '<a href="'.$e_params['markers']['{courselink}'].'">'.$e_params['markers']['{courselink}'].'</a>';
							$e_params['action_name'] = 'OnSelfEnrolmentIntoPaidCourse';

							$_JLMS_PLUGINS->loadBotGroup('emails');
							$plugin_result_array = $_JLMS_PLUGINS->trigger('OnSelfEnrolmentIntoPaidCourse', array (& $e_params));
						}
						
						if (is_object($_JLMS_PLUGINS) && method_exists($_JLMS_PLUGINS, 'loadBotGroup')) {
							$_JLMS_PLUGINS->loadBotGroup('user');
							$args = array();
							$args[] = $user_info;
							$_JLMS_PLUGINS->trigger('onCourseJoin', $args);
						}
					} else {
						//user was placed to the course WL.
					}
				}
			}	
			$i++;
		}

		if (!empty($sub_conferences)) {	
			
			$conf_items_str = implode(',',$sub_conferences);
			$query = "SELECT credits FROM #__lmsce_conf_user_credits WHERE user_id = $user_id";
			$JLMS_DB->setQuery($query);
			$user_credits = intval($JLMS_DB->LoadResult());
			$query = "SELECT id, num_credits FROM #__lmsce_conf_subs WHERE id IN ($conf_items_str)";
			$JLMS_DB->setQuery($query);
			$credits_data = $JLMS_DB->LoadObjectList();
			$credits = 0;
			foreach ($sub_conferences as $subc) {
				foreach ($credits_data as $cd) {
					if ($cd->id == $subc) {
						$credits = $credits + $cd->num_credits; break;
					}
				}
			}
			$c_c = $user_credits + $credits;
			$query = "SELECT count(*) FROM #__lmsce_conf_user_credits WHERE user_id = $user_id";
			$JLMS_DB->setQuery($query);
			if ($JLMS_DB->LoadResult()) {				
				$query = "UPDATE #__lmsce_conf_user_credits SET credits = $c_c WHERE user_id = $user_id";
				$JLMS_DB->setQuery($query);
				$JLMS_DB->query();
			} else {			
				$query = "INSERT INTO #__lmsce_conf_user_credits (credits, user_id) VALUES ($c_c, $user_id)";
				$JLMS_DB->setQuery($query);
				$JLMS_DB->query();
			}
		}
	}
}
function JLMS_CART_generateinvoice($item_number, $params_proc, $force_generate = false, $force_email = true) {
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;
	
	$query = "SELECT p.* FROM #__lms_payments as p WHERE p.id=". (int) $item_number;
	$JLMS_DB->setQuery($query);
	$paym_res = $JLMS_DB->loadObjectList();
		
	$course_subs = array();
	$conf_subs = array();
	$payment_type = 0;
	if (count($paym_res)) {			
		$first_payment_id = $paym_res[0]->parent_id?$paym_res[0]->parent_id:$paym_res[0]->id;		
		$payment_type = $paym_res[0]->payment_type;
		if (!$payment_type) {
						
			$query = "SELECT s.sub_name as description, s.price as price, s.discount as discount FROM #__lms_subscriptions as s WHERE s.id = ".$paym_res[0]->sub_id;
			$JLMS_DB->setQuery($query);
			$payment_sub_info = $JLMS_DB->loadObjectList();
			if (count($payment_sub_info)) {
				$paym_res[0]->description = $payment_sub_info[0]->description;
				$paym_res[0]->price = $payment_sub_info[0]->price;
				$paym_res[0]->discount = $payment_sub_info[0]->discount;
			} else {
				$paym_res = null;
			}
		} elseif ($payment_type == 1) {			
			$query = "SELECT s.price as price FROM #__lms_subscriptions_custom as s WHERE s.id = ".$paym_res[0]->sub_id;
			$JLMS_DB->setQuery($query);
			$payment_sub_info = $JLMS_DB->loadObjectList();
			if (count($payment_sub_info)) {
				$paym_res[0]->description = $JLMS_CONFIG->get('custom_subscr_name', 'Custom subscription');
				$paym_res[0]->price = $payment_sub_info[0]->price;
				$paym_res[0]->discount = 0;
			} else {
				$paym_res = null;
			}
		} elseif ($payment_type == 2) {
			$query = "SELECT * FROM `#__lms_payment_items` WHERE payment_id = ".intval($first_payment_id);
			$JLMS_DB->setQuery($query);
			$payment_items = $JLMS_DB->LoadObjectList();
			
			foreach ($payment_items as $pitem) {
				if ($pitem->item_type == 0) {
					$course_subs[] = $pitem->item_id;
				} elseif ($pitem->item_type == 1) {
					$conf_subs[] = $pitem->item_id;
				}
			}
		} else {
			$paym_res = null;
		}
	}
	
	if(count($paym_res)) {
		$do_generate = false;
		if ($force_generate) {
			$do_generate = true;
		} elseif (is_object($params_proc) && method_exists($params_proc, 'get') ) {
			if($params_proc->get('subscr_status') == 2 && $JLMS_CONFIG->get('jlms_subscr_status_email', 0) && strtolower($paym_res[0]->status) == 'completed') {
				$do_generate = true;
			}
		}	

	if($do_generate) {
					
		$print_row = new stdClass();
		$query = "SELECT * FROM #__lms_subscriptions_config";
		$JLMS_DB->setQuery($query);
		$rowz = $JLMS_DB->loadObjectList();
		if(count($rowz)) {
			$print_row = $rowz[0];
		}
		if(!isset($print_row->mail_subj)){
			$print_row->mail_subj = 'Invoice for your order';
		}
		if(!isset($print_row->mail_body)){
			$print_row->mail_body = "Hello. Check the attached file to see detailed information about your purchase.";
		}
		if(!isset($print_row->thanks_text)){
			$print_row->thanks_text = "THANK YOU FOR YOUR BUSINESS";
		}
		if(!isset($print_row->site_name)){
			$print_row->site_name = str_replace("http://",'',$JLMS_CONFIG->get('live_site'));
		}
		if(!isset($print_row->site_descr)){
			$print_row->site_descr = $JLMS_CONFIG->get('sitename');
		}
		if(!isset($print_row->comp_descr)){ $print_row->comp_descr = ''; }
		if(!isset($print_row->comments)){ $print_row->comments = ''; }
		if(!isset($print_row->invoice_descr)){ $print_row->invoice_descr = ''; }
		$print_row->invoice_number = $item_number;		

		$o_vars = get_object_vars($print_row);
		foreach ($o_vars as $ov => $ovv) {
			$print_row->$ov = str_replace('{order_id}', $item_number, $print_row->$ov);
			$print_row->$ov = str_replace('{ORDER_ID}', $item_number, $print_row->$ov);
			//var_dump($print_row);die;
			//$total = number_format($payment->price + $payment->tax_amount, 2, '.', '').''.$JLMS_CONFIG->get('jlms_cur_code');
			//$print_row->$ov = str_replace('{TOTAL}', $total, $print_row->$ov);
			//sprintf("%01.2f",$print_row->quantity * $print_row->price + $print_row->tax_amount)
		}
		require(_JOOMLMS_FRONT_HOME . '/includes/classes/lms.cb_join.php');
		$custom_invoice_fields = $JLMS_CONFIG->get('custom_invoice_fields', array());
		$usr = $paym_res[0]->user_id;
		if (!empty($custom_invoice_fields)) {
			foreach ($custom_invoice_fields as $cif) {
				$fname = $cif->var_name;
				$print_row->{$fname} = JLMSCBJoin::getASSOC($cif->profile_var, $usr);
			}
		} else {
			$def_cb_templ = 'lms_cb_company';
			$print_row->company = JLMSCBJoin::getASSOC($def_cb_templ, $usr);
			$def_cb_templ = 'lms_cb_address';
			$print_row->address = JLMSCBJoin::getASSOC($def_cb_templ, $usr);
			$def_cb_templ = 'lms_cb_city';
			$print_row->city = JLMSCBJoin::getASSOC($def_cb_templ, $usr);
			$def_cb_templ = 'lms_cb_state';
			$print_row->state = JLMSCBJoin::getASSOC($def_cb_templ, $usr);
			$def_cb_templ = 'lms_cb_pcode';
			$print_row->pcode = JLMSCBJoin::getASSOC($def_cb_templ, $usr);
			$def_cb_templ = 'lms_cb_phone';
			$print_row->phone = JLMSCBJoin::getASSOC($def_cb_templ, $usr);
		}
					
		$print_row->tax_amount = $paym_res[0]->tax_amount;
		$print_row->quantity = 1;
		$print_row->name = JLMSCBJoin::getASSOC('name', $usr);
		$print_row->price = $paym_res[0]->amount - $paym_res[0]->tax_amount + $paym_res[0]->tax2_amount;
		$print_row->date = substr($paym_res[0]->date, 0, 10);
		$print_row->shipping = 0;
		$print_row->payment_details = array();
		$print_row->payment_details2 = array();
		
		if ($payment_type == 2) {
						
			$print_row->description = '';

			if (!empty($course_subs)) {
								
				$query = "SELECT id, sub_name, price, account_type, discount FROM #__lms_subscriptions WHERE id IN (".implode(',',$course_subs).")";
				$JLMS_DB->SetQuery($query);
				$course_subs_full = $JLMS_DB->LoadObjectList();				
				if (count($course_subs_full)) {					
					foreach ($course_subs as $course_sub) {
						$new_item = new stdClass();
						$new_item->quantity = 1;
						$new_item->name = '';
						$new_item->unit_price = 0;
						$new_item->price = 0;
						$new_item->id = $course_sub;
						foreach ($course_subs_full as $course_sub_full) {
							if ($course_sub_full->id == $course_sub) {
								$new_item->name = $course_sub_full->sub_name;
								$new_item->price = $course_sub_full->price;
								if ($course_sub_full->account_type == 5) {
									$new_item->price = $course_sub_full->price*(1 - ($course_sub_full->discount/100));
								}
								$new_item->unit_price = $new_item->price;
								$pd_i = 0;
								while ($pd_i < count($print_row->payment_details)) {
									if ($print_row->payment_details[$pd_i]->id == $course_sub) {
										break;
									}
									$pd_i ++;
								}
								if (isset($print_row->payment_details[$pd_i]->id) && $print_row->payment_details[$pd_i]->id == $course_sub) {
									$print_row->payment_details[$pd_i]->quantity ++;
								} else {
									$print_row->payment_details[] = $new_item;
								}
								break;
							}
						}
					}
				}
			}

			if (!empty($conf_subs)) {			
				$query = "SELECT id, sub_name, price FROM #__lmsce_conf_subs WHERE id IN (".implode(',',$conf_subs).")";
				$JLMS_DB->SetQuery($query);
				$conf_subs_full = $JLMS_DB->LoadObjectList();
				if (count($conf_subs_full)) {					
					foreach ($conf_subs as $conf_sub) {
						$new_item = new stdClass();
						$new_item->quantity = 1;
						$new_item->name = '';
						$new_item->unit_price = 0;
						$new_item->price = 0;
						$new_item->id = $conf_sub;
						foreach ($conf_subs_full as $conf_sub_full) {
							if ($conf_sub_full->id == $conf_sub) {
								$new_item->name = $conf_sub_full->sub_name;
								$new_item->unit_price = $conf_sub_full->price;
								$new_item->price = $conf_sub_full->price;
								$pd_i = 0;
								while ($pd_i < count($print_row->payment_details2)) {
									if ($print_row->payment_details2[$pd_i]->id == $conf_sub) {
										break;
									}
									$pd_i ++;
								}
								if (isset($print_row->payment_details2[$pd_i]->id) && $print_row->payment_details2[$pd_i]->id == $conf_sub) {
									$print_row->payment_details2[$pd_i]->quantity ++;
								} else {
									$print_row->payment_details2[] = $new_item;
								}
								break;
							}
						}
					}
				}
			}
		} else {			
			$print_row->description = isset($paym_res[0]->description) ? $paym_res[0]->description : '';
		}
		
		$query = "SELECT count(*) FROM #__lms_subs_invoice WHERE subid = '".$item_number."'";
		$JLMS_DB->setQuery( $query );
		$is_invoice_already_exists =  $JLMS_DB->LoadResult();
					
		if ($is_invoice_already_exists) {			
			//do not generate another invoice and do not send another email with invoice to the user
			if ($force_generate) {			
				require_once(_JOOMLMS_FRONT_HOME.'/includes/classes/lms.pdf_invoice.php');
				$file_path = $JLMS_CONFIG->get('jlms_subscr_invoice_path');
				if ($file_path) {
					$o_vars = get_object_vars($print_row);
					foreach ($o_vars as $ov => $ovv) {
						$total = number_format($print_row->quantity * $print_row->price + $print_row->tax_amount, 2, '.', '').''.$JLMS_CONFIG->get('jlms_cur_code');
						$print_row->$ov = str_replace('{TOTAL}', $total, $print_row->$ov);
					}
					$filenamez = md5(time().$my->id).".pdf";
					$file_path .= "/".$filenamez;
					$my_pdf = new JLMS_PDF_invoice();
					$my_pdf->makeInvoicePDF($print_row, $file_path);

					$query = "UPDATE #__lms_subs_invoice SET filename = '".$filenamez."' WHERE subid = '".$item_number."'";
					$JLMS_DB->setQuery( $query );
					$JLMS_DB->query();
				}
			}
		} else {
			require_once(_JOOMLMS_FRONT_HOME.'/includes/classes/lms.pdf_invoice.php');
			
			$file_path = $JLMS_CONFIG->get('jlms_subscr_invoice_path');//$JLMS_DB->loadResult();
			if ($file_path) {
				$o_vars = get_object_vars($print_row);
				foreach ($o_vars as $ov => $ovv) {
					$total = number_format($print_row->quantity * $print_row->price + $print_row->tax_amount, 2, '.', '').''.$JLMS_CONFIG->get('jlms_cur_code');
					$print_row->$ov = str_replace('{TOTAL}', $total, $print_row->$ov);
				}
							
				$filenamez = md5(time().$my->id).".pdf";
				$file_path .= "/".$filenamez;
				$my_pdf = new JLMS_PDF_invoice();
				$my_pdf->makeInvoicePDF($print_row, $file_path);

				$query = "INSERT INTO #__lms_subs_invoice(subid,filename) VALUES('".$item_number."','".$filenamez."')";
				$JLMS_DB->setQuery( $query );
				$JLMS_DB->query();
				if ($force_email) {
				include(_JOOMLMS_FRONT_HOME.'/joomla_lms.mailbox.php');

				$subject = stripslashes($print_row->mail_subj);
				$body = stripslashes($print_row->mail_body);
				$p_user = $paym_res[0]->user_id;
				$query = "SELECT email FROM #__users WHERE id = ".intval($p_user);
				$JLMS_DB->setQuery( $query );
				$user_email = $JLMS_DB->LoadResult();
				
				JLMS_mosMail( '', '', $user_email, $subject, $body, 0, NULL, NULL, $file_path, 'invoice.pdf' );
				}
			}
		}
	}
	}	
}
?>