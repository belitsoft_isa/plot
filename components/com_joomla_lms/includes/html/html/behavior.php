<?php
/**
* includes/html/html/behaviour.php
* Joomla LMS Component
* HTML library 20.03.2007
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

abstract class JLMS_HTMLBehavior
{

	public static function mootools( $extras = false )
	{
		if( JLMS_J30version() ) 
		{
			JHTML::_('behavior.framework', $extras );
		} else {
			JHTML::_('behavior.mootools');
		}		
	}
}
