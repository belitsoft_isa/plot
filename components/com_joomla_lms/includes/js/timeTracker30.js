var TimeTracker = new Class({
	initialize: function(options){
		this.is_active = 0;
		
		this.interval = options.interval || 1;
		this.interval = this.interval * 1000;
		
		this.ajax_url_handler = options.url_handler || 'index.php';
		this.ajax_method = options.method || 'post';
		
		this.course_id = options.course_id || 0;
		this.user_id = options.user_id || 0;
		
		this.resource_type = options.resource_type || 0;
		this.resource_id = options.resource_id || 0;
		this.item_id = options.item_id || 0;
		this.item_type = 0;
		
		this.item_id_old = null;
		
		this.show_online = (options.show_online!= null) ? options.show_online : 0;
		this.show_online_pulse = (options.show_online_pulse!= null) ? options.show_online_pulse : 0;
		this.show_status = (options.show_status!= null) ? options.show_status : 0;
		
//		this.vposition = options.vposition || 'bottom';
//		this.hposition = options.hposition || 'right';
		
		this.opacity = 1;
		
		this.el_id = null;
		
		this.el_view = null;
		this.el_view_course = null;
		this.el_view_resource = null;
		this.el_online = null;
		this.el_status = null;
		
		this._auto = null;
		
		this.limit_min = options.limit_min || 5;
		this.time_point = new Date().getTime();
		this.delta_time = null;
		
		this.temptimer = null;
		
		this.step_time_point = null;
		this.step_time = null;
		
		this.time_check = 0;
		
		var that = this;
		
		this.el_time_remaining = 'time_remaining';
		if(this.resource_type == 11){
			this.el_time_remaining = 'time_remaining_quiz';
		}
		
		window.document.addEvents({
			'click': this.active_on.bind(this),
			'dblclick': this.active_on.bind(this),
			'mousemove': this.active_on.bind(this),
			'mousewheel': this.active_on.bind(this),
			'scroll': this.active_on.bind(this),
			'keypress': this.active_on.bind(this),
			'resize': this.active_on.bind(this),
			'move': this.active_on.bind(this)
		});
	},
	
	start: function(){
		this.build_view();
		this.unhide_online();
		if(this.show_online){
			this.loop(1);
		}
		this.period();
	},
	
	period: function(){
		clearInterval(this._auto);
		this._auto = this.loop.periodical(this.interval, this);	
	},
	
	stop: function(){
		clearInterval(this._auto);
	},
	
	setresourceid: function(resource_id){
		this.resource_id = resource_id;
	},
	
	getitemid: function(item_id, item_type){
		if(!(item_type!= null)){
			item_type = 0;
		}
		this.item_id_old = this.item_id;
		this.item_id = item_id;
		this.item_type = item_type;
	},
	
	getelid: function(pre_text){
		var elid = '';
		if(this.item_id){
			elid += 'step_'+this.el_time_remaining;
		} else {
			elid += pre_text;
			elid += '_'+this.course_id;
			elid += '_'+this.user_id;
			elid += this.resource_type ? '_'+this.resource_type : '';
			elid += this.resource_id ? '_'+this.resource_id : '';
			//elid += this.item_id ? '_'+this.item_id : '';
		}
		return elid;
	},
	
	build_view: function(){
		if(this.show_online || this.show_dump){
			//this.el_id = this.getelid('tt_view');
			this.el_id = 'tt_view';
			if(($(this.el_id))!= null){
				this.el_view = $(this.el_id);
			} else {
				this.el_view = new Element('div', {
					'id': this.el_id,
					'styles': {
						'position': 'fixed', 
						'display': 'block',
						'right': '15px',
						'bottom': '15px',
						'z-index': 100,
						'width': 'auto'
					}
				}).set( 'html', '');
				$$('body')[0].adopt(this.el_view);
			}
			if((this.el_view)!= null){
				this.el_id = 'tt_view_time';
				if(($(this.el_id))!= null){
					this.el_view_time = $(this.el_id);
				} else {
					this.el_view_time = new Element('div', {
						'id': this.el_id,
						'styles': {
							'position': 'relative', 
							'display': 'block',
							'width': 'auto',
							'float': 'right'
						}
					}).set( 'html', '');
					this.el_view.adopt(this.el_view_time);
				}
				
				this.build_online();
				this.build_status();
			} 
		}
	},
	
	build_online: function(){
		if(this.show_online){
			this.el_id = this.getelid('tt_online');
			if(($(this.el_id))!= null){
				this.el_online = $(this.el_id);
			} else {
				this.el_online = new Element('div', {
					'id': this.el_id,
					'styles': {
						'float': 'right',
						'display': 'block',
						'z-index': 100,
						'width': 'auto',
						'opacity': this.opacity, 
						//'color': '#339966',
						'margin-left': '20px',
						'padding': '0px 10px',
						'text-align': 'center',
						//'border': '2px dotted #cccccc',
						'border-radius': '10px 10px 10px 10px',
						'box-shadow': '0 0 4px 3px'
					}
				}).set( 'html', '');
				if(this.resource_id){
					this.el_online.setStyles({
						'float': 'left',
						'color': '#99CC33'
					});
				}
				this.el_view_time.adopt(this.el_online);
			}
			if((this.el_online)!= null){
				this.online_fx = new Fx.Tween(this.el_online, 'opacity', {duration: 200, transition: Fx.Transitions.linear});
			}
		}
	},
	
	unhide_online: function(){
		if((this.el_online)!= null){
			this.el_online.set( 'html', '').setStyles({'display': 'block'});
		}
	},
	
	hide_online: function(){
		if((this.el_online)!= null){
			this.el_online.set( 'html', '').setStyles({'display': 'none'});
		}
	},
	
	toogle_online: function(){
		if((this.el_online)!= null){
			if(this.el_online.getStyle('display') == 'block'){
				this.hide_online();
			} else {
				this.unhide_online();
			}
		}
	},
	
	build_status: function(){
		if(this.show_status){
			//this.el_id = this.getelid('tt_status');
			this.el_id = 'tt_status';
			if(($(this.el_id))!= null){
				this.el_status = $(this.el_id);
			} else {
				this.el_status = this.el_online.clone()
				.setProperties({
					'id': this.el_id
				})
				.setStyles({
					'margin': 0,
					'padding': '0px 5px'
				}).set( 'html', '');
			}
			this.el_view.adopt(this.el_status);
		}
	},
	
	status: function(){
		if((this.el_status)!= null){
			var status = 'active';
			this.el_status.setStyles({'color': '#cccccc'});
			if(this.is_active){
				//status = 'active';
				this.el_status.setStyles({'color': '#00cc00'});
			}
			this.el_status.set( 'html', status);
		}
	},
	
	active_on: function(){
		this.is_active = 1;
		this.time_point = new Date().getTime();
		this.status();
	},
	
	active_off: function(){
		if(this.is_active && this.show_online_pulse && this.online_fx != null){
			this.online_fx.start(0, this.opacity);
		}
		this.is_active = 0;
		this.status();
	},
	
	requestData: function(data_vars){
		var el_online = this.el_online;
		new Request.HTML({
			url: this.ajax_url_handler,
			method: this.ajax_method,
			async: true,
			update: el_online,
			data: data_vars,
			onRequest: this.stop_time.bind(this),
			onSuccess: this.time.bind(this, 1)
		}).send();
	},
	
	updateElement: function(html){
		if((this.el_online)!= null){
			this.el_online.set('html', html);
		}
	},
	
	loop: function(start){
		if(!(start!= null)){
			start = 0;
		}
		
		if(start){
			this.time_point = new Date().getTime();
		}
		
		this.delta_time = new Date().getTime() - this.time_point;
		
		if(this.delta_time <= (this.limit_min * 60000)){
			this.is_active = 1;
		} else {
			this.is_active = 0;
		}
		if(start || (this.resource_type == 9 && this.item_type == 6)){
			this.is_active = 1;
		}
		var data_vars = 'no_html=1';
		data_vars += '&course_id='+this.course_id;
		data_vars += '&user_id='+this.user_id;
		data_vars += this.resource_type ? '&resource_type='+this.resource_type : '';
		data_vars += this.resource_id ? '&resource_id='+this.resource_id : '';
		data_vars += this.item_id ? '&item_id='+this.item_id : '';
		data_vars += this.item_type ? '&item_type='+this.item_type : '';
		data_vars += '&is_active='+this.is_active;
		data_vars += this.step_time ? '&step_time='+this.step_time : '';
		if(start || this.item_id != this.item_id_old){
			data_vars += '&start=1';
			this.item_id_old = this.item_id;
		}
		this.requestData(data_vars);
		this.step_time = 0;
	},
	
	time: function(s){
		if(!s){
			s = 0;
		}
		if(this.resource_id){
			if(this.item_id){
				
				if(s && this.is_active){
					this.step_time_point = new Date().getTime();
				}
				
				if(new Date().getTime() - this.time_point <= (this.limit_min * 60000)){
					this.step_time_temp = new Date().getTime() - this.step_time_point;
					if(Math.floor(this.step_time_temp / 1000) <= (this.interval / 1000)){
						this.step_time = this.step_time_temp;
					}
				} else {
					this.stop_time();
				}
				
				var step_time_remaining = $('step_'+this.el_time_remaining);
				var time_begin = step_time_remaining.get('text');
				var time_sec = this.helperTimeSec(time_begin);
				
				if(s && this.time_check && time_sec > 0 && time_sec < this.time_check){
					//time_sec = this.time_check;
				}
				
				if(time_sec){
					this.show_timeremaining();
				}
				if(time_sec && this.is_active){
					time_sec = time_sec - 1;
				}
				
				if(!s){
					this.time_check = time_sec;
				}
				var time_show = this.helperTimeNormal(time_sec);
				$('step_'+this.el_time_remaining).set('html', time_show);
				if(!this.is_active){
					this.stop_time();
				}
				if(this.resource_type == 11 && s){
					$('id_quest_time_is_up').set('value', 0);
				}
				if(!time_sec){
					if(this.resource_type == 11){
						$('id_quest_time_is_up').set('value', 1);
						$('step_'+this.el_time_remaining).set('html', 'has expired, the question counted as incorrect');
						this.stop_time();
					} else {
						this.hide_timeremaining();
						this.stop_time();
					}
				} else {
					clearTimeout(this.temptimer);
					this.temptimer = this.time.delay(1000, this);
				}
			} else {
				this.hide_timeremaining();
			}
		}
	},
	
	stop_time: function(){
		if(this.resource_id){
			if(this.item_id){
				clearTimeout(this.temptimer);
			}
		}
	},
	
	hide_timeremaining: function(){
		var time_remainning = $(this.el_time_remaining);
		if((time_remainning)!= null){
			if(time_remainning.getStyle('display') == 'block'){
				time_remainning.setStyle('display', 'none');
			}
		}
	},
	
	show_timeremaining: function(){
		var time_remainning = $(this.el_time_remaining);
		if((time_remainning)!= null){
			if(time_remainning.getStyle('display') == 'none' || time_remainning.getStyle('display') == ''){
				time_remainning.setStyle('display', 'block');
			}
		}
	},
	
	helperTimeNormal: function(sec){
		var time_m = Math.floor(sec / 60);
		var time_s = sec - time_m * 60;
		return this.fn_str_pad(time_m, 2, '0', 'STR_PAD_LEFT')+':'+this.fn_str_pad(time_s, 2, '0', 'STR_PAD_LEFT');
	},
	
	helperTimeSec: function(normal){
		var parts = normal.split(':');
		var sec = 0;
		if(parts.length == 2){
			sec = Number(parts[0]) * 60 + Number(parts[1]);
		}
		return sec;
	},
	
	fn_str_pad: function str_pad (input, pad_length, pad_string, pad_type) {
		var half = '',
			pad_to_go; 
		var str_pad_repeater = function (s, len) {
			var collect = '',
				i;
			 while (collect.length < len) {
				collect += s;
			}
			collect = collect.substr(0, len);
			 return collect;
		};
		
		input += '';
		pad_string = pad_string !== undefined ? pad_string : ' '; 
		if (pad_type != 'STR_PAD_LEFT' && pad_type != 'STR_PAD_RIGHT' && pad_type != 'STR_PAD_BOTH') {
			pad_type = 'STR_PAD_RIGHT';
		}
		if ((pad_to_go = pad_length - input.length) > 0) {
			if (pad_type == 'STR_PAD_LEFT') {
				input = str_pad_repeater(pad_string, pad_to_go) + input;
			} else if (pad_type == 'STR_PAD_RIGHT') {
				input = input + str_pad_repeater(pad_string, pad_to_go);
			} else if (pad_type == 'STR_PAD_BOTH') {
				half = str_pad_repeater(pad_string, Math.ceil(pad_to_go / 2));
				input = half + input + half;
				input = input.substr(0, pad_length);
			}
		} 
		return input;
	}
});