<?php

// incoming data from scormfunctions.js : $score - $max - $min - $lesson_status - $time
global $my, $database, $Itemid;
global $JLMS_SESSION;

$lesson_status = mosGetParam($_GET, 'lesson_status', '');
$score = mosGetParam($_GET, 'score', '');
$time = mosGetParam($_GET, 'time', '');
$my_sco_identifier = mosGetParam($_GET, 'sco_identifier', '');
$contentId = $JLMS_SESSION->get('content_id');
$items = $JLMS_SESSION->get('items');
$origin = mosGetParam($_GET ,'origin', '');
$max = mosGetParam($_GET, 'max', '');
$min = mosGetParam($_GET, 'min', '');
$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
$scorm_id = intval(mosGetParam($_REQUEST, 'id', 0));
$scorm_info = JLMS_GetSCORM_info( $scorm_id );
if (!$scorm_info) {
	mosErrorAlert('Bad SCORM identifier');
}
$file = $scorm_info->file_info;

$array_status=array(
					'completed' => 'Scorm Completed',
					'passed' => 'Scorm Passed',
					'failed' => 'Scorm Failed',
					'incomplete' => 'Scorm Incomplete',
					'not attempted' => 'Scorm Not Attempted'
);
//save this sco-id in session so that we can check what last sco has been closed
$JLMS_SESSION->set('last_sco_closed', $my_sco_identifier);
// check $lesson_status validity
if (!$lesson_status) {
	$lesson_status = 'incomplete';
}
//prepare a small reverse-index of "clusterinfo"=>"item index"
$items_clusterinfo_dictionary = array();
foreach($items as $key=>$content){
	$items_clusterinfo_dictionary[$content['clusterinfo']] = $key;
}
if (!$JLMS_SESSION->get('dont_save_last')) {
	//check if there are dependent clusters
	$sub_cluster_completed = true;
	$dict = $JLMS_SESSION->get('items_dictionary');
	$base_child_cluster = $items[$dict[$my_sco_identifier]]['clusterinfo'] * 10;
	for ($i = $base_child_cluster; $i < ($base_child_cluster + 10); $i++){
		$j = isset($items_clusterinfo_dictionary[$i]) ? $items_clusterinfo_dictionary[$i] : null;
		if (isset($items[$j]) && is_array($items[$j])){
			$sub_lesson_status = JLMS_GetSCO_status($my->id, $contentId, $items[$j]['identifier']);
/*			$query = "SELECT status FROM #__lms_scorm_sco WHERE content_id = '".$contentId."' AND user_id = '".$my->id."'"
			. "\n AND sco_identifier = '".$items[$j]['identifier']."'";
			$database->SetQuery( $query );
			$sub_lesson_status = $database->LoadResult();*/
			if ((($sub_lesson_status) != 'completed') && ($sub_lesson_status != 'passed' )) {
				$sub_cluster_completed = false;
			}
		}
	}
	if($sub_cluster_completed){
		//error_log("Element $my_sco_identifier has no incomplete children, change status to $lesson_status",0);
		JLMS_UpdateSCO($my->id, $contentId, $my_sco_identifier, $lesson_status, $score, $time);
		/*$query = "UPDATE #__lms_scorm_sco SET score = '".$score."', status = '".$lesson_status."', sco_time = '".$time."'"
		. "\n WHERE user_id = '".$my->id."' and sco_identifier = '".$my_sco_identifier."' AND content_id = '".$contentId."'";
		$database->SetQuery( $query );
		$database->query();*/
	} else {
		//error_log("Element $my_sco_identifier has incomplete children, set status to incomplete)",0);
		JLMS_UpdateSCO($my->id, $contentId, $my_sco_identifier, 'incomplete', $score, $time);
		/*$query = "UPDATE #__lms_scorm_sco SET score = '".$score."', status = 'incomplete', sco_time = '".$time."'"
		. "\n WHERE user_id = '".$my->id."' and sco_identifier = '".$my_sco_identifier."' AND content_id = '".$contentId."'";
		$database->SetQuery( $query );
		$database->query();*/
	}
} else {
	//error_log("Element $my_sco_identifier cannot be saved (restart conditions preventing it)",0);
}
/*==================================================================================
  SEARCHING FOR COMPLETE CLUSTERS AND IF ANY, UPDATE THEM TO COMPLETED IF NECCESSARY
  ==================================================================================*/

//if ($lesson_status=='completed') {
$clustercompleted = true;
//$i=0; //get to the current element
$it_dict = $JLMS_SESSION->get('items_dictionary');
$i = $it_dict[$my_sco_identifier];
//do {
//  $i++;
//} while (($items[$i]['identifier'] != $my_sco_identifier) and ($i <= count($items)));
// now $i is the index of the current element
//$items[$i]['status'] = $lesson_status;
//api_session_unregister($items);
//api_session_register($items);


//now check if the current element completes the cluster
//a. get the parent's clusterinfo (if clusterinfo = 23, parent is 2)
$startcluster = floor(($items[$i]['clusterinfo'])/10);
//b. get through all elements of the same cluster (all elements with clusterinfo of 2* - max 10 elems)
for ($cluster=($startcluster*10); (($cluster<=($startcluster*10+9)) && ($clustercompleted==true)); $cluster++) {
	$i = (!empty($items_clusterinfo_dictionary[$cluster])?$items_clusterinfo_dictionary[$cluster]:0);
	$id = isset($items[$i]['identifier'])?$items[$i]['identifier']:'';
	$cluster_lesson_status = JLMS_GetSCO_status($my->id, $contentId, $id);
	/*$query = "SELECT status FROM #__lms_scorm_sco WHERE content_id = '".$contentId."'"
	. "\n AND user_id = '".$my->id."' AND sco_identifier = '".$id."'";
	$database->SetQuery( $query );
	$cluster_lesson_status = $database->LoadResult();*/
	if ((isset($items[$i]) && is_array($items[$i])) && ((($cluster_lesson_status) != 'completed') && ($cluster_lesson_status != 'passed' ))) { $clustercompleted = false; }
}

if ($clustercompleted) { //if every sub-element of this cluster was completed
	$i = (!empty($items_clusterinfo_dictionary[$startcluster])?$items_clusterinfo_dictionary[$startcluster]:0);	
	if (isset($items[$i]['identifier'])) {
		$my_sco_identifier = $items[$i]['identifier'];
		JLMS_UpdateSCO($my->id, $contentId, $my_sco_identifier, 'completed');
		/*$query = "UPDATE #__lms_scorm_sco SET status = 'completed' WHERE user_id = '".$my->id."'"
		. "\n AND sco_identifier = '".$my_sco_identifier."' AND content_id = '".$contentId."'";
		$database->SetQuery( $query );
		$database->query();*/
	}
} else { // if at least one element was not completed
	$i = (!empty($items_clusterinfo_dictionary[$startcluster])?$items_clusterinfo_dictionary[$startcluster]:0);	
	$my_sco_identifier = isset($items[$i]['identifier'])?$items[$i]['identifier']:'';
	if ($my_sco_identifier) {
		JLMS_UpdateSCO($my->id, $contentId, $my_sco_identifier, 'incomplete');
		/*$query = "UPDATE #__lms_scorm_sco SET status = 'incomplete' WHERE user_id = '".$my->id."'"
		. "\n AND sco_identifier = '".$my_sco_identifier."' AND content_id = '".$contentId."'";
		$database->SetQuery( $query );
		$database->query();*/
	}
}

//$origin can be 'terminate', 'finish' or 'commit'

/*=============================================
  SENDING MESSAGE ABOUT STATUS TO MESSAGE FRAME
  =============================================*/
?>
<html><head><link rel='stylesheet' type='text/css' href='../css/scorm.css'>
<script type='text/javascript'>
/* <![CDATA[ */
function reloadcontents() {
	newloc=addtime(parent.contents.document.location);
	top.contents.document.location=newloc; //in order to refresh
}
function addtime(input) {
	rnd=Math.random()*100;
	if (input.toString().indexOf("#")==-1) {
		newstring=input+'&rnd='+rnd;
		return(newstring);
	} else {
		t=input.toString().indexOf("#");
		sub1=input.toString().substr(0,t);
		sub2=input.toString().substr(t,input.length);
		newstring=sub1+'&rnd='+rnd+sub2;
		return(newstring);
	}
}
/* ]]> */
</script>
</head><body onload='javascript:reloadcontents();'>

<?php
if (($lesson_status=='completed') or ($lesson_status=='passed'))
{ 
	//echo "<img src='../img/right.gif' alt='right'>";
	echo "<div class='message'>"
				.'ScormThisStatus'." : ".$array_status[$lesson_status]
			."	</div>"; 
}
else 
{ 
	//echo "<img src='../img/wrong.gif' alt='wrong'>";
	echo "<div class='message'>"
				.'ScormThisStatus'." : ".$array_status[$lesson_status]
			."</div>";
}
echo "</body></html>";
?>