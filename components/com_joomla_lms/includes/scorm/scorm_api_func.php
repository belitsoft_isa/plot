<?php
/**
*	API event handler functions for Scorm 1.1, 1.2 and 1.3
*/
global $my, $database, $JLMS_SESSION, $JLMS_CONFIG;
$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
$scorm_id = intval(mosGetParam($_REQUEST, 'id', 0));
$scorm_info = JLMS_GetSCORM_info( $scorm_id );
if (!$scorm_info) {
	mosErrorAlert('Bad SCORM identifier');
}
$old_s_identifier = $JLMS_SESSION->get('old_sco_identifier');
$my_s_identifier = $JLMS_SESSION->get('s_identifier');
$file = $scorm_info->file_info;

//in some cases (manual clicks), there is no "old" s_identifier because there is no "new" one.
if(empty($old_s_identifier)){
	$old_s_identifier = $my_s_identifier;
}
?>
<html><head><script type='text/javascript'>
/* <![CDATA[ */
var alerts=0; //debug output level. 0 = none, 1=light, 2=a lot, 3(not implemented)=all
if (alerts>1) { alert('scormfunctions.php included'); }

function APIobject() {
  this.LMSInitialize=LMSInitialize;  //for Scorm 1.2
  this.Initialize=LMSInitialize;     //for Scorm 1.3
  this.LMSGetValue=LMSGetValue;
  this.GetValue=LMSGetValue;
  this.LMSSetValue=LMSSetValue;
  this.SetValue=LMSSetValue;
  this.LMSCommit=LMSCommit;
  this.Commit=LMSCommit;
  this.LMSFinish=LMSFinish;
  this.Finish=LMSFinish;
  this.LMSGetLastError=LMSGetLastError;
  this.GetLastError=LMSGetLastError;
  this.LMSGetErrorString=LMSGetErrorString;
  this.GetErrorString=LMSGetErrorString;
  this.LMSGetDiagnostic=LMSGetDiagnostic;
  this.GetDiagnostic=LMSGetDiagnostic;
  this.Terminate=Terminate;  //only in Scorm 1.3
}

//it is not sure that the scos use the above declarations

API = new APIobject(); //for scrom 1.2
api = new APIobject(); //for scrom 1.2
API_1484_11 = new APIobject();  //for scrom 1.3
api_1484_11 = new APIobject();  //for scrom 1.3

var G_NoError = 0;
var G_GeneralException = 101;
var G_ServerBusy = 102;
var G_InvalidArgumentError = 201;
var G_ElementCannotHaveChildren = 202;
var G_ElementIsNotAnArray = 203;
var G_NotInitialized = 301;
var G_NotImplementedError = 401;
var G_InvalidSetValue = 402;
var G_ElementIsReadOnly = 403;
var G_ElementIsWriteOnly = 404;
var G_IncorrectDataType = 405;

var G_LastError = G_NoError ;

var commit = false ;

var score=0;
var max=0;
var min=0;
var lesson_status='';
var session_time=0;

function LMSInitialize() {  //this is the initialize function of all APIobjects
  if (alerts>0) { alert('LMSInitialise() called (by SCORM content)'); }
  //initialise the lesson status between two lessons, to avoid status override
  lesson_status = '';
  return('true');
}

function Initialize() {  //this is the initialize function of all APIobjects
  return LMSInitialize();
}


function LMSGetValue(param) {
	var result;
	switch(param) {
	case 'cmi.core._children'		:
	case 'cmi.core_children'		:
result='entry, exit, lesson_status, student_id, student_name, lesson_location, total_time, credit, lesson_mode, score, session_time';		break;
	case 'cmi.core.entry'			: result='';		break;
	case 'cmi.core.exit'			: result='';		break;
	case 'cmi.core.lesson_status'	: 
    if(lesson_status != '') {result=lesson_status;}
    else{<?php
		$query = "SELECT status FROM #__lms_scorm_sco WHERE user_id = '".$my->id."' AND sco_identifier = '".$my_s_identifier."'";
		$database->SetQuery( $query );
		$status = $database->LoadResult();
		if (!$status) {$status = "not attempted";} 
        echo " result='$status';";?>
        } 
    break;
	case 'cmi.core.student_id'	   : <?php echo "result='$my->id';"; ?> break;
	case 'cmi.core.student_name'	: 
	  <?php
		$who = $my->username." (".$my->name.")";
	    	echo "{ result='$who'; }"; 
	  ?>	break;
	case 'cmi.core.lesson_location'	: result='';		break;
	case 'cmi.core.total_time'	: result='0000:00:00.00';break;
	case 'cmi.core.score._children'	: result='raw,min,max';	break;
	case 'cmi.core.score.raw'	: result=score;		break;
	case 'cmi.core.score.max'	: result='100';		break;
	case 'cmi.core.score.min'	: result='0';		break;
	case 'cmi.core.score'		: result='0';		break;
	case 'cmi.core.credit'		: result='credit';	break;
	case 'cmi.core.lesson_mode'	: result='normal';	break;
	case 'cmi.suspend_data'		: result='';		break;
	case 'cmi.launch_data'		: result='';		break;
	case 'cmi.objectives._count'	: result='0';		break;
	default : 			  result='';		break;
	}
    	if (alerts>0) { alert("SCORM calls LMSGetValue('"+param+"')\nReturned '"+result+"'"); }
	return result;
}

function GetValue(param) {
	return LMSGetValue(param);
}

function LMSSetValue(param, val) {
    if (alerts>0) { alert("SCORM calls LMSSetValue('"+param+"','"+val+"')"); }
	switch(param) {
	case 'cmi.core.score.raw'		: score= val ;			break;
	case 'cmi.core.score.max'		: max = val;			break;
	case 'cmi.core.score.min'		: min = val;			break;
	case 'cmi.core.lesson_status'	: lesson_status = val;	break;
	case 'cmi.completion_status'	: lesson_status = val;	break; //1.3
	case 'cmi.core.session_time'	: session_time = val;	break;
	case 'cmi.score.scaled'			: score = val ;			break; //1.3
	case 'cmi.success_status'		: success_status = val; break; //1.3
	}
	return(true);
}

function SetValue(param, val) {
	return LMSSetValue(param, val);
}

function savedata(origin) { //origin can be 'commit', 'finish' or 'terminate'
    param = 'origin='+origin+'&score='+score+'&max='+max+'&min='+min+'&lesson_status='+lesson_status+'&time='+session_time;
	url = "<?php echo $JLMS_CONFIG->get('live_site');?>/index.php?tmpl=component&option=com_joomla_lms&Itemid=37&task=scorm_closesco&<?php echo "course_id=$course_id&id=$scorm_id&sco_identifier=$old_s_identifier&"; ?>" + param + "";
    scowindow=open(url,'message');
    if (alerts>1) { alert('saving data : '+url); }
}

function LMSCommit(val) {
    if (alerts>0) { alert('LMSCommit() called'); }
	commit = true ;
	savedata('commit');
	return('true');
}

function Commit(val) {
	return LMSCommit(val);
}

function LMSFinish(val) {
  if (( commit == false ) && (alerts>0)) { alert('LMSFinish() called without LMSCommit()'); }
	if ( commit == true ) {
   if(alerts>0) { alert('LMSFinish() called');}
		savedata('finish');
	}
	return('true');
}

function Finish(val) {
	return LMSFinish(val);
}

function LMSGetLastError() {
    if (alerts>1) { alert('LMSGetLastError() called'); }
	return(G_LastError);
}

function GetLastError() {
	return LMSGetLastError();
}

function LMSGetErrorString(errCode){
    if (alerts>1) { alert('LMSGetErrorString() called'); }
	return('No error !');
}

function GetErrorString(errCode){
	return LMSGetErrorString(errCode);
}

function LMSGetDiagnostic(errCode){
    if (alerts>1) { alert('LMSGetDiagnostic() called'); }
	return(API.LMSGetLastError());
}

function GetDiagnostic(errCode){
	return LMSGetDiagnostic(errCode);
}

function Terminate(){
	if (alerts>0) { alert('Terminate() called'); }
	commit = true;
	savedata('terminate');
	return (true);
}
/* ]]> */
</script>
</head>
<body><i><b>This is an API / API_1484_11 system window. Nothing to be worried about!</b></i></body></html>
