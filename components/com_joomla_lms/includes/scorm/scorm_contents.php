<?php
/**
============================================================================== 
*	Show the table of contents of a scorm-based tutorial
*
* This script is a double-pass script. When called, it initialises stuff
* (generally depending on $menu or $openfirst parameters), then calls
* the opensco.php script which opens the content in another frame and
* calls this script back without any action parameter.
*	@author   Denes Nagy <darkden@freemail.hu>
*	@author   Yannick Warnier <yannick.warnier@dokeos.com>
*	@access   public
*
*	@package dokeos.scorm
*
============================================================================== 
*/

//Initialisation includes
global $my, $database, $Itemid, $JLMS_CONFIG;
global $JLMS_SESSION;
include(_JOOMLMS_FRONT_HOME . "/includes/scormparsing.lib.php");
//error_log($_SERVER['REQUEST_URI']."---menu=".$_POST['menu'],0);
//error_log("---Script started. s_identifier is now:".$_SESSION['s_identifier']." and old s_identifier is now:".$_SESSION['old_sco_identifier'],0);
$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
$scorm_id = intval(mosGetParam($_REQUEST, 'id', 0));
$scorm_info = JLMS_GetSCORM_info( $scorm_id );
if (!$scorm_info) {
	mosErrorAlert('Bad SCORM identifier');
}
$edoceo = $scorm_info->is_escorm?'yes':'no';
$file = $scorm_info->file_info;
$openDir = $scorm_info->openDir_info;
//Parameters reception
#$edoceo	= strval(mosGetParam($_GET, 'edoceo', 'no'));
#$file = 'http://192.168.0.201/lms_scorm/0064_76ba30608e36954fcfc8c7ff2900d8cf/imsmanifest.xml';
#$openDir = '0064_76ba30608e36954fcfc8c7ff2900d8cf';
$menu = strval(mosGetParam($_REQUEST, 'menu' ,''));
$openfirst = strval(mosGetParam($_REQUEST, 'openfirst', 'no'));
$request_file = $file;
$sco_href_id = '';
// Check if the requested file is in the right location (scorm folder in course directory and no .. in the path)
/*$file_path = 'http://192.168.0.201/lms_scorm/'.$openDir;//api_get_path(SYS_COURSE_PATH).$_course['path'].'/scorm';
if(substr($request_file,0,strlen($file_path)) != $file_path || strpos($request_file,'..') > 0) {
	echo "<script> alert('Access to this location denied'); window.history.go(-1); </script>\n";
	exit();
//	api_not_allowed();
}*/
//$s_identifier		= $_SESSION['s_identifier'];
#$_uid = $my->id;//					= $_SESSION['_uid'];

//Charset settings (very important for imported contents)
#$charset = GetXMLEncode($_GET['file']);
#header('Content-Type: text/html; charset='. $charset);
//The following charset describes the encoding of most language files
//For further enhancements and in order to ensure internationalisation, we
//should make every language file UTF-8, or adapt get_lang to be able to sort
//out the file encoding and use htmlentities if required.
//If using unicode characters (courses in non-european alphabets), change this
//to $charset_lang = 'UTF-8';
$charset_lang = 'ISO-8859-1';

//Latest language inits
$array_status=array(
					'completed' => 'Scorm Completed',
					'passed' => 'Scorm Passed',
					'failed' => 'Scorm Failed',
					'incomplete' => 'Scorm Incomplete',
					'not attempted' => 'Scorm Not Attempted'
);


//Database table names init

//if the last sco visited hasn't been closed, mark it complete:
if ($JLMS_SESSION->has('last_sco_closed') && $JLMS_SESSION->has('old_sco_identifier') && $JLMS_SESSION->get('last_sco_closed') && $JLMS_SESSION->get('old_sco_identifier') ) {
	if($JLMS_SESSION->get('last_sco_closed')  != $JLMS_SESSION->get('old_sco_identifier') ){
		$sco_status = JLMS_GetSCO_status($my->id, $JLMS_SESSION->get('content_id'), $JLMS_SESSION->get('old_sco_identifier'));
		/*$query = "SELECT status FROM #__lms_scorm_sco WHERE content_id = '".$JLMS_SESSION->get('content_id')."'"
		. "\n AND sco_identifier = '".$JLMS_SESSION->get('old_sco_identifier')."'"
		. "\n AND user_id = '".$my->id."'";
		$database->SetQuery( $query );
		$sco_status = $database->LoadResult();*/
		if ($sco_status == 'not attempted') {
			JLMS_UpdateSCO($my->id, $JLMS_SESSION->get('content_id'), $JLMS_SESSION->get('old_sco_identifier'), 'completed', '0', '00:00');
			/*$query = "UPDATE #__lms_scorm_sco SET score = '0', status = 'completed', time = '00:00'"
			. "\n WHERE (user_id = '".$my->id."' AND sco_identifier = '".$JLMS_SESSION->get('old_sco_identifier')."'"
			. "\n AND content_id = '".$JLMS_SESSION->get('content_id')."'";
			$database->SetQuery( $query );
			$database->query();*/
			$old_identif = $JLMS_SESSION->get('old_sco_identifier');
			$JLMS_SESSION->set('last_sco_closed', $old_identif);
		}
	}
}

//Backup latest SCO-identifier opened (to use as reference to the last element seen in closesco.php) 
//This operation needs only to be done when the user takes an action, not on automatic reloading
//of the script. This is checked with the $menu parameter (set on user actions)

# (DEN) AXTUNG (po etomu commentu syuda nugno vstavit' "if ($menu) {"
# (DEN) a moget i ne nado :)
//if ($menu) {
	$s_identif = $JLMS_SESSION->get('s_identifier');
	$JLMS_SESSION->set('old_sco_identifier', $s_identif);
//}

//Special condition to avoid buggy database refresh in case of restart (see closesco.php)
if ($JLMS_SESSION->get('just_started') == true) {
	$JLMS_SESSION->set('dont_save_last', true);
	$JLMS_SESSION->set('just_started', false);
} else {
	$JLMS_SESSION->set('dont_save_last', false);
}

//Ensure the items array (TOC) and other session variables are really erased on first visit or on restart
if ( ($openfirst == 'yes') || ($menu == 'restart') ){
	//error_log("Openfirst or restart was set (unset session[s_identifier] and session[old_sco_identifier]",3,'C:\e.txt');
	//api_session_unregister('items');
	$JLMS_SESSION->set('items', array());
	$items = array();
	$JLMS_SESSION->set('items_dictionary', array());
	$JLMS_SESSION->set('charset_lang', 'ISO-8859-1');
	$items_dictionary = array();
	$JLMS_SESSION->set('defaultorgtitle', '');
	$defaultorgtitle = '';
	$JLMS_SESSION->set('content_id', '');
	if($menu == 'restart'){
		$JLMS_SESSION->set('just_started', true);
	}
	$JLMS_SESSION->clear('s_identifier');
	$JLMS_SESSION->clear('sco_identifier');
	$JLMS_SESSION->clear('old_sco_identifier');
} else {
	$items =  $JLMS_SESSION->get('items');
	$defaultorgtitle =  $JLMS_SESSION->get('defaultorgtitle');
	if(!empty($menu)){
		$JLMS_SESSION->set('just_started', false);
	}
}
$items_dictionary = $JLMS_SESSION->get('items_dictionary', array());
$charset_lang = $JLMS_SESSION->get('charset_lang', 'ISO-8859-1');
$output = '<html><head></head><body>';

//incoming variables :
//- $request_file
//- $tabledraw -> if true, then the program draws a help table for showing imsmanifest.xml tags (technical)

// More init (display tweaks)
$tabledraw = false;  //if true, then technical information is shown
$wrap = true; //if false then the toc is not in a table, if true, then it is
$tablewidth = 255;  //this is the width of the content tables

$defaultorgref = '';
$version = '0.0';
$inorg = false; //whether we are inside an organisation or not
$intitle = $inmeta = $ingeneral = false;
$inversion = false;
$prereq = false;
$initem = 0; //how deep we are in the hieracrchy of itmes
$itemindex = 0;
$previouslevel = 1;
$clusterinfo = 0;

$exit_after_menu = false; //used to disable printing of javascript redirections in output.

/**
 * This function displays a message in the message frame.
 *
 * For some reason (apparently JavaScript reasons), the message shown 
 * cannot be more than 66 characters. Together with the enveloppe, it 
 * gets to something around 250 characters, which apparently is too 
 * big for the "write" method 
 * @param		string	The message to show. Should be smaller than 66 characters.
 * @return void	Doesn't return anything, just writes to the message frame.
 */
function message($text) {
	//ugly little patch for text encoding of accentuated characters - see other comments on 'accentuated' characters
	// and improvements to this patch below in this file
	#global $charset_lang;
	#$text = htmlentities($text,ENT_QUOTES,$charset_lang);
	$string = "<script type='text/javascript'>\n/* <![CDATA[ */\n".
		"zwindow=open('','message');".
		"s='<html><head></head><body style=\'background-color:transparent\'>".
		"<div class=\"message\">$text</div></body></html>';".
		"z=zwindow.document;".
		"z.write(s);".
		"z.close();".
		"\n/* ]]> */\n</script>";
	return $string;
}



/****************************************** PROCESSING **********************************/
//Real processing starts here

//1. Check whether we need to build the TOC ($items array) or if it already exists
if ( ($openfirst=='yes') || ($menu=='restart') ){
	//error_log("Openfirst or restart was set (generate $items)",0);

	/*====================
	  PARSING THE XML FILE
	  ====================*/
	  // 0.0 pre steps
		if ($fp1 = fopen($request_file, "r")) {
			$fline = fgets($fp1);
			//fseek($fp1,0);
			$match = array();
			preg_match('/encoding="([0-9a-zA-Z-]*)"/i', $fline, $match); //find quoted encoding
			if (isset($match[1]) && $match[1] != "ISO-8859-1") {
				$charset_lang = $match[1];
				$JLMS_SESSION->set('charset_lang', $charset_lang);
			}
		}
	  
	//1.1
	if (!(list($xml_parser, $fp) = new_xml_parser($request_file))) {
	   die("<font color='red'>Error : could not open XML input for SCORM package $scorm_id in ($request_file)</font>");
	}

	if ($tabledraw) {
	  $output .= "<table border='0'>"
			."<tr>"
				."<td>Row</td>"
				."<td>Inorg</td>"
				."<td>Initem</td>"
				."<td>Tag name</td>"
				."<td>Attributes</td>"
			."</tr>";
	}

	while ($data = fread($fp, 4096)) {  //reads the file in 4096 byte amounts
	   if (!xml_parse($xml_parser, $data, feof($fp))) {
		   die(sprintf("XML error: %s at line %d\n",
					   xml_error_string(xml_get_error_code($xml_parser)),
					   xml_get_current_line_number($xml_parser)));
	   }
	}
	if ($tabledraw) {
	  $output .= "</table>";  
	  $output .= "parse complete<br /><hr />";  
	  $output .= "<br />Total lines in xml file : ";
	  $output .= xml_get_current_line_number($xml_parser);  
	  $output .= " (xml_get_current_line_number)";  
	  $output .= "<br /><br />";
	}

	//build an index of items (item identifier => item index in $items). This should improve parsing speed.
	foreach($items as $key => $content){
		$items_dictionary[$content['identifier']] = $key;
	}
	$JLMS_SESSION->set('items_dictionary', $items_dictionary);
	
	//As we asked to start from the beginning, set the current identifier to the first elem's
	$i = 1;
	while (isset($items[$i]) && ($items[$i]['href']=='')) { $i ++;}
//	for(;$items[$i]['href']=='';$i++){/*do nothing (get the index of the first item of href<>'')*/}
	$JLMS_SESSION->set('s_identifier', $items[$i]['identifier']);
	$sco_href_id = $i;
	$href = $JLMS_CONFIG->get('live_site') . "/" . _JOOMLMS_SCORM_FOLDER . "/".$openDir."/".$items[$i]['href'];
}


//2. Compute the maximum depth level ot the TOC.
//		For fixed width : width=$tablewidth (in two tables) and enable nbsp substitution in two places
// 	$items is a result of the imsmanifest.xml parsing
$maxlevel = 1;
$i = 1;

while (isset($items[$i]) && $items[$i]) {
	if (($items[$i]['level']) > $maxlevel) { $maxlevel = $items[$i]['level']; }
	$i++;
}



//3. Check if the content was ever opened or not, 
//			If not, add a line to scorm_main with the title and course code to record its existence
/*$sql = "SELECT dokeosCourse FROM $TBL_SCORM_MAIN WHERE "
			." (contentTitle='".$openDir."' and dokeosCourse='".$_course['official_code']."')";
$result = api_sql_query($sql,__FILE__,__LINE__);
$numrows = mysql_num_rows($result);

if ($numrows==0) { //this means that this Scorm content was never opened by anyone in this dokeos course
	//$result = api_sql_query("SELECT MAX(contentId) FROM $TBL_SCORM_MAIN");
	//$ar = mysql_fetch_array($result);
	//$maxcontentId = $ar['MAX(contentId)']+1;
	$sql = "INSERT INTO $TBL_SCORM_MAIN (contentTitle, dokeosCourse) VALUES "
			." ('".$openDir."','".$_course['official_code']."')";
	$result = api_sql_query($sql,__FILE__,__LINE__);
}*/

//4. Get the contentId for this content (whether it has just been inserted or not) to check if the
//			current user ever opened it.
/*$sql = "SELECT contentId FROM $TBL_SCORM_MAIN "
        ." WHERE (contentTitle='".$openDir."' and dokeosCourse='".$_course['official_code']."')";

$result = api_sql_query($sql,__FILE__,__LINE__);
$ar = mysql_fetch_array($result);
$mycontentId = $ar['contentId']; //it has to exist, because the previous step checked it existed and if not inserted it
$_SESSION['contentId'] = $mycontentId;
*/
$mycontentId = $scorm_id;
$JLMS_SESSION->set('content_id', $mycontentId);
$everopened = true;
$numrows2 = 0; 
$result2 = '';
$query = "SELECT count(*) FROM #__lms_scorm_sco WHERE user_id = '".$my->id."' AND content_id = '".$mycontentId."'";
$database->SetQuery( $query );
$numrows2 = $database->LoadResult();
//error_log("mycontentId = $mycontentId", 3, 'C:\e.txt');
if (!$numrows2) { //this means that this Scorm content was not opened by the current student
	$everopened = false;
}


/*if($openfirst=='yes' or $menu='restart'){
	$s_identifier = $items[1]['identifier'];
	$href=api_get_path('WEB_COURSE_PATH').$_course['path']."/scorm".$openDir."/".$items[1]['href'];
	$output .= "<script type='text/javascript'>",
		"nextwindow=open('opensco.php?sco_href=$href&sco_identifier=$s_identifier&file=$request_file&edoceo=$edoceo&openDir=$openDir','message');",
		"</script>";
}*/



/*=========================
  IF RESTART WAS CLICKED...
  =========================*/
//5 If the user clicked on "restart", the $items array has been cleaned 
// and no element has any special mark whatsoever, except the first one which is highlighted
if ( $menu == 'restart' ) { //Restrart clicked
	//error_log("Restart was set...",0);
	if ( $version == '1.1' ) {
		//5.1.a Ignore if version of SCORM is <1.2 (or that's basically what is intended)
		//display warning message in message frame (side-menu bottom)

		//comment 11.12.2006
		#$output .= '(DEN) MESSAGE = ScormNoStatus';//message(htmlentities(get_lang('ScormNoStatus'),ENT_QUOTE,$charset_lang)); 
	} else {
		//error_log("Update database for restart",0);
		//5.1.b.1 Loads a blank page in the "sco" frame, probably to enable scorm API reinit
		/*
		//$output .= "<script type='text/javascript'>
		//	xwindow=open('about:blank','sco');
		//	</script>";
		*/
		
		//5.1.b.2 update the status for the current user and content in the SCORM data table
		#JLMS_UpdateSCO($my->id, $mycontentId, $sco_identifier, $status, $score = null, $sco_time = null)
		# (DEN) For all $sco_identifier's ??
		$query = "UPDATE #__lms_scorm_sco SET score = '0', status = 'not attempted', sco_time = '00:00'"
		. "\n WHERE user_id = '".$my->id."' AND content_id = '".$mycontentId."'";
		$database->SetQuery( $query );
		$database->query();

		//5.1.b.3 clean session vars about this content
		$JLMS_SESSION->clear('s_href');
		$i = 1;
		while (isset($items[$i]) && ($items[$i]['href']=='')) { $i ++;}
		$JLMS_SESSION->set('s_identifier', $items[$i]['identifier']);
		//error_log("New s_identifier generated: ".$_SESSION['s_identifier'],0);
		//api_session_unregister('s_identifier');
		//5.1.b.4 display info message in message frame (side-menu bottom)
		//$output .=message("<img src=\'../img/restart.jpg\' alt='restart'/>".get_lang('ScormRestarted'));//the img tag fails because of the htmlentities() call in message()
		//$output .=message(htmlentities(get_lang('ScormRestarted'),ENT_QUOTES,$charset_lang));
	}
 //that's all for the special "restart" treatment, although it includes loading the scorm document - see rest of script

} elseif (($menu=='prev') || ($menu=='next')) {
	//error_log("Menu was 'prev' or 'next'",3, 'C:\e.txt');

	/*==============================
	  IF PREV OR NEXT WAS CLICKED...
	  ==============================*/
	//5.2.15 If 'previous' or 'next' were clicked, then we might need to reload this script in order
	// to gain benefits of the database updates before we display the TOC
	
	if ($version == '1.3') { 
		//comment 11.12.2006
		#$output .= message('Sequencing for 1.3 not yet available'); 
	} else {
		// if the current identifier is empty, set to first item identifier
		$i = 1;
		if (!$JLMS_SESSION->get('s_identifier')) {
		//if (empty($_SESSION['s_identifier'])) { 
			$i = 1;
			while (isset($items[$i]) && ($items[$i]['href']=='')) { $i ++;}/*get index of first elem with href*/
			$JLMS_SESSION->set('s_identifier', $items[$i]['identifier']);
			//error_log("s_identifier was empty - new one: ".$JLMS_SESSION->get('s_identifier'),3, 'C:\e.txt');
		}
		if($openfirst == 'yes'){
		 //do nothing so that we start on first element
		 //$s_identifier = $items[1]['identifier'];
			//error_log("Openfirst was set, found first valid elem: ".$items[$i]['identifier'],3, 'C:\e.txt');
			while (isset($items[$i]) && ($items[$i]['href']=='')) { $i ++;}
		} else {
			if (isset($items_dictionary[$JLMS_SESSION->get('s_identifier')])) {
				$i = $items_dictionary[$JLMS_SESSION->get('s_identifier')];
			} else { $i = 0; }
			if ($menu == 'next') {
				//error_log("Menu was 'next'",3, 'C:\e.txt');
				//echo '<pre>';
				//print_r($items);die;
				//error_log("Prev i = ".$i,3, 'C:\e.txt');
				if (!$i) { $i = 0; }
				/*$i ++;
				while (isset($items[$i]) && ($items[$i]['href']=='')) { $i ++;}*/

				do {  // we take the next sco which has a href
					$i++;
				} while (($i <= count($items)) && (!isset($items[$i]['href']) || (isset($items[$i]['href']) && $items[$i]['href'] == '')));
				/*while ($i <= count($items)) {
					$i ++;
					if (isset($items[$i]['href']) && $items[$i]['href'] != '') {
						break;
					}
				}*/
				//error_log("Now i = ".$i,3, 'C:\e.txt');
				
				
				if ($i > count($items)) {
					$i = count($items);
					//comment 11.12.2006
					#$output .= message('ScormNoNext'); 
					//$exit_after_menu = true; 
				}
			}
			if ($menu == 'prev') {
				//error_log("Menu was 'prev'",3, 'C:\e.txt');
				//error_log("Prev i = ".$i,3, 'C:\e.txt');
				if (!$i) { $i = 0; }
				do {  // we take the previous sco which has href
					$i--;
				} while (($i >= 1) && (!isset($items[$i]['href']) || (isset($items[$i]['href']) && $items[$i]['href'] == '')));
				//error_log("Now i = ".$i,3, 'C:\e.txt');
				if ($i < 1) {
					$i = 1;
					//comment 11.12.2006
					#$output .= message('ScormNoPrev'); 
					//$exit_after_menu = true; 
				}
			}
		}
		//sco opening begin
		$sco_href_id = $i;
		/*ob_start();
		print_r($items);
		$rrr = ob_get_contents();
		ob_end_clean();
		//error_log("ITEMS = ".$rrr,3, 'C:\e.txt');
		*/

		/*echo '<pre>';
		print_r($items);die;*/
		if (!$i) { $i = 0; }
		//error_log("MENU = ".$menu,3, 'C:\e.txt');
		if ($menu == 'next') {
			/*if (isset($items[$i+1]) && isset($items[$i+1]['href']) && $items[$i+1]['href']) {
				$i = $i + 1;
			}*/
			$href = $JLMS_CONFIG->get('live_site') . "/" . _JOOMLMS_SCORM_FOLDER . "/".$openDir."/".$items[$i]['href'];
			$JLMS_SESSION->set('s_identifier', $items[$i]['identifier']);
			//error_log("New s_identifier: ".$JLMS_SESSION->get('s_identifier')." - Old one is: ".$JLMS_SESSION->get('old_sco_identifier'),3, 'C:\e.txt');
			if ($JLMS_SESSION->get('s_identifier') && ($JLMS_SESSION->get('s_identifier') == $JLMS_SESSION->get('old_sco_identifier'))) {
				$exit_after_menu = true; 
			} elseif ($JLMS_SESSION->get('s_identifier') && ($JLMS_SESSION->get('s_identifier') != $JLMS_SESSION->get('old_sco_identifier'))) {
				$exit_after_menu = false; 
			}
		} elseif($menu == 'prev') {
			if (!isset($items[$i])) {
				$i = 1;
			}
			$href = $JLMS_CONFIG->get('live_site') . "/" . _JOOMLMS_SCORM_FOLDER . "/".$openDir."/".$items[$i]['href'];
			$JLMS_SESSION->set('s_identifier', $items[$i]['identifier']);
			//error_log("New s_identifier: ".$JLMS_SESSION->get('s_identifier')." - Old one is: ".$JLMS_SESSION->get('old_sco_identifier'),3, 'C:\e.txt');
			if ($JLMS_SESSION->get('s_identifier') && ($JLMS_SESSION->get('s_identifier') == $JLMS_SESSION->get('old_sco_identifier'))) {
				$exit_after_menu = true; 
			} elseif ($JLMS_SESSION->get('s_identifier') && ($JLMS_SESSION->get('s_identifier') != $JLMS_SESSION->get('old_sco_identifier'))) {
				$exit_after_menu = false; 
			}
		} else {
			$href = $JLMS_CONFIG->get('live_site') . "/" . _JOOMLMS_SCORM_FOLDER . "/".$openDir."/".$items[$i]['href'];
			$JLMS_SESSION->set('s_identifier', $items[$i]['identifier']);
			//error_log("New s_identifier: ".$JLMS_SESSION->get('s_identifier')." - Old one is: ".$JLMS_SESSION->get('old_sco_identifier'),3, 'C:\e.txt');
		}
		$sco_href_id = $i;
		//$s_identifier = $identifier;	
	}
} elseif ($menu == 'my_status') {
	//error_log("Menu was 'my_status'",0);

/*===========================
  IF MY STATUS WAS CLICKED...
  ===========================*/
//6. The user chose to display the status screen. 
// @todo It is a very bad idea to mix status display and the rest of the TOC display.
//  Ideally, this should be fixed by including here two different display scripts depending on what to display.
	
	//allows to not highlight an element if currently displaying status
	$JLMS_SESSION->set('displaying_status', true);
	
	if ($version == '1.1') { 
		$output .= message('ScormNoStatus'); 
	} else {
		$w = $tablewidth - 20;
		$output .= "<br />";
	
		//if display in fullscreen required
		$output .= "<table align='center'>";
		/*if (strcmp($_GET["fs"],"true")==0)
		{ $output .= "<table align='center'>"; }

		else
		{ $output .= "<table class='margin_table'>"; }*/
		
		$output .="<tr><td><div class='title'>ScormMystatus</div></td></tr>"
			."<tr><td>&nbsp;</td></tr>"
			."<tr><td>"
				."<table border='0' class='data_table'><tr>\n"
					."<td><div class='mystatusfirstrow'>ScormLessonTitle</div></td>\n"
					."<td><div class='mystatusfirstrow'>ScormStatus</div></td>\n"
					."<td><div class='mystatusfirstrow'>ScormScore</div></td>\n"
					."<td><div class='mystatusfirstrow'>ScormTime</div></td></tr>\n";

		//going through the items using the $items[] array instead of the database order ensures
		// we get them in the same order as in the imsmanifest file, which is rather random when using
		// the database table
		$counter = 0;
		foreach($items as $i => $myitem ) {
			$query = "SELECT * FROM #__lms_scorm_sco WHERE user_id = '".$my->id."' AND content_id = '".$mycontentId."'"
			. "\n AND sco_identifier = '".$myitem['identifier']."'";// order by scoId";
			$database->SetQuery( $query );
			$ar = $database->LoadObjectList();
			if (count($ar)) {
				$ar = $ar[0];

				$counter++;
				if (($counter % 2)==0) { $oddclass="row_odd"; } else { $oddclass="row_even"; }

				$lesson_status=$ar->status;
				$score=$ar->score;
				$time=$ar->sco_time;
				$scoIdentifier=$ar->sco_identifier;
				$title=$myitem['title'];
				if (strlen($title)>65) {
					$title=substr($title,0,64);
					$title.="...";
				}
				//$title=str_replace(' ','&nbsp;',$title);
				//Remove "NaN" if any (@todo: locate the source of these NaN)
				$time = str_replace('NaN','00',$time);
				if (($lesson_status=='completed') || ($lesson_status=='passed')) { $color='green'; } else { $color='black'; }
				$output .= "<tr class='$oddclass'>\n"
							."<td><div class='mystatus'>$title</div></td>\n"
							."<td><font color='$color'><div class='mystatus'>".$array_status[$lesson_status]."</div></font></td>\n"
							."<td><div class='mystatus' align='center'>".($score==0?'-':$score)."</div></td>\n"
							."<td><div class='mystatus'>$time</div></td>\n"
						."</tr>\n";
			}
		}

		$output .= "</table></td></tr></table>";
	}
	$output .= "</body></html>";
	//We just wanted the status, so skip the rest now (we don't need any footer anyway)
	echo $output;
	exit();
}
//echo "<pre>".print_r($items,true)."</pre>";
//if the action requested is any of restart, openfirst, next or prev, open the content in the content frame
//This is done by calling opensco.php in the 'message' frame, which in turn calls the document in the 'sco' frame.
# (DEN) 1 line below
$time= time();
if(($menu == 'restart' || $menu == 'next' || $menu == 'prev' || $openfirst == 'yes') && ($exit_after_menu == false)){
	//error_log("Menu was 'prev' or 'next' or restart or openfirst was set and could include javascript for opensco and contents.php.",0);
	//error_log('get_better_anchor_target returned '.$target_identifier,0);
	$output .= "<script type='text/javascript'>\n <!--//--><![CDATA[//><!-- \n".
		"nextwindow=open('index.php?tmpl=component&option=com_joomla_lms&Itemid=37&task=scorm_opensco&course_id=".$course_id."&id=".$scorm_id."&href_item=".$sco_href_id."&sco_identifier=".$JLMS_SESSION->get('s_identifier')."','message');".
		//this line enables the movement of the menu
				//"thiswindow=open('contents.php?file=$request_file&openDir=$openDir&time=$time&edoceo=$edoceo#$identifier','contents');".
				"thiswindow=open('index.php?tmpl=component&option=com_joomla_lms&Itemid=37&task=scorm_contents&course_id=$course_id&id=$scorm_id&time=$time','contents');".
				"\n //--><!]]> \n</script>";
			//sco opening end
}


/*=======================
  TABLE OF CONTENTS TITLE
  =======================*/
//6. If we are here, it's only because the user didn't choose the status display 
//   (otherwise we would have exited). SHOW the TOC here... 
//6.1 Deal with the title

$t = $defaultorgtitle;
$t = str_replace(' ', '&nbsp;', $t);
if ($wrap) {
	$output .= "<div><b>".(($charset_lang != 'ISO-8859-1')?htmlentities($defaultorgtitle, ENT_QUOTES, $charset_lang):$defaultorgtitle)."</b></div><br />";
} else {
	$output .= "<table border='0' cellspacing='0' cellpadding='0' width='$tablewidth'>
		<tr>
			<td>
				<div><b>$t</b></div>
			</td>
		</tr>
	</table>
	<br />
	<table border='0' cellspacing='0' cellpadding='0' width='$tablewidth'>";
}

//remember the title in the session (for later reuse)
$JLMS_SESSION->set('defaultorgtitle', $defaultorgtitle);

/*====================================
  TABLE OF CONTENTS LISTING ROW BY ROW
  ====================================*/
//6.2 Displaying TOC

//$output .= "<pre>".print_r($items,true)."</pre>";

//6.2.1 Init vars to compute total items completed
$num_of_completed=0;
$i=1;

while (isset($items[$i]) && $items[$i]) {
	//error_log("Now processing item $i: ".$items[$i]['identifier'],0);

	//6.2.2 Set whether to highlight the item or not (don't highlight if displaying status)
	$bold = false;
	if( ( $items[$i]['identifier'] == $JLMS_SESSION->get('s_identifier')) && (!$JLMS_SESSION->get('displaying_status'))){
		//error_log("Item is the current one",0);
		$bold = true;
	}
	
	if (!$wrap) { $output .= "<tr>"; } //do not wrap this item with the previous one }
	
	//6.2.3 if this user never opened this content 
	// make initial entries in the scorm data table for this student
	// the order of insertions is the same as in the file (so it is the one we want to keep for status display)
	// $index comes from outside this script and represents the scoId in the table
	if ($everopened == false) {
		$index = intval(mosGetParam($_REQUEST, 'index', 0));
		//error_log("index = ".$index."; sco_i = ".mosGetParam($_REQUEST, 'sco_identifier', 0),3, 'C:\e.txt');
		$tmp_title = $items[$i]['title'];
		if ($charset_lang != 'ISO-8859-1') {
			$tmp_title = htmlentities($tmp_title, ENT_QUOTES, $charset_lang);
		}
		$query = "INSERT INTO #__lms_scorm_sco (content_id, sco_id, sco_identifier, sco_title, status, user_id, score, sco_time)"
		. "\n VALUES ('".$mycontentId."', '".$index."', '".$items[$i]['identifier']."', ".$database->Quote($tmp_title).", "
		. "\n 'not attempted', '".$my->id."', '0', '00:00')";
		$database->SetQuery( $query );
		$database->query();
	}
	
	//6.2.4 Display spaces/cells an amount of time that corresponds to the depth of the element in the hierarchy
	if ($wrap) { 
		$output .= str_repeat("&nbsp;&nbsp;",$items[$i]['level']-1); 
	} else { 
		$output .= "<td>".str_repeat("&nbsp;&nbsp;",$items[$i]['level']-1)."</td>"; 
	}
	$col=$maxlevel-$items[$i]['level']+1; //prepare the number of columns to display
	if (!$wrap) { $output .= '<td colspan="'.$col.'"><table border="0" cellspacing="0" cellpadding="0">'; }
	
	//6.2.5 Get the document/content path
	//$href="../../courses/$_course[path]/scorm".$openDir."/".urlencode($items[$i]['href']);
	if (isset($items[$i]['href'])) {
		if(substr($items[$i]['href'],0,4)=='http') {
			$href = $items[$i]['href'];
		} else {
			$href = $JLMS_CONFIG->get('live_site') . "/" . _JOOMLMS_SCORM_FOLDER . "/".$openDir."/".$items[$i]['href'];
		}
		$sco_href_id = $i;
	}
	//6.2.6 Get useful values in practical variables
	$identifier = $items[$i]['identifier'];
	$prereq = isset($items[$i]['prereq'])?$items[$i]['prereq']:0;
	
	//6.2.7 Get the recorded status for this document/content
	$lesson_status = JLMS_GetSCO_status($my->id, $mycontentId, $identifier);
	/*$query = "SELECT status FROM #__lms_scorm_sco WHERE content_id = '".$mycontentId."' AND user_id = '".$my->id."'"
	. "\n AND sco_identifier = '".$identifier."'";
	$database->SetQuery( $query );
	$r = $database->LoadResult();
	$lesson_status = $r?$r:'';*/
	
	//6.2.8 Count this lesson as completed if applicable
	if (($lesson_status == 'completed') || ($lesson_status == 'passed')) { $num_of_completed++; }
	
	//6.2.9 display this document/content name as a link to the document/content itself
	if (isset($items[$i]['href']) && ($items[$i]['href']!='') ) {
		if (!$wrap) { $output .= "<tr><td>"; } //display choice
		
		//6.2.9.1 if the lesson was completed, display a little icon
		if (($lesson_status=='completed') or ($lesson_status=='passed')) { 
			$output .= '<img src="components/com_joomla_lms/lms_images/checkbox_on2.gif" border="0" width="13" height="11" alt="on"/>'; 
		} else {
			//insert the image but make it invisible, so that the space taken is identical (no movement when image appears)
			$output .= '<img src="components/com_joomla_lms/lms_images/checkbox_on2.gif" border="0" width="13" height="11" alt="on" style="visibility: hidden"/>'; 
			//$output .= "&nbsp;"; 
		}
		
		if (!$wrap) { $output .= "</td><td>"; } //display choice
		
		//6.2.9.2 Add HTML anchor
		$output .= "<a name='$identifier'>";
		//6.2.9.3 Highlight if applicable
		if ($bold) { $output .= "<b>";}
		//6.2.9.4 Display the link
		$output .= "<a title='". $items[$i]['title']. "' href='index.php?tmpl=component&option=com_joomla_lms&Itemid=37&task=scorm_opensco&course_id=".$course_id."&id=".$scorm_id;
		if ($version=='1.3') { //SCORM version 1.3
			if ($items[$i]['parameters']!='') { 
				// there should probably be an additional '?' here... maybe...
				$output .= "{$items[$i]['parameters']}"."&"; //add parameters to the link 
			} else { 
				$output .= "&"; 
			}
		} else { //version is not 1.3 
			$output .= "&"; 
		}
		$output .= "href_item=".$sco_href_id."&sco_identifier=$identifier' target='message' class='";
		//as you might realise, this links points to the message frame, to opensco.php, which will then open the content
		//in the contents frame
		
		//change CSS class to "completed" if completed or passed
		if (($lesson_status=='completed') or ($lesson_status=='passed')) { $output .= $array_status[$lesson_status]; }
		$output .= "'>"; 
		//error_log("Added link to opensco with sco_identifier=".$items[$i]['identifier'],0);

	}else{
		if (($lesson_status=='completed') or ($lesson_status=='passed')) { 
			$output .= '<img src="components/com_joomla_lms/lms_images/checkbox_on2.gif" border="0" width="13" height="11" alt="on"/>'; 
		} else {
			//insert the image but make it invisible, so that the space taken is identical (no movement when image appears)
			$output .= '<img src="components/com_joomla_lms/lms_images/checkbox_on2.gif" border="0" width="13" height="11" alt="on" style="visibility: hidden"/>'; 
		}
	}
	
	//6.2.10 Simulate htmlentities() just for spaces
	$t=$items[$i]['title'];
	
	//6.2.11 Display title (unfiltered if wrapped display - which is the default)
	$t=str_replace(' ','&nbsp;',$t);
	$twrap = $items[$i]['title'];
	if ($charset_lang != 'ISO-8859-1') {
		$twrap = htmlentities($twrap, ENT_QUOTES, $charset_lang);
	}
	//cut the title string if too long to fit in typical frame width
	$max_len = 37-(3*$items[$i]['level']);
	if(strlen($twrap)>$max_len){
		$twrap = str_replace(' ','&nbsp;',substr($twrap,0,$max_len-3).'...');
	}

	if (!$wrap) { $output .= $t; } else { $output .= $twrap; }
	
	if (isset($items[$i]['href']) && ($items[$i]['href'] != '') ) { $output .= "</a>"; } //see above for opening anchor tag "a name=..."
	if ($bold) { $output .= "</b>";}   
	if (!$wrap) { $output .= "</td></tr></table></td></tr>\n"; } else { $output .= "<br />\n"; }
	$i++;
}

//6.2.12 Display percentage complete
if ( count($items) != 0 ) { 
    $percent=round($num_of_completed/count($items)*100); 
} else { //no item found. Display an error.
	//comment 11.12.2006
    #$output .= message('ScormNoItems');
	echo $output; exit();
}

$npercent=100-$percent;
if (($percent==0) and ($openfirst=='yes') and ($version != '1.3') and ($menu=='')) { $menu="next"; } 
//ie. the first lesson appears if nothing is completed, not if in 1.3 and the user clicked onto Launch just now

if (!$wrap) { $output .= "</table>\n"; } // display-choice

/*=================
  COMPLETION STATUS
  =================*/

//6.2.13 Display the completion status of the whole learning path
//! temporary fix for the accentuated characters bug (due to different encoding
// between the imsmanifest file - which is the source to this frame's encoding - and the language file)
// The fix is temporary because it will most probably fail on any non-european special character
// To really fix this, there should be a function to check the language files' encoding and put it
// in the third argument to htmlentities() here below
$output .= '<br /><a name="statustable"></a><table border="0"><tr><td>'//ScormCompstatus'."<br />"
    .'<table border="0" cellpadding="0" cellspacing="0"><tr><td>'
    .'<img src="components/com_joomla_lms/lms_images/bars/bar_1.gif" width="1" height="12">'
    .'<img src="components/com_joomla_lms/lms_images/bars/bar_1u.gif" width="'.$percent.'" height="12">'
    .'<img src="components/com_joomla_lms/lms_images/bars/bar_1m.gif" width="1" height="12">'
    .'<img src="components/com_joomla_lms/lms_images/bars/bar_1r.gif" width="'.$npercent.'" height="12">'
    .'<img src="components/com_joomla_lms/lms_images/bars/bar_1.gif" width="1" height="12"></td></tr></table>'
    ."</td><td valign=\"bottom\"><font align=\"left\"><br />$percent&nbsp;%</td></tr></table>\n";

if ($tabledraw) {
    $output .= "<br /><br /><hr /><br />";
    $i=1;
    while ($items[$i]) {
        $output .= "Index : {$items[$i]['index']} "
            ."Identifierref : {$items[$i]['identifierref']} "
            ."Identifier : {$items[$i]['identifier']} "
            ."Level : {$items[$i]['level']}   "
            ."Title : {$items[$i]['title']} "
            ."Href : {$items[$i]['href']} "
            ."Prereq : {$items[$i]['prereq']} "
            ."Parameters : {$items[$i]['parameters']} "
            ."Clusterinfo : {$items[$i]['clusterinfo']}<br />\n";
        $i++;
    }
    $output .= "<br />\n";  
    $output .= "defaultorg : $defaultorgref<br />defaultorgtitle : $defaultorgtitle";
}

//6.2.14 If the parser was used, close it (and the XML file too)
if ( ($openfirst == 'yes') || ($menu == 'restart') ){
	xml_parser_free($xml_parser);
	fclose($fp);
}

//if($menu=='restart'){unset($_SESSION['s_identifier']);}
$JLMS_SESSION->set('content_id', $mycontentId);
$JLMS_SESSION->set('items', $items);

//if(empty($_SESSION['old_sco_identifier'])){
//	$_SESSION['old_sco_identifier'] = $_SESSION['s_identifier'];
//}
$JLMS_SESSION->set('displaying_status', false);

//if the user didn't click a menu button, then assume he clicked a SCORM button in the content,
// and that the element that needs to be updated by closesco.php is the latest element we have here,
// namely $_SESSION['s_identifier'], instead of $_SESSION['old_s_identifier']
if(empty($menu)){
	$old_s = $JLMS_SESSION->get('s_identifier');
	$JLMS_SESSION->set('old_sco_identifier', $old_s);
}
echo $output;
?>