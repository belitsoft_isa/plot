<?php
/**
============================================================================== 
* This file is the buttons menu that allows going forward-backward in the
* SCORM sequence. It is independent from the current element viewed.
============================================================================== 
//this file loads in when the whole content is loaded, and does not refresh while in operation !
*/
$time=time();
global $my, $database, $Itemid;
global $JLMS_SESSION;
$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
$scorm_id = intval(mosGetParam($_REQUEST, 'id', 0));
$scorm_info = JLMS_GetSCORM_info( $scorm_id );
if (!$scorm_info) {
	mosErrorAlert('Bad SCORM identifier');
}
$file = $scorm_info->file_info;
$openDir = $scorm_info->openDir_info;
$edoceo 	= $scorm_info->is_escorm?'yes':'no';
$who = $my->username . " (".$my->name.")";//$_user ['firstName']." ".$_user ['lastName'];
?>
<script>
	var is_contents_visible = 0;
	function jlms_ChangeContents_fr() {
		if (is_contents_visible == 0) {
			parent.document.getElementById('contents_iframe').style.display = '';
			is_contents_visible = 1;
		} else if (is_contents_visible == 1) {
			parent.document.getElementById('contents_iframe').style.display = 'none';
			is_contents_visible = 0;
		}
	}
</script>
<form action="index.php?tmpl=component&option=com_joomla_lms&Itemid=37&task=scorm_contents<?php echo "&course_id=$course_id&id=$scorm_id&time=$time"; ?>" method="post" target="contents" name='theform'>
<center>
<table cellpadding="0" cellspacing="0" border="0"><tr><td>
<?php
	JLMS_ShowHeading('');
	$toolbar = array();
	$toolbar[] = array('btn_type' => 'prev', 'btn_js' => "javascript:document.theform.menu.value='prev';document.theform.submit();");
	$toolbar[] = array('btn_type' => 'next', 'btn_js' => "javascript:document.theform.menu.value='next';document.theform.submit();");
	$toolbar[] = array('btn_type' => 'restart', 'btn_js' => "javascript:document.theform.menu.value='restart';document.theform.submit();");
	$toolbar[] = array('btn_type' => 'contents', 'btn_js' => "javascript:jlms_ChangeContents_fr();");
	echo JLMS_ShowToolbar($toolbar, false);
/*
<table cellpadding='0' cellspacing='0' align='left'><tr><td><font color='white'><div align='left' style='margin-left: 0.5em'>
<a href="index.php?tmpl=component&option=com_joomla_lms&Itemid=37&task=scorm_contents&menu=my_status<?php echo "&course_id=$course_id&id=$scorm_id&time=$time"; ?>" target="sco"><img border="0" src="components/com_joomla_lms/images/scorm_img/scorm_stats.png" width="16px" height="16px" title="<?php echo 'ScormMystatus'; ?>"></a> &nbsp;&nbsp;
<a href="javascript:document.theform.menu.value='prev';document.theform.submit();"><img border="0" src="components/com_joomla_lms/images/scorm_img/scorm_prev.png" width="16px" height="16px" title="<?php echo 'ScormPrevious'; ?>"></a>
&nbsp;&nbsp;
<a href="javascript:document.theform.menu.value='next';document.theform.submit();"><img border="0" src="components/com_joomla_lms/images/scorm_img/scorm_next.png" width="16px" height="16px" title="<?php echo 'ScormNext'; ?>"></a>
&nbsp;&nbsp;
<a href="javascript:document.theform.menu.value='restart';document.theform.submit();"><img border="0" src="components/com_joomla_lms/images/scorm_img/scorm_restart.png" width="16px" height="16px" title="<?php echo 'ScormRestart'; ?>"></a>
&nbsp;&nbsp;
</div></font></td></tr></table> */ ?>
</td></tr></table>
</center>
<input type='hidden' name='menu' value='init' />
</form>
<?php
$JLMS_SESSION->clear('s_href');
//api_session_unregister('s_identifier');
?>