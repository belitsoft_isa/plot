<?php
/**
* /languages/french/chat.lang.php
* Joomla LMS Component
* * * ElearningForce DK
**/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

$JLMS_LANGUAGE['_JLMS_CHAT_TITLE'] = 'Chat';
$JLMS_LANGUAGE['_JLMS_CHAT_TBL_HEAD_CHAT'] = 'Chat';
$JLMS_LANGUAGE['_JLMS_CHAT_TBL_HEAD_USERS'] = 'Utilisateurs';
$JLMS_LANGUAGE['_JLMS_CHAT_WELCOME_MESSAGE'] = 'Bienvenue � notre chat!';
$JLMS_LANGUAGE['_JLMS_CHAT_BTN_POST'] = 'Envoyer Message';
$JLMS_LANGUAGE['_JLMS_CHAT_COURSE_CHAT'] = 'Chat du cours';
$JLMS_LANGUAGE['_JLMS_CHAT_GROUP_CHAT'] = 'Chat du groupe';
?>