<?php
/**
* /languages/english/course_dropbox.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$JLMS_LANGUAGE['_JLMS_DROP_ERROR_NO_SEND_TO'] = "发送消息失败: 未选择收件人。"; //fix one user (ticket  [QDHZ-1096])


/* 1.0.6 */
$JLMS_LANGUAGE['_JLMS_DROP_NAME'] = "名称:";
$JLMS_LANGUAGE['_JLMS_SELECT_FILE_ENTER_NAME'] = "请选择文件或输入名称。";
$JLMS_LANGUAGE['_JLMS_DROP_ADD_ITEM'] = '添加交流项至交流箱';
$JLMS_LANGUAGE['_JLMS_DROP_BTN_ADD_ITEM'] = '添加交流项';
$JLMS_LANGUAGE['_JLMS_DROP_SELECT_ITEM'] = '请选择交流项';
$JLMS_LANGUAGE['_JLMS_DROP_TITLE'] = '课程交流箱';
$JLMS_LANGUAGE['_JLMS_DROP_TBL_HEAD_NAME'] = '姓名';
$JLMS_LANGUAGE['_JLMS_DROP_TBL_HEAD_FROM'] = '由';
$JLMS_LANGUAGE['_JLMS_DROP_TBL_HEAD_TO'] = '至';
$JLMS_LANGUAGE['_JLMS_DROP_TBL_HEAD_CORRECTED'] = '已更正';
$JLMS_LANGUAGE['_JLMS_DROP_TBL_HEAD_DESCR'] = '评论';
$JLMS_LANGUAGE['_JLMS_DROP_STATUS_READ'] = '已阅';
$JLMS_LANGUAGE['_JLMS_DROP_STATUS_UNREAD'] = '未阅';
$JLMS_LANGUAGE['_JLMS_DROP_STATUS_CORRECT'] = '已更正';
$JLMS_LANGUAGE['_JLMS_DROP_ITEM_LINK'] = '下载';

$JLMS_LANGUAGE['_JLMS_DROP_SET_READ'] = "标记为'已阅'";
$JLMS_LANGUAGE['_JLMS_DROP_SET_UNREAD'] = "标记为'未阅'";
$JLMS_LANGUAGE['_JLMS_DROP_SET_CORR'] = '更正';

$JLMS_LANGUAGE['_JLMS_DROP_NEW_ITEM'] = '新建交流项';
$JLMS_LANGUAGE['_JLMS_DROP_DEL_ITEM'] = '删除交流项';
$JLMS_LANGUAGE['_JLMS_DROP_SEND_TO'] = '发送至:';
$JLMS_LANGUAGE['_JLMS_DROP_MARK_AS_CORRECTED'] = "标记为'已更正':";

$JLMS_LANGUAGE['_JLMS_DROP_LINK_FULL_DESCR'] = "查看完整说明";
$JLMS_LANGUAGE['_JLMS_DROP_LINK_MORE_TEXT'] = "更多";
$JLMS_LANGUAGE['_JLMS_DROP_SENDER'] = "发件人:";
$JLMS_LANGUAGE['_JLMS_DROP_RECV'] = "收件人:";
//header
$JLMS_LANGUAGE['_JLMS_DROP_INBOX'] = "收件箱:";
$JLMS_LANGUAGE['_JLMS_DROP_OUTBOX'] = "发件箱:";
?>