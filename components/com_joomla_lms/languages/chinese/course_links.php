<?php
/**
* /languages/english/course_links.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/* 1.0.7 */
$JLMS_LANGUAGE['_JLMS_LINKS_TYPE_IFRAME'] = '在内联框架内打开';
$JLMS_LANGUAGE['_JLMS_LINKS_TYPE_SQBOX'] = '在SqueezeBox窗口内打开';
$JLMS_LANGUAGE['_JLMS_LINKS_DISPLAY_WIDTH'] = '显示宽度';
$JLMS_LANGUAGE['_JLMS_LINKS_DISPLAY_HEIGHT'] = '显示高度';

/* 1.0.4 */
$JLMS_LANGUAGE['_JLMS_LINKS_TYPE_NEW_WINDOW'] = '在新窗口打开';
$JLMS_LANGUAGE['_JLMS_LINKS_TYPE_SAME_WINDOW'] = '在同一窗口打开';
$JLMS_LANGUAGE['_JLMS_LINKS_ENTER_LINK_NAME'] = '请输入链接名称。';
$JLMS_LANGUAGE['_JLMS_LINKS_ENTER_VALID_LINK_NAME'] = '请输入有效链接名称。 (如. \'http://site.com/\')';
$JLMS_LANGUAGE['_JLMS_LINKS_CREATE_LINK'] = '新建链接';
$JLMS_LANGUAGE['_JLMS_LINKS_BTN_CREATE_LINK'] = '创建链接';
$JLMS_LANGUAGE['_JLMS_LINKS_EDIT_LINK'] = '编辑课程链接';
$JLMS_LANGUAGE['_JLMS_LINKS_BTN_EDIT_LINK'] = '保存';
// image titles
$JLMS_LANGUAGE['_JLMS_LINKS_IMG_NEW_LINK'] = '新建链接';
$JLMS_LANGUAGE['_JLMS_LINKS_IMG_DEL_LINK'] = '删除';
$JLMS_LANGUAGE['_JLMS_LINKS_IMG_EDIT_LINK'] = '编辑';

$JLMS_LANGUAGE['_JLMS_LINKS_COURSE_LINKS'] = '课程链接';
$JLMS_LANGUAGE['_JLMS_LINKS_TBL_HEAD_LINK'] = '链接';
$JLMS_LANGUAGE['_JLMS_LINKS_TBL_HEAD_OWNER'] = '拥有者';
$JLMS_LANGUAGE['_JLMS_LINKS_TBL_HEAD_DESCR'] = '说明';
$JLMS_LANGUAGE['_JLMS_LINKS_TBL_LINK_TITLE'] = '查看链接';

?>