<?php
/**
* /languages/english/mailbox.lang.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$JLMS_LANGUAGE['_JLMS_MB_COURSE_CONTACTS'] = 'Course Contacts';
$JLMS_LANGUAGE['_JLMS_MB_CONTACTS'] = 'Contacts';

$JLMS_LANGUAGE['_JLMS_MB_INBOX'] = 'Inbox (X/Y)';
$JLMS_LANGUAGE['_JLMS_MB_OUTBOX'] = 'Outbox (Y)';
$JLMS_LANGUAGE['_JLMS_MB_FROM'] = 'From';

$JLMS_LANGUAGE['_JLMS_MB_COURSE_NAME'] = 'Course Name';

/* 1.0.6 */
// reply to message
$JLMS_LANGUAGE['_JLMS_MB_REPL_RE'] = '回复:';
$JLMS_LANGUAGE['_JLMS_MB_REPL_REPLY'] = '回复';
$JLMS_LANGUAGE['_JLMS_MB_REPL_SEL'] = '请选中回复信息';
$JLMS_LANGUAGE['_JLMS_MB_REPL_DF'] = 'l, F d, Y, g:i:s A';
$JLMS_LANGUAGE['_JLMS_MB_REPL_YW'] = '您提到';

/* 1.0.5 */
$JLMS_LANGUAGE['_JLMS_MB_MK_READ'] = '标记已阅';
$JLMS_LANGUAGE['_JLMS_MB_MK_UNREAD'] = '标记未阅';
$JLMS_LANGUAGE['_JLMS_MB_TO'] = 'To';
$JLMS_LANGUAGE['_JLMS_MB_DATE'] = '日期';
$JLMS_LANGUAGE['_JLMS_MB_SENDER'] = '发件人';
$JLMS_LANGUAGE['_JLMS_MB_RECEPIENTS'] = '收件人';
$JLMS_LANGUAGE['_JLMS_COMPOSE_ALT_TITLE'] = '撰写';
$JLMS_LANGUAGE['_JLMS_INBOX_ALT_TITLE'] = '收件箱';
$JLMS_LANGUAGE['_JLMS_OUTBOX_ALT_TITLE'] = '发件箱';
$JLMS_LANGUAGE['_JLMS_MB_SEL_ITEM'] = '请选中邮件项';
$JLMS_LANGUAGE['_JLMS_MB_DEL_ITEM'] = '您确定要删除该项?';
$JLMS_LANGUAGE['_JLMS_MB_ENTER_USERNAME'] = '请选中收件人';
$JLMS_LANGUAGE['_JLMS_MB_ALL_USRS'] = '全部用户';
$JLMS_LANGUAGE['_JLMS_MB_ATTACHMENT'] = '附件:';

$JLMS_LANGUAGE['_JLMS_MB_TITLE'] = '邮箱';
$JLMS_LANGUAGE['_JLMS_MB_TBL_HEAD_STU'] = '学生';
$JLMS_LANGUAGE['_JLMS_MB_TBL_HEAD_GROUP'] = '分组';
$JLMS_LANGUAGE['_JLMS_MB_FILTER_ALL_RESULTS'] = '全部';
$JLMS_LANGUAGE['_JLMS_MB_FILTER_ALL_GROUPS'] = '全部分组';
$JLMS_LANGUAGE['_JLMS_MB_FILTER_ALL_STUDENTS'] = '全部用户';
$JLMS_LANGUAGE['_JLMS_MB_SELECT_A_USER'] = '请选择用户';
$JLMS_LANGUAGE['_JLMS_MB_SUBJECT'] = '主题';
$JLMS_LANGUAGE['_JLMS_MB_TEXT'] = '文本';
$JLMS_LANGUAGE['_JLMS_MB_ATTACH'] = '附加文件';
$JLMS_LANGUAGE['_JLMS_MB_HIDE'] = '隐藏';
$JLMS_LANGUAGE['_JLMS_MB_ENTER_SUBJECT'] = '请输入主题';
$JLMS_LANGUAGE['_JLMS_MB_TEXT_CLEAR'] = '文本字段为空。您确定继续？';
$JLMS_LANGUAGE['_JLMS_MB_TEXT_CHOOSE'] = '选择收件人:';
$JLMS_LANGUAGE['_JLMS_MB_SENT_SUCCESS'] = '邮件已成功发送!';
$JLMS_LANGUAGE['_JLMS_MB_SENT_ERROR'] = '邮件发送有错误!';
?>