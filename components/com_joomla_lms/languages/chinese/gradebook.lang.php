<?php
/**
* /languages/english/gradebook.lang.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$JLMS_LANGUAGE['_JLMS_GB_ADD_NEW_FIELD'] = '新建';

$JLMS_LANGUAGE['_JLMS_TIME_SPENT_TABLE'] = '花费时间';

/* 1.0.6 */
$JLMS_LANGUAGE['_JLMS_GB_CRT_ENABLED'] = "启用证书:";

/* 1.0.5 */
$JLMS_LANGUAGE['_JLMS_COURSE_COMPLETION_TABLE'] = '课程完成';
$JLMS_LANGUAGE['_JLMS_COURSE_COMPLETED_ADMIN'] = "用户已完成课程";
$JLMS_LANGUAGE['_JLMS_COURSE_COMPLETED_USER'] = '您已完成课程';
$JLMS_LANGUAGE['_CONFIGURE_COURSE_COMPLETION'] = '配置课程完成';
$JLMS_LANGUAGE['_JLMS_GB_PRINT_CERTIFICATE'] = '打印证书';
$JLMS_LANGUAGE['_JLMS_GB_COURSE_CERTIFICATE'] = '课程证书';
$JLMS_LANGUAGE['_JLMS_GB_CERTIFICATES'] = '证书';
/* certificates: */
$JLMS_LANGUAGE['_JLMS_CRTF_USE_DEFAULT'] = '使用默认证书';
$JLMS_LANGUAGE['_JLMS_CRTF_FONT'] = '文本字体';
$JLMS_LANGUAGE['_JLMS_CRTF_TEXT_FIELDS'] = '自定义文本字段';
$JLMS_LANGUAGE['_JLMS_CRTF_SHORT_TEXT_FIELD'] = '文本字段';
$JLMS_LANGUAGE['_JLMS_CRTF_SHORT_SHADOW'] = '阴影';
$JLMS_LANGUAGE['_JLMS_CRTF_SHORT_X'] = 'X';
$JLMS_LANGUAGE['_JLMS_CRTF_SHORT_Y'] = 'Y';
$JLMS_LANGUAGE['_JLMS_CRTF_SHORT_HEIGHT'] = 'H';
$JLMS_LANGUAGE['_JLMS_CRTF_SHORT_FONT'] = '字体';
$JLMS_LANGUAGE['_JLMS_GB_TITLE'] = '成绩手册';
$JLMS_LANGUAGE['_JLMS_GBI_TITLE'] = '成绩手册项目';
$JLMS_LANGUAGE['_JLMS_GBS_TITLE'] = '成绩手册比例';
$JLMS_LANGUAGE['_JLMS_GBC_TITLE'] = '课程证书';
$JLMS_LANGUAGE['_JLMS_GB_EDIT_ITEM'] = '编辑成绩手册项目';
$JLMS_LANGUAGE['_JLMS_GBS_EDIT_SCALE'] = '编辑成绩手册比例';
$JLMS_LANGUAGE['_JLMS_GB_NEW_ITEM'] = '新成绩手册项目';
$JLMS_LANGUAGE['_JLMS_GB_DEL_ITEM'] = '删除成绩手册项目';
$JLMS_LANGUAGE['_JLMS_GBS_NEW_ITEM'] = '新成绩手册比例';
$JLMS_LANGUAGE['_JLMS_GBS_DEL_ITEM'] = '删除成绩手册比例';
$JLMS_LANGUAGE['_JLMS_GB_ENTER_NAME'] = '请输入成绩手册项目名称';
$JLMS_LANGUAGE['_JLMS_GB_ENTER_POINTS'] = '请输入有效分数';
$JLMS_LANGUAGE['_JLMS_GBS_ENTER_VALID_NUMS'] = '请输入有效的最低及最高分数';
$JLMS_LANGUAGE['_JLMS_GBI_CATEGORY'] = '类别:';
$JLMS_LANGUAGE['_JLMS_GBI_POINTS'] = '最高分数:';
$JLMS_LANGUAGE['_JLMS_GBI_TBL_HEAD_NAME'] = '名称';
$JLMS_LANGUAGE['_JLMS_GBI_TBL_HEAD_CAT'] = '类别';
$JLMS_LANGUAGE['_JLMS_GBI_TBL_HEAD_POINTS'] = '最高分数';
$JLMS_LANGUAGE['_JLMS_GBI_TBL_HEAD_DESCR'] = '说明';
$JLMS_LANGUAGE['_JLMS_GB_TBL_HEAD_STU'] = '学生';
$JLMS_LANGUAGE['_JLMS_GB_TBL_HEAD_GROUP'] = '群组';
$JLMS_LANGUAGE['_JLMS_GB_TBL_HEAD_CRT'] = '证书';
$JLMS_LANGUAGE['_JLMS_GBS_TBL_HEAD_NAME'] = '比例名称';
$JLMS_LANGUAGE['_JLMS_GBS_TBL_HEAD_MINPOINTS'] = '最低百分比';
$JLMS_LANGUAGE['_JLMS_GBS_TBL_HEAD_MAXPOINTS'] = '最高百分比';
$JLMS_LANGUAGE['_JLMS_GB_FILTER_ALL_GROUPS'] = '所有群组';
$JLMS_LANGUAGE['_JLMS_GB_VIEW_USER'] = '查看用户成绩等级';
$JLMS_LANGUAGE['_JLMS_GB_MARK_CRT'] = '标记可取得证书的用户';
$JLMS_LANGUAGE['_JLMS_GB_UNMARK_CRT'] = '取消标记可取得证书的用户';
$JLMS_LANGUAGE['_JLMS_GB_USER_HAVE_CRT'] = '具有证书的用户';
$JLMS_LANGUAGE['_JLMS_GB_USER_HAVE_NO_CRT'] = '不能取得证书的用户';
$JLMS_LANGUAGE['_JLMS_GB_SCORM_COMPLETED'] = '已完成';
$JLMS_LANGUAGE['_JLMS_GB_SCORM_INCOMPLETED'] = '未完成';
$JLMS_LANGUAGE['_JLMS_GB_POINTS'] = ' 分数.';
$JLMS_LANGUAGE['_JLMS_GB_SCORM_RESULTS'] = 'SCORM成绩';
$JLMS_LANGUAGE['_JLMS_GB_QUIZ_RESULTS'] = '考试成绩';
$JLMS_LANGUAGE['_JLMS_GB_GBI_RESULTS'] = '成绩手册成绩';

//gradebook menu 
$JLMS_LANGUAGE['_JLMS_GB_MENU_ITEMS'] = '定义成绩手册项目';
$JLMS_LANGUAGE['_JLMS_GB_MENU_CERTS'] = '创建课程证书';
$JLMS_LANGUAGE['_JLMS_GB_MENU_SCALE'] = '定义成绩手册比例';
//certificate
$JLMS_LANGUAGE['_JLMS_GB_CRT_NAME'] = '证书名称:'; // added in 1.0.3
$JLMS_LANGUAGE['_JLMS_GB_CRT_TEXT'] = '证书文本:';
$JLMS_LANGUAGE['_JLMS_GB_CRT_TEXT_X'] = 'X 文本的座标 (px):';
$JLMS_LANGUAGE['_JLMS_GB_CRT_TEXT_Y'] = 'Y 文本的座标 (px):';
$JLMS_LANGUAGE['_JLMS_GB_CRT_TEXT_ALIGN'] = '文本调整文本:';
$JLMS_LANGUAGE['_JLMS_GB_CRT_TEXT_SHADOW'] = '显示阴影:';
$JLMS_LANGUAGE['_JLMS_GB_CRT_TEXT_SIZE'] = '文本的高度 (px):';
$JLMS_LANGUAGE['_JLMS_GB_CRTF_TEXT_NOTE'] = " * 你可以用下列常数: <br />'#姓名#and '#用户名称#' - 输入学生和用户名,<br />#课程# - 输入课程名称,<br />#登记答桉# - 输入用户注册问题的答桉,<br />#日期# - 输入现时的日期.<br /> 您也可以使用日期格式, 例如: #日期(年-月-日)# 输出为 '2007-01-01'; #日期(日-月-年)# 输出为'01 January 2007'. 参看「说明」以获得更多详情.";
//mes for user to download certificate
$JLMS_LANGUAGE['_JLMS_GB_CRT_USER_LINK'] = '点击 #此处# 以下载证书'; // 在#中的文字附有链接word between # - would be link
$JLMS_LANGUAGE['_JLMS_GB_USER_INFORMATION'] = '用户信息';
$JLMS_LANGUAGE['_JLMS_GB_REG_INFORMATION'] = '注册信息';
?>