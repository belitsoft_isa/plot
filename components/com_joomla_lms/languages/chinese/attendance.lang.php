<?php
/**
* /languages/english/attendance.lang.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$JLMS_LANGUAGE['_JLMS_ATT_CANNOT_CHANGE_RECORDS'] = 'You cannot change attendance records for the selected date';
/* 1.0.4 */
$JLMS_LANGUAGE['_JLMS_ATT_WITH_SELECTED'] = 'Mark the selected';
$JLMS_LANGUAGE['_JLMS_ATT_TITLE'] = '出席登记';
$JLMS_LANGUAGE['_JLMS_ATT_TITLE_STU'] = '我的出席纪录';
$JLMS_LANGUAGE['_JLMS_ATT_TITLE_EXPORT'] = '导出出席纪录至Excel';
$JLMS_LANGUAGE['_JLMS_ATT_TBL_HEAD_STU'] = '学生';
$JLMS_LANGUAGE['_JLMS_ATT_TBL_HEAD_GROUP'] = '小组';
$JLMS_LANGUAGE['_JLMS_ATT_FILTER_ALL_GROUPS'] = '所有组别';
$JLMS_LANGUAGE['_JLMS_ATT_VIEW_STU_ATTENDANCE'] = '学生出席纪录';
$JLMS_LANGUAGE['_JLMS_ATT_STATUS_ATTENDED'] = '已出席';
$JLMS_LANGUAGE['_JLMS_ATT_STATUS_NOTATTENDED'] = '未出席';
$JLMS_LANGUAGE['_JLMS_ATT_MARK_ATTENDED'] = "标记为'出席'";
$JLMS_LANGUAGE['_JLMS_ATT_MARK_NOTATTENDED'] = "标记为'未出席'";

$JLMS_LANGUAGE['_JLMS_ATT_SELECT_USER'] = "选择用户:";
$JLMS_LANGUAGE['_JLMS_ATT_FILTER_ALL_USERS'] = "所有学生";
?>