<?php
/**
* /languages/english/homework.lang.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/*1.0.7*/
$JLMS_LANGUAGE['_JLMS_HW_AWAITING_REVIEW'] = "等待教师审查";
$JLMS_LANGUAGE['_JLMS_HW_MARK_COMPLETE'] = '标记为已完成';
$JLMS_LANGUAGE['_JLMS_HW_STATUS_NOT_PASSED'] = '未通过';
$JLMS_LANGUAGE['_JLMS_HW_STATUS_PASSED'] = '已通过';
$JLMS_LANGUAGE['_JLMS_HW_GRADE'] = '等级:';

$JLMS_LANGUAGE['_JLMS_HW_TBL_HEAD_GRADE'] = '等级';
$JLMS_LANGUAGE['_JLMS_HW_STATUS_INCOMPLETE'] = '未完成';

$JLMS_LANGUAGE['_JLMS_HW_WRITE_TEXT'] = '输入文本';
$JLMS_LANGUAGE['_JLMS_HW_GRADE_THIS_ACTIVITY'] = '分级/标记该活动:';
$JLMS_LANGUAGE['_JLMS_HW_LEARNERS_SUBS'] = "提交作业:"; 
 
$JLMS_LANGUAGE['_JLMS_HW_OFFLINE_ACTIVITY'] = '脱机活动';
$JLMS_LANGUAGE['_JLMS_HW_WRITE_TEXT'] = '输入文本';
$JLMS_LANGUAGE['_JLMS_HW_UPLOAD_FILE'] = '上传文件';
$JLMS_LANGUAGE['_JLMS_HW_ACTIVITY_TYPE'] = '活动类型';
$JLMS_LANGUAGE['_JLMS_HW_GRADED_ACTIVITY'] = '按等级的活动';

$JLMS_LANGUAGE['_JLMS_HW_TEACHER_COMMS'] = '教师意见:';

/* 1.0.5 */
$JLMS_LANGUAGE['_JLMS_HW_TITLE_HW'] = '作业';
$JLMS_LANGUAGE['_JLMS_HW_TBL_HEAD_DESCR'] = '说明';
$JLMS_LANGUAGE['_JLMS_HW_TBL_HEAD_HW'] = '作业';
$JLMS_LANGUAGE['_JLMS_HW_TBL_HEAD_DATE'] = '日期';
$JLMS_LANGUAGE['_JLMS_HW_TBL_HEAD_ENDDATE'] = '结束日期';
$JLMS_LANGUAGE['_JLMS_HW_TBL_LINK_TITLE'] = '查看作业';
$JLMS_LANGUAGE['_JLMS_HW_TBL_LINK_TITLE_T'] = '检视统计';
$JLMS_LANGUAGE['_JLMS_HW_IMG_DEL_HW'] = '删除作业';
$JLMS_LANGUAGE['_JLMS_HW_IMG_EDIT_HW'] = '编辑作业';
$JLMS_LANGUAGE['_JLMS_HW_IMG_NEW_HW'] = '新建作业';
$JLMS_LANGUAGE['_JLMS_HW_ENTER_HW_NAME'] = '输入作业名称';
$JLMS_LANGUAGE['_JLMS_HW_EDIT_HW'] = '编辑作业任务';
$JLMS_LANGUAGE['_JLMS_HW_CREATE_HW'] = '创建作业任务';
$JLMS_LANGUAGE['_JLMS_HW_SET_INCOMPLETE'] = '标记为未完成';
$JLMS_LANGUAGE['_JLMS_HW_STATUS_COMPLETED'] = '已完成';

//view and stats pages
$JLMS_LANGUAGE['_JLMS_HW_DATE_OF_COMPLETING'] = '完成日期:';
$JLMS_LANGUAGE['_JLMS_HW_HOMEWORK_TASK'] = '作业任务:';
$JLMS_LANGUAGE['_JLMS_HW_HOMEWORK_DATE'] = '作业日期:';
$JLMS_LANGUAGE['_JLMS_HW_HOMEWORK_ENDDATE'] = '完成日期:';
$JLMS_LANGUAGE['_JLMS_HW_FILTER_HW'] = '显示作业';
$JLMS_LANGUAGE['_JLMS_HW_TBL_HEAD_STU'] = '学生';
$JLMS_LANGUAGE['_JLMS_HW_TBL_HEAD_GROUP'] = '小组';
$JLMS_LANGUAGE['_JLMS_HW_FILTER_ALL_RESULTS'] = '全部';
$JLMS_LANGUAGE['_JLMS_HW_FILTER_ALL_GROUPS'] = '全部小组';
$JLMS_LANGUAGE['_JLMS_HW_FILTER_ALL_STUDENTS'] = '全部用户';
$JLMS_LANGUAGE['_JLMS_HW_LINK_SEND_EMAIL'] = '发送电子邮件';
?>