<?php
/**
* /languages/english/chat.lang.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$JLMS_LANGUAGE['_JLMS_CHAT_TITLE'] = '讨论';
$JLMS_LANGUAGE['_JLMS_CHAT_TBL_HEAD_CHAT'] = '讨论';
$JLMS_LANGUAGE['_JLMS_CHAT_TBL_HEAD_USERS'] = '用户';
$JLMS_LANGUAGE['_JLMS_CHAT_WELCOME_MESSAGE'] = '欢迎来到讨论室!';
$JLMS_LANGUAGE['_JLMS_CHAT_BTN_POST'] = '发布信息';
$JLMS_LANGUAGE['_JLMS_CHAT_COURSE_CHAT'] = '课程讨论';
$JLMS_LANGUAGE['_JLMS_CHAT_GROUP_CHAT'] = '小组讨论';
?>