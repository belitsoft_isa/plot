<?php
/**
* /languages/english/tracking.lang.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/* 1.0.7 */
$JLMS_LANGUAGE['_JLMS_TRACKING_LATEST_COURSE_ACTIVITIES'] = "最新的课程活动";
$JLMS_LANGUAGE['_JLMS_TRACKING_LATEST_COURSE_ACTIVITIES_REPORT'] = "最新的课程活动报告";
$JLMS_LANGUAGE['_JLMS_TRACKING_STATISTICS_REPORTS'] = "统计报表";
$JLMS_LANGUAGE['_JLMS_TRACKING_DOCUMENTS_STATISTICS'] = "文档下载统计";
$JLMS_LANGUAGE['_JLMS_TRACKING_LPATHS_STATISTICS'] = "学习路径统计";
$JLMS_LANGUAGE['_JLMS_TRACKING_QUIZZES_REPORT'] = "考试报告";
$JLMS_LANGUAGE['_JLMS_TRACKING_QUIZZES_ANSWERS_STATISTICS'] = "考试答案统计";

$JLMS_LANGUAGE['_JLMS_TRACKING_NO_STATISTICS'] = "没有可用统计";
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_H_ACTIVITY'] = "活跃度";
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_H_TIME'] = "时间";

/* course activity types */
$JLMS_LANGUAGE['_JLMS_TRACK_ACT_CHAT_MESSAGE'] = "发布讨论";
$JLMS_LANGUAGE['_JLMS_TRACK_ACT_VIEWS'] = "查看";//e.g. Views 'announcements'
$JLMS_LANGUAGE['_JLMS_TRACK_ACT_TAKES'] = "参与";// e.g. takes 'final quiz'


/* 1.0.3 */
$JLMS_LANGUAGE['_JLMS_TRACK_IMG_MONTH_STATS'] = "月度统计";
$JLMS_LANGUAGE['_JLMS_TRACK_IMG_YEAR_STATS'] = "年度统计";
$JLMS_LANGUAGE['_JLMS_TRACK_IMG_MOST_ACTIVE'] = "做活跃的用户";
$JLMS_LANGUAGE['_JLMS_TRACK_IMG_DAILY'] = "日常分布";
$JLMS_LANGUAGE['_JLMS_TRACK_IMG_WEEKLY'] = "每周分布";
$JLMS_LANGUAGE['_JLMS_TRACK_TITLE'] = "追踪";
$JLMS_LANGUAGE['_JLMS_TRACK_CLEAR_TITLE'] = "清除追踪统计";
$JLMS_LANGUAGE['_JLMS_TRACK_TITLE_ACCESS'] = "访问至 ";
$JLMS_LANGUAGE['_JLMS_TRACK_TITLE_1'] = "文档";
$JLMS_LANGUAGE['_JLMS_TRACK_TITLE_2'] = "链接";
$JLMS_LANGUAGE['_JLMS_TRACK_TITLE_3'] = "交流箱";
$JLMS_LANGUAGE['_JLMS_TRACK_TITLE_4'] = "学习路径";
$JLMS_LANGUAGE['_JLMS_TRACK_TITLE_5'] = "作业";
$JLMS_LANGUAGE['_JLMS_TRACK_TITLE_6'] = "公告";
$JLMS_LANGUAGE['_JLMS_TRACK_TITLE_7'] = "会议";
$JLMS_LANGUAGE['_JLMS_TRACK_TITLE_8'] = "讨论";
$JLMS_LANGUAGE['_JLMS_TRACK_TITLE_9'] = "学习路径播放器";
$JLMS_LANGUAGE['_JLMS_TRACK_TITLE_10'] = "论坛";
$JLMS_LANGUAGE['_JLMS_TRACK_TITLE_11'] = "考试";
$JLMS_LANGUAGE['_JLMS_TRACK_TITLE_TOOLS'] = "工具";

$JLMS_LANGUAGE['_JLMS_TRACK_TBL_H_DATE'] = "日期";
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_H_STU'] = "学生";

$JLMS_LANGUAGE['_JLMS_TRACK_TBL_H_DOCS'] = "文档";
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_H_LINKS'] = "链接";
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_H_DROP'] = "交流箱";
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_H_LPATH'] = "学习路径";
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_H_HW'] = "作业";
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_H_ANNOUNC'] = "公告";
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_H_CONF'] = "会议";
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_H_CHAT'] = "讨论";
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_H_LPPLAY'] = "学习路径参与者";
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_H_FORUM'] = "论坛";
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_H_QUIZ'] = "考试";

//page1: documents文件
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_DOC_NAME'] = "文档";
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_DOC_DOWNS'] = "下载";
$JLMS_LANGUAGE['_JLMS_TRACK_TBL_DOC_LAST'] = "上次下载";
$JLMS_LANGUAGE['_JLMS_TRACK_VIEW_DETAILS'] = "查看详情";
$JLMS_LANGUAGE['_JLMS_TRACK_CLEAR_ALL'] = "清除数据库中所有结果。";
$JLMS_LANGUAGE['_JLMS_TRACK_CLEAR_PERIOD'] = "或选择一个时段。";
?>