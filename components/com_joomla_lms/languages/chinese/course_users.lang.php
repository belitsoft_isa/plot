<?php
/**
* /languages/english/course_users.lang.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$JLMS_LANGUAGE['_JLMS_USERS_NOT_ASSIGN_TYPE_USERS'] = '您不可指派该类型用户';
/* 1.0.6 */
$JLMS_LANGUAGE['_JLMS_USERS_ROLE'] = '选择用户角色:';
$JLMS_LANGUAGE['_JLMS_USERS_SELECT_ROLE'] = '请选择用户角色';
$JLMS_LANGUAGE['_JLMS_TBL_HEAD_USERS_ROLE'] = '角色';


/* 1.0.5 */
$JLMS_LANGUAGE['_JLMS_USERS_TBL_HEAD_GROUP'] = '用户群组';

/* 1.0.4 */
$JLMS_LANGUAGE['_JLMS_USERS_LIFETIME_ACC'] = "终生访问 access";
$JLMS_LANGUAGE['_JLMS_USERS_DTD_ACC'] = "截止日期访问";
$JLMS_LANGUAGE['_JLMS_USERS_XDAYS_ACC'] = "X 天访问";
$JLMS_LANGUAGE['_JLMS_USERS_XDAYS_NUMBER'] = "天数:";
/* 1.0.2 */
$JLMS_LANGUAGE['_JLMS_USERS_CSV_DELETE'] = "删除用户";
$JLMS_LANGUAGE['_JLMS_USERS_CSV_ER_LEARNER'] = "用户已经注册该课程。";
$JLMS_LANGUAGE['_JLMS_USERS_CSV_ER_NAME'] = "该用户名的用户已经注册了Joomla 。";
$JLMS_LANGUAGE['_JLMS_USERS_CSV_ER_EMAIL'] = "该邮箱的用户已经注册了Joomla 。";

/* 1.0.0 */
$JLMS_LANGUAGE['_JLMS_USERGROUPS_TITLE'] = '课程用户群';
$JLMS_LANGUAGE['_JLMS_USERS_TITLE'] = '用户';
$JLMS_LANGUAGE['_JLMS_USERS_ASSISTS_TITLE'] = '助理';
$JLMS_LANGUAGE['_JLMS_USERS_TBL_HEAD_GROUPNAME'] = '名称';
$JLMS_LANGUAGE['_JLMS_USERS_TBL_HEAD_GROUPDESCR'] = '说明';
$JLMS_LANGUAGE['_JLMS_USERS_TBL_HEAD_USERNAME'] = '用户名称';
$JLMS_LANGUAGE['_JLMS_USERS_TBL_HEAD_USER_ADDINFO'] = '附加信息';
$JLMS_LANGUAGE['_JLMS_USERS_TBL_HEAD_USER_COMMENT'] = '评论';
$JLMS_LANGUAGE['_JLMS_USERS_TBL_HEAD_USER_ACC_PERIOD'] = '访问期间';
$JLMS_LANGUAGE['_JLMS_USERS_TBL_HEAD_USER_GROUP'] = '群组';
$JLMS_LANGUAGE['_JLMS_USERS_TBL_HEAD_LOG_USER'] = '用户信息';
$JLMS_LANGUAGE['_JLMS_USERS_TBL_HEAD_LOG_RESULT'] = '结果';
$JLMS_LANGUAGE['_JLMS_USERS_GROUP_LINK_TITLE'] = '查看用户';
$JLMS_LANGUAGE['_JLMS_USERGROUP_EDIT_TITLE'] = '编辑用户群组';
$JLMS_LANGUAGE['_JLMS_USERGROUP_NEW_TITLE'] = '新建用户群组';
$JLMS_LANGUAGE['_JLMS_ENTER_USERGROUP_NAME'] = '请输入用户群组名称。';
$JLMS_LANGUAGE['_JLMS_USER_ADD_TITLE'] = '添加用户';
$JLMS_LANGUAGE['_JLMS_USER_ASSIST_ADD_TITLE'] = '添加助理';
$JLMS_LANGUAGE['_JLMS_USER_IMPORT_TITLE'] = '从CSV文件导入用户';
$JLMS_LANGUAGE['_JLMS_USER_EDIT_TITLE'] = '编辑用户';
$JLMS_LANGUAGE['_JLMS_USER_IMPORT_LOG_TITLE'] = '导入用户群日志';
$JLMS_LANGUAGE['_JLMS_USER_INFO'] = '用户信息:';
$JLMS_LANGUAGE['_JLMS_USER_GROUP_INFO'] = '群组:';
$JLMS_LANGUAGE['_JLMS_USER_USERNAME'] = '用户名称:';
$JLMS_LANGUAGE['_JLMS_USER_SELECT_USER'] = '请选择用户。';
$JLMS_LANGUAGE['_JLMS_USER_NAME'] = '名称:';
$JLMS_LANGUAGE['_JLMS_USER_EMAIL'] = '电邮:';
$JLMS_LANGUAGE['_JLMS_USER_PASS'] = '密码:';
$JLMS_LANGUAGE['_JLMS_USER_ADD_TO_GROUP'] = '添加至群组';
//toolbar alt's and titles
$JLMS_LANGUAGE['_JLMS_USER_ALT_ADDUSER'] = '添加用户';
$JLMS_LANGUAGE['_JLMS_USER_ALT_DELUSER'] = '删除用户';
$JLMS_LANGUAGE['_JLMS_USER_ALT_EDITUSER'] = '编辑用户';
$JLMS_LANGUAGE['_JLMS_USER_ALT_EXPGROUP'] = '导出用户群';
$JLMS_LANGUAGE['_JLMS_USER_ALT_NEWGROUP'] = '新建用户群';
$JLMS_LANGUAGE['_JLMS_USER_ALT_DELGROUP'] = '删除用户群';
$JLMS_LANGUAGE['_JLMS_USER_ALT_EDITGROUP'] = '编辑用户群';

$JLMS_LANGUAGE['_JLMS_USER_NO_GROUP_NAME'] = '未加入群组的用户';
//teacher assistants
$JLMS_LANGUAGE['_JLMS_USER_ASSIST_GROUP_NAME'] = '教师助理';
$JLMS_LANGUAGE['_JLMS_USER_ASSIST_GROUP_DESCR'] = '该群组由系统建立';

$JLMS_LANGUAGE['_JLMS_USERS_IND_GROUP_CHAT'] = '单个群组讨论:';
$JLMS_LANGUAGE['_JLMS_USERS_IND_GROUP_FORUM'] = '单个群组论坛:';

//users delete
$JLMS_LANGUAGE['_JLMS_USERS_DEL_TITLE'] = '删除用户';
$JLMS_LANGUAGE['_JLMS_USERS_DEL_ALERT_MESSAGE'] = "以下列表内的用户将从当前课程中予以删除。用户的统计及成绩也会被删除。用户仍可访问本站。如果想把用户在系统中完全删除，请联系网站管理员。如果你只想把该用户从群组中删除，请更改此用户信息并改变群组。 点击'是'继续删除。";
$JLMS_LANGUAGE['_JLMS_USERS_DEL_A_ALERT_MESSAGE'] = "以下列表内的助理将会从当前课程中予以删除，但他们仍可访问本站。点击'是'继续。";
?>