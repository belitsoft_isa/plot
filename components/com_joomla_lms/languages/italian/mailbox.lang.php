<?php
/**
* /languages/italian/mailbox.lang.php
* Joomla LMS Component
* * * ElearningForce DK
**/

// no direct access
defined( '_VALID_MOS' ) or die( 'Restricted access' );

/* 1.0.5 */
$JLMS_LANGUAGE['_JLMS_MB_MK_READ'] = 'Make read';
$JLMS_LANGUAGE['_JLMS_MB_MK_UNREAD'] = 'Make unread';
$JLMS_LANGUAGE['_JLMS_MB_TO'] = 'To';
$JLMS_LANGUAGE['_JLMS_MB_DATE'] = 'Date';
$JLMS_LANGUAGE['_JLMS_MB_SENDER'] = 'Sender';
$JLMS_LANGUAGE['_JLMS_MB_RECEPIENTS'] = 'Recepients';
$JLMS_LANGUAGE['_JLMS_COMPOSE_ALT_TITLE'] = 'Compose';
$JLMS_LANGUAGE['_JLMS_INBOX_ALT_TITLE'] = 'Inbox';
$JLMS_LANGUAGE['_JLMS_OUTBOX_ALT_TITLE'] = 'Outbox';
$JLMS_LANGUAGE['_JLMS_MB_SEL_ITEM'] = 'Please select an item';
$JLMS_LANGUAGE['_JLMS_MB_DEL_ITEM'] = 'Do you realy want to delete?';
$JLMS_LANGUAGE['_JLMS_MB_ENTER_USERNAME'] = 'Please select recepient';
$JLMS_LANGUAGE['_JLMS_MB_ALL_USRS'] = 'All users';
$JLMS_LANGUAGE['_JLMS_MB_ATTACHMENT'] = 'Atachment :';


$JLMS_LANGUAGE['_JLMS_MB_TITLE'] = 'Casella e-mail';

$JLMS_LANGUAGE['_JLMS_MB_TBL_HEAD_STU'] = 'Studente';
$JLMS_LANGUAGE['_JLMS_MB_TBL_HEAD_GROUP'] = 'Gruppo';
$JLMS_LANGUAGE['_JLMS_MB_FILTER_ALL_RESULTS'] = 'Tutti';
$JLMS_LANGUAGE['_JLMS_MB_FILTER_ALL_GROUPS'] = 'Tutti i gruppi';
$JLMS_LANGUAGE['_JLMS_MB_FILTER_ALL_STUDENTS'] = 'Tutti gli usuari';
$JLMS_LANGUAGE['_JLMS_MB_SELECT_A_USER'] = 'Selezionare un usuario.';
$JLMS_LANGUAGE['_JLMS_MB_SUBJECT'] = 'Materia';
$JLMS_LANGUAGE['_JLMS_MB_TEXT'] = 'Testo';
$JLMS_LANGUAGE['_JLMS_MB_ATTACH'] = 'Allega un file';
$JLMS_LANGUAGE['_JLMS_MB_HIDE'] = 'Nascondi';
$JLMS_LANGUAGE['_JLMS_MB_ENTER_SUBJECT'] = 'Inserire materia';
$JLMS_LANGUAGE['_JLMS_MB_TEXT_CLEAR'] = 'Il campo di testo � vuoto. Desideri continuare?';
$JLMS_LANGUAGE['_JLMS_MB_TEXT_CHOOSE'] = 'Scegli gli usuari a cui mandare e-mail :';
$JLMS_LANGUAGE['_JLMS_MB_SENT_SUCCESS'] = 'E-mail inviata con successo!';
$JLMS_LANGUAGE['_JLMS_MB_SENT_ERROR'] = 'E-mail inviata con degli errori!';
?>