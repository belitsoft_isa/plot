<?php
/**
* joomla_lms.tracking.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );
require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.tracking.html.php");
require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_grades.lib.php");

$task 	= mosGetParam( $_REQUEST, 'task', '' );

if ($task != 'get_lp_stats' && $task != 'get_docs_stats' && $task != 'get_quiz_stats' && (mosGetParam($_REQUEST,'view') != 'csv' && mosGetParam($_REQUEST,'view') != 'xls')){

	global $JLMS_CONFIG;

	$course_id = $JLMS_CONFIG->get('course_id',0);
	$pathway = array();
	$pathway[] = array('name' => _JLMS_PATHWAY_HOME, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"), 'is_home' => true);
	$pathway[] = array('name' => $JLMS_CONFIG->get('course_name'), 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$course_id"), 'is_course' => true);
	$pathway[] = array('name' => _JLMS_TOOLBAR_TRACK, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=tracking&amp;id=$course_id"));
	JLMSAppendPathWay($pathway);
	JLMS_ShowHeading();
}

$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) ); 

//echo $task; die;

switch ($task) {
	case 'tracking':
		global $JLMS_CONFIG;
		$mem_lim = $JLMS_CONFIG->get('memory_limit_gradebook', 0);
		if ($mem_lim) {
			JLMS_adjust_memory_limit($mem_lim);
		}
		$page 	= intval(mosGetParam( $_REQUEST, 'page', 0 ));
		if (!in_array($page,array(1,2,3,4,5,6,7,8,9,10,11,12,13,14))) { $page = 0; }
		//12 - documents downloads statistics
		//13 - learning paths statistics
		//14 - latest course activities report

		$doc = JFactory::getDocument();
		$tracking_styles = '.jlms_users_box_track { width: 250px !important; }';
		if (method_exists($doc, 'addStyleDeclaration')) {
			$doc->addStyleDeclaration( $tracking_styles );
		}

		switch ($page) {
			case 0:		JLMS_showTracking( $id, $option );				break;
			default:	JLMS_showpageTracking( $id, $option, $page );	break;
		}
	break;
	case 'track_clear':		JLMS_showTR_clearPage( $id, $option );		break;
	case 'track_do_clear':	JLMS_clearTracking( $id, $option );			break;
	case 'get_lp_stats':	JLMS_ajaxGetLPStatsXML( $id, $option );		break;
	case 'get_docs_stats':	JLMS_ajaxGetDOCSStatsXML( $id, $option );	break;
	case 'get_quiz_stats':	JLMS_ajaxGetQUIZStatsXML( $id, $option );	break;
	
}

function helperChilds(&$steps=array(), $step_id, $childs=array()){
	if(count($steps)){
		foreach($steps as $step){
			if($step_id == $step->parent_id){
				$childs[] = $step->id;
				$childs = helperChilds($steps, $step->id, $childs);
			}
		}
	}
	return $childs;
}

function JLMS_ajaxGetQUIZStatsXML( $course_id, $option ){
	global $my, $JLMS_DB, $JLMS_CONFIG, $JLMS_SESSION;
	
	$JLMS_ACL = JLMSFactory::getACL();
	
	$assigned_groups_only = $JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only');
	
	$groups_mode = $JLMS_CONFIG->get('use_global_groups', 1);
	
	$filt_group = intval( mosGetParam( $_GET, 'filt_group', $JLMS_SESSION->get('filt_group', 0) ) );
	$filt_subgroup = intval( mosGetParam( $_GET, 'filt_subgroup', $JLMS_SESSION->get('filt_subgroup', 0) ) );

	if(!$filt_group) {
		$filt_subgroup = 0;
	}
	
	$members = "'0'";
	$members_array = array();
	if($JLMS_ACL->_role_type == 2 || $JLMS_ACL->_role_type == 4) {
		if($assigned_groups_only) {
			$groups_where_admin_manager = JLMS_ACL_HELPER::GetAssignedGroups($my->id, $course_id);
				
			if(count($groups_where_admin_manager) == 1) {
				$filt_group = $groups_where_admin_manager[0];
			}
			$groups_where_admin_manager = implode(',', $groups_where_admin_manager);
				if($groups_where_admin_manager != '') {
					$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE (group_id IN ($groups_where_admin_manager) OR subgroup1_id IN ($groups_where_admin_manager))"
						. ($filt_group ? ("\n AND group_id = '".$filt_group."'") : '')
						. ($filt_subgroup ? ("\n AND subgroup1_id = '".$filt_subgroup."'") : '')
					;
					
					$JLMS_DB->setQuery($query);
					$members = JLMSDatabaseHelper::LoadResultArray();
					
					$members_array = $members;
					
					$members = implode(',', $members);
					if($members == '')
						$members = "'0'";
				}		
		}
	}
	
	$quiz_id = intval( mosGetParam( $_REQUEST, 'quiz_id', 0 ) );
	$user_id = intval( mosGetParam( $_REQUEST, 'user_id', 0 ) );
	$cont_msg = '';
	$query = "SELECT user_id FROM #__lms_users_in_groups WHERE course_id = $course_id"
			. ($assigned_groups_only ? ("\n AND user_id IN ($members)") :'')
			;
		
	$JLMS_DB->SetQuery( $query );
		$uids = JLMSDatabaseHelper::LoadResultArray();
		$query = "SELECT user_id FROM #__lms_user_courses WHERE course_id = $course_id"
				. ($assigned_groups_only ? ("\n AND user_id IN ($members)") :'')
				;
				
		$JLMS_DB->SetQuery( $query );
		$uids = array_merge($uids,JLMSDatabaseHelper::LoadResultArray());
		if(count($uids)){
			$uids_str = implode(',',$uids);
			$query = "SELECT * FROM #__lms_quiz_results as r, #__users as u ";
			
			if($groups_mode && $filt_group) {
				$query .= ", #__lms_users_in_global_groups as d";	
			}
			
			$query .= " WHERE r.course_id = $course_id AND r.user_id = u.id AND quiz_id='".$quiz_id."' AND r.user_id IN (".$uids_str.")";
			
			if($groups_mode && $filt_group) {
				$query .= " AND d.user_id = u.id"
				. ($filt_group ? ("\n AND d.group_id = '".$filt_group."'") : '')
				. ($filt_subgroup ? ("\n AND d.subgroup1_id = '".$filt_subgroup."'") : '')
			;
			}	
			
			
			$JLMS_DB->SetQuery( $query );
			$usr_res = $JLMS_DB->LoadObjectList();
			if(count($usr_res))
			{
				$cont_msg .= '<table width="100%" cellspacing="0" cellpadding="0">';
			}
			$k=0;
			for($i=0,$n=count($usr_res);$i<$n;$i++){
				$r_img = 'btn_cancel';
				$r_sta = _JLMS_LPATH_STU_LPSTATUS_NOTCOMPLETED;
				if($usr_res[$i]->user_passed){
					$r_img = 'btn_accept';
					$r_sta = _JLMS_LPATH_STU_LPSTATUS_COMPLETED;
				}
				$cont_msg .= '<tr class="sectiontableentry'.($k%2+1).'"><td width="16px">&nbsp;</td><td width="16px"><img class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/btn_edit_user.png" width=\'16\' height=\'16\' alt="user" title="user" /></td><td><b>'.$usr_res[$i]->username.', '.$usr_res[$i]->name.'</b></td>';
				
				$cont_msg .= '<td valign="middle" align="center" width="16">';
						$cont_msg .= "<img class='JLMS_png' src='".$JLMS_CONFIG->get('live_site')."/components/com_joomla_lms/lms_images/toolbar/".$r_img.".png' width='16' height='16' alt='".$r_sta."' title='".$r_sta."' />";
				$cont_msg .= '</td><td align="left" style="padding-left:80px;" width="190">'.$usr_res[$i]->user_score.'</td>';
				$cont_msg .='</tr>';
				$k++;
			}
			if(count($usr_res))
			{
				$cont_msg .= '</table>';
			}
		}
		$iso = explode( '=', _ISO );
		echo "\n"."debug mode";
		$debug_str = ' ';
		ob_end_clean();
		header ('Expires: Fri, 14 Mar 1980 20:53:00 GMT');
		header ('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header ('Cache-Control: no-cache, must-revalidate');
		header ('Pragma: no-cache');
		if (class_exists('JFactory')) {
			$document=JFactory::getDocument();
			$charset_xml = $document->getCharset();
			header ('Content-Type: text/xml; charset='.$charset_xml);
		} else {
			header ('Content-Type: text/xml');
		}
		echo '<?xml version="1.0" encoding="'.$iso[1].'" standalone="yes"?>';
		echo '<response>' . "\n";
		echo "\t" . '<task>lpath_xml</task>' . "\n";
		echo "\t" . '<user_id>'.$uids_str.'</user_id>' . "\n";
		echo "\t" . '<quiz_id>'.$quiz_id.'</quiz_id>' . "\n";
		echo "\t" . '<stats_table><![CDATA['.($cont_msg?$cont_msg:' ').']]></stats_table>' . "\n";
		echo "\t" . '<debug><![CDATA['.$debug_str.']]></debug>' . "\n";
		echo '</response>' . "\n";
		exit();
}

function JLMS_ajaxGetLPStatsXML( $course_id, $option ) {
	global $my, $JLMS_DB, $JLMS_CONFIG, $JLMS_SESSION;
	
	$JLMS_ACL = JLMSFactory::getACL();
	$Itemid = $JLMS_CONFIG->get('Itemid');
	
	$assigned_groups_only = $JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only');
	
	$groups_mode = $JLMS_CONFIG->get('use_global_groups', 1);
	
	$filt_group = intval( mosGetParam( $_GET, 'filt_group', $JLMS_SESSION->get('filt_group', 0) ) );
	$filt_subgroup = intval( mosGetParam( $_GET, 'filt_subgroup', $JLMS_SESSION->get('filt_subgroup', 0) ) );
	$filter_lpath = JRequest::getVar('filter_lpath', 0);	

	if(!$filt_group) {
		$filt_subgroup = 0;
	}
	
	$members = "'0'";
	$members_array = array();
	if($JLMS_ACL->_role_type == 2 || $JLMS_ACL->_role_type == 4) {
		if($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only')) {
				$groups_where_admin_manager = JLMS_ACL_HELPER::GetAssignedGroups($my->id, $course_id);
				
			if(count($groups_where_admin_manager)) {
				if(JLMS_ACL_HELPER::GetCountAssignedGroups($my->id, $course_id) == 1) {	
					$filt_group = $groups_where_admin_manager[0];
				}
			}
			
			if(count($groups_where_admin_manager)) {
				$groups_where_admin_manager = implode(',', $groups_where_admin_manager);
					if($groups_where_admin_manager != '') {
						$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE (group_id IN ($groups_where_admin_manager) OR subgroup1_id IN ($groups_where_admin_manager))"
							. ($filt_group ? ("\n AND group_id = '".$filt_group."'") : '')
							. ($filt_subgroup ? ("\n AND subgroup1_id = '".$filt_subgroup."'") : '')
						;
						
						$JLMS_DB->setQuery($query);
						$members = JLMSDatabaseHelper::LoadResultArray();
						
						$members_array = $members;
						
						$members = implode(',', $members);
						if($members == '')
							$members = "'0'";
					}
			}
			else {
				$groups_where_admin_manager = "'0'";
			}
		}
	}
	
	$usertype = JLMS_GetUserType($my->id, $course_id);
	$lpath_id = intval( mosGetParam( $_REQUEST, 'lpath_id', 0 ) );
	$user_id = intval( mosGetParam( $_REQUEST, 'user_id', 0 ) );
	$mode = strval( mosGetParam( $_REQUEST, 'mode', '' ) );
	
	require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_scorm.lib.php");

	/* *
	 * 04.10.2007 (DEN) - Tom task - possibility to mark incompleted SCORM as completed (and otherwise)
	 * This would be works only if there is any information about progress (for any user) in the tracking table for this SCORM
	 * (names of tracking parameters are taken from other users tracking)
	 */
	
	if ($mode && $mode == 'scormstatus') {
		$nstat = intval( mosGetParam( $_REQUEST, 'nstat', 0 ) );
		if ($nstat) { $nstat = 1; }

		$query = "SELECT user_id FROM #__lms_users_in_groups WHERE course_id = $course_id"
		. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND user_id IN ($members)") :'')
		;
		$JLMS_DB->SetQuery( $query );
		$uids = JLMSDatabaseHelper::LoadResultArray();
		$query = "SELECT user_id FROM #__lms_user_courses WHERE course_id = $course_id"
		. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND user_id IN ($members)") :'')
		;
		$JLMS_DB->SetQuery( $query );
		$uids = array_merge($uids,JLMSDatabaseHelper::LoadResultArray());
		$XML_data = '&nbsp;';
		if (in_array($user_id, $uids)) {
			$uids1 = array();
			$uids1[] = $user_id;
			$course_params = $JLMS_CONFIG->get('course_params');
			$params_tt = new JLMSParameters($course_params);
			$scorm_ans = & JLMS_Get_N_SCORM_SCO_userResults($uids1, $lpath_id, $params_tt->get('track_type', 0));
			$user_status = 0; $tmp_var = false;
			foreach ($scorm_ans as $sa) {
				foreach ($sa->track_data as $trd) {
					if ($trd->user_id == $user_id) {
						if (($trd->element == 'cmi.completion_status') || ($trd->element == 'cmi.core.lesson_status') || ($trd->element == 'cmi.core.completion_status') || ($trd->element == 'cmi.lesson_status')) {
							if (($trd->value == 'completed') || ($trd->value == 'passed')) {
								if (!$tmp_var) {
									$user_status = 1;
								}
							} else {
								$tmp_var = true;
								$user_status = 0;
							}
						}
					}
				}
			}

			$alt = ($user_status)?_JLMS_LPATH_STU_LPSTATUS_COMPLETED:_JLMS_LPATH_STU_LPSTATUS_NOTCOMPLETED;
			$image = ($user_status)?'btn_accept.png':'btn_cancel.png';
			$new_s_status = ($user_status) ? '0' : '1';

			$XML_data = '<a class="jlms_img_link" id="sc_progress_a_'.$user_id.'_'.$lpath_id.'" href="javascript:ChangeScormProgress('.$user_id.','.$lpath_id.','.$new_s_status.',this);"><img id="sc_progress_img_'.$user_id.'_'.$lpath_id.'" align="absmiddle" class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/'.$image.'" width="16" height="16" border="0" alt="'.$alt.'" title="'.$alt.'" /></a>';

			if (($user_status && !$nstat) || (!$user_status && $nstat)) {
				$query = "SELECT distinct element, scoid FROM #__lms_n_scorm_scoes_track WHERE scormid = $lpath_id AND element IN ('cmi.completion_status', 'cmi.core.lesson_status', 'cmi.core.completion_status', 'cmi.lesson_status')";
				$JLMS_DB->SetQuery($query);
				$stats_sn = $JLMS_DB->LoadObjectList();
				if (!empty($stats_sn)) {
					$query = "SELECT distinct element, scoid FROM #__lms_n_scorm_scoes_track WHERE userid = $user_id AND scormid = $lpath_id AND element IN ('cmi.completion_status', 'cmi.core.lesson_status', 'cmi.core.completion_status', 'cmi.lesson_status')";
					$JLMS_DB->SetQuery($query);
					$stats_user_sn = $JLMS_DB->LoadObjectList();
					$added_stats = array();
					if (!empty($stats_user_sn)) {
						foreach ($stats_sn as $stsn) {
							$present = false;
							foreach ($stats_user_sn as $stusn) {
								if ($stusn->element == $stsn->element && $stusn->scoid == $stsn->scoid) {
									$present = true; break;
								}
							}
							if (!$present) {
								$added_stats[] = $stsn;
							}
						}
					} else {
						$added_stats = $stats_sn;
					}
					$query = "SELECT max(attempt) FROM #__lms_n_scorm_scoes_track WHERE scormid = $lpath_id AND userid = $user_id";
					$JLMS_DB->SetQuery($query);
					$attempt = $JLMS_DB->LoadResult();
					if ($nstat) {
						if (!$attempt) { $attempt = 1; }
						if (!empty($added_stats)) {
							foreach ($added_stats as $as) {
								$query = "INSERT INTO #__lms_n_scorm_scoes_track (userid, scormid, scoid, attempt, element, value, timemodified) VALUES ($user_id, $lpath_id, $as->scoid, $attempt, ".$JLMS_DB->Quote($as->element).", 'completed', '".(time() - date('Z'))."')";
								$JLMS_DB->SetQuery($query);
								$JLMS_DB->query();
							}
						}
					}
					$new_set_stat = $nstat ? 'completed' : 'incomplete';

					$query = "UPDATE #__lms_n_scorm_scoes_track SET value = '$new_set_stat', timemodified = '".(time() - date('Z'))."' WHERE scormid = $lpath_id AND userid = $user_id AND attempt = $attempt AND element IN ('cmi.completion_status', 'cmi.core.lesson_status', 'cmi.core.completion_status', 'cmi.lesson_status')";
					$JLMS_DB->SetQuery($query);
					$JLMS_DB->query();
				}
			}

			$scorm_ans = & JLMS_Get_N_SCORM_SCO_userResults($uids1, $lpath_id, $params_tt->get('track_type', 0));
			$user_status = 0; $tmp_var = false;
			foreach ($scorm_ans as $sa) {
				foreach ($sa->track_data as $trd) {
					if ($trd->user_id == $user_id) {
						if (($trd->element == 'cmi.completion_status') || ($trd->element == 'cmi.core.lesson_status') || ($trd->element == 'cmi.core.completion_status') || ($trd->element == 'cmi.lesson_status')) {
							if (($trd->value == 'completed') || ($trd->value == 'passed')) {
								if (!$tmp_var) {
									$user_status = 1;
								}
							} else {
								$tmp_var = true;
								$user_status = 0;
							}
						}
					}
				}
			}

			$alt = ($user_status)?_JLMS_LPATH_STU_LPSTATUS_COMPLETED:_JLMS_LPATH_STU_LPSTATUS_NOTCOMPLETED;
			$image = ($user_status)?'btn_accept.png':'btn_cancel.png';
			$new_s_status = ($user_status) ? '0' : '1';

			$XML_data = '<a class="jlms_img_link" id="sc_progress_a_'.$user_id.'_'.$lpath_id.'" href="javascript:ChangeScormProgress('.$user_id.','.$lpath_id.','.$new_s_status.',this, \'scormstatus\');"><img id="sc_progress_img_'.$user_id.'_'.$lpath_id.'" align="absmiddle" class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/'.$image.'" width="16" height="16" border="0" alt="'.$alt.'" title="'.$alt.'" /></a>';
			
			
			$SCC = new JLMS_SCORMCourseComplete();
			$SCC->getInit($user_id, $course_id);
		}
		$iso = explode( '=', _ISO );
		echo "\n"."debug mode";
		$debug_str = ' ';
		if (true || $JLMS_CONFIG->get('debug_mode', false)) {
			$debug_str = ob_get_contents();
		}
		ob_end_clean();
		header ('Expires: Fri, 14 Mar 1980 20:53:00 GMT');
		header ('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header ('Cache-Control: no-cache, must-revalidate');
		header ('Pragma: no-cache');
		if (class_exists('JFactory')) {
			$document=JFactory::getDocument();
			$charset_xml = $document->getCharset();
			header ('Content-Type: text/xml; charset='.$charset_xml);
		} else {
			header ('Content-Type: text/xml');
		}
		echo '<?xml version="1.0" encoding="'.$iso[1].'" standalone="yes"?>';
		echo '<response>' . "\n";
		echo "\t" . '<task>scorm_progress_xml</task>' . "\n";
		echo "\t" . '<user_id>'.$user_id.'</user_id>' . "\n";
		echo "\t" . '<lpath_id>'.$lpath_id.'</lpath_id>' . "\n";
		echo "\t" . '<stats_table><![CDATA['.($XML_data?$XML_data:' ').']]></stats_table>' . "\n";
		echo "\t" . '<debug><![CDATA['.$debug_str.']]></debug>' . "\n";
		echo '</response>' . "\n";
		JLMS_die();
	}	/* END of 'Set SCORM progress' mod */
	
	if($mode && $mode == 'lpathstatus'){
		$nstat = intval( mosGetParam( $_REQUEST, 'nstat', 0 ) );
		//if ($nstat) { $nstat = 1; }
	
		$query = "SELECT user_id FROM #__lms_users_in_groups WHERE course_id = $course_id"
		. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND user_id IN ($members)") :'')
		;
		$JLMS_DB->SetQuery( $query );
		$uids = JLMSDatabaseHelper::LoadResultArray();
		$query = "SELECT user_id FROM #__lms_user_courses WHERE course_id = $course_id"
		. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND user_id IN ($members)") :'')
		;
		$JLMS_DB->SetQuery( $query );
		$uids = array_merge($uids,JLMSDatabaseHelper::LoadResultArray());
		
		$XML_data = '&nbsp;';
		if (in_array($user_id, $uids)) {
			
			$query = "SELECT * FROM #__lms_learn_path_results WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND user_id = '".$user_id."'";
			$JLMS_DB->setQuery($query);
			$lpath_result = $JLMS_DB->loadObject();
			
			$query = "SELECT * FROM #__lms_learn_path_steps WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
			$JLMS_DB->setQuery($query);
			$steps = $JLMS_DB->loadObjectList();
			
			if($nstat == 1){
				if(isset($lpath_result->course_id) && isset($lpath_result->user_id) && $lpath_result->user_id){
					$query = "UPDATE #__lms_learn_path_results SET user_status = '".$nstat."' WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND user_id = '".$user_id."'";
					$JLMS_DB->setQuery($query);
					if($JLMS_DB->query()){ //fix
						$query = "UPDATE #__lms_learn_path_grades SET user_status = '".$nstat."' WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND user_id = '".$user_id."'";
						$JLMS_DB->setQuery($query);
						$JLMS_DB->query();
					}
					
					if(isset($steps) && count($steps)){
						foreach($steps as $step){
							$query = "SELECT * FROM #__lms_learn_path_step_results WHERE result_id = '".$lpath_result->id."' AND step_id = '".$step->id."'";
							$JLMS_DB->setQuery($query);
							$step_obj = $JLMS_DB->loadObject();
							
							if(isset($step_obj) && isset($step_obj->id)){
								$query = "UPDATE #__lms_learn_path_step_results SET step_status = '".$nstat."' WHERE result_id = '".$result_id."' AND step_id = '".$step->id."'";
								$JLMS_DB->setQuery($query);
								if($JLMS_DB->query()){
									//JLMS_updateSpentTime($course_id, $lpath_id, $step->id, $user_id);
								}
							} else {
								$query = "INSERT INTO #__lms_learn_path_step_results (id, result_id, step_id, step_status)"
								. "\n VALUES"
								. "\n ('', ".$lpath_result->id.", ".$step->id.", ".$nstat.")"
								;
								$JLMS_DB->setQuery($query);
								if($JLMS_DB->query()){
									//JLMS_writeSpentTime($course_id, $lpath_id, $step->id, $user_id);
								}
							}
						}
					}
				} else {
					$query = "INSERT INTO #__lms_learn_path_results"
					. "\n (id, course_id, lpath_id, user_id, user_points, user_time, user_status, start_time, end_time)"
					. "\n VALUES"
					. "\n ('', ".$course_id.", ".$lpath_id.", ".$user_id.", 0, 0, ".$nstat.", '', '')"
					;
					$JLMS_DB->setQuery($query);
					$JLMS_DB->query();
					$result_id = $JLMS_DB->insertid();
					if(true){ //fix
						$query = "INSERT INTO #__lms_learn_path_grades"
						. "\n (id, course_id, lpath_id, user_id, user_points, user_time, user_status, start_time, end_time)"
						. "\n VALUES"
						. "\n ('', ".$course_id.", ".$lpath_id.", ".$user_id.", 0, 0, ".$nstat.", '', '')"
						;
						$JLMS_DB->setQuery($query);
						$JLMS_DB->query();
					}
					
					if($result_id){
						if(isset($steps) && count($steps)){
							foreach($steps as $step){
								$query = "SELECT * FROM #__lms_learn_path_step_results WHERE result_id = '".$result_id."' AND step_id = '".$step->id."'";
								$JLMS_DB->setQuery($query);
								$step_obj = $JLMS_DB->loadObject();
								
								if(isset($step_obj) && isset($step_obj->id)){
									$query = "UPDATE #__lms_learn_path_step_results SET step_status = '".$nstat."' WHERE result_id = '".$result_id."' AND step_id = '".$step->id."'";
									$JLMS_DB->setQuery($query);
									if($JLMS_DB->query()){
										//JLMS_updateSpentTime($course_id, $lpath_id, $step->id, $user_id);
									}
								} else {
									$query = "INSERT INTO #__lms_learn_path_step_results (id, result_id, step_id, step_status)"
									. "\n VALUES"
									. "\n ('', ".$result_id.", ".$step->id.", ".$nstat.")"
									;
									$JLMS_DB->setQuery($query);
									if($JLMS_DB->query()){
										//JLMS_writeSpentTime($course_id, $lpath_id, $step->id, $user_id);
									}
								}
							}
						}
						
					}
				}
			} elseif($nstat == 2){
				if(isset($lpath_result->id) && $lpath_result->id){
					$query = "DELETE FROM #__lms_learn_path_step_results WHERE result_id = '".$lpath_result->id."'";
					$JLMS_DB->setQuery($query);
					if($JLMS_DB->query()){
						$step_ids = array();
						foreach($steps as $step){
							$step_ids[] = $step->id;
						}
						//JLMS_deleteSpentTime($course_id, $user_id, $lpath_id, $step_ids);
					}
				}
				//if(!isset($lpath_result->last_step_id) || !$lpath_result->last_step_id){
					$query = "DELETE FROM #__lms_learn_path_results WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND user_id = '".$user_id."'";
					$JLMS_DB->setQuery($query);
					if($JLMS_DB->query()){ //fix
						$query = "DELETE FROM #__lms_learn_path_grades WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND user_id = '".$user_id."'";
						$JLMS_DB->setQuery($query);
						$JLMS_DB->query();
					}
					
				//}
			}
			$user_status = $nstat;
			
			$SCC = new JLMS_SCORMCourseComplete();
			$SCC->getInit($user_id, $course_id);
			
			$noredirect = JRequest::getVar('noredirect', 0);
			if(!$noredirect){
				$app = JFactory::getApplication();
				$link_redirect = 'index.php?option=com_joomla_lms&Itemid='.$Itemid.'&task=tracking&page=13&id='.$course_id;
				if($filter_lpath){
					$link_redirect .= '&filter_lpath='.$filter_lpath;
				}
				$link_redirect .= '&filter_stu='.$user_id;
				$app->redirect($link_redirect);
			}
		}
		
		$alt = ($user_status)?_JLMS_LPATH_STU_LPSTATUS_COMPLETED:_JLMS_LPATH_STU_LPSTATUS_NOTCOMPLETED;
		$image = ($user_status == 1)?'btn_accept.png':'btn_cancel.png';
		$new_s_status = ($user_status == 1) ? '2' : '1';
	
		$XML_data = '<a class="jlms_img_link" id="sc_progress_a_'.$user_id.'_'.$lpath_id.'" href="javascript:ChangeScormProgress('.$user_id.','.$lpath_id.','.$new_s_status.',this, \'lpathstatus\');"><img id="sc_progress_img_'.$user_id.'_'.$lpath_id.'" align="absmiddle" class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/'.$image.'" width="16" height="16" border="0" alt="'.$alt.'" title="'.$alt.'" /></a>';
		
		$iso = explode( '=', _ISO );
		echo "\n"."debug mode";
		$debug_str = ' ';
		if (true || $JLMS_CONFIG->get('debug_mode', false)) {
			$debug_str = ob_get_contents();
		}
		ob_end_clean();
		header ('Expires: Fri, 14 Mar 1980 20:53:00 GMT');
		header ('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header ('Cache-Control: no-cache, must-revalidate');
		header ('Pragma: no-cache');
		if (class_exists('JFactory')) {
			$document=JFactory::getDocument();
			$charset_xml = $document->getCharset();
			header ('Content-Type: text/xml; charset='.$charset_xml);
		} else {
			header ('Content-Type: text/xml');
		}
		echo '<?xml version="1.0" encoding="'.$iso[1].'" standalone="yes"?>';
		echo '<response>' . "\n";
		echo "\t" . '<task>scorm_progress_xml</task>' . "\n";
		echo "\t" . '<user_id>'.$user_id.'</user_id>' . "\n";
		echo "\t" . '<lpath_id>'.$lpath_id.'</lpath_id>' . "\n";
		echo "\t" . '<stats_table><![CDATA['.($XML_data?$XML_data:' ').']]></stats_table>' . "\n";
		echo "\t" . '<debug><![CDATA['.$debug_str.']]></debug>' . "\n";
		echo '</response>' . "\n";
		JLMS_die();	
	}
	
	if($mode && $mode == 'lpathstepstatus'){
		$app = JFactory::getApplication();
		$db = JFactory::getDBO();
		
		$lpath_id = JRequest::getVar('lpath_id', 0);
		$user_id = JRequest::getVar('user_id', 0);
		$cid = JRequest::getVar('cid', array());
		$status = JRequest::getVar('status', 0);
		
		$query = "SELECT * FROM #__lms_learn_path_steps WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
		$db->setQuery($query);
		$steps = $db->loadObjectList();
		
		$childs = array();
		if(count($cid)){
			foreach($cid as $step_id){
				$childs[$step_id] = array();
				$childs[$step_id] = helperChilds($steps, $step_id, $childs[$step_id]);
				
				$query = "SELECT * FROM #__lms_learn_path_results WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND user_id = '".$user_id."'";
				$db->setQuery($query);
				$lpath_result = $db->loadObject();
				if(isset($lpath_result->id) && $lpath_result->id){
					$result_id = $lpath_result->id;
				}
				
				if(isset($result_id)){
					$query = "SELECT * FROM #__lms_learn_path_step_results WHERE result_id = '".$result_id."' AND step_id = '".$step_id."'";
					$db->setQuery($query);
					$step_obj = $db->loadObject();
				}
				
				if($status == 1){
					if(isset($step_obj) && isset($step_obj->id)){
						$query = "UPDATE #__lms_learn_path_step_results SET step_status = '".$status."' WHERE result_id = '".$result_id."' AND step_id = '".$step_id."'";
						$db->setQuery($query);
						if($db->query()){
							//JLMS_updateSpentTime($course_id, $lpath_id, $step_id, $user_id);
						}
					} else {
						if(isset($lpath_result->course_id) && isset($lpath_result->user_id) && $lpath_result->user_id){
							$query = "UPDATE #__lms_learn_path_results SET user_status = '0' WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND user_id = '".$user_id."'";
							$db->setQuery($query);
							if($db->query()){ //fix
								$query = "UPDATE #__lms_learn_path_grades SET user_status = '0' WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND user_id = '".$user_id."'";
								$db->setQuery($query);
								$db->query();
							}
							
						} else {
							$query = "INSERT INTO #__lms_learn_path_results"
							. "\n (id, course_id, lpath_id, user_id, user_points, user_time, user_status, start_time, end_time)"
							. "\n VALUES"
							. "\n ('', ".$course_id.", ".$lpath_id.", ".$user_id.", 0, 0, 0, '', '')"
							;
							$db->setQuery($query);
							$db->query();
							$result_id = $db->insertid();
							if(true){ //fix
								$query = "INSERT INTO #__lms_learn_path_grades"
								. "\n (id, course_id, lpath_id, user_id, user_points, user_time, user_status, start_time, end_time)"
								. "\n VALUES"
								. "\n ('', ".$course_id.", ".$lpath_id.", ".$user_id.", 0, 0, ".$nstat.", '', '')"
								;
								$JLMS_DB->setQuery($query);
								$JLMS_DB->query();
							}
							
						}
						
						$query = "INSERT INTO #__lms_learn_path_step_results (id, result_id, step_id, step_status)"
						. "\n VALUES"
						. "\n ('', ".$result_id.", ".$step_id.", ".$status.")"
						;
						$db->setQuery($query);
						if($db->query()){
							//JLMS_writeSpentTime($course_id, $lpath_id, $step_id, $user_id);
						}
						
						if(isset($childs[$step_id]) && count($childs[$step_id])){
							foreach($childs[$step_id] as $ch_step_id){
								$query = "INSERT INTO #__lms_learn_path_step_results (id, result_id, step_id, step_status)"
								. "\n VALUES"
								. "\n ('', ".$result_id.", ".$ch_step_id.", ".$status.")"
								;
								$db->setQuery($query);
								if($db->query()){
									//JLMS_writeSpentTime($course_id, $lpath_id, $ch_step_id, $user_id);
								}
							}
						}
					}
				} else {
					if(isset($step_obj) && isset($step_obj->id)){
						$query = "DELETE FROM #__lms_learn_path_step_results WHERE result_id = '".$result_id."' AND step_id = '".$step_id."'";
						$db->setQuery($query);
						$db->query();
						
						//JLMS_deleteSpentTime($course_id, $user_id, $lpath_id, array($step_id));
						
						if(isset($childs[$step_id]) && count($childs[$step_id])){
							$query = "DELETE FROM #__lms_learn_path_step_results WHERE result_id = '".$result_id."' AND step_id IN (".implode(',', $childs[$step_id]).")";
							$db->setQuery($query);
							$db->query();
							
							//JLMS_deleteSpentTime($course_id, $user_id, $lpath_id, $childs[$step_id]);
						}
						
						$query = "UPDATE #__lms_learn_path_results SET user_status = '0' WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND user_id = '".$user_id."'";
						$db->setQuery($query);
						if($db->query()){ //fix
							$query = "UPDATE #__lms_learn_path_grades SET user_status = '0' WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND user_id = '".$user_id."'";
							$db->setQuery($query);
							$db->query();
						}
					}
				}
			}
		}
		
		for($i=0;$i<count($steps);$i++){
			$query = "SELECT b.step_status"
			. "\n FROM #__lms_learn_path_results as a, #__lms_learn_path_step_results as b"
			. "\n WHERE a.id = b.result_id"
			. "\n AND a.course_id = '".$steps[$i]->course_id."'"
			. "\n AND a.lpath_id = '".$steps[$i]->lpath_id."'"
			. "\n AND a.user_id = '".$user_id."'"
			. "\n AND b.step_id = '".$steps[$i]->id."'"
			;
			$db->setQuery($query);
			$step_status = $db->loadResult();
			if(!$step_status){
				$step_status = 0;
			}
			$steps[$i]->step_status = $step_status;
		}
		
		$completed = 0;
		foreach($steps as $step){
			if($step->step_status){
				$completed++;
			}
		}
		
		if($status == 1){
			if($completed == count($steps)){
				$query = "UPDATE #__lms_learn_path_results SET user_status = '1' WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND user_id = '".$user_id."'";
				$db->setQuery($query);
				if($db->query()){ //fix
					$query = "UPDATE #__lms_learn_path_grades SET user_status = '1' WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND user_id = '".$user_id."'";
					$db->setQuery($query);
					$db->query();
				}
			}
		} else {
			if(!$completed){
				$query = "DELETE FROM #__lms_learn_path_results WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND user_id = '".$user_id."'";
				$db->setQuery($query);
				if($db->query()){ //fix
					$query = "DELETE FROM #__lms_learn_path_results WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND user_id = '".$user_id."'";
					$db->setQuery($query);
					$db->query();
				}
			}
		}
		
		//$link_redirect = 'index.php?option=com_joomla_lms&task=tracking&page=13&id='.$course_id.'&filter_lpath='.$lpath_id.'&filter_stu='.$user_id;
		$link_redirect = 'index.php?option=com_joomla_lms&Itemid='.$Itemid.'&task=tracking&page=13&id='.$course_id;
		if($filter_lpath){
			$link_redirect .= '&filter_lpath='.$filter_lpath;
		}
		$link_redirect .= '&filter_stu='.$user_id;
		$app->redirect($link_redirect);
	}

	$query = "SELECT id, item_id, lp_type FROM #__lms_learn_paths WHERE id = $lpath_id AND course_id = $course_id";
	$JLMS_DB->SetQuery( $query );
	$lptmp = $JLMS_DB->LoadObject();
	
	$XML_data = '';
	if ($course_id && $JLMS_ACL->CheckPermissions('tracking', 'manage') && $lpath_id && isset($lptmp->id) && ($lpath_id == $lptmp->id) ){
		$query = "SELECT user_id FROM #__lms_users_in_groups WHERE course_id = $course_id"
		. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND user_id IN ($members)") :'')
		;
		$JLMS_DB->SetQuery( $query );
		$uids = JLMSDatabaseHelper::loadResultArray();
		$query = "SELECT user_id FROM #__lms_user_courses WHERE course_id = $course_id"
		. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND user_id IN ($members)") :'')
		;
		$JLMS_DB->SetQuery( $query );
		$uids = array_merge($uids,JLMSDatabaseHelper::loadResultArray());
		
		if(isset($user_id) && $user_id){
			$uids = array();
			$uids[] = $user_id;
		}
	
		if (empty($uids)) { $uids = array(0); }
		$uidss = implode(',',$uids);
		
		if ($lptmp->item_id) {
			$sc_ids = array();
			$sc_id = $lptmp->item_id;
			
			if ($lptmp->lp_type == 1 || $lptmp->lp_type == 2) { // new SCORM tracking
				$course_params = $JLMS_CONFIG->get('course_params');
				$params_tt = new JLMSParameters($course_params);
				$scorm_ans = & JLMS_Get_N_SCORM_SCO_userResults($uids, $sc_id, $params_tt->get('track_type', 0));
				$sc_user_ids = array();
				$lpath_users = array();
				foreach ($scorm_ans as $sa) {
					foreach ($sa->track_data as $trd) {
						$sc_user_ids[] = $trd->user_id;
					}
				}
				$sc_user_ids = array_unique($sc_user_ids);
				if (count($sc_user_ids)) {
					$uidss2 = implode(',',$sc_user_ids);
					$query = "SELECT u.username, u.name, u.email, u.id as user_id"
					. "\n FROM #__users as u"
					. "\n WHERE u.id IN ($uidss) AND u.id IN ($uidss2)"
					. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND u.id IN ($members)") :'')
					. "\n ORDER BY u.username, u.name";
					$JLMS_DB->SetQuery( $query );
					$lpath_users = $JLMS_DB->LoadObjectList();
					
				}
				if (!empty($uids)) {
					$XML_data .= '<table cellpadding=0 cellspacing=0 border=0 width="100%">';
					if (!empty($scorm_ans) && !empty($lpath_users)) {
						$k = 1;
						foreach ($lpath_users as $lpu) {
							$user_score = 0;
							$user_status = 0;
							$tmp_var = false;
							foreach ($scorm_ans as $sa) {
								foreach ($sa->track_data as $trd) {
									if ($trd->user_id == $lpu->user_id) {
										if ($trd->element == 'cmi.score.raw' || $trd->element == 'cmi.core.score.raw') {
											$user_score = $trd->value;
										}
										if (($trd->element == 'cmi.completion_status') || ($trd->element == 'cmi.core.lesson_status') || ($trd->element == 'cmi.core.completion_status') || ($trd->element == 'cmi.lesson_status')) {
											if (($trd->value == 'completed') || ($trd->value == 'passed')) {
												if (!$tmp_var) {
													$user_status = 1;
												}
											} else {
												$tmp_var = true;
												$user_status = 0;
											}
										}
									}
								}
							}
							$XML_data .= '<tr class="sectiontableentry'.$k.'"><td width="16px">&nbsp;</td>';
							$XML_data .= '<td width="16px"><img class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/btn_edit_user.png" width=\'16\' height=\'16\' alt="user" title="user" /></td>';
							$XML_data .= '<td colspan="2"><b>'.$lpu->username.', '.$lpu->name.'</b></td>';
	
							$alt = ($user_status)?_JLMS_LPATH_STU_LPSTATUS_COMPLETED:_JLMS_LPATH_STU_LPSTATUS_NOTCOMPLETED;
							$image = ($user_status)?'btn_accept.png':'btn_cancel.png';
							$new_s_status = ($user_status) ? '0' : '1';
	
							$r_img = '<a id="sc_progress_a_'.$lpu->user_id.'_'.$sc_id.'" href="javascript:ChangeScormProgress('.$lpu->user_id.','.$sc_id.','.$new_s_status.',this);"><img id="sc_progress_img_'.$lpu->user_id.'_'.$sc_id.'" align="absmiddle" class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/'.$image.'" width="16" height="16" border="0" alt="'.$alt.'" title="'.$alt.'" /></a>';
							$XML_data .= '<td width="16" id="sc_progress_td_'.$lpu->user_id.'_'.$sc_id.'">'.$r_img.'</td><td width="30%" align="left">'.($user_score?$user_score:'-').'</td>';
	
							$XML_data .= '</tr>'. "\n";
							$k = 3 - $k;
							for ($jj = 0, $mm = count($scorm_ans); $jj < $mm; $jj ++) {
								$sa = $scorm_ans[$jj];
							//foreach ($scorm_ans as $sa) {
								$XML_data .= '<tr class="sectiontableentry'.$k.'"><td width="16px">&nbsp;</td>';
								$sub_tree_index = 1;
								if (($jj+1) == $mm) {
									$sub_tree_index = 2;
								}
								// get stats about SCO
								$sco_score = 0;
								$sco_status = 0;
								$tmp_var = false;
								foreach ($sa->track_data as $satd) {
									if ($satd->user_id == $lpu->user_id) {
										if ($satd->element == 'cmi.score.raw' || $satd->element == 'cmi.core.score.raw') {
											$sco_score = $satd->value;
										}
										if (($satd->element == 'cmi.completion_status') || ($satd->element == 'cmi.core.lesson_status')) {
											if (($satd->value == 'completed') || ($satd->value == 'passed')) {
												if (!$tmp_var) {
													$sco_status = 1;
												}
											} else {
												$tmp_var = true;
												$sco_status = 0;
											}
										}
									}
								}
								//end of SCO stats
								$tree_img = "<img class='JLMS_png' src='".$JLMS_CONFIG->get('live_site')."/components/com_joomla_lms/lms_images/treeview/sub".$sub_tree_index.".png' width='16' height='16' alt='sub' border='0' />";
								$XML_data .= '<td width="16px">'.$tree_img.'</td>';
								$XML_data .= '<td colspan="2">'.$sa->title.'</td>';
								$alt = ($sco_status)?_JLMS_LPATH_STU_LPSTATUS_COMPLETED:_JLMS_LPATH_STU_LPSTATUS_NOTCOMPLETED;
								$image = ($sco_status)?'btn_accept.png':'btn_cancel.png';
	
								$r_img = '<img align="absmiddle" class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/'.$image.'" width="16" height="16" border="0" alt="'.$alt.'" title="'.$alt.'" />';
								$XML_data .= '<td width="16px">'.$r_img.'</td><td width="30%" align="left">'.($sco_score?$sco_score:'-').'</td>';
	
								$XML_data .= '</tr>'. "\n";
								$k = 3 - $k;
								for ($jjj = 0, $mmm = count($sa->track_data); $jjj < $mmm; $jjj ++) {
									$trd = $sa->track_data[$jjj];
								//foreach ($sa->track_data as $trd) {
									if ($trd->user_id == $lpu->user_id) {
										$XML_data .= '<tr class="sectiontableentry'.$k.'"><td width="16px">&nbsp;</td>';
										if ($sub_tree_index == 1) {
											$tree_img = "<img class='JLMS_png' src='".$JLMS_CONFIG->get('live_site')."/components/com_joomla_lms/lms_images/treeview/line.png' width='16' height='16' alt='sub' border='0' />";
										} else {//if ($sub_tree_index == 2) {
											$tree_img = "<img class='JLMS_png' src='".$JLMS_CONFIG->get('live_site')."/components/com_joomla_lms/lms_images/treeview/empty_line.png' width='16' height='16' alt='sub' border='0' />";
										}
										$XML_data .= '<td width="16px">'.$tree_img.'</td>';
										$sub_tree_index2 = 2;
										$jjjj = $jjj + 1;
										while ($jjjj < $mmm) {
											if ($sa->track_data[$jjjj]->user_id == $lpu->user_id) {
												$sub_tree_index2 = 1; break;
											}
											$jjjj ++;
										}
										$tree_img2 = "<img class='JLMS_png' src='".$JLMS_CONFIG->get('live_site')."/components/com_joomla_lms/lms_images/treeview/sub".$sub_tree_index2.".png' width='16' height='16' alt='sub' border='0' />";
										$XML_data .= '<td width="16px">'.$tree_img2.'</td>';
										$XML_data .= '<td><i>'.$trd->element.'</i></td>';
										$XML_data .= '<td>&nbsp;</td>';
										$XML_data .= '<td width="30%" align="left" style="text-align:left">'.$trd->value.'</td>';
										$XML_data .= '</tr>'. "\n";
									}
									$k = 3 - $k;
								}
							}
						}
					}
					$lost_users = array();
					$uidss2 = '';
					if (count($sc_user_ids)) {
						$uidss2 = ' AND u.id NOT IN ('.implode(',',$sc_user_ids).')';
					}
					$query = "SELECT u.username, u.name, u.email, u.id as user_id"
					. "\n FROM #__users as u"
					. "\n WHERE u.id IN ($uidss)".$uidss2
					. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND u.id IN ($members)") :'')
					. "\n ORDER BY u.username, u.name";
					$JLMS_DB->SetQuery( $query );
					$lost_users = $JLMS_DB->LoadObjectList();
					$user_score = 0;
					if (!isset($k)) { $k = 1; }
					foreach ($lost_users as $lostu) {
						$XML_data .= '<tr class="sectiontableentry'.$k.'"><td width="16px">&nbsp;</td>';
						$XML_data .= '<td width="16px"><img class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/btn_edit_user.png" width=\'16\' height=\'16\' alt="user" title="user" /></td>';
						$XML_data .= '<td colspan="2"><b>'.$lostu->username.', '.$lostu->name.'</b></td>';

						$alt = _JLMS_LPATH_STU_LPSTATUS_NOTCOMPLETED;
						$image = 'btn_cancel.png';
						$new_s_status = '1';
						

						$r_img = '<a id="sc_progress_a_'.$lostu->user_id.'_'.$sc_id.'" href="javascript:ChangeScormProgress('.$lostu->user_id.','.$sc_id.','.$new_s_status.',this);"><img id="sc_progress_img_'.$lostu->user_id.'_'.$sc_id.'" align="absmiddle" class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/'.$image.'" width="16" height="16" border="0" alt="'.$alt.'" title="'.$alt.'" /></a>';
						$XML_data .= '<td width="16" id="sc_progress_td_'.$lostu->user_id.'_'.$sc_id.'">'.$r_img.'</td><td width="30%" align="left">'.($user_score?$user_score:'-').'</td>';

						$XML_data .= '</tr>'. "\n";
						$k = 3 - $k;
					}
					$XML_data .= '</table>';
				}
			} else { // old SCORM
				$scorm_ans = & JLMS_GetSCORM_SCO_userResults($uids, $sc_id);

				$separate = false;
				if (isset($scorm_ans[0]) && isset($scorm_ans[0]->sco_count) && ($scorm_ans[0]->sco_count > 1)) {
					$separate = true;
				}
				$sc_user_ids = array();
				$lpath_users = array();
				foreach ($scorm_ans as $sa) {
					$sc_user_ids[] = $sa->user_id;
				}
				$sc_user_ids = array_unique($sc_user_ids);
				if (count($sc_user_ids)) {
					$uidss2 = implode(',',$sc_user_ids);
					$query = "SELECT u.username, u.name, u.email, u.id as user_id"
					. "\n FROM #__users as u"
					. "\n WHERE u.id IN ($uidss) AND u.id IN ($uidss2)"
					. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND u.id IN ($members)") :'')
					. "\n ORDER BY u.username, u.name";
					$JLMS_DB->SetQuery( $query );
					$lpath_stats = $JLMS_DB->LoadObjectList();
				}
				if (!$separate) {
					/*$query = "SELECT u.username, u.name, u.email, u.id as user_id"
					. "\n FROM #__users as u"
					. "\n WHERE u.id IN ($uidss)"
					. "\n ORDER BY u.username, u.name";
					$JLMS_DB->SetQuery( $query );
					$lpath_stats = $JLMS_DB->LoadObjectList();*/
					foreach ($scorm_ans as $sa) {
						$i = 0;
						while ($i < count($lpath_stats)) {
							if ($lpath_stats[$i]->user_id == $sa->user_id) {
								$lpath_stats[$i]->s_status = $sa->status;
								$lpath_stats[$i]->s_score = $sa->score;
							}
							$i ++;
						}
					}
					if (!empty($lpath_stats)) {
						$XML_data .= '<table cellpadding=0 cellspacing=0 border=0 width="100%">';
					}
					$k = 1;
					foreach ($lpath_stats as $lpst) {
						$XML_data .= '<tr class="sectiontableentry'.$k.'"><td width="16px">&nbsp;</td>';
						$alt = (isset($lpst->s_status) && $lpst->s_status)?_JLMS_LPATH_STU_LPSTATUS_COMPLETED:_JLMS_LPATH_STU_LPSTATUS_NOTCOMPLETED;
						$image = (isset($lpst->s_status) && $lpst->s_status)?'btn_accept.png':'btn_cancel.png';

						$r_img = '<img align="absmiddle" class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/'.$image.'" width="16" height="16" border="0" alt="'.$alt.'" />';
						$XML_data .= '<td width="16px"><img class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/btn_edit_user.png" width=\'16\' height=\'16\' alt="user" title="user" /></td>';
						$XML_data .= '<td><b>'.$lpst->username.', '.$lpst->name.'</b></td><td width="16px">'.$r_img.'</td><td width="30%" align="left">'.((isset($lpst->s_score) && $lpst->s_score)?$lpst->s_score:'-').'</td>';
						$XML_data .= '</tr>'. "\n";
						$k = 3 - $k;
					}
					if (!empty($lpath_stats)) {
						$XML_data .= '</table>';
					}
				} else { //get stats for separate SCORM's
					/*$query = "SELECT u.username, u.name, u.email, u.id as user_id"
					. "\n FROM #__users as u"
					. "\n WHERE u.id IN ($uidss)"
					. "\n ORDER BY u.username, u.name";
					$JLMS_DB->SetQuery( $query );
					$lpath_stats = $JLMS_DB->LoadObjectList();*/
					for ($jj = 0, $mm = count($lpath_stats); $jj < $mm; $jj ++) {
						$lpath_stats[$jj]->user_results = array();
						$lpath_stats[$jj]->s_status = 1;
						$lpath_stats[$jj]->s_score = 0;
						$user_ans = 0;
						for ($ii = 0, $nn = count($scorm_ans); $ii < $nn; $ii ++) {
							if ($lpath_stats[$jj]->user_id == $scorm_ans[$ii]->user_id) {
								$user_ans ++;
								$lpath_stats[$jj]->user_results[] = $scorm_ans[$ii];
								if (!$scorm_ans[$ii]->status) { $lpath_stats[$jj]->s_status = 0; }
								$lpath_stats[$jj]->s_score = $lpath_stats[$jj]->s_score + intval($scorm_ans[$ii]->score);
							}
						}
						if (!$user_ans) {
							$lpath_stats[$jj]->s_status = 0;
						}
					}
					if (!empty($lpath_stats)) {
						$XML_data .= '<table cellpadding=0 cellspacing=0 border=0 width="100%">';
					}
					$k = 1;
					foreach ($lpath_stats as $lpst) {
						$XML_data .= '<tr class="sectiontableentry'.$k.'"><td width="16px">&nbsp;</td>';
						$alt = ($lpst->s_status)?_JLMS_LPATH_STU_LPSTATUS_COMPLETED:_JLMS_LPATH_STU_LPSTATUS_NOTCOMPLETED;
						$image = ($lpst->s_status)?'btn_accept.png':'btn_cancel.png';
			
						$r_img = '<img align="absmiddle" class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/'.$image.'" width="16" height="16" border="0" alt="'.$alt.'" />';
						$XML_data .= '<td width="16px"><img class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/btn_edit_user.png" width=\'16\' height=\'16\' alt="user" title="user" /></td>';
						$XML_data .= '<td><b>'.$lpst->username.', '.$lpst->name.'</b></td><td width="16px">'.$r_img.'</td><td align="left" width="30%">'.($lpst->s_score?$lpst->s_score:'-').'</td>';
						$XML_data .= '</tr>'. "\n";
						$d = 0;
						foreach ($lpst->user_results as $each_sco_results) {
							$XML_data .= '<tr class="sectiontableentry'.$k.'"><td width="16px">&nbsp;</td>';
							$alt = ($each_sco_results->status)?_JLMS_LPATH_STU_LPSTATUS_COMPLETED:_JLMS_LPATH_STU_LPSTATUS_NOTCOMPLETED;
							$image = ($each_sco_results->status)?'btn_accept.png':'btn_cancel.png';
							$t_image = 'sub1';
							if ($d == (count($lpst->user_results) - 1)) {
								$t_image = 'sub2';
							}
							$r_img = '<img align="absmiddle" class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/'.$image.'" width="16" height="16" border="0" alt="'.$alt.'" />';
							$t_img = '<img align="absmiddle" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/treeview/'.$t_image.'.png" width="16" height="16" border="0" alt="'.$t_image.'" />';
							$XML_data .= '<td width="16px">'.$t_img.'</td>';
							$XML_data .= '<td>'.$each_sco_results->sco_title.'</td><td width="16px">'.$r_img.'</td><td align="left" width="30%">'.($each_sco_results->score?$each_sco_results->score:'-').'</td>';
							$XML_data .= '</tr>'. "\n";
							$d ++;
							$k = 3 - $k;
						}
						$k = 3 - $k;
					}
					if (!empty($lpath_stats)) {
						$XML_data .= '</table>';
					}
				}
			}
		} else {
			$query = "SELECT b.user_status as r_status, b.start_time as r_start, b.end_time as r_end, u.username, u.name, u.email, u.id as user_id, b.lpath_id"
			#. "\n FROM #__users as u LEFT JOIN #__lms_learn_path_results as b"
			#. "\n ON b.lpath_id = $lpath_id AND b.user_id = u.id"
			#. "\n WHERE u.id IN ($uidss)"
			. "\n FROM #__users as u, #__lms_learn_path_results as b";
			
			if($groups_mode && $filt_group) {
				$query .= ", #__lms_users_in_global_groups as d";	
			}
			
			$query .= " WHERE b.lpath_id = $lpath_id AND b.user_id = u.id"
			. "\n AND u.id IN ($uidss)"
			. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND u.id IN ($members)") :'');
			
			if($groups_mode && $filt_group) {
				$query .= " AND d.user_id = u.id"
				. ($filt_group ? ("\n AND d.group_id = '".$filt_group."'") : '')
				. ($filt_subgroup ? ("\n AND d.subgroup1_id = '".$filt_subgroup."'") : '')
				;
			}	
			
			$query .=" ORDER BY u.username, u.name";
			$JLMS_DB->SetQuery( $query );
			$lpath_stats = $JLMS_DB->LoadObjectList();
			
			/*echo $JLMS_DB->geterrormsg();
			print_r($lpath_stats);
			echo $JLMS_DB->getQuery();*/

			$course_params = $JLMS_CONFIG->get('course_params');
			$params = new JLMSParameters($course_params);

			// 15 August 2007 (DEN) tracking type task
			if ($params->get('track_type', 0) == 1) {
				$query = "SELECT b.lpath_id, b.user_status as r_status, b.start_time as r_start, b.end_time as r_end, u.username, u.name, u.email, u.id as user_id"
				. "\n FROM #__lms_learn_path_grades as b, #__users as u"
				. "\n WHERE b.lpath_id = $lpath_id AND b.user_id IN ( $uidss ) AND b.user_id = u.id"
//				. (!$view_all_users ? ("\n AND u.id IN ($members)") :'')
				. "\n ORDER BY u.username, u.name";
				$JLMS_DB->SetQuery( $query );
				$lp_res_pre2 = $JLMS_DB->LoadObjectList();
				foreach ($lp_res_pre2 as $lpr2) {
					$h = 0;
					while ($h < count($lpath_stats)) {
						if ($lpr2->lpath_id == $lpath_stats[$h]->lpath_id) {
							$do_update = false;
							if ($lpr2->r_status > $lpath_stats[$h]->r_status) {
								$do_update = true;
							} elseif ($lpr2->r_status == $lpath_stats[$h]->r_status && $lpath_stats[$h]->r_end != '000-00-00 00:00:00'  && $lpr2->r_end != '000-00-00 00:00:00') {
								$f_time = strtotime($lpath_stats[$h]->r_end) - strtotime($lpath_stats[$h]->r_start);
								$s_time = strtotime($lpr2->r_end) - strtotime($lpr2->r_start);
								if ($s_time < $f_time) {
									$do_update = true;
								}
							}
							if ($do_update) {
								$lpath_stats[$h]->r_status = $lpr2->r_status;
								$lpath_stats[$h]->r_start = $lpr2->r_start;
								$lpath_stats[$h]->r_end = $lpr2->r_end;
							}
							break;
						}
						$h ++;
					}
				}
			}

			if (!empty($lpath_stats)) {
				$XML_data .= '<table cellpadding=0 cellspacing=0 border=0 width="100%">';
			}
			$k = 1;
			foreach ($lpath_stats as $lpst) {
				$XML_data .= '<tr class="sectiontableentry'.$k.'"><td width="16px">&nbsp;</td>';
				$alt = ($lpst->r_status)?_JLMS_LPATH_STU_LPSTATUS_COMPLETED:_JLMS_LPATH_STU_LPSTATUS_NOTCOMPLETED;
				$image = ($lpst->r_status)?'btn_accept.png':'btn_cancel.png';
	
				$r_img = '<img align="absmiddle" class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/'.$image.'" width="16" height="16" border="0" alt="'.$alt.'" />';
				$XML_data .= '<td width="16px"><img class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/btn_edit_user.png" width=\'16\' height=\'16\' alt="user" title="user" /></td>';
				$XML_data .= '<td><b>'.$lpst->username.', '.$lpst->name.'</b></td>';
				$XML_data .= '<td width="16px">'.$r_img.'</td>';
				$XML_data .= '<td width="30%" align="left">'.($lpst->r_start?JLMS_dateToDisplay($lpst->r_start, false, $JLMS_CONFIG->get('offset')*60*60, ' H:i:s'):'-').'</td>';
				$XML_data .= '<td width="30%" align="left">'.($lpst->r_end?JLMS_dateToDisplay($lpst->r_end, false, $JLMS_CONFIG->get('offset')*60*60, ' H:i:s'):'-').'</td>';
				$XML_data .= '</tr>'. "\n";
				$k = 3 - $k;
			}
			if (!empty($lpath_stats)) {
				$XML_data .= '</table>';
			}
		}
	}
	$iso = explode( '=', _ISO );
	echo "\n"."debug mode";
	$debug_str = ' ';
	if ($JLMS_CONFIG->get('debug_mode', false)) {
		$debug_str = ob_get_contents();
	}
	ob_end_clean();
	header ('Expires: Fri, 14 Mar 1980 20:53:00 GMT');
	header ('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
	header ('Cache-Control: no-cache, must-revalidate');
	header ('Pragma: no-cache');
	if (class_exists('JFactory')) {
		$document=JFactory::getDocument();
		$charset_xml = $document->getCharset();
		header ('Content-Type: text/xml; charset='.$charset_xml);
	} else {
		header ('Content-Type: text/xml');
	}
	echo '<?xml version="1.0" encoding="'.$iso[1].'" standalone="yes"?>';
	echo '<response>' . "\n";
	echo "\t" . '<task>lpath_xml</task>' . "\n";
	echo "\t" . '<stats_table><![CDATA['.($XML_data?$XML_data:' ').']]></stats_table>' . "\n";
	echo "\t" . '<debug><![CDATA['.$debug_str.']]></debug>' . "\n";
	echo '</response>' . "\n";
	JLMS_die();
}
function JLMS_ajaxGetDOCSStatsXML( $course_id, $option ) {
	global $my, $JLMS_DB, $JLMS_SESSION, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();

	$groups_mode = $JLMS_CONFIG->get('use_global_groups', 1);
	
	$filt_group = intval( mosGetParam( $_GET, 'filt_group', $JLMS_SESSION->get('filt_group', 0) ) );
	$filt_subgroup = intval( mosGetParam( $_GET, 'filt_subgroup', $JLMS_SESSION->get('filt_subgroup', 0) ) );

	if(!$filt_group) {
		$filt_subgroup = 0;
	}

	/* time offsets */
	$offset_hours = 0;
	$offset_minutes = 0;
	$total_offset = $JLMS_CONFIG->get('offset');
	$track_time = 'b.track_time';
	/* time offsets */

	$members = "'0'";
	$members_array = array();
	if($JLMS_ACL->_role_type == 2 || $JLMS_ACL->_role_type == 4) {
		if($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only')) {
			$groups_where_admin_manager = JLMS_ACL_HELPER::GetAssignedGroups($my->id, $course_id);
				
			if(count($groups_where_admin_manager)) {
				if(JLMS_ACL_HELPER::GetCountAssignedGroups($my->id, $course_id) == 1) {	
					$filt_group = $groups_where_admin_manager[0];
				}
			}
			
			if(count($groups_where_admin_manager)) {
				$groups_where_admin_manager = implode(',', $groups_where_admin_manager);
					if($groups_where_admin_manager != '') {
						$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE (group_id IN ($groups_where_admin_manager) OR subgroup1_id IN ($groups_where_admin_manager))"
							. ($filt_group ? ("\n AND group_id = '".$filt_group."'") : '')
							. ($filt_subgroup ? ("\n AND subgroup1_id = '".$filt_subgroup."'") : '')
						;
						
						$JLMS_DB->setQuery($query);
						$members = JLMSDatabaseHelper::LoadResultArray();
						
						$members_array = $members;
						
						$members = implode(',', $members);
						if($members == '')
							$members = "'0'";
					}
			}
			else {
				$groups_where_admin_manager = "'0'";
			}
		}
	}
	
	$usertype = JLMS_GetUserType($my->id, $course_id);
	$doc_id = intval( mosGetParam( $_REQUEST, 'doc_id', 0 ) );
	$colspan = intval( mosGetParam( $_REQUEST, 'colspan', 1 ) );
	$query = "SELECT id FROM #__lms_documents WHERE id = $doc_id AND course_id = $course_id";
	$JLMS_DB->SetQuery( $query );
	$doc_tmp = $JLMS_DB->LoadResult();
	$XML_data = '';
	if ($course_id && $JLMS_ACL->CheckPermissions('tracking', 'manage') && $doc_id && $doc_tmp && ($doc_id == $doc_tmp) ) {
		$query = "SELECT user_id FROM #__lms_users_in_groups WHERE course_id = $course_id";
		$JLMS_DB->SetQuery( $query );
		$uids = JLMSDatabaseHelper::LoadResultArray();
		$query = "SELECT user_id FROM #__lms_user_courses WHERE course_id = $course_id";
		$JLMS_DB->SetQuery( $query );
		$uids = array_merge($uids,JLMSDatabaseHelper::LoadResultArray());
		if (empty($uids)) { $uids = array(0); }
		$uidss = implode(',',$uids);
		$query = "SELECT count(b.id) as count_downs, max($track_time) as mtt, u.id, u.username, u.name, u.email"
		#. "\n FROM #__users as u LEFT JOIN #__lms_track_downloads as b"
		#. "\n ON b.doc_id = $doc_id AND b.user_id = u.id"
		#. "\n WHERE u.id IN ($uidss)"
		."\n FROM #__users as u, #__lms_track_downloads as b";
		if($groups_mode && $filt_group) {
			$query .= ", #__lms_users_in_global_groups as d";	
		}
		
		$query .= " WHERE b.doc_id = $doc_id AND b.user_id = u.id"
		. "\n AND u.id IN ($uidss)"
		. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND u.id IN ($members)") :'');
		
		if($groups_mode && $filt_group) {
			$query .= " AND d.user_id = b.user_id"
			. ($filt_group ? ("\n AND d.group_id = '".$filt_group."'") : '')
			. ($filt_subgroup ? ("\n AND d.subgroup1_id = '".$filt_subgroup."'") : '')
			;
		}	
		
		$query .= " GROUP BY u.id, u.username, u.name, u.email"
		. "\n ORDER BY u.username, u.name";
		$JLMS_DB->SetQuery( $query );
		$doc_stats = $JLMS_DB->LoadObjectList();
		
		if (!empty($doc_stats)) {
			$XML_data .= '<table cellpadding=0 cellspacing=0 border=0 width="100%" class="'.JLMSCSS::_('jlmslist').'" style="margin:0px;padding:0px">';
			/*$XML_data .= '<tr style="display:none;visibility:hidden">';
			$XML_data .= '<'.JLMSCSS::tableheadertag().' width="16" class="'.JLMSCSS::_('sectiontableheader').'">&nbsp;</'.JLMSCSS::tableheadertag().'>';
			$XML_data .= '<'.JLMSCSS::tableheadertag().' width="16" class="'.JLMSCSS::_('sectiontableheader').'" colspan="1">&nbsp;</'.JLMSCSS::tableheadertag().'>';
			$XML_data .= '<'.JLMSCSS::tableheadertag().' class="'.JLMSCSS::_('sectiontableheader').'" width="45%">'._JLMS_TRACK_TBL_DOC_NAME.'</'.JLMSCSS::tableheadertag().'>';
			$XML_data .= '<'.JLMSCSS::tableheadertag().' class="'.JLMSCSS::_('sectiontableheader').'" width="100" align="center">'._JLMS_TRACK_TBL_DOC_DOWNS.'</'.JLMSCSS::tableheadertag().'>';
			$XML_data .= '<'.JLMSCSS::tableheadertag().' class="'.JLMSCSS::_('sectiontableheader').'" width="100" align="center">'._JLMS_TRACK_TBL_DOC_LAST.'</'.JLMSCSS::tableheadertag().'>';
			$XML_data .= '</tr>';*/
		}
		$k = 1;
		foreach ($doc_stats as $dst) {
			$XML_data .= '<tr class="'.JLMSCSS::_('sectiontableentry'.$k).'">';
			#$XML_data .= '<td colspan="'.$colspan.'">'.$dst->username.', '.$dst->name.'</td>';
			$XML_data .= '<td width="16px">&nbsp;</td>';
			$XML_data .= '<td width="16px"><img class="JLMS_png" src="'.$JLMS_CONFIG->get('live_site').'/components/com_joomla_lms/lms_images/toolbar/btn_edit_user.png" width=\'16\' height=\'16\' alt="user" title="user" /></td>';
			$XML_data .= '<td>'.$dst->username.', '.$dst->name.'</td>';
			$XML_data .= '<td width="130px" align="center">'.$dst->count_downs.'</td>';
			$XML_data .= '<td width="130px" align="center">'.($dst->mtt?JLMS_dateToDisplay($dst->mtt, false, $JLMS_CONFIG->get('offset')*60*60, ' H:i:s'):'-').'</td>';
			$XML_data .= '</tr>'. "\n";
			$k = 3 - $k;
		}
		if (!empty($doc_stats)) {
			$XML_data .= '</table>';
		}
	}
	$iso = explode( '=', _ISO );
	echo "\n"."debug mode";
	$debug_str = ' ';
	global $JLMS_CONFIG;
	if ($JLMS_CONFIG->get('debug_mode', false)) {
		$debug_str = ob_get_contents();
	}
	ob_end_clean();
	header ('Expires: Fri, 14 Mar 1980 20:53:00 GMT');
	header ('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
	header ('Cache-Control: no-cache, must-revalidate');
	header ('Pragma: no-cache');
	if (class_exists('JFactory')) {
		$document=JFactory::getDocument();
		$charset_xml = $document->getCharset();
		header ('Content-Type: text/xml; charset='.$charset_xml);
	} else {
		header ('Content-Type: text/xml');
	}
	echo '<?xml version="1.0" encoding="'.$iso[1].'" standalone="yes"?>';
	echo '<response>' . "\n";
	echo "\t" . '<task>doc_xml</task>' . "\n";
	echo "\t" . '<stats_table><![CDATA['.($XML_data?$XML_data:' ').']]></stats_table>' . "\n";
	echo "\t" . '<debug><![CDATA['.$debug_str.']]></debug>' . "\n";
	echo '</response>' . "\n";
	exit();
}
function JLMS_TRACKING_getTitle($page) {
	$title = '';
	switch ($page) {
		case 0:	$title = _JLMS_TRACK_TITLE_TOOLS;	break;
		case 1:	$title = _JLMS_TRACK_TITLE_1;		break;
		case 2:	$title = _JLMS_TRACK_TITLE_2;		break;
		case 3:	$title = _JLMS_TRACK_TITLE_3;		break;
		case 4:	$title = _JLMS_TRACK_TITLE_4;		break;
		case 5:	$title = _JLMS_TRACK_TITLE_5;		break;
		case 6:	$title = _JLMS_TRACK_TITLE_6;		break;
		case 7:	$title = _JLMS_TRACK_TITLE_7;		break;
		case 8:	$title = _JLMS_TRACK_TITLE_8;		break;
		case 9:	$title = _JLMS_TRACK_TITLE_9;		break;
		case 10: $title = _JLMS_TRACK_TITLE_10;		break;
		case 11: $title = _JLMS_TRACK_TITLE_11;		break;
		default:$title = _JLMS_TRACK_TITLE_TOOLS;	break;
	}
	return $title;
}
function JLMS_TR_temp_fix($text) {
	if (function_exists('html_entity_decode')) {
		$text = @html_entity_decode($text, ENT_QUOTES, 'UTF-8'); 
	} else { 
		if (function_exists('get_html_translation_table')) {
			$trans_tbl = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
			$trans_tbl = array_flip($trans_tbl);
			$text = strtr($text, $trans_tbl);
		}
	}
	return $text;
}
function JLMS_showTracking( $id, $option) {
	global $my, $JLMS_DB, $Itemid, $JLMS_SESSION, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();

	$groups_mode = $JLMS_CONFIG->get('use_global_groups', 1);

	$usertype = JLMS_GetUserType($my->id, $id);
	if ($id && $JLMS_ACL->CheckPermissions('tracking', 'manage')) {

		/* time offsets */
		$offset_hours = 0;
		$offset_minutes = 0;
		$total_offset = $JLMS_CONFIG->get('offset');
		$track_time = 'b.track_time';
		if ($total_offset) {
			$offset_hours = intval(($total_offset*60)/60);
			$offset_minutes = $total_offset*60 - $offset_hours*60;
			$track_time = "ADDTIME(b.track_time, '".$offset_hours.":".$offset_minutes.":00.000000')";
		}
		/* time offsets */

		$filter_year = intval( mosGetParam( $_REQUEST, 'filter_year', $JLMS_SESSION->get('filter_year', date('Y')) ) );
		if (isset($_REQUEST['filter_month'])) {
			$filter_month = intval( mosGetParam( $_REQUEST, 'filter_month', 0 ) );
		} else {
			$filter_month = $JLMS_SESSION->get('filter_month', 0);
		}
		if (strlen(''+$filter_year) != 4) {
			$filter_year = date('Y');
		}
		
		$filter_stu = intval( mosGetParam( $_REQUEST, 'filter_stu', $JLMS_SESSION->get('filter_stu', 0) ) );
		
		$filt_group = intval( mosGetParam( $_GET, 'filt_group', $JLMS_SESSION->get('filt_group', 0) ) );
		$filt_subgroup = intval( mosGetParam( $_GET, 'filt_subgroup', $JLMS_SESSION->get('filt_subgroup', 0) ) );

		if(!$filt_group) {
			$filt_subgroup = 0;
		}
		
		$JLMS_SESSION->set('filter_year', $filter_year);
		$JLMS_SESSION->set('filter_month', $filter_month);
		$JLMS_SESSION->set('filter_stu', $filter_stu);
		
		$JLMS_SESSION->set('filt_group', $filt_group);
		$JLMS_SESSION->set('filt_subgroup', $filt_subgroup);
		
		$members = "'0'";
		$members_array = array();
		if($JLMS_ACL->_role_type == 2 || $JLMS_ACL->_role_type == 4) {
			if($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only')) {
				$groups_where_admin_manager = JLMS_ACL_HELPER::GetAssignedGroups($my->id, $id);

				if(count($groups_where_admin_manager)) {
					if(JLMS_ACL_HELPER::GetCountAssignedGroups($my->id, $id) == 1) {	
						$filt_group = $groups_where_admin_manager[0];
					}
				}

				if(count($groups_where_admin_manager)) {
					$groups_where_admin_manager = implode(',', $groups_where_admin_manager);
						if($groups_where_admin_manager != '') {
							$query = "SELECT distinct user_id FROM #__lms_users_in_global_groups WHERE (group_id IN ($groups_where_admin_manager) OR subgroup1_id IN ($groups_where_admin_manager))"
								. ($filt_group ? ("\n AND group_id = '".$filt_group."'") : '')
								. ($filt_subgroup ? ("\n AND subgroup1_id = '".$filt_subgroup."'") : '')
							;
							
							$JLMS_DB->setQuery($query);
							$members = JLMSDatabaseHelper::LoadResultArray();
							
							$members_array = $members;
							
							$members = implode(',', $members);
							if($members == '')
								$members = "'0'";
						}
				}	
				else {
					$groups_where_admin_manager = "'0'";
				}
		
			}
		}

		$lists = array();
		if ($filter_month) {
			if($groups_mode && $filt_group) {
				$query = "SELECT count(b.user_id) as count_hits, page_id, EXTRACT( YEAR FROM $track_time) as year, EXTRACT( DAY FROM $track_time) as month FROM #__lms_track_hits as b, #__lms_users_in_groups as c, #__lms_users_in_global_groups as d"
				. "\n WHERE b.course_id = '".$id."' AND b.course_id = c.course_id AND b.page_id <> 0 AND b.user_id = c.user_id AND d.user_id = b.user_id"
				. "\n AND EXTRACT( YEAR FROM $track_time) = '".$filter_year."'"
				. "\n AND EXTRACT( MONTH FROM $track_time) = '".$filter_month."'"
				. ($filter_stu ? ("\n AND b.user_id = '".$filter_stu."'") : '')
				. ($filt_group ? ("\n AND d.group_id = '".$filt_group."'") : '')
				. ($filt_subgroup ? ("\n AND d.subgroup1_id = '".$filt_subgroup."'") : '')
				. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
				. "\n GROUP BY year, month, page_id"
				. "\n ORDER BY year, month, page_id"
				;
			}
			else {
				$query = "SELECT count(b.user_id) as count_hits, page_id, EXTRACT( YEAR FROM $track_time) as year, EXTRACT( DAY FROM $track_time) as month FROM #__lms_track_hits as b, #__lms_users_in_groups as c"
				. "\n WHERE b.course_id = '".$id."' AND b.course_id = c.course_id AND b.page_id <> 0 AND b.user_id = c.user_id"
				. "\n AND EXTRACT( YEAR FROM $track_time) = '".$filter_year."'"
				. "\n AND EXTRACT( MONTH FROM $track_time) = '".$filter_month."'"
				. ($filter_stu ? ("\n AND b.user_id = '".$filter_stu."'") : '')
				. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
				. "\n GROUP BY year, month, page_id"
				. "\n ORDER BY year, month, page_id"
				;
			}
			$lists['is_month'] = true;
			$lists['month'] = $filter_month;
		} else {
			if($groups_mode && $filt_group) {
				$query = "SELECT count(b.user_id) as count_hits, page_id, EXTRACT( YEAR FROM $track_time) as year, EXTRACT( MONTH FROM $track_time) as month FROM #__lms_track_hits as b, #__lms_users_in_groups as c, #__lms_users_in_global_groups as d"
				. "\n WHERE b.course_id = '".$id."' AND b.course_id = c.course_id AND b.page_id <> 0 AND b.user_id = c.user_id AND d.user_id = b.user_id"
				. "\n AND EXTRACT( YEAR FROM $track_time) = '".$filter_year."'"
				. ($filter_stu ? ("\n AND b.user_id = '".$filter_stu."'") : '')
				. ($filt_group ? ("\n AND d.group_id = '".$filt_group."'") : '')
				. ($filt_subgroup ? ("\n AND d.subgroup1_id = '".$filt_subgroup."'") : '')
				. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
				. "\n GROUP BY year, month, page_id"
				. "\n ORDER BY year, month, page_id"
				;
			}
			else {
				$query = "SELECT count(b.user_id) as count_hits, page_id, EXTRACT( YEAR FROM $track_time) as year, EXTRACT( MONTH FROM $track_time) as month FROM #__lms_track_hits as b, #__lms_users_in_groups as c"
				. "\n WHERE b.course_id = '".$id."' AND b.course_id = c.course_id AND b.page_id <> 0 AND b.user_id = c.user_id"
				. "\n AND EXTRACT( YEAR FROM $track_time) = '".$filter_year."'"
				. ($filter_stu ? ("\n AND b.user_id = '".$filter_stu."'") : '')
				. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
				. "\n GROUP BY year, month, page_id"
				. "\n ORDER BY year, month, page_id"
				;
			}
				
			$lists['is_month'] = false;
		}
		$JLMS_DB->SetQuery( $query );
		$rows = $JLMS_DB->LoadObjectList();

		$latest_activities = JLMS_tracking_get_latest_activities($id, 0, 10, $members);

		$lists['year'] = $filter_year;
		$query = "SELECT distinct EXTRACT( YEAR FROM $track_time) as value, EXTRACT( YEAR FROM $track_time) as text FROM #__lms_track_hits as b"
		. "\n WHERE b.course_id = '".$id."' AND b.page_id <> 0"
		. "\n ORDER BY value"
		;
		$JLMS_DB->SetQuery( $query );
		$y_items = array();
		$y_items = $JLMS_DB->LoadObjectList();
		$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=tracking&amp;id=$id";
		$link = $link ."&amp;filter_year=".JLMS_SELECTED_INDEX_MARKER;			
		$link = processSelectedIndexMarker( $link );
		$lists['filter'] = mosHTML::selectList($y_items, 'filter_year', 'class="inputbox" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filter_year );
		$query = "SELECT distinct EXTRACT( MONTH FROM $track_time) as value, EXTRACT( MONTH FROM $track_time) as text FROM #__lms_track_hits as b"
		. "\n WHERE b.course_id = '".$id."' AND b.page_id <> 0"
		. "\n ORDER BY value"
		;
		$JLMS_DB->SetQuery( $query );
		$m_items = array();
		$mmmm = new stdClass();
		$mmmm->value = 0;
		$mmmm->text = '&nbsp;';
		$m_items[] = $mmmm;//mosHTML::makeOption(0, ' ');
		$mm = $JLMS_DB->LoadObjectList();
		$i = 0;
		while ($i<count($mm)) {
			$mm[$i]->text = month_lang(strftime("%m",mktime(0,0,0,$mm[$i]->value+1,0,0)),0,2);
			$i ++;
		}
		$m_items = array_merge($m_items, $mm);
		$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=tracking&amp;id=$id";
		$link = $link ."&amp;filter_month=".JLMS_SELECTED_INDEX_MARKER;			
		$link = processSelectedIndexMarker( $link );
		$lists['filter_month'] = mosHTML::selectList($m_items, 'filter_month', 'class="inputbox" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filter_month );
		$students = array();
		$r = new stdClass();
		$r->id = 0;$r->username = _JLMS_SB_ALL_USERS;$r->name = '';$r->email = '';$r->ug_name = '';
		$students[] = $r;
		$students = array_merge($students, JLMS_getCourseStudentsList($id, $filt_group, $filt_subgroup));
		
		if ($JLMS_CONFIG->get('use_global_groups', 1) && $JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only')) {
			foreach( $students as $k=>$v) {
				if( !in_array($v->id,$members_array) ) {
					unset($students[$k]);					
				}
			}
		}
		
		$students_tun = array();
		foreach($students as $st) {
			$sto = new stdClass();
			$sto->value = $st->id;
			$sto->text = $st->username;
			$students_tun[] = $sto;
		}
		$link = "index.php?option=$option&amp;Itemid=$Itemid&task=tracking&id=$id";
		$link = $link ."&amp;filter_stu=".JLMS_SELECTED_INDEX_MARKER;			
		$link = processSelectedIndexMarker( $link );
		$lists['filter_stu'] = mosHTML::selectList($students_tun, 'filter_stu', 'class="inputbox jlms_users_box_track" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filter_stu );

		// 21 May 2007
		// prepare tracking images
		$track_images = array();

		// 18 August 2007 - changes (DEN) - added check for GD and FreeType support
		$generate_images = true;
		$msg = '';
		if (!function_exists('imageftbbox') || !function_exists('imagecreatetruecolor')) {
			$generate_images = false;
			$sec = false;
			if (!function_exists('imagecreatetruecolor')) {
				$msg = 'This function requires GD 2.0.1 or later (2.0.28 or later is recommended).';
				$sec = true;
			}
			if (!function_exists('imageftbbox')) {
				$msg .= ($sec?'<br />':'').'This function is only available if PHP is compiled with freetype support.';
			}
		} // end of GD and FreeType support check


		if ($JLMS_CONFIG->get('temp_folder', '') && $generate_images) { // temp folder setup is ready.
			$img_gl_width = $JLMS_CONFIG->get('visual_set_tracking_image_base_width', 400); //parameter added 09.06.2007
			$img_gl_height = $JLMS_CONFIG->get('visual_set_tracking_image_base_height', 260); //parameter added 09.06.2007
			require_once(_JOOMLMS_FRONT_HOME . "/includes/libchart/libchart.php");
			JLMS_cleanLibChartCache();

			// Year/Month statistic
			$img_names = array();
			$img_counts = array();
			$i = 0;
			while ($i < count($rows)) {
				$row = $rows[$i];
				$subtotal = 0;
				if ($lists['is_month']) {
					$img_names[] = JLMS_dateToDisplay(mktime(0,0,0,$lists['month'],$row->month,$lists['year']), true);
				} else {
					$img_names[] = month_lang(strftime("%m", mktime(0,0,0,$row->month+1,0,0)),0,2);
				}
				$month = $rows[$i]->month;
				$year = $rows[$i]->year;
				do {
					if ($rows[$i]->month == $month && $rows[$i]->year == $year) {
						$subtotal = $subtotal + $rows[$i]->count_hits;
					}
					$i ++;
				} while($i < count($rows) && $rows[$i]->month == $month && $rows[$i]->year == $year);
				$img_counts[] = $subtotal;
				if (isset($rows[$i]->month) && $rows[$i]->month != $month) { $i --;}
				$i ++;
			}

			$chart = new VerticalChart($img_gl_width, $img_gl_height, 'no_title');//(700, 70 + count($img_names)*30);
			for ($i = 0, $n = count($img_names); $i < $n; $i ++) {
				$chart->addPoint(new Point($img_names[$i], $img_counts[$i]));
			}
			if ($lists['is_month']) {
				$title = _JLMS_TRACK_IMG_MONTH_STATS;
			} else {
				$title = _JLMS_TRACK_IMG_YEAR_STATS;
			}
			$title = JLMS_TR_temp_fix($title);

			$mas[0]=$title;
			
			$title = '';
			
			$chart->setTitle($title);
			$filename = time() . '_' . md5(uniqid(rand(), true)) . ".png";
			$new_image_ym_stats = new stdClass();
			$new_image_ym_stats->filename = $filename; $new_image_ym_stats->width = $img_gl_width; $new_image_ym_stats->height = $img_gl_height; $new_image_ym_stats->alt = $title;
			$new_image_ym_stats->title = $mas[0];
			$chart->render(JPATH_SITE . "/".$JLMS_CONFIG->get('temp_folder', '')."/$filename");

			// Tools statistics
			$img_names = array();
			$img_counts = array();
			for ($j=0;$j<11;$j++) {
				$img_counts[$j] = 0;
			}
			$i = 0;
			while ($i < count($rows)) {
				$row = $rows[$i];
	
				$month = $rows[$i]->month;
				$year = $rows[$i]->year;
				$j = 1;
				do {
					while ($j < $rows[$i]->page_id) {
						$j ++;
					}
					if ($rows[$i]->month == $month && $rows[$i]->year == $year) {
						$img_counts[$j-1] = $img_counts[$j-1] + $rows[$i]->count_hits;
					}
					$j ++;
					$i ++;
				} while($i < count($rows) && $rows[$i]->month == $month && $rows[$i]->year == $year);
				while ($j <=11) {
					$j ++;
				}
				if (isset($rows[$i]->month) && $rows[$i]->month != $month) { $i --;}
				$i ++;
			}
			for ($j=0;$j<11;$j++) {
				$img_names[$j] = JLMS_TRACKING_getTitle($j + 1) . " (".$img_counts[$j] .")";
			}

			$chart = new PieChart($img_gl_width*2, $img_gl_height+40, false, 'no_title');//(700, 70 + count($img_names)*30);
			for ($i = 0, $n = count($img_names); $i < $n; $i ++) {
				$chart->addPoint(new Point($img_names[$i], $img_counts[$i]));
			}
			$title = _JLMS_TRACK_TITLE_ACCESS . JLMS_TRACKING_getTitle(null);
			$title = JLMS_TR_temp_fix($title);
			
			$mas[1]=$title;
			$title = '';
			
			$chart->setTitle($title);
			
			$filename = time() . '_' . md5(uniqid(rand(), true)) . ".png";
			$new_image_tools_stats = new stdClass();
			$new_image_tools_stats->filename = $filename; $new_image_tools_stats->width = $img_gl_width*2; $new_image_tools_stats->height = $img_gl_height+40; $new_image_tools_stats->alt = $title;
			$new_image_tools_stats->title = $mas[1];
			$chart->render(JPATH_SITE . "/".$JLMS_CONFIG->get('temp_folder', '')."/$filename");

			// Most active users:
			$all_users = array();
			if ($filter_month) {
				if($groups_mode && $filt_group) {
					$query = "SELECT count(b.user_id) as count_hits, b.user_id, a.username, a.name FROM #__lms_track_hits as b, #__users as a, #__lms_users_in_groups as c, #__lms_users_in_global_groups as d"
					. "\n WHERE b.course_id = '".$id."' AND b.course_id = c.course_id AND b.page_id <> 0 AND b.user_id = a.id AND b.user_id = c.user_id AND d.user_id = b.user_id"
					. "\n AND EXTRACT( YEAR FROM $track_time) = '".$filter_year."'"
					. "\n AND EXTRACT( MONTH FROM $track_time) = '".$filter_month."'"
					. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
					. ($filt_group ? ("\n AND d.group_id = '".$filt_group."'") : '')
					. ($filt_subgroup ? ("\n AND d.subgroup1_id = '".$filt_subgroup."'") : '')
					. "\n GROUP BY b.user_id, a.username, a.name"
					. "\n ORDER BY count_hits"
					. "\n LIMIT 0, 10"
					;
				}
				else {
					$query = "SELECT count(b.user_id) as count_hits, b.user_id, a.username, a.name FROM #__lms_track_hits as b, #__users as a, #__lms_users_in_groups as c"
					. "\n WHERE b.course_id = '".$id."' AND b.course_id = c.course_id AND b.page_id <> 0 AND b.user_id = a.id AND b.user_id = c.user_id"
					. "\n AND EXTRACT( YEAR FROM $track_time) = '".$filter_year."'"
					. "\n AND EXTRACT( MONTH FROM $track_time) = '".$filter_month."'"
					. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
					. "\n GROUP BY b.user_id, a.username, a.name"
					. "\n ORDER BY count_hits"
					. "\n LIMIT 0, 10"
					;
				}
				
			} else {
				$query = "SELECT count(b.user_id) as count_hits, b.user_id, a.username, a.name FROM #__lms_track_hits as b, #__users as a, #__lms_users_in_groups as c"
				. "\n WHERE b.course_id = '".$id."' AND b.course_id = c.course_id AND b.page_id <> 0 AND b.user_id = a.id AND b.user_id = c.user_id"
				. "\n AND EXTRACT( YEAR FROM $track_time) = '".$filter_year."'"
				. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
				. "\n GROUP BY b.user_id, a.username, a.name"
				. "\n ORDER BY count_hits"
				. "\n LIMIT 0, 10"
				;
			}
			$JLMS_DB->SetQuery( $query );
			$urows = $JLMS_DB->LoadObjectList();
			
			$chart = new PieChart($img_gl_width*2, $img_gl_height ,true, 'no_title');//(700, 70 + count($img_names)*30);
			for ($i = 0, $n = count($urows); $i < $n; $i ++) {
				$chart->addPoint(new Point($urows[$i]->name.", ".$urows[$i]->username . " (".$urows[$i]->count_hits.")", $urows[$i]->count_hits));
			}
			$title = _JLMS_TRACK_IMG_MOST_ACTIVE;
			$title = JLMS_TR_temp_fix($title);
			
			$mas[2]=$title;
			$title = '';
			
			$chart->setTitle($title);
			$filename = time() . '_' . md5(uniqid(rand(), true)) . ".png";
			$new_image_most_active = new stdClass();
			$new_image_most_active->filename = $filename; $new_image_most_active->width = $img_gl_width*2; $new_image_most_active->height = $img_gl_height; $new_image_most_active->alt = $title;
			$new_image_most_active->title = $mas[2];
			$chart->render(JPATH_SITE . "/".$JLMS_CONFIG->get('temp_folder', '')."/$filename");

			// Daily distribution
			$all_users = array();
			if ($filter_month) {
				if($groups_mode && $filt_group) {
					$query = "SELECT count(b.user_id) as count_hits, EXTRACT( HOUR FROM $track_time) tr_hour FROM #__lms_track_hits as b, #__lms_users_in_groups as c, #__lms_users_in_global_groups as d"
					. "\n WHERE b.course_id = '".$id."' AND b.course_id = c.course_id AND b.page_id <> 0 AND b.user_id = c.user_id AND d.user_id = b.user_id"
					. "\n AND EXTRACT( YEAR FROM $track_time) = '".$filter_year."'"
					. "\n AND EXTRACT( MONTH FROM $track_time) = '".$filter_month."'"
					. ($filter_stu ? ("\n AND b.user_id = '".$filter_stu."'") : '')
					. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
					. ($filt_group ? ("\n AND d.group_id = '".$filt_group."'") : '')
					. ($filt_subgroup ? ("\n AND d.subgroup1_id = '".$filt_subgroup."'") : '')
					. "\n GROUP BY tr_hour"
					. "\n ORDER BY tr_hour"
					//. "\n LIMIT 0, 10"
					;
				}
				else {
					$query = "SELECT count(b.user_id) as count_hits, EXTRACT( HOUR FROM $track_time) tr_hour FROM #__lms_track_hits as b, #__lms_users_in_groups as c"
					. "\n WHERE b.course_id = '".$id."' AND b.course_id = c.course_id AND b.page_id <> 0 AND b.user_id = c.user_id"
					. "\n AND EXTRACT( YEAR FROM $track_time) = '".$filter_year."'"
					. "\n AND EXTRACT( MONTH FROM $track_time) = '".$filter_month."'"
					. ($filter_stu ? ("\n AND b.user_id = '".$filter_stu."'") : '')
					. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
					. "\n GROUP BY tr_hour"
					. "\n ORDER BY tr_hour"
					//. "\n LIMIT 0, 10"
					;
				}
			} else {
				$query = "SELECT count(b.user_id) as count_hits, EXTRACT( HOUR FROM $track_time) tr_hour FROM #__lms_track_hits as b, #__lms_users_in_groups as c"
				. "\n WHERE b.course_id = '".$id."' AND b.course_id = c.course_id AND b.page_id <> 0 AND b.user_id = c.user_id"
				. "\n AND EXTRACT( YEAR FROM $track_time) = '".$filter_year."'"
				. ($filter_stu ? ("\n AND b.user_id = '".$filter_stu."'") : '')
				. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
				. "\n GROUP BY tr_hour"
				. "\n ORDER BY tr_hour"
				//. "\n LIMIT 0, 10"
				;
			}
			$JLMS_DB->SetQuery( $query );
			$old_throws = $JLMS_DB->LoadObjectList();
			
			
			$throws = array();
			for ($i = 0; $i <24; $i ++) {
				$is_exists = false;
				foreach ($old_throws as $throw) {
					if ($throw->tr_hour == $i) {
						$is_exists = true;
						$throws[] = $throw;
						break;
					}
				}
				if (!$is_exists) {
					$rt = new stdClass();
					$rt->count_hits = 0;
					$rt->tr_hour = $i;
					$throws[] = $rt;
				}
			}
			$chart = new HorizontalChart($img_gl_width, $img_gl_height*2, 'no_title');//(700, 70 + count($img_names)*30);
			for ($i = count($throws)-1; $i >= 0; $i --) {
				$chart->addPoint(new Point(str_pad($throws[$i]->tr_hour,2,'0', STR_PAD_LEFT).":00 - ".str_pad($throws[$i]->tr_hour+1,2,'0', STR_PAD_LEFT).":00", $throws[$i]->count_hits));
			}
			$title = _JLMS_TRACK_IMG_DAILY;
			$title = JLMS_TR_temp_fix($title);
			
			$mas[3] = $title;
			$title = '';
			
			$chart->setTitle($title);
			$filename = time() . '_' . md5(uniqid(rand(), true)) . ".png";
			$new_image_daily = new stdClass();
			$new_image_daily->filename = $filename; $new_image_daily->width = $img_gl_width; $new_image_daily->height = $img_gl_height*2; $new_image_daily->alt = $title;
			$new_image_daily->title = $mas[3];
			$chart->render(JPATH_SITE . "/".$JLMS_CONFIG->get('temp_folder', '')."/$filename");


			// Weekly distribution
			$all_users = array();
			if ($filter_month) {
				if($groups_mode && $filt_group) {
					//NOTE: there is no bug with users_in_global_groups table here, because filt_group is enabled and therefore we will query only one record from in_global_groups table
					$query = "SELECT count(b.user_id) as count_hits, WEEKDAY($track_time) tr_hour FROM #__lms_track_hits as b, #__lms_users_in_groups as c, #__lms_users_in_global_groups as d"
					. "\n WHERE b.course_id = '".$id."' AND b.course_id = c.course_id AND b.page_id <> 0 AND b.user_id = c.user_id AND d.user_id = b.user_id"
					. "\n AND EXTRACT( YEAR FROM $track_time) = '".$filter_year."'"
					. "\n AND EXTRACT( MONTH FROM $track_time) = '".$filter_month."'"
					. ($filter_stu ? ("\n AND b.user_id = '".$filter_stu."'") : '')
					. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
					. ($filt_group ? ("\n AND d.group_id = '".$filt_group."'") : '')
					. ($filt_subgroup ? ("\n AND d.subgroup1_id = '".$filt_subgroup."'") : '')
					. "\n GROUP BY tr_hour"
					. "\n ORDER BY tr_hour"
					//. "\n LIMIT 0, 10"
					;
				}
				else {
					$query = "SELECT count(b.user_id) as count_hits, WEEKDAY($track_time) tr_hour FROM #__lms_track_hits as b, #__lms_users_in_groups as c"
					. "\n WHERE b.course_id = '".$id."' AND b.course_id = c.course_id AND b.page_id <> 0 AND b.user_id = c.user_id"
					. "\n AND EXTRACT( YEAR FROM $track_time) = '".$filter_year."'"
					. "\n AND EXTRACT( MONTH FROM $track_time) = '".$filter_month."'"
					. ($filter_stu ? ("\n AND b.user_id = '".$filter_stu."'") : '')
					. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
					. "\n GROUP BY tr_hour"
					. "\n ORDER BY tr_hour"
					//. "\n LIMIT 0, 10"
					;
				}
			} else {
				$query = "SELECT count(b.user_id) as count_hits, WEEKDAY($track_time) tr_hour FROM #__lms_track_hits as b, #__lms_users_in_groups as c"
				. "\n WHERE b.course_id = '".$id."' AND b.course_id = c.course_id AND b.page_id <> 0 AND b.user_id = c.user_id"
				. "\n AND EXTRACT( YEAR FROM $track_time) = '".$filter_year."'"
				. ($filter_stu ? ("\n AND b.user_id = '".$filter_stu."'") : '')
				. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
				. "\n GROUP BY tr_hour"
				. "\n ORDER BY tr_hour"
				//. "\n LIMIT 0, 10"
				;
			}
			$JLMS_DB->SetQuery( $query );
			$old_throws = $JLMS_DB->LoadObjectList();
			
			$throws = array();
			for ($i = 0; $i <7; $i ++) {
				$is_exists = false;
				foreach ($old_throws as $throw) {
					if ($throw->tr_hour == $i) {
						$is_exists = true;
						$throws[] = $throw;
						break;
					}
				}
				if (!$is_exists) {
					$rt = new stdClass();
					$rt->count_hits = 0;
					$rt->tr_hour = $i;
					$throws[] = $rt;
				}
			}
			$chart->setLabelMarginLeft(50);
			global $JLMS_CONFIG;
			$date_format_fdow = $JLMS_CONFIG->get('date_format_fdow');
			$t_ar = array();
			$t_ar[] = 'Monday'; $t_ar[] = 'Tuesday'; $t_ar[] = 'Wednesday'; $t_ar[] = 'Thursday'; $t_ar[] = 'Friday'; $t_ar[] = 'Saturday'; $t_ar[] = 'Sunday';

			$chart = new HorizontalChart($img_gl_width, $img_gl_height, 'no_title');//(700, 70 + count($img_names)*30);
			if ($date_format_fdow == 1) {
				$beg = 0;
				$end = count($throws);
			} else {
				$beg = 0;
				$end = count($throws) - 1;
	
			}
			for ($i = $end-1; $i >= $beg; $i --) {
				$chart->addPoint(new Point(day_month_lang(date('w',strtotime($t_ar[$i])),0,1,0,0), $throws[$i]->count_hits));
			}
			if ($date_format_fdow == 1) {
				//
			} else {
				$chart->addPoint(new Point(day_month_lang(date('w',strtotime($t_ar[6])),0,1,0,0), $throws[6]->count_hits));
			}
			
			$title = _JLMS_TRACK_IMG_WEEKLY;
			$title = JLMS_TR_temp_fix($title);
			
			$mas[4] = $title;
			$title = '';
			
			$chart->setTitle($title);
			$filename = time() . '_' . md5(uniqid(rand(), true)) . ".png";
			$new_image_weekly = new stdClass();
			$new_image_weekly->filename = $filename; $new_image_weekly->width = $img_gl_width; $new_image_weekly->height = $img_gl_height; $new_image_weekly->alt = $title;
			$new_image_weekly->title = $mas[4];
			$chart->render(JPATH_SITE . "/".$JLMS_CONFIG->get('temp_folder', '')."/$filename");


			$track_images[] = $new_image_ym_stats;
			$track_images[] = $new_image_weekly;
			$track_images[] = $new_image_daily;
			$track_images[] = $new_image_tools_stats;
			
			if(!$filter_stu) {
				$track_images[] = $new_image_most_active;
			}
		}

		//filer groups
		if ($JLMS_CONFIG->get('use_global_groups', 1)) {
			
			$g_items = array();
			$g_items[] = mosHTML::makeOption(0, _JLMS_ATT_FILTER_ALL_GROUPS);
			
			$query = "SELECT distinct a.id as value, a.ug_name as text"
			. "\n FROM #__lms_usergroups as a"
			. "\n WHERE a.course_id = 0"
			. "\n AND a.parent_id = 0"
			. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND a.id IN ($groups_where_admin_manager)") :'')			
			. "\n ORDER BY a.ug_name"
			;
		
			$JLMS_DB->SetQuery( $query );
			$groups = $JLMS_DB->LoadObjectList();
			
			$g_items = array_merge($g_items, $groups);
			$link = "index.php?option=$option&amp;Itemid=$Itemid&task=tracking&amp;id=$id";
			$link = $link ."&amp;filt_group=".JLMS_SELECTED_INDEX_MARKER;			
			$link = processSelectedIndexMarker( $link );
			$lists['filter2'] = mosHTML::selectList($g_items, 'filt_group', 'class="inputbox" style="width:250px;" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filt_group );
			
			if($filt_group) {
				$g_items = array();
				$g_items[] = mosHTML::makeOption(0, _JLMS_FILTER_ALL_SUBGROUPS);
	
				$query = "SELECT distinct a.id as value, a.ug_name as text"
				. "\n FROM #__lms_usergroups as a"
				. "\n WHERE a.owner_id = 0 AND a.course_id = 0"
				. "\n AND a.parent_id = $filt_group"
				. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND a.parent_id IN ($groups_where_admin_manager)") :'')				
				. "\n ORDER BY a.ug_name"
				;
				$JLMS_DB->SetQuery( $query );
				$sbugroups = $JLMS_DB->LoadObjectList();
				
				if(count($sbugroups)) {
					$g_items = array_merge($g_items, $sbugroups);
					$link = "index.php?option=$option&amp;Itemid=$Itemid&task=tracking&id=$id";
					$link = $link ."&amp;filt_group=".$filt_group."&amp;filt_subgroup=".JLMS_SELECTED_INDEX_MARKER;			
					$link = processSelectedIndexMarker( $link );
					$lists['filter3'] = mosHTML::selectList($g_items, 'filt_subgroup', 'class="inputbox" style="width:250px;" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filt_subgroup );
				}
			}
		}

		JLMS_tracking_html::showTracking( $id, $option, $rows, $lists, $track_images, $latest_activities, $msg );
	} else {
		//JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid"));
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$id"));
	}
}
function JLMS_days_in_month($a_month, $a_year) {
   return date('t', strtotime($a_year . '-' . $a_month . '-01'));
}
function JLMS_showpageTracking( $id, $option, $page) {
	global $my, $JLMS_DB, $Itemid, $JLMS_SESSION, $JLMS_CONFIG;
	$usertype = JLMS_GetUserType($my->id, $id);

	$JLMS_ACL = JLMSFactory::getACL();
	$is_teacher = $JLMS_ACL->isTeacher();
	
	$groups_mode = $JLMS_CONFIG->get('use_global_groups', 1);
	
	$view = mosGetParam($_REQUEST, 'view', '');
	$reporting_header = array();

	/* time offsets */
	$offset_hours = 0;
	$offset_minutes = 0;
	$total_offset = $JLMS_CONFIG->get('offset');
	$track_time = 'b.track_time';
	/* time offsets */
	
	
	$lists = array();
	
	$lists['f_lpath'] = 0;

	if ($id && $JLMS_ACL->CheckPermissions('tracking', 'manage')) {
		$filter_year = intval( mosGetParam( $_REQUEST, 'filter_year', $JLMS_SESSION->get('filter_year', date('Y')) ) );
		$filter_month = intval( mosGetParam( $_REQUEST, 'filter_month', $JLMS_SESSION->get('filter_month', date('m')) ) );
		
		$filter_stu = intval( mosGetParam( $_REQUEST, 'filter_stu', $JLMS_SESSION->get('filter_stu', 0) ) );
		
		#$filter_day = intval( mosGetParam( $_REQUEST, 'filter_day', $JLMS_SESSION->get('filter_day', 0) ) );
		$filt_group = intval( mosGetParam( $_GET, 'filt_group', $JLMS_SESSION->get('filt_group', 0) ) );
		$filt_subgroup = intval( mosGetParam( $_GET, 'filt_subgroup', $JLMS_SESSION->get('filt_subgroup', 0) ) );
		
		$filter_lpath = intval( mosGetParam( $_REQUEST, 'filter_lpath', $JLMS_SESSION->get('filter_lpath', 0) ) );

		if (isset($_REQUEST['filter_day'])) {
			$filter_day = intval( mosGetParam( $_REQUEST, 'filter_day', 0 ) );
		} else {
			$filter_day = $JLMS_SESSION->get('filter_day', 0);
		}
		if (strlen(''+$filter_year) != 4) {
			$filter_year = date('Y');
		}
		
		if(!$filt_group) {
			$filt_subgroup = 0;
		}
		
		$JLMS_SESSION->set('filt_group', $filt_group);
		$JLMS_SESSION->set('filt_subgroup', $filt_subgroup);
		
		$members = "'0'";
		$members_array = array();
		if($JLMS_ACL->_role_type == 2 || $JLMS_ACL->_role_type == 4) {
			if($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only')) {
				$groups_where_admin_manager = JLMS_ACL_HELPER::GetAssignedGroups($my->id, $id);
					
				if(count($groups_where_admin_manager)) {
					if(JLMS_ACL_HELPER::GetCountAssignedGroups($my->id, $id) == 1) {	
						$filt_group = $groups_where_admin_manager[0];
					}
				}
				
				if(count($groups_where_admin_manager)) {
					$groups_where_admin_manager = implode(',', $groups_where_admin_manager);
						if($groups_where_admin_manager != '') {
							$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE (group_id IN ($groups_where_admin_manager) OR subgroup1_id IN ($groups_where_admin_manager))"
								. ($filt_group ? ("\n AND group_id = '".$filt_group."'") : '')
								. ($filt_subgroup ? ("\n AND subgroup1_id = '".$filt_subgroup."'") : '')
							;
							
							$JLMS_DB->setQuery($query);
							$members = JLMSDatabaseHelper::LoadResultArray();
							
							$members_array = $members;
							
							$members = implode(',', $members);
							if($members == '')
								$members = "'0'";
						}
				}			
			}
		}
		
		if (!in_array($filter_month,array(1,2,3,4,5,6,7,8,9,10,11,12))) { $filter_month = date('m'); }
		$m_ar = array();
		$i = 0;
		if ($filter_day) {
			while ($i < 5) {
				$r = new stdClass();
				$r->day = $filter_day - 2 + $i;
				$t = JLMS_days_in_month($filter_month, $filter_year);
				if ($r->day < 1) {
					$r->month = $filter_month - 1;
					$r->year = $filter_year;
					if ($r->month < 1) { $r->month = $r->month + 12; $r->year = $filter_year-1; }
					
					$t = JLMS_days_in_month($r->month, $r->year);
					$r->day = $r->day + $t;
				} elseif ($r->day > $t) {
					$r->month = $filter_month + 1;
					$r->year = $filter_year;
					if ($r->month > 12) { $r->month = $r->month - 12; $r->year = $filter_year+1; }
					$t = JLMS_days_in_month($r->month, $r->year);
					$r->day = $r->day - $t;
				} else {
					$r->month = $filter_month;
					$r->year = $filter_year;
				}
				$i ++;
				$m_ar[] = $r;
			}
			$lists['is_day'] = true;
			$lists['day'] = $filter_day;
		} else {
			while ($i < 5) {
				$r = new stdClass();
				$r->month = $filter_month - 2 + $i;
				if ($r->month < 1) { $r->month = $r->month + 12; $r->year = $filter_year-1;
				} elseif ($r->month > 12) { $r->month = $r->month - 12; $r->year = $filter_year+1;
				} else { $r->year = $filter_year;}
				$i ++;
				$m_ar[] = $r;
			}
			$lists['is_day'] = false;
		}
		#$filter_stu = intval( mosGetParam( $_REQUEST, 'filter_stu', $JLMS_SESSION->get('filter_stu', 0) ) );
		$JLMS_SESSION->set('filter_year', $filter_year);
		$JLMS_SESSION->set('filter_month', $filter_month);
		$JLMS_SESSION->set('filter_day', $filter_day);
		$JLMS_SESSION->set('filter_stu', $filter_stu);
		#$JLMS_SESSION->set('filter_stu', $filter_stu);
		
		$query = "SELECT count(b.user_id) as count_hits, b.page_id, b.user_id, u.username,"
		. "\n EXTRACT( YEAR FROM $track_time) as year,"
		. "\n EXTRACT( MONTH FROM $track_time) as month"
		. ($filter_day ? "\n , EXTRACT( DAY FROM $track_time) as day":'' )
		. "\n FROM #__lms_track_hits as b LEFT JOIN #__users as u ON u.id = b.user_id, #__lms_users_in_groups as c";

		if($groups_mode && $filt_group) {
			$query .= ", #__lms_users_in_global_groups as d";	
		}
		
		$query .= " WHERE b.course_id = '".$id."' AND b.course_id = c.course_id AND b.page_id <> 0 AND b.user_id = c.user_id AND c.course_id = '".$id."'"
		. ($filter_stu ? ("\n AND b.user_id = '".$filter_stu."'") : '')
		. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND b.user_id IN ($members)") :'')
		. "\n AND (";
		
		if ($filter_day) {
			$i = 0;
			foreach ($m_ar as $ma) {
				$query .= "\n ( EXTRACT( YEAR FROM $track_time) = '".$ma->year."' AND EXTRACT( MONTH FROM $track_time) = '".$ma->month."' AND EXTRACT( DAY FROM $track_time) = '".$ma->day."' )".(($i<4)?' OR':'');
				$i ++;
			}
		} else {
			$i = 0;
			foreach ($m_ar as $ma) {
				$query .= "\n ( EXTRACT( YEAR FROM $track_time) = '".$ma->year."' AND EXTRACT( MONTH FROM $track_time) = '".$ma->month."' )".(($i<4)?' OR':'');
				$i ++;
			}
		}
		$query .= "\n )"
		#. "\n AND EXTRACT( YEAR FROM b.track_time) = '".$filter_year."'"
		#. "\n AND EXTRACT( MONTH FROM b.track_time) IN('".($filter_month-1)."','".$filter_month."','".($filter_month+1)."')"
		#. ($filter_stu ? ("\n AND b.user_id = '".$filter_stu."'") : '')
		. "\n AND b.page_id = '".$page."'";
		
		if($groups_mode && $filt_group) {
			$query .= " AND d.user_id = b.user_id"
			. ($filt_group ? ("\n AND d.group_id = '".$filt_group."'") : '')
			. ($filt_subgroup ? ("\n AND d.subgroup1_id = '".$filt_subgroup."'") : '')
			;
		}	
		
		$query .= ($filter_day ? "\n GROUP BY u.username, year, month, day, b.page_id":"\n GROUP BY u.username, year, month, b.page_id")
			. ($filter_day ? "\n ORDER BY u.username, year, month, day, b.page_id":"\n ORDER BY u.username, year, month, b.page_id")
		;
		
		$JLMS_DB->SetQuery( $query );
		$rows = $JLMS_DB->LoadObjectList();
		
		$lists['year'] = $filter_year;
		$lists['page'] = $page;
		$lists['months'] = $m_ar;
		$lists['month'] = $filter_month;
		$query = "SELECT distinct EXTRACT( YEAR FROM $track_time) as value, EXTRACT( YEAR FROM $track_time) as text FROM #__lms_track_hits as b"
		. "\n WHERE b.course_id = '".$id."' AND b.page_id <> 0"
		. "\n ORDER BY value"
		;
		$JLMS_DB->SetQuery( $query );
		$y_items = array();
		$y_items = $JLMS_DB->LoadObjectList();
		$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=tracking&amp;id=$id&amp;page=$page";
		$link = $link ."&amp;filter_year=".JLMS_SELECTED_INDEX_MARKER;			
		$link = processSelectedIndexMarker( $link );
		$lists['filter'] = mosHTML::selectList($y_items, 'filter_year', 'class="inputbox" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filter_year );
		
		$query = "SELECT distinct EXTRACT( MONTH FROM $track_time) as value, EXTRACT( MONTH FROM $track_time) as text FROM #__lms_track_hits as b"
		. "\n WHERE b.course_id = '".$id."' AND b.page_id <> 0"
		. "\n AND EXTRACT( YEAR FROM $track_time) = '".$filter_year."'"
		. "\n ORDER BY value"
		;
		$JLMS_DB->SetQuery( $query );
		$m_items = array();
		$m_items = $JLMS_DB->LoadObjectList();
		$i = 0;
		while ($i<count($m_items)) {
			$m_items[$i]->text = month_lang(strftime('%m',mktime(0,0,0,$m_items[$i]->value+1,0,0)),0,2);
			$i ++;
		}
		$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=tracking&amp;id=$id&amp;page=$page";
		$link = $link ."&amp;filter_month=".JLMS_SELECTED_INDEX_MARKER;			
		$link = processSelectedIndexMarker( $link );
		$lists['filter_month'] = mosHTML::selectList($m_items, 'filter_month', 'class="inputbox" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filter_month );

		$query = "SELECT distinct EXTRACT( DAY FROM $track_time) as value, EXTRACT( DAY FROM $track_time) as text FROM #__lms_track_hits as b"
		. "\n WHERE b.course_id = '".$id."' AND b.page_id <> 0"
		. "\n AND EXTRACT( YEAR FROM $track_time) = '".$filter_year."'"
		. "\n AND EXTRACT( MONTH FROM $track_time) = '".$filter_month."'"
		. "\n ORDER BY value"
		;
		$JLMS_DB->SetQuery( $query );
		$d_items = array();
		$d_items[] = mosHTML::makeOption('&nbsp;', 0);
		$dd = $JLMS_DB->LoadObjectList();
		$i = 0;
		while ($i<count($dd)) {
			$dd[$i]->text = JLMS_dateToDisplay(mktime(0,0,0,$filter_month,$dd[$i]->value,$filter_year), true);
			$i ++;
		}
		$d_items = array_merge($d_items, $dd);
		$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=tracking&amp;id=$id&amp;page=$page";
		$link = $link ."&amp;filter_day=".JLMS_SELECTED_INDEX_MARKER;			
		$link = processSelectedIndexMarker( $link );
		$lists['filter_day'] = mosHTML::selectList($d_items, 'filter_day', 'class="inputbox" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filter_day );

		$students = array();
		$r = new stdClass();
		$r->id = 0;$r->username = _JLMS_SB_ALL_USERS;$r->name = '';$r->email = '';$r->ug_name = '';
		$students[] = $r;
		$students = array_merge($students, JLMS_getCourseStudentsList($id, $filt_group, $filt_subgroup));
		$students_tun = array();
		
		
		if ($JLMS_CONFIG->get('use_global_groups', 1) && $JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only')) {
			foreach( $students as $k=>$v) {
				if( !in_array($v->id,$members_array) ) {
					unset($students[$k]);					
				}
			}
		}
		
		foreach($students as $st) {
			$sto = new stdClass();
			$sto->value = $st->id;
			$sto->text = $st->username;
			$students_tun[] = $sto;
		}
		$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=tracking&amp;id=$id&amp;page=$page";
		$link .= $filter_lpath ? "&amp;filter_lpath=".$filter_lpath : '';
		$link = $link ."&amp;filter_stu=".JLMS_SELECTED_INDEX_MARKER;			
		$link = processSelectedIndexMarker( $link );
		$lists['filter_stu'] = mosHTML::selectList($students_tun, 'filter_stu', 'class="inputbox jlms_users_box_track" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filter_stu );
		
		$users_reporting = array();
		foreach($students_tun as $grp){
			if($filter_stu && $grp->value == $filter_stu){
				$users_reporting[] = $grp->text;
			}
		}
		$name_users_reporting[] = 'User';
		$reporting_header['name_users'] = $name_users_reporting;
		$reporting_header['users'] = $users_reporting;
		
		
		$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=tracking&amp;id=$id&amp;page=$page";
		$link .= $filter_stu ? "&amp;filter_stu=".$filter_stu : '';
		$link = $link ."&amp;filter_lpath=".JLMS_SELECTED_INDEX_MARKER;			
		$link = processSelectedIndexMarker( $link );		
		$query = "SELECT a.id as value, a.lpath_name as text"
		. "\n FROM #__lms_learn_paths as a "
		. "\n WHERE a.course_id = '".$id."'"
		. "\n ORDER BY a.ordering, a.lpath_name";
		$JLMS_DB->SetQuery( $query );
		$lpaths_data = $JLMS_DB->LoadObjectList();
		$lpaths = array();
		$lpaths[] = mosHTML::makeOption(0, _JLMS_FILTER_LPATHS);
		$lpaths = array_merge($lpaths, $lpaths_data);
		$lists['filter_lpath'] = mosHTML::selectList($lpaths, 'filter_lpath', 'class="inputbox" style="width:250px;" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filter_lpath );
		
		$pages = array();
		$i = 0;
		while ($i < 11 ) {
			$newpage = new stdClass();
			$newpage->value = ($i + 1);
			$newpage->text = JLMS_TRACKING_getTitle($i + 1);
			$pages[] = $newpage;
			$i ++;
		}
$add_js = "
		function jlms_change_tracking_page(sel_element) {
			var id = sel_element.options[sel_element.selectedIndex].value;
			var redirect_url = '';
			switch (id) {
";
foreach ($pages as $newpage) {
	$add_js .= "
				case '$newpage->value':
					redirect_url = '".sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=tracking&id=$id&page=".$newpage->value)."';
				break;
";
}

$add_js .= "
				default:
				break;
			}
			if (redirect_url) {
				top.location.href = redirect_url;
			}
		}
		";
$JLMS_CONFIG->set('jlms_aditional_js_code', $JLMS_CONFIG->get('jlms_aditional_js_code','').$add_js);
		/*$link = "index.php?option=$option&amp;Itemid=$Itemid&task=tracking&id=$id";
		$link = $link ."&amp;page='+this.options[selectedIndex].value+'";
		$link = sefRelToAbs($link);
		$link = str_replace('%5C%27',"'", $link);$link = str_replace('%5B',"[", $link);$link = str_replace('%5D',"]", $link);$link = str_replace('%20',"+", $link);$link = str_replace("\\\\\\","", $link);$link = str_replace('%27',"'", $link);
		*/
		$lists['filter_pages'] = mosHTML::selectList($pages, 'page', 'class="inputbox" size="1" onchange="jlms_change_tracking_page(this)"', 'value', 'text', $page );

		if ($page == 14) {
			$latest_activities = JLMS_tracking_get_latest_activities($id, 0, 500, $members, $filt_group, $filter_stu);
			$lists['page14_stats'] = & $latest_activities;
		}

		//page stats
		if ($page == 13 && $filter_stu && !$filter_lpath){// page '4' changed to the page '13'
			
			if($JLMS_CONFIG->get('enabled_current_step_scorm', false)){ //enabled SCORMs current step (not completed)
				$JLMS_SCS = new JLMS_SCORMCurrentStep();
				$JLMS_SCS->saveCurrentStep();
			}
			
			$query = "SELECT a.*, b.user_status as r_status, b.start_time as r_start, b.end_time as r_end"
			. "\n FROM #__lms_learn_paths as a"
			. "\n LEFT JOIN #__lms_learn_path_results as b"
			. "\n ON a.id = b.lpath_id AND b.course_id = '".$id."'"
			.($filter_stu ? "\n AND b.user_id = '".$filter_stu."'" : '')
			. "\n WHERE 1"
			. "\n AND a.course_id = '".$id."'"
			#. "\n AND a.published = 1"
			. "\n ORDER BY a.ordering";
			$JLMS_DB->SetQuery( $query );
			$lpath_stats = $JLMS_DB->LoadObjectList();
			
			//echo '<pre>';
			//print_r($lpath_stats);
			//echo '</pre>';

			//TODO: review thsi code... seems liek it incorrectly handles 'file library' resources
			for($i=0;$i<count($lpath_stats);$i++) {
				if($lpath_stats[$i]->lp_type == 2){
					$query = "SELECT scorm_package FROM #__lms_n_scorm WHERE id = '".$lpath_stats[$i]->item_id."'";
					$JLMS_DB->SetQuery( $query );
					$scorm_package = $JLMS_DB->LoadResult();

					$outer_doc = null;

					if ($scorm_package) {
						$query = "SELECT id FROM #__lms_n_scorm WHERE scorm_package = '".$scorm_package."' AND course_id = 0";
						$JLMS_DB->SetQuery( $query );
						$scorm_lib_id = $JLMS_DB->LoadResult();	
						
						$query = "SELECT outdoc_share, owner_id, allow_link FROM #__lms_outer_documents WHERE file_id = '".$scorm_lib_id."' AND folder_flag = 3";
						$JLMS_DB->SetQuery( $query );
						$outer_doc = $JLMS_DB->LoadObject();	
					}
					if(is_object($outer_doc) && isset($outer_doc->allow_link) && $outer_doc->allow_link == 1 ) {
					} else {
						unset($lpath_stats[$i]);
					}
				}
				
				$time_spent=0;
				if($JLMS_CONFIG->get('enable_timetracking', false)){
					if($filter_stu && $lpath_stats[$i]->id){
						$query = "SELECT SUM(time_spent)"
						. "\n FROM #__lms_time_tracking_resources"
						. "\n WHERE 1"
						. "\n AND course_id = '".$id."'"
						. "\n AND user_id = '".$filter_stu."'"
						. "\n AND resource_type = '9'"
						. "\n AND resource_id = '".$lpath_stats[$i]->id."'"
						;
						$JLMS_DB->setQuery($query);
						$time_spent = $JLMS_DB->loadResult() ? $JLMS_DB->loadResult() : 0;
					}
				}
				$lpath_stats[$i]->time_spent = JLMS_getTimeNormal($time_spent);
				
				if($JLMS_CONFIG->get('enabled_current_step_scorm', false)){ //enabled SCORMs current step (not completed)
					if($lpath_stats[$i]->lp_type){
						$lpath_stats[$i]->select_list_current_step = $JLMS_SCS->getInit($filter_stu, $lpath_stats[$i]->item_id);
					}
				}
			}
			
			$mas = array();
			foreach ($lpath_stats as $k=>$v) {
				$mas[] = $lpath_stats[$k];	
			}
			unset($lpath_stats);
			$lpath_stats = &$mas;

			$user_ids = array();
			$user_ids[] = $filter_stu;
			JLMS_LP_populate_results($id, $lpath_stats, $user_ids);
			
			/*
			$sc_ids = array();
			$scn_ids = array();
			foreach ($lpath_stats as $ls) {
				if ($ls->item_id) {
					if ($ls->lp_type == 1 || $ls->lp_type == 2) {
						$scn_ids[] = $ls->item_id;
					} else {
						$sc_ids[] = $ls->item_id;
					}
				}
			}
			if (count($sc_ids) || count($scn_ids)) {
				require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_scorm.lib.php");
				$user_ids = array();
				$user_ids[] = $filter_stu;
				if (count($sc_ids)) {
					$scorm_ans = & JLMS_GetSCORM_userResults($user_ids, $sc_ids);
					foreach ($scorm_ans as $sa) {
						$i = 0;
						while ($i < count($lpath_stats)) {
							if ($lpath_stats[$i]->item_id == $sa->content_id && !$lpath_stats[$i]->lp_type) {
								$lpath_stats[$i]->s_status = $sa->status;
								$lpath_stats[$i]->s_score = $sa->score;
							}
							$i ++;
						}
					}
				}
				if (count($scn_ids)) {
					global $JLMS_CONFIG;
					$course_params = $JLMS_CONFIG->get('course_params');
					$params_tt = new JLMSParameters($course_params);
					$scormn_ans = & JLMS_Get_N_SCORM_userResults($user_ids, $scn_ids, $params_tt->get('track_type', 0));
					foreach ($scormn_ans as $san) {
						$i = 0;
						while ($i < count($lpath_stats)) {
							if ($lpath_stats[$i]->item_id == $san->content_id && ($lpath_stats[$i]->lp_type==1 || $lpath_stats[$i]->lp_type==2)) {
								$lpath_stats[$i]->s_status = $san->status;
								$lpath_stats[$i]->s_score = $san->score;
								$lpath_stats[$i]->r_end = $san->scn_timemodified;
							}
							$i ++;
						}
					}
				}
			}
			*/
			
			$lists['page13_stats'] = & $lpath_stats;
		}
		if ($page == 13 && !$filter_stu && !$filter_lpath) { // show only list of LPath's/SCORM's
			$query = "SELECT a.*"
			. "\n FROM #__lms_learn_paths as a "
			. "\n WHERE a.course_id = '".$id."'"
			#. "\n AND a.published = 1"
			. "\n ORDER BY a.ordering";
			$JLMS_DB->SetQuery( $query );
			$lpath_stats = $JLMS_DB->LoadObjectList();

			for($i=0;$i<count($lpath_stats);$i++) {
				if($lpath_stats[$i]->lp_type == 2) {
					$query = "SELECT scorm_package FROM #__lms_n_scorm WHERE id = '".$lpath_stats[$i]->item_id."'";
					$JLMS_DB->SetQuery( $query );
					$scorm_package = $JLMS_DB->LoadResult();

					$outer_doc = null;

					if ($scorm_package) {
						$query = "SELECT id FROM #__lms_n_scorm WHERE scorm_package = '".$scorm_package."' AND course_id = 0";
						$JLMS_DB->SetQuery( $query );
						$scorm_lib_id = $JLMS_DB->LoadResult();	
						
						$query = "SELECT outdoc_share, owner_id, allow_link FROM #__lms_outer_documents WHERE file_id = '".$scorm_lib_id."' AND folder_flag = 3";
						$JLMS_DB->SetQuery( $query );
						$outer_doc = $JLMS_DB->LoadObject();	
					}
					if(is_object($outer_doc) && isset($outer_doc->allow_link) && $outer_doc->allow_link == 1 ) {
					} else {
						unset($lpath_stats[$i]);
					}
				}
			}

			$mas = array();
			foreach ($lpath_stats as $k=>$v) {
				$mas[] = $lpath_stats[$k];	
			}
			unset($lpath_stats);
			$lists['page13_stats'] = & $mas;
		}
		
		$lists['header_page'] = '';
		$lists['show_ajax_data'] = 0;
		
		if ($page == 13 && $filter_lpath){
			$filter_stus = array();
			if($filter_lpath){
				$query = "SELECT b.user_id"
				. "\n FROM #__lms_learn_paths as a"
				. "\n, #__lms_learn_path_results as b"
				. "\n, #__lms_users_in_groups as c"
				. "\n WHERE 1"
				. "\n AND a.id = b.lpath_id"
				. "\n AND b.course_id = c.course_id"
				. "\n AND b.user_id = c.user_id"
				. "\n AND a.course_id = '".$id."'"
				. "\n AND b.lpath_id = '".$filter_lpath."'"
				;
				$JLMS_DB->setQuery($query);
				$filter_stus = JLMSDatabaseHelper::LoadResultArray();
			}

			/*		
			$query = "SELECT DISTINCT(u.id) as user_id, u.username, u.name"
			. "\n, lp.*"
			. "\n, lpr1.user_status as r_status, lpr1.start_time as r_start, lpr1.end_time as r_end"
			. "\n FROM #__users as u, #__lms_users_in_groups as uig"
			. "\n LEFT JOIN #__lms_learn_path_results as lpr1"
			. "\n ON lpr1.user_id = uig.user_id AND lpr1.course_id = uig.course_id"
			. "\n, #__lms_learn_paths as lp"
			. "\n LEFT JOIN #__lms_learn_path_results as lpr2"
			. "\n ON lpr2.lpath_id = lp.id"
			. "\n WHERE 1"
			. "\n AND u.id = uig.user_id"
			. "\n AND uig.course_id = '".$id."'"
			.($filter_lpath ? "\n AND lp.id = '".$filter_lpath."'" : '')
			.($filter_stu ? "\n AND u.id = '".$filter_stu."'" : '')
//			.($filter_lpath && count($filter_stus) ? "\n AND u.id IN (".implode(',', $filter_stus).")" : '')
			. "\n ORDER BY lp.ordering"
			;
			*/

			$query = "SELECT *"
			. "\n FROM #__lms_learn_paths"
			. "\n WHERE id = '".$filter_lpath."'"
			;
			$JLMS_DB->SetQuery( $query );
			$lpaths = $JLMS_DB->LoadObjectList();
			
			//echo '<pre>';
			//print_r($lpaths);
			//echo '</pre>';
			
			if(isset($lpaths[0]) && $lpaths[0]->id){
				$lists['header_page'] = $lpaths[0]->lpath_name;
				if(in_array($lpaths[0]->lp_type, array(1,2))){
					$lists['show_ajax_data'] = 1;
				}
			}
			
							$query = "SELECT a.*, a.step_name as doc_name, c.file_name, b.file_id, b.folder_flag, f.link_href, e.c_title"
							. "\n FROM #__lms_learn_path_steps as a"
							. "\n LEFT JOIN #__lms_documents as b ON a.item_id = b.id AND a.step_type = 2"
							. "\n LEFT JOIN #__lms_files as c ON b.file_id = c.id"
							. "\n LEFT JOIN #__lms_links as f ON a.item_id = f.id AND a.step_type = 3"
							. "\n LEFT JOIN #__lms_quiz_t_quiz as e ON a.item_id = e.c_id AND a.step_type = 5"
							//. "\n LEFT JOIN #__lms_learn_path_conds as d ON a.cond_id = d.id"
							//. "\n LEFT JOIN #__lms_learn_path_steps as e ON d.ref_step = e.id"
							. "\n WHERE a.lpath_id = '".$filter_lpath."' AND a.course_id = '".$id."'"
							. "\n ORDER BY a.parent_id, a.ordering"
							;
							$JLMS_DB->SetQuery( $query );
							$lpath = $JLMS_DB->LoadObjectList();
					
							for($i=0;$i<count($lpath);$i++) {
					
								if($lpath[$i]->parent_id > 0 && $lpath[$i]->item_id > 0 && $lpath[$i]->step_type == 6) {
					
									$query = "SELECT item_id, lp_type FROM #__lms_learn_paths WHERE id = '".$lpath[$i]->item_id."'";
									$JLMS_DB->SetQuery( $query );
									$learn_path_info = $JLMS_DB->LoadObjectList();
									if (count($learn_path_info)) {
										$learn_path_id = $learn_path_info[0]->item_id;
										$lp_type = $learn_path_info[0]->lp_type;
										if(isset($scorm_package)) { unset($scorm_package); }
										if($learn_path_id && $lp_type == 2) {
											$query = "SELECT scorm_package FROM #__lms_n_scorm WHERE id = '".$learn_path_id."'";
											$JLMS_DB->SetQuery( $query );
											$scorm_package = $JLMS_DB->LoadResult();
											if(isset($scorm_package) && $scorm_package) {
												$query = "SELECT id FROM #__lms_n_scorm WHERE scorm_package = '".$scorm_package."'"
												. "\n AND course_id = 0"	
												. "\n LIMIT 1"
												;
												$JLMS_DB->SetQuery( $query );
												$scorm_lib_id = $JLMS_DB->LoadResult();
												if($scorm_lib_id) {			
													$query = "SELECT outdoc_share, owner_id, allow_link FROM #__lms_outer_documents WHERE file_id = '".$scorm_lib_id."' AND folder_flag = 3";
													$JLMS_DB->SetQuery( $query );
													$outer_doc = $JLMS_DB->LoadObject();
													if(is_object($outer_doc) && isset($outer_doc->allow_link) && $outer_doc->allow_link == 1 ) {
														// 'share to courses' is still enabled
													} else {
														// 'share to courses' is disabled.... show 'resource is not available' message to teacher
														$lpath[$i]->lpath_name = _JLMS_LP_RESOURSE_ISUNAV;
													}
												} else {
													$lpath[$i]->lpath_name = _JLMS_LP_RESOURSE_ISUNAV;
												}
											} else {
												$lpath[$i]->lpath_name = _JLMS_LP_RESOURSE_ISUNAV;
											}
										}
									}
								}
							}
					
							for($j=0;$j<count($lpath);$j++){
								if($lpath[$j]->folder_flag == 3){
									$query = "SELECT a.*, b.file_name FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0 WHERE a.id=".$lpath[$j]->file_id;
									$JLMS_DB->SetQuery( $query );
									$out_row = $JLMS_DB->LoadObjectList();
									if (count($out_row) && ($out_row[0]->allow_link == 1 )) {
										$lpath[$j]->file_id = $out_row[0]->file_id;
										$lpath[$j]->file_name = $out_row[0]->file_name;
									} else {
										$lpath[$j]->doc_name = _JLMS_LP_RESOURSE_ISUNAV;
									}
								}
							}
							$query = "SELECT * FROM #__lms_learn_path_conds WHERE course_id = '".$id."' AND lpath_id = '".$filter_lpath."'";
							$JLMS_DB->SetQuery( $query );
							$conds = $JLMS_DB->LoadObjectList();
							$t2 = 0;
							while ($t2 < count($lpath)) {
								$lpath[$t2]->is_condition = false;
								$t2 ++;
							}
							$t = 0;
							while ($t < count($conds)) {
								$step = $conds[$t]->step_id;
								$t2 = 0;
								while ($t2 < count($lpath)) {
									if ($lpath[$t2]->id == $step) {
										$lpath[$t2]->is_condition = true;
									}
									$t2 ++;
								}
								$t ++;
							}
							$lpath = JLMS_GetLPathTreeStructure($lpath);
							$lpath = AppendFileIcons_toList($lpath);
							
							for($i=0;$i<count($lpath);$i++){
								$query = "SELECT b.step_status"
								. "\n FROM #__lms_learn_path_results as a, #__lms_learn_path_step_results as b"
								. "\n WHERE a.id = b.result_id"
								. "\n AND a.course_id = '".$lpath[$i]->course_id."'"
								. "\n AND a.lpath_id = '".$lpath[$i]->lpath_id."'"
								. "\n AND a.user_id = '".$filter_stu."'"
								. "\n AND b.step_id = '".$lpath[$i]->id."'"
								;
								$JLMS_DB->setQuery($query);
								$step_status = $JLMS_DB->loadResult();
								if(!$step_status){
									$step_status = 0;
								}
								$lpath[$i]->step_status = $step_status;
							}
							
							//echo '<pre>';
							//print_r($query);
							//print_r($lpath);
							//echo '</pre>';
							
							$lists['lpath'] = $lpath;

			if ($filt_group) {
				$groups_mode = $JLMS_CONFIG->get('use_global_groups', 1);
				if ($groups_mode) {
					$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE group_id = '".$filt_group."'";
					$JLMS_DB->SetQuery( $query );
					$allowed_ids = JLMSDatabaseHelper::LoadResultArray();
					if (empty($allowed_ids)) {
						$allowed_ids_str = '0';
					} else {
						$allowed_ids_str = implode(',', $allowed_ids);
					}
					$query = "SELECT u.*, u.name as u_name"
					. "\n FROM #__users as u, #__lms_users_in_groups as uig"
					. "\n WHERE 1"
					. "\n AND u.id IN (".$allowed_ids_str.")"
					. "\n AND u.id = uig.user_id AND uig.course_id = '".$id."'"
					.($filter_stu ? "\n AND u.id = '".$filter_stu."'" : '')
					. "\n ORDER BY u.username"
					;
				} else {
					$query = "SELECT u.*, u.name as u_name"
					. "\n FROM #__users as u, #__lms_users_in_groups as uig"
					. "\n WHERE 1"
					. "\n AND u.id = uig.user_id AND uig.course_id = '".$id."'"
					. "\n AND uig.group_id = '".$filt_group."'"//filter by 'local' group
					.($filter_stu ? "\n AND u.id = '".$filter_stu."'" : '')
					. "\n ORDER BY u.username"
					;
				}
			} else {
				$query = "SELECT u.*, u.name as u_name"
				. "\n FROM #__users as u, #__lms_users_in_groups as uig"
				. "\n WHERE 1"
				. "\n AND u.id = uig.user_id AND uig.course_id = '".$id."'"
				.($filter_stu ? "\n AND u.id = '".$filter_stu."'" : '')
				. "\n ORDER BY u.username"
				;
			}
			$JLMS_DB->SetQuery( $query );
			$users = $JLMS_DB->loadObjectList();
			
			//echo '<pre>';
			//print_r($users);
			//echo '</pre>';

			$lpath_stats = array();
			for($i=0;$i<count($users);$i++){
				$user_details = $users[$i];
				$lpath_stats[$i] = new stdClass();
				$lpath_stats[$i]->user_id = $user_details->id;
				$lpath_stats[$i]->username = $user_details->username;
                $lpath_stats[$i]->u_name = $user_details->u_name;
                $lpath_stats[$i]->email = $user_details->email;

				$tmp = array();
				$tmp = get_object_vars($lpaths[0]);
				foreach($tmp as $key=>$value){
					$lpath_stats[$i]->{$key} = $value;
				}
			}

			//TODO: review thsi code... seems liek it incorrectly handles 'file library' resources
			for($i=0;$i<count($lpath_stats);$i++) {
				if($lpath_stats[$i]->lp_type == 2){
					$query = "SELECT scorm_package FROM #__lms_n_scorm WHERE id = '".$lpath_stats[$i]->item_id."'";
					$JLMS_DB->SetQuery( $query );
					$scorm_package = $JLMS_DB->LoadResult();

					$outer_doc = null;

					if ($scorm_package) {
						$query = "SELECT id FROM #__lms_n_scorm WHERE scorm_package = '".$scorm_package."' AND course_id = 0";
						$JLMS_DB->SetQuery( $query );
						$scorm_lib_id = $JLMS_DB->LoadResult();	
						
						$query = "SELECT outdoc_share, owner_id, allow_link FROM #__lms_outer_documents WHERE file_id = '".$scorm_lib_id."' AND folder_flag = 3";
						$JLMS_DB->SetQuery( $query );
						$outer_doc = $JLMS_DB->LoadObject();	
					}
					if(is_object($outer_doc) && isset($outer_doc->allow_link) && $outer_doc->allow_link == 1 ) {
					} else {
						unset($lpath_stats[$i]);
					}
				}
				
				$time_spent=0;
				if($JLMS_CONFIG->get('enable_timetracking', false)){
					if($filter_lpath /*&& count($filter_stus)*/ && $lpath_stats[$i]->id){
						$query = "SELECT SUM(time_spent)"
						. "\n FROM #__lms_time_tracking_resources"
						. "\n WHERE 1"
						. "\n AND course_id = '".$id."'"
						. "\n AND user_id = '".$lpath_stats[$i]->user_id."'"
						. "\n AND resource_type = '9'"
						. "\n AND resource_id = '".$lpath_stats[$i]->id."'"
						;
						$JLMS_DB->setQuery($query);
						$time_spent = $JLMS_DB->loadResult() ? $JLMS_DB->loadResult() : 0;
					}
				}
				$lpath_stats[$i]->time_spent = JLMS_getTimeNormal($time_spent);
			}
			
			for($i=0;$i<count($lpath_stats);$i++) {
			
				$lpath_in = array();
				$lpath_in[] = $lpath_stats[$i];
				
				$user_ids = array();
				$user_ids[] = $lpath_stats[$i]->user_id;
				
				JLMS_LP_populate_results($id, $lpath_in, $user_ids);
				
				$tmp = array();
				$tmp = get_object_vars($lpath_in[0]);
				foreach($tmp as $key=>$value){
					$lpath_stats[$i]->{$key} = $value;
				}
			}
						
			$lists['f_lpath'] = 1;
			
			$lists['page13_stats'] = $lpath_stats;
		}
		
		if ($page == 5 && $filter_stu) {
			$query = "SELECT a.*, b.id as result_id, b.hw_status, b.hw_date"
			. "\n FROM #__lms_homework as a LEFT JOIN #__lms_homework_results as b ON a.id = b.hw_id AND b.user_id = '".$filter_stu."' AND b.course_id = '".$id."'"
			. "\n WHERE a.course_id = '".$id."'"
			. "\n ORDER BY a.post_date, a.hw_name";
			$JLMS_DB->SetQuery( $query );
			$hw_stats = $JLMS_DB->LoadObjectList();
			$lists['page5_stats'] = & $hw_stats;
		}
		if ($page == 12 && $filter_stu) {
			$my_documents = GetTeacherDocuments($my->id, $id, 1);//in joomla_lms.main.php
			$my_documents = JLMS_GetTreeStructure( $my_documents );
			$my_documents = AppendFileIcons_toList( $my_documents );
			$doc_ids = array();
			foreach( $my_documents as $my_doc) {
				$doc_ids[] = $my_doc->id;
			}
			if (count($doc_ids)) {
				$doc_id_str = implode(',',$doc_ids);
				$query = "SELECT count(id) as count_downs, doc_id FROM #__lms_track_downloads as a WHERE user_id = '".$filter_stu."' AND doc_id IN ($doc_id_str)"
				. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND user_id IN ($members)") :'')
				."\n GROUP BY doc_id";
				$JLMS_DB->SetQuery( $query );
				$doc_downs_count = $JLMS_DB->LoadObjectList();
				$query = "SELECT max(track_time) as last_time, doc_id FROM #__lms_track_downloads WHERE user_id = '".$filter_stu."' AND doc_id IN ($doc_id_str)"
				. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND user_id IN ($members)") :'')
				."\n GROUP BY doc_id";
				$JLMS_DB->SetQuery( $query );
				$doc_downs_last = $JLMS_DB->LoadObjectList();
				$i = 0;
				while ($i < count($my_documents)) {
					$my_documents[$i]->downloads = 0;
					$my_documents[$i]->last_access = '';
					foreach ($doc_downs_count as $dd) {
						if ($dd->doc_id == $my_documents[$i]->id) {
							$my_documents[$i]->downloads = $dd->count_downs;
							break;
						}
					}
					foreach ($doc_downs_last as $ddl) {
						if ($ddl->doc_id == $my_documents[$i]->id) {
							$my_documents[$i]->last_access = $ddl->last_time;
							break;
						}
					}
					$i ++;
				}
			}
			$lists['page12_stats'] = & $my_documents;
		}
		if ($page == 12 && !$filter_stu) {
			//TODO: filter by group doesn't work here!
			$my_documents = GetTeacherDocuments($my->id, $id, 1);//in joomla_lms.main.php
			$my_documents = JLMS_GetTreeStructure( $my_documents );
			$my_documents = AppendFileIcons_toList( $my_documents );
			$doc_ids = array();
			foreach( $my_documents as $my_doc) {
				$doc_ids[] = $my_doc->id;
			}
			if (count($doc_ids)) {
				$doc_id_str = implode(',',$doc_ids);
				$query = "SELECT count(id) as count_downs, doc_id FROM #__lms_track_downloads WHERE doc_id IN ($doc_id_str) GROUP BY doc_id";
				$JLMS_DB->SetQuery( $query );
				$doc_downs_count = $JLMS_DB->LoadObjectList();
				$query = "SELECT max(track_time) as last_time, doc_id FROM #__lms_track_downloads WHERE doc_id IN ($doc_id_str) GROUP BY doc_id";
				$JLMS_DB->SetQuery( $query );
				$doc_downs_last = $JLMS_DB->LoadObjectList();
				$i = 0;
				while ($i < count($my_documents)) {
					$my_documents[$i]->downloads = 0;
					$my_documents[$i]->last_access = '';
					foreach ($doc_downs_count as $dd) {
						if ($dd->doc_id == $my_documents[$i]->id) {
							$my_documents[$i]->downloads = $dd->count_downs;
							break;
						}
					}
					foreach ($doc_downs_last as $ddl) {
						if ($ddl->doc_id == $my_documents[$i]->id) {
							$my_documents[$i]->last_access = $ddl->last_time;
							break;
						}
					}
					$i ++;
				}
			}
			$lists['page12_stats'] = & $my_documents;
		}
		if ($page == 11 && !$filter_stu) {
			$query = "SELECT * FROM #__lms_quiz_t_quiz WHERE course_id=$id ORDER BY c_title";
			$JLMS_DB->SetQuery( $query );
			$quiz_stat = $JLMS_DB->LoadObjectList();
			$lists['page11_stats'] = & $quiz_stat;	
		}
		if ($page == 11 && $filter_stu) {
			$query = "SELECT * FROM #__lms_quiz_t_quiz WHERE course_id=$id ORDER BY c_title";
			$JLMS_DB->SetQuery( $query );
			$quiz_stat = $JLMS_DB->LoadObjectList();
			$lists['page11_stats'] = & $quiz_stat;	
		}

		$lists['page'] = $page;
		
		$lists['filter_stu_val'] = 0;
		
		if($filter_stu){
			$lists['filter_stu_val'] = $filter_stu;
		} else 
		if($filter_lpath){
			$lists['filter_stu_val'] = $filter_lpath;
		} else {
			$lists['filter_stu_val'] = $filter_stu;
		}
		
		$lists['filter_lpath_val'] = $filter_lpath;
		$lists['filter_student_val'] = $filter_stu;
		
		//--------------------------
		/*for($i=0;$i<count($lists['page4_stats']);$i++) {
			if($lists['page4_stats'][$i]->lp_type == 2) {
				$query = "SELECT scorm_package FROM #__lms_n_scorm WHERE id = '".$lists['page4_stats'][$i]->item_id."'";
				$JLMS_DB->SetQuery( $query );
				$scorm_package = $JLMS_DB->LoadResult();

				$outer_doc = null;

				if ($scorm_package) {
					$query = "SELECT id FROM #__lms_n_scorm WHERE scorm_package = '".$scorm_package."' AND course_id = 0";
					$JLMS_DB->SetQuery( $query );
					$scorm_lib_id = $JLMS_DB->LoadResult();	
					
					$query = "SELECT outdoc_share, owner_id, allow_link FROM #__lms_outer_documents WHERE file_id = '".$scorm_lib_id."' AND folder_flag = 3";
					$JLMS_DB->SetQuery( $query );
					$outer_doc = $JLMS_DB->LoadObject();	
				}
				if(is_object($outer_doc) && isset($outer_doc->allow_link) && $outer_doc->allow_link == 1 ) {
					if($outer_doc->outdoc_share == 0 &&  $outer_doc->owner_id != $my->id) {
						if($is_teacher) {
							$lists['page4_stats'][$i]->lpath_name = _JLMS_LP_RESOURSE_ISUNAV;
							$lists['page4_stats'][$i]->is_link = 1;
						} else {						
							unset($lists['page4_stats'][$i]);
						}	
					}
					if($outer_doc->outdoc_share == 1 && !$is_teacher) {
						unset($lists['page4_stats'][$i]);
					}
				} elseif($is_teacher) {
					$lists['page4_stats'][$i]->lpath_name = _JLMS_LP_RESOURSE_ISUNAV;
					$lists['page4_stats'][$i]->is_link = 1;
				} else {
					unset($lists['page4_stats'][$i]);
				}
			}
		}
		
		$mas = array();
		foreach ($lists['page4_stats'] as $k=>$v) {
			$mas[] = $lists['page4_stats'][$k];	
		}
		unset($lists['page4_stats']);
		$lists['page4_stats'] = $mas;
		*/
		//---------------------------------
		
		//filer groups
		if ($JLMS_CONFIG->get('use_global_groups', 1)){
			$g_items = array();
			$g_items[] = mosHTML::makeOption(0, _JLMS_ATT_FILTER_ALL_GROUPS);
			
			$query = "SELECT distinct a.id as value, a.ug_name as text"
			. "\n FROM #__lms_usergroups as a"
			. "\n WHERE a.course_id = 0"
			. "\n AND a.parent_id = 0"
			. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND a.id IN ($groups_where_admin_manager)") :'')			
			. "\n ORDER BY a.ug_name"
			;
		
			$JLMS_DB->SetQuery( $query );
			$groups = $JLMS_DB->LoadObjectList();
			
			$g_items = array_merge($g_items, $groups);
			$link = "index.php?option=$option&amp;Itemid=$Itemid&task=tracking&amp;id=$id&amp;page=$page";
			$link .= $filter_lpath ? "&amp;filter_lpath=".$filter_lpath : '';
			$link .= $filter_stu ? "&amp;filter_stu=".$filter_stu : '';
			$link = $link ."&amp;filt_group=".JLMS_SELECTED_INDEX_MARKER;			
			$link = processSelectedIndexMarker( $link );
			$lists['filter2'] = mosHTML::selectList($g_items, 'filt_group', 'class="inputbox" style="width:250px;" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filt_group );
			
			$groups_reporting = array();
			foreach($g_items as $grp){
				if($filt_group && $grp->value == $filt_group){
					$groups_reporting[] = $grp->text;
				}
			}
			$name_groups_reporting[] = 'Usergroup';
			$reporting_header['name_groups'] = $name_groups_reporting;
			$reporting_header['groups'] = $groups_reporting;
			
			if($filt_group) {
				$g_items = array();
				$g_items[] = mosHTML::makeOption(0, _JLMS_FILTER_ALL_SUBGROUPS);
	
				$query = "SELECT distinct a.id as value, a.ug_name as text"
				. "\n FROM #__lms_usergroups as a"
				. "\n WHERE a.course_id = 0"
				. "\n AND a.parent_id = $filt_group"
				. ($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ? ("\n AND a.parent_id IN ($groups_where_admin_manager)") :'')				
				. "\n ORDER BY a.ug_name"
				;
				$JLMS_DB->SetQuery( $query );
				$sbugroups = $JLMS_DB->LoadObjectList();
				
				if(count($sbugroups)) {
					$g_items = array_merge($g_items, $sbugroups);
					$link = "index.php?option=$option&amp;Itemid=$Itemid&task=tracking&id=$id&amp;page=$page";
					$link .= $filter_lpath ? "&amp;filter_lpath=".$filter_lpath : '';
					$link = $link ."&amp;filt_group=".$filt_group."&amp;filt_subgroup=".JLMS_SELECTED_INDEX_MARKER;			
					$link = processSelectedIndexMarker( $link );
					$lists['filter3'] = mosHTML::selectList($g_items, 'filt_subgroup', 'class="inputbox" style="width:250px;" size="1" onchange="document.location.href=\''. $link .'\';"', 'value', 'text', $filt_subgroup );
				}
			}
		}
		
		/*Reports DATA prepare*/
		$name_courses_reporting[] = 'Course';
		$reporting_header['name_courses'] = $name_courses_reporting;
		$reporting_header['courses'] = JLMS_getCourseName($id);
		
		require_once(_JOOMLMS_FRONT_HOME . "/includes/libraries/lms.lib.reporting.php");
		
		$report_title = '';
		if(isset($lists['page13_stats']) && count($lists['page13_stats'])){
			$report_title = 'learning paths statistics';
		}
		if(isset($lists['page14_stats']) && count($lists['page14_stats'])){
			$report_title = 'latest course activities report';
		}
		
		if($view == 'xls'){
			if(isset($lists['page13_stats']) && count($lists['page13_stats'])){
				
				$data_info = array();
				$data_grade = array();
				
				$data_info[0][] = str_replace(':', '', _JLMS_LPATH_TBL_HEAD_NAME);
				
				$i=1;
				foreach($lists['page13_stats'] as $p13_stat){
					if($lists['f_lpath']){
						$data_info[$i][] = $p13_stat->username;
					} else {
						$data_info[$i][] = $p13_stat->lpath_name;
					}
					$i++;
				}
				
				if($JLMS_CONFIG->get('enable_timetracking', false)){
					$data_grade[0][] = str_replace(':', '', _JLMS_LPATH_TBL_TIME_SPENT);
				}
				$data_grade[0][] = str_replace(':', '', _JLMS_LPATH_TBL_STARTING);
				$data_grade[0][] = str_replace(':', '', _JLMS_LPATH_TBL_ENDING);
				
				$i=1;
				foreach($lists['page13_stats'] as $p13_stat){
					if($JLMS_CONFIG->get('enable_timetracking', false)){
						$data_grade[$i][] = $p13_stat->time_spent;
					}
					
					if(!$p13_stat->item_id){
						if (isset($p13_stat->r_status) && $p13_stat->r_status == 1) {
							$data_grade[$i][] = isset($p13_stat->r_start) ? JLMS_dateToDisplay($p13_stat->r_start, false, $JLMS_CONFIG->get('offset')*60*60, ' H:i:s') : '';
							$data_grade[$i][] = isset($p13_stat->r_end) ? JLMS_dateToDisplay($p13_stat->r_end, false, $JLMS_CONFIG->get('offset')*60*60, ' H:i:s') : '';
						} elseif (isset($p13_stat->r_status) && $p13_stat->r_status == 0) {
							$data_grade[$i][] = isset($p13_stat->r_start) ? JLMS_dateToDisplay($p13_stat->r_start, false, $JLMS_CONFIG->get('offset')*60*60, ' H:i:s') : '';
							$data_grade[$i][] = '';
						} else {
							$data_grade[$i][] = ''; 
							$data_grade[$i][] = '';
						}
					} else {
						if ($p13_stat->lp_type == 1 || $p13_stat->lp_type == 2) {
							$data_grade[$i][] = isset($p13_stat->r_start) ? JLMS_dateToDisplay($p13_stat->r_start, true, $JLMS_CONFIG->get('offset')*60*60, ' H:i:s') : '';
							$data_grade[$i][] = isset($p13_stat->r_end) ? JLMS_dateToDisplay($p13_stat->r_end, true, $JLMS_CONFIG->get('offset')*60*60, ' H:i:s') : '';
						}
					}
					
					$i++;
				}
				
				$results['data_info'] = $data_info;
				$results['data_grade'] = $data_grade;
				
				$data_tr = array();
				$k = 1;
				foreach($data_info as $n=>$d){
					$data_tr[] = $k;
					$k = 3 - $k;
				}
				$results['data_tr'] = $data_tr;
			}
			
			if(isset($lists['page14_stats']) && count($lists['page14_stats'])){
				
				$data_info = array();
				$data_grade = array();
				
				$data_info[0][] = str_replace(':', '', _JLMS_UI_USERNAME);
				$data_info[0][] = str_replace(':', '', _JLMS_UI_NAME);
				$data_info[0][] = str_replace(':', '', _JLMS_UI_EMAIL);
				
				$i=1;
				foreach($lists['page14_stats'] as $p14_stat){
					if(isset($p14_stat->username) && isset($p14_stat->user) && isset($p14_stat->email)){
						$data_info[$i][] = $p14_stat->username;
						$data_info[$i][] = $p14_stat->user;
						$data_info[$i][] = $p14_stat->email;
						$i++;
					}
				}
				
				$data_grade[0][] = _JLMS_TRACK_TBL_H_ACTIVITY;
				$data_grade[0][] = _JLMS_TRACK_TBL_H_TIME;
				
				$i=1;
				foreach($lists['page14_stats'] as $p14_stat){
					if(isset($p14_stat->username) && isset($p14_stat->user) && isset($p14_stat->email)){
						$data_grade[$i][] = $p14_stat->activity_rep;
						$data_grade[$i][] = JLMS_dateToDisplay($p14_stat->time, true, $JLMS_CONFIG->get('offset')*60*60, ' H:i:s');
						$i++;
					}
				}
				
				$results['data_info'] = $data_info;
				$results['data_grade'] = $data_grade;
				
				$data_tr = array();
				$k = 1;
				foreach($data_info as $n=>$d){
					$data_tr[] = $k;
					$k = 3 - $k;
				}
				$results['data_tr'] = $data_tr;
			}
		}
		if($view == 'csv'){
			if(isset($lists['page13_stats']) && count($lists['page13_stats'])){
				$data[0][] = str_replace(':', '', _JLMS_LPATH_TBL_HEAD_NAME);
				if($JLMS_CONFIG->get('enable_timetracking', false)){
					$data[0][] = str_replace(':', '', _JLMS_LPATH_TBL_TIME_SPENT);
				}
				$data[0][] = str_replace(':', '', _JLMS_LPATH_TBL_STARTING);
				$data[0][] = str_replace(':', '', _JLMS_LPATH_TBL_ENDING);
				
				$i=1;
				foreach($lists['page13_stats'] as $p13_stat){
					if($lists['f_lpath']){
						$data[$i][] = $p13_stat->username;
					} else {
						$data[$i][] = $p13_stat->lpath_name;
					}
					if($JLMS_CONFIG->get('enable_timetracking', false)){
						$data[$i][] = $p13_stat->time_spent;
					}
					
					if(!$p13_stat->item_id){
						if (isset($p13_stat->r_status) && $p13_stat->r_status == 1) {
							$data[$i][] = isset($p13_stat->r_start) ? JLMS_dateToDisplay($p13_stat->r_start, false, $JLMS_CONFIG->get('offset')*60*60, ' H:i:s') : '';
							$data[$i][] = isset($p13_stat->r_end) ? JLMS_dateToDisplay($p13_stat->r_end, false, $JLMS_CONFIG->get('offset')*60*60, ' H:i:s') : '';
						} elseif (isset($p13_stat->r_status) && $p13_stat->r_status == 0) {
							$data[$i][] = isset($p13_stat->r_start) ? JLMS_dateToDisplay($p13_stat->r_start, false, $JLMS_CONFIG->get('offset')*60*60, ' H:i:s') : '';
							$data[$i][] = '';
						}
					} else {
						if ($p13_stat->lp_type == 1 || $p13_stat->lp_type == 2) {
							$data[$i][] = isset($p13_stat->r_start) ? JLMS_dateToDisplay($p13_stat->r_start, true, $JLMS_CONFIG->get('offset')*60*60, ' H:i:s') : '';
							$data[$i][] = isset($p13_stat->r_end) ? JLMS_dateToDisplay($p13_stat->r_end, true, $JLMS_CONFIG->get('offset')*60*60, ' H:i:s') : '';
						}
					}
					
					$i++;
				}
			}
			if(isset($lists['page14_stats']) && count($lists['page14_stats'])){
				$data[0][] = str_replace(':', '', _JLMS_UI_USERNAME);
				$data[0][] = str_replace(':', '', _JLMS_UI_NAME);
				$data[0][] = str_replace(':', '', _JLMS_UI_EMAIL);
				$data[0][] = _JLMS_TRACK_TBL_H_ACTIVITY;
				$data[0][] = _JLMS_TRACK_TBL_H_TIME;
				
				$i=1;
				foreach($lists['page14_stats'] as $p14_stat){
					if(isset($p14_stat->username) && isset($p14_stat->user) && isset($p14_stat->email)){
						$data[$i][] = $p14_stat->username;
						$data[$i][] = $p14_stat->user;
						$data[$i][] = $p14_stat->email;
						$data[$i][] = $p14_stat->activity_rep;
						$data[$i][] = JLMS_dateToDisplay($p14_stat->time, true, $JLMS_CONFIG->get('offset')*60*60, ' H:i:s');
						$i++;
					}
				}
			}	
		}
		/*Reports DATA prepare*/
			
		$lists['view'] = $view;
		if($view == 'csv'){
			JLMS_reporting::outputCSV($data, $report_title);
		} else 
		if($view == 'xls'){
			JLMS_reporting::exportXLS($results, $reporting_header, 'gradebook_report', $report_title);
		} else {
			JLMS_tracking_html::showpageTracking( $id, $option, $rows, $lists );
		}
	} else {
		//JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid"));
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$id"));
	}
}
function JLMS_showTR_clearPage( $id, $option ) {
	global $my, $JLMS_DB, $Itemid;
	$JLMS_ACL = JLMSFactory::getACL();
	if ($id && $JLMS_ACL->CheckPermissions('tracking', 'clear_stats') ) {
		$lists = array();
		JLMS_tracking_html::showTR_clear( $id, $option, $lists );
	} else {
		JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=tracking&id=$id"));
	}
}
function JLMS_clearTracking( $id, $option ) {
	$db = & JLMSFactory::getDB();
	$user = JLMSFactory::getUser();
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$JLMS_ACL = JLMSFactory::getACL();
	$Itemid = $JLMS_CONFIG->get('Itemid');

	if ($id && $JLMS_ACL->CheckPermissions('tracking', 'clear_stats') ) {

		/* time offsets */
		$offset_hours = 0;
		$offset_minutes = 0;
		$total_offset = $JLMS_CONFIG->get('offset');
		$track_time = 'track_time';
		$track_time_lp = 'start_time';
		if ($total_offset) {
			$offset_hours = intval(($total_offset*60)/60);
			$offset_minutes = $total_offset*60 - $offset_hours*60;
			$track_time = "ADDTIME(track_time, '".$offset_hours.":".$offset_minutes.":00.000000')";
			$track_time_lp = "ADDTIME(start_time, '".$offset_hours.":".$offset_minutes.":00.000000')";
		}
		/* time offsets */

		$tr_clear_type = intval(mosGetParam($_REQUEST, 'tr_clear_type', 1));
		$query_add = '';
		$query_add_lp = '';
		if ($tr_clear_type == 1) {
			$query_add = '';
			$query_add_lp = '';
		} elseif ($tr_clear_type == 2) {
			$start_date = JLMS_dateToDB(mosGetParam($_REQUEST,'start_date',date('Y-m-d')));
			$end_date = JLMS_dateToDB(mosGetParam($_REQUEST,'end_date',date('Y-m-d')));
			$start_date .= ' 00:00:00';
			$end_date .= ' 23:59:59';
			$query_add = " AND $track_time >= '".$start_date."' AND $track_time <= '".$end_date."'";
			$query_add_lp = " AND $track_time_lp >= '".$start_date."' AND $track_time_lp <= '".$end_date."'";
		}

		if($JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only')) {
			$groups_where_admin_manager = JLMS_ACL_HELPER::GetAssignedGroups($user->get('id'), $id);

			if(count($groups_where_admin_manager)) {
				if(JLMS_ACL_HELPER::GetCountAssignedGroups($user->get('id'), $id) == 1) {	
					$filt_group = $groups_where_admin_manager[0];
				}
			}
			$members = '0';
			if(count($groups_where_admin_manager)) {
				$groups_where_admin_manager = implode(',', $groups_where_admin_manager);
				if($groups_where_admin_manager != '') {
					$query = "SELECT distinct user_id FROM #__lms_users_in_global_groups WHERE (group_id IN ($groups_where_admin_manager) OR subgroup1_id IN ($groups_where_admin_manager))";
					$db->setQuery($query);
					$members_array = JLMSDatabaseHelper::loadResultArray();
					if (count($members_array)) {
						$members = implode(',', $members_array);
					}
				}
			}
			$query_add .= " AND user_id IN ($members)";
			$query_add_lp .= " AND user_id IN ($members)";
		}

		$query = "DELETE FROM #__lms_track_chat WHERE course_id = '".$id."'".$query_add;
		$db->SetQuery( $query );
		$db->query();

		$query = "DELETE FROM #__lms_track_hits WHERE course_id = '".$id."'".$query_add;
		$db->SetQuery( $query );
		$db->query();

		$query = "SELECT id FROM #__lms_documents WHERE course_id = '".$id."'";
		$db->SetQuery( $query );
		$doc_ids = JLMSDatabaseHelper::LoadResultArray();
		if (count($doc_ids)) {
			$d_str = implode(',',$doc_ids);
			$query = "DELETE FROM #__lms_track_downloads WHERE doc_id IN ($d_str)".$query_add;
			$db->SetQuery( $query );
			$db->query();
		}
		$query = "DELETE FROM #__lms_track_learnpath_stats WHERE course_id = '".$id."'".$query_add_lp;
		$db->SetQuery( $query );
		$db->query();
	}
	JLMSRedirect(sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=tracking&id=$id"));
}

function JLMS_tracking_get_latest_activities($course_id, $lstart = 0, $slimit = 10, $members = '0', $filt_group = 0, $filt_stu = 0) {
	$db = JFactory::getDbo();
	$user = JLMSFactory::getUser();
	$my_id = $user->get('id');
	$db = JFactory::getDbo();
	$JLMS_ACL = JLMSFactory::getACL();
	$JLMS_CONFIG = JLMSFactory::getConfig();

	$filt_group = $filt_group ? ($JLMS_CONFIG->get('use_global_groups', 1) ? $filt_group : 0) : 0;

	$latest_activities = array();

	$where_user = '';
	$limit = $slimit * 3;//we need more items in case if there are a lot of duplicate activities

	$trhits = array();
	$trchat = array();
	$trdownloads = array();
	$trlearnpaths = array();

	if ( $JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only') ) {
		$assigned_groups = $JLMS_ACL->getMyGroupsAssigned();
		if (count($assigned_groups)) {
			$query = "SELECT a.*, u.name, u.username, u.email"
			. "\n FROM #__lms_track_hits as a, #__users as u"
			. ($filt_group ? ", #__lms_users_in_global_groups as gg" : '')
			. "\n WHERE a.user_id IN ($members)"
			. ($filt_group ? "\n AND gg.user_id = a.user_id AND gg.group_id = $filt_group" : '')
			. ($filt_stu ? "\n AND a.user_id = $filt_stu" : '')
			. "\n AND a.user_id <> $my_id AND  a.user_id = u.id AND a.course_id = $course_id"
			. "\n ORDER BY a.track_time DESC LIMIT $lstart, $limit";
			$db->SetQuery($query);
			$trhits = $db->LoadObjectList();
			$query = "SELECT a.*, u.name, u.username, u.email"
			. "\n FROM #__lms_chat_history as a, #__users as u"
			. ($filt_group ? ", #__lms_users_in_global_groups as gg" : '')
			. "\n WHERE a.user_id IN ($members)"
			. ($filt_group ? "\n AND gg.user_id = a.user_id AND gg.group_id = $filt_group" : '')
			. ($filt_stu ? "\n AND a.user_id = $filt_stu" : '')
			. "\n AND a.user_id <> $my_id AND a.user_id = u.id AND a.course_id = $course_id"
			. "\n ORDER BY a.id DESC LIMIT $lstart, $limit";
			$db->SetQuery($query);
			$trchat = $db->LoadObjectList();
			$query = "SELECT a.*, b.doc_name, u.name, u.username, u.email"
			. "\n, f.file_name, b.file_id, b.folder_flag"
			. "\n FROM #__lms_track_downloads as a, #__users as u, #__lms_documents as b LEFT JOIN #__lms_files as f ON b.file_id = f.id"
			. ($filt_group ? ", #__lms_users_in_global_groups as gg" : '')
			. "\n WHERE a.user_id IN ($members)"
			. ($filt_group ? "\n AND gg.user_id = a.user_id AND gg.group_id = $filt_group" : '')
			. ($filt_stu ? "\n AND a.user_id = $filt_stu" : '')
			. "\n AND a.user_id <> $my_id AND a.doc_id = b.id AND b.course_id = $course_id AND a.user_id = u.id"
			. "\n ORDER BY a.id DESC LIMIT $lstart, $limit";
			$db->SetQuery($query);
			$trdownloads = $db->LoadObjectList();
			$query = "SELECT a.*, b.lpath_name, u.name, u.username, u.email"
			. "\n FROM #__lms_track_learnpath_stats as a, #__lms_learn_paths as b, #__users as u"
			. ($filt_group ? ", #__lms_users_in_global_groups as gg" : '')
			. "\n WHERE a.user_id IN ($members)"
			. ($filt_group ? "\n AND gg.user_id = a.user_id AND gg.group_id = $filt_group" : '')
			. ($filt_stu ? "\n AND a.user_id = $filt_stu" : '')
			. "\n AND a.user_id <> $my_id AND a.course_id = $course_id AND a.lpath_id = b.id AND a.user_id = u.id"
			. "\n ORDER BY a.id DESC LIMIT $lstart, $limit";
			$db->SetQuery($query);
			$trlearnpaths = $db->LoadObjectList();
			$query = "SELECT a.*, a.c_date_time as track_time, a.c_student_id as user_id, b.c_title as quiz_name, u.name, u.username, u.email"
			. "\n FROM #__lms_quiz_r_student_quiz as a, #__lms_quiz_t_quiz as b, #__users as u"
			. ($filt_group ? ", #__lms_users_in_global_groups as gg" : '')
			. "\n WHERE a.c_student_id IN ($members)"
			. ($filt_group ? "\n AND gg.user_id = a.c_student_id AND gg.group_id = $filt_group" : '')
			. ($filt_stu ? "\n AND a.c_student_id = $filt_stu" : '')
			. "\n AND a.c_student_id <> $my_id AND b.course_id = $course_id AND a.c_quiz_id = b.c_id AND a.c_student_id = u.id"
			. "\n ORDER BY a.c_id DESC LIMIT $lstart, $limit";
			$db->SetQuery($query);
			$trquizzes = $db->LoadObjectList();
		}
	} else {
		$query = "SELECT a.*, u.name, u.username, u.email"
		. "\n FROM #__lms_track_hits as a, #__users as u"
		. ($filt_group ? ", #__lms_users_in_global_groups as gg" : '')
		. "\n WHERE a.user_id <> $my_id"
		. ($filt_group ? "\n AND gg.user_id = a.user_id AND gg.group_id = $filt_group" : '')
		. ($filt_stu ? "\n AND a.user_id = $filt_stu" : '')
		. "\n AND a.user_id = u.id AND a.course_id = $course_id"
		. "\n ORDER BY a.track_time DESC LIMIT $lstart, $limit";
		$db->SetQuery($query);
		$trhits = $db->LoadObjectList();
		$query = "SELECT a.*, u.name, u.username, u.email"
		. "\n FROM #__lms_chat_history as a, #__users as u"
		. ($filt_group ? ", #__lms_users_in_global_groups as gg" : '')
		. "\n WHERE a.user_id <> $my_id"
		. ($filt_group ? "\n AND gg.user_id = a.user_id AND gg.group_id = $filt_group" : '')
		. ($filt_stu ? " AND a.user_id = $filt_stu" : '')
		. "\n AND a.user_id = u.id AND a.course_id = $course_id"
		. "\n ORDER BY a.id DESC LIMIT $lstart, $limit";
		$db->SetQuery($query);
		$trchat = $db->LoadObjectList();
		$query = "SELECT a.*, b.doc_name, u.name, u.username, u.email, f.file_name, b.file_id, b.folder_flag"
		. "\n FROM #__lms_track_downloads as a, #__users as u, #__lms_documents as b LEFT JOIN #__lms_files as f ON b.file_id = f.id"
		. ($filt_group ? ", #__lms_users_in_global_groups as gg" : '')
		. "\n WHERE a.user_id <> $my_id"
		. ($filt_group ? "\n AND gg.user_id = a.user_id AND gg.group_id = $filt_group" : '')
		. ($filt_stu ? " AND a.user_id = $filt_stu" : '')
		. "\n AND a.doc_id = b.id AND b.course_id = $course_id AND a.user_id = u.id"
		. "\n ORDER BY a.id DESC LIMIT $lstart, $limit";
		$db->SetQuery($query);
		$trdownloads = $db->LoadObjectList();
		$query = "SELECT a.*, b.lpath_name, u.name, u.username, u.email"
		. "\n FROM #__lms_track_learnpath_stats as a, #__lms_learn_paths as b, #__users as u"
		. ($filt_group ? ", #__lms_users_in_global_groups as gg" : '')
		. "\n WHERE a.user_id <> $my_id"
		. ($filt_group ? "\n AND gg.user_id = a.user_id AND gg.group_id = $filt_group" : '')
		. ($filt_stu ? "\n AND a.user_id = $filt_stu" : '')
		. "\n AND a.course_id = $course_id AND a.lpath_id = b.id AND a.user_id = u.id"
		. "\n ORDER BY a.id DESC LIMIT $lstart, $limit";
		$db->SetQuery($query);
		$trlearnpaths = $db->LoadObjectList();
		$query = "SELECT a.*, a.c_date_time as track_time, a.c_student_id as user_id, b.c_title as quiz_name, u.name, u.username, u.email"
		. "\n FROM #__lms_quiz_r_student_quiz as a, #__lms_quiz_t_quiz as b, #__users as u"
		. ($filt_group ? ", #__lms_users_in_global_groups as gg" : '')
		. "\n WHERE a.c_student_id <> $my_id"
		. ($filt_group ? "\n AND gg.user_id = a.c_student_id AND gg.group_id = $filt_group" : '')
		. ($filt_stu ? "\n AND a.c_student_id = $filt_stu" : '')
		. "\n AND b.course_id = $course_id AND a.c_quiz_id = b.c_id AND a.c_student_id = u.id"
		. "\n ORDER BY a.c_id DESC LIMIT $lstart, $limit";
		$db->SetQuery($query);
		$trquizzes = $db->LoadObjectList();
	}
	AppendFileIcons_toList($trdownloads);

	foreach ($trchat as $trchat_activity) {
		$new_activity = new stdClass();
		$new_activity->time = strtotime($trchat_activity->mes_time);
		$new_activity->user = $trchat_activity->name;
		$new_activity->username = $trchat_activity->username;
		$new_activity->email = $trchat_activity->email;
		$new_activity->user_id = $trchat_activity->user_id;
		$new_activity->activity = _JLMS_TRACK_ACT_CHAT_MESSAGE;
		$new_activity->activity_rep = _JLMS_TRACK_ACT_CHAT_MESSAGE;
		$new_activity->page = 8;
		$new_activity->is_primary = 1;
		$new_activity->icon = 'toolbar/tlb_chat.png';
		$latest_activities[] = $new_activity;
	}

	foreach ($trdownloads as $trd_activity) {
		$new_activity = new stdClass();
		$new_activity->time = strtotime($trd_activity->track_time);
		$new_activity->user = $trd_activity->name;
		$new_activity->username = $trd_activity->username;
		$new_activity->email = $trd_activity->email;
		$new_activity->user_id = $trd_activity->user_id;
		$new_activity->activity = _JLMS_TRACK_ACT_VIEWS." '".$trd_activity->doc_name."'";
		$new_activity->activity_rep = $trd_activity->doc_name;
		$new_activity->page = 1;
		$new_activity->icon = 'files/'.$trd_activity->file_icon.'.png';
		$new_activity->is_primary = 1;
		$latest_activities[] = $new_activity;
	}

	foreach ($trlearnpaths as $trl_activity) {
		$new_activity = new stdClass();
		$new_activity->time = strtotime($trl_activity->start_time);
		$new_activity->user = $trl_activity->name;
		$new_activity->username = $trl_activity->username;
		$new_activity->email = $trl_activity->email;
		$new_activity->user_id = $trl_activity->user_id;
		$new_activity->activity = _JLMS_TRACK_ACT_TAKES." '".$trl_activity->lpath_name."'";
		$new_activity->activity_rep = $trl_activity->lpath_name;
		$new_activity->page = 4;
		$new_activity->icon = 'toolbar/tlb_lpath.png';
		$new_activity->is_primary = 1;
		$latest_activities[] = $new_activity;
	}

	foreach ($trquizzes as $trq_activity) {
		$new_activity = new stdClass();
		$new_activity->time = strtotime($trq_activity->track_time);
		$new_activity->user = $trq_activity->name;
		$new_activity->username = $trq_activity->username;
		$new_activity->email = $trq_activity->email;
		$new_activity->user_id = $trq_activity->user_id;
		$new_activity->activity = _JLMS_TRACK_ACT_TAKES." '".$trq_activity->quiz_name."'";
		$new_activity->activity_rep = $trq_activity->quiz_name;
		$new_activity->page = 11;
		$new_activity->icon = 'toolbar/tlb_quiz.png';
		$new_activity->is_primary = 1;
		$latest_activities[] = $new_activity;
	}

	foreach ($trhits as $trhit) {
		$new_activity = new stdClass();
		$new_activity->time = strtotime($trhit->track_time);
		$new_activity->user = $trhit->name;
		$new_activity->username = $trhit->username;
		$new_activity->email = $trhit->email;
		$new_activity->user_id = $trhit->user_id;
		$new_activity->activity = '';
		$new_activity->activity_rep = '';
		$new_activity->is_primary = 0;
		switch ($trhit->page_id) {
			case 0:
				$new_activity->activity = _JLMS_TRACK_ACT_VIEWS." '"._JLMS_COURSES_DETAILS."'";
				$new_activity->activity_rep = _JLMS_COURSES_DETAILS;
				$new_activity->page = 0;
				$new_activity->icon = 'toolbar/tlb_courses.png';
			break;
			case 1:
				$new_activity->activity = _JLMS_TRACK_ACT_VIEWS." '"._JLMS_TRACK_TITLE_1."'";
				$new_activity->activity_rep = _JLMS_TRACK_TITLE_1;
				$new_activity->page = 1;
				$new_activity->icon = 'toolbar/tlb_docs.png';
			break;
			case 2:
				$new_activity->activity = _JLMS_TRACK_ACT_VIEWS." '"._JLMS_TRACK_TITLE_2."'";
				$new_activity->activity_rep = _JLMS_TRACK_TITLE_2;
				$new_activity->page = 2;
				$new_activity->icon = 'toolbar/tlb_links.png';
			break;
			case 3:
				$new_activity->activity = _JLMS_TRACK_ACT_VIEWS." '"._JLMS_TRACK_TITLE_3."'";
				$new_activity->activity_rep = _JLMS_TRACK_TITLE_3;
				$new_activity->page = 3;
				$new_activity->icon = 'toolbar/tlb_dropbox.png';
			break;
			case 4:
				$new_activity->activity = _JLMS_TRACK_ACT_VIEWS." '"._JLMS_TRACK_TITLE_4."'";
				$new_activity->activity_rep = _JLMS_TRACK_TITLE_4;
				$new_activity->page = 4;
				$new_activity->icon = 'toolbar/tlb_lpath.png';
			break;
			case 5:
				$new_activity->activity = _JLMS_TRACK_ACT_VIEWS." '"._JLMS_TRACK_TITLE_5."'";
				$new_activity->activity_rep = _JLMS_TRACK_TITLE_5;
				$new_activity->page = 5;
				$new_activity->icon = 'toolbar/tlb_homework.png';
			break;
			case 6:
				$new_activity->activity = _JLMS_TRACK_ACT_VIEWS." '"._JLMS_TRACK_TITLE_6."'";
				$new_activity->activity_rep = _JLMS_TRACK_TITLE_6;
				$new_activity->page = 6;
				$new_activity->icon = 'toolbar/tlb_agenda.png';
			break;
			case 7:
				$new_activity->activity = _JLMS_TRACK_ACT_VIEWS." '"._JLMS_TRACK_TITLE_7."'";
				$new_activity->activity_rep = _JLMS_TRACK_TITLE_7;
				$new_activity->page = 7;
				$new_activity->icon = 'toolbar/btn_cam.png';
			break;
			case 8:
				$new_activity->activity = _JLMS_TRACK_ACT_VIEWS." '"._JLMS_TRACK_TITLE_8."'";
				$new_activity->activity_rep = _JLMS_TRACK_TITLE_8;
				$new_activity->page = 8;
				$new_activity->icon = 'toolbar/tlb_chat.png';
			break;
			case 10:
				$new_activity->activity = _JLMS_TRACK_ACT_VIEWS." '"._JLMS_TRACK_TITLE_10."'";
				$new_activity->activity_rep = _JLMS_TRACK_TITLE_10;
				$new_activity->page = 10;
				$new_activity->icon = 'toolbar/tlb_forum.png';
			break;
			case 11:
				$new_activity->activity = _JLMS_TRACK_ACT_VIEWS." '"._JLMS_TRACK_TITLE_11."'";
				$new_activity->activity_rep = _JLMS_TRACK_TITLE_11;
				$new_activity->page = 11;
				$new_activity->icon = 'toolbar/tlb_quiz.png';
			break;
		}
		if ($new_activity->activity) {
			//check previous activity
			/*$no_duplicates = true;
			if (isset($latest_activities[count($latest_activities) - 1])) {
				if ($new_activity->time == $latest_activities[count($latest_activities) - 1]->time) {
					if ($new_activity->user == $latest_activities[count($latest_activities) - 1]->user) {
						if ($new_activity->activity == $latest_activities[count($latest_activities) - 1]->activity) {
							$no_duplicates = false;
						}
					}
				}
			}
			if ($no_duplicates) {*/
				$latest_activities[] = $new_activity;
			/*}*/
		}
	}
//var_dump($latest_activities);die;
	//sort $latest_activities by user_id - to remove duplicate activities for the same user
	$user_ids = array();
	for ($i = 0, $n = count($latest_activities); $i < ($n - 1); $i ++) {
		if (!in_array($latest_activities[$i]->user_id, $user_ids)) {
			$user_ids[] = $latest_activities[$i]->user_id;
		}
		for ($j = $i+1, $m = count($latest_activities); $j < $m; $j ++) {
			//check if $j element is later then $i; if not - replace them
			$replace = false;
			/*if ($latest_activities[$j]->time < $latest_activities[$i]->time) {
				//ok
			} elseif ($latest_activities[$j]->time == $latest_activities[$i]->time) {
				//check users
				if ($latest_activities[$j]->user_id < $latest_activities[$i]->user_id) {
					$replace = true;
				}
			} else {
				//replace them
				$replace = true;
			}*/
			if ($latest_activities[$j]->user_id < $latest_activities[$i]->user_id) {
				$replace = true;
			} elseif ($latest_activities[$j]->user_id == $latest_activities[$i]->user_id) {
				if ($latest_activities[$j]->time > $latest_activities[$i]->time) {
					$replace = true;
				} elseif ($latest_activities[$j]->time == $latest_activities[$i]->time) {
					if ($latest_activities[$j]->is_primary > $latest_activities[$i]->is_primary) {
						$replace = true;
					}
				}
			}
			
			if ($replace) {
				$k = new stdClass(); 
				$k->time = $latest_activities[$j]->time; 
				$k->user = $latest_activities[$j]->user;
				$k->username = $latest_activities[$j]->username;
				$k->email = $latest_activities[$j]->email;
				$k->user_id = $latest_activities[$j]->user_id; 
				$k->activity = $latest_activities[$j]->activity;
				$k->activity_rep = $latest_activities[$j]->activity_rep;
			 	$k->page = $latest_activities[$j]->page; 
			 	$k->is_primary = $latest_activities[$j]->is_primary;
			 	$k->icon = $latest_activities[$j]->icon;
				$latest_activities[$j] = $latest_activities[$i];
				$latest_activities[$i] = $k;
			}
		}
	}

	//rearrange array of activities and remove all duplicates except of 'primary' activities, e.g. remove 'view documents' activity if user twice clicked documents page
	$latest_activities_reloaded = array();
	$last_user = 0;
	$last_tool = 0;
	$last_time = 0;
	$last_primary = 0;
	foreach($latest_activities as $latest_activity) {
		$force_primary = false;
		if ($latest_activity->user_id == $last_user && $latest_activity->page == $last_tool && !$latest_activity->is_primary) {
			//skip
			if ($last_time == $latest_activity->time) {
				$force_primary = true;
			} elseif ($last_primary) {
				$latest_activities_reloaded[] = $latest_activity;
			}
		} else {
			$latest_activities_reloaded[] = $latest_activity;
		}
		$last_user = $latest_activity->user_id;
		$last_tool = $latest_activity->page;
		$last_primary = $force_primary ? 1 : $latest_activity->is_primary;
		$last_time = $latest_activity->time;
	}

	//sort $latest_activities_reloaded by time
	for ($i = 0, $n = count($latest_activities_reloaded); $i < ($n - 1); $i ++) {
		for ($j = $i+1, $m = count($latest_activities_reloaded); $j < $m; $j ++) {
			//check if $j element is later then $i; if not - replace them
			$replace = false;
			if ($latest_activities_reloaded[$j]->time < $latest_activities_reloaded[$i]->time) {
				//ok
			} elseif ($latest_activities_reloaded[$j]->time == $latest_activities_reloaded[$i]->time) {
				//check users
				if ($latest_activities_reloaded[$j]->user_id < $latest_activities_reloaded[$i]->user_id) {
					$replace = true;
				}
			} else {
				//replace them
				$replace = true;
			}
			if ($replace) {
				$k = new stdClass(); 
				$k->time = $latest_activities_reloaded[$j]->time; 
				$k->user = $latest_activities_reloaded[$j]->user;
				$k->username = $latest_activities_reloaded[$j]->username;
				$k->email = $latest_activities_reloaded[$j]->email;
				$k->user_id = $latest_activities_reloaded[$j]->user_id; 
				$k->activity = $latest_activities_reloaded[$j]->activity;
				$k->activity_rep = $latest_activities_reloaded[$j]->activity_rep;
				$k->page = $latest_activities_reloaded[$j]->page; 
				$k->is_primary = $latest_activities_reloaded[$j]->is_primary;
				$k->icon = $latest_activities_reloaded[$j]->icon;
				$latest_activities_reloaded[$j] = $latest_activities_reloaded[$i];
				$latest_activities_reloaded[$i] = $k;
			}
		}
	}

	$ret_activities = array();
	for ($i = $lstart; $i < $slimit; $i ++) {
		if (isset($latest_activities_reloaded[$i])) {
			$ret_activities[] = $latest_activities_reloaded[$i];
		}
	}
	return $ret_activities;
}

function JLMS_updateSpentTime($course_id, $lpath_id, $step_id, $user_id){
	
	$JLMS_DB = & JLMSFactory::getDB();
	
	//$query = "SELECT cond_time, cond_time_sec"
	$query = "SELECT cond_time"
	. "\n FROM #__lms_learn_path_conds"
	. "\n WHERE 1"
	. "\n AND course_id = '".$course_id."'"
	. "\n AND lpath_id = '".$lpath_id."'"
	. "\n AND step_id = '".$step_id."'"
	. "\n AND ref_step = 0"
	;
	$JLMS_DB->setQuery($query);
	$cond_times = $JLMS_DB->loadObject();
	
	//$cond_time = $cond_times->cond_time * 60 + $cond_times->cond_time_sec + 1;
	$cond_time = $cond_times->cond_time + 1;
	
	$query = "SELECT time_spent"
	. "\n FROM #__lms_time_tracking_resources"
	. "\n WHERE 1"
	. "\n AND course_id = '".$course_id."'"
	. "\n AND user_id = '".$user_id."'"
	. "\n AND resource_type = '9'"
	. "\n AND resource_id = '".$lpath_id."'"
	. "\n AND item_id = '".$step_id."'"
	;
	$JLMS_DB->setQuery($query);
	$time_spent = $JLMS_DB->loadResult();
	
	if($time_spent < $cond_time){
		$query = "UPDATE #__lms_time_tracking_resources"
		. "\n SET time_spent = '".$cond_time."'"
		. "\n WHERE 1"
		. "\n AND course_id = '".$course_id."'"
		. "\n AND user_id = '".$user_id."'"
		. "\n AND resource_type = '9'"
		. "\n AND resource_id = '".$lpath_id."'"
		. "\n AND item_id = '".$step_id."'"
		;
		$JLMS_DB->setQuery($query);
		$JLMS_DB->query();
	}
	
}

function JLMS_writeSpentTime($course_id, $lpath_id, $step_id, $user_id){
	
	$JLMS_DB = & JLMSFactory::getDB();
	
	//$query = "SELECT cond_time, cond_time_sec"
	$query = "SELECT cond_time"
	. "\n FROM #__lms_learn_path_conds"
	. "\n WHERE 1"
	. "\n AND course_id = '".$course_id."'"
	. "\n AND lpath_id = '".$lpath_id."'"
	. "\n AND step_id = '".$step_id."'"
	. "\n AND ref_step = 0"
	;
	$JLMS_DB->setQuery($query);
	$cond_times = $JLMS_DB->loadObject();
	
	//$cond_time = $cond_times->cond_time * 60 + $cond_times->cond_time_sec + 1;
	$cond_time = $cond_times->cond_time + 1;
	
	$query = "INSERT INTO #__lms_time_tracking_resources"
	. "\n (id, course_id, user_id, resource_type, resource_id, item_id, time_spent)"
	. "\n VALUES"
	. "\n ('', ".$course_id.", ".$user_id.", 9, ".$lpath_id.", ".$step_id.", ".$cond_time.")"
	;
	$JLMS_DB->setQuery($query);
	$JLMS_DB->query();
	
}

function JLMS_deleteSpentTime($course_id, $user_id, $resource_id, $item_ids){
	
	$JLMS_DB = & JLMSFactory::getDB();
	
	if(isset($item_ids) && count($item_ids)){
		$query = "DELETE FROM #__lms_time_tracking_resources"
		. "\n WHERE 1"
		. "\n AND course_id = '".$course_id."'"
		. "\n AND user_id = '".$user_id."'"
		. "\n AND resource_type = '9'"
		. "\n AND resource_id = '".$resource_id."'"
		. "\n AND item_id IN (".implode(',', $item_ids).")"
		;
		$JLMS_DB->setQuery($query);
		$JLMS_DB->query();
	}
	
}

?>