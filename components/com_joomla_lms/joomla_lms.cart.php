<?php
/**
* joomla_lms.cart.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

require_once(_JOOMLMS_FRONT_HOME . DS . "joomla_lms.subscriptions.html.php"); //TODO: (low) remove this file?
require_once(_JOOMLMS_FRONT_HOME . DS . "joomla_lms.cart.html.php");
require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "libraries" . DS . "lms.lib.user_sessions.php");
require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "libraries" . DS . "lms.lib.recurrent.pay.php");
require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "joomla_lms.subscription.lib.php");


$task 	= mosGetParam( $_REQUEST, 'task', '' );
if ($task != 'callback') {
	global $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id',0);
	$pathway = array();
	$pathway[] = array('name' => _JLMS_PATHWAY_HOME, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"), 'is_home' => true);
	$pathway[] = array('name' => _JLMS_HEAD_SUBSCRIPTION_STR, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=subscription"));
	if ($task == 'show_cart' || $task == 'apply_coupon_code') {
		$pathway[] = array('name' => _JLMS_MY_CART, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=subscription"));
	}
	JLMSAppendPathWay($pathway);
}
if ($task == 'subscription' || $task == 'show_cart' || $task == 'apply_coupon_code' ){
	JLMS_ShowHeading();
	$doc = JFactory::getDocument();
	$doc->setTitle($doc->getTitle().' - '._JLMS_HEAD_SUBSCRIPTION_STR);
}
$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) );
$cid = josGetArrayInts('cid', $_POST);
if (!is_array( $cid )) { $cid = array(0); }						
	
switch ($task) {
	//susbcription for one course
	case 'subscription':
		if (!empty($cid) && isset($cid[0]) && $cid[0]){
			$id = $cid[0];
		}
		JLMS_CART_listSubs($id, $option);
	break;

	case 'add_to_cart':
		JLMS_CART_addItem($option);
	break;

	case 'remove_from_cart':
		JLMS_CART_removeItem($option);
	break;

	case 'update_cart':
		JLMS_CART_updateItems($option);
	break;

	case 'checkout_cart':
		JLMS_CART_checkoutItems($option);
	break;

	case 'apply_coupon_code':
	case 'show_cart':
		JLMS_CART_showItems($option);
	break;

	case 'cart_login':
		JLMS_CART_login($option);
	break;

	case 'cart_register':
		JLMS_CART_register($option);
	break;

	case 'cart_register_cb':
		JLMS_CART_register($option, 1);
	break;

	case 'callback':
		JLMS_CART_callback($option);
	break;

	case 'course_subscribe':
		JLMS_CART_course_subscribe( $id, $option );
	break;

	case 'get_payment_invoice':
		JLMS_CART_get_invoice( $id, $option );
	break;
}
function JLMS_CART_get_invoice($id, $option) {
	global $JLMS_CONFIG;
	$Itemid = $JLMS_CONFIG->get('Itemid');
	$db = JFactory::getDbo();
	$user = JLMSFactory::getUser();

	if ($id && $user->get('id')) {
		$query = "SELECT b.filename FROM #__lms_payments as a, #__lms_subs_invoice as b WHERE a.id = $id AND a.user_id = ".$user->get('id')." AND a.id = b.subid AND a.payment_type = 2";
		$db->setQuery( $query );
		$invoice_file = $db->LoadResult();
	}
	$invoice_file_path = '';
	if ($invoice_file) {
		$invoice_file_path = $JLMS_CONFIG->get('jlms_subscr_invoice_path') . DS . $invoice_file;
	}
	if ($invoice_file_path && file_exists($invoice_file_path) && is_readable( $invoice_file_path ) ) {
		$file_name = 'invoice_'.(str_pad($id, 5, '0', STR_PAD_LEFT)).'.pdf';

		@set_time_limit(0);

		$v_date = date("Y-m-d H:i:s");
		if (preg_match('/Opera(\/| )([0-9].[0-9]{1,2})/', $_SERVER['HTTP_USER_AGENT'])) {
			$UserBrowser = "Opera";
		}
		elseif (preg_match('/MSIE ([0-9].[0-9]{1,2})/', $_SERVER['HTTP_USER_AGENT'])) {
			$UserBrowser = "IE";
		} else {
			$UserBrowser = '';
		}
		$mime_type = ($UserBrowser == 'IE' || $UserBrowser == 'Opera') ? 'application/octetstream' : 'application/octet-stream';
		header('Content-Type: ' . $mime_type );
		header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		if ($UserBrowser == 'IE') {
			header('Content-Disposition: attachment; filename="' . $file_name . '";');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Content-Length: '. filesize($invoice_file_path)); 
			header('Pragma: public');
		} else {
			header('Content-Disposition: attachment; filename="' . $file_name . '";');
			header('Content-Length: '. filesize($invoice_file_path)); 
			header('Pragma: no-cache');
		}
		@ob_end_clean();
		readfile( $invoice_file_path );
		exit();
	}
	JLMSRedirect(JLMSRoute::_("index.php?option=".$option."&amp;Itemid=".$Itemid));
}

function JLMS_CART_callback( $option ) {	
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;
	$proc_id = intval(mosGetParam($_REQUEST, 'proc', 0));
	$item_number = intval(mosGetParam($_POST, 'item_number', 0));
	$is_sub = intval(mosGetParam($_REQUEST, 'subscr', 0)); //TODO: (low) rename parameter		
	$query = "SELECT * FROM #__lms_subscriptions_procs WHERE id=".$proc_id;
	$JLMS_DB->setQuery( $query );
	$proc = $JLMS_DB->loadObject();

	if ($JLMS_CONFIG->get('debug_mode', false)) {
		jimport('joomla.error.log');
		$log = & JLog::getInstance('payments.log');
		
		ob_start();
		echo "called in function callback()<br />";
		var_dump( $_REQUEST );
		$content = ob_get_contents();
		ob_end_clean();
		$entry['COMMENT'] = $content;
		$log->addEntry( $entry );
	}

	if (is_object($proc) && isset($proc->id) && $proc->id) {				
		if ( file_exists(_JOOMLMS_FRONT_HOME.DS.'includes'.DS.'processors'.DS.$proc->filename.'.php' )){						
			require_once(_JOOMLMS_FRONT_HOME.DS.'includes'.DS.'processors'.DS.$proc->filename.'.php');
			
								
			$newClass = "JLMS_".$proc->filename;
			$newProcObj = new $newClass();	
			if ($JLMS_CONFIG->get('debug_mode', false)) {
				//jimport('joomla.error.log');
				//$log =&  JLog::getInstance('payments.log');							
			}
			//TODO: (normal) check if recurrent method callback function exists in the payment module
			if ( $is_sub ) {															 
				if( method_exists( $newProcObj, 'validate_recurrent_subscription' )  ) {											
					$payment_id =  $newProcObj->validate_recurrent_subscription($proc);
				} else {					
					die();
				}
			} else {								
				$payment_id = $item_number;	
				//////////
				if ($JLMS_CONFIG->get('debug_mode', false)) {
					//jimport('joomla.error.log');
					$fileName = date('Y-m-d').'.paylog.log';
					$options['format'] = "{DATE}\t{TIME}\t{MESSAGE}";
					$log = & JLog::getInstance($fileName, $options);
					$entry['level'] = 'level';
					$entry['code'] = 'code';
					$entry['message'] = 'beforevalidate';		
					$log->addEntry( $entry );
				}
				//////////			
				$newProcObj->validate_callback($proc);
			}
			//TODO: (high) invoices for recurrent payments				

			$params_proc = new mosParameters( $proc->params );		
			
			JLMS_CART_generateinvoice( $payment_id, $params_proc );

		} else {				
			JLMSRedirect(sefRelToAbs("index.php?option=".$option."&amp;Itemid=".$Itemid."&amp;task=subscription"));
		}
	}
}

function JLMS_CART_checklicense($user_id) {
	global $JLMS_CONFIG;
	$do_add = false;
	if ($JLMS_CONFIG->get('license_lms_users')) {
		$total_students = $JLMS_CONFIG->get('license_cur_users');
		if (intval($total_students) < intval($JLMS_CONFIG->get('license_lms_users'))) {
			$do_add = true;
		}
		if (!$do_add && !$user_id) {
			$do_add = true;
		} elseif (!$do_add) {
			global $JLMS_DB;
			$query = "SELECT count(*) FROM #__lms_users_in_groups WHERE user_id = '".$user_id."'";
			$JLMS_DB->SetQuery( $query );
			if ($JLMS_DB->LoadResult()) {
				$do_add = true;
			}
		}
	} else {
		$do_add = true;
	}
	return $do_add;
}

function JLMS_CART_GetSubs(&$avail_courses, $sub_ids = array(), $course_id = 0, $category_id = 0, $category_course_ids = array()) {
	global $JLMS_DB, $JLMS_CONFIG, $my;	
	
	$where1 = '';
	$where2 = '';
	if (count($sub_ids)) {
		$sub_ids_str = implode(',', $sub_ids);
		if ($JLMS_CONFIG->get('cart_conf_subs_integration', false)) {
			$conf_ids = array();
			$conf_ids[] = 0;
			$sub_ids2 = array();
			$sub_ids2[] = 0;
			foreach ($sub_ids as $sub_id) {
				if (substr($sub_id,0,1) == 'a') {
					$test = intval(substr($sub_id,1));
					if ($test && 'a'.$test == $sub_id) {
						$conf_ids[] = $test;
					}
				} elseif(intval($sub_id)) {
					$sub_ids2[] = intval($sub_id);
				}
			}
			$sub_ids_str = implode(',', $sub_ids2);
			$conf_ids_str = implode(',', $conf_ids);
			$where2 = ' WHERE a.id IN ('.$conf_ids_str.')';
		}
		$where1 = ' AND a.id IN ('.$sub_ids_str.')';
	}      
	
	/*Restricted Subscription*/
	$query = "SELECT group_id"
	. "\n FROM #__lms_users_in_global_groups"
	. "\n WHERE 1"
	. "\n AND user_id = '".$my->id."'"
	;
	$JLMS_DB->setQuery($query);
	$group_ids = JLMSDatabaseHelper::loadResultArray();
	/*Restricted Subscription*/
    
    $where1 .= ' AND c.prereq_mode = 0';
	
	$query = "SELECT a.*, b.*, c.course_name "
	."FROM `#__lms_subscriptions` as a, `#__lms_subscriptions_courses` as b LEFT JOIN #__lms_courses as c ON b.course_id = c.id"
	."\n WHERE a.id = b.sub_id AND a.published = 1 AND ( (a.account_type = 2 AND a.end_date >= '".date('Y-m-d')."') OR (a.account_type = 1 OR a.account_type = 3 OR a.account_type = 4 OR a.account_type = 5 OR a.account_type = 6) )"
	.$where1;
	/*Restricted Subscription*/
	$query .= "\n AND (";
	if(count($group_ids)){
		$query .= "\n (a.restricted = 1";
		$query .= "\n AND (";
		for($i=0;$i<count($group_ids);$i++){
			if($i){
				$query .= "\n OR";	
			}
			$query .= "\n a.restricted_groups LIKE '%".$group_ids[$i]."%'";	
		}
		$query .= "\n )) OR ";
	} 
	$query .= "a.restricted = 0)";
	/*Restricted Subscription*/
	$query .= "\n ORDER BY a.sub_name, a.start_date, c.course_name, a.id";
	$JLMS_DB->setQuery($query);
	$subscriptions = $JLMS_DB->loadObjectList();
			
	$query = "SELECT ps.subscr_id, p.p1, p.p2, p.p3, p.srt, p.src, p.sra, p.t1, p.t2, p.t3 FROM #__lms_plans AS p, #__lms_plans_subscriptions AS ps WHERE p.id = ps.plan_id AND p.published = 1";
	$JLMS_DB->setQuery($query);
	$plans = $JLMS_DB->loadObjectList('subscr_id');
			
	for( $i = 0; $i < count($subscriptions); $i++   ) 
	{
		$sub_id = $subscriptions[$i]->id;		
		if( isset($plans[$sub_id]) ) 
		{			
			$subscriptions[$i]->p1 = $plans[$sub_id]->p1;
			$subscriptions[$i]->p2 = $plans[$sub_id]->p2;
			$subscriptions[$i]->p3 = $plans[$sub_id]->p3;
			$subscriptions[$i]->t1 = $plans[$sub_id]->t1;
			$subscriptions[$i]->t2 = $plans[$sub_id]->t2;
			$subscriptions[$i]->t3 = $plans[$sub_id]->t3;
			$subscriptions[$i]->srt = $plans[$sub_id]->srt;
			$subscriptions[$i]->src = $plans[$sub_id]->src;
			$subscriptions[$i]->sra = $plans[$sub_id]->sra;			 
		} else 
		{
			$subscriptions[$i]->p1 = 0;
			$subscriptions[$i]->p2 = 0;
			$subscriptions[$i]->p3 = 0;
			$subscriptions[$i]->t1 = 0;
			$subscriptions[$i]->t2 = 0;
			$subscriptions[$i]->t3 = 0;
			$subscriptions[$i]->srt = 0;
			$subscriptions[$i]->src = 0;
			$subscriptions[$i]->sra = 0;
		}
	}
		
	$new_subs = array();
	if ($JLMS_CONFIG->get('cart_conf_subs_integration', false) && !$course_id && !$category_id) {
		$query = "SELECT a.*"
		."FROM `#__lmsce_conf_subs` as a ".$where2
		."\n ORDER BY a.sub_name, a.id";
		$JLMS_DB->setQuery($query);
		$conf_subscriptions = $JLMS_DB->loadObjectList();
		for ($j = 0, $m = count($conf_subscriptions); $j < $m; $j ++) {
			$subscription = $conf_subscriptions[$j];
			$new_sub = new stdClass();
			$new_sub->id = 'a'.$subscription->id;
			$new_sub->price = $subscription->price;
			$new_sub->discount = 0;
			$new_sub->access_days = 0;
			$new_sub->account_type = 100;
			$new_sub->details = $subscription->num_credits;
			$new_sub->allow_multiple = ($JLMS_CONFIG->get('multiple_subs_checkout', true) ? 1 : 0);
			$new_sub->sub_name = $subscription->sub_name;
			$new_sub->start_date = null;
			$new_sub->end_date = null;
			$new_sub->description = (isset($subscription->sub_descr) && $subscription->sub_descr) ? $subscription->sub_descr : '';
			$new_sub->courses = array();
			$new_sub->course_names = array();			
			$new_subs[] = $new_sub;
		}
	}

	for ($j = 0, $m = count($subscriptions); $j < $m; $j ++) {
		$subscription = $subscriptions[$j];
		$do_new = true;
		for ($i = 0, $n = count($new_subs); $i < $n; $i ++) {
			if ($new_subs[$i]->id == $subscription->id) {
				$new_subs[$i]->courses[] = $subscription->course_id;
				$new_subs[$i]->course_names[] = $subscription->course_name;
				$do_new = false;
			}
		}
		if ($do_new) {
			$if_mogno = true;
			for ($k = 0, $f = count($subscriptions); $k < $f; $k ++) {
				if ($subscriptions[$k]->id == $subscription->id) {
					if (!in_array($subscriptions[$k]->course_id, $avail_courses)) {
						$if_mogno = false; break;
					}
				}
			}
			if ($if_mogno && ($course_id || $category_id)) {
				$if_mogno = false;						
				for ($k = 0, $f = count($subscriptions); $k < $f; $k ++) {
					if ($subscriptions[$k]->id == $subscription->id) {
						if ( $subscriptions[$k]->course_id == $course_id || ($category_id && in_array($subscriptions[$k]->course_id, $category_course_ids)) ) {
							$if_mogno = true;
							if ($course_id) {
								if ($subscriptions[$k]->course_id == $course_id) {
								} else {
									$if_mogno = false;
								}
							}
							break;
						}
					}
				}
			}
			if ($if_mogno) {
				$new_sub = new stdClass();
				$new_sub->id = $subscription->id;
				$new_sub->price = $subscription->price;
				$new_sub->discount = $subscription->discount;
				$new_sub->access_days = $subscription->access_days;
				$new_sub->account_type = $subscription->account_type;
				$new_sub->allow_multiple = ($subscription->account_type == 4 && ($JLMS_CONFIG->get('multiple_subs_checkout', true))) ? 1 : 0;
				$new_sub->sub_name = $subscription->sub_name;
				$new_sub->start_date = $subscription->start_date;
				$new_sub->end_date = $subscription->end_date;
				$new_sub->description = $subscription->sub_descr;
				$new_sub->courses = array($subscription->course_id);
				$new_sub->course_names = array($subscription->course_name);
				$new_sub->p1 = $subscription->p1;
				$new_sub->p2 = $subscription->p2;
				$new_sub->p3 = $subscription->p3;
				$new_sub->a1 = $subscription->a1;
				$new_sub->a2 = $subscription->a2;
				$new_sub->a3 = $subscription->a3;
				$new_sub->t1 = $subscription->t1;
				$new_sub->t2 = $subscription->t2;
				$new_sub->t3 = $subscription->t3;
				$new_sub->srt = $subscription->srt;
				$new_sub->src = $subscription->src;
				$new_sub->sra = $subscription->sra;
								
				$new_subs[] = $new_sub;
			}
		}
	}

	return $new_subs;
}

function JLMS_CART_GetSubsLimited($ids, &$all_subs, $limitstart, $limit) {
	global $JLMS_DB, $JLMS_CONFIG, $my;

	$sub_limited = array();
	if (count($ids)) {
		$conf_ids = array();
		$sub_ids = array();
		if ($JLMS_CONFIG->get('cart_conf_subs_integration', false)) {
			foreach ($ids as $sub_id) {
				if (substr($sub_id,0,1) == 'a') {
					$test = intval(substr($sub_id,1));
					if ($test && 'a'.$test == $sub_id) {
						$conf_ids[] = $test;
					}
				} elseif(intval($sub_id)) {
					$sub_ids[] = intval($sub_id);
				}
			}

			$ids_string = implode(",", $sub_ids);
			$conf_ids_string = implode(",", $conf_ids);

			$query = "SELECT count(id) FROM `#__lmsce_conf_subs`";
			$JLMS_DB->setQuery($query);
			$confs_total = $JLMS_DB->loadResult();

			$query = "SELECT id "
			."FROM `#__lmsce_conf_subs` "
			."\n WHERE id IN ($conf_ids_string) "
			. "\n ORDER BY sub_name "
			. ($limit ? ("\n LIMIT $limitstart, $limit") : '');
			$JLMS_DB->setQuery($query);
			$conf_subscriptions = $JLMS_DB->loadObjectList();
			if (count($conf_subscriptions)) {

				foreach($conf_subscriptions as $conf_subscription){
					if (in_array('a'.$conf_subscription->id, $ids) ){
						foreach($all_subs as $sub){
							if ($sub->id == 'a'.$conf_subscription->id){
								$sub_limited[] = $sub;
							}
						}
					}
				}

				$limit = $limit - count($conf_subscriptions);
			}
			$limitstart = $limitstart - ($confs_total ? $confs_total : 0);
			if ($limitstart <= 0) { $limitstart = 0;}
		} else {
			$ids_string = implode(",", $ids);
		}
		if ($limit > 0) { // if this variable was not redefined in the 'Conf_subs' section
			/*Restricted Subscription*/
			$query = "SELECT group_id"
			. "\n FROM #__lms_users_in_global_groups"
			. "\n WHERE 1"
			. "\n AND user_id = '".$my->id."'"
			;
			$JLMS_DB->setQuery($query);
			$group_ids = JLMSDatabaseHelper::loadResultArray();
			/*Restricted Subscription*/
			
			$query = "SELECT id "
			."FROM `#__lms_subscriptions` "
			."\n WHERE id IN ($ids_string)";
			/*Restricted Subscription*/
			$query .= "\n AND (";
			if(count($group_ids)){
				$query .= "\n (restricted = 1";
				$query .= "\n AND (";
				for($i=0;$i<count($group_ids);$i++){
					if($i){
						$query .= "\n OR";	
					}
					$query .= "\n restricted_groups LIKE '%".$group_ids[$i]."%'";	
				}
				$query .= "\n )) OR ";
			} 
			$query .= "restricted = 0)";
			/*Restricted Subscription*/
			$query .= "\n ORDER BY sub_name, start_date "
			. ($limit ? ("\n LIMIT $limitstart, $limit") : '');
			$JLMS_DB->setQuery($query);
			$subscriptions = $JLMS_DB->loadObjectList();
		
			foreach($subscriptions as $subscription){
				if (in_array($subscription->id, $ids) ){
					foreach($all_subs as $sub){
						if ($sub->id == $subscription->id){
							$sub_limited[] = $sub;
						}
					}
				}
			}
		}
	}
	return $sub_limited;
}
function JLMS_CART_GetSubsIDs(&$subs) {
	$ids = array(0);
	foreach($subs as $sub){
		$ids[] = $sub->id;
	}
	return $ids;
}
function JLMS_CART_GetCourses($user_id) {
	global $JLMS_DB, $JLMS_CONFIG, $my, $JLMS_ACL;
	
	$JLMS_ACL = JLMSFactory::getACL();
	
	if($JLMS_ACL->_role_type < 2) {	
		$query1 = "SELECT group_id FROM `#__lms_users_in_global_groups` WHERE user_id = '".$my->id."'";
		$JLMS_DB->setQuery($query1);
		$user_group_ids = JLMSDatabaseHelper::loadResultArray();
	}
	else {
		$query1 = "SELECT group_id FROM `#__lms_users_in_global_groups` WHERE user_id = '".$my->id."'";
		$JLMS_DB->setQuery($query1);
		$user_group_ids = JLMSDatabaseHelper::loadResultArray();
		
		$query1 = "SELECT group_id FROM `#__lms_user_assign_groups` WHERE user_id = '".$my->id."'";
		$JLMS_DB->setQuery($query1);
		$user_group_ids1 = JLMSDatabaseHelper::loadResultArray();
		
		$user_group_ids = array_merge($user_group_ids,$user_group_ids1);
	}	
	
	$restricted_courses = JLMS_illegal_courses($user_group_ids);
	
	$restricted_courses = implode(',', $restricted_courses);
	
		if($restricted_courses == '')
			$restricted_courses = "''";
			
	if ($JLMS_CONFIG->get('sec_cat_use', 0)){
		$query = "SELECT gid, paid, id AS value, course_name AS text, cat_id, sec_cat FROM `#__lms_courses` WHERE published = 1"
			. ( $JLMS_CONFIG->get('show_future_courses', false) ? '' : "\n AND ( ((publish_start = 1) AND (start_date <= '".date('Y-m-d')."')) OR (publish_start = 0) )" )
			. "\n AND ( ((publish_end = 1) AND (end_date >= '".date('Y-m-d')."')) OR (publish_end = 0) ) AND id NOT IN ($restricted_courses) ORDER BY course_name";
	} else {
		$query = "SELECT gid, paid, id AS value, course_name AS text, cat_id, '' as sec_cat FROM `#__lms_courses` WHERE published = 1"
			. ( $JLMS_CONFIG->get('show_future_courses', false) ? '' : "\n AND ( ((publish_start = 1) AND (start_date <= '".date('Y-m-d')."')) OR (publish_start = 0) )" )
			. "\n AND ( ((publish_end = 1) AND (end_date >= '".date('Y-m-d')."')) OR (publish_end = 0) ) AND id NOT IN ($restricted_courses) ORDER BY course_name";
	}
	$JLMS_DB->setQuery($query);
	$courses = $JLMS_DB->loadObjectList();
	$courses_ret = array();
	foreach($courses as $course){
		$can_enroll = JLMS_checkCourseGID($user_id, $course->gid);
		if ($can_enroll){
			$courses_ret[] = $course;
		}
	}
	return $courses_ret;
}

function JLMS_CART_GetPaidCourses(&$courses){
	$courseDis = array();
	foreach($courses as $course){
		if ($course->paid){
			$courseDis[] = $course;
		}
	}
	return $courseDis;
}
function JLMS_CART_GetCoursesIDs(&$courses) {
	$avail_courses = array();
	foreach($courses as $course){
		$avail_courses[] = $course->value;
	}
	return $avail_courses;
}

function JLMS_CART_getCartItems($cart, $item_type = 0) {
	global $JLMS_CONFIG;
	$items = array();
	if ($item_type == 0) {
		foreach ($cart as $cart_item) {
			if (intval($cart_item) == $cart_item && $cart_item) {
				$items[] = $cart_item;
			}
		}
	} elseif ($item_type == 1 && $JLMS_CONFIG->get('cart_conf_subs_integration', false)) {
		foreach ($cart as $cart_item) {
			if (substr($cart_item,0,1) == 'a') {
				$test = intval(substr($cart_item,1));
				if ($test && 'a'.$test == $cart_item) {
					$items[] = $test; // intval !!!
				}
			}
		}
	}
	return $items;
}

function JLMS_CART_GenerateCart($generate_from_ids) {
	global $JLMS_CONFIG;
	$current_cart = array();
	if (is_array($generate_from_ids)) {
		if (!empty($generate_from_ids)) {
			foreach ($generate_from_ids as $current_cart_tmp_item) {
				if ($JLMS_CONFIG->get('cart_conf_subs_integration', false) && substr($current_cart_tmp_item,0,1) == 'a' ) {
					$test = intval(substr($current_cart_tmp_item,1));
					if ($test && 'a'.$test == $current_cart_tmp_item) {
						$current_cart[] = 'a'.$test;
					}
				} elseif(intval($current_cart_tmp_item) && intval($current_cart_tmp_item) == $current_cart_tmp_item) {
					$current_cart[] = intval($current_cart_tmp_item);
				}
			}
		}
	}

	$checked_cart = JLMS_CART_CheckCart($current_cart);
	return $checked_cart;
}

function JLMS_CART_CheckCart($current_cart) {
	global $my;
	$checked_cart = array();

	$courses = JLMS_CART_GetCourses($my->id);
	$avail_courses = JLMS_CART_GetCoursesIDs($courses);
	$new_subs = JLMS_CART_GetSubs($avail_courses, $current_cart);
	$subs_ids = JLMS_CART_GetSubsIDs($new_subs);
	foreach ($current_cart as $cc) {
		if (in_array($cc, $subs_ids)) {
			if (in_array($cc, $checked_cart)) {
				foreach ($new_subs as $ns) {
					if ($ns->id == $cc) {
						if (isset($ns->allow_multiple) && $ns->allow_multiple) {
							$checked_cart[] = $cc;
						}
						break;
					}
				}
			} else {
				$checked_cart[] = $cc;
			}
		}
	}

	return $checked_cart;
}

function JLMS_multicats($last_catid, $tmp, $i=0){
	global $JLMS_DB;
	
	$query = "SELECT parent FROM #__lms_course_cats WHERE id = '".$last_catid."'";
	$JLMS_DB->setQuery($query);
	$parent = $JLMS_DB->loadResult();
	$tmp[$i] = new stdClass();
	$tmp[$i]->catid = $last_catid;
	$tmp[$i]->parent = isset($parent)?$parent:0;
	if($parent){
		$last_catid = $parent;
		$i++;
		$tmp = JLMS_multicats($last_catid, $tmp, $i);
	}
	return $tmp;
}

function JLMS_CART_listSubs($course_id, $option) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG, $JLMS_SESSION, $JLMS_ACL;
       
	if(isset($_REQUEST['category_filter']) && $_REQUEST['category_filter']) {
		//DEN (22.09.2010) : fix for compatibility with old filters, do not remove this code
		$tmp_course_filter = JRequest::getInt('category_filter');
		if ($tmp_course_filter) {
			$_REQUEST['level_id_0'] = $tmp_course_filter;
		}
	}
    
    $prereqCourses = JLMS_prereq_courses( $course_id );
        
    if( !empty( $prereqCourses ) ) 
    {
        JLMSRedirect(sefRelToAbs("index.php?option=$option&task=details_course&id=".$course_id));
    }

	$JLMS_ACL = JLMSFactory::getACL();
	
	$ignore_free_session_enroll = false;
	$course_id1 = intval(mosGetParam($_REQUEST, 'course_filter', 0 ));
	if (!$course_id1) {
		if (!$course_id && $JLMS_SESSION->get('course_filter')) {
			$after_reg = intval(mosGetParam($_REQUEST, 'after_reg', 0 ));
			if (!$after_reg) {
				$ignore_free_session_enroll = true;
			}
		}
		$course_id1 = ($course_id ? $course_id : (isset($_REQUEST['course_filter']) ? 0 : $JLMS_SESSION->get('course_filter')));
	}

	$course_id = $course_id1;
	$JLMS_SESSION->set('course_filter', $course_id);
	
	//FLMS multicat
	$query_1 = '';
	if($JLMS_ACL->_role_type == 2 || $JLMS_ACL->_role_type == 3 || $JLMS_ACL->_role_type == 4) {
		$view_all_course_categories = $JLMS_ACL->CheckPermissions('advanced', 'view_all_course_categories');
		if(!$view_all_course_categories) {
			$query = "SELECT a.group_id FROM `#__lms_user_assign_groups` as a WHERE a.user_id = '".$my->id."'";
			$JLMS_DB->setQuery($query);
			$user_group_ids = JLMSDatabaseHelper::loadResultArray();
			if (count($user_group_ids)) {
				$query_1 = "  ( `restricted` = 0 OR (`restricted` = 1 AND (`groups` LIKE '%|$user_group_ids[0]|%'";
				for($i=1;$i<count($user_group_ids);$i++) {
					$query_1 .= "\n OR `groups` like '%|$user_group_ids[$i]|%'";
				}
				$query_1 .=  "\n ) ) ) \n ";
			}
		}
	}

	$levels = array();
	if ($JLMS_CONFIG->get('multicat_use', 0)){
		$category_id = intval(mosGetParam($_REQUEST, 'level_id_0', 0));
		$query = "SELECT * FROM #__lms_course_cats_config ORDER BY id";
		$JLMS_DB->setQuery($query);
		$levels = $JLMS_DB->loadObjectList();
		if(count($levels) == 0){
			for($i=0;$i<5;$i++){
				$levels[$i] = new stdClass();
				$levels[$i]->cat_name = _JLMS_COURSES_COURSES_GROUPS;
			}
		}
		$level_id = array();
		for($i=0;$i<count($levels);$i++){
			if(isset($_REQUEST['category_filter']) && $_REQUEST['category_filter']){
				if($i == 0){
					$level_id[$i] = $_REQUEST['category_filter'];
					$parent_id[$i] = 0;
				} else {
					$level_id[$i] = 0;	
					$parent_id[$i] = $level_id[$i-1];
				}
			} else {
				if($i == 0){
					$level_id[$i] = intval( mosGetParam( $_REQUEST, 'level_id_'.$i, $JLMS_SESSION->get('FLMS_level_id_'.$i, $category_id) ) );
					$JLMS_SESSION->set('FLMS_level_id_'.$i.'', $level_id[$i]);
				} else {
					$level_id[$i] = intval( mosGetParam( $_REQUEST, 'level_id_'.$i, $JLMS_SESSION->get('FLMS_level_id_'.$i, 0) ) );
					$JLMS_SESSION->set('FLMS_level_id_'.$i.'', $level_id[$i]);
				}
				if($i == 0){
					$parent_id[$i] = 0;
				} else {
					$parent_id[$i] = $level_id[$i-1];
				}
				if($i == 0 || $parent_id[$i]){ //(Max): extra requests
					$query = "SELECT * FROM `#__lms_course_cats` WHERE parent = '".$parent_id[$i]."'"
					. ( $query_1 ? " AND ".$query_1 : "" )
					."\n ORDER BY c_category";

					$JLMS_DB->setQuery($query);
					$groups = $JLMS_DB->loadObjectList();
					if(count($groups)==0){
						$level_id[$i] = 0;	
						$JLMS_SESSION->set('FLMS_level_id_'.$i.'', $level_id[$i]);
					}
				}
			}
		}
		
		for($i=0;$i<count($levels);$i++){
			if($i > 0 && $level_id[$i - 1] == 0){
				$level_id[$i] = 0;
				$_REQUEST['filter_id_'.$i] = $level_id[$i];
				$JLMS_SESSION->set('FLMS_level_id_'.$i.'', $level_id[$i]);
				$parent_id[$i] = 0;
			} elseif($i == 0 && $level_id[$i] == 0) {
				$level_id[$i] = 0;
				$_REQUEST['filter_id_'.$i] = $level_id[$i];
				$JLMS_SESSION->set('FLMS_level_id_'.$i.'', $level_id[$i]);
				$parent_id[$i] = 0;
			}
		}
	} else {
		$category_id = intval(mosGetParam($_REQUEST, 'category_filter', 0));
	}

	$new_cid = array();
	if ($course_id) {
		$query = "SELECT gid FROM `#__lms_courses` WHERE id = $course_id ";
		$JLMS_DB->setQuery($query);
		$course_gid = $JLMS_DB->loadResult();

		$can_enroll = JLMS_checkCourseGID($my->id, $course_gid);
		if (!$can_enroll){
			JLMSRedirect(sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"));
		}
	}

	if (JLMS_CART_checklicense($my->id)) {
		$proceed_to_cart = true;
		if ($course_id) {
			$query = "SELECT * FROM `#__lms_courses` WHERE id = $course_id";
			$JLMS_DB->setQuery($query);
			$course_info = $JLMS_DB->loadObject();
			if (is_object($course_info) && isset($course_info->id)) {
				if (!$course_info->paid && !$ignore_free_session_enroll) {
					$course_usertype = 0;
					if ( in_array($course_info->id, $JLMS_CONFIG->get('teacher_in_courses',array(0))) ) {
						$course_usertype = 1;
					} elseif ( in_array($course_info->id, $JLMS_CONFIG->get('student_in_courses',array(0))) ) {
						$course_usertype = 2;
					}
					if (!$course_usertype) {
						JLMS_subscription_html::JLMS_free_subscriptions($option, $course_info, $course_id, false);
						$proceed_to_cart = false;
					} else {
						JLMSRedirect(sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$course_id"), '_clear_');
					}
				} elseif (!$course_info->paid){
					$course_id = 0;
					$JLMS_SESSION->clear('course_filter');
				}
			}
		}
		
		if ($proceed_to_cart) {

			$type[] = mosHTML::makeOption( 0, _JLMS_SUBSCR_ALL_COURSES );
			
			$courses = JLMS_CART_GetCourses($my->id);            
			$courseDis = JLMS_CART_GetPaidCourses($courses);
			$avail_courses = JLMS_CART_GetCoursesIDs($courses);           
			
//			$courses = array_merge($type, $courseDis); // for populate to filter
			$courses = $courseDis;

			$tmp = array();
			$tmp_catids = array();
			foreach ($courses as $row){
				if(isset($row->cat_id) && $row->cat_id){
					$last_catid = $row->cat_id;
					$tmp = JLMS_multicats($last_catid, $tmp);
					if ($JLMS_CONFIG->get('sec_cat_use', 0)) {
						// TODO - this is not necessry if $level_id[1] is activated
						$sec_cats = $row->sec_cat;
						if ($sec_cats) {
							$sec_cats_arr = explode('|',$sec_cats);
							if (!empty($sec_cats_arr)) {
								foreach ($sec_cats_arr as $sec_cats_elem) {
									$tmp = JLMS_multicats($sec_cats_elem, $tmp, count($tmp));
								}
							}
						}
					}
					foreach($tmp as $t){
						if( $t->catid && !in_array($t->catid, $tmp_catids) ){
							$tmp_catids[] = $t->catid;	
						}
					}
				}
			}
			$cat_string = implode(",", $tmp_catids);
			//var_dump($cat_string);

			/*		
			$query = "SELECT id, c_category FROM #__lms_course_cats WHERE id IN ($cat_string) ORDER BY c_category";
			$JLMS_DB->setQuery($query);
			$categories = $JLMS_DB->loadObjectList();
			*/
			
			//NEW MUSLTICATS
//			$tmp_level = array();
			$last_catid = 0;
			
			$tmp_cats_filter = JLMS_getFilterMulticategories($last_catid, $query_1, 'level_id_');
			/*
			if(isset($_REQUEST['category_filter']) && $_REQUEST['category_filter']){
				$last_catid = $_REQUEST['category_filter'];
				$category_id = $_REQUEST['category_filter'];
			} else {
				$i=0;
				foreach($_REQUEST as $key=>$item){
					if(preg_match('#level_id_(\d+)#', $key, $result)){
						if($item){
							$tmp_level[$i] = $result;
							$last_catid = $item;
							$i++;
						}	
					}	
				}
			}
			
			$query = "SELECT * FROM #__lms_course_cats"
					. ( $query_1 ? " WHERE ".$query_1 : "" )
					."\n ORDER BY id";
					
			$JLMS_DB->setQuery($query);
			$all_cats = $JLMS_DB->loadObjectList();
			
			$tmp_cats_filter = array();
			$children = array();
			foreach($all_cats as $cat){
				$pt = $cat->parent;
				$list = @$children[$pt] ? $children[$pt] : array();
				array_push($list, $cat->id);
				$children[$pt] = $list;
			}
			$tmp_cats_filter[0] = $last_catid;
			$i=1;
			foreach($children as $key=>$childs){
				if($last_catid == $key){
					foreach($children[$key] as $v){
						if(!in_array($v, $tmp_cats_filter)){
							$tmp_cats_filter[$i] = $v;
							$i++;
						}
					}
				}
			}
			foreach($children as $key=>$childs){
				if(in_array($key, $tmp_cats_filter)){
					foreach($children[$key] as $v){
						if(!in_array($v, $tmp_cats_filter)){
							$tmp_cats_filter[$i] = $v;
							$i++;
						}
					}
				}
			}
			$tmp_cats_filter = array_unique($tmp_cats_filter);
			*/
			$catids = implode(",", $tmp_cats_filter);
			//NEW MUSLTICATS
			$tmp_courses = array();
			$category_course_ids = array();
			foreach($courses as $course){
				$is_added = false;
				if( in_array($course->cat_id, $tmp_cats_filter)){
					$tmp_courses[] = $course;
					$is_added = true;
					if(isset($course->value) && $course->value){
						$category_course_ids[] = $course->value;	
					}
				}
				if (!$is_added) {
					if($JLMS_CONFIG->get('sec_cat_use', 0)) {
						foreach ($tmp_cats_filter as $tmp_cats_filter_one) {
							if (strpos($course->sec_cat, '|'.$tmp_cats_filter_one.'|') !== false) {
								$tmp_courses[] = $course;
								$category_course_ids[] = $course->value;
								break;
							}
						}
					}
				}	
			}

			$f_courses = array();
			$f_courses[] = mosHTML::makeOption( 0, _JLMS_SUBSCR_ALL_COURSES );
			$f_courses = array_merge($f_courses, $tmp_courses);

			if( !in_array($course_id, $category_course_ids) ){
				$course_id = 0;
			}

			/*
			$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=subscription";
			$link = $link ."&amp;category_filter='+this.options[selectedIndex].value+'";
			$link = $link ."&amp;course_filter=0";
			$link = sefRelToAbs($link);
			$link = str_replace('%5C%27',"'", $link);$link = str_replace('%5B',"[", $link);$link = str_replace('%5D',"]", $link);$link = str_replace('%20',"+", $link);$link = str_replace("\\\\\\","", $link);$link = str_replace('%27',"'", $link);
			$lists['category_filter'] = mosHTML::selectList($f_categories, 'category_filter', 'class="inputbox" size="1" onchange="document.location.href=\''.$link.'\';"', 'value', 'text', $category_id);
			*/
			
			//FLMS multicat
			if ($JLMS_CONFIG->get('multicat_use', 0)){
				$javascript = 'onclick="javascript:read_filter();" onchange="javascript:write_filter();document.JLMS_adminForm.task.value=\'subscription\';document.JLMS_adminForm.Itemid.value=\''.$Itemid.'\';document.JLMS_adminForm.submit();"';
				$type_level = array();
				for($i=0;$i<count($levels);$i++){
					if($i == 0 || $parent_id[$i]){ //(Max): extra requests
						if($i == 0){
							if (!$cat_string) {
								$cat_string = '0';
							}
							$query = "SELECT * FROM `#__lms_course_cats` WHERE parent = '".$parent_id[$i]."' AND id IN ($cat_string) "
							. ( $query_1 ? " AND ".$query_1 : "" )
							. "ORDER BY c_category"; //AND id IN ($cat_string)
						} else { 
							$query = "SELECT * FROM `#__lms_course_cats` WHERE parent = '".$parent_id[$i]."'"
							. ( $query_1 ? " AND ".$query_1 : "" )
							. "ORDER BY c_category";
						}
						$JLMS_DB->setQuery($query);
						$groups = $JLMS_DB->loadObjectList();
						
						if($parent_id[$i] && $i > 0 && count($groups)){
							$type_level[$i][] = mosHTML::makeOption( 0, ' &nbsp; ' );
							foreach ($groups as $group){
								$type_level[$i][] = mosHTML::makeOption( $group->id, $group->c_category );
							}
							$lists['level_'.$i.''] = mosHTML::selectList($type_level[$i], 'level_id_'.$i, 'class="inputbox" size="1" style="width: 266px;" '.$javascript, 'value', 'text', $level_id[$i] ); //onchange="document.location.href=\''. $link_multi .'\';"
						} elseif($i == 0) {
							$type_level[$i][] = mosHTML::makeOption( 0, ' &nbsp; ' );
							foreach ($groups as $group){
								$type_level[$i][] = mosHTML::makeOption( $group->id, $group->c_category );
							}
							$lists['level_'.$i.''] = mosHTML::selectList($type_level[$i], 'level_id_'.$i, 'class="inputbox" size="1" style="width: 266px;" '.$javascript, 'value', 'text', $level_id[$i] ); //onchange="document.location.href=\''. $link_multi .'\';"
						}
					}
				}
			}
			//FLMS multicat
			
			//category filter created
			/*
			$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=subscription";
			if ($JLMS_CONFIG->get('multicat_use', 0)){
				
				$link = $link ."&amp;category_filter='+getCategoryValue()+'";
			} else {
				$link = $link ."&amp;category_filter='+getCategoryValue()+'";
			}
			$link = $link ."&amp;course_filter='+this.options[selectedIndex].value+'";
			$link = sefRelToAbs($link);
			$link = str_replace('%5C%27',"'", $link);$link = str_replace('%5B',"[", $link);$link = str_replace('%5D',"]", $link);$link = str_replace('%20',"+", $link);$link = str_replace("\\\\\\","", $link);$link = str_replace('%27',"'", $link);
			*/
			$lists['course_filter'] = mosHTML::selectList($f_courses, 'course_filter', 'class="inputbox" size="1" style="width: 266px;" onchange="document.JLMS_adminForm.task.value=\'subscription\';document.JLMS_adminForm.submit();"', 'value', 'text', $course_id );
			$lists['course_filter_big'] = (count($courseDis) ? true : false);
			$lists['course_filter_cur'] = $course_id;

			$limit = intval( mosGetParam( $_REQUEST, 'limit', $JLMS_SESSION->get('list_limit',$JLMS_CONFIG->get('list_limit')) ) );
			$JLMS_SESSION->set('list_limit', $limit);
			$limitstart = intval( mosGetParam( $_REQUEST, 'limitstart', 0 ) );
			
			$new_subs = JLMS_CART_GetSubs($avail_courses, array(), $course_id, $category_id, $category_course_ids);//array();
			
			$ids = JLMS_CART_GetSubsIDs($new_subs);

			$total = count($new_subs);
			$ids_string = implode(",", $ids);
			require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
			$pageNav = new JLMSPageNav( $total, $limitstart, $limit );
				
			
			$sub_limited = JLMS_CART_GetSubsLimited($ids, $new_subs, $pageNav->limitstart, $pageNav->limit);	
			
			JLMS_CART_html::ListSubs($option , $sub_limited, $pageNav, $lists, $levels );
		}

	} else {
		$msg = 'Sorry user limit is exceeded, please contact your LMS administrator.';
		$JLMS_SESSION->set('joomlalms_sys_message',$msg);

		JLMSRedirect (sefRelToAbs("index.php?option=$option&amp;task=courses&amp;Itemid=$Itemid"));
	}
}

function JLMS_CART_addItem($option) {
	global $Itemid, $JLMS_DB, $JLMS_SESSION;
	$jlms_subs = mosGetParam($_REQUEST, 'jlms_sub', array());
	
	if (!is_array($jlms_subs)) 
	{
		$jlms_subs = array();
	}
	
	if (!empty($jlms_subs)) {		 
		$current_cart_tmp_cookie = array();
		if( isset($_COOKIE['joomlalms_cart_contents']) ) 
		{
			$current_cart_tmp_cookie = explode(',',$_COOKIE['joomlalms_cart_contents'] );			 
		}
				
		$quoted_jlms_subs = array_map( array( $JLMS_DB, 'quote' ), array_merge( $jlms_subs, $current_cart_tmp_cookie ) );		
						
		$query = "SELECT count(account_type) FROM #__lms_subscriptions WHERE id IN (".implode(', ', $quoted_jlms_subs).") AND account_type = 6";
		$JLMS_DB->setQuery( $query );
		$recurrent_subs_count = $JLMS_DB->loadResult( $query );	

		if( $recurrent_subs_count > 1 ) 
		{			
			$JLMS_SESSION->set('joomlalms_sys_message', _JLMS_RECURRENT_MORE_THAN_ONE);		
			$url = sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=subscription");
			//TODO: (low) show cart to remove previously added item or to checkout ? 	
		} else {			
			$current_cart = array();
			$current_cart_tmp_cookie = isset($_COOKIE['joomlalms_cart_contents']) ? $_COOKIE['joomlalms_cart_contents'] : '';
			if ( $current_cart_tmp_cookie ) {
				$current_cart_tmp_cookie = urldecode($current_cart_tmp_cookie);
				$current_cart_tmp = explode(',',$current_cart_tmp_cookie);
				$current_cart = JLMS_CART_GenerateCart($current_cart_tmp);
			}
						
			foreach ($jlms_subs as $jlms_sub_item) {
				if ($jlms_sub_item) {
					$current_cart[] = $jlms_sub_item;
				}
			}

			$current_cart = JLMS_CART_CheckCart($current_cart);
			$current_cart_cookie = implode(',', $current_cart);						

			JLMSCookie::setcookie('joomlalms_cart_contents', $current_cart_cookie,time()+2*24*60*60, '/'); // 2 days

			$url = sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=show_cart");
		}
	} else {
		$url = sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=show_cart"); 
	}	

	JLMSRedirect ( $url );
}

function JLMS_CART_removeItem($option) {
	global $Itemid;
	$jlms_subs = mosGetParam($_REQUEST, 'jlms_sub', array());
	if (!is_array($jlms_subs)) {
		$jlms_subs = array();
	}
	if (!empty($jlms_subs)) {
		$current_cart = array();
		$current_cart_tmp_cookie = isset($_COOKIE['joomlalms_cart_contents']) ? $_COOKIE['joomlalms_cart_contents'] : '';
		if ($current_cart_tmp_cookie) {
			$current_cart_tmp_cookie = urldecode($current_cart_tmp_cookie);
			$current_cart_tmp = explode(',',$current_cart_tmp_cookie);
			$current_cart = JLMS_CART_GenerateCart($current_cart_tmp);
		}
		$new_cart = array();
		$i = 0;
		while ($i < count($jlms_subs)) {
			if (substr($jlms_subs[$i],0,1) == 'a') {
				$test = intval(substr($jlms_subs[$i],1));
				if ($test && 'a'.$test == $jlms_subs[$i]) {
					$jlms_subs[$i] = 'a'.$test;
				}
			} else {
				$jlms_subs[$i] = intval($jlms_subs[$i]);
			}
			//$jlms_subs[$i] = intval($jlms_subs[$i]);
			$i ++;
		}
		foreach ($current_cart as $cc) {
			if (in_array($cc, $jlms_subs)) {

			} elseif ($cc) {
				$new_cart[] = $cc;
			}
		}

		$current_cart_cookie = implode(',', $new_cart);
		JLMSCookie::setcookie('joomlalms_cart_contents', $current_cart_cookie,time()+2*24*60*60, '/'); // 2 days
	}
	JLMSRedirect (sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=show_cart"));
}

function JLMS_CART_updateItems($option) {
	global $Itemid;
	$jlms_subs = mosGetParam($_REQUEST, 'jlms_sub_ids', array());
	$jlms_subs_counts = mosGetParam($_REQUEST, 'sub_count', array());
	if (!empty($jlms_subs) && !empty($jlms_subs_counts)) {
		if (count($jlms_subs) == count($jlms_subs_counts)) {
			$current_cart_tmp = array();
			$i = 0;
			while ($i < count($jlms_subs)) {
				$cur_item_tmp = $jlms_subs[$i];
				$cur_item_count = intval($jlms_subs_counts[$i]);
				if ($cur_item_tmp && $cur_item_count) {
					for ($j=0;$j<$cur_item_count;$j++) {
						$current_cart_tmp[] = $cur_item_tmp;
					}
				}
				$i ++;
			}
			$current_cart = JLMS_CART_GenerateCart($current_cart_tmp);
			$current_cart_cookie = implode(',', $current_cart);
			JLMSCookie::setcookie('joomlalms_cart_contents', $current_cart_cookie,time()+2*24*60*60, '/'); // 2 days
		}
	}
	JLMSRedirect (sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=show_cart"));
}


function JLMS_CART_checkoutItems($option) {
	global $Itemid, $my, $JLMS_CONFIG, $JLMS_DB;
	$jlms_subs = mosGetParam($_REQUEST, 'jlms_sub_ids', array());
	$jlms_subs_counts = mosGetParam($_REQUEST, 'sub_count', array());
	
	$app = JFactory::getApplication();
	
	$task2 = strval( mosGetParam( $_REQUEST, 'task2', '' ) );
	$coupon_code = $app->getUserStateFromRequest( $option.'_dis_coupon_code', 'dis_coupon_code', '' );
	$coupon_code2 = $app->getUserStateFromRequest( $option.'_dis_coupon_code2', 'dis_coupon_code2', '' );
		
	if( !$coupon_code && $coupon_code2 ) {
		$coupon_code = $coupon_code2;
		$app->setUserState( 'dis_coupon_code', $coupon_code2 );
	}

	if ($task2 == 'show_info') {		
		$is_not_auth = true;
		$order_id = intval( mosGetParam( $_REQUEST, 'order_id', 0 ) );
		$proc_id = intval( mosGetParam( $_REQUEST, 'proc_id', 0 ) );

		if ($my->id && $order_id && $proc_id) {		
			$query = "SELECT * FROM `#__lms_subscriptions_procs` WHERE id = $proc_id AND published = 1 ";
			$JLMS_DB->setQuery($query);
			$processor = $JLMS_DB->loadObject();
			if (is_object($processor) && isset($processor->filename) && $processor->filename) {
				if (file_exists(_JOOMLMS_FRONT_HOME.'/includes/processors/'.$processor->filename.'.php')) {
					$query = "SELECT * FROM `#__lms_payments` WHERE id = $order_id AND user_id = $my->id ";
					$JLMS_DB->setQuery($query);
					$payments_objlist = $JLMS_DB->LoadObjectList();
					if (count($payments_objlist)) {
						$payment = $payments_objlist[0];
						$payment->sub_name = '';
						if ($payment->payment_type == 2) {
							$query = "SELECT * FROM `#__lms_payment_items` WHERE payment_id = ".intval($order_id);
							$JLMS_DB->setQuery($query);
							$payment_items = $JLMS_DB->LoadObjectList();
							if (count($payment_items) == 1) {
								if (isset($payment_items[0]->item_type)) {
									if (!$payment_items[0]->item_type) {
										$query = "SELECT sub_name FROM `#__lms_subscriptions` WHERE id = ".intval($payment_items[0]->item_id);
										$JLMS_DB->setQuery($query);
										$payment->sub_name = $JLMS_DB->LoadResult();
									}
								}
							}
						}
						$payment->price = $payment->amount - $payment->tax_amount;
						require_once(_JOOMLMS_FRONT_HOME.'/includes/processors/'.$processor->filename.'.php');
						$newClass = "JLMS_".$processor->filename;
						$newProcObj = new $newClass();
						$newProcObj->show_checkout($option, $payment, $order_id, $processor);
						$is_not_auth = false;
					}
				}
			}
		}
	} else {		
	$is_not_auth = true;
	if (!empty($jlms_subs) && !empty($jlms_subs_counts) && $my->id) {
		if (count($jlms_subs) == count($jlms_subs_counts)) {
			$current_cart_tmp = array();
			$i = 0;
			while ($i < count($jlms_subs)) {
				$cur_item_tmp = $jlms_subs[$i];
				$cur_item_count = intval($jlms_subs_counts[$i]);
				if ($cur_item_tmp && $cur_item_count) {
					for ($j=0;$j<$cur_item_count;$j++) {
						$current_cart_tmp[] = $cur_item_tmp;
					}
				}
				$i ++;
			}
			
			$current_cart = JLMS_CART_GenerateCart($current_cart_tmp);
						
			if (!empty($current_cart)) {
				$jlms_tax_counting = $JLMS_CONFIG->get('enabletax', false);

				$payment_amount = 0;

				$proc_id = intval(mosGetParam($_REQUEST, 'proc_id', 0));
				if ($proc_id) {
					$query = "SELECT * FROM `#__lms_subscriptions_procs` WHERE id = $proc_id AND published = 1 ";
				} else {
					$query = "SELECT * FROM `#__lms_subscriptions_procs` WHERE default_p = 1 AND published = 1 ";
				}
				$JLMS_DB->setQuery($query);
				$processor = $JLMS_DB->loadObject();
				if (is_object($processor) && isset($processor->id)){
					$payment_type = 2;
					$payment = new stdClass();
					$payment->payment_type = 2;
					$payment->sub_id = 0;
					$payment->txn_id = '';
					$payment->proc_id = $processor->id;
					$payment->processor = $processor->name;
					$payment->status = 'pending';
					$payment->amount = 0; // we should estimate it !!!!!!!!!!!
					$payment->tax_amount = 0;
					$payment->date = gmdate('Y-m-d H:i:s');
					$payment->user_id = $my->id;

					$course_subs = JLMS_CART_getCartItems($current_cart, 0);
					$conf_subs = JLMS_CART_getCartItems($current_cart, 1);

					$courses = JLMS_CART_GetCourses($my->id);
					$avail_courses = JLMS_CART_GetCoursesIDs($courses);
					$new_subs = JLMS_CART_GetSubs($avail_courses, $current_cart);//array();					

					$amount = 0;
					$tax_amount = 0;
					$recurrent_obj = 0;
					if (!empty($new_subs)) {
						$i =0;
						while ($i < count($new_subs)) {
							if ( isset($new_subs[$i]->allow_multiple) && $new_subs[$i]->allow_multiple ) {
								$new_subs[$i]->count_items = 0;								
								foreach($current_cart as $cc) {
									if ($cc == $new_subs[$i]->id) {
										$new_subs[$i]->count_items ++;
									}
								}
							} else {
								$new_subs[$i]->allow_multiple = 0;
								$new_subs[$i]->count_items = 1;
							}
							$i ++;
						}
						$rows2 = array();
						$isset_country = false;
						$jlms_tax_counting = $JLMS_CONFIG->get('enabletax');
						
						if ($jlms_tax_counting){
							$is_cb_installed = $JLMS_CONFIG->get('is_cb_installed', 0);
							$get_country_info = $JLMS_CONFIG->get('get_country_info', 0);
							$cb_country_filed_id = intval($JLMS_CONFIG->get('jlms_cb_country'));
							
							if($is_cb_installed && $get_country_info && $cb_country_filed_id){ //by Max (get country info)
								$query = "SELECT cf.name"
								. "\n FROM #__comprofiler_fields as cf"
								. "\n WHERE 1"
								. "\n AND cf.fieldid = '".$cb_country_filed_id."'"
								;
								$JLMS_DB->setQuery($query);
								$cb_country_field_name = $JLMS_DB->loadResult();
								
								$query = "SELECT ".$cb_country_field_name.""
								. "\n FROM #__comprofiler"
								. "\n WHERE 1"
								. "\n AND user_id = '".$my->id."'"
								;
								$JLMS_DB->setQuery($query);
								$country_name = $JLMS_DB->loadResult();
								
								require_once('components'. DS .$option. DS .'includes'. DS .'libraries'. DS .'lms.lib.countries.php');
								$CodeCountry = new CodeCountries();
								$code = $CodeCountry->code($country_name);
								if($code){
									$user_country = $code;	
								}
								$user_country_name = '';
								$us_state = '';
							} else {
								$ip_address = $_SERVER['REMOTE_ADDR'];
								//$ip_address = '86.57.158.98';
								//$ip_address = '213.184.248.211';
								//$ip_address = '12.225.42.19';
								//$ip_address = '111.215.41.12';
								$isset_country = false;
								if(@ini_get('allow_url_fopen')){
									$fn = @file('http://api.hostip.info/get_html.php?ip='.$ip_address);
									// country ip identified
									if($fn != false){
										$ip_info = implode('',$fn);
										
										preg_match_all("(\(..\))", $ip_info, $dop);
										$user_country = str_replace('(','',str_replace(")",'',$dop[0][0]));
						
										preg_match_all("(\:.*\()", $ip_info, $dop2);
										$user_country_name = str_replace(': ','',str_replace(" (",'',$dop2[0][0]));
										preg_match_all("(\, ..)", $ip_info, $dop3);
										$us_state = @str_replace(', ','',$dop3[0][0]);
										//echo $us_state;
									}
								}
							}
						
							if(isset($user_country)){	
								$query = "SELECT * FROM #__lms_subscriptions_countries WHERE published = 1 AND code='".$user_country."' ";
								$JLMS_DB->setQuery( $query );
								$rows2 = $JLMS_DB->loadObjectList();							
								
								// if no country found
								if (!count($rows2)) {
									// check if in EU
									$query = "SELECT * FROM #__lms_subscriptions_countries WHERE published = 1 AND code='EU' AND list REGEXP '".$user_country."' ";
									$JLMS_DB->setQuery( $query );
									$rows_eu = $JLMS_DB->loadObjectList();
									if (count($rows_eu)) {
										$isset_country = true;
										$rows2[0]->tax_type = $rows_eu[0]->tax_type;
										$rows2[0]->tax = $rows_eu[0]->tax;
										$user_country_name = $rows_eu[0]->name.' ('.$user_country_name.')';
									}
								} else {
									$isset_country = true;
								}
								// additional check for US
								if ($user_country == 'US') {
									$query = "SELECT * FROM #__lms_subscriptions_countries WHERE published = 1 AND code = 'US-".$us_state."' ";
									$JLMS_DB->setQuery( $query );
									$rows_states = $JLMS_DB->loadObjectList();
									if (count($rows_states)){
										$isset_country = true;
										$rows2 = array();
										$rows2[0]->tax_type = $rows_states[0]->tax_type;
										$rows2[0]->tax = $rows_states[0]->tax;
										$user_country_name = 'United states ('.$rows_states[0]->name.' )';
									}
								}
							}	
							//10.01.09 (Max) default tax option
							if(!$isset_country){
								$rows2 = array();
								$rows2[0]->tax_type = $JLMS_CONFIG->get('default_tax_type', 1);
								$rows2[0]->tax = $JLMS_CONFIG->get('default_tax', 0);	
							}
						}						
						
						$discounts['t_coupon_disc'] = JLMS_DISCOUNTS::getTotalCouponDiscount( $coupon_code, $new_subs );
						$discounts['t_disc'] = JLMS_DISCOUNTS::getTotalDiscounts( $new_subs );
																		
							   
						$total_subs = 0;
						$total_disc = 0;
												
						foreach ($new_subs as $new_sub){						
							JLMS_CART_html::initSubscriptionPaymentParams( $new_sub, $rows2 );					
							
							if( $new_sub->account_type == '6' )
								$recurrent_obj = $new_sub; 					
							
							if (isset($new_sub->count_items) && $new_sub->count_items && isset($new_sub->allow_multiple) && $new_sub->allow_multiple) {	
								$total_subs += $new_sub->sub_total*$new_sub->count_items;							
								$tax_amount += $new_sub->tax_amount*$new_sub->count_items;
								$total_disc += $new_sub->disc*$new_sub->count_items;								
							} else {								
								$total_subs += $new_sub->sub_total;
								$tax_amount += $new_sub->tax_amount;
								$total_disc += $new_sub->disc;								
							}													
						}
						
						$total_disc += ($discounts['t_coupon_disc'] + $discounts['t_disc']);
						
						if (isset($rows2[0]->tax_type) && $rows2[0]->tax_type == 2) {
							$tax_amount += $rows2[0]->tax; // if tx is not in percentage....
						}
						
						$amount = $total_subs - $total_disc;														
										
						if( $amount < 0 ) 
							$amount = $tax_amount;
						else	
							$amount += $tax_amount;						
											
						$price_diff = $total_subs - $total_disc;
								 
						if( $price_diff < 0 ) {
							$balance = abs($price_diff);
						} else {
							$balance = 0;
						}						
																		
						if( $recurrent_obj ) 
						{																								
							JLMS_RECURRENT_PAY::initPricesObjects( $recurrent_obj );							 						
							JLMS_RECURRENT_PAY::recalcSubsParams( $recurrent_obj, 'total', $balance, $amount );							
						}											
					}		
														
										
					if ($amount > 0 || $amount == 0){						
						// if price of selected subcriptions is 0 - enrol user fo free...
						
						if( $amount == 0 && $recurrent_obj ) 
						{
							$amount = JLMS_RECURRENT_PAY::getAmountFromReccurentPrice( $recurrent_obj, 'total', $balance, $amount );							
						}
						
						$payment->amount = $amount;
						$payment->tax_amount = $tax_amount;					
						$payment->cur_code = $JLMS_CONFIG->get('jlms_cur_code');
						
						$JLMS_DB->insertObject('#__lms_payments', $payment, 'id');						
						$payment_id = intval($JLMS_DB->insertid());
						$payment->balance = $balance;						
						
						$payment_status = $payment->status;
						
						$payment->tax_type	= isset($rows2[0]->tax_type) ? $rows2[0]->tax_type : $JLMS_CONFIG->get('default_tax_type', 1);
						$payment->tax		= isset($rows2[0]->tax) ? $rows2[0]->tax : $JLMS_CONFIG->get('default_tax', 0);
						$payment->recurrent_obj = $recurrent_obj;						
						
						$coupon = JLMS_DISCOUNTS::getCouponByCode( $coupon_code );					
												
						if( $coupon ) {						 
							$coupon_usage_statistic = new stdClass();
							$coupon_usage_statistic->id = null;
							$coupon_usage_statistic->coupon_code = $coupon->code;
							$coupon_usage_statistic->coupon_id = $coupon->id;
							$coupon_usage_statistic->payment_id = $payment_id;
							$coupon_usage_statistic->user_id = $my->id;
							$coupon_usage_statistic->date = gmdate('Y-m-d H:i:s');						
							
							$JLMS_DB->InsertObject('#__lms_disc_c_usage_stats', $coupon_usage_statistic, 'id');
						}
						
						$payment_info = new stdClass();
						$payment_info->payment_id = $payment_id;
						$payment_info->user_ip = JLMS_fetch_user_ip();
						$payment_info->user_alt_ip = JLMS_fetch_alt_user_ip();
						$query = "CREATE TABLE IF NOT EXISTS `#__lms_payment_info` ( `payment_id` INT NOT NULL, `user_ip` CHAR( 15 ) NOT NULL, `user_alt_ip` CHAR( 15 ) NOT NULL, PRIMARY KEY ( `payment_id` ) );";
						$JLMS_DB->setQuery($query);
						$JLMS_DB->query();
						$JLMS_DB->InsertObject('#__lms_payment_info', $payment_info);
						
						$payment->price = $payment->amount - $payment->tax_amount;
						$payment->sub_name = '';
						if (count($new_subs) == 1 && isset($new_subs[0]->sub_name)) {
							$payment->sub_name = $new_subs[0]->sub_name;
						}
	
						foreach ($course_subs as $course_sub) {
							$query = "INSERT INTO `#__lms_payment_items` (payment_id, item_id, item_type) VALUES ($payment_id, $course_sub, 0)";
							$JLMS_DB->setQuery($query);
							$JLMS_DB->query();
						}
							
						foreach ($conf_subs as $conf_sub) {
							$query = "INSERT INTO `#__lms_payment_items` (payment_id, item_id, item_type) VALUES ($payment_id, $conf_sub, 1)";
							$JLMS_DB->setQuery($query);
							$JLMS_DB->query();
						}
						
						$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
						$_JLMS_PLUGINS->loadBotGroup('system');
						$plugin_result_array = $_JLMS_PLUGINS->trigger('OnAfterUpdatePayment', array($payment_id, $payment_status));

						$payment->subscriptions = $current_cart;
						$payment->course_subscriptions = $course_subs;
						$payment->conf_subscriptions = $conf_subs;

						$params_proc = new mosParameters( $processor->params );
						if ($JLMS_CONFIG->get('jlms_subscr_status_email',false) && ($params_proc->get('subscr_status') == 1) && $amount > 0 ) {
							// print invoice only if amount > 0 .... don't print invoices fro free enrolls.
							// invoice on pending;
							// TODO: move this code to another file (i.e. 'classes/jlms_pdf_invoice.php') and make it universal for sending invoices
							$print_row = new stdClass();
							$query = "SELECT * FROM #__lms_subscriptions_config";
							$JLMS_DB->setQuery($query);
							$rowz = $JLMS_DB->loadObjectList();
							if(count($rowz)) {
								$print_row = $rowz[0];
							}
							if(!isset($print_row->mail_subj)){
								$print_row->mail_subj = 'Invoice for your order';
							}
							if(!isset($print_row->mail_body)){
								$print_row->mail_body = "Hello. Check the attached file to see detailed information about your purchase.";
							}
							if(!isset($print_row->thanks_text)){
								$print_row->thanks_text = "THANK YOU FOR YOUR BUSINESS";
							}
							if(!isset($print_row->site_name)){
								$print_row->site_name = str_replace("http://",'',$JLMS_CONFIG->get('live_site'));
							}
							if(!isset($print_row->site_descr)){
								$app = JFactory::getApplication();
								$print_row->site_descr = $app->getCfg('sitename');
							}
							if(!isset($print_row->comp_descr)){ $print_row->comp_descr = ''; }
							if(!isset($print_row->comments)){ $print_row->comments = ''; }
							if(!isset($print_row->invoice_descr)){ $print_row->invoice_descr = ''; }
							$print_row->invoice_number = $payment_id;

								$o_vars = get_object_vars($print_row);
								foreach ($o_vars as $ov => $ovv) {
									$print_row->$ov = str_replace('{order_id}', $payment_id, $print_row->$ov);
									$print_row->$ov = str_replace('{ORDER_ID}', $payment_id, $print_row->$ov);
									$total = number_format($payment->price + $payment->tax_amount, 2, '.', '').''.$JLMS_CONFIG->get('jlms_cur_code');
									$print_row->$ov = str_replace('{TOTAL}', $total, $print_row->$ov);
								}
								require_once(_JOOMLMS_FRONT_HOME . '/includes/classes/lms.cb_join.php');
								$custom_invoice_fields = $JLMS_CONFIG->get('custom_invoice_fields', array());
								if (!empty($custom_invoice_fields)) {
									foreach ($custom_invoice_fields as $cif) {
										$fname = $cif->var_name;
										$print_row->{$fname} = JLMSCBJoin::getASSOC($cif->profile_var);
									}
								} else {
									$def_cb_templ = 'lms_cb_company';
									$print_row->company = JLMSCBJoin::getASSOC($def_cb_templ);
									$def_cb_templ = 'lms_cb_address';
									$print_row->address = JLMSCBJoin::getASSOC($def_cb_templ);
									$def_cb_templ = 'lms_cb_city';
									$print_row->city = JLMSCBJoin::getASSOC($def_cb_templ);
									$def_cb_templ = 'lms_cb_state';
									$print_row->state = JLMSCBJoin::getASSOC($def_cb_templ);
									$def_cb_templ = 'lms_cb_pcode';
									$print_row->pcode = JLMSCBJoin::getASSOC($def_cb_templ);
									$def_cb_templ = 'lms_cb_phone';
									$print_row->phone = JLMSCBJoin::getASSOC($def_cb_templ);
								}
								$print_row->tax_amount = $tax_amount;
								$print_row->quantity = 1;
								$print_row->name = $my->name;
								$print_row->price = $payment->price;
								$print_row->date = substr($payment->date, 0, 10);
								$print_row->shipping = 0;
								$print_sub_name = $payment->sub_name;
								if (!$payment->sub_name && isset($payment->subscriptions) && count($payment->subscriptions) && ( strpos($params_proc->get( 'item_name'), '[sub]') !== false || strpos($params_proc->get( 'item_name'), '[SUB]') !== false) ) {
									$subscr_ids = implode(',', $payment->subscriptions);
									$query = "SELECT id, sub_name FROM #__lms_subscriptions WHERE id IN ($subscr_ids)";
									$JLMS_DB->SetQuery($query);
									$subs_names_list_db = $JLMS_DB->LoadObjectList();
									//create item name from list of all subscriptions separated by comma, ordered as in cart
									$subs_names_list = array();
									foreach ($payment->subscriptions as $cart_sub_id) {
										foreach ($subs_names_list_db as $subname_item) {
											if ($subname_item->id == $cart_sub_id) {
												$subs_names_list[] = $subname_item->sub_name;
												break;
											}
										}
									}
									$print_sub_name = implode(', ', $subs_names_list);
								}
								$print_row->description = $params_proc->get( 'item_name');
								if ( strpos($print_row->description, '[sub]') !== false || strpos($print_row->description, '[SUB]') !== false) {
									//compare and repalce both uppercase and lowercase strings (stri_repalce not used for PHP4 compat)
									$print_row->description = str_replace('[sub]', $print_sub_name, $print_row->description);
									$print_row->description = str_replace('[SUB]', $print_sub_name, $print_row->description);
								}

								$print_row->payment_details = array();
								$new_course_subs = array();
								foreach ($course_subs as $course_sub) {
									if (intval($course_sub)) {
										$new_course_subs[] = intval($course_sub);
									}
								}
								if (!empty($new_course_subs)) {
									$query = "SELECT id, sub_name, price, account_type, discount FROM #__lms_subscriptions WHERE id IN (".implode(',',$new_course_subs).")";
									$JLMS_DB->SetQuery($query);
									$course_subs_full = $JLMS_DB->LoadObjectList();
									if (count($course_subs_full)) {
										foreach ($new_course_subs as $course_sub) {
											$new_item = new stdClass();
											$new_item->quantity = 1;
											$new_item->name = '';
											$new_item->unit_price = 0;
											$new_item->price = 0;
											$new_item->id = $course_sub;
											foreach ($course_subs_full as $course_sub_full) {
												if ($course_sub_full->id == $course_sub) {
													$new_item->name = $course_sub_full->sub_name;
													$new_item->price = $course_sub_full->price;
													if ($course_sub_full->account_type == 5) {
														$new_item->price = $course_sub_full->price*(1 - ($course_sub_full->discount/100));
													}
													$new_item->unit_price = $new_item->price;
													$pd_i = 0;
													while ($pd_i < count($print_row->payment_details)) {
														if ($print_row->payment_details[$pd_i]->id == $course_sub) {
															break;
														}
														$pd_i ++;
													}
													if (isset($print_row->payment_details[$pd_i]->id) && $print_row->payment_details[$pd_i]->id == $course_sub) {
														$print_row->payment_details[$pd_i]->quantity ++;
													} else {
														$print_row->payment_details[] = $new_item;
													}
													break;
												}
											}
										}
									}
								}

								$print_row->payment_details2 = array();
								if (!empty($conf_subs)) {
									$query = "SELECT id, sub_name, price FROM #__lmsce_conf_subs WHERE id IN (".implode(',',$conf_subs).")";
									$JLMS_DB->SetQuery($query);
									$conf_subs_full = $JLMS_DB->LoadObjectList();
									if (count($conf_subs_full)) {
										foreach ($conf_subs as $conf_sub) {
											$new_item = new stdClass();
											$new_item->quantity = 1;
											$new_item->name = '';
											$new_item->unit_price = 0;
											$new_item->price = 0;
											$new_item->id = $conf_sub;
											foreach ($conf_subs_full as $conf_sub_full) {
												if ($conf_sub_full->id == $conf_sub) {
													$new_item->name = $conf_sub_full->sub_name;
													$new_item->unit_price = $conf_sub_full->price;
													$new_item->price = $conf_sub_full->price;
													$pd_i = 0;
													while ($pd_i < count($print_row->payment_details2)) {
														if ($print_row->payment_details2[$pd_i]->id == $conf_sub) {
															break;
														}
														$pd_i ++;
													}
													if (isset($print_row->payment_details2[$pd_i]->id) && $print_row->payment_details2[$pd_i]->id == $conf_sub) {
														$print_row->payment_details2[$pd_i]->quantity ++;
													} else {
														$print_row->payment_details2[] = $new_item;
													}
													break;
												}
											}
										}
									}
								}

								require_once(_JOOMLMS_FRONT_HOME.'/includes/classes/lms.pdf_invoice.php');
	
								$file_path = $JLMS_CONFIG->get('jlms_subscr_invoice_path');//$JLMS_DB->loadResult();
								if ($file_path) {
									$filenamez = md5(time().$my->id).".pdf";
									$file_path .= "/".$filenamez;
									$my_pdf = new JLMS_PDF_invoice();
									$my_pdf->makeInvoicePDF($print_row, $file_path);
									/*$query = "UPDATE #__lms_subscriptions_config SET invoice_number = ".($print_row->invoice_number+1);
									$JLMS_DB->setQuery( $query );
									$JLMS_DB->query();*/
									$query = "INSERT INTO #__lms_subs_invoice(subid,filename) VALUES('".$payment_id."','".$filenamez."')";
									$JLMS_DB->setQuery( $query );
									$JLMS_DB->query();
									require_once(_JOOMLMS_FRONT_HOME.'/joomla_lms.mailbox.php');
									$subject = stripslashes($print_row->mail_subj);
									$body = stripslashes($print_row->mail_body);
									JLMS_mosMail( '', '', $my->email, $subject, $body, 0, NULL, NULL, $file_path, 'invoice.pdf' );
								}
							}
						} else {
							$payment->amount = $amount;
							$payment->balance = $balance;
							$payment->tax_type	= 0;
							$payment->tax		= 0;
							$payment->recurrent_obj = 0;
						}
						
						if( $recurrent_obj ) 
						{											
							$query = "INSERT INTO #__lms_payments_checksum VALUES( '".$payment_id."', '".$recurrent_obj->price1->get('a')."', '".$recurrent_obj->price2->get('a')."', '".$recurrent_obj->price3->get('a')."', '".$recurrent_obj->price1->get('p')."', '".$recurrent_obj->price2->get('p')."', '".$recurrent_obj->price3->get('p')."', '".$recurrent_obj->price3->get('srt')."' )";	
							$JLMS_DB->setQuery( $query );
							$JLMS_DB->query();	
						}
						
						if ( $amount > 0 || ( $recurrent_obj && ( $recurrent_obj->price1->a > 0 || $recurrent_obj->price2->a > 0 || $recurrent_obj->price3->a > 0 ))) {							
							// use selected payment processor checkout
							if (file_exists(_JOOMLMS_FRONT_HOME.'/includes/processors/'.$processor->filename.'.php')) {
								require_once(_JOOMLMS_FRONT_HOME.'/includes/processors/'.$processor->filename.'.php');

								$newClass = "JLMS_".$processor->filename;
								$newProcObj = new $newClass();

								if ($processor->filename == 'authorize_aim' || $processor->filename == 'paypal' || $processor->filename == '2checkout' || $processor->filename == 'worldpay') {
									//do nothing..... we will clear cookies in the processor code (if necessary... e.g. cart will not be cleaned if checkout failed)
								} else {
									if (method_exists($newProcObj, 'CleanCartOnCheckout')) {
										$newProcObj->CleanCartOnCheckout();
									} else {									
										// 'offline transfer' processor or any other custom payment processor
										JLMSCookie::setcookie('joomlalms_cart_contents', '', time()-24*60*60, '/'); // remove cart cookies
									}
								}			
										
																
								$newProcObj->show_checkout($option, $payment, $payment_id, $processor);
								$is_not_auth = false;
							} else {
								JLMSRedirect(sefRelToAbs("index.php?option=".$option."&Itemid=".$Itemid."&task=show_cart"), 'Payment method is not allowed');
							}
						} else {
							//enroll user for free ...
							JLMSCookie::setcookie('joomlalms_cart_contents', '', time()-24*60*60, '/'); // remove cart cookies

							$payment_date = gmdate('Y-m-d H:i:s'); //fix UTC
							
							$query = "SELECT status FROM `#__lms_payments` WHERE id = $payment_id";
							$JLMS_DB->setQuery($query);
							$prev_payment = $JLMS_DB->LoadResult();
							
							jlms_update_payment( $payment_id, '', 'Completed', $payment_date, 0 );							 
							
							if ( $prev_payment == 'Completed' ) {
							} else {								
								jlms_register_new_user( $payment_id );								
							}					
							
							JLMSRedirect(sefRelToAbs("index.php?option=".$option."&Itemid=".$Itemid));
						}
				}
			}
		}
	}
	}
	if ($is_not_auth) {
		JLMSRedirect(sefRelToAbs("index.php?option=".$option."&Itemid=".$Itemid."&task=show_cart"), _NOT_AUTH);
	}
}

function JLMS_CART_GetProcs( $is_recurrent = false ) {
	global $JLMS_DB;
	$query = "SELECT * FROM `#__lms_subscriptions_procs` WHERE published=1 ORDER BY ordering";
	$JLMS_DB->setQuery($query);
	$procs = $JLMS_DB->loadObjectList();
		
	if( $is_recurrent ) 
	{
		$res = array();
		foreach( $procs AS $proc ) 
		{
			if ( file_exists(_JOOMLMS_FRONT_HOME.'/includes/processors/'.$proc->filename.'.php') )
			{
					require_once(_JOOMLMS_FRONT_HOME.'/includes/processors/'.$proc->filename.'.php');
					$newClass = "JLMS_".$proc->filename;
					$newProcObj = new $newClass();
					
					if( method_exists( $newProcObj, 'validate_recurrent_subscription' ) ) {
						$res[] = $proc;
					}				
			}		
		}
	} else {
		$res = $procs;
	}
	
	return $res;
}

function JLMS_CART_GetUserInfo($user_id) {
	global $JLMS_CONFIG, $JLMS_DB;
	$user = null;
	$cb = $JLMS_CONFIG->get('is_cb_installed',0);
	if ($user_id){
		if ($cb){
			$cb_config = $JLMS_CONFIG->getByPrefix('jlms_cb_', array());
			if (!empty($cb_config)) {
				$cb_select = implode(',', $cb_config);
				$query = "SELECT fieldid, name FROM `#__comprofiler_fields` WHERE fieldid IN (".$cb_select.")";
				$JLMS_DB->setQuery($query);
				$cb_fs = $JLMS_DB->loadObjectList();
			} else {
				$cb_fs = array();
			}

			$cb_fields_a = array();
			$cb_fields_assoc = array();
			foreach ($cb_fs as $cbf) {
				$cb_fields_a[] = $cbf->name;
				$cb_fields_assoc[$cbf->fieldid] = $cbf->name;
			}
			$user_info = null;
			if (!empty($cb_fields_a)) {
				$cb_fields = implode(',',$cb_fields_a);

				$query = "SELECT firstname, lastname, ".$cb_fields." FROM `#__comprofiler` WHERE user_id = ".$user_id." ";
				$JLMS_DB->setQuery($query);
				$user_info = $JLMS_DB->loadObject();
			}
			$user = new stdClass();
			$user->first_name = isset($user_info->firstname)?$user_info->firstname:'';
			$user->last_name = isset($user_info->lastname)?$user_info->lastname:'';
			$user->address = ($JLMS_CONFIG->get('jlms_cb_address', 0) && isset($cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_address', 0)])) ? $user_info->{$cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_address', 0)]} : '';
			$user->city = ($JLMS_CONFIG->get('jlms_cb_city', 0) && isset($cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_city', 0)])) ? $user_info->{$cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_city', 0)]} : '';
			$user->state = ($JLMS_CONFIG->get('jlms_cb_state', 0) && isset($cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_state', 0)])) ? $user_info->{$cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_state', 0)]} : '';
			$user->postal_code = ($JLMS_CONFIG->get('jlms_cb_postal_code', 0) && isset($cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_postal_code', 0)])) ? $user_info->{$cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_postal_code', 0)]} : '';
			$user->country = ($JLMS_CONFIG->get('jlms_cb_country', 0) && isset($cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_country', 0)])) ? $user_info->{$cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_country', 0)]} : '';
			$user->phone = ($JLMS_CONFIG->get('jlms_cb_phone', 0) && isset($cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_phone', 0)])) ? $user_info->{$cb_fields_assoc[$JLMS_CONFIG->get('jlms_cb_phone', 0)]} : '';
		}
	}
	return $user;
}

function JLMS_CART_showItems($option) {
	global $JLMS_DB, $Itemid, $my, $JLMS_CONFIG, $JLMS_SESSION;
	$dis_coupon_code = JRequest::getVar('dis_coupon_code');
	
	$current_cart_tmp_cookie = isset($_COOKIE['joomlalms_cart_contents']) ? $_COOKIE['joomlalms_cart_contents'] : '';
	$current_cart = array();
	$is_recurrent = false;
	
	if ($current_cart_tmp_cookie) {
		$current_cart_tmp_cookie = urldecode($current_cart_tmp_cookie);
		$current_cart_tmp = explode(',',$current_cart_tmp_cookie);
		$current_cart = JLMS_CART_GenerateCart($current_cart_tmp);
	}
	$lists = array();
	if (!empty($current_cart)) {

		$courses = JLMS_CART_GetCourses($my->id);
		$avail_courses = JLMS_CART_GetCoursesIDs($courses);
		$new_subs = JLMS_CART_GetSubs($avail_courses, $current_cart );//array();
		
		if( (JRequest::getVar('task') == 'apply_coupon_code') && $dis_coupon_code && !JLMS_DISCOUNTS::isDiscountCouponValid( $dis_coupon_code, $new_subs )) 
		{			
			$JLMS_SESSION->set( 'joomlalms_sys_message', _JLMS_COUPON_IS_NOT_AVAILABLE );			
		}
		
		foreach( $new_subs AS $n_sub ) 
		{
			if( $n_sub->account_type == '6' ) 
			{
				$is_recurrent = true; 
				
				break;
			}
			
		}
		
		if (!empty($new_subs)) {

			if ($my->id) {
				if ($JLMS_CONFIG->get('use_secure_checkout', false) && $JLMS_CONFIG->get('secure_url') && !$JLMS_CONFIG->get('under_ssl', false)) {
					JLMSRedirect($JLMS_CONFIG->get('secure_url')."/index.php?option=com_joomla_lms&Itemid=$Itemid&task=show_cart");
				}
			}

			$i =0;
			while ($i < count($new_subs)) {
				if ( isset($new_subs[$i]->allow_multiple) && $new_subs[$i]->allow_multiple ) {
					$new_subs[$i]->count_items = 0;
					foreach($current_cart as $cc) {
						if ($cc == $new_subs[$i]->id) {
							$new_subs[$i]->count_items ++;
						}
					}
				} else {
					$new_subs[$i]->allow_multiple = 0;
					$new_subs[$i]->count_items = 1;
				}
				$i ++;
			}

			if ($my->id) {
				$procs = JLMS_CART_GetProcs( $is_recurrent );
				$user = JLMS_CART_GetUserInfo($my->id);
				JLMS_CART_html::ShowCartCheckOut($option, $new_subs, $lists, $procs, $user );
			} else {
				JLMS_CART_html::ShowCart($option, $new_subs, $lists );
			}

		} else {
			$new_subs = array();
			JLMS_CART_html::ShowCart($option, $new_subs, $lists );
		}
	} else {
		$new_subs = array();
		JLMS_CART_html::ShowCart($option, $new_subs, $lists );
	}
}

function JLMS_CART_login($option){
	global $Itemid;
	if (JLMS_UserSessions::processLogin()) {
		JLMSredirect(sefRelToAbs("index.php?option=".$option."&amp;Itemid=".$Itemid."&amp;task=show_cart"), _JLMS_LOGIN_SUCCESS, false);
	}

	JLMSredirect(sefRelToAbs("index.php?option=".$option."&amp;Itemid=".$Itemid."&amp;task=show_cart"), _JLMS_LOGIN_INCORRECT, false);
}

function JLMS_CART_register( $option, $from_cb = 0 ) 
{
	global $Itemid;
	
	$msg = JLMS_UserSessions::register( $from_cb );
	
	JLMSredirect(sefRelToAbs("index.php?option=".$option."&Itemid=".$Itemid."&task=show_cart&after_reg=1"), $msg, false);
}

/**
 * IMPORTANT: the same function is declarated in the course_users.php
 * Implement your changes to the both functions! 
**/
function FLMS_access_global($cid, $course_id){
	global $JLMS_DB;
	$obj = new stdClass();

	$cids = implode(",", $cid);
	$query = "SELECT group_id FROM #__lms_users_in_global_groups WHERE user_id IN (".$cids.")";
	$JLMS_DB->setQuery($query);
	$in_group = JLMSDatabaseHelper::loadResultArray();

	if (count($in_group)) {
		$in_groups = implode(",", $in_group);
		$query = "SELECT * FROM #__lms_usergroups WHERE id IN (".$in_groups.") AND course_id = '0'";
		$JLMS_DB->setQuery($query);
		$detail_group = $JLMS_DB->loadObjectList();

		$x_start = 0;
		$x_end = 0;
		$out_start = '';
		$out_end = '';
		$out_start_publish = 0;
		$out_end_publish = 0;
		foreach($detail_group as $group) {
			if (isset($group->publish_start_date)) { //TODO: check if field is present in  db (for compatibility with old DBs - e.g. if there was an error during updating)
				if(!$group->publish_start_date){
					$out_start = '';
					$out_start_publish = 0;
					$x_start = strtotime($group->start_date);
					break;
				} else if($x_start == 0 && $group->publish_start_date && strtotime($group->start_date) > $x_start){
					$out_start = $group->start_date;
					$out_start_publish = 1;
					$x_start = strtotime($group->start_date);
				} else if($group->publish_start_date && strtotime($group->start_date) < $x_start) {
					$out_start = $group->start_date;
					$out_start_publish = 1;
					$x_start = strtotime($group->start_date);
				}
			}
		}
		foreach($detail_group as $group){
			if (isset($group->publish_end_date)) { // if fields is exists in the DB (temporary) - for compatibility without ant DB changes
				if(!$group->publish_end_date){
					$out_end = '';
					$out_end_publish = 0;
					$x_end = strtotime($group->end_date);
					break;
				} else if($x_end == 0 && $group->publish_end_date && strtotime($group->end_date) > $x_end){
					$out_end = $group->end_date;
					$out_end_publish = 1;
					$x_end = strtotime($group->end_date);
				} else if($group->publish_end_date && strtotime($group->end_date) > $x_end){
					$out_end = $group->end_date;
					$out_end_publish = 1;
					$x_end = strtotime($group->end_date);
				}
			}
		}
		$obj->start_date = $out_start;
		$obj->end_date = $out_end;
		$obj->publish_start = $out_start_publish;
		$obj->publish_end = $out_end_publish;
	} else {
		$obj->start_date = '';
		$obj->end_date = '';
		$obj->publish_start = 0;
		$obj->publish_end = 0;
	}

	return $obj;
}

function JLMS_CART_course_subscribe( $id, $option ){
	$_JLMS_PLUGINS = & JLMSFactory::getPlugins();
	global $my, $JLMS_ACL, $JLMS_DB, $Itemid, $JLMS_CONFIG, $JLMS_SESSION;
	$query = "SELECT * FROM `#__lms_courses` WHERE id = '$id' AND self_reg != '0'";
        
	$JLMS_DB->setQuery($query);
	$course = $JLMS_DB->loadObject();
	if ( is_object($course) && isset($course->gid)) {
		$can_enroll = JLMS_checkCourseGID($my->id, $course->gid);
	} else {
		$can_enroll = false;
	}
	if ($can_enroll && $my->id){ /* 03.Sept.2007 - (DEN) - $my->id - na vsyakii sluchai ;) */
		if ($course->paid == 0 && $course->self_reg == 1){
			$do_add = false;
			if ($JLMS_CONFIG->get('license_lms_users')) {
				$query = "SELECT count(distinct user_id) FROM #__lms_users_in_groups";
				$JLMS_DB->SetQuery( $query );
				$total_students = $JLMS_DB->LoadResult();
				if (intval($total_students) < intval($JLMS_CONFIG->get('license_lms_users'))) {
					$do_add = true;
				}
				if (!$do_add) {
					$query = "SELECT count(*) FROM `#__lms_users_in_groups` WHERE user_id = '".$my->id."'";
					$JLMS_DB->SetQuery( $query );
					if ($JLMS_DB->LoadResult()) {
						$do_add = true;
					}
				}
			} else {
				$do_add = true;
			}
			if ($do_add) {
				$user_info = new stdClass();
				$user_info->user_id = intval($my->id);
				$user_info->group_id = 0; //??? or what?
				$user_info->course_id = intval($id);

				$course_info = new stdClass();
				$course_info->course_id = $id;
				$params = new JLMSParameters($course->params);
				$course_info->max_attendees = $params->get('max_attendees', 0);

				$_JLMS_PLUGINS->loadBotGroup('course');
				$plugin_result_array = $_JLMS_PLUGINS->trigger('onCourseJoinAttempt', array($user_info, $course_info));

				$allow_join = true;
				foreach ($plugin_result_array as $result) {
					if (!$result) $allow_join = false;
				}

				if ($allow_join) {
					if($JLMS_CONFIG->get('use_global_groups',1)){
						//FLMS mod
						$cid = array();
						$cid[] = intval($my->id);
						$access_pediod_global = FLMS_access_global($cid, $id);
						
						$query = "SELECT COUNT(*)"
						. "\n FROM #__lms_users_in_groups"
						. "\n WHERE 1"
						. "\n AND course_id = '".intval($id)."'"
						. "\n AND user_id = '".intval($my->id)."'"
						;
						$JLMS_DB->setQuery($query);
						if($JLMS_DB->loadResult()){
							$query = "UPDATE #__lms_users_in_groups"
							. "\n SET"
							. "\n publish_start = '".$access_pediod_global->publish_start."'"
							. "\n, start_date = '".$access_pediod_global->start_date."'"
							. "\n, publish_end = '".$access_pediod_global->publish_end."'"
							. "\n, end_date = '".$access_pediod_global->end_date."'"
							. "\n, enrol_time = '".JLMS_gmdate()."'"
							. "\n WHERE 1"
							. "\n AND course_id = '".intval($id)."'"
							. "\n AND user_id = '".intval($my->id)."'"
							;
						} else {
							$query = "INSERT INTO `#__lms_users_in_groups`"
							."\n ( course_id, user_id, publish_start, start_date, publish_end, end_date, enrol_time )"
							. "\n VALUES"
							. "\n (".intval($id).",".intval($my->id).",".$access_pediod_global->publish_start.",'".$access_pediod_global->start_date."',".$access_pediod_global->publish_end.",'".$access_pediod_global->end_date."', '".JLMS_gmdate()."')"
							;
						}
					} else {
						$query = "SELECT COUNT(*)"
						. "\n FROM #__lms_users_in_groups"
						. "\n WHERE 1"
						. "\n AND course_id = '".intval($id)."'"
						. "\n AND user_id = '".intval($my->id)."'"
						;
						$JLMS_DB->setQuery($query);
						if($JLMS_DB->loadResult()){
							$query = "UPDATE #__lms_users_in_groups"
							. "\n SET"
							. "\n enrol_time = '".JLMS_gmdate()."'"
							. "\n WHERE 1"
							. "\n AND course_id = '".intval($id)."'"
							. "\n AND user_id = '".intval($my->id)."'"
							;
						} else {
							$query = "INSERT INTO `#__lms_users_in_groups`"
							. "\n ( course_id, user_id, enrol_time )"
							. "\n VALUES"
							. "\n (".intval($id).", ".intval($my->id).", '".JLMS_gmdate()."')";
						}
					}
					$JLMS_DB->setQuery($query);					
					if( $JLMS_DB->query() ) 
					{
						if ($JLMS_DB->geterrormsg()) {						 
							$msg = 'ERROR. Please contact site Administator.';
						} else {
							$msg = str_replace('#COURSENAME#', $course->course_name, _JLMS_CONGRATULATIONS);
						}

						$course = new stdClass();
						$course->course_alias = '';
						$course->course_name = '';			

						$query = "SELECT course_name, name_alias FROM #__lms_courses WHERE id = '".$id."'";
						$JLMS_DB->setQuery( $query );
						$course = $JLMS_DB->loadObject();			

						$e_params['user_id'] = $my->id;
						$e_params['course_id'] = $id;					
						$e_params['markers']['{email}'] = $my->email;	
						$e_params['markers']['{name}'] = $my->name;										
						$e_params['markers']['{username}'] = $my->username;
						$e_params['markers']['{coursename}'] = $course->course_name;//( $course->course_alias )?$course->course_alias:$course->course_name;																
						$e_params['markers']['{courselink}'] = JLMSEmailRoute("index.php?option=com_joomla_lms&Itemid=$Itemid&task=details_course&id=$id");
						$e_params['markers']['{courselink}'] = '<a href="'.$e_params['markers']['{courselink}'].'">'.$e_params['markers']['{courselink}'].'</a>';
						$e_params['action_name'] = 'OnSelfEnrolmentIntoFreeCourse';					
						

						$_JLMS_PLUGINS->loadBotGroup('emails');
						$plugin_result_array = $_JLMS_PLUGINS->trigger('OnSelfEnrolmentIntoFreeCourse', array (& $e_params));				
					}			

					$_JLMS_PLUGINS->loadBotGroup('user');
					$_JLMS_PLUGINS->trigger('onCourseJoin', array($user_info));
				} else {
					$msg = _JLMS_ADDED_TO_QUEUE;
				}

				$JLMS_SESSION->set('joomlalms_sys_message',$msg);
				$JLMS_SESSION->set('joomlalms_just_joined',1);
				JLMSRedirect (sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$id"));
			} else {

				$msg = 'Sorry user limit is exceeded, please contact your LMS administrator.';
				$JLMS_SESSION->set('joomlalms_sys_message',$msg);

				JLMSRedirect (sefRelToAbs("index.php?option=$option&amp;task=courses&amp;Itemid=$Itemid"));
			}
		}
		else{
			JLMSRedirect (sefRelToAbs("index.php?option=$option&amp;task=courses&amp;Itemid=$Itemid"));
		}
	}else{
		JLMSRedirect (sefRelToAbs("index.php?option=$option&amp;task=courses&amp;Itemid=$Itemid"));
	}
}
?>