<?php
/**
* joomla_lms.mailbox.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );
require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.mailbox.html.php");

$task 	= mosGetParam( $_REQUEST, 'task', '' );
if ($task == 'mailbox_reply' || $task == 'mailbox_new' || $task == 'mailbox' || $task == 'mail_sendbox' || $task == 'mail_send' || $task == 'mail_view' ) {
	global $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id',0);
	$pathway = array();
	$pathway[] = array('name' => _JLMS_PATHWAY_HOME, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"), 'is_home' => true);
	$pathway[] = array('name' => $JLMS_CONFIG->get('course_name'), 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$course_id"), 'is_course' => true);
	$pathway[] = array('name' => _JLMS_TOOLBAR_MAILBOX, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=mailbox&amp;id=$course_id"));
	JLMSAppendPathWay($pathway);
	JLMS_ShowHeading();
}

$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) ); 
$cid = mosGetParam( $_POST, 'cid', array(0) );
if (!is_array( $cid )) { $cid = array(0); }

switch ($task) {
	case 'mailbox_reply':	JLMS_showMailbox( $id, $option, $cid[0] ); break;
	case 'mailbox_new':		JLMS_showMailbox( $id, $option, 0 ); break;
	case 'mailbox':			JLMS_showInbox( $option ); break;
	case 'mail_sendbox':	JLMS_showOutbox( $option ); break;
	case 'mail_send':		JLMS_MailSend( $course_id, $option); break;
	case 'mail_view':		JLMS_viewmail($course_id, $option); break;
	case 'mfile_load':		JLMS_loadfile($option);	break;
	case 'mail_delete':		JLMS_maildelete($cid, $option, $course_id);	break;
	case 'mk_read':			JLMS_mk_read(1, $option, $course_id);	break;
	case 'mk_unread':		JLMS_mk_read(0, $option, $course_id);	break;
}

function JLMS_getMailboxCourseStudentsList($course_id, $group_id = 0, $subgroup_id = 0) {
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$db = JFactory::getDbo();
	//(Max): global groups
	if ($JLMS_CONFIG->get('use_global_groups', 1)){
		$query = "SELECT DISTINCT a.id, a.name, a.username, a.email "
//		. ($group_id ? "\n b.ug_name" : "\n '' as ug_name")
		. "\n FROM #__users as a, #__lms_users_in_groups as c"
		. ($group_id ? "\n , #__lms_users_in_global_groups as b" : '')
		. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."'"
		. ($group_id ? "\n AND a.id = b.user_id AND b.group_id = '".$group_id."'" : '')
		. ($subgroup_id ? "\n AND b.subgroup1_id = '".$subgroup_id."'" : '')
		. "\n ORDER BY a.username"
		;
	} else {
		$query = "SELECT DISTINCT a.id, a.name, a.username, a.email, b.ug_name"
		. "\n FROM #__users as a, #__lms_users_in_groups as c LEFT JOIN #__lms_usergroups as b ON c.group_id = b.id AND b.course_id = '".$course_id."'"
		. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."'"
		. ($group_id ? ("\n AND c.group_id = '".$group_id."'") : '' )
		. "\n ORDER BY b.ug_name, a.username";
	}
	$db->SetQuery( $query );
	$users = $db->LoadObjectList();
	
	$i = 0;
	while ($i < count($users)) {		
		$users[$i]->username = $users[$i]->name . ' ('.$users[$i]->username.')';
		$i ++;
	}	
	return $users;
}

function JLMS_getMailboxCourseStudentsList2($course_id, $group_id = 0) {
	global $JLMS_DB, $JLMS_CONFIG;
	if ($group_id) {
		$query = "SELECT DISTINCT a.id, a.name, a.username, a.email, '' as ug_name"
		. "\n FROM #__users as a, #__lms_users_in_groups as c, #__lms_users_in_global_groups as d"
		. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."' AND a.id = d.user_id"
		. ($group_id ? ("\n AND d.group_id = '".$group_id."'") : '' )
		. "\n ORDER BY a.username";
	} else {
		$query = "SELECT DISTINCT a.id, a.name, a.username, a.email, b.ug_name"
		. "\n FROM #__lms_users_in_groups as c, #__users as a"
		. "\n LEFT JOIN #__lms_users_in_global_groups as d ON a.id = d.user_id"
		. "\n LEFT JOIN #__lms_usergroups as b ON d.group_id = b.id"
		. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."'"
		. "\n ORDER BY b.ug_name, a.username";		
	}
	$JLMS_DB->SetQuery( $query );
	$users = $JLMS_DB->LoadObjectList();
		
	if (!$group_id) {
		$processed_users = array();
		$users2 = array();
		$i = 0;
		while ($i < count($users)) {
			if (!in_array($users[$i]->id,$processed_users)) {
				$users2[] = $users[$i];
				$processed_users[] = $users[$i]->id;
			}
			$i ++;	
		}
		$i = 0;
		while ($i < count($users2)) {
			$users2[$i]->username = $users2[$i]->username . ' ('.$users2[$i]->name.')';
			$i ++;
		}
		return $users2;
	} else {
		$i = 0;
		while ($i < count($users)) 
		{
			$users[$i]->username = $users[$i]->username . ' ('.$users[$i]->name.')';			
			$i ++;
		}
		return $users;
	}
}

function JLMS_showInbox($option)
{
	global $my, $JLMS_DB, $Itemid, $JLMS_SESSION, $JLMS_CONFIG;
	$course_id = intval(mosGetParam($_REQUEST, 'id', 0));
	$limit		= intval( mosGetParam( $_REQUEST, 'limit', $JLMS_SESSION->get('list_limit',$JLMS_CONFIG->get('list_limit')) ) );
	$JLMS_SESSION->set('list_limit', $limit);
	$limitstart = intval( mosGetParam( $_REQUEST, 'limitstart', 0 ) );
	
	$query = "SELECT COUNT(*) FROM #__lms_messages as m, #__lms_messages_to as mt, #__users as u WHERE m.id = mt.id AND u.id = m.sender_id  AND mt.user_id = ".$my->id." AND mt.del='0' ORDER by m.data";
	$JLMS_DB->setQuery($query);
	$total = $JLMS_DB->loadResult();
	$query = "SELECT COUNT(*) FROM #__lms_messages as m, #__lms_messages_to as mt, #__users as u WHERE m.id = mt.id AND u.id = m.sender_id  AND mt.user_id = ".$my->id." AND mt.del='0' AND mt.is_read = 0 ORDER by m.data";
	$JLMS_DB->setQuery($query);
	$unread = $JLMS_DB->loadResult();
	require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
	$pageNav = new JLMSPageNav( $total, $limitstart, $limit );
	
	$query = "SELECT m.*,mt.is_read,u.username"
	. "\n FROM #__lms_messages as m, #__lms_messages_to as mt, #__users as u"
	. "\n WHERE m.id = mt.id AND u.id = m.sender_id AND mt.user_id = ".$my->id." AND mt.del='0'"
	. "\n ORDER by m.data desc"
	. "\n LIMIT $pageNav->limitstart,$pageNav->limit";
	$JLMS_DB->setQuery($query);
	$rows = $JLMS_DB->loadObjectList();
	JLMS_mailbox_html::JLMS_showInbox($rows, $course_id, $pageNav, $option, $unread, $total);
}
function JLMS_showOutbox($option)
{
	global $my, $JLMS_DB, $Itemid, $JLMS_SESSION, $JLMS_CONFIG;
	$course_id = intval(mosGetParam($_REQUEST, 'id', 0));
	$limit		= intval( mosGetParam( $_REQUEST, 'limit', $JLMS_SESSION->get('list_limit', $JLMS_CONFIG->get('list_limit')) ) );
	$JLMS_SESSION->set('list_limit', $limit);
	$limitstart = intval( mosGetParam( $_REQUEST, 'limitstart', 0 ) );

	$query = "SELECT COUNT(*) FROM #__lms_messages as m WHERE m.sender_id = ".$my->id." AND m.del='0' ORDER by m.data";
	$JLMS_DB->setQuery($query);
	$total = $JLMS_DB->loadResult();
	require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.pagination.php");
	$pageNav = new JLMSPageNav( $total, $limitstart, $limit );
	
	$query = "SELECT m.* FROM #__lms_messages as m WHERE m.sender_id = ".$my->id." AND m.del='0' ORDER by m.data desc LIMIT $pageNav->limitstart,$pageNav->limit";
	$JLMS_DB->setQuery($query);
	$row = $JLMS_DB->loadObjectList();
	JLMS_mailbox_html::JLMS_showOutbox($row, $course_id, $pageNav, $option, $total);
}

function JLMS_showMailbox( $id, $option, $repl ) {

	global $my, $JLMS_DB, $Itemid, $JLMS_SESSION, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$assigned_groups_only = $JLMS_ACL->CheckPermissions('advanced', 'assigned_groups_only');
	$course_id = intval(mosGetParam($_REQUEST, 'id', 0));
	$users = array();
	
//	if ( (JLMS_GetUserType($my->id, $course_id) == 1 || JLMS_GetUserType($my->id, $course_id) == 2 )){
  	if($my->id){
		$limit		= intval( mosGetParam( $_REQUEST, 'limit', $JLMS_SESSION->get('list_limit', $JLMS_CONFIG->get('list_limit')) ) );
		$JLMS_SESSION->set('list_limit', $limit);
		//$filt_hw = intval( mosGetParam( $_GET, 'filt_hw', $JLMS_SESSION->get('filt_hw', 0) ) );
		//$JLMS_SESSION->set('filt_hw', $filt_hw);
		$filt_group = intval( mosGetParam( $_REQUEST, 'filt_group', $JLMS_SESSION->get('filt_group', 0) ) );
		$filter_stu = intval( mosGetParam( $_REQUEST, 'filter_stu', $JLMS_SESSION->get('filter_stu_h', 0) ) );
		if ($filt_group != $JLMS_SESSION->get('filt_group', 0)) {
			$filter_stu = 0;
		}
		if($filt_group){
			if ($JLMS_CONFIG->get('use_global_groups', 1)){
				$query = "SELECT a.ug_name as text FROM #__lms_usergroups as a, #__lms_users_in_global_groups as b"
				. "\n WHERE b.group_id = a.id AND a.id = '".$filt_group."'";
			}else{
				$query = "SELECT a.ug_name as text FROM #__lms_usergroups as a, #__lms_users_in_groups as b"
				. "\n WHERE a.course_id = '".$course_id."' AND b.group_id = a.id AND a.id = '".$filt_group."' ";
			}
			$JLMS_DB->SetQuery( $query );
			$filtGroupName = $JLMS_DB->loadResult();
			if( !$filtGroupName ) $filt_group = 0;
		}
		
		$JLMS_SESSION->set('filt_group', $filt_group);
		$JLMS_SESSION->set('filter_stu_h', $filter_stu);
		
		//members group
		$members = "'0'";
		$members_array = array();
		
		$users1 = array();
		
		if($assigned_groups_only) {
			if($JLMS_ACL->_role_type == 2 || $JLMS_ACL->_role_type == 4) {
				$groups_where_admin_manager = JLMS_ACL_HELPER::GetAssignedGroups($my->id, $id);			}
			elseif ( $JLMS_ACL->_role_type < 2 ) {
				$groups_where_admin_manager = JLMS_ACL_HELPER::GetUserGlobalGroup($my->id, $id);
			}
			
			if(count($groups_where_admin_manager) == 1) {
				$filt_group = $groups_where_admin_manager[0];
			}
			$groups_where_admin_manager = implode(',', $groups_where_admin_manager);
				if($groups_where_admin_manager != '') {
					$query = "SELECT user_id FROM #__lms_users_in_global_groups WHERE (group_id IN ($groups_where_admin_manager) OR subgroup1_id IN ($groups_where_admin_manager))"
						. ($filt_group ? ("\n AND group_id = '".$filt_group."'") : '')
//							. ($filt_subgroup ? ("\n AND subgroup1_id = '".$filt_subgroup."'") : '')
					;
					
					$JLMS_DB->setQuery($query);
					$members = JLMSDatabaseHelper::loadResultArray();
					
					$members_array = $members;
					
					$members = implode(',', $members);
					if($members == '')
						$members = "'0'";
				}		
		}
		
		$lists = array();		
		
		/*$JLMS_DB->setQuery("SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'mess_alearn' ");
		$mess_alearn = $JLMS_DB->loadResult();*/
		//changed by DEN
		$mess_alearn = $JLMS_CONFIG->get('mess_alearn', 0);
		
		$limitstart = intval( mosGetParam( $_REQUEST, 'limitstart', 0 ) );
		if ($course_id){
			if( !$filt_group ) 
			{
				$query = "SELECT a.* FROM #__users as a, #__lms_user_courses as c"
				. "\n WHERE a.id = c.user_id ".(($course_id)?"AND c.course_id = '".$course_id."'":'')." AND c.user_id <> '".$my->id."'"
				. "\n ORDER BY a.username";
				$JLMS_DB->SetQuery( $query );
				$users1 = $JLMS_DB->LoadObjectList();
							
				$i = 0;
				while ($i < count($users1)) {
					$users1[$i]->username = _JLMS_ROLE_TEACHER . ' - '.$users1[$i]->username . ' ('.$users1[$i]->name.')';
					$users1[$i]->is_teacher = true;
					$i ++;
				}
			}
			
			//if($filt_group) $users1 = array(0);
			$users = $users1;
			if($JLMS_ACL->CheckPermissions('mailbox', 'manage')) {
				if ($JLMS_CONFIG->get('use_global_groups', 1)){
					/*$query = "SELECT a.id, a.name, a.username, a.email, b.ug_name"
					. "\n FROM #__users as a, #__lms_users_in_global_groups as c LEFT JOIN #__lms_usergroups as b ON c.group_id = b.id "
					. "\n WHERE a.id = c.user_id"
					. ($filt_group ? ("\n AND c.group_id = '".$filt_group."'") : '' )
					. "\n ORDER BY b.ug_name, a.username";
					$JLMS_DB->SetQuery( $query );
					$users2 = $JLMS_DB->LoadObjectList();
					$i = 0;
					while ($i < count($users2)) {
						$users2[$i]->username = ($users2[$i]->ug_name?$users2[$i]->ug_name:'NO_GROUP').' - '.$users2[$i]->username . ' ('.$users2[$i]->name.')';
						$i ++;
					}
					$users = array_merge($users1, $users2);*/
					// (17.07.2008) - changed by DEN
					$users = array_merge($users1, JLMS_getMailboxCourseStudentsList2($course_id, $filt_group));
					
					if ( $assigned_groups_only ) {
						foreach( $users as $k=>$v) {
							if( !in_array($v->id,$members_array) ) {
								unset($users[$k]);					
							}
						}
						sort($users);
					}

					
				} else {					
					$users = array_merge($users1, JLMS_getMailboxCourseStudentsList($course_id, $filt_group));					
				}
			}
			
		} else {
			$query = "SELECT a.* FROM #__users as a, #__lms_users as b WHERE b.user_id = a.id AND a.id = '".$my->id."'";
			$JLMS_DB->setQuery($query);
			$cur_usr = $JLMS_DB->LoadObjectList();
			if(count($cur_usr)){
				if( !$filt_group ) 
				{
					$query = "SELECT a.* FROM #__users as a, #__lms_users as c"
					. "\n WHERE a.id = c.user_id  AND c.user_id <> '".$my->id."'"
					. "\n ORDER BY a.username";
					$JLMS_DB->SetQuery( $query );
					$users1 = $JLMS_DB->LoadObjectList();
					
					$i = 0;
					while ($i < count($users1)) {
						$users1[$i]->username = _JLMS_ROLE_TEACHER . ' - '.$users1[$i]->username . ' ('.$users1[$i]->name.')';
						$users1[$i]->is_teacher = true;
						$i ++;
					}
				}
				//if($filt_group) $users1 = array(0);
				if ($JLMS_CONFIG->get('use_global_groups', 1)){
					if ($filt_group) {
						$query = "SELECT distinct a.id, a.name, a.username, a.email, '' as ug_name"
						. "\n FROM #__users as a,  #__lms_users_in_global_groups as d"
						. "\n WHERE a.id = d.user_id"
						. ($filt_group ? ("\n AND d.group_id = '".$filt_group."'") : '' )
						. "\n ORDER BY a.username";
					} else {
						$query = "SELECT distinct a.id, a.name, a.username, a.email, b.ug_name"
						. "\n FROM #__users as a, #__lms_users_in_groups as c"
						. "\n LEFT JOIN #__lms_users_in_global_groups as d ON c.user_id = d.user_id"
						. "\n LEFT JOIN #__lms_usergroups as b ON d.group_id = b.id"
						. "\n WHERE a.id = c.user_id"
						. "\n ORDER BY b.ug_name, a.username";		
					}
					$JLMS_DB->SetQuery( $query );
					$users2 = $JLMS_DB->LoadObjectList();
				} else {
					$users2 = array();	
				}
				$i = 0;
				while ($i < count($users2)) {
					$users2[$i]->username = $users2[$i]->username . ' ('.$users2[$i]->name.')';
					$i ++;
				}
				//if($mess_alearn){
				if($filt_group){
					$users = array();
					if($JLMS_ACL->CheckPermissions('lms', 'create_course')){	
						$users = $users2;	
					}
				} else {
					$users = $users1;
					if($JLMS_ACL->CheckPermissions('lms', 'create_course')){
						$users = array_merge($users1, $users2);
					}
				}
				/*}else{
					$users = $users1;
				}*/

			} else {
				if( !$filt_group ) 
				{
					$query = "SELECT a.* FROM #__users as a, #__lms_users as c"
					. "\n WHERE a.id = c.user_id AND c.user_id <> '".$my->id."'"
					. "\n ORDER BY a.username";
					$JLMS_DB->SetQuery( $query );
					$users1 = $JLMS_DB->LoadObjectList();
					$i = 0;
					while ($i < count($users1)) {
						$users1[$i]->username = _JLMS_ROLE_TEACHER . ' - '.$users1[$i]->username . ' ('.$users1[$i]->name.')';
						$users1[$i]->is_teacher = true;
						$i ++;
					}
				}
				
				//if($filt_group) $users = array(0);
				if ($JLMS_CONFIG->get('use_global_groups', 1)){
					$query = "SELECT a.id, a.name, a.username, a.email, b.ug_name"
					. "\n FROM #__users as a, #__lms_users_in_global_groups as c LEFT JOIN #__lms_usergroups as b ON c.group_id = b.id "
					. "\n WHERE a.id = c.user_id"
					. ($filt_group ? ("\n AND c.group_id = '".$filt_group."'") : '' )
					. "\n ORDER BY b.ug_name, a.username";
					$JLMS_DB->SetQuery( $query );
					$users2 = $JLMS_DB->LoadObjectList();
				}else{	
					$users2 = array();	
				}
				$users = array();
				$i = 0;
				while ($i < count($users2)) {
					$users2[$i]->username = $users2[$i]->username . ' ('.$users2[$i]->name.')';
					$i ++;
				}
				
//				if($mess_alearn)
				if(!$filt_group){
					$users = $users1;
				}
				if($JLMS_ACL->CheckPermissions('mailbox', 'manage')){
					$users = array_merge($users1, $users2);
				}
			}
			
		}
		
		
		
		$g_items = array();
		$g_items[] = mosHTML::makeOption(0, _JLMS_MB_FILTER_ALL_GROUPS);
		
		if ($JLMS_CONFIG->get('use_global_groups', 1)){			
			$query = "SELECT distinct a.id as value, a.ug_name as text FROM #__lms_usergroups as a, #__lms_users_in_global_groups as b"
			. "\n WHERE b.group_id = a.id ORDER BY a.ug_name";
		}else{			
			$query = "SELECT distinct a.id as value, a.ug_name as text FROM #__lms_usergroups as a, #__lms_users_in_groups as b"
			. "\n WHERE a.course_id = '".$course_id."' AND b.group_id = a.id ORDER BY a.ug_name";
		}
		$JLMS_DB->SetQuery( $query );
		$groups = $JLMS_DB->LoadObjectList();
			
		$g_items = array_merge($g_items, $groups);
		/*
		$link = "index.php?option=$option&amp;Itemid=$Itemid&amp;task=mailbox&amp;course_id=$course_id&amp;id=$id";
		$link = $link ."&amp;filt_group=' + this.options[selectedIndex].value + '";
		*/
		$lists['filter2'] = mosHTML::selectList($g_items, 'filt_group', 'class="inputbox" style=\'width:100%\' size="1" onchange="submitbutton(\'mailbox_new\');"', 'value', 'text', $filt_group );


		$g_users = array();
		$lists['mailbox_users'] = '<select multiple="multiple" size="7" style="width: 100%; height: auto;" class="inputbox chzn-done" id="mailbox_users" name="mailbox_users[]">'."\n";
		
		if(!$filt_group)
		{
			$query = "SELECT pm_name as text, id as value FROM #__lms_messagelist";
			$JLMS_DB->setQuery($query);
			$sup_mail = $JLMS_DB->loadObjectList();
			if (count($sup_mail)) {				
				for($i=0;$i<count($sup_mail);$i++)
				{
					$g_users[$i+1]->value = "-".$sup_mail[$i]->value;
					$g_users[$i+1]->text = $sup_mail[$i]->text;
					$lists['mailbox_users'] .= '<option style="font-weight: bold;" value="mail_'.$sup_mail[$i]->value.'">- '.$sup_mail[$i]->text.' -</option>'."\n";
				}		
			}
		}		
		
		if(!$filt_group)
		{
			
		}	
		else 
		{
			if (($JLMS_ACL->CheckPermissions('mailbox', 'manage') && $course_id ) || $JLMS_ACL->CheckPermissions('lms', 'create_course')){
				if ($JLMS_CONFIG->get('use_global_groups', 1)){
					$query = "SELECT a.ug_name as text FROM #__lms_usergroups as a, #__lms_users_in_global_groups as b"
					. "\n WHERE b.group_id = a.id AND a.id = '".$filt_group."'";
				}else{
					$query = "SELECT a.ug_name as text FROM #__lms_usergroups as a, #__lms_users_in_groups as b"
					. "\n WHERE a.course_id = '".$course_id."' AND b.group_id = a.id AND a.id = '".$filt_group."' ";
				}
				$JLMS_DB->SetQuery( $query );
				//$g_users[] = mosHTML::makeOption(0, $JLMS_DB->loadResult());
				$any_contacts_in_group = false;
				foreach ($users as $uo) {
					if (isset($uo->is_teacher) && $uo->is_teacher) {
					} else {
						$any_contacts_in_group = true;
						break;
					}
				}
				if ($any_contacts_in_group) {
					//$lists['mailbox_users'] .= '<optgroup label="'.$JLMS_DB->loadResult().'">';
					foreach ($users as $uo) {
						if (isset($uo->is_teacher) && $uo->is_teacher) {
						} else {
							$lists['mailbox_users'] .= '<option value="'.$uo->id.'">'.$uo->username.'</option>'."\n";
						}
					}
					//$lists['mailbox_users'] .= '</optgroup>';
				}
			}
		}	
			
			
		$users_tun = array();
		$optgroup_html_code = '';
		
		if (count($users)) {	
			$add_optgroup = false;			
			foreach ($users as $uo) 
			{
				$ut = new stdClass();
				$ut->value = $uo->id;
				$ut->text = $uo->username;
				$users_tun[] = $ut;
				$do_add = true;
				if (!$filt_group) {
				} else {
					if (isset($uo->is_teacher) && $uo->is_teacher) {
					} else {
						//if filter by group is enabled -  learner is already added to the list
						$do_add = false;
					}
				}
				if ($do_add) 
				{					
					$add_optgroup = true;	
					if (isset($uo->is_teacher) && $uo->is_teacher) { 				
						$optgroup_html_code .= '<option value="teacher_'.$ut->value.'">'.$ut->text.'</option>'."\n";
					} else {
						$optgroup_html_code .= '<option value="'.$ut->value.'">'.$ut->text.'</option>'."\n";
					}
				}
			}
			if ($add_optgroup) {
				//$course_address_book_name = $course_id ? _JLMS_MB_COURSE_CONTACTS : _JLMS_MB_CONTACTS;
				//$lists['mailbox_users'] .= '<optgroup label="'.$JLMS_CONFIG->get('course_address_book_name', $course_address_book_name).'">';
				$lists['mailbox_users'] .= $optgroup_html_code;
				//$lists['mailbox_users'] .= '</optgroup>';
			}
		}
		
		if( isset($sup_mail[0]) || $optgroup_html_code ) 
		{
			if( $filt_group )
				$lists['mailbox_users'] .= '<option style="font-weight: bold;" value="group_'.$filt_group.'">'.$filtGroupName.' ('._JLMS_SB_ALL_USERS.')</option>'."\n";
			else
				$lists['mailbox_users'] .= '<option style="font-weight: bold;" value="all">'._JLMS_SB_ALL_USERS.'</option>'."\n";
		}

		$lists['mailbox_users'] .= '</select>';
		//$lists['mailbox_users'] = mosHTML::selectList($g_users, 'mailbox_users[]', 'class="inputbox" style=\'width:340px\' size="7" multiple="multiple" ', 'value', 'text' );		
		
		//-------reply function --------//
		if($repl && count($users)){
			$query = "SELECT m.*,mt.is_read,u.username FROM #__lms_messages as m, #__lms_messages_to as mt, #__users as u WHERE m.id = mt.id AND u.id = m.sender_id  AND mt.user_id = ".$my->id." AND m.id=".$repl;
			$JLMS_DB->setQuery($query);
			$repl_row = $JLMS_DB->loadObject();		
			$lists['repl'] = $repl_row;	
			
			if( $lists['repl'] ) 
			{
				foreach( $users AS $uo ) 
				{
					if( $uo->id == $lists['repl']->sender_id ) 
					{
						if (isset($uo->is_teacher) && $uo->is_teacher) { 				
							$_POST['recipients'][] = 'teacher_'.$uo->id;
						} else {
							$_POST['recipients'][] = $uo->id;
						}			
					}
				}
			
			}				
		}
		
		$lists['recipients'] = '<select class="inputbox chzn-done" multiple="multiple" name="recipients[]" id="recipients" style="width: 100%; height: auto;" size="7">'."\n";
									
		if(isset($_POST['recipients']) && count($_POST['recipients']))
		{				
			$rGroupIds = array();
			$rMailIds = array();
			$rUserIds = array();
			$rTeacherIds = array();	
			$rAll = false;			
			
			for($i=0;$i<count($_POST['recipients']);$i++)
			{								
				if( strpos($_POST['recipients'][$i], 'group') !== false ) {					
					$rGroupIds[] = str_replace('group_', '', $_POST['recipients'][$i] );	
				} else if( strpos($_POST['recipients'][$i], 'mail') !== false )	{
					$rMailIds[] = str_replace('mail_', '', $_POST['recipients'][$i] );	
				} else if( strpos($_POST['recipients'][$i], 'teacher') !== false )	{
					$rTeacherIds[] = str_replace('teacher_', '', $_POST['recipients'][$i] );	
				}else if( strpos($_POST['recipients'][$i], 'all') !== false )	{
					$rAll = true;	
				} else {
					$rUserIds[] = $_POST['recipients'][$i];
				}				
			}		
															
			$rMails = array();
			$rGroups = array();
			$rTeachers = array();
			$rUsers = array();
			
			if( !empty($rMailIds) ) 
			{
				$query = "SELECT id, pm_name FROM #__lms_messagelist WHERE id IN (".implode( ',', $rMailIds ).")";
				$JLMS_DB->SetQuery( $query );
				$rMails = $JLMS_DB->LoadObjectList();
			}
			if(!empty($rGroupIds))
			{
				$query = "SELECT distinct a.id, a.ug_name FROM #__lms_usergroups as a
							WHERE a.id IN (".implode(',', $rGroupIds).")  ORDER BY a.ug_name";
				$JLMS_DB->SetQuery( $query );
				$rGroups = $JLMS_DB->LoadObjectList();
			}		
							
			if( !empty($rTeacherIds) ) 
			{
				$query = "SELECT id, username, name FROM #__users WHERE id IN (".implode(',', $rTeacherIds).")";
				$JLMS_DB->SetQuery( $query );
				$rTeachers = $JLMS_DB->LoadObjectList();
			}			
						
			if(!empty($rUserIds))
			{											
				if($JLMS_CONFIG->get('use_global_groups', 0))
				{					
					$query = "SELECT a.id, a.name, a.username, a.email, b.ug_name"
					. "\n FROM #__users as a, #__lms_users_in_global_groups as c"
					. "\n LEFT JOIN #__lms_usergroups as b ON c.group_id = b.id AND b.course_id = '0'"
					. "\n WHERE a.id = c.user_id AND a.id IN (".implode(',', $rUserIds).")"					
					. "\n ORDER BY b.ug_name, a.username";
					$JLMS_DB->SetQuery( $query );
					$rUsers = $JLMS_DB->LoadObjectList('id');
				} else {
					$query = "SELECT a.id, a.name, a.username, a.email, b.ug_name"
					. "\n FROM #__users as a, #__lms_users_in_groups as c"
					. "\n LEFT JOIN #__lms_usergroups as b ON c.group_id = b.id AND b.course_id = '".$course_id."'"
					. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."' AND a.id IN (".implode(',', $rUserIds).")"
					. "\n ORDER BY b.ug_name, a.username";
					$JLMS_DB->SetQuery( $query );
					$rUsers = $JLMS_DB->LoadObjectList('id');
				}
							
				$noGroupsIds =  array_diff( $rUserIds, array_keys($rUsers)  );				
				
				if( !empty($noGroupsIds) ) 
				{
					$query = "SELECT id, username, name, email, '' AS ug_name FROM #__users WHERE id IN (".implode(',', $noGroupsIds).")";
					$JLMS_DB->SetQuery( $query );
					$noGroupsUsers = $JLMS_DB->LoadObjectList();
					
					$rUsers = array_merge($noGroupsUsers, $rUsers);
				}															
			}
													
			if( !empty($rMails) ) 
			{
				foreach( $rMails AS $rMail  ) 
				{							
					$lists['recipients'] .= '<option style="font-weight: bold;" value="mail_'.$rMail->id.'">- '.$rMail->pm_name.' -</option>'."\n";
				}
			}
			
			if( $rAll ) 
			{
				$lists['recipients'] .= '<option style="font-weight: bold;" value="all">- '._JLMS_MB_ALL_USRS.' -</option>'."\n";
			}				
			
			if( !empty($rGroups) ) 
			{
				foreach( $rGroups AS $rGroup  ) 
				{							
					$lists['recipients'] .= '<option style="font-weight: bold;" value="group_'.$rGroup->id.'">'.$rGroup->ug_name.' ('._JLMS_SB_ALL_USERS.')</option>'."\n";
				}
			}
						
			if( !empty($rTeachers) ) 
			{
				foreach( $rTeachers AS $rTeacher  ) 
				{						 							
					$lists['recipients'] .= '<option value="teacher_'.$rTeacher->id.'">'._JLMS_ROLE_TEACHER . ' - '.$rTeacher->username . ' ('.$rTeacher->name.')'.'</option>'."\n";
				}
			}
							
			if( !empty($rUsers) ) 
			{
				foreach( $rUsers AS $rUser  ) 
				{							
					$lists['recipients'] .= '<option value="'.$rUser->id.'">'.$rUser->username . ' ('.$rUser->name.')'.'</option>'."\n";
				}
			}						
					
		}			
		$lists['recipients'] .= '</select>';			

		JLMS_mailbox_html::mailbox_users( $mailbox_users, $option, $course_id, $lists, $filt_group );
	
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;id=$course_id") );
	}
}

function JLMS_sendMessage($course_id, $option){
	global $my, $JLMS_DB, $Itemid, $JLMS_SESSION, $JLMS_CONFIG;
	$sendto = array();
	$is_sended = 0;
	$file_id = 0;
	$jlms_subject = strval(mosGetParam($_REQUEST, 'jlms_subject', ''));
	$jlms_mailbox_letter = strval(mosGetParam($_REQUEST, 'jlms_mailbox_letter', '', 2));
	$m_course_id = intval(mosGetParam($_REQUEST, 'm_course_id', '', -1));
	
	$m_course_id = ($m_course_id != -1) ? $m_course_id : $course_id;
	
	$mess_alearn = $JLMS_CONFIG->get('mess_alearn', 0);
	
	$JLMS_ACL = JLMSFactory::getACL();		
	
	//(Max): fix for mail
	$jlms_mailbox_letter_external = $jlms_mailbox_letter;
	preg_match('#\<br(\s*)\/>\s*(-*)\s*\<br(\s*)\/>#', $jlms_mailbox_letter_external, $result);
	if(isset($result[2]) && strlen($result[2]) == 50){
		$jlms_mailbox_letter_external = preg_replace('#\<br(\s*)\/>\s*(-*)\s*\<br(\s*)\/>#', "\n".$result[2]."\n", $jlms_mailbox_letter_external);	
	}
	$jlms_mailbox_letter_external = preg_replace('#\<br(\s*)?\/?\>#', "\n", $jlms_mailbox_letter_external);
	$jlms_mailbox_letter_external = strip_tags($jlms_mailbox_letter_external);
	$jlms_mailbox_letter_external = str_replace(" -", "-", $jlms_mailbox_letter_external);
	$jlms_mailbox_letter_external = str_replace("- ", "-", $jlms_mailbox_letter_external);
	$jlms_mailbox_letter_external = str_replace("&gt;", ">", $jlms_mailbox_letter_external);
	$jlms_mailbox_letter_external = str_replace('\"', '"', $jlms_mailbox_letter_external);
	$jlms_mailbox_letter_external = str_replace("\'", "'", $jlms_mailbox_letter_external);
	
	if(isset($_FILES['jlms_attach_file']) && $_FILES['jlms_attach_file']['name'])
	{
		$file_id = JLMS_uploadFile( $course_id, 'jlms_attach_file');
	}
	$file_path = null;
	$filename = null;
	if($file_id)
	{
		$query = "SELECT * FROM #__lms_files WHERE id='".$file_id."'";
		$JLMS_DB->setQuery($query);
		$my_file = $JLMS_DB->loadObjectList();
		if (count($my_file))
		{
			$file_path = _JOOMLMS_DOC_FOLDER.'/'.$my_file[0]->file_srv_name;
			$filename = $my_file[0]->file_name;
		}
	}
	
	$rGroupIds = array();
	$rMailIds = array();
	$rUserIds = array();
	$rTeacherIds = array();	
	$rAll = false;			
	
	for($i=0;$i<count($_POST['recipients']);$i++)
	{								
		if( strpos($_POST['recipients'][$i], 'group') !== false ) {					
			$rGroupIds[] = str_replace('group_', '', $_POST['recipients'][$i] );	
		} else if( strpos($_POST['recipients'][$i], 'mail') !== false )	{
			$rMailIds[] = str_replace('mail_', '', $_POST['recipients'][$i] );	
		} else if( strpos($_POST['recipients'][$i], 'teacher') !== false )	{
			$rTeacherIds[] = str_replace('teacher_', '', $_POST['recipients'][$i] );	
		}else if( strpos($_POST['recipients'][$i], 'all') !== false )	{
			$rAll = true;	
		} else {
			$rUserIds[] = $_POST['recipients'][$i];
		}				
	}
	
	if( !empty($rUserIds) || !empty($rTeacherIds) )
	{
		$sendto = array_merge( $rUserIds ,$rTeacherIds );				
	}	
		
	$temp_usrs = array();
	
	if($rAll)
	{
		if($course_id){
			$query = "SELECT a.* FROM #__users as a, #__lms_user_courses as c"
			. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."' AND c.user_id <> '".$my->id."'"
			. "\n ORDER BY a.username";
			$JLMS_DB->SetQuery( $query );
			$temp_usrs = array_merge($temp_usrs,$JLMS_DB->LoadObjectList());
//								if($usertype == 1 || $mess_alearn)	{
			if($JLMS_ACL->CheckPermissions('mailbox', 'manage'))	{
				$temp_usrs = array_merge($temp_usrs,JLMS_getMailboxCourseStudentsList($course_id, 0));
			}
		}else{
			$query = "SELECT a.* FROM #__users as a, #__lms_users as c"
			. "\n WHERE a.id = c.user_id ".(($course_id)?"AND c.course_id = '".$course_id."'":'')." AND c.user_id <> '".$my->id."'"
			. "\n ORDER BY a.username";
			$JLMS_DB->SetQuery( $query );
			$users1 = $JLMS_DB->LoadObjectList();

			$i = 0;
			while ($i < count($users1)) {
				$users1[$i]->username = _JLMS_ROLE_TEACHER . ' - '.$users1[$i]->username . ' ('.$users1[$i]->name.')';
				$i ++;
			}
//								if($usertype == 1 || $mess_alearn)	{
			if($JLMS_ACL->CheckPermissions('mailbox', 'manage')){
				$query = "SELECT a.id, a.name, a.username, a.email, b.ug_name"
				. "\n FROM #__users as a, #__lms_users_in_global_groups as c LEFT JOIN #__lms_usergroups as b ON c.group_id = b.id "
				. "\n WHERE a.id = c.user_id"
				. ($filt_group ? ("\n AND c.group_id = '".$filt_group."'") : '' )
				. "\n ORDER BY b.ug_name, a.username";
				$JLMS_DB->SetQuery( $query );
				$users2 = $JLMS_DB->LoadObjectList();
			} else {
				$users2 = array();
			}
			/*$i = 0;
			while ($i < count($users2)) {
				$users2[$i]->username = ($users2[$i]->ug_name?$users2[$i]->ug_name:'NO_GROUP').' - '.$users2[$i]->username . ' ('.$users2[$i]->name.')';
				$i ++;
			}*/
			// (17.07.2008) commented by DEN
			$temp_usrs = array_merge($users1, $users2);
		}
		
		$is_sended = 1;
		$query = "SELECT * FROM #__lms_messagelist";
		$JLMS_DB->setQuery($query);
		$sup_mail = $JLMS_DB->loadObjectList();
		
		for($j=0;$j<count($sup_mail);$j++)
		{
			JLMS_mosMail('','',$sup_mail[$j]->pm_email,$jlms_subject,$jlms_mailbox_letter_external,0,null,null,$file_path,$filename);
		}	
	}

	if(  !empty($rGroupIds) )
	{
		for($i=0;$i<count($rGroupIds);$i++)
		{									
			if($course_id){
				
				if ($JLMS_CONFIG->get('use_global_groups', 1))
				{
//										if($usertype == 1 || $mess_alearn)	
					if($JLMS_ACL->CheckPermissions('mailbox', 'manage'))	
					{
						$temp_usrs = array_merge($temp_usrs,JLMS_getMailboxCourseStudentsList2($course_id, $rGroupIds[$i]));
					} else {
						$query = "SELECT a.* FROM #__users as a, #__lms_user_courses as c"
						. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."' AND c.user_id <> '".$my->id."'"
						. "\n ORDER BY a.username";
						$JLMS_DB->SetQuery( $query );
						$temp_usrs = array_merge($temp_usrs,$JLMS_DB->LoadObjectList());
					}	
				}else{
//										if($usertype == 1 || $mess_alearn)	
					if($JLMS_ACL->CheckPermissions('mailbox', 'manage'))	
					{
						$temp_usrs = array_merge($temp_usrs,JLMS_getMailboxCourseStudentsList($course_id, $rGroupIds[$i]));
					} else {
						$query = "SELECT a.* FROM #__users as a, #__lms_user_courses as c"
						. "\n WHERE a.id = c.user_id AND c.course_id = '".$course_id."' AND c.user_id <> '".$my->id."'"
						. "\n ORDER BY a.username";
						$JLMS_DB->SetQuery( $query );
						$temp_usrs = array_merge($temp_usrs,$JLMS_DB->LoadObjectList());
					}	
				}
			} else {	
//									if($usertype == 1 || $mess_alearn)	
				if($JLMS_ACL->CheckPermissions('mailbox', 'manage'))	
				{		
						$query = "SELECT a.id, a.name, a.username, a.email, b.ug_name"
						. "\n FROM #__users as a, #__lms_users_in_global_groups as c LEFT JOIN #__lms_usergroups as b ON c.group_id = b.id "
						. "\n WHERE a.id = c.user_id"
						. ($rGroupIds[$i] ? ("\n AND c.group_id = '".$rGroupIds[$i]."'") : '' )
						. "\n ORDER BY b.ug_name, a.username";
				} else {
						$query = "SELECT a.* FROM #__users as a, #__lms_users as c"
						. "\n WHERE a.id = c.user_id AND c.user_id <> '".$my->id."'"
						. "\n ORDER BY a.username";
				}	
				$JLMS_DB->SetQuery( $query );
				$users2 = $JLMS_DB->LoadObjectList();
				/*$i = 0;
				while ($i < count($users2)) {
					$users2[$i]->username = ($users2[$i]->ug_name?$users2[$i]->ug_name:'NO_GROUP').' - '.$users2[$i]->username . ' ('.$users2[$i]->name.')';
					$i ++;
				}*/
				// (17.07.2008) commented by DEN
				$temp_usrs = $users2;
			}				
		}
	}
				
	if( !empty($rMailIds) && !$is_sended)
	{
		for($i=0;$i<count($rMailIds);$i++)
		{
			$query = "SELECT * FROM #__lms_messagelist WHERE id=".intval(abs($rMailIds[$i]));
			$JLMS_DB->setQuery($query);
			$sup_mail = $JLMS_DB->loadObjectList();
			
			$query = "SELECT name, email FROM #__users WHERE id = '".$my->id."'";
			$JLMS_DB->setQuery($query);
			$from_user = $JLMS_DB->loadObject();
					
			for($j=0;$j<count($sup_mail);$j++)
			{
				JLMS_mosMail($from_user->email, $from_user->name, $sup_mail[$j]->pm_email,$jlms_subject,$jlms_mailbox_letter_external,0,null,null,$file_path,$filename);
			}
		}
	}

	$res_usr = array();	
	if (count($sendto))
	for($i=0;$i<count($sendto);$i++)
	{

		if(!in_array($sendto[$i],$res_usr))
		$res_usr[] = $sendto[$i];
	}
	if (count($temp_usrs))
	for($i=0;$i<count($temp_usrs);$i++)
	{		
		if(!in_array($temp_usrs[$i]->id,$res_usr))
		$res_usr[] = $temp_usrs[$i]->id;		
	}
		
	if(count($res_usr))
	{
        $query = "INSERT INTO #__lms_messages(id, sender_id, course_id, subject, data, message, file, del)";
        $query .= "\n VALUES('',".$JLMS_DB->quote($my->id).",".$JLMS_DB->quote($m_course_id).",".$JLMS_DB->quote($jlms_subject).",'".date("Y-m-d H:i:s")."',".$JLMS_DB->quote($jlms_mailbox_letter).",".$JLMS_DB->quote($file_id).",'0')";
        $JLMS_DB->setQuery($query);
        $JLMS_DB->query();
	}
	
	$query = "SELECT max(id) FROM #__lms_messages";
	$JLMS_DB->setQuery($query);
	$mail_id = $JLMS_DB->loadResult();
	$link  = $JLMS_CONFIG->get('live_site').'/index.php?option='.$option.'&amp;Itemid='.$Itemid.'&amp;task=mail_view&amp;id='.$course_id.'&amp;view_id='.$mail_id;
		
	$JLMS_DB->setQuery("SELECT lms_config_value FROM `#__lms_config` WHERE lms_config_var = 'mess_enotify' ");
	$mess_enotify = $JLMS_DB->loadResult();
	
	$JLMS_DB->setQuery("SELECT message_value FROM `#__lms_message_configuration` WHERE message_conf = 'mail_title' ");
	$mail_title = $JLMS_DB->loadResult();
	
						if(!isset($mail_title) || !$mail_title){
							$mail_title = "{sitename}: New message notification";
						}
						
	$mail_title = str_replace('{sitename}',$JLMS_CONFIG->get('sitename'),$mail_title);
	$JLMS_DB->setQuery("SELECT message_value FROM `#__lms_message_configuration` WHERE message_conf = 'mail_body' ");
	$mail_body = $JLMS_DB->loadResult();
	if(!isset($mail_body) || !$mail_body){
						$mail_body = "Hello,
							{name} has just sent new message '{title}'.
							This message is located at {link}
							
							This is the message that was sent:
							***************
							{message}
							***************
							Please do not reply to this email. "
							;
					}
	$mail_body = nl2br(stripslashes($mail_body));
	$mail_body = str_replace('{sitename}',$JLMS_CONFIG->get('sitename'),$mail_body);
	$mail_body = str_replace('{name}',$my->name,$mail_body);
	$mail_body = str_replace('{email}',$my->email,$mail_body);
	$mail_body = str_replace('{title}',$jlms_subject,$mail_body);
	$mail_body = str_replace('{message}',$jlms_mailbox_letter,$mail_body);
	$mail_body = str_replace('{link}','<a href="'.$link.'">'.$link.'</a>',$mail_body);
	
	$res = false;
	
	for($i=0;$i<count($res_usr);$i++)
	{
		
		$query = "INSERT INTO #__lms_messages_to(id,user_id,is_read,del) VALUES (".$mail_id.",".intval($res_usr[$i]).",'0','0')";
		$JLMS_DB->setQuery($query);
		$JLMS_DB->query();
		
		$res = true;
		
		if($mess_enotify)
		{			
			$JLMS_DB->setQuery("SELECT email FROM #__users WHERE id=".$res_usr[$i]);
						
			JLMS_mosMail('','',$JLMS_DB->loadResult(),$mail_title,$mail_body,1);
		}
		
	}
	
	return $res;
}

function JLMS_MailSend($course_id, $option)
{	
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$Itemid = $JLMS_CONFIG->get('Itemid');
	
	JLMS_sendMessage($course_id, $option);
	JLMSRedirect( sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;id=$course_id&amp;task=mail_sendbox") );
		
}
function JLMS_viewmail($course_id, $option)
{
	global $my, $JLMS_DB, $Itemid, $JLMS_SESSION;
	$view_id = intval(mosGetParam($_REQUEST, 'view_id', 0));
	$inb = intval(mosGetParam($_REQUEST, 'inb', 0));
	if (!$view_id) {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&id=$course_id&task=mailbox") );
	}
	if (!$inb) {
		$query = "SELECT m.*,mt.is_read,u.username FROM #__lms_messages as m, #__lms_messages_to as mt, #__users as u WHERE m.id = mt.id AND u.id = m.sender_id  AND mt.user_id = ".$my->id." AND m.id=".$view_id;
	} else {
		$query = "SELECT m.* FROM #__lms_messages as m WHERE m.sender_id = ".$my->id." AND m.id=".$view_id;
	}	
	$JLMS_DB->setQuery($query);
	$row = $JLMS_DB->loadObject();
	if (!$inb) {
		$query = "UPDATE #__lms_messages_to SET is_read='1' WHERE id=$view_id AND user_id=".$my->id;
		$JLMS_DB->setQuery($query);
		$JLMS_DB->query();
	}
	if (is_object($row)) {
		JLMS_mailbox_html::mailbox_view( $row, $option, $course_id, $inb );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&id=$course_id&task=mailbox") );
	}
}
function JLMS_maildelete($cid, $option, $course_id)
{
	global $my, $JLMS_DB, $Itemid, $JLMS_SESSION;
	
	$sendbox = intval(mosGetParam($_REQUEST, 'sendbox', 0));
	if(count($cid))
	{
		$cids = implode(',',$cid);
		if(!$sendbox)
		{
			
			$query = "UPDATE #__lms_messages_to SET del='1' WHERE id IN ($cids) AND user_id=".$my->id;
			$JLMS_DB->setQuery($query);
			$JLMS_DB->query();
			
		}
		else 
		{
			$query = "UPDATE #__lms_messages SET del='1' WHERE id IN ($cids) AND sender_id=".$my->id;
			$JLMS_DB->setQuery($query);
			$JLMS_DB->query();
			
		}
		foreach ($cid as $cids)
		{
			$query = "SELECT del FROM #__lms_messages WHERE id IN ($cids)";
			$JLMS_DB->setQuery($query);
			if($JLMS_DB->loadResult())
			{
				$query = "SELECT COUNT(*) FROM #__lms_messages as m, #__lms_messages_to as mt WHERE m.id=mt.id AND m.del='1' AND mt.del='0' AND m.id IN ($cids)";
				$JLMS_DB->setQuery($query);
				if(!$JLMS_DB->loadResult())
				{
					$query = "DELETE FROM #__lms_messages_to WHERE id IN ($cids)";
					$JLMS_DB->setQuery($query);
					$JLMS_DB->query();
					$query = "SELECT file FROM #__lms_messages WHERE id IN ($cids) AND file!=0";
					$JLMS_DB->setQuery($query);
					$file_ids = JLMSDatabaseHelper::loadResultArray();
					if($file_ids)
					JLMS_deleteFiles($file_ids);
					$query = "DELETE FROM #__lms_messages WHERE id IN ($cids)";
					$JLMS_DB->setQuery($query);
					$JLMS_DB->query();
				}
			}	
		}	
		
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;id=$course_id&amp;task=mailbox") );
}
function JLMS_mk_read($resto,$option,$course_id)
{
	global $my, $JLMS_DB, $Itemid, $JLMS_SESSION;
	$view_id = intval(mosGetParam($_REQUEST, 'view_id', 0));
	if(!$view_id) die();
	$query = "UPDATE #__lms_messages_to SET is_read='".$resto."' WHERE id=$view_id AND user_id=".$my->id;
		$JLMS_DB->setQuery($query);
		$JLMS_DB->query();
	JLMSRedirect( sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;id=$course_id&amp;task=mailbox") );	
}

function JLMS_loadfile($option)
{
	global $my,$JLMS_DB;
	$view_id = intval(mosGetParam($_REQUEST, 'view_id', 0));
	$query = "SELECT m.file FROM #__lms_messages as m, #__lms_messages_to as mt WHERE m.id=mt.id AND m.id = ".$view_id." AND (m.sender_id=".$my->id." OR mt.user_id=".$my->id.")";
	$JLMS_DB->setQuery($query);
	$file_id = $JLMS_DB->loadResult();
	if($file_id){
		JLMS_downloadFile( $file_id, $option);
	}	
	JLMSRedirect( sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;id=$course_id&amp;task=mailbox") );	
}

function getExtension($chaine)
{	
	$taille = strlen($chaine)-1;
	for ($i = $taille; $i >= 0; $i--)
		if ($chaine["$i"] == '.') break;
		
	return substr($chaine, $i+1, strlen($chaine)-($i+1));
}
?>