<?php
/**
* joomla_lms.outdocs.hlprphp
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

class JLMS_changeHeightSCORM extends JObject {
    
    var $id = null;
    var $update_all_recent_sources = null;
    
    public function __construct($id){
        parent::__construct();
        
        $this->db = & JLMSFactory::getDB();
        $this->session = & JLMSFactory::getSession();
        $this->user = & JFactory::getUser();
        
        $this->setId($id);
        $this->update_all_recent_sources = JRequest::getVar('update_all_recent_sources', 0);
        
        $params_scorm_in_library = $this->getParamsSCORMInLibrary();
        $scorm_package_id = $this->getScormPackageId($params_scorm_in_library);
		
		$sources_update = $this->getSourcesWichToBeUpdate($scorm_package_id);
		$sources_update = AppendFileIcons_toList( $sources_update );
		for($i=0;$i<count($sources_update);$i++) {
			if($sources_update[$i]->folder_flag == 3) {
				$sources_update[$i]->file_icon = 'tlb_scorm';
			}
		}
		
		if(!count($sources_update) && !in_array($this->update_all_recent_sources, array(1,2))){
			$this->update_all_recent_sources = 3;
		}
        
        if(in_array($this->update_all_recent_sources, array(1,2))){
            $stored_request_vars = unserialize($this->session->get('store_request_vars_'.$this->user->id));
            if(isset($stored_request_vars)){
                $_REQUEST = $stored_request_vars;
            }
            $this->session->clear('store_request_vars_'.$this->user->id);
            $stored_post_vars = unserialize($this->session->get('store_post_vars_'.$this->user->id));
            if(isset($stored_post_vars)){
                $_POST = $stored_post_vars;
            }
            $this->session->clear('store_request_vars_'.$this->user->id);
            $stored_files_vars = unserialize($this->session->get('store_files_vars_'.$this->user->id));
            if(isset($stored_files_vars)){
                $_FILES = $stored_files_vars;
            }
            $this->session->clear('store_files_vars_'.$this->user->id);
            
			if($this->update_all_recent_sources == 2){
				$update_params = $this->getUpdateParams($params_scorm_in_library);
				$this->updateAllResourcesRelatedWithSCORMInLibrary($scorm_package_id, $update_params);
			}
        } else
        if(!$this->update_all_recent_sources){
            
            $this->storeVarsInSession($_REQUEST, 'store_request_vars_'.$this->user->id);
            $this->storeVarsInSession($_POST, 'store_post_vars_'.$this->user->id);
            $this->storeVarsInSession($_FILES, 'store_files_vars_'.$this->user->id);
			
			if($this->checkData($params_scorm_in_library)){
				JLMS_outdocs_html::confirm_updateLinkedResources($this->id, $sources_update, 'com_joomla_lms');
			} else {
				$this->update_all_recent_sources = 1;
			}
        }
    }
    
    function setId($id){
        $this->id = $id;
    }
    
    function getId(){
        return $this->id;
    }
	
	function checkData($params_scorm_in_library){
		
		$request_params = JRequest::getVar('params', 'array');
		$str_request_params = '';
		if(isset($request_params) && is_array($request_params)){
			$tmp = array();
			foreach($request_params as $field=>$value){
				$tmp[] = $field.'='.$value;
			}
			if(count($tmp)){
				$str_request_params = implode("\n", $tmp);
			}
		}
		
		$scorm_width = JRequest::getVar('scorm_width', null);
		$scorm_height = JRequest::getVar('scorm_height', null);
		
		if($params_scorm_in_library->params != $str_request_params){
			return 1;
		}
		if($params_scorm_in_library->width != $scorm_width){
			return 1;
		}
		if($params_scorm_in_library->height != $scorm_height){
			return 1;
		}
		
		return 0;
	}
    
    function storeVarsInSession($vars, $name){
        $value = isset($vars) ? serialize($vars) : '';
        $this->session->clear($name);
        $this->session->set($name, $value);
    }
    
    function getParamsSCORMInLibrary(){
        
        $query = "SELECT b.*"
        . "\n FROM"
        . "\n #__lms_outer_documents as a,"
        . "\n #__lms_n_scorm as b"
        . "\n WHERE 1"
        . "\n AND a.file_id = b.id"
        . "\n AND b.course_id = 0"
        . "\n AND a.id = ".$this->db->quote($this->id)
        ;
        $this->db->setQuery($query);
        $params_scorm_in_library = $this->db->loadObject();
        
        return $params_scorm_in_library;
    }
    
    function getScormPackageId($params_scorm_in_library){
        
        $scorm_package_id = false;
        
        if(isset($params_scorm_in_library->scorm_package) && $params_scorm_in_library->scorm_package){
            $scorm_package_id = $params_scorm_in_library->scorm_package;
        }
        
        return $scorm_package_id;
    }
	
	function getStoredParams($params_scorm_in_library){
        
        $stired_params = array();
        
        if(isset($params_scorm_in_library->params)){
            $stired_params['params'] = $params_scorm_in_library->params;
        }
        
        if(isset($params_scorm_in_library->width)){
            $stired_params['width'] = $params_scorm_in_library->width;
        }
        
        if(isset($params_scorm_in_library->height)){
            $stired_params['height'] = $params_scorm_in_library->height;
        }
        
        return $stired_params;
    }
    
    function getUpdateParams($params_scorm_in_library){
        
        $update_params = array();
		
		$params = JRequest::getVar('params', 'array');
		if (is_array( $params )) {
			$txt = array();
			foreach ( $params as $k=>$v) {
				$txt[] = "$k=$v";
			}
			$scorm_params = implode( "\n", $txt );
		}
		$update_params['params'] = $scorm_params;
		$update_params['width'] = JRequest::getVar('scorm_width', 0);
		$update_params['height'] = JRequest::getVar('scorm_height', 0);
        
        return $update_params;
    }
    
    function getSourcesWichToBeUpdate($scorm_package_id){
        
        $query = "SELECT a.*, b.course_name"
		. "\n, (
		SELECT ab.doc_name
		FROM
			#__lms_n_scorm as aa,
			#__lms_outer_documents as ab
		WHERE 1
		AND aa.scorm_package = a.scorm_package
		AND aa.course_id = ".$this->db->quote(0)."
		AND aa.id = ab.file_id
		) as doc_name"
		. "\n, (
		SELECT ab.folder_flag
		FROM
			#__lms_n_scorm as aa,
			#__lms_outer_documents as ab
		WHERE 1
		AND aa.scorm_package = a.scorm_package
		AND aa.course_id = ".$this->db->quote(0)."
		AND aa.id = ab.file_id
		) as folder_flag"
        . "\n FROM"
        . "\n #__lms_n_scorm as a"
        . "\n, #__lms_courses as b"
        . "\n WHERE 1"
        . "\n AND a.scorm_package = ".$this->db->quote($scorm_package_id)
        . "\n AND a.course_id > ".$this->db->quote(0)
        . "\n AND a.course_id = b.id"
        ;
        $this->db->setQuery($query);
        $sources_update = $this->db->loadObjectList();
        
        return $sources_update;
    }
    
    function updateAllResourcesRelatedWithSCORMInLibrary($scorm_package_id, $update_params){
        
        $sets = array();
        foreach($update_params as $field=>$value){
            $sets[] = $field . ' = '.$this->db->quote($value);
        }
        $sets_string = implode(', ', $sets);
        
        $query = "UPDATE #__lms_n_scorm"
        . "\n SET"
        . "\n ". $sets_string
        . "\n WHERE 1"
        . "\n AND scorm_package = ".$this->db->quote($scorm_package_id)
        . "\n AND course_id > ".$this->db->quote(0)
        ;
        $this->db->setQuery($query);
        $this->db->query();
    }
}
