<?php
/**
* joomla_lms.quiz.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/

// no direct access
defined( '_JLMS_EXEC' ) or die( 'Restricted access' );
global $JLMS_CONFIG;
if ($JLMS_CONFIG->get('plugin_quiz')) {
	require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.quiz.html.php");
	
	global $JLMS_LANGUAGE;
	JLMS_require_lang($JLMS_LANGUAGE, array('course_docs.lang'), $JLMS_CONFIG->get('default_language'));
	/*require_once(_JOOMLMS_FRONT_HOME . "/languages/".$JLMS_CONFIG->get('default_language')."/subscription.lang.php");*/
	JLMS_processLanguage( $JLMS_LANGUAGE );

	global $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id', 0);
	$pathway = array();
	$pathway[] = array('name' => _JLMS_PATHWAY_HOME, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"), 'is_home' => true);
	if($course_id){
		$pathway[] = array('name' => $JLMS_CONFIG->get('course_name'), 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$course_id"), 'is_course' => true);
		$pathway[] = array('name' => _JLMS_TOOLBAR_QUIZZES, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=quizzes&amp;id=$course_id"));
	} else {
		$pathway[] = array('name' => _JLMS_GLOBAL_QUEST_POOL, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=quizzes&page=setup_gqp"));
	}
	JLMSAppendPathWay($pathway);

	$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) ); 
	$task 	= mosGetParam( $_REQUEST, 'task', '' );
	$atask 	= mosGetParam( $_REQUEST, 'atask', '' );
	$page = strval(mosGetParam( $_REQUEST, 'page', '' ));
	
	$view = strval( mosGetParam( $_REQUEST, 'view', '' ) );
	
	if ($task != 'quiz_ajax_action' && $page != 'csv_report' && $task != 'print_quiz_result' && $task != 'print_quiz_cert' ) {
		if (($task == 'quizzes' && $page == 'createhotspot') || ($task == 'quizzes' && $page == 'createhotspot_gqp') || ($task == 'quizzes' && $page == 'imgs_v') || ($task == 'quiz_action' && $atask == 'email_results') || ($task == 'quizzes' && ($page == 'export_quest' || $page == 'export_quest_gqp' ) ) || ($task == 'quizzes' && $page == 'reports' && $view == 'xls') ) { //|| ($task == 'quizzes' && $page == 'import_quest')
		} else {
			JLMS_ShowHeading();
		}
	}

	global $JLMS_DB;
	$JLMS_ACL = JLMSFactory::getACL();
	
	switch ($task) {
		case 'quizzes':
			require_once(_JOOMLMS_FRONT_HOME . "/includes/quiz/joomlaquiz.class.php");
			require_once(_JOOMLMS_FRONT_HOME . "/includes/quiz/admin.joomlaquiz.class.php");
			require_once(_JOOMLMS_FRONT_HOME . "/includes/quiz/admin_html.joomlaquiz.class.php");
			
			$gqp_tasks = array('setup_gqp', 'add_quest_gqp', 'save_quest_gqp', 'apply_quest_gqp', 'editA_quest_gqp', 'edit_quest_gqp', 'cancel_quest_gqp', 'del_quest_gqp','uploadimage_gqp','createhotspot_gqp', 'quiz_bars_gqp', 'copy_quest_sel_gqp', 'move_quest_sel_gqp', 'copy_quest_save_gqp', 'move_quest_save_gqp', 'publish_quest', 'unpublish_quest', 'publish_quest_gqp', 'unpublish_quest_gqp', 'view_preview', 'category_gqp', 'new_category_gqp', 'edit_category_gqp', 'delete_category_gqp', 'apply_category_gqp', 'save_category_gqp', 'export_quest_gqp', 'import_quest_gqp', 'import_quest_run_gqp');
			
			if ($course_id){
				$page = strval(mosGetParam( $_REQUEST, 'page', '' ));
				$c_id = intval( mosGetParam( $_REQUEST, 'c_id', 0 ) );
				$cid = mosGetParam( $_REQUEST, 'cid', array(0) );

				$category_tasks = array('cats', 'add_cat', 'edit_cat', 'editA_cat', 'apply_cat', 'save_cat', 'del_cat', 'cancel_cat');
				$quizzes_tasks = array('quizzes', 'add_quiz', 'edit_quiz', 'editA_quiz', 'apply_quiz', 'save_quiz', 'del_quiz', 'cancel_quiz', 'move_quiz_sel', 'copy_quiz_sel', 'move_quiz_save', 'copy_quiz_save');
				$quests_tasks = array('setup_pool', 'setup_quest', 'add_quest', 'edit_quest', 'editA_quest', 'apply_quest', 'save_quest', 'del_quest', 'cancel_quest', 'quest_orderup', 'quest_orderdown', 'saveorederall', 'move_quest_sel', 'move_quest_save', 'copy_quest_sel', 'copy_quest_save', 'uploadimage', 'createhotspot', 'view_preview', 'export_quest', 'import_quest', 'import_quest_run');
				$imgs_tasks = array('imgs', 'add_imgs', 'edit_imgs', 'editA_imgs', 'save_imgs', 'del_imgs', 'cancel_imgs');
				$imgs_view_tasks = array('imgs_v');
				$publish_tasks = array('publish_quiz', 'unpublish_quiz', 'publish_quest', 'unpublish_quest');
				$reports_tasks = array('reports', 'csv_report', 'stu_reportA', 'stu_report', 'del_report', 'quest_reportA', 'quest_report', 'del_stu_report', 'quiz_bars', 'view_answ_survey');
				$certificates_tasks = array('certificates', 'add_crtf', 'edit_crtf', 'editA_crtf', 'save_crtf', 'apply_crtf', 'cancel_crtf', 'del_crtf', 'preview_crtf');
				
				if (!$page) {
					if ( $JLMS_ACL->CheckPermissions('quizzes', 'manage') || $JLMS_ACL->CheckPermissions('quizzes', 'manage_pool') || $JLMS_ACL->CheckPermissions('quizzes', 'view_stats') || $JLMS_ACL->CheckPermissions('quizzes', 'publish') ) {
						JLMS_quiz_admin_class::JQ_ListQuizzes( $option, $page, $course_id );
					} elseif ($JLMS_ACL->CheckPermissions('quizzes', 'view')) {
						JLMS_quiz_admin_class::JQ_ListQuizzes_Stu( $option, $course_id );
					}
				} elseif (in_array($page, $category_tasks)) {
					if ($JLMS_ACL->CheckPermissions('quizzes', 'manage')) {
						switch ($page) {
							case 'cats':			JLMS_quiz_admin_class::JQ_ListCategories( $option, $page, $course_id );						break;
							case 'add_cat':			JLMS_quiz_admin_class::JQ_editCategory( '0', $option, $page, $course_id );					break;
							case 'edit_cat':		JLMS_quiz_admin_class::JQ_editCategory( intval( $cid[0] ), $option, $page, $course_id );	break;
							case 'editA_cat':		JLMS_quiz_admin_class::JQ_editCategory( $c_id, $option, $page, $course_id );				break;
							case 'apply_cat':
							case 'save_cat':		JLMS_quiz_admin_class::JQ_saveCategory( $option, $page, $course_id );						break;
							case 'del_cat':			JLMS_quiz_admin_class::JQ_removeCategory( $cid, $option, $page, $course_id );				break; 		
							case 'cancel_cat':		JLMS_quiz_admin_class::JQ_cancelCategory( $option, $page, $course_id );						break;
						}
					} else {
						JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=quizzes&id=$course_id") );
					}
				} elseif (in_array($page, $publish_tasks)) {
					if ($JLMS_ACL->CheckPermissions('quizzes', 'publish')) {
						switch ($page) {
							case 'publish_quiz':	JLMS_quiz_admin_class::JQ_changeQuiz( $cid, 1, $option, $page, $course_id );				break;
							case 'unpublish_quiz':	JLMS_quiz_admin_class::JQ_changeQuiz( $cid, 0, $option, $page, $course_id );				break;
							case 'publish_quest':	JLMS_quiz_admin_class::JQ_changeQuest( $cid, 1, $option, $page, $course_id );				break;
							case 'unpublish_quest':	JLMS_quiz_admin_class::JQ_changeQuest( $cid, 0, $option, $page, $course_id );				break;
						}
					} else {
						JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=quizzes&id=$course_id") );
					}
				} elseif (in_array($page, $quizzes_tasks)) {
					if ($JLMS_ACL->CheckPermissions('quizzes', 'manage')) {
						switch ($page) {
							case 'quizzes':			JLMS_quiz_admin_class::JQ_ListQuizzes( $option, $page, $course_id );						break;
							case 'add_quiz':		JLMS_quiz_admin_class::JQ_editQuiz( '0', $option, $page, $course_id );						break;
							case 'edit_quiz':		JLMS_quiz_admin_class::JQ_editQuiz( intval( $cid[0] ), $option, $page, $course_id );		break;
							case 'editA_quiz':		JLMS_quiz_admin_class::JQ_editQuiz( $c_id, $option, $page, $course_id );					break;
							case 'apply_quiz':
							case 'save_quiz':		JLMS_quiz_admin_class::JQ_saveQuiz( $option, $page, $course_id );							break;
							case 'del_quiz':		JLMS_quiz_admin_class::JQ_removeQuiz( $cid, $option, $page, $course_id );					break; 		
							case 'cancel_quiz':		JLMS_quiz_admin_class::JQ_cancelQuiz( $option, $page, $course_id );							break;
							case 'move_quiz_sel':	JLMS_quiz_admin_class::JQ_moveQuizSelect( $option, $page, $course_id, $cid );				break;
							case 'move_quiz_save':	JLMS_quiz_admin_class::JQ_moveQuizSave( $option, $page, $course_id, $cid );					break;
							case 'copy_quiz_sel':	JLMS_quiz_admin_class::JQ_moveQuizSelect( $option, $page, $course_id, $cid );				break;
							case 'copy_quiz_save':	JLMS_quiz_admin_class::JQ_copyQuizSave( $option, $page, $course_id, $cid );					break;
						}
					} else {
						JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=quizzes&id=$course_id") );
					}
				} elseif (in_array($page, $quests_tasks)) {
					if ($JLMS_ACL->CheckPermissions('quizzes', 'manage') || $JLMS_ACL->CheckPermissions('quizzes', 'manage_pool')) {
						switch ($page) {
							case 'setup_pool':
							case 'setup_quest':		JLMS_quiz_admin_class::JQ_ListQuestions( $option, $page, $course_id );						break;
							
							case 'add_quest':		JLMS_quiz_admin_class::JQ_editQuestion( '0', $option, 0, $page, $course_id );				break;
							case 'edit_quest':		JLMS_quiz_admin_class::JQ_editQuestion( intval( $cid[0] ), $option, 0, $page, $course_id);	break;
							case 'editA_quest':		JLMS_quiz_admin_class::JQ_editQuestion( $c_id, $option, 0, $page, $course_id );				break;
							case 'apply_quest':
							case 'save_quest':		JLMS_quiz_admin_class::JQ_saveQuestion( $option, $page, $course_id );						break;
							case 'del_quest':		JLMS_quiz_admin_class::JQ_removeQuestion( $cid, $option, $page, $course_id );				break;
							case 'cancel_quest':	JLMS_quiz_admin_class::JQ_cancelQuestion( $option, $page, $course_id );						break;
							case 'quest_orderup':
							case 'quest_orderdown':	JLMS_quiz_admin_class::JQ_orderQuest_new( $option, $page, $course_id );						break;
							case 'saveorederall':	JLMS_quiz_admin_class::JQ_orderAllQuests( $option, $page, $course_id );						break;
		
							case 'move_quest_sel':	JLMS_quiz_admin_class::JQ_moveQuestionSelect( $option, $page, $course_id, $cid );			break;
							case 'move_quest_save':	JLMS_quiz_admin_class::JQ_moveQuestionSave( $option, $page, $course_id, $cid );				break;
							case 'copy_quest_sel':	JLMS_quiz_admin_class::JQ_moveQuestionSelect( $option, $page, $course_id, $cid );			break;
							case 'copy_quest_save':	JLMS_quiz_admin_class::JQ_copyQuestionSave( $option, $page, $course_id, $cid );				break;
		
							case 'uploadimage':		jlms_quiz_upload_popup( $option, $page, $course_id );										break;
							case 'createhotspot':	jlms_quiz_create_hotspot( $option, $page, $course_id );										break;

							case 'view_preview':	JLMS_quiz_previewQuestion( $option, $course_id );											break;
							
							case 'export_quest':	JLMS_quiz_admin_class::JQ_exportQuestions( $option, $page, $course_id, $cid );				break;
							case 'import_quest':	JLMS_quiz_admin_class::JQ_importQuestions( $option, $page, $course_id );					break;
							case 'import_quest_run':JLMS_quiz_admin_class::JQ_importQuestionsRun( $option, $page, $course_id );					break;
							
						}
					} else {
						JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=quizzes&id=$course_id") );
					}
				} elseif (in_array($page, $imgs_tasks)) {
					if ($JLMS_ACL->CheckPermissions('quizzes', 'manage')) {
						switch ($page) {
							case 'imgs':		JLMS_quiz_admin_class::JQ_ImgsList( $option, $page, $course_id );								break;
							case 'add_imgs':	JLMS_quiz_admin_class::JQ_ImgsList_edit( '0', $option, $course_id );							break;	
							case 'edit_imgs':	JLMS_quiz_admin_class::JQ_ImgsList_edit( intval($cid[0]), $option, $course_id );				break;
							case 'editA_imgs':	JLMS_quiz_admin_class::JQ_ImgsList_edit( $c_id, $option, $course_id );							break;
							case 'save_imgs':	JLMS_quiz_admin_class::JQ_ImgsList_save( $option );												break;		
							case 'del_imgs':	JLMS_quiz_admin_class::JQ_ImgsList_del( $cid, $option, $page, $course_id );						break;
							case 'cancel_imgs':	JLMS_quiz_admin_class::JQ_ImgListcancel( $option, $page, $course_id );							break;
						}
					} else {
						JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=quizzes&id=$course_id") );
					}
				} elseif (in_array($page, $imgs_view_tasks)) {
					if ($JLMS_ACL->CheckPermissions('quizzes', 'view')) {
						switch ($page) {
							case 'imgs_v':		
												JLMS_quiz_admin_class::JQ_ImgsList_v( $option, JRequest::getInt('file_id', 0) );				
							break;
						}
					} else {
						JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=quizzes&id=$course_id") );
					}
				} elseif (in_array($page, $reports_tasks)) {
					if ($JLMS_ACL->CheckPermissions('quizzes', 'view_stats')) {
						/**
						Max: Answers users on question survey
						**/
						if($page == 'view_answ_survey'){
							$quiz_id = intval( mosGetParam( $_REQUEST, 'quiz_id', 0 ) );
							$quest_id = intval( mosGetParam( $_REQUEST, 'quest_id', 0 ) );
						}
						//echo $page;
						//echo $cid[0]; die;
						
						
						switch ($page) {
							case 'reports':			JLMS_quiz_admin_class::JQ_view_quizReport( $option, $page, $course_id );					break;
							case 'csv_report':		JLMS_quiz_admin_class::JQ_view_quizReport( $option, $page, $course_id, 1 );					break;
							case 'stu_reportA':		JLMS_quiz_admin_class::JQ_view_stuReport( $c_id, $option, $page, $course_id );				break;
							case 'stu_report':		JLMS_quiz_admin_class::JQ_view_stuReport( intval( $cid[0] ), $option, $page, $course_id );	break;
							case 'del_report':		JLMS_quiz_admin_class::JQ_delete_quizReport( $cid, $option, $page, $course_id );			break;
							case 'quest_reportA':	JLMS_quiz_admin_class::JQ_view_questReport( $c_id, $option, $page, $course_id );			break;
							case 'quest_report':	JLMS_quiz_admin_class::JQ_view_questReport(intval( $cid[0] ), $option, $page, $course_id);	break;
							case 'del_stu_report':	JLMS_quiz_admin_class::JQ_delete_stuReport( $cid, $option, $page, $course_id );				break;
							case 'quiz_bars':		JLMS_quiz_admin_class::JQ_ViewCharts(intval( $cid[0] ), $option, $course_id);				break;
							
							case 'view_answ_survey':JLMS_quiz_admin_class::JQ_ViewAnswersSurvey($option, $course_id, $quiz_id, $quest_id);	break;
						}
					} else {
						JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=quizzes&id=$course_id") );
					}
				} elseif (in_array($page, $certificates_tasks)) {
					if ($JLMS_ACL->CheckPermissions('quizzes', 'manage')) {
						switch ($page) {
							case 'certificates':	JLMS_quiz_Show_Certificates( $option, $course_id );											break;
							case 'add_crtf':		JLMS_quiz_editCertificate( '0', $option, $course_id );										break;
							case 'edit_crtf':		JLMS_quiz_editCertificate( intval( $cid[0] ), $option, $course_id );						break;
							case 'editA_crtf':		JLMS_quiz_editCertificate( $c_id, $option, $course_id );									break;
							case 'save_crtf':		JLMS_quiz_saveCertificate(  $option, $course_id, $page );									break;
							case 'apply_crtf':		JLMS_quiz_saveCertificate(  $option, $course_id, $page );									break;
							case 'cancel_crtf':		JLMS_quiz_cancelCertificate( $option, $course_id );											break;
							case 'del_crtf':		JLMS_quiz_deleteCertificate( $option, $course_id );											break;
							case 'preview_crtf':	JLMS_quiz_previewCertificate( $option, $course_id );										break;
						}
					} else {
						JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=quizzes&id=$course_id") );
					}
				}
				else {
					if ( $JLMS_ACL->CheckPermissions('quizzes', 'manage') || $JLMS_ACL->CheckPermissions('quizzes', 'manage_pool') || $JLMS_ACL->CheckPermissions('quizzes', 'view_stats') || $JLMS_ACL->CheckPermissions('quizzes', 'publish') ) {
						JLMS_quiz_admin_class::JQ_ListQuizzes( $option, $page, $course_id );
					} elseif ($JLMS_ACL->CheckPermissions('quizzes', 'view')) {
						JLMS_quiz_admin_class::JQ_ListQuizzes_Stu( $option, $course_id );
					}
				}
			} elseif (in_array($page, $gqp_tasks)) {
				if ($JLMS_ACL->CheckPermissions('quizzes', 'manage') || $JLMS_ACL->CheckPermissions('quizzes', 'manage_pool')) {
					
					$page = strval(mosGetParam( $_REQUEST, 'page', '' ));
					$c_id = intval( mosGetParam( $_REQUEST, 'c_id', 0 ) );
					$cid = mosGetParam( $_REQUEST, 'cid', array(0) );
					
					$course_id = 0;
					$gqp = true;
				
					$JLMS_SESSION->set('jlms_section', _JLMS_TOOLBAR_GQP_PARENT);
					
					if(!$c_id){
						$c_id = $cid[0];
					}
					
//					if($page == 'category_gqp'){	
//						echo $page;
//						die;
//					}
					
					switch ($page) {
						case 'setup_gqp':			JLMS_quiz_admin_class::JQ_ListQuestions( $option, $page, $course_id, $gqp );					break;
						
						case 'add_quest_gqp':		JLMS_quiz_admin_class::JQ_editQuestion( '0', $option, 0, $page, $course_id, $gqp );				break;
						case 'edit_quest_gqp':	
						case 'editA_quest_gqp':		JLMS_quiz_admin_class::JQ_editQuestion( $c_id, $option, 0, $page, $course_id, $gqp );			break;
						case 'apply_quest_gqp':
						case 'save_quest_gqp':		JLMS_quiz_admin_class::JQ_saveQuestion( $option, $page, $course_id, $gqp );						break;
						
						case 'move_quest_sel_gqp':	JLMS_quiz_admin_class::JQ_moveQuestionSelect( $option, $page, $course_id, $cid, $gqp, 'move_quest_sel_gqp');		break;
						case 'move_quest_save_gqp':	JLMS_quiz_admin_class::JQ_moveQuestionSave( $option, $page, $course_id, $cid, $gqp);				break;
						case 'copy_quest_sel_gqp':	JLMS_quiz_admin_class::JQ_moveQuestionSelect( $option, $page, $course_id, $cid, $gqp, 'copy_quest_sel_gqp' );		break;
						case 'copy_quest_save_gqp':	JLMS_quiz_admin_class::JQ_copyQuestionSave( $option, $page, $course_id, $cid, '','',$gqp, 0 );		break;
						
						case 'del_quest_gqp':		JLMS_quiz_admin_class::JQ_removeQuestion( $cid, $option, $page, $course_id, 0, $gqp );			break;
						case 'cancel_quest_gqp':	JLMS_quiz_admin_class::JQ_cancelQuestion( $option, $page, $course_id, $gqp );					break;
						case 'quiz_bars_gqp':		JLMS_quiz_admin_class::JQ_ViewCharts(intval( $cid[0] ), $option, $course_id, $gqp);				break;
						
						case 'uploadimage_gqp':		jlms_quiz_upload_popup( $option, $page, $course_id, $gqp );									break;
						case 'createhotspot_gqp':	jlms_quiz_create_hotspot( $option, $page, $course_id, $gqp );								break;
						
						case 'publish_quest_gqp':
						case 'publish_quest':		JLMS_quiz_admin_class::JQ_changeQuest( $cid, 1, $option, $page, 0, 1 );							break;
						case 'unpublish_quest_gqp':
						case 'unpublish_quest':		JLMS_quiz_admin_class::JQ_changeQuest( $cid, 0, $option, $page, 0, 1 );							break;
						
						case 'view_preview':		JLMS_quiz_previewQuestion( $option, $course_id, true );											break;
						
						case 'category_gqp':		JLMS_quiz_admin_class::JQ_ListCategoryGQP($option, $page, $course_id);							break;
						case 'new_category_gqp':	JLMS_quiz_admin_class::JQ_editCategoryGQP(0, $option, $page, $course_id);						break;
						case 'edit_category_gqp':	JLMS_quiz_admin_class::JQ_editCategoryGQP($c_id, $option, $page, $course_id);					break;
						case 'delete_category_gqp':	JLMS_quiz_admin_class::JQ_deleteCategoryGQP($cid, $option, $page, $course_id);					break;
						case 'apply_category_gqp':
						case 'save_category_gqp':	JLMS_quiz_admin_class::JQ_saveCategoryGQP( $option, $page, $course_id);							break;
						
						case 'export_quest_gqp':	JLMS_quiz_admin_class::JQ_exportQuestions( $option, $page, $course_id, $cid, $gqp );				break;
						case 'import_quest_gqp':	JLMS_quiz_admin_class::JQ_importQuestions( $option, $page, $course_id, $gqp );						break;
						case 'import_quest_run_gqp':JLMS_quiz_admin_class::JQ_importQuestionsRun( $option, $page, $course_id, $gqp );					break;
					}
				} else {
					JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
				}
			} else {
				 JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
			}
		break;
		case 'show_quiz':
			if ($JLMS_ACL->CheckPermissions('quizzes', 'view')) {
				#require_once(_JOOMLMS_FRONT_HOME . "/includes/quiz/joomlaquiz.class.php");
				require_once(_JOOMLMS_FRONT_HOME . "/includes/quiz/front.joomlaquiz.class.php");
				require_once(_JOOMLMS_FRONT_HOME . "/includes/quiz/front_html.joomlaquiz.class.php");
				$quiz_id = intval( mosGetParam( $_REQUEST, 'quiz_id', 0 ) );
				$redirect = false;
				if ($id) {
					$query = "SELECT course_id, c_title FROM #__lms_quiz_t_quiz WHERE c_id = '".$quiz_id."'"
					 . ( $JLMS_ACL->CheckPermissions('quizzes', 'view_all') ? '' : " AND published = 1" );
					$JLMS_DB->SetQuery( $query );
					$quiz_data = $JLMS_DB->LoadObjectList();
					if (count($quiz_data) && ($quiz_data[0]->course_id == $id) ) {	//SHOW QUIZ FOR STUDENT
						//JLMS_showPageImageHeaderMenu( $option, $id, 'quiz', $quiz_data[0]->c_title );
								JLMS_TMPL::OpenMT();
								$params = array();
								JLMS_TMPL::ShowHeader('quiz', $quiz_data[0]->c_title, $params);
								JLMS_TMPL::CloseMT();
								
								
						JLMS_quiz_front_class::JQ_doQuiz( $option, $quiz_id, $id );
					} else { JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=quizzes&id=$id") );}
				} else { JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );}

			}
		break;
		case 'quiz_ajax_action':
			if ($JLMS_ACL->CheckPermissions('quizzes', 'view')) {
				$jq_task = mosGetParam($_REQUEST, 'atask', '');
				
				//if($atask != 'start'){
				//	sleep(35);
				//}
				
				$quiz = intval(mosGetParam($_REQUEST, 'quiz', 0));
				$quest_id = intval( mosGetParam( $_REQUEST, 'quest_id', 0 ) );
				$gqp = false;
				if(!$id && !$quiz){
					$gqp = true;
				}
				if ($id && $quiz || ($JLMS_ACL->CheckPermissions('quizzes', 'manage') && $id && !$quiz) || $gqp) {
					if ($JLMS_ACL->CheckPermissions('quizzes', 'manage') || ( ($jq_task != 'preview_quest') && ($jq_task != 'next_preview') ) ) {
						if ( ($jq_task == 'preview_quest') || ($jq_task == 'next_preview') ) {
							if ($quiz) {
								$query = "SELECT course_id FROM #__lms_quiz_t_quiz WHERE c_id = '".$quiz."'";
							} else {
								$query = "SELECT course_id FROM #__lms_quiz_t_question WHERE c_id = '".$quest_id."'";
							}
						} else {
							$query = "SELECT course_id FROM #__lms_quiz_t_quiz WHERE c_id = '".$quiz."'"
							 . ( $JLMS_ACL->CheckPermissions('quizzes', 'view_all') ? '' : " AND published = 1" );
						}
						$JLMS_DB->SetQuery( $query );
						$quiz_course = $JLMS_DB->LoadResult();
						if ($gqp || ($quiz_course && ($quiz_course == $id))) {
							require_once(_JOOMLMS_FRONT_HOME . "/includes/quiz/ajax_quiz.class.php");
							JLMS_quiz_ajax_class::JQ_ajax_main($jq_task);
						} else {
							exit;
						}
					} else {
						exit;
					}
				} else {
					exit;
				}
			} else {
				exit;
			}
		break;
		case 'print_quiz_result':
			if ($JLMS_ACL->CheckPermissions('quizzes', 'view') || $JLMS_ACL->isStaff()) {
				require_once(_JOOMLMS_FRONT_HOME . "/includes/quiz/front.joomlaquiz.class.php");
				JLMS_quiz_front_class::JQ_printResults();
			}
		break;
		case 'print_quiz_cert':
			//if ($JLMS_ACL->CheckPermissions('quizzes', 'view') || $JLMS_ACL->isStaff()) {
			//proeducate certificate
			$juser = JFactory::getUser();
			if(isset($juser->id) && $juser->id){
			//proeducate certificate	
				require_once(_JOOMLMS_FRONT_HOME . "/includes/quiz/front.joomlaquiz.class.php");
				JLMS_quiz_front_class::JQ_printCertificate();
			}
		break;
		
		/*no jacascript support task*/
		case 'quiz_action':
			if ($JLMS_ACL->CheckPermissions('quizzes', 'view')) {
				$jq_task = mosGetParam($_REQUEST, 'atask', '');
				$quiz = intval(mosGetParam($_REQUEST, 'quiz', 0));
				if ($id && $quiz) {
					$query = "SELECT course_id, c_title FROM #__lms_quiz_t_quiz WHERE c_id = '".$quiz."'"
					 . ( $JLMS_ACL->CheckPermissions('quizzes', 'view_all') ? '' : " AND published = 1" );
					$JLMS_DB->SetQuery( $query );
					$quiz_data = $JLMS_DB->LoadObjectList();
					if ($JLMS_ACL->CheckPermissions('quizzes', 'manage') || ( ($jq_task != 'preview_quest') && ($jq_task != 'next_preview') ) ) {
						if ( ($jq_task == 'preview_quest') || ($jq_task == 'next_preview') ) {
							$query = "SELECT course_id FROM #__lms_quiz_t_quiz WHERE c_id = '".$quiz."'";
						} else {
							$query = "SELECT course_id FROM #__lms_quiz_t_quiz WHERE c_id = '".$quiz."'"
							 . ( $JLMS_ACL->CheckPermissions('quizzes', 'view_all') ? '' : " AND published = 1" );
						}
						$JLMS_DB->SetQuery( $query );
						$quiz_course = $JLMS_DB->LoadResult();
						if ( $quiz_course && ($quiz_course == $id) ) {
							if($jq_task != 'email_results'){
								JLMS_showPageImageHeaderMenu( $option, $id, 'quiz', $quiz_data[0]->c_title );
							}
							require_once(_JOOMLMS_FRONT_HOME . "/includes/quiz/ajax_quiz.class.php");
	//						JLMS_quiz_ajax_class::JQ_ajax_main($jq_task);
							JLMS_quiz_ajax_class::JQ_main_nojs($jq_task);
						} else {
							exit;
						}
					} else {
						exit;
					}
				} else {
					exit;
				}
			} else {
				exit;
			}
		break;
		/*no jacascript support task*/
	}
} else {
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
}
function JLMS_quiz_Show_Certificates( $option, $id ) {
	global $my, $JLMS_DB, $Itemid;
	if ($id && JLMS_GetUserType($my->id, $id) == 1) {	
		$query = "SELECT * FROM #__lms_certificates WHERE course_id = '".$id."' AND crtf_type = 2 AND parent_id = 0";
		$JLMS_DB->SetQuery( $query );
		$rows = $JLMS_DB->LoadObjectList();
		JLMS_quiz_html::ShowCertificates( $option, $id, $rows );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
	}
}
function JLMS_quiz_editCertificate( $id, $option, $course_id ) {
	global $JLMS_DB, $my, $Itemid;
	if ($course_id && JLMS_GetUserType($my->id, $course_id) == 1) {
		$row = new mos_Joomla_LMS_quiz_Certificate( $JLMS_DB );
		$query = "SELECT * FROM #__lms_certificates WHERE id = '".$id."' AND course_id = '".$course_id."' AND crtf_type = 2 AND parent_id = 0";
		$JLMS_DB->SetQuery( $query );
		$row1 = $JLMS_DB->loadObject();
		$crtf_id = (isset($row1->id) && $row1->id) ? $row1->id : 0;
		$row->id = $crtf_id;
		if (is_object($row1)) {
			$row->course_id = isset($row1->course_id) ? $row1->course_id : 0;
			$row->crtf_type = isset($row1->crtf_type) ? $row1->crtf_type : 0;
			$row->parent_id = isset($row1->parent_id) ? $row1->parent_id : 0;
			$row->published = isset($row1->published) ? $row1->published : 0;
			$row->file_id = isset($row1->file_id) ? $row1->file_id : 0;
			$row->crtf_text = isset($row1->crtf_text) ? $row1->crtf_text : 0;
			$row->crtf_name = isset($row1->crtf_name) ? $row1->crtf_name : '';
			$row->text_x = isset($row1->text_x) ? $row1->text_x : 0;
			$row->text_y = isset($row1->text_y) ? $row1->text_y : 0;
			$row->text_size = isset($row1->text_size) ? $row1->text_size : 0;
			$row->crtf_align = isset($row1->crtf_align) ? $row1->crtf_align : 0;
			$row->crtf_shadow = isset($row1->crtf_shadow) ? $row1->crtf_shadow : 0;
			$row->crtf_font = isset($row1->crtf_font) ? $row1->crtf_font : 0;
		}
		if ($crtf_id) {
			$query = "SELECT * FROM #__lms_certificates WHERE course_id = '".$course_id."' AND parent_id = $crtf_id";
			$JLMS_DB->SetQuery( $query );
			$row->add_certificates = $JLMS_DB->loadObjectList();
		} else {
			$row->add_certificates = array();
		}
		$lists = array();
		JLMS_quiz_html::ShowCertificate( $course_id, $option, $row, $lists );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
	}
}
function JLMS_quiz_cancelCertificate( $option, $id ) {
	global $Itemid;
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=quizzes&id=$id&page=certificates") );
}
function JLMS_quiz_saveCertificate( $option, $id, $page ) {
	global $my, $Itemid;
	if ($id && JLMS_GetUserType($my->id, $id) == 1) {
		if ($page == 'apply_crtf') {
			$redirect_url = "index.php?option=$option&Itemid=$Itemid&task=quizzes&id=$id&page=editA_crtf&c_id={id}";
		} else {
			$redirect_url = "index.php?option=$option&Itemid=$Itemid&task=quizzes&id=$id&page=certificates";
		}
		require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_certificates.php");
		JLMS_Certificates::JLMS_saveCertificate($id, $option, 2, $redirect_url);
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
	}
}
function JLMS_quiz_previewCertificate( $option, $id ) {
	global $my, $Itemid, $JLMS_DB;
	if ($id && JLMS_GetUserType($my->id, $id) == 1) {
		$crtf_id = intval(mosGetParam($_REQUEST, 'crtf_id', 0));
		$query = "SELECT id FROM #__lms_certificates WHERE id = '".$crtf_id."' AND course_id = '".$id."' AND crtf_type = 2";
		$JLMS_DB->SetQuery( $query );
		$certificate = $JLMS_DB->LoadResult();
		if ($certificate) {
			require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_certificates.php");
			$tm_obj = new stdClass();
			$query = "SELECT * FROM #__users WHERE id = '".$my->id."'";
			$JLMS_DB->SetQuery( $query );
			$u_data = $JLMS_DB->LoadObjectList();
			$tm_obj->username = isset($u_data[0]->username)?$u_data[0]->username:'';
			$tm_obj->name = isset($u_data[0]->name)?$u_data[0]->name:'';
			$tm_obj->crtf_spec_answer = '';
			/*$query = "SELECT b.user_answer FROM #__lms_courses as a, #__lms_spec_reg_answers as b WHERE a.id = $id AND a.spec_reg = 1 AND a.id = b.course_id AND b.user_id = ".$my->id;
			$JLMS_DB->SetQuery( $query );
			$user_answer = $JLMS_DB->LoadResult();
			if ($user_answer) {
				$tm_obj->crtf_spec_answer = $user_answer;
			}*/
			$tm_obj->is_preview = true;
			JLMS_Certificates::JLMS_outputCertificate( $certificate, $id, $tm_obj );
		}
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
	}
}
function JLMS_quiz_deleteCertificate( $option, $id ) {
	global $my, $Itemid, $JLMS_DB;
	if ($id && JLMS_GetUserType($my->id, $id) == 1) {
		$cid = mosGetParam( $_REQUEST, 'cid', array(0) );
		$cid_str = implode(',',$cid);
		$query = "SELECT id FROM #__lms_certificates WHERE id IN ($cid_str) AND course_id = '".$id."' AND crtf_type = 2";
		$JLMS_DB->setQuery( $query );
		$del_crtfs = JLMSDatabaseHelper::LoadResultArray();
		if (count( $del_crtfs )) {
			$cid_str = implode(',', $del_crtfs);
			$query = "SELECT distinct file_id FROM #__lms_certificates WHERE id IN ($cid_str)";
			$JLMS_DB->setQuery( $query );
			$del_files = JLMSDatabaseHelper::LoadResultArray();
			$query = "DELETE FROM #__lms_certificates WHERE id IN ($cid_str)";
			$JLMS_DB->setQuery( $query );
			$JLMS_DB->query();
			JLMS_deleteFiles($del_files);
		}
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=quizzes&id=$id&page=certificates") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
	}
}
function JLMS_quiz_previewQuestion( $option, $course_id, $gqp=false ) {
	global $my, $JLMS_DB, $Itemid;
	
	if ($gqp || ($course_id && JLMS_GetUserType($my->id, $course_id) == 1)){
		$c_id = intval( mosGetParam( $_REQUEST, 'c_id', 0 ) );
		$query = "SELECT a.*"
		. "\n FROM #__lms_quiz_t_quiz as a, #__lms_quiz_t_question as b"
		. "\n WHERE 1"
		. "\n AND b.c_id = '".$c_id."'"
		. "\n AND b.c_quiz_id = a.c_id"
		. "\n AND a.course_id = '".$course_id."'"
		;
		$JLMS_DB->SetQuery( $query );
		$quiz_data = $JLMS_DB->LoadObjectList();
		
		if (count( $quiz_data )) {
			require_once(_JOOMLMS_FRONT_HOME . "/includes/quiz/front.joomlaquiz.class.php");
			require_once(_JOOMLMS_FRONT_HOME . "/includes/quiz/front_html.joomlaquiz.class.php");
			JLMS_quiz_front_class::JQ_previewQuestion( $option, $course_id );
		} else {
			$query = "SELECT a.*"
			. "\n FROM #__lms_quiz_t_question as a"
			. "\n WHERE 1"
			. "\n AND a.c_id = '".$c_id."'"
			. "\n AND a.c_quiz_id = 0"
			. "\n AND a.course_id = '".$course_id."'"
			;
			$JLMS_DB->SetQuery( $query );
			$quiz_data = $JLMS_DB->LoadObjectList();
			
			if (count( $quiz_data )) {
				require_once(_JOOMLMS_FRONT_HOME . "/includes/quiz/front.joomlaquiz.class.php");
				require_once(_JOOMLMS_FRONT_HOME . "/includes/quiz/front_html.joomlaquiz.class.php");
				JLMS_quiz_front_class::JQ_previewQuestion( $option, $course_id, true );
			}
		}
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid") );
	}
}
function jlms_quiz_create_hotspot( $option, $page, $course_id, $gqp = false ) {
	global $my, $Itemid, $JLMS_DB;
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$hs_message = '';
	$hotspot = intval(mosGetParam( $_REQUEST, 'hotspot', 0));
	if (!$hotspot) {
		echo "No image associated with the question.<br> Select image from a list, re-save the question and try again";
		exit;
	}
	$image_name = '';
	$query = "SELECT c_image FROM #__lms_quiz_t_question WHERE c_id = '".$hotspot."' AND course_id = '".$course_id."'";
	$JLMS_DB->SetQuery( $query );
	$image_name = $JLMS_DB->LoadResult();
	if (!$image_name) {
		echo "No image associated with the question.<br> Select image from a list, re-save the question and try again";
		exit;
	}
	$hs_task = mosGetParam($_REQUEST, 'hs_task', '');
	if ($hs_task == 'save_hs') {
		$c_start_x = intval(mosGetParam($_REQUEST, 'c_start_x', 0));
		$c_start_y = intval(mosGetParam($_REQUEST, 'c_start_y', 0));
		$c_end_x = intval(mosGetParam($_REQUEST, 'c_end_x', 0));
		$c_end_y = intval(mosGetParam($_REQUEST, 'c_end_y', 0));
		$c_width = $c_end_x - $c_start_x;
		if ($c_width < 0) $c_width = 0;
		$c_height = $c_end_y - $c_start_y;
		if ($c_height < 0) $c_height = 0;
		$query = "DELETE FROM #__lms_quiz_t_hotspot WHERE c_question_id = '".$hotspot."'";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$query = "INSERT INTO #__lms_quiz_t_hotspot (c_question_id, c_start_x, c_start_y, c_width, c_height) "
		. "\n VALUES('".$hotspot."', '".$c_start_x."', '".$c_start_y."', '".$c_width."', '".$c_height."')";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$hs_message = "HotSpot Area saved.";
	}
	$query = "SELECT * FROM #__lms_quiz_t_hotspot WHERE c_question_id = '".$hotspot."'";
	$JLMS_DB->SetQuery( $query );
	$hotspot_data = $JLMS_DB->LoadObjectList();
	$hs_lefttop_x = 0;
	$hs_lefttop_y = 0;
	$hs_rightbottom_x = 0;
	$hs_rightbottom_y = 0;
	if (isset($hotspot_data[0])) {
		$hs_lefttop_x = $hotspot_data[0]->c_start_x;
		$hs_lefttop_y = $hotspot_data[0]->c_start_y;
		$hs_rightbottom_x = $hotspot_data[0]->c_start_x + $hotspot_data[0]->c_width;
		$hs_rightbottom_y = $hotspot_data[0]->c_start_y + $hotspot_data[0]->c_height;
	}
	$directory = 'joomlaquiz';
	$css = mosGetParam($_REQUEST,'t','');
	$iso = explode( '=', _ISO );
	// xml prolog
	echo '<?xml version="1.0" encoding="'. $iso[1] .'"?' .'>';
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<title>Create a HotSpot Area</title>
	</head>
	<body marginheight="0" marginwidth="0" topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0">
	<!--link rel="stylesheet" href="../../templates/<?php echo $css; ?>/css/template_css.css" type="text/css" /-->
	<script language="javascript" type="text/javascript">
	<!--
	var hs_step = 1;
	var hs_begin_x = 0;
	var hs_begin_y = 0;
	var hs_lefttop_x = <?php echo $hs_lefttop_x;?>;
	var hs_lefttop_y = <?php echo $hs_lefttop_y;?>;
	var hs_rightbottom_x = <?php echo $hs_rightbottom_x;?>;
	var hs_rightbottom_y = <?php echo $hs_rightbottom_y;?>;
	function getObj(name) {
	  if (document.getElementById)  {  return document.getElementById(name);  }
	  else if (document.all)  {  return document.all[name];  }
	  else if (document.layers)  {  return document.layers[name];  }
	}
	function JQ_img_click_handler(hs_event) {
		if(!hs_event) { var hs_event = window.event }
		var hs_div = getObj('div_hotspot_rec');
		var hs_img = getObj('img_hotspot');
		if (hs_step == 1) {
			hs_div.style.left = hs_event.clientX+"px";
			hs_begin_x = hs_event.clientX
			hs_div.style.top = hs_event.clientY+"px";
			hs_begin_y = hs_event.clientY;
			hs_div.style.width = 0;
			hs_div.style.height = 0;
			hs_lefttop_x = hs_event.clientX - hs_img.offsetLeft;
			hs_lefttop_y = hs_event.clientY - hs_img.offsetTop;
		}
		if (hs_step == 2) {
			hs_div.style.width = (((hs_event.clientX - hs_begin_x) > 0)?(hs_event.clientX - hs_begin_x):0)+"px";
			hs_div.style.height = (((hs_event.clientY - hs_begin_y) > 0)?(hs_event.clientY - hs_begin_y):0)+"px";
			hs_rightbottom_x = hs_event.clientX - hs_img.offsetLeft;
			hs_rightbottom_y = hs_event.clientY - hs_img.offsetTop;
		}
		if (hs_step == 1) {hs_step = 2;} else { hs_step = 1;}
		getObj('div_log').innerHTML = "Left-Top corner: X = "+hs_lefttop_x+"; Y = "+hs_lefttop_y+"<br>Right-Bottom corner: X = "+hs_rightbottom_x+"; Y = "+hs_rightbottom_y;
	}
	function JQ_img_move_handler(hs_event) {
		if(!hs_event) { var hs_event = window.event }
		if (hs_step == 2) {
			var hs_img = getObj('img_hotspot');
			var hs_div = getObj('div_hotspot_rec');
			hs_div.style.width = (((hs_event.clientX - hs_begin_x) > 0)?(hs_event.clientX - hs_begin_x):0)+"px";
			hs_div.style.height = (((hs_event.clientY - hs_begin_y) > 0)?(hs_event.clientY - hs_begin_y):0)+"px";
			hs_rightbottom_x = hs_event.clientX - hs_img.offsetLeft;
			hs_rightbottom_y = hs_event.clientY - hs_img.offsetTop;
			getObj('div_log').innerHTML = "Left-Top corner: X = "+hs_lefttop_x+"; Y = "+hs_lefttop_y+"<br>Right-Bottom corner: X = "+hs_rightbottom_x+"; Y = "+hs_rightbottom_y;
		}
	}
	function jq_SaveHotSpotArea() {
		if ((hs_lefttop_x == 0) && (hs_lefttop_y == 0) && (hs_rightbottom_x == 0) && (hs_rightbottom_y == 0)) {
			alert("HotSpot Area not defined");return false;
		} else {
			var form = document.HS_form;
			form.c_start_x.value = hs_lefttop_x;
			form.c_start_y.value = hs_lefttop_y;
			form.c_end_x.value = hs_rightbottom_x;
			form.c_end_y.value = hs_rightbottom_y;
			form.submit();
		}
	}
	//-->
	</script>
	<table cellpadding="0" cellspacing="0" border="0" width="100%" height="600px"><tr><td align="left" valign="top">
	<form method="post" action="<?php echo sefRelToAbs("index.php?tmpl=component&option=$option&Itemid=$Itemid");?>" name="HS_form">
		<div id="div_hotspot_rec" style="background-color:#FFFFFF; <?php if (preg_match('/MSIE ([0-9].[0-9]{1,2})/', $_SERVER['HTTP_USER_AGENT'])) { echo "filter:alpha(opacity=50);";}?> -moz-opacity:.50; opacity:.50; border:1px solid #000000; position:absolute; left:<?php echo $hs_lefttop_x;?>px; top:<?php echo $hs_lefttop_y;?>px; width:<?php echo ($hs_rightbottom_x - $hs_lefttop_x);?>px; height:<?php echo ($hs_rightbottom_y - $hs_lefttop_y);?>px; " onMouseDown="JQ_img_click_handler(event)" onMouseMove="JQ_img_move_handler(event)"><img src="blank.png" border="0" width="1" height="1"></div>
		<img id="img_hotspot" src="<?php echo $JLMS_CONFIG->get('live_site');?>/images/joomlaquiz/images/<?php echo $image_name;?>" onclick="JQ_img_click_handler(event)" onMouseMove="JQ_img_move_handler(event)">
		<div id="div_log"><?php echo "Left-Top corner: X = ".$hs_lefttop_x."; Y = ".$hs_lefttop_y."<br>Right-Bottom corner: X = ".$hs_rightbottom_x."; Y = ".$hs_rightbottom_y;?></div>
		<input type="hidden" name="option" value="<?php echo $option;?>">
		<input type="hidden" name="Itemid" value="<?php echo $Itemid;?>">
		<input type="hidden" name="task" value="quizzes">
		<input type="hidden" name="id" value="<?php echo $course_id;?>">
		<input type="hidden" name="no_html" value="1">
		
		<?php if(!$gqp) {?>	
			<input type="hidden" name="page" value="createhotspot">
		<?php }	
			else {?>	
			<input type="hidden" name="page" value="createhotspot_gqp">
		<?php }?>
			
		<input type="hidden" name="hotspot" value="<?php echo $hotspot;?>" />
		<input type="hidden" name="t" value="<?php echo $css;?>" />
		<input type="hidden" name="c_start_x" value="<?php echo $hs_lefttop_x;?>">
		<input type="hidden" name="c_start_y" value="<?php echo $hs_lefttop_y;?>">
		<input type="hidden" name="c_end_x" value="<?php echo $hs_rightbottom_x;?>">
		<input type="hidden" name="c_end_y" value="<?php echo $hs_rightbottom_y;?>">
		<input type="hidden" name="hs_task" value="save_hs">
		<input class="inputbox" type="button" name="cr_hotspot" value="Save HotSpot" onclick="jq_SaveHotSpotArea();" />
	</form>
	</td></tr>
	<tr><td valign="bottom" align="center">
		<div class="message"><?php echo $hs_message;?></div>
		<div><input class="inputbox" type="button" name="close_hotspot" value="Close Window" onclick="window.close();" /></div><br />
	</td></tr></table>
	</body>
	</html>
<?php
}
function jlms_quiz_upload_popup($option, $page, $course_id, $gqp = false){
	global $my, $Itemid;
	$JLMS_CONFIG = JLMSFactory::getConfig();
	if (($course_id && JLMS_GetUserType($my->id, $course_id) == 1) || $gqp) {
		if (isset($_FILES['userfile'])) {
			$userfile2=(isset($_FILES['userfile']['tmp_name']) ? $_FILES['userfile']['tmp_name'] : "");
			$userfile_name=(isset($_FILES['userfile']['name']) ? $_FILES['userfile']['name'] : "");

			$base_Dir = $JLMS_CONFIG->get('absolute_path')."/images/joomlaquiz/images/";
			if (empty($userfile_name)) {
				if(!$gqp) {
					$lnk = sefRelToAbs("index.php?tmpl=component&option=$option&amp;Itemid=$Itemid&amp;task=quizzes&amp;id=$course_id&amp;page=uploadimage");
				} else {
					$lnk = sefRelToAbs("index.php?tmpl=component&option=$option&amp;Itemid=$Itemid&amp;task=quizzes&amp;page=uploadimage_gqp");
				}
				echo "<script>alert('Please select an image to upload'); document.location.href='".$lnk."';</script>";
			}

			$filename = explode(".", $userfile_name);

			//TODO: this check together with one above doesn't work carefully !!! (filename can contain more than 1 dot)
			if (preg_match("/[^0-9a-zA-Z_]/i", $filename[0])) {
				mosErrorAlert("File must only contain alphanumeric characters and no spaces please.");
			}

			if (file_exists($base_Dir.$userfile_name)) {
				mosErrorAlert("Image ".$userfile_name." already exists.");
			}

			if ((strcasecmp(substr($userfile_name,-4),".gif")) && (strcasecmp(substr($userfile_name,-4),".jpg")) && (strcasecmp(substr($userfile_name,-4),".png")) && (strcasecmp(substr($userfile_name,-4),".bmp")) ) {
				mosErrorAlert("The file must be gif, png, jpg or bmp");
			}

			if (!move_uploaded_file ($_FILES['userfile']['tmp_name'],$base_Dir.$_FILES['userfile']['name']) || !mosChmod($base_Dir.$_FILES['userfile']['name'])) {
				mosErrorAlert("Upload of ".$userfile_name." failed");
			} else {
				mosErrorAlert("Upload of ".$userfile_name." successful");
			}
		}	
	?>
		<form name='jlms_quiz_upload_form' enctype='multipart/form-data' method='post' action='<?php echo sefRelToAbs("index.php?tmpl=component&option=$option&amp;Itemid=$Itemid");?>'>
		<input type='file' name='userfile' size='20' />
		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="Itemid" value="<?php echo $Itemid;?>" />
		<input type="hidden" name="task" value="quizzes" />

		<?php if(!$gqp) {?>
			<input type="hidden" name="page" value="uploadimage" />
		<?php }
		else {?>
			<input type="hidden" name="page" value="uploadimage_gqp" />
		<?php }?>

		<input type="hidden" name="id" value="<?php echo $course_id;?>" />
		<br /><br /><input type='submit' class="button" name='up' value='upload' />
		</form>
<?php
	}
}
?>