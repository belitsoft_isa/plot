<?php
/**
* joomla_lms.course_lpath.php
* (c) JoomaLMS eLearning Software http://www.joomlalms.com/
**/
 
############################################################################
/*
//TODO: here we will create 'todo' list of points for lpaths code refactoring:

1. functions JLMS_saveLPathChapter(), JLMS_saveLPathDoc(), JLMS_saveLPathLink() sdelat'
odnoi funkciei (v kotoruyu vsunut' 'unset(step_type)') potomu shto vse ravno v nix tol'ko redaktirovanie i step_type ne menyaetsa


*/
############################################################################

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once(_JOOMLMS_FRONT_HOME . "/joomla_lms.course_lpath.html.php");
require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_grades.lib.php");
$id 	= intval( mosGetParam( $_REQUEST, 'id', 0 ) );
$task 	= mosGetParam( $_REQUEST, 'task', '' );
if ( !in_array($task, array('save_lpath', 'scorm_contents', 'scorm_funcs', 'scorm_opensco', 'scorm_closesco', 'scorm_load_data', 'lpath_add_scorm', 'lpath_add_quiz', 'lpath_add_link', 'lpath_add_doc', 'change_lpath')) ) {
	global $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id',0);
	$pathway = array();
	$pathway[] = array('name' => _JLMS_PATHWAY_HOME, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid"), 'is_home' => true);
	$pathway[] = array('name' => $JLMS_CONFIG->get('course_name'), 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=details_course&amp;id=$course_id"), 'is_course' => true);
	$pathway[] = array('name' => _JLMS_TOOLBAR_LPATH, 'link' => sefRelToAbs("index.php?option=$option&amp;Itemid=$Itemid&amp;task=learnpaths&amp;id=$course_id"));
	JLMSAppendPathWay($pathway);
	JLMS_ShowHeading();
}
###########################		LEARNING PATH'S		##############################

//echo $task; die;

switch ($task) {
	case 'learnpaths':			JLMS_showCourseLPaths( $option );		break;

	case 'lpath_delete':		JLMS_doDeleteLPaths( $option );			break;
	case 'lpath_orderup':
	case 'lpath_orderdown':		JLMS_OrderLPaths( $option );			break;

	case 'new_lpath_scorm':
	case 'new_lpath':			JLMS_editLPath( 0, $option, $task );	break;
	case 'cancel_scormlink':
	case 'cancel_lpath':		JLMS_cancelLPath( $option );			break;
	case 'save_lpath':			JLMS_saveLPath( $option );				break;
	case 'edit_lpath':		$cid = mosGetParam( $_POST, 'cid', array(0) );
	if (!is_array( $cid )) { $cid = array(0); }
	JLMS_editLPath( $cid[0], $option, $task );			break;

	case 'edit_lpathA':			JLMS_editLPath( $id, $option, $task );	break;

	case 'change_lpath':		JLMS_changeLPath( $option );			break;

	case 'compose_lpath':		JLMS_ComposeLPath( $id, $option );		break;

	case 'lpath_add_doc':		JLMS_addDocToLPath( $id, $option );		break;
	case 'lpath_add_link':		JLMS_addLinkToLPath( $id, $option );	break;


	case 'lpath_add_quiz':		JLMS_addQuizToLPath( $id, $option );	break;

	case 'lpath_add_scorm':		JLMS_addScormToLPath( $id, $option );	break;

	case 'cancel_lpath_step':	JLMS_cancelLPathStep( $id, $option );	break;
	case 'new_lpath_chapter':	JLMS_showLPathAddChap( $id, $option );	break;

	case 'lpath_item_orderup':
	case 'lpath_item_orderdown':JLMS_OrderLPathItems( $id, $option );	break;
	case 'lpath_saveorederall':	JLMS_OrderAllLPath( $id, $option );		break;
	
	case 'lpath_item_saveorder':JLMS_SaveOrderItems( $id, $option );	break;

	case 'lpath_add_chapter':	JLMS_lpath_AddChapter( $id, $option );	break;
	case 'lpath_save_chapter':	JLMS_saveLPathChapter( $option );		break;

	case 'lpath_add_content':	JLMS_lpath_AddContent( $id, $option );	break;
	case 'lpath_save_content':	JLMS_saveLPathContent( $option );		break;

	case 'lpath_save_doc':		JLMS_saveLPathDoc( $option );			break;
	case 'lpath_save_link':		JLMS_saveLPathLink( $option );			break;
	case 'lpath_save_quiz':		JLMS_saveLPathQuiz( $option );			break;
	case 'lpath_save_scorm':	JLMS_saveLPathScorm( $option );			break;

	case 'add_lpath_step':		JLMS_lpath_AddStep( $id, $option );		break;
	case 'lpath_item_edit':		JLMS_LPath_editItem( $id, $option );	break;

	case 'lpath_item_delete':	JLMS_LPath_delItem( $id, $option );		break;

	case 'download_scorm':		JLMS_downloadSCORM( $id, $option );		break;
	
	case 'lpath_step_cond':		JLMS_cond_LPathStep( $id, $option );	break;
	case 'lpath_save_cond':		JLMS_save_Cond( $id, $option );			break;
	case 'lpath_cond_delete':	JLMS_delete_Cond( $id, $option );		break;


	case 'show_lp_content':		JLMS_showLPContent( $id, $option );		break;

	case 'new_scorm_from_library': JLMS_NewScormLink($option);			break;
	case 'save_scormlink':		JLMS_SaveScormLink($option);			break;			
	
}

function JLMS_SaveScormLink($option) {
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;

	$cid = mosGetParam( $_POST, 'cid', array(0) );
	$course_id = $JLMS_CONFIG->get('course_id');
	$JLMS_ACL = JLMSFactory::getACL();
	$is_teacher = $JLMS_ACL->isTeacher();
	if ( $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage')) {
		$use_any_library_resource = $JLMS_ACL->CheckPermissions('library', 'only_own_items') ? false : ( $JLMS_ACL->CheckPermissions('library', 'manage') ? true : false);

		for($k=0;$k<count($cid);$k++) {
			$cid_k = intval($cid[$k]);
			if ($cid_k) {
				if ($use_any_library_resource && $is_teacher) {
					$query = "SELECT file_id FROM #__lms_outer_documents WHERE id = '".$cid_k."' AND folder_flag = 3 AND allow_link = 1";
				} elseif ($is_teacher) {
					// folder_flag = 3 - this is a scorm resource
					// allow_link = 1 - sharing to courses is enabled
					// outdoc_share = 2 - public resource
					// OR outdoc_share = 1 - or resource for 'teachers'
					// outdoc_share = 0 AND owner_id = $my->id - or private resource and user is its owner
					$query = "SELECT file_id FROM #__lms_outer_documents WHERE id = '".$cid_k."' AND folder_flag = 3 AND allow_link = 1 AND (outdoc_share = 2 OR outdoc_share = 1 OR (outdoc_share = 0 AND owner_id = $my->id))";
				} else {
					// course assistant
					$query = "SELECT file_id FROM #__lms_outer_documents WHERE id = '".$cid_k."' AND folder_flag = 3 AND allow_link = 1 AND (outdoc_share = 2 OR (outdoc_share = 0 AND owner_id = $my->id))";
				}
				$JLMS_DB->SetQuery( $query );
				$scorm_id = $JLMS_DB->loadResult();

				if ($scorm_id) {
					$query = "SELECT * FROM #__lms_n_scorm WHERE id = '".$scorm_id."'";
					$JLMS_DB->SetQuery( $query );
					$n_scorm_rec_ar = $JLMS_DB->LoadObjectList();

					if (count($n_scorm_rec_ar)) {
						$scorm_package = $n_scorm_rec_ar[0]->scorm_package;

						//-------------------------#__lms_n_scorm
						$query = "INSERT INTO
									  #__lms_n_scorm
									SELECT
									      NULL as id, '".$course_id."' AS course_id, scorm_name,
									      scorm_package, summary, version,
									      maxgrade,grademethod,maxattempt,
									      updatefreq,md5hash,launch,
									      skipview,hidebrowse,hidetoc,
									      hidenav,auto,popup,params,
									      width,height,timemodified
									FROM
									  #__lms_n_scorm
									WHERE
									  id = '".$scorm_id."'";
						$JLMS_DB->SetQuery( $query );
						$JLMS_DB->query();
						$insert_scorm_id = $JLMS_DB->insertid();

						//----------------------------#__lms_n_scorm_scoes
						$query = "SELECT * FROM #__lms_n_scorm_scoes WHERE scorm = '".$scorm_id."'";
						$JLMS_DB->SetQuery( $query );
						$old_scoes = $JLMS_DB->LoadObjectList();

						$old_scoes_ids = array();
						if (count($old_scoes) && $insert_scorm_id) {
							for($i=0;$i<count($old_scoes);$i++) {
								$query = "INSERT INTO #__lms_n_scorm_scoes (scorm, manifest, organization, parent, identifier, launch, scormtype, title)"
								. "\n VALUES (".$insert_scorm_id.", ".$JLMS_DB->quote($old_scoes[$i]->manifest).", ".$JLMS_DB->quote($old_scoes[$i]->organization).", ".$JLMS_DB->quote($old_scoes[$i]->parent).", ".$JLMS_DB->quote($old_scoes[$i]->identifier).", ".$JLMS_DB->quote($old_scoes[$i]->launch).", ".$JLMS_DB->quote($old_scoes[$i]->scormtype).", ".$JLMS_DB->quote($old_scoes[$i]->title).")";
								$JLMS_DB->SetQuery( $query );
								$JLMS_DB->query();
								$old_scoes[$i]->new_id = $JLMS_DB->insertid();
								$old_scoes_ids[] = $old_scoes[$i]->id;
							}
							$old_scoes_str = implode(',', $old_scoes_ids);
							$query = "SELECT * FROM #__lms_n_scorm_scoes_data WHERE scoid IN ($old_scoes_str)";
							$JLMS_DB->SetQuery( $query );
							$old_scoes_data = $JLMS_DB->LoadObjectList();
							if (count($old_scoes_data)) {
								foreach ($old_scoes_data as $sco_data) {
									$new_sco_id = 0;
									foreach ($old_scoes as $old_sco) {
										if ($old_sco->id == $sco_data->scoid) {
											$new_sco_id = $old_sco->new_id;break;
										}
									}
									if ($new_sco_id) {
										$query = "INSERT INTO #__lms_n_scorm_scoes_data (scoid, name, value) VALUES (".$new_sco_id.", ".$JLMS_DB->quote($sco_data->name).", ".$JLMS_DB->quote($sco_data->value).")";
										$JLMS_DB->SetQuery( $query );
										$JLMS_DB->query();
									}
								}
							}
						}
						$query = "SELECT * "
						. "\n FROM #__lms_outer_documents WHERE id = '".$cid_k."'";
						$JLMS_DB->SetQuery( $query );
						$scorm_info = $JLMS_DB->loadObject();

						$query = "INSERT INTO #__lms_learn_paths (course_id, owner_id, item_id, lp_type, lpath_name)"
						. "\n VALUES ($course_id, $my->id, $insert_scorm_id, 2, ".$JLMS_DB->quote($scorm_info->doc_name).")";
						$JLMS_DB->SetQuery($query);
						$JLMS_DB->query();
						$learn_path_insert_id = $JLMS_DB->insertid();

						if ($learn_path_insert_id) {
							$query = "INSERT INTO #__lms_n_scorm_lib (lib_id, lpath_id, course_id, lib_n_scorm_id, lpath_n_scorm_id, scorm_package)"
							. "\n VALUES ($cid_k, $learn_path_insert_id, $course_id, $scorm_id, $insert_scorm_id, $scorm_package)";
							$JLMS_DB->SetQuery($query);
							$JLMS_DB->query();
						}
					}
				}
			}
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
}

function JLMS_NewScormLink($option){
	global $JLMS_DB, $my, $Itemid, $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id');
	$id = intval(mosgetparam($_REQUEST,'e_id',0));
	$JLMS_ACL = JLMSFactory::getACL();
	$lists = array();
	$is_teacher = $JLMS_ACL->isTeacher();
	if ( $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') ) {
		$use_any_library_resource = $JLMS_ACL->CheckPermissions('library', 'only_own_items') ? false : ( $JLMS_ACL->CheckPermissions('library', 'manage') ? true : false);
		//get files and folder to build tree structure
		$query = "SELECT *, file_id as file_name"
		. "\n FROM #__lms_outer_documents WHERE folder_flag IN ( 1,3 ) ORDER BY folder_flag asc,parent_id desc";

		$JLMS_DB->SetQuery( $query );
		$rows = $JLMS_DB->loadObjectList();

		$do_reassign = false;
		$count = count($rows);
		for($i=0;$i<$count;$i++) {
			if(isset($rows[$i]->folder_flag) && $rows[$i]->folder_flag == 3) { // scorm resource
				if (isset($rows[$i]->allow_link) && $rows[$i]->allow_link) {
					if ($use_any_library_resource && $is_teacher) {
						// I can add any shared resource into the course
					} elseif (!$rows[$i]->outdoc_share && $rows[$i]->owner_id == $my->id && $is_teacher) {
						// I am an owner of the resource... I can add private resource into the course
					} elseif ($rows[$i]->outdoc_share == 1 && $is_teacher) {
						// I am a teacher of the course... I can add 'teacher only' resource into the course
					} elseif ($rows[$i]->outdoc_share == 2) {
						// I can add 'public' resource into the course
					} else {
						unset($rows[$i]);
						$do_reassign = true;
					}
				} else {
					unset($rows[$i]);
					$do_reassign = true;
				}
			}
		}

		if ($do_reassign) {
			$mas = array();
			foreach ($rows as $k=>$v) {
				$mas[] = $rows[$k];	
			}
			unset($rows);
			$rows = &$mas;
		}

		$rows = JLMS_GetTreeStructure( $rows );
		$rows = AppendFileIcons_toList( $rows );

		$rows2 = array(); // set of collapsed items - we will add all ids into it to collapse all items
		$do_reassign = false;
		foreach ($rows as $i=>$v) {
			$rows2[] = $rows[$i]->id;
			if($rows[$i]->folder_flag == 1) {
				$flag = 0;

				$temp[0] = $rows[$i]->id;

				while ($flag < 1 ) {
					if(JLMS_children($temp[0], $rows) && count(JLMS_children($temp[0], $rows))) {
						$temp = JLMS_children($temp[0], $rows);

						for($m=0;$m<count($temp);$m++) {					
							if($rows[JLMS_children_i($temp[$m],$rows)]->folder_flag == 3) {
								$flag = $rows[JLMS_children_i($temp[$m],$rows)]->id;
								break(2);
							}
						}	
					} else {
						break;
					}	
				}
				if($flag == 0) {
					unset($rows[$i]);
					$do_reassign = true;
				}
			}	
		}

		if ($do_reassign) {
			$mas2 = array();
			foreach ($rows as $k=>$v) {
				$mas2[] = $rows[$k];	
			}
			unset($rows);
			$rows = &$mas2;
		}

		/*
		//this db query is unnecessary, as we have already create set of all our ids
		$query = "SELECT id FROM #__lms_outer_documents";
		$JLMS_DB->SetQuery($query);
		$rows2 = JLMSDatabaseHelper::loadResultArray();*/

		$lists = array();
		$lists['collapsed_folders'] = $rows2;

		JLMS_course_lpath_html::NewScormLink($rows, $course_id, $option, $lists);	
	}	
}

function JLMS_children_i ($id, $rows) {
	foreach ($rows as $i=>$v) {
		if($rows[$i]->id == $id) {
			return $i;		
		}
	}
}

function JLMS_children ($id,$rows) {
	$flag=array();
	foreach ($rows as $i=>$v) {
		if($rows[$i]->parent_id == $id) {
			$flag[] = $rows[$i]->id;
		}
	}
	if(count($flag)) 	return $flag;
	else 				return false;
}

function JLMS_OrderAllLPath(  $course_id,$option ) {
	global $JLMS_DB, $my, $Itemid;
	$JLMS_ACL = JLMSFactory::getACL();

	if ($JLMS_ACL->CheckPermissions('lpaths', 'order')) {

		$cid = mosGetParam( $_POST, 'cid', array(0) );
		$order = mosGetParam( $_POST, 'order', array(0) );

		if (is_array( $cid ) && !empty($cid) && is_array( $order ) && !empty($order) && (count($cid) == count($order)) ) {
			$total		= count( $cid );
			$order 		= mosGetParam( $_POST, 'order', array(0) );
			$query = "SELECT id, ordering FROM #__lms_learn_paths WHERE course_id = '".$course_id."' ORDER BY ordering, id";
			$JLMS_DB->SetQuery( $query );
			$id_array_obj = $JLMS_DB->LoadObjectList();
			
			$id_ord_array = array();
			foreach ($id_array_obj as $iao) {
				$id_ord_array[$iao->id] = $iao->ordering;
			}
			for( $i=0; $i < $total; $i++ ) {
				$quest_id = intval($cid[$i]);
				if ($quest_id) {
					if (isset($id_ord_array[$quest_id]) && ($id_ord_array[$quest_id] == (intval($order[$i]))) ) {
					
					} else {
						$query = "UPDATE #__lms_learn_paths SET ordering = '".intval($order[$i])."' WHERE id = '".$quest_id."'"
						. "\n AND course_id = '".$course_id."'";
						$JLMS_DB->SetQuery( $query );
						$JLMS_DB->query();
					}
				}
			}
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
}

function JLMS_SaveOrderItems( $id, $option ) {
	global $JLMS_DB, $Itemid;
	
	$cid 		= mosGetParam( $_POST, 'cid', array(0) );
	$course_id	= mosGetParam( $_REQUEST, 'course_id', 0 );

	$total		= count( $cid );
	$order 		= josGetArrayInts( 'order' );

	$row 		= new mos_Joomla_LMS_LearnPathStep( $JLMS_DB );

	$conditions = array();

	// update ordering values
	for( $i=0; $i < $total; $i++ ) {
		$row->load( (int) $cid[$i] );
		
		if ($row->ordering != $order[$i]) {
			$row->ordering = $order[$i];

			if (!$row->store()) {
				echo "<script> alert('".$JLMS_DB->getErrorMsg()."'); window.history.go(-1); </script>\n";
				exit();
			} // if
			// remember to updateOrder this group
			$condition = "course_id = " . $JLMS_DB->Quote( $row->course_id ) 
			. " AND lpath_id = " . $JLMS_DB->Quote( $row->lpath_id ) 
			. " AND parent_id = " . $JLMS_DB->Quote( $row->parent_id )
			;
			$found = false;
			foreach ( $conditions as $cond )
				if ($cond[1]==$condition) {
					$found = true;
					break;
				} // if
			if (!$found) $conditions[] = array($row->id, $condition);
		} // if
	} // for

	// execute updateOrder for each group
	foreach ( $conditions as $cond ) {
		$row->load( $cond[0] );
		$row->updateOrder( $cond[1] );
	}

	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$id") );
} // saveOrder

function JLMS_showLPContent( $id, $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$do_redirect = true;
	$course_id = $JLMS_CONFIG->get('course_id');
	$lpath_id = intval(mosGetParam($_REQUEST, 'lpath_id', 0));
	if ( $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') ) {
		$query = "SELECT a.* FROM #__lms_learn_path_steps as a"
		. "\n WHERE a.id = '".$id."' AND a.course_id = '".$course_id."' AND a.lpath_id = '".$lpath_id."' AND a.step_type = 4";
		$JLMS_DB->SetQuery( $query );
		$row_lp = $JLMS_DB->LoadObject();
		if ( is_object($row_lp) ) {
			if ($row_lp->step_name) {
				$do_redirect = false;
				$lists = array();
				JLMS_course_lpath_html::show_LPContent( $course_id, $lpath_id, $option, $row_lp, $lists, 'content' );
			}
		}
	}
	if ($do_redirect) {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
function JLMS_editLPath( $id, $option, $task ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG, $my;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');
	
	if ( $JLMS_ACL->CheckPermissions('lpaths', 'manage') && ( ($id && (JLMS_GetLPathCourse($id) == $course_id)) || !$id ) ) {
		$AND_ST = "";
		if( false !== ( $enroll_period = JLMS_getEnrolPeriod( $my->id, $course_id )) ) 
		{
			$AND_ST = " AND IF(is_time_related, (show_period < '".$enroll_period."' ), 1) ";	
		}
	
		$row = new mos_Joomla_LMS_LearnPath( $JLMS_DB );
		$row->addCond( $AND_ST );
		$row->load( $id );
				
		if ($id) {
		} else {
			$row->published = 0;
		}
		$lists = array();
		$lists['added_prereqs'] = array();
		$lists['select_prereqs'] = array();
		if ($id) {
			$query = "SELECT * FROM #__lms_learn_paths WHERE course_id = '".$course_id."' AND id <> ".intval($id)." ORDER BY ordering, lpath_name, id";
			$JLMS_DB->SetQuery($query);
			$lps = $JLMS_DB->loadObjectList();
			//if (!empty($lps)) {
			$lps_a = array();
			//$lps_a[] = mosHTML::makeOption(0, _JLMS_LPATH_SELECT_ITEM);
			foreach ($lps as $lp) {
				$lpa = new stdClass();
				$lpa->value = $lp->id;
				$lpa->text = $lp->lpath_name;
				$lps_a[] = $lpa;
			}
			//$lists['lps'] = mosHTML::selectList($lps_a, 'lpath_new_prereq', 'class="inputbox" size="1" ', 'value', 'text', 0 );
			$lists['select_prereqs'] = mosHTML::selectList($lps_a, 'select_prereqs[]', 'class="inputbox chzn-done" size="7" multiple="multiple" style="width: 100%; height: auto;" ', 'value', 'text', 0 );
			//}

			$query = "SELECT a.lpath_id, a.req_id AS value, a.time_minutes, b.lpath_name AS text"
			. "\n FROM #__lms_learn_path_prerequisites as a, #__lms_learn_paths as b"
			. "\n WHERE a.lpath_id = $id AND a.req_id = b.id ORDER BY b.ordering, b.lpath_name, b.id";
			$JLMS_DB->SetQuery($query);
			$lp_prereqs = $JLMS_DB->loadObjectList();		
			
			$lists['added_prereqs'] = mosHTML::selectList($lp_prereqs, 'prereqs[]', 'class="inputbox chzn-done" size="7" multiple="multiple" style="width: 100%; height: auto;" ', 'value', 'text', 0 );

			$row->scorm_height = 0;
			$row->scorm_width = 0;
			$lp_params = new JLMSParameters($row->lp_params);

			if ($row->item_id) {
				if ($row->lp_type == 1 || $row->lp_type == 2) {
					$query = "SELECT width, height, params FROM #__lms_n_scorm WHERE id = '".$row->item_id."'";
					$JLMS_DB->SetQuery( $query );
					$f_p = $JLMS_DB->LoadObject();
					if (is_object($f_p)) {
						if ($f_p->height) {
							$row->scorm_height = $f_p->height;
						}
						if ($f_p->width) {
							$row->scorm_width = $f_p->width;
						}
						$params = new JLMSParameters($f_p->params);
					} else {
						$params = new JLMSParameters('scorm_nav_bar=1'."\n".'scorm_layout=0');
					}
				} else {
					$query = "SELECT window_height FROM #__lms_scorm_packages WHERE id = '".$row->item_id."'";
					$JLMS_DB->SetQuery( $query );
					$wh = $JLMS_DB->LoadResult();
					if ($wh) {
						$row->scorm_height = $wh;
					}
					$params = new JLMSParameters('scorm_nav_bar=1'."\n".'scorm_layout=0');
				}
			} else {
				$params = new JLMSParameters('scorm_nav_bar=1'."\n".'scorm_layout=0');
			}
			$compl_msg = $lp_params->get('lpath_completion_msg','');
			if ($compl_msg) {
				$compl_msg = base64_decode($compl_msg);
			}
			$row->lpath_completion_msg = $compl_msg;//$lp_params->get('lpath_completion_msg','');
			//check if forum for course created
			$course_forum_created = intval($JLMS_CONFIG->get('course_forum'));
			$params->set('course_forum_created', $course_forum_created);
			if (!$course_forum_created) {
				$lp_params->set('add_forum', 0);
			}
			//recheck lpath forum for being disabled by disabling course forum... as fun as shit...
			/*$query = "SELECT is_active FROM #__lms_forum_details WHERE course_id = $course_id AND board_type = 2 AND group_id = $id";
			$JLMS_DB->setQuery($query);
			$tmp = $JLMS_DB->loadObject();
			if ($tmp) if ($tmp->is_active == 0) $lp_params->set('add_forum', 0);*/
				//rechecked
			//checked
			if ($row->item_id) {
				JLMS_course_lpath_html::newLPath_SCORM( $row, $lists, $option, $course_id, $params, $lp_params );
			} else {
				JLMS_course_lpath_html::newLPath( $row, $lists, $option, $course_id, $params, $lp_params );
			}
		} else {
			$row->scorm_height = '0';
			$row->scorm_width = '0';
			$params = new JLMSParameters('scorm_nav_bar=0'."\n".'scorm_layout=0');
			$lp_params = new JLMSParameters('show_in_gradebook=0'."\n".'hide_in_list=0'."\n".'add_forum=0'); //added add_forum parameter (by TPETb)
			$row->lpath_completion_msg = '';
			//check if forum for course created
			$course_forum_created = intval($JLMS_CONFIG->get('course_forum'));
			$params->set('course_forum_created', $course_forum_created);
			//checked
			if ($task == 'new_lpath_scorm') {
				JLMS_course_lpath_html::newLPath_SCORM( $row, $lists, $option, $course_id, $params, $lp_params );
			} else {
				JLMS_course_lpath_html::newLPath( $row, $lists, $option, $course_id, $params, $lp_params );
			}
		}
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
function JLMS_cancelLPath( $option ) {
	global $Itemid, $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id');
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
}
function JLMS_cancelLPathStep( $id, $option ) {
	global $Itemid, $JLMS_CONFIG;
	$course_id = $JLMS_CONFIG->get('course_id');
	$lpath_id = intval(mosGetParam($_REQUEST, 'lpath_id', 0));
	if (!$lpath_id) {
		$lpath_id = $id;
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
}

function JLMS_saveLPath( $option ) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');
	
	$lp_save_error = 'JLMS_saveLPath() function failed with error at line _line_. Reason: ';
	$id = intval(mosGetParam($_REQUEST, 'id', 0));
	
	if ( $JLMS_ACL->CheckPermissions('lpaths', 'manage') && ( ($id && (JLMS_GetLPathCourse($id) == $course_id)) || !$id ) ) {
		@set_time_limit(0);
		$row = new mos_Joomla_LMS_LearnPath( $JLMS_DB );
		if (!$row->bind( $_POST )) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
				
		//13.04.2007
		$row->lpath_name = strval(JLMS_getParam_LowFilter($_POST, 'lpath_name', ''));
		$row->lpath_name = JLMS_Process_ContentNames($row->lpath_name);
		//strval(mosGetParam($_POST, 'lpath_name', ''));
		$row->owner_id = $my->id;
		// filter input
		/* 23.02.2007 (changed by DEN)
		Embedding swf and video support + MAMBOTS support */
		$row->lpath_description = strval(JLMS_getParam_LowFilter($_POST, 'lpath_description', ''));
		$row->lpath_description = JLMS_ProcessText_LowFilter($row->lpath_description);
		$row->lpath_shortdescription = strval(JLMS_getParam_LowFilter($_POST, 'lpath_shortdescription', ''));
		$row->lpath_shortdescription = JLMS_ProcessText_HardFilter($row->lpath_shortdescription);
		

		$lpath_type= intval(mosGetParam($_REQUEST, 'lpath_type', 1)); // 2 - scorm; 1- lpath
		$scorm_params = '';
		
		if ($lpath_type == 2) {
			$params = mosGetParam( $_POST, 'params', '' );
			if (is_array( $params )) {
				$txt = array();
				foreach ( $params as $k=>$v) {
					$txt[] = "$k=$v";
				}
				$scorm_params = implode( "\n", $txt );
			}
		}

		$lp_params = '';
		$scorm_lpath_item_published = false;
		$lp_params_p= mosGetParam( $_POST, 'lp_params', '' );
		if (is_array( $lp_params_p )) {
			$txt = array();
			foreach ( $lp_params_p as $k=>$v) {
				$txt[] = "$k=$v";
				if ($k == 'published' && $v) {
					$scorm_lpath_item_published = true;
				}
			}
			$lp_params = implode( "\n", $txt );
		}					
		
		if (!$id && $lpath_type == 2) { // new SCORM - its 'published' propert was stored in the lp_params
			$row->published = $scorm_lpath_item_published ? 1 : 0;
		}

		$lpath_completion_msg = strval(JLMS_getParam_LowFilter($_POST, 'lpath_completion_msg', ''));
		if ($lpath_completion_msg) {
			$lpath_completion_msg = JLMS_ProcessText_LowFilter($lpath_completion_msg);
			$lpath_completion_msg = base64_encode($lpath_completion_msg);
			$lp_params .= "\n" . 'lpath_completion_msg=' . $lpath_completion_msg;
		}

		$row->lp_params = $lp_params;

		$scorm_id = 0;
		$lp_params = new JLMSParameters($row->lp_params);
		//$to_be_continue = false;
		$query = "SELECT lp_type FROM #__lms_learn_paths WHERE id='".$id."'";
		$JLMS_DB->setQuery($query);
		$lp_type = $JLMS_DB->loadResult(); //lp_type == 2 - from library

		if (!$id || $lp_type != 2) { //new resource or not from the library			
			$row->lp_type = 0;
			if ($lpath_type == 2) { //scorm
				$scorm_result = false;
				$scorm_upl_type = intval(mosGetParam($_REQUEST, 'scorm_upl_type', 1));
				$from_ftp = false;
				$req = 'scorm_file';
				if ($scorm_upl_type == 2) {
					$from_ftp = true;
					$req = strval(mosGetParam($_REQUEST, 'scorm_ftp_file', ''));
				}
				$scorm_height = intval(mosGetParam($_REQUEST, 'scorm_height', 0));
				$scorm_width = intval(mosGetParam($_REQUEST, 'scorm_width', 0));
				//if NEW scorm resource and file IS NOT selected
				if (!$row->id && (!isset($_FILES['scorm_file']['name'])) && !strval(mosGetParam($_REQUEST, 'scorm_ftp_file', ''))) {
					echo "<script> alert('"._JLMS_EM_SELECT_FILE."'); window.history.go(-1); </script>\n";
					exit();

				//if EXISTENT scorm resource (after edit) and file IS selected
				} elseif($row->id && ((isset($_FILES['scorm_file']['name']) && $_FILES['scorm_file']['name']) || strval(mosGetParam($_REQUEST, 'scorm_ftp_file', '')))) {
					
					//Max (05.06.2013) - fix delete old folder and file scorm when edit
					$query = "SELECT item_id FROM #__lms_learn_paths WHERE id = '".$row->id."'";
					$JLMS_DB->setQuery($query);
					$lms_n_scrom_id = $JLMS_DB->loadResult();
					if(!isset($lms_n_scrom_id)){
						$lms_n_scrom_id = 0;
					}
					//Max (05.06.2013) - fix delete old folder and file scorm when edit
					
					//nothing was uploaded ???
					if(!$_FILES['scorm_file']['name'] && !strval(mosGetParam($_REQUEST, 'scorm_ftp_file', ''))) {
						$query = "SELECT scorm_package FROM #__lms_n_scorm WHERE id = $row->file_id";
						$JLMS_DB->setQuery($query);
						$scorm_pack_id = $JLMS_DB->loadResult();
					//something was uploaded ...
					} else {
						if(mosGetParam($_REQUEST, 'scorm_ftp_file', '')) {
							$from_ftp = 1;
							$req = strval(mosGetParam($_REQUEST, 'scorm_ftp_file', ''));
						}	
						$scorm_pack_id = JLMS_uploadSCORM($req, $from_ftp, $lms_n_scrom_id); //Max (05.06.2013) - fix delete old folder and file scorm when edit
					}

					$query = "SELECT package_srv_name FROM #__lms_scorm_packages WHERE id = $scorm_pack_id";
					$JLMS_DB->setQuery($query);
					$package_srv_name = $JLMS_DB->loadResult();
					
					$reference = _JOOMLMS_SCORM_FOLDER_PATH . "/" . $package_srv_name;
					$md5hash = md5_file($reference);
								
					$query = "SELECT item_id FROM #__lms_learn_paths WHERE id = $row->id";
					$JLMS_DB->setQuery($query);
					$item_id = $JLMS_DB->loadResult();

					$query = "UPDATE #__lms_n_scorm SET height = ".$scorm_height.", width = ".$scorm_width.", scorm_package = $scorm_pack_id , md5hash = '".$md5hash."', params = ".$JLMS_DB->Quote($scorm_params)." WHERE id = $item_id";
					$JLMS_DB->SetQuery($query);
					$JLMS_DB->query();

				// NEW scorm resource and somethig was uploaded	
				} elseif((isset($_FILES['scorm_file']['name']) && $_FILES['scorm_file']['name']) || strval(mosGetParam($_REQUEST, 'scorm_ftp_file', ''))) {
					$scorm_pack_id = JLMS_uploadSCORM($req, $from_ftp);

				// if after edit of existent SCORM resource adn nothing was uploaded
				} else {
					$query = "SELECT c.id FROM #__lms_learn_paths AS a, #__lms_n_scorm AS b, #__lms_scorm_packages AS c WHERE a.id='".$row->id."' AND b.id=a.item_id AND c.id=b.scorm_package";
					$JLMS_DB->setQuery($query);
					$scorm_pack_id = $JLMS_DB->loadResult();
					
					$query = "SELECT item_id FROM #__lms_learn_paths WHERE id = $row->id";
					$JLMS_DB->setQuery($query);
					$item_id = $JLMS_DB->loadResult();

					$query = "UPDATE #__lms_n_scorm SET height = ".$scorm_height.", width = ".$scorm_width.", params = ".$JLMS_DB->Quote($scorm_params)." WHERE id = $item_id";
					$JLMS_DB->SetQuery($query);
					$JLMS_DB->query();
				}

				// if after the last DB operation we've got an error -> alert about it
				// [TODO]: remove this code?
				$msg_db = $JLMS_DB->geterrormsg();
				if ($msg_db) {
					echo "<script> alert('".$msg_db."');</script>\n";
				}

				// if the SCORM package is associated with our SCORM resource
				if ($scorm_pack_id) {
					$scorm_height = intval(mosGetParam($_REQUEST, 'scorm_height', 600));
					$scorm_width = intval(mosGetParam($_REQUEST, 'scorm_width', 100));
					$query = "UPDATE #__lms_scorm_packages SET window_height = $scorm_height WHERE id = $scorm_pack_id";
					$JLMS_DB->SetQuery($query);
					$JLMS_DB->query();
					// HERE we need to update this package (insert records into `#__lms_n_scorm`, `#__lms_n_scorm_scoes` tables
					require_once(_JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.class.php");
					require_once(_JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.lib.php");
					require_once(_JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.operate.php");

					//if our SCORM resource is NEW and n_scorm record is not created yet.
					if(!$row->id) {
						$scorm = JLMS_SCORM_ADD_INSTANCE($course_id, $scorm_pack_id);
						if (isset($scorm->id) && $scorm->id) {
							$scorm_result = true;
							$row->item_id = $scorm->id;
							$scorm_id = $scorm->id;
							$row->lp_type = 1;
							if ($scorm_params) {
								$query = "UPDATE #__lms_n_scorm SET height = ".$scorm_height.", width = ".$scorm_width.", params = ".$JLMS_DB->Quote($scorm_params)." WHERE id = $scorm_id";
								$JLMS_DB->SetQuery($query);
								$JLMS_DB->query();
							}
						} else {
							if(!$row->id) {
								$query = "SELECT * FROM #__lms_scorm_packages WHERE id = $scorm_pack_id";
								$JLMS_DB->SetQuery($query);
								$del_scorm = $JLMS_DB->LoadObject();
								if (is_object($del_scorm)) {
									require_once(_JOOMLMS_FRONT_HOME . "/includes/jlms_dir_operation.php");
									$scorm_folder = JPATH_SITE . "/" . _JOOMLMS_SCORM_FOLDER . "/";
									deldir( $scorm_folder . $del_scorm->folder_srv_name . "/" );
									unlink( $scorm_folder . $del_scorm->package_srv_name );
									$query = "DELETE FROM #__lms_scorm_packages WHERE id = $scorm_pack_id";
									$JLMS_DB->SetQuery( $query );
									$JLMS_DB->query();
								}
								$lp_save_error_tmp = str_replace('_line_', __LINE__, $lp_save_error) . 'failed to add SCORM instance.';
								echo "<script> alert('".$lp_save_error_tmp."'); window.history.go(-1); </script>\n";
								exit();
							}
						}
					}
				} else {
					//unnecessary ERROR ???
					if(!$row->id) {
						$lp_save_error_tmp = str_replace('_line_', __LINE__, $lp_save_error) . 'failed to upload SCORM instance.';
						echo "<script> alert('".$lp_save_error_tmp."'); window.history.go(-1); </script>\n";
						exit();
					}
				}
				if (!$row->lpath_name) {
					$def_scorm_name = "SCORM package";
					if ($from_ftp && $req) {
						$def_scorm_name = $req;
					}
					$row->lpath_name = (isset($_FILES['scorm_file']['name']) ? $_FILES['scorm_file']['name'] : $def_scorm_name);
				}
			}
//			$query = "SELECT max(ordering) FROM #__lms_learn_paths WHERE course_id = '".$course_id."'";
//			$JLMS_DB->SetQuery( $query );
//			$new_ordering = $JLMS_DB->LoadResult();
//			$row->ordering = intval($new_ordering) + 1;
			
			if($row->id && $lp_type != 2) {
				$row->lp_type = 1;
			}
			
			$days = intval(mosGetParam($_POST, 'days', ''));
			$hours = intval(mosGetParam($_POST, 'hours', ''));
			$mins = intval(mosGetParam($_POST, 'mins', ''));
			
			if( $row->is_time_related ) {
				$row->show_period = JLMS_HTML::_('showperiod.getminsvalue', $days, $hours, $mins );
			}

			if (!$row->check()) {
				echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
				exit();
			}
			if (!$row->store()) {
				echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
				exit();
			}
		} else {
			$query = "SELECT * FROM #__lms_learn_paths WHERE id = '".$row->id."'";
			$JLMS_DB->SetQuery( $query );
			$old_lp = $JLMS_DB->LoadObject();
			if (is_object($old_lp)) {
				if ($old_lp->item_id) {
					$scorm_height = intval(mosGetParam($_REQUEST, 'scorm_height', 600));
					$scorm_width = intval(mosGetParam($_REQUEST, 'scorm_width', 600));
					if ($old_lp->lp_type == 1 || $old_lp->lp_type == 2) {
						// n_scorm API
						$scorm_id = intval($old_lp->item_id);

						$query = "UPDATE #__lms_n_scorm SET height = ".$scorm_height.", width = ".$scorm_width.", params = ".$JLMS_DB->Quote($scorm_params)." WHERE id = $scorm_id";
						$JLMS_DB->SetQuery( $query );
						$JLMS_DB->query();
					} elseif ($old_lp->lp_type == 0) {
						// old scorm (need to be upgraded into n_scorm
						$scorm_pack_id = intval($old_lp->item_id);
						$query = "SELECT * FROM #__lms_n_scorm WHERE course_id = $course_id AND scorm_package = $scorm_pack_id";
						$JLMS_DB->SetQuery($query);
						$scorm_n = $JLMS_DB->LoadObject();
						if (is_object($scorm_n)) {
							$query = "SELECT * FROM #__lms_scorm_packages WHERE id = $scorm_n->scorm_package";
							$JLMS_DB->SetQuery($query);
							$scorm_ref = $JLMS_DB->LoadObject();
							if (is_object($scorm_ref)) {
								$query = "UPDATE #__lms_scorm_packages SET window_height = $scorm_height WHERE id = $scorm_n->scorm_package";
								$JLMS_DB->SetQuery($query);
								$JLMS_DB->query();
								$query = "UPDATE #__lms_n_scorm SET height = ".$scorm_height.", width = ".$scorm_width.", params = ".$JLMS_DB->Quote($scorm_params)." WHERE id = $scorm_n->id";
								$JLMS_DB->SetQuery($query);
								$JLMS_DB->query();
							} else {
								$lp_save_error_tmp = str_replace('_line_', __LINE__, $lp_save_error) . 'old SCORM instance not found.';
								echo "<script> alert('".$lp_save_error_tmp."'); window.history.go(-1); </script>\n";
								exit();
							}
						} else {
							$query = "SELECT * FROM #__lms_scorm_packages WHERE id = $scorm_pack_id AND course_id = $course_id";
							$JLMS_DB->SetQuery($query);
							$scorm_pack = $JLMS_DB->LoadObject();
							if (is_object($scorm_pack)) {
								$query = "UPDATE #__lms_scorm_packages SET window_height = $scorm_height WHERE id = $scorm_pack_id";
								$JLMS_DB->SetQuery($query);
								$JLMS_DB->query();
								require_once(_JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.class.php");
								require_once(_JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.lib.php");
								require_once( _JOOMLMS_FRONT_HOME . "/includes/n_scorm/lms_scorm.operate.php");
								$scorm_n = JLMS_SCORM_ADD_INSTANCE($course_id, $scorm_pack_id);
								if (isset($scorm_n->id) && $scorm_n->id) {
									$query = "UPDATE #__lms_n_scorm SET height = ".$scorm_height.", width = ".$scorm_width.", params = ".$JLMS_DB->Quote($scorm_params)." WHERE id = $scorm_n->id";
									$JLMS_DB->SetQuery($query);
									$JLMS_DB->query();
								}
							} else {
								$lp_save_error_tmp = str_replace('_line_', __LINE__, $lp_save_error) . 'SCORM Package instance not found.';
								echo "<script> alert('".$lp_save_error_tmp."'); window.history.go(-1); </script>\n";
								exit();
							}
						}
						if (isset($scorm_n->id) && $scorm_n->id) {
							$row->lp_type = 1;
							$row->item_id = $scorm_n->id;
						} else {
							$lp_save_error_tmp = str_replace('_line_', __LINE__, $lp_save_error) . 'Failed to update SCORM instance.';
							echo "<script> alert('".$lp_save_error_tmp."'); window.history.go(-1); </script>\n";
							exit();
						}
					}
				}
			}
			if (!$row->check()) {
				echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
				exit();
			}
			if (!$row->store()) {
				echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
				exit();
			}
		}

        $isActive = isset($lp_params_p['add_forum']) ? $lp_params_p['add_forum'] : 0;

		$query = "UPDATE `#__lms_forum_details` AS fd SET fd.is_active = ".(int)$isActive.", fd.need_update = 1
			WHERE fd.board_type IN (SELECT f.id FROM `#__lms_forums` AS f WHERE f.forum_level = 1) 
				AND fd.course_id = '".$row->course_id."' AND fd.group_id = '".$row->id."'";
		$JLMS_DB->setQuery($query);
		if( $JLMS_DB->query() ) 
		{		
			$forum = & JLMS_SMF::getInstance();
			if( $forum )
				$forum->applyChanges( $row->course_id );
		}	
		
			//save prerequisites			
		$prereqs = JRequest::getVar('prereqs', array());
				
		if ( !empty($prereqs) ) 
		{
			$query = "SELECT req_id FROM #__lms_learn_path_prerequisites WHERE lpath_id = '$id'";
			$JLMS_DB->SetQuery($query);
			$addedPrereqs = JLMSDatabaseHelper::loadResultArray();						
			
			$addPrereqs = array_diff($prereqs, $addedPrereqs);
			$remPrereqs = array_diff($addedPrereqs, $prereqs);	
							
			if( !empty($remPrereqs) ) 
			{
				$query = "DELETE FROM #__lms_learn_path_prerequisites WHERE lpath_id = '$id' AND req_id IN (".implode(',', $remPrereqs).")";
				$JLMS_DB->SetQuery($query);
				$JLMS_DB->query();
			}
						
			$values = array();
			foreach($addPrereqs as $addPrereq){
				if( $addPrereq ) 
				{
					$values[] = "($id, $addPrereq, 0)";
				}
			}
			
			if( !empty($values) ) 
			{
				$query = "INSERT INTO #__lms_learn_path_prerequisites(lpath_id, req_id, time_minutes) VALUES ".implode(',', $values);
				$JLMS_DB->SetQuery($query);
				$JLMS_DB->query();
			}
		} else {
			$query = "DELETE FROM #__lms_learn_path_prerequisites WHERE lpath_id = '$id'";
			$JLMS_DB->SetQuery($query);
			$JLMS_DB->query();			
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
}

function JLMS_changeLPath( $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');

	if ( $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'publish') ) {
		$state = intval(mosGetParam($_REQUEST, 'state', 0));
		if ($state != 1) { $state = 0; }
		$cid = mosGetParam( $_REQUEST, 'cid', array(0) );
		$cid2 = intval(mosGetParam( $_REQUEST, 'cid2', 0 ));
		if ($cid2) {
			$cid = array();
			$cid[] = $cid2;
		}
		if (!is_array( $cid )) {
			$cid = array(0);
		}
		if (!is_array( $cid ) || count( $cid ) < 1) {
			$action = 1 ? 'Publish' : 'Unpublish';
			echo "<script> alert('Select an item to $action'); window.history.go(-1);</script>\n";
			exit();
		}
		$cids = implode( ',', $cid );
		$query = "UPDATE #__lms_learn_paths"
		. "\n SET published = $state"
		. "\n WHERE id IN ( $cids ) AND course_id = $course_id"
		;
		$JLMS_DB->setQuery( $query );
		if (!$JLMS_DB->query()) {
			echo "<script> alert('".$JLMS_DB->getErrorMsg()."'); window.history.go(-1); </script>\n";
			exit();
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
}

function JLMS_showCourseLPaths( $option ) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG, $JLMS_SESSION;
	$JLMS_ACL = JLMSFactory::getACL();

	$is_teacher = $JLMS_ACL->isTeacher();

	$course_id = $JLMS_CONFIG->get('course_id');
	
	$AND_ST = "";
	if( false !== ( $enroll_period = JLMS_getEnrolPeriod( $my->id, $course_id )) ) 
	{
		$AND_ST = " AND IF(a.is_time_related, (a.show_period < '".$enroll_period."' ), 1) ";	
	}
	
	if ( $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'view') ) {
		$JLMS_SESSION->clear('redirect_to_learnpath');
		if ($JLMS_ACL->CheckPermissions('lpaths', 'view_all')) {
			$query = "SELECT a.*, b.name as author_name"
			. "\n FROM #__lms_learn_paths as a"
			. "\n LEFT JOIN #__users as b ON a.owner_id = b.id"
			. "\n WHERE a.course_id = '".$course_id."'".$AND_ST
			."\n ORDER BY a.ordering, a.lpath_name";
		} elseif($JLMS_ACL->CheckPermissions('lpaths', 'view')) {
			//$query = "SELECT a.*, b.user_status as r_status, b.start_time as r_start, b.end_time as r_end"
			//. "\n FROM #__lms_learn_paths as a LEFT JOIN #__lms_learn_path_results as b ON a.id = b.lpath_id AND b.course_id = '".$course_id."' AND b.user_id = '".$my->id."'"
			$query = "SELECT a.*, '' as r_status, '' as r_start, '' as r_end, b.name as author_name"
			. "\n FROM #__lms_learn_paths as a LEFT JOIN #__users as b ON a.owner_id = b.id"
			. "\n WHERE a.course_id = '".$course_id."'".$AND_ST
			. "\n AND a.published = 1"
			."\n ORDER BY a.ordering, a.lpath_name";
		}
		$JLMS_DB->SetQuery( $query );
		$lpaths = $JLMS_DB->LoadObjectList();

		$new_lpaths = array();
		for($i=0;$i<count($lpaths);$i++) {
			$is_unset = false;
			if($lpaths[$i]->lp_type == 2) { // 0 - native; 1 - scorm; 2 - scorm from library
				$outer_doc = null;
				$query = "SELECT scorm_package, params as scorm_params, height as scorm_height, width as scorm_width FROM #__lms_n_scorm WHERE id = '".$lpaths[$i]->item_id."'";
				$JLMS_DB->SetQuery( $query );

				unset($outer_doc);
				$scorm_info = $JLMS_DB->LoadObject();
				if (is_object($scorm_info)) {	
					$lpaths[$i]->scorm_params = $scorm_info->scorm_params;
					$lpaths[$i]->scorm_height = $scorm_info->scorm_height;
					$lpaths[$i]->scorm_width = $scorm_info->scorm_width;
					$scorm_package = $scorm_info->scorm_package;
					$query = "SELECT id FROM #__lms_n_scorm WHERE scorm_package = '".$scorm_package."' AND course_id = 0";
					$JLMS_DB->SetQuery( $query );
					$scorm_lib_id = $JLMS_DB->LoadResult();

					$query = "SELECT outdoc_share, owner_id, allow_link FROM #__lms_outer_documents WHERE file_id = '".$scorm_lib_id."' AND folder_flag = 3";
					$JLMS_DB->SetQuery( $query );
					$outer_doc = $JLMS_DB->LoadObject();
				}
				if(isset($outer_doc) && is_object($outer_doc) && isset($outer_doc->allow_link) && $outer_doc->allow_link == 1 ) {
					// 'share to courses' is still enabled
				} elseif($is_teacher) {
					// 'share to courses' is disabled.... show 'resource is not available' message to teacher
					$lpaths[$i]->lpath_name = _JLMS_LP_RESOURSE_ISUNAV;
					//$lpaths[$i]->is_link = true;
					unset($lpaths[$i]->scorm_params);
				} else {
					// hide 'unavailable' resource for learners
					$is_unset = true;
				}
			} elseif ($lpaths[$i]->lp_type == 1) {
				$query = "SELECT params, height as scorm_height, width as scorm_width FROM #__lms_n_scorm WHERE id = '".$lpaths[$i]->item_id."'";
				$JLMS_DB->SetQuery( $query );
				$scorm_info1 = $JLMS_DB->LoadObject();
				if (is_object($scorm_info1) && isset($scorm_info1->params)) {
					$lpaths[$i]->scorm_params = $scorm_info1->params;
					$lpaths[$i]->scorm_height = $scorm_info1->scorm_height;
					$lpaths[$i]->scorm_width = $scorm_info1->scorm_width;	
				}
			}
			if (!$is_unset) {
				$new_lpaths[] = $lpaths[$i];
			}
		}

		unset($lpaths);
		$lpaths = & $new_lpaths;

		if ($JLMS_ACL->CheckPermissions('lpaths', 'view_all')) {
			//check if prerequisites are configured? to show this status on the 'publish/unpublish' status button
			$lpath_ids = array();
			foreach ($lpaths as $lpath) {
				$lpath_ids[] = $lpath->id;
			}
			if (!empty($lpath_ids)) {
				$lpath_ids_str = implode(',', $lpath_ids);
				// get the list of prerequisites
				// SELECT from two tables (+ #__lms_learn_paths) - because the prereq lpath could be deleted...
				$query = "SELECT a.* FROM #__lms_learn_path_prerequisites as a, #__lms_learn_paths as b"
				. "\n WHERE a.lpath_id IN ($lpath_ids_str) AND a.req_id = b.id";
				$JLMS_DB->SetQuery($query);
				$prereqs = $JLMS_DB->LoadObjectList();
				if (!empty($prereqs)) {
					// compare lists of prereqs to the lists of lpaths.
					$i = 0;
					while ($i < count($lpaths)) {
						$is_prereqs = false;
						$o = 0;
						while ($o < count($prereqs)) {
							if ($prereqs[$o]->lpath_id == $lpaths[$i]->id) {
								$is_prereqs = true; break;
							}
							$o ++;
						}
						if ($is_prereqs) {
							$lpaths[$i]->is_prereqs = true;
						}
						$i ++;
					}
				}
			}
		} else {
			require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_grades.lib.php");
			$user_ids = array();
			$user_ids[] = $my->id;
			JLMS_LP_populate_results($course_id, $lpaths, $user_ids);

			// 13 August 2007 (DEN) Check for prerequisites.
			// 1. get the list of lpath_ids.
			$lpath_ids = array();
			foreach ($lpaths as $lpath) {
				$lpath_ids[] = $lpath->id;
			}
			if (!empty($lpath_ids)) {
				$lpath_ids_str = implode(',', $lpath_ids);
				// 2. get the list of prerequisites
				// SELECT from two tables (+ #__lms_learn_paths) - because the prereq lpath could be deleted...
				$query = "SELECT a.* FROM #__lms_learn_path_prerequisites as a, #__lms_learn_paths as b"
				. "\n WHERE a.lpath_id IN ($lpath_ids_str) AND a.req_id = b.id";
				$JLMS_DB->SetQuery($query);
				$prereqs = $JLMS_DB->LoadObjectList();
				
				if (!empty($prereqs)) {
					// 3. compare lists of prereqs to the lists of lpaths.
					$i = 0;
					while ($i < count($lpaths)) {
						$is_hidden = false;
						$o = 0;
						while ($o < count($prereqs)) {
							if ($prereqs[$o]->lpath_id == $lpaths[$i]->id) {
								$j = 0;
								while ($j < count($lpaths)) {
									if ($lpaths[$j]->id == $prereqs[$o]->req_id) {
										if (!$lpaths[$j]->item_id) {
											if (empty($lpaths[$j]->r_status)) {
												$is_hidden = true;
												break;
											} else {
												$end_time = strtotime($lpaths[$j]->r_end);
												$current_time = strtotime(JHTML::_('date', null, "Y-m-d H:i:s"));
												if($current_time > $end_time && (($current_time - $end_time) < ($prereqs[$o]->time_minutes*60))){
													$is_hidden = true;
													break;	
												}
											}
										} else {
											if (empty($lpaths[$j]->s_status)) {
												$is_hidden = true;
												break;
											} else {
												$end_time = strtotime($lpaths[$j]->r_end);
												$current_time = strtotime(JHTML::_('date', null, "Y-m-d H:i:s"));
												if($current_time > $end_time && (($current_time - $end_time) < ($prereqs[$o]->time_minutes*60))){
													$is_hidden = true;
													break;	
												}
											}
										}
									}
									$j ++;
								}
							}
							$o ++;
						}
						$lpaths[$i]->is_hidden = $is_hidden;
						$i ++;
					}
				}
			}
		}
		
		//echo '<pre>';
		//print_r($lpaths);
		//echo '</pre>';
		
		$lms_titles_cache = & JLMSFactory::getTitles();
		$lms_titles_cache->setArray('learn_paths', $lpaths, 'id', 'lpath_name');
		JLMS_course_lpath_html::showCourseLPaths( $course_id, $option, $lpaths );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$course_id") );
	}
}

function JLMS_OrderLPaths( $option ) {
	global $JLMS_DB, $task, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');

	if ($JLMS_ACL->CheckPermissions('lpaths', 'order')) {
		$order_id = intval(mosGetParam($_REQUEST, 'row_id', 0));
		$query = "SELECT id FROM #__lms_learn_paths WHERE course_id = '".$course_id."' ORDER BY ordering";
		$JLMS_DB->SetQuery( $query );
		$id_array = JLMSDatabaseHelper::LoadResultArray();
		if (count($id_array)) {
			$i = 0;$j = 0;
			while ($i < count($id_array)) {
				if ($id_array[$i] == $order_id) { $j = $i;}
				$i++;
			}
			$do_update = true;
			if (($task == 'lpath_orderup') && ($j) ) {
				$tmp = $id_array[$j-1];
				$id_array[$j-1] = $id_array[$j];
				$id_array[$j] = $tmp;
			} elseif (($task == 'lpath_orderdown') && ($j < (count($id_array)-1)) ) {
				$tmp = $id_array[$j+1];
				$id_array[$j+1] = $id_array[$j];
				$id_array[$j] = $tmp;
			}
			$i = 0;
			foreach ($id_array as $lpath_id) {
				$query = "UPDATE #__lms_learn_paths SET ordering = '".$i."' WHERE id = '".$lpath_id."' and course_id = '".$course_id."'";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$i ++;
			}
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
}

function JLMS_OrderLPathItems( $lpath_id, $option ) {
	global $JLMS_DB, $task, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$order_id = intval(mosGetParam($_REQUEST, 'row_id', 0));
	$course_id = $JLMS_CONFIG->get('course_id');

	if ( $lpath_id && $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id)) {
		$query = "SELECT parent_id FROM #__lms_learn_path_steps WHERE id = '".$order_id."'";
		$JLMS_DB->SetQuery( $query );
		$item_parent = $JLMS_DB->LoadResult();
		$query = "SELECT id FROM #__lms_learn_path_steps WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND parent_id = '".$item_parent."' ORDER BY ordering";
		$JLMS_DB->SetQuery( $query );
		$id_array = JLMSDatabaseHelper::LoadResultArray();
		
		if (count($id_array)) {
			$i = 0;$j = 0;
			while ($i < count($id_array)) {
				if ($id_array[$i] == $order_id) { $j = $i;}
				$i++;
			}
			
			$do_update = true;
			if (($task == 'lpath_item_orderup') && ($j) ) {
				$tmp = $id_array[$j-1];
				$id_array[$j-1] = $id_array[$j];
				$id_array[$j] = $tmp;
			} elseif (($task == 'lpath_item_orderdown') && ($j < (count($id_array)-1)) ) {
				$tmp = $id_array[$j+1];
				$id_array[$j+1] = $id_array[$j];
				$id_array[$j] = $tmp;
			}
			
			$i = 1;
			foreach ($id_array as $lpath_item_id) {
				$query = "UPDATE #__lms_learn_path_steps SET ordering = '".$i."' WHERE id = '".$lpath_item_id."' and course_id = '".$course_id."' and lpath_id = '".$lpath_id."'";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$i ++;
			}
		}
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}

function JLMS_doDeleteLPaths( $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');
	if ( $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') ) {
		$cid = mosGetParam( $_POST, 'cid', array(0) );
		if (!is_array( $cid )) {
			$cid = array(0);
		}
		$i = 0;
		while ($i < count($cid)) {
			$cid[$i] = intval($cid[$i]);
			$i ++;
		}
		$cids_pre = implode(',',$cid);
		// delete JLMS learning paths (non SCORM)
		$query = "SELECT id FROM #__lms_learn_paths WHERE id IN ($cids_pre) AND course_id = '".$course_id."' AND item_id = 0";
		$JLMS_DB->SetQuery( $query );
		$cid = JLMSDatabaseHelper::LoadResultArray();
		if (count($cid)) {
			$cids = implode(',',$cid);
			
			//topics
				$query = "DELETE FROM #__lms_topic_items WHERE item_id IN ($cids) AND item_type = 7";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
			//-------------
			
			$query = "DELETE FROM #__lms_learn_paths WHERE id IN ($cids)";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();

			// 13 August 2007 (DEN) - deleting of prerequisites
			$query = "DELETE FROM #__lms_learn_path_prerequisites WHERE lpath_id IN ($cids) OR req_id IN ($cids)";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();

			$query = "DELETE FROM #__lms_learn_path_results WHERE lpath_id IN ($cids) AND course_id = '".$course_id."'";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			$query = "SELECT id FROM #__lms_learn_path_steps WHERE lpath_id IN ($cids) AND course_id = '".$course_id."'";
			$JLMS_DB->SetQuery( $query );
			$s_cid =  JLMSDatabaseHelper::LoadResultArray();
			if (count($s_cid)) {
				$s_cids = implode(',',$s_cid);
				$query = "DELETE FROM #__lms_learn_path_steps WHERE id IN ($s_cids)";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$query = "DELETE FROM #__lms_learn_path_step_results WHERE step_id IN ($s_cids)";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
				$query = "DELETE FROM #__lms_learn_path_conds WHERE course_id = '".$course_id."' AND lpath_id IN ($cids) AND step_id IN ($s_cids)";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
			}

			//find and delete forum boards and groups of chosen L_paths
			require_once(_JOOMLMS_FRONT_HOME . "/includes/lms_del_operations.php");
			JLMS_DelOp_deleteLpaths( $cid );
			//deleted
		}
		//delete SCORM's
		$query = "SELECT id, lp_type, item_id FROM #__lms_learn_paths WHERE id IN ($cids_pre) AND course_id = '".$course_id."' AND item_id <> 0";
		$JLMS_DB->SetQuery( $query );
		$i_cid = $JLMS_DB->LoadObjectList();
               
		if (count($i_cid)) {
			
			//topics
				$query = "DELETE FROM #__lms_topic_items WHERE item_id IN ($cids_pre) AND item_type = 7";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
			//-------------
            
            $query = "DELETE FROM #__lms_learn_path_steps WHERE item_id IN ($cids_pre) AND course_id = '".$course_id."' AND step_type = 6";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			
			//$i_cids = implode(',',$i_cid);
			$query = "DELETE FROM #__lms_learn_paths WHERE id IN ($cids_pre) AND course_id = '".$course_id."' AND item_id <> 0";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();

			$sc_cid = array();$scn_cid = array();
			$lppr_cid = array();
			$lib_table_records = array();
			foreach ($i_cid as $ic) {
				if ($ic->lp_type == 1 || $ic->lp_type == 2) {
					$scn_cid[] = $ic->item_id;
				} else {
					$sc_cid[] = $ic->item_id;
				}
				if ($ic->lp_type == 2) {
					$lib_table_records[] = $ic->id;
				}
				$lppr_cid[] = $ic->id;
			}
			if (count($lib_table_records)) {
				$lib_table_records_str = implode(',',$lib_table_records);
				$query = "DELETE FROM #__lms_n_scorm_lib WHERE lpath_id IN ($lib_table_records_str)";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
			}
			if (!empty($lppr_cid)) {
				$lpprs = implode(',',$lppr_cid);
				// 13 August 2007 (DEN) - deleting of prerequisites
				$query = "DELETE FROM #__lms_learn_path_prerequisites WHERE lpath_id IN ($lpprs) OR req_id IN ($lpprs)";
				$JLMS_DB->SetQuery( $query );
				$JLMS_DB->query();
			}
			if (count($scn_cid)) {
				$scn_cids = implode(',',$scn_cid);
				$query = "SELECT id, scorm_package FROM #__lms_n_scorm WHERE id IN ($scn_cids) AND course_id = ".$course_id;
				// this 'course_id' additional check will prevent removing of the library scorm_pack
				$JLMS_DB->SetQuery( $query );
				$scn_r_cid = $JLMS_DB->LoadObjectList();
				if (count($scn_r_cid)) {
					$scn_cid = array();
					foreach ($scn_r_cid as $irc) {
						$scn_cid[] = $irc->id;
						$sc_cid[] = $irc->scorm_package;
					}
					if (count($scn_cid)) {
						$scn_cids = implode(',',$scn_cid);
						$query = "DELETE FROM #__lms_n_scorm WHERE id IN ($scn_cids) AND course_id = ".$course_id;
						$JLMS_DB->SetQuery( $query );
						$JLMS_DB->query();
						$query = "SELECT id FROM #__lms_n_scorm_scoes WHERE scorm IN ($scn_cids)";
						$JLMS_DB->SetQuery( $query );
						$scoes_ids = JLMSDatabaseHelper::LoadResultArray();
						if (count($scoes_ids)) {
							$query = "DELETE FROM #__lms_n_scorm_scoes WHERE scorm IN ($scn_cids)";
							$JLMS_DB->SetQuery( $query );
							$JLMS_DB->query();
							$sco_cids = implode(',',$scoes_ids);
							$query = "DELETE FROM #__lms_n_scorm_scoes_data WHERE scoid IN ($sco_cids)";
							$JLMS_DB->SetQuery( $query );
							$JLMS_DB->query();
							$query = "DELETE FROM #__lms_n_scorm_scoes_track WHERE scoid IN ($sco_cids)";
							$JLMS_DB->SetQuery( $query );
							$JLMS_DB->query();
							$query = "DELETE FROM #__lms_n_scorm_seq_mapinfo WHERE scoid IN ($sco_cids)";
							$JLMS_DB->SetQuery( $query );
							$JLMS_DB->query();
							$query = "DELETE FROM #__lms_n_scorm_seq_objective WHERE scoid IN ($sco_cids)";
							$JLMS_DB->SetQuery( $query );
							$JLMS_DB->query();
							$query = "DELETE FROM #__lms_n_scorm_seq_rolluprulecond WHERE scoid IN ($sco_cids)";
							$JLMS_DB->SetQuery( $query );
							$JLMS_DB->query();
							$query = "DELETE FROM #__lms_n_scorm_seq_rulecond WHERE scoid IN ($sco_cids)";
							$JLMS_DB->SetQuery( $query );
							$JLMS_DB->query();
							$query = "DELETE FROM #__lms_n_scorm_seq_ruleconds WHERE scoid IN ($sco_cids)";
							$JLMS_DB->SetQuery( $query );
							$JLMS_DB->query();
						}
					}
				}
			}
			if (count($sc_cid)) {
				$sc_cids = implode(',',$sc_cid);
				$query = "SELECT * FROM #__lms_scorm_packages WHERE id IN ($sc_cids) AND course_id = '".$course_id."'";
				// this 'course_id' additional check will prevent removing of the library scorm_pack
				$JLMS_DB->SetQuery( $query );
				$del_scorms = $JLMS_DB->LoadObjectList();
				if (count($del_scorms)) {
					$scorm_ids = array();
					require_once(_JOOMLMS_FRONT_HOME . "/includes/jlms_dir_operation.php");
					$scorm_folder = JPATH_SITE . "/" . _JOOMLMS_SCORM_FOLDER . "/";
					foreach ($del_scorms as $del_scorm) {
						deldir( $scorm_folder . $del_scorm->folder_srv_name . "/" );
						unlink( $scorm_folder . $del_scorm->package_srv_name );
						$scorm_ids[] = $del_scorm->id;
					}
					$sc_cids = implode(',',$scorm_ids);
					$query = "DELETE FROM #__lms_scorm_packages WHERE id IN ($sc_cids)";
					$JLMS_DB->SetQuery( $query );
					$JLMS_DB->query();
					$query = "DELETE FROM #__lms_scorm_sco WHERE content_id IN ($sc_cids)";
					$JLMS_DB->SetQuery( $query );
					$JLMS_DB->query();
				}
			}
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
}

// + (may be) dop. proverki v SQL zapros shtoby lishnego ne podcepil iz LEFT JOIn tablic (lishnee - eto to gde user ne owner)
// + (IMPORTANT) posmotret' nafiga v zaprose c.file_name (tip) shtoby iconki failov pravil'nye narisovat'
function JLMS_ComposeLPath( $id, $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG, $my;
	$JLMS_ACL = JLMSFactory::getACL();

	$is_teacher = $JLMS_ACL->isTeacher(); 

	$course_id = $JLMS_CONFIG->get('course_id');
	//$course_id = intval(mosGetParam($_REQUEST, 'course_id', 0));
	if ( $id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($id) == $course_id) ) {
		//, d.ref_step, d.cond_type, d.cond_value, e.step_name as ref_name"
		$query = "SELECT a.*, a.step_name as doc_name, c.file_name, b.file_id, b.folder_flag, f.link_href, e.c_title"
		. "\n FROM #__lms_learn_path_steps as a"
		. "\n LEFT JOIN #__lms_documents as b ON a.item_id = b.id AND a.step_type = 2"
		. "\n LEFT JOIN #__lms_files as c ON b.file_id = c.id"
		. "\n LEFT JOIN #__lms_links as f ON a.item_id = f.id AND a.step_type = 3"
		. "\n LEFT JOIN #__lms_quiz_t_quiz as e ON a.item_id = e.c_id AND a.step_type = 5"
		//. "\n LEFT JOIN #__lms_learn_path_conds as d ON a.cond_id = d.id"
		//. "\n LEFT JOIN #__lms_learn_path_steps as e ON d.ref_step = e.id"
		. "\n WHERE a.lpath_id = '".$id."' AND a.course_id = '".$course_id."'"
		. "\n ORDER BY a.parent_id, a.ordering"
		;
		$JLMS_DB->SetQuery( $query );
		$lpath = $JLMS_DB->LoadObjectList();

		for($i=0;$i<count($lpath);$i++) {

			if($lpath[$i]->parent_id > 0 && $lpath[$i]->item_id > 0 && $lpath[$i]->step_type == 6) {

				$query = "SELECT item_id, lp_type FROM #__lms_learn_paths WHERE id = '".$lpath[$i]->item_id."'";
				$JLMS_DB->SetQuery( $query );
				$learn_path_info = $JLMS_DB->LoadObjectList();
				if (count($learn_path_info)) {
					$learn_path_id = $learn_path_info[0]->item_id;
					$lp_type = $learn_path_info[0]->lp_type;
					if(isset($scorm_package)) { unset($scorm_package); }
					if($learn_path_id && $lp_type == 2) {
						$query = "SELECT scorm_package FROM #__lms_n_scorm WHERE id = '".$learn_path_id."'";
						$JLMS_DB->SetQuery( $query );
						$scorm_package = $JLMS_DB->LoadResult();
						if(isset($scorm_package) && $scorm_package) {
							$query = "SELECT id FROM #__lms_n_scorm WHERE scorm_package = '".$scorm_package."'"
							. "\n AND course_id = 0"	
							. "\n LIMIT 1"
							;
							$JLMS_DB->SetQuery( $query );
							$scorm_lib_id = $JLMS_DB->LoadResult();
							if($scorm_lib_id) {			
								$query = "SELECT outdoc_share, owner_id, allow_link FROM #__lms_outer_documents WHERE file_id = '".$scorm_lib_id."' AND folder_flag = 3";
								$JLMS_DB->SetQuery( $query );
								$outer_doc = $JLMS_DB->LoadObject();
								if(is_object($outer_doc) && isset($outer_doc->allow_link) && $outer_doc->allow_link == 1 ) {
									// 'share to courses' is still enabled
								} else {
									// 'share to courses' is disabled.... show 'resource is not available' message to teacher
									$lpath[$i]->lpath_name = _JLMS_LP_RESOURSE_ISUNAV;
								}
							} else {
								$lpath[$i]->lpath_name = _JLMS_LP_RESOURSE_ISUNAV;
							}
						} else {
							$lpath[$i]->lpath_name = _JLMS_LP_RESOURSE_ISUNAV;
						}
					}
				}
			}
		}

		for($j=0;$j<count($lpath);$j++){
			if($lpath[$j]->folder_flag == 3){
				$query = "SELECT a.*, b.file_name FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0 WHERE a.id=".$lpath[$j]->file_id;
				$JLMS_DB->SetQuery( $query );
				$out_row = $JLMS_DB->LoadObjectList();
				if (count($out_row) && ($out_row[0]->allow_link == 1 )) {
					$lpath[$j]->file_id = $out_row[0]->file_id;
					$lpath[$j]->file_name = $out_row[0]->file_name;
				} else {
					$lpath[$j]->doc_name = _JLMS_LP_RESOURSE_ISUNAV;
				}
			}
		}
		$query = "SELECT * FROM #__lms_learn_path_conds WHERE course_id = '".$course_id."' AND lpath_id = '".$id."'";
		$JLMS_DB->SetQuery( $query );
		$conds = $JLMS_DB->LoadObjectList();
		$t2 = 0;
		while ($t2 < count($lpath)) {
			$lpath[$t2]->is_condition = false;
			$t2 ++;
		}
		$t = 0;
		while ($t < count($conds)) {
			$step = $conds[$t]->step_id;
			$t2 = 0;
			while ($t2 < count($lpath)) {
				if ($lpath[$t2]->id == $step) {
					$lpath[$t2]->is_condition = true;
				}
				$t2 ++;
			}
			$t ++;
		}
		$lpath = JLMS_GetLPathTreeStructure($lpath);
		$lpath = AppendFileIcons_toList($lpath);

		JLMS_course_lpath_html::showCourseLPath( $course_id, $id, $lpath, $conds, $option );
	} elseif ( $id && $JLMS_ACL->CheckPermissions('lpaths', 'view') && (JLMS_GetLPathCourse($id) == $course_id) ) {
		// if student - Redirect to "show_lpath"
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=show_lpath&course_id=$course_id&id=$id") );
	} else {

		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
function GetCourseQuizzes($course_id, $published = 0) {
	global $JLMS_DB;
	$query = "SELECT a.*, b.c_category "
	. "\n FROM #__lms_quiz_t_quiz as a LEFT JOIN #__lms_quiz_t_category as b ON a.c_category_id = b.c_id "
	. "\n WHERE a.course_id = '".$course_id."'"
	. ($published ? "\n AND a.published = 1" : '' )
	#. "\n AND a.owner_id = '".$user_id."'" //(11.12.2006)
	. "\n ORDER BY a.c_title, b.c_category";
	$JLMS_DB->SetQuery( $query );
	$rows = $JLMS_DB->LoadObjectList();
	return $rows;
}
// don't use this functiona for any other reasons...otherwise - check FileLibrary usage
function GetCourseScorms($course_id, $published = 0) {
	global $JLMS_DB, $my;

	$JLMS_ACL = JLMSFactory::getACL();
	$is_teacher = $JLMS_ACL->isTeacher(); 

	$query = "SELECT a.* "
	. "\n FROM #__lms_learn_paths as a "
	. "\n WHERE a.course_id = $course_id AND (a.lp_type = 2 OR ((a.lp_type = 0 OR a.lp_type = 1) AND a.item_id <> 0) )"
	. ($published ? "\n AND a.published = 1" : '' )
	#. "\n AND a.owner_id = '".$user_id."'" //(11.12.2006)
	. "\n ORDER BY a.ordering, a.lpath_name";
	$JLMS_DB->SetQuery( $query );
	$rows = $JLMS_DB->LoadObjectList();

	$do_reassign = false;
	for($i=0;$i<count($rows);$i++) {
		if($rows[$i]->item_id > 0 && $rows[$i]->lp_type == 2) {

			$query = "SELECT scorm_package FROM #__lms_n_scorm WHERE id = '".$rows[$i]->item_id."'";
			$JLMS_DB->SetQuery( $query );
			$scorm_package = $JLMS_DB->LoadResult();

			if ($scorm_package) {
				$query = "SELECT id FROM #__lms_n_scorm WHERE scorm_package = '".$scorm_package."'"
				. "\n AND course_id = 0"
				. "\n LIMIT 1"
				;
				$JLMS_DB->SetQuery( $query );
				$scorm_lib_id = $JLMS_DB->LoadResult();

				if ($scorm_lib_id) {
					unset($outer_doc);
					$query = "SELECT owner_id, allow_link, outdoc_share FROM #__lms_outer_documents WHERE file_id = '".$scorm_lib_id."' AND folder_flag = 3";
					$JLMS_DB->SetQuery( $query );
					$outer_doc = $JLMS_DB->LoadObject();
					if(is_object($outer_doc) && isset($outer_doc->allow_link) && $outer_doc->allow_link == 1 ) {
						// 'share to courses' is still enabled
					} else {
						// 'share to courses' is disabled....
						unset($rows[$i]);
						$do_reassign = true;
					}
				} else {
					unset($rows[$i]);
					$do_reassign = true;
				}
			} else {
				unset($rows[$i]);
				$do_reassign = true;
			}
		}
	}

	if ($do_reassign) {
		$mas = array();
		foreach ($rows as $k=>$v) {
			$mas[] = $rows[$k];	
		}
		unset($rows);
		$rows = &$mas;
	}

	return $rows;
}
// to do: dobavit' proverki na access rights (24.10 - OK)
// + potom vse eti proverki' dobavit' i v function SAVE (24.10 - gde eto?)
function JLMS_lpath_AddStep( $lpath_id, $option ) {
	global $my, $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');

	if ( $lpath_id && $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) ) {
		$parent = intval(mosGetParam($_REQUEST, 'parent', 0));
		if (JLMS_GetLPathElementType($parent) != 1) { $parent = 0;}
		
		$lpath_chaps = GetLPathChapters($lpath_id);
		
		$lists = array();
		
		$js = "onchange='document.swapParent.parent.value = this.value;
		                 "./*document.swapParent.step_description.value = step_description_ifr.document.getElementById(\"tinymce\").innerHTML;*/""."
		                 document.swapParent.step_shortdescription.value = document.contentForm.step_shortdescription.value;
		                 document.swapParent.step_name.value = document.contentForm.step_name.value;
		                 if(document.chapForm != undefined) document.swapParent.chap_step_name.value = document.chapForm.step_name.value;
		                 document.swapParent.doc_cid.value = getFormCheckboxes(\"docForm\", \"cid\");
		                 document.swapParent.link_cid.value = getFormCheckboxes(\"linkForm\", \"cid\");
		                 document.swapParent.quiz_cid.value = getFormCheckboxes(\"quizForm\", \"cid\");
		                 document.swapParent.scorm_cid.value = getFormCheckboxes(\"scormForm\", \"cid\");
		                 if(document.scormForm != undefined) document.swapParent.paramsdescription_position.value = document.scormForm.paramsdescription_position.value;
		                 if(document.scormForm != undefined) document.swapParent.scorm_step_description.value = document.scormForm.step_description.value;
		                 document.swapParent.submit();' ";
        $js = str_replace(array('\n', '\r'), '', $js);
		$lists['lpath_chaps'] = mosHTML::selectList($lpath_chaps, 'lpath_chapter', 'class="inputbox" size="1" '.$js, 'id', 'step_name', $parent ); //old version (not use)
		
		$lpath_chaps1 = array();
        $lpath_chaps1[0] = new stdClass();
		$lpath_chaps1[0]->id = 0;
		$lpath_chaps1[0]->step_name = _JLMS_LPATH_ROOT_ELEMENT;
		$lpath_chaps1 = array_merge($lpath_chaps1, $lpath_chaps);		
		
		$lists['lpath_content_chapter'] = mosHTML::selectList($lpath_chaps1, 'lpath_content_chapter', 'class="inputbox" size="1" style="width:266px" '.$js, 'id', 'step_name', $parent );
		$lists['lpath_chaps_chapter'] = mosHTML::selectList($lpath_chaps1, 'lpath_chapts_chapter', 'class="inputbox" size="1" style="width:266px" '.$js, 'id', 'step_name', $parent );
		$lists['lpath_links_chapter'] = mosHTML::selectList($lpath_chaps1, 'lpath_links_chapter', 'class="inputbox" size="1" style="width:266px" '.$js, 'id', 'step_name', $parent );
		$lists['lpath_docs_chapter'] = mosHTML::selectList($lpath_chaps1, 'lpath_docs_chapter', 'class="inputbox" size="1" style="width:266px" '.$js, 'id', 'step_name', $parent );
		$lists['lpath_quizzes_chapter'] = mosHTML::selectList($lpath_chaps1, 'lpath_quizzes_chapter', 'class="inputbox" size="1" style="width:266px" '.$js, 'id', 'step_name', $parent );
		$lists['lpath_scorms_chapter'] = mosHTML::selectList($lpath_chaps1, 'lpath_scorms_chapter', 'class="inputbox" size="1" style="width:266px" '.$js, 'id', 'step_name', $parent );

		$query = "SELECT id as value, step_name as text FROM #__lms_learn_path_steps WHERE lpath_id = '". $lpath_id."' AND parent_id = '".$parent."' ORDER BY ordering";
		$JLMS_DB->SetQuery( $query );
		$items_rows = $JLMS_DB->LoadObjectList();
		$order_items = array();
		$order_items[] = mosHTML::makeOption(-1, _JLMS_SB_FIRST_ITEM);
		$order_items = array_merge($order_items, $items_rows);
		$order_items_last = array();
		$order_items_last[] = mosHTML::makeOption(-2, _JLMS_SB_LAST_ITEM);
		$order_items = array_merge($order_items, $order_items_last);
		
		$lists['lpath_content_order'] = mosHTML::selectList($order_items, 'lpath_content_order', 'class="inputbox" size="1" style="width:266px" ', 'value', 'text', -2 );		
		$lists['lpath_chaps_order'] = mosHTML::selectList($order_items, 'lpath_chaps_order', 'class="inputbox" size="1" style="width:266px" ', 'value', 'text', -2 );		
		$lists['lpath_links_order'] = mosHTML::selectList($order_items, 'lpath_links_order', 'class="inputbox" size="1" style="width:266px" ', 'value', 'text', -2 );
		$lists['lpath_docs_order'] = mosHTML::selectList($order_items, 'lpath_docs_order', 'class="inputbox" size="1" style="width:266px" ', 'value', 'text', -2 );		
		$lists['lpath_quizzes_order'] = mosHTML::selectList($order_items, 'lpath_quizzes_order', 'class="inputbox" size="1" style="width:266px" ', 'value', 'text', -2 );		
		$lists['lpath_scorms_order'] = mosHTML::selectList($order_items, 'lpath_scorms_order', 'class="inputbox" size="1" style="width:266px" ', 'value', 'text', -2 );		
		
		$my_documents = GetTeacherDocuments($my->id, $course_id, 1);//in joomla_lms.main.php
		$my_links = GetTeacherLinks($my->id, $course_id, 1);
		$my_quizzes = GetCourseQuizzes($course_id, 1);
		$my_scorms = GetCourseScorms($course_id, 1);
		
		$desc_pos = array();
		$desc_pos[] = mosHTML::makeOption(0, _JLMS_HIDDEN);
		$desc_pos[] = mosHTML::makeOption(1, _JLMS_TOP);
		$desc_pos[] = mosHTML::makeOption(2, _JLMS_BOTTOM);

        $selectedDescriptionPos = mosGetParam($_REQUEST, 'paramsdescription_position', 0);
		$lists['description_position'] = mosHTML::selectList($desc_pos, 'params[description_position]', 'class="inputbox" size="1" style="width:266px" ', 'value', 'text', $selectedDescriptionPos );

        // check if form have been submitted and remember it to show
        // 2 means to allow html tags
        $savedContent['step_description'] = mosGetParam($_REQUEST, 'step_description', '', 2);
        $savedContent['step_shortdescription'] = mosGetParam($_REQUEST, 'step_shortdescription', '');
        $savedContent['step_name'] = mosGetParam($_REQUEST, 'step_name', '');
        $savedContent['chap_step_name'] = mosGetParam($_REQUEST, 'chap_step_name', '');
        $savedContent['doc_cid'] = mosGetParam($_REQUEST, 'doc_cid', '');
        $savedContent['link_cid'] = mosGetParam($_REQUEST, 'link_cid', '');
        $savedContent['quiz_cid'] = mosGetParam($_REQUEST, 'quiz_cid', '');
        $savedContent['scorm_step_description'] = mosGetParam($_REQUEST, 'scorm_step_description', '', 2);
        $savedContent['scorm_cid'] = mosGetParam($_REQUEST, 'scorm_cid', '');

		$my_documents = JLMS_GetTreeStructure( $my_documents );
		$my_documents = AppendFileIcons_toList( $my_documents );
		JLMS_course_lpath_html::addItemToLPath( $lpath_id, $course_id, $option, $my_documents, $my_links, $my_quizzes, $my_scorms, $lists, $parent, $savedContent);
		//insert code here
	} else {
		JLMSRedirect("index.php?option=$option&Itemid=$Itemid&task=details_course&id=$course_id");
	}
}
// to do: add proverki na teachera / ownera
function JLMS_showLPathAddChap( $lpath_id, $option ) {
	global $Itemid, $JLMS_DB, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');
	if ( $lpath_id && $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) ) {
		$lpath_chaps = array();
		$lpath_chaps[0]->id = 0;
		$lpath_chaps[0]->step_name = _JLMS_LPATH_ROOT_ELEMENT;
		$lpath_chaps = array_merge($lpath_chaps, GetLPathChapters($lpath_id));
		$lists = array();
		$lists['lpath_chaps'] = mosHTML::selectList($lpath_chaps, 'lpath_chapter', 'class="inputbox" size="1" ', 'id', 'step_name', 0 );
		JLMS_course_lpath_html::addChapterToLPath( $lpath_id, $course_id, $option, $lists );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
// to do: vstavit' proverki na ownera LPath, na pravil'nost' $lpath_chapter i t.d. i t.p.
// (24.10) vstavil proverki (ESHE OSTALOS' NA CHAPTER)
function JLMS_addDocToLPath( $lpath_id, $option ) {
	global $Itemid, $JLMS_DB, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');

	$lpath_chapter = intval(mosGetParam($_REQUEST, 'lpath_docs_chapter', 0));
	$lpath_order = intval(mosGetParam($_REQUEST, 'lpath_docs_order', -1));
	if ( $lpath_id && $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) ) {
		$cid = mosGetParam( $_POST, 'cid', array(0) );
		if (is_array( $cid )) {
			$new_order = 0;
			if ($lpath_order == -1) {
				$new_order = 0;
			} elseif ($lpath_order == -2) {
				$query = "SELECT max(ordering) FROM #__lms_learn_path_steps WHERE lpath_id = '".$lpath_id."' AND course_id = '".$course_id."' AND parent_id = '".$lpath_chapter."'";
				$JLMS_DB->SetQuery( $query );
				$ordering = $JLMS_DB->LoadResult();
				$new_order = ($ordering + 1);
			} else {
				$query = "SELECT ordering FROM #__lms_learn_path_steps WHERE id = '".$lpath_order."'";
				$JLMS_DB->SetQuery( $query );
				$new_order = intval($JLMS_DB->LoadResult()) + 1;
			}
			$i = 0;
			while ($i < count($cid)) {
				$cid[$i] = intval($cid[$i]);
				$i ++;
			}
			$cids = implode(',',$cid);
			for($z=0;$z<count($cid);$z++){
				$query = "SELECT id, doc_name, doc_description, folder_flag, file_id"
					. "\n FROM #__lms_documents WHERE id = '".$cid[$z]."' AND course_id = '".$course_id."'";
				$JLMS_DB->setQuery($query);
				$tmp_obj = $JLMS_DB->loadObjectList();
				if (count($tmp_obj)) {
					$do_add_resource = true;
					if($tmp_obj[0]->folder_flag == 3){
						$query = "SELECT a.*, b.file_name FROM #__lms_outer_documents as a LEFT JOIN #__lms_files as b ON a.file_id = b.id AND a.folder_flag = 0 WHERE a.id=".$tmp_obj[0]->file_id;
						$JLMS_DB->SetQuery( $query );
						$out_row = $JLMS_DB->LoadObjectList();

						if (count($out_row) && isset($out_row[0]->allow_link) && $out_row[0]->allow_link == 1) {
							$tmp_obj[0]->doc_name = $out_row[0]->doc_name;
							$tmp_obj[0]->doc_description = $out_row[0]->doc_description;
						} else {
							$do_add_resource = false;
						}
					}
					if ($do_add_resource) {
						$query = "INSERT INTO #__lms_learn_path_steps"
								. "\n (course_id, lpath_id, item_id, step_type, parent_id, step_name, step_description, ordering)"
								. "\n VALUES('".$course_id."', '".$lpath_id."', '".$tmp_obj[0]->id."', 2, '".$lpath_chapter."', ".$JLMS_DB->quote($tmp_obj[0]->doc_name).", ".$JLMS_DB->quote($tmp_obj[0]->doc_description).", '".$new_order."')";	
						$JLMS_DB->SetQuery( $query );
						$JLMS_DB->query();
					}
				}
			}
			$query = "SELECT id FROM #__lms_learn_path_steps WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND parent_id = '".$lpath_chapter."' ORDER BY ordering, step_name, id";
			$JLMS_DB->SetQuery( $query );
			$id_array = JLMSDatabaseHelper::LoadResultArray();
			if (count($id_array)) { JLMS_orderLPathStepList($id_array); }
		}
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
// to do: vstavit' proverki na ownera LPath, na pravil'nost' $lpath_chapter i t.d. i t.p.
// (BUG) pri sortirovke.
// 			(esli vstavlyat' s (ordering + 1) (kak shas) a u kogo-to takoi ordering uge est' to
// 			oni potom sortiruyatsa po imeni i moget tak popast' shto nash element na 1 otodvinetsa vniz
function JLMS_addLinkToLPath( $lpath_id, $option ) {
	global $Itemid, $JLMS_DB, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');

	$lpath_chapter = intval(mosGetParam($_REQUEST, 'lpath_links_chapter', 0));
	$lpath_order = intval(mosGetParam($_REQUEST, 'lpath_links_order', -1));
	if ( $lpath_id && $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) ) {
		$cid = mosGetParam( $_POST, 'cid', array(0) );
		if (is_array( $cid )) {
			$new_order = 0;
			if ($lpath_order == -1) {
				$new_order = 0;
			} else {
				$query = "SELECT ordering FROM #__lms_learn_path_steps WHERE id = '".$lpath_order."'";
				$JLMS_DB->SetQuery( $query );
				$new_order = intval($JLMS_DB->LoadResult()) + 1;
			}
			$i = 0;
			while ($i < count($cid)) {
				$cid[$i] = intval($cid[$i]);
				$i ++;
			}
			$cids = implode(',',$cid);
			$query = "INSERT INTO #__lms_learn_path_steps"
			. "\n (course_id, lpath_id, item_id, step_type, parent_id, step_name, step_description, ordering)"
			. "\n SELECT '".$course_id."', '".$lpath_id."', id, 3, '".$lpath_chapter."', link_name, link_description, '".$new_order."'"
			. "\n FROM #__lms_links WHERE id IN ( $cids ) AND course_id = '".$course_id."'";
			// /\ changed 13.02.2007 from :
			//. "\n FROM #__lms_links WHERE id IN ( $cids ) AND owner_id = '".$my->id."' AND course_id = '".$course_id."'"; (+ global $my)
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			$query = "SELECT id FROM #__lms_learn_path_steps WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND parent_id = '".$lpath_chapter."' ORDER BY ordering, step_name, id";
			$JLMS_DB->SetQuery( $query );
			$id_array = JLMSDatabaseHelper::LoadResultArray();
			if (count($id_array)) { JLMS_orderLPathStepList($id_array); }
		}
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
// to do: sdelat' eto vse po 4elove4eski
// to do: proverki na teachera, ownera (est' 21.10)
// (24.10) ulu4shennye proverki
function JLMS_LPath_editItem( $lpath_id, $option ) {
	global $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');

	$do_redirect = true;
	if ( $course_id && $lpath_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) ) {
		$cid = mosGetParam( $_POST, 'cid', array(0) );
		if (is_array( $cid )) {
			$edit_item = intval($cid[0]);
			if ( $edit_item && (JLMS_GetStepLpath($edit_item) == $lpath_id) ) {
				$do_redirect = false;
				JLMS_editLPathStep( $edit_item, $lpath_id, $course_id, $option );
			}
		}
	}
	if ($do_redirect) {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}

// to do: udalyat' vse otovsyudu dlya udalyaemogo LearningPatha
// to do: proverki na teachera
// to do: proverka na 'lpath' + na to shto etot 'lpath' v etom kurse.
// (27.10)
// todo: sdelat' normal'noe udalenie. Seichas udalyayutsa itemy odnoi stepeni vlogennosti (DONE - 11.12.2006)
function JLMS_LPath_delItem( $lpath_id, $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');

	if ( $lpath_id && $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) ) {
		$cid = mosGetParam( $_POST, 'cid', array(0) );
		if (!is_array( $cid )) {
			$cid = array(0);
		}
		$i = 0;
		while ($i < count($cid)) {
			$cid[$i] = intval($cid[$i]);
			$i ++;
		}
		$cids = implode(',',$cid);

		$all_childs = array();
		$do_find_childs = true;
		$parents = $cid;
		while($do_find_childs) {
			$parents_str = implode( ',', $parents );
			$query = "SELECT id FROM #__lms_learn_path_steps WHERE course_id = '".$course_id."' AND parent_id IN ( $parents_str )";
			$JLMS_DB->SetQuery( $query );
			$childs = JLMSDatabaseHelper::LoadResultArray();
			if (count($childs)) {
				$all_childs = array_merge($all_childs, $childs);
				$parents = $childs;
				$do_find_childs = true;
			} else {
				$do_find_childs = false;
			}
		}
		$all_lp_items = array_merge($all_childs, $cid);
		$all_lpi_ids = implode( ',', $all_lp_items );

		$query = "SELECT id FROM #__lms_learn_path_steps WHERE id IN ($all_lpi_ids) AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
		$JLMS_DB->SetQuery( $query );
		$s_cid =  JLMSDatabaseHelper::LoadResultArray();
		$s_cids = implode(',',$s_cid);
		$query = "DELETE FROM #__lms_learn_path_steps WHERE id IN ($s_cids)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$query = "DELETE FROM #__lms_learn_path_step_results WHERE step_id IN ($s_cids)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$query = "DELETE FROM #__lms_learn_path_conds WHERE course_id = '".$course_id."' AND lpath_id='".$lpath_id."' AND step_id IN ($s_cids)";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();

		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}

// (tip) eta function vyzyvaetsa iz predyduschei (a ta iz joomla_lms.php) (task = 'lpath_item_edit')
// + edit dlya vsego ostal'nogo
// !!!!!!!!!! (TIP) (24.10) V etoi funcktion proverki na teachera(ownera) ne nugny t.k. oni v vyzyvayushei funkcii.
function JLMS_editLPathStep( $id, $lpath_id, $course_id, $option ) {
	global $JLMS_DB, $Itemid;
	$row = new mos_Joomla_LMS_LearnPathStep( $JLMS_DB );
	$row->load( $id );
	$lists = array();
	$lpath_chaps = array();
	$lpath_chaps1 = array();
	$step_type = JLMS_GetLPathStepType($id);
	
	/*New - Fix - step not chapter*/
	if(isset($step_type) && $step_type){
		$lpath_chaps1[0] = new stdClass();
		$lpath_chaps1[0]->id = 0;
		$lpath_chaps1[0]->step_name = _JLMS_LPATH_ROOT_ELEMENT;
		$lpath_chaps = GetLPathChapters($lpath_id);
		$lpath_chaps1 = array_merge($lpath_chaps1, $lpath_chaps);
	}
	
	$params = array();
	if(isset($row->params)){
		$params = new JLMSParameters($row->params);
	} else {
		$params = new JLMSParameters($row->params);
	}
	
	$lists['lpath_chaps'] = mosHTML::selectList($lpath_chaps1, 'parent_id', 'class="inputbox" size="1" style="width:299px" ', 'id', 'step_name', $row->parent_id );
	
	$desc_pos = array();
	$desc_pos[] = mosHTML::makeOption(0, _JLMS_HIDDEN);
	$desc_pos[] = mosHTML::makeOption(1, _JLMS_TOP);
	$desc_pos[] = mosHTML::makeOption(2, _JLMS_BOTTOM);
	$lists['description_position'] = mosHTML::selectList($desc_pos, 'params[description_position]', 'class="inputbox" size="1" style="width:299px" ', 'value', 'text', $params->get('description_position', 0) );

	if ($step_type == 1) {
		JLMS_course_lpath_html::editLPathStep_Chapter( $row, $lists, $option, $course_id, $lpath_id );
	} elseif ($step_type == 2) {
		JLMS_course_lpath_html::editLPathStep_Doc( $row, $lists, $option, $course_id, $lpath_id );
	} elseif ($step_type == 3) {
		JLMS_course_lpath_html::editLPathStep_Link( $row, $lists, $option, $course_id, $lpath_id );
	} elseif ($step_type == 4) {
		JLMS_course_lpath_html::editLPathStep_Content( $row, $lists, $option, $course_id, $lpath_id );
	} elseif ($step_type == 5) {
		JLMS_course_lpath_html::editLPathStep_Quiz( $row, $lists, $option, $course_id, $lpath_id );
	} elseif ($step_type == 6) {
		JLMS_course_lpath_html::editLPathStep_Scorm( $row, $lists, $option, $course_id, $lpath_id );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
// + v function check() vstavit' proverki na dostupnost' redaktirovaniya i soxraneniya etogo stepa
// + (processing) shto-to sdelat' s proverkami vvedennogo koda. (nemnogo izmenil inputfilter - teper' ne teryayutsa ruskie bukvy , no nugno potestit' na 4em-nit' drugom)
// + WYSIWYG ???
// (WARNING) 'unset()' stoyat shtoby slu4aino (ili namerenno) ne izmenilsya kurs i lpath
// (03.11) (sdelal normal'noe soxranenie 'step_name' (vse obrezaetsa i t.d.i t.p.....)
function JLMS_saveLPathChapter( $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');

	$lpath_id = intval(mosGetParam($_REQUEST, 'lpath_id', 0));
	if ( $lpath_id && $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) ) {
		$row = new mos_Joomla_LMS_LearnPathStep( $JLMS_DB );
		if (!$row->bind( $_POST )) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		$step_name = isset($_REQUEST['step_name'])?strval($_REQUEST['step_name']):'';
		$step_name = (get_magic_quotes_gpc()) ? stripslashes( $step_name ) : $step_name;
		$row->step_name	= ampReplace(strip_tags($step_name));

		$row->step_type = 1;
		unset($row->course_id);
		unset($row->lpath_id);

		// filter input
		/* 23.02.2007 (changed by DEN)
		Embedding swf and video support + MAMBOTS support */
		$row->step_description = strval(JLMS_getParam_LowFilter($_POST, 'step_description', ''));
		$row->step_description = JLMS_ProcessText_LowFilter($row->step_description);
		$row->step_shortdescription = strval(JLMS_getParam_LowFilter($_POST, 'step_shortdescription', ''));
		$row->step_shortdescription = JLMS_ProcessText_HardFilter($row->step_shortdescription);

		if ($row->parent_id && $row->id) {
			$is_allowed = true;
			if ($row->parent_id != $row->id) {
				$do_while = true;
				$temp_ids = array();
				$temp_ids[] = intval($row->id);
				$rrr = 0;
				while ($do_while) {
					$temp_ids_str = implode(',',$temp_ids);
					$query = "SELECT id FROM #__lms_learn_path_steps WHERE course_id = $course_id AND lpath_id = $lpath_id AND parent_id IN ($temp_ids_str)";
					$JLMS_DB->SetQuery($query);
					$temp_ids = JLMSDatabaseHelper::LoadResultArray();

					if (count($temp_ids)) {
						if (in_array($row->parent_id,$temp_ids)) {
							$is_allowed = false;
							break;
						}
					} else {
						$do_while = false;
					}
					$rrr++;
					if ($rrr > 100) { // na vsyakii sluchai shtoby nichego nezaciklilos'
						$do_while = false;
					}
				}
			} else {
				$is_allowed = false;
			}
			if (!$is_allowed) {
				$row->parent_id = 0;
			}
		}

		if (!$row->check()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if (!$row->store()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
function JLMS_saveLPathContent( $option ) {
	global $JLMS_DB, $Itemid,  $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');

	$lpath_id = intval(mosGetParam($_REQUEST, 'lpath_id', 0));
	if ( $lpath_id && $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) ) {
		$row = new mos_Joomla_LMS_LearnPathStep( $JLMS_DB );
		if (!$row->bind( $_POST )) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		$step_name = isset($_REQUEST['step_name'])?strval($_REQUEST['step_name']):'';
		$step_name = (get_magic_quotes_gpc()) ? stripslashes( $step_name ) : $step_name;
		$row->step_name	= ampReplace(strip_tags($step_name));

		$row->step_type = 4;
		unset($row->course_id);
		unset($row->lpath_id);

		// filter input
		/* 23.02.2007 (changed by DEN)
		Embedding swf and video support + MAMBOTS support */
		$row->step_description = strval(JLMS_getParam_LowFilter($_POST, 'step_description', ''));
		$row->step_description = JLMS_ProcessText_LowFilter($row->step_description);
		$row->step_shortdescription = strval(JLMS_getParam_LowFilter($_POST, 'step_shortdescription', ''));
		$row->step_shortdescription = JLMS_ProcessText_HardFilter($row->step_shortdescription);

		if (!$row->check()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if (!$row->store()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
// (WARNING) propisano mnogo 'unset(...)' (is it correct?) (dlya togo shtoby slu4aino ne pomenyalos' lishnego ni4ego)
// eta fuction tol'ko dlya update !! (ne dlya pervogo save)
function JLMS_saveLPathDoc( $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');
	$lpath_id = intval(mosGetParam($_REQUEST, 'lpath_id', 0));
	if ( $lpath_id && $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) ) {
		$row = new mos_Joomla_LMS_LearnPathStep( $JLMS_DB );
		if (!$row->bind( $_POST )) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		//$row->step_name = strval(mosGetParam($_REQUEST, 'step_name', ''));
		$step_name = isset($_REQUEST['step_name'])?strval($_REQUEST['step_name']):'';
		$step_name = (get_magic_quotes_gpc()) ? stripslashes( $step_name ) : $step_name;
		$row->step_name	= ampReplace(strip_tags($step_name));
		$row->step_type = 2;
		unset($row->item_id);
		unset($row->course_id);
		unset($row->lpath_id);
		// filter input
		/* 26.02.2007 (changed by DEN)
		Embedding swf and video support + MAMBOTS support */
		$row->step_description = strval(JLMS_getParam_LowFilter($_POST, 'step_description', ''));
		$row->step_description = JLMS_ProcessText_LowFilter($row->step_description);
		$row->step_shortdescription = strval(JLMS_getParam_LowFilter($_POST, 'step_shortdescription', ''));
		$row->step_shortdescription = JLMS_ProcessText_HardFilter($row->step_shortdescription);
		//$row->owner_id = $my->id; (+ global $my)
		if (!$row->check()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if (!$row->store()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
function JLMS_saveLPathQuiz( $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');
	$lpath_id = intval(mosGetParam($_REQUEST, 'lpath_id', 0));
	if ( $lpath_id && $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) ) {
		$row = new mos_Joomla_LMS_LearnPathStep( $JLMS_DB );
		if (!$row->bind( $_POST )) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		//$row->step_name = strval(mosGetParam($_REQUEST, 'step_name', ''));
		$step_name = isset($_REQUEST['step_name'])?strval($_REQUEST['step_name']):'';
		$step_name = (get_magic_quotes_gpc()) ? stripslashes( $step_name ) : $step_name;
		$row->step_name	= ampReplace(strip_tags($step_name));
		$row->step_type = 5;
		unset($row->item_id);
		unset($row->course_id);
		unset($row->lpath_id);
		unset($row->step_description);
		// filter input
		/* 26.02.2007 (changed by DEN)
		Embedding swf and video support + MAMBOTS support */
		//$row->step_description = strval(JLMS_getParam_LowFilter($_POST, 'step_description', ''));
		//$row->step_description = JLMS_ProcessText_LowFilter($row->step_description);
		$row->step_shortdescription = strval(JLMS_getParam_LowFilter($_POST, 'step_shortdescription', ''));
		$row->step_shortdescription = JLMS_ProcessText_HardFilter($row->step_shortdescription);
		//$row->owner_id = $my->id; (+ global $my)
		if (!$row->check()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if (!$row->store()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
function JLMS_saveLPathScorm( $option ) {	
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');
	$lpath_id = intval(mosGetParam($_REQUEST, 'lpath_id', 0));
	$pre_params = mosGetParam($_REQUEST, 'params', '');
	$params = '';
	if(is_array($pre_params)){
		foreach($pre_params as $param=>$value){
			$temp_params[] = $param.'='.$value;
		}
		$params = implode("\n", $temp_params);
	}
	if ( $lpath_id && $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) ) {
		$row = new mos_Joomla_LMS_LearnPathStep( $JLMS_DB );
		if (!$row->bind( $_POST )) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		//$row->step_name = strval(mosGetParam($_REQUEST, 'step_name', ''));
		$step_name = isset($_REQUEST['step_name'])?strval($_REQUEST['step_name']):'';
		$step_name = (get_magic_quotes_gpc()) ? stripslashes( $step_name ) : $step_name;
		$row->step_name	= ampReplace(strip_tags($step_name));
		$row->step_type = 6;
		unset($row->item_id);
		unset($row->course_id);
		unset($row->lpath_id);
		#unset($row->step_description);
		
		$row->step_shortdescription = strval(JLMS_getParam_LowFilter($_POST, 'step_shortdescription', ''));
		$row->step_shortdescription = JLMS_ProcessText_HardFilter($row->step_shortdescription);
		
		$row->step_description = strval(JLMS_getParam_LowFilter($_POST, 'step_description', ''));
		$row->step_description = JLMS_ProcessText_HardFilter($row->step_description);
		
		$row->params = $params;
		
		if (!$row->check()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if (!$row->store()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
function JLMS_saveLPathLink( $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');

	if ($JLMS_ACL->CheckPermissions('lpaths', 'manage')) {
		$lpath_id = intval(mosGetParam($_REQUEST, 'lpath_id', 0));
		$row = new mos_Joomla_LMS_LearnPathStep( $JLMS_DB );
		if (!$row->bind( $_POST )) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		//$row->step_name = strval(mosGetParam($_REQUEST, 'step_name', ''));
		$step_name = isset($_REQUEST['step_name'])?strval($_REQUEST['step_name']):'';
		$step_name = (get_magic_quotes_gpc()) ? stripslashes( $step_name ) : $step_name;
		$row->step_name	= ampReplace(strip_tags($step_name));
		$row->step_type = 3;
		unset($row->item_id);
		unset($row->course_id);
		unset($row->lpath_id);
		// filter input
		/* 26.02.2007 (changed by DEN)
		Embedding swf and video support + MAMBOTS support */
		$row->step_description = strval(JLMS_getParam_LowFilter($_POST, 'step_description', ''));
		$row->step_description = JLMS_ProcessText_LowFilter($row->step_description);
		$row->step_shortdescription = strval(JLMS_getParam_LowFilter($_POST, 'step_shortdescription', ''));
		$row->step_shortdescription = JLMS_ProcessText_HardFilter($row->step_shortdescription);
		//$row->owner_id = $my->id; (+ global $my)
		if (!$row->check()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		if (!$row->store()) {
			echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
// (tip) zdes' proverka na teachera ne nugna t.k. eta function vyzyvaetsa tol'ko iz dr.funcs gde est' proverki
function JLMS_orderLPathStepList($id_array) {
	global $JLMS_DB;
	$i = 0;
	foreach ($id_array as $lpath_id) {
		$query = "UPDATE #__lms_learn_path_steps SET ordering = '".$i."' WHERE id = '".$lpath_id."'";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
		$i ++;
	}
}

// to do: create class + ispol'zovat' bind() + (add params manually) + check() + store()
// + add descriptions
// + add ordering field to '#__lms_learn_path' table
// (25.10 - peredelana function s etimi trebovaniyami)
function JLMS_lpath_AddChapter( $lpath_id, $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');

	if ( $course_id && $lpath_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) ) {
		//$new_chapter_name = strval(mosGetParam($_REQUEST, 'step_name', ''));
		$new_chapter_name = isset($_REQUEST['step_name'])?strval($_REQUEST['step_name']):'';
		$new_chapter_name = (get_magic_quotes_gpc()) ? stripslashes( $new_chapter_name ) : $new_chapter_name;
		$new_chapter_name = ampReplace(strip_tags($new_chapter_name));
		if ($new_chapter_name) {
			$row = new mos_Joomla_LMS_LearnPathStep( $JLMS_DB );
			if (!$row->bind( $_POST )) {
				echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
				exit();
			}
			$row->id = 0; // only new record; !!!!!
			$row->lpath_id = $lpath_id;
			$row->step_type = 1;
			$row->item_id = 0;
			$row->step_name = $new_chapter_name;

			// filter input
			/* 23.02.2007 (changed by DEN)
			Embedding swf and video support + MAMBOTS support */
			$row->step_description = strval(JLMS_getParam_LowFilter($_POST, 'step_description', ''));
			$row->step_description = JLMS_ProcessText_LowFilter($row->step_description);
			$row->step_shortdescription = strval(JLMS_getParam_LowFilter($_POST, 'step_shortdescription', ''));
			$row->step_shortdescription = JLMS_ProcessText_HardFilter($row->step_shortdescription);

			$parent_id = intval(mosGetParam($_REQUEST, 'lpath_chaps_chapter', 0));
			$row->parent_id = $parent_id;
			if (isset($_REQUEST['lpath_chaps_order'])) {
				$chap_ordering = intval(mosGetParam($_REQUEST, 'lpath_chaps_order', 0));
				if ($chap_ordering == -1) {
					$row->ordering = 0;
				} elseif ($chap_ordering == -2) {
					$query = "SELECT max(ordering) FROM #__lms_learn_path_steps WHERE lpath_id = '".$lpath_id."' AND course_id = '".$course_id."' AND parent_id = '".$parent_id."'";
					$JLMS_DB->SetQuery( $query );
					$ordering = $JLMS_DB->LoadResult();
					$row->ordering = ($ordering + 1);
				} else {
					$query = "SELECT max(ordering) FROM #__lms_learn_path_steps WHERE id = '".$chap_ordering."'";
					$JLMS_DB->SetQuery( $query );
					$ordering = $JLMS_DB->LoadResult();
					$row->ordering = ($ordering + 1);
				}
			} else {
				$query = "SELECT max(ordering) FROM #__lms_learn_path_steps WHERE lpath_id = '".$lpath_id."' AND course_id = '".$course_id."' AND parent_id = '".$parent_id."'";
				$JLMS_DB->SetQuery( $query );
				$ordering = $JLMS_DB->LoadResult();
				if ($ordering) { $ordering ++; } else { $ordering = 0; }
				$row->ordering = $ordering;
			}
			if (!$row->check()) {
				echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
				exit();
			}
			if (!$row->store()) {
				echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
				exit();
			}
			$query = "SELECT id FROM #__lms_learn_path_steps WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND parent_id = '".$parent_id."' ORDER BY ordering, step_name, id";
			$JLMS_DB->SetQuery( $query );
			$id_array = JLMSDatabaseHelper::LoadResultArray();
			if (count($id_array)) { JLMS_orderLPathStepList($id_array); }
		}
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
function JLMS_lpath_AddContent( $lpath_id, $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');
	
	if ( $course_id && $lpath_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) ) {
		$new_content_name = strval(mosGetParam($_REQUEST, 'step_name', ''));
		$new_content_name = isset($_REQUEST['step_name'])?strval($_REQUEST['step_name']):'';
		$new_content_name = (get_magic_quotes_gpc()) ? stripslashes( $new_content_name ) : $new_content_name;
		$new_content_name = ampReplace(strip_tags($new_content_name));
		if ($new_content_name) {
			$row = new mos_Joomla_LMS_LearnPathStep( $JLMS_DB );
			if (!$row->bind( $_POST )) {
				echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
				exit();
			}
			$row->id = 0; // only new record; !!!!!
			$row->lpath_id = $lpath_id;
			$row->step_type = 4;
			$row->item_id = 0;
			$row->step_name = $new_content_name;

			// filter input
			/* 23.02.2007 (changed by DEN)
			Embedding swf and video support + MAMBOTS support */
			$row->step_description = strval(JLMS_getParam_LowFilter($_POST, 'step_description', ''));
			$row->step_description = JLMS_ProcessText_LowFilter($row->step_description);
			$row->step_shortdescription = strval(JLMS_getParam_LowFilter($_POST, 'step_shortdescription', ''));
			$row->step_shortdescription = JLMS_ProcessText_HardFilter($row->step_shortdescription);

			$parent_id = intval(mosGetParam($_REQUEST, 'lpath_content_chapter', 0));
			$row->parent_id = $parent_id;
			if (isset($_REQUEST['lpath_content_order'])) {
				$chap_ordering = intval(mosGetParam($_REQUEST, 'lpath_content_order', 0));
				if ($chap_ordering == -1) {
					$row->ordering = 0;
				} elseif ($chap_ordering == -2) {
					$query = "SELECT max(ordering) FROM #__lms_learn_path_steps WHERE lpath_id = '".$lpath_id."' AND course_id = '".$course_id."' AND parent_id = '".$parent_id."'";
					$JLMS_DB->SetQuery( $query );
					$ordering = $JLMS_DB->LoadResult();
					$row->ordering = ($ordering + 1);
				} else {
					$query = "SELECT max(ordering) FROM #__lms_learn_path_steps WHERE id = '".$chap_ordering."'";
					$JLMS_DB->SetQuery( $query );
					$ordering = $JLMS_DB->LoadResult();
					$row->ordering = ($ordering + 1);
				}
			} else {
				$query = "SELECT max(ordering) FROM #__lms_learn_path_steps WHERE lpath_id = '".$lpath_id."' AND course_id = '".$course_id."' AND parent_id = '".$parent_id."'";
				$JLMS_DB->SetQuery( $query );
				$ordering = $JLMS_DB->LoadResult();
				if ($ordering) { $ordering ++; } else { $ordering = 0; }
				$row->ordering = $ordering;
			}
			if (!$row->check()) {
				echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
				exit();
			}
			if (!$row->store()) {
				echo "<script> alert('".$row->getError()."'); window.history.go(-1); </script>\n";
				exit();
			}
			$query = "SELECT id FROM #__lms_learn_path_steps WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND parent_id = '".$parent_id."' ORDER BY ordering, step_name, id";
			$JLMS_DB->SetQuery( $query );
			$id_array = JLMSDatabaseHelper::LoadResultArray();
			if (count($id_array)) { JLMS_orderLPathStepList($id_array); }
		}
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
function GetLPathChapters($lpath_id) {
	global $JLMS_DB;
	$query = "SELECT * FROM #__lms_learn_path_steps WHERE lpath_id = '".$lpath_id."' AND step_type = 1";
	$JLMS_DB->SetQuery( $query );
	return $JLMS_DB->LoadObjectList();
}
function JLMS_GetLPathOwner($file_id) {
	global $JLMS_DB;
	$query = "SELECT owner_id FROM #__lms_learn_paths WHERE id = '".$file_id."'";
	$JLMS_DB->SetQuery( $query );
	return $JLMS_DB->LoadResult();
}
function JLMS_GetLPathCourse($lpath_id) {
	global $JLMS_DB;
	$query = "SELECT course_id FROM #__lms_learn_paths WHERE id = '".$lpath_id."'";
	$JLMS_DB->SetQuery( $query );
	return $JLMS_DB->LoadResult();
}
function JLMS_GetStepLPath($step_id) {
	global $JLMS_DB;
	$query = "SELECT lpath_id FROM #__lms_learn_path_steps WHERE id = '".$step_id."'";
	$JLMS_DB->SetQuery( $query );
	return $JLMS_DB->LoadResult();
}
function JLMS_GetLPathStepType($step_id) {
	global $JLMS_DB;
	$query = "SELECT step_type FROM #__lms_learn_path_steps WHERE id = '".$step_id."'";
	$JLMS_DB->SetQuery( $query );
	return $JLMS_DB->LoadResult();
}

//(24.10) (TIP) seichas scorm-paket moget ska4at' lyuboi teacher (+assistant) etogo kursa.
//			(v dal'neishem kogda budut assistenty vozmogny izmeneniya funkcii)
function JLMS_downloadSCORM( $id, $option) {
	global $Itemid, $JLMS_DB, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');

	if ( $id && $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') ) {
		$query = "SELECT * FROM #__lms_learn_paths WHERE id = $id AND course_id = ".$course_id;
		$JLMS_DB->SetQuery($query);
		$lp = $JLMS_DB->LoadObject();
		if (is_object($lp)) {
			$scorm_pack_id = 0;
			if ($lp->lp_type == 1 || $lp->lp_type == 2) {
				if ($lp->item_id) {
					$query = "SELECT scorm_package FROM #__lms_n_scorm WHERE id = $lp->item_id AND course_id = ".$course_id;
					$JLMS_DB->SetQuery($query);
					$scorm_pack_id = $JLMS_DB->loadResult();
				}
			} elseif($lp->lp_type == 0) {
				$scorm_pack_id = $lp->item_id;
			}
			if ($scorm_pack_id) {
				$query = "SELECT package_srv_name, package_user_name FROM #__lms_scorm_packages"
				. "\n WHERE id = '".$scorm_pack_id."' AND course_id = '".$course_id."'";// AND owner_id = '".$my->id."'"; (+ global $my)
				$JLMS_DB->SetQuery($query);
				$file_char = $JLMS_DB->LoadObjectList();
				if (count($file_char) == 1) {
					$srv_name = _JOOMLMS_SCORM_FOLDER_PATH . "/" . $file_char[0]->package_srv_name;
					$file_name = $file_char[0]->package_user_name;
					if ( file_exists( $srv_name ) && is_readable( $srv_name ) ){
						$v_date = gmdate("Y-m-d H:i:s");
						if (preg_match('/Opera(\/| )([0-9].[0-9]{1,2})/', $_SERVER['HTTP_USER_AGENT'])) {
							$UserBrowser = "Opera";
						}
						elseif (preg_match('/MSIE ([0-9].[0-9]{1,2})/', $_SERVER['HTTP_USER_AGENT'])) {
							$UserBrowser = "IE";
						} else {
							$UserBrowser = '';
						}
						$mime_type = ($UserBrowser == 'IE' || $UserBrowser == 'Opera') ? 'application/octetstream' : 'application/octet-stream';
						header('Content-Type: ' . $mime_type );
						header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
						if ($UserBrowser == 'IE') {
							header('Content-Disposition: attachment; filename="' . $file_name . '";');
							header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
							header('Pragma: public');
						} else {
							header('Content-Disposition: attachment; filename="' . $file_name . '";');
							header('Pragma: no-cache');
						}
						@ob_end_clean();
						readfile( $srv_name );
						exit();
					}
				}
			} else {
				//export native Learning path
				require_once (_JOOMLMS_FRONT_HOME.'/includes/jlms_course_export.php');
				$cid = array();
				$cid[] = 2;
				$resources = array();
				$resources[2] = array();
				$resources[2][] = $lp->id;
				JLMS_courseExport ($option, $course_id, 'exp', $cid, $resources );
			}
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
}

function JLMS_cond_LPathStep( $step_id, $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');

	$lpath_id = intval(mosGetParam($_REQUEST, 'lpath_id', 0));
	if ( $course_id && $lpath_id && $step_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) && (JLMS_GetStepLPath($step_id) == $lpath_id) ) {
		$query = "SELECT * FROM #__lms_learn_path_steps WHERE id = '".$step_id."'";
		$JLMS_DB->SetQuery( $query );
		$step = $JLMS_DB->loadObject();
		
		$query = "SELECT a.*, b.step_name, c.step_name as ref_step_name"
		. "\n FROM #__lms_learn_path_conds as a"
		. "\n LEFT JOIN #__lms_learn_path_steps as c ON a.ref_step=c.id"
		. "\n, #__lms_learn_path_steps as b"
		. "\n WHERE a.step_id = b.id"
		. "\n AND b.id = '".$step_id."' ORDER BY b.ordering";
		$JLMS_DB->SetQuery( $query );
		$conds = $JLMS_DB->LoadObjectList();
		
		$query = "SELECT a.id, a.step_name"
		. "\n FROM #__lms_learn_path_steps as a"		
		. "\n WHERE a.id NOT IN(SELECT b.ref_step FROM #__lms_learn_path_conds AS b WHERE b.step_id = ".$step_id." )"
		. "\n AND a.id != '".$step_id."' ORDER BY a.ordering";
		$JLMS_DB->SetQuery( $query );
		$lpath_contents = $JLMS_DB->LoadObjectList();		
		
		$lists = array();
		$lists['lpath_steps'] = mosHTML::selectList($lpath_contents, 'ref_step', 'class="inputbox" size="1" ', 'id', 'step_name', null );
		
		
		JLMS_course_lpath_html::showStepConditions( $course_id, $lpath_id, $step_id, $option, $conds, $lists, $step );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
	}
}
function JLMS_save_Cond( $step_id, $option ) {
	$db = JFactory::getDbo();
	$JLMS_CONFIG = JLMSFactory::getConfig();
	$JLMS_ACL = JLMSFactory::getACL();
	$Itemid = $JLMS_CONFIG->get('Itemid');
	$course_id = $JLMS_CONFIG->get('course_id');

	$lpath_id = intval(mosGetParam($_REQUEST, 'lpath_id', 0));
	$ref_step = intval(mosGetParam($_REQUEST, 'ref_step', 0));
	$cond_time = intval(mosGetParam($_REQUEST, 'cond_time', 0));
	$cond_time_sec = intval(mosGetParam($_REQUEST, 'cond_time_sec', 0));
	$mode = mosGetParam($_REQUEST, 'mode', '');
	if ( $course_id && $lpath_id && $step_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) && (JLMS_GetStepLPath($step_id) == $lpath_id) ) {
		if($mode == 'access'){
			if ( $ref_step && (JLMS_GetStepLPath($ref_step) == $lpath_id) ) {
				$query = "SELECT count(*) FROM #__lms_learn_path_conds WHERE step_id = '".$step_id."' AND ref_step = '".$ref_step."' AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
				$db->SetQuery( $query );
				$conds_count = $db->LoadResult();
				if ($conds_count) {
	
				} else {
					$parent_id = $step_id;
					$max_steps = 0;
					$parents = array();
					while ($parent_id && ($max_steps < 30)) {
						$query = "SELECT parent_id FROM #__lms_learn_path_steps WHERE id = $parent_id";
						$db->SetQuery( $query );
						$parent_id = $db->loadResult();
						if ($parent_id) {
							$parents[] = $parent_id;
						}
					}
					if (!in_array($ref_step, $parents)) {
						if ($step_id == $ref_step) {
							//impossible
						} else {
							$timetraking_fileds = '';
							$timetraking_values = '';
							if($JLMS_CONFIG->get('enable_timetracking', false)){
								//$timetraking_fileds = ', cond_time, cond_time_sec';
								//$timetraking_values = ', "'.$cond_time.'", "'.$cond_time_sec.'"';
								$timetraking_fileds = ', cond_time';
								$timetraking_values = ', "'.$cond_time.'"';
							}
							$query = "INSERT INTO #__lms_learn_path_conds (course_id, lpath_id, step_id, ref_step, cond_type, cond_value".$timetraking_fileds.")"	//, cond_time (if Timetracking enabled) Next version!!!
							. "\n VALUES ('".$course_id."', '".$lpath_id."', '".$step_id."', '".$ref_step."', 1, 0".$timetraking_values.")";						//, '".$cond_time."' (if Timetracking enabled) Next version!!!
							$db->SetQuery( $query );
							$db->query();
						}
					}
				}
			}
		}
		if($mode == 'completion'){
			$query = "SELECT count(*) FROM #__lms_learn_path_conds WHERE step_id = '".$step_id."' AND ref_step = '".$ref_step."' AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."'";
			$db->SetQuery( $query );
			$conds_count = $db->LoadResult();
			if ($conds_count) {
			} else {
				$parent_id = $step_id;
				$max_steps = 0;
				$parents = array();
				while ($parent_id && ($max_steps < 30)) {
					$query = "SELECT parent_id FROM #__lms_learn_path_steps WHERE id = $parent_id";
					$db->SetQuery( $query );
					$parent_id = $db->loadResult();
					if ($parent_id) {
						$parents[] = $parent_id;
					}
				}
				if (!in_array($ref_step, $parents)) {
					if ($step_id == $ref_step) {
						//impossible
					} else {
						$timetraking_fileds = '';
						$timetraking_values = '';
						if($JLMS_CONFIG->get('enable_timetracking', false)){
							//$timetraking_fileds = ', cond_time, cond_time_sec';
							//$timetraking_values = ', "'.$cond_time.'", "'.$cond_time_sec.'"';
							$timetraking_fileds = ', cond_time';
							$timetraking_values = ', "'.$cond_time.'"';
						}
						$query = "INSERT INTO #__lms_learn_path_conds (course_id, lpath_id, step_id, ref_step, cond_type, cond_value".$timetraking_fileds.")"	//, cond_time (if Timetracking enabled) Next version!!!
						. "\n VALUES ('".$course_id."', '".$lpath_id."', '".$step_id."', '".$ref_step."', 1, 0".$timetraking_values.")";						//, '".$cond_time."' (if Timetracking enabled) Next version!!!
						$db->SetQuery( $query );
						$db->query();
					}
				}
			}
		}
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=lpath_step_cond&course_id=$course_id&lpath_id=$lpath_id&id=$step_id") );
}
function JLMS_delete_Cond( $step_id, $option ) {
	global $JLMS_DB, $Itemid, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');

	$lpath_id = intval(mosGetParam($_REQUEST, 'lpath_id', 0));
	if ( $course_id && $lpath_id && $step_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) && (JLMS_GetStepLPath($step_id) == $lpath_id) ) {
		$cid = mosGetParam( $_POST, 'cid', array(0) );
		if (!is_array( $cid )) {
			$cid = array(0);
		}
		$i = 0;
		while ($i < count($cid)) {
			$cid[$i] = intval($cid[$i]);
			$i ++;
		}
		$cids = implode(',',$cid);
		$query = "DELETE FROM #__lms_learn_path_conds WHERE id IN ($cids) AND course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND step_id = '".$step_id."'";
		$JLMS_DB->SetQuery( $query );
		$JLMS_DB->query();
	}
	JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=lpath_step_cond&course_id=$course_id&lpath_id=$lpath_id&id=$step_id") );
}
function JLMS_addQuizToLPath( $lpath_id, $option ) {
	global $Itemid, $JLMS_DB, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');

	$lpath_chapter = intval(mosGetParam($_REQUEST, 'lpath_quizzes_chapter', 0));
	$lpath_order = intval(mosGetParam($_REQUEST, 'lpath_quizzes_order', -1));
	if ( $lpath_id && $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) ) {
		$cid = mosGetParam( $_POST, 'cid', array(0) );
		if (is_array( $cid )) {
			$new_order = 0;
			if ($lpath_order == -1) {
				$new_order = 0;
			} elseif ($lpath_order == -2) {
				$query = "SELECT max(ordering) FROM #__lms_learn_path_steps WHERE lpath_id = '".$lpath_id."' AND course_id = '".$course_id."' AND parent_id = '".$lpath_chapter."'";
				$JLMS_DB->SetQuery( $query );
				$ordering = $JLMS_DB->LoadResult();
				$new_order = ($ordering + 1);
			} else {
				$query = "SELECT ordering FROM #__lms_learn_path_steps WHERE id = '".$lpath_order."'";
				$JLMS_DB->SetQuery( $query );
				$new_order = intval($JLMS_DB->LoadResult()) + 1;
			}
			$i = 0;
			while ($i < count($cid)) {
				$cid[$i] = intval($cid[$i]);
				$i ++;
			}
			$cids = implode(',',$cid);
			$query = "INSERT INTO #__lms_learn_path_steps"
			. "\n (course_id, lpath_id, item_id, step_type, parent_id, step_name, step_description, ordering)"
			. "\n SELECT '".$course_id."', '".$lpath_id."', c_id, 5, '".$lpath_chapter."', c_title, '', '".$new_order."'"
			. "\n FROM #__lms_quiz_t_quiz WHERE c_id IN ( $cids ) AND course_id = '".$course_id."'";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			$query = "SELECT id FROM #__lms_learn_path_steps WHERE course_id = '".$course_id."' AND lpath_id = '".$lpath_id."' AND parent_id = '".$lpath_chapter."' ORDER BY ordering, step_name, id";
			$JLMS_DB->SetQuery( $query );
			$id_array = JLMSDatabaseHelper::LoadResultArray();
			if (count($id_array)) { JLMS_orderLPathStepList($id_array); }
		}
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}

function JLMS_addScormToLPath( $lpath_id, $option ) {
	global $Itemid, $JLMS_DB, $JLMS_CONFIG;
	$JLMS_ACL = JLMSFactory::getACL();
	$course_id = $JLMS_CONFIG->get('course_id');
	$lpath_chapter = intval(mosGetParam($_REQUEST, 'lpath_scorms_chapter', 0));
	$lpath_order = intval(mosGetParam($_REQUEST, 'lpath_scorms_order', -1));
	$step_description = mosGetParam($_REQUEST, 'step_description', '');
	$pre_params = mosGetParam($_REQUEST, 'params', '');
	$params = '';
	if(is_array($pre_params)){
		foreach($pre_params as $param=>$value){
			$temp_params[] = $param.'='.$value;
		}
		$params = implode("\n", $temp_params);
	}
	if ( $lpath_id && $course_id && $JLMS_ACL->CheckPermissions('lpaths', 'manage') && (JLMS_GetLPathCourse($lpath_id) == $course_id) ) {
		$cid = mosGetParam( $_POST, 'cid', array(0) );
		if (is_array( $cid )) {
			$new_order = 0;
			if ($lpath_order == -1) {
				$new_order = 0;
			} elseif ($lpath_order == -2) {
				$query = "SELECT max(ordering) FROM #__lms_learn_path_steps WHERE lpath_id = '".$lpath_id."' AND course_id = '".$course_id."' AND parent_id = '".$lpath_chapter."'";
				$JLMS_DB->SetQuery( $query );
				$ordering = $JLMS_DB->LoadResult();
				$new_order = ($ordering + 1);
			} else {
				$query = "SELECT ordering FROM #__lms_learn_path_steps WHERE id = '".$lpath_order."'";
				$JLMS_DB->SetQuery( $query );
				$new_order = intval($JLMS_DB->LoadResult()) + 1;
			}
			$i = 0;
			while ($i < count($cid)) {
				$cid[$i] = intval($cid[$i]);
				$i ++;
			}
			$cids = implode(',',$cid);
			$query = "INSERT INTO #__lms_learn_path_steps"
			. "\n (course_id, lpath_id, item_id, step_type, parent_id, step_name, step_description, ordering)"
			. "\n SELECT $course_id, $lpath_id, id, 6, $lpath_chapter, lpath_name, '".$step_description."', $new_order"
			. "\n FROM #__lms_learn_paths WHERE id IN ( $cids ) AND course_id = $course_id AND (lp_type IN (1,2) OR (lp_type = 0 AND item_id <> 0) )";
			$JLMS_DB->SetQuery( $query );
			$JLMS_DB->query();
			
			$insert_id = $JLMS_DB->insertid();
			if($insert_id){
				$query = "UPDATE #__lms_learn_path_steps SET params = ".$JLMS_DB->Quote($params)." WHERE id = '".$insert_id."'";
				$JLMS_DB->setQuery($query);
				$JLMS_DB->query();
			}
			
			if($JLMS_DB->getErrorMsg()){
				JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id", _JLMS_DB_CHECK) );
			}
			
			$query = "SELECT id FROM #__lms_learn_path_steps WHERE course_id = $course_id AND lpath_id = $lpath_id AND parent_id = $lpath_chapter ORDER BY ordering, step_name, id";
			$JLMS_DB->SetQuery( $query );
			$id_array = JLMSDatabaseHelper::LoadResultArray();
			if (count($id_array)) { JLMS_orderLPathStepList($id_array); }
		}
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=compose_lpath&course_id=$course_id&id=$lpath_id") );
	} else {
		JLMSRedirect( sefRelToAbs("index.php?option=$option&Itemid=$Itemid&task=learnpaths&id=$course_id") );
	}
}
?>