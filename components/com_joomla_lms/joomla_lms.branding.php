<?php
/**
* joomla_lms.branding.php
* JoomaLMS eLearning Software http://www.joomlalms.com/
* * * (c) ElearningForce Inc - http://www.elearningforce.biz/
**/

defined( '_JLMS_EXEC' ) or die( 'Restricted access' );

if (!function_exists('JLMS_cp1251_to_utf8')) {
	if (!defined('_JOOMLMS_FRONT_HOME')) { define('_JOOMLMS_FRONT_HOME', dirname(__FILE__)); }
	require_once(_JOOMLMS_FRONT_HOME. "/includes/libraries/lms.lib.language.php");
}

function JLMS_showPoweredBy($skip_indent = false) {
	global $JLMS_CONFIG;
	$branding_option = 0;
	$brand_options = array();
	$branding_option = $JLMS_CONFIG->get('branding_option', 0);
	if (!$branding_option) {
		/*$branding_option = rand(0, 7);*/
		$branding_option = rand(8, 9);
		$branding_option_ins = $branding_option + 1;
		global $JLMS_DB;
		$query = "DELETE FROM #__lms_config WHERE lms_config_var = 'branding_option'";
		$JLMS_DB->SetQuery($query);
		$JLMS_DB->query();
		$query = "INSERT INTO #__lms_config (lms_config_var, lms_config_value) VALUES ('branding_option', '$branding_option_ins')";
		$JLMS_DB->SetQuery($query);
		$JLMS_DB->query();
	} else {
		$branding_option = $branding_option - 1;
	}

	/*$new_option = new stdClass();
	$new_option->poweredby = '<a target="_blank" href="http://www.joomlalms.com/" title="SCORM compliant E-learning software for Joomla, cost-effective LMS solution and rapid authoring tools">Learning Management System powered by Joomla<span style="font-size:1px">&nbsp;</span>LMS</a>';
	$new_option->copyright = 'Copyright &copy; '.date('Y').', <a target="_blank" href="http://www.elearningforce.biz/" title="Best Joomla extensions, components, modules ">Elearningforce</a>';
	$brand_options[] = $new_option;

	$new_option = new stdClass();
	$new_option->poweredby = '<a target="_blank" href="http://www.joomlalms.com/" title="Educational software, learning system and LMS with built in authoring software ">Learning system powered by Joomla<span style="font-size:1px">&nbsp;</span>LMS software</a>';
	$new_option->copyright = 'Copyright &copy; '.date('Y').', <a target="_blank" href="http://www.elearningforce.biz/" title="Professional Joomla extensions, joomla components, joomla modules, Joomla templates, joomla add-ons and plugins">Elearningforce</a>';
	$brand_options[] = $new_option;

	$new_option = new stdClass();
	$new_option->poweredby = '<a target="_blank" href="http://www.joomlalms.com/" title="Affordable learning system for non-technicians, cost-effective Learning management software">E-Learning software powered by Joomla<span style="font-size:1px">&nbsp;</span>LMS system</a>';
	$new_option->copyright = 'Copyright &copy; '.date('Y').', <a target="_blank" href="http://www.elearningforce.biz/" title="Great Joomla components, modules, extensions">Elearningforce</a>';
	$brand_options[] = $new_option;

	$new_option = new stdClass();
	$new_option->poweredby = '<a target="_blank" href="http://www.joomlalms.com/" title="Learn with joy using Joomla LMS learning system, authoring tools">Educational software powered by Joomla<span style="font-size:1px">&nbsp;</span>LMS</a>';
	$new_option->copyright = 'Copyright &copy; '.date('Y').', <a target="_blank" href="http://www.elearningforce.biz/" title="Popular Joomla extensions, components, modules">Elearningforce</a>';
	$brand_options[] = $new_option;

	$new_option = new stdClass();
	$new_option->poweredby = '<a target="_blank" href="http://www.joomlalms.com/" title="SCORM compliant affordable Learning Management System(LMS) with built in rapid authoring tools">Learning Management System powered by Joomla<span style="font-size:1px">&nbsp;</span>LMS</a>';
	$new_option->copyright = 'Copyright &copy; '.date('Y').', <a target="_blank" href="http://www.elearningforce.biz/" title="Best Joomla extensions, components, modules">Elearningforce</a>';
	$brand_options[] = $new_option;

	$new_option = new stdClass();
	$new_option->poweredby = '<a target="_blank" href="http://www.joomlalms.com/" title="Educational and learning tools, learning management system with built in authoring software">Learning system powered by Joomla<span style="font-size:1px">&nbsp;</span>LMS software</a>';
	$new_option->copyright = 'Copyright &copy; '.date('Y').', <a target="_blank" href="http://www.elearningforce.biz/" title="Professional Joomla extensions, joomla components, joomla modules, Joomla templates, joomla add-ons and plugins">Elearningforce</a>';
	$brand_options[] = $new_option;

	$new_option = new stdClass();
	$new_option->poweredby = '<a target="_blank" href="http://www.joomlalms.com/" title="Professional learning system, cost-effective educational management software">E-Learning software powered by Joomla<span style="font-size:1px">&nbsp;</span>LMS system</a>';
	$new_option->copyright = 'Copyright &copy; '.date('Y').', <a target="_blank" href="http://www.elearningforce.biz/" title="Great Joomla components, modules, extensions">Elearningforce</a>';
	$brand_options[] = $new_option;

	$new_option = new stdClass();
	$new_option->poweredby = '<a target="_blank" href="http://www.joomlalms.com/" title="Use the power of E-learning software utilizing Joomla LMS educational system, authoring tools">Educational software powered by Joomla Learning system</a>';
	$new_option->copyright = 'Copyright &copy; '.date('Y').', <a target="_blank" href="http://www.elearningforce.biz/" title="Popular Joomla extensions, components, modules">Elearningforce</a>';
	$brand_options[] = $new_option;*/
	
	$new_option = new stdClass();
	$new_option->poweredby = 'LMS e-learning software by <a target="_blank" href="http://www.joomlalms.com/" rel="nofollow">JoomlaLMS</a>';
	$new_option->copyright = 'Copyright &copy; 2006 - '.date('Y');
	$brand_options[] = $new_option;
	$brand_options[] = $new_option;
	$brand_options[] = $new_option;
	$brand_options[] = $new_option;

	$new_option = new stdClass();
	$new_option->poweredby = 'Learning Management System by <a  target="_blank" href="http://www.joomlalms.com/"  rel="nofollow">JoomlaLMS</a>';
	$new_option->copyright = 'Copyright &copy; 2006 - '.date('Y');
	$brand_options[] = $new_option;
	$brand_options[] = $new_option;
	$brand_options[] = $new_option;
	$brand_options[] = $new_option;

	$new_option = new stdClass();
	$new_option->poweredby = 'eLearning software by <a target="_blank" href="http://www.joomlalms.com/" title="eLearning software by JoomlaLMS" rel="nofollow">JoomlaLMS</a>';
	$new_option->copyright = 'Copyright &copy; 2006 - '.date('Y');
	$brand_options[] = $new_option;

	$new_option = new stdClass();
	$new_option->poweredby = '<a target="_blank" href="http://www.joomlalms.com/" title="JoomlaLMS Learning Management System" rel="nofollow">JoomlaLMS</a> Learning Management System';
	$new_option->copyright = 'Copyright &copy; 2006 - '.date('Y');
	$brand_options[] = $new_option;

	if ($JLMS_CONFIG->get('default_language', 'english') == 'russian') {
		$brand_options = array();
		$new_option = new stdClass();
		$new_option->poweredby = 'Система дистанционного обучения <a target="_blank" href="http://elearningsoft.ru/joomla-lms.html" title="Система дистанционного обучения JoomlaLMS" rel="nofollow">JoomlaLMS</a>';
		$new_option->copyright = 'Copyright &copy; 2006 - '.date('Y');
		if (class_exists('JFactory')) { // Joomla 1.5
			$utf8_str = JLMS_cp1251_to_utf8("Система дистанционного обучения JoomlaLMS");
			$new_option->poweredby = '<a  target="_blank" href="http://elearningsoft.ru/joomla-lms.html" title="'.$utf8_str.'" rel="nofollow">'.$utf8_str.'</a>';
		}
		$brand_options[] = $new_option;
	}

	if(!isset($brand_options[$branding_option])) {
		$branding_option = 0;
	}
	echo '<div style="width:100%; text-align:center" align="center">';
		echo $skip_indent ? '' : '<br />';
		echo $brand_options[$branding_option]->poweredby;
		if ($brand_options[$branding_option]->copyright) {
			echo '<br />';
			echo $brand_options[$branding_option]->copyright;
		}
	echo '</div>';
	$secret_hash_key = '08r5328arlji15889s6401pbd9roeiqe';
	return $secret_hash_key;
}
?>