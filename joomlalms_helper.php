<?php
/**
 * 30 May 2014
 * JoomlaLMS Installation Helper 1.3.3
 * 
 * Powered by JoomlaLMS
 * 
 * * *
 * Put this file into your Joomla root directory.
 */

$GLOBALS['JLMSHLPR'] = array();
global $JLMSHLPR;
$JLMSHLPR['HLPR_VERSION'] = 133;
$JLMSHLPR['HLPR_VERSION_JOOMLA_MAX_OFFICIAL'] = 33;
$JLMSHLPR['JOOMLA_MAX_VERSION'] = '3.3';
$JLMSHLPR['JOOMLA_MIN_VERSION'] = '1.5';
$JLMSHLPR['PHP_MAX_VERSION'] = '5.5';
$JLMSHLPR['PHP_MIN_VERSION'] = '5.3';
$JLMSHLPR['PHP_SEC_MAX_VERSION'] = '5.4';
$JLMSHLPR['PHP_SEC_MIN_VERSION'] = '5.0';
$JLMSHLPR['IONCUBE_MAX_VERSION'] = 'X';
$JLMSHLPR['IONCUBE_MIN_VERSION'] = '4.4';
$JLMSHLPR['IONCUBE_SEC_MAX_VERSION'] = 'X';
$JLMSHLPR['IONCUBE_SEC_MIN_VERSION'] = '4.0';

@set_time_limit(3600);
if (!defined('_JEXEC')) { define( '_JEXEC', 1 ); }
if ( !defined( '_JLMS_EXEC' ) ) { define( '_JLMS_EXEC', 1 ); }

if (!defined('JPATH_BASE')) { define('JPATH_BASE', dirname(__FILE__) ); }
if (!defined('DS')) { define( 'DS', DIRECTORY_SEPARATOR ); }
if (defined('E_STRICT')) {
	//hide "Strict Standards:" PHP warnings
	$errorlevel_original=error_reporting();
	$error_bits = array();
	$errorlevel = $errorlevel_original;
	while ($errorlevel > 0) {
		for($i = 0, $n = 0; $i <= $errorlevel; $i = 1 * pow(2, $n), $n++) {
			$end = $i;
		}
		$error_bits[] = $end;
		$errorlevel = $errorlevel - $end;
	}
	if (defined('E_STRICT') && in_array(E_STRICT, $error_bits)) {
		error_reporting($errorlevel_original ^ E_STRICT);
	}
}

function joomlaVersion() 
{
	static $version;
	 
	if( !isset($version) ) 
	{ 
		if (file_exists(JPATH_BASE.'/includes/joomla.php') && file_exists(JPATH_BASE.'/globals.php') && file_exists(JPATH_BASE.'/configuration.php')) 	{
			$version = 10;		
		} else if (file_exists(JPATH_BASE.'/includes/joomla.php') && file_exists(JPATH_BASE.'/configuration.php') && file_exists(JPATH_BASE.'/libraries/loader.php')) {	
			$version = 15;			
		} else if ( file_exists(JPATH_BASE.'/configuration.php') && file_exists(JPATH_BASE.'/libraries/loader.php') ) {		
			$version = 16;			
		}
	}
	
	return $version;
}

function jlmsJ30() 
{
	$version = new JVersion();
	
	if( strnatcasecmp( $version->RELEASE, '3.0' ) >= 0 ) 
	{
		return true; 
	} else {
		return false;
	}
}
//

$step = isset($_REQUEST['step'])? $_REQUEST['step'] : '';
if (!in_array($step, array('image_tr','image_tl','image_t','image_br','image_bl','image_b','image_tick','image_error','image_warning','image_back','image_forward', 'getcss'))) {	
	switch( joomlaVersion() ) 
	{		
		case 15:
			require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );
			require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );	
					
			if (class_exists('JApplication')) {
				$mainframe = JApplication::getInstance('site');
			} else {
				$mainframe = JFactory::getApplication('site');
			}
	
			if (!class_exists('JPluginHelper')) { // J1.5RC4 compat
				$mainframe->initialise();
			}
	
			JPluginHelper::importPlugin('system');
			require_once(JPATH_BASE.DS.'administrator'.DS.'includes'.DS.'pcl'.DS.'pclzip.lib.php');
			require_once(JPATH_BASE.DS.'administrator'.DS.'includes'.DS.'pcl'.DS.'pclerror.lib.php' );
		break;
		case 16:
			define('_JREQUEST_NO_CLEAN', 1 );		
			require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );		
			require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );		
			jimport('joomla.plugin.helper');			
				
			$app = JFactory::getApplication('site');		
			$app->initialise();	
			
			$database = JFactory::getDBO();
	
			JPluginHelper::importPlugin('system');		
			
			jimport('joomla.filesystem.archive');	
		break;
		
		default:
			LH_MainPage();
			die;	
	}	
	
	if( joomlaVersion() == 15 || joomlaVersion() == 16 ) 
	{		
		jimport('joomla.installer.installer');
		class JLMSInstaller extends JInstaller 
		{
			function resetSteps() 
			{
				$this->_stepStack = array(); 
			}
			
			public static function getObjInstance()
			{
				static $instance;
		
				if (!isset ($instance)) {
					$instance = new JLMSInstaller();
				}
				return $instance;
			}
		}	
	}			
}
$step = isset($_REQUEST['step'])? $_REQUEST['step'] : '';

//echo $step; die;

switch ($step) {
	case 'image_tr':
	case 'image_tl':
	case 'image_t':
	case 'image_br':
	case 'image_bl':
	case 'image_b':
	case 'image_tick':
	case 'image_error':
	case 'image_warning':
	case 'image_back':
	case 'image_forward':
		LH_ShowImage($step);
	break;
	case 'getcss':
		LH_GetCSS();
	break;
	case 'preinstall':
		LH_PreInstallCheck();
	break;
	case 'install':
		LH_DoInstall();
	break;
	case 'clear_directory':
		LH_ClearDirectory();
	break;	
	default:
		LH_MainPage();
	break;
}

function LH_ClearDirectory() {
	
	if (file_exists(JPATH_BASE."/components/com_joomla_lms/")) {
		jh_deldir(JPATH_BASE."/components/com_joomla_lms/");
	}
	if (file_exists(JPATH_BASE."/administrator/components/com_joomla_lms/")) {
		jh_deldir(JPATH_BASE."/administrator/components/com_joomla_lms/");
	}
	$app = JFactory::getApplication('site');
	
	$link = 'joomlalms_helper.php?step=preinstall';
	
	$app->redirect( JUri::base() . $link );		
}

function LH_DoInstall() {	
	LH_ShowBegin();
	
	$database = JFactory::getDBO();	
 						
	$lang = JFactory::getLanguage();
	$lang->load( 'com_installer', JPATH_BASE. DS . 'administrator');
	jimport( 'joomla.application.component.model' );
	jimport( 'joomla.installer.installer' );
	//JPluginHelper::importPlugin('system');
	$installer = JLMSInstaller::getObjInstance();	

	if( joomlaVersion() == 16 ) 
	{
		//smt 3.0 update : id -> extension_id
		$query = "SELECT `extension_id` FROM #__extensions WHERE `type` = 'component' AND `element` = 'com_joomla_lms'";
		$database->SetQuery($query);
		$res = $database->LoadResult();		
	} else {
		$query = "SELECT id FROM #__components WHERE `option` = 'com_joomla_lms' AND parent = 0 AND iscore = 0";
		$database->SetQuery($query);
		$res = $database->LoadResult();
	}
		
	$upload_dir_temp = '';
	if ($res) {
		$temp_folder_upload = md5(uniqid(rand(), true));
		$temp_path_upload = dirname(__FILE__). "/tmp/".$temp_folder_upload;
		if (mkdir($temp_path_upload)) {
			$upload_dir_temp = $temp_folder_upload;
			jh_copyUploadDirr(dirname(__FILE__)."/components/com_joomla_lms/upload", $temp_path_upload);
		}
		$installer = JLMSInstaller::getObjInstance();
		$result = false;
					
		$result	= $installer->uninstall('component', $res );
		if ($result === false) {
			echo "<div class='jlms_test_error'><span class='jlms_txt_error'>Installer</span><br />An error occurred during uninstallation of the previous version.</div>";
		} else {
			echo "<div class='jlms_test_ok'><span class='jlms_txt_ok'>Installer</span><br />Previous version has been successfully removed.</div>";
		}
	}
	
	$zip_file = LH_SearchForFile(dirname(__FILE__));
	$next = true;
	if ($zip_file) {			
		$jtf_folder = 'tmp';
		$temp_folder1 = md5(uniqid(rand(), true));
		$temp_path1 = dirname(__FILE__). "/tmp/".$temp_folder1;
		if (mkdir($temp_path1)) {
			$jtf_folder = 'tmp/'.$temp_folder1;
		}
		$paths = LH_PreparePackage($zip_file, $jtf_folder);
		
		$install_path = isset($paths['install_path']) ? $paths['install_path'] : '';
		$error_log = isset($paths['error_log']) ? $paths['error_log'] : '';
		$menu_module = isset($paths['menu_module']) ? $paths['menu_module'] : '';
		$menu_module_new = isset($paths['menu_module_new']) ? $paths['menu_module_new'] : '';
        $mailbox_module = isset($paths['mailbox_module']) ? $paths['mailbox_module'] : '';
        $homework_module = isset($paths['homework_module']) ? $paths['homework_module'] : '';
        $dropbox_module = isset($paths['dropbox_module']) ? $paths['dropbox_module'] : '';
        $certificates_module = isset($paths['certificates_module']) ? $paths['certificates_module'] : '';
        $forum_posts_module = isset($paths['forum_posts_module']) ? $paths['forum_posts_module'] : '';
        $announcements_module = isset($paths['announcements_module']) ? $paths['announcements_module'] : '';
               
		$courses_module = isset($paths['courses_module']) ? $paths['courses_module'] : '';
		$courses_module_new = isset($paths['courses_module_new']) ? $paths['courses_module_new'] : '';
		$search_bot = isset($paths['search_bot']) ? $paths['search_bot'] : '';
		$search_bot_new = isset($paths['search_bot_new']) ? $paths['search_bot_new'] : '';
		$paypalbtn_bot = isset($paths['paypalbtn_bot']) ? $paths['paypalbtn_bot'] : '';
		$paypalbtn_bot_new = isset($paths['paypalbtn_bot_new']) ? $paths['paypalbtn_bot_new'] : '';
		$sqbox_bot_new = isset($paths['sqbox_bot_new']) ? $paths['sqbox_bot_new'] : '';
		$cb_plugin_old = isset($paths['cb_plugin_old']) ? $paths['cb_plugin_old'] : '';
		$cb_plugin_new = isset($paths['cb_plugin_new']) ? $paths['cb_plugin_new'] : '';

		if ($error_log) {
			echo "<div class='jlms_test_error'><span class='jlms_txt_error'>Errors occurres during the installation:</span><br />".$error_log."</div>";
		}
		
		if ($install_path) {
			if (!file_exists($install_path."/index.html")) 
			{
				if (file_exists(dirname(__FILE__)."/components/index.html")) {
					@copy(dirname(__FILE__)."/components/index.html", $install_path."/index.html");
				}
			}
							
			$installer = JLMSInstaller::getObjInstance();
			$path = JPath::clean( $install_path );					

			if (!is_dir( $path )) {
				$path = dirname( $path );
			}
			
						
			@ob_start();
			echo '---';
			jh_KeepDB();				
 
			 $result = $installer->install( $path );

			 if (jlmsJ30())
				{
				$start_path = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_joomla_lms'.DS;
				if (file_exists($start_path. 'install.joomla_lms.php'))
				{
					require_once($start_path. 'install.joomla_lms.php');
					com_install();
					
				}				
				}
			@ob_end_clean();			
			$add_notes = '';
			if ($result) {
				if( joomlaVersion() == 16 ) 
				{
					jimport('joomla.filesystem.file');
										
					$metadata_file = JPATH_SITE.DS.'components'.DS.'com_joomla_lms'.DS.'metadata.xml';
					$metadata_file_renamed = JPATH_SITE.DS.'components'.DS.'com_joomla_lms'.DS.'metadata.xml_';
					
					if ( JFile::exists( $metadata_file ) ) 
					{
						JFile::move( $metadata_file, $metadata_file_renamed );
					}															
				} 
													
				jh_KeepDB();
				$installer->resetSteps();        
       
                if ($announcements_module) {					
					$minstaller = JLMSInstaller::getObjInstance();
					$path = JPath::clean( $announcements_module );
					if (!is_dir( $path )) {
						$path = dirname( $path );
					}
					@ob_start();
					echo '---';
					$result = $minstaller->install( $path );
					@ob_end_clean();
					if ($result) {
						$add_notes .= '<br /><font color="green"><b>JoomlaLMS announcements module has been successfully installed. You can publish it later.</b></font>';
					} else {
						$add_notes .= '<br /><font color="orange"><b>JoomlaLMS announcements module was not installed.</b></font>';
					}												
											
				}
				if ($announcements_module) { jh_deldir($announcements_module."/"); }
				jh_KeepDB();
                
                if ($forum_posts_module) {					
					$minstaller = JLMSInstaller::getObjInstance();
					$path = JPath::clean( $forum_posts_module );
					if (!is_dir( $path )) {
						$path = dirname( $path );
					}
					@ob_start();
					echo '---';
					$result = $minstaller->install( $path );
					@ob_end_clean();
					if ($result) {
						$add_notes .= '<br /><font color="green"><b>JoomlaLMS forum posts module has been successfully installed. You can publish it later.</b></font>';
					} else {
						$add_notes .= '<br /><font color="orange"><b>JoomlaLMS forum posts module was not installed.</b></font>';
					}												
											
				}
				if ($forum_posts_module) { jh_deldir($forum_posts_module."/"); }
				jh_KeepDB();
                
                if ($certificates_module) {					
					$minstaller =JLMSInstaller::getObjInstance();
					$path = JPath::clean( $certificates_module );
					if (!is_dir( $path )) {
						$path = dirname( $path );
					}
					@ob_start();
					echo '---';
					$result = $minstaller->install( $path );
					@ob_end_clean();
					if ($result) {
						$add_notes .= '<br /><font color="green"><b>JoomlaLMS certificates module has been successfully installed. You can publish it later.</b></font>';
					} else {
						$add_notes .= '<br /><font color="orange"><b>JoomlaLMS certificates module was not installed.</b></font>';
					}											
										
				}
				if ($certificates_module) { jh_deldir($certificates_module."/"); }
				jh_KeepDB();
                
                if ($dropbox_module) {					
					$minstaller = JLMSInstaller::getObjInstance();
					$path = JPath::clean( $dropbox_module );
					if (!is_dir( $path )) {
						$path = dirname( $path );
					}
					@ob_start();
					echo '---';
					$result = $minstaller->install( $path );
					@ob_end_clean();
					if ($result) {
						$add_notes .= '<br /><font color="green"><b>JoomlaLMS dropbox module has been successfully installed. You can publish it later.</b></font>';
					} else {
						$add_notes .= '<br /><font color="orange"><b>JoomlaLMS dropbox module was not installed.</b></font>';
					}												
											
				}
				if ($dropbox_module) { jh_deldir($dropbox_module."/"); }
				jh_KeepDB();
                
                if ($homework_module) {					
					$minstaller = JLMSInstaller::getObjInstance();
					$path = JPath::clean( $homework_module );
					if (!is_dir( $path )) {
						$path = dirname( $path );
					}
					@ob_start();
					echo '---';
					$result = $minstaller->install( $path );
					@ob_end_clean();
					if ($result) {
						$add_notes .= '<br /><font color="green"><b>JoomlaLMS homework module has been successfully installed. You can publish it later.</b></font>';
					} else {
						$add_notes .= '<br /><font color="orange"><b>JoomlaLMS homework module was not installed.</b></font>';
					}											
											
				}
				if ($mailbox_module) { jh_deldir($homework_module."/"); }
				jh_KeepDB();

                if ($mailbox_module) {					
					$minstaller = JLMSInstaller::getObjInstance();
					$path = JPath::clean( $mailbox_module );
					if (!is_dir( $path )) {
						$path = dirname( $path );
					}
					@ob_start();
					echo '---';
					$result = $minstaller->install( $path );
					@ob_end_clean();
					if ($result) {
						$add_notes .= '<br /><font color="green"><b>JoomlaLMS mailbox module has been successfully installed. You can publish it later.</b></font>';
					} else {
						$add_notes .= '<br /><font color="orange"><b>JoomlaLMS mailbox module was not installed.</b></font>';
					}				
											
				}
				if ($mailbox_module) { jh_deldir($mailbox_module."/"); }
				jh_KeepDB();

				/* LMS menu module */
				if ($menu_module_new) {					
					$minstaller = JLMSInstaller::getObjInstance();
					$path = JPath::clean( $menu_module_new );
					if (!is_dir( $path )) {
						$path = dirname( $path );
					}
					@ob_start();
					echo '---';
					$result = $minstaller->install( $path );
					@ob_end_clean();
					if ($result) {
						$add_notes .= '<br /><font color="green"><b>JoomlaLMS menu module has been successfully installed. You can publish it later.</b></font>';
					} else {
						$add_notes .= '<br /><font color="orange"><b>JoomlaLMS menu module was not installed.</b></font>';
					}	
											
				}
				if ($menu_module) { jh_deldir($menu_module."/"); }
				jh_KeepDB();

				/* LMS courses module */
				if ($courses_module_new) {					
					$minstaller =JLMSInstaller::getObjInstance();
					$path = JPath::clean( $courses_module_new );
					if (!is_dir( $path )) {
						$path = dirname( $path );
					}
					@ob_start();
					echo '---';
					$result = $minstaller->install( $path );
					@ob_end_clean();
					if ($result) {
						$add_notes .= '<br /><font color="green"><b>JoomlaLMS courses module has been successfully installed. You can publish it later.</b></font>';
					} else {
						$add_notes .= '<br /><font color="orange"><b>JoomlaLMS courses module was not installed.</b></font>';
					}				
				}
				if ($courses_module) { jh_deldir($courses_module."/"); }
				jh_KeepDB();

				/* LMS search bot */
				if ($search_bot_new) {					
					$binstaller = JLMSInstaller::getObjInstance();
					$path = JPath::clean( $search_bot_new );
					if (!is_dir( $path )) {
						$path = dirname( $path );
					}
					@ob_start();
					echo '---';
					$result = $binstaller->install( $path );
					@ob_end_clean();
					if ($result) {
						$add_notes .= '<br /><font color="green"><b>JoomlaLMS search plugin has been successfully installed. You can publish it later.</b></font>';
					} else {
						$add_notes .= '<br /><font color="orange"><b>JoomlaLMS search plugin was not installed.</b></font>';
					}				
					jh_deldir($search_bot_new."/");
				}
				if ($search_bot) { jh_deldir($search_bot."/"); }
				jh_KeepDB();

				/* PaypalBtn content bot */
				if ($paypalbtn_bot_new) {				
					$binstaller = JLMSInstaller::getObjInstance();
					$path = JPath::clean( $paypalbtn_bot_new );
					if (!is_dir( $path )) {
						$path = dirname( $path );
					}
					@ob_start();
					echo '---';
					$result = $binstaller->install( $path );
					@ob_end_clean();
					if ($result) {
						$add_notes .= '<br /><font color="green"><b>JoomlaLMS PayPalButton content plugin has been successfully installed. You can publish it later.</b></font>';
					} else {
						$add_notes .= '<br /><font color="orange"><b>JoomlaLMS PayPalButton content plugin was not installed.</b></font>';
					}					
					jh_deldir($paypalbtn_bot_new."/");
				}
				if ($paypalbtn_bot) { jh_deldir($paypalbtn_bot."/"); }
				jh_KeepDB();

				/* SquezzeBox content bot */
				if ($sqbox_bot_new) 
                {				
					$binstaller = JLMSInstaller::getObjInstance();
					$path = JPath::clean( $sqbox_bot_new );
					if (!is_dir( $path )) {
						$path = dirname( $path );
					}
					@ob_start();
					echo '---';
					$result = $binstaller->install( $path );
					@ob_end_clean();
					if ($result) {
						$add_notes .= '<br /><font color="green"><b>JoomlaLMS SqueezeBox content plugin has been successfully installed. You can publish it later.</b></font>';
					} else {
						$add_notes .= '<br /><font color="orange"><b>JoomlaLMS SqueezeBox content plugin was not installed.</b></font>';
					}
					
					jh_deldir($sqbox_bot_new."/");
				}
				jh_KeepDB();

				/* LMS CB plugins */
				if ($cb_plugin_old || $cb_plugin_new) {
					$ueConfig = array();
					if (file_exists(dirname(__FILE__). "/administrator/components/com_comprofiler/ue_config.php" )) {
						require_once(dirname(__FILE__). "/administrator/components/com_comprofiler/ue_config.php" );
					}
					$do_cb_plugin = '';
					if (isset($ueConfig['version']) && $ueConfig['version'] == '1.1' && $cb_plugin_new) {
						$do_cb_plugin = $cb_plugin_new;
					} elseif (isset($ueConfig['version']) && $ueConfig['version'] == '1.0.2' && $cb_plugin_old) {
						$do_cb_plugin = $cb_plugin_old;
					}
					if ($do_cb_plugin) {
						$query = "SELECT count(*) FROM #__comprofiler_plugin WHERE `element` = 'cb.joomlalms.autoenroll' AND type = 'user'";
						$database->SetQuery($query);
						$is_cb_i = $database->LoadResult();
						if ($is_cb_i) {
							if (is_writable(dirname(__FILE__). "/components/com_comprofiler/plugin/user/plug_joomlalmsautoenroll/cb.joomlalms.autoenroll.php") && is_writable(dirname(__FILE__). "/components/com_comprofiler/plugin/user/plug_joomlalmsautoenroll/cb.joomlalms.autoenroll.xml")) {
								@copy($do_cb_plugin."/cb.joomlalms.autoenroll.php", dirname(__FILE__). "/components/com_comprofiler/plugin/user/plug_joomlalmsautoenroll/cb.joomlalms.autoenroll.php");
								@copy($do_cb_plugin."/cb.joomlalms.autoenroll.xml", dirname(__FILE__). "/components/com_comprofiler/plugin/user/plug_joomlalmsautoenroll/cb.joomlalms.autoenroll.xml");
								@copy($do_cb_plugin."/index.html", dirname(__FILE__). "/components/com_comprofiler/plugin/user/plug_joomlalmsautoenroll/index.html");
								$add_notes .= '<br /><font color="green"><b>JoomlaLMS CB autoenrollment plugin has been successfully updated.</b></font>';
							} else {
								$add_notes .= '<br /><font color="orange"><b>JoomlaLMS CB autoenrollment plugin was not updated. Files are not writable.</b></font>';
							}
						} else {
							@mkdir(dirname(__FILE__). "/components/com_comprofiler/plugin/user/plug_joomlalmsautoenroll");
							if (file_exists(dirname(__FILE__). "/components/com_comprofiler/plugin/user/plug_joomlalmsautoenroll")) {
								@copy($do_cb_plugin."/cb.joomlalms.autoenroll.php", dirname(__FILE__). "/components/com_comprofiler/plugin/user/plug_joomlalmsautoenroll/cb.joomlalms.autoenroll.php");
								@copy($do_cb_plugin."/cb.joomlalms.autoenroll.xml", dirname(__FILE__). "/components/com_comprofiler/plugin/user/plug_joomlalmsautoenroll/cb.joomlalms.autoenroll.xml");
								@copy($do_cb_plugin."/index.html", dirname(__FILE__). "/components/com_comprofiler/plugin/user/plug_joomlalmsautoenroll/index.html");
								$query = "INSERT INTO #__comprofiler_plugin (name, element, type, folder, access, ordering, published, iscore, client_id)"
								. "\n" . "VALUES ('JoomlaLMS AutoEnroll', 'cb.joomlalms.autoenroll', 'user', 'plug_joomlalmsautoenroll', 0, 0, 0, 0, 0)";
								$database->SetQuery($query);
								$database->query();
								$add_notes .= '<br /><font color="green"><b>JoomlaLMS CB autoenrollment plugin has been successfully installed. You can publish it later.</b></font>';
							} else {
								$add_notes .= '<br /><font color="orange"><b>JoomlaLMS CB autoenrollment plugin was not installed.</b></font>';
							}
						}
					}
					if ($cb_plugin_old) { jh_deldir($cb_plugin_old."/"); }
					if ($cb_plugin_new) { jh_deldir($cb_plugin_new."/"); }
				}

				if ($upload_dir_temp) {
					$temp_path_upload = dirname(__FILE__). "/tmp/".$upload_dir_temp;
					jh_copyUploadDirr($temp_path_upload, dirname(__FILE__)."/components/com_joomla_lms/upload");
					jh_deldir($temp_path_upload."/");
				}
				echo "<div class='jlms_test_ok'><span class='jlms_txt_ok'>Installer</span><br />Trying to install $zip_file<br /><font color='green'><b>New component has been successfully installed.</b></font>".$add_notes."</div>";
				$jlms_admin_mainfile = dirname(__FILE__)."/administrator/components/com_joomla_lms/admin.joomla_lms.php";
				$jlms_front_mainfile = dirname(__FILE__)."/components/com_joomla_lms/joomla_lms.php";
				if (file_exists($jlms_admin_mainfile) && file_exists($jlms_front_mainfile)) {
					// life is good ;)
					jimport('joomla.filesystem.folder');
					//SMT 3.0d update
					require_once(JPATH_SITE . DS . 'components' . DS . 'com_joomla_lms' . DS . "includes" . DS . "classes" . DS . "lms.factory.php");
					//
					
					require_once(JPATH_SITE . DS . 'components' . DS . 'com_joomla_lms' . DS . "includes".DS."libraries".DS."lms.lib.language.php");
					
					$folders = JFolder::folders(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_joomla_lms'.DS.'language');
					
					$lang = JFactory::getLanguage();
					$locale = $lang->getLocale();
					
					if( $folders && $locale ) 
					{
						$intersect = array_intersect( $folders, $locale );
					}
					
					if( isset($intersect[0]) )
						$languageName = $intersect[0];   
					
					global $JLMS_LANGUAGE;
					JLMS_require_lang( $JLMS_LANGUAGE, 'admin.install.lang', $languageName, 'backend', true );	 
					JLMS_processLanguage( $JLMS_LANGUAGE, false, 'backend' );
					
					$thakYouForInstall = str_replace( 'index.php?option=com_joomla_lms&task=lms_maintenance&page=check_database', JURI::base().'administrator/index.php?option=com_joomla_lms&task=lms_maintenance&page=check_database',_JLMS_THANK_YOU_FOR_INSTALL);
					$thakYouForInstall = str_replace( '../components/com_joomla_lms/lms_images/joomlalms_box.png', 'components/com_joomla_lms/lms_images/joomlalms_box.png',$thakYouForInstall);
									
					echo "<br /><div style=\"border: 1px solid #DDDDBB; margin-bottom: 3px; padding: 3px; \">".$thakYouForInstall."</div>";
					$jlms_license_file = dirname(__FILE__)."/joomlalms.txt";
					if (!file_exists($jlms_license_file)) {
						echo "<br /><div class='jlms_test_notice' style='text-align:left'><span class='jlms_txt_error'>IMPORTANT:</span> JoomlaLMS license file is missing. Please copy the license file to your site folder. You can donwload your license file from your MembersArea at <a href='http://www.joomlalms.com'>www.joomlalms.com</a>, where you can also download a trial license file if you haven't purchased JoomlaLMS yet.</div>";
					}
					
				} else {
					echo "<div class='jlms_test_warning'><span class='jlms_txt_ok'>Testing component installation.</span><br /><font color='red'><b>Seems like some component files are missing.</b></font><br />Under Joomla Administration Area check if the component is successfully installed and reinstall it if necessary.<br />Contact JoomlaLMS support if the problem occurs regularly.</div>";
				}
			} else {
				$txt_messages = array();
				
				$app = JFactory::getApplication('site');
				
				$messages = $app->getMessageQueue();
				// Build the sorted message list
				if (is_array($messages) && count($messages)) {
					foreach ($messages as $msg)
					{
						if (isset($msg['type']) && isset($msg['message'])) {
							$txt_messages[] = $msg['message'];
						}
					}
				}
				foreach ($txt_messages as $txt_message) {
					$add_notes .= '<br />'.$txt_message;
				}					
				echo "<div class='jlms_test_ok'><span class='jlms_txt_ok'>Installer</span><br />Trying to install $zip_file<br /><font color='red'><b>An error occurred during the component's installation.</b></font>".$add_notes."</div>";
			}				
			jh_deldir($install_path."/");
		}
	}


	echo "<div class='jlms_simple'>";
	LH_ShowPrevBtn('joomlalms_helper.php?step=preinstall');
	echo "<br /></div>";
	LH_ShowEnd();
}

function LH_PreInstallCheck() {	
	LH_ShowBegin();
	
	$result0 = LH_TestHelperVersion();
	if (!$result0['result']) {
		LH_ShowBlockResults($result0);
	}
	
	$database = JFactory::getDBO();	
		
	if( joomlaVersion() == 16 ) 
	{
		$query = "SELECT count(*) FROM #__extensions WHERE `type` = 'component' AND `element` = 'com_joomla_lms'";
		$database->SetQuery($query);
		$res = $database->LoadResult();		
	} else {
		$query = "SELECT count(*) FROM #__components WHERE `option` = 'com_joomla_lms'";
		$database->SetQuery($query);
		$res = $database->LoadResult();
	}
	
	if ($res) {
		$database->SetQuery("SELECT lms_config_value FROM #__lms_config WHERE lms_config_var = 'jlms_version'");
		$prev_version = $database->LoadResult();
		if ($prev_version) {
			echo "<div class='jlms_test_warning'><span class='jlms_txt_warning'>Checking for previous version</span><br />JoomlaLMS version $prev_version is installed. Do you want to proceed with the installation?<br />The current installation will be removed (all database information will be kept) and a new one will be installed.</div>";
		} else {
			echo "<div class='jlms_test_warning'><span class='jlms_txt_warning'>Checking for previous version</span><br />DB entries of another JoomlaLMS installation are found, but it seems that it was corrupted. You should proceed the installation.<br />The current installation will be removed (all database information will be kept) and a new one will be installed.</div>";
		}
	}
	$zip_file = LH_SearchForFile(dirname(__FILE__));
	$next = true;
	if ($zip_file) {
		echo "<div class='jlms_test_ok'><span class='jlms_txt_ok'>JoomlaLMS package</span><br />The installation package was found: <b>$zip_file</b></div>";
	} else {
		$next = false;
		echo "<div class='jlms_test_error'><span class='jlms_txt_error'>JoomlaLMS package</span><br />The installation package wasn't found. <b>Please upload JoomlaLMS package into the Joomla! CMS root folder.</b></div>";		
	}
	
	$wr_folder = dirname(__FILE__). "/tmp";
	
	if (is_writeable($wr_folder)) {
	} else {
		$next = false;
		echo "<div class='jlms_test_error'><span class='jlms_txt_error'>Temporary folder</span><br />Joomla! temporary folder (tmp) is not writable. <b>Please check permissions and try again.</b></div>";
	}

	$result1 = LH_TestPermis();
	//$status = $status && $result1['result'];
	if($result1[1] == 1)
		$next = false;
	LH_ShowBlockResults($result1[0],false);
	
	$result1jlms = LH_TestPermisJLMS();
	//$status = $status && $result1['result'];
	if(!$result1jlms['result']) {
		$result1jlms['class'] = 'error';
		LH_ShowBlockResults($result1jlms,false);
	}
	
	$result = LH_TestDirectory();
	//$status = $status && $result1['result'];
	if(isset($result['class']) && $result['class'] == 'ERROR') {
		$next = false;
	LH_ShowBlockResults($result,false);
	}
		
	
	if ($next) {
		echo "<div class='jlms_simple'>";
		LH_ShowPrevBtn('joomlalms_helper.php');
		LH_ShowNextBtn('joomlalms_helper.php?step=install');
		echo "<br /></div>";
	} else {
		echo "<br /><div class='jlms_test_notice' style='text-align:center'><span class='jlms_txt_error'>Installation process was stopped.</span></div>";		
	}
	LH_ShowEnd();
}

function LH_TestDirectory() {
	
	$database = JFactory::getDBO();	
	
	$ret = array('result' => true, 'class' => 'warning', 'caption' => 'Previous installation of the component was corrupted.', 'text' => 'Broken installation of the JoomlaLMS component was found. Before installing new you should remove broken files.<br /> <a href = "joomlalms_helper.php?step=clear_directory">Remove broken files</a>.', 'instructions' => '');

	if( joomlaVersion() == 16 ) 
	{
		$query = "SELECT count(*) FROM #__extensions WHERE `type` = 'component' AND `element` = 'com_joomla_lms'";
		$database->SetQuery($query);
		$res = $database->LoadResult();		
	} else {
		$query = "SELECT count(*) FROM #__components WHERE `option` = 'com_joomla_lms'";
		$database->SetQuery($query);
		$res = $database->LoadResult();
	}
	
	$files_front = LH_SearchForFileDirectory(dirname(__FILE__).DS."components".DS."com_joomla_lms");
	$files_back = LH_SearchForFileDirectory(dirname(__FILE__).DS."administrator".DS."components".DS."com_joomla_lms");
	
	if(($files_front || $files_back) && !$res) 
	{
		//$ret['instructions']='<a href = "joomlalms_helper.php?step=clear_directory">clear direcotory</a>';
		$ret['class'] = 'ERROR';		
	}
	else {
		$ret = array();
	}	
	
	return $ret;
}

function LH_SearchForFileDirectory($dir) {
	$priority = 10;
	$zip_file = '';
	$flag = 0;
	
	if ($current_dir = @opendir( $dir )) {		
		//$old_umask = umask(0);
		while ($entryname = readdir( $current_dir )) {
			if ($entryname != '.' and $entryname != '..') 
			{
				$flag = 1;
			}
		}
	closedir( $current_dir );
	}	
	
	return $flag;
}

function LH_TestPermis() {
	
	$ret = array('result' => true, 'class' => 'warning', 'caption' => 'Directory Permissions', 'text' => 'For all Joomla! functions and features to work ALL of the following directories should be writeable:', 'instructions' => '');
	
	$components_folder = dirname(__FILE__). "/components";
	$admin_components_folder = dirname(__FILE__). "/administrator/components";
	$modules_folder = dirname(__FILE__). "/modules";	
	$mambots_folder = dirname(__FILE__). "/plugins"; 
	$mambots_folder_system = dirname(__FILE__). "/plugins/system"; 
	$mambots_folder_search = dirname(__FILE__). "/plugins/search";	 
	
	$ret['instructions']='<table width="100%" class="table">
	';
	
	$flag = 0;
	$flag1 = 0;
	
	if (is_writeable($admin_components_folder)) {
		$ret['instructions'] .= "<tr><td>administrator/components/</td><td align='right'><b><font color=\"green\">Writeable</font></b></td></tr>";
	} else {
		$next = false;
		$ret['instructions'] .= "<tr><td>administrator/components/</td><td align='right'><b><font color=\"red\">Not Writeable</font></b></td></tr>";
		$flag =1;
		$flag1 = 1;
	}
	
	if (is_writeable($components_folder)) {
		$ret['instructions'] .= "<tr><td>components/</td><td align='right'><b><font color=\"green\">Writeable</font></b></td></tr>";
	} else {
		$next = false;
		$ret['instructions'] .= "<tr><td>components/</td><td align='right'><b><font color=\"red\">Not Writeable</font></b></td></tr>";
		$flag =1;
		$flag1 = 1;
	}
	
	if (is_writeable($modules_folder)) {
		$ret['instructions'] .= "<tr><td>modules/</td><td align='right'><b><font color=\"green\">Writeable</font></b></td></tr>";
	} else {
		$ret['instructions'] .= "<tr><td>modules/</td><td align='right'><b><font color=\"red\">Not Writeable</font></b></td></tr>";
		$flag =1;
	}
	
	if (is_writeable($mambots_folder_system)) {
		$ret['instructions'] .= "<tr><td>plugins/system</td><td align='right'><b><font color=\"green\">Writeable</font></b></td></tr>";
	} else {
		$ret['instructions'] .= "<tr><td>plugins/system</td><td align='right'><b><font color=\"red\">Not Writeable</font></b></td></tr>";
		$flag =1;
	}
	if (is_writeable($mambots_folder_search)) {
		$ret['instructions'] .= "<tr><td>plugins/search</td><td align='right'><b><font color=\"green\">Writeable</font></b></td></tr>";
	} else {
		$ret['instructions'] .= "<tr><td>plugins/search</td><td align='right'><b><font color=\"red\">Not Writeable</font></b></td></tr>";
		$flag =1;
	}
	if (is_writeable($mambots_folder)) {
		$ret['instructions'] .= "<tr><td>plugins/</td><td align='right'><b><font color=\"green\">Writeable</font></b></td></tr>";
	} else {
		$ret['instructions'] .= "<tr><td>plugins/</td><td align='right'><b><font color=\"red\">Not Writeable</font></b></td></tr>";
		$flag =1;
	}
	
	$ret['instructions'] .='</table>';
	
	if(!$flag)
		$ret['class'] = 'OK';

	if($flag1)
		$ret['class'] = 'ERROR';	
		
	
		$return[0] = $ret;
		$return[1] = $flag1;
		
	return $return;
}

function LH_TestPermisJLMS() {
	
	$ret = array('result' => true, 'class' => 'ok', 'caption' => 'JoomlaLMS system files', 'text' => 'The following system files should be writable in order to sucessfully reinstall JoomlaLMS', 'instructions' => '');

	if (file_exists(dirname(__FILE__). "/"."plugins/system/jlms/jlms.php")) {
		if (!is_writeable(dirname(__FILE__). "/"."plugins/system/jlms/jlms.php")) {
			$ret['result'] = false;
			$ret['class'] = 'warning';
			$ret['instructions']='<table width="100%" class="table">
			';
			$ret['instructions'] .= "<tr><td>plugins/system/jlms/jlms.php</td><td align='right'><b><font color=\"red\">Not Writeable</font></b></td></tr>";
			$ret['instructions'] .= "<tr><td>plugins/system/jlms/jlms.xml</td><td align='right'><b><font color=\"red\">Not Writeable</font></b></td></tr>";
			$ret['instructions'] .='</table>';
		}
	} elseif (file_exists(dirname(__FILE__). "/"."plugins/system/jlms.php")) {
		//joomla 1.5.x
		if (!is_writeable(dirname(__FILE__). "/"."plugins/system/jlms.php")) {
			$ret['result'] = false;
			$ret['class'] = 'warning';
			$ret['instructions']='<table width="100%" class="table">
			';
			$ret['instructions'] .= "<tr><td>plugins/system/jlms.php</td><td align='right'><b><font color=\"red\">Not Writeable</font></b></td></tr>";
			$ret['instructions'] .= "<tr><td>plugins/system/jlms.xml</td><td align='right'><b><font color=\"red\">Not Writeable</font></b></td></tr>";
			$ret['instructions'] .='</table>';
		}
	}
	return $ret;
}

function LH_GetCSS() {?>
html {
  height: 100%;
}
body {
	color:#333333;
	font-family:Helvetica,Arial,sans-serif;
	font-size:12px;
	line-height:1.3em;
	text-align:center;
}
div.nav_link a {
	font-size:12px;
}
a:hover {
	color:#0B3768;
}
a:link, a:visited {
	color:#135CAE;
}
div.center {
	text-align:center;
}
#page_bg {
	background:#F7F7F7 none repeat scroll 0%;
}
#whitebox {
	background:#FFFFFF none repeat scroll 0%;
	width:750px;
	margin:auto;
}
#whitebox div {
	text-align:left;
}
#whitebox_tr {
	background:transparent url(joomlalms_helper.php?step=image_tr) no-repeat scroll 100% 0pt;
	height:13px;
	overflow:hidden;
}
#whitebox_t {
	background:#FFFFFF url(joomlalms_helper.php?step=image_t) repeat-x left top;
}
#whitebox_tl {
	background:transparent url(joomlalms_helper.php?step=image_tl) no-repeat scroll 0pt;
}
#whitebox_m {
	border-left:1px solid #CCCCCC;
	border-right:1px solid #CCCCCC;
	width:auto;
	zoom:1;
	/*padding: 0px 7px;-right:7px;*/
}
#area {
	margin:0px 10px 0px 10px;
	zoom:1;
}
.clr {
	clear:both;
}
#whitebox_b {
	background:transparent url(joomlalms_helper.php?step=image_b) repeat-x scroll 0pt 100%;
}
#whitebox_bl {
	background:transparent url(joomlalms_helper.php?step=image_br) no-repeat scroll 100% 0pt;
}
#whitebox_br {
	background:transparent url(joomlalms_helper.php?step=image_bl) no-repeat scroll 0pt;
	height:13px;
}
#whitebox div {
	text-align:left;
}
#inst_footer {
	background:#F7F7F7 none repeat scroll 0%;
}
#inst_footer div {
	background:#F7F7F7 none repeat scroll 0%;
	text-align:right;
	font-size:10px;
	padding-right: 10px;
	padding-left: 10px;
}
#inst_tip {
	 zoom:1;
}
#inst_tip div {
	text-align:left;
	font-size:10px;
	padding-right: 10px;
	padding-left: 10px;
	font-style:italic;
}
div.jlms_test_ok {
	border: 1px solid #BBDDBB;
	background-color: #DDFFDD;
	margin-bottom:3px;
	padding:3px;
	zoom:1;
	background: #DDFFDD url(joomlalms_helper.php?step=image_tick) right no-repeat;
}
div.jlms_test_elements3 {
float:left;
width:234px;
}
div.jlms_test_elementsmargin3px {
margin-right:3px;
}
div.jlms_simple {
	padding:3px;
}
div.jlms_test_notice {
	border: 1px solid #DDDDBB;
	background-color: #FFFFDD;
	margin-bottom:3px;
	padding:3px;
	zoom:1;
}
div.jlms_test_error {
	border: 1px solid #DDBBBB;
	background-color: #FFDDDD;
	margin-bottom:3px;
	padding:3px;
	zoom:1;
	background: #FFDDDD url(joomlalms_helper.php?step=image_error) right no-repeat;
}
div.jlms_test_warning {
	border: 1px solid #DDDDBB;
	background-color: #FFFFDD;
	margin-bottom:3px;
	padding:3px;
	zoom:1;
	background: #FFFFDD url(joomlalms_helper.php?step=image_warning) right no-repeat;
}
span.jlms_txt_ok {
	color:green;
	font-weight:bold;
}
span.jlms_txt_error {
	color:red;
	font-weight:bold;
}
span.jlms_txt_warning {
	color:orange;
	font-weight:bold;
}
.table {
	color:#333333;
	font-family:Helvetica,Arial,sans-serif;
	font-size:12px;
	line-height:1.3em;
	text-align:left;
}
<?php
}

function LH_ShowImage($image) {
	header('Content-type: image/png');
	switch ($image){
		case 'image_tr':
			echo base64_decode("iVBORw0KGgoAAAANSUhEUgAAAA0AAAANCAIAAAD9iXMrAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAsSAAALEgHS3X78AAAAH3RFWHRTb2Z0d2FyZQBNYWNyb21lZGlhIEZpcmV3b3JrcyA4tWjSeAAAABZ0RVh0Q3JlYXRpb24gVGltZQAwNC8yNi8wNcVTO0AAAACsSURBVCiRhdA7CoQwFEbhPxIkIrfwAWKh4P63kk2IhVioYFIoBq+FM4MD4+TUX3WE1hrviEgplaaplBLfCWYGwMz7vhtj5nm21hJRURR3/XL3tm3r+35Zlqqqoih6dFfjOLZt2zTNRYOfCECWZXVdd13nnPvnAOR5TkTDMHgcgLIsjTHOOY9TSsVxPE2TxwFIkmRdV78jImut34VhCCB4+vdJCAEgOI6Dmb36BJoQTjhe4QLZAAAAAElFTkSuQmCC");
		break;
		case 'image_tl':
			echo base64_decode("iVBORw0KGgoAAAANSUhEUgAAAA0AAAANCAIAAAD9iXMrAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAsSAAALEgHS3X78AAAAH3RFWHRTb2Z0d2FyZQBNYWNyb21lZGlhIEZpcmV3b3JrcyA4tWjSeAAAABZ0RVh0Q3JlYXRpb24gVGltZQAwNC8yNi8wNcVTO0AAAACfSURBVCiRjdGxDoQgEATQQQk0QoAGS/3/j7KgoqFZEii2uMLk7rx46rT7kplkRWsNxzBzKaX3TkQAhBAAxLdj5pxzrdUY47231o7juJ8+rrWWUnLOxRi11j8l8o22bVuWJYSAs8i9LqW0rqv3/hQBGADknJ1zFwjAwMxENM/zBQIwlFKmaVJK3bje+7/tB0dExph7B0BK+cjtn7l3T/IChItCKKPq+94AAAAASUVORK5CYII=");
		break;
		case 'image_t':
			echo base64_decode("iVBORw0KGgoAAAANSUhEUgAAACgAAAAICAIAAAAEMCoMAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAsSAAALEgHS3X78AAAAH3RFWHRTb2Z0d2FyZQBNYWNyb21lZGlhIEZpcmV3b3JrcyA4tWjSeAAAABZ0RVh0Q3JlYXRpb24gVGltZQAwNC8yNi8wNcVTO0AAAAAhSURBVCiRYzx79izDQADG////D4jFTANi66jFoxbTEgAADZkFc6aJHYEAAAAASUVORK5CYII=");
		break;
		case 'image_b':
			echo base64_decode("iVBORw0KGgoAAAANSUhEUgAAADQAAAAKCAIAAAB0auphAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAsSAAALEgHS3X78AAAAH3RFWHRTb2Z0d2FyZQBNYWNyb21lZGlhIEZpcmV3b3JrcyA4tWjSeAAAABZ0RVh0Q3JlYXRpb24gVGltZQAwNC8yNi8wNcVTO0AAAAAuSURBVDiNY/z//z/DYAVMA+0AfGDUceSCUceRC0YdRy4Y1I5jOXv27EC7AScAAAfZBXra92rUAAAAAElFTkSuQmCC");
		break;
		case 'image_br':
			echo base64_decode("iVBORw0KGgoAAAANSUhEUgAAAA0AAAANCAIAAAD9iXMrAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAsSAAALEgHS3X78AAAAH3RFWHRTb2Z0d2FyZQBNYWNyb21lZGlhIEZpcmV3b3JrcyA4tWjSeAAAABZ0RVh0Q3JlYXRpb24gVGltZQAwNC8yNi8wNcVTO0AAAACcSURBVCiRjdExCsQgEAVQs1goRAQbO+9/GMktIoLNqCFqIDGFYVlY0PxuhseHYaZaKxplWZbPELWM3XVdr1zO+ZULIczzPHYAQAgZuJRSzlkI0XO1VmMMYwxj3HPW2m3bpJSoc4dzzlqrlMIYI4Sm/3+UUtZ1jTEqpSilbfm48zyP4/DeA8C+75xzKWVrepzW+jswxgghQohf0XID8c5JjrbW/XwAAAAASUVORK5CYII=");
		break;
		case 'image_bl':
			echo base64_decode("iVBORw0KGgoAAAANSUhEUgAAAA0AAAANCAIAAAD9iXMrAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAsSAAALEgHS3X78AAAAH3RFWHRTb2Z0d2FyZQBNYWNyb21lZGlhIEZpcmV3b3JrcyA4tWjSeAAAABZ0RVh0Q3JlYXRpb24gVGltZQAwNC8yNi8wNcVTO0AAAACWSURBVCiRlc07CsQgGATgf0VQgg+wsgi5/2E8hVYiWKigYIhbBFIsuzE71TB8MGCMGQ+C4Fn+cb33ueOcp5TmjlIaY5w7pVQpZXqNMMacc+fcGOPOAYDWOud8/44AAGO8bZtzLoRwHMdX96q1nq3Waq1ljK3rSgj56QBg33fvfUppWRYppRCCUooQ+nSXjjG21nLO1/gGP3ptFeFq01YAAAAASUVORK5CYII=");
		break;
		case 'image_tick':
			echo base64_decode("iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAAEEfUpiAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAArSSURBVHjaYrz7/+5/BiTAAiJEP4sCyf8MNXucGJiY/jF9YGJiZODl5WMI1CxhAPL/CbAxczH8+vWLYdXJfgaAAGJEN4MhfqHi/0+fPv7///v///v/719m+Pb51//84pL/3759/e+xAGiyf5/I/58/f/z/9OXdf5B2gACCmzFtaQaDuKQYw8/f3xg+Mj5iKHbbcvAbwzd7BqA5Nz5//gQ2F0THJ6eA6dYdIf8D5rP9Z/nH8E/p1eeHDBdO3mHYsmMHw/zZs4BO/Mlw5vEOhn9vhBngVhzau/rJklsVMmzMTAzfbvEwzOs5/xkozAsQQJj+QAPgsOH6y3lg3v4cB05mPoY//34xnHuwl6E1+RhExZP/Tw5denAY7EiYQ6u3uP33mMsA9iaD62QGuOT/f3//L1+16v+Xz1/+Z63TASl4ywSyhI2NDYzdfQMYAv18GRgY/zO8/fCSgZeB9zIL43cWhu8/fjCYWtkx3Lx8geHv378MbMysDCI8MgyfGT7bMwCNeVG/2f//mzevwUH848eP/45Aa/3rVBFBzcjA+CBzsqrCH5a/DBLssgzeSmVPzB28ZECeAAhAN7m8QhSGYfyZOQcjJKVRUqREVm6DEiJlIrktaPAXSDELkZrEgqUplxQhpVgICyIWWGiQSyZi4zaTjWSOmTkzzpnv9RlRI77t+/a8X8/vef7ywSPK2uPtvfnSB/cFfAEJqbF5aCjpPJUipezfPv0IiF7tvnXLVFyc0oSq9HYEmAJGDKI2HC+yA5OHHfD4JFzK+5huetr1Chz0t8DCUj/0ifEwZQ2AexVyQafTwTo6hgS9Ho119eheL4ImzoWy6B5HVmFFEu7Y3XP5KEiRKQQG4543t7ZRn8USNMfjcQdnm/YZaplNpOpFkJOcuyI0YAo/KgiAuasXsuzFYL8FRs5kY20l+IPPJ/AFxhgeXy8himGQHABPovCJgVYP5h4bpmLI++b/in5NLb37/aQoCqmqSrxBxIXpymmjynFQ1VAcnd/aHkIaIUA46bTm5rAoH4aNO8hIKIBGA/gDPkzZzFi+mEBaRD7s9mvMj5zeEyj5P4w8XaINroD25vrMwGuIzEzDkRrDoEI1/N79EICwag1pKgzDzznb3HRz6Vql24phF69l5A3LWf3oR0pGFvSjfhRh0gW6YIXRjwiKgiAIioJKSIwuRoWBP4Kyi/cLOstb6nKbLqezbe4+t6/vHEmwJL/DOT/Od97ne877Pc/zLeqHxYbw7xdiiD9PWM3kXePD/CmfBUJGhF0ZJ3rVq5Mm/fDr/stgZPBb/cfv97eIhGJECaOp+YIIUgMKBAw6bDUoWffYnJCeoVkQ4ENDVfOYqy3naOYdxEbGIRjyg2VYXok1vbehH38Pp3AEqkCeubiwbA6E5R5tTbXdJmdzTnn+a0SJ5PDPeKiMQzwD34wbBSnHsDvlNFiXHMPiN5ofer15DkAGWd2rgcvrL+hewhuc/qdJHo8HBw4fQWqcDlpFGkK2JbjZvUdDd6qJB2hprd2WukKHEAnOK2RZFsMGAw6VlKL6SSX8AR8ObrwGu8eK+OVyuIy2OP67+qFqZKoK55mIC3ST2Ywz58vx+sVzKm8vFRUDWUQM3EE7jRoV6nqqtPw2ugMOSOkEt6JELObjR8AKUHryFG5cvTLHZrblDG1uCGGvCD+9xtm5WLEahqku9Pb3IT17M6wTEyjatx8aVTxysrL54j8AHItAiCaO2wNtzIZZ0A5Dg7H4UTRxOqYJNxKSUsialDRCw49aOsybye/nTOYig5ZOUnCPITtuMcTiHO3mJMAqtKpxWowxRz8cDgc6WxqRnJjIW5e7KSZ/4kmEUpS9zUOCIh0uCwNPtG8Zz4BD0Y+09mylofLLPkmmXU5iNBlpunr5lbkwISFCLtXsJHsrpCT7LEPGXKb2ITJ78T8nXaVILs99NlBcpURl+0UoFUqIWQkiqAoNNj2KHkjRN1mP8S4h7h7/NOCVBjYt6AWJO6L1XMX2LCs7CJrl3AEBeaQMAocSUyYvnl7/2ugUOXMXs3NYHpZ/GTUZlo4ah1NXJqztUqu1TjvsuoXs/FuA0ss1pq06DONP73BKC2VcCwMGjMAGCqgTAQMMSWQbiMtwkWlI5peR6abTxU1nFrbEJfJBDZnxyy7iBzIRmXEKqCG6Tdi4OBBqoKMgUCj3UnoB2kOP7zlQgglhF0/S5JzkNO/t+T/v7zySH9BAe0nCM/RzidykNjcnkoqlIk7Muemoi51wBpNTxf0vP1l/ySBrcVoc7K/NVc+PWHUQebnivRTetEhlpCmxoCve5xZYK2YcRsxYTZCxKrwQe3QgN/HgwqLaxZIWntwsxsaWSkuupe+GpG2gJt1XrYa31BfRmqcQ45+KLYwWPCrxF+8aM45RDJrvwTDbAQeZAb/uF1gLzDI9hkwjKNCeNhbtPmbwLMJNE2BczM3rzZVq3VR9cogqGvlxR/FEaBadHidtWufmrSTFSSUKdJoa0aD/Ek52EbYlMzhfM/QT91EY+IGxKO8tg4BeGyQwuWiyTJR/ty9JG6ZFUcJJZEYVw+G0Ptwcpfx6BYaNKzYZGxGHJsPX+KX/EiyOaah8lURSrZCPx6DileZ7Nh9bynpDZsWz3ER5XUGSX6AP3kj9DM9F7H+o4EqlEqbxcbx3+kMUvXoIXX91IyoykkYxj6xtJTiQeAreCnpn0ohQUTIcQQacrc1P0bj876wlQPt46vrtyqQpbhSZESWID0oXZrvhKVg1NpVKBcPAIA6VHkbZsXewMyEB9XW12LcnHwQD4N2H3yTJoXlIDc2HnTXDbJ5DoCIcw9I/8VvrtTSK275yCsimRiw6yAhNIv13rHrXxsF5TJmcnKKgb8M4aiTHDUP1V1cQELAFZFtrjuvBGHrCNv8k6uYCFBI71K5AATZ6Jm4hAy/bbLAJI+DEIglYllRNL0oIpHv79Dh15iP81NBIR50DwzDUboLrq1UoLnlNqH7/S0WounwJGo1GwNaV7kj+Y/38vWVxmnYrkQ8fwyHFkoWq5ryo7pVCpZyIcyQEZ/q3TTfi5lA10iMPEMnE49OKT1BTW4ddxMbR0VGI274dd+62Cn+8cP4ccnNyKGlWCOJxfB7XIMhRSEFg+lv/fCM8+Sj8MD0/JiSQlV1spNOgFTpLRhFemHWkJZ55ll5uQHXnOcg5BnaHHQV796Cvpwt5ubn44cd6zNG6qbjw8VpwvtV8Any1nqBrAiXxXe14H3eHmxCiDsbyogSDw9PIDS9FbEJKv8cx13xAuaT84/yVgxk69nekaDNwdvcNyKVeBEZLUJMZXav5FqNjYzhx/LjwMcQ74fqL7wJP5DKxgmY+hzP1L6J7ohVb/cIhmvdFp16H0px3jSWFJ/vtsGdvaESkzM77XR3M502H46xyC5JD0lCWVom4wKeJWqhXIFJjXatViwVB0aRJwHLQiNFnasfF5jL0jLfDT+mNMEUiOjv6wC2L8cWJxj6fiAA1hQl9oBXzqDxlGGIu/vxm/Ji7Gzww+cgViPBNQIRmJ/yYIAosgtkxjiGzDkNzvbBTV1TeMmhkWrhnVej5W4cdW59B+ZHLekkgo/Dg9CNvQx52l+ecytut36d2jjZhwjYIq3tSOOtukrfYLQdcUkhcKgQw4dib9bp+V1K2y8m4LdSv9AeZ2ePiuZW61MaLmCoj8nfH0L3f4+D9v8C6fDLZAxDGAAAAAElFTkSuQmCC");
		break;
		case 'image_warning':
			echo base64_decode("iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAAEEfUpiAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAisSURBVHjaYvz//+5/BiTAAiL+/JFj+P//P8P3AjYGJpDAP6D4v3//GBhTNzEwgLS8XFf//8/Pj0BFdz8DBBAjuhlMH/ZtYdix8w7DXwYliICAU+CR33//MDDsiYYoAWn59fvP/y85IIvu/gcIILAZ3+7dO8rOxmLNJKTK8P/rcwaGpwcZXh/dzSCePQ3iDBZBRZa/YlYMK7ceZ/jDr8vwTzGEgfnpSYgdIIJNkNGc6c48hnPnzzMwMQGFTpYxiLSduwm3E4YvTwr8/6Ja7P+vX3eOwMQAAgjDH+gAbMX77QsYGL4wMvz7DDTx/jmGLzumg1x2Em7F379/gPjv/9evX////fv3/1/H8oHGv94NkgObwPgZGDJ//zJ8/fYVGKh/GRi/fgKKfnKBW/F9fzNQwR8GYSEhBkZGFoY/N8+huuGfSsIvZmYWhinTZzIw/f3I8O3mRYQrYd75c74LbP/vLa7/v//+eBAmzgRT+FoqcP/3xZYM+z4FMHCwvNGBiQMEoJv8XRKKojj+ve9VWigStjQ0VEtjBrkZEUK01xa0Bc32F7i3tTSVS9AQbSElkUsE4R9Q5PIcAqVMebz39P44nRsEmnjgDPdy7vnxPZ/7TwfhAYk3HXYRvtfzUgVwxSRmFpcfnERaOG7EyMjckFB/pdqVE9L+BylN7JrCMGTFe+x9ksqQ4ij1XCD/2FIQvIy06MwyQLEkjJYQfL67r6DZbNkSIMM8GYJRIfqhjW66QypZS2byT/r1itMz1VKh6/uws1mYidjrl9CfHejVXb7VmZERrLcu9qgXfHP7AXmNxu92oyiivm3/Okt+AaRY6cE3zqAecwenFXmzDTc+jdtyGeelEuz+nOohZCcGvVWEO+ENaThCo0HssX22s5la34eYXQFqRRjvC9X0EaaWstjILfCXwfzYBAPWNYjXBGRKQK+Nw/1HAMqqpqeJIAw/M/vRrw1plQMpfhC5aDBAoxgTaNTAsSeNXDT+Ag/+DRNN9OLJoxiOcgKRBi/qwUSDCaQS01artS0YC1vSLruz4zsrB8UllUn2MDvzvvPM+z7PM1310G3of0+jy77ri87nr1Ne6wfNXejRI4icOLmsxeMCsC/STysEgfGqMXt3vOfMBdpMHYqkqD0+xbfJJSpg2yXsNhuwP60jOnytao2cTe9DYLZ7s9ch+7LwmYoVcIkLeoKWE4Pg6SkYm++RlC6aH+bSlAD7iCSZIMjKEhWTbCLRwuILaqEWMJHoCWkX4Tdr0HsH//UMYNdyN9dpKwuYp2JIC3+Qjei8VYD8XgA/di4sgZdxaiWqCA82a7qGbXtrL5hoTC7BKkvwawVEj4+EJYApXIIr2gHkRCyO8pdK4D/qVqAlJjrwGnUYyaNrYQmIZZegkXmpAIXAc8Xv6wQAHchEPzxqJEN1JzSBlcmtydIzOp/TG+PhcnYiSMY5I/jPIRwT/sB5pcSx0ATc2PB2VubBdYMK2MG9Bw+pC7TMSUyrjyAKbxDJ3Q937j0ZDXv9V8GLM4jFYojTx7mm6kpFlBDlMqzTQ/kuWkjmnZkrk/zGCjS6v0/RbHEabn0DVZ7BqZu3laP3HIBAjebkx9QdGHPjQP01kL9F8mZovX2J4sA0FVV++x811paezveNtp7AcB1U6vQ45R5jYiy1ypgcOoSctXe+NH9y1lY2bh4k518C1F4usU1cURj+r8fjmbHxK3ESQ50UpzhEPISAKLCCUqkSS4SQYNGqqroAVVU3IPZsYddl2SGkqiwQLWXRSikIBOJVtaTQIpFAHiZOkybx2/G8LufemfAShATElceWpZl7zz33nO//Zzk8oCpjk3RRHphJl18kAosu5YWn6E/rcnnyigDUq669Ak51oln7+/onbn2WaovMisqILWEwjS6yAQFan7sWHEoTmjXY9YaUFG5TP6oxqK0fwuje8ofallvJkB+nat72xgA4T16cPn98V2rTTgRiHeDBGE2WIOhE6M6Q7NWnRec/xYSIcZtajQKZnyIrMAI+9y+YTXQJJNA49S1qq3ej45sLeWAksyhRGTOdoNEmhAE8sdlfxcu06KhSqYg7g4OwTAt9fVsRi8Wk7/Bu0MAi5PJiWQ8q42fh1isgBEJNi9qpDtNXZpFGkk0fVCJJOLNDlBqL1nd9YeUeK2nU6nUUy2U0m82XLIJ/j/iUhiQS3JkxOFQtSloAuMJf68GeDbMzmFqL+cIDOtfi05RzGYgjSdegs65UKn4A3JOOhSDEdGaF2PMbrfcfZeEv2HoAaqcwAs3eJQTAP9K7em7zCgna3F2pDgs7oyaGpumwbJKVsghORiZPaSFLIHZx8VxtFNwJgU8Nw6XdaxkpQeklBEBz6KzoRLOwh3+nZqNAmAJPqTgxTpekHR3NIxIOw/FxLQpRFqMYYvcqmaypArkRG+r6vQTV0sCiNvjFUeqPbNn3qP6QeDh1hUAa8LPgLZbNZqEbmizAhbrwZgpJ9WYTA+BaGu7ILZhRA0b/FxKRS/QVcsT0dNvNeqo/a949DcXogtPaR53WIKW3sHH9OmmmRSaCJB2iSxhtnwnVu3+SfjWYhWlYhTkonx6F3p68BMx8vGwSWo3Uo9IPn2WT3cSCrSegRFciPzaGrw59TXpdQSIex4+nTyFOmbAFmEZ+IR93DI6eg3P5DGrxHiSP/Akl9Hi5JHyG3maB3i1+/tKIr8nA3f4dmNGOIGmbCEAcgcgCpQGscBm4dhhOpAfujQso1hiiRwYRabfeBsUvjrlJbbZ27vOWTEcVzgYyyLn93oMyTVXg9jFg4hLcML3rXf0Jk24c5QPnEQonNru2Od3bq/3j19qKtxWj6v+zyq07Z77ftSnwK1KtVIxBnbgvdm8QrpNw7w9i5uEorncfhJXbg1Q0hJ07uu7RybQ8b6XfVQ0n583gvfx4s6vxeCinlMcRIDg5Latln3eu4gMh1cmQZn/wut2+F3v+ruMJoXibOBmWOuYAAAAASUVORK5CYII=");
		break;
		case 'image_error':
			echo base64_decode("iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAAEEfUpiAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAArDSURBVHjaYvx/9+5/BiTAAiL+yMkx/Pv3l+FqYhIDE5D/7+fPHwwrVq1hYOTkZGAAaXl96eL/Hz9+/G/lYP8PEECM6GYwbPX0/P/9+7f/P/8DAUjy05vX/5PTM4GC3/9Pl5H5z/D+1q3TX79++X88IACsAiCAwIaC8K7oqP9vdu78/2rTpv9HXFz+/3/48DRIHOyuV48fMRxR12B4+uIFw6MnTxgqt25luJyVZfKcmxvirP0KCv9llFX/v3j58r+ukcn/PXv3/V8PlHp++fJbuDN/MDEd2hPgb8fIyMzwTE+HIbW+AexqgADC9AcaYAKTrKzHXh4+wvD20SOGB+vWMczz90OoAJrw++W+vf+B4QUOgE+fPv6/09v7f56WFsSb28PDgOH2/X9ze9v/I0eP/K+srf3/7c/v/3vc3CAKlktIgHVdunL5f0pm9v/fv3/9//rt6/+ToWEQBbe2bXv5BWi0DtB7//79+69vav7/+7+//+cDgxUekhuATvkCtAYURSCwh5///7dXry6A5ODe3Dp7JgPD4mUML+RkGaJWrDjA+eePA0gcIADdZO+SUBiF8eP1liYJDYGgS0ODNyiIppbETegfEFyrpS1IHGoICloUQ/0LbJAcwk0wB6EwFdNrEWH5gZcG86NUUu/1ek+vwpWiepezvAee5/ec508OvFIZS/j9plbuHgbdLmiXGNiw76dV7c7aL1BymMhx1zc2G9aiVygQjbwgEDQ88qKIvXYbX/1+zFit6CErctATDx/P+du8w4EDov/M68VA8GLCNRKN4qnThfVGHTtvVWS3d/BcpcJ+s/kgexwGzGZqM50GhVoFGo0G9uwOUNI0EGzQ53nwuJwwGPAgEbOl4xNo5fPwyLKwFQ6Po6bUBj00E3FSBQmybA4qHAeFQhE4cnq1Wh0uQyGgKBqo6SmouN0gDoegNRq/MSiX74JEVpXNYiqVwGKpNK6AQDiMolk3mcfxxBgGkxYLHo04FIulHzGNXqPfy8SXV1bVOh0sHB7AjN4A75EIvPh8MM8wECOF2E0mn+Yoyijv/H/OCkVBoulPMiVKFGeJv8W/vn0JwFjVhTQVhuFn52zHSU5t6sqcVjP7WdFSagtKtm68CEKK0ExKpcJQJJAIusnuIpIKLyqjmy6EIUY/BmJhaOFFimZpGU5niU1sR7e5X89+Tu9OaFSSHTjn4jvf+3zP937P83yr+mG1R/7XCHnH43QKfc33Ld5v02DoVHafOzus26H3QBAO/pPBnNPZM/PkqVljMoJJSYGMur3I85h/8RIfnrfD1NY2l6vVpq3IYHr00xuZw2HWXboIBRWLkWi8D0gmPShztOC0G2Cvr0/z1Na6C8zm1N8AfKLYu9jXX5jd0ECrRhAVhF9Sj4SRbDQBCUqE+TkMVlen6mcc75WeBcNSZoR6KioOqE+VY2rSjsbbTYgS9bioFAoFmu40w9r2CGsMBnCZmdCXlsJq2m9YDh0xJeWthiYm5m6Bx7OAeBZfa2yMtwdXSbaCEMI6TYbESk1hxJI7AxMTIM13SwA+t1snU6kQDYWwfdtWHC4qgs8XwOUrDQgEQ9Bt2gxL4c/mJ+blwW+3g2PlWFQq0yUAThTX+odHSJuiRNto3AeBDBQIBMGTD8rLTtA/msgwkHEc3ENDEKmxsUhEIQGwavWo98skRDKKKkmFstNVmHe5yIlRYhDEkWPHwXEJEgO/bQxypRJe2mYiy36XAOQeT3KQkEUqOllZhTGbTVrtwb272FuQD550kKvfhQQCsdVdQKrFghjLAi7XnmU3Dlmts51qtRijfZypPk/fmJRG8RSNX042m0308bx0YTzU7xTHX3W5lhJpSYnC45ISLo0SwzQ4gEjAD0bGEBMZFHT+focDXVlZ0FRWYKC/H3XP2v+4OwDuaGvrrCtzPTpJfWM1NeA7OuBoacFrsnE3FauKi/Fu5GO8OPQvN8Z6x8eZ+Vs3wYx+lmItmJ6BSURw6PqNr/nZORv/185ekWWn6A3LRJGRhcM6Gktayc4/BCi92mOausPoae+lLbQ2VchU4iwVA2xjJGYYNUXxsU2GjUFJxCDRZTOb+29/bIlLlizTP/aKGcuyZMa5GE3M5qbB4mMYjYJBRXE6qbOIrCgWbKFvofR17869qIHoHsab3H9ub+/3/b7vfOec7+n4QKt1Ed9hdiHNgmskyIKGoBIFIaNR5ieRyCWYSp+NTyYHvJ4AhiOcsa49eystJmOpmXRpIPIVMJJ4Eff5EGWTIp4+hCIhxGlnSt/e7Cmtq4vqYzELUWz9txBProBO137PfSPdu/vHytkVFdBRNXRspJifjyyzGZqsrPH30mmk799HilydvutFMhhEjGo6wsHwUXp63d1YuPP7cPmKFbf4bvl/JyAIVwc8npGexkZ7wbKlMJEYp8xf8ICW0v98CiJPaQW7jVFXFyLnzyPOiiS8Xtw+dw53+vqwqrU1aMvPH2VFZj05gezs1pM7dlROGxxE3sqVeG59nTp93W43esg+yyqXwEDhTaXSEzqkhaAVcOr0aQwFAnBUvwETrVuGkxqglkYuX0FqwItAtxs32s9hXmNjeFFt7XVSnH1yAqLY0eF0Lhjbvx+2TRsxffNmpNjfPfv2qbyi1+uRSKZgs1qxZvVqNREleNvZs2jjh3XkIYWwOP2oralRWTFDjPh+PoDoyVOQSKl9Fy/B292N5cePh0uKi/38Q9GjBMamTOk8UlVVPoeEPefbbygfsyAzYDaJ6/KVqzjY1PSgQyIrkMTMGTMQDIZUh6AkQguIxfYKrHZUq79nMhJbIqj48H66DbGuLiSJHZfTCcuSxVjXfOSCZnh44cMpuCQFg/M17FOK6BbzFPJPkRIkjJD8X3yhBOWvbEMnvc++/T8hw+e9ntvjNo4nXkT63tiwQZE/Bk+plVAwofgikUEl3qGrf8C0dBlEKnS45xZGE4k844QxnC8a9MNjFktelDwXHxxAjrWAZRvXRSN7+js14Ludu5BgAJGlTRMHAk+oBDra0kK51GPD+vWqnkiKA4OkCrJE2h863IRkOAStXsQY22ooLIRBp7s/iUt1eoNrhmMVYjzxvV0/QNLrVCK+2HkZjppafPTxJ4hSLoeHhmA2mfD1l5/j1aWVGOD8S2zDAWrua9UONB/7TcWLIm4akrj/l4MIkX/NRUUI+IcQ4fO59fUQYrGRxwxuwu+/8KvdLjv5yP3VF7Litl0ul7x8ZZU802qTHWtq6YH7VZP3UGqSyYT8/gcfyrPnFsuFJS/Jr1c75Js9N1VZunvsqOwUBLnFZJLP1tfLn/G7e7kFSaFQ+yNTPXFdUm4SyhlnXZ1q+lvLXpaD/XfUzUvZMxJ0mcqeNTo6qjpOxUA/TCQQCPDvvWpyLLN8ae1a+ZDyDatVblm3TuZWJDdteTck+/1nJsZ7zJ0+uHyD0cjA+U1vzsu6dg16iwWzt27F9IYG5JANNY/tURTTSBi+g4fg2b4dEYLZnJsLY1UV2ogPrcmIt06ccE8TxAK+angaMYqEtJqujt27K1KHmyHc+FMxIuNtU02TCrVxO8o7h+ASbTb8FQ6jj1Q8770t/fZ3tvimJpNlCsyeTgsmXwqu+7g59YfjcfGmp8ee6r2NKHk/k4hD4uaQnWOEsbAARWVlnVNN5pGcZPJ5Jjjn/6jhM9vzZ73+BiGcSbPVOPrGAAAAAElFTkSuQmCC");
		break;
		case 'image_back':
			echo base64_decode("iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAAEEfUpiAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAApNSURBVHjaYvwPBAxIgAVEfP78mYGJiZHBvmc3A8PvP3/uCwoK///69cv/5ceu/2cAaXnx9tP/Hz9+/DerXPAfIIAY0c1gUipexPD5y2eG379/gvhXGEB6lVXU/3/+8uU/Q8KC/wwiiX1gA999/gLS/R8ggOBmuDUtZRATl2T49us3w6NvjAxnyt0OAoXtQbbuAboLrOvly5f/RUREwOyQKTv+s6XN/88EVGX16vN3sAt4eLgZODk5GZiYmRl2XHrMIPzjBQPcir41e5907uORefPtIgPP+1sMHzf2fAYK8wIEEIY/0AE4bP7+/XsgbMZ+Bz4OZoZff/4x7L3ygOHF5GSICqABhw5fe/D/E9Chnz5/AjvQrXvLf4b4uf8hpkdPBguCsIaGBpytU7sOJP+WBWRLfHw8w/kLVxg+ffoANvX7r/8ML9+B2ZdZWP5/Z1i4cCFY4v3798B4YmIQ4GJkkBHiAQnZM/1eWvgyasZ+sAIhIWEGRkYmBpa4OQxf7l8Bi8G8+YAldrICM6Mgg7jgL4ZqZ6Un6X4OMiAFAAGELRy+fvn2/Uz/hkP2l59/Yfj08y+DiSw/Q3WI3XlgKBuihxPcgE9fvh32mLDLNtxUgSHbQZ3h979/DP/+/WdgZWZiePbpO0POslMMn779YDh89zvDn1nhB5mZme3hBsT1r2IQEZdiaPY3AAkB/QEMXyDr5+//DBysjCg2WndsY/jIKshQa8bzJNHdQoYJGBO3F596zdAeaATUyMiwbdtWBiFhEQZbOycGN1dXhsePH6MYkO+ixfDz7VOGpDWPQGFwiAmo6zfQvQz/oRZ5e3szzJo5k+HSxXMM9+7eZli/fgPEr0DDQV669vwDAysLMPrePQEJM4NzybLdxx/zps/5/+jlJ3hMw/CPH9////z58/+379//n7z99D9D7LT/gtHt/8/dvP8IHH7/EeCscNKE/zzJc//nzH/8/+mbL////vn1/zsw+2UuPPifIWLKf7Oy2f/5PfNBah/ANOFKziffvP/IdPjiTVNQcDoaa50W4OUFiZuiKwQIQEjZvTQZxXH8O92LU7fZ8mXlMFO0KE2iRFxpddWLBnoRQlcRkXWnEfUPZFgJFSUUWt4pRC+KehFkRTOnppGbzeY2bc5pai73bG5r83lO53lsw1DyB+fmnPN8n3N+5/v5bsrDZiXeYE5vdc2TOx2G0tmlIG2YCFfLD4zp9mT/pGsl/z2B3mT7eKvHekhGu6yQi2kucQKAIsp459gi2qpypyt0BdoNBe519g28++4raqw6CI1Kjt8rrJBwvBMf9IzhrWUOjoAYx1JC0/cvlEVF+FRBy+t+0xs7U9R+qRSqeCkCYRYc1WVZQoU4VB/ZhRpqICXnw/MZmbbLYJxeK/D+Wvd4/ouLJTTUVhBL/8qPGGocPwU7HAoJG4uzkpGnVUMVWMTpJhN/gn5B4GXvyNGSnDSwf90WqebmJ8jN3sEHUnSuvnI/5hk/lOmp+Gqf0giv0G10oCx/Z3RTRUUlvlmsSNNsg8/LCEEZqTiJGEuBMHLi6LX15syG7AzEeAMhGjFS9PUZoFSqMG6dgFQqwy+3W8iqSAUpmfzVWNofCRvA1LxntQeaRBlGnG7odMU01DwwGT8jLVWNH7MutLa2RQVkEhpV9EUQYuH3LWOfNml1wWR3uhTVT9dBxI+6uhuko6NdAMrn95Mvk7NEdO4REZ29S9wexsRbQJyXpZ3xMp7tjkUvMtQKrOkjamtrERsrFsIlQSbC4ZtdKMhQwzg0hC1KRUok2YnJ5jDjzEPyamBhHcpBijIhLDnZ0EkSLrcQ0fErxM34hiM0RnFu1w9bUNVITtweJU09cyQcChKOC5NB2wyJP/+YKOnHqlM1ZNBss6yJgH9xXvItfyq83lI4ycihSSqCy/0BiQo5kjkPAgtOOJ/VGyQSSfFmOFMXc72jdudW88TU3oLcrJHdmenMRiTy9UeA0qsupq0yDD/t6Q/0n2Z1aaFDB6sbUvnbdGFMEEbUxRijzghbohck2/RCWDLivNiWGENivJhe4RYz4wWocWo0xoSYZgiM8TfQDQKVFtCyltLSFlZKe05b33NaEBzOMZu8pyfNab/3+97nr1vVg3EqPxWpfkLEF0PgIDdKpGmxncqyFT35rwZ65wMhjph+cNRN2YPJQGaGHFKZTOAaTx3iKpaiHFzBZbgXlqAWc2iqynfWP1UY0Ws1HP1G0ZYbCJHJ/TAwwVzqcZZrtBpoMyQoM2ehlCBk0imgkDLCczz5Z2nhYVcAQzMLWFphQfqHEDVkD0vx54wbZ2tMrrOvVTtWjfCeDbAs+8sH313TXLk1X7zToMZblRZU7jYiRvSLxRN3f1kk2Ch/ASNKad/P4x60dtqJuhwC4SgCEi1+n5zDuRqD6/zRWocQvTZpwDv+h3uupuUbq8mUjdO1e/DK3ocRph0JvEqtkeJN+sJ/SyxOdSH6R2NSiRhf9E3h055J+Ei9lKQx/b4k8uIejLz36rBKpSpZr6dxjz/gO9TyrVWVZcCFI6V4sdiMSJRN6WpaW0Xp4u/5zxhGhM7uXrxBgtXe3r6hAd5KjtAGzhy2QkmYcXm8KNaL4Eg8hMr3r5QkOPb6+gZ+u9gxWDAbSKK+bAeeeMRAwEpu0Gb+5XA40dzcDKPRhLxdj6KCosebJ08gJ8eMurq6u8bDO0qVZTueKzQisMwhSOEuRy/HDa8Elzr699Mjg4Kek+BrJ7xEUwJWATkGR7PmkyCfCltbP0EwtCgEP5VaI/ywOXen8O73zcNaWAASzE0Wp4wXTWA5moTVrEeEJD8sY2DQssLIusgfjx/GHeEEaKdJCb9bAk0kGhOGnZWVhcbGRnR3d+FU09tYiYThnLRjzn2bfMIvFAEWNpuNLGBxbeFVONOEoJKLkbtNRhhYAc9VfmSS6DIF4BAymL+JR5FXPL83l4Q5zqF9YAbrWUn/C4Rjdzod8Pm8mJlx4srXX6L2UDUdqR8cOX9FRYVgmwI+CJUiqmQalvwovxqYFu51CjluLywKDdRXFPCmblpjAcuxQwffvVzWN83izMtFOF71OAxq5v6UjBaemppCXl7+Gkv4hpSZMjS1XceF70fInrXYJotj9OY0Xq/Jx2enj12lx6o26AAlyJ5nzl8+0OnkcKDQhI/qqrDHJL/nwqkSrwGWj69yomBwJYZnP/wJ/eNzyMnWQRtfxOiIHe8ce9rV0vDC5OrimynhyI+9vyqOttosoYgMTz5mxEsl+1Gam4HyXXJhxqK0MKzRkgYuk9BpiRkMOtw4+fk1DE54kKlTotAgx8TwEMSJOK5+fGqiKH8Hj2Tj/XjBjf4xh6LhYsfum35SQAKvVmuGLtMC2iAqC0LQKTl4QssYnQ1gfDaIaDgGKVmnSSOFOraA0Vtj2Gcxo+3cCXtetoE/ytwHdcMu70JQ2WbrL7WNzWLKdwe+FRVt2EgJyQMm7oUkyUJNXeboFWh4vtJevc/K6tUKPjaV/183/LcXz+E+Khk/+nTuVzxIvP8LRf9f4f27GyAAAAAASUVORK5CYII=");
		break;
		case 'image_forward':
			echo base64_decode("iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAAEEfUpiAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAApESURBVHjaYvwPBAxIgAVEfP78mYGJiZHBvmc3A8PPnz/Pfv365b+8gvL/5ceu/2cAaXnx9tP/Hz9+/DerXPAfIIAY0c1gUipexPD5y2eG379/gvhXGED6lVXU/+/avfs/Q8KC/wwiiX3/QYLvPn8B6f4PEEBwM9yaljKIiUsyfPv1m+HRN0aGM+VuB4HC9iBb9wDd9f/Nm9f/J0yYANYNwiFTdvxnS5v/H2TCv3vPXzNePnWUwdXVFeISZmYG0bwlDLw/3oB9xqgkKcqw4Tfvk7SSKzJvvl1k4Hl/i+Hzxp7PQDlegADC8Ac6AIfN379/D4TN2O/Ax8HM8OvPP4a9Vx4wvJicDFEBNODQ4WsP/n8COvTT509gB7p1b/nPED/3P8T06MlgQX4BQbgPQFindh1I/i0T1BaG4KBABj19YwY+Pn6G77/+M7x89wEkfJmJ5f93sAJlZSWGSxfPMnwBhrMAFwuDjBAPAyygXvj3bgYb++3bN3A0MYRP/q8aU4cS1A9YYicrMDMKMogL/mKodlZ6ku7nIAMyAiCAsIXD1y/fvp/p33DI/vLzLwyffv5lMJHlZ6gOsTvPyclpiB5OcAM+ffl22GPCLttwUwWGbAd1ht///jH8+/efgZWZieHZp+8MOctOMXz69oPh8N3vDH9mhR9kZma2hxsQ17+KQURciqHZ34DhFzA1sLOzMYBM/fn7PwMHKyOKjdYd2xg+sgoy1JrxPEl0t5Bh+P/v31OGiCn/f3z7Cg9joOn/Tcws/8vKKfwHJhCU8J9/8Mp/qXxgwkxZCbL7IBMDI+NuoHsZ/iNZxMrKxnD96mUGWRlJhrVr10D8ysgI9tK15x8YWFmAcffuCUiYGZxLlu0+/pg3fc7/Ry8/odgGwj9+fP8PzFr/v33//v/k7af/GWKn/ReMbv9/7ub9R+Dw+48AZ4WTJvznSZ77P2f+4/9P33z5//fPr//fgfGaufDgf5A3zcpm/+f3zAepfQDThCs5n3zz/iPT4Ys3TYHJlcHRWOu0AC8vSNwUXSFAAMLK/qWpKA7jz+aWTtvmdJbpbGJlZZqEimhpBUG+JBQZQr8YEa36SSLqPwh7gUoTClF/DKLI0BFBVrSVLyFks01zy73p8G1s17ktt7vTuVu7KEkeuBfuPfc+53u+9/k8d1MeNhuiDe7ppmbmyb3Xg9UuT5A2TIAbp0pMlQW7Fulc1X8r0BnMn+8MTB1OpF2WSkQIUfg4AAWU8T7TEp415TtPVxarNhR42Pdl+IPVV97RVIpMuQS/w2w04Tgntg+Y8H5yDraACMczVp2PLtXzIkLu1PN2yPDOwpT3XqmGPHkLAiEWEarLsoQKRaA5uhctJwogi/jwYjZR1T/43blW4ONN7c+il5eraKiFkUBX5Q4hNY6fgh33V0WeEoWqNMgDS2joNHAVDEUF3owYj1Xt2Q72r9vU6lyUlJTSTPUjJVG4rmGtZw5hnvFDlr0NPyz2zKjAqH0R9UVZ/EN6vQ4p0lTU1DUgXamEVqvl55LEIngCIWQl0W3rjLlRgXEqkEr3HR85OTmw26bBeL1QqdTQaK6hubkZQUomtzWW9kfMBmCf98Z8ICZhjDncqC2MNbatrR3hcBhWqwXdXV1obDwbi0/aDRH9Ilhl4fet4ODutNiKRuusS6rp5uGhFRCn08Ff+ynmHFA+mnffpl1EcOEJEZx/QNxexsBZQLhfvcO5zHhhW1oG54iJCRMUCgWPcEICNatASBsqwpHb/SjemQaBxwWFTJoRT3ZiMNuMOPeYvBpe+AflIEWZEJbU3u8jKVfp7+jkdeJmfKNxGnmce3Wjk2jqIDV3x0nnwBwJrQZpWIXIiHmWJF98SmT0ZXldCxkxmifXRMB6nD2+la9lt3rKphkJMlPLMeP+hK1SCZQRLwILDjietw6KxeKKzXCmLo7oxy2OdOMv+4Hi/LyxfbnZzEYkcuOPAKVXX0xbdRT+2ttS6H/AjrXQocAqMMqAOUWGo4KLxizGGEcEjE5ChMUHw5It+rItcYTE+KA+LS7GxAe2aTZNjNEQ08gWKAM2EFdSkPLPFkrpHwqD0va2eO4tIDKYMm9y+tDe2/P9zjnfd767Wz2wUXgpSPVjAi4YGg6BQBhbo0UahWE3evJvACxz/gBLTH/OOkPeg0lEUqIE4oQEnmtcl4mrWAyxcMwvY8a3CIWQRbMpZ6z2aEEwRaVk6T8O7hpAgJbcD73DzOXOsTKlSglVogiH9MkooRHSqaWQihn+Po78Tkrc7/DjzqQPiysRkP4hQIBGlsT4c3IG56p0jnNvVNrXF+FDAUQikZsff9+lvH5vrihLo8B7FQZU5GoRJvqFo7EHH6ZkAvAfYARx7fvF5sKljhGiLgv/Ugh+kQp/jM7ifJXGcaHumJ13FNsAcNumZmarWm8Ydbp0nDmWh9efehxLdCJezuI51igd/+CeEgrjKARbgIlFQly9PY4vO0fhIfWSkefp8awiO+rCwEfV/XK5vHiznrLTc77ZF1q/M8qTNfj0RAleLdIjGIrw4nPt2lW8c/IkLJZuvudc7zmlZxgBiTUeSM5XkgbjBB3gw5eNkNHMOFxuFKUIYI/tQUXL9eIYG+neDMD6rcVqdPpXUXtoH55+QkODtcon466amhpSRT0a3m1E+dFKZO9/ElqtDmfOnoVzanzH4eI2ismQRiKphX+ZxbzfT6ZOgrtuES6395TSLX08AFLOpEnvEtWNQT5tDHabXre0XERBfh68njmkpmqgz8xCx80u1NS9hfwDhdirTUdubh7IScNPiQg/DWQM034WRn0KgsEwliiUTIRv2S3aj3Td5+WcyhkK0jRT3ajs4XhNt/BiYWEBZrMZ6uRU+H1e/rtwKMS9CZCnVeB08/uor68ncI9hrXCQ0zbTKBi0D62A4yrXMlFoGQgGkMhI/vYVQiGjzN2jJDPO4krvJD98mztLHh3l5eW0Y6JURi+qq6vR1NSIwsLCLYwQbEwrZ+K4U3Ct/KZ3gv9dLZVg2rfAA6gtN3FLXbdubDJPHS+1XLHYnv359gQuZqjQaCrk0XPXxMQkBgcHdxYSfiiFG6DjxYtCRgmb27ph7p9C2l4VGHYFHvsU3q7KQWVJ3ijdZPqHDpCD7HzxwldHOsZYHCnQ4bMaE/J0kocmjodwY2C5k0uIgvMrYbz0yU/osc0iI10NVXQB1oERfPDm847Whlc2km+nhAM/Wn6T1l0yGwLBBDxzQIvXiktRkpmIsv0SnvuCNWFYB8BRMkHEcK9o6LPP4NTXXegbdiFJLUOBRoLh/jsQxqL49fPTwwdz9lGvof0vu+Buz5Bd2vBFe+7vXmIFDa9KpYc6yUBDQ1KWH4BaxsIVWIbV6YfNOY/QUhhiWp06pRiKsA/We0M4bNCj7XzTSHa6hitl5qNuw1tu37yszdxTYh5yYtxzH54VOR1YSw7JBSbqhmg1AgWhzEiRouF4xUjlYWMkRSHlbFPZ/92GO11uiimKJP41BciiSHgUe/8XWbZu9IAUQX4AAAAASUVORK5CYII=");
		break;
	}
	die;
}

function LH_MainPage() {
	LH_ShowBegin();
	$result0 = LH_TestHelperVersion();
	if (!$result0['result']) {
		LH_ShowBlockResults($result0);
	}
	echo 'JoomlaLMS requirements:<br /><br />';
	$status = true;
	$result1 = LH_TestJoomla();
	$status = $status && $result1['result'];
	$result1a = LH_TestPHPVersion();	
	$result1b = LH_TestIoncube();
	$status = $status && $result1b['result'];
	$result_to_int = ($result1['result']? 1 : 0) + ($result1a['result']? 1 : 0) + ($result1b['result']? 1 : 0);
	$result_to_int2 = ($result1['class'] == 'ok'? 1 : 0) + ($result1a['class'] == 'ok'? 1 : 0) + ($result1b['class'] == 'ok'? 1 : 0);
	if ($result_to_int2 < $result_to_int) {
		$result_to_int = $result_to_int2;
	}
	if ($result_to_int == 3) {
		LH_ShowBlockResults($result1, true, '232px', '3px', true);
		LH_ShowBlockResults($result1a, true, '232px', '3px', true);
		LH_ShowBlockResults($result1b, true, '234px', '0px', true);
		echo '<div style="clear:both"></div>';
	} elseif ($result_to_int == 2) {
		$first_block_shown = false;
		$first_block_skipped = false;
		if ($result1['result'] && $result1['class'] == 'ok') {
			$first_block_skipped = true;
		} else {
			LH_ShowBlockResults($result1);
		}
		if ($result1a['result'] && $result1a['class'] == 'ok') {
			if ($first_block_skipped) {
				LH_ShowBlockResults($result1, true, $first_block_shown ? '355px' : '354px', $first_block_shown ? '0px' : '3px', true);
				$first_block_shown = true;
				$first_block_skipped = false;
			}
			LH_ShowBlockResults($result1a, true, $first_block_shown ? '355px' : '354px', $first_block_shown ? '0px' : '3px', true);
			$first_block_shown = true;
		} else {
			LH_ShowBlockResults($result1a);
		}
		if ($result1b['result'] && $result1b['class'] == 'ok') {
			if ($first_block_skipped) {
				LH_ShowBlockResults($result1, true, $first_block_shown ? '355px' : '354px', $first_block_shown ? '0px' : '3px', true);
				$first_block_shown = true;
				$first_block_skipped = false;
			}
			LH_ShowBlockResults($result1b, true, $first_block_shown ? '355px' : '354px', $first_block_shown ? '0px' : '3px', true);
			$first_block_shown = true;
		} else {
			LH_ShowBlockResults($result1b);
		}
		echo '<div style="clear:both"></div>';
	} else {
		LH_ShowBlockResults($result1);
		LH_ShowBlockResults($result1a);
		LH_ShowBlockResults($result1b);
	}
	echo '<br />Recommended PHP settings for proper work of all JoomlaLMS functions:<br /><br />';
	$result1 = LH_TestGD();
	$status = $status && (true || $result1['result']);
	LH_ShowBlockResults($result1);
	$result1 = LH_TestMEMLIMIT();
	$status = $status && (true || $result1['result']);
	LH_ShowBlockResults($result1);
	$result1 = LH_TestFileUploads();
	$status = $status && (true || $result1['result']);
	LH_ShowBlockResults($result1);
	$result1 = LH_TestCURL($result1);
	$status = $status && (true || $result1['result']);
	LH_ShowBlockResults($result1);
	
	if ($status) {
		echo "<div class='jlms_simple'>";
		LH_ShowNextBtn('joomlalms_helper.php?step=preinstall');
		echo "<br /></div>";
	} else {
		echo "<br /><div class='jlms_test_notice' style='text-align:center'><span class='jlms_txt_error'>Pre-installation test has failed.</span></div>";
	}
	
//	$result1 = LH_TestCURL();
//	LH_ShowBlockResults($result1);
	
	LH_ShowEnd();
}

function LH_testCURL(&$result) {
	$ret = array('result' => true, 'class' => 'warning', 'caption' => 'Payment modules', 'text' => '', 'instructions' => '');
	$ret['instructions'] = '';
	$ret['text'] = '';
  	if (!function_exists('curl_init'))
 	 {
		$ret['instructions'] = 'You server must support cURL library for PHP and have the correct configuration in order to support Authorise.NET payment module.<br>';
		$ret['text'] = "PHP cURL library (php_curl) is not installed.<br>";
 	 }
 	else {
 		$ret['text'] = "PHP cURL library is installed.";
 	}

 	if (!function_exists('fsockopen')) {
 		$ret['instructions'] .= '\'fsockopen()\' function is required in order to support Paypal payment module.<br>';
 		$ret['text'] .= "PHP \'fsockopen()\' function (php_sockets module) is not available.<br>";
 	}
 	else {
 		$ret['text'] .= "PHP 'fsockopen()' function is available.<br>";
 	}
 	
 	if( function_exists('curl_init') && function_exists('fsockopen')) {
 		$ret['text'] .= "Using of payment modules is possible.";
 		$ret['class'] = 'OK';
 	}	
 		
 	 return $ret;	
}


function LH_ShowBlockResults(&$result, $en_instr = true, $el_width = '', $el_margin='', $el_floatleft = false) {
	echo '<div class="jlms_test_'.$result['class'].'"'.(($el_width || $el_margin || $el_floatleft)? ('style="'.(($el_width?('width:'.$el_width.';'):'').($el_margin?(($el_width?' ':'').'margin-right:'.$el_margin.';'):'') . ($el_floatleft?((($el_width || $el_margin)?' ':'').'float:left;'):'')).'"'):'').'><span class="jlms_txt_'.$result['class'].'">'.$result['caption'].'</span>'.($result['text'] ? ('<br />'.$result['text']) : '').'</div>';
	if (!empty($result['instructions'])) {
		if($en_instr) {
			echo '<div class="jlms_test_notice"><b>Instructions: </b>'.$result['instructions'].'</div>';
		}
		else {
			echo '<div class="jlms_test_notice">'.$result['instructions'].'</div>';
		}
	}
}



function LH_ShowBegin() {
	echo '<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<title>JoomlaLMS installation</title>
	<link type="text/css" rel="stylesheet" href="joomlalms_helper.php?step=getcss" />
	</head>';
	echo '<body id="page_bg"><br /><br /><br /><div style="width:100%; text-align:center">';
					echo '<div id="whitebox">';
						echo '<div id="whitebox_t">
								<div id="whitebox_tl">
									<div id="whitebox_tr"><!--x--></div>
								</div>
							</div>';
						echo '<div id="whitebox_m">';
							echo '<div id="area">';
}

function LH_ShowEnd() {
	echo '<br /><div id="inst_tip"><div>Remember to remove this script after JoomlaLMS installation.</div></div>';
	echo '</div>';
							echo '<div class="clr"></div>';
						echo '</div>';
						echo '<div id="whitebox_b">
								<div id="whitebox_bl">
									<div id="whitebox_br"><!--x--></div>
								</div>
							</div>';
						echo '<div id="inst_footer"><div style="float:left">Copyright &copy; '.date('Y').' by <a href="http://www.joomlalms.com" target="_blank">JoomlaLMS</a></div><div style="float:right">JoomlaLMS Installation Helper 1.3.3 (30 May 2014)<br /></div></div>';
					echo '</div>';
		echo '</div>';
	echo '</body>';
	echo '</html>';
}

function LH_ShowNextBtn($link) {
	echo '<div style="float:right;"><table cellpadding="0" cellspacing="0" border="0"><tr><td valign="middle"><div class="nav_link"><a href="'.$link.'">NEXT</a>&nbsp;</div></td><td>';
	echo '<a href="'.$link.'"><img src="joomlalms_helper.php?step=image_forward" width="32" height="32" border="0" /></a></td></tr></table></div>';
}

function LH_ShowPrevBtn($link) {
	echo '<div style="float:left"><table cellpadding="0" cellspacing="0" border="0"><tr><td><a href="'.$link.'"><img src="joomlalms_helper.php?step=image_back" width="32" height="32" border="0" /></a></td><td>';
	echo '<div class="nav_link">&nbsp;<a href="'.$link.'">BACK</a></div></td></tr></table></div>';
}

function LH_TestMQGPC() {
	$ret = array('result' => true, 'class' => 'warning', 'caption' => 'PHP magic_quotes_gpc setting', 'text' => '', 'instructions' => '');
	if (@ini_get('magic_quotes_gpc') != '1' ) {
		$ret['text'] = "PHP magic_quotes_gpc setting is 'OFF' instead of 'ON'";
		$ret['instructions'] = "Enabling this setting will improve the security of your site. If you are using not-trusted 3pd extensions which can be vulnerable to SQl-injections enable this setting. The SQL injections will still be possible, but the risk will be reduced.<br />\n";
		$ret['instructions'] .= "For more information please visit the <a target='_blank' href='http://help.joomla.org/component/option,com_easyfaq/task,view/id,167/Itemid,268/'>FAQ</a> page on the official <a target='_blank' href='http://help.joomla.org/component/option,com_easyfaq/task,cat/catid,226/Itemid,99999999/'>Joomla! Help Site</a>.";
	} else {
		$ret['class'] = 'ok';
		$ret['text'] = "PHP magic_quotes_gpc setting is 'ON'";
	}
	return $ret;
}

function LH_TestRG() {
	$ret = array('result' => true, 'class' => 'warning', 'caption' => 'PHP register_globals setting', 'text' => '', 'instructions' => '');
	if ( @ini_get('register_globals') == '1' ) {
		$ret['text'] = "PHP register_globals setting is 'ON' instead of 'OFF'";
	} else {
		$ret['class'] = 'ok';
		$ret['text'] = "PHP register_globals setting is 'OFF'";
	}
	return $ret;
}

function LH_TestMEMLIMIT() {
	$ret = array('result' => true, 'class' => 'warning', 'caption' => 'PHP Memory Limit', 'text' => '', 'instructions' => '');
	$a = @ini_get('memory_limit');
	if (!$a) {
		$a = get_cfg_var('memory_limit');
	}
	$ml = intval($a);
	$add_instructions = false;
	if ($ml > 0 && $ml < 9) {
		$ret['text'] = 'PHP Memory Limit is set to '.$ml.'M.';
		$add_instructions = true;
		$ret['instructions'] = "In order for all JoomlaLMS and Joomla functions to work properly you should extend this setting.<br />\n";
	} elseif ($ml > 0 && $ml < 15) {
		$ret['text'] = 'PHP Memory Limit is set to '.$ml.'M.';
		$add_instructions = true;
		$ret['instructions'] = "This is enough for working with standard Joomla installation.<br />\n";
	} elseif ($ml > 0) {
		$ret['class'] = 'ok';
		$ret['text'] = 'PHP Memory Limit is set to '.$ml.'M.';
	} else {
		$ret['class'] = 'ok';
		$ret['text'] = 'PHP Memory Limit setting is disabled.';
	}
	if ($add_instructions) {
		$ret['instructions'] .= "If you get the error <b>\"Fatal error: Allowed memory size of X bytes exhausted\"</b> you will need more memory.<br />\n";
		$ret['instructions'] .= "To increase this setting modify your php.ini, try this value first:<br />\n";
		$ret['instructions'] .= "&nbsp;&nbsp;&nbsp;memory_limit = 20M<br />\n";
		$ret['instructions'] .= "If you still get the error you will need more memory like:<br />\n";
		$ret['instructions'] .= "&nbsp;&nbsp;&nbsp;memory_limit = 40M<br />\n";
		$ret['instructions'] .= "If, however, you have set your limit to 40M and the fatal memory error does not show, you can try lowering it down like:<br />\n";
		$ret['instructions'] .= "&nbsp;&nbsp;&nbsp;memory_limit = 32M<br />\n";
		$ret['instructions'] .= "If you still don't get the error, try lowering the memory_limit further. The idea is to have the least amount of memory_limit specification possible as long as you don't get fatal memory errors.";
	}
	return $ret;
}

function LH_TestFileUploads() {
	$ret = array('result' => true, 'class' => 'warning', 'caption' => 'File Uploads', 'text' => '', 'instructions' => '');
	$a = intval(@ini_get('post_max_size'));
	$b = intval(@ini_get('upload_max_filesize'));
	$limit = 0;
	if ($a || $b) {
		$limit = ($a < $b) ? $a : $b;
		if (!$limit) {
			$limit = $b;
		}
	}
	$ret['text'] = 'PHP post_max_size setting is '.@ini_get('post_max_size').'<br />';
	$ret['text'] .= 'PHP upload_max_filesize setting is '.@ini_get('upload_max_filesize').'<br />';
	if ($limit && $limit < 5) {
		$ret['instructions'] = 'You could upload documents, SCORM packages or other learning materials with the size of no more than '.$limit.'Mb.<br />';
		$ret['instructions'] .= 'To enlarge the possible size of uploaded files you should increase post_max_size and upload_max_filesize PHP settings.<br />';
		$ret['instructions'] .= "Add the below to the relevant php.ini file (recommended, if you have access). Note that for some hosts this is a system-wide setting. However, for hosts running PHP as a CGI script with suexec (for example) you may be able to put these directives in the php.ini file in your Joomla root directory.<br />";
		$ret['instructions'] .= "&nbsp;&nbsp;&nbsp;upload_max_filesize = 10M ;<br />\n";
		$ret['instructions'] .= "&nbsp;&nbsp;&nbsp;post_max_size = 20M ;<br />\n";
		$ret['instructions'] .= "If you don't have the access to your php.ini file add the below to your .htaccess file in your Joomla root directory.<br />\n";
		$ret['instructions'] .= "&nbsp;&nbsp;&nbsp;php_value upload_max_filesize 10M<br />\n";
		$ret['instructions'] .= "&nbsp;&nbsp;&nbsp;php_value post_max_size 20M<br />\n";
	} else {
		$ret['class'] = 'ok';
	}
	return $ret;
}

function LH_TestGD() {
	$ret = array('result' => true, 'class' => 'warning', 'caption' => 'PHP GD-support', 'text' => '', 'instructions' => '');
	$add_install_instructions = true;
	if(function_exists("gd_info")) {
		$info = gd_info();
		$version = $info["GD Version"];
		if(strpos($version,'2.') !== false && function_exists('imagecreatetruecolor')) {
			$ret['class'] = 'ok';
			$ret['text'] = 'GD2 Library '.$version.' is installed.';
			$add_install_instructions = false;
		} else {
			$ret['text'] = 'GD Library '.$version.' is installed.';
		}
	} else {
		$ret['text'] = 'GD image library is not installed.';
	}
	if ($add_install_instructions) {
		$ret['instructions'] = "In order for all JoomlaLMS functions (such as printing certificates and graphical overview of the course tracking) to work properly you should install GD2 Library.<br />\n";
		$ret['instructions'] .= "To enable GD-support configure PHP --with-gd[=DIR], where DIR is the GD base install directory. To use the recommended bundled version of the GD library, use the configure option --with-gd. GD library requires libpng and libjpeg to compile.<br />\n";
		$ret['instructions'] .= "In Windows you'll include the GD2 DLL php_gd2.dll as an extension to php.ini.<br />\n";
	} else {
		if(function_exists("gd_info")) {
			$info = gd_info();
			if (empty($info["PNG Support"])) {
				$ret['class'] = 'warning';
				$ret['instructions'] .= "<br /><b>PNG support</b> should be enabled.<br />\n";
				$ret['instructions'] .= "To enable support for png add --with-png-dir=DIR to the PHP configure line. Note, libpng requires the zlib library, therefore add --with-zlib-dir[=DIR] to your configure line.<br />\n";
				$ret['instructions'] .= "Alternatively contact your hosting provider or system administrator and ask them to enable PHP GD PNG support.<br />\n";
			} else {
				$ret['text'] .= "<br />PNG support is enabled.";
			}
			if (empty($info["FreeType Support"])) {
				$ret['class'] = 'warning';
				$ret['instructions'] .= "<br /><b>FreeType Support</b> should be enabled.<br />\n";
				$ret['instructions'] .= "To enable support for FreeType 2 add --with-freetype-dir=DIR to the PHP configure line.<br />\n";
				$ret['instructions'] .= "Alternatively, contact your hosting provider or system administrator, and ask them to enable PHP GD FreeType support.<br />\n";
			} else {
				$ret['text'] .= "<br />FreeType support is enabled";
			}
		}
	}
	return $ret;
}

function LH_TestHelperVersion() {
	global $JLMSHLPR;

	$ret = array('result' => false, 'class' => 'error', 'caption' => 'Checking JoomlaLMS Helper', 'text' => '<b>This version of JoomlaLMS Helper seems outdated, please upgrade joomlalms_helper.php before proceeding with the installation</b>', 'instructions' => '');

	$version_tobe_installed = intval(LH_SearchForFileVer(dirname(__FILE__)));
	if ($version_tobe_installed) {
		if ($version_tobe_installed > $JLMSHLPR['HLPR_VERSION']) {
			$ret['result'] = false;
		} else {
			$ret['result'] = true;
		}
	} else {
		$ret['result'] = true;
	}
	$ret['text'] = '<b>This version of JoomlaLMS Helper seems to be outdated, please update joomlalms_helper.php before proceeding with the installation</b>';
	return $ret;
}

function LH_TestJoomla() {
	$ret = array('result' => false, 'class' => 'error', 'caption' => 'Joomla! CMS', 'text' => 'Joomla! CMS not found', 'instructions' => '');
	if (file_exists(dirname(__FILE__)."/includes/version.php")) {
		require_once(dirname(__FILE__)."/includes/version.php");
		
		$_VERSION = new JVersion();
		$result = $_VERSION->getShortVersion();
		
		$ret['result'] = true;
		$ret['caption'] = 'Joomla! CMS is installed';
		$ret['class'] = 'ok';
		$ret['text'] = '<b>Version:</b> '.$result;		
	} elseif(file_exists(dirname(__FILE__)."/libraries/joomla/version.php")) {
		require_once(dirname(__FILE__)."/libraries/joomla/version.php");
		
		$_VERSION = new JVersion();
		$result = $_VERSION->getShortVersion();
		
		$ret['result'] = true;
		$ret['caption'] = 'Joomla! CMS installed';
		$ret['text'] = '<b>Version:</b> '.$result;
		/* LEGACY CHECK */
		if (!defined('_JEXEC')) { define( '_JEXEC', 1 ); }
		if (!defined('JPATH_BASE')) { define('JPATH_BASE', dirname(__FILE__) ); }
		if (!defined('DS')) { define( 'DS', DIRECTORY_SEPARATOR ); }

		if (file_exists(JPATH_BASE.'/includes/joomla.php') && file_exists(JPATH_BASE.'/configuration.php') && file_exists(JPATH_BASE.'/libraries/loader.php')) {
			require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );
			require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );
			
			if (class_exists('JApplication')) {
				$app = JApplication::getInstance('site');
			} else {
				$app = JFactory::getApplication('site');
			}
			JPluginHelper::importPlugin('system');

			$database = JFactory::getDBO();
			$query = "SELECT published FROM #__plugins WHERE element = 'legacy' AND folder = 'system'";
			$database->SetQuery($query);
			$leg = $database->LoadResult();
			if ($leg) {
				$ret['class'] = 'ok';
				$ret['text'] .= '<br /><b>Legacy Mode:</b> On';
			} else {
				$ret['class'] = 'ok';
				$ret['text'] .= '<br /><b>Legacy Mode:</b> Off.';			
			}
		} else if ( file_exists(JPATH_BASE.'/configuration.php') && file_exists(JPATH_BASE.'/libraries/loader.php') ) {	//Joomla 1.6		
			require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );
			require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );
			jimport('joomla.plugin.helper');
						
			if (class_exists('JApplication')) {				
				$app = JApplication::getInstance('site');
			} else {				
				$app = JFactory::getApplication('site');
			}
			
			JPluginHelper::importPlugin('system');
			

			$database = JFactory::getDBO();
			$query = "SELECT published FROM #__plugins WHERE element = 'legacy' AND folder = 'system'";
			$database->SetQuery($query);
			$leg = $database->LoadResult();
			if ($leg) {
				$ret['class'] = 'ok';
				$ret['text'] .= '<br /><b>Legacy Mode:</b> On';
			} else {
				$ret['class'] = 'ok';
				$ret['text'] .= '<br /><b>Legacy Mode:</b> Off.';
				//$ret['instructions'] = "JoomlaLMS requires enabled Legacy mode. You don't need to publish this plugin, it will be automatically published before the component's installation.";
			}
		} else {
			$ret['caption'] = 'Joomla! CMS has not been properly installed or there was an error while getting information about your CMS.';
			$ret['class'] = 'error';
			$ret['result'] = false;
			$ret['text'] .= '<br /><b>Legacy Mode:</b> Unable to get information about Legacy mode plugin. It seems that your Joomla! CMS has not been properly installed. The installation process might cause errors.';
		}		
	} elseif(file_exists(dirname(__FILE__)."/libraries/cms/version/version.php")) {
		require_once(dirname(__FILE__)."/libraries/cms/version/version.php");
		
		$_VERSION = new JVersion();
		$result = $_VERSION->getShortVersion();

		$ret['result'] = true;
		$ret['caption'] = 'Joomla! CMS is installed';
		$ret['text'] = '<b>Version:</b> '.$result;
		$ret['class'] = 'ok';

		$version_intval = intval(substr($result,0,1))*10 +  intval(substr($result,2,1));
		global $JLMSHLPR;
		if ($version_intval > $JLMSHLPR['HLPR_VERSION_JOOMLA_MAX_OFFICIAL']) {
			$ret['class'] = 'warning';
			$ret['result'] = true;
			$ret['text'] = "<b>Version:</b> ".$result."<br />This version of Joomla is not officially supported or this installation helper file is out of date";
		}

		/* LEGACY CHECK */
		if (!defined('_JEXEC')) { define( '_JEXEC', 1 ); }
		if (!defined('JPATH_BASE')) { define('JPATH_BASE', dirname(__FILE__) ); }
		if (!defined('DS')) { define( 'DS', DIRECTORY_SEPARATOR ); }

    } else {
		$ret['result'] = false;
		$ret['class'] = 'error';
		$ret['instructions'] = "Joomla! CMS was not found. Please make sure that Joomla is installed and this script is uploaded into the Joomla root folder";
	}
	
	return $ret;
}

function LH_TestPHPVersion() 
{
	$phpVersion = phpversion();
		
	if (strnatcmp($phpVersion,'5.0.0') >= 0)
    {
		if (strnatcmp($phpVersion,'5.3.0') >= 0) {
			$ret['caption'] = 'PHP Information';
			$ret['class'] = 'ok';
			$ret['result'] = true;
			$ret['text'] = '<b>Version:</b> '.$phpVersion;
			if (strnatcmp($phpVersion,'5.5.0') >= 0) {
				$version_tobe_installed = intval(LH_SearchForFileVer(dirname(__FILE__)));
				if ($version_tobe_installed == 131) {
					$ret['class'] = 'warning';
					$ret['text'] .="<br />The JoomlaLMS version you are trying to install is not compatible with this version of PHP";
				}
			}
			if (strnatcmp($phpVersion,'5.6.0') >= 0) {
				$ret['class'] = 'warning';
				$ret['text'] .="<br />This version of PHP is not officially supported or this installation helper file is out of date";
			}
		} else {
			$ret['caption'] = 'PHP Information';
			$ret['class'] = 'warning';
			$ret['result'] = true;
			$ret['text'] = '<b>Version:</b> '.$phpVersion;
			$ret['instructions'] = "<b>It is recommended to upgrade your PHP.</b><br />You can't use the latest JoomlaLMS version, but you can install <b>JoomlaLMS 1.3.1</b>";
		}
    }
    else
    {  			
        $ret['caption'] = 'PHP Information';
        $ret['class'] = 'warning';
		$ret['result'] = false;
		$ret['text'] = '<b>Version:</b> '.$phpVersion;
		$ret['instructions'] = '<b>Please upgrade your PHP.</b> PHP 5 is required in order to run JoomlaLMS 1.3.1. PHP 5.3.x+ is required in order to run JoomlaLMS 1.3.3 or higher. If you still use old version of PHP - you can request old JoomlaLMS package by submitting request at <a href="http://www.joomlalms.com/helpdesk/ticket_submit.php">www.joomlalms.com/helpdesk/ticket_submit.php</a>';
    } 
    
    return $ret;
}
function LH_TestIoncube() {
	$ret = array('result' => false, 'class' => 'error', 'caption' => 'ionCube Loader','text' => '', 'instructions' => '');
	if (extension_loaded('ionCube Loader') ) {
		$ioncube_loader_version = ioncube_loader_version();
		$ioncube_loader_version_major = substr($ioncube_loader_version,0,1);
		$ioncube_loader_version_minor = substr($ioncube_loader_version,2,1);
		//if ($ioncube_loader_version_major < 3 || ($ioncube_loader_version_major == 3 && $ioncube_loader_version_minor < 1) ) {
		if ($ioncube_loader_version_major < 4) {
			$ret['instructions'] = "Ioncube loader is installed but needs to be updated.<br />\n
			<b>It is recommended to use ioncube loader version 4.4 or higher</b>.<br />\n
			The most recent version of the loader can be found <a href=\"http://www.ioncube.com/loaders.php\" target=\"_blank\">here</a>.";
		} elseif ($ioncube_loader_version_major == 4 && $ioncube_loader_version_minor < 4 ){
			$ret['class'] = 'warning';
			$ret['result'] = true;
			$ret['instructions'] = "It is recommended to update your ioncube loader.<br />\n
			<b>Ioncube loader 4.4 or higher is required in order to run JoomlaLMS 1.3.3, but you can still use JoomlaLMS 1.3.1</b><br />\n
			The most recent version of the loader can be found <a href=\"http://www.ioncube.com/loaders.php\" target=\"_blank\">here</a>.";

		} else {
			$ret['class'] = 'ok';
			$ret['result'] = true;
		}
		$ret['caption'] = 'ionCube Loader is installed';
		$ret['text'] = '<b>Version:</b> '.$ioncube_loader_version;
	} else {
		$ret['caption'] = 'ionCube Loader';
		$ret['text'] = 'ionCube Loader not installed';
		$ret['instructions'] = "Testing whether your system supports run-time loading...<br />\n";
		$sys_info = array();
		$sys_info = LH_SysInfo();
		$try_to_install = true;
		if (isset($sys_info['THREAD_SAFE']) && $sys_info['THREAD_SAFE'] && isset($sys_info['CGI_CLI']) && !$sys_info['CGI_CLI']) {
			$ret['instructions'] .= "<br />\nYour PHP install appears to have <b>threading support</b> and run-time Loading is only possible on <b>threaded web servers if using the CGI, FastCGI or CLI interface.</b><br />\n";
			$ret['instructions'] .= "To run encoded files please install the Loader into the <b>php.ini</b> file. Instructions can be found <a href=\"http://www.ioncube.com/loader_installation.php\" target=\"_blank\">here</a>.<br />\n";
			$try_to_install = false;
		}
		if (isset($sys_info['DEBUG_BUILD']) && $sys_info['DEBUG_BUILD']) {
			$ret['instructions'] .= "<br />\nYour PHP installation appears to be built with <b>enabled debugging support</b>.ionCube Loaders are incompatible with this option.<br />Debugging support in PHP produces slower execution, it's not recommended for production builds and was probably installed by a mistake.<br />\n";
			$ret['instructions'] .= "You should rebuild PHP without the --enable-debug option.<br />\n";
			$try_to_install = false;
		}
		if (@ini_get('safe_mode') ) {
			$ret['instructions'] .= "<br />\nPHP <b>safe mode is enabled</b> and run time loading will not be possible.<br />";
			$ret['instructions'] .= "To run encoded files please install the Loader into the <b>php.ini</b> file. Instructions can be found <a href=\"http://www.ioncube.com/loader_installation.php\" target=\"_blank\">here</a><br />\n";
			$ret['instructions'] .= "Alternatively contact your hosting provider or system administrator and ask them to disable safe mode for your account.<br />\n";
			$try_to_install = false;
		}

		$_php_version = phpversion();
		$_u = php_uname();
		$_os = substr($_u,0,strpos($_u,' '));
		$_os_key = strtolower(substr($_u,0,3));

		//show system information
		$sys_info_text = '';
		if ( (isset($sys_info['PHP_INI']) && $sys_info['PHP_INI']) || $_php_version || $_os || isset($sys_info['THREAD_SAFE']) ) {
			$sys_info_text .="<br /><br />";
			if ($_os) {
				$sys_info_text .="<b>Operating system</b>: ".$_os."<br />";
			}
			if ($_php_version) {
				$sys_info_text .="<b>PHP version</b>: $_php_version<br />";
			}
			if (isset($sys_info['THREAD_SAFE'])) {
				$sys_info_text .="<b>Thread safety</b>: ".($sys_info['THREAD_SAFE'] ? 'Yes' : 'No')."<br />";
			}
			if (isset($sys_info['PHP_INI']) && $sys_info['PHP_INI']) {
				$sys_info_text .="<b>Path to php.ini</b>: ".$sys_info['PHP_INI']."<br />";
			}
		}


		if ($try_to_install) {
			/* Look for a Loader */
			$_php_family = substr($_php_version,0,3);

			$_loader_sfix = (($_os_key == 'win') ? '.dll' : '.so');

			$_ln_old="ioncube_loader.$_loader_sfix";
			$_ln_old_loc="/ioncube/$_ln_old";

			$_ln_new="ioncube_loader_${_os_key}_${_php_family}${_loader_sfix}";
			$_ln_new_loc="/ioncube/$_ln_new";

			$ret['instructions'] .= "Looking for Loader '$_ln_new'<br />\n";

			$_extdir = @ini_get('extension_dir');
			if ($_extdir == './') { $_extdir = '.'; }

			$_oid = $_id = realpath($_extdir);

			$_here = dirname(__FILE__);
			if ((@$_id[1]) == ':') {
				$_id = str_replace('\\','/',substr($_id,2));
				$_here = str_replace('\\','/',substr($_here,2));
			}
			$_rd=str_repeat('/..',substr_count($_id,'/')).$_here.'/';
		
			if ($_oid !== false) {
				$ret['instructions'] .=  "Extensions Dir: $_extdir ($_id)<br />\n";
				$ret['instructions'] .=  "Relative Path:  $_rd<br />\n";
			} else {
				$ret['instructions'] .=  "Extensions Dir: $_extdir (NOT FOUND)<br />\n";
		
				$ret['instructions'] .= "The directory set for the <b>extension_dir</b> entry in the php.ini file <b>may not exist</b>, and run time loading will not be possible.<br />\n";
				$ret['instructions'] .= "Please ask your hosting provider or system administrator to create the directory<br />$_extdir<br />ensuring that it is accessible by the web server software. They do not need to restart the server. Then rerun this script.";
				$ret['instructions'] .= " As an alternative your host could install the Loader into the php.ini file. Instructions can be found <a href=\"http://www.ioncube.com/loader_installation.php\" target=\"_blank\">here</a>.<br />\n";
				$try_to_install = false;
			}

			if ($try_to_install) {
				$_ln = '';
				$_i=strlen($_rd);
				while($_i--) {
					if($_rd[$_i]=='/') {
						$_lp=substr($_rd,0,$_i).$_ln_new_loc;
						$_fqlp=$_oid.$_lp;
						if(@file_exists($_fqlp)) {
							$ret['instructions'] .= "Loader $_fqlp was found.<br />\n";
							$_ln=$_lp;
							break;
						}
					}
				}
				// If Loader not found, try the fallback of in the extensions directory
				if (!$_ln) {
					if (@file_exists($_id.$_ln_new_loc)) {
						$_ln = $_ln_new_loc;
					}
					if ($_ln) {
						$ret['instructions'] .= "Loader $_ln was found in the extensions directory.<br />\n";
					}
				}
				$ret['instructions'] .= "<br />\n";

				if ($_ln) {
					$ret['instructions'] .= "Trying to install the Loader - this may produce an error...<br />\n";
					if ($_php_family < '5.3')  // function dl() is depricated in PHP 5.3+
					{
						@dl($_ln);
					}
					if( extension_loaded('ionCube Loader') ) {
						$ret['class'] = 'ok';
						$ret['result'] = true;
						$ret['text'] = 'ionCube Loader has been successfully installed';
						$ret['instructions'] .= "The Loader has been successfully installed and encoded files should be able to automatically install the Loader when needed.<br />\n";
						$ret['instructions'] .= "No changes to your php.ini file are required in order to use encoded files on this system.";
					} else {
						$ret['instructions'] .= "Loader was not installed.";
					}
				} else {
					$ret['instructions'] .= "Run-time loading should be possible on your system but no suitable Loader was found.<br />\n";
					$ret['instructions'] .= "The Loader for <b>$_os</b> (PHP $_php_family) is required.<br />\n";
					$ret['instructions'] .= "Loaders can be downloaded from <a href=\"http://www.ioncube.com/loaders.php\" target=\"_blank\">www.ioncube.com</a><br />\n";
					$ret['instructions'] .= "Please download the appropriate loader, extract the package and upload the 'ioncube' folder into the root directory of your site (e.g. '/public_html/ioncube/' or '/htdocs/ioncube/'). <b>After that run this script again.</b>";
				}
			}
		}

		if (empty($ret['instructions'])) {
			$ret['instructions'] = "Run-time loading is not currently possible.<br />\n"."Please contact JoomlaLMS support providing a link to this script.";
		}
		if ($sys_info_text) {
			$ret['instructions'] .= $sys_info_text;
		}
	}
	return $ret;
}


function LH_SysInfo() {
	$thread_safe = false;
	$debug_build = false;
	$cgi_cli = false;
	$php_ini_path = '';

	ob_start();
	phpinfo(INFO_GENERAL);
	$php_info = ob_get_contents();
	ob_end_clean();
//echo $php_info;die;
	foreach (preg_split("/\s/",$php_info) as $line) {
		if (preg_match("/command/i", $line)) {
			continue;
		}

		if (preg_match('/thread safety.*(enabled|yes)/Ui',$line)) {
			$thread_safe = true;
		}

		if (preg_match('/debug.*(enabled|yes)/Ui',$line)) {
			$debug_build = true;
		}
		if (preg_match('~configuration file.*(</B></td><TD ALIGN="left">| => |v">)([^ <]*)~i',$line,$match)) {
			$php_ini_path = $match[2];
			if ($php_ini_path) {
				if (!@file_exists($php_ini_path)) {
					$php_ini_path = '';
				}
			}
		}
		if (!$php_ini_path && function_exists('php_ini_loaded_file')) {
	        $php_ini_path = php_ini_loaded_file();
	        if ($php_ini_path === false) {
	            $php_ini_path = '';
	        }
	    }

		$cgi_cli = ((strpos(php_sapi_name(),'cgi') !== false) || (strpos(php_sapi_name(),'cli') !== false));
	}

	return array('THREAD_SAFE' => $thread_safe, 'DEBUG_BUILD' => $debug_build, 'PHP_INI'     => $php_ini_path, 'CGI_CLI'     => $cgi_cli);
}

function LH_SearchForFile($dir) {
	$priority = 10;
	$zip_file = '';
	$ver = '0';
	/*
	0 = 1.0.0 
	1 = 1.0.1
	2 = 1.0.2
	3 = 1.0.3
	4 = 1.0.4
	5 = 1.0.5
	6 = 1.0.6
	*/
	if ($current_dir = @opendir( $dir )) {
		//$old_umask = umask(0);
		while ($entryname = readdir( $current_dir )) {
			if ($entryname != '.' and $entryname != '..') {
				if (is_dir( $dir . "/".$entryname )) {

				} else {
					if (substr($entryname,-4) == '.zip') {
						if (strtolower(substr($entryname,5,11)) == '_joomlalms_' && strtolower(substr($entryname,21,13)) == '_pro_unzip1st' && $priority > 1) {
							if (!$zip_file) { $zip_file = $entryname; }
							$priority = 1.5;
							$ver1 = intval(substr($entryname,20,1));
							if ($ver1 > $ver) {
								$zip_file = $entryname;
								$ver = $ver1;
							}
							
						}
						if (strtolower(substr($entryname,5,11)) == '_joomlalms_' && strtolower(substr($entryname,24,13)) == '_pro_unzip1st' && $priority > 1) {
							//XXXXX_JoomlaLMS_1[1].0.X_PRO_unzip1st.zip
							if (!$zip_file) { $zip_file = $entryname; }
							$priority = 1.5;
							$ver1 = intval(substr($entryname,23,1));
							if ($ver1 > $ver) {
								$zip_file = $entryname;
								$ver = $ver1;
							}
							
						}
						if (strtolower(substr($entryname,5,11)) == '_joomlalms_' && strtolower(substr($entryname,21,13)) == '_std_unzip1st' && $priority > 2) {
							if (!$zip_file) { $zip_file = $entryname; }
							$priority = 2.5;
							$ver1 = intval(substr($entryname,20,1));
							if ($ver1 > $ver) {
								$zip_file = $entryname;
								$ver = $ver1;
							}
							
						}
						if (strtolower(substr($entryname,5,11)) == '_joomlalms_' && strtolower(substr($entryname,24,13)) == '_std_unzip1st' && $priority > 2) {
							//XXXXX_JoomlaLMS_1[1].0.X_STD_unzip1st.zip
							if (!$zip_file) { $zip_file = $entryname; }
							$priority = 2.5;
							$ver1 = intval(substr($entryname,23,1));
							if ($ver1 > $ver) {
								$zip_file = $entryname;
								$ver = $ver1;
							}
							
						}
						if (strtolower(substr($entryname,0,10)) == 'joomlalms_' && strtolower(substr($entryname,15,15)) == '_trial_unzip1st' && $priority > 5) {
							if (!$zip_file) { $zip_file = $entryname; }
							$priority = 5.5;
							$ver1 = intval(substr($entryname,14,1));
							if ($ver1 > $ver) {
								$zip_file = $entryname;
								$ver = $ver1;
							}
						}
						if (strtolower(substr($entryname,0,10)) == 'joomlalms_' && strtolower(substr($entryname,18,15)) == '_trial_unzip1st' && $priority > 5) {
							//joomlalms_1[1].0.6_trial_unzip1st.zip
							if (!$zip_file) { $zip_file = $entryname; }
							$priority = 5.5;
							$ver1 = intval(substr($entryname,17,1));
							if ($ver1 > $ver) {
								$zip_file = $entryname;
								$ver = $ver1;
							}
						}
						if (strtolower($entryname) == 'com_joomla_lms.zip' && $priority > 7) {
							$zip_file = $entryname; $priority = 7.5;
						}
					}
				}
			}
		}
		closedir( $current_dir );
	} else {
		echo "Failed to open DIR $dir";
	}
	return $zip_file;
}

function LH_SearchForFileVer($dir) {
	$ver = '0';
	if ($current_dir = @opendir( $dir )) {
		//$old_umask = umask(0);
		while ($entryname = readdir( $current_dir )) {
			if ($entryname != '.' and $entryname != '..') {
				if (is_dir( $dir . "/".$entryname )) {
				} else {
					if (substr($entryname,-4) == '.zip') {
						if (strtolower(substr($entryname,5,11)) == '_joomlalms_' && strtolower(substr($entryname,21,13)) == '_pro_unzip1st') {
							$ver1 = intval(substr($entryname,16,1))*100 + intval(substr($entryname,18,1))*10 + intval(substr($entryname,20,1));
							if ($ver1 > $ver) {
								$ver = $ver1;
							}
						}
						if (strtolower(substr($entryname,5,11)) == '_joomlalms_' && strtolower(substr($entryname,24,13)) == '_pro_unzip1st') {
							//XXXXX_JoomlaLMS_1[1].0.X_PRO_unzip1st.zip
							$ver1 = intval(substr($entryname,16,1))*100 + intval(substr($entryname,21,1))*10 + intval(substr($entryname,23,1));
							if ($ver1 > $ver) {
								$ver = $ver1;
							}
						}
						if (strtolower(substr($entryname,5,11)) == '_joomlalms_' && strtolower(substr($entryname,21,13)) == '_std_unzip1st') {
							$ver1 = intval(substr($entryname,16,1))*100 + intval(substr($entryname,18,1))*10 + intval(substr($entryname,20,1));
							if ($ver1 > $ver) {
								$ver = $ver1;
							}
						}
						if (strtolower(substr($entryname,5,11)) == '_joomlalms_' && strtolower(substr($entryname,24,13)) == '_std_unzip1st') {
							//XXXXX_JoomlaLMS_1[1].0.X_STD_unzip1st.zip
							$ver1 = intval(substr($entryname,16,1))*100 + intval(substr($entryname,21,1))*10 + intval(substr($entryname,23,1));
							if ($ver1 > $ver) {
								$zip_file = $entryname;
								$ver = $ver1;
							}
							
						}
						if (strtolower(substr($entryname,0,10)) == 'joomlalms_' && strtolower(substr($entryname,15,15)) == '_trial_unzip1st') {
							$ver1 = intval(substr($entryname,10,1))*100 + intval(substr($entryname,12,1))*10 + intval(substr($entryname,14,1));
							if ($ver1 > $ver) {
								$ver = $ver1;
							}
						}
						if (strtolower(substr($entryname,0,10)) == 'joomlalms_' && strtolower(substr($entryname,18,15)) == '_trial_unzip1st' && $priority > 5) {
							//joomlalms_1[1].0.6_trial_unzip1st.zip
							$ver1 = intval(substr($entryname,10,1))*100 + intval(substr($entryname,15,1))*10 + intval(substr($entryname,17,1));
							if ($ver1 > $ver) {
								$ver = $ver1;
							}
						}
					}
				}
			}
		}
		closedir( $current_dir );
	} else {
		//echo "Failed to open DIR $dir";
	}
	return $ver;
}


function LH_PreparePackage($zip_file, $jtf) {
	$paths = array();
	$paths['install_path'] = '';
	$paths['error_log'] = '';
	$paths['menu_module'] = '';
	$paths['menu_module_new'] = '';
    $paths['mailbox_module'] = '';
    $paths['homework_module'] = '';
    $paths['dropbox_module'] = '';
    $paths['certificates_module'] = '';
    $paths['forum_posts_module'] = '';
    $paths['announcements_module'] = '';
	$paths['courses_module'] = '';
	$paths['search_bot'] = '';
	$paths['search_bot_new'] = '';
	$paths['paypalbtn_bot'] = '';
	$paths['paypalbtn_bot_new'] = '';
	$paths['sqbox_bot_new'] = '';
	$paths['cb_plugin_old'] = '';
	$paths['cb_plugin_new'] = '';
	if ($zip_file == 'com_joomla_lms.zip') {
		jh_extractBackupArchive(dirname(__FILE__)."/".$zip_file, dirname(__FILE__)."/".$jtf."/");
		$paths['install_path'] = dirname(__FILE__) ."/".$jtf."/com_joomla_lms";
		if (file_exists($paths['install_path']."/")) {
		} else {
			$paths['error_log'] .= "Unknown error at line ".__LINE__.".<br />";
		}
	} else {
		$temp_folder = md5(uniqid(rand(), true));
		$temp_path = dirname(__FILE__). "/".$jtf."/".$temp_folder;
		if (true) {
			jh_extractBackupArchive(dirname(__FILE__)."/".$zip_file, $temp_path."/");
			if (file_exists($temp_path."/com_joomla_lms.zip")) {
				jh_extractBackupArchive($temp_path."/com_joomla_lms.zip", dirname(__FILE__)."/".$jtf."/");
				$paths['install_path'] = dirname(__FILE__) ."/".$jtf."/com_joomla_lms";
				
				if (file_exists($temp_path."/modules_plugins/mod_jlms_menu.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/mod_lms_menu/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/mod_lms_menu/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/mod_jlms_menu.zip", dirname(__FILE__)."/".$jtf."/mod_lms_menu/");
						$paths['menu_module'] = dirname(__FILE__)."/".$jtf."/mod_lms_menu";
					}
				}
				if (file_exists($temp_path."/modules_plugins/mod_jlms_menu_1.5.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/mod_lms_menu_new/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/mod_lms_menu_new/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/mod_jlms_menu_1.5.zip", dirname(__FILE__)."/".$jtf."/mod_lms_menu_new/");
						$paths['menu_module_new'] = dirname(__FILE__)."/".$jtf."/mod_lms_menu_new";
					}
				}
				if (file_exists($temp_path."/modules_plugins/mod_lms_courses.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/mod_lms_courses/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/mod_lms_courses/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/mod_lms_courses.zip", dirname(__FILE__)."/".$jtf."/mod_lms_courses/");
						$paths['courses_module'] = dirname(__FILE__)."/".$jtf."/mod_lms_courses";
					}
				}
                if (file_exists($temp_path."/modules_plugins/mod_jlms_mailbox.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/mod_jlms_mailbox/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/mod_jlms_mailbox/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/mod_jlms_mailbox.zip", dirname(__FILE__)."/".$jtf."/mod_jlms_mailbox/");
						$paths['mailbox_module'] = dirname(__FILE__)."/".$jtf."/mod_jlms_mailbox";
					}
				}
                if (file_exists($temp_path."/modules_plugins/mod_jlms_homework.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/mod_jlms_homework/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/mod_jlms_homework/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/mod_jlms_homework.zip", dirname(__FILE__)."/".$jtf."/mod_jlms_homework/");
						$paths['homework_module'] = dirname(__FILE__)."/".$jtf."/mod_jlms_homework";
					}
				}
                if (file_exists($temp_path."/modules_plugins/mod_jlms_dropbox.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/mod_jlms_dropbox/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/mod_jlms_dropbox/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/mod_jlms_dropbox.zip", dirname(__FILE__)."/".$jtf."/mod_jlms_dropbox/");
						$paths['dropbox_module'] = dirname(__FILE__)."/".$jtf."/mod_jlms_dropbox";                        
					}
				}
                if (file_exists($temp_path."/modules_plugins/mod_jlms_certificates.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/mod_jlms_certificates/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/mod_jlms_certificates/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/mod_jlms_certificates.zip", dirname(__FILE__)."/".$jtf."/mod_jlms_certificates/");
						$paths['certificates_module'] = dirname(__FILE__)."/".$jtf."/mod_jlms_certificates";
					}
				}
                if (file_exists($temp_path."/modules_plugins/mod_jlms_forum_posts.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/mod_jlms_forum_posts/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/mod_jlms_forum_posts/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/mod_jlms_forum_posts.zip", dirname(__FILE__)."/".$jtf."/mod_jlms_forum_posts/");
						$paths['forum_posts_module'] = dirname(__FILE__)."/".$jtf."/mod_jlms_forum_posts";
					}
				}
                if (file_exists($temp_path."/modules_plugins/mod_jlms_announcements.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/mod_jlms_announcements/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/mod_jlms_announcements/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/mod_jlms_announcements.zip", dirname(__FILE__)."/".$jtf."/mod_jlms_announcements/");
						$paths['announcements_module'] = dirname(__FILE__)."/".$jtf."/mod_jlms_announcements";
					}
				}
				if (file_exists($temp_path."/modules_plugins/mod_lms_courses_1.5.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/mod_lms_courses_new/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/mod_lms_courses_new/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/mod_lms_courses_1.5.zip", dirname(__FILE__)."/".$jtf."/mod_lms_courses_new/");
						$paths['courses_module_new'] = dirname(__FILE__)."/".$jtf."/mod_lms_courses_new";
					}
				}
				if (file_exists($temp_path."/modules_plugins/jlms_searchbot.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/bot_lms_search/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/bot_lms_search/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/jlms_searchbot.zip", dirname(__FILE__)."/".$jtf."/bot_lms_search/");
						$paths['search_bot'] = dirname(__FILE__)."/".$jtf."/bot_lms_search";
					}
				}
				if (file_exists($temp_path."/modules_plugins/jlms_searchplugin_1.5.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/bot_lms_search_new/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/bot_lms_search_new/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/jlms_searchplugin_1.5.zip", dirname(__FILE__)."/".$jtf."/bot_lms_search_new/");
						$paths['search_bot_new'] = dirname(__FILE__)."/".$jtf."/bot_lms_search_new";
					}
				}
				if (file_exists($temp_path."/modules_plugins/joomlalms_paypalbtn.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/bot_paypal_btn/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/bot_paypal_btn/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/joomlalms_paypalbtn.zip", dirname(__FILE__)."/".$jtf."/bot_paypal_btn/");
						$paths['paypalbtn_bot'] = dirname(__FILE__)."/".$jtf."/bot_paypal_btn";
					}
				}
				if (file_exists($temp_path."/modules_plugins/joomlalms_paypalbtn_1.5.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/bot_paypal_btn_new/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/bot_paypal_btn_new/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/joomlalms_paypalbtn_1.5.zip", dirname(__FILE__)."/".$jtf."/bot_paypal_btn_new/");
						$paths['paypalbtn_bot_new'] = dirname(__FILE__)."/".$jtf."/bot_paypal_btn_new";
					}
				}
				if (file_exists($temp_path."/modules_plugins/jlmssqueezebox.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/sqbox_bot_new/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/sqbox_bot_new/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/jlmssqueezebox.zip", dirname(__FILE__)."/".$jtf."/sqbox_bot_new/");
						$paths['sqbox_bot_new'] = dirname(__FILE__)."/".$jtf."/sqbox_bot_new";
					}
				}
				if (file_exists($temp_path."/modules_plugins/cb_joomlalms_autoenroll.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/cb_lms_autoenroll_old/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/cb_lms_autoenroll_old/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/cb_joomlalms_autoenroll.zip", dirname(__FILE__)."/".$jtf."/cb_lms_autoenroll_old/");
						$paths['cb_plugin_old'] = dirname(__FILE__)."/".$jtf."/cb_lms_autoenroll_old";
					}
				}
				if (file_exists($temp_path."/modules_plugins/cb_joomlalms_autoenroll_1.1.zip")) {
					$do_extract = false;
					if (!file_exists(dirname(__FILE__)."/".$jtf."/cb_lms_autoenroll_new/")) {
						if (mkdir(dirname(__FILE__)."/".$jtf."/cb_lms_autoenroll_new/")) {
							$do_extract = true;
						}
					} else { $do_extract = true; }
					if ($do_extract) {
						jh_extractBackupArchive($temp_path."/modules_plugins/cb_joomlalms_autoenroll_1.1.zip", dirname(__FILE__)."/".$jtf."/cb_lms_autoenroll_new/");
						$paths['cb_plugin_new'] = dirname(__FILE__)."/".$jtf."/cb_lms_autoenroll_new";
					}
				}
				
				if (file_exists($paths['install_path'])) {
				} else {
					$paths['error_log'] .= "Unknown error in line ".__LINE__.".<br />";
				}
			} else {
				$paths['error_log'] .= "Unable to unpack JoomlaLMS package.<br />";
			}
			sleep(1);
			jh_deldir($temp_path."/");
		} else {
			$paths['error_log'] .= "Function mkdir() failed in line ".__LINE__.".<br />";
		}
	}
	
	return $paths;
}
function jh_mosPathName($p_path,$p_addtrailingslash = true) {
	$retval = "";

	$isWin = (substr(PHP_OS, 0, 3) == 'WIN');

	if ($isWin)	{
		$retval = str_replace( '/', '\\', $p_path );
		if ($p_addtrailingslash) {
			if (substr( $retval, -1 ) != '\\') {
				$retval .= '\\';
			}
		}

		// Check if UNC path
		$unc = substr($retval,0,2) == '\\\\' ? 1 : 0;

		// Remove double \\
		$retval = str_replace( '\\\\', '\\', $retval );

		// If UNC path, we have to add one \ in front or everything breaks!
		if ( $unc == 1 ) {
			$retval = '\\'.$retval;
		}
	} else {
		$retval = str_replace( '\\', '/', $p_path );
		if ($p_addtrailingslash) {
			if (substr( $retval, -1 ) != '/') {
				$retval .= '/';
			}
		}

		// Check if UNC path
		$unc = substr($retval,0,2) == '//' ? 1 : 0;

		// Remove double //
		$retval = str_replace('//','/',$retval);

		// If UNC path, we have to add one / in front or everything breaks!
		if ( $unc == 1 ) {
			$retval = '/'.$retval;
		}
	}

	return $retval;
}
function jh_extractBackupArchive($archivename , $extractdir) {
	$archivename = jh_mosPathName( $archivename, false );	
	if (preg_match('/\.zip$/i', $archivename )) {
		// Extract functions
		if( joomlaVersion() == 16 ) 
		{			
			JArchive::extract( $archivename, $extractdir );			
		} else {
			$backupfile = new PclZip( $archivename );
			$ret = $backupfile->extract( PCLZIP_OPT_PATH, $extractdir );
		}

	}
	return true;
}
function jh_deldir( $dir, $only_clean = false ) {
	
	$current_dir = opendir( $dir );	
	//$old_umask = umask(0);
	while ($entryname = readdir( $current_dir )) {
		if ($entryname != '.' and $entryname != '..') {
			if (is_dir( $dir . $entryname )) {
				jh_deldir( jh_mosPathName( $dir . $entryname ) );
			} else {
                //@chmod($dir . $entryname, 0777);
				@unlink( $dir . $entryname );
			}
		}
	}
	//umask($old_umask);
	closedir( $current_dir );
	if (!$only_clean) {
		return @rmdir( $dir );
	} else {
		return true;
	}
}
function jh_copyUploadDirr($fromDir,$toDir) {
	$errors=array();
	$messages=array();
	if (!is_writable($toDir)) {
		$errors[]='target '.$toDir.' is not writable';
	}
	if (!is_dir($toDir)) {
		$errors[]='target '.$toDir.' is not a directory';
	}
	if (!is_dir($fromDir)) {
		$errors[]='source '.$fromDir.' is not a directory';
	}
	if (!empty($errors))
	{
		return false;
	}

	$exceptions=array('.','..','JLMS_intro.swf');

	$handle=opendir($fromDir);
	while (false!==($item=readdir($handle)))
	if (!in_array($item,$exceptions))
	{

		$from=str_replace('//','/',$fromDir.'/'.$item);
		$to=str_replace('//','/',$toDir.'/'.$item);
		if (is_file($from))
		{
			if (@copy($from,$to))
			{
				/*chmod($to,$chmod);*/
				touch($to,filemtime($from));
				$messages[]='File copied from '.$from.' to '.$to;
			}
			else
			$errors[]='cannot copy file from '.$from.' to '.$to;
		}
		if (is_dir($from))
		{
			if (@mkdir($to))
			{
				/*chmod($to,$chmod);*/
				$messages[]='Directory created: '.$to;
			}
			else
			$errors[]='cannot create directory '.$to;
			jh_copyUploadDirr($from,$to);
		}
	}
	closedir($handle);
	return true;
}
function jh_KeepDB() 
{	
	$database = JFactory::getDBO();
		
	$query = "SELECT * FROM #__users LIMIT 0,1";
	$database->SetQuery( $query );
	$database->query();	
	
	if ($database->geterrormsg()) 
	{
		sleep(1);
		
		$conf = JFactory::getConfig();

		$host 		= $conf->getValue('config.host');
		$user 		= $conf->getValue('config.user');
		$password 	= $conf->getValue('config.password');
		$database	= $conf->getValue('config.db');
		$prefix 	= $conf->getValue('config.dbprefix');
		$driver 	= $conf->getValue('config.dbtype');
		$debug 		= $conf->getValue('config.debug');
		
		$options	= array ('driver' => $driver, 'host' => $host, 'user' => $user, 'password' => $password, 'database' => $database, 'prefix' => $prefix, mt_rand() => mt_rand() );

		$database = JDatabase::getInstance( $options );				
	}
}
?>