<?php
defined('_JEXEC') or die;
jimport('joomla.plugin.plugin');

class plgSystemPlot extends JPlugin
{

    function onAfterInitialise()
    {
        require_once(JPATH_ADMINISTRATOR . '/components/com_plot/requires.php');
    }

    function onAfterRoute()
    {
        $app = JFactory::getApplication();

        if (JRequest::getVar('option') == 'com_joomla_lms' && JRequest::getVar('task') == 'change_course' && JRequest::getVar('state') == 1) {
            $courseId = JRequest::getInt('id');
            $course = JFactory::getDbo()->setQuery("SELECT `owner_id`, `course_name` FROM `#__lms_courses` WHERE `id` = $courseId")->loadObject();
            $courseCreatorId = $course->owner_id;
            require_once JPATH_ROOT . '/components/com_plot/helpers/jlms.php';
            PlotHelper::sendSystemMessage($courseCreatorId, "Созданный вами курс '$course->course_name' был опубликован");
        }

        if (JRequest::getVar('option') == 'com_joomla_lms' && JRequest::getVar('task') == 'publish_course' && $app->isAdmin()) {
            $coursesIdArr = JRequest::getVar('cid');
            $courseId = (int)$coursesIdArr[0];
            $course = JFactory::getDbo()->setQuery("SELECT `owner_id`, `course_name` FROM `#__lms_courses` WHERE `id` = $courseId")->loadObject();
            $courseCreatorId = $course->owner_id;
            require_once JPATH_ROOT . '/components/com_plot/helpers/jlms.php';
            PlotHelper::sendSystemMessage($courseCreatorId, "Созданный вами курс '$course->course_name' был опубликован");
        }

        if (JRequest::getVar('option') == 'com_joomla_lms' && (JRequest::getVar('task') == 'save_course' || JRequest::getVar('task') == 'apply_course') && $app->isAdmin()) {
            $courseId = JRequest::getVar('id');
            $course = JFactory::getDbo()->setQuery("SELECT `owner_id`, `course_name` FROM `#__lms_courses` WHERE `id` = $courseId")->loadObject();
            $courseCreatorId = $course->owner_id;
            require_once JPATH_ROOT . '/components/com_plot/helpers/jlms.php';
            PlotHelper::sendSystemMessage($courseCreatorId, "Созданный вами курс '$course->course_name' был отредактирован администратором");
        }

        if (JRequest::getVar('option') == 'com_joomla_lms' && JRequest::getVar('task') == 'course_delete_yes') {
            $courseId = JRequest::getInt('id');
            if ($app->isAdmin()) {
                $courseIdArr = JRequest::getVar('cid');
                $courseId = $courseIdArr[0];
            }
            $course = JFactory::getDbo()->setQuery("SELECT `owner_id`, `course_name` FROM `#__lms_courses` WHERE `id` = $courseId")->loadObject();
            $courseCreatorId = $course->owner_id;
            require_once JPATH_ROOT . '/components/com_plot/helpers/jlms.php';
            PlotHelper::sendSystemMessage($courseCreatorId, "Созданный вами курс '$course->course_name' был удален администратором.");
        }

        # add button "do test" to reading book mode
        # add meta tag to reading book mode
        # redirect if no access to book
        if (JRequest::getVar('option') == 'com_html5flippingbook' && JRequest::getVar('view') == 'publication' && $app->isSite()) {
            require_once JPATH_ROOT . '/components/com_plot/models/publication.php';
            if (!PlotHelper::bookReadAccess(JRequest::getVar('id'))) {
                // $app->redirect( jRoute::_('index.php?option=com_plot&view=publication&id='.JRequest::getVar('id'), false) );
            }
            $doc = JFactory::getDocument();
            $bookId = JRequest::getVar('id');
            $bookModel = JModelLegacy::getInstance('publication', 'plotModel');
            JRequest::setVar('bookId', $bookId);
            $book = $bookModel->getBook();
            $additionalClass = plotUser::factory()->isParent() ? 'plot-parent' : 'plot-child';
            $quizLink = JRoute::_('index.php?option=com_joomlaquiz&view=quiz&quiz_id=' . (int)$book->id_quiz);
            /*  $button = "<button onclick=\"window.location.href=\'".$quizLink."\'\" class=\"$additionalClass reading-mode-do-quiz-button\">"
              ."<svg preserveAspectRatio=\"xMidYMid meet\" viewBox=\"0 0 57.5 57.1\">"
              ."<path style=\"fill:#BC4D02;\" d=\"M15.9,0h24.4c1.2,0,2.2,0.5,3,1.2c0.8,0.8,1.2,1.8,1.2,3v3.1c0,1.2-0.5,2.2-1.2,3c-0.8,0.8-1.8,1.2-3,1.2h-1v7.6c3.3,5.3,7.2,11.6,10.5,17.3c3.3,5.6,6,10.4,7.2,12.8c0.3,0.6,0.4,1.2,0.4,1.8c0,1.1-0.5,2.2-1.2,3.1c-0.7,0.8-1.6,1.5-2.6,2c-1.1,0.6-2.4,0.9-3.6,0.9H7.4c-1.2,0-2.5-0.3-3.6-0.9c-1-0.5-1.9-1.2-2.6-2c-0.8-0.9-1.2-2-1.2-3.1c0-0.6,0.1-1.2,0.4-1.8c1.9-3.7,7.4-13.6,12.3-22.4c1.7-3.1,3.4-6.1,4.2-7.6v-7.6h-1c-1.2,0-2.2-0.5-3-1.2c-0.8-0.8-1.2-1.8-1.2-3V4.2c0-1.2,0.5-2.2,1.2-3C13.7,0.5,14.8,0,15.9,0L15.9,0z M40.4,3H15.9c-0.3,0-0.7,0.1-0.9,0.4c-0.2,0.2-0.4,0.5-0.4,0.9v3.1c0,0.3,0.1,0.7,0.4,0.9c0.2,0.2,0.5,0.4,0.9,0.4h2.5H20v1.5v9.5v0.4l-0.2,0.4c-1.9,3.4-3.1,5.6-4.4,8C10.4,37,5,46.9,3.1,50.6C3,50.8,3,50.9,3,51.1c0,0.4,0.2,0.8,0.5,1.2c0.4,0.5,1,0.9,1.6,1.2c0.7,0.4,1.5,0.6,2.3,0.6h42.7c0.7,0,1.5-0.2,2.3-0.6c0.6-0.3,1.2-0.7,1.6-1.2c0.3-0.4,0.5-0.8,0.5-1.2c0-0.2,0-0.3-0.1-0.5c-1.2-2.4-3.9-7.2-7.1-12.6	c-3.4-5.7-7.3-12.2-10.7-17.6L36.4,20v-0.4v-9.5V8.6h1.5h2.5c0.3,0,0.7-0.1,0.9-0.4c0.2-0.2,0.4-0.5,0.4-0.9V4.2c0-0.3-0.1-0.7-0.4-0.9C41,3.1,40.7,3,40.4,3L40.4,3z\"/>"
              ."<path style=\"fill-rule:evenodd;clip-rule:evenodd;fill:#BC4D02;\" d=\"M6,52.2l11.1-20.6c1.9,0.9,4.5,1.7,7.7,1.3c3.2-0.4,4.8-3.7,7.2-5.8c1.5-1.1,2.7-2.2,4.3-2.9l15.1,28L6,52.2z M20.6,37.6c1.5,0,2.8,1.2,2.8,2.8s-1.2,2.8-2.8,2.8c-1.5,0-2.8-1.2-2.8-2.8S19.1,37.6,20.6,37.6L20.6,37.6z M30.3,38.7c2.8,0,5,2.2,5,5c0,2.8-2.2,5-5,5c-2.8,0-5-2.2-5-5C25.3,41,27.6,38.7,30.3,38.7L30.3,38.7z M34.4,30.8c1.5,0,2.8,1.2,2.8,2.8c0,1.5-1.2,2.8-2.8,2.8c-1.5,0-2.8-1.2-2.8-2.8C31.6,32,32.9,30.8,34.4,30.8L34.4,30.8z\"/>"
              ."<path style=\"fill-rule:evenodd;clip-rule:evenodd;fill:#BC4D02;\" d=\"M27.1,25.5c2,0,3.7-1.7,3.7-3.7c0-2-1.7-3.7-3.7-3.7c-2,0-3.7,1.7-3.7,3.7C23.4,23.8,25.1,25.5,27.1,25.5L27.1,25.5z\"/>"
              ."<path style=\"fill-rule:evenodd;clip-rule:evenodd;fill:#BC4D02;\" d=\"M29,15.3c2.7,0,4.8-2.2,4.8-4.8c0-2.7-2.2-4.8-4.8-4.8s-4.8,2.2-4.8,4.8C24.2,13.2,26.4,15.3,29,15.3L29,15.3z\"/>"
              ."<path style=\"fill-rule:evenodd;clip-rule:evenodd;fill:#FCE2A2;\" d=\"M15.3,28.3C10.4,37,5,46.9,3.1,50.6C3,50.8,3,50.9,3,51.1c0,0.4,0.2,0.8,0.5,1.2c0.4,0.5,1,0.9,1.6,1.2c0.7,0.4,1.5,0.6,2.3,0.6h42.7c0.7,0,1.5-0.2,2.3-0.6c0.6-0.3,1.2-0.7,1.6-1.2c0.3-0.4,0.5-0.8,0.5-1.2c0-0.2,0-0.3-0.1-0.5c-1.2-2.4-3.9-7.2-7.1-12.6c-3.4-5.7-7.3-12.2-10.7-17.6L36.4,20v-0.4v-9.5V8.6h1.5h2.5c0.3,0,0.7-0.1,0.9-0.4c0.2-0.2,0.4-0.5,0.4-0.9V4.2c0-0.3-0.1-0.7-0.4-0.9C41,3.1,40.7,3,40.4,3H15.9c-0.3,0-0.7,0.1-0.9,0.4c-0.2,0.2-0.4,0.5-0.4,0.9v3.1c0,0.3,0.1,0.7,0.4,0.9c0.2,0.2,0.5,0.4,0.9,0.4h2.5H20v1.5v9.5v0.4l-0.2,0.4C17.9,23.6,16.6,25.9,15.3,28.3L15.3,28.3z M17.1,31.6c1.9,0.9,4.5,1.7,7.7,1.3c3.2-0.4,4.8-3.7,7.2-5.8c1.5-1.1,2.7-2.2,4.3-2.9l15.1,28L6,52.2L17.1,31.6z M23.4,21.8c0-2,1.7-3.7,3.7-3.7s3.7,1.7,3.7,3.7c0,2-1.7,3.7-3.7,3.7S23.4,23.8,23.4,21.8L23.4,21.8zM29,15.3c-2.7,0-4.8-2.2-4.8-4.8c0-2.7,2.2-4.8,4.8-4.8c2.7,0,4.8,2.2,4.8,4.8C33.9,13.2,31.7,15.3,29,15.3L29,15.3z\"/>"
              ."<path style=\"fill-rule:evenodd;clip-rule:evenodd;fill:#FCE2A2;\" d=\"M20.6,37.6c-1.5,0-2.8,1.2-2.8,2.8s1.2,2.8,2.8,2.8c1.5,0,2.8-1.2,2.8-2.8S22.2,37.6,20.6,37.6L20.6,37.6z\"/>"
              ."<path style=\"fill-rule:evenodd;clip-rule:evenodd;fill:#FCE2A2;\" d=\"M30.3,38.7c-2.8,0-5,2.2-5,5c0,2.8,2.2,5,5,5c2.8,0,5-2.2,5-5C35.3,41,33.1,38.7,30.3,38.7L30.3,38.7z\"/>"
              ."<path style=\"fill-rule:evenodd;clip-rule:evenodd;fill:#FCE2A2;\" d=\"M31.6,33.5c0,1.5,1.2,2.8,2.8,2.8c1.5,0,2.8-1.2,2.8-2.8c0-1.5-1.2-2.8-2.8-2.8C32.9,30.8,31.6,32,31.6,33.5L31.6,33.5z\"/>"
              ."</svg>Пройти тест</button>";
              $script =
              "jQuery(document).ready(function(){"
                  ."jQuery('<div style=\"float: right;\">$button</div>').insertAfter('.fb_topBar .tb_social');"
              ."});";
              $doc->addScriptDeclaration($script);*/

            $additionalMetaTag = '<meta name="viewport" content="width=device-width; initial-scale=1; minimal-ui">';
            $doc->addCustomTag($additionalMetaTag);
        }
        if (JRequest::getVar('option') == 'com_easysocial' && JRequest::getVar('view') == 'profile' && $app->isSite()) {
            JFactory::getApplication()->redirect(JRoute::_("index.php?option=com_plot&view=profile&id=" . JRequest::getInt('id'), false, -1));
        }

    }


    function onBeforeCompileHead()
    {

        $jinput = JFactory::getApplication()->input;
        $option = $jinput->get('option', '');
        $view = $jinput->get('view', '');
        $task=$jinput->get('task', '');
        $document = JFactory::getDocument();
        if (JFactory::getApplication()->isAdmin()) {


            $style = $this->prepareAdminStyle();
            $script = $this->prepareAdminScript();
            $document->addStyleDeclaration($style);
            $document->addScriptDeclaration($script);

            if ($option == 'com_joomlaquiz' && ($view == 'questions' || $view == 'quizzes')) {
                $quizstyle = $this->prepareAdminQuizStyle();
                $quizscript = $this->prepareAdminQuizScript();
                $document->addStyleDeclaration($quizstyle);
                $document->addScriptDeclaration($quizscript);
            }
        }else{

            if ($option == 'com_joomblog' || ($option == 'com_content' && $task='view' && $view='default')){
                $buffer =$document->getBuffer('component');
                $plugin			= JPluginHelper::getPlugin('content', 'accordionfaq');
                $pluginParams 	= new JRegistry;
                $pluginParams->loadString($plugin->params);
                $fline='{accordionfaq faqid=accordion3 faqclass="lightnessfaq defaulticon headerbackground headerborder contentbackground contentborder round5" active=item1}';
                $this->_doOutput( $buffer, $pluginParams, $fline);

            }

        }

        //die(var_dump($_REQUEST));

    }



    protected function _doOutput( &$article, &$params, &$faqline )
    {
        $html = "";
        $document = JFactory::getDocument();

        $cssfile = "plugins/content/accordionfaq/css/accordionfaq.css";
        $faqlinks = $params->get("faqlinks","");

        if (strtolower($faqlinks) == "space")
        {
            $faqlinks = "&nbsp;";
        }
        if (! $this->_editParamValue( "faqid", $params, "accordion1", $faqid, $html))
        {
             $this->_formattedError( $html, $faqline );
        }
        if (! $this->_editParamValue( "faqclass", $params, "lightnessfaq defaulticon headerbackground headerborder contentbackground contentborder round5", $faqclass, $html))
        {
            $this->_formattedError( $html, $faqline );
        }
        $header = $params->get("header",'h3');
        if (! $this->_editTrueFalse( "autoheight", $params, "0", $autoheight, $html))
        {
            $this->_formattedError( $html, $faqline );
        }
        if (! $this->_editTrueFalse( "autonumber", $params, "0", $autonumber, $html))
        {
            $this->_formattedError( $html, $faqline );
        }
        if (! $this->_editTrueFalse( "alwaysopen", $params, "0", $alwaysopen, $html))
        {
           $this->_formattedError( $html, $faqline );
        }
        if (! $this->_editTrueFalse( "openmultiple", $params, "0", $openmultiple, $html))
        {
            $this->_formattedError( $html, $faqline );
        }
        if (! $this->_editTrueFalse( "scrollonopen", $params, "0", $scrollonopen, $html))
        {
             $this->_formattedError( $html, $faqline );
        }
        if (! $this->_editTrueFalse( "warnings", $params, "1", $warnings, $html))
        {
             $this->_formattedError( $html, $faqline );
        }
        if (! $this->_editTrueFalse( "usedynamiccssload", $params, "1", $usedynamiccssload, $html))
        {
            $this->_formattedError( $html, $faqline );
        }
        if (!$this->_editTrueFalse( "keyaccess", $params, "1", $keyaccess, $html))
        {
           $this->_formattedError( $html, $faqline );
        }
        if (! $this->_editEvent( "event", $params, "click", $event, $html))
        {
           $this->_formattedError( $html, $faqline );
        }
        if (! $this->_editAnimation( "animation", $params, "none", $animation, $html))
        {
            $this->_formattedError( $html, $faqline );
        }
        $active = $params->get("active","");
        if (! $this->_editNumeric( "scrolltime", $params, 1000, $scrolltime, $html))
        {
            $this->_formattedError( $html, $faqline );
        }
        $scrolltime = max( $scrolltime, 1);
        if (! $this->_editNumeric( "scrolloffset", $params, 0, $scrolloffset, $html))
        {
            $this->_formattedError( $html, $faqline );
        }

        $activearr[0] = "false";
        $faqitem = JRequest::getString( 'faqitem', '' );

        if ( $active != "" && $faqitem == "")
        {
            $activearr = explode(',',$active );
            for( $i = 0; $i < count($activearr); $i++)
            {
                $activearr[$i] = JString::trim( $activearr[$i]);
                if (! is_numeric($activearr[$i]))
                {
                    $activearr[$i] = "'#".$activearr[$i]."'";
                }
            }
        }
        if ($faqitem != "")
        {
            $faqitems = explode(',', $faqitem);
            $i = 0;
            do
            {
                $faqitems[$i] = JString::trim( $faqitems[$i] );
                if (JString::strpos( $faqitems[$i], $faqid ) === 0)
                {
                    $faqitemtarget = JString::substr( $faqitems[$i], JString::strlen( $faqid ));
                    if ($faqitemtarget != "")
                    {
                        if (is_numeric($faqitemtarget))
                        {
                            $faqitemid = (int)$faqitemtarget;
                            $activearr[$i] = $faqitemid;
                            $jumpto = $faqitems[$i];
                        }
                        else
                        {
                            $activearr[$i] = "'#".$faqitemtarget."'";
                            $jumpto = $faqitemtarget;
                        }
                    }
                    else
                    {
                        $jumpto = $faqitem;
                    }
                }
                $i++;
            }
            while(! isset($jumpto) && $i < count($faqitems) );
        }

        $printfaq = JRequest::getString( "print", 'false');
        if ($printfaq == "1")
        {
            if (isset( $jumpto ))
            {
                unset( $jumpto );
            }
            $animation = 'false';
            $printfaq = 'true';
        }
        else
        {
            $printfaq = 'false';
        }
        $browser	= JBrowser::getInstance();
        $isIE6		= false;

        if ($browser->getBrowser() == "msie" && $browser->getMajor() <= 6)
        {
            $isIE6 = true;
            $ie6css = JPATH_BASE . DIRECTORY_SEPARATOR . $cssfile;
            $ie6css = JPath::clean( $ie6css, DIRECTORY_SEPARATOR );
            if (JString::substr($ie6css, JString::strlen( $ie6css ) - 4, 4) == '.css')
            {
                $ie6css = JString::substr_replace($ie6css, '-ie6.css', JString::strlen( $ie6css ) - 4, 4 );
            }
            if (JFile::exists($ie6css))
            {
                $styledata = JFile::read($ie6css);
                $newtext = preg_replace( "/src=( )*'( )*([^' ]+)'/i", "src='" . JURI::root(true) . "\\3" . "'", $styledata );
                $newtext = preg_replace( "/url\(( )*'( )*([^' ]+)'/i", "url('" . JURI::root(true) . "\\3" . "'", $newtext );
                $document->addStyleDeclaration($newtext);
            }
        }
        $document->addStyleSheet( JURI::root(true)."/".$cssfile );

        $cssbase = JPATH_BASE . DIRECTORY_SEPARATOR . $cssfile;
        $cssbase = JPath::clean( $cssbase, DIRECTORY_SEPARATOR );
        $cssfilename = JFile::getName( $cssbase );
        $cssbase = str_replace( $cssfilename, '', $cssbase );
        $faqbase = str_replace( $cssfilename, '', $cssfile );
        $faqclassarray = preg_split( "/[\s]+/", $faqclass );
        for($i = 0; $i < count($faqclassarray); $i++)
        {
            if (preg_match("/(.*)faq$/i", $faqclassarray[$i], $match ))
            {
                $faqfile = $cssbase.$faqclassarray[$i].".css";
                if (JFile::exists($faqfile))
                {
                    if ($usedynamiccssload == 'tru')
                    {
                        $document->addStyleSheet( JURI::root(true)."/".$faqbase."css.php?id=".$faqid."&amp;faq=".$faqclassarray[$i] );
                    }
                    else
                    {
                        $document->addStyleSheet( JURI::root(true)."/".$faqbase.$faqclassarray[$i].".css" );
                    }
                    if ($isIE6)
                    {
                        $ie6css = $cssbase.$faqclassarray[$i]."-ie6.css";
                        if (JFile::exists($ie6css))
                        {
                            $styledata = JFile::read($ie6css);
                            $newtext = preg_replace( "/src=( )*'( )*([^' ]+)'/i", "src='" . JURI::root(true) . "\\3" . "'", $styledata );
                            $newtext = preg_replace( "/url\(( )*'( )*([^' ]+)'/i", "url('" . JURI::root(true) . "\\3" . "'", $newtext );
                            $newtext = preg_replace( "/\.".$faqclassarray[$i]."/", "#".$faqid.".".$faqclassarray[$i], $newtext );
                            $document->addStyleDeclaration($newtext);
                        }
                    }
                }
                else
                {
                    if ($warnings == 'tru')
                    {
                        $warntext = "WARNING: CSS file for faqclass ".$faqclassarray[$i]." does not exist (".$faqfile.").";
                        $html .= $this->_formattedError( $warntext, $faqline );
                    }
                }
            }
            else
            {
                if ($warnings == 'tru')
                {
                    if (! $this->_editFaqClass( $faqclassarray[$i], $warntext))
                    {
                        $html .= $this->_formattedError( $warntext, $faqline );
                    }
                }
            }
        }

        $includejquery = $params->get('includejquery', 1);
        if ($includejquery != 0)
        {
            $jquerynoconflict = $params->get('jquerynoconflict', 1);
            if ($jquerynoconflict == 1)
            {
                JHTML::_('jquery.framework' );
            }
            else
            {
                JHTML::_('jquery.framework', false );
            }
        }

        JHTML::_('script', 'plugins/content/accordionfaq/js/preparefaq.js' );
        if ($openmultiple == 'tru' && $animation !== 'false')
        {
            $duration = 300;
            $easing = 'swing';
            if ($animation == "'slide'")
            {
                $duration = 300;
                $easing = 'swing';
            }
            else
                if ($animation == "'easeslide'")
                {
                    $duration = 700;
                    $easing = 'easeinout';
                }
                else
                    if ($animation == "'bounceslide'")
                    {
                        $duration = 1000;
                        $easing = 'bounceout';
                    }
        }

        $script  = "// <!--\n";
        $script .= "preparefaq.onFunctionAvailable( 'jQuery', 300, function() {\n";
        $script .= "	preparefaq.setjQuery();\n";
        if ($printfaq === 'false' && $openmultiple == 'fals')
        {
            $script .= "	preparefaq.loadScript( '". JURI::root(true) . "/plugins/content/accordionfaq/js/jquery.accordionfaq.js' );\n";
        }
        if ($animation !== "false")
        {
            $script .= "	preparefaq.loadScript( '". JURI::root(true) . "/plugins/content/accordionfaq/js/jquery.easing.js' );\n";
        }
        $script .= "/***********************************************\n";
        $script .= "* Scrolling HTML bookmarks- © Dynamic Drive DHTML code library (www.dynamicdrive.com)\n";
        $script .= "* This notice MUST stay intact for legal use\n";
        $script .= "* Visit Project Page at http://www.dynamicdrive.com for full source code\n";
        $script .= "***********************************************/\n";
        $script .= "	preparefaq.loadScript( '". JURI::root(true) . "/plugins/content/accordionfaq/js/bookmarkscroll.js' );\n";
        $script .= "	preparefaq.getjQuery()(document).ready(function(){ \n";
        $script .= "		preparefaq.exec( { \n";
        $script .= "		    id: '".$faqid."'\n";
        $script .= "		  , header: '".$header."'\n";
        $script .= "		  , alwaysopen: ".$alwaysopen."e\n";
        $script .= "		  , autonumber: ".$autonumber."e\n";
        $script .= "		  , keyaccess: ".$keyaccess."e\n";
        $script .= "		  , print: ".$printfaq."\n";
        $script .= "		  , scrolltime: ".$scrolltime."\n";
        $script .= "		  , scrolloffset: ".$scrolloffset."\n";
        if ($faqlinks != "")
        {
            $script .= "		  , faqlinks: '".$faqlinks."'\n";
        }
        $script .= "		  , scrollonopen: ".$scrollonopen."e\n";
        if ($openmultiple == 'tru')
        {
            $script .= "		  , event: '".$event."'\n";
            $script .= "		  , onevent: function() { \n";
            if ($animation === 'false')
            {
                $script .= "				preparefaq.getjQuery()(this).toggleClass('selected').next().toggle( 1, preparefaq.accordionChange );\n";
            }
            else
            {
                $script .= "				preparefaq.getjQuery()(this).toggleClass('selected').next().slideToggle( ".$duration.", '".$easing."', preparefaq.accordionChange );\n";
            }
            $script .= "				return true;\n";
            $script .= "			}\n";
        }
        $script .= "		} );\n";
        $script .= "		preparefaq.onFunctionAvailable( 'bookmarkscroll.init', 300, function() {\n";
        $script .= "				bookmarkscroll.init();\n";
        $script .= "		});\n";
        if ($openmultiple == 'tru')
        {
            $script .= "		preparefaq.getjQuery()('#".$faqid."').addClass('selected');\n";
            if ($activearr[0] !== 'false' && $printfaq === 'false')
            {
                if (count($activearr) == 1 && $activearr[0] == "'#*'")
                {
                    $script .= "		preparefaq.getjQuery()('".$header.".accordionfaqheader.".$faqid."').toggleClass('selected').next().toggle();\n";
                }
                else
                {
                    $script .= "		var target;\n";
                    for ( $i = 0; $i < count($activearr); $i++)
                    {
                        if (is_numeric($activearr[$i]))
                        {
                            $script .= "		target = preparefaq.getjQuery()('#".$faqid.$activearr[$i]."');\n";
                        }
                        else
                        {
                            $activeval = str_replace( "'", '', $activearr[$i] );
                            $script .= "		target = preparefaq.getjQuery()('".$activeval."');\n";
                        }
                        $script .= "		if (typeof(target) !== 'undefined') {\n";
                        $script .= "			target.toggleClass('selected').next().toggle();\n";
                        $script .= "		};\n";
                    }
                }
            }
        }
        else
        {
            $script .= "		preparefaq.onFunctionAvailable( 'preparefaq.getjQuery().fn.accordionfaq', 300, function() {\n";
            $script .= "			preparefaq.getjQuery()('#".$faqid."').accordionfaq( { \n";
            $script .= "				  header: '".$header.".accordionfaqheader.".$faqid."'\n";
            $script .= "				, autoheight: ".$autoheight."e\n";
            $script .= "				, alwaysOpen: ".$alwaysopen."e\n";
            $script .= "				, active: ".$activearr[0]."\n";
            $script .= "			 	, animated: ".$animation."\n";
            $script .= "			 	, event: '".$event."'\n";
            $script .= "			});\n";
            $script .= "			preparefaq.getjQuery()('#".$faqid."').bind( 'change.faq-accordion', preparefaq.accordionChangeUI );\n";
            $script .= "		});\n";
        }
        if (isset($jumpto))
        {
            $script .= "		preparefaq.onIdAvailable( '".$jumpto."', 300, function() {\n";
            $script .= "			preparefaq.onFunctionAvailable( 'bookmarkscroll.scrollTo', 300, function() {\n";
            $script .= "				preparefaq.jumpToFaqItem( '".$jumpto."' );\n";
            $script .= "			});\n";
            $script .= "		});\n";
        }
        $script .= "	});\n";
        $script .= "});\n";
        $script .= "// -->\n";
        $document->addScriptDeclaration( $script );

        $html 	.= "<div id=\"".$faqid."\" class=\"accordionfaq ".$faqclass."\">";
        $html 	.= "<p></p>";
        $html 	.= "</div>";
        return $html;
    }

    protected function _editNumeric( $paramname, &$params, $default, &$value, &$errortext )
    {
        $paramvalue = $params->get( $paramname, $default );
        if (is_numeric($paramvalue))
        {
            $value = (int)$paramvalue;
            return true;
        }
        $errortext = "ERROR: Parameter ".$paramname."='".$paramvalue."' is invalid. ";
        $errortext .= "'".$paramvalue."' must be a number.";
        return false;
    }

    protected function _editTrueFalse( $paramname, &$params, $default, &$value, &$errortext )
    {
        $valid['true'] = "tru";
        $valid['false'] = "fals";
        $valid['yes'] = "tru";
        $valid['no'] = "fals";
        $valid['on'] = "tru";
        $valid['off'] = "fals";
        $valid['t'] = "tru";
        $valid['f'] = "fals";
        $valid['y'] = "tru";
        $valid['n'] = "fals";
        $valid['1'] = "tru";
        $valid['0'] = "fals";
        return $this->_editParam( $paramname, $params, $default, $valid, $value, $errortext );
    }

    protected function _editEvent( $paramname, &$params, $default, &$value, &$errortext )
    {
        $valid['click'] = "click";
        $valid['dblclick'] = "dblclick";
        $valid['mouseover'] = "mouseover";
        return $this->_editParam( $paramname, $params, $default, $valid, $value, $errortext );
    }

    protected function _editParamNames( &$params, &$errortext )
    {

        $valid['faqid'] = "1";
        $valid['cssfile'] = "1";
        $valid['includejquery'] = "1";
        $valid['jquerynoconflict'] = "1";
        $valid['faqclass'] = "1";
        $valid['header'] = "1";
        $valid['autonumber'] = "1";
        $valid['autoheight'] = "1";
        $valid['alwaysopen'] = "1";
        $valid['active'] = "1";
        $valid['animation'] = "1";
        $valid['event'] = "1";
        $valid['scrolltime'] = "1";
        $valid['scrolloffset'] = "1";
        $valid['warnings'] = "1";
        $valid['usedynamiccssload'] = "1";
        $valid['faqlinks'] = "1";
        $valid['keyaccess'] = "1";
        $valid['openmultiple'] = "1";
        $valid['scrollonopen'] = "1";
        $paramnamesarray = (array)$params;
        $paramnames = array_keys( $paramnamesarray );
        foreach( $paramnames as $paramname)
        {
            if (! isset( $valid[strtolower($paramname)]))
            {
                $validvalues = array_keys( $valid );
                $validlist = implode( $validvalues, ", ");
                $errortext = "ERROR: Parameter '".$paramname."' is invalid. ";
                $errortext .= "Acceptable values for '".$paramname."' are ".$validlist.".";
                return false;
            }
        }
        return true;
    }

    protected function _editParamValue( $paramname, &$params, $default, &$value, &$errortext )
    {
        $value = $params->get( $paramname, $default );
        if ( strlen( $value ) != strcspn( $value, "\"|'!~`@#$%^&()+{}[]\\/?<>;:.,?"))
        {
            $errortext = "ERROR: Value for ".$paramname." parameter '".$value."' is invalid.";
            return false;
        }
        return true;
    }

    protected function _editFaqClass( $classname, &$errortext )
    {
        $valid['round3'] = "1";
        $valid['round5'] = "1";
        $valid['round7'] = "1";
        $valid['round9'] = "1";
        $valid['headerbackground'] = "1";
        $valid['headerborder'] = "1";
        $valid['contentbackground'] = "1";
        $valid['contentborder'] = "1";
        $valid['border'] = "1";
        $valid['bcolor'] = "1";
        $valid['defaulticon'] = "1";
        $valid['onoff'] = "1";
        $valid['plus'] = "1";
        $valid['plus2'] = "1";
        $valid['plus3'] = "1";
        $valid['arrow'] = "1";
        $valid['greenarrow'] = "1";
        $valid['orangearrow'] = "1";
        $valid['orangearrow2'] = "1";
        $valid['help'] = "1";
        $valid['help2'] = "1";
        $valid['power'] = "1";
        $valid['check'] = "1";
        $valid['rtl'] = "1";
        $valid['alignright'] = "1";
        $valid['alignleft'] = "1";
        $valid['aligncenter'] = "1";
        if (! isset( $valid[strtolower($classname)]))
        {
            $validvalues = array_keys( $valid );
            $validlist = implode( $validvalues, ", ");
            $errortext = "WARNING: faqclass '".$classname."' is invalid. ";
            $errortext .= "Acceptable values for '".$classname."' are ".$validlist.".";
            return false;
        }
        return true;
    }

    protected function _editAnimation( $paramname, &$params, $default, &$value, &$errortext )
    {
        $valid['none'] = "false";
        $valid['slide'] = "'slide'";
        $valid['easeslide'] = "'easeslide'";
        $valid['bounceslide'] = "'bounceslide'";
        return $this->_editParam( $paramname, $params, $default, $valid, $value, $errortext );
    }

    protected function _editParam( $paramname, &$params, $default, &$valid, &$value, &$errortext )
    {
        $paramvalue = $params->get( $paramname, $default );
        if (isset($valid[strtolower($paramvalue)]))
        {
            $value = $valid[strtolower($paramvalue)];
            return true;
        }
        $validvalues = array_keys( $valid );
        $validlist = implode( $validvalues, ", ");
        $errortext = "ERROR: Parameter ".$paramname."='".$paramvalue."' is invalid. ";
        $errortext .= "Acceptable values for '".$paramvalue."' are ".$validlist.".";
        return false;
    }

    protected function _formattedError( $errortext, $faqline )
    {
        $text = "<p style=\"color: red;background-color: yellow;visibility: visible\">\n";
        $text .= "<b>".htmlspecialchars($faqline)."<br/>";
        $text .= "Accordionfaq plugin,  ".$errortext."</b>";
        $text .= "</p>\n";
        return $text;
    }






    function prepareAdminStyle()
    {
        ob_start();
        ?>
        .chzn-container .chzn-results{
<!--        max-height:100%!important;-->
        }

        #tm-navbar{
        float: right;
        }
        <?php
        $content = ob_get_clean();
        return $content;
    }

    function prepareAdminScript()
    {
        ob_start();
        ?>
        jQuery(document).ready(function () {

        jQuery('#tm-navbar').addClass('span10');
        });
        <?php
        $content = ob_get_clean();
        return $content;
    }

    function prepareAdminQuizStyle()
    {
        ob_start();
        ?>


        #jp-navbar{
        float: right;
        }
        <?php
        $content = ob_get_clean();
        return $content;
    }

    function prepareAdminQuizScript()
    {
        ob_start();
        ?>
        jQuery(document).ready(function () {
        jQuery('#jp-navbar').addClass('span10');
        });
        <?php
        $content = ob_get_clean();
        return $content;
    }

}

