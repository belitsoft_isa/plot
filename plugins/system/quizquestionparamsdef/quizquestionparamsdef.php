<?php
/**
 * Attachments plugin for inserting attachments lists into content
 *
 * @package     Attachments
 * @subpackage  Main_Attachments_Plugin
 *
 * @author      Jonathan M. Cameron <jmcameron@jmcameron.net>
 * @copyright   Copyright (C) 2007-2013 Jonathan M. Cameron, All Rights Reserved
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://joomlacode.org/gf/project/attachments/frs/
 */

defined('_JEXEC') or die('Restricted access');


/**
 * Attachments plugin
 *
 * @package  Attachments
 * @since    1.3.4
 */
class plgSystemQuizquestionparamsdef extends JPlugin
{

    function onBeforeCompileHead()
    {
        if (JFactory::getApplication()->isAdmin()) {
            $jinput = JFactory::getApplication()->input;
            $option = $jinput->get('option', '');
            $view = $jinput->get('view', '');
            $layout = $jinput->get('layout', '');
            $c_id = $jinput->get('c_id', 0, 'INT');
            if ($option == 'com_joomlaquiz' && $view == 'question' && $layout == 'edit' && $c_id == 0) {
                $document = JFactory::getDocument();
                $script = $this->prepareScript();
                $document->addScriptDeclaration($script);
            }
        }
    }

    function prepareScript()
    {

        ob_start();
        ?>
        jQuery(document).ready(function () {
        var c_separator=document.getElementById('jform_c_separator'),
            c_random=document.getElementById('jformc_random');
            jQuery(c_separator).find("option[value='0']").removeAttr('selected');
            jQuery(c_separator).find("option[value='1']").attr('selected','selected');
            jQuery(c_separator).trigger('liszt:updated');

            jQuery(c_random).find("option[value='0']").removeAttr('selected');
            jQuery(c_random).find("option[value='1']").attr('selected','selected');
            jQuery(c_random).trigger('liszt:updated');

        });
    <?php
        $content = ob_get_clean();
        return $content;
    }
}
