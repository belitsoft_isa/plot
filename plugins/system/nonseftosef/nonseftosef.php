<?php
/**
 *  @Copyright
 *  @package     NSTS - Non-SEF to SEF
 *  @author      Viktor Vogel {@link http://www.kubik-rubik.de}
 *  @version     3-2 - 2013-11-10
 *  @link        http://joomla-extensions.kubik-rubik.de/
 *
 *  @license GNU/GPL
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') or die('Restricted access');

class PlgSystemNonseftosef extends JPlugin
{
    protected $_app;

    function __construct(&$subject, $config)
    {
        // Redirection in the backend would be a stupid idea...
        $this->_app = JFactory::getApplication();

        if($this->_app->isAdmin())
        {
            return;
        }

        parent::__construct($subject, $config);
    }

    // Call the plugin as soon as possible to avoid unneccessary workload
    function onAfterInitialise()
    {
        // Execute the plugin only if SEF is activated
        if(JFactory::getConfig()->get('sef') == 1)
        {
            // Only manipulate HTML document requests to avoid errors in other document types such as JSON or XML
            if(JFactory::getDocument() instanceof JDocumentHTML)
            {
                $query_parameters = JFactory::getURI()->getQuery(true);

                if(array_key_exists('option', $query_parameters))
                {
                    // Is the option request variable a component call (as it is usually)?
                    if(strtolower(substr($query_parameters['option'], 0, 4) == 'com_'))
                    {
                        // Should we exclude some components of being redirected?
                        $exclude_components = array_filter(array_map('trim', explode("\n", $this->params->get('exclude_components'))));

                        if(!empty($exclude_components))
                        {
                            // If we have a hit, then stop the execution of the plugin
                            if(in_array($query_parameters['option'], $exclude_components))
                            {
                                return;
                            }
                        }
                        
                        if ( $_SERVER['REQUEST_METHOD'] != 'POST' ) {
                            $this->_app->redirect(JRoute::_(str_replace(JFactory::getURI()->base(), '', JFactory::getURI()->tostring())));
                        }
                        
                    }
                }
            }
        }
    }

}
