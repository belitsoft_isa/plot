<?php
/**
 * Attachments plugin for inserting attachments lists into content
 *
 * @package     Attachments
 * @subpackage  Main_Attachments_Plugin
 *
 * @author      Jonathan M. Cameron <jmcameron@jmcameron.net>
 * @copyright   Copyright (C) 2007-2013 Jonathan M. Cameron, All Rights Reserved
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://joomlacode.org/gf/project/attachments/frs/
 */

defined('_JEXEC') or die('Restricted access');



/**
 * Attachments plugin
 *
 * @package  Attachments
 * @since    1.3.4
 */
class plgSystemParentaddbackbutton extends JPlugin
{

    protected $autoloadLanguage = true;


    function onBeforeCompileHead()
    {
        if (!JFactory::getApplication()->isAdmin()) {
            $jinput = JFactory::getApplication()->input;
            $option = $jinput->get('option', '');
            $document = JFactory::getDocument();

            if($option=='com_joomlaquiz' && plotUser::factory()->isParent()){
                $script = $this->prepareScript();
                $document->addScriptDeclaration($script);
            }
        }
    }

    function prepareScript()
    {
        $book_id=(int)$this->getBookIdByQuizId();
        ob_start();
        ?>

        jQuery(document).ready(function () {
        <?php if($book_id){ ?>
            var str='',
            link='<?php echo  JRoute::_("index.php?option=com_plot&view=publication&bookId=".$book_id)?>';
                str+='<a href="'+link+'" class="link-back">';
                str+='<?php echo  JText::_("COM_PLOT_GO_BACK"); ?>';
                str+='</a>';

            jQuery('.parent-txt article.text-field .joomlaquiz_container').before(str)

        <?php } ?>
        });



        <?php
        $content = ob_get_clean();
        return $content;
    }

    function getBookIdByQuizId(){
        $jinput = JFactory::getApplication()->input;
        $quiz_id = $jinput->get('quiz_id', 0, 'INT');
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('id_pub')
            ->from('#__plot_book_quiz_map')
            ->where('id_quiz=' .$quiz_id);

        $result = $db->setQuery($query)->loadResult();
        return $result;
    }

}
