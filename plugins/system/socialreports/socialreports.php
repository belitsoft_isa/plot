<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

class plgSystemSocialreports extends JPlugin
{

    function onAfterInitialise()
    {
        if (JFactory::getApplication()->isAdmin()) {
            $jinput = JFactory::getApplication()->input;
            $option = $jinput->get('option', '');
            $view = $jinput->get('view', '');
            if ($option == 'com_easysocial' && $view == 'reports') {
                $doc = JFactory::getDocument();
                $doc->addScriptDeclaration(
                    "jQuery(document).ready(function(){" .
                    "var adminForm=jQuery('#adminForm').find('tbody').find('tr');" .
                    "jQuery(adminForm).each(function() {" .
                    "var data_uid=jQuery(this).attr('data-uid')," .
                    "second_td=jQuery(this).find('td:nth-child(2)').find('div:first-child').find('a').attr('href', '/index.php?option=com_plot&view=profile&id='+data_uid);" .
                    "});" .
                    "});"
                );
            }
        }
    }

}

?>
