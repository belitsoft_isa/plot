<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

class plgSystemQuizRemoveBackButton extends JPlugin
{

    function onAfterInitialise()
    {
        $task = JFactory::getApplication()->input->get('task', '');
        $option = JFactory::getApplication()->input->get('option', '');
        $view = JFactory::getApplication()->input->get('view', '');
        $quiz_id = JFactory::getApplication()->input->get('quiz_id', 0);
        $my = plotUser::factory();
        if (!JFactory::getApplication()->isAdmin()) {

            $doc = JFactory::getDocument();

            if ($view == 'quiz' && $quiz_id) {

                $doc->addScriptDeclaration(
                    "jQuery(document).ready(function(){" .
                    "var backButton=document.getElementsByClassName('link-back'),
                         observeDOM = (function(){
                                        var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
                                        eventListenerSupported = window.addEventListener;

                                        return function(obj, callback){
                                                    if( MutationObserver ){
                                                         // define a new observer
                                                        var obs = new MutationObserver(function(mutations, observer){
                                                            if( mutations[0].addedNodes.length || mutations[0].removedNodes.length )
                                                                 callback();
                                                         });
                                                        // have the observer observe foo for changes in children
                                                        obs.observe( obj, { childList:true, subtree:true });
                                                    }
                                                    else if( eventListenerSupported ){
                                                            obj.addEventListener('DOMNodeInserted', callback, false);
                                                            obj.addEventListener('DOMNodeRemoved', callback, false);
                                                    }
                                        }
                    })();

                    // Observe a specific DOM element:
                    observeDOM( document.getElementById('jq_quiz_container_tbl') ,function(){
                       var resultsTable=jQuery('.jq_results_container');
                            if(resultsTable!==null && resultsTable.length){
                                jQuery(backButton).show();
                            }
                    });

                    if(backButton!== null){
                        jQuery('.jq_quiz_task_container').live('click', function(){
                            jQuery(backButton).hide();
                        });
                    }
                    });"

                );


            }
        }
    }


}

?>
