<?php

defined('_JEXEC') or die('Restricted access');

class plgSystemPoints extends JPlugin
{

    function onAfterInitialise()
    {

        if (JFactory::getApplication()->isAdmin()) {
            $jinput = JFactory::getApplication()->input;
            $option = $jinput->get('option', '');
            $view = $jinput->get('view', '');
            if ($option == "com_easysocial" && $view == "points") {
                $doc = JFactory::getDocument();

                $doc->addScriptDeclaration(
                    "jQuery(document).ready(function(){ " .
                    "var table=jQuery('#pointsTable tbody tr td:nth-child(7)');" .
                    "table.each(function() {".
                        "if(jQuery(this).text()==37 || jQuery(this).text()==38){".
                            "jQuery(this).prev('td').prev('td').prev('td').html('')".
                        "}".
                    "});  " .
                    "});"
                );
            }
        }

    }


}

?>
