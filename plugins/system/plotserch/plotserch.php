<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

class plgSystemPlotserch extends JPlugin
{

    function onAfterInitialise()
    {
        $option=JRequest::getVar('option');
        $view=JRequest::getVar('view');

       if($option=='com_plot' && $view=='search'){
           JFactory::getApplication()->setUserState( "com_plot.children_offset",0);
           JFactory::getApplication()->setUserState( "com_plot.adults_offset",0);
           JFactory::getApplication()->setUserState( "com_plot.books_offset",0);
           JFactory::getApplication()->setUserState( "com_plot.courses_offset",0);
           JFactory::getApplication()->setUserState( "com_plot.events_offset",0);
       }elseif($option=='com_plot' && $view=='photos'){
           JFactory::getApplication()->setUserState( "com_plot.photos_offset",0);
           JFactory::getApplication()->setUserState( "com_plot.videos_offset",0);
           JFactory::getApplication()->setUserState( "com_plot.certificate_offset",0);
       }

    }


}

?>
