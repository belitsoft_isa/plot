<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

class plgSystemQuizpassed extends JPlugin
{

    protected $autoloadLanguage = true;

    public function onAfterInitialise()
    {
        $jq_task = JFactory::getApplication()->input->get('ajax_task', '');
        $option = JFactory::getApplication()->input->get('option', '');
        $quiz_id = intval(JFactory::getApplication()->input->get('quiz', 0));
        $stu_quiz_id = intval(JFactory::getApplication()->input->get('stu_quiz_id', 0));
        $user_unique_id = strval(JFactory::getApplication()->input->get('user_unique_id', ''));
        $database = JFactory::getDBO();
        if ($jq_task == 'finish_stop' && $option == 'com_joomlaquiz') {
            require_once JPATH_BASE . '/components/com_joomlaquiz/helpers/joomlaquiz.php';

            $query = "SELECT * FROM #__quiz_t_quiz WHERE c_id = '" . $quiz_id . "'";
            $database->SetQuery($query);
            $quiz_info = $database->LoadObjectList();

            $user_passed = 0;
            if (count($quiz_info)) {
                $quiz_info = $quiz_info[0];
                $query = "SELECT SUM(c_score) FROM #__quiz_r_student_question WHERE c_stu_quiz_id = '" . $stu_quiz_id . "'";
                $database->SetQuery($query);
                $user_score = $database->LoadResult();
                if (!$user_score) $user_score = 0;
                $query = "SELECT q_chain FROM #__quiz_q_chain "
                    . "\n WHERE s_unique_id = '" . $user_unique_id . "'";
                $database->SetQuery($query);
                $qch_ids = $database->LoadResult();
                $qch_ids = str_replace('*', ',', $qch_ids);
                $max_score = JoomlaquizHelper::getTotalScore($qch_ids, $quiz_id);
                $query = "SELECT 1 FROM #__quiz_t_question AS q, #__quiz_r_student_question AS sq WHERE q.c_id IN (" . $qch_ids . ") AND q.published = 1 AND q.c_manual = 1 AND q.c_id = sq.c_question_id AND sq.c_stu_quiz_id = '" . $stu_quiz_id . "' AND sq.reviewed = 0";
                $database->SetQuery($query);
                $c_manual = (int)$database->LoadResult();

                $nugno_score = ($quiz_info->c_passing_score * $max_score) / 100;

                if (!$c_manual && ($user_score >= $nugno_score)) {
                    $user_passed = 1;
                }
                if ($user_passed) {
                    $this->plot($quiz_id);
                }
            }
        }
    }

    public function plot($quizId)
    {
        require_once(JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/foundry.php');
        require_once(JPATH_ADMINISTRATOR . '/components/com_plot/entities/points.php');
        require_once(JPATH_ADMINISTRATOR . '/components/com_plot/entities/book.php');
        require_once(JPATH_SITE . '/components/com_plot/models/conversations.php');
        $my = plotUser::factory();
        $bookId = $this->getBookId($quizId);
       /* if (!$bookId || $my->isSiteAdmin() || $my->isBookAuthor($bookId)) {
            return false;
        */

        $this->setPlotBookAsReadNow($bookId);
        $bookObj=new plotBook($bookId);
        $buyer=$bookObj->whoBoughtBook($bookId,$my->id );
        $conversationModel =JModelLegacy::getInstance('conversations', 'PlotModel');
        if($my->getSocialFieldData('UNSUBSCRIBE')==1 || $my->getSocialFieldData('UNSUBSCRIBE')==NULL) {
            $conversationModel->sendMessageSystem((int)$my->id, 'plot-book-readed-' . $bookId . '-' . $my->id);
        }
        if(plotUser::factory((int)$buyer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory((int)$buyer)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
            $conversationModel->sendMessageSystem((int)$buyer, 'plot-book-readed-' . $bookId . '-' . $my->id);
        }

        $config = JFactory::getConfig();
        $sender = array(
            $config->get( 'mailfrom' ),
            $config->get( 'fromname' )
        );

        $actor='<a href="'.JRoute::_('index.php?option=com_plot&view=profile&id='.$my->id).'">'.plotUser::factory($my->id)->name.'</a>';
        $target='<a href="'.JRoute::_('index.php?option=com_plot&view=publication&bookId='.$bookId).'">'.$bookObj->c_title.'</a>';


        if(plotUser::factory((int)$buyer)->getSocialFieldData('UNSUBSCRIBE')==1 || plotUser::factory((int)$buyer)->getSocialFieldData('UNSUBSCRIBE')==NULL) {
            $mailer = JFactory::getMailer();
            $mailer->addRecipient(plotUser::factory($buyer)->email);
            $mailer->setSender($sender);
            $mailer->setSubject(JText::_('COM_PLOT_SEND_READED_BOOK_SUBJECT'));
            $mailer->isHTML(true);
            $mailer->setBody(JText::sprintf("COM_PLOT_SEND_READED_BOOK",$actor, $target));
            $mailer->Send();
        }
        if($my->getSocialFieldData('UNSUBSCRIBE')==1 || $my->getSocialFieldData('UNSUBSCRIBE')==NULL) {
            $mailer = JFactory::getMailer();
            $mailer->addRecipient($my->email);
            $mailer->setSender($sender);
            $mailer->setSubject(JText::_('COM_PLOT_SEND_READED_BOOK_SUBJECT'));
            $mailer->isHTML(true);
            $mailer->setBody(JText::sprintf("COM_PLOT_SEND_READED_BOOK",$actor, $target));
            $mailer->Send();
        }


        /*if ($this->isAlreadyPaidForBookReading($bookId)) {
            return false;
        }
        $countPoints = $this->getCountPointsForBookReading($quizId);
        $countMoney = $this->getCountMoneyForBookReading($bookId);
        $this->setBookAsFinished($bookId);
        $my->addMoney($countMoney);
        Foundry::points()->assignCustom($my->id, $countPoints, 'Книга прочитана');
        $this->updatePointsHistory($bookId);*/
    }


    private function getBookId($quiz_id)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true);
        $query->select('a.id_pub');
        $query->from('`#__plot_book_quiz_map` AS a');
        $query->where('a.`id_quiz` = ' . (int)$quiz_id);
        $db->setQuery($query);
        $bookId = (int)$db->loadResult();
        return $bookId;
    }
    
    private function setPlotBookAsReadNow($bookId)
    {
        $db = Foundry::db();


        $query = $db->getQuery(true)
            ->update('`#__plot_books`')
            ->set('`read` =1')
            ->set('`date` =NOW()')
            ->where('`book_id` = ' . (int)$bookId)
            ->where('`child_id` = ' . (int)plotUser::factory()->id);
        $db->setQuery($query)->query();
        return true;
    }
    
    private function isAlreadyPaidForBookReading($bookId)
    {
        $finishedBooks = plotUser::factory()->getFinishedBooks();
        $finishedBooksIds = array();
        foreach ($finishedBooks AS $finishedBook) {
            $finishedBooksIds[] = $finishedBook->book_id;
        }
        if (in_array($bookId, $finishedBooksIds)) {
            return true;
        }
        return false;
    }
    
    private function getCountPointsForBookReading($quizId)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true);
        $query->select('p.count_points');
        $query->from('`#__plot_book_quiz_map` AS a');
        $query->innerJoin('`#__plot_count_points` AS p ON a.id_pub=p.entity_id AND p.entity="book"');
        $query->where('a.`id_quiz` = ' . (int)$quizId);
        $db->setQuery($query);
        $count_points = (int)$db->loadResult();
        return $count_points;
    }
 
    private function getCountMoneyForBookReading($bookId)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true);
        $query->select('finished_price');
        $query->from('`#__plot_books_paid` ');
        $query->where('`child_id` = ' . (int)plotUser::factory()->id);
        $query->where('`book_id`=' . (int)$bookId);
        $db->setQuery($query);
        $count_money = (int)$db->loadResult();
        return $count_money;
    }
    
    private function setBookAsFinished($bookId)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->update('`#__plot_books_paid`')
            ->set('`finished` = 1')
            ->set('`finished_date` = "'.Foundry::date()->toMySQL().'"')
            ->where('`child_id` = ' . (int)plotUser::factory()->id)
            ->where('`book_id`=' . (int)$bookId);
        $db->setQuery($query)->query();
    }
    
    private function updatePointsHistory($bookId)
    {
        $db = Foundry::db();
        $query = $db->getQuery(true);
        $query->select('id');
        $query->from('`#__social_points_history` ');
        $query->where('`points_id` = 0');
        $query->where('`user_id`=' . (int)plotUser::factory()->id);
        $query->where('message="Книга прочитана"');
        $query->order('created DESC');
        $db->setQuery($query);
        $pointsHistory = (int)$db->loadResult();
        if ($pointsHistory) {
            $social_history = (object)array(
                'id' => (int)$pointsHistory,
                'points_id' => 37
            );
            $db->updateObject('#__social_points_history', $social_history, 'id');
            $plotPoints = new stdClass();
            $plotPoints->sph_id = $pointsHistory;
            $plotPoints->entity_id = (int)$bookId;
            $db->insertObject('#__plot_points_history', $plotPoints);
        }
    }
    
}
