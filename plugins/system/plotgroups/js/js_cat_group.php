<?php
defined('_JEXEC') or die;
$jinput = JFactory::getApplication()->input;
$plot = $jinput->get('plot', 0, 'INT');

?>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<script src="<?php echo JUri::root() . '/media/jui/js/jquery.min.js'; ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo JUri::root() . 'plugins/system/plotgroups/js/plotEasysocial.js'; ?>"></script>
<script type="text/javascript">

    jQuery(document).ready(function () {

        //replaceLinkInStream(jQuery('#fd').find('.es-stream-list').find('div.es-stream-title'));

       // replaceMemberWidget(jQuery('.widget-list-grid'));
       // replaceMemberWidget(jQuery('.es-content').find('ul.group-members'));


        <?php if($plot){
        ?>
        showVideo();
        <?php
        }?>


    });

    function replaceLinkInStream(el) {
        jQuery(el).each(function () {
            var item = jQuery(this).find('a'),
                item_href = jQuery(item).attr('href'),
                res_href = item_href.replace('/community/profile/', '/profile/');
            jQuery(item).attr('href', res_href);
        });
    }

    function replaceMemberWidget(el) {
        jQuery(el).find('li').each(function () {
            var item = jQuery(this).find('a'),
                item_href = jQuery(item).attr('href'),
                res_href;

            if (item_href) {
                res_href = item_href.replace('/community/profile/', '/profile/');
                item_href.replace('/community/profile/', '/profile/');
                jQuery(item).attr('href', res_href);
            }


            jQuery(item).removeAttr('data-popbox').removeAttr('data-popbox-position');
        });
    }

    function addActivePlotClass(className){
        jQuery('.'+className).addClass('active');
    }


    (function() {
        var proxied = window.XMLHttpRequest.prototype.send;
        window.XMLHttpRequest.prototype.send = function() {
            //console.log( arguments );
            //Here is where you can add any code to process the request.
            //If you want to pass the Ajax request object, pass the 'pointer' below
            var pointer = this
            var intervalId = window.setInterval(function(){
                if(pointer.readyState != 4){
                    return;
                }
                //console.log( pointer.responseText );
                replaceMemberWidget(jQuery('.widget-list-grid'));
                replaceMemberWidget(jQuery('.es-content').find('ul.group-members'));
                replaceLinkInStream(jQuery('.es-stream-title'));

                //Here is where you can add any code to process the response.
                //If you want to pass the Ajax request object, pass the 'pointer' below
                clearInterval(intervalId);

            }, 1);//I found a delay of 1 to be sufficient, modify it as you need.
            return proxied.apply(this, [].slice.call(arguments));
        };


    })();

</script>