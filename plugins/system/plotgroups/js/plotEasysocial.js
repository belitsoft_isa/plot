window.plotEasysocial = window.plotEasysocial || {};

plotEasysocial.Menu = {
  createWrap: function (elementId, headtext, arr) {
    var str = '';
    str += '<div id="' + elementId + '" class="es-widget">';
    str += plotEasysocial.Menu.createHeader(headtext);
    str += plotEasysocial.Menu.createBody(arr);
    str += '</div>';
    str += '<hr>';
    return str;
  },
  createHeader: function (headtext) {
    var str = '';
    str += '<div class="es-widget-head">';
    str += '<div class="pull-left widget-title">';
    str += headtext
    str += '</div>'
    str += '</div>';
    return str;
  },
  createBody: function (arr) {
    var str = '',
      count_arr = arr.length,
      i;
    str += '<div class="es-widget-body">';
    str += '<ul class="widget-list plot-menu" >';
    for (i = 0; i < count_arr; i++) {
      str += '<li class="plot-item ' + arr[i].className + '"';
      if (arr[i].entity) {
        str += ' data-entity="' + arr[i].entity + '" '
      }

      if (arr[i].data_id) {
        str += ' data_id="' + arr[i].data_id + '" '
      }

      str += ' >';
      if (arr[i].counter) {
        str += '<span class="counter">' + parseInt(arr[i].counter) + '</span>';
      }
      str += '<a href="javascript:void(0)" onclick="' + arr[i].funkName + '">';
      if (arr[i].icon) {
        str += arr[i].icon;
      }
      str += arr[i].link_text;
      str += '</a>';
      str += '</li>';
    }
    str += '</ul>';
    str += ' </div>';
    return str;

  }

};

plotEasysocial.ProgramFilter = {
  createSelect: function (arr, options) {
    var str = '',
      count_arr = arr.length,
      i;
    str += '<select id="' + options.opt_id + '" name="' + options.opt_name + '" onchange="' + options.funkName + '">';
    str += '<option value="">' + options.def_text + '</option>';
    for (i = 0; i < count_arr; i++) {
      str += '<option value="' + arr[i].item_id + '">' + arr[i].item_title + '</option>';
    }
    str += '</select>';

    return str;
  },
  createButton: function (options) {
    var str = '';
    str += '<input type="' + options.button_type + '" id="' + options.opt_id + '" value="' + options.def_text + '" onclick="' + options.funkName + '" />';
    return str;
  },
  createTitle: function (options) {
    var str = '';
    str += '<' + options.el + ' class="' + options.opt_class + '" >' + options.def_text + '</' + options.el + '>';
    return str;
  }
};


plotEasysocial.Content = {
  getVideoById: function (elId,svg_link) {
    jQuery.post(
      'index.php?option=com_plot&task=program.getVideo',
      {
        id: elId
      },
      function (response) {
        var str = '',
          items;
        if (response.item) {
          items = response.item;
          str += '<h4 class="plot-groups-main-title"><svg xml:base="'+svg_link+'" viewBox="0 0 32 32"><use xlink:href="#question-circle"></use></svg>Описание программы';
          str += '</h4>';
          str += '<div class="iframe-wrap">';
          //str += '<table>';
          //str += '<tr>';
          //str += '<td>';
          str += ' <iframe  scrolling="no" width="560" height="315" src="//www.youtube.com/embed/' + items.youtubeNumber + '?autoplay=0&showinfo=0" frameborder="0" allowfullscreen="yes"></iframe>';
          //str += '</td>'
          //str += '</tr>'
          //str += '</table>'
          str += '</div>'
          str += '<div class="about">';
          str += items.description;
          str += '</div>';

          return plotEasysocial.Content.insertContent(str);
        }

      }
    );
  },
  getCoursesById: function (elId, menu_opts) {
    jQuery.post(
      'index.php?option=com_plot&task=program.getCourses',
      {
        id: elId
      },
      function (response) {
        jQuery('.es-content-wrap').html(response);
        plotEasysocial.Content.createMenuTitle(menu_opts);
        //plotEasysocial.SVG.sprite();
      }
    );
  },
  getBooksById: function (elId, menu_opts) {
    jQuery.post(
      'index.php?option=com_plot&task=program.getBooks',
      {
        id: elId
      },
      function (response) {
        var str = '',
          i,
          count_items;
        jQuery('.es-content-wrap').html(response);
        plotEasysocial.Content.createMenuTitle(menu_opts);

      }
    );
  },
  getProgramsById: function (elId, menu_opts) {
    var ajaxScrollData = {
      id: elId
    };
    jQuery.post(
      'index.php?option=com_plot&task=program.getPrograms',
      {
        id: elId
      },
      function (response) {
        var str = '',
          i,
          count_items;
        jQuery('.es-content-wrap').html(response);
        plotEasysocial.Content.createMenuTitle(menu_opts);
        plotEasysocial.Progress.createProgress('.plot-programs-progress');
      }
    );
  },
  getAllPrograms: function (programlimit) {

    var ajaxScrollData = {};

    jQuery(window).unbind('scroll');

    jQuery('#fd').scrollPagination({
      nop: parseInt(programlimit),
      offset: parseInt(programlimit),
      error: '',
      delay: 10,
      scroll: true,
      postUrl: 'index.php?option=com_plot&task=program.getAllPrograms',
      appendDataTo: '.es-content',
      userData: ajaxScrollData,
      afterLoad: {
        afterLoadAction: function () {
          jQuery('#fd .es-content').show();
             plotEasysocial.Progress.createProgress('.plot-programs-progress');
            jPlotUp.Arrow.initialize('positionCameras');
        }
      }
    });


  },
  getMyPrograms: function (programlimit) {

    var ajaxScrollData = {};

    jQuery(window).unbind('scroll');

    jQuery('#fd').scrollPagination({
      nop: parseInt(programlimit),
      offset: parseInt(programlimit),
      error: '',
      delay: 10,
      scroll: true,
      postUrl: 'index.php?option=com_plot&task=program.getMyPrograms',
      appendDataTo: '.es-content',
      userData: ajaxScrollData,
      afterLoad: {
        afterLoadAction: function () {
          jQuery('#fd .es-content').show();
          plotEasysocial.Progress.createProgress('.plot-programs-progress');
            jPlotUp.Arrow.initialize('positionCameras');
        }
      }
    });


  },
  getFeaturedPrograms: function (programlimit) {

    var ajaxScrollData = {};

    jQuery(window).unbind('scroll');

    jQuery('#fd').scrollPagination({
      nop: parseInt(programlimit),
      offset: parseInt(programlimit),
      error: '',
      delay: 10,
      scroll: true,
      postUrl: 'index.php?option=com_plot&task=program.getFeaturedPrograms',
      appendDataTo: '.es-content',
      userData: ajaxScrollData,
      afterLoad: {
        afterLoadAction: function () {
          jQuery('#fd .es-content').show();
          plotEasysocial.Progress.createProgress('.plot-programs-progress');
            jPlotUp.Arrow.initialize('positionCameras');
        }
      }
    });


  },
  getGroupsByCategory: function (elId, programlimit) {
    var ajaxScrollData = {
      id: elId
    };

    jQuery(window).unbind('scroll');

    jQuery('#fd').scrollPagination({
      nop: parseInt(programlimit),
      offset: parseInt(programlimit),
      error: '',
      delay: 10,
      scroll: true,
      postUrl: 'index.php?option=com_plot&task=program.getGroupsByCategory',
      appendDataTo: '.es-content',
      userData: ajaxScrollData,
      afterLoad: {
        afterLoadAction: function () {
          jQuery('#fd .es-content').show();
          plotEasysocial.Progress.createProgress('.plot-programs-progress');
            jPlotUp.Arrow.initialize('positionCameras');
        }
      }
    });

  },
  getGroupsBylevel: function (entity, elId, level, age, programlimit) {

    var ajaxScrollData = {
      id: elId,
      entity: entity,
      level: level,
      age: age
    };

    jQuery(window).unbind('scroll');

    jQuery('#fd').scrollPagination({
      nop: parseInt(programlimit),
      offset: parseInt(programlimit),
      error: '',
      delay: 10,
      scroll: true,
      postUrl: 'index.php?option=com_plot&task=program.getGroupsBylevel',
      appendDataTo: '.es-content',
      userData: ajaxScrollData,
      afterLoad: {
        afterLoadAction: function () {
          jQuery('#fd .es-content').show();
          plotEasysocial.Progress.createProgress('.plot-programs-progress');
            jPlotUp.Arrow.initialize('positionCameras');
        }
      }
    });


  },
  getGroupsByAge: function (entity, elId, level, age, programlimit) {

    var ajaxScrollData = {
      id: elId,
      entity: entity,
      level: level,
      age: age
    };

    jQuery(window).unbind('scroll');

    jQuery('#fd').scrollPagination({
      nop: parseInt(programlimit),
      offset: parseInt(programlimit),
      error: '',
      delay: 10,
      scroll: true,
      postUrl: 'index.php?option=com_plot&task=program.getGroupsByAge',
      appendDataTo: '.es-content',
      userData: ajaxScrollData,
      afterLoad: {
        afterLoadAction: function () {
          jQuery('#fd .es-content').show();
          plotEasysocial.Progress.createProgress('.plot-programs-progress');
            jPlotUp.Arrow.initialize('positionCameras');
        }
      }
    });

  },
  getClearGroup: function (entity, elId, programlimit) {

    var ajaxScrollData = {
      id: elId,
      entity: entity
    };

    jQuery(window).unbind('scroll');

    jQuery('#fd').scrollPagination({
      nop: parseInt(programlimit),
      offset: parseInt(programlimit),
      error: '',
      delay: 10,
      scroll: true,
      postUrl: 'index.php?option=com_plot&task=program.getClearGroup',
      appendDataTo: '.es-content',
      userData: ajaxScrollData,
      afterLoad: {
        afterLoadAction: function () {
          jQuery('#fd .es-content').show();
          plotEasysocial.Progress.createProgress('.plot-programs-progress');
        }
      }
    });

  },
  insertContent: function (data) {
    jQuery('.es-content-wrap').html(data);
  },
  insertContentGroups: function (data) {
    jQuery('.media-listing').html(data);
    jQuery('#fd .es-content').show();
  },
  createMenuTitle: function (menu_opts) {

    var str = '';
    str += '<h4 class="plot-groups-main-title">';
    if (menu_opts.icon) {
      str += menu_opts.icon;
    }
    if (menu_opts.def_text) {
      str += menu_opts.def_text;
    }
    str += '</h4>'
    jQuery(document.querySelector('.plot-program-entity')).before(str);


  },
  isElementExist: function (selector) {
    if (document.querySelector(selector)) {
      return true
    } else {
      return false;
    }
  }


};
plotEasysocial.MainProgram = {
  createAdditionalInfo: function (level, age, link) {
    jQuery('.es-profile-header-meta').before('<div class="plot-main-program-info"><div class="plot-main-program-age"><a href="'+link+'" class="about-levels">' + age + '<svg viewBox="0 0 32 32"><use xlink:href="#question-circle"></use></svg></a></div><div class="plot-main-program-level">' + level + '</div></div>')
  },
  createProgress: function (progress_text, progress,progress_alternative_text) {
    if (progress>0) {
      jQuery('.es-profile-header-footer nav').before("<div class='plot-main-program-progress'><div class='plot-programs-progress' data-progress='" + progress + "'></div></div><span>" + progress_text + "</span>");
      //jQuery("<div class='plot-main-program-progress'><div class='plot-programs-progress' data-progress='"+progress+"'></div></div><span>"+progress_text+"</span>").appendTo(".es-profile-header-footer");
      plotEasysocial.Progress.createProgress('.plot-programs-progress');
    }else{
        jQuery('.es-profile-header-footer nav').before("<div class='plot-main-program-progress'></div><strong>" + progress_alternative_text + "</strong>");
    }
  },
    declensionWordUsers:function(titles){
        var item=jQuery('.es-profile-header-meta').find('.ies-users').siblings('a'),
            count_users=parseInt(jQuery(item).text());

        jQuery(item).text(count_users+' '+plotEasysocial.Words.ending(count_users,titles));

    }
};
plotEasysocial.Progress = {
  createProgress: function (selector) {
    jQuery(selector).each(function () {
      var percent = parseInt(jQuery(this).attr('data-progress'));
      jQuery(this).simple_progressbar({
        value: percent,
        height: '15px',
        internalPadding: '3',
        normalColor: '#C56B35',
        backgroundColor: '#ffffff'
      });
    });
  }
};
plotEasysocial.SelectMenu={
    createSelectMenu:function(params){
        jQuery('#'+params.opt_id).selectmenu({appendTo:'.es-toolbar',
            change: function () {
                params.funkName(this);

            }});
    }
}
plotEasysocial.Words={
    ending:function(number, titles){
        cases = [2, 0, 1, 1, 1, 2];
        return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
    }
}
plotEasysocial.SVG = {
  sprite: function () {
    jQuery('#svg-sprite').empty();
    jQuery('#svg-sprite').html(SVG_SPRITE);
    plotEasysocial.SVG.replaceLinkSvg(jQuery(document).find('svg'));
    plotEasysocial.SVG.replaceLinkUse(jQuery(document).find('svg').find('use'));
  },
  replaceLinkSvg: function (el) {
    jQuery(el).each(function () {
      var item = jQuery(this);
      jQuery(item).attr('xml:base', location.href);
    });
  },
  replaceLinkUse: function (el) {
    jQuery(el).each(function () {
      var item = jQuery(this),
        item_href = jQuery(item).attr('xlink:href'),
        res_href = location.href + item_href;
      jQuery(item).removeAttr('xlink:href');
      jQuery(item).attr('xlink:href', res_href);
    });
  }
}
