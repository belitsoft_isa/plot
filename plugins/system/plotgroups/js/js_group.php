<?php
defined('_JEXEC') or die;
$jinput = JFactory::getApplication()->input;
$plot = $jinput->get('plot', 0, 'INT');

?>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<script src="<?php echo JUri::root() . '/media/jui/js/jquery.min.js'; ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo JUri::root() . 'plugins/system/plotgroups/js/plotEasysocial.js'; ?>"></script>
<script type="text/javascript">

    jQuery(document).ready(function () {
        var arr = new Array();

        arr.push({
            funkName: 'showVideo()',
            link_text: '<?php echo JText::_("PLG_PLOTGROUPS_MENU_ABOUT")?>',
            className:'plot-about-menu'
        });
        arr.push({
            funkName: 'showCourses()',
            link_text: '<?php echo JText::_("PLG_PLOTGROUPS_MENU_COURSES")?>',
            className:'plot-courses-menu'
        });
        arr.push({
            funkName: 'showBooks()',
            link_text: '<?php echo JText::_("PLG_PLOTGROUPS_MENU_BOOKS")?>',
            className:'plot-books-menu'
        });
        arr.push({
            funkName: 'showPrograms()',
            link_text: '<?php echo JText::_("PLG_PLOTGROUPS_MENU_PROGRAMS")?>',
            className:'plot-programs-menu'
        });

        jQuery('#fd').find('.es-sidebar').prepend(plotEasysocial.Menu.createWrap('plot-menu', '<?php echo  JText::_("PLG_PLOTGROUPS_MENU_TITLE"); ?>', arr));
        replaceLinkInStream(jQuery('#fd').find('.es-stream-list').find('div.es-stream-title'));

        replaceMemberWidget(jQuery('.widget-list-grid'));
        replaceMemberWidget(jQuery('.es-content').find('ul.group-members'));


        <?php if($plot){
        ?>
        showVideo();
        <?php
        }?>


    });

    function replaceLinkInStream(el) {
        jQuery(el).each(function () {
            var item = jQuery(this).find('a'),
                item_href = jQuery(item).attr('href'),
                res_href = item_href.replace('/community/profile/', '/profile/');
            jQuery(item).attr('href', res_href);
        });
    }

    function replaceMemberWidget(el) {
        jQuery(el).find('li').each(function () {
            var item = jQuery(this).find('a'),
                item_href = jQuery(item).attr('href'),
                res_href;

            if (item_href) {
                res_href = item_href.replace('/community/profile/', '/profile/');
                item_href.replace('/community/profile/', '/profile/');
                jQuery(item).attr('href', res_href);
            }


            jQuery(item).removeAttr('data-popbox').removeAttr('data-popbox-position');
        });
    }

    function showVideo(){
        var elId=jQuery('#fd').find('.es-profile-header').attr('data-id');
        plotEasysocial.Content.getVideoById(elId);
        removeActiveClass();
        addActivePlotClass('plot-about-menu');
    }

    function showCourses(){
        var elId=jQuery('#fd').find('.es-profile-header').attr('data-id');
        plotEasysocial.Content.getCoursesById(elId);
        removeActiveClass();
        addActivePlotClass('plot-courses-menu');
    }

    function showBooks(){
        var elId=jQuery('#fd').find('.es-profile-header').attr('data-id');
        plotEasysocial.Content.getBooksById(elId);
        removeActiveClass();
        addActivePlotClass('plot-books-menu');
    }

    function showPrograms(){
        var elId=jQuery('#fd').find('.es-profile-header').attr('data-id');
        plotEasysocial.Content.getProgramsById(elId);
        removeActiveClass();
        addActivePlotClass('plot-programs-menu');
    }

    function removeActiveClass(){
        jQuery('.widget-list').each(function(){
        jQuery(this).find('li').removeClass('active');
        });
    }
    function addActivePlotClass(className){
        jQuery('.'+className).addClass('active');
    }


    (function() {
        var proxied = window.XMLHttpRequest.prototype.send;
        window.XMLHttpRequest.prototype.send = function() {
            //console.log( arguments );
            //Here is where you can add any code to process the request.
            //If you want to pass the Ajax request object, pass the 'pointer' below
            var pointer = this
            var intervalId = window.setInterval(function(){
                if(pointer.readyState != 4){
                    return;
                }
                //console.log( pointer.responseText );
                replaceMemberWidget(jQuery('.widget-list-grid'));
                replaceMemberWidget(jQuery('.es-content').find('ul.group-members'));
                replaceLinkInStream(jQuery('.es-stream-title'));

                //Here is where you can add any code to process the response.
                //If you want to pass the Ajax request object, pass the 'pointer' below
                clearInterval(intervalId);

            }, 1);//I found a delay of 1 to be sufficient, modify it as you need.
            return proxied.apply(this, [].slice.call(arguments));
        };


    })();

</script>