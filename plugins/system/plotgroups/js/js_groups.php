
<?php
defined('_JEXEC') or die;
$groupsObj = new plotProgram();
$groups_list = $groupsObj->getAllCatigories();
$groups_levels = $groupsObj->getAllLevels();
$groups_ages=plotAges::getList();

?>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<script src="<?php echo JUri::root() . '/media/jui/js/jquery.min.js'; ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script src="<?php echo JUri::root() . 'components/com_plot/libraries/scrolling_ajax_pagination/javascript.js'; ?>"></script>
<script type="text/javascript" charset="utf-8"
        src="<?php echo JUri::root() . 'plugins/system/plotgroups/js/plotEasysocial.js'; ?>"></script>

<style>
    #fd ul.es-pagination {
        display: none !important;
    }

    #fd .media-listing {
        display: none;
    }

    #fd .es-sidebar .es-widget {
        display: none;
    }
</style>
<script type="text/javascript" charset="utf-8">
    jQuery(document).ready(function () {
        var wrap = jQuery('#fd'),
            groups_list = jQuery(wrap).find('.list-media'),
            arr = new Array(),
            arr2 = new Array(),
            options_level = {
                opt_name: 'level',
                opt_id: 'level_plot_select',
                def_text: '<?php echo  JText::_("PLG_PLOTGROUPS_PROGRAM_FILTER_ALL_LEVELS")?>',
                funkName: 'plotFilterLevel(this)'
            },
            options_ages = {
                opt_name: 'ages',
                opt_id: 'level_plot_ages',
                def_text: '<?php echo  JText::_("PLG_PLOTGROUPS_PROGRAM_FILTER_ALL_AGES")?>',
                funkName: 'plotFilterAges(this)'
            },
            levels_arr = new Array(),
            ages_arr = new Array();

        arr.push({
            funkName: 'showAllGroups()',
            link_text: '<?php echo JText::_("PLG_PLOTGROUPS_MENU_ALL_GROUPS")?>',
            className: 'plot-all-group-menu'
        });
        arr.push({
            funkName: 'showMyGroups()',
            link_text: '<?php echo JText::_("PLG_PLOTGROUPS_MENU_MY_GROUPS")?>',
            className: 'plot-my-groups-menu'
        });
        <?php
        foreach($groups_list AS $group){
        ?>
        arr2.push({
            funkName: "showCategoryGroups('<?php echo $group->id;?>')",
            link_text: "<?php echo $group->title; ?>",
            className: "plot-categories-group-menu"
        });
        <?php
        }
        ?>
        <?php foreach($groups_levels AS $item){
        ?>
        levels_arr.push({
            item_id: '<?php echo  $item->id;?>',
            item_title: '<?php echo  $item->title; ?>'
        });
        <?php
        } ?>

        <?php foreach($groups_ages AS $item){
       ?>
        ages_arr.push({
            item_id: '<?php echo  $item->id;?>',
            item_title: '<?php echo  $item->title; ?>'
        });
        <?php
        } ?>

        jQuery('.es-toolbar').prepend(plotEasysocial.ProgramFilter.createSelect(levels_arr, options_level));
        jQuery('.es-toolbar').prepend(plotEasysocial.ProgramFilter.createSelect(ages_arr, options_ages));
        jQuery('#fd').find('.es-sidebar').html('');
        jQuery('#fd').find('.es-sidebar').prepend(plotEasysocial.Menu.createWrap('plot-menu', '<?php echo JText::_("PLG_PLOTGROUPS_MENU_CATEGORIES")?>', arr2));
        jQuery('#fd').find('.es-sidebar').prepend(plotEasysocial.Menu.createWrap('plot-menu', '', arr));
        jQuery('#fd').find('.es-sidebar').find('.es-widget').each(function () {
            jQuery(this).show();
        });
        replaceLinkProfile(groups_list);

        addParamToGroupLink(groups_list);

        function replaceLinkProfile(groups_list) {
            jQuery(groups_list).find('div.media').each(function () {
                var item = jQuery(this).find('.ies-user').next('a'),
                    item_href = jQuery(item).attr('href'),
                    res_href = item_href.replace('/community/profile/', '/profile/');
                jQuery(item).attr('href', res_href);

            });
        }

        function addParamToGroupLink(groups_list) {
            jQuery(groups_list).find('.media-name').each(function () {
                var item = jQuery(this).find('a'),
                    item_href = jQuery(item).attr('href');
                jQuery(item).attr('href', item_href + '?plot=1');
            });
        }

        function replaceLinkInStream(el) {
            jQuery(el).each(function () {
                var item = jQuery(this),
                    item_href = jQuery(item).attr('href'),
                    res_href = item_href.replace('/community/profile/', '/profile/');
                jQuery(item).attr('href', res_href);
            });
        }

        showAllGroups();

    });

    function showAllGroups() {
        plotEasysocial.Content.getAllPrograms();
        removeActiveClass();
        //addActivePlotClass('plot-all-group-menu');

    }

    function showMyGroups() {
        plotEasysocial.Content.getMyPrograms();
        removeActiveClass();
        //addActivePlotClass('plot-courses-menu');
    }

    function showCategoryGroups(elId) {
        plotEasysocial.Content.getGroupsByCategory(elId);

    }

    function removeActiveClass() {
        jQuery('.widget-list').each(function () {
            jQuery(this).find('li').removeClass('active');
        });
    }


    function plotFilterLevel(el) {
        plotEasysocial.Content. getGroupsBylevel(jQuery(el).val());
    }

    function plotFilterAges(el){
        plotEasysocial.Content.getGroupsByAge(jQuery(el).val());
    }
    /*(function() {
     var proxied = window.XMLHttpRequest.prototype.send;
     window.XMLHttpRequest.prototype.send = function() {
     //console.log( arguments );
     //Here is where you can add any code to process the request.
     //If you want to pass the Ajax request object, pass the 'pointer' below
     var pointer = this
     var intervalId = window.setInterval(function(){
     if(pointer.readyState != 4){
     return;
     }
     //console.log( pointer.responseText );

     replaceLinkProfile(jQuery('#fd').find('.list-media'));
     replaceLinkInStream(jQuery('#fd').find('.list-media').find('.media-meta').find('span').find('.ies-user').next('a'));
     //Here is where you can add any code to process the response.
     //If you want to pass the Ajax request object, pass the 'pointer' below
     clearInterval(intervalId);

     }, 1);//I found a delay of 1 to be sufficient, modify it as you need.
     return proxied.apply(this, [].slice.call(arguments));
     };


     })();*/
</script>