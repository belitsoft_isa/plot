<?php
/**
 * Attachments plugin for inserting attachments lists into content
 *
 * @package     Attachments
 * @subpackage  Main_Attachments_Plugin
 *
 * @author      Jonathan M. Cameron <jmcameron@jmcameron.net>
 * @copyright   Copyright (C) 2007-2013 Jonathan M. Cameron, All Rights Reserved
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://joomlacode.org/gf/project/attachments/frs/
 */

defined('_JEXEC') or die('Restricted access');


/**
 * Attachments plugin
 *
 * @package  Attachments
 * @since    1.3.4
 */
class plgSystemPlotgroups extends JPlugin
{
    protected $autoloadLanguage = true;


    function onBeforeCompileHead()
    {
        if (!JFactory::getApplication()->isAdmin()) {
            $jinput = JFactory::getApplication()->input;
            $option = $jinput->get('option', '');
            $view = $jinput->get('view', '');
            $layout = $jinput->get('layout', '');
            $app = JFactory::getApplication();
            //die(var_dump($_REQUEST));
            $document = JFactory::getDocument();

            if ($option == 'com_easysocial') {
                if ($view == 'community' || $view == 'conversations' || $view == 'groups') {
                    if (plotUser::factory()->id) {
                        if ($view == 'groups' && !$layout) {

                            $document->addScript('/media/jui/js/jquery.min.js');
                            $document->addScript('/media/jui/js/jquery.ui.core.min.js');
                            $document->addScript('/components/com_plot/libraries/scrolling_ajax_pagination/javascript.js');
                            $document->addScript('/plugins/system/plotgroups/js/plotEasysocial.js');
                            $document->addScript('/templates/plot/js/jquery_simple_progressbar.js');

                            $script = $this->prepareGroupsScript();
                            $style = $this->prepareGroupsStyle();
                            $document->addStyleDeclaration($style);
                            $document->addScriptDeclaration($script);
                            $document->setTitle(JText::_('PLG_PLOTGROUPS_MENU_PROGRAMS'));
                        } elseif ($view == 'groups' && $layout == 'item') {

                            $document->addScript('/media/jui/js/jquery.min.js');
                            $document->addScript('/media/jui/js/jquery.ui.core.min.js');
                            $document->addScript('/plugins/system/plotgroups/js/plotEasysocial.js');
                            $document->addScript('/components/com_plot/libraries/scrolling_ajax_pagination/javascript.js');


                            $document->addScript('/templates/plot/js/jquery_simple_progressbar.js');
                            $script = $this->prepareAGroupScript();
                            $style = $this->prepareGroupsStyle();
                            $document->addScriptDeclaration($script);
                            $document->addStyleDeclaration($style);
                            $document->setTitle(JText::_('PLG_PLOTGROUPS_PROGRAM_TITLE'));
                        } else {

                            return;
                        }
                    } else {
                        if ($view == 'groups') {
                            if ($view == 'groups' && !$layout) {

                                $document->addScript('/media/jui/js/jquery.min.js');
                                $document->addScript('/media/jui/js/jquery.ui.core.min.js');
                                $document->addScript('/plugins/system/plotgroups/js/plotEasysocial.js');
                                $document->addScript('/components/com_plot/libraries/scrolling_ajax_pagination/javascript.js');

                                $document->addScript('/templates/plot/js/jquery_simple_progressbar.js');
                                $document->addScript('/templates/plot/js/jquery.jscrollpane.min.js');

                                $script = $this->prepareGroupsScript();
                                $style = $this->prepareGroupsStyle();
                                $document->addStyleDeclaration($style);
                                $document->addScriptDeclaration($script);
                                $document->setTitle(JText::_('PLG_PLOTGROUPS_MENU_PROGRAMS'));
                                return;
                            } elseif ($view == 'groups' && $layout == 'item') {
                                $document->addScript('/media/jui/js/jquery.min.js');
                                $document->addScript('/media/jui/js/jquery.ui.core.min.js');
                                $document->addScript('/components/com_plot/libraries/scrolling_ajax_pagination/javascript.js');
                                $document->addScript('/plugins/system/plotgroups/js/plotEasysocial.js');

                                $document->addScript('/templates/plot/js/jquery_simple_progressbar.js');
                                $script = $this->prepareAGroupScript();
                                $style = $this->prepareGroupsStyle();
                                $document->addScriptDeclaration($script);
                                $document->addStyleDeclaration($style);
                                $document->setTitle(JText::_('PLG_PLOTGROUPS_PROGRAM_TITLE'));
                                return;
                            } else {

                                return;
                            }
                        } else {

                            JFactory::getApplication()->redirect(JRoute::_("index.php?option=com_plot&view=river", false, -1));
                            return;
                        }

                    }

                }
            }
        }

    }

    function prepareGroupsScript()
{
    $groupsObj = new plotProgram();
    $groups_list = $groupsObj->getAllCatigories();
    $groups_levels = $groupsObj->getAllLevels();
    $groups_ages = plotAges::getList();
    $progObj = new plotProgram();
    $groups_allcount = $progObj->getCountAllPrograms();
    $groups_mycount = plotUser::factory()->getCountMyPrograms();
    $groups_featuredcount = $progObj->getCountFeaturedPrograms();

    ob_start();
    ?>

        jQuery(document).ready(function () {
            var wrap = jQuery('#fd'),
                groups_list = jQuery(wrap).find('.list-media'),
                levels_arr = new Array(),
                ages_arr = new Array(),
                options_clearbutton = {
                    opt_id: 'plot_clear_button',
                    def_text: '<?php echo JText::_("PLG_PLOTGROUPS_PROGRAM_FILTER_CLEAR") ?>',
                    funkName: 'plotClear(this)',
                    button_type: 'button'
                },
                options_ages = {
                    opt_name: 'ages',
                    opt_id: 'level_plot_ages',
                    def_text: '<?php echo JText::_("PLG_PLOTGROUPS_PROGRAM_FILTER_ALL_AGES") ?>',
                    funkName: function (arg) {
                        plotFilterAges(arg);
                    }
                },
                arr = new Array(),
                arr2 = new Array(),
                options_level = {
                    opt_name: 'level',
                    opt_id: 'level_plot_select',
                    def_text: '<?php echo JText::_("PLG_PLOTGROUPS_PROGRAM_FILTER_ALL_LEVELS") ?>',
                    funkName: function (arg) {
                        plotFilterLevel(arg);
                    }
                },
                options_groumstitle = {
                    el: 'h4',
                    opt_class: 'plot-groups-main-title',
                    opt_text: 'plot-groups-main-title',
                    def_text: '<?php echo JText::_("PLG_PLOTGROUPS_PROGRAMS_LIST_MAIN_TITLE"); ?>'
                };

            arr.push({
                funkName: 'showAllGroups()',
                link_text: '<?php echo JText::_("PLG_PLOTGROUPS_MENU_ALL_GROUPS") ?>',
                className: 'plot-all-group-menu active',
                icon: '<i class="ies-briefcase-2 mr-5"></i>',
                entity: 'all-groups',
                counter: '<?php echo (int)$groups_allcount; ?>'
            });
            arr.push({
                funkName: 'showMyGroups()',
                link_text: '<?php echo JText::_("PLG_PLOTGROUPS_MENU_MY_GROUPS") ?>',
                className: 'plot-my-groups-menu',
                icon: '<i class="ies-briefcase-2 mr-5"></i>',
                entity: 'my-groups',
                counter: '<?php echo (int)$groups_mycount; ?>'
            });
            arr.push({
                funkName: 'showFeaturedGroups()',
                link_text: '<?php echo JText::_("PLG_PLOTGROUPS_MENU_FEATURED_GROUPS") ?>',
                className: 'plot-featured-groups-menu',
                icon: '<i class="ies-briefcase-2 mr-5"></i>',
                entity: 'featured-groups',
                counter: '<?php echo (int)$groups_featuredcount; ?>'
            });
            <?php
            foreach ($groups_list AS $group) {
                ?>
            arr2.push({
                funkName: "showCategoryGroups('<?php echo $group->id; ?>')",
                link_text: "<?php echo $group->title; ?>",
                className: "plot-categories-group-menu-<?php echo $group->id; ?>",
                entity: "cats",
                icon: '<i class="ies-briefcase-2 mr-5"></i>',
                data_id: "<?php echo $group->id; ?>",
                counter: '<?php echo (int)$group->count_cat; ?>'
            });
            <?php
            }
            ?>
            <?php foreach ($groups_levels AS $item) {
            ?>
            levels_arr.push({
                item_id: '<?php echo $item->id; ?>',
                item_title: '<?php echo $item->title; ?>'
            });
            <?php
            } ?>

            <?php foreach ($groups_ages AS $item) {
            ?>
            ages_arr.push({
                item_id: '<?php echo $item->id; ?>',
                item_title: '<?php echo $item->title; ?>'
            });
            <?php
            } ?>

            jQuery('#fd').on('click', 'li.plot-item', function () {
                jQuery(this).addClass('active');
            });

            jQuery('.es-content').html('');
            showAllGroups();

            jQuery('.es-toolbar').prepend(plotEasysocial.ProgramFilter.createButton(options_clearbutton));
            jQuery('.es-toolbar').prepend(plotEasysocial.ProgramFilter.createSelect(levels_arr, options_level));
            plotEasysocial.SelectMenu.createSelectMenu(options_level);
            jQuery('.es-toolbar').prepend(plotEasysocial.ProgramFilter.createSelect(ages_arr, options_ages));
            plotEasysocial.SelectMenu.createSelectMenu(options_ages);
            jQuery('.es-toolbar').prepend(plotEasysocial.ProgramFilter.createTitle(options_groumstitle));
            jQuery('#fd').find('.es-sidebar').html('');
            jQuery('#fd').find('.es-sidebar').prepend(plotEasysocial.Menu.createWrap('plot-menu', '<?php echo JText::_("PLG_PLOTGROUPS_MENU_CATEGORIES") ?>', arr2));
            jQuery('#fd').find('.es-sidebar').prepend(plotEasysocial.Menu.createWrap('plot-menu', '', arr));
            jQuery('#fd').find('.es-sidebar').find('.es-widget').each(function () {
                jQuery(this).show();
            });
            replaceLinkProfile(groups_list);

            addParamToGroupLink(groups_list);

            function replaceLinkProfile(groups_list) {
                jQuery(groups_list).find('div.media').each(function () {
                    var item = jQuery(this).find('.ies-user').next('a'),
                        item_href = jQuery(item).attr('href'),
                        res_href = item_href.replace('/community/profile/', '/profile/');
                    jQuery(item).attr('href', res_href);

                });
            }

            function addParamToGroupLink(groups_list) {
                jQuery(groups_list).find('.media-name').each(function () {
                    var item = jQuery(this).find('a'),
                        item_href = jQuery(item).attr('href');
                    jQuery(item).attr('href', item_href + '?plot=1');
                });
            }

            function replaceLinkInStream(el) {
                jQuery(el).each(function () {
                    var item = jQuery(this),
                        item_href = jQuery(item).attr('href'),
                        res_href = item_href.replace('/community/profile/', '/profile/');
                    jQuery(item).attr('href', res_href);
                });
            }


        });

        function showAllGroups() {
            jQuery('.es-content').html('');
            plotEasysocial.Content.getAllPrograms('<?php echo plotGlobalConfig::getVar("programsLimit"); ?>');
            removeActiveClass();

            jQuery('.es-content').trigger("scroll");

        }

        function showMyGroups() {
            jQuery('.es-content').html('');
            plotEasysocial.Content.getMyPrograms('<?php echo plotGlobalConfig::getVar("programsLimit"); ?>');
            removeActiveClass();

            jQuery('.es-content').trigger("scroll");
        }


        function showCategoryGroups(elId) {
            jQuery('.es-content').html('');
            removeActiveClass();
            plotEasysocial.Content.getGroupsByCategory(elId, '<?php echo plotGlobalConfig::getVar("programsLimit"); ?>');
            jQuery('.es-content').trigger("scroll");
        }

        function showFeaturedGroups() {
            jQuery('.es-content').html('');
            plotEasysocial.Content.getFeaturedPrograms('<?php echo plotGlobalConfig::getVar("programsLimit"); ?>');
            removeActiveClass();

            jQuery('.es-content').trigger("scroll");
        }

        function removeActiveClass() {
            jQuery('.widget-list').each(function () {
                jQuery(this).find('li').removeClass('active');
            });
        }


        function plotFilterLevel(el) {
            var entity = jQuery('.widget-list').find('li.active').attr('data-entity'),
                data_id = jQuery('.widget-list').find('li.active').attr('data_id'),
                age = jQuery("#level_plot_ages option:selected").val();
            jQuery('.es-content').html('');
            plotEasysocial.Content.getGroupsBylevel(entity, data_id, jQuery(el).val(), age, '<?php echo plotGlobalConfig::getVar("programsLimit"); ?>');
            jQuery('.es-content').trigger("scroll");
        }

        function plotFilterAges(el) {
            var entity = jQuery('.widget-list').find('li.active').attr('data-entity'),
                data_id = jQuery('.widget-list').find('li.active').attr('data_id'),
                level = jQuery("#level_plot_select option:selected").val();
            jQuery('.es-content').html('');
            console.log(jQuery(el).val());
            plotEasysocial.Content.getGroupsByAge(entity, data_id, level, jQuery(el).val(), '<?php echo plotGlobalConfig::getVar("programsLimit"); ?>');
            jQuery('.es-content').trigger("scroll");
        }

        function plotClear() {
            var entity = jQuery('.widget-list').find('li.active').attr('data-entity'),
                data_id = jQuery('.widget-list').find('li.active').attr('data_id');
            jQuery("#level_plot_ages option[value='']").attr('selected', 'selected');
            jQuery("#level_plot_select option[value='']").attr('selected', 'selected');
            jQuery('.es-content').html('');
            plotEasysocial.Content.getClearGroup(entity, data_id, '<?php echo plotGlobalConfig::getVar("programsLimit"); ?>');
            jQuery("#level_plot_select").selectmenu("refresh");
            jQuery("#level_plot_ages").selectmenu("refresh");
            jQuery('.es-content').trigger("scroll");
        }

      document.addEventListener("touchmove", function(){
            jQuery('#fd').trigger('scroll');
    }, false);

        <?php
        $content = ob_get_clean();
        return $content;
    }

    function prepareGroupsStyle()
    {
        ob_start();
        ?>
        #
        fd
        ul.es - pagination
        {
            display: none
            !important;
        }

        #
        fd.media - listing
        {
            display: none;
        }

        #
        fd.es - sidebar.es - widget
        {
            <!--        display: none;-->
        }

        #
        fd.es - profile - header - meta
        span:first - child
        {
            display: none
            !important;
        }
        <?php
        $content = ob_get_clean();
        return $content;
    }

    function prepareAGroupScript()
    {
        $jinput = JFactory::getApplication()->input;
        $plot = $jinput->get('plot', 0, 'INT');
        $id= $jinput->get('id', 0, 'INT');
        $progObj=new plotProgram($id);
        $ages=implode(', ',$progObj->getProgramAgesTitle());
        $agesstr='';
        if($ages){
            $agesstr="<span>".JText::_('PLG_PLOTGROUPS_MAIN_PROGRAM_AGES')."</span>".$ages;
        }

        $my = plotUser::factory();
        $progress = '';
        if ($my->id) {
            $progress = (int)$my->programProgress($id);
        }
        $count_courses = $progObj->getCountCourses();
        $count_books = $progObj->getCountBooks();
        $count_subprograms = $progObj->getCountSubPrograms();
        ob_start();
        ?>
        <!--<script>-->
        jQuery(document).ready(function () {
            var arr = new Array();

            arr.push({
                funkName: 'showVideo()',
                link_text: '<?php echo JText::_("PLG_PLOTGROUPS_MENU_ABOUT")?>',
                className: 'plot-about-menu',
                icon: '<svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>" viewBox="0 0 32 32"><use xlink:href="#question-circle"></use></svg>',
                counter: ''
            });
            arr.push({
                funkName: 'showCourses()',
                link_text: '<?php echo JText::_("PLG_PLOTGROUPS_MENU_COURSES")?>',
                className: 'plot-courses-menu',
                icon: '<svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>"  viewBox="0 0 32 32"><use xlink:href="#academic-hat"></use></svg>',
                counter: '<?php echo  (int)$count_courses; ?>'
            });
            arr.push({
                funkName: 'showBooks()',
                link_text: '<?php echo JText::_("PLG_PLOTGROUPS_MENU_BOOKS")?>',
                className: 'plot-books-menu',
                icon: '<svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>"  viewBox="0 0 32 32"><use xlink:href="#notebook"></use></svg>',
                counter: '<?php echo (int)$count_books;?>'
            });
            arr.push({
                funkName: 'showPrograms()',
                link_text: '<?php echo JText::_("PLG_PLOTGROUPS_MENU_PROGRAMS")?>',
                className: 'plot-programs-menu',
                icon: '<svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>"  viewBox="0 0 41 32"><use xlink:href="#cubes"></use></svg>',
                counter: '<?php echo (int)$count_subprograms; ?>'
            });

            plotEasysocial.MainProgram.createProgress('<?php echo JText::_("PLG_PLOTGROUPS_MAIN_PROGRAM_PROGRESS")?>', '<?php echo $progress; ?>','<?php echo  JText::_("PLG_PLOTGROUPS_MAIN_PROGRAM_START_THIS_PROGRAMM")?>');
            plotEasysocial.MainProgram.createAdditionalInfo('<?php echo $agesstr;?>', '<?php echo "<span>".JText::_("PLG_PLOTGROUPS_MAIN_PROGRAM_LEVEL")."</span>".$progObj->level_title ?>','<?php echo  JRoute::_("index.php?option=com_plot&view=k2article&id=".plotGlobalConfig::getVar("footerLinkK2idForChildrens"));?>');
        plotEasysocial.MainProgram.declensionWordUsers(['участник', 'участника', 'участников']);
            jQuery('#fd').find('.es-sidebar').prepend(plotEasysocial.Menu.createWrap('plot-menu', '<?php echo  JText::_("PLG_PLOTGROUPS_MENU_TITLE"); ?>', arr));
            replaceLinkInStream(jQuery('#fd').find('.es-stream-list').find('div.es-stream-title'));

            replaceMemberWidget(jQuery('.widget-list-grid'));
            replaceMemberWidget(jQuery('.es-content').find('ul.group-members'));


            <?php if($plot){
            ?>
            showVideo();
            <?php
            }?>

            jQuery('#fd').on('click', '.widget-list li', function () {
                removeActiveClass();
                jQuery(this).addClass('active');
            });

            //plotEasysocial.SVG.sprite();


        });

        function replaceLinkInStream(el) {
            jQuery(el).each(function () {
                var item = jQuery(this).find('a'),
                    item_href = jQuery(item).attr('href'),
                    res_href = item_href.replace('/community/profile/', '/profile/');
                jQuery(item).attr('href', res_href);
            });
        }

        function replaceMemberWidget(el) {
            jQuery(el).find('li').each(function () {
                var item = jQuery(this).find('a'),
                    item_href = jQuery(item).attr('href'),
                    res_href;

                if (item_href) {
                    res_href = item_href.replace('/community/profile/', '/profile/');
                    item_href.replace('/community/profile/', '/profile/');
                    jQuery(item).attr('href', res_href);
                }


                jQuery(item).removeAttr('data-popbox').removeAttr('data-popbox-position');
            });
        }

        function showVideo() {
            var elId = jQuery('#fd').find('.es-profile-header').attr('data-id');
            plotEasysocial.Content.getVideoById(elId, '<?php echo $_SERVER['REQUEST_URI'];?>');
            removeActiveClass();

        }

        function showCourses() {
            var elId = jQuery('#fd').find('.es-profile-header').attr('data-id'),
                courses_opt = {
                    def_text: '<?php echo JText::_("PLG_PLOTGROUPS_COURSES_LIST_TITLE") ?>',
                    icon: '<svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>"  viewBox="0 0 32 32"><use xlink:href="#academic-hat"></use></svg>',

                };
            plotEasysocial.Content.getCoursesById(elId, courses_opt);
            removeActiveClass();

        }

        function showBooks() {
            var elId = jQuery('#fd').find('.es-profile-header').attr('data-id'),
                books_opt = {
                    def_text: '<?php echo JText::_("PLG_PLOTGROUPS_BOOKS_LIST_TITLE") ?>',
                    icon: '<svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>" viewBox="0 0 32 32"><use xlink:href="#notebook"></use></svg>',
                };
            plotEasysocial.Content.getBooksById(elId, books_opt);
            removeActiveClass();

        }

        function showPrograms() {
            jQuery('.es-content-wrap').html('');
            var elId = jQuery('#fd').find('.es-profile-header').attr('data-id'),

                programs_opt = {
                    def_text: '<?php echo JText::_("PLG_PLOTGROUPS_PROGRAMS_LIST_TITLE") ?>',
                    icon: '<svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>"  viewBox="0 0 41 32"><use xlink:href="#cubes"></use></svg>',
                };
            plotEasysocial.Content.getProgramsById(elId, programs_opt);
            //removeActiveClass();
            jQuery('.es-content-wrap').trigger("scroll");

        }

        function removeActiveClass() {
            jQuery('.widget-list').each(function () {
                jQuery(this).find('li').removeClass('active');
            });
        }
        function addActivePlotClass(className) {
            console.log('12');
            jQuery('.' + className).addClass('active');
        }


        (function () {
            var proxied = window.XMLHttpRequest.prototype.send;
            window.XMLHttpRequest.prototype.send = function () {
                //console.log( arguments );
                //Here is where you can add any code to process the request.
                //If you want to pass the Ajax request object, pass the 'pointer' below
                var pointer = this
                var intervalId = window.setInterval(function () {
                    if (pointer.readyState != 4) {
                        return;
                    }
                    //console.log( pointer.responseText );
                    replaceMemberWidget(jQuery('.widget-list-grid'));
                    replaceMemberWidget(jQuery('.es-content').find('ul.group-members'));
                    replaceLinkInStream(jQuery('.es-stream-title'));

                    //Here is where you can add any code to process the response.
                    //If you want to pass the Ajax request object, pass the 'pointer' below
                    clearInterval(intervalId);

                }, 1);//I found a delay of 1 to be sufficient, modify it as you need.
                return proxied.apply(this, [].slice.call(arguments));
            };

        })();
        <?php
        $content = ob_get_clean();
        return $content;
    }

}


