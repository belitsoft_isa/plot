<?php
/**
 * Attachments plugin for inserting attachments lists into content
 *
 * @package     Attachments
 * @subpackage  Main_Attachments_Plugin
 *
 * @author      Jonathan M. Cameron <jmcameron@jmcameron.net>
 * @copyright   Copyright (C) 2007-2013 Jonathan M. Cameron, All Rights Reserved
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://joomlacode.org/gf/project/attachments/frs/
 */

defined('_JEXEC') or die('Restricted access');


/**
 * Attachments plugin
 *
 * @package  Attachments
 * @since    1.3.4
 */
class plgSystemQuizquestions extends JPlugin
{

    protected $autoloadLanguage = true;


    function onBeforeCompileHead()
    {
        if (JFactory::getApplication()->isAdmin()) {
            $jinput = JFactory::getApplication()->input;
            $option = $jinput->get('option', '');
            $view = $jinput->get('view', '');
            if ($option == 'com_joomlaquiz' && $view == 'questions') {
                $document = JFactory::getDocument();
                $script = $this->prepareScript();
                $document->addScriptDeclaration($script);
            }
        }
    }

    function prepareScript()
    {

        ob_start();
        ?>
        jQuery(document).ready(function(){
            jQuery('#questionsList').find('td.has-context a').each(function(){
                if(jQuery(this).attr('title')!='' && jQuery(this).text()==''){
                var q_title=jQuery(this).attr('title'),
                    symbols=60;
                 if(q_title.length>symbols){
                    jQuery(this).text(q_title.substr(0, symbols)+'...');
                 }else{
                    jQuery(this).text(q_title);
                 }

                }
            });
        });
        <?php
        $content = ob_get_clean();
        return $content;
    }

}
