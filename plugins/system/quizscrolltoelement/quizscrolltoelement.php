<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

class plgSystemQuizscrolltoelement extends JPlugin
{

    function onAfterInitialise()
    {

        if (!JFactory::getApplication()->isAdmin()) {
            $jinput = JFactory::getApplication()->input;
            $option = $jinput->get('option', '');
            $task = $jinput->get('task', '');
            $view= $jinput->get('view', '');
            if ($option == "com_joomlaquiz" &&  $view == "quiz") {
                $doc = JFactory::getDocument();

                $doc->addScriptDeclaration(
                    "jQuery(document).ready(function(){".
                    "var code = ".
                   "function ScrollToElement(theElement){
                       var selectedPosX = 0;
                       var selectedPosY = 0;
                       while(theElement != null){
                           try{
                              // selectedPosX += theElement.offsetLeft;
                              // selectedPosY += theElement.offsetTop;
                               theElement = theElement.offsetParent;
                           } catch(e){}
                       }
                       try{
                           window.scrollTo(selectedPosX,selectedPosY-10);
                       } catch(e){}

                   }".
                   ", ".
                    "script = document.createElement('script');".
                    "script.setAttribute('type', 'text/javascript');".
                    "script.appendChild(document.createTextNode(code));".
                    "document.body.appendChild(script);".
                    "});"
                );
           }
        }
    }


}

?>
