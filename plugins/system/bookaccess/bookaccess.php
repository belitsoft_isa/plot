<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

class plgSystemBookaccess extends JPlugin
{

    function onAfterInitialise()
    {


        if (!JFactory::getApplication()->isAdmin()) {
            $jinput = JFactory::getApplication()->input;
            $option = $jinput->get('option', '');
            $view = $jinput->get('view', '');
            $id = $jinput->get('id', 0, 'INT');
            $curr=FRoute::current();
            $arr=explode('/',$curr);
            $url_options=$this->HTML5FlippingBookParseRoute($arr);

            if (isset($url_options['options']) && $url_options['options'] == "html5flippingbook" && isset($url_options['view']) && $url_options['view'] == "publication" && isset($url_options['id']) && (!plotUser::factory()->isSiteAdmin()) && !(int)$this->bookReadAccess($url_options['id']) && !plotUser::factory()->isBookAuthor($url_options['id'])) {
if((int)$url_options['id']){
                $language = JFactory::getLanguage();
                $language->load('com_html5flippingbook', JPATH_SITE, null, true);
                ?>
                <div>
                    Доступ запрещен
                    <br/><br/>
                </div>
                <?php die;
            }}
        }

    }




    function bookReadAccess($book_id)
    {
        $user = plotUser::factory();
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->clear()
            ->select('id')
            ->from('`#__plot_books` ')
            ->where('book_id=' . (int)$book_id)
            ->where('(parent_id=' . (int)$user->id . ' OR child_id=' . (int)$user->id . ')');
        $db->setQuery($query);
        return $db->loadResult();
    }





    function HTML5FlippingBookParseRoute($segments)
    {
        $vars = array();


if(count($segments)>3) {
    switch ($segments[3]) {
        case 'publication': {
            $numSegments = count($segments);
            $vars['options'] = $segments[2];
            $vars['view'] = $segments[3];
            if ($numSegments > 5) $vars['id'] = $segments[5];
            if ($numSegments > 6) $vars['tmpl'] = (empty($segments[6]) ? ' component' : $segments[6]);
            else
                $vars['tmpl'] = 'component';
            break;
        }

        case 'css':
            $vars['options'] = $segments[2];
            $vars['view'] = 'html5flippingbook';
            $vars['tmpl'] = 'component';
            $vars['task'] = 'templatecss';
            $vars['template_id'] = str_replace('.css', '', $segments[1]);
            break;
    }
}
        return $vars;
    }





}

?>
