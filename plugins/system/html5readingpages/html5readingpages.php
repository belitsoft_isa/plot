<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

class plgSystemHtml5readingpages extends JPlugin
{

    function onAfterInitialise()
    {
        $jinput = JFactory::getApplication()->input;

        $option = $jinput->get('option', '');
        $task = $jinput->get('task', '');
        $curr = FRoute::current();
        $arr = explode('/', $curr);

        $url_options = $this->HTML5FlippingBookParseRoute($arr);

        if (
            isset($url_options['options']) && 
            $url_options['options'] == 'html5flippingbook' && 
            isset($url_options['id']) && 
            isset($url_options['view']) && 
            $url_options['view'] == 'publication'
            #$url_options['id'] == 'index.php?option=com_html5flippingbook&task=userPublAction&tmpl=component'
        ) {
            $page = $jinput->getInt('page', 0);
            $publID = $jinput->getInt('pubID', 0);
            $response = array();
            $update = FALSE;
            if ($page != 0) {
                $db = JFactory::$database;
                $user = JFactory::getUser();
                $query = $db->getQuery(true)
                        ->select('`read`')
                        ->from('`#__plot_books`')
                        ->where('`child_id` = '.$user->get('id').' AND `book_id` = '.$publID);
                $db->setQuery($query);
                $read = $db->loadResult();
                $query = $db->getQuery(true)
                        ->update('`#__plot_books`')
                        ->set("`read` = 0")
                        ->where('`child_id` = '.$user->get('id').' AND `book_id` = '.$publID);
                $db->setQuery($query);

                try {
                    $db->execute();
                    $response = array("error" => 0, "message" => "SUCCESS");
                } catch (RuntimeException $e) {
                    $response = array("error" => 1, "message" => $e->getMessage());
                }

                if ($read != 1) {
                    //get pages
                    $query = $db->getQuery(true)
                            ->select('params')
                            ->from('`#__plot_user_pages`')
                            ->where('`user_id` = '.$user->get('id').' AND `book_id` = '.$publID);
                    $db->setQuery($query);
                    $params = $db->loadResult();
                    if ($params) {
                        $params = json_decode($params, true);
                        if (!in_array($page, $params)) {
                            array_push($params, $page);
                            if ($page % 2 == 0 && $page != 2) {
                                if (!in_array($page - 1, $params)) {
                                    array_push($params, $page - 1);
                                }
                            }
                            $query = $db->getQuery(true)
                                    ->update('`#__plot_user_pages`')
                                    ->set("`params` = '".json_encode($params, JSON_FORCE_OBJECT)."'")
                                    ->where('`user_id` = '.$user->get('id').' AND `book_id` = '.$publID);

                            $db->setQuery($query);
                            try {
                                $db->execute();
                                $response = array("error" => 0, "message" => "SUCCESS");
                            } catch (RuntimeException $e) {
                                $response = array("error" => 1, "message" => $e->getMessage());
                            }
                        }
                    } else {
                        $pages = array();
                        $pages[] = $page;
                        $record = new stdClass();
                        $record->user_id = $user->get('id');
                        $record->book_id = $publID;
                        $record->params = json_encode($pages, JSON_FORCE_OBJECT);
                        $db->insertObject('#__plot_user_pages', $record);
                    }
                }
            }
        }
    }

    function HTML5FlippingBookParseRoute($segments)
    {
        $vars = array();

        if (count($segments) > 3) {
            switch ($segments[3]) {
                case 'publication': {
                        $numSegments = count($segments);
                        $vars['options'] = $segments[2];
                        $vars['view'] = $segments[3];
                        if ($numSegments > 5)
                            $vars['id'] = $segments[5];
                        if ($numSegments > 6)
                            $vars['tmpl'] = (empty($segments[6]) ? ' component' : $segments[6]);
                        else
                            $vars['tmpl'] = 'component';
                        break;
                    }

                case 'css':
                    $vars['options'] = $segments[2];
                    $vars['view'] = 'html5flippingbook';
                    $vars['tmpl'] = 'component';
                    $vars['task'] = 'templatecss';
                    $vars['template_id'] = str_replace('.css', '', $segments[1]);
                    break;
            }
        }
        
        return $vars;
    }

}
