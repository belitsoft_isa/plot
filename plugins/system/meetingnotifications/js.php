<?php
defined('_JEXEC') or die;
$link = JRoute::_('index.php?option=com_plot&task=notifications.replaceText');
$essaylink = JRoute::_('index.php?option=com_plot&task=essay.replaceText');
$courselink = JRoute::_('index.php?option=com_plot&task=course.replaceText');
$coursefinishedlink = JRoute::_('index.php?option=com_plot&task=course.courseFinishedReplaceText');
$friendreuestlink = JRoute::_('index.php?option=com_plot&task=profile.friendRequestReplaceText');
$friendrejectlink = JRoute::_('index.php?option=com_plot&task=profile.friendRejectReplaceText');
$friendapprovelink = JRoute::_('index.php?option=com_plot&task=profile.friendApproveReplaceText');
$bookmoneylink = JRoute::_('index.php?option=com_plot&task=essay.bookMoneyReplaceText');
$essayapprovedlink = JRoute::_('index.php?option=com_plot&task=essay.essayApprovedReplaceText');
$essayrejectedlink = JRoute::_('index.php?option=com_plot&task=essay.essayRejectedReplaceText');
$programfinishedlink = JRoute::_('index.php?option=com_plot&task=program.programFinishedReplaceText');
$bookreadedlink = JRoute::_('index.php?option=com_plot&task=publication.bookReadedReplaceText');
$coursepassedlink = JRoute::_('index.php?option=com_plot&task=course.coursePassedReplaceText');
$coursegrtmoneylink = JRoute::_('index.php?option=com_plot&task=course.courseMoneyReplaceText');
$bookbouylink=JRoute::_('index.php?option=com_plot&task=publication.bookBuyReplaceText');
$coursebouylink=JRoute::_('index.php?option=com_plot&task=course.courseBuyReplaceText');
?>
<script src="<?php echo JUri::root() . '/media/jui/js/jquery.min.js'; ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo JUri::root() . 'media/jui/js/jquery.ui.core.min.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo JUri::root() . 'plugins/system/meetingnotifications/waitsync.js'; ?>"></script>

<script>
function showMessagesArea() {
    jQuery('ul.conversation-list').show();
}

function isExistElements(arr, replaced_text) {
    jQuery(document.getElementById('fd')).find('.es-container').find('.es-content').find('ul.conversation-list').find('li.conversation-item').each(function () {
        var li_content = jQuery(this).find('.content-column').find('.conversation-meta').html();

        if (li_content.indexOf(replaced_text) != -1) {//if (li_content.indexOf(replaced_text) != -1) {
            //console.log(replaced_text)
            arr.push(this);
        }

    });

    return arr;
}

jQuery(document).ready(function () {

    var vulture = new WaitSync(function () {
        showMessagesArea();
    });


    var arr_eaasy = new Array(),
        arr_coursevideo = new Array(),
        arr_notification = new Array(),
        arr_friendrequest = new Array(),
        arr_friendreject = new Array(),
        arr_friendapprove = new Array(),
        arr_bookmoney = new Array(),
        arr_essayapproved = new Array(),
        arr_essayrejected = new Array(),
        arr_bookreaded = new Array(),
        arr_programfinished = new Array(),
        arr_coursefinished= new Array(),
        arr_coursepassed= new Array(),
        arr_coursemoney= new Array(),
        arr_bookbuy= new Array(),
        arr_coursebuy= new Array();


    arr_eaasy = isExistElements(arr_eaasy, 'plot-essay-add');
    arr_coursevideo = isExistElements(arr_coursevideo, 'plot-coursevideo');
    arr_notification = isExistElements(arr_notification, 'plot-notification');
    arr_friendrequest = isExistElements(arr_friendrequest, 'plot-user-friend-request');
    arr_friendreject = isExistElements(arr_friendreject, 'plot-user-friend-reject');
    arr_friendapprove = isExistElements(arr_friendapprove, 'plot-user-friend-approve');
    arr_bookmoney = isExistElements(arr_bookmoney, 'plot-money-book-get');
    arr_essayapproved = isExistElements(arr_essayapproved, 'plot-essay-approved');
    arr_essayrejected = isExistElements(arr_essayrejected, 'plot-essay-reject');
    arr_bookreaded = isExistElements(arr_bookreaded, 'plot-book-readed');
    arr_programfinished = isExistElements(arr_programfinished, 'plot-program-finished');
    arr_coursefinished= isExistElements(arr_coursefinished, 'plot-course-finished');
    arr_coursepassed=isExistElements(arr_coursepassed, 'plot-course-passed');
    arr_coursemoney = isExistElements(arr_coursemoney, 'plot-money-course-get');
    arr_bookbuy = isExistElements(arr_bookbuy, 'plot-book-buy'),
        arr_coursebuy = isExistElements(arr_coursebuy, 'plot-course-buy');

    function socialReplace(send_url, replaced_text) {

        var plot_ids = [];
        var thats = [];
        var liContArr = [];

        jQuery('#fd').find('.es-container').find('.es-content').find('ul.conversation-list').find('li.conversation-item').each(function () {
            var li_content = jQuery(this).find('.content-column').find('.conversation-meta').text().trim(),
                essay_id = 0;

            var replacedLength = replaced_text.length;


            if (li_content.indexOf(replaced_text) != -1) {
                liContArr.push(li_content);
                essay_id = li_content.replace(replaced_text + '-', '');
                plot_ids.push(essay_id);
                thats.push(this);
            }

        });

        if (plot_ids.length) {
            jQuery.post(send_url, {ids: plot_ids},
                vulture.wrap(
                    function (response) {
                        var data = jQuery.parseJSON(response),
                            count_array = thats.length,
                            i;
                        if (data.length) {
                            for (i = 0; i < count_array; i++) {
                                if (data[i]) {
//                                        if (jQuery(thats[i]).find('.content-column').find('.conversation-meta').text().trim() == liContArr[i]) {
                                    jQuery(thats[i]).find('.content-column').find('.conversation-meta').html(
                                        data[i].msg
                                    );
//                                        }
                                }

                            }

                            SqueezeBox.initialize();
                        }

                    })
            );

        }


    }


    if (arr_coursebuy.length==0 || arr_bookbuy.length==0 || arr_coursemoney.length==0 && arr_coursepassed.length==0 && arr_coursefinished.length ==0 && arr_eaasy.length == 0 && arr_coursevideo.length == 0 && arr_notification.length == 0 && arr_friendrequest.length == 0 && arr_friendreject.length == 0 && arr_friendapprove.length == 0 && arr_bookmoney==0 && arr_essayapproved==0 && arr_essayrejected==0 && arr_bookreaded==0 && arr_programfinished==0) {
        showMessagesArea();
    }

    if (arr_coursebuy.length != 0) {

        socialReplace('<?php echo  $coursebouylink ; ?>', 'plot-course-buy');
    }

    if (arr_bookbuy.length != 0) {

        socialReplace('<?php echo  $bookbouylink ; ?>', 'plot-book-buy');
    }

    if (arr_coursepassed.length != 0) {

        socialReplace('<?php echo  $coursepassedlink ; ?>', 'plot-course-passed');
    }

    if (arr_coursemoney.length != 0) {

        socialReplace('<?php echo  $coursegrtmoneylink; ?>', 'plot-money-course-get');
    }

    if (arr_coursefinished.length != 0) {

        socialReplace('<?php echo  $coursefinishedlink; ?>', 'plot-course-finished');
    }

    if (arr_eaasy.length != 0) {

        socialReplace('<?php echo  $essaylink; ?>', 'plot-essay-add');
    }
    if (arr_coursevideo.length != 0) {

        //replace coursevideo
        socialReplace('<?php echo  $courselink; ?>', 'plot-coursevideo');
    }
    if (arr_notification.length != 0) {

        //replace notification
        socialReplace('<?php echo  $link; ?>', 'plot-notification');
    }
    if (arr_friendrequest.length != 0) {

        //replace friend request
        socialReplace('<?php echo  $friendreuestlink; ?>', 'plot-user-friend-request');
    }
    if (arr_friendreject.length != 0) {

        //replace friend reject
        socialReplace('<?php echo  $friendrejectlink; ?>', 'plot-user-friend-reject');
    }
    if (arr_friendapprove.length != 0) {

        //replace friend approve
        socialReplace('<?php echo  $friendapprovelink; ?>', 'plot-user-friend-approve');
    }

    if (arr_bookmoney.length != 0) {
        //replace money for book get
        socialReplace('<?php echo  $bookmoneylink; ?>', 'plot-money-book-get');
    }


    if (arr_essayapproved.length != 0) {
        //replace essay approved
       socialReplace('<?php echo  $essayapprovedlink; ?>', 'plot-essay-approved');
    }

    if (arr_essayrejected.length != 0) {
        //replace essay rejected
        socialReplace('<?php echo  $essayrejectedlink; ?>', 'plot-essay-reject');
    }

    if (arr_bookreaded.length != 0) {
        //replace book readed
        socialReplace('<?php echo $bookreadedlink; ?>', 'plot-book-readed');
    }

    if (arr_programfinished.length != 0) {
        //replace program finished
        socialReplace('<?php echo $programfinishedlink; ?>', 'plot-program-finished');
    }


});


(function () {
    var proxied = window.XMLHttpRequest.prototype.send;
    var vulture = new WaitSync(function () {
        showMessagesArea();
    });


    var arr_eaasy = new Array(),
        arr_coursevideo = new Array(),
        arr_notification = new Array(),
        arr_friendrequest = new Array(),
        arr_friendreject = new Array(),
        arr_friendapprove = new Array(),
        arr_bookmoney = new Array(),
        arr_essayapproved = new Array(),
        arr_essayrejected = new Array(),
        arr_bookreaded = new Array(),
        arr_programfinished = new Array(),
        arr_coursefinished= new Array(),
        arr_coursepassed= new Array(),
        arr_coursemoney= new Array(),
        arr_bookbuy= new Array(),
        arr_coursebuy= new Array();


    window.XMLHttpRequest.prototype.send = function () {
        console.log( arguments );
        arr_eaasy = isExistElements(arr_eaasy, 'plot-essay-add');
        arr_coursevideo = isExistElements(arr_coursevideo, 'plot-coursevideo');
        arr_notification = isExistElements(arr_notification, 'plot-notification');
        arr_friendrequest = isExistElements(arr_friendrequest, 'plot-user-friend-request');
        arr_friendreject = isExistElements(arr_friendreject, 'plot-user-friend-reject');
        arr_friendapprove = isExistElements(arr_friendapprove, 'plot-user-friend-approve');
        arr_bookmoney = isExistElements(arr_bookmoney, 'plot-money-book-get');
        arr_essayapproved = isExistElements(arr_essayapproved, 'plot-essay-approved');
        arr_essayrejected = isExistElements(arr_essayrejected, 'plot-essay-reject');
        arr_bookreaded = isExistElements(arr_bookreaded, 'plot-book-readed');
        arr_programfinished = isExistElements(arr_programfinished, 'plot-program-finished');
        arr_coursefinished= isExistElements(arr_coursefinished, 'plot-course-finished');
        arr_coursepassed=isExistElements(arr_coursepassed, 'plot-course-passed');
        arr_coursemoney = isExistElements(arr_coursemoney, 'plot-money-course-get');
        arr_bookbuy = isExistElements(arr_bookbuy, 'plot-book-buy'),
            arr_coursebuy = isExistElements(arr_coursebuy, 'plot-course-buy');

        var pointer = this
        var intervalId = window.setInterval(function () {
            if (pointer.readyState != 4) {
                return;
            }
            console.log( pointer.responseText );

            if (arr_coursebuy.length != 0) {

                socialReplace('<?php echo  $coursebouylink ; ?>', 'plot-course-buy');
            }

            if (arr_bookbuy.length != 0) {

                socialReplace('<?php echo  $bookbouylink ; ?>', 'plot-book-buy');
            }

            if (arr_coursepassed.length != 0) {

                socialReplace('<?php echo  $coursepassedlink ; ?>', 'plot-course-passed');
            }

            if (arr_coursemoney.length != 0) {

                socialReplace('<?php echo  $coursegrtmoneylink; ?>', 'plot-money-course-get');
            }

            if (arr_coursefinished.length != 0) {

                socialReplace('<?php echo  $coursefinishedlink; ?>', 'plot-course-finished');
            }

            if (arr_eaasy.length != 0) {

                socialReplace('<?php echo  $essaylink; ?>', 'plot-essay-add');
            }
            if (arr_coursevideo.length != 0) {

                //replace coursevideo
                socialReplace('<?php echo  $courselink; ?>', 'plot-coursevideo');
            }
            if (arr_notification.length != 0) {

                //replace notification
                socialReplace('<?php echo  $link; ?>', 'plot-notification');
            }
            if (arr_friendrequest.length != 0) {

                //replace friend request
                socialReplace('<?php echo  $friendreuestlink; ?>', 'plot-user-friend-request');
            }
            if (arr_friendreject.length != 0) {

                //replace friend reject
                socialReplace('<?php echo  $friendrejectlink; ?>', 'plot-user-friend-reject');
            }
            if (arr_friendapprove.length != 0) {

                //replace friend approve
                socialReplace('<?php echo  $friendapprovelink; ?>', 'plot-user-friend-approve');
            }

            if (arr_bookmoney.length != 0) {
                //replace money for book get
                socialReplace('<?php echo  $bookmoneylink; ?>', 'plot-money-book-get');
            }


            if (arr_essayapproved.length != 0) {
                //replace essay approved
                socialReplace('<?php echo  $essayapprovedlink; ?>', 'plot-essay-approved');
            }

            if (arr_essayrejected.length != 0) {
                //replace essay rejected
                socialReplace('<?php echo  $essayrejectedlink; ?>', 'plot-essay-reject');
            }

            if (arr_bookreaded.length != 0) {
                //replace book readed
                socialReplace('<?php echo $bookreadedlink; ?>', 'plot-book-readed');
            }

            if (arr_programfinished.length != 0) {
                //replace program finished
                socialReplace('<?php echo $programfinishedlink; ?>', 'plot-program-finished');
            }

            clearInterval(intervalId);
        }, 1);//I found a delay of 1 to be sufficient, modify it as you need.
        return proxied.apply(this, [].slice.call(arguments));
    };

    function socialReplace(send_url, replaced_text) {

        var plot_ids = [];
        var thats = [];
        var liContArr = [];

        jQuery('#fd').find('.es-container').find('.es-content').find('ul.conversation-list').find('li.conversation-item').each(function () {
            var li_content = jQuery(this).find('.content-column').find('.conversation-meta').text().trim(),
                essay_id = 0;

            var replacedLength = replaced_text.length;


            if (li_content.indexOf(replaced_text) != -1) {
                liContArr.push(li_content);
                essay_id = li_content.replace(replaced_text + '-', '');
                plot_ids.push(essay_id);
                thats.push(this);
            }

        });

        if (plot_ids.length) {
            jQuery.post(send_url, {ids: plot_ids},
                vulture.wrap(
                    function (response) {
                        var data = jQuery.parseJSON(response),
                            count_array = thats.length,
                            i;
                        if (data.length) {
                            for (i = 0; i < count_array; i++) {
                                if (data[i]) {
//                                        if (jQuery(thats[i]).find('.content-column').find('.conversation-meta').text().trim() == liContArr[i]) {
                                    jQuery(thats[i]).find('.content-column').find('.conversation-meta').html(
                                        data[i].msg
                                    );
//                                        }
                                }

                            }

                            SqueezeBox.initialize();
                        }

                    })
            );

        }


    }
})();


</script>
<style>
    div.top div.wrap {
        height: auto !important;
    }
</style>