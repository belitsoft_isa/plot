<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');
require_once(JPATH_ROOT . '/administrator/components/com_plot/plot_config.php');
require_once(JPATH_ROOT . '/components/com_plot/helpers/plot.php');

class plgSystemHtml5replacescripts extends JPlugin
{
//----------------------------------------------------------------------------------------------------

    public function __construct(&$subject, $config = array())
    {
        parent::__construct($subject, $config);

    }



    public function onBeforeCompileHead()
    {

        if (!JFactory::getApplication()->isAdmin()) {
            $jinput = JFactory::getApplication()->input;
            $option = $jinput->get('option', '');
            $view = $jinput->get('view', '');
            $layout = $jinput->get('layout', '');
            $id = $jinput->get('id', 0, 'INT');
            $fullscreen = $jinput->get('fullscreen', 'INT', 0);

            if ($option == 'com_html5flippingbook' && $view == 'publication' && $layout == 'mobile') {
                $doc = JFactory::getDocument();
                $doc->addScript(JUri::root() . "media/system/js/mootools-core.js");
                $doc->addScript(JUri::root() . "media/system/js/core.js");
                $doc->addScript(JUri::root() . "media/system/js/mootools-more.js");
                $doc->addScript(JUri::root() . "media/system/js/modal.js");

            }


           if ($option == 'com_html5flippingbook' && $view == 'publication' && $layout == 'mobile') {
               $quizId= $this->getQuizIdByBookId((int)$id);
               $doc = JFactory::getDocument();
               $style = '.plot-child, .plot-parent {line-height: 20px; font-size: 14px; margin: 0 20px 0 0; border-radius: 3px; float: right;}'
               .'.plot-child{border: 1px solid #82442F; color: #FCA83A; background-color: #82442F; padding: 8px 10px 8px 16px;}'
               .'button:hover{cursor: pointer;-webkit-box-shadow: 4px 4px 5px 0px rgba(50, 50, 50, 0.75); -moz-box-shadow:    4px 4px 5px 0px rgba(50, 50, 50, 0.75); box-shadow: 4px 4px 5px 0px rgba(50, 50, 50, 0.75); -webkit-transition: all 0.5s ease; -moz-transition: all 0.5s ease; -o-transition: all 0.5s ease;}'
               .'button svg{width: 20px; height: 20px; float: left; margin: 0 8px 0 -8px; }'
               .'.plot-parent svg{display: none;}'
               .'.plot-parent{border: 1px solid #007bb2;background: -moz-linear-gradient(top,  rgba(127,208,232,1) 0%, rgba(0,112,140,1) 100%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(127,208,232,1)), color-stop(100%,rgba(0,112,140,1))); background: -webkit-linear-gradient(top,  rgba(127,208,232,1) 0%,rgba(0,112,140,1) 100%); background: -o-linear-gradient(top,  rgba(127,208,232,1) 0%,rgba(0,112,140,1) 100%); background: -ms-linear-gradient(top,  rgba(127,208,232,1) 0%,rgba(0,112,140,1) 100%); background: linear-gradient(to bottom,  rgba(127,208,232,1) 0%,rgba(0,112,140,1) 100%); color:#fff; padding: 8px 10px;}'
               .'#mainFlipBookDiv .flipbook .odd .gradient, #mainFlipBookDiv .flipbook .even .gradient {width: 420px;}'
               .'.flipbook-page{padding: 20px;}';
               $doc->addStyleDeclaration( $style );
               $doc->addScriptDeclaration(
                   "jQuery(document).ready(function(){" .
                   "jQuery('#page-header a.ui-icon-home').attr('href','" . JRoute::_('index.php?option=com_plot&view=profile') . "')" .
                   "});"
               );
          }

           if ($option == 'com_html5flippingbook' && $view == 'publication' && $id) {

               $quizId= $this->getQuizIdByBookId((int)$id);
               $link=JRoute::_('index.php?option=com_joomlaquiz&view=quiz&quiz_id='.(int)$quizId);
               $button='';
               if(PlotHelper::quizAccess((int)$quizId) || plotUser::factory()->isSiteAdmin() || plotUser::factory()->isBookAuthor($quizId)) {
//                    if(plotUser::factory()->isParent()){
//                        $button='<button id="plot-test" class="plot-parent add hover-shadow" id="plot-test">Пройти тест</button>';
//                    }else{
//                        $button = '<button id="plot-test" class="plot-child hover-shadow" >';
//                        $button .= '<svg viewBox="0 0 57.5 57.1" preserveAspectRatio="xMidYMid meet">';
//                        $button .= '<use xlink:href="#test"></use>';
//                        $button .= '</svg>';
//                        $button .= '<span>Пройти тест</span>';
//                        $button .= '</button>';
//                    }


               }
               $doc = JFactory::getDocument();
               $doc->addScriptDeclaration(
                   "jQuery(document).ready(function(){" .
                   "jQuery('.fa-times').attr('onclick','window.location.href=\'" . JRoute::_('index.php?option=com_plot&view=publication&bookId=' . (int)$id) . "\'');" .
                   "jQuery('.fb_topBar').find('.tb_social').prepend('".$button."');".
                   "jQuery('#plot-test').on('click',function(){".
                   "window.location.href='".$link."';".
                   "})".
                   "});"
               );

               $style = '.plot-child, .plot-parent {line-height: 20px; font-size: 14px; margin: 0 20px 0 0; border-radius: 3px; float: right;}'
                   .'.plot-child{border: 1px solid #82442F; color: #FCA83A; background-color: #82442F; padding: 8px 10px 8px 16px;}'
                   .'button:hover{cursor: pointer;-webkit-box-shadow: 4px 4px 5px 0px rgba(50, 50, 50, 0.75); -moz-box-shadow:    4px 4px 5px 0px rgba(50, 50, 50, 0.75); box-shadow: 4px 4px 5px 0px rgba(50, 50, 50, 0.75); -webkit-transition: all 0.5s ease; -moz-transition: all 0.5s ease; -o-transition: all 0.5s ease;}'
                   .'button svg{width: 20px; height: 20px; float: left; margin: 0 8px 0 -8px; }'
                   .'.plot-parent svg{display: none;}'
                   .'.plot-parent{border: 1px solid #007bb2;background: -moz-linear-gradient(top,  rgba(127,208,232,1) 0%, rgba(0,112,140,1) 100%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(127,208,232,1)), color-stop(100%,rgba(0,112,140,1))); background: -webkit-linear-gradient(top,  rgba(127,208,232,1) 0%,rgba(0,112,140,1) 100%); background: -o-linear-gradient(top,  rgba(127,208,232,1) 0%,rgba(0,112,140,1) 100%); background: -ms-linear-gradient(top,  rgba(127,208,232,1) 0%,rgba(0,112,140,1) 100%); background: linear-gradient(to bottom,  rgba(127,208,232,1) 0%,rgba(0,112,140,1) 100%); color:#fff; padding: 8px 10px;}'
                   .'#mainFlipBookDiv .flipbook .odd .gradient, #mainFlipBookDiv .flipbook .even .gradient {width: 420px;}'
                   .'.flipbook-page{padding: 20px 20px 20px 25px;}';
               $doc->addStyleDeclaration( $style );


           }
       }


    }

    function getQuizIdByBookId($bookId){
        $database = Foundry::db();
        $query = $database->getQuery(true);
        $query->select('a.id_quiz');
        $query->from('`#__plot_book_quiz_map` AS a');
        $query->where('a.`id_pub` = ' . (int)$bookId);
        $database->setQuery($query);
        $quz_id = (int)$database->loadResult();
       return $quz_id;
    }


    function HTML5FlippingBookParseRoute($segments)
    {
        $vars = array();


        if (count($segments) > 3) {
            switch ($segments[3]) {
                case 'publication': {
                    $numSegments = count($segments);
                    $vars['options'] = $segments[2];
                    $vars['view'] = $segments[3];
                    if ($numSegments > 5) $vars['id'] = $segments[5];
                    if ($numSegments > 6) $vars['tmpl'] = (empty($segments[6]) ? ' component' : $segments[6]);
                    else
                        $vars['tmpl'] = 'component';
                    break;
                }

                case 'css':
                    $vars['options'] = $segments[2];
                    $vars['view'] = 'html5flippingbook';
                    $vars['tmpl'] = 'component';
                    $vars['task'] = 'templatecss';
                    $vars['template_id'] = str_replace('.css', '', $segments[1]);
                    break;
            }
        }
        return $vars;
    }

}

?>
