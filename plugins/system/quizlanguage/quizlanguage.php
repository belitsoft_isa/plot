<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

class plgSystemQuizlanguage extends JPlugin
{

    function onAfterInitialise()
    {
        if (!JFactory::getApplication()->isAdmin()) {
            JFactory::getLanguage()->load('custom', JPATH_SITE, null, true);

        }
    }


}

?>
