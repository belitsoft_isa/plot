<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

class plgSystemHtml5crop extends JPlugin
{

    function onAfterInitialise()
    {

        if (JFactory::getApplication()->isAdmin()) {
            $jinput = JFactory::getApplication()->input;
            $option = $jinput->get('option', '');
            $view = $jinput->get('view', '');
            $layout = $jinput->get('layout', '');
            $id = $jinput->get('id', 0, 'INT');

            if ($option == "com_html5flippingbook" && $view == "publication" && $layout == "edit") {
                $doc = JFactory::getDocument();

                $doc->addScriptDeclaration(
                    "jQuery(document).ready(function(){" .
                    "count_load=0;".
                    "jQuery('body').append('<a class=\'modal\' id=\'plot-crop-form\' href=\'#\' ></a>');" .
                    "jQuery('#jform_c_thumb_preview').on('load', function(){" .
                    "if(count_load<1){".
                    "count_load++;".
                    "}else{".
                    "var img_src=jQuery('#jform_c_thumb_preview').attr('src')," .
                    "link='index.php?option=com_plot&tmpl=component&task=resolution.ajaxForm&img='+img_src;" .
                    "if(img_src!='".JUri::root()."administrator/components/com_html5flippingbook/assets/thumbnails/no_image.png'){".
                    "jQuery('#plot-crop-form').attr('href',link);" .
                    "jQuery('#plot-crop-form').attr('rel','{handler:\"iframe\", size: {x: 744, y: 525}}');" .
                    "document.getElementById('plot-crop-form').click();".
                    "}".
                    "}".
                    "}); " .
                    "});"
                );


            }
        }

    }


}

?>
