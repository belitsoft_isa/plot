<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

class plgSystemCoursecrop extends JPlugin
{

    function onAfterInitialise()
    {
        if (JFactory::getApplication()->isAdmin()) {
            $jinput = JFactory::getApplication()->input;
            $option = $jinput->get('option', '');
            $view = $jinput->get('view', '');
            $layout = $jinput->get('layout', '');
            $id = $jinput->get('id', 0, 'INT');

            if ($option == "com_plot" && $view == "course" && $layout == "edit") {
                $doc = JFactory::getDocument();
$doc->addScriptDeclaration(
                    "jQuery(document).ready(function(){" .
                        "count_load=0;".
                        "jQuery('body').append('<a class=\'modal\' id=\'plot-crop-form\' href=\'#\' ></a>');" .
                        "jQuery('#jform_image').on('change', function(){" .
                            "window.parent.SqueezeBox.close();".
                        "var img_src=jQuery('#jform_image').val()," .
                        "link='index.php?option=com_plot&tmpl=component&task=resolution.ajaxFormCourse&img='+img_src;" .
                        "jQuery('#plot-crop-form').attr('href',link);" .
                        "jQuery('#plot-crop-form').attr('rel','{handler:\"iframe\", size: {x: 744, y: 525}}');" .
                        "document.getElementById('plot-crop-form').click();".


                        "}); " .
                    "});"
                );
            }
        }

    }


}

?>
