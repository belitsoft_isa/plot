<?php
// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

class plgContentjlmsSqueezeBox extends JPlugin
{
	function plgContentjlmsSqueezeBox( &$subject, $params )
	{
		parent::__construct( $subject, $params );
	}
	
	function onContentPrepare($context, &$article, &$params, $limitstart )
	{
		$this->onPrepareContent($article, $params, $limitstart);
	}
	
	function onPrepareContent( &$article, &$params, $limitstart )
	{
		
		$source_file_base 	= JURI::base();
		
		jimport('joomla.html.parameter');
		
		$plugin				= JPluginHelper::getPlugin('content', 'jlmssqueezebox');
		if(class_exists('JParameter')) {
			$pluginParams		= new JParameter( $plugin->params );
		} else {
			$pluginParams		= $this->params;
		}
		
		$p_width = $pluginParams->get('width', 750);
		$p_height = $pluginParams->get('height', 540);
		$p_zoom_icons = $pluginParams->get('zoom_icons', 1);
		
		$document = JFactory::getDocument();
		
		if(!defined('_JLMS_DONTUSE_IONCUBED_FILES')) {
			if (file_exists(JPATH_SITE . DS . 'components' . DS . 'com_joomla_lms' . DS . 'validatejlmsioncubelicense.php')) {
				require_once(JPATH_SITE . DS . 'components' . DS . 'com_joomla_lms' . DS . 'validatejlmsioncubelicense.php');
			}
		}
		
		if(!defined('_JLMS_DONTUSE_IONCUBED_FILES') && file_exists(JPATH_SITE . DS . 'components' . DS . 'com_joomla_lms' . DS . 'joomla_lms.php')){
			//JoomlaLMS Functions Include
			if (!defined('DS')) { define( 'DS', DIRECTORY_SEPARATOR ); }
			if (!defined('_JOOMLMS_FRONT_HOME')) { define('_JOOMLMS_FRONT_HOME', JPATH_SITE . DS . 'components' . DS . 'com_joomla_lms'); }
			
			if(!defined('_JLMS_EXEC')){ define('_JLMS_EXEC', 1); }
			
			require_once(_JOOMLMS_FRONT_HOME . DS . 'includes' . DS . 'component.legacy.php');
			require_once(_JOOMLMS_FRONT_HOME . DS . 'includes' . DS . 'lms_legacy.php');
			require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.factory.php");
			
			$GLOBALS['JLMS_DB'] = & JLMSFactory::getDB();
			global $JLMS_DB;
			
			require_once(_JOOMLMS_FRONT_HOME . DS . 'joomla_lms.func.php');
			require_once(_JOOMLMS_FRONT_HOME . DS . 'joomla_lms.main.php');
			require_once(_JOOMLMS_FRONT_HOME . DS . 'includes' . DS . 'classes' . DS . 'lms.acl.php');			
			
			$GLOBALS['JLMS_CONFIG'] = & JLMSFactory::getConfig();
			global $JLMS_CONFIG;
			
			$path_text_process = _JOOMLMS_FRONT_HOME . DS . 'includes' . DS . 'jlms_text_process.php';
			if(file_exists($path_text_process)){
				require_once($path_text_process);
			}
			//JoomlaLMS Functions Include
			JLMS_HTML::_('behavior.mootools');
			JLMS_SqueezeBox();
			
			$text = $article->text;
			$key = 'jlms_box';
			if (strpos($text, '{'.$key) !== false) { //speed up
			    $reg = '#{'.$key.'\s+([a-z]+=(?:"|&quot;)[^}]*(?:"|&quot;))*\s*}([^{]+){/'.$key.'}#';
			    if (preg_match_all($reg, $text, $matches, PREG_PATTERN_ORDER) > 0){
					
			    	$i=0;
			    	foreach($matches[0] as $match){
						$text_search = $matches[0][$i];
						$text_params = strval($matches[1][$i]);
				    	$text_in_tag = $matches[2][$i];
				    	
				    	$suffix = '';
				    	$title = '';
				    	$url_in_tag = '';
				    	$width_value = 0;
				    	$height_value = 0;
				    	
						preg_match_all('#group="([^"]+)"#', $text_params, $matches_suffix, PREG_PATTERN_ORDER);
				    	if(isset($matches_suffix[0][0])){
				    		$suffix = strval($matches_suffix[1][0]);
				    	}
				    	
						preg_match_all('#title="([^"]+)"#', $text_params, $matches_title, PREG_PATTERN_ORDER);
				    	if(isset($matches_title[0][0])){
				    		$title = strval($matches_title[1][0]);
				    	}
						
						preg_match_all('#url="([^"]+)"#', $text_params, $matches_url, PREG_PATTERN_ORDER);
						if(isset($matches_url[0][0])){
							$url_in_tag = strval($matches_url[1][0]);
							
							$helper = 'iframe';
							if(preg_match_all('#\.(jpg|jpeg|png|gif|bmp)$#', $url_in_tag, $ext, PREG_PATTERN_ORDER)){
								$helper = 'image';
							}
						}
						preg_match_all('#width="(\d+)"#', $text_params, $matches_width, PREG_PATTERN_ORDER);
						if(isset($matches_width[0][0])){
							$width_value = intval($matches_width[1][0]);	
						}
						preg_match_all('#height="(\d+)"#', $text_params, $matches_height, PREG_PATTERN_ORDER);
						if(isset($matches_height[0][0])){
							$height_value = intval($matches_height[1][0]);	
						}
						
						if($url_in_tag){
							if(strpos($url_in_tag, 'http') === false){
								if(preg_match('#^(\/).*#', $url_in_tag, $out)){
									if(isset($out[1]) && $out[1] == '/'){
										$url_in_tag = substr($url_in_tag, 1);
									}
								}
								$url_in_tag = JURI::root() . $url_in_tag;
							}
						}
						
						$params_replace = '';
						if($url_in_tag != '' && $text_in_tag != ''){
							$params_replace .= 'href="'.$url_in_tag.'"';
							if($helper == 'iframe' || $helper == 'image'){
								if($width_value && $height_value){
									$params_replace .= ' rel="{handler:\''.$helper.'\', size:{x:'.$width_value.',y:'.$height_value.'}}"';
								} else {
									$params_replace .= ' rel="{handler:\''.$helper.'\', size:{x:'.$p_width.',y:'.$p_height.'}}"';
								}
							}
							if($title != ''){
								$params_replace .= ' title="'.$title.'"';	
							}
							if($suffix != ''){
								$suffix = ' '.$suffix;
							}
							$text_replace = '<a '.$params_replace.' class="jlms_modal'.$suffix.'">'.$text_in_tag.'</a>';
							$text = str_replace($text_search, $text_replace, $text);
						}
						$i++;
			    	}
			    	$article->text = $text;
			    }
			}
		}
	}
}