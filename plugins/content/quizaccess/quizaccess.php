<?php

defined('_JEXEC') or die('Restricted access');

class plgContentQuizaccess extends JPlugin
{

    protected $autoloadLanguage = true;
    function onContentPrepare($context, &$article, &$params, $page = 0)
    {
        JFactory::getLanguage()->load('custom', JPATH_SITE, null, true);
        $quiz_id = JFactory::getApplication()->input->get('quiz_id', 0, 'INT');
        JFactory::getLanguage()->load('custom', JPATH_SITE, null, true);
        $document = JFactory::getDocument();
        if ($context == 'com_joomlaquiz' && $quiz_id && !plotUser::factory()->id) {
            $article->text='<span style="color:red;">' . JText::_("PLG_QUIZACCESS_FE_NO_AUTHORIZE_RIGHTS_VIEW_QUIZ") . '</span>';

            $document->addStyleDeclaration('#jq_start_link_container, #jq_quiz_container_title{ display:none;}');
        } elseif ($context == 'com_joomlaquiz' && $quiz_id && !(int)$this->quizAccess($quiz_id) && plotUser::factory()->id) {

            $article->text='<span style="color:red;">' . JText::_("PLG_QUIZACCESS_FE_AUTHORIZE_RIGHTS_VIEW_QUIZ") . '</span>';
            $document->addStyleDeclaration('#jq_start_link_container, #jq_quiz_container_title{ display:none;}');
        }
    }




    function quizAccess($quiz_id)
    {
        $user = plotUser::factory();
        $db = Foundry::db();
        $query = $db->getQuery(true)
            ->clear()
            ->select('q.id')
            ->from('`#__plot_book_quiz_map` AS q ')
            ->innerJoin('`#__plot_books_paid` AS b ON b.book_id=q.id_pub')
            ->where('q.id_quiz=' . (int)$quiz_id)
            ->where('child_id=' . (int)$user->id);
        $db->setQuery($query);
        return $db->loadResult();
    }

}

?>
