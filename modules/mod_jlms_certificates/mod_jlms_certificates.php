<?php
// no direct access
defined('_JEXEC') or die;

if (!defined('DS')) { define( 'DS', DIRECTORY_SEPARATOR ); }
if (!defined('_JOOMLMS_FRONT_HOME')) { define('_JOOMLMS_FRONT_HOME', JPATH_SITE . DS . 'components' . DS . 'com_joomla_lms'); }

if(!defined('_JLMS_EXEC')){ define('_JLMS_EXEC', 1); }

if(!defined('_JLMS_DONTUSE_IONCUBED_FILES')) {
	if (file_exists(_JOOMLMS_FRONT_HOME . DS . 'validatejlmsioncubelicense.php')) {
		require_once(_JOOMLMS_FRONT_HOME . DS . 'validatejlmsioncubelicense.php');
	}
}
if(!defined('_JLMS_DONTUSE_IONCUBED_FILES')){
	require_once dirname(__FILE__).'/helper.php';

	$certificates = modCertificatesHelper::getData($params);
	$class_sfx = htmlspecialchars($params->get('class_sfx'));
	$show_sn = htmlspecialchars($params->get('show_sn'));

	require JModuleHelper::getLayoutPath('mod_jlms_certificates');
}