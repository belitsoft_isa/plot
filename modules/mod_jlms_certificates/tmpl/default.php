<?php
// no direct access
defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$css = '
	sup.crt_uniq{
		color: #ccc;
	}
';
$doc->addStyleDeclaration($css);
?>

<?php
if(count($certificates)){
?>
<ul class="menu<?php echo $class_sfx; ?>">
	<?php
	foreach($certificates as $c){
		$link = 'index.php?option=com_joomla_lms&task=gradebook';
		$link .= $c->course_id ? '&id='.$c->course_id : '';
		$link = JRoute::_($link);
		?>
		<li>
			<a href="<?php echo JRoute::_($link);?>">
				<span><?php echo $c->course_name;?></span>
				<?php
				if(isset($c->crt_date) && false){
					?>
					<sup class="crt_uniq"><?php echo JLMS_dateToDisplay($c->crt_date);?></sup>
					<?php
				}
				if(isset($c->uniq_id) && $c->uniq_id && $show_sn){
					?>
					<sup class="crt_uniq"><?php echo strtoupper($c->uniq_id);?></sup>
					<?php
				}
				?>
			</a>
		</li>
		<?php
	}
	?>
</ul>
<?php
}