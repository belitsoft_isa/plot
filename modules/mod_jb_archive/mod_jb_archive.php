<?php
/**
 * JoomBlog Archive Module for Joomla
 * @version    $Id: mod_jb_archive.php 2011-03-16 17:30:15
 * @package    JoomBlog
 * @subpackage mod_jb_archive.php
 * @author     JoomPlace Team
 * @Copyright  Copyright (C) JoomPlace, www.joomplace.com
 * @license    GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */

defined('_JEXEC') or die('Restricted access');

if (!defined('JB_COM_PATH'))
	require_once(JPATH_ROOT . '/components/com_joomblog/defines.joomblog.php');

require_once(JPATH_ROOT . '/administrator/components/com_joomblog/config.joomblog.php');
require_once(JB_COM_PATH . '/libraries/datamanager.php');
require_once(JB_COM_PATH . '/functions.joomblog.php');

require_once(dirname(__FILE__) . '/helper.php');

$_JB_CONFIGURATION = new JB_Configuration();

$doc =& JFactory::getDocument();
$mainframe = JFactory::getApplication();

if (!empty($_JB_CONFIGURATION))
{
	if ($_JB_CONFIGURATION->get('overrideTemplate'))
	{
		$file = JURI::base() . 'templates/' . $mainframe->getTemplate() . '/com_joomblog/module.archive.css';
	}
	else
	{
		$file = JURI::base() . 'components/com_joomblog/templates/' . $_JB_CONFIGURATION->get('template') . '/module.archive.css';
	}
}

$doc->addStyleSheet($file);

$list = modJbArchivePostsHelper::getList($params);
$itemid = modJbArchivePostsHelper::getItemid();

require(JModuleHelper::getLayoutPath('mod_jb_archive'));