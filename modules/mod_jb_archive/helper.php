<?php
/**
 * JoomBlog Archive Module for Joomla
 * @version $Id: helper.php 2011-03-16 17:30:15
 * @package JoomBlog
 * @subpackage helper.php
 * @author JoomPlace Team
 * @Copyright Copyright (C) JoomPlace, www.joomplace.com
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */

defined('_JEXEC') or die('Restricted access');

//require_once (JPATH_SITE.DS.'components'.DS.'com_joomblog'.DS.'router.php');
require_once(JPATH_ROOT . '/administrator/components/com_joomblog/config.joomblog.php');

class modJbArchivePostsHelper
{
	function getList(&$params)
	{
		$_JB_CONFIGURATION = new JB_Configuration();

		$db = &JFactory::getDBO();
		$user	=& JFactory::getUser();

		$managedSections = $_JB_CONFIGURATION->managedSections;
		$posts_type = $params->get('posts_type');
		$show_posts = $params->get('show_posts');


		/*if ( !isset($managedSections) ){
			$query = "SELECT value FROM #__joomblog_config WHERE name='all' LIMIT 1 ";
			$db->setQuery($query);
			$config = $db->loadResult();
			eval($config);
		}*/

		$sections = implode(",",jbGetCategoryArray($managedSections));

		$where = " AND ( c.catid IN ( ".$sections." ) ) ";
		switch ($posts_type)
		{
			case 1: default: $cstate_where = ' c.state = 1 '; break;
			case 2: $cstate_where = ' c.state = 2 '; break;
			case 3: $cstate_where = ' (c.state = 1 OR c.state = 2) '; break;
		}

		$sql = "
     		SELECT c.title, c.alias, c.id, c.created_by, p.posts,
        	DATE_FORMAT( c.created,'%M') AS monthname,
        	MONTH( c.created ) AS created_month,
        	YEAR( c.created ) AS created_year
      		FROM #__joomblog_posts AS c
      		LEFT JOIN `#__joomblog_privacy` AS `p` ON `p`.`postid`=`c`.`id` AND `p`.`isblog`=0
        	WHERE $cstate_where AND c.created < NOW() $where
        	ORDER BY created_year DESC, created_month DESC";

		// IS CACHED
		if ( $Result = jbCacheChecker('mod_archive', $sql) )
			return $Result;

		$db->setQuery($sql);
		$posts = $db->loadObjectList('id');

		if ( $posts )
		{
			$postsIDS = array();
			foreach ($posts as $post)
				$postsIDS[$post->id] = $post->id;

			$db->setQuery(" SELECT b.blog_id as bid, lb.title as btitle, p.posts, p.comments, b.content_id  " .
				" FROM #__joomblog_blogs as b, #__joomblog_list_blogs as lb " .
				" LEFT JOIN `#__joomblog_privacy` AS `p` ON `p`.`postid`=`lb`.`id` AND `p`.`isblog`=1 ".
				" WHERE b.content_id IN (".implode(',', $postsIDS).") AND lb.id = b.blog_id AND lb.approved=1 AND lb.published=1 ");

			$blogs = $db->loadObjectList();

			if ( $blogs )
			{
				foreach ($blogs as $blog)
				{
					$posts[$blog->content_id]->blog_posts = $blog->posts;
				}
			}

			foreach ($posts as $post)
			{
				$notAdd = false;

				switch ( $post->posts )
				{
					case 0: break;
					case 1: if (!$user->id) { $notAdd = true; } break;
					case 2:
						if (!$user->id)
							$notAdd = true;
						else
							if ( !isFriends($user->id, $post->created_by) && $user->id!=$post->created_by )
								$notAdd = true;
						break;
					case 3:
						if (!$user->id)
							$notAdd = true;
						else
							if ($user->id!=$post->created_by)
								$notAdd = true;
						break;
				}

				switch ( $post->blog_posts )
				{
					case 0:	break;
					case 1: if (!$user->id) { $notAdd = true; } break;
					case 2:
						if (!$user->id)
							$notAdd = true;
						else
							if (!isFriends($user->id, $post->created_by) && $user->id!=$post->created_by)
								$notAdd = true;
						break;
					case 3:
						if (!$user->id)
							$notAdd = true;
						else
							if ($user->id!=$post->created_by)
								$notAdd = true;
						break;
				}

				if ( !$notAdd )
				{
					if ( empty($Result[$post->created_year]) )	$Result[$post->created_year] = array('month'=>array(), 'count'=>0);

					if ( empty($Result[$post->created_year]['month'][$post->created_month]) )
					{
						$Result[$post->created_year]['month'][$post->created_month] = (object)array(
							'monthname'=>$post->monthname,
							'created_month'=>$post->created_month,
							'created_year'=>$post->created_year,
							'count_month'=>1,
							'posts' => array( 0 => $post ),
						);
						$Result[$post->created_year]['count']++;
					}
					else
					{
						if ( sizeof($Result[$post->created_year]['month'][$post->created_month]->posts) < 5 && $show_posts)
							$Result[$post->created_year]['month'][$post->created_month]->posts[] = $post;

						$Result[$post->created_year]['month'][$post->created_month]->count_month++;
						$Result[$post->created_year]['count']++;
					}
				}
			}
		}

		jbCacheChecker('mod_archive', $sql, $Result);

		return $Result;
	}

	function getItemid(){
		$Itemid = JRequest::getInt('Itemid');

		$menu 	= &JSite::getMenu();
		$items	= $menu->getItems('link', 'index.php?option=com_joomblog&view=default');

		return $items?$items[0]->id:$Itemid;
	}
}
