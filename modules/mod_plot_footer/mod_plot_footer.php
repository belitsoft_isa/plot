<?php
defined('_JEXEC') or die;
require_once __DIR__ . '/helper.php';

$view = JFactory::getApplication()->input->getCmd('view', '');
$currentTemplate = JFactory::getApplication()->getTemplate();
$id=JFactory::getApplication()->input->getInt('id');

if($id){
    $my = plotUser::factory($id);
}else{
    $my = plotUser::factory();
}


if ($view == 'river') {
    require JModuleHelper::getLayoutPath('mod_plot_footer', 'default_river');
} else {
    if ($my->isParent()) {
        require JModuleHelper::getLayoutPath('mod_plot_footer', 'default_parent');
    } else {
        require JModuleHelper::getLayoutPath('mod_plot_footer', 'default');
    }
}
