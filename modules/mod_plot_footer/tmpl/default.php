<?php
defined('_JEXEC') or die;
?>

<footer class="child-profile">
    <div class="wrap child-profile">
        <div class="add-left aside-left child-profile"></div>
        <div class="add-right aside-right child-profile"></div>
        <svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>" viewBox="0 0 36.3 45.1" preserveAspectRatio="xMidYMid meet" class="frog">
            <use xlink:href="#frog"></use>
        </svg>
        <div class="cpr-socnet">
            <copyright>2014-<?php echo date('Y');?> &#169 Naplotu - Разработано <a href="http://belitsoft.ru" target="_blank" title="Разработано Belitsoft.ru" rel="nofollow">Bel<i>it</i>soft</a></copyright>
            <div class="socialnet">
                <a target="_blank" href="https://www.facebook.com/naplotu">
                <svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>" viewBox="0 0 266.9 266.9" preserveAspectRatio="xMinYMin meet" class="icon"><use xlink:href="#fb_logo"></use></svg>
                </a>
                <a target="_blank" href="http://vk.com/naplotu_com">
                <svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>" viewBox="0 0 415 415" xmlns="http://www.w3.org/2000/svg" class="icon"><use xlink:href="#vk_logo"></use></svg>
                </a>
                <a target="_blank" href="http://instagram.com/naplotu">
                    <svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>" viewBox="0 0 109.5 109.5" xmlns="http://www.w3.org/2000/svg" class="icon"><use xlink:href="#instagram"></use></svg>
                </a>
                <a target="_blank" href="https://www.youtube.com/channel/UCadX_K1R0gDIFBxZuWusyjw">
                    <svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>" viewBox="0 0 109.5 109.5" xmlns="http://www.w3.org/2000/svg" class="icon"><use xlink:href="#youtube-logo"></use></svg>
                </a>
            </div>
        </div>
        <div class="footer-nav">
            <menu>
                <li><a href="<?php echo JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('footerLinkK2idForChildrens')); ?>"><?php echo  JText::_("MOD_PLOT_FOOTER_FOR_CHILD"); ?></a></li>
                <li><a href="<?php echo JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('footerLinkK2idForParents')); ?>">Для родителей</a></li>
                <li><a href="<?php echo JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('footerLinkK2idCommunities')); ?>">Сообщества</a></li>
                <li><a href="<?php echo JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('footerLinkK2idThink')); ?>">Думать</a></li>
                <li><a href="<?php echo JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('footerLinkK2idToAuthors')); ?>">Авторам</a></li>
                <li><a href="<?php echo JRoute::_('index.php?option=com_joomblog&view=default&Itemid='.plotGlobalConfig::getVar('footerLinkidJoomBlog')); ?>">Блог</a></li>
            </menu>
            <nav>
                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('footerLinkK2idAboutProject')); ?>">О нас</a>
                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('footerLinkK2idUsersAgreement')); ?>">Пользовательское соглашение</a>
                <a href="<?php echo JRoute::_('index.php?option=com_plot&view=contacts'); ?>">Контакты</a>
            </nav>
        </div>
    </div>
</footer>
                