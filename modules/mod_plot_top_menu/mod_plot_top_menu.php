<?php
defined('_JEXEC') or die;
require_once __DIR__.'/helper.php';

$view = JFactory::getApplication()->input->getCmd('view', '');
$task = JFactory::getApplication()->input->getCmd('task', '');
$option= JFactory::getApplication()->input->getCmd('option', '');

$my = plotUser::factory();
JModelLegacy::addIncludePath(JPATH_ROOT.DS.'components'.DS.'com_plot'.DS.'models', 'Plot');
JModelLegacy::addIncludePath(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_easysocial'.DS.'models', 'EasySocial');
$coursesModel = JModelLegacy::getInstance('courses', 'plotModel');
$publicationsModel = JModelLegacy::getInstance('publications', 'plotModel');
$conversationsModel = JModelLegacy::getInstance('conversations', 'EasySocialModel');

$my->countNewCourses = $coursesModel->getNewCoursesCount($my->id);
$my->countNewBooks = $publicationsModel->getNewBooksCount($my->id);
$my->newMessages = $conversationsModel->getNewCount($my->id, false);

if ($my->isParent()) {
    require JModuleHelper::getLayoutPath('mod_plot_top_menu', 'default_parent');
} else {
    require JModuleHelper::getLayoutPath('mod_plot_top_menu', 'default');
}

