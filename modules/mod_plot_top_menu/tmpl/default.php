<?php
defined('_JEXEC') or die;
?>

<menu <?php echo (plotUser::factory()->id)?'class="child-profile menu-top"':'class="child-profile menu-top no-signing"';?>">
    <li <?php echo $view == 'profile' ? 'class="active"' : ''; ?>>
        <a href="<?php echo JRoute::_('index.php?option=com_plot&view=profile&id='.$my->id); ?>"><?php echo JText::_('MOD_PLOT_TOP_MENU_PROFILE'); ?></a>
    </li>
    <li <?php echo ($view == 'courses' || $view == 'course')  ? 'class="active"' : ''; ?>>
        <a href="<?php echo JRoute::_('index.php?option=com_plot&view=courses'); ?>"><?php echo JText::_('MOD_PLOT_TOP_MENU_TO_LEARN'); ?></a>
        <?php if ($my->countNewCourses) { ?>
            <b><?php echo $my->countNewCourses; ?></b>
        <?php } ?>
    </li>
    <li <?php echo ($view == 'publications' || $view == 'publication') ? 'class="active"' : ''; ?>>
        <a href="<?php echo JRoute::_('index.php?option=com_plot&view=publications'); ?>"><?php echo JText::_('MOD_PLOT_TOP_MENU_TO_READ'); ?></a>
        <?php if ($my->countNewBooks) { ?>
            <b><?php echo $my->countNewBooks; ?></b>
        <?php } ?>
    </li>
    <li <?php echo ($view == 'events' || $view == 'event') ? 'class="active"' : ''; ?>>
        <a href="<?php echo JRoute::_('index.php?option=com_plot&view=events'); ?>"><?php echo JText::_('MOD_PLOT_TOP_MENU_TO_THINK'); ?></a>
    </li>

    <li <?php echo (!$view && $task=='view' && $option=='com_content') ? 'class="active"' : '' ;?><?php //echo ($view == 'groups' || $view == 'group') ? 'class="active"' : ''; ?>>
        <a href="http://naplotu.com/joomblog/post/konkurs-voprosov-na-livelib" ><?php echo JText::_('COM_PLOT_COMPETITION'); ?></a>
        <!--<a href="<?php echo JRoute::_('index.php?option=com_easysocial&view=groups&Itemid=122'); ?>"><?php echo JText::_('MOD_PLOT_TOP_MENU_PROGRAM'); ?></a>-->
    </li>

    <?php

    if(plotUser::factory()->id){ ?>
    <li <?php echo ($view == 'conversations') ? 'class="active"' : ''; ?>>
        <a href="<?php echo JRoute::_('index.php?option=com_easysocial&view=conversations'); ?>"><?php echo JText::_('MOD_PLOT_TOP_MENU_TO_COMMUNICATE'); ?></a>
        <?php if ((int)$my->newMessages) { ?>
            <b><?php echo (int)$my->newMessages; ?></b>
        <?php } ?>
    </li>
    <?php } ?>
</menu>