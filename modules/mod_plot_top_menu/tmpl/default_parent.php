<?php
defined('_JEXEC') or die;
?>

<div class="parent-main-menu">
    <menu class="wrap parent-menu">
        <li <?php echo $view == 'profile' ? 'class="active"' : ''; ?>>
            <a class="main-a" href="<?php echo JRoute::_('index.php?option=com_plot&view=profile&id='.$my->id); ?>">Мой профиль</a>
        </li>
        <li <?php echo ($view == 'course' || $view == 'courses') ? 'class="active"' : ''; ?>>
            <a class="main-a" href="<?php echo JRoute::_('index.php?option=com_plot&view=courses'); ?>">Учить</a>
            <?php if ($my->countNewCourses) { ?>
            <b><?php echo $my->countNewCourses; ?></b>
            <?php } ?>
        </li>
        <li <?php echo ($view == 'publication' || $view == 'publications') ? 'class="active"' : ''; ?>>
            <a class="main-a" href="<?php echo JRoute::_('index.php?option=com_plot&view=publications'); ?>">Читать</a>
            <?php if ($my->countNewBooks) { ?>
                <b><?php echo $my->countNewBooks; ?></b>
            <?php } ?>
        </li>

        <li <?php echo ($view == 'events' || $view=='event') ? 'class="active"' : ''; ?>>
            <a class="main-a" href="<?php echo JRoute::_('index.php?option=com_plot&view=events'); ?>">Думать</a>
        </li>

        <li <?php echo ($view == 'groups' || $view == 'group') ? 'class="active"' : ''; ?>>
            <a class="main-a" href="<?php echo JRoute::_('index.php?option=com_easysocial&view=groups&Itemid=122'); ?>"><?php echo JText::_('MOD_PLOT_TOP_MENU_PROGRAM'); ?></a>
        </li>
<?php

if(plotUser::factory()->id){ ?>
        <li <?php echo ($view=='conversations') ? 'class="active"' : ''; ?>>
            <a class="main-a" href="<?php echo JRoute::_('index.php?option=com_easysocial&view=conversations'); ?>">Сообщения</a>
            <?php if ((int)$my->newMessages) { ?>
                <b><?php echo (int)$my->newMessages; ?></b>
            <?php } ?>
        </li>
<?php } ?>
        <li <?php echo ($view == 'wallet') ? 'class="active"' : ''; ?>>
            <a class="main-a" href="<?php echo JRoute::_('index.php?option=com_plot&view=wallet'); ?>">Кошелек</a>
        </li>
        <li <?php echo (!$view && $task=='view' && $option=='com_content') ? 'class="active"' : '' ;?> >
            <a href="http://naplotu.com/joomblog/post/konkurs-voprosov-na-livelib" class="main-a"><?php echo JText::_('COM_PLOT_COMPETITION'); ?></a>
        </li>
    </menu>
</div>