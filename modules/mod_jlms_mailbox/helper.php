<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

class modMailBoxHelper{
	
	function __construct($params){
	}
	
	public static function getData($params){
		
		$user = JFactory::getUser();
		
		if (!defined('_JOOMLMS_FRONT_HOME')) { define('_JOOMLMS_FRONT_HOME', JPATH_SITE . DS . 'components' . DS . 'com_joomla_lms'); }
		
		if(!defined('_JLMS_EXEC')){ define('_JLMS_EXEC', 1); }
		
		require_once(_JOOMLMS_FRONT_HOME . DS . 'includes' . DS . 'component.legacy.php');
		require_once(_JOOMLMS_FRONT_HOME . DS . 'includes' . DS . 'lms_legacy.php');
		require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.factory.php");
		
		$GLOBALS['JLMS_DB'] = & JLMSFactory::getDB();
		
		require_once(_JOOMLMS_FRONT_HOME . DS . 'joomla_lms.func.php');
		require_once(_JOOMLMS_FRONT_HOME . DS . 'joomla_lms.main.php');
		require_once(_JOOMLMS_FRONT_HOME . DS . 'includes' . DS . 'classes' . DS . 'lms.acl.php');
		
		$str_my_courses_ids = 0;	

		if($params->get('course_related_items', 0)){
			$JLMS_CONFIG = & JLMSFactory::getConfig();
			if($JLMS_CONFIG->get('course_id')){
				$str_my_courses_ids = $JLMS_CONFIG->get('course_id');
			}
		}
		
		$count = $params->get('count', 5);
		
		$mailbox = my_mailbox($str_my_courses_ids, $count, $user->id);
		
		return $mailbox;
	}
	
}