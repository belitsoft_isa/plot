<?php
// no direct access
defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$css = '
	sup.hint{
		margin: 0;
		padding: 0 2px;
		background: #bbbb00;
		color: #ffffff;
		font-size: 0.7em;
		line-height: 0px;
	}
';
$doc->addStyleDeclaration($css);

if(count($mailbox)){
?>
<ul class="menu<?php echo $class_sfx; ?>">
	<?php
	foreach($mailbox as $m){
		$link = 'index.php?option=com_joomla_lms&task=mail_view';
		$link .= isset($m->course_id) && $m->course_id ? '&id='.$m->course_id : '';
		$link .= '&view_id='.$m->id;
		?>
		<li>
			<a href="<?php echo JRoute::_($link);?>">
				<span><?php echo $m->subject;?></span>
				<?php
				if(!$m->is_read){
					?>	
					<sup class="hint">new</sup>
					<?php
				}
				?>
			</a>
		</li>
		<?php
	}
	?>
</ul>
<?php
}