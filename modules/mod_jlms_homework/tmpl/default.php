<?php
// no direct access
defined('_JEXEC') or die;

if(count($homeworks)){
?>
<ul class="menu<?php echo $class_sfx; ?>">
	<?php
	foreach($homeworks as $h){
		$link = 'index.php?option=com_joomla_lms&task=hw_view&course_id='.$h->course_id.'&id='.$h->course_id;
		?>
		<li>
			<a href="<?php echo JRoute::_($link);?>">
				<span><?php echo $h->hw_name;?></span>
			</a>
		</li>
		<?php
	}
	?>
</ul>
<?php
}