<?php
// no direct access
defined('_JEXEC') or die;

if (!defined('DS')) { define( 'DS', DIRECTORY_SEPARATOR ); }
if (!defined('_JOOMLMS_FRONT_HOME')) { define('_JOOMLMS_FRONT_HOME', JPATH_SITE . DS . 'components' . DS . 'com_joomla_lms'); }

if(!defined('_JLMS_EXEC')){ define('_JLMS_EXEC', 1); }

if(!defined('_JLMS_DONTUSE_IONCUBED_FILES')) {
	require_once(_JOOMLMS_FRONT_HOME . DS . 'validatejlmsioncubelicense.php');
}
if(!defined('_JLMS_DONTUSE_IONCUBED_FILES')){

	require_once dirname(__FILE__).'/helper.php';

	require_once(_JOOMLMS_FRONT_HOME . DS . 'includes' . DS . 'component.legacy.php');
	require_once(_JOOMLMS_FRONT_HOME . DS . 'includes' . DS . 'lms_legacy.php');
	require_once(_JOOMLMS_FRONT_HOME . DS . 'includes' . DS . 'forums' . DS . 'smf' . DS .'smf.php');
	require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.factory.php");
	require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.css.php");
	require_once(_JOOMLMS_FRONT_HOME . DS . 'joomla_lms.func.php');
	require_once(_JOOMLMS_FRONT_HOME . DS . 'joomla_lms.main.php');
	require_once(_JOOMLMS_FRONT_HOME . DS . 'includes' . DS . 'classes' . DS . 'lms.acl.php');

	$GLOBALS['JLMS_DB'] = & JLMSFactory::getDB();
	$GLOBALS['JLMS_CONFIG'] = & JLMSFactory::getConfig();

	global $JLMS_CONFIG, $JLMS_LANGUAGE;

	$doc = JFactory::getDocument();
	JLMS_addStyleSheet();

	JLMS_require_lang($JLMS_LANGUAGE, 'main.lang', $JLMS_CONFIG->get('default_language'));
	JLMS_processLanguage( $JLMS_LANGUAGE );

	$latest_forum_post = modLatestForumPostHelper::getData($params);
	$class_sfx = htmlspecialchars($params->get('class_sfx'));

	require JModuleHelper::getLayoutPath('mod_jlms_forum_posts');

	addDomReadyScript();
}