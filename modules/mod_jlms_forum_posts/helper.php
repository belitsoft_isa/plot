<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

class modLatestForumPostHelper{
	
	function __construct($params){
	}
	
	public static function getData($params)
	{	
		global $JLMS_CONFIG, $JLMS_LANGUAGE;
			
		$user = JFactory::getUser();		
		$JLMS_ACL = JLMSFactory::getACL();
				
		require_once(_JOOMLMS_FRONT_HOME . DS . "includes" . DS . "classes" . DS . "lms.params.new.php");		
		
		$params = new JLMSParameters($JLMS_CONFIG->get('course_params'));		
		
		if( JRequest::getVar('option') == 'com_joomla_lms' && JRequest::getVar('task') == 'details_course' && $params->get('latest_forum_posts_view',0)  ) 
		{
			$str_my_courses_ids = JRequest::getInt('id', 0);  
		} else {
			$my_courses_ids = $JLMS_ACL->getMyCourses($JLMS_ACL->_role_type);
			$str_my_courses_ids = implode(',', $my_courses_ids);	
		}
						
		$count = $params->get('count', 5);
		
		$latest_forum_post = false;
		if(function_exists('latest_forum_posts')){
			$latest_forum_post = latest_forum_posts($str_my_courses_ids, $count, $user->id);
		}
		
		return $latest_forum_post;
	}
	
}