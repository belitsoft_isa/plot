<?php
// no direct access
defined('_JEXEC') or die;

if(is_array($latest_forum_post) && count($latest_forum_post)){

$doc = JFactory::getDocument();
$css = '
	sup.crt_uniq{
		color: #ccc;
	}
';
$doc->addStyleDeclaration($css);	
	
?>
<ul class="menu<?php echo $class_sfx; ?>">
	<?php
	foreach($latest_forum_post as $lfpitem){			
		$subject = $lfpitem->subject;
		$body = '';
		if(isset($lfpitem->poster_time) && $lfpitem->poster_time){
			$body = _JLMS_DATE.' '.JLMS_dateToDisplay(date("Y-m-d H:i:s", $lfpitem->poster_time));
		}
				
		$body .= "<br />"._JLMS_COURSE.': '.$lfpitem->course_name;
		$body .= "<br />"._JLMS_TOPIC.': '.$lfpitem->topic_name;
		$body .= "<br />"._JLMS_AUTHOR.': '.$lfpitem->poster_name;		
		
		$in_tag = strlen($lfpitem->body) > 90 ? substr($lfpitem->body, 0, 90).'...' : $lfpitem->body;				
		
		$link = 'index.php?option=com_joomla_lms&task=course_forum';
		$link .= isset($lfpitem->course_id) && $lfpitem->course_id ? '&id='.$lfpitem->course_id : '';
		$link .= '&topic_id='.$lfpitem->id_topic;
		$link .= '&message_id='.$lfpitem->id_msg;
		$link = JRoute::_($link);
		?>
		<li>			
			<span><?php echo JLMS_toolTip($subject, $body, $in_tag, $link, 1, 120, 'false', 'jlms_ttip_posts');?></span>			
		</li>
		<?php
	}
	?>
</ul>
<?php
} else 
if($latest_forum_post === false){
	?>
	<dl id="system-message">
		<dd class="notice mesage">
			<ul>
				<li>
					Please upgrade JoomlaLMS to the latest version in order to use this module
				</li>
			</ul>
		</dd>
	</dl>
	<?php
}