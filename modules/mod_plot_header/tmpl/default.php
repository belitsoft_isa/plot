<?php
defined('_JEXEC') or die;
?>

<script type="text/javascript" src="<?php echo JUri::root().'components/com_plot/assets/js/search.js'; ?>" async="async" ></script>
<script type="text/javascript" src="<?php echo JUri::root().'components/com_plot/assets/js/jquery.slicknav.js'; ?>"></script>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('header .user-photo, .user-top .circle-img, .looking-at-user-header .user-photo').click(function(){
            // hide -> waiting for content loading -> show
            jQuery('#sbox-content').hide();
        });

        jQuery('#hp-navigation').on('click','li', function(){
            plotremoveActiveClass();
            jQuery(this).addClass('active');
        });

    });

function plotremoveActiveClass(){
    jQuery('#hp-navigation').find('li.active').removeClass('active');
}

</script>
<?php # </editor-fold> ?>

<?php if ($view == 'river') { ?>

<header class="home-page">
    <div class="wrap">
        <a class="btn green modal" rel="{size: {x: 580, y: 451}, handler:'iframe'}" href="<?php echo JRoute::_('index.php?option=com_plot&task=registration.ajaxGetLoginPopupHtml');?>">
            <?php echo JText::_('COM_PLOT_HEADER_LOGIN'); ?>
        </a>
        <div id="logo" style="margin: 0.5em 0.5em 0.5em 0;">
            <span>naplotu</span>
            <i>образовательная соцсеть</i>
        </div>
        <menu>
            <ul id="hp-navigation">
                <li class="active"><a href="#about_plot">О нас</a></li>
				<li><a href="<?php echo JRoute::_('index.php?option=com_plot&view=publications'); ?>"><?php echo JText::_('Книги'); ?></a></li>
				<li><a href="<?php echo JRoute::_('index.php?option=com_plot&view=courses'); ?>"><?php echo JText::_('Курсы'); ?></a></li>
                <li> <a href="http://naplotu.com/joomblog/post/konkurs-voprosov-na-livelib" >!!! Конкурс !!!</a></li>
               <!-- <li><a href="#for_children" >Детям</a></li>-->
                <li><a href="#for_parents">Родителям</a></li>
                <li><a href="#for_authors">Авторам</a></li>
                <!--<li><a href="<?php echo JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('footerLinkK2idCompanies')); ?>">Компаниям</a></li>-->
                <li><a href="<?php echo JRoute::_('index.php?option=com_joomblog&view=default&Itemid=258'); ?>">Блог</a></li>
                <li><a href="#how_it_works">Как это работает?</a></li>

                <!--li><a href="<?php #echo JRoute::_('index.php?option=com_plot&view=k2article&id='.plotGlobalConfig::getVar('footerLinkK2idForChildrens')); ?>">ЧаВо</a></li-->
                <li><a class="signing modal" rel="{size: {x: 580, y: 451}, handler:'iframe'}" href="<?php echo JRoute::_('index.php?option=com_plot&task=registration.ajaxGetLoginPopupHtml');?>"><?php echo JText::_('COM_PLOT_HEADER_LOGIN'); ?></a></li>
            </ul>
        </menu>
    </div>
    <div id="offcanvas"></div>
</header>

<?php } else { ?>

<header class="child-profile">
    <div class="wrap child-profile">
        <div>
            <div class="logo-wrap">
                <a id="logo" href="<?php echo JRoute::_('index.php?option=com_plot&view=river'); ?>" title="naplotu.com">
                    <span>naplotu</span>
                    <i>образовательная соцсеть</i>
                </a>
            </div>
            <form id="search-form" class="search" action="<?php echo JRoute::_('index.php?option=com_plot&view=search'); ?>" method="POST">
                <input id="plot-search" type="text" autocomplete="off" name="search" placeholder="Искать" onfocus="searchInputFocus();" />
                <input id="search-submit" type="submit" value="" />
                <label for="search-submit" onclick="jQuery('#search-form').submit();">
                    <svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>" viewBox="0 0 49.9 19.4" preserveAspectRatio="xMidYMid meet" class="search-submit"><use xlink:href="#specs"></use></svg>
                </label>
                <div id="plot-search-results"></div>
            </form>
        </div>
        <?php if ($my->id) { ?>
            <div class="user-top">
                <div>
                    <span class="user-name"><?php echo $my->name; ?></span><br/>
                    <p class="level">
                        <svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>" viewBox="0 0 19.8 30" preserveAspectRatio="xMinYMin meet" class="cup"><use xlink:href="#cup"></use></svg>
                        <i><span><?php echo $my->level->title; ?>й </span> <?php echo  JText::_('COM_PLOT_MY_LEVEL');?></i>
                    </p>
                </div>
                <a class="user-photo modal" rel="{size: {x: 744, y: 500}, handler:'iframe', iframeOptions: {scrolling: 'yes'}}"
                   href="<?php echo JRoute::_('index.php?option=com_plot&task=profileedit.ajaxGetEditProfilePopupHtml'); ?>">
                    <?php if ($friends_count) { ?>
                        <b class="friends"><?php echo $friends_count; ?></b>
                    <?php } ?>
                    <img id="plot-my-avatar" src="<?php echo $my->plotGetAvatar(); ?>" alt="">
                </a>

                <?php if($user->id==$my->id) { ?>
                <button class="settings no-box-shadow header-profile-menu-show-button">
                    <svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>" viewBox="0 0 37.1 37.1" preserveAspectRatio="xMinYMin meet"><use xlink:href="#settings"></use></svg>
                </button>
                <ul class="header-profile-menu">
                    <li><a href="<?php echo JRoute::_('index.php?option=com_plot&task=registration.logout'); ?>"><?php echo JText::_('COM_PLOT_LOGOUT'); ?></a></li>
                </ul>
                <?php } else { ?>
                    <?php if ($view=='profile') { ?>
                    <button class="settings" title="Вернуться к себе" onclick="window.location.href = '<?php echo JRoute::_('index.php?option=com_plot&view=profile&id='.$my->id); ?>';"><svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>" viewBox="0 0 459 459"><use xlink:href="#reply"></use></svg></button>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } else { ?>
            <a class="btn green modal" rel="{size: {x: 580, y: 451}, handler:'iframe'}" href="<?php echo JRoute::_('index.php?option=com_plot&task=registration.ajaxGetLoginPopupHtml');?>">
                <?php echo JText::_('COM_PLOT_HEADER_LOGIN'); ?>
            </a>
        <?php } ?>
    </div>
</header>
<?php } ?>
<script type="text/javascript">
    jQuery('#hp-navigation').slicknav({
        prependTo:'#offcanvas',
        label:'',
        closeOnClick:'true'
    });
</script>
