<?php
defined('_JEXEC') or die;
?>

<script type="text/javascript" src="<?php echo JUri::root().'components/com_plot/assets/js/search.js'; ?>"></script>

<?php # <editor-fold defaultstate="collapsed" desc="JS"> ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('header .user-photo, .user-top .circle-img, .looking-at-user-header .user-photo').click(function(){
            // hide -> waiting for content loading -> show
            jQuery('#sbox-content').hide();
        });
    });
</script>
<?php # </editor-fold> ?>

<header class="parent-profile">
    <div class="wrap parent-profile">
        <div>
            <div class="logo-wrap">
                <a id="logo" href="<?php echo JRoute::_('index.php?option=com_plot&view=river'); ?>" title="naplotu.com">
                    <span>naplotu</span>
                    <i>образовательная соцсеть</i>
                </a>
            </div>
            <form id="search-form" class="search" action="<?php echo JRoute::_('index.php?option=com_plot&view=search'); ?>" method="POST">
                <input id="plot-search" name="search" type="text" autocomplete="off" placeholder="Искать" onfocus="searchInputFocus();"/>
                <input id="search-submit" type="submit" value=""/>
                <label for="search-submit" onclick="jQuery('#search-form').submit();">
                    <svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>" viewBox="0 0 49.9 19.4" preserveAspectRatio="xMidYMid meet" class="search-submit"><use xlink:href="#specs"></use></svg>
                </label>
                <div id="plot-search-results"></div>
            </form>
        </div>
        <?php if ($my->id) { ?>

        <div class="user-top">
            <a class="circle-img modal"
               href="<?php echo JRoute::_('index.php?option=com_plot&task=profileedit.ajaxGetEditProfilePopupHtml'); ?>"  
               rel="{size: {x: 744, y: 500}, handler:'iframe', iframeOptions: {scrolling: 'yes'}}">
                <?php if ($friends_count) { ?>
                    <b class="friends"><?php echo $friends_count; ?></b>
                <?php } ?>
                <img id="plot-my-avatar" src="<?php echo $my->getSquareAvatarUrl(); ?>" alt="<?php echo $my->name; ?>">
            </a>

            <?php if ($user->id == $my->id) { ?>
                <button class="arrow-right" onclick="window.location.href = '<?php echo JRoute::_('index.php?option=com_plot&task=registration.logout'); ?>';"><?php echo JText::_('MOD_PLOT_HEADER_EXIT'); ?></button>
            <?php } else { ?>
                <?php if ($view == 'profile' || $view == 'photos') { ?>
                <button title="Вернуться к себе" class="settings" onclick="window.location.href = '<?php echo JRoute::_('index.php?option=com_plot&view=profile&id='.$my->id); ?>';"><svg xml:base="<?php echo $_SERVER['REQUEST_URI'];?>" viewBox="0 0 459 459"><use xlink:href="#reply"></use></svg></button>
                <?php } ?>
            <?php }
            ?>

        </div>
        <?php } ?>
    </div>
</header>
