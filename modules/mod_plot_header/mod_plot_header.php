<?php
defined('_JEXEC') or die;
require_once __DIR__ . '/helper.php';

$app = JFactory::getApplication();
$templateUrl = JUri::root() . 'templates/' . $app->getTemplate() . '/';

$view = JRequest::getVar('view', '');

$id = JRequest::getInt('id', 0);
if ($view == 'profile' || $view == 'photos') {
    if ($id) {
        $user = plotUser::factory($id);
    } else {
        $user = plotUser::factory();
    }
} else {
    $user = plotUser::factory();
}

$my = plotUser::factory();
$my->level = $my->getLevel();

# get count friends
$db = JFactory::getDbo();
$query = 'SELECT COALESCE(count(`id`), 0) FROM `#__social_friends` '.'WHERE `target_id`='.$db->quote($my->id).' '.'AND `state`='.$db->quote(SOCIAL_FRIENDS_STATE_PENDING);
$friends_count = $db->setQuery($query)->loadResult();

if ($my->isParent()) {
    require JModuleHelper::getLayoutPath('mod_plot_header', 'default_parent');
} else {
    require JModuleHelper::getLayoutPath('mod_plot_header', 'default');
}
