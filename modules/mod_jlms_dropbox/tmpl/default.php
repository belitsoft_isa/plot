<?php
// no direct access
defined('_JEXEC') or die;

if(count($dropboxs)){
?>
<ul class="menu<?php echo $class_sfx; ?>">
	<?php
	foreach($dropboxs as $d){
		$link = 'index.php?option=com_joomla_lms&task=dropbox&id='.$d->course_id;
		?>
		<li>
			<a href="<?php echo JRoute::_($link);?>">
				<span><?php echo $d->drp_name;?></span>
			</a>
		</li>
		<?php
	}
	?>
</ul>
<?php
}