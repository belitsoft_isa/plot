<?php
// no direct access
defined('_JEXEC') or die;

if(count($announcements)){
?>
<ul class="menu<?php echo $class_sfx; ?>">
	<?php
	foreach($announcements as $a){
		$link = 'index.php?option=com_joomla_lms&task=agenda&id='.$a->course_id.'&agenda_id='.$a->agenda_id.'&date='.date("Y-m", strtotime($a->start_date)).'#anc'.$a->agenda_id.'-'.date("Y-m", strtotime($a->start_date));
		?>
		<li>
			<a href="<?php echo JRoute::_($link);?>">
				<span><?php echo $a->title;?></span>
			</a>
		</li>
		<?php
	}
	?>
</ul>
<?php
}