<?php
defined('_JEXEC') or die;

require_once(dirname(__FILE__) . '/helper.php');

$helper = new SocialactionsHelper($_SERVER['REQUEST_URI']);

$network['facebook'] = $helper->formatNumber($helper->get('facebook'));
$network['odnoklassniki_ru'] = $helper->formatNumber($helper->get('ok'));
$network['vk'] = $helper->formatNumber($helper->get('vk'));

JFactory::getDocument()->addScript(JUri::root() . 'modules/mod_mnz_socialactions/tmpl/social.js');
JFactory::getDocument()->addScriptDeclaration('appendData('.json_encode($network).');');

require JModuleHelper::getLayoutPath('mod_mnz_socialactions', $params->get('layout', 'default'));