/**
 * Created by drosha on 25.05.15.
 */
appendData = function (socialData) {
	jQuery(document).ready(function () {
		var triggered = 0;
		// at4-share-count-anchor
		jQuery('body').on('DOMSubtreeModified', function(a,b,c){
			if (triggered == 0 && jQuery('#at4-share').size() == 1) {
				triggered = 1;
				jQuery('#at4-share > a').addClass('at4-share-count-anchor');
				for (key in socialData) {
					jQuery('<div />').html('<span class="at4-share-count">'+socialData[key]+'</span>').addClass('at4-share-count-container').insertAfter('.aticon-' + key);
				}
			}
		});
	});
};