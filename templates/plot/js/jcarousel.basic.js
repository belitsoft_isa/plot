(function($) {
    $(function() {
        $('.new-event .jcarousel, .my-progress .my-courses .jcarousel, .progress-history .my-courses .jcarousel')
            .on('jcarousel:create jcarousel:reload', function() {
                var element = $(this),
                    width = element.innerWidth()*0.32,//32% ширина li в случае, если выводится по 3 элемента
                    margin=element.innerWidth()*0.02;//2% значение margin в случае


                // This shows 1 item at a time.
                // Divide `width` to the number of items you want to display,
                // eg. `width = width / 3` to display 3 items at a time.
                element.jcarousel('items').css('width', width + 'px');
                element.jcarousel('items').css('margin-right', margin + 'px');
        })
        $('.my-progress .my-books .jcarousel, .progress-history .my-books .jcarousel')
            .on('jcarousel:create jcarousel:reload', function() {
                var element = $(this),
                    width = element.innerWidth()*0.15,//15% ширина li в случае, если выводится по 6 элементов
                    margin=element.innerWidth()*0.02;//2% значение margin

                // This shows 1 item at a time.
                // Divide `width` to the number of items you want to display,
                // eg. `width = width / 3` to display 3 items at a time.
                element.jcarousel('items').css('width', width + 'px');
                element.jcarousel('items').css('margin-right', margin + 'px');
        })
        $('.my-progress  .my-certificates .jcarousel, .progress-history  .my-certificates .jcarousel, .parent-library .jcarousel, #parent-read .jcarousel')
            .on('jcarousel:create jcarousel:reload', function() {
                var element = $(this),
                    width = element.innerWidth()*0.18,//18% ширина li в случае, если выводится по 5 элементов
                    margin=(element.innerWidth()*0.025)-1;//2.5% значение margin

                // This shows 1 item at a time.
                // Divide `width` to the number of items you want to display,
                // eg. `width = width / 3` to display 3 items at a time.
                element.jcarousel('items').css('width', width + 'px');
                element.jcarousel('items').css('margin-right', margin + 'px');
        });

        // parent-all-videos
        // #parent-all-portfolio .jcarousel,
        $('#parent-all-event .jcarousel, #parent-all-progress .jcarousel, #parent-courses-jcarousel .jcarousel, #parent-all-photos .jcarousel, .checkbox-interest .jcarousel, #parent-all-videos .jcarousel, #parent-studied .jcarousel, #parent-certificates .jcarousel, #parent-all-meetings .jcarousel, #child-programs-list .jcarousel, #parent-programs-list .jcarousel')
            .on('jcarousel:create jcarousel:reload', function() {
                var element = $(this),
                    width = element.innerWidth()*0.235,//23.5% ширина li в случае, если выводится по 4 элемента
                    margin=element.innerWidth()*0.02;//2% значение margin в случае

                // This shows 1 item at a time.
                // Divide `width` to the number of items you want to display,
                // eg. `width = width / 3` to display 3 items at a time.
                element.jcarousel('items').css('width', width + 'px');
                element.jcarousel('items').css('margin-right', margin + 'px');
            });

        $('.new-event .jcarousel, .my-progress .jcarousel, .progress-history .jcarousel, #parent-all-event .jcarousel, #parent-all-progress .jcarousel, #parent-courses-jcarousel .jcarousel, #parent-all-photos .jcarousel, .checkbox-interest .jcarousel, #parent-all-videos .jcarousel, .parent-library .jcarousel, #parent-read .jcarousel, #parent-studied .jcarousel,#parent-certificates .jcarousel, #parent-all-meetings .jcarousel, #child-programs-list .jcarousel, #parent-programs-list .jcarousel').jcarousel();
        $('.jcarousel-control-prev')
        .on('jcarouselcontrol:active', function() {
        $(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function() {
        $(this).addClass('inactive');
        })
        .jcarouselControl({
        target: '-=3'
        });
        $('.jcarousel-control-next')
        .on('jcarouselcontrol:active', function() {
        $(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function() {
        $(this).addClass('inactive');
        })
        .jcarouselControl({
        target: '+=3'
        });
        $('.jcarousel-pagination').jcarouselPagination({
            animation: 'slow',
            'perPage': 4,
            item: function(page) {
                return '<a href="#' + page + '"></a>';
            }
        });

        $('.jcarousel-pagination')
        .on('jcarouselpagination:active', 'a', function() {
            $(this).addClass('active');
        })
        .on('jcarouselpagination:inactive', 'a', function() {
            $(this).removeClass('active');
        })
        .jcarouselPagination();

    });
})(jQuery);
