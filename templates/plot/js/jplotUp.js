window.jPlotUp = window.jPlotUp || {};

jPlotUp.Arrow = {
    initialize: function(elId){
        if (jQuery('.main-wrap-overflow').scrollTop() > 120) {
            window.document.getElementById(elId).style.display = 'block';
            jPlotUp.Arrow.upElementId = elId;
            jPlotUp.Arrow.isVisible = true;
        } else {
            window.document.getElementById(elId).style.display = 'none';
            jPlotUp.Arrow.upElementId = elId;
            jPlotUp.Arrow.isVisible = false;
        }
        jQuery('#'+elId).click(function(){
            jPlotUp.Arrow.GoToTop();
        });

        jQuery('.main-wrap-overflow').scroll(function(){
            jPlotUp.Arrow.ShowOrHide(this, elId);
        });
    },
    GoToTop: function(){
        jQuery(".main-wrap-overflow").animate({
            scrollTop: 0
        }, 400, function(){
            jQuery('.main-wrap-overflow').focus();
        });
    },
    ShowOrHide: function(that, elId){
        if (jQuery('.main-wrap-overflow').scrollTop() > 120 && !jPlotUp.Arrow.isVisible) {
            jQuery('#' + elId).fadeIn();
            jPlotUp.Arrow.isVisible = true;
        } else if (jQuery('.main-wrap-overflow').scrollTop() <= 120 && jPlotUp.Arrow.isVisible) {
            jQuery('#' + elId).fadeOut();
            jPlotUp.Arrow.isVisible = false;
        }
    }
};

