jPlot = {
    
    helper: {
        getWordEnding: function (value, word0, word1, word2) {
            var match1 = new RegExp('1\d$');
            var match2 = new RegExp('1$');
            var match3 = new RegExp('(2|3|4)$');
            if (match1.test(value)) {
                return value + ' ' + word2;
            } else if (match2.test(value)) {
                return value + ' ' + word0;
            } else if (match3.test(value)) {
                return value + ' ' + word1;
            } else  {
                return value + ' ' + word2;
            }
        },
        cropString: function (str, symbols) {
            if (str.length <= symbols) {
                return str;
            }
            return str.substring(0, symbols)+'...';
        },


        getNumEnding:function(iNumber, aEndings)
        {
            var sEnding, i;
            iNumber = iNumber % 100;
            if (iNumber>=11 && iNumber<=19) {
                sEnding=aEndings[2];
            }
            else {
                i = iNumber % 10;
                switch (i)
                {
                    case (1): sEnding = aEndings[0]; break;
                    case (2):
                    case (3):
                    case (4): sEnding = aEndings[1]; break;
                    default: sEnding = aEndings[2];
                }
            }
            return iNumber+' '+sEnding;
        },
        declOfNum:function(number, titles){
            cases = [2, 0, 1, 1, 1, 2];
            return number+' '+titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
        }

    },
    
    showMessage: function(str) {
        SqueezeBox.open(str, {handler: "string", size: {x:300, y:150}});
        jQuery('#sbox-window #sbox-content').html( str );
    },
    
    showEnqueuedMessage: function(html) {
        var messagesHtml;
        if (html && html !== 'undefined') {
            messagesHtml = html;
        } else {
            messagesHtml = jQuery('#system-message .alert > div').html();
        }
        if (messagesHtml !== 'undefined' && messagesHtml) {
            jQuery('body').append('<div id="enqueued-message">'+messagesHtml+'</div>');
            SqueezeBox.open($('enqueued-message'), {handler: 'adopt', size: {x:300, y:150}});
        }
    },
    
    addSocialBlockToSpecifiedContainer: function(containerClass, data) {

        jQuery(document).ready(function () {
            var triggered = 0,
                socialData = jQuery.parseJSON(data),
                flag_at4_exist=0;
            jQuery('body').on('DOMSubtreeModified', function(a,b,c){
                if (triggered == 0 && jQuery('#at4-share').size() == 1) {
                    triggered = 1;
                    jQuery('#at4-share > a').addClass('at4-share-count-anchor');
                    for (key in socialData) {
                        jQuery('<div />').html('<span class="at4-share-count">'+socialData[key]+'</span>').addClass('at4-share-count-container').insertAfter('.aticon-' + key);
                    }
                }



            });
            setTimeout(function move_social_buttons_at4() {
                if (document.body.contains(document.querySelector('.at4-share-outer-right'))) {
                    jQuery(".at4-share-outer-right").appendTo(".main-wrap-overflow");
                    jQuery("#at4-thankyou").appendTo(".main-wrap-overflow");
                } else {
                    setTimeout(move_social_buttons_at4, 50);
                }
            }, 50);


        });

    },
    
    validate: {
        text: function(){
            
        },
        email: function(email) 
        {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
    },
    
    youtube: {
        isValidUrl: function(url){
            var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
            return (url.match(p)) ? RegExp.$1 : false;
        }
    },
    
    profile: {
        showPopupDataAndRefreshSelectsAndScrollpanes: function(){
            jQuery('#sbox-content', window.parent.document).show();
            jQuery('#friends-filter-tags').selectmenu('refresh');
            jQuery('#plot-tags').selectmenu('refresh');
            jQuery('.scroll-pane').jScrollPane();
        }
    }

};
