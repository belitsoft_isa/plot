<?php
defined('_JEXEC') or die;

require_once JPATH_BASE.'/administrator/components/com_plot/entities/phtml.php';
require_once JPATH_BASE.'/components/com_plot/helpers/sociationshelper.php';
PHtml::_('behavior.modal');

$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

$menuIdFromLink = JFactory::getDbo()->setQuery("SELECT `id` FROM `#__menu` WHERE `link` = 'index.php?option=".JRequest::getString('option')."&view=".JRequest::getString('view')."'")->loadResult();
if (!$menuIdFromLink) {
    $menuIdFromLink = 0;
}
$app->getMenu()->setActive( $menuIdFromLink );
$activeMenu = $app->getMenu()->getActive();
if ($activeMenu) {
    $doc->setMetaData('description', $activeMenu->params->get('menu-meta_description'));
    $doc->setMetaData('keywords', $activeMenu->params->get('menu-meta_keywords'));
    $doc->setTitle($activeMenu->title);
}

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');
$my = plotUser::factory();
$refererUrl = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : JRoute::_('index.php?option=com_plot&view=profile&id='.$my->id);

if($task == "edit" || $layout == "form" ) {
    $fullWidth = 1;
} else {
    $fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript('templates/' .$this->template. '/js/template.js');
$doc->addScript('templates/' .$this->template. '/js/jquery-ui.min.js');
$doc->addScript('templates/' .$this->template. '/js/jplot.js');


// Add Stylesheets
$doc->addStyleSheet('templates/'.$this->template.'/css/style.css');


// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

// Add current user information
$user = JFactory::getUser();

// require plot component files
$entitiesFiles = glob(JPATH_ADMINISTRATOR.'/components/com_plot/entities'.'/*.php');
foreach ($entitiesFiles as $file) {
    require_once $file;
}

// Adjusting content width
if ($this->countModules('position-7') && $this->countModules('position-8')) {
	$span = "span6";
} elseif ($this->countModules('position-7') && !$this->countModules('position-8')) {
	$span = "span9";
} elseif (!$this->countModules('position-7') && $this->countModules('position-8')) {
	$span = "span9";
} else{
	$span = "span12";
}

// Logo file or site title param
if ($this->params->get('logoFile')) {
	$logo = '<img src="'. JUri::root() . $this->params->get('logoFile') .'" alt="'. $sitename .'" />';
} elseif ($this->params->get('sitetitle')) {
	$logo = '<span class="site-title" title="'. $sitename .'">'. htmlspecialchars($this->params->get('sitetitle')) .'</span>';
} else {
	$logo = '<span class="site-title" title="'. $sitename .'">'. $sitename .'</span>';
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>

<!--    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>-->
    <meta name="viewport" content="width=device-width"/>
    <jdoc:include type="head" />
    <?php
    // Use of Google Font
    if ($this->params->get('googleFont')) {	?>
    <style type="text/css">
        h1,h2,h3,h4,h5,h6,.site-title{
            font-family: '<?php echo str_replace('+', ' ', $this->params->get('googleFontName'));?>', sans-serif;
        }
    </style>
    <?php } ?>
    <?php
    // Template color
    if ($this->params->get('templateColor')) { ?>
    <style type="text/css">
        body.site {
                border-top: 3px solid <?php echo $this->params->get('templateColor');?>;
                background-color: <?php echo $this->params->get('templateBackgroundColor');?>
        } a {
                color: <?php echo $this->params->get('templateColor');?>;
        }
        .navbar-inner, .nav-list > .active > a, .nav-list > .active > a:hover, .dropdown-menu li > a:hover, .dropdown-menu .active > a, .dropdown-menu .active > a:hover, .nav-pills > .active > a, .nav-pills > .active > a:hover,
        .btn-primary {
            background: <?php echo $this->params->get('templateColor');?>;
        }
        .navbar-inner {
            -moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
            -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
            box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
        }
    </style>
    <?php } ?>
    <!--[if lt IE 9]>
        <script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
    <![endif]-->
<?php
$helper = new SocialactionsHelper($_SERVER['REQUEST_URI']);

$network['facebook'] = $helper->formatNumber($helper->get('facebook'));
$network['odnoklassniki_ru'] = $helper->formatNumber($helper->get('ok'));
$network['vk'] = $helper->formatNumber($helper->get('vk'));
?>
    <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/plot/js/svgsprite.js"></script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-557eb77ce6bae614" async="async"></script>
    <script type="text/javascript" src="<?php echo  JUri::root().'templates/plot/js/jplotUp.js'; ?>" ></script>
    <script type="text/javascript">

        function isMobile() {


            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                return true;
            }

            if (sessionStorage.desktop) // desktop storage
                return false;
            else if (localStorage.mobile) // mobile storage
                return true;

            // nothing found.. assume desktop
            jQuery("html").css("height","100%");
            return false;
        }


        jQuery(document).ready(function(){
            jPlot.addSocialBlockToSpecifiedContainer('.social-share','<?php echo json_encode($network); ?>');
            <?php if ($app->getMessageQueue()) { ?>
            jPlot.showEnqueuedMessage();
            <?php } ?>
            jPlotUp.Arrow.initialize('positionCameras');


            if(isMobile()){
                var cometchat_interval_id= setInterval(function() {
                    if(document.getElementById('cometchat')){
                        jQuery('#cometchat').show();
                        jQuery('#cometchat_base').addClass("mobile");
                        clearInterval(cometchat_interval_id);
                    }
                }, 1000);
            }

        });
    </script>

    <script type="text/javascript" src="<?php echo  JUri::root();?>share42/share42.js" async="async" ></script>
    <link type="text/css" href="/cometchat/cometchatcss.php" rel="stylesheet" charset="utf-8">
    <script type="text/javascript" src="/cometchat/cometchatjs.php" charset="utf-8" async="async" ></script>
</head>

<body class="<?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '');
?>" >
<div class="main-wrap-overflow">
    <div class="main-content">
        <div id="svg-sprite"></div>
        <script type="text/javascript">
            document.getElementById("svg-sprite").innerHTML = SVG_SPRITE;//don't move to <head> because Safari IOS6 don't work
        </script>

        <?php if ($option == 'com_plot' && $view != 'k2article' && $view != 'contacts') { ?>

        <div class="hidden system-message-container">
            <jdoc:include type="message" />
        </div>

        <jdoc:include type="component" />

        <?php } else { ?>

            <?php if (!$my->isParent()) { ?>

                <?php # <editor-fold defaultstate="collapsed" desc="MAIN"> ?>
                <main class="child-library child-txt">
                    <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_header') ); ?>
                    <div class="wrap main-wrap child-library">
                        <div class="aside-left child-library"></div>
                        <div class="aside-right child-library"></div>
                        <section class="child-library child-txt">
                            <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_top_menu') ); ?>
                            <article class="text-field">
                                <a href="<?php echo $refererUrl; ?>" class="link-back hover-shadow">
                                    Вернуться
                                    <svg class="butterfly" viewBox="0 0 22.9 46" preserveAspectRatio="xMinYMin meet">
                                        <use xlink:href="#butterfly"></use>
                                    </svg>
                                </a>
                                <jdoc:include type="component" />
                            </article>
                        </section>
                    </div>
                </main>
                <?php # </editor-fold> ?>

            <?php } else { ?>


                <?php # <editor-fold defaultstate="collapsed" desc="TOP"> ?>
                <div class="top parent-profile">
                    <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_header') ); ?>
                    <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_top_menu') ); ?>
                </div>
                <?php # </editor-fold> ?>
                <?php # <editor-fold defaultstate="collapsed" desc="MAIN"> ?>
                <main class="parent-profile parent-txt">
                    <div class="wrap main-wrap">
                        <div class="aside-left parent-profile"></div>
                        <div class="aside-right parent-profile"></div>
                        <section class="parent-txt">
                            <article class="text-field">
                                <jdoc:include type="component" />
                            </article>
                        </section>
                    </div>
                </main>
                <?php # </editor-fold> ?>

            <?php } ?>
        <?php } ?>

        <?php if (($option == 'com_plot' && $view == 'k2article') /*|| ($option == 'com_easysocial' && $view == 'groups')*/ ) { ?>
            <div class="social-share-wrap">
                <div class="social-share"></div>
            </div>
        <?php } ?>
        <div id="positionCameras"><ul><li title="Move Up" class="cameraLeft" id="cameraUp"></li></ul></div>
        <div class="push"></div>
    </div>
    <?php echo JModuleHelper::renderModule( JModuleHelper::getModule('mod_plot_footer') ); ?>
 </div>

    <!--    before closed body script for reformal.ru recall system-->
    <script type="text/javascript">
        var reformalOptions = {
            project_id: 848940,
            project_host: "naplotu.reformal.ru",
            force_new_window: true,
            tab_orientation: "left",
            tab_indent: "50%",
            tab_bg_color: "#F05A00",
            tab_border_color: "#FFFFFF",
            tab_image_url: "http://tab.reformal.ru/T9GC0LfRi9Cy0Ysg0Lgg0L%252FRgNC10LTQu9C%252B0LbQtdC90LjRjw==/FFFFFF/2a94cfe6511106e7a48d0af3904e3090/left/1/tab.png",
            tab_border_width: 2
        };
        (function() {
            var script = document.createElement('script');
            script.type = 'text/javascript'; script.async = true;
            script.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'media.reformal.ru/widgets/v3/reformal.js';
            document.getElementsByTagName('head')[0].appendChild(script);
        })();
    </script>
    <noscript>
            <a href="http://reformal.ru"><img src="http://media.reformal.ru/reformal.png" /></a>
            <a href="http://naplotu.reformal.ru">Oтзывы и предложения для Naplotu.com</a>
    </noscript>
<jdoc:include type="modules" name="debug" style="none" />



</body>
</html>
