module.exports = function(grunt) {

    // 1. Вся настройка находится здесь
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        //Настройки различных модулей GruntJS, их нужно предварительно установить через менеджер пакетов npm, или                           добавить в файл package.json перед запуском npm install
        concat: {
            dist: {
                src: [
                    'templates/plot/js/*.js' // Все JS в папке libs
                ],
                dest: 'templates/plot/js/global.js'
            },
            core: {
                src: [
                    'components/com_plot/views/profile/tmpl/default.js',
                    'components/com_plot/views/profile/tmpl/svgsprite.js'
                ],
                dest: 'components/com_plot/views/profile/tmpl/global.js'
            }
        },
        uglify: {
            build: {
                src: 'templates/plot/js/global.js',
                dest: 'templates/plot/js/global.min.js'
            }
        },
        watch: {
            scripts: {
                files: ['templates/plot/js/*.js'],
                tasks: ['concat', 'uglify'],
                options: {
                    spawn: false
                }
            },
            sass: {
                files: ['e:/project/plot/css/scss/*.scss'],
                tasks: ['compass'],
                options: {
                    spawn: false
                }
            },
            options: {
                livereload: true
            },
            configFiles: {
                files: [ 'Gruntfile.js'],
                options: {
                    reload: true
                }
            },
            elements: {
                files: [
                    'components/com_plot/views/profile/tmpl/default.svgdata'
                ],
                tasks: ['elements', 'concat:core', 'clean:file']
            }
        },
        compass: {
            dev: {
                options: {
                    sassDir: 'e:/project/plot/css/scss/',
                    cssDir: 'templates/plot/css',
                    imagesPath: 'templates/plot/img/',
                    generatedImagesPath: 'templates/plot/img/',
                    httpImagesPath: '../img/',
                    httpGeneratedImagesPath: '../img/',
                    noLineComments: false,
                    outputStyle: 'expanded',
                    asset_cache_buster: false
                }
            }
        },
        clean: {
            file: "components/com_plot/views/profile/tmpl/svgsprite.js"
            //,folderTwo: ["assetsOne/js/", "assetsTwo/js/"]
        },
        svgmin: {
            options: {
                plugins: [
                    {
                        removeViewBox: false
                    }, {
                        removeUselessStrokeAndFill: false
                    }
                ]
            },
            dist: {
                files: {
                    'dist/unicorn.svg': 'app/unicorn.svg'
                }
            }
        },
        svg2string: {
            elements: {
                options: {
                    wrapLines: false,
                    symbols: 'symbols'
                },
                files: {
                    'templates/plot/js/svgsprite.js': [
                        'templates/plot/svgsprite.svg'
                    ]
                }
            }
        },
        weinre: {
            dev: {
                options: {
                    httpPort: 8080,
                    boundHost: 'localhost',
                    verbose: false,
                    debug: false,
                    readTimeout: 5,
                    deathTimeout: 15
                }
            }
        }
    });


        //sass: {
        //    dev: {
        //        options: {
        //            style: 'expanded',
        //            //banner: '<%= tag.banner %>',
        //            compass: true       //импортируем настройки файла конфигурации compass config.rb
        //        },
        //        files: {
        //            'e:/project/plot/css/scss/style.css': 'e:/project/plot/css/scss/style.scss'
        //        }
        //    },
        //    dist: {
        //        options: {
        //            style: 'compressed',
        //            compass: true  //импортируем настройки файла конфигурации compass config.rb
        //        },
        //        files: {
        //            'templates/plot/css/style.css': 'e:/project/plot/css/scss/style.scss'
        //        }
        //    }
        //},


    // 3. Тут мы указываем Grunt, что хотим использовать этот плагин
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-svg2string');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-svgmin');
    grunt.loadNpmTasks('grunt-weinre');


    // 4. Указываем, какие задачи выполняются, когда мы вводим «grunt» или "grunt svg" в терминале
    grunt.registerTask('default', ['watch', 'compass', 'concat', 'sass']);
    grunt.registerTask('svg', ['svg2string']);
    grunt.registerTask('weinre', ['weinre']);

};